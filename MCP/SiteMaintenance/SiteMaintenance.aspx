﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="SiteMaintenance.aspx.cs" Inherits="SiteMaintenance_SiteMaintenance" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
<style>
    #ContentPlaceHolder1_divAllSites table td input {margin-right: 10px;}
</style>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Site Maintenance</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <h3>Site Maintenance</h3>
                    <p></p>
                    <div class="form-group clearfix">
                        <div class="col-md-12 col-sm-10">
                            <br />
                            <div id="divMaintenanceMsg" runat="server">
                                <h2>Update message with HTML tags</h2>
                                <asp:TextBox id="txtMaintenanceMsg" runat="server" Columns="100" TextMode="MultiLine" rows="8"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="rfvMaintenanceMsg" ErrorMessage="( Required )" CssClass="text-warning" ValidationGroup="valOfflineMsg" ControlToValidate="txtMaintenanceMsg"></asp:RequiredFieldValidator>
                            </div>

                            <div id="divAllSites" runat="server" visible="false">
                            <asp:Literal ID="ltrlHeaderMsg" runat="server"> </asp:Literal>
                                <br />
                                <asp:CheckBox id="checkuncheck" runat="server" AutoPostBack="true" OnCheckedChanged="checkuncheck_CheckedChanged" />&nbsp;&nbsp;<label> Check/Uncheck All</label>
                                <br />
                                <asp:CheckBoxList ID="chkAllSites" CellPadding="10" CellSpacing="10" runat="server" OnDataBound="chkAllSites_DataBound" >

                                </asp:CheckBoxList>
                            </div>

                        </div>
                        
                            <div class="button_section">
                               
                                <ul>
                                    <li>
                                        <asp:Button ID="cmdSave" runat="server" CssClass="btn" Text="Save" ValidationGroup="valOfflineMsg" OnClick="cmdSave_Click" />
                                    </li>
                                    
                                </ul>
                            </div>
                        
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

