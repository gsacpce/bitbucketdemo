﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using Microsoft.Security.Application;
using System.IO;
using System.Text;
using System.Configuration;

public partial class SiteMaintenance_SiteMaintenance : System.Web.UI.Page
{
    public string strMsgFilePath;
    public bool flgSitesOffline = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        /*Sachin Chauhan Start : 27-11-15 : If All Sites are down then display*/

        if (File.Exists(HttpContext.Current.Server.MapPath("~/SiteMaintenance/AllSitesdown.txt")) )
        {
            if (!IsPostBack)
            {
                divMaintenanceMsg.Visible = false;
                divAllSites.Visible = true;
                BindAllStores();
                cmdSave.Text = "Offline Selected Site(s).";
            }
            
            if ( !cmdSave.Text.Contains("online") )
                cmdSave.Attributes.Add("onclick", "javascript:confirm('Are you sure you want to bring selected sites offline')");

            string strAllSiteDownFilePath = HttpContext.Current.Server.MapPath("~/SiteMaintenance/AllSitesdown.txt");
            //IEnumerable<String> lstAllSiteDownInfo = File.ReadLines(strAllSiteDownFilePath) ;
            
            /*Read AllSitedown.txt File to see how many sites are currently offline*/
            string[] strAllSiteOfflineData = File.ReadAllLines(strAllSiteDownFilePath);
            for (int i = 0; i < strAllSiteOfflineData.Length ; i++)
            {
                if ( strAllSiteOfflineData[i].Contains("offline") )
                {
                    ltrlHeaderMsg.Text = "<h2>Select Site(s) from below list to go ONLINE</h2>";
                    cmdSave.Text = "Bring Selected Site(s) Online.";
                    cmdSave.Attributes.Add("onclick", "javascript:confirm('Are you sure you want to bring selected sites online')");
                    flgSitesOffline = true;
                    break;
                }
            }

            for (int i = 0; i < strAllSiteOfflineData.Length; i++)
            {
                if (strAllSiteOfflineData[i].Contains("online"))
                {
                    flgSitesOffline = false;
                    divMaintenanceMsg.Visible = true;
                    divAllSites.Visible = false;
                    cmdSave.Text = "Save";
                    strMsgFilePath = HttpContext.Current.Server.MapPath("~/SiteMaintenance/MessagePage.htm");
                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(GlobalFunctions.ReadFile(strMsgFilePath));
                    HtmlNode divOfflineMessage = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='divOfflineMessage']");
                    txtMaintenanceMsg.Text = divOfflineMessage.InnerHtml;
                    break;
                }
            }

            /*Checkmark all the site name checkboxes currently in offline mode*/
            if (flgSitesOffline)
            {
                for (int i = 1; i < strAllSiteOfflineData.Length; i++)
                {
                    string[] strStoreNamePath = strAllSiteOfflineData[i].Split('\\');
                    string strStoreName = strStoreNamePath[strStoreNamePath.Length - 1];

                    foreach (ListItem offLineSite in chkAllSites.Items)
                    {
                        if (offLineSite.Value.ToLower().Equals(strStoreName.ToLower()))
                        {
                            offLineSite.Selected = true;
                            flgSitesOffline = true;
                        }

                    }
                }
            }
            
            
                
        }
        else
        {
            /*Sachin Chauhan Start : 27-11-15 : Read existing site under mainteneance message from file*/
            divAllSites.Visible = false;
            strMsgFilePath = HttpContext.Current.Server.MapPath("~/SiteMaintenance/MessagePage.htm");
            if (!IsPostBack)
            {
                strMsgFilePath = HttpContext.Current.Server.MapPath("~/SiteMaintenance/MessagePage.htm");
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(GlobalFunctions.ReadFile(strMsgFilePath));
                HtmlNode divOfflineMessage = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='divOfflineMessage']");

                txtMaintenanceMsg.Text = divOfflineMessage.InnerHtml;
            }

        }
       
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
             #region Offline Message Save
                    if (!File.Exists(HttpContext.Current.Server.MapPath("~/SiteMaintenance/AllSitesdown.txt")))
                    {
                        if (txtMaintenanceMsg.Text.Trim().Equals(String.Empty))
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "The message can not be empty", AlertType.Warning);
                        else
                        {
                            try
                            {
                                /*Replace changed message back to message html page*/
                                HtmlDocument htmlDocument = new HtmlDocument();
                                htmlDocument.LoadHtml(GlobalFunctions.ReadFile(strMsgFilePath));
                                HtmlNode divOfflineMessage = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='divOfflineMessage']");

                                divOfflineMessage.InnerHtml = txtMaintenanceMsg.Text;
                                htmlDocument.Save(strMsgFilePath);

                                string[] msgFileDir = strMsgFilePath.Split('\\');
                                string strMsgFileDir = string.Empty;

                                for (int i = 0; i < msgFileDir.Length - 1; i++)
                                {
                                    strMsgFileDir += msgFileDir[i] + "\\";
                                }

                                /*Create empty file just to make sure system knows all or some of the sites are down*/
                                if (!File.Exists(strMsgFileDir + "AllSitesDown.Txt"))
                                {
                                    FileStream fs = null;
                                    using (fs = File.Create(strMsgFileDir + "AllSitesDown.Txt"))
                                    {
                                        fs.Close();
                                    }
                                }


                                GlobalFunctions.ShowModalAlertMessages(this.Page, "The message updated successfully", "SiteMaintenance.aspx", AlertType.Success);

                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error while updating message.", AlertType.Failure);
                            }
                        }

                    }  
                    #endregion

                    Exceptions.WriteInfoLog("Offline MSG saved");

             #region Site Offline Process
                    bool flgNoSiteSelected = true;
                    //Exceptions.WriteInfoLog("1" + flgNoSiteSelected);
                    if (!flgSitesOffline )
                    {
                        //Exceptions.WriteInfoLog("2" + flgSitesOffline);
                        if (File.Exists(HttpContext.Current.Server.MapPath("~/SiteMaintenance/AllSitesdown.txt")))
                        {
                            //Exceptions.WriteInfoLog("3" + HttpContext.Current.Server.MapPath("~/SiteMaintenance/AllSitesdown.txt"));
                            UserBE objUser = Session["User"] as UserBE;
                            string strMsgFilePath = HttpContext.Current.Server.MapPath("~/SiteMaintenance/MessagePage.htm");
                            //Exceptions.WriteInfoLog("4" + strMsgFilePath);
                            string[] msgFileDir = strMsgFilePath.Split('\\');
                            string strMsgFileDir = string.Empty;
                            //Exceptions.WriteInfoLog("5" + msgFileDir);
                            for (int i = 0; i < msgFileDir.Length - 1; i++)
                            {
                                strMsgFileDir += msgFileDir[i] + "\\";
                                //Exceptions.WriteInfoLog("6" + strMsgFileDir);
                            }

                            StringBuilder sbSiteNames = new StringBuilder();
                            foreach (ListItem chkSiteName in chkAllSites.Items)
                            {
                                //Exceptions.WriteInfoLog("7" + chkSiteName.Text);
                                if (chkSiteName.Selected)
                                {
                                    //Exceptions.WriteInfoLog("8" + chkSiteName.Text);
                                    sbSiteNames.AppendLine(objUser.EmailId + " has put below sites offline on : " + DateTime.Now);
                                    sbSiteNames.AppendLine(chkSiteName.Text);
                                    //Exceptions.WriteInfoLog("9" + chkSiteName.Text);
                                    //break;
                                }
                            }

                            //File.WriteAllText(HttpContext.Current.Server.MapPath("~/SiteMaintenance/AllSitesdown.txt"), sbSiteNames.ToString());

                            foreach (ListItem chkSiteName in chkAllSites.Items)
                            {
                                //Exceptions.WriteInfoLog("10" + chkSiteName.Text);
                                if (chkSiteName.Selected)
                                {
                                    try
                                    {
                                        //Exceptions.WriteInfoLog("10.1" + strMsgFilePath);
                                        
                                        /*Sachin Chauhan Start : 15 02 2015 : Commented below fixed path and read from confign file*/
                                        //string strSiteDirName = @"C:\inetpub\website\brandadditionv2.cwwws.com\" + chkSiteName.Value;
                                        string strSiteDirName = @"" + ConfigurationManager.AppSettings["SiteOfflineFilePath"] + "";
                                        //Exceptions.WriteInfoLog("11" + strSiteDirName);
                                        //Exceptions.WriteInfoLog(strMsgFilePath+" - - - "+strSiteDirName);
                                        /*Sachin Chauhan End : 15 02 2015*/
                                        File.Copy(strMsgFilePath, strSiteDirName +"" + chkSiteName.Text + "\\app_offline.htm", true);

                                        //Exceptions.WriteInfoLog("12" + strMsgFilePath);
                                        // Exceptions.WriteInfoLog("13" + strSiteDirName +"" + chkSiteName.Text + "\\app_offline.htm");
                                        sbSiteNames.AppendLine(strSiteDirName);
                                       // Exceptions.WriteInfoLog("14" + strSiteDirName);
                                        flgNoSiteSelected = false;
                                        Exceptions.WriteInfoLog("File Copied !!");
                                        
                                    }
                                    catch (Exception ex)
                                    {
                                        Exceptions.WriteExceptionLog(ex);
                                        //throw;
                                    }

                                }
                            }
                            File.WriteAllText(strMsgFileDir + "AllSitesDown.Txt", sbSiteNames.ToString());

                            Exceptions.WriteInfoLog(strMsgFileDir + "AllSitesDown.Txt"+ sbSiteNames.ToString());

                

                            if ( flgNoSiteSelected )
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "No site(s) selected to go OFFLINE.", AlertType.Warning);
                            else
                            {
                                cmdSave.Text = "Bring Selected Site(s) Online.";
                                cmdSave.Attributes.Add("onclick", "javascript:confirm('Are you sure you want to bring selected sites online')");
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "All selected site(s) are OFFLINE now.", AlertType.Success);
                            }                    
                        }
                    }
        
                    #endregion

                    Exceptions.WriteInfoLog("Sites went Offline");

             #region "Site Online Process"
                    if (flgSitesOffline )
                    {
                        string strAllSiteDownFilePath = HttpContext.Current.Server.MapPath("~/SiteMaintenance/AllSitesdown.txt");
                        string[] strAllSiteOfflineData = File.ReadAllLines(strAllSiteDownFilePath);
            
                        /*Delete Appoffline file from site directories*/
                        try
                        {
                            for (int i = 1; i < strAllSiteOfflineData.Length; i++)
                            {
                                string[] strStoreNamePath = strAllSiteOfflineData[i].Split('\\');
                                string strStoreName = strStoreNamePath[strStoreNamePath.Length - 1];
                                string strSiteOfflineFilePath = ConfigurationManager.AppSettings["SiteOfflineFilePath"] + "\\" +strAllSiteOfflineData[i] + "\\app_offline.htm";
                                Exceptions.WriteInfoLog("File to be deleted - " + strSiteOfflineFilePath);
                                if (File.Exists(strSiteOfflineFilePath))
                                {
                                    File.Delete(strSiteOfflineFilePath);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                        }

                        /*Uncheck sitenames*/            
                        foreach (ListItem chkSiteName in chkAllSites.Items)
                        {
                            if (chkSiteName.Selected)
                            {
                                chkSiteName.Selected = false;
                            }
                        }
            
                        /*Show offline message div. Hide site names checkbox list*/
                        divMaintenanceMsg.Visible = true;
                        divAllSites.Visible = false;

            
                        strMsgFilePath = HttpContext.Current.Server.MapPath("~/SiteMaintenance/MessagePage.htm");
                        HtmlDocument htmlDocument = new HtmlDocument();
                        htmlDocument.LoadHtml(GlobalFunctions.ReadFile(strMsgFilePath));
                        HtmlNode divOfflineMessage = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='divOfflineMessage']");
                        txtMaintenanceMsg.Text = divOfflineMessage.InnerHtml;
                        cmdSave.Text = "Save";

                        /*Delete recent log file*/
                        File.Delete(strAllSiteDownFilePath);

                        GlobalFunctions.ShowModalAlertMessages(this.Page, "All selected site(s) are ONLINE now.", AlertType.Success);
                    }
                    #endregion                    

                    Exceptions.WriteInfoLog("Sites are online now");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            //throw;
        }

       
    }

    private void BindAllStores()
    {
        try
        {
            List<StoreBE> StoreCollection = StoreBL.GetAllStoreDetails();
            chkAllSites.DataSource = StoreCollection;
            chkAllSites.DataTextField = "StoreName";
            chkAllSites.DataValueField = "StoreName";
            chkAllSites.DataBind();

            //ltrlHeaderMsg.Text = "<h2>Select Site(s) from below list to go OFFLINE</h2>";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }



    protected void chkAllSites_DataBound(object sender, EventArgs e)
    {
        
    }
    protected void checkuncheck_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox  checkuncheck = sender as CheckBox ;
        
        foreach (ListItem cb in chkAllSites.Items)
	    {
            cb.Selected = checkuncheck.Checked;
	    }
    }
}