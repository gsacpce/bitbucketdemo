﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master"
    AutoEventWireup="true" CodeFile="TeamSupportAutoLogin.aspx.cs" Inherits="Admin_TeamSupportAutoLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <script src="JS/aes.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <iframe id="Iframe" width="100%" height="700"></iframe>
    <script type="text/javascript">
        $(document).ready(function () {
            var encrypted = CryptoJS.AES.encrypt(Date.now() + ",<%=LoginUserEmail %>", "4fd59369-bb55-4528-85d8-4a3b6ab98eff");
            $("#Iframe").attr("src", "https://portal.na2.teamsupport.com/login.aspx?Organizationid=986864&authtoken=" + encrypted + "");
        });
    </script>
</asp:Content>
