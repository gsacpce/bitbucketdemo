﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login_Login : System.Web.UI.Page
{
    public string host = GlobalFunctions.GetImageVirtualPath();
    int LoginWrongAttempts = Convert.ToInt16(ConfigurationManager.AppSettings["LoginWrongAttempts"]);
    int LoginWaitTime = Convert.ToInt16(ConfigurationManager.AppSettings["LoginWaitTime"]);
    //BasePage objBase = new BasePage();

    /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    /*Sachin Chauhan End : 22 02 2016 */

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 18-08-15
    /// Scope   : Page_PreInit event of the page
    /// Modified by : Sachin Chauhan
    /// Modified Date : 22 02 2016
    /// Modify Scope : Added CSRF attack prevention code
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (host.Contains("localhost"))
        {
            // no csrf for localhost
        }
        else
        {
            /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
            #region "CSRF prevention code"
            try
            {
                //First, check for the existence of the Anti-XSS cookie
                var requestCookie = Request.Cookies[AntiXsrfTokenKey];
                Guid requestCookieGuidValue;

                //If the CSRF cookie is found, parse the token from the cookie.
                //Then, set the global page variable and view state user
                //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
                //method.
                if (requestCookie != null
                && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
                {
                    //Set the global token variable so the cookie value can be
                    //validated against the value in the view state form field in
                    //the Page.PreLoad method.
                    _antiXsrfTokenValue = requestCookie.Value;

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;
                }
                //If the CSRF cookie is not found, then this is a new session.
                else
                {
                    //Generate a new Anti-XSRF token
                    _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;

                    //Create the non-persistent CSRF cookie
                    var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                    {
                        //Set the HttpOnly property to prevent the cookie from
                        //being accessed by client side script
                        HttpOnly = true,

                        //Add the Anti-XSRF token to the cookie value
                        Value = _antiXsrfTokenValue
                    };

                    //If we are using SSL, the cookie should be set to secure to
                    //prevent it from being sent over HTTP connections
                    if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                        responseCookie.Secure = true;

                    //Add the CSRF cookie to the response
                    Response.Cookies.Set(responseCookie);
                }

                Page.PreLoad += Page_PreLoad;
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            #endregion
            /*Sachin Chauhan End : 22 02 2016*/
        }
    }

    /// <summary>
    /// Author  : Sachin Chauhan
    /// Date : 22 02 2016
    /// Scope : Added CSRF attack prevention code
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    protected void Page_PreLoad(object sender, EventArgs e)
    {

        if (host.Contains("localhost"))
        {
            // no csrf for localhost
        }
        else
        {
            //During the initial page load, add the Anti-XSRF token and user
            //name to the ViewState
            if (!IsPostBack)
            {
                //Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

                //If a user name is assigned, set the user name
                //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                if (Session["ContextUserGUID"] == null)
                    Session["ContextuserGUID"] = Guid.NewGuid().ToString();

                ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
            }
            //During all subsequent post backs to the page, the token value from
            //the cookie should be validated against the token in the view state
            //form field. Additionally user name should be compared to the
            //authenticated users name
            else
            {
                //Validate the Anti-XSRF token
                //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())

                    Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
            }
        }
    }

    /// <summary>
    /// Author  : Prajwal Hegde
    /// Date    : 28-07-16
    /// Scope   : Page Load Function
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns>Void</returns>
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Author  : Prajwal Hegde
    /// Date    : 28-07-16
    /// Scope   : Submit Button Function
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns>Void</returns>

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (dvCaptcha.Visible)
            {
                string theCode = Session["MCPCaptchaText"].ToString();
                if (Sanitizer.GetSafeHtmlFragment(txtCaptcha.Text.ToLower()) != theCode.ToLower())
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Invalid captcha text.", AlertType.Warning);
                    txtCaptcha.Text = "";
                    return;
                }
            }
            string pass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(txtPassword.Text.Trim()), "SHA512", null);
            UserBE objBE = new UserBE();
            objBE.EmailId = Sanitizer.GetSafeHtmlFragment(txtUserName.Text.Trim());
            objBE.Password = pass;
            objBE.IPAddress = GlobalFunctions.GetIpAddress();
            objBE.CurrencyId = 3; //Hardcode get currencyId selected from splash page
            #region Commented by vikram to allow all Role Login then Superadmin
            //   int validateUser = UserBL.ExecuteLoginDetails(Constants.USP_ValidateLoginDetails, false, objBE, "SuperAdmin", LoginWrongAttempts, LoginWaitTime);
            #endregion
            #region Commented by vikram to allow all Role Login then Superadmin
            int validateUser = UserBL.ExecuteAdminLogin(Constants.USP_ValidateAdminLogin, false, objBE, LoginWrongAttempts, LoginWaitTime);
            #endregion
            if (validateUser == 0 || validateUser == 2) // -- User does not exist || -- Incorrect user password
            {
                dvCaptcha.Visible = true;
                txtCaptcha.Text = "";
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Invalid, username and password.", AlertType.Warning);
                //return;
            }
            else if (validateUser == 3) // -- User blocked 
            {
                dvCaptcha.Visible = true;
                txtCaptcha.Text = "";
                GlobalFunctions.ShowModalAlertMessages(this.Page, "User account has been blocked for " + LoginWaitTime + " minute.", AlertType.Warning);
                //return;
            }
            else if (validateUser == 1) // -- Success
            {
                UserBE objUser = new UserBE();
                UserBE objBEs = new UserBE();
                objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(txtUserName.Text.Trim());
                objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                objUser = UserBL.GetMCPUserDetails(Constants.USP_GetLoginDetails, false, objBEs);
                if (objUser != null || objUser.EmailId != null)
                {
                    Session["User"] = objUser;
                    Session["CaptchaText"] = null;
                   // objBase.CreateActivityLog("Main Control Panel", "Logged - In", "");
                    Response.Redirect("~/DashBoard.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
   

}