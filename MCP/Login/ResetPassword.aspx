﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="Login_ResetPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../CSS/bootstrap.min.css" rel="stylesheet" />
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="../css/jquery.smartmenus.bootstrap.css" rel="stylesheet" />
    <link href="../css/common-admin.css" rel="stylesheet" />
    <link href="../css/radio.css" rel="stylesheet" />
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- SmartMenus jQuery plugin -->
    <script type="text/javascript" src="../js/jquery.smartmenus.js"></script>
    <!-- SmartMenus jQuery Bootstrap Addon -->
    <script type="text/javascript" src="../js/jquery.smartmenus.bootstrap.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    
    <div>
        <div class="text-center logo" style="margin-top: 50px">
            <a href="#">
                <img src="<%=host%>Images/Logo/logo-brandAddition.png" alt="logo"></a>
        </div>
        <!--Forgot Password Form-->
        <div runat="server" id="dvForgot" visible="false" style="margin-top: 25px">
            <div class="admin_page">
                <div class="container ">
                    <div class="wrap_container">
                        <div class="content">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3>Having trouble signing in?
                                    </h3>
                                    <p>Enter your email address and we'll help you reset your password.</p>
                                </div>
                                <div class="col-md-3  text-right">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row collapsableContent" style="margin-left: 20px">
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="name">Email Address <span style="color: red">*</span>:</label>
                                <div class="col-md-5 col-sm-12">
                                    <asp:TextBox ID="txtForgotEmail" autocomplete="off" runat="server" CssClass="form-control" placeholder="Email Address" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtForgotEmail" ErrorMessage="Enter Email Address"
                                        SetFocusOnError="True" Display="None" CssClass="errortext" ForeColor="" ValidationGroup="ForgotInfoValidate"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtForgotEmail"
                                        ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None"
                                        CssClass="errortext" ForeColor="" SetFocusOnError="True" ValidationGroup="ForgotInfoValidate"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="category">Captcha Verification <span style="color: red">*</span>:</label>
                                <div class="col-md-5 col-sm-12">
                                    <div style="margin-bottom:15px;">
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtCaptcha" placeholder="Captcha Text" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCaptcha" ErrorMessage="Enter Captcha Text"
                                            SetFocusOnError="True" Display="None" CssClass="errortext" ForeColor="" ValidationGroup="ForgotInfoValidate"></asp:RequiredFieldValidator>
                                    </div>
                                    <img id="CaptchaImg" src="" runat="server" ondragstart="return false;" ondrop="return false;" />
                                    
                                </div>
                            </div>
                            <div id="dvMsg" runat="server" visible="false">
                                <p><strong>If the email address you’ve entered is associated with a customer account in our records, you will receive an email from us with instructions for resetting your password. Although these emails are usually instant, they can sometimes be delayed. If you don’t receive your email within 30 minutes, please check your junk mail and/or ensure that you entered your correct email address and try again.</strong></p>
                            </div>
                            <div id="dvError" runat="server" visible="false">
                                <p><strong>We’re sorry, there was a problem with your request</strong></p>
                            </div>
                        </div>
                        <div class="buttonPanel container">
                            <ul class="button_section">
                                <li>
                                    <asp:Button ID="btnSubmitForgot" runat="server" OnClick="btnSubmitForgot_Click" ToolTip="Submit"
                                        Text="Submit" CssClass="btn" ValidationGroup="ForgotInfoValidate" />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="false"
                                        ShowMessageBox="true" ToolTip="Following are incomplete fields of the form" CssClass="errortext"
                                        ForeColor="" ValidationGroup="ForgotInfoValidate" />
                                </li>
                                <li>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="btn gray" Text="Cancel" OnClick="btnCancel_Click" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Forgot Password Form-->

        <!--Reset Password Form-->
        <div id="dvReset" runat="server" visible="false" style="margin-top: 25px">
            <div class="admin_page">
                <div class="container ">
                    <div class="wrap_container">
                        <div class="content">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3>Reset Password
                                    </h3>
                                    <p>Enter your desired password for login.</p>
                                </div>
                                <div class="col-md-3  text-right">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row collapsableContent" style="margin-left: 20px">
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="name">Password <span style="color: red">*</span>:</label>
                                <div class="col-md-5 col-sm-12">
                                    <asp:TextBox ID="txtPassword" autocomplete="off" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password can not be empty"
                                        SetFocusOnError="True" Display="None" CssClass="errortext" ForeColor="" ValidationGroup="ForgotInfoValidate2"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="category">Confirm Password <span style="color: red">*</span>:</label>
                                <div class="col-md-5 col-sm-12">
                                    <div style="margin-top: 15px;">
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtConfirmPassword" placeholder="Confirm Password" autocomplete="off" TextMode="Password" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="Confirm Password can not be empty"
                                            SetFocusOnError="True" Display="None" CssClass="errortext" ForeColor="" ValidationGroup="ForgotInfoValidate2"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="comparePasswords" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"
                                            ErrorMessage="Your passwords do not match up!" Display="None" ValidationGroup="ForgotInfoValidate2" />
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="buttonPanel container">
                            <ul class="button_section">
                                <li>
                                    <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" ToolTip="Submit"
                                        Text="Submit" CssClass="btn" ValidationGroup="ForgotInfoValidate2" />
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="false"
                                        ShowMessageBox="true" ToolTip="Following are incomplete fields of the form" CssClass="errortext"
                                        ForeColor="" ValidationGroup="ForgotInfoValidate2" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Reset Password Form-->
    </div>

   </form>
    <script>
        function createCode() {
            var random;
            var temp = "";
            for (var i = 0; i < 5; i++) {
                temp += Math.round(Math.random() * 8);
            }
            //document.getElementById('<%= CaptchaImg.ClientID %>').src = "JpegImage.aspx?code=" + temp;
            document.all.CaptchaImg.src = "JpegImage.aspx?code=" + temp;
        }
        window.onload = createCode();
    </script>
</body>
</html>