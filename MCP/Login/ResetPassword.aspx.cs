﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login_ResetPassword : System.Web.UI.Page
{
    Int16 UserId = 0;
    string UserEmail = "";
    public static string host = GlobalFunctions.GetImageVirtualPath();
    //BasePage objBase = new BasePage();

    /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    /*Sachin Chauhan End : 22 02 2016 */

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 18-08-15
    /// Scope   : Page_PreInit event of the page
    /// Modified by : Sachin Chauhan
    /// Modified Date : 22 02 2016
    /// Modify Scope : Added CSRF attack prevention code
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    protected void Page_Init(object sender, EventArgs e)
    {
        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        #region "CSRF prevention code"
        try
        {
            //First, check for the existence of the Anti-XSS cookie
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;

            //If the CSRF cookie is found, parse the token from the cookie.
            //Then, set the global page variable and view state user
            //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
            //method.
            if (requestCookie != null
            && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                //Set the global token variable so the cookie value can be
                //validated against the value in the view state form field in
                //the Page.PreLoad method.
                _antiXsrfTokenValue = requestCookie.Value;

                //Set the view state user key, which will be validated by the
                //framework during each request
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            //If the CSRF cookie is not found, then this is a new session.
            else
            {
                //Generate a new Anti-XSRF token
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                //Set the view state user key, which will be validated by the
                //framework during each request
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                //Create the non-persistent CSRF cookie
                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    //Set the HttpOnly property to prevent the cookie from
                    //being accessed by client side script
                    HttpOnly = true,

                    //Add the Anti-XSRF token to the cookie value
                    Value = _antiXsrfTokenValue
                };

                //If we are using SSL, the cookie should be set to secure to
                //prevent it from being sent over HTTP connections
                if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                    responseCookie.Secure = true;

                //Add the CSRF cookie to the response
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += Page_PreLoad;
        }
        catch (Exception ex)
        {
            Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
            Exceptions.WriteExceptionLog(ex);
            //throw;
        }
        #endregion
        /*Sachin Chauhan End : 22 02 2016*/
    }

    /// <summary>
    /// Author  : Sachin Chauhan
    /// Date : 22 02 2016
    /// Scope : Added CSRF attack prevention code
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    protected void Page_PreLoad(object sender, EventArgs e)
    {
        //During the initial page load, add the Anti-XSRF token and user
        //name to the ViewState
        if (!IsPostBack)
        {
            //Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

            //If a user name is assigned, set the user name
            //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            if (Session["ContextUserGUID"] == null)
                Session["ContextuserGUID"] = Guid.NewGuid().ToString();

            ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
        }
        //During all subsequent post backs to the page, the token value from
        //the cookie should be validated against the token in the view state
        //form field. Additionally user name should be compared to the
        //authenticated users name
        else
        {
            //Validate the Anti-XSRF token
            //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())

                Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Convert.ToString(Request.QueryString["Request"]) != "" && Convert.ToString(Request.QueryString["Request"]) != null)
                {
                    dvForgot.Visible = false;
                    dvReset.Visible = true;
                    string str = GlobalFunctions.Decrypt(Convert.ToString(Request.QueryString["Request"]));
                    if (str != "")
                    { string[] val = str.Split('+'); UserId = val[0].To_Int16(); UserEmail = val[1].ToString(); VerifyRequest(UserEmail); }
                    else
                    { dvForgot.Visible = true; dvReset.Visible = false; dvError.Visible = true; dvMsg.Visible = false; }
                }
                else
                { dvForgot.Visible = true; dvReset.Visible = false; }
            }
        }
        catch (Exception ex)
        { Exceptions.WriteExceptionLog(ex); }
    }

    protected void VerifyRequest(string UserEmail)
    {
        try
        {
            
            UserBE lst = new UserBE();
            UserBE objBEs = new UserBE();
            objBEs.EmailId = UserEmail;
            lst = UserBL.GetMCPUserDetails(Constants.USP_GetLoginDetails, false, objBEs);
            if (lst != null || lst.EmailId != null)
            {
                Session["UserLst"] = lst;
                DateTime currentDateTime = lst.CurrentTimeStamp;
                DateTime reqDateTime = lst.ResetTime;
                reqDateTime = reqDateTime.AddMinutes(30);
                if (currentDateTime <= reqDateTime)
                {
                    if (lst.IsActive)
                    {
                        dvReset.Visible = true;
                        dvForgot.Visible = false;
                        dvMsg.Visible = false;
                        dvError.Visible = false;
                        txtPassword.Text = "";
                        txtConfirmPassword.Text = "";
                    }
                    else
                    {
                        dvReset.Visible = false;
                        dvForgot.Visible = true;
                        dvMsg.Visible = false;
                        dvError.Visible = true;
                        txtForgotEmail.Text = "";
                        txtCaptcha.Text = "";
                        UserBE objBE = new UserBE();
                        objBE.UserId = lst.UserId;
                    }
                }
                else
                {
                    dvReset.Visible = false;
                    dvForgot.Visible = true;
                    dvMsg.Visible = false;
                    dvError.Visible = true;
                    txtForgotEmail.Text = "";
                    txtCaptcha.Text = "";
                    UserBE objBE = new UserBE();
                    objBE.UserId = lst.UserId;
                   
                }
            }
            else
            {
                dvReset.Visible = false;
                dvForgot.Visible = true;
                dvMsg.Visible = false;
                dvError.Visible = true;
                txtForgotEmail.Text = "";
                txtCaptcha.Text = "";
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void btnSubmitForgot_Click(object sender, EventArgs e)
    {
        try
        {
            string theCode = Session["MCPCaptchaText"].ToString();
            if (Sanitizer.GetSafeHtmlFragment(txtCaptcha.Text.ToLower()) != theCode.ToLower())
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("Login/Captcha"), AlertType.Warning);
                txtCaptcha.Text = "";
                return;
            }
            string EncryptStr = "";
            UserBE lst = new UserBE();
            UserBE objBEs = new UserBE();
            objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(txtForgotEmail.Text.Trim());
            objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
            objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
            lst = UserBL.GetMCPUserDetails(Constants.USP_GetLoginDetails, false, objBEs);
            Exceptions.WriteInfoLog(objBEs.EmailId + " clicked Forgot password ");

            if (lst != null || lst.EmailId != null)
            {
                EncryptStr = lst.UserId + "+" + lst.EmailId;
                EncryptStr = GlobalFunctions.Encrypt(EncryptStr);
                string Subject = "";
                StringBuilder MailBody = new StringBuilder();
                string ToAddress = lst.EmailId;
                string FromAddress = "";
                string CC = "";
                string BCC = "";
                bool IsBodyHTML = true;
                string url = ConfigurationManager.AppSettings["McpURL"] + "Login/ResetPassword.aspx?Request=" + EncryptStr;
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = "Forgot password";
                objEmail.LanguageId = 1;//1 English
                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                //Exceptions.WriteInfoLog(objBEs.EmailId + " Post getting Forgot password email template ");
                //Exceptions.WriteInfoLog(objBEs.EmailId + " lstEmail Count " + (lstEmail == null).ToString() );
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    //Exceptions.WriteInfoLog(objBEs.EmailId + " Inside If condition ");
                    Subject = lstEmail[0].Subject;
                    FromAddress = lstEmail[0].FromEmailId;
                    CC = lstEmail[0].CCEmailId;
                    BCC = lstEmail[0].BCCEmailId;
                    IsBodyHTML = lstEmail[0].IsHtml;
                    MailBody.Append(lstEmail[0].Body.Replace("@UserName", lst.FirstName + " " + lst.LastName).Replace("@URL", url));

                    //Exceptions.WriteInfoLog(objBEs.EmailId + " B4 Forgot password mail sending");
                    GlobalFunctions.SendEmail(ToAddress, FromAddress, "", CC, BCC, Subject, MailBody.ToString(), "", "", IsBodyHTML);
                    //Exceptions.WriteInfoLog(objBEs.EmailId + " Past Forgot password mail sent");
                    UserBE objBE = new UserBE();
                    objBE.UserId = lst.UserId;
                    //int i = UserBL.ExecuteResetDetails(Constants.USP_ResetMCPPassword, true, objBE, "I");
                    txtCaptcha.Text = "";
                    txtForgotEmail.Text = "";
                    dvMsg.Visible = true;
                }
                else
                {
                    //Exceptions.WriteInfoLog(objBEs.EmailId + " Inside Else condition B4 AlertMsg");
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/SendEmailFailed"), AlertType.Failure); return;
                    //Exceptions.WriteInfoLog(objBEs.EmailId + " Inside Else condition post AlertMsg");
                }
            }
            else
            { dvMsg.Visible = true; }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/Login/Login.aspx");
    }

    private bool ValidatePassword(string regExp)
    {
        try
        {
            Regex reg = new Regex(regExp); //(regExp, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(Sanitizer.GetSafeHtmlFragment(txtPassword.Text.Trim()));
        }
        catch (Exception ex)
        { Exceptions.WriteExceptionLog(ex); return false; }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {

            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            string minPass = "5";
            bool flag = false;
            if (flag)
            {
                string regExp = @"^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[\W_])\S{" + minPass + ",20}$";
                bool check = ValidatePassword(regExp);
                if (!check)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/RegistrationPasswordCheck"), AlertType.Warning);
                    return;
                }
            }

            UserBE lst = (UserBE)Session["UserLst"];
            string SaltPass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(txtPassword.Text.Trim()), "SHA512", null);
            UserBE objBE = new UserBE();
            objBE.UserId = lst.UserId;
            objBE.Password = SaltPass;
            objBE.Action = Convert.ToInt16(DBAction.Update);
            int i = UserBL.ExecuteResetDetails(Constants.USP_ResetMCPPassword, false, objBE, Convert.ToString(objBE.Action));

            string Subject = "";
            StringBuilder MailBody = new StringBuilder();
            string ToAddress = lst.EmailId;
            string FromAddress = "";
            string CC = "";
            string BCC = "";
            bool IsBodyHTML = true;
            List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
            EmailManagementBE objEmail = new EmailManagementBE();
            objEmail.EmailTemplateName = "Change Passwords";
            objEmail.LanguageId = 1;//GlobalFunctions.GetLanguageId();
            lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
            if (lstEmail != null && lstEmail.Count > 0)
            {
                Subject = lstEmail[0].Subject;
                FromAddress = lstEmail[0].FromEmailId;
                CC = lstEmail[0].CCEmailId;
                BCC = lstEmail[0].BCCEmailId;
                IsBodyHTML = lstEmail[0].IsHtml;
                MailBody.Append(lstEmail[0].Body.Replace("@UserName", lst.FirstName + " " + lst.LastName).Replace("@Email", lst.EmailId));
                //objBase.CreateActivityLog("Update Password", "Updated", lst.EmailId);
                GlobalFunctions.SendEmail(ToAddress, FromAddress, "", CC, BCC, Subject, MailBody.ToString(), "", "", IsBodyHTML);
            }
            Response.Redirect(GlobalFunctions.GetVirtualPath() + "Login/Login.aspx");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
}