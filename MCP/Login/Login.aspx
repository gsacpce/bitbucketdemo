﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Brand Addition</title>

    <!-- core CSS -->
    <link href="../CSS/bootstrap.min.css" rel="stylesheet" />
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="../css/jquery.smartmenus.bootstrap.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css' />
    <link href="../css/common-admin.css" rel="stylesheet" />
    <link href="../css/radio.css" rel="stylesheet" />
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- SmartMenus jQuery plugin -->
    <script type="text/javascript" src="../js/jquery.smartmenus.js"></script>
    <!-- SmartMenus jQuery Bootstrap Addon -->
    <script type="text/javascript" src="../js/jquery.smartmenus.bootstrap.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var heightx = $('.col-md-12').innerHeight();
            $('.col-md-12').parent().css('height', heightx);
            $('.col-md-12').parent().css('margin-top', -heightx / 2);
            $(window).resize(function () {
                var heightx = $('.col-md-12').innerHeight();
                $('.col-md-12').parent().css('height', heightx);
                $('.col-md-12').parent().css('margin-top', -heightx / 2);
            });
        });
    </script>
    <script type="text/javascript">
        function Validate() {
            var name = document.getElementById('<%= txtUserName.ClientID %>');
            var pass = document.getElementById('<%= txtPassword.ClientID %>');
            var dv = document.getElementById('<%= dvCaptcha.ClientID %>');
            if (name.value == "" || pass.value == "") {
                alert("Enter Username & Password !!!");
                return false;
            }
            if (dv != "" || dv != null) {
                var captcha = document.getElementById('<%= txtCaptcha.ClientID %>');
                if (captcha.value == "") {
                    alert("Captcha text can not be empty !!!");
                    return false;
                }
            }
            return true;
        }
    </script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">
        <!--Admin page-->
        <div class="logpage">

            <div class="col-md-12">
                <div class="text-center logo">
                    <a href="#">
                        <img src="../Images/Logo/logo-brandAddition.png" width="370" height="69" alt="Logo" /></a>
                </div>
                <div class="col-md-6 border_right">
                    <h6>Welcome to Corporate Store</h6>
                    <p>This is the control panel for corporate stores administration, using this tool you can manage all aspects of a corporate stores, which includes:</p>
                    <ul>
                        <li>Stores</li>
                        <li>Catalogue Setup</li>
                        <li>Orders and Procurement</li>
                        <li>User Setup</li>
                        <li>Store Designs</li>
                        <li>Other Features</li>
                    </ul>
                    <p>Unauthorised access to this area is not allowed if you are an administrator of a corporate store and have troubles signing in please contact <a href="mailto:badevsupport@powerweave.com">badevsupport@powerweave.com</a></p>
                   <p>If you have forgotten your password you can click the  <a href="ResetPassword.aspx"> Forgot password? </a> link.</p>
                    <%--<p class="no_margin">Please save URL of the site by clicking "Want to Bookmark this Site ?.</p>--%>
                </div>
                <div class="col-md-6">
                    <div class="form-horizontal" role="form" runat="server">
                        <div class="form-group">

                            <div class="col-sm-12">
                                <asp:TextBox CssClass="form-control" runat="server" ID="txtUserName" placeholder="User Name" />
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-sm-12">
                                <asp:TextBox CssClass="form-control" runat="server" ID="txtPassword" placeholder="Password" TextMode="Password" />
                            </div>
                        </div>
                        <div class="form-group" id="dvCaptcha" runat="server" visible="false">
                            <div class="col-sm-12">
                                <img id="CaptchaImg" src="" runat="server" ondragstart="return false;" ondrop="return false;" />
                                <div style="margin-top: 10px">
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtCaptcha" placeholder="Captcha Text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" col-sm-6">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-default col-sm-12" Text="Login" OnClick="btnSubmit_Click" OnClientClick="return Validate();" />
                            </div>
                            <div class="col-sm-6 check text-right" id="dvRemember" runat="server" visible="false">
                                <label>
                                    <input type="checkbox" />
                                    Remember me</label>
                            </div>
                        </div>
                         <div class="form-group">
                            <div class=" col-sm-6">
                                <a href="TeamSupportAutoLogin.aspx">Support</a>
                            </div>
                        </div>
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                        <!-- This modal popup is used to display warning messages -->

                        <!-- // This modal popup is used to display warning messages -->
                    </div>

                </div>

            </div>
        </div>
        <div id="myWarningModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-exclamation-triangle fa-2x text-warning"></i>Warning!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-warning text-center fade in">
                            <h4><span id="WarningMessage"></span></h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning center-block" id="btnWarningOK" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--Admin page-->
    <script>

        function createCode() {
            var random;
            var temp = "";
            for (var i = 0; i < 5; i++) {
                temp += Math.round(Math.random() * 8);
            }
            document.all.CaptchaImg.src = "JpegImage.aspx?code=" + temp;
        }

        window.onload = createCode();

    </script>

</body>
</html>
