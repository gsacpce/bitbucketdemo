﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        Exception exUnhandled = HttpContext.Current.Server.GetLastError();
        Exceptions.WriteExceptionLog(exUnhandled);
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

    void Application_BeginRequest(object sender, EventArgs e)
    {
        HttpContext.Current.Response.AddHeader("x-frame-options", "SAMEORIGIN");
        HttpApplication application = ((HttpApplication)(sender));
        HttpRequest request = application.Request;
        HttpResponse responce = application.Response;
        string absoluteUri = request.Url.AbsoluteUri;

        if (!absoluteUri.ToLower().Contains("localhost"))
        {
            if (absoluteUri.ToLower().Contains("http://brand-estore.com"))
            {
                responce.Redirect(absoluteUri.Replace("http://brand-estore.com", "https://www.brand-estore.com"), true);
            }
            if (absoluteUri.ToLower().Contains("https://brand-estore.com"))
            {
                responce.Redirect(absoluteUri.Replace("https://brand-estore.com", "https://www.brand-estore.com"), true);
            }
            if (absoluteUri.ToLower().Contains("http://www.brand-estore.com"))
            {
                responce.Redirect(absoluteUri.Replace("http://www.brand-estore.com", "https://www.brand-estore.com"), true);
            }
            if (absoluteUri.ToLower().Contains("http://"))
            {
                responce.Redirect(absoluteUri.Replace("http://", "https://"), true);
            }
        }
    }
       
</script>
