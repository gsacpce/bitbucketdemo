﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Configuration;

public partial class Master_MCPMaster : System.Web.UI.MasterPage
{
    #region Variables

    public string host = GlobalFunctions.GetImageVirtualPath();

    #endregion

    /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    /*Sachin Chauhan End : 22 02 2016 */

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 18-08-15
    /// Scope   : Page_PreInit event of the page
    /// Modified by : Sachin Chauhan
    /// Modified Date : 22 02 2016
    /// Modify Scope : Added CSRF attack prevention code
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    protected void Page_Init(object sender, EventArgs e)
    {
        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        #region "CSRF prevention code"
        try
        {
            //First, check for the existence of the Anti-XSS cookie
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;

            //If the CSRF cookie is found, parse the token from the cookie.
            //Then, set the global page variable and view state user
            //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
            //method.
            if (requestCookie != null
            && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                //Set the global token variable so the cookie value can be
                //validated against the value in the view state form field in
                //the Page.PreLoad method.
                _antiXsrfTokenValue = requestCookie.Value;

                //Set the view state user key, which will be validated by the
                //framework during each request
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            //If the CSRF cookie is not found, then this is a new session.
            else
            {
                //Generate a new Anti-XSRF token
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                //Set the view state user key, which will be validated by the
                //framework during each request
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                //Create the non-persistent CSRF cookie
                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    //Set the HttpOnly property to prevent the cookie from
                    //being accessed by client side script
                    HttpOnly = true,

                    //Add the Anti-XSRF token to the cookie value
                    Value = _antiXsrfTokenValue
                };

                //If we are using SSL, the cookie should be set to secure to
                //prevent it from being sent over HTTP connections
                if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                    responseCookie.Secure = true;

                //Add the CSRF cookie to the response
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }
        catch (Exception ex)
        {
            Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
            Exceptions.WriteExceptionLog(ex);
            //throw;
        }
        #endregion
        /*Sachin Chauhan End : 22 02 2016*/

        try
        {
            if (Session["User"] == null)
                Response.Redirect("~/Login/Login.aspx", true);
            else
            {
                aLogo.HRef = Convert.ToString(ConfigurationManager.AppSettings["ImgURL"]) + "DashBoard.aspx";
                UserBE objUserBE = new UserBE();
                objUserBE = Session["User"] as UserBE;
                ltrUserName.Text = "Hello " + objUserBE.FirstName;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    /// <summary>
    /// Author  : Sachin Chauhan
    /// Date : 22 02 2016
    /// Scope : Added CSRF attack prevention code
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        //During the initial page load, add the Anti-XSRF token and user
        //name to the ViewState
        if (!IsPostBack)
        {
            //Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

            //If a user name is assigned, set the user name
            //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            if (Session["ContextUserGUID"] == null)
                Session["ContextuserGUID"] = Guid.NewGuid().ToString();

            ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
        }
        //During all subsequent post backs to the page, the token value from
        //the cookie should be validated against the token in the view state
        //form field. Additionally user name should be compared to the
        //authenticated users name
        else
        {
            //Validate the Anti-XSRF token
            //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())

                Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
        }
    }

   

    #region Page load

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetAllStores();
        }
    }

    #endregion

    #region Get All Stores

    private void GetAllStores()
    {
        try
        {
            List<StoreBE> getAllStores = StoreBL.GetAllStoreDetails();
            ddlStores.DataSource = getAllStores.FindAll(x=>x.IsCreated == true);
            ddlStores.DataTextField = "StoreName";
            ddlStores.DataValueField = "StoreId";
            ddlStores.DataBind();
            ddlStores.Items.Insert(0, new ListItem("All Stores", "0"));
            ddlStores.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion
}
