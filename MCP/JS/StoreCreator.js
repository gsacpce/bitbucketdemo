﻿$(document).ready(function () {
    
    $("#txtStoreName").keyup(function (e) {
        ////debugger;
            //var key = e.charCode;
            //if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
            //    e.preventDefault();
            //    $("#lblURL").append(e.key);
            //}

        $("#lblURL").text(StoreURL + $("#txtStoreName").val().replace(/[^a-zA-Z0-9]/g, '').replace(' ', ''));
            
        
    })

    $('#txtStoreName').keypress(function (e) {
       var regex = new RegExp("^[!@#%&*/_?|\{}<>$+~`^=()':;]+$");
             var str = String.fromCharCode(e.charCode);
        if (!regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $('#txtStoreName').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    //$('#<%= txt1.ClientID %>').keyup(function (e) {
    //    //debugger;
    //    var string = document.getElementById('<%= txt1.ClientID %>').value;
    //    string = string.replace(/[^a-zA-Z0-9]/g, '');
    //    string = string.replace(' ', '');
    //    document.getElementById('<%= lbl1.ClientID %>').innerText = string;
    //});

    $(".chkShow input[type=checkbox]").each(function () {
        //debugger;
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultLanguage').css('display', 'block');
        }
        else {
            $(this).parent().parent().parent().find('#dvDefaultLanguage').css('display', 'none');
        }
    })

    $(".chkShowCurrency input[type=checkbox]").each(function () {
        //debugger;
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'block');
            if ($("#rblStoreType_0").is(":checked") == true)
                $(this).parent().parent().parent().find('#dvBASYSDetails').css('display', 'block');
        }
        else {
            $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'none');
        }
    })

    $(".chkPLDisplayType input[type=checkbox]").each(function () {
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultDisplayType').css('display', 'block');
        }
        else {
            $(this).parent().parent().parent().find('#dvDefaultDisplayType').css('display', 'none');
        }
    })

    $(".chkPLSorting input[type=checkbox]").each(function () {
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvPLSorting').css('display', 'block');
        }
        else {
            $(this).parent().parent().parent().find('#dvPLSorting').css('display', 'none');
        }
    })

    $(".chkSCPaymentGateway input[type=checkbox]").each(function () {
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvSCPaymentGateway').css('display', 'block');
        }
        else {
            $(this).parent().parent().parent().find('#dvSCPaymentGateway').css('display', 'none');
        }
    })

    //$(".chkSCAlternativePayment input[type=checkbox]").each(function () {
    //    if ($(this).is(":checked") == true) {
    //        $(this).parent().parent().parent().find('#dvSCAlternativePayment').css('display', 'block');
    //    }
    //    else {
    //        $(this).parent().parent().parent().find('#dvSCAlternativePayment').css('display', 'none');
    //    }
    //})


    $(".chkShow input[type=checkbox]").click(function () {
        ////debugger;
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultLanguage').css('display', 'block');
            SetFirstDefaultRadioButton('DefaultLanguage', $(this).parent().parent().parent().find('#dvDefaultLanguage').find('input:radio'), $(this).parent().attr("class"),true);
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvDefaultLanguage').css('display', 'none');
            $(this).parent().parent().parent().find('#dvDefaultLanguage').find('input:radio').prop("checked", false);
            SetFirstDefaultRadioButton('DefaultLanguage', $(this).parent().parent().parent().find('#dvDefaultLanguage').find('input:radio'), $(this).parent().attr("class"),false);
        }
    })

    $(".chkShowCurrency input[type=checkbox]").click(function () {
        //debugger;
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'block');
            
            SetFirstDefaultRadioButton('DefaultCurrency', $(this).parent().parent().parent().find('#dvDefaultCurrency').find('input:radio'), $(this).parent().attr("class"), true);
            if ($("#rblStoreType_0").is(":checked") == true) {
                $(this).parent().parent().parent().find('#dvBASYSDetails').css('display', 'block');
                $(this).parent().parent().parent().find('.BASYSPopup').modal('show');
            }
            else {
                $(this).parent().parent().parent().find('#dvBASYSDetails').css('display', 'none');
            }
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvDefaultCurrency').css('display', 'none');
            $(this).parent(0).parent(0).parent(0).find('#dvBASYSDetails').css('display', 'none');
            $(this).parent().parent().parent().find('#dvDefaultCurrency').find('input:radio').prop("checked", false);
            SetFirstDefaultRadioButton('DefaultCurrency', $(this).parent().parent().parent().find('#dvDefaultCurrency').find('input:radio'), $(this).parent().attr("class"),false);
        }

        if ($("#rblStoreType_0").is(":checked") == true) {
            var chkCount = $(".chkShowCurrency input[type=checkbox]:checked").length;
            if (chkCount == 0)
                $(this).parent().parent().parent().parent().find('#dvBASYSDetailHeading').css('display', 'none');
            else
                $(this).parent().parent().parent().parent().find('#dvBASYSDetailHeading').css('display', 'block');
        }
    })

    $(".chkPLDisplayType input[type=checkbox]").click(function () {

        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultDisplayType').css('display', 'block');
            SetFirstDefaultRadioButton('DefaultDisplayType', $(this).parent().parent().parent().find('#dvDefaultDisplayType').find('input:radio'), $(this).parent().attr("class"),true);
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvDefaultDisplayType').css('display', 'none');
            $(this).parent().parent().parent().find('#dvDefaultDisplayType').find('input:radio').prop("checked", false);
            SetFirstDefaultRadioButton('DefaultDisplayType', $(this).parent().parent().parent().find('#dvDefaultDisplayType').find('input:radio'), $(this).parent().attr("class"),false);
        }
    })

    $("#rblProductPagination_0").click(function () {
        $('#txtProductsPerPage').removeAttr("disabled");
        //ValidatorEnable($('#cvProductsPerPage'), true);
    })

    $("#rblProductPagination_1").click(function () {
        $('#txtProductsPerPage').attr("disabled", "disabled");
        $('#txtProductsPerPage').val("");
        //ValidatorEnable($('#cvProductsPerPage'), false);
    })

    $(".chkPLSorting input[type=checkbox]").click(function () {

        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvPLSorting').css('display', 'block');
            SetFirstDefaultRadioButton('PLSorting', $(this).parent().parent().parent().find('#dvPLSorting').find('input:radio'), $(this).parent().attr("class"),true);
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvPLSorting').css('display', 'none');
            $(this).parent().parent().parent().find('#dvPLSorting').find('input:radio').prop("checked", false);
            SetFirstDefaultRadioButton('PLSorting', $(this).parent().parent().parent().find('#dvPLSorting').find('input:radio'), $(this).parent().attr("class"),false);
        }
    })

    $(".chkSCPaymentGateway input[type=checkbox]").click(function () {

        if ($(this).is(":checked") == true) {
            if ($(this).next().text().toLowerCase().indexOf("point") >= 0) {
                //$(this).parent().parent().parent().find('#dvPointData').show();
                //$(this).parent().parent().parent().find('#ContentPlaceHolder1_rptSCPaymentGateway_rfvPointData_2').css("display", "block");
            }
            $(this).parent().parent().parent().find('#dvSCPaymentGateway').css('display', 'block');
            SetFirstDefaultRadioButton('SCPaymentGateway', $(this).parent().parent().parent().find('#dvSCPaymentGateway').find('input:radio'), $(this).parent().attr("class"),true);
        } else {
            if ($(this).next().text().toLowerCase().indexOf("point") >= 0) {
                //$(this).parent().parent().parent().find('#dvPointData').hide();
                //$(this).parent().parent().parent().find('#ContentPlaceHolder1_rptSCPaymentGateway_rfvPointData_2').css("display", "none");
            }
            $(this).parent(0).parent(0).parent(0).find('#dvSCPaymentGateway').css('display', 'none');
            $(this).parent().parent().parent().find('#dvSCPaymentGateway').find('input:radio').prop("checked", false);
            SetFirstDefaultRadioButton('SCPaymentGateway', $(this).parent().parent().parent().find('#dvSCPaymentGateway').find('input:radio'), $(this).parent().attr("class"),false);
        }
    })

    $("#chkEnablePoints").click(function () {
        if ($(this).is(":checked") == true) {
            $("#dvPoints").css("display", "");
            $("#liPaymentGatewayHeading").css("display", "none");
            $('#txtPoints').removeAttr("disabled");
            var validatorObject = document.getElementById('ContentPlaceHolder1_cvSCPaymentGateway');
            validatorObject.enabled = false;
            validatorObject.isvalid = true;
            ValidatorUpdateDisplay(validatorObject);
            var validatorObject1 = document.getElementById('ContentPlaceHolder1_cvPointData');
            validatorObject1.enabled = true;
            validatorObject1.isvalid = true;
            ValidatorUpdateDisplay(validatorObject1);
        }
        else {
            $("#dvPoints").css("display", "none");
            $("#liPaymentGatewayHeading").css("display", "");
            $('#txtPoints').attr("disabled", "disabled");
            $('#txtPoints').val("");
            var validatorObject = document.getElementById('ContentPlaceHolder1_cvSCPaymentGateway');
            validatorObject.enabled = true;
            validatorObject.isvalid = true;
            ValidatorUpdateDisplay(validatorObject);
            var validatorObject1 = document.getElementById('ContentPlaceHolder1_cvPointData');
            validatorObject1.enabled = false;
            validatorObject1.isvalid = true;
            ValidatorUpdateDisplay(validatorObject1);
        }
    })

    //$(".chkSCAlternativePayment input[type=checkbox]").click(function () {

    //    if ($(this).is(":checked") == true) {
    //        $(this).parent().parent().parent().find('#dvSCAlternativePayment').css('display', 'block');
    //        SetFirstDefaultRadioButton('SCAlternativePayment', $(this).parent().parent().parent().find('#dvSCAlternativePayment').find('input:radio'), $(this).parent().attr("class"),true);
    //    } else {
    //        $(this).parent(0).parent(0).parent(0).find('#dvSCAlternativePayment').css('display', 'none');
    //        $(this).parent().parent().parent().find('#dvSCAlternativePayment').find('input:radio').prop("checked", false);
    //        SetFirstDefaultRadioButton('SCAlternativePayment', $(this).parent().parent().parent().find('#dvSCAlternativePayment').find('input:radio'), $(this).parent().attr("class"),false);
    //    }
    //})

    $("#rblCRCustomerType_0").click(function () {
        //debugger;
        $('#ddlCRCustomerHierarchyLevel').attr("disabled", false);
        $('#dvApprovalRequired').css('display', '');
    })

    $("#rblCRCustomerType_1").click(function () {
        //debugger;
        $('#ddlCRCustomerHierarchyLevel').attr("disabled", true);
        $('#ddlCRCustomerHierarchyLevel').val('0');
        $('#dvApprovalRequired').css('display', 'none');
    })

    $("#rblCRApprovalRequired_0").click(function () {
        $('#txtCRNotificationEmail').removeAttr("disabled");
    })

    $("#rblCRApprovalRequired_1").click(function () {
        $('#txtCRNotificationEmail').attr("disabled", "disabled");
        $('#txtCRNotificationEmail').val('');
    })

    $("#chkCLEnableCookie").click(function () {
        if ($(this).is(":checked") == true) {
            $("#txtCLCookieLifetime").removeAttr("disabled");
            $("#txtCLCookieMessage").removeAttr("disabled");
        }
        else {
            $("#txtCLCookieLifetime").attr("disabled", "disabled");
            $("#txtCLCookieMessage").attr("disabled", "disabled");
            $("#txtCLCookieLifetime").val('');
            $("#txtCLCookieMessage").val('');
        }
    })

    $("#chkSCInternationalOrders").click(function () {
        if ($(this).is(":checked") == true)
            $("#txtSCMessage").removeAttr("disabled");
        else {
            $("#txtSCMessage").attr("disabled", "disabled");
            $("#txtSCMessage").val('');
        }
    })

    $("#chkAllowBackOrder").click(function () {
        ////debugger;
        if ($(this).is(":checked") == true)
            $("#txtAllowBackOrder").removeAttr("disabled");
        else {
            $('#txtAllowBackOrder').attr("disabled", "disabled");
            $('#txtAllowBackOrder').val("");
        }
    })

    $("#chkCREnableDomainValidation").click(function () {
        if ($(this).is(":checked") == true) {
            $("#dvCREnableDomainValidation").css("display", "");
            $("#rblCREnableDomainValidation_0").attr("checked",true);
        }
        else
            $("#dvCREnableDomainValidation").css("display", "none");
    })

    $("#chkCREnableEmailValidation").click(function () {
        if ($(this).is(":checked") == true) {
            $("#dvCREnableEmailValidation").css("display", "");
            $("#rblCREnableEmailValidation_0").attr("checked", true);
        }
        else
            $("#dvCREnableEmailValidation").css("display", "none");
    })

    $('#pdffile').change(function () {
        $('#subfile').val($(this).val());
    });

    $('#chkSelectDeSelectAll').click(function () {
        if ($(this).is(":checked") == true) {
            $(".chkShow input[type=checkbox]").prop("checked", true);
        }
        else {
            $(".chkShow input[type=checkbox]").prop("checked", false);
        }
    })

    $('#chkEnableFilter').click(function () {
        if ($(this).is(":checked") == true) {
            $(".chkPLFilter input[type=checkbox]").prop("checked", true);
        }
        else {
            $(".chkPLFilter input[type=checkbox]").prop("checked", false);
        }
    })

    $("#rblStoreType_0").click(function () {
        $(".chkShowCurrency input[type=checkbox]").each(function () {
            $(this).prop("checked", false);
            $(this).parent().parent().parent().find('#rbDefaultCurrency').prop("checked", false);
            $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'none'); 
            $(this).parent().parent().parent().find('#dvBASYSDetails').css('display', 'none');
        })
    })

    $("#rblStoreType_1").click(function () {
        ////debugger;
        $(".BASYSHeading").css("display", "none");
        $(".chkShowCurrency input[type=checkbox]").each(function () {
            $(this).prop("checked", false);
            $(this).parent().parent().parent().find('#rbDefaultCurrency').prop("checked", false);
            $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'none');
            $(this).parent().parent().parent().find('#dvBASYSDetails').css('display', 'none');
        })
    })

    $('.BASYSSave').click(function () {
        ////debugger;
        //$('.BASYSPopup').modal('hide');
    })

    $('.BASYSCancel').click(function () {
        var IsConfirmed = confirm("Do you want to uncheck this currency. If yes, click OK, else fill the mandatory details and click save.")
        if (IsConfirmed) {
            debugger;
            $('.BASYSPopup').modal('hide');
            $(this).parent().parent().parent().parent().parent().parent().find('.chkShowCurrency input[type=checkbox]').prop("checked", false);
            $(this).parent().parent().parent().parent().parent().parent().find('#dvDefaultCurrency').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('#rbDefaultCurrency').prop("checked", false);
            $(this).parent().parent().parent().parent().parent().parent().find('#dvBASYSDetails').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvCatalogueId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvGroupId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvDivisionId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvDefaultCustomerId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvCatalogueAliasId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvKeyGroupId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvSourceCodeId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvSalesPersonId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvDefaultCompanyId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvHelpDeskEmailId').css("display", "none"); 
            $(this).parent().parent().parent().parent().parent().parent().find('.revHelpDeskEmailId').css("display", "none");

            var chkCount = $(".chkShowCurrency input[type=checkbox]:checked").length;
            if (chkCount == 0)
                $(this).parent().parent().parent().parent().parent().parent().find('#dvBASYSDetailHeading').css('display', 'none');
            else
                $(this).parent().parent().parent().parent().parent().parent().find('#dvBASYSDetailHeading').css('display', 'block');
        }
        //alert($(this).parent().parent().parent().parent().find('#txtCatalogueId').val());
    })
    
    $('.BASYSDetail').click(function () {
        $(this).parent().find('.BASYSPopup').modal('show');
    })

    $("#rblCRStoreType_0").click(function () {
        //debugger;
        $('#divVatPercentage').css('display', 'none');
    })

    $("#rblCRStoreType_1").click(function () {
        //debugger;
        $('#txtB2CVatPercentage').val('');
        $('#divVatPercentage').css('display', '');
    })

})

function SetUniqueRadioButton(nameregex, current) {
    ////debugger;
    re = new RegExp(nameregex);
    for (i = 0; i < document.forms[0].elements.length; i++) {
        elm = document.forms[0].elements[i]
        if (elm.type == 'radio') {
            if (re.test(elm.name)) {
                elm.checked = false;
            }
        }
    }
    current.checked = true;
}

function SetFirstDefaultRadioButton(nameregex, current, chkClass, action) {
    //debugger;
    var isChecked = false;
    re = new RegExp(nameregex);
    var chkCount = $("." + chkClass + " input[type=checkbox]:checked").length;

    if (chkCount == 1) {
        for (i = 0; i < document.forms[0].elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'radio') {
                if (re.test(elm.name)) {
                    if ($(elm).is(":checked") == true) {
                        isChecked = true;
                        return;
                    }
                }
            }
        }
        if (action) {
            if (!isChecked) {
                $(current).prop("checked", true);
            }
        }
        else {
            if (!isChecked) {
                $("." + chkClass + " input[type=checkbox]:checked").parent().parent().parent().find('input:radio').prop("checked", true);
            }
        }
    }
}

function ValidateLanguage(sender, args) {
    var inputsC = $("#dvLanguages input:checkbox");
    var inputsR = $("#dvLanguages input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked)
                {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateCurrency(sender, args) {
    var inputsC = $("#dvCurrencies input:checkbox");
    var inputsR = $("#dvCurrencies input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateDisplayType(sender, args) {
    var inputsC = $("#dvDisplayType input:checkbox");
    var inputsR = $("#dvDisplayType input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateProductsPerPage(sender, args) {

    if ($("#rblProductPagination_0").is(":checked") == true) {
        if ($.trim($("#txtProductsPerPage").val()) == "" || $.trim($("#txtProductsPerPage").val()) == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidatePointData(sender, args) {

    if ($("#chkEnablePoints").is(":checked") == true) {
        if ($.trim($("#txtPoints").val()) == "" || $.trim($("#txtPoints").val()) == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateResetPasswordBlockAccountDuration(sender, args) {
    if ($.trim($("#txtCRResetPasswordBlockAccountDuration").val()) == "")
        return args.IsValid = false;
    else
        return args.IsValid = true;
}

function ValidateSorting(sender, args) {
    var inputsC = $("#dvPLSorting input:checkbox");
    var inputsR = $("#dvPLSorting input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateFilter(sender, args) {
    var inputsC = $("#dvPLFilter input:checkbox");
    var inputsR = $("#dvPLFilter input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateSCPaymentGateway(sender, args) {
    var inputsC = $("#dvSCPaymentGateway input:checkbox");
    var inputsR = $("#dvSCPaymentGateway input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

//function ValidateSCAlternativePayments(sender, args) {
//    var inputsC = $("#dvSCAlternativePayments input:checkbox");
//    var inputsR = $("#dvSCAlternativePayments input:radio");
//    for (var i = 0; i < inputsC.length; i++) {
//        if (inputsC[i].checked) {
//            for (var j = 0; j < inputsR.length; j++) {
//                if (inputsR[j].checked) {
//                    args.IsValid = true;
//                    return;
//                }
//            }
//        }
//    }
//    args.IsValid = false;
//}

function ValidateCRCustomerHierarchyLevel(sender, args) {

    if ($("#rblCRCustomerType_0").is(":checked") == true) {
        if ($("#ddlCRCustomerHierarchyLevel").get(0).selectedIndex == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateAllowBackOrder(sender, args) {

    if ($("#chkAllowBackOrder").is(":checked") == true) {
        if ($.trim($("#txtAllowBackOrder").val()) == "" || $.trim($("#txtAllowBackOrder").val()) == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateCLCookieLifetime(sender, args) {

    if ($("#chkCLEnableCookie").is(":checked") == true) {
        if ($("#txtCLCookieLifetime").val() == '')
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateCLCookieMessage(sender, args) {

    if ($("#chkCLEnableCookie").is(":checked") == true) {
        if ($("#txtCLCookieMessage").val() == '')
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateTemplate(sender, args) {
    ////debugger;
    var inputsR = $("#dvDefaultTemplate input:radio");
    for (var j = 0; j < inputsR.length; j++) {
        if (inputsR[j].checked) {
            args.IsValid = true;
            return;
        }
    }

    args.IsValid = false;
}

function ValidateB2CVatPercentage(sender, args) {

    if ($("#rblCRStoreType_1").is(":checked") == true) {
        if ($.trim($("#txtB2CVatPercentage").val()) == "" || $.trim($("#txtB2CVatPercentage").val()) == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function isNumeric(keyCode) {
    if (keyCode == 16 || keyCode == 18)
        isShift = true;

    return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 190 || keyCode == 9 || keyCode == 14 || keyCode == 15 || keyCode == 37 || keyCode == 39 || (keyCode >= 96 && keyCode <= 105)) && isShift == false);
}
var isShift = false;
function keyUP(keyCode) {
    if (keyCode == 16 || keyCode == 18 || keyCode == 9)
        isShift = false;
}






