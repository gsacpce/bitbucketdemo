﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System.Linq;
using System.Reflection;

public partial class RoleManagement_AddEditRole : System.Web.UI.Page
{
    List<RoleManagementBE> lstRBE = new List<RoleManagementBE>();
    RoleManagementBE objRoleManagementBE = new RoleManagementBE();
    BasePage objBase = new BasePage();
    string mode = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            mode = Request.QueryString["mode"];
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized("RoleList.aspx");

        if (!IsPostBack)
        {
            BindMenu();
        }


    }

    #region Binding

    protected void BindAssignedMenus()
    {
        try
        {
            #region MCP
            objRoleManagementBE.RoleID = Convert.ToInt16(Session["RoleId"]);
            lstRBE = RoleManagementBL.GetAssignedMenu(Constants.usp_ManageRole, objRoleManagementBE);
            if (lstRBE != null)
            {
                if (lstRBE.Count > 0)
                {
                    if (lstRBE.Count > 0)
                    {
                        for (int i = 0; i < lstRBE.Count; i++)
                        {
                            string Id = lstRBE[i].MenuID.ToString();

                            foreach (RepeaterItem rptpItem in rptMCPPMenu.Items)
                            {
                                CheckBox chkMCPPW = rptpItem.FindControl("chkMCPPW") as CheckBox;

                                if (chkMCPPW.Attributes["rel"].ToString() == Id)
                                {
                                    chkMCPPW.Checked = true;
                                }

                                Repeater rptMCPSMenu = rptpItem.FindControl("rptMCPSMenu") as Repeater;

                                if (rptMCPSMenu != null)
                                {
                                    foreach (RepeaterItem rptSItem in rptMCPSMenu.Items)
                                    {
                                        CheckBox chkMCPSW = rptSItem.FindControl("chkMCPSW") as CheckBox;

                                        if (chkMCPSW.Attributes["rel"].ToString() == Id)
                                        {
                                            chkMCPSW.Checked = true;
                                        }

                                        Repeater rptMCPSSMenu = rptSItem.FindControl("rptMCPSSMenu") as Repeater;

                                        if (rptMCPSSMenu != null)
                                        {
                                            foreach (RepeaterItem rptSSItem in rptMCPSSMenu.Items)
                                            {
                                                CheckBox chkMCPSSW = rptSSItem.FindControl("chkMCPSSW") as CheckBox;

                                                if (chkMCPSSW.Attributes["rel"].ToString() == Id)
                                                {
                                                    chkMCPSSW.Checked = true;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            int PCnt = 0, PSCnt = 0;
            foreach (RepeaterItem rptpItem in rptMCPPMenu.Items)
            {
                CheckBox chkMCPPW = rptpItem.FindControl("chkMCPPW") as CheckBox;

                Repeater rptMCPSMenu = rptpItem.FindControl("rptMCPSMenu") as Repeater;

                if (rptMCPSMenu != null)
                {
                    foreach (RepeaterItem rptSItem in rptMCPSMenu.Items)
                    {
                        CheckBox chkMCPSW = rptSItem.FindControl("chkMCPSW") as CheckBox;

                        if (chkMCPSW.Checked)
                        {
                            PCnt++;
                        }

                        Repeater rptMCPSSMenu = rptSItem.FindControl("rptMCPSSMenu") as Repeater;

                        if (rptMCPSSMenu != null)
                        {
                            foreach (RepeaterItem rptSSItem in rptMCPSSMenu.Items)
                            {
                                CheckBox chkMCPSSW = rptSSItem.FindControl("chkMCPSSW") as CheckBox;

                                if (chkMCPSSW.Checked)
                                {
                                    PSCnt++;
                                }
                            }

                            if (rptMCPSSMenu.Items.Count == PSCnt && rptMCPSSMenu.Items.Count != 0)
                            {
                                chkMCPSW.Checked = true;
                                PSCnt = 0;
                                PCnt++;
                            }
                        }
                    }

                    if (rptMCPSMenu.Items.Count == PCnt && rptMCPSMenu.Items.Count != 0)
                    {
                        chkMCPPW.Checked = true;
                        PCnt = 0;
                    }
                }
            }

            #endregion
            #region CP
            if (lstRBE != null)
            {
                if (lstRBE.Count > 0)
                {
                    if (lstRBE.Count > 0)
                    {
                        for (int i = 0; i < lstRBE.Count; i++)
                        {
                            string Id = lstRBE[i].MenuID.ToString();

                            foreach (RepeaterItem rptpItem in rptStorePMenu.Items)
                            {
                                CheckBox chkstorePW = rptpItem.FindControl("chkstorePW") as CheckBox;

                                if (chkstorePW.Attributes["rel"].ToString() == Id)
                                {
                                    chkstorePW.Checked = true;
                                }

                                Repeater rptStoreSMenu = rptpItem.FindControl("rptStoreSMenu") as Repeater;

                                if (rptStoreSMenu != null)
                                {
                                    foreach (RepeaterItem rptSItem in rptStoreSMenu.Items)
                                    {
                                        CheckBox chkstoreSW = rptSItem.FindControl("chkstoreSW") as CheckBox;

                                        if (chkstoreSW.Attributes["rel"].ToString() == Id)
                                        {
                                            chkstoreSW.Checked = true;
                                        }

                                        Repeater rptStoreSSMenu = rptSItem.FindControl("rptStoreSSMenu") as Repeater;

                                        if (rptStoreSSMenu != null)
                                        {
                                            foreach (RepeaterItem rptSSItem in rptStoreSSMenu.Items)
                                            {
                                                CheckBox chkStoreSSW = rptSSItem.FindControl("chkStoreSSW") as CheckBox;

                                                if (chkStoreSSW.Attributes["rel"].ToString() == Id)
                                                {
                                                    chkStoreSSW.Checked = true;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            int SCnt = 0, SSCnt = 0;
            foreach (RepeaterItem rptpItem in rptStorePMenu.Items)
            {
                CheckBox chkstorePW = rptpItem.FindControl("chkstorePW") as CheckBox;

                Repeater rptStoreSMenu = rptpItem.FindControl("rptStoreSMenu") as Repeater;

                if (rptStoreSMenu != null)
                {
                    foreach (RepeaterItem rptSItem in rptStoreSMenu.Items)
                    {
                        CheckBox chkstoreSW = rptSItem.FindControl("chkstoreSW") as CheckBox;

                        if (chkstoreSW.Checked)
                        {
                            SCnt++;
                        }

                        Repeater rptStoreSSMenu = rptSItem.FindControl("rptStoreSSMenu") as Repeater;

                        if (rptStoreSSMenu != null)
                        {
                            foreach (RepeaterItem rptSSItem in rptStoreSSMenu.Items)
                            {
                                CheckBox chkStoreSSW = rptSSItem.FindControl("chkStoreSSW") as CheckBox;

                                if (chkStoreSSW.Checked)
                                {
                                    SSCnt++;
                                }
                            }

                            if (rptStoreSSMenu.Items.Count == SSCnt && rptStoreSSMenu.Items.Count != 0)
                            {
                                chkstoreSW.Checked = true;
                                SSCnt = 0;
                                SCnt++;
                            }
                        }
                    }

                    if (rptStoreSMenu.Items.Count == SCnt && rptStoreSMenu.Items.Count != 0)
                    {
                        chkstorePW.Checked = true;
                        SCnt = 0;
                    }
                }
            }
            #endregion
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void ReadRole()
    {
        try
        {
            if (Convert.ToInt16(Session["RoleId"]) > 0)
                objRoleManagementBE.RoleID = Convert.ToInt16(Session["RoleId"]);
            else
                objRoleManagementBE.RoleID = 0;
            List<RoleManagementBE> lstRBE = RoleManagementBL.ReadRole(Constants.usp_ManageRole, objRoleManagementBE);
            txtRoleName.Text = lstRBE[0].RoleName;
            if (lstRBE[0].IsActive)
            {
                chkIsActive.Checked = true;
            }
            else
            {
                chkIsActive.Checked = false;
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    protected void BindMenu()
    {
        try
        {

            if (Convert.ToInt16(Session["RoleId"]) > 0)
                objRoleManagementBE.RoleID = Convert.ToInt16(Session["RoleId"]);
            else
                objRoleManagementBE.RoleID = 0;
            lstRBE = RoleManagementBL.GetMenuList(Constants.usp_ManageMenu, objRoleManagementBE);
            if (lstRBE != null)
            {
                if (lstRBE.Count > 0)
                {
                    List<RoleManagementBE> lstMCP = lstRBE.FindAll(x => x.IsMCP == true);
                    lstMCP = lstMCP.FindAll(x => x.ParentMenuId == 0);
                    //lstMCP = lstMCP.FindAll(x => x.IsSys == false);

                    List<RoleManagementBE> lstStore = lstRBE.FindAll(x => x.IsMCP == false);
                    lstStore = lstStore.FindAll(x => x.ParentMenuId == 0);
                    //lstStore = lstStore.FindAll(x => x.IsSys == false);

                    rptMCPPMenu.DataSource = lstMCP;
                    rptMCPPMenu.DataBind();

                    rptStorePMenu.DataSource = lstStore;
                    rptStorePMenu.DataBind();

                    if (Convert.ToInt16(Session["RoleId"]) > 0)
                    {
                        ReadRole();
                        BindAssignedMenus();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    //protected void BindMenu()
    //{
    //    try
    //    {
    //        if (Convert.ToInt16(Session["RoleId"]) > 0)
    //            objRoleManagementBE.RoleID = Convert.ToInt16(Session["RoleId"]);
    //        else
    //            objRoleManagementBE.RoleID = 0;
    //        lstRBE = RoleManagementBL.GetMenuList(Constants.usp_ManageMenu, objRoleManagementBE);
    //        if (lstRBE != null)
    //        {
    //            if (lstRBE.Count > 0)
    //            {
    //                lstRBE = lstRBE.FindAll(x => x.ParentMenuId == 0);
    //                lstRBE = lstRBE.FindAll(x => x.IsSys == false);

    //                rptMCPPMenu.DataSource = lstRBE;
    //                rptMCPPMenu.DataBind();

    //                rptStorePMenu.DataSource = lstRBE;
    //                rptStorePMenu.DataBind();

    //                if (Convert.ToInt16(Session["RoleId"]) > 0)
    //                    BindAssignedMenus();
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Exceptions.WriteExceptionLog(ex);
    //    }
    //}

    protected void rptMCPPMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl spanCategoryName = e.Item.FindControl("spanCategoryName") as System.Web.UI.HtmlControls.HtmlGenericControl;
                Repeater rptMCPSMenu = e.Item.FindControl("rptMCPSMenu") as Repeater;
                Label lblisactive = e.Item.FindControl("lblisactive") as Label;
                lblisactive.Text = (DataBinder.Eval(e.Item.DataItem, "IsActive").ToString() == "True" ? "Acitve" : "InActive");
                CheckBox chkMCPPW = e.Item.FindControl("chkMCPPW") as CheckBox;
                Label lblPCName = e.Item.FindControl("lblPCName") as Label;
                chkMCPPW.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString());
                chkMCPPW.Attributes.Add("Level", "PC");
                chkMCPPW.Attributes.Add("L1", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");
                chkMCPPW.Attributes.Add("L2", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");
                chkMCPPW.Attributes.Add("L3", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");

                lstRBE = RoleManagementBL.GetMenuList(Constants.usp_ManageMenu, objRoleManagementBE);
                lstRBE = lstRBE.FindAll(x => x.ParentMenuId.Equals(DataBinder.Eval(e.Item.DataItem, "MenuId")));
                if (lstRBE.Count != 0)
                {
                    spanCategoryName.Attributes.Add("class", "paneltab");
                }
                rptMCPSMenu.DataSource = lstRBE;
                rptMCPSMenu.DataBind();
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    protected void rptMCPSMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptMCPSSMenu = e.Item.FindControl("rptMCPSSMenu") as Repeater;
                System.Web.UI.HtmlControls.HtmlGenericControl spanCategoryName = e.Item.FindControl("spanCategoryName") as System.Web.UI.HtmlControls.HtmlGenericControl;
                CheckBox chkMCPSW = e.Item.FindControl("chkMCPSW") as CheckBox;
                Label lblisactive = e.Item.FindControl("lblisactive") as Label;
                lblisactive.Text = (DataBinder.Eval(e.Item.DataItem, "IsActive").ToString() == "True" ? "Acitve" : "InActive");

                chkMCPSW.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString());
                chkMCPSW.Attributes.Add("Level", "SC");
                chkMCPSW.Attributes.Add("L1", DataBinder.Eval(e.Item.DataItem, "ParentMenuId").ToString() + "W");
                chkMCPSW.Attributes.Add("L2", DataBinder.Eval(e.Item.DataItem, "ParentMenuId").ToString() + "W");
                chkMCPSW.Attributes.Add("L2", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");

                lstRBE = RoleManagementBL.GetMenuList(Constants.usp_ManageMenu, objRoleManagementBE);
                lstRBE = lstRBE.FindAll(x => x.ParentMenuId.Equals(DataBinder.Eval(e.Item.DataItem, "MenuId")));


                if (lstRBE.Count != 0)
                {
                    spanCategoryName.Attributes.Add("class", "arrowUl paneltab");
                }
                else
                {
                    spanCategoryName.Attributes.Add("class", "arrowUl");
                }

                rptMCPSSMenu.DataSource = lstRBE;
                rptMCPSSMenu.DataBind();
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    protected void rptMCPSSMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    CheckBox chkMCPSSW = e.Item.FindControl("chkMCPSSW") as CheckBox;
        //    Label lblisactive = e.Item.FindControl("lblisactive") as Label;
        //    lblisactive.Text = (DataBinder.Eval(e.Item.DataItem, "IsActive").ToString() == "True" ? "Acitve" : "InActive");

        //    chkMCPSSW.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString());
        //    chkMCPSSW.Attributes.Add("Level", "SSC");
        //    chkMCPSSW.Attributes.Add("L1", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");
        //    chkMCPSSW.Attributes.Add("L2", DataBinder.Eval(e.Item.DataItem, "ParentMenuId").ToString() + "W");
        //    chkMCPSSW.Attributes.Add("L3", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");

        //}
    }

    protected void rptStoreSMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptStoreSSMenu = e.Item.FindControl("rptStoreSSMenu") as Repeater;
                System.Web.UI.HtmlControls.HtmlGenericControl spanCategoryName = e.Item.FindControl("spanCategoryName") as System.Web.UI.HtmlControls.HtmlGenericControl;
                CheckBox chkstoreSW = e.Item.FindControl("chkstoreSW") as CheckBox;
                Label lblisactive = e.Item.FindControl("lblisactive") as Label;
                lblisactive.Text = (DataBinder.Eval(e.Item.DataItem, "IsActive").ToString() == "True" ? "Acitve" : "InActive");

                chkstoreSW.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString());
                chkstoreSW.Attributes.Add("Level", "SC");
                chkstoreSW.Attributes.Add("L1", DataBinder.Eval(e.Item.DataItem, "ParentMenuId").ToString() + "W");
                chkstoreSW.Attributes.Add("L2", DataBinder.Eval(e.Item.DataItem, "ParentMenuId").ToString() + "W");
                chkstoreSW.Attributes.Add("L2", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");

                lstRBE = RoleManagementBL.GetMenuList(Constants.usp_ManageMenu, objRoleManagementBE);
                lstRBE = lstRBE.FindAll(x => x.ParentMenuId.Equals(DataBinder.Eval(e.Item.DataItem, "MenuId")));


                if (lstRBE.Count != 0)
                {
                    spanCategoryName.Attributes.Add("class", "arrowUl paneltab");
                }
                else
                {
                    spanCategoryName.Attributes.Add("class", "arrowUl");
                }

                rptStoreSSMenu.DataSource = lstRBE;
                rptStoreSSMenu.DataBind();
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    protected void rptStoreSSMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    CheckBox chkMCPSSW = e.Item.FindControl("chkMCPSSW") as CheckBox;
        //    Label lblisactive = e.Item.FindControl("lblisactive") as Label;
        //    lblisactive.Text = (DataBinder.Eval(e.Item.DataItem, "IsActive").ToString() == "True" ? "Acitve" : "InActive");

        //    chkMCPSSW.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString());
        //    chkMCPSSW.Attributes.Add("Level", "SSC");
        //    chkMCPSSW.Attributes.Add("L1", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");
        //    chkMCPSSW.Attributes.Add("L2", DataBinder.Eval(e.Item.DataItem, "ParentMenuId").ToString() + "W");
        //    chkMCPSSW.Attributes.Add("L3", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");

        //}
    }

    protected void rptStorePMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl spanCategoryName = e.Item.FindControl("spanCategoryName") as System.Web.UI.HtmlControls.HtmlGenericControl;
                Repeater rptStoreSMenu = e.Item.FindControl("rptStoreSMenu") as Repeater;
                Label lblisactive = e.Item.FindControl("lblisactive") as Label;
                lblisactive.Text = (DataBinder.Eval(e.Item.DataItem, "IsActive").ToString() == "True" ? "Acitve" : "InActive");
                CheckBox chkstorePW = e.Item.FindControl("chkstorePW") as CheckBox;
                Label lblPCName = e.Item.FindControl("lblPCName") as Label;
                chkstorePW.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString());
                chkstorePW.Attributes.Add("Level", "PC");
                chkstorePW.Attributes.Add("L1", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");
                chkstorePW.Attributes.Add("L2", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");
                chkstorePW.Attributes.Add("L3", DataBinder.Eval(e.Item.DataItem, "MenuId").ToString() + "W");

                lstRBE = RoleManagementBL.GetMenuList(Constants.usp_ManageMenu, objRoleManagementBE);
                lstRBE = lstRBE.FindAll(x => x.ParentMenuId.Equals(DataBinder.Eval(e.Item.DataItem, "MenuId")));
                if (lstRBE.Count != 0)
                {
                    spanCategoryName.Attributes.Add("class", "paneltab");
                }
                rptStoreSMenu.DataSource = lstRBE;
                rptStoreSMenu.DataBind();
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    #endregion

    #region Save and update

    protected void btnSaveRole_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            bool res = false;
            int roleId = SaveRole();
            if (mode == "a")
            {
                if (roleId == 0)
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Role already Exists.", AlertType.Failure);
                else
                    res = SaveMenu(roleId);
                if (res)
                {
                    objBase.CreateActivityLog("Add / Edit Role", "Added", roleId + "_" + txtRoleName.Text);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Role has been added successfully.", "RoleList.aspx", AlertType.Success);
                    //Response.Redirect("RoleManagementList.aspx");
                }
            }
            else
            {
                res = SaveMenu(Convert.ToInt16(Session["RoleId"]));
                if (res)
                {
                    objBase.CreateActivityLog("Add / Edit Role", "Updated", roleId + "_" + txtRoleName.Text);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Role has been Updated successfully.", "RoleList.aspx", AlertType.Success);
                }
            }
        }
    }

    protected string GetSelectedMenus()
    {
        string strRes = "";
        int counterp = 1;
        try
        {
            #region MCP
            foreach (RepeaterItem rptpItem in rptMCPPMenu.Items)
            {
                CheckBox chkMCPPW = rptpItem.FindControl("chkMCPPW") as CheckBox;

                Repeater rptSC = rptpItem.FindControl("rptMCPSMenu") as Repeater;

                if (chkMCPPW.Checked)
                {
                    strRes = strRes + "," + chkMCPPW.Attributes["rel"].ToString();
                }
                foreach (RepeaterItem rptSItem in rptSC.Items)
                {
                    CheckBox chkMCPSW = rptSItem.FindControl("chkMCPSW") as CheckBox;
                    Repeater rptSSC = rptSItem.FindControl("rptMCPSSMenu") as Repeater;
                    if (chkMCPSW.Checked)
                    {
                        string a = chkMCPPW.Attributes["rel"].ToString();
                        if ((strRes.Contains("," + a)) || (strRes.Contains(a + ",")))
                        {

                        }
                        else if (counterp == 1)
                        {
                            strRes = strRes + "," + chkMCPPW.Attributes["rel"].ToString();
                            counterp += 1;
                        }

                    }

                    if (rptSSC.Items.Count == 0)
                    {
                        if (chkMCPSW.Checked)
                        {
                            strRes = strRes + "," + chkMCPSW.Attributes["rel"].ToString();
                        }
                    }
                    else
                    {
                        foreach (RepeaterItem rptSSItem in rptSSC.Items)
                        {
                            CheckBox chkMCPSSW = rptSSItem.FindControl("chkMCPSSW") as CheckBox;
                            if (chkMCPSSW.Checked)
                            {
                                strRes = strRes + "," + chkMCPSSW.Attributes["rel"].ToString();
                            }
                        }
                    }
                }

            }
            #endregion
            #region CP
            foreach (RepeaterItem rptpItem in rptStorePMenu.Items)
            {
                CheckBox chkstorePW = rptpItem.FindControl("chkstorePW") as CheckBox;

                Repeater rptSC = rptpItem.FindControl("rptStoreSMenu") as Repeater;

                if (chkstorePW.Checked)
                {
                    strRes = strRes + "," + chkstorePW.Attributes["rel"].ToString();
                }
                foreach (RepeaterItem rptSItem in rptSC.Items)
                {
                    CheckBox chkstoreSW = rptSItem.FindControl("chkstoreSW") as CheckBox;
                    Repeater rptSSC = rptSItem.FindControl("rptStoreSSMenu") as Repeater;
                    if (chkstoreSW.Checked)
                    {
                        string a = chkstorePW.Attributes["rel"].ToString();
                        if ((strRes.Contains("," + a)) || (strRes.Contains(a + ",")))
                        {

                        }
                        else if (counterp == 1)
                        {
                            strRes = strRes + "," + chkstorePW.Attributes["rel"].ToString();
                            counterp += 1;
                        }

                    }

                    if (rptSSC.Items.Count == 0)
                    {
                        if (chkstoreSW.Checked)
                        {
                            strRes = strRes + "," + chkstoreSW.Attributes["rel"].ToString();
                        }
                    }
                    else
                    {
                        foreach (RepeaterItem rptSSItem in rptSSC.Items)
                        {
                            CheckBox chkStoreSSW = rptSSItem.FindControl("chkStoreSSW") as CheckBox;
                            if (chkStoreSSW.Checked)
                            {
                                strRes = strRes + "," + chkStoreSSW.Attributes["rel"].ToString();
                            }
                        }
                    }
                }

            }
            #endregion
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }

        return strRes.Trim(',');
    }

    protected int SaveRole()
    {
        RoleManagementBE objRoleManagementBE = new RoleManagementBE();
        int res = 0;
        try
        {
            UserBE lst = Session["User"] as UserBE;
            objRoleManagementBE.RoleName = txtRoleName.Text.ToString();
            objRoleManagementBE.CreatedBy = lst.UserId;
            if (chkIsActive.Checked)
                objRoleManagementBE.IsActive = true;
            else
                objRoleManagementBE.IsActive = false;
            if (mode == "a")
            {
                objRoleManagementBE.RoleID = 0;
            }
            else
            {
                objRoleManagementBE.RoleID = Convert.ToInt16(Session["RoleId"]);
            }
            res = RoleManagementBL.InsertUpdateRoles(Constants.usp_ManageRole, objRoleManagementBE, mode);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return res;
    }

    protected bool SaveMenu(int RoleId)
    {
        RoleManagementBE objRoleManagementBE = new RoleManagementBE();
        bool res = false;
        try
        {
            UserBE lst = Session["User"] as UserBE;
            if (mode == "a")
            {
                objRoleManagementBE.RoleName = txtRoleName.Text.ToString();
                objRoleManagementBE.RoleID = RoleId;
                objRoleManagementBE.CreatedBy = lst.UserId;
                objRoleManagementBE.MenuIdCSV = GetSelectedMenus();
            }
            else
            {
                objRoleManagementBE.RoleName = txtRoleName.Text.ToString();
                objRoleManagementBE.RoleID = RoleId;
                objRoleManagementBE.MenuIdCSV = GetSelectedMenus();
                objRoleManagementBE.CreatedBy = lst.UserId;
            }
            res = RoleManagementBL.InsertUpdateMenu(Constants.usp_ManageMenu, objRoleManagementBE, mode);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return res;
    }


    #endregion

    protected void checkAllMCPcheckbox()
    {
        try
        {
            Control HeaderTemplate = rptMCPPMenu.Controls[0].Controls[0];
            CheckBox chkMCPSelectAll = HeaderTemplate.FindControl("chkMCPSelectAll") as CheckBox;
            if (chkMCPSelectAll.Checked)
            {
                foreach (RepeaterItem rptpItem in rptMCPPMenu.Items)
                {
                    CheckBox chkMCPPW = rptpItem.FindControl("chkMCPPW") as CheckBox;
                    chkMCPPW.Checked = true;
                    Repeater rptMCPSMenu = rptpItem.FindControl("rptMCPSMenu") as Repeater;
                    foreach (RepeaterItem rptSItem in rptMCPSMenu.Items)
                    {
                        CheckBox chkMCPSW = rptSItem.FindControl("chkMCPSW") as CheckBox;
                        chkMCPSW.Checked = true;
                    }
                }
            }
            else
            {
                foreach (RepeaterItem rptpItem in rptMCPPMenu.Items)
                {
                    CheckBox chkMCPPW = rptpItem.FindControl("chkMCPPW") as CheckBox;
                    chkMCPPW.Checked = false;
                    Repeater rptMCPSMenu = rptpItem.FindControl("rptMCPSMenu") as Repeater;
                    foreach (RepeaterItem rptSItem in rptMCPSMenu.Items)
                    {
                        CheckBox chkMCPSW = rptSItem.FindControl("chkMCPSW") as CheckBox;
                        chkMCPSW.Checked = false;
                    }
                }
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    protected void checkAllStorecheckbox()
    {
        try
        {
            Control HeaderTemplate = rptStorePMenu.Controls[0].Controls[0];
            CheckBox chkStoreSelectAll = HeaderTemplate.FindControl("chkStoreSelectAll") as CheckBox;
            if (chkStoreSelectAll.Checked)
            {
                foreach (RepeaterItem rptpItem in rptStorePMenu.Items)
                {
                    CheckBox chkstorePW = rptpItem.FindControl("chkstorePW") as CheckBox;
                    chkstorePW.Checked = true;
                    Repeater rptStoreSMenu = rptpItem.FindControl("rptStoreSMenu") as Repeater;
                    foreach (RepeaterItem rptSItem in rptStoreSMenu.Items)
                    {
                        CheckBox chkstoreSW = rptSItem.FindControl("chkstoreSW") as CheckBox;
                        chkstoreSW.Checked = true;
                    }
                }
            }
            else
            {
                foreach (RepeaterItem rptpItem in rptStorePMenu.Items)
                {
                    CheckBox chkstorePW = rptpItem.FindControl("chkstorePW") as CheckBox;
                    chkstorePW.Checked = false;
                    Repeater rptStoreSMenu = rptpItem.FindControl("rptStoreSMenu") as Repeater;
                    foreach (RepeaterItem rptSItem in rptStoreSMenu.Items)
                    {
                        CheckBox chkstoreSW = rptSItem.FindControl("chkstoreSW") as CheckBox;
                        chkstoreSW.Checked = false;
                    }
                }
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    protected void chkStoreSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        checkAllStorecheckbox();
    }

    protected void chkMCPSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        checkAllMCPcheckbox();
    }










}