﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="RoleList.aspx.cs" Inherits="RoleManagement_RoleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function ValidateTextbox(clientId) {
           
            var obj = document.getElementById(clientId);
            var iChars = "!@#$%^\"&+=[]{}|:<>?"
            //alert(obj.value.length);
            //"!@#$%^&*()+=-[]\\\';,./{}|\":<>?"
            for (var i = 0; i < obj.value.length; i++) {
                if (iChars.indexOf(obj.value.charAt(i)) != -1) {
                    alert("Search Text has special characters. \nThese are not allowed.\n Please remove them and try again.");
                    obj.value = "";
                    return false;
                }
            }
            return true;
        }
    </script>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Setting</li>
            <li>Role Management</li>
        </ol>
    </div>
    <div class="admin_page">
        <section class="container mainContainer role_management role_listing">
            <div class="wrap_container ">



                <div class="content">
                   
                        <section class="mainContainer">
                            <h3 class="mainHead">Manage Role
                            </h3>
                          
                                <div class="add_newrole">
                                        <asp:Button ID="btnAddNew" runat="server" CssClass="save" Text="Add New Role" OnClick="btnAddNew_Click" />
                                    
                                </div>
                                <div class="clear">
                               
                                <div id="accordionx" class="search2 accordion">
                    <h3>
                        Search Roles</h3>
                    <div class="searchInnerbox">
                        <div class="box1 box1Extra" style="background: none !important;">
                            <asp:Panel runat="server" DefaultButton="btnSearch">
                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Role Name" CssClass="input1"></asp:TextBox>
                                <asp:HiddenField ID="hdnrolename" runat="server" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                     OnClick="btnSearch_Click" CssClass="save" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="save" OnClick="btnReset_Click"/>
                            </asp:Panel>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="customer" style="margin: 0px !important;">
                                <span class="allcosutomer">All Roles -
                    <asp:Literal ID="ltrNoOfRecords" runat="server" Text=""></asp:Literal></span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <section class=" all_customer dragblnextBlock manage_pro">
                                    <asp:GridView runat="server" ID="gvRoles" TabIndex="0" CellPadding="3" Width="100%" PageSize="50"
                                        DataKeyNames="RoleId,RoleName" AutoGenerateColumns="False" OnPageIndexChanging="gvRoles_PageIndexChanging"
                                        CssClass="all_customer_inner allcutomerEtracls" EmptyDataText="No role exists."
                                        AllowPaging="true">
                                        <HeaderStyle Font-Bold="true" />
                                        <RowStyle HorizontalAlign="Center" Height="30px" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="left" Visible="false">
                                                <ItemTemplate>
                                                    <%#DataBinder.Eval(Container.DataItem, "RoleId")%>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Role Name" SortExpression="RoleName">
                                                <ItemTemplate>
                                                    <%#DataBinder.Eval(Container.DataItem, "RoleName")%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="left" Width="50%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Eval("IsActive").ToString().ToLower() == "true" ? true : false %>' OnCheckedChanged="chkIsActive_CheckedChanged"
                                                        AutoPostBack="true"></asp:CheckBox>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                ItemStyle-VerticalAlign="Top">
                                                <ItemTemplate>
                                                    <div class="action">
                                                        <a class="icon" href="#"></a>
                                                        <div class="action_hover">
                                                            <span></span>
                                                            <div class="other_option">
                                                               
                                                                    <asp:LinkButton ID="lnkEditGroups" runat="server" CommandArgument='<%# Eval("RoleId") %>' OnClick="lnkEditGroups_Click">Edit</asp:LinkButton>
                                                           
                                                              
                                                               <%--     <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClientClick="javascript:return confirm('Are you sure you want to delete this role?')"
                                                                        CommandArgument='<%# Eval("RoleId") %>' CommandName="DeleteRole" Text="">Delete</asp:LinkButton>--%>
                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="paging" />
                                    </asp:GridView>
                                </section>
                            </div>
                            
                                <div class="add_newrole">
                                    
                                        <asp:Button ID="Button1" runat="server" CssClass="save" Text="Add New Role" OnClick="btnAddNew_Click" />
                             </div>
                            <div class="clearfix"></div>
                        </section>
               
                </div>
            </div>
        </section>
    </div>
</asp:Content>

