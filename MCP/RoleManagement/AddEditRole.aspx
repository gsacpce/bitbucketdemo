﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="AddEditRole.aspx.cs" Inherits="RoleManagement_AddEditRole" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="JS/Utilities.js" type="text/javascript"></script>
    <%-- <link href="../CSS/admin.css" rel="stylesheet" />--%>
    <style>
        .tophd td {
            border-bottom: 1px solid #ccc;
            border-right: 1px solid #ccc;
            padding: 13px 5px;
            text-align: center;
        }

        .DetailsBC {
            border: 1px solid #ccc;
            border-radius: 0;
            margin: 15px;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            margin: 0;
            padding: 0;
        }

        ul {
            margin: 0;
            padding: 0;
        }

        .tophd td {
            border-bottom: 1px solid #ccc;
            border-right: 1px solid #ccc;
            padding: 13px 5px;
            text-align: center;
        }

        .DetailsBC .dragbltop tr td {
            border-right: medium none;
        }

        .dragbltop tr td.firsttd {
            text-align: left;
            width: 40%;
        }

        .DetailsBC td.firsttd {
            border-left: medium none;
        }

        .dragbltop tr td {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            border-color: -moz-use-text-color #cccccc #cccccc;
            border-image: none;
            border-style: none solid solid;
            border-width: medium 1px 1px;
            padding: 13px 5px;
            text-align: center;
        }

        .top_roldiv {
            margin-top: 20px;
        }

        .addedittble .paneltab, .addedittblesecond .paneltab {
            background: url("../images/UI/minus.png") no-repeat scroll left center;
            cursor: pointer;
            padding-left: 0px;
        }

        .addedittble td.firsttd {
            background: none repeat scroll 0 0 #F7F7F7;
            font-weight: bold;
        }

        .addedittble .paneltabplus, .addedittblesecond .paneltabplus {
            background: url("../images/UI/plus.png") no-repeat scroll left center rgba(0, 0, 0, 0) !important;
        }

        .paneltab {
            cursor: pointer;
            background: url("../images/UI/minus.png") no-repeat scroll right center transparent;
            padding-right: 20px;
        }

        .paneltabplus {
            background: url("../images/UI/plus.png") no-repeat scroll right center transparent !important;
        }

        .arrowUl {
            background: url("../images/UI/arrow.png") no-repeat scroll left center transparent;
            margin: 0 0 0 9px;
            padding: 0 0 0 22px !important;
        }

        .secondul1 .arrowUl {
            background: url("../images/UI/arrow.png") no-repeat scroll left center transparent;
            margin: 0 0 0 50px;
            padding: 0 0 0 23px !important;
        }
        .hide
        {
            display:none;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {

            $(".paneltab").bind("click", function () {
                fn_Toggle($(this));
            });

            $('.Radios input').click(function () {
                jQuery('.Radios').removeClass('chkBoxmark');
                jQuery(this).parent().addClass('chkBoxmark');
                return false;
            });

            $('.chkBox input').click(function () {
                alert("h1");
                jQuery(this).parent().toggleClass('chkBoxmark');
                return false;
            });

            $('.CsA h3').click(function () {
                $(this).next().stop().slideToggle();
                $(this).toggleClass('rightA');
            });
        });
    </script>
    <script type="text/javascript">
        function SelectCheckbox(Id) {
            var Level = $('#' + Id)[0].parentElement.attributes["level"].nodeValue;
            var CurrId = $('#' + Id)[0].parentElement.attributes["rel"].nodeValue;
            var IsChecked = $('#' + Id)[0].checked;

            var PCnt = 0;
            var SCnt = 0;
            var SSCnt = 0;

            if (Level == "PC") {
                //subcat
                $('span[l1="' + CurrId + 'W"]').each(function (index) {

                    this.childNodes[0].checked = IsChecked;

                    //sub cat
                    $('span[l2="' + CurrId + 'W"]').each(function (index) {
                        this.childNodes[0].checked = IsChecked;
                    });
                });
            }

            if (Level == "SC") {
                //sub sub cat
                $('span[l2="' + CurrId + 'W"]').each(function (index) {
                    this.childNodes[0].checked = IsChecked;
                });
            }

        }
    </script>
    <script>
        function SelectAllCheckboxes(chk) {
            
            alert($(this).attr('Checked'));
            $('li >table >tbody >tr >td > span >input:checkbox').attr('checked', chk.checked);

        }

    </script>

    <script>
        function fn_Toggle(obj) {
            $(obj).toggleClass("paneltabplus");
            $(obj).parent().parent().parent().parent().parent().find('ul').slideToggle("slow");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <div class="container">
        <ol class="breadcrumb">
           <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Setting</li>
            <li>Role Management</li>
        </ol>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content" id="role_magmrnt">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Manage Role
                        </h3>

                        <div class="top_roldiv">
                            <div class="repeterV">
                                <div class="in col-md-2">
                                    <asp:Label ID="lblname" runat="server" Text="Name of Role"></asp:Label><span style="color: Red;">*</span>
                                </div>
                                <div class="in col-md-3">
                                    <asp:TextBox ID="txtRoleName" Style="border: 1px solid #ccc;" runat="server"></asp:TextBox>

                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="repeterV">
                                <div class="in col-md-2">
                                    Status:
                                </div>
                                <div class="in col-md-3">
                                    <asp:CheckBox ID="chkIsActive" runat="server" Style="margin-top: 14px;" />
                                </div>
                            </div>


                            <div class="clearfix"></div>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ControlToValidate="txtRoleName"
                                runat="server" ErrorMessage="Please enter role name" ValidationGroup="add"></asp:RequiredFieldValidator>
                            <div><h3 class="mainHead">Main Control Panel's Menus</h3></div>
                            <section id="divsection" class="all_customer dragblnextBlock manage_pro DetailsBC">
                                <asp:Repeater ID="rptMCPPMenu" runat="server" OnItemDataBound="rptMCPPMenu_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="tblMCPPMenu" width="100%" cellpadding="0" cellspacing="0" class="tophd">
                                            <tr>
                                                <td style="width: 40%; font-size: medium;">Menu Name
                                                </td>
                                                <td style="width: 60%; font-size: medium;border-right: none;">Select(Select All 
                                        <asp:CheckBox ID="chkMCPSelectAll" runat="server" Text="" OnCheckedChanged="chkMCPSelectAll_CheckedChanged" AutoPostBack="true" />
                                                    )
                                                </td>
                                                <td style="font-size: medium; border-right: none;">
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li class="firstLitab">
                                            <table width="100%" border="0" class="dragbltop addedittble">
                                                <tbody>
                                                    <tr id="trCategory" runat="server">
                                                        <td class="firsttd">
                                                            <span id="spanCategoryName" runat="server"></span>
                                                            <asp:Label ID="lblPCName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MenuName") %>'></asp:Label>

                                                        </td>
                                                        <td class="secTds ">
                                                            <asp:CheckBox ID="chkMCPPW" runat="server" CssClass="checkBoxClass" onclick="SelectCheckbox(this.id)" />
                                                        </td>
                                                        <td class="hide">
                                                            <asp:Label ID="lblisactive" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <ul class="secondul">
                                                <asp:Repeater ID="rptMCPSMenu" runat="server" OnItemDataBound="rptMCPSMenu_ItemDataBound">
                                                    <ItemTemplate>
                                                        <li>
                                                            <table width="100%" border="0" class="dragbltop addedittblesecond">
                                                                <tbody>
                                                                    <tr id="trCategory" runat="server">
                                                                        <td class="firsttd">
                                                                            <span id="spanCategoryName" runat="server"></span>
                                                                            <asp:Label ID="smenuname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MenuName") %>'></asp:Label>

                                                                        </td>
                                                                        <td class="secTds">
                                                                            <asp:CheckBox ID="chkMCPSW" runat="server" CssClass="checkBoxClass" onclick="SelectCheckbox(this.id)" />
                                                                        </td>
                                                                        <td class="hide">
                                                                            <asp:Label ID="lblisactive" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <ul class="secondul secondul1">
                                                                <asp:Repeater ID="rptMCPSSMenu" runat="server" OnItemDataBound="rptMCPSSMenu_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <li>
                                                                            <table width="100%" border="0" class="dragbltop addedittblesecond">
                                                                                <tbody>
                                                                                    <tr id="trCategory" runat="server">
                                                                                        <td class="firsttd">
                                                                                            <span class="arrowUl">
                                                                                                <asp:Label ID="ssmenuname" Text='<%# DataBinder.Eval(Container.DataItem, "MenuName") %>'
                                                                                                    runat="server"></asp:Label>
                                                                                            </span>
                                                                                        </td>
                                                                                        <td class="secTds">
                                                                                            <asp:CheckBox ID="chkMCPSSW" runat="server" CssClass="checkBoxClass" onclick="SelectCheckbox(this.id)" />
                                                                                        </td>
                                                                                        <td class="hide">
                                                                                            <asp:Label ID="lblisactive" runat="server"></asp:Label>
                                                                                            - </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </section>
                            <div><h3 class="mainHead">Store Control Panel's Menus</h3></div>
                             <section id="divsectionStore" class="all_customer dragblnextBlock manage_pro DetailsBC">
                                <asp:Repeater ID="rptStorePMenu" runat="server" OnItemDataBound="rptStorePMenu_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="tblStorePMenu" width="100%" cellpadding="0" cellspacing="0" class="tophd">
                                            <tr>
                                                <td style="width: 40%; font-size: medium;">Menu Name
                                                </td>
                                                <td style="width: 60%; font-size: medium;border-right: none;">Select(Select All 
                                        <asp:CheckBox ID="chkStoreSelectAll" runat="server" Text="" OnCheckedChanged="chkStoreSelectAll_CheckedChanged" AutoPostBack="true" />
                                                    )
                                                </td>
                                                <td style="font-size: medium; border-right: none;">
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li class="firstLitab">
                                            <table width="100%" border="0" class="dragbltop addedittble">
                                                <tbody>
                                                    <tr id="trCategory" runat="server">
                                                        <td class="firsttd">
                                                            <span id="spanCategoryName" runat="server"></span>
                                                            <asp:Label ID="lblPCName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MenuName") %>'></asp:Label>

                                                        </td>
                                                        <td class="secTds ">
                                                            <asp:CheckBox ID="chkstorePW" runat="server" CssClass="checkBoxClass" onclick="SelectCheckbox(this.id)" />
                                                        </td>
                                                        <td class="hide">
                                                            <asp:Label ID="lblisactive" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <ul class="secondul">
                                                <asp:Repeater ID="rptStoreSMenu" runat="server" OnItemDataBound="rptStoreSMenu_ItemDataBound">
                                                    <ItemTemplate>
                                                        <li>
                                                            <table width="100%" border="0" class="dragbltop addedittblesecond">
                                                                <tbody>
                                                                    <tr id="trCategory" runat="server">
                                                                        <td class="firsttd">
                                                                            <span id="spanCategoryName" runat="server"></span>
                                                                            <asp:Label ID="smenuname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MenuName") %>'></asp:Label>

                                                                        </td>
                                                                        <td class="secTds">
                                                                            <asp:CheckBox ID="chkstoreSW" runat="server" CssClass="checkBoxClass" onclick="SelectCheckbox(this.id)" />
                                                                        </td>
                                                                        <td class="hide">
                                                                            <asp:Label ID="lblisactive" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <ul class="secondul secondul1">
                                                                <asp:Repeater ID="rptStoreSSMenu" runat="server" OnItemDataBound="rptStoreSSMenu_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <li>
                                                                            <table width="100%" border="0" class="dragbltop addedittblesecond">
                                                                                <tbody>
                                                                                    <tr id="trCategory" runat="server">
                                                                                        <td class="firsttd">
                                                                                            <span class="arrowUl">
                                                                                                <asp:Label ID="ssmenuname" Text='<%# DataBinder.Eval(Container.DataItem, "MenuName") %>'
                                                                                                    runat="server"></asp:Label>
                                                                                            </span>
                                                                                        </td>
                                                                                        <td class="secTds">
                                                                                            <asp:CheckBox ID="chkStoreSSW" runat="server" CssClass="checkBoxClass" onclick="SelectCheckbox(this.id)" />
                                                                                        </td>
                                                                                        <td class="hide">
                                                                                            <asp:Label ID="lblisactive" runat="server"></asp:Label>
                                                                                            - </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </section>
                            <div class="button_section">
                                   <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="List" ShowSummary="false" ShowMessageBox="true"
                                        ValidationGroup="add" CssClass="errortext" />
                                <asp:Button ID="btnSaveRole" runat="server" CssClass="btn" Text="Save" ValidationGroup="add" OnClick="btnSaveRole_Click" />
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

