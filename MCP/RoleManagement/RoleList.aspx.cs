﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;


public partial class RoleManagement_RoleList : System.Web.UI.Page
{
    RoleManagementBE objRoleManagementBE = new RoleManagementBE();
    List<RoleManagementBE> lstRBE = new List<RoleManagementBE>();
    BasePage objBase = new BasePage();

    protected void Page_Load(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized(HttpContext.Current.Request.Url.Segments[HttpContext.Current.Request.Url.Segments.Length - 1]);
        if (!IsPostBack)
        {
            BindRoles();
        }
        btnSearch.Attributes.Add("onmousedown", "ValidateTextbox('" + txtSearch.ClientID + "');");
    }
    protected void BindRoles()
    {
        try
        {
            lstRBE = RoleManagementBL.GetListRole(Constants.usp_ManageRole);
            if (lstRBE != null)
            {
                if (lstRBE.Count > 0)
                {
                    RoleManagementBE superAdminRole = lstRBE.FirstOrDefault(x => x.RoleName.ToLower() == "superadmin");
                    if (superAdminRole != null)
                        lstRBE.Remove(superAdminRole);
                    gvRoles.DataSource = lstRBE;
                    ltrNoOfRecords.Text = lstRBE[0].TotalRow.ToString();
                    //gvList.VirtualItemCount = lstRBE[0].TotalRow;
                    gvRoles.DataBind();
                }
                else
                {
                    gvRoles.DataSource = null;
                    gvRoles.DataBind();
                    ltrNoOfRecords.Text = "0";
                }
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void SearchRole()
    {
        try
        {
            objRoleManagementBE.RoleName = txtSearch.Text.ToString();
            lstRBE = RoleManagementBL.SearchRole(Constants.usp_ManageRole, objRoleManagementBE);
            if (lstRBE != null)
            {
                if (lstRBE.Count > 0)
                {
                    RoleManagementBE superAdminRole = lstRBE.FirstOrDefault(x => x.RoleName.ToLower() == "superadmin");
                    if (superAdminRole != null)
                        lstRBE.Remove(superAdminRole);
                    gvRoles.DataSource = lstRBE;
                    ltrNoOfRecords.Text = lstRBE.Count.ToString();
                    gvRoles.DataBind();
                }
            }
            else
            {
                gvRoles.DataSource = null;
                gvRoles.DataBind();
                ltrNoOfRecords.Text = "0";
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session["RoleId"] = 0;
        Response.Redirect("AddEditRole.aspx?mode=a");
    }
    protected void lnkEditGroups_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            int RoleId = Convert.ToInt32(lnk.CommandArgument);
            Session["RoleId"] = RoleId;
            Response.Redirect("AddEditRole.aspx?mode=e");
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void chkIsActive_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            int res;
            CheckBox chkIsActive = (sender as CheckBox);

            GridViewRow row = (sender as CheckBox).NamingContainer as GridViewRow;
            int id1 = Convert.ToInt16(gvRoles.DataKeys[row.RowIndex].Values["RoleId"]);
            string RoleName = Convert.ToString(gvRoles.DataKeys[row.RowIndex].Values["RoleName"]);

            objRoleManagementBE.RoleID = id1;


            if ((row.FindControl("chkIsActive") as CheckBox).Checked)
            {
                objRoleManagementBE.IsActive = true;
                res = RoleManagementBL.UpdateRolestatus(Constants.usp_ManageRole, objRoleManagementBE);
            }

            else
            {
                objRoleManagementBE.IsActive = false;
                res = RoleManagementBL.UpdateRolestatus(Constants.usp_ManageRole, objRoleManagementBE);
            }
            if (res > 0)
            {
                //true
               objBase.CreateActivityLog("Role Management List", "Updated", id1+"_"+RoleName);
                BindRoles();
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SearchRole();
    }
    protected void gvRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRoles.PageIndex = e.NewPageIndex;
        BindRoles();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtSearch.Text = "";
        BindRoles();
    }
}