﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using Microsoft.Security.Application;

public partial class UserManagement_AddEditUser : System.Web.UI.Page
{
    string mode;
    List<RoleManagementBE> lstRBE = new List<RoleManagementBE>();
    BasePage objBase = new BasePage();

    protected void Page_Init(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized("UserManagementList.aspx");
        try
        {
            mode = Request.QueryString["mode"];
            if (mode == "e")
            {
                password.Attributes.Add("style", "display:none;");
                btnAdd.Visible = false;
                btnUpdate.Visible = true;
                btnSubmitandAssign.Visible = false;
                btnUpdateandAssign.Visible = true;
            }
            else
            {
                password.Attributes.Add("style", "display:block;");
                btnUpdate.Visible = false;
                btnAdd.Visible = true;
                btnUpdateandAssign.Visible = false;
                btnSubmitandAssign.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
    protected void ClearAllFields()
    {
        txtpassword.Text = "";
        txtConfirmpwd.Text = "";
        txtContactNumber.Text = "";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtEmailId.Text = "";
        chkIsActive.Checked = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDropDownRole();
            if (mode == "a")
            {
                ClearAllFields();
            }
            else
            {
                ReadUser();
                btnSubmitandAssign.Visible = false;
            }
        }
    }
    protected void ReadUser()
    {
        UserBE objUserBE = new UserBE();
        try
        {
            if (mode == "e")
                objUserBE.UserId = Convert.ToInt16(Session["AEUserId"]);
            else
                objUserBE.UserId = 0;

            objUserBE.Action = Convert.ToInt16(DBAction.Select);
            List<UserBE> lstUBE = UserBL.GetMCPUsers<UserBE>(objUserBE);
            if (lstUBE.Count > 0)
            {
                txtConfirmpwd.Text = lstUBE[0].Password;
                ddlRole.SelectedValue = Convert.ToString(lstUBE[0].RoleId);
                txtFirstName.Text = lstUBE[0].FirstName;
                txtMiddleName.Text = lstUBE[0].MiddleName;
                txtLastName.Text = lstUBE[0].LastName;
                txtEmailId.Text = lstUBE[0].EmailId;
                txtContactNumber.Text = Convert.ToString(lstUBE[0].Phone);
                if (lstUBE[0].IsActive)
                    chkIsActive.Checked = true;
                else
                    chkIsActive.Checked = false;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
    protected void BindDropDownRole()
    {
        try
        {
            RoleManagementBE objRoleManagementBE = new RoleManagementBE();
            //objRoleManagementBE.Action = Convert.ToInt16(DBAction.Select);
            lstRBE = RoleManagementBL.GetListRole(Constants.usp_ManageRole);
            lstRBE = lstRBE.FindAll(x => x.IsActive == true);
            ddlRole.DataSource = lstRBE;
            ddlRole.DataValueField = "RoleId";
            ddlRole.DataTextField = "RoleName";
            ddlRole.DataBind();
            ddlRole.Items.Insert(0, new ListItem("-Select Role-", "0"));
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    protected int SaveUser()
    {
        UserBE objUserBE = new UserBE();
        int res = 0;
        try
        {
            string SaltPass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(txtpassword.Text.Trim()), "SHA512", null);
            UserBE lst = Session["User"] as UserBE;
            if (mode == "a")
            {
                objUserBE.Action = Convert.ToInt16(DBAction.Insert);
            }
            else
            {
                objUserBE.Action = Convert.ToInt16(DBAction.Update);
            }
            objUserBE.FirstName = txtFirstName.Text.ToString();
            objUserBE.MiddleName = txtMiddleName.Text.ToString();
            objUserBE.LastName = txtLastName.Text.ToString();
            objUserBE.Password = SaltPass;
            objUserBE.EmailId = txtEmailId.Text.ToString();
            objUserBE.RoleId = Convert.ToInt16(ddlRole.SelectedValue);
            objUserBE.Phone = txtContactNumber.Text.ToString();
            objUserBE.ModifiedBy = lst.UserId;
            if (chkIsActive.Checked)
                objUserBE.IsActive = true;
            else
                objUserBE.IsActive = false;
            if (mode == "a")
            {
                objUserBE.UserId = 0;
            }
            else
            {
                objUserBE.UserId = Convert.ToInt16(Session["AEUserId"]);
            }
            res = UserBL.InsertUpdateMCPUser(objUserBE);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return 0;
        }
        return res;
    }



    protected void SaveData(bool AssignStores)
    {
        if (Page.IsValid)
        {
            int UserId = SaveUser();
            if (mode == "a")
            {
                if (UserId == 0)
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "User already Exists.", AlertType.Failure);
                else
                    if (UserId > 0)
                    {
                        if (AssignStores)
                        {
                            objBase.CreateActivityLog("Add / Edit User Management", "Added", UserId + "_" + txtEmailId.Text);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "User has been added successfully.", "AssignStores.aspx?mode=a", AlertType.Success);
                        }
                        else
                        {
                            objBase.CreateActivityLog("Add / Edit User Management", "Added", UserId + "_" + txtEmailId.Text);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "User has been added successfully.", "UserManagementList.aspx", AlertType.Success);
                        }
                    }
            }
            else
            {
                if (AssignStores)
                {
                    objBase.CreateActivityLog("Add / Edit User Management", "Updated",UserId + " || " + txtFirstName.Text + " " + txtLastName.Text);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "User has been Updated successfully.", "AssignStores.aspx?mode=e", AlertType.Success);
                }
                else
                {
                    objBase.CreateActivityLog("Add / Edit User Management", "Updated",UserId + " || " + txtFirstName.Text + " " + txtLastName.Text);
                    Session["AEUserId"] = null;
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "User has been Updated successfully.", AlertType.Success);
                }
            }
        }
    }

    protected void btnSubmitandAssign_Click(object sender, EventArgs e)
    {
        // assign store
        SaveData(true);
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        SaveData(false);
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        // if you don't want assign store
        SaveData(false);
    }
}