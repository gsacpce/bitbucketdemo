﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserManagement_AssignStores : System.Web.UI.Page
{
    BasePage objBase = new BasePage();

    protected void Page_Load(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized(HttpContext.Current.Request.Url.Segments[HttpContext.Current.Request.Url.Segments.Length - 1]);
        if (!IsPostBack)
        {
            try
            {
                BindDropDownUser();
                SetSelectedUser();
                BindAssignedStores();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }
    }
    protected void SetSelectedUser()
    {
        try
        {
            if (Session["AEUserId"] != null)
            {
                int UserId = Convert.ToInt16(Session["AEUserId"]);
                ddlUsers.SelectedValue = Convert.ToString(UserId);
            }
            else
            {
                int UserId = 0;
                ddlUsers.SelectedValue = Convert.ToString(UserId);
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void BindDropDownUser()
    {
        UserBE objUserBE = new UserBE();
        List<UserBE> lstUBE = new List<UserBE>();
        try
        {
            objUserBE.Action = Convert.ToInt16(DBAction.Select);
            lstUBE = UserBL.GetMCPUsers<UserBE>(objUserBE);
            lstUBE = lstUBE.FindAll(x => x.IsActive == true);
            if (lstUBE != null)
            {
                if (lstUBE.Count > 0)
                {
                    ddlUsers.DataSource = lstUBE;
                    ddlUsers.DataValueField = "UserId";
                    ddlUsers.DataTextField = "UserName";
                    ddlUsers.DataBind();
                    ddlUsers.Items.Insert(0, new ListItem("-Select Users-", "0"));
                }
                else
                {
                    ddlUsers.DataSource = null;
                    ddlUsers.DataBind();
                }
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected string GetSelectedRecords()
    {
        string strRes = "";
        try
        {
            foreach (GridViewRow row in gvStore.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkIsAssign = (row.Cells[0].FindControl("chkIsAssign") as CheckBox);
                    if (chkIsAssign.Checked)
                    {
                        int StoreId = Convert.ToInt32(gvStore.DataKeys[row.RowIndex].Value);
                        strRes = strRes + Convert.ToString(StoreId);
                        strRes = strRes + ",";
                    }
                }
            }
            if (strRes.Length > 0)
            {
                strRes = strRes.Substring(0, strRes.Length - 1);
            }
        }
        catch(Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return strRes;
    }
    protected void BindAssignedStores()
    {
        try
        {
            UserBE objUserBE = new UserBE();
            objUserBE.Action = Convert.ToInt16(DBAction.Select);
            objUserBE.UserId = Convert.ToInt16(ddlUsers.SelectedValue);
            List<StoreBE> storeslist = UserBL.GetAssignedStores(objUserBE);
            if (storeslist != null && storeslist.Count > 0)
            {
                gvStore.DataSource = storeslist;
                gvStore.DataBind();
            }
            else
            {
                gvStore.DataSource = null;
                gvStore.DataBind();
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected bool SaveAssignedStores()
    {
        UserBE objUserBE = new UserBE();
        bool res = false;
        try
        {
            UserBE lst = Session["User"] as UserBE;
            objUserBE.IsActive = true;
            objUserBE.UserId = Convert.ToInt16(ddlUsers.SelectedValue);
            objUserBE.StoreIdCSV = GetSelectedRecords();
            objUserBE.CreatedBy = lst.UserId;
            objUserBE.Action = Convert.ToInt16(DBAction.Insert);

            res = UserBL.SaveAssignStore(objUserBE);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return res;
    }
    protected void GetAllStoreToAssign()
    {
        List<StoreBE> getAllStores = new List<StoreBE>();
        try
        {
            getAllStores = StoreBL.GetAllStoreDetails();
            getAllStores = getAllStores.FindAll(x => x.IsCreated == true);
            gvStore.DataSource = getAllStores;
            gvStore.DataBind();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }

    }

    protected void btnAssignStore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow row = (sender as Button).NamingContainer as GridViewRow;
                
                bool res = SaveAssignedStores();
                if (res)
                {
                    Session["AEUserId"] = null;
                    objBase.CreateActivityLog("Assign Stores", "Updated", Convert.ToString(ddlUsers.SelectedValue) + "_" + Convert.ToString(ddlUsers.SelectedItem.Text));
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Records are successfully updated.", "UserManagementList.aspx", AlertType.Success);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
    protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindAssignedStores();
    }
}