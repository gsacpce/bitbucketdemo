﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="AddEditUser.aspx.cs" Inherits="UserManagement_AddEditUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div class="container">
        <ol class="breadcrumb">
           <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Setting</li>
            <li>User Management</li>
        </ol>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content creat_mnguser" id="role_magmrnt">
                    <h3 class="mainHead">Manage User
                    </h3>
                    <p></p>
                    <section class="mainContainer padingbottom select_data">
                        <div class="DetailsB">
                            <br />
                            <br />
                            <div class="repeterV">
                                <div class="col-md-2">
                                    Full Name: <span style="color: Red;">*</span>
                                </div>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtFirstName" placeholder="First Name" runat="server" MaxLength="100" CssClass="col-md-3"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ControlToValidate="txtFirstName" runat="server" ErrorMessage="Please enter first name" ValidationGroup="ValidateAdd"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="rftupdateFName" Display="None" ControlToValidate="txtFirstName" runat="server" ErrorMessage="Please enter First name" ValidationGroup="ValidateUpdate"></asp:RequiredFieldValidator>
                                 
                                     <asp:TextBox ID="txtMiddleName" placeholder="Middle Name" runat="server" MaxLength="100" CssClass="col-md-3"></asp:TextBox>
                                   

                                    <asp:TextBox ID="txtLastName" placeholder="Last Name" CssClass="marginright col-md-3" runat="server" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ControlToValidate="txtLastName" runat="server" ErrorMessage="Please enter last name" ValidationGroup="ValidateAdd"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="rftupdateLName" Display="None" ControlToValidate="txtLastName" runat="server" ErrorMessage="Please enter last name" ValidationGroup="ValidateUpdate"></asp:RequiredFieldValidator>
                               </div>
                            </div>

                            <div class="repeterV">

                                <div class="col-md-2">
                                    Email ID: <span style="color: Red;">*</span>
                                </div>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtEmailId" runat="server" MaxLength="100" CssClass="col-md-6" placeholder="Email Id"></asp:TextBox>
                                     (Same will be the username)
                           <asp:RegularExpressionValidator
                               ID="revEmail" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Email address is not valid"
                               ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None"
                               CssClass="errortext" ForeColor="" SetFocusOnError="True" ValidationGroup="ValidateAdd"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Email address is not valid"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None"
                                        CssClass="errortext" ForeColor="" SetFocusOnError="True" ValidationGroup="ValidateUpdate"></asp:RegularExpressionValidator>

                                    <asp:RequiredFieldValidator ID="rftUpdateEmail" Display="None" ControlToValidate="txtEmailId" runat="server" ErrorMessage="Please enter Email Id" ValidationGroup="ValidateAdd"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="rfvEmail" Display="None" ControlToValidate="txtEmailId" runat="server" ErrorMessage="Please enter Email Id" ValidationGroup="ValidateUpdate"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="repeterV" id="password" runat="server">
                                <div class="col-md-2">
                                    Password: <span style="color: Red;">*</span>
                                </div>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtpassword" placeholder="Password" runat="server" MaxLength="20" CssClass="col-md-3" TextMode="Password"></asp:TextBox>

                                    &nbsp&nbsp;
                        <asp:TextBox ID="txtConfirmpwd" placeholder="Confirm Password" CssClass="marginright col-md-3" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="None" runat="server" ControlToValidate="txtpassword" ErrorMessage="Please enter password" ValidationGroup="ValidateAdd"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="None" runat="server" ControlToValidate="txtConfirmpwd" ErrorMessage="Please enter confirm password" ValidationGroup="ValidateAdd"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cmpvalidatepwd" runat="server" ControlToValidate="txtpassword" ControlToCompare="txtConfirmpwd" ErrorMessage="Password and Confirm Password Should be same." ValidationGroup="ValidateAdd"></asp:CompareValidator>
                                </div>
                            </div>
                            <div class="repeterV">
                                <div class="col-md-2">
                                    Role: <span style="color: Red;">*</span>
                                </div>
                                <div class="col-md-10">

                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlRole" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server"
                                        ControlToValidate="ddlRole" ValueToCompare="0" Operator="NotEqual" Display="None" Type="String" ErrorMessage="Please select role" ValidationGroup="ValidateAdd"></asp:CompareValidator>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server"
                                        ControlToValidate="ddlRole" ValueToCompare="0" Operator="NotEqual" Display="None" Type="String" ErrorMessage="Please select role" ValidationGroup="ValidateUpdate"></asp:CompareValidator>

                                </div>
                            </div>
                            <div class="repeterV">
                                <div class="col-md-2">Status:</div>
                                <div class="col-md-10">
                                    <span class="top10">
                                        <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" /></span>
                                </div>

                            </div>



                            <div class="repeterV">
                                <div class="col-md-2">Contact Number:</div>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtContactNumber" runat="server" MaxLength="20"></asp:TextBox>
                                    <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContactNumber" ErrorMessage="Please enter valid contact number"
                                        ValidationExpression="[0-9- ]*" Display="None"
                                        CssClass="errortext" ForeColor="" SetFocusOnError="True" ValidationGroup="accountInfoValidate"></asp:RegularExpressionValidator>
                                </div>

                            </div>




                        </div>
                        <div class="clearfix"></div>
                        <div class="bottom_part my">
                            <ul class="button_section">
                                <li>

                                    <asp:Button ID="btnAdd" CssClass="Newbtn save" runat="server" Text="Save & Exit " ValidationGroup="ValidateAdd" OnClick="btnAdd_Click" />

                                    <asp:Button ID="btnSubmitandAssign" CssClass="Newbtn save" runat="server" Text="Save & Assign Store " ValidationGroup="ValidateAdd" OnClick="btnSubmitandAssign_Click" />
                                </li>

                                <li>
                                    <asp:Button ID="btnUpdate" CssClass="Newbtn save" runat="server" Text="Update" ValidationGroup="ValidateUpdate" OnClick="btnUpdate_Click" />
                                    <asp:ValidationSummary ID="valAdmin" runat="server" DisplayMode="List" ShowSummary="false" ShowMessageBox="true"
                                        ValidationGroup="ValidateAdd" CssClass="errortext" />
                                    <asp:Button ID="btnUpdateandAssign" CssClass="Newbtn save" runat="server" Text="Update & Assign Store" ValidationGroup="ValidateUpdate" OnClick="btnSubmitandAssign_Click" />
                                </li>
                            

                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="List" ShowSummary="false" ShowMessageBox="true"
                                    ValidationGroup="ValidateUpdate" CssClass="errortext" />
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>

</asp:Content>

