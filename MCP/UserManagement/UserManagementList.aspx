﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="UserManagementList.aspx.cs" Inherits="UserManagement_UserManagementList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ol class="breadcrumb">
           <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Setting</li>
            <li>User Management</li>
        </ol>
    </div>
    <div class="admin_page">
        <section class="container mainContainer role_management role_listing manage_users">
            <div class="wrap_container ">
                <div class="content">
                    <section class="mainContainer">
                        <h3 class="mainHead">Manage Users
                        </h3>
                        <div class="add_newrole">
                            <asp:Button ID="btnAddNew" runat="server" CssClass="save" Text="Add New Users" OnClick="btnAddNew_Click" />
                        </div>
                        <div class="clear">
                            <div id="accordionx" class="search2 accordion">
                                <h3>Search Users</h3>
                                <div class="searchInnerbox">
                                    <div class="box1 box1Extra" style="background: none !important;">
                                        <asp:Panel runat="server" DefaultButton="btnSearch">
                                            <asp:TextBox ID="txtSearch" runat="server" placeholder="Search" CssClass="input1"></asp:TextBox>
                                            <asp:HiddenField ID="hdnrolename" runat="server" />
                                            <asp:Button ID="btnSearch" runat="server" Text="Search"
                                                OnClick="btnSearch_Click" CssClass="save" />
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="save" OnClick="btnReset_Click" />
                                        </asp:Panel>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="customer" style="margin: 0px !important;">
                            <span class="allcosutomer">All Users -
                    <asp:Literal ID="ltrNoOfRecords" runat="server" Text=""></asp:Literal></span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <section class=" all_customer dragblnextBlock manage_pro">
                                <asp:GridView runat="server" ID="gvUsers" TabIndex="0" CellPadding="3" Width="100%" PageSize="50"
                                    DataKeyNames="UserId,UserName" AutoGenerateColumns="False" OnPageIndexChanging="gvUsers_PageIndexChanging"
                                    CssClass="all_customer_inner allcutomerEtracls" EmptyDataText="No user exists."
                                    AllowPaging="true">
                                    <HeaderStyle Font-Bold="true" />
                                    <RowStyle HorizontalAlign="Center" Height="30px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="left" Visible="false">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "UserId")%>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User Name" SortExpression="UserName">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "UserName")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "RoleName")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email ID">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "EmailId")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="25%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Created Date">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem,"CreatedDate","{0:dd/MM/yyyy}") %>  
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Eval("IsActive").ToString().ToLower() == "true" ? true : false %>' OnCheckedChanged="chkIsActive_CheckedChanged"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="15%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="other_option">

                                                            <asp:LinkButton ID="lnkEditGroups" runat="server" CommandArgument='<%# Eval("UserId") %>' OnClick="lnkEditGroups_Click">Edit</asp:LinkButton>
                                                  <%--          <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClientClick="javascript:return confirm('Are you sure you want to delete this role?')"
                                                                CommandArgument='<%# Eval("RoleId") %>' CommandName="DeleteRole" Text="">Delete</asp:LinkButton>--%>
                                                        </div>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </section>
                        </div>

                        <div class="add_newrole">

                            <asp:Button ID="Button1" runat="server" CssClass="save" Text="Add New Users" OnClick="btnAddNew_Click" />
                        </div>
                        <div class="clearfix"></div>
                    </section>

                </div>
            </div>
        </section>
    </div>
</asp:Content>

