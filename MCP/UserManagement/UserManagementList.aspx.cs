﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;

public partial class UserManagement_UserManagementList : System.Web.UI.Page
{
    UserBE objUserBE = new UserBE();
    List<UserBE> lstUBE = new List<UserBE>();
    BasePage objbase = new BasePage();

    protected void Page_Load(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized(HttpContext.Current.Request.Url.Segments[HttpContext.Current.Request.Url.Segments.Length - 1]);
        if (!IsPostBack)
        {
            BindUser();
        }
        btnSearch.Attributes.Add("onmousedown", "ValidateTextbox('" + txtSearch.ClientID + "');");
    }
    protected void BindUser()
    {
        try
        {
            objUserBE.Action = Convert.ToInt16(DBAction.Select);
            objUserBE.UserId = 0;
            lstUBE = UserBL.GetMCPUsers<UserBE>(objUserBE);
            if (lstUBE != null)
            {
                if (lstUBE.Count > 0)
                {
                    gvUsers.DataSource = lstUBE;
                    ltrNoOfRecords.Text = lstUBE.Count.ToString();
                    gvUsers.DataBind();
                }
                else
                {
                    gvUsers.DataSource = null;
                    gvUsers.DataBind();
                    ltrNoOfRecords.Text = "0";
                }
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void SearchUser()
    {
        try
        {
            objUserBE.Action = Convert.ToInt16(DBAction.Select);
            objUserBE.FirstName = txtSearch.Text.ToString();
            lstUBE = UserBL.GetMCPUsers<UserBE>(objUserBE);
            if (lstUBE != null)
            {
                if (lstUBE.Count > 0)
                {
                    gvUsers.DataSource = lstUBE;
                    ltrNoOfRecords.Text = lstUBE.Count.ToString();
                    gvUsers.DataBind();
                }
            }
            else
            {
                gvUsers.DataSource = null;
                gvUsers.DataBind();
                ltrNoOfRecords.Text = "0";
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session["AEUserId"] = 0;
        Response.Redirect("AddEditUser.aspx?mode=a");
    }
    protected void lnkEditGroups_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            int UserId = Convert.ToInt32(lnk.CommandArgument);
            Session["AEUserId"] = UserId;
            Response.Redirect("AddEditUser.aspx?mode=e");
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void chkIsActive_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            UserBE objUserBEnew = Session["User"] as UserBE;


            int res;

            CheckBox chkIsActive = (sender as CheckBox);

            GridViewRow row = (sender as CheckBox).NamingContainer as GridViewRow;
            int id1 = Convert.ToInt16(gvUsers.DataKeys[row.RowIndex].Values["UserId"]);
            string UserName = Convert.ToString(gvUsers.DataKeys[row.RowIndex].Values["UserName"]);

            objUserBE.UserId = Convert.ToInt16(id1);
            objUserBE.Action = Convert.ToInt16(DBAction.Update);
            objUserBE.ModifiedBy = Convert.ToInt16(objUserBEnew.UserId);
            if ((row.FindControl("chkIsActive") as CheckBox).Checked)
            {
                objUserBE.IsActive = true;
                res = UserBL.InsertUpdateMCPUser(objUserBE);
            }
            else
            {
                objUserBE.IsActive = false;
                res = UserBL.InsertUpdateMCPUser(objUserBE);
            }
            if (res > 0)
            {
                //true
                objbase.CreateActivityLog("User Management", "Updated", id1 + "_" + UserName);
                BindUser();
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SearchUser();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtSearch.Text = "";
        BindUser();
    }
    protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsers.PageIndex = e.NewPageIndex;
        BindUser();
    }
    //protected void lnkAssignStores_Click(object sender, EventArgs e)
    //{
    //    LinkButton lnk = (LinkButton)sender;
    //    int UserId = Convert.ToInt32(lnk.CommandArgument);
    //    Session["AEUserId"] = UserId;
    //    Response.Redirect("AssignStores.aspx?mode=e");
    //}
}