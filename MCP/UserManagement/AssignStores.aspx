﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="AssignStores.aspx.cs" Inherits="UserManagement_AssignStores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        // Let's use a lowercase function name to keep with JavaScript conventions
        function selectAll(invoker) {
            // Since ASP.NET checkboxes are really HTML input elements
            //  let's get all the inputs
            var inputElements = document.getElementsByTagName('input');
            for (var i = 0 ; i < inputElements.length ; i++) {
                var myElement = inputElements[i];
                // Filter through the input types looking for checkboxes
                if (myElement.type === "checkbox") {
                    // Use the invoker (our calling element) as the reference 
                    //  for our checkbox status
                    myElement.checked = invoker.checked;
                }
            }
        }
    </script>
      <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Setting</li>
            <li>Assign Stores</li>
        </ol>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content" id="role_magmrnt">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Assign Stores
                        </h3>
                        <div class="assign_wrap_select">
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                           
                            <div class="col-md-2">
                                Select Users 
                            </div>
                            <div class="select-style selectpicker">
                                <asp:DropDownList ID="ddlUsers" runat="server" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                        </div>
                        
                        <div class="table-responsive">
                            <section class=" all_customer dragblnextBlock manage_pro">
                                <asp:GridView runat="server" ID="gvStore" TabIndex="0" CellPadding="3" Width="100%" PageSize="50"
                                    DataKeyNames="StoreId" AutoGenerateColumns="False"
                                    CssClass="all_customer_inner allcutomerEtracls new_assign" EmptyDataText="No store exists."
                                    AllowPaging="true">
                                    <HeaderStyle Font-Bold="true" />
                                    <RowStyle HorizontalAlign="Center" Height="30px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="left" Visible="false">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "StoreId")%>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Store Name" SortExpression="UserName">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "StoreName")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="50%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsAssign" runat="server" Checked='<%# Eval("IsActive").ToString().ToLower() == "true" ? true : false %>'></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                              (Select All)  <asp:CheckBox ID="chkAll" runat="Server" OnClick="selectAll(this)"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </section>
                        </div>
                    </section>
                </div>
                <div class="button_section">
                    <asp:Button ID="btnAssignStore" runat="server" CssClass="btn" Text="Save" ValidationGroup="add" OnClick="btnAssignStore_Click" />
                    <%--        <asp:Button ID="btnAssignStore" runat="server" CssClass="btn" Text="Save" ValidationGroup="add" OnClick="GetSelectedRecords"/>
                    --%>
                </div>
            </div>
        </section>
    </div>

</asp:Content>

