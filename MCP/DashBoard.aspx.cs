﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DashBoard : System.Web.UI.Page
{
    #region Variables

    UserBE objUserBE;
    List<MenuBE> lstMenuBE;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["User"] == null)
            Response.Redirect("~/Login/Login.aspx", true);
        else
        {
            BindMenu();
        }

    }


    private void BindMenu()
    {
        objUserBE = new UserBE();
        objUserBE = Session["User"] as UserBE;
        lstMenuBE = MenuBL.GetAllStoreMenuDetails(objUserBE.RoleId, true);
        if (lstMenuBE != null)
        {
            if (lstMenuBE.Count > 0)
            {
                Panel objPanel = null;
                Literal objLiteral = null;
                lstMenuBE.Remove(lstMenuBE.Find(x => x.MenuName.ToLower() == "dashboard"));
                lstMenuBE.Remove(lstMenuBE.Find(x => x.MenuName.ToLower() == "logout"));
                List<MenuBE> lstParentMenu = new List<MenuBE>();
                lstParentMenu = lstMenuBE.FindAll(x => x.ParentMenuId == 0);

                if (lstParentMenu != null)
                {
                    if (lstParentMenu.Count > 0)
                    {
                        foreach (var menuitem in lstParentMenu)
                        {
                            objLiteral = new Literal();
                            if (menuitem.MenuUrl.ToLower().Contains("javascript") || menuitem.MenuUrl.ToLower().Contains("#") || string.IsNullOrEmpty(menuitem.MenuUrl))
                                objLiteral.Text = "<span class='titledivcase'><a href='javascript:void(0)'>" + menuitem.MenuName + "</a></span>";
                            else
                                objLiteral.Text = "<span class='titledivcase'><a href=" + Convert.ToString(ConfigurationManager.AppSettings["ImgURL"]) + menuitem.MenuUrl + ">" + menuitem.MenuName + "</a></span>";


                            Literal objChildLiteral = new Literal();
                            List<MenuBE> lstChildMenu = lstMenuBE.FindAll(x => x.ParentMenuId == menuitem.MenuId);
                            StringBuilder sb = new StringBuilder();
                            if (lstChildMenu != null)
                            {
                                if (lstChildMenu.Count > 0)
                                {
                                    sb.Append("<ul>");
                                    foreach (var childmenuitem in lstChildMenu)
                                    {
                                        sb.Append("<li>");
                                        sb.Append("<a href=" + Convert.ToString(ConfigurationManager.AppSettings["ImgURL"]) + childmenuitem.MenuUrl + ">" + childmenuitem.MenuName + "</a>");
                                        sb.Append("</li>");

                                    }
                                    sb.Append("</ul>");
                                }
                            }
                            
                            objPanel = new Panel();
                            objPanel.Controls.Add(objLiteral);
                            objChildLiteral.Text = sb.ToString();
                            objPanel.Controls.Add(objChildLiteral);
                            objPanel.CssClass = "block";
                            dvDashboard.Controls.Add(objPanel);
                        }
                    }
                }
            }
        }
    }
}