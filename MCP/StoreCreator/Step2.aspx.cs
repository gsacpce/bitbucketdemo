﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class StoreCreator_Step2 : System.Web.UI.Page
{
    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Added by sanchit for authorization

        GlobalFunctions.IsAuthorized("step1.aspx");

        if (Session["PreviousPage"] != null && Session["StoreId"] != null)
        {
            if (Request.QueryString["id"] != null)
            {
                Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);
                if (Convert.ToString(Session["PreviousPage"]) == "step1" && Convert.ToInt16(Session["StoreId"]) == StoreId) { }
                else
                    Response.Redirect("~/UnAuthorized.html");
            }
            else
                Response.Redirect("~/UnAuthorized.html");
        }
        else
            Response.Redirect("~/UnAuthorized.html");

        #endregion

        if (!IsPostBack)
        {
            Exceptions.WriteInfoLog("StoreCreator_Step2 start");
            GetStoreFeatures();
        }
    }

    #endregion

    #region Get Store Feature Details

    private void GetStoreFeatures()
    {
        try
        {
            Exceptions.WriteInfoLog("GetStoreFeatures start");
            Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);
            List<FeatureBE> GetAllFeatures = FeatureBL.GetAllFeatures(StoreId);
            Exceptions.WriteInfoLog("After GetAllFeatures");

            #region Get Max Order Qty per Item - Added By Snehal 23 09 2016
            FeatureBE OD_AllowMaxOrderQtyPerItemFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.Trim() == "SS_MaxAllowQty");
            if (OD_AllowMaxOrderQtyPerItemFeature != null)
            {
                chkMaxAllowQty.Text = OD_AllowMaxOrderQtyPerItemFeature.FeatureAlias;
                chkMaxAllowQty.Checked = OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].IsEnabled;
                hdnMaxOrderQtyStoreFeatureDetailId.Value = Convert.ToString(OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].StoreFeatureDetailId);
                if (OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].IsEnabled)
                {
                    txtAllowMaxOrder.Enabled = true;
                    txtAllowMaxOrder.Text = OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].FeatureDefaultValue;
                }
                else
                    txtAllowMaxOrder.Enabled = false;
            }
            #endregion

            #region Get Display Type

            FeatureBE PL_DisplaytypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_displaytype");
            if (PL_DisplaytypeFeature != null)
            {
                lblPLDisplayTypeHeading.Text = PL_DisplaytypeFeature.FeatureAlias;
                rptPLDisplayType.DataSource = PL_DisplaytypeFeature.FeatureValues;
                rptPLDisplayType.DataBind();
            }

            #endregion

            #region Enable text field on Product enquiry page for Color
            FeatureBE PD_ColorProdEnquiry = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_colorprodenquiry");
            if (PD_ColorProdEnquiry != null)
            {
                ChkProductEnquiryColor.Text = PD_ColorProdEnquiry.FeatureAlias;
                ChkProductEnquiryColor.Checked = PD_ColorProdEnquiry.FeatureValues[0].IsEnabled;
                hdnProductEnquiryColor.Value = Convert.ToString(PD_ColorProdEnquiry.FeatureValues[0].StoreFeatureDetailId);
            }
            #endregion

            #region To Enable Only Submit Enquiry on store
            FeatureBE SubmitEnquiry = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_submit_enquiry");
            if (SubmitEnquiry != null)
            {
                ChkSubmitEnquiry.Text = SubmitEnquiry.FeatureAlias;
                ChkSubmitEnquiry.Checked = SubmitEnquiry.FeatureValues[0].IsEnabled;
                hdnSubmitEnquiry.Value = Convert.ToString(SubmitEnquiry.FeatureValues[0].StoreFeatureDetailId);
            }
            #endregion

            #region Get Compare Products

            FeatureBE PL_ProductComparisonFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_compareproducts");
            if (PL_ProductComparisonFeature != null)
            {
                chkProductComparison.Text = PL_ProductComparisonFeature.FeatureAlias;
                chkProductComparison.Checked = PL_ProductComparisonFeature.FeatureValues[0].IsEnabled;
                hdnPLProductComparisonStoreFeatureDetailId.Value = Convert.ToString(PL_ProductComparisonFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Pagination

            FeatureBE PL_PaginationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_pagination");
            if (PL_PaginationFeature != null)
            {
                lblPaginationHeading.Text = PL_PaginationFeature.FeatureAlias;
                rblProductPagination.DataSource = PL_PaginationFeature.FeatureValues;
                rblProductPagination.DataTextField = "FeatureValue";
                rblProductPagination.DataValueField = "StoreFeatureDetailId";
                rblProductPagination.DataBind();
                if (PL_PaginationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblProductPagination.SelectedValue = Convert.ToString(PL_PaginationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblProductPagination.SelectedValue = Convert.ToString(PL_PaginationFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "lazy load").StoreFeatureDetailId);
                FeatureBE.FeatureValueBE FeatureValueBEInstance = PL_PaginationFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "pagination");
                if (FeatureValueBEInstance.IsEnabled)
                {
                    txtProductsPerPage.Enabled = true;
                    txtProductsPerPage.Text = FeatureValueBEInstance.FeatureDefaultValue;
                }
                else
                    txtProductsPerPage.Enabled = false;

            }

            #endregion

            #region Get Sorting

            FeatureBE PL_SortingFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_sorting");
            if (PL_SortingFeature != null)
            {
                lblPLSortingHeading.Text = PL_SortingFeature.FeatureAlias;
                rptPLSorting.DataSource = PL_SortingFeature.FeatureValues;
                rptPLSorting.DataBind();
            }

            #endregion

            #region Get Filter

            FeatureBE PL_FilterFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_filter");
            if (PL_FilterFeature != null)
            {
                lblPLFilterHeading.Text = PL_FilterFeature.FeatureAlias;

                //if (PL_FilterFeature.FeatureValues.Count == PL_FilterFeature.FeatureValues.FindAll(x => x.IsEnabled == true).Count)
                //    chkEnableFilter.Checked = true;
                //else
                //    chkEnableFilter.Checked = false;

                rptPLFilter.DataSource = PL_FilterFeature.FeatureValues;
                rptPLFilter.DataBind();
            }

            #endregion

            #region Get QuickView

            FeatureBE PL_QuickviewFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_quickview");
            if (PL_QuickviewFeature != null)
            {
                chkQuickView.Text = PL_QuickviewFeature.FeatureAlias;
                chkQuickView.Checked = PL_QuickviewFeature.FeatureValues[0].IsEnabled;
                hdnPLQuickViewStoreFeatureDetailId.Value = Convert.ToString(PL_QuickviewFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail Print

            FeatureBE PD_EnablePrintFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableprint");
            if (PD_EnablePrintFeature != null)
            {
                chkPDPrint.Text = PD_EnablePrintFeature.FeatureAlias;
                chkPDPrint.Checked = PD_EnablePrintFeature.FeatureValues[0].IsEnabled;
                hdnPDPrintStoreFeatureDetailId.Value = Convert.ToString(PD_EnablePrintFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail Email

            FeatureBE PD_EnableEmailFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableemail");
            if (PD_EnableEmailFeature != null)
            {
                chkPDEmail.Text = PD_EnableEmailFeature.FeatureAlias;
                chkPDEmail.Checked = PD_EnableEmailFeature.FeatureValues[0].IsEnabled;
                hdnPDEmailStoreFeatureDetailId.Value = Convert.ToString(PD_EnableEmailFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail download image

            FeatureBE PD_EnableDownloadImageFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enabledownloadimage");
            if (PD_EnableDownloadImageFeature != null)
            {
                chkPDDownloadImage.Text = PD_EnableDownloadImageFeature.FeatureAlias;
                chkPDDownloadImage.Checked = PD_EnableDownloadImageFeature.FeatureValues[0].IsEnabled;
                hdnPDDownloadImageStoreFeatureDetailId.Value = Convert.ToString(PD_EnableDownloadImageFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail social media

            FeatureBE PD_EnableSocialMediaFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesocialmedia");
            if (PD_EnableSocialMediaFeature != null)
            {
                chkPDSocialMedia.Text = PD_EnableSocialMediaFeature.FeatureAlias;
                chkPDSocialMedia.Checked = PD_EnableSocialMediaFeature.FeatureValues[0].IsEnabled;
                hdnPDSocialMediaStoreFeatureDetailId.Value = Convert.ToString(PD_EnableSocialMediaFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail recently viewed products

            FeatureBE PD_EnableRecentlyViewedProductsFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablerecentlyviewedproducts");
            if (PD_EnableRecentlyViewedProductsFeature != null)
            {
                chkPDRecentlyViewedProducts.Text = PD_EnableRecentlyViewedProductsFeature.FeatureAlias;
                chkPDRecentlyViewedProducts.Checked = PD_EnableRecentlyViewedProductsFeature.FeatureValues[0].IsEnabled;
                hdnPDRecentlyViewedProductsStoreFeatureDetailId.Value = Convert.ToString(PD_EnableRecentlyViewedProductsFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail you may also like

            FeatureBE PD_EnableYouMayAlsoLikeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableyoumayalsolike");
            if (PD_EnableYouMayAlsoLikeFeature != null)
            {
                chkPDYouMayAlsoLike.Text = PD_EnableYouMayAlsoLikeFeature.FeatureAlias;
                chkPDYouMayAlsoLike.Checked = PD_EnableYouMayAlsoLikeFeature.FeatureValues[0].IsEnabled;
                hdnPDYouMayAlsoLikeStoreFeatureDetailId.Value = Convert.ToString(PD_EnableYouMayAlsoLikeFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail wishlist

            FeatureBE PD_EnableWishlistFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablewishlist");
            if (PD_EnableWishlistFeature != null)
            {
                chkPDWishlist.Text = PD_EnableWishlistFeature.FeatureAlias;
                chkPDWishlist.Checked = PD_EnableWishlistFeature.FeatureValues[0].IsEnabled;
                hdnPDWishlistStoreFeatureDetailId.Value = Convert.ToString(PD_EnableWishlistFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail review rating

            FeatureBE PD_EnableReviewRatingFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablereviewrating");
            if (PD_EnableReviewRatingFeature != null)
            {
                chkPDReviewRating.Text = PD_EnableReviewRatingFeature.FeatureAlias;
                chkPDReviewRating.Checked = PD_EnableReviewRatingFeature.FeatureValues[0].IsEnabled;
                hdnPDReviewRatingStoreFeatureDetailId.Value = Convert.ToString(PD_EnableReviewRatingFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail save pdf

            FeatureBE PD_EnableSavePDFFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesavepdf");
            if (PD_EnableSavePDFFeature != null)
            {
                chkPDSavePDF.Text = PD_EnableSavePDFFeature.FeatureAlias;
                chkPDSavePDF.Checked = PD_EnableSavePDFFeature.FeatureValues[0].IsEnabled;
                hdnPDSavePDFStoreFeatureDetailId.Value = Convert.ToString(PD_EnableSavePDFFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Detail Section Icons

            FeatureBE PD_EnableSectionIconsFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesectionicons");
            if (PD_EnableSectionIconsFeature != null)
            {
                chkPDSectionIcons.Text = PD_EnableSectionIconsFeature.FeatureAlias;
                chkPDSectionIcons.Checked = PD_EnableSectionIconsFeature.FeatureValues[0].IsEnabled;
                hdnPDSectionIconsStoreFeatureDetailId.Value = Convert.ToString(PD_EnableSectionIconsFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Enable Points

            FeatureBE SS_EnablePoints = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_enablepoints");
            if (SS_EnablePoints != null)
            {
                chkEnablePoints.Text = SS_EnablePoints.FeatureAlias;
                chkEnablePoints.Checked = SS_EnablePoints.FeatureValues[0].IsEnabled;
                hdnEnablePoints.Value = Convert.ToString(SS_EnablePoints.FeatureValues[0].StoreFeatureDetailId);
                txtPoints.Text = Convert.ToString(SS_EnablePoints.FeatureValues[0].FeatureDefaultValue);
                List<StoreBE> GetAllStores = StoreBL.GetAllStoreDetails();
                if (GetAllStores != null)
                {
                    ltrSelectedCurrency.Text = GetAllStores.FirstOrDefault(x => x.StoreId == StoreId).StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencySymbol;
                }
                if (chkEnablePoints.Checked)
                {
                    dvPoints.Style["display"] = "";
                    liPaymentGatewayHeading.Style["display"] = "none";
                    cvSCPaymentGateway.Enabled = false;
                    cvPointData.Enabled = true;
                }
                else
                {
                    dvPoints.Style["display"] = "none";
                    liPaymentGatewayHeading.Style["display"] = "";
                    cvSCPaymentGateway.Enabled = true;
                    cvPointData.Enabled = false;
                }
            }
            #endregion
            #region Get Payment Gateway

            FeatureBE SC_PaymentGatewayFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_paymentgateway");
            if (SC_PaymentGatewayFeature != null)
            {
                lblSCPaymentGatewayHeading.Text = SC_PaymentGatewayFeature.FeatureAlias;
                rptSCPaymentGateway.DataSource = SC_PaymentGatewayFeature.FeatureValues;
                rptSCPaymentGateway.DataBind();
            }

            #endregion

            #region Get Alternative Payments

            //FeatureBE SC_AlternativePaymentFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_alternativepayments");
            //if (SC_AlternativePaymentFeature != null)
            //{
            //    lblSCAlternativePaymentsHeading.Text = SC_AlternativePaymentFeature.FeatureAlias;
            //    rptSCAlternativePayments.DataSource = SC_AlternativePaymentFeature.FeatureValues;
            //    rptSCAlternativePayments.DataBind();
            //}

            #endregion

            #region  Gift Certificate
            FeatureBE SC_GiftCertificate = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_giftcertificate");
            if (SC_GiftCertificate != null)
            {
                ltGiftCertificate.Text = SC_GiftCertificate.FeatureAlias;
                rdlIsGiftCertificateAllow.DataSource = SC_GiftCertificate.FeatureValues;
                rdlIsGiftCertificateAllow.DataTextField = "FeatureValue";
                rdlIsGiftCertificateAllow.DataValueField = "StoreFeatureDetailId";
                rdlIsGiftCertificateAllow.DataBind();
                if (SC_GiftCertificate.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                { rdlIsGiftCertificateAllow.SelectedValue = Convert.ToString(SC_GiftCertificate.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId); }
                else
                { rdlIsGiftCertificateAllow.SelectedValue = Convert.ToString(SC_GiftCertificate.FeatureValues[1].StoreFeatureDetailId); }
            }
            #endregion

            #region Get International Orders

            FeatureBE SC_InternationalOrdersFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_internationalorders");
            if (SC_InternationalOrdersFeature != null)
            {
                lblSCInternationalOrders.Text = SC_InternationalOrdersFeature.FeatureAlias;
                chkSCInternationalOrders.Checked = SC_InternationalOrdersFeature.FeatureValues[0].IsEnabled;
                hdnSCInternationalOrdersStoreFeatureDetailId.Value = Convert.ToString(SC_InternationalOrdersFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region International Orders Message

            FeatureBE SC_MessageFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_message");
            if (SC_MessageFeature != null)
            {
                lblSCMessage.Text = SC_MessageFeature.FeatureAlias;
                txtSCMessage.Text = SC_MessageFeature.FeatureValues[0].FeatureDefaultValue;
                if (chkSCInternationalOrders.Checked)
                    txtSCMessage.Enabled = true;
                else
                    txtSCMessage.Enabled = false;
                hdnSCMessageStoreFeatureDetailId.Value = Convert.ToString(SC_MessageFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Minimum Order Value

            FeatureBE SC_MOVFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_mov");
            if (SC_MOVFeature != null)
            {
                lblSCMOV.Text = SC_MOVFeature.FeatureAlias;
                txtSCMOV.Text = SC_MOVFeature.FeatureValues[0].FeatureDefaultValue;
                hdnSCMOVStoreFeatureDetailId.Value = Convert.ToString(SC_MOVFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Allow Back Order

            FeatureBE OD_AllowBackOrderFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "od_allowbackorder");
            if (OD_AllowBackOrderFeature != null)
            {
                chkAllowBackOrder.Text = OD_AllowBackOrderFeature.FeatureAlias;
                chkAllowBackOrder.Checked = OD_AllowBackOrderFeature.FeatureValues[0].IsEnabled;
                hdnODAllowBackOrderStoreFeatureDetailId.Value = Convert.ToString(OD_AllowBackOrderFeature.FeatureValues[0].StoreFeatureDetailId);

                if (OD_AllowBackOrderFeature.FeatureValues[0].IsEnabled)
                {
                    txtAllowBackOrder.Enabled = true;
                    txtAllowBackOrder.Text = OD_AllowBackOrderFeature.FeatureValues[0].FeatureDefaultValue;
                }
                else
                    txtAllowBackOrder.Enabled = false;

            }

            #endregion

            #region "Custom Punchout"
            FeatureBE CustomPunchout = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "C_punchout");
            if (CustomPunchout != null)
            {
                //ltrHomeLink.Text = SS_HomeLink.FeatureAlias;
                rdoCustomPunchout.DataSource = CustomPunchout.FeatureValues;
                rdoCustomPunchout.DataTextField = "FeatureValue";
                rdoCustomPunchout.DataValueField = "StoreFeatureDetailId";
                rdoCustomPunchout.DataBind();
                if (CustomPunchout.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                {
                    rdoCustomPunchout.SelectedValue = Convert.ToString(CustomPunchout.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                }
                else if (CustomPunchout.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                {
                    rdoCustomPunchout.SelectedValue = Convert.ToString(CustomPunchout.FeatureValues[1].StoreFeatureDetailId);
                }
                else
                {
                    rdoCustomPunchout.SelectedValue = Convert.ToString(CustomPunchout.FeatureValues[0].StoreFeatureDetailId);
                }

            }
            #endregion

            #region OderHistory 
            FeatureBE OrderHistoryMonthstoReturn = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "orderhistorymonthstoreturn");
            if (OrderHistoryMonthstoReturn != null)
            {
                lblOrderHistoryMonthstoReturn.Text = OrderHistoryMonthstoReturn.FeatureAlias;
                txtOrderHistoryMonthstoReturn.Text = OrderHistoryMonthstoReturn.FeatureValues[0].FeatureDefaultValue;
                hdnOrderHistoryMonthstoReturn.Value = Convert.ToString(OrderHistoryMonthstoReturn.FeatureValues[0].StoreFeatureDetailId);
            }
            #endregion

            #region Get Category Display Option
            FeatureBE CR_CategoryDisplayFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_categorydisplay");
            if (CR_CategoryDisplayFeature != null)
            {
                lblCategoryDisplay.Text = CR_CategoryDisplayFeature.FeatureAlias;
                rblCategoryDisplay.DataSource = CR_CategoryDisplayFeature.FeatureValues;
                rblCategoryDisplay.DataTextField = "FeatureValue";
                rblCategoryDisplay.DataValueField = "StoreFeatureDetailId";
                rblCategoryDisplay.DataBind();
                if (CR_CategoryDisplayFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblCategoryDisplay.SelectedValue = Convert.ToString(CR_CategoryDisplayFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblCategoryDisplay.SelectedValue = Convert.ToString(CR_CategoryDisplayFeature.FeatureValues[1].StoreFeatureDetailId);
            }
            #endregion

            #region Get Basket Popup Option
            FeatureBE CR_BasketPopup = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_basketpopup");
            if (CR_BasketPopup != null)
            {
                lblBasketPopup.Text = CR_BasketPopup.FeatureAlias;
                rblBasketPopup.DataSource = CR_BasketPopup.FeatureValues;
                rblBasketPopup.DataTextField = "FeatureValue";
                rblBasketPopup.DataValueField = "StoreFeatureDetailId";
                rblBasketPopup.DataBind();
                if (CR_BasketPopup.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblBasketPopup.SelectedValue = Convert.ToString(CR_BasketPopup.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblBasketPopup.SelectedValue = Convert.ToString(CR_BasketPopup.FeatureValues[1].StoreFeatureDetailId);
            }
            #endregion

            #region Get Enable Punchout

            //FeatureBE SC_EnablePunchoutFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_enablepunchout");
            //if (SC_EnablePunchoutFeature != null)
            //{
            //    chkSCEnablePunchout.Text = SC_EnablePunchoutFeature.FeatureAlias;
            //    chkSCEnablePunchout.Checked = SC_EnablePunchoutFeature.FeatureValues[0].IsEnabled;
            //    hdnSCEnablePunchoutStoreFeatureDetailId.Value = Convert.ToString(SC_EnablePunchoutFeature.FeatureValues[0].StoreFeatureDetailId);
            //}

            StoreBE store = StoreBL.GetStoreDetails(StoreId);
            chkSCEnablePunchout.Checked = store.IsPunchOut;
            chkSCEnablePunchout.Text = "Enable Punchout";

            #endregion

            #region Get Approve All Orders

            FeatureBE SC_ApproveAllOrdersFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_approveallorders");
            if (SC_ApproveAllOrdersFeature != null)
            {
                lblSCApproveAllOrders.Text = SC_ApproveAllOrdersFeature.FeatureAlias;
                rblSCApproveAllOrders.DataSource = SC_ApproveAllOrdersFeature.FeatureValues;
                rblSCApproveAllOrders.DataTextField = "FeatureValue";
                rblSCApproveAllOrders.DataValueField = "StoreFeatureDetailId";
                if (SC_ApproveAllOrdersFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblSCApproveAllOrders.SelectedValue = Convert.ToString(SC_ApproveAllOrdersFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblSCApproveAllOrders.SelectedValue = Convert.ToString(SC_ApproveAllOrdersFeature.FeatureValues[1].StoreFeatureDetailId);
                rblSCApproveAllOrders.DataBind();
            }

            #endregion

            #region Get Customer Type

            FeatureBE CR_CustomerTypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_customertype");
            if (CR_CustomerTypeFeature != null)
            {
                lblCRCustomerTypeHeading.Text = CR_CustomerTypeFeature.FeatureAlias;
                rblCRCustomerType.DataSource = CR_CustomerTypeFeature.FeatureValues;
                rblCRCustomerType.DataTextField = "FeatureValue";
                rblCRCustomerType.DataValueField = "StoreFeatureDetailId";
                rblCRCustomerType.DataBind();
                if (CR_CustomerTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblCRCustomerType.SelectedValue = Convert.ToString(CR_CustomerTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblCRCustomerType.SelectedValue = Convert.ToString(CR_CustomerTypeFeature.FeatureValues[1].StoreFeatureDetailId);

                FeatureBE.FeatureValueBE FeatureValueBEInstance = CR_CustomerTypeFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "hierarchy");
                if (FeatureValueBEInstance.IsEnabled)
                {
                    ddlCRCustomerHierarchyLevel.Enabled = true;
                    FeatureBE CR_CustomerHierarchyLevelFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_customerhierarchylevel");
                    ddlCRCustomerHierarchyLevel.SelectedValue = FeatureValueBEInstance.FeatureDefaultValue;
                    dvApprovalRequired.Style["display"] = "";
                }
                else
                {
                    ddlCRCustomerHierarchyLevel.Enabled = false;
                    ddlCRCustomerHierarchyLevel.SelectedIndex = 0;
                    dvApprovalRequired.Style["display"] = "none";
                }




            }

            #endregion

            #region Get Email Approval Required

            FeatureBE CR_ApprovalRequiredFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_approvalrequired");
            if (CR_ApprovalRequiredFeature != null)
            {
                lblCRApprovalRequired.Text = CR_ApprovalRequiredFeature.FeatureAlias;
                rblCRApprovalRequired.DataSource = CR_ApprovalRequiredFeature.FeatureValues;
                rblCRApprovalRequired.DataTextField = "FeatureValue";
                rblCRApprovalRequired.DataValueField = "StoreFeatureDetailId";
                rblCRApprovalRequired.DataBind();
                if (CR_ApprovalRequiredFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblCRApprovalRequired.SelectedValue = Convert.ToString(CR_ApprovalRequiredFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblCRApprovalRequired.SelectedValue = Convert.ToString(CR_ApprovalRequiredFeature.FeatureValues[1].StoreFeatureDetailId);

                FeatureBE.FeatureValueBE FeatureValueBEInstance = CR_ApprovalRequiredFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "no");
                if (FeatureValueBEInstance.IsEnabled)
                    txtCRNotificationEmail.Enabled = false;
                else
                    txtCRNotificationEmail.Enabled = true;

                if (CR_ApprovalRequiredFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) == null)
                    txtCRNotificationEmail.Enabled = false;
            }

            #endregion
            #region Get Notification Email

            FeatureBE CR_NotificationEmailFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_notificationemail");
            if (CR_NotificationEmailFeature != null)
            {
                lblCRNotificationEmail.Text = CR_NotificationEmailFeature.FeatureAlias;
                txtCRNotificationEmail.Attributes.Add("placeholder", CR_NotificationEmailFeature.FeatureValues[0].DefaultValue);
                txtCRNotificationEmail.Text = (!string.IsNullOrEmpty(CR_NotificationEmailFeature.FeatureValues[0].FeatureDefaultValue) 
                                              && CR_NotificationEmailFeature.FeatureValues[0].FeatureDefaultValue != CR_NotificationEmailFeature.FeatureValues[0].DefaultValue) 
                                              ? CR_NotificationEmailFeature.FeatureValues[0].FeatureDefaultValue : string.Empty;
                hdnCRNotificationEmailStoreFeatureDetailId.Value = Convert.ToString(CR_NotificationEmailFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get WhiteList Validation

            FeatureBE CR_WhiteListValidationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_whitelistvalidation");
            if (CR_WhiteListValidationFeature != null)
            {
                lblCRWhiteListValidation.Text = CR_WhiteListValidationFeature.FeatureAlias;
                rblCRWhiteListValidation.DataSource = CR_WhiteListValidationFeature.FeatureValues;
                rblCRWhiteListValidation.DataTextField = "FeatureValue";
                rblCRWhiteListValidation.DataValueField = "StoreFeatureDetailId";
                rblCRWhiteListValidation.DataBind();
                if (CR_WhiteListValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblCRWhiteListValidation.SelectedValue = Convert.ToString(CR_WhiteListValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblCRWhiteListValidation.SelectedValue = Convert.ToString(CR_WhiteListValidationFeature.FeatureValues[1].StoreFeatureDetailId);
            }

            #endregion

            #region Get Domain Validation

            FeatureBE CR_DomainValidationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation");
            if (CR_DomainValidationFeature != null)
            {
                chkCREnableDomainValidation.Text = CR_DomainValidationFeature.FeatureAlias;
                chkCREnableDomainValidation.Text = CR_DomainValidationFeature.FeatureAlias;
                rblCREnableDomainValidation.DataSource = CR_DomainValidationFeature.FeatureValues;
                rblCREnableDomainValidation.DataTextField = "FeatureValue";
                rblCREnableDomainValidation.DataValueField = "StoreFeatureDetailId";
                rblCREnableDomainValidation.DataBind();
                if (CR_DomainValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                {
                    chkCREnableDomainValidation.Checked = true;
                    dvCREnableDomainValidation.Style["display"] = "";
                    rblCREnableDomainValidation.SelectedValue = Convert.ToString(CR_DomainValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                }
                else
                {
                    chkCREnableDomainValidation.Checked = false;
                    dvCREnableDomainValidation.Style["display"] = "none";
                    //rblCREnableDomainValidation.SelectedValue = Convert.ToString(CR_DomainValidationFeature.FeatureValues[1].StoreFeatureDetailId);
                }
            }

            #endregion

            #region Get Email Validation

            FeatureBE CR_EmailValidationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation");
            if (CR_EmailValidationFeature != null)
            {
                chkCREnableEmailValidation.Text = CR_EmailValidationFeature.FeatureAlias;
                chkCREnableEmailValidation.Text = CR_EmailValidationFeature.FeatureAlias;
                rblCREnableEmailValidation.DataSource = CR_EmailValidationFeature.FeatureValues;
                rblCREnableEmailValidation.DataTextField = "FeatureValue";
                rblCREnableEmailValidation.DataValueField = "StoreFeatureDetailId";
                rblCREnableEmailValidation.DataBind();
                if (CR_EmailValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                {
                    chkCREnableEmailValidation.Checked = true;
                    dvCREnableEmailValidation.Style["display"] = "";
                    rblCREnableEmailValidation.SelectedValue = Convert.ToString(CR_EmailValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                }
                else
                {
                    chkCREnableEmailValidation.Checked = false;
                    dvCREnableEmailValidation.Style["display"] = "none";
                    //rblCREnableDomainValidation.SelectedValue = Convert.ToString(CR_DomainValidationFeature.FeatureValues[1].StoreFeatureDetailId);
                }
            }

            #endregion

            #region Get Enable SSO

            FeatureBE CR_EnableSSOFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_enablesso");
            if (CR_EnableSSOFeature != null)
            {
                chkCREnableSSO.Text = CR_EnableSSOFeature.FeatureAlias;
                chkCREnableSSO.Checked = CR_EnableSSOFeature.FeatureValues[0].IsEnabled;
                hdnCREnableSSOStoreFeatureDetailId.Value = Convert.ToString(CR_EnableSSOFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Product Stock Due Date(Add Days) Added By Ravi Gohil
            FeatureBE PD_StockDueDate = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_stockduedate");
            if (PD_StockDueDate != null)
            {
                lblStockDueDate.Text = PD_StockDueDate.FeatureAlias;
                txtStockDueDate.Text = PD_StockDueDate.FeatureValues[0].FeatureDefaultValue;
                hdnStockDueDate.Value = Convert.ToString(PD_StockDueDate.FeatureValues[0].StoreFeatureDetailId);
            }
            #endregion

            #region Get Enable Email Me Basket Button
            /*Added By Hardik "04/Oct/2016" */
            FeatureBE SC_EmailMeCopyOfBasket = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_email_me_basket");
            if (SC_EmailMeCopyOfBasket != null)
            {
                ChkEmailCopyBasketBtn.Text = SC_EmailMeCopyOfBasket.FeatureAlias;
                ChkEmailCopyBasketBtn.Checked = SC_EmailMeCopyOfBasket.FeatureValues[0].IsEnabled;
                hdnEmailCopyBasketBtn.Value = Convert.ToString(SC_EmailMeCopyOfBasket.FeatureValues[0].StoreFeatureDetailId);
            }
            #endregion

            #region Get Reset Password Block Account Duration

            FeatureBE CR_ResetPasswordBlockAccountDurationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_resetpasswordblockaccountduration");
            if (CR_ResetPasswordBlockAccountDurationFeature != null)
            {
                lblCRResetPasswordBlockAccountDuration.Text = CR_ResetPasswordBlockAccountDurationFeature.FeatureAlias;
                txtCRResetPasswordBlockAccountDuration.Text = CR_ResetPasswordBlockAccountDurationFeature.FeatureValues[0].FeatureDefaultValue;
                hdnCRResetPasswordBlockAccountDurationStoreFeatureDetailId.Value = Convert.ToString(CR_ResetPasswordBlockAccountDurationFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Store Access

            FeatureBE CR_StoreAccessFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_storeaccess");
            if (CR_StoreAccessFeature != null)
            {
                lblCRStoreAccessHeading.Text = CR_StoreAccessFeature.FeatureAlias;
                rblCRStoreAccess.DataSource = CR_StoreAccessFeature.FeatureValues;
                rblCRStoreAccess.DataTextField = "FeatureValue";
                rblCRStoreAccess.DataValueField = "StoreFeatureDetailId";
                rblCRStoreAccess.DataBind();
                if (CR_StoreAccessFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblCRStoreAccess.SelectedValue = Convert.ToString(CR_StoreAccessFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblCRStoreAccess.SelectedValue = Convert.ToString(CR_StoreAccessFeature.FeatureValues[1].StoreFeatureDetailId);
            }

            #endregion

            #region UDFS in Order confirmation email

            FeatureBE OC_UDFEmail = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "oc_udfemail");
            if (OC_UDFEmail != null)
            {
                ChkCOnfirmationEmail.Text = OC_UDFEmail.FeatureAlias;
                ChkCOnfirmationEmail.Checked = OC_UDFEmail.FeatureValues[0].IsEnabled;
                hdnCOemail.Value = Convert.ToString(OC_UDFEmail.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Home Link

            FeatureBE SS_HomeLink = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "SS_HomeLink");
            if (SS_HomeLink != null)
            {
                ltrHomeLink.Text = SS_HomeLink.FeatureAlias;
                rdoLinkPosition.DataSource = SS_HomeLink.FeatureValues;
                rdoLinkPosition.DataTextField = "FeatureValue";
                rdoLinkPosition.DataValueField = "StoreFeatureDetailId";
                rdoLinkPosition.DataBind();
                if (SS_HomeLink.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rdoLinkPosition.SelectedValue = Convert.ToString(SS_HomeLink.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rdoLinkPosition.SelectedValue = Convert.ToString(SS_HomeLink.FeatureValues[1].StoreFeatureDetailId);
            }

            #endregion

            #region Get Store Type

            FeatureBE CR_StoreTypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_storetype");
            if (CR_StoreTypeFeature != null)
            {
                lblCRStoreTypeHeading.Text = CR_StoreTypeFeature.FeatureAlias;
                rblCRStoreType.DataSource = CR_StoreTypeFeature.FeatureValues;
                rblCRStoreType.DataTextField = "FeatureValue";
                rblCRStoreType.DataValueField = "StoreFeatureDetailId";
                rblCRStoreType.DataBind();
                if (CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rblCRStoreType.SelectedValue = Convert.ToString(CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rblCRStoreType.SelectedValue = Convert.ToString(CR_StoreTypeFeature.FeatureValues[1].StoreFeatureDetailId);

                FeatureBE.FeatureValueBE StoreTypeB2CFeatureValueBEInstance = CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "b2c");
                FeatureBE.FeatureValueBE StoreTypeB2BFeatureValueBEInstance = CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "b2b");

                if ((StoreTypeB2CFeatureValueBEInstance.IsEnabled == false && StoreTypeB2BFeatureValueBEInstance.IsEnabled == false))
                {
                    divVatPercentage.Style["display"] = "";
                    txtB2CVatPercentage.Text = StoreTypeB2CFeatureValueBEInstance.FeatureDefaultValue;
                }
                else
                {
                    if (StoreTypeB2CFeatureValueBEInstance.IsEnabled)
                    {
                        divVatPercentage.Style["display"] = "";
                        txtB2CVatPercentage.Text = StoreTypeB2CFeatureValueBEInstance.FeatureDefaultValue;
                    }
                    else
                    {
                        divVatPercentage.Style["display"] = "none";
                    }
                }
            }

            #endregion

            #region Get Password Length

            FeatureBE PS_MinimumPasswordLengthFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength");
            if (PS_MinimumPasswordLengthFeature != null)
            {
                lblPSMinimumPasswordLength.Text = PS_MinimumPasswordLengthFeature.FeatureAlias;
                txtPSMinimumPasswordLength.Text = PS_MinimumPasswordLengthFeature.FeatureValues[0].FeatureDefaultValue;
                hdnPSMinimumPasswordLengthStoreFeatureDetailId.Value = Convert.ToString(PS_MinimumPasswordLengthFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Password Expression Commented by SHRIGANESH SINGH for Password Policy 18 May 2016

            //FeatureBE PS_PasswordExpressionFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordexpression");
            //if (PS_PasswordExpressionFeature != null)
            //{
            //    lblPSPasswordExpression.Text = PS_PasswordExpressionFeature.FeatureAlias;
            //    //chkPSPasswordExpression.Checked = PS_PasswordExpressionFeature.FeatureValues[0].IsEnabled;
            //    //hdnPSPasswordExpressionStoreFeatureDetailId.Value = Convert.ToString(PS_PasswordExpressionFeature.FeatureValues[0].StoreFeatureDetailId);
            //    rdPwdType.DataSource = PS_PasswordExpressionFeature.FeatureValues;
            //    rdPwdType.DataTextField = "FeatureValue";
            //    rdPwdType.DataValueField = "StoreFeatureDetailId";
            //    rdPwdType.DataBind();

            //    if (PS_PasswordExpressionFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
            //        rdPwdType.SelectedValue = Convert.ToString(PS_PasswordExpressionFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
            //    else
            //        rdPwdType.SelectedIndex = 0;
            //}

            #endregion

            #region Get Password Policy Added by SHRIGANESH SINGH 09 May 2016

            FeatureBE PS_PasswordPolicyFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy");
            if (PS_PasswordPolicyFeature != null)
            {
                lblPasswordPolicy.Text = PS_PasswordPolicyFeature.FeatureAlias;
                rdPasswordType.DataSource = PS_PasswordPolicyFeature.FeatureValues;
                rdPasswordType.DataTextField = "FeatureValue";
                rdPasswordType.DataValueField = "StoreFeatureDetailId";
                rdPasswordType.DataBind();

                if (PS_PasswordPolicyFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                {
                    rdPasswordType.SelectedValue = Convert.ToString(PS_PasswordPolicyFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                }
                else
                {
                    rdPasswordType.SelectedIndex = 0;
                }
            }

            #endregion

            #region Get Force Password Change

            FeatureBE PS_ForcePasswordChangeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_forcepasswordchange");
            if (PS_ForcePasswordChangeFeature != null)
            {
                lblPSForcePasswordChange.Text = PS_ForcePasswordChangeFeature.FeatureAlias;
                chkPSForcePasswordChange.Checked = PS_ForcePasswordChangeFeature.FeatureValues[0].IsEnabled;
                hdnPSForcePasswordChangeStoreFeatureDetailId.Value = Convert.ToString(PS_ForcePasswordChangeFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Password Validity

            FeatureBE PS_PasswordValidityFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordvalidity");
            if (PS_PasswordValidityFeature != null)
            {
                lblPSPasswordValidity.Text = PS_PasswordValidityFeature.FeatureAlias;
                txtPSPasswordValidity.Text = PS_PasswordValidityFeature.FeatureValues[0].FeatureDefaultValue;
                hdnPSPasswordValidityStoreFeatureDetailId.Value = Convert.ToString(PS_PasswordValidityFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Enable Coupon Code

            FeatureBE SS_EnableCouponCode = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_enablecouponcode");
            if (SS_EnableCouponCode != null)
            {
                chkEnableCouponCode.Text = SS_EnableCouponCode.FeatureAlias;
                chkEnableCouponCode.Checked = SS_EnableCouponCode.FeatureValues[0].IsEnabled;
                hdnEnableCouponCode.Value = Convert.ToString(SS_EnableCouponCode.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Enable Captcha

            FeatureBE CR_EnableCaptchaFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_enablecaptcha");
            if (CR_EnableCaptchaFeature != null)
            {
                chkCREnableCaptcha.Text = CR_EnableCaptchaFeature.FeatureAlias;
                chkCREnableCaptcha.Checked = CR_EnableCaptchaFeature.FeatureValues[0].IsEnabled;
                hdnCREnableCaptchaStoreFeatureDetailId.Value = Convert.ToString(CR_EnableCaptchaFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Get Enable Cookie

            FeatureBE CL_EnableCookieFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_enablecookie");
            if (CL_EnableCookieFeature != null)
            {
                lblCLEnableCookie.Text = CL_EnableCookieFeature.FeatureAlias;
                chkCLEnableCookie.Checked = CL_EnableCookieFeature.FeatureValues[0].IsEnabled;
                if (chkCLEnableCookie.Checked)
                {
                    txtCLCookieLifetime.Enabled = true;
                    txtCLCookieMessage.Enabled = true;
                }
                else
                {
                    txtCLCookieLifetime.Enabled = false;
                    txtCLCookieMessage.Enabled = false;
                }
                hdnCLEnableCookieStoreFeatureDetailId.Value = Convert.ToString(CL_EnableCookieFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Ravi Gohil For MCP Features

            FeatureBE SS_SearchSetting = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "SS_SearchSetting");
            if (SS_SearchSetting != null)
            {
                ltrSearchSetting.Text = SS_SearchSetting.FeatureAlias;
                rdoSearchSetting.DataSource = SS_SearchSetting.FeatureValues;
                rdoSearchSetting.DataTextField = "FeatureValue";
                rdoSearchSetting.DataValueField = "StoreFeatureDetailId";
                rdoSearchSetting.DataBind();
                if (SS_SearchSetting.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    rdoSearchSetting.SelectedValue = Convert.ToString(SS_SearchSetting.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                else
                    rdoSearchSetting.SelectedValue = Convert.ToString(SS_SearchSetting.FeatureValues[1].StoreFeatureDetailId);
            }
            #endregion

            #region Get Cookie Lifetime

            FeatureBE CL_CookieLifetimeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_cookielifetime");
            if (CL_CookieLifetimeFeature != null)
            {
                lblCLCookieLifetime.Text = CL_CookieLifetimeFeature.FeatureAlias;
                days.InnerText = CL_CookieLifetimeFeature.FeatureValues[0].DefaultValue;
                txtCLCookieLifetime.Text = CL_CookieLifetimeFeature.FeatureValues[0].FeatureDefaultValue;
                hdnCLCookieLifetimeStoreFeatureDetailId.Value = Convert.ToString(CL_CookieLifetimeFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Cookie Message

            FeatureBE CL_CookieMessageFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_cookiemessage");
            if (CL_CookieMessageFeature != null)
            {
                lblCLCookieMessage.Text = CL_CookieMessageFeature.FeatureAlias;
                txtCLCookieMessage.Text = CL_CookieMessageFeature.FeatureValues[0].FeatureDefaultValue;
                hdnCookieMessageStoreFeatureDetailId.Value = Convert.ToString(CL_CookieMessageFeature.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Save Store Feature Details

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        SaveStoreFeatureDetails(sender);
    }

    private void SaveStoreFeatureDetails(object sender)
    {
        try
        {
            UserBE user = (UserBE)Session["User"];
            Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);
            List<FeatureBE> GetAllFeatures = FeatureBL.GetAllFeatures(StoreId);

            #region Update DisplayType

            foreach (RepeaterItem item in rptPLDisplayType.Items)
            {
                CheckBox chkPLDisplayType = (CheckBox)item.FindControl("chkPLDisplayType");
                HtmlInputRadioButton rbPLDisplayType = (HtmlInputRadioButton)item.FindControl("rbPLDisplayType");
                HiddenField hdnPLDisplayTypeStoreFeatureDetailId = (HiddenField)item.FindControl("hdnPLDisplayTypeStoreFeatureDetailId");

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLDisplayTypeStoreFeatureDetailId.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(chkPLDisplayType.Checked));
                DictionaryInstance.Add("IsDefault", Convert.ToString(rbPLDisplayType.Checked));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Pagination

            foreach (ListItem item in rblProductPagination.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                if (item.Text.ToLower() == "pagination")
                    DictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtProductsPerPage.Text.Trim()));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Allow Punchout - Added By Snehal 22 09 2016
            Dictionary<string, string> SC_StorePunchout = new Dictionary<string, string>();
            SC_StorePunchout.Add("StoreId", Convert.ToString(StoreId));
            SC_StorePunchout.Add("IsPunchout", Convert.ToString(chkPunchout.Checked));

            StoreBL.UpdateStorePunchout(SC_StorePunchout);
            #endregion

            #region Custom Punchout 

            if (chkPunchout.Checked == true)
            {
                foreach (ListItem item in rdoCustomPunchout.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance);
                }
            }
            #endregion

            #region Update Stock Due Date for Product Details page
            Dictionary<string, string> StockDueDateDictionaryInstance = new Dictionary<string, string>();
            StockDueDateDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            StockDueDateDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnStockDueDate.Value));
            StockDueDateDictionaryInstance.Add("IsEnabled", Convert.ToString(1));
            StockDueDateDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtStockDueDate.Text.Trim()));
            StockDueDateDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(StockDueDateDictionaryInstance);
            #endregion

            #region Update Order History Months To Return
            Dictionary<string, string> OrderHistoryMonthstoReturn = new Dictionary<string, string>();
            OrderHistoryMonthstoReturn.Add("StoreId", Convert.ToString(StoreId));
            OrderHistoryMonthstoReturn.Add("StoreFeatureDetailId", Convert.ToString(hdnOrderHistoryMonthstoReturn.Value));
            OrderHistoryMonthstoReturn.Add("IsEnabled", Convert.ToString(1));
            OrderHistoryMonthstoReturn.Add("FeatureDefaultValue", Convert.ToString(txtOrderHistoryMonthstoReturn.Text.Trim()));
            OrderHistoryMonthstoReturn.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(OrderHistoryMonthstoReturn);
            #endregion

            #region Update Sorting

            foreach (RepeaterItem item in rptPLSorting.Items)
            {
                CheckBox chkPLSorting = (CheckBox)item.FindControl("chkPLSorting");
                HtmlInputRadioButton rbPLSorting = (HtmlInputRadioButton)item.FindControl("rbPLSorting");
                HiddenField hdnPLSortingStoreFeatureDetailId = (HiddenField)item.FindControl("hdnPLSortingStoreFeatureDetailId");

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLSortingStoreFeatureDetailId.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(chkPLSorting.Checked));
                DictionaryInstance.Add("IsDefault", Convert.ToString(rbPLSorting.Checked));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Filter

            int filterCount = 0;
            foreach (RepeaterItem item in rptPLFilter.Items)
            {
                if (item.ItemIndex == 0)
                    continue;

                if (!((CheckBox)item.FindControl("chkPLFilter")).Checked)
                    filterCount += 1;
            }

            foreach (RepeaterItem item in rptPLFilter.Items)
            {
                
                CheckBox chkPLFilter = (CheckBox)item.FindControl("chkPLFilter");
                HtmlInputRadioButton rbPLFilter = (HtmlInputRadioButton)item.FindControl("rbPLFilter");
                HiddenField hdnPLFilterStoreFeatureDetailId = (HiddenField)item.FindControl("hdnPLFilterStoreFeatureDetailId");

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLFilterStoreFeatureDetailId.Value));
                if (item.ItemIndex == 0)
                {
                    if (filterCount == 4)
                        DictionaryInstance.Add("IsEnabled", Convert.ToString(false));
                    else
                        DictionaryInstance.Add("IsEnabled", Convert.ToString(true));
                }
                else
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(chkPLFilter.Checked));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Basket Popup Option
            foreach (ListItem item in rblBasketPopup.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }
            #endregion

            #region Update Enable Email Me a copy btn SC
            /*Added By Hardik "04/Oct/2016" */
            Dictionary<string, string> SC_btnEmailMeCopyOfBasketInstance = new Dictionary<string, string>();
            SC_btnEmailMeCopyOfBasketInstance.Add("StoreId", Convert.ToString(StoreId));
            SC_btnEmailMeCopyOfBasketInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnEmailCopyBasketBtn.Value));
            SC_btnEmailMeCopyOfBasketInstance.Add("IsEnabled", Convert.ToString(ChkEmailCopyBasketBtn.Checked));
            SC_btnEmailMeCopyOfBasketInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(SC_btnEmailMeCopyOfBasketInstance);
            #endregion

            #region Update QuickView

            Dictionary<string, string> QuickViewDictionaryInstance = new Dictionary<string, string>();
            QuickViewDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            QuickViewDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLQuickViewStoreFeatureDetailId.Value));
            QuickViewDictionaryInstance.Add("IsEnabled", Convert.ToString(chkQuickView.Checked));
            QuickViewDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(QuickViewDictionaryInstance);

            #endregion

            #region To Add text field for color on product enquiry page
            Dictionary<string, string> PD_ColorProdEnquiryInstance = new Dictionary<string, string>();
            PD_ColorProdEnquiryInstance.Add("StoreId", Convert.ToString(StoreId));
            PD_ColorProdEnquiryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnProductEnquiryColor.Value));
            PD_ColorProdEnquiryInstance.Add("IsEnabled", Convert.ToString(ChkProductEnquiryColor.Checked));
            PD_ColorProdEnquiryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(PD_ColorProdEnquiryInstance);
            #endregion

            #region Update Product Detail Print

            Dictionary<string, string> PrintDictionaryInstance = new Dictionary<string, string>();
            PrintDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            PrintDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDPrintStoreFeatureDetailId.Value));
            PrintDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDPrint.Checked));
            PrintDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(PrintDictionaryInstance);

            #endregion

            #region update Order confirmation email

            Dictionary<string, string> OCEmailInstance = new Dictionary<string, string>();
            OCEmailInstance.Add("StoreId", Convert.ToString(StoreId));
            OCEmailInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCOemail.Value));
            OCEmailInstance.Add("IsEnabled", Convert.ToString(ChkCOnfirmationEmail.Checked));
            OCEmailInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(OCEmailInstance);

            #endregion

            #region Update Product Detail Email

            Dictionary<string, string> EmailDictionaryInstance = new Dictionary<string, string>();
            EmailDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            EmailDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDEmailStoreFeatureDetailId.Value));
            EmailDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDEmail.Checked));
            EmailDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(EmailDictionaryInstance);

            #endregion

            #region To Enable Only Submit Enquiry on store
            Dictionary<string, string> SS_SubmitEnquiry = new Dictionary<string, string>();
            SS_SubmitEnquiry.Add("StoreId", Convert.ToString(StoreId));
            SS_SubmitEnquiry.Add("StoreFeatureDetailId", Convert.ToString(hdnSubmitEnquiry.Value));
            SS_SubmitEnquiry.Add("IsEnabled", Convert.ToString(ChkSubmitEnquiry.Checked));
            SS_SubmitEnquiry.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(SS_SubmitEnquiry);
            #endregion

            #region Update Category Display feature
            foreach (ListItem item in rblCategoryDisplay.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }
            #endregion

            #region Update Product Detail download image

            Dictionary<string, string> DownloadImageDictionaryInstance = new Dictionary<string, string>();
            DownloadImageDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            DownloadImageDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDDownloadImageStoreFeatureDetailId.Value));
            DownloadImageDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDDownloadImage.Checked));
            DownloadImageDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(DownloadImageDictionaryInstance);

            #endregion

            #region Update Product Detail social media

            Dictionary<string, string> SocialMediaDictionaryInstance = new Dictionary<string, string>();
            SocialMediaDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            SocialMediaDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDSocialMediaStoreFeatureDetailId.Value));
            SocialMediaDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDSocialMedia.Checked));
            SocialMediaDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(SocialMediaDictionaryInstance);

            #endregion

            #region Update Product Detail recently viewed products

            Dictionary<string, string> RecentlyViewedProductsDictionaryInstance = new Dictionary<string, string>();
            RecentlyViewedProductsDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            RecentlyViewedProductsDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDRecentlyViewedProductsStoreFeatureDetailId.Value));
            RecentlyViewedProductsDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDRecentlyViewedProducts.Checked));
            RecentlyViewedProductsDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(RecentlyViewedProductsDictionaryInstance);

            #endregion

            #region Update Product Detail you may also like

            //Dictionary<string, string> YouMayAlsoLikeDictionaryInstance = new Dictionary<string, string>();
            //YouMayAlsoLikeDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            //YouMayAlsoLikeDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDYouMayAlsoLikeStoreFeatureDetailId.Value));
            //YouMayAlsoLikeDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDYouMayAlsoLike.Checked));
            //YouMayAlsoLikeDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            //StoreBL.UpdateStoreDetail(YouMayAlsoLikeDictionaryInstance);

            FeatureBE PD_EnableYouMayAlsoLikeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableyoumayalsolike");
            if (PD_EnableYouMayAlsoLikeFeature != null)
            {
                foreach (FeatureBE.FeatureValueBE item in PD_EnableYouMayAlsoLikeFeature.FeatureValues)
                {
                    Dictionary<string, string> YouMayAlsoLikeDictionaryInstance = new Dictionary<string, string>();
                    YouMayAlsoLikeDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    YouMayAlsoLikeDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.StoreFeatureDetailId));
                    YouMayAlsoLikeDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDYouMayAlsoLike.Checked));
                    YouMayAlsoLikeDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                    StoreBL.UpdateStoreDetail(YouMayAlsoLikeDictionaryInstance);
                }
            }

            #endregion

            #region Update Product Detail wishlist

            Dictionary<string, string> WishListDictionaryInstance = new Dictionary<string, string>();
            WishListDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            WishListDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDWishlistStoreFeatureDetailId.Value));
            WishListDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDWishlist.Checked));
            WishListDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(WishListDictionaryInstance);

            #endregion

            #region Update Product Detail review rating

            Dictionary<string, string> ReviewRatingDictionaryInstance = new Dictionary<string, string>();
            ReviewRatingDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            ReviewRatingDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDReviewRatingStoreFeatureDetailId.Value));
            ReviewRatingDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDReviewRating.Checked));
            ReviewRatingDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(ReviewRatingDictionaryInstance);

            #endregion

            #region Update Product Detail save PDF

            Dictionary<string, string> SavePDFDictionaryInstance = new Dictionary<string, string>();
            SavePDFDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            SavePDFDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDSavePDFStoreFeatureDetailId.Value));
            SavePDFDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDSavePDF.Checked));
            SavePDFDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(SavePDFDictionaryInstance);

            #endregion

            #region Update Product Detail section icons

            Dictionary<string, string> SectionIconsDictionaryInstance = new Dictionary<string, string>();
            SectionIconsDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            SectionIconsDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDSectionIconsStoreFeatureDetailId.Value));
            SectionIconsDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDSectionIcons.Checked));
            SectionIconsDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(SectionIconsDictionaryInstance);

            #endregion

            #region Update Search Setting
            foreach (ListItem item in rdoSearchSetting.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }
            #endregion
            
            #region Update points settings

            Dictionary<string, string> SS_EnablePointsDictionaryInstance = new Dictionary<string, string>();
            SS_EnablePointsDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            SS_EnablePointsDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnEnablePoints.Value));
            SS_EnablePointsDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnablePoints.Checked));
            SS_EnablePointsDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));

            SS_EnablePointsDictionaryInstance.Add("FeatureDefaultValue", txtPoints.Text);
            StoreBL.UpdateStoreDetail(SS_EnablePointsDictionaryInstance);

            #endregion

            #region Update Payment Gateway

            foreach (RepeaterItem item in rptSCPaymentGateway.Items)
            {
                CheckBox chkSCPaymentGateway = (CheckBox)item.FindControl("chkSCPaymentGateway");
                HtmlInputRadioButton rbSCPaymentGateway = (HtmlInputRadioButton)item.FindControl("rbSCPaymentGateway");
                HiddenField hdnSCPaymentGatewayStoreFeatureDetailId = (HiddenField)item.FindControl("hdnSCPaymentGatewayStoreFeatureDetailId");

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnSCPaymentGatewayStoreFeatureDetailId.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(chkSCPaymentGateway.Checked));
                DictionaryInstance.Add("IsDefault", Convert.ToString(rbSCPaymentGateway.Checked));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Alternative Payments

            //foreach (RepeaterItem item in rptSCAlternativePayments.Items)
            //{
            //    CheckBox chkSCAlternativePayment = (CheckBox)item.FindControl("chkSCAlternativePayment");
            //    HtmlInputRadioButton rbSCAlternativePayment = (HtmlInputRadioButton)item.FindControl("rbSCAlternativePayment");
            //    HiddenField hdnSCAlternativePaymentStoreFeatureDetailId = (HiddenField)item.FindControl("hdnSCAlternativePaymentStoreFeatureDetailId");

            //    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
            //    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            //    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnSCAlternativePaymentStoreFeatureDetailId.Value));
            //    DictionaryInstance.Add("IsEnabled", Convert.ToString(chkSCAlternativePayment.Checked));
            //    DictionaryInstance.Add("IsDefault", Convert.ToString(rbSCAlternativePayment.Checked));
            //    DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            //    StoreBL.UpdateStoreDetail(DictionaryInstance);
            //}

            #endregion

            #region Update Gift Certificate

            foreach (ListItem item in rdlIsGiftCertificateAllow.Items)
            {
                Dictionary<string, string> GiftCertificateDictionaryInstance = new Dictionary<string, string>();
                GiftCertificateDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                GiftCertificateDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                GiftCertificateDictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                GiftCertificateDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(GiftCertificateDictionaryInstance);
            }

            #endregion

            #region Update International Orders

            Dictionary<string, string> InternationalOrdersDictionaryInstance = new Dictionary<string, string>();
            InternationalOrdersDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            InternationalOrdersDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnSCInternationalOrdersStoreFeatureDetailId.Value));
            InternationalOrdersDictionaryInstance.Add("IsEnabled", Convert.ToString(chkSCInternationalOrders.Checked));
            InternationalOrdersDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(InternationalOrdersDictionaryInstance);

            #endregion

            #region Update International Orders Message

            Dictionary<string, string> InternationalOrdersMessageDictionaryInstance = new Dictionary<string, string>();
            InternationalOrdersMessageDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            InternationalOrdersMessageDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnSCMessageStoreFeatureDetailId.Value));
            InternationalOrdersMessageDictionaryInstance.Add("IsEnabled", Convert.ToString(chkSCInternationalOrders.Checked));
            InternationalOrdersMessageDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtSCMessage.Text.Trim()));
            InternationalOrdersMessageDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(InternationalOrdersMessageDictionaryInstance);

            #endregion

            #region Update Minimum Order Value

            Dictionary<string, string> MOVDictionaryInstance = new Dictionary<string, string>();
            MOVDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            MOVDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnSCMOVStoreFeatureDetailId.Value));
            if (!string.IsNullOrEmpty(txtSCMOV.Text.Trim()))
                MOVDictionaryInstance.Add("IsEnabled", Convert.ToString(true));
            else
                MOVDictionaryInstance.Add("IsEnabled", Convert.ToString(false));
            MOVDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtSCMOV.Text.Trim()));
            MOVDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(MOVDictionaryInstance);

            #endregion

            #region Update Allow BackOrder

            Dictionary<string, string> AllowBackOrderDictionaryInstance = new Dictionary<string, string>();
            AllowBackOrderDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            AllowBackOrderDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnODAllowBackOrderStoreFeatureDetailId.Value));
            AllowBackOrderDictionaryInstance.Add("IsEnabled", Convert.ToString(chkAllowBackOrder.Checked));
            AllowBackOrderDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtAllowBackOrder.Text.Trim()));
            AllowBackOrderDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(AllowBackOrderDictionaryInstance);

            #endregion

            #region Update Enable Punchout

            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("IsPunchOut", Convert.ToString(chkSCEnablePunchout.Checked));
            dictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            StoreBL.UpdateIsPunchout(dictionaryInstance);

            //Dictionary<string, string> EnablePunchoutDictionaryInstance = new Dictionary<string, string>();
            //EnablePunchoutDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            //EnablePunchoutDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnSCEnablePunchoutStoreFeatureDetailId.Value));
            //EnablePunchoutDictionaryInstance.Add("IsEnabled", Convert.ToString(chkSCEnablePunchout.Checked));
            //EnablePunchoutDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            //StoreBL.UpdateStoreDetail(EnablePunchoutDictionaryInstance);

            #endregion

            #region Update Approve All Orders

            foreach (ListItem item in rblSCApproveAllOrders.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Customer Type

            foreach (ListItem item in rblCRCustomerType.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                if (item.Text.ToLower() == "hierarchy")
                    DictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(ddlCRCustomerHierarchyLevel.SelectedValue));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Email Approval Required

            foreach (ListItem item in rblCRApprovalRequired.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Email Notification

            Dictionary<string, string> EmailNotificationDictionaryInstance = new Dictionary<string, string>();
            EmailNotificationDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            EmailNotificationDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCRNotificationEmailStoreFeatureDetailId.Value));
            EmailNotificationDictionaryInstance.Add("IsEnabled", Convert.ToString(rblCRApprovalRequired.Items[0].Selected));
            EmailNotificationDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtCRNotificationEmail.Text.Trim()));
            EmailNotificationDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(EmailNotificationDictionaryInstance);

            #endregion

            #region Update White List Validation

            foreach (ListItem item in rblCRWhiteListValidation.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update QuickView

            Dictionary<string, string> ProductComparisonDictionaryInstance = new Dictionary<string, string>();
            ProductComparisonDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            ProductComparisonDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLProductComparisonStoreFeatureDetailId.Value));
            ProductComparisonDictionaryInstance.Add("IsEnabled", Convert.ToString(chkProductComparison.Checked));
            ProductComparisonDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(ProductComparisonDictionaryInstance);

            #endregion

            #region Update Domain Validation

            foreach (ListItem item in rblCREnableDomainValidation.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                if (chkCREnableDomainValidation.Checked)
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                else
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(false));
                
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Email Validation

            foreach (ListItem item in rblCREnableEmailValidation.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                if (chkCREnableEmailValidation.Checked)
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                else
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(false));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Home Link Position
            foreach (ListItem item in rdoLinkPosition.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }
            #endregion

            #region Update Enable SSO

            Dictionary<string, string> EnableSSODictionaryInstance = new Dictionary<string, string>();
            EnableSSODictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            EnableSSODictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCREnableSSOStoreFeatureDetailId.Value));
            EnableSSODictionaryInstance.Add("IsEnabled", Convert.ToString(chkCREnableSSO.Checked));
            EnableSSODictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(EnableSSODictionaryInstance);

            #endregion

            #region Update Reset Password Block Account Duration

            Dictionary<string, string> ResetPasswordBlockAccountDurationDictionaryInstance = new Dictionary<string, string>();
            ResetPasswordBlockAccountDurationDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            ResetPasswordBlockAccountDurationDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCRResetPasswordBlockAccountDurationStoreFeatureDetailId.Value));
            ResetPasswordBlockAccountDurationDictionaryInstance.Add("IsEnabled", Convert.ToString(true));
            ResetPasswordBlockAccountDurationDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtCRResetPasswordBlockAccountDuration.Text.Trim()));
            ResetPasswordBlockAccountDurationDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(ResetPasswordBlockAccountDurationDictionaryInstance);

            #endregion

            #region Update Store Access

            foreach (ListItem item in rblCRStoreAccess.Items)
            {

                Exceptions.WriteInfoLog("Store Access selected:"+ Convert.ToString(item.Selected));
                Exceptions.WriteInfoLog("Store FeatureDetailId:" + Convert.ToString(item.Value));
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Store Type

            foreach (ListItem item in rblCRStoreType.Items)
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                if(item.Selected && item.Text.ToLower() == "b2c")
                DictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtB2CVatPercentage.Text.Trim()));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance);
            }

            #endregion

            #region Update Minimum Password Length

            Dictionary<string, string> MinimumPasswordLengthDictionaryInstance = new Dictionary<string, string>();
            MinimumPasswordLengthDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            MinimumPasswordLengthDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPSMinimumPasswordLengthStoreFeatureDetailId.Value));
            MinimumPasswordLengthDictionaryInstance.Add("IsEnabled", Convert.ToString(1));
            MinimumPasswordLengthDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtPSMinimumPasswordLength.Text.Trim()));
            MinimumPasswordLengthDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(MinimumPasswordLengthDictionaryInstance);

            #endregion

            #region Update Password Expression Commented by SHRIGANESH SINGH for Password Policy 18 May 2016

            //Dictionary<string, string> PasswordExpressionDictionaryInstance = new Dictionary<string, string>();
            //PasswordExpressionDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            //PasswordExpressionDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPSPasswordExpressionStoreFeatureDetailId.Value));
            //PasswordExpressionDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPSPasswordExpression.Checked));
            //PasswordExpressionDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            //StoreBL.UpdateStoreDetail(PasswordExpressionDictionaryInstance);

            //foreach (ListItem item in rdPwdType.Items)
            //{
            //    Dictionary<string, string> PasswordExpressionDictionaryInstance = new Dictionary<string, string>();
            //    PasswordExpressionDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            //    PasswordExpressionDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
            //    PasswordExpressionDictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
            //    PasswordExpressionDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            //    StoreBL.UpdateStoreDetail(PasswordExpressionDictionaryInstance);
            //}

            #endregion

            #region Update Password Policy Added by SHRIGANESH SINGH 09 May 2016

            foreach (ListItem item in rdPasswordType.Items)
            {
                Dictionary<string, string> PasswordPolicyDictionaryInstance = new Dictionary<string, string>();
                PasswordPolicyDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                PasswordPolicyDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                PasswordPolicyDictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                PasswordPolicyDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                StoreBL.UpdateStoreDetail(PasswordPolicyDictionaryInstance);
            }

            #endregion

            #region Update Force Password

            Dictionary<string, string> ForcePasswordDictionaryInstance = new Dictionary<string, string>();
            ForcePasswordDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            ForcePasswordDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPSForcePasswordChangeStoreFeatureDetailId.Value));
            ForcePasswordDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPSForcePasswordChange.Checked));
            ForcePasswordDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(ForcePasswordDictionaryInstance);

            #endregion

            #region Update Password Validity

            Dictionary<string, string> PasswordValidityDictionaryInstance = new Dictionary<string, string>();
            PasswordValidityDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            PasswordValidityDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPSPasswordValidityStoreFeatureDetailId.Value));
            PasswordValidityDictionaryInstance.Add("IsEnabled", Convert.ToString(1));
            PasswordValidityDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtPSPasswordValidity.Text.Trim()));
            PasswordValidityDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(PasswordValidityDictionaryInstance);

            #endregion

            #region Update Enable Captcha

            Dictionary<string, string> EnableCaptchaDictionaryInstance = new Dictionary<string, string>();
            EnableCaptchaDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            EnableCaptchaDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCREnableCaptchaStoreFeatureDetailId.Value));
            EnableCaptchaDictionaryInstance.Add("IsEnabled", Convert.ToString(chkCREnableCaptcha.Checked));
            EnableCaptchaDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(EnableCaptchaDictionaryInstance);

            #endregion

            #region Update Enable Cookie

            Dictionary<string, string> EnableCookieDictionaryInstance = new Dictionary<string, string>();
            EnableCookieDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            EnableCookieDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCLEnableCookieStoreFeatureDetailId.Value));
            EnableCookieDictionaryInstance.Add("IsEnabled", Convert.ToString(chkCLEnableCookie.Checked));
            EnableCookieDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(EnableCookieDictionaryInstance);

            #endregion

            #region Update Cookie Lifetime

            Dictionary<string, string> CookieLifetimeDictionaryInstance = new Dictionary<string, string>();
            CookieLifetimeDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            CookieLifetimeDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCLCookieLifetimeStoreFeatureDetailId.Value));
            CookieLifetimeDictionaryInstance.Add("IsEnabled", Convert.ToString(chkCLEnableCookie.Checked));
            CookieLifetimeDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtCLCookieLifetime.Text.Trim()));
            CookieLifetimeDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(CookieLifetimeDictionaryInstance);

            #endregion

            #region Update Allow Max Order Qty Per Item
            Dictionary<string, string> AllowMaxOrderQtyPerItemDictionaryInstance = new Dictionary<string, string>();
            AllowMaxOrderQtyPerItemDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            AllowMaxOrderQtyPerItemDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnMaxOrderQtyStoreFeatureDetailId.Value));
            AllowMaxOrderQtyPerItemDictionaryInstance.Add("IsEnabled", Convert.ToString(chkMaxAllowQty.Checked));
            AllowMaxOrderQtyPerItemDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtAllowMaxOrder.Text.Trim()));
            AllowMaxOrderQtyPerItemDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(AllowMaxOrderQtyPerItemDictionaryInstance);
            #endregion

            #region Update Cookie Message

            Dictionary<string, string> CookieMessageDictionaryInstance = new Dictionary<string, string>();
            CookieMessageDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            CookieMessageDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCookieMessageStoreFeatureDetailId.Value));
            CookieMessageDictionaryInstance.Add("IsEnabled", Convert.ToString(chkCLEnableCookie.Checked));
            CookieMessageDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtCLCookieMessage.Text.Trim()));
            CookieMessageDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(CookieMessageDictionaryInstance);

            #endregion

            #region Update Coupon code settings

            Dictionary<string, string> SC_EnableCouponCodeDictionaryInstance = new Dictionary<string, string>();
            SC_EnableCouponCodeDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            SC_EnableCouponCodeDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnEnableCouponCode.Value));
            SC_EnableCouponCodeDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnableCouponCode.Checked));
            SC_EnableCouponCodeDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(SC_EnableCouponCodeDictionaryInstance);

            #endregion

            #region Update Punchout Registration settings - Added By Snehal 20 12 2016

            Dictionary<string, string> SC_PunchoutRegDictionaryInstance = new Dictionary<string, string>();
            SC_PunchoutRegDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            SC_PunchoutRegDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnIsPunchoutReg.Value));
            SC_PunchoutRegDictionaryInstance.Add("IsEnabled", Convert.ToString(chkIsPunchoutRegister.Checked));
            SC_PunchoutRegDictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
            StoreBL.UpdateStoreDetail(SC_PunchoutRegDictionaryInstance);

            #endregion

            #region Get IsPunchout Registration

            FeatureBE SS_IsPunchoutRegistration = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_ispunchoutregistration");
            if (SS_IsPunchoutRegistration != null)
            {
                //chkIsPunchoutRegister.Text = SS_IsPunchoutRegistration.FeatureAlias;
                chkIsPunchoutRegister.Checked = SS_IsPunchoutRegistration.FeatureValues[0].IsEnabled;
                hdnIsPunchoutReg.Value = Convert.ToString(SS_IsPunchoutRegistration.FeatureValues[0].StoreFeatureDetailId);
            }

            #endregion

            #region Redirection

            Session["AllStores"] = null;
            Session["SortedView"] = null;

            switch (((Button)sender).CommandName)
            {
                case "SaveClose": 
                    Session["PreviousPage"] = null;
                    Session["StoreId"] = null;
                    Response.Redirect("~/DashBoard.aspx");
                    break;
                case "SaveContinue": 
                    Session["PreviousPage"] = "step2";
                    Session["StoreId"] = StoreId;
                    Response.Redirect("~/StoreCreator/Step3.aspx?id=" + StoreId + "");
                    break;
                default:
                    break;
            }

            #endregion

            #region Points Update
            
            #endregion
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Cancel/Back Button redirection

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Session["PreviousPage"] = null;
            Session["StoreId"] = null;
            Response.Redirect("~/DashBoard.aspx");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void cmdBack_Click(object sender, EventArgs e)
    {
        try
        {
            Session["PreviousPage"] = "step2";
            Session["StoreId"] = Convert.ToInt16(Request.QueryString["id"]);
            Response.Redirect("~/StoreCreator/Step1.aspx?m=edit&id=" + Convert.ToInt16(Request.QueryString["id"]) + "");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Events

    protected void rptPLDisplayType_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                HiddenField hdnPLDisplayTypeStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLDisplayTypeStoreFeatureDetailId");
                hdnPLDisplayTypeStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

                CheckBox chkPLDisplayType = (CheckBox)e.Item.FindControl("chkPLDisplayType");
                chkPLDisplayType.Text = FeatureValue.FeatureValue;
                chkPLDisplayType.Checked = FeatureValue.IsEnabled;

                HtmlInputRadioButton rbPLDisplayType = (HtmlInputRadioButton)e.Item.FindControl("rbPLDisplayType");
                rbPLDisplayType.Attributes.Add("onClick", "SetUniqueRadioButton('DefaultDisplayType',this);");
                if (chkPLDisplayType.Checked)
                {
                    rbPLDisplayType.Checked = FeatureValue.IsDefault;
                    HtmlGenericControl dvDefaultDisplayType = (HtmlGenericControl)e.Item.FindControl("dvDefaultDisplayType");
                    dvDefaultDisplayType.Style.Add("display", "block");
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void rptPLSorting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                HiddenField hdnPLSortingStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLSortingStoreFeatureDetailId");
                hdnPLSortingStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

                CheckBox chkPLSorting = (CheckBox)e.Item.FindControl("chkPLSorting");
                chkPLSorting.Text = FeatureValue.FeatureValue;
                chkPLSorting.Checked = FeatureValue.IsEnabled;

                HtmlInputRadioButton rbPLSorting = (HtmlInputRadioButton)e.Item.FindControl("rbPLSorting");
                rbPLSorting.Attributes.Add("onClick", "SetUniqueRadioButton('PLSorting',this);");
                if (chkPLSorting.Checked)
                {
                    rbPLSorting.Checked = FeatureValue.IsDefault;
                    HtmlGenericControl dvPLSorting = (HtmlGenericControl)e.Item.FindControl("dvPLSorting");
                    dvPLSorting.Style.Add("display", "block");
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void rptPLFilter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.ItemIndex == 0)
                {
                   HtmlGenericControl liFilter = (HtmlGenericControl)e.Item.FindControl("liFilter");
                   liFilter.Style["display"] = "none";
                }
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                HiddenField hdnPLFilterStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLFilterStoreFeatureDetailId");
                hdnPLFilterStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

                CheckBox chkPLFilter = (CheckBox)e.Item.FindControl("chkPLFilter");
                chkPLFilter.Text = FeatureValue.FeatureValue;
                chkPLFilter.Checked = FeatureValue.IsEnabled;

                //HtmlInputRadioButton rbPLFilter = (HtmlInputRadioButton)e.Item.FindControl("rbPLFilter");
                //rbPLFilter.Attributes.Add("onClick", "SetUniqueRadioButton('PLFilter',this);");
                //if (chkPLFilter.Checked)
                //{
                //    rbPLFilter.Checked = FeatureValue.IsDefault;
                //    HtmlGenericControl dvPLFilter = (HtmlGenericControl)e.Item.FindControl("dvPLFilter");
                //    dvPLFilter.Style.Add("display", "block");
                //}
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void rptSCPaymentGateway_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                HiddenField hdnSCPaymentGatewayStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnSCPaymentGatewayStoreFeatureDetailId");
                hdnSCPaymentGatewayStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

                CheckBox chkSCPaymentGateway = (CheckBox)e.Item.FindControl("chkSCPaymentGateway");
                chkSCPaymentGateway.Text = FeatureValue.FeatureValue;
                chkSCPaymentGateway.Checked = FeatureValue.IsEnabled;

                HtmlInputRadioButton rbSCPaymentGateway = (HtmlInputRadioButton)e.Item.FindControl("rbSCPaymentGateway");
                rbSCPaymentGateway.Attributes.Add("onClick", "SetUniqueRadioButton('SCPaymentGateway',this);");
                if (chkSCPaymentGateway.Checked)
                {
                    rbSCPaymentGateway.Checked = FeatureValue.IsDefault;
                    HtmlGenericControl dvSCPaymentGateway = (HtmlGenericControl)e.Item.FindControl("dvSCPaymentGateway");
                    dvSCPaymentGateway.Style.Add("display", "block");
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    //protected void rptSCAlternativePayments_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    try
    //    {
    //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //        {
    //            FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
    //            HiddenField hdnSCAlternativePaymentStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnSCAlternativePaymentStoreFeatureDetailId");
    //            hdnSCAlternativePaymentStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

    //            CheckBox chkSCAlternativePayment = (CheckBox)e.Item.FindControl("chkSCAlternativePayment");
    //            chkSCAlternativePayment.Text = FeatureValue.FeatureValue;
    //            chkSCAlternativePayment.Checked = FeatureValue.IsEnabled;

    //            HtmlInputRadioButton rbSCAlternativePayment = (HtmlInputRadioButton)e.Item.FindControl("rbSCAlternativePayment");
    //            rbSCAlternativePayment.Attributes.Add("onClick", "SetUniqueRadioButton('SCAlternativePayment',this);");
    //            if (chkSCAlternativePayment.Checked)
    //            {
    //                rbSCAlternativePayment.Checked = FeatureValue.IsDefault;
    //                HtmlGenericControl dvSCAlternativePayment = (HtmlGenericControl)e.Item.FindControl("dvSCAlternativePayment");
    //                dvSCAlternativePayment.Style.Add("display", "block");
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Exceptions.WriteExceptionLog(ex);
    //    }
    //}

    #endregion

}