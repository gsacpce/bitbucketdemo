﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="ViewTemplate.aspx.cs" Inherits="StoreCreator_ViewTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Templates</li>
            <li>View Template</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <h3>Templates</h3>
                    <p></p>
                    <div class="table-responsive table_store">
                        <asp:GridView ID="gvAllTemplates" runat="server" AutoGenerateColumns="false" CssClass="table" OnRowDataBound="gvAllTemplates_DataBound"
                            AllowPaging="true" PageSize="5" OnPageIndexChanging="gvAllTemplates_PageIndexChanging"
                            AllowSorting="true" OnSorting="gvAllTemplates_Sorting" OnRowCommand="gvAllTemplates_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="Template" SortExpression="TemplateName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTemplateName" runat="server" Text='<% #Eval("TemplateName")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date Created" SortExpression="CreatedDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTemplateCreatedDate" runat="server" Text='<% #Eval("CreatedDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Created By">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedBy" runat="server" Text='<% #Eval("CreatedByName")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Used in No. of Stores">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUsedInNoOfStores" runat="server" Text='<% #Eval("NoOfStores")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <div class="action">
                                            <a class="icon" href="#"></a>
                                            <div class="action_hover">
                                                <span></span>
                                                <div class="other_option">
                                                    <asp:LinkButton ID="lnkUpload" runat="server" CommandName="UploadTemplate" CommandArgument="">Upload</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDownload" runat="server" CommandName="DownloadTemplate" CommandArgument="">Download</asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            var originalLeave = $.fn.popover.Constructor.prototype.leave;
            $.fn.popover.Constructor.prototype.leave = function (obj) {
                var self = obj instanceof this.constructor ?
                    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
                var container, timeout;

                originalLeave.call(this, obj);

                if (obj.currentTarget) {
                    container = $(obj.currentTarget).siblings('.popover')
                    timeout = self.timeout;
                    container.one('mouseenter', function () {
                        //We entered the actual popover – call off the dogs
                        clearTimeout(timeout);
                        //Let's monitor popover content instead
                        container.one('mouseleave', function () {
                            $.fn.popover.Constructor.prototype.leave.call(self, self);
                        });
                    })
                }
            };

            $('body').popover({ selector: '[data-popover]', trigger: 'click hover', placement: 'right', delay: { show: 50, hide: 400 } });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;


            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;

            });
        });
    </script>
</asp:Content>

