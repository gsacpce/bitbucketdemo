﻿//using DotNetUtilities.Utilities;
using HtmlAgilityPack;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class StoreCreator_Step3 : System.Web.UI.Page
{
    #region Variables

    StoreBE StoreInstance;

    #endregion

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Added by sanchit for authorization

        GlobalFunctions.IsAuthorized("step1.aspx");

        if (Session["PreviousPage"] != null && Session["StoreId"] != null)
        {
            if (Request.QueryString["id"] != null)
            {
                Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);
                if (Convert.ToString(Session["PreviousPage"]) == "step2" && Convert.ToInt16(Session["StoreId"]) == StoreId) { }
                else
                    Response.Redirect("~/UnAuthorized.html");
            }
            else
                Response.Redirect("~/UnAuthorized.html");
        }
        else
            Response.Redirect("~/UnAuthorized.html");

        #endregion

        if (!IsPostBack)
        {
            GetAllTemplates();
        }
    }

    #endregion

    #region Get All Templates

    private void GetAllTemplates()
    {
        try
        {
            Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);
            StoreInstance = StoreBL.GetStoreDetails(StoreId);
            List<TemplateBE> GetAllTemplates = TemplateBL.GetAllTemplateDetails();
            rptTemplates.DataSource = GetAllTemplates;
            rptTemplates.DataBind();
            string count = GetAllTemplates != null ? Convert.ToString(GetAllTemplates.Count) : "0";
            if (count == "1")
                lblTemplateCount.Text = "Total " + count + " Stores Template";
            else
                lblTemplateCount.Text = "Total " + count + " Stores Templates";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Add Template

    protected void cmdAddTemplate_Click(object sender, EventArgs e)
    {
        try
        {
            Session["TemplateMode"] = "create";
            Response.Redirect("~/StoreCreator/AddEditTemplate.aspx");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Save Store template

    protected void cmdSaveTemplate_Click(object sender, EventArgs e)
    {
        SaveTemplate();
    }

    private void SaveTemplate()
    {
        try
        {
            UserBE user = (UserBE)Session["User"];
            Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);

            foreach (RepeaterItem item in rptTemplates.Items)
            {
                HtmlInputRadioButton rdbSelectTemplate = (HtmlInputRadioButton)item.FindControl("rdbSelectTemplate");
                HiddenField hdnTemplateId = (HiddenField)item.FindControl("hdnTemplateId");
                HiddenField hdnTemplateName = (HiddenField)item.FindControl("hdnTemplateName");

                if (rdbSelectTemplate.Checked)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                    DictionaryInstance.Add("TemplateId", Convert.ToString(hdnTemplateId.Value));
                    bool IsUpdated = StoreBL.UpdateStoreTemplate(DictionaryInstance);

                    if (IsUpdated)
                    {
                        Session["AllStores"] = null;
                        Session["SortedView"] = null;
                        Session["PreviousPage"] = "step3";
                        Session["StoreId"] = StoreId;
                        Response.Redirect("~/StoreCreator/Step4.aspx?id=" + StoreId + "");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Events

    protected void rptTemplates_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TemplateBE Template = (TemplateBE)e.Item.DataItem;

                Image imgTemplate = (Image)e.Item.FindControl("imgTemplate");
                imgTemplate.ImageUrl = Convert.ToString(GlobalFunctions.GetSetting("CS_TEMPLATEIMAGEPATH")).Replace("@templatename", Template.TemplateName);

                HtmlInputRadioButton rdbSelectTemplate = (HtmlInputRadioButton)e.Item.FindControl("rdbSelectTemplate");
                rdbSelectTemplate.Attributes.Add("onClick", "SetUniqueRadioButton('SelectTemplate',this);");

                if (StoreInstance.TemplateId == Template.TemplateId)
                    rdbSelectTemplate.Checked = true;

                Label lblSelectTemplateText = (Label)e.Item.FindControl("lblSelectTemplateText");
                lblSelectTemplateText.Text = Template.TemplateName;

                HiddenField hdnTemplateId = (HiddenField)e.Item.FindControl("hdnTemplateId");
                hdnTemplateId.Value = Convert.ToString(Template.TemplateId);

                HiddenField hdnTemplateName = (HiddenField)e.Item.FindControl("hdnTemplateName");
                hdnTemplateName.Value = Convert.ToString(Template.TemplateName);
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion
}