﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO.Compression;
using PWGlobalEcomm.BusinessLogic;
using HtmlAgilityPack;
using System.Security.AccessControl;
using System.Configuration;

public partial class StoreCreator_AddEditTemplate : System.Web.UI.Page
{
    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized("RedirectToAddTemplate.aspx");
        if (!IsPostBack)
        {
            GetAddEditTemplateDetails();
        }
    }

    #endregion

    #region Get Add/Edit Template Details

    private void GetAddEditTemplateDetails()
    {
        try
        {
            if (Session["TemplateMode"] != null)
            {
                if (Convert.ToString(Session["TemplateMode"]) == "edit")
                {
                    if (Session["TemplateId"] != null && Session["TemplateName"] != null)
                    {
                        int TemplateId = Convert.ToInt16(Session["TemplateId"]);
                        txtTemplateName.Text = Convert.ToString(Session["TemplateName"]);
                        cmdAddEditTemplate.Text = "Update Template";
                        txtTemplateName.Enabled = false;
                        List<StoreBE> StoreCollection = StoreBL.GetAllStoreDetails();
                        if (StoreCollection != null)
                        {
                            if (StoreCollection.Count > 0)
                            {
                                rptTemplateStores.DataSource = StoreCollection.FindAll(x => x.TemplateId == TemplateId);
                                rptTemplateStores.DataBind();
                            }
                        }
                    }
                }
                else
                {
                    dvTemplateStores.Style["display"] = "none";
                    cmdAddEditTemplate.Text = "Add Template";
                }

            }
            else
            {
                dvTemplateStores.Style["display"] = "none";
                Session["TemplateMode"] = "create";
                cmdAddEditTemplate.Text = "Add Template";
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Save Template

    protected void cmdAddEditTemplate_Click(object sender, EventArgs e)
    {
        AddEditTemplate();
    }

    private void AddEditTemplate()
    {
        try
        {
            if (fuUploadTemplate.HasFile)
            {
                UserBE user = (UserBE)Session["User"];

                if (Session["TemplateMode"] != null)
                {
                    //Chk for File Extension
                    string[] strFileTypes = { ".zip" };
                    bool bChkFileType = false;
                    bChkFileType = GlobalFunctions.CheckFileExtension(fuUploadTemplate, strFileTypes);
                    if (!bChkFileType)
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
                        SetFocus(fuUploadTemplate.ClientID);
                        return;
                    }

                    if (Convert.ToString(Session["TemplateMode"]) == "create")
                    {
                        if (!TemplateBL.IsTemplateExists(txtTemplateName.Text.Trim()))
                        {
                            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                            dictionaryInstance.Add("TemplateName", Convert.ToString(txtTemplateName.Text));
                            dictionaryInstance.Add("CreatedBy", Convert.ToString(user.UserId));
                            TemplateBL.InsertTemplate(dictionaryInstance);
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Template name already exists", AlertType.Warning);
                            return;
                        }
                    }
                    else if (Convert.ToString(Session["TemplateMode"]) == "edit")
                    {
                        if (Session["TemplateId"] != null)
                        {
                            string TemplateId = Convert.ToString(Session["TemplateId"]);
                            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                            dictionaryInstance.Add("TemplateId", Convert.ToString(TemplateId));
                            dictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                            TemplateBL.UpdateTemplate(dictionaryInstance);
                        }
                    }

                    string filename = Path.GetFileName(fuUploadTemplate.FileName);
                    string strTemplatePathWithExtension = GlobalFunctions.GetPhysicalFolderPath() + @"\Templates\" + filename;
                    string strTemplatePathWithoutExtension = GlobalFunctions.GetPhysicalFolderPath() + @"\Templates\" + Path.GetFileNameWithoutExtension(filename);
                    string strTemplateBackupPath = GlobalFunctions.GetPhysicalFolderPath() + @"\Template_BKP\";

                    if (File.Exists(strTemplatePathWithExtension))
                    {
                        string dateTimeFormat = DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + "_"
                                            + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();

                        File.Move(strTemplatePathWithExtension, strTemplateBackupPath + Path.GetFileNameWithoutExtension(filename) + "_" + dateTimeFormat + ".zip");
                    }

                    fuUploadTemplate.SaveAs(strTemplatePathWithExtension);

                    if (Directory.Exists(strTemplatePathWithoutExtension))
                        Directory.Delete(strTemplatePathWithoutExtension, true);

                    ZipFile.ExtractToDirectory(GlobalFunctions.GetPhysicalFolderPath() + @"\Templates\" + filename, GlobalFunctions.GetPhysicalFolderPath() + @"\Templates\");

                    DirectoryInfo HtmlDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Htmls");
                    FileInfo[] HtmlFileInfo = HtmlDirInfo.GetFiles();

                    HtmlDocument HtmlDoc = new HtmlDocument();
                    HtmlDocument AspxDoc = new HtmlDocument();

                    foreach (FileInfo HtmlFile in HtmlFileInfo)
                    {
                        switch (Path.GetFileNameWithoutExtension(HtmlFile.FullName).ToLower())
                        {
                            case "home":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace Modalpopup

                                //HtmlNode htmlModalPopupNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmodalpopup");

                                //DirectoryInfo MasterDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Master");
                                //FileInfo[] MasterFileInfo = MasterDirInfo.GetFiles();

                                //foreach (FileInfo MasterFile in MasterFileInfo)
                                //{
                                //    switch (Path.GetFileNameWithoutExtension(MasterFile.FullName).ToLower())
                                //    {
                                //        case "masterpage":
                                //            AspxDoc.Load(MasterFile.FullName);
                                //            HtmlNode Aspxnode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmodalpopup");
                                //            Aspxnode.InnerHtml = Aspxnode.InnerHtml.Replace(Aspxnode.InnerHtml, htmlModalPopupNode.InnerHtml);
                                //            AspxDoc.Save(MasterFile.FullName);
                                //            break;
                                //        default:
                                //            break;
                                //    }
                                //}

                                #endregion

                                #region Replace Header and Footer

                                HtmlNode htmlHeaderNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divheader");
                                HtmlNode htmlFooterNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divfooter");
                                //HtmlNode htmlMenuNode = htmlHeaderNode.Descendants("ul").FirstOrDefault(y => y.Id.ToLower() == "ulmenu");
                                //htmlMenuNode.InnerHtml = htmlMenuNode.InnerHtml.Replace(htmlMenuNode.InnerHtml, "<uc:Menu ID='menu' runat='server' />");

                                DirectoryInfo HeaderfooterDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Master" + @"\UserControls");
                                FileInfo[] HeaderfooterFileInfo = HeaderfooterDirInfo.GetFiles();

                                foreach (FileInfo HeaderFooterFile in HeaderfooterFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(HeaderFooterFile.FullName).ToLower())
                                    {
                                        case "header":
                                            AspxDoc.Load(HeaderFooterFile.FullName);
                                            HtmlNode HeaderAspxnode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divheader");

                                            HeaderAspxnode.InnerHtml = HeaderAspxnode.InnerHtml.Replace(HeaderAspxnode.InnerHtml, htmlHeaderNode.InnerHtml);
                                            AspxDoc.Save(HeaderFooterFile.FullName);
                                            break;
                                        case "footer":
                                            AspxDoc.Load(HeaderFooterFile.FullName);
                                            HtmlNode FooterAspxnode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divfooter");
                                            FooterAspxnode.InnerHtml = FooterAspxnode.InnerHtml.Replace(FooterAspxnode.InnerHtml, htmlFooterNode.InnerHtml);
                                            AspxDoc.Save(HeaderFooterFile.FullName);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                #region Replace Home page main content

                                HtmlNode htmlHomeMaincontentNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");

                                DirectoryInfo HomeDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Home");
                                FileInfo[] HomeFileInfo = HomeDirInfo.GetFiles();

                                foreach (FileInfo HomeFile in HomeFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(HomeFile.FullName).ToLower())
                                    {
                                        case "home":
                                            AspxDoc.Load(HomeFile.FullName);
                                            HtmlNode Aspxnode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divhomecontainer");
                                            Aspxnode.InnerHtml = Aspxnode.InnerHtml.Replace(Aspxnode.InnerHtml, htmlHomeMaincontentNode.InnerHtml);
                                            AspxDoc.Save(HomeFile.FullName);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                break;
                            case "productlisting":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace ProductListing page

                                DirectoryInfo ProductsDataDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Products");
                                FileInfo[] ProductsDataFileInfo = ProductsDataDirInfo.GetFiles();
                                string ProductListingPagePath = "";

                                foreach (FileInfo ProductsDataFile in ProductsDataFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(ProductsDataFile.FullName).ToLower())
                                    {
                                        case "filter_ajax":
                                            #region Replace filter_ajax page

                                            HtmlNode htmlFilterDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvfilterdata");
                                            AspxDoc.Load(ProductsDataFile.FullName);
                                            HtmlNode AspxFilterNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divfilter");
                                            AspxFilterNode.InnerHtml = AspxFilterNode.InnerHtml.Replace(AspxFilterNode.InnerHtml, htmlFilterDataNode.InnerHtml);

                                            AspxDoc.Save(ProductsDataFile.FullName);

                                            #endregion
                                            break;
                                        case "products_ajax":
                                            #region Replace products_ajax page

                                            HtmlNode htmlProductDataNode = HtmlDoc.DocumentNode.Descendants("ul").FirstOrDefault(x => x.Id.ToLower() == "dvproductdata");
                                            HtmlNode htmlProductLiNode = htmlProductDataNode.ChildNodes.FirstOrDefault(x => x.Name == "li");

                                            AspxDoc.Load(ProductsDataFile.FullName);
                                            HtmlNode AspxProductDataNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "productdata");
                                            AspxProductDataNode.InnerHtml = AspxProductDataNode.InnerHtml.Replace(AspxProductDataNode.InnerHtml, htmlProductDataNode.InnerHtml);
                                            AspxProductDataNode.InnerHtml = "<asp:repeater id='rptProducts' runat='server' onitemdatabound='rptProducts_ItemDataBound'>" +
                                                                            "<ItemTemplate>" + htmlProductLiNode.OuterHtml +
                                                                            "</ItemTemplate>" +
                                                                            "</asp:repeater><input type='hidden' class='totalrecords' id='hidTotalRecords' runat='server' />";
                                            AspxDoc.Save(ProductsDataFile.FullName);

                                            #endregion
                                            break;
                                        case "productlisting":
                                            #region Replace other changes in productlisting page

                                            ProductListingPagePath = ProductsDataFile.FullName;
                                            HtmlNode htmlProductListingDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(ProductsDataFile.FullName);
                                            HtmlNode AspxProductListingnode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxProductListingnode.InnerHtml = AspxProductListingnode.InnerHtml.Replace(AspxProductListingnode.InnerHtml, htmlProductListingDataNode.InnerHtml);
                                            AspxDoc.Save(ProductsDataFile.FullName);

                                            #endregion
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                AspxDoc.Load(ProductListingPagePath);

                                HtmlNode AspxFilterDeleteNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvfilterdata");
                                AspxFilterDeleteNode.InnerHtml = string.Empty;

                                HtmlNode AspxProductDataDeleteNode = AspxDoc.DocumentNode.Descendants("ul").FirstOrDefault(x => x.Id.ToLower() == "dvproductdata");
                                AspxProductDataDeleteNode.InnerHtml = string.Empty;

                                AspxDoc.Save(ProductListingPagePath);

                                #endregion

                                break;
                            case "productdetails":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace ProductDetail page

                                DirectoryInfo ProductDetailsDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Products");
                                FileInfo[] ProductDetailsFileInfo = ProductDetailsDirInfo.GetFiles();
                                string ProductDetailPagePath = "";

                                foreach (FileInfo ProductDetailFile in ProductDetailsFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(ProductDetailFile.FullName).ToLower())
                                    {
                                        case "productdetails":
                                            #region Replace other changes in productDetail page

                                            ProductDetailPagePath = ProductDetailFile.FullName;
                                            HtmlNode htmlProductDetailDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(ProductDetailFile.FullName);
                                            HtmlNode AspxProductDetailnode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxProductDetailnode.InnerHtml = AspxProductDetailnode.InnerHtml.Replace(AspxProductDetailnode.InnerHtml, htmlProductDetailDataNode.InnerHtml);
                                            AspxDoc.Save(ProductDetailFile.FullName);

                                            HtmlNode HtmlaSocialMediaNode = HtmlDoc.DocumentNode.Descendants("a").FirstOrDefault(x => x.Id.ToLower() == "asocialmedia");

                                            string aSocialMediaData = "<asp:Repeater ID='rptSocialMedia' runat='server' OnItemDataBound='rptSocialMedia_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmlaSocialMediaNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdvSocialMediaIconNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvsocialmediaicon");
                                            AspxdvSocialMediaIconNode.InnerHtml = AspxdvSocialMediaIconNode.InnerHtml.Replace(AspxdvSocialMediaIconNode.InnerHtml, aSocialMediaData);
                                            AspxDoc.Save(ProductDetailFile.FullName);

                                            #endregion
                                            break;
                                        case "productreview":
                                            #region Replace Product Review page

                                            HtmlNode HtmldivProductReviewNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divproductreview");

                                            string ProductReviewData = "<asp:Repeater ID='rptProductReview' runat='server' OnItemDataBound='rptProductReview_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmldivProductReviewNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            AspxDoc.Load(ProductDetailFile.FullName);
                                            HtmlNode AspxdvReviewAndRatingNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvproductreview");
                                            AspxdvReviewAndRatingNode.InnerHtml = AspxdvReviewAndRatingNode.InnerHtml.Replace(AspxdvReviewAndRatingNode.InnerHtml, ProductReviewData);
                                            AspxDoc.Save(ProductDetailFile.FullName);

                                            #endregion
                                            break;
                                        case "products_recentlyviewedproducts":
                                            #region Replace Recently Viewed Products page

                                            HtmlNode htmldvRecentlyViewContainerDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvrecentlyviewcontainer");
                                            AspxDoc.Load(ProductDetailFile.FullName);
                                            HtmlNode AspxdvRecentlyViewContainerNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvrecentlyviewcontainer");
                                            AspxdvRecentlyViewContainerNode.InnerHtml = AspxdvRecentlyViewContainerNode.InnerHtml.Replace(AspxdvRecentlyViewContainerNode.InnerHtml, htmldvRecentlyViewContainerDataNode.InnerHtml);
                                            AspxDoc.Save(ProductDetailFile.FullName);

                                            HtmlNode HtmlliPageNode = HtmlDoc.DocumentNode.Descendants("li").FirstOrDefault(x => x.Id.ToLower() == "lirecentlyviewed");

                                            string liPageData = "<asp:repeater id='rptRecentlyViewed' runat='server' onitemdatabound='rptRecentlyViewed_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmlliPageNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxowldemorNode = AspxDoc.DocumentNode.Descendants("ul").FirstOrDefault(x => x.Id.ToLower() == "owl-demo");
                                            AspxowldemorNode.InnerHtml = AspxowldemorNode.InnerHtml.Replace(AspxowldemorNode.InnerHtml, liPageData);
                                            AspxDoc.Save(ProductDetailFile.FullName);

                                            #endregion
                                            break;
                                        case "products_youmayalsolike":
                                            #region Replace You May Also Like page

                                            HtmlNode htmldvYouMayAlsoLikeDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvyoumayalsolike");
                                            AspxDoc.Load(ProductDetailFile.FullName);
                                            HtmlNode AspxdvYouMayAlsoLikeNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvyoumayalsolike");
                                            AspxdvYouMayAlsoLikeNode.InnerHtml = AspxdvYouMayAlsoLikeNode.InnerHtml.Replace(AspxdvYouMayAlsoLikeNode.InnerHtml, htmldvYouMayAlsoLikeDataNode.InnerHtml);

                                            AspxDoc.Save(ProductDetailFile.FullName);

                                            #endregion
                                            break;
                                        default: break;
                                    }
                                }

                                AspxDoc.Load(ProductDetailPagePath);

                                HtmlNode AspxdvReviewAndRatingDeleteNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvreviewandrating");
                                AspxdvReviewAndRatingDeleteNode.InnerHtml = string.Empty;

                                HtmlNode AspxdvRecentlyViewContainerDeleteNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvrecentlyviewcontainer");
                                AspxdvRecentlyViewContainerDeleteNode.InnerHtml = string.Empty;

                                HtmlNode AspxdvYouMayAlsoLikeDeleteNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "dvyoumayalsolike");
                                AspxdvYouMayAlsoLikeDeleteNode.InnerHtml = string.Empty;

                                AspxDoc.Save(ProductDetailPagePath);

                                #endregion

                                break;
                            case "basket":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace Basket page

                                DirectoryInfo BasketDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\ShoppingCart");
                                FileInfo[] BasketFileInfo = BasketDirInfo.GetFiles();

                                foreach (FileInfo BasketFile in BasketFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(BasketFile.FullName).ToLower())
                                    {
                                        case "basket":
                                            #region Replace basket page

                                            HtmlNode htmlBasketDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(BasketFile.FullName);
                                            HtmlNode AspxBasketNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxBasketNode.InnerHtml = AspxBasketNode.InnerHtml.Replace(AspxBasketNode.InnerHtml, htmlBasketDataNode.InnerHtml);
                                            AspxDoc.Save(BasketFile.FullName);

                                            HtmlNode HtmlBasketListingNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divbasketlisting");
                                            HtmlNode HtmlHeaderTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divheadertemplate");
                                            HtmlNode HtmlItemTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divitemtemplate");
                                            HtmlNode HtmlFooterTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divfootertemplate");

                                            HtmlBasketListingNode.InnerHtml = "<asp:Repeater ID='rptBasketListing' runat='server' OnItemDataBound='rptBasketListing_ItemDataBound'>" +
                                                                              "<HeaderTemplate>" + HtmlHeaderTemplateNode.InnerHtml +
                                                                              "</HeaderTemplate>" +
                                                                              "<ItemTemplate>" + HtmlItemTemplateNode.InnerHtml +
                                                                              "</ItemTemplate>" +
                                                                              "<FooterTemplate>" + HtmlFooterTemplateNode.InnerHtml +
                                                                              "</FooterTemplate>" +
                                                                              "</asp:Repeater>" +
                                                                              "<div id='dvEmptyBasket' runat='server'></div>";

                                            HtmlNode AspxBasketListingNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divbasketlisting");
                                            AspxBasketListingNode.InnerHtml = AspxBasketListingNode.InnerHtml.Replace(AspxBasketListingNode.InnerHtml, HtmlBasketListingNode.InnerHtml);
                                            AspxDoc.Save(BasketFile.FullName);

                                            #endregion
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                break;
                            case "categorylisting":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace CategoryListing page

                                DirectoryInfo CategoryListingDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Products");
                                FileInfo[] CategoryListingFileInfo = CategoryListingDirInfo.GetFiles();

                                foreach (FileInfo CategoryListingFile in CategoryListingFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(CategoryListingFile.FullName).ToLower())
                                    {
                                        case "categorylisting":
                                            #region Replace CategoryListing page

                                            HtmlNode htmlCategoryListingDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(CategoryListingFile.FullName);
                                            HtmlNode AspxCategoryListingNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxCategoryListingNode.InnerHtml = AspxCategoryListingNode.InnerHtml.Replace(AspxCategoryListingNode.InnerHtml, htmlCategoryListingDataNode.InnerHtml);
                                            AspxDoc.Save(CategoryListingFile.FullName);

                                            //HtmlNode HtmlsubcategoryNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divsubcategory");
                                            HtmlNode HtmllisubcategoryNode = HtmlDoc.DocumentNode.Descendants("li").FirstOrDefault(x => x.Id.ToLower() == "lisubcategory");

                                            string subcategoryData = "<ul>" +
                                                                     "<asp:Repeater ID='rptSubCategory' runat='server'>" +
                                                                     "<ItemTemplate>" + HtmllisubcategoryNode.OuterHtml +
                                                                     "</ItemTemplate>" +
                                                                     "</asp:Repeater>" +
                                                                     "</ul>";

                                            HtmlNode AspxsubcategoryNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divsubcategory");
                                            AspxsubcategoryNode.InnerHtml = AspxsubcategoryNode.InnerHtml.Replace(AspxsubcategoryNode.InnerHtml, subcategoryData);
                                            AspxDoc.Save(CategoryListingFile.FullName);

                                            #endregion
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                break;
                            case "payment":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace Payment page

                                DirectoryInfo PaymentDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\ShoppingCart");
                                FileInfo[] PaymentFileInfo = PaymentDirInfo.GetFiles();

                                foreach (FileInfo PaymentFile in PaymentFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(PaymentFile.FullName).ToLower())
                                    {
                                        case "payment":
                                            #region Replace Payment page

                                            HtmlNode htmlPaymentDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(PaymentFile.FullName);
                                            HtmlNode AspxPaymentNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxPaymentNode.InnerHtml = AspxPaymentNode.InnerHtml.Replace(AspxPaymentNode.InnerHtml, htmlPaymentDataNode.InnerHtml);
                                            AspxDoc.Save(PaymentFile.FullName);

                                            HtmlNode HtmlItemTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divitemtemplate");
                                            HtmlNode HtmlFooterTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divfootertemplate");

                                            string PaymentBasketListingData = "<asp:Repeater ID='rptBasketListing' runat='server' OnItemDataBound='rptBasketListing_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmlItemTemplateNode.InnerHtml +
                                                                             "</ItemTemplate>" +
                                                                             "<FooterTemplate>" + HtmlFooterTemplateNode.InnerHtml +
                                                                             "</FooterTemplate>" +
                                                                             "</asp:Repeater>" +
                                                                             "<div class='checkout_totals_outer pageSmlText' id='divStatictext' runat='server'></div>";

                                            HtmlNode AspxPaymentBasketListingNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "header4_space");
                                            AspxPaymentBasketListingNode.InnerHtml = AspxPaymentBasketListingNode.InnerHtml.Replace(AspxPaymentBasketListingNode.InnerHtml, PaymentBasketListingData);
                                            AspxDoc.Save(PaymentFile.FullName);

                                            #endregion
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                break;
                            case "orderhistory":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace Order History page

                                DirectoryInfo OrderHistoryDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Orders");
                                FileInfo[] OrderHistoryFileInfo = OrderHistoryDirInfo.GetFiles();

                                foreach (FileInfo OrderHistoryFile in OrderHistoryFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(OrderHistoryFile.FullName).ToLower())
                                    {
                                        case "orderhistory":
                                            #region Replace Order History page

                                            HtmlNode htmlOrderHistoryDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(OrderHistoryFile.FullName);
                                            HtmlNode AspxOrderHistoryNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxOrderHistoryNode.InnerHtml = AspxOrderHistoryNode.InnerHtml.Replace(AspxOrderHistoryNode.InnerHtml, htmlOrderHistoryDataNode.InnerHtml);
                                            AspxDoc.Save(OrderHistoryFile.FullName);

                                            HtmlNode HtmlliPageNode = HtmlDoc.DocumentNode.Descendants("li").FirstOrDefault(x => x.Id.ToLower() == "lipage");

                                            string liPageData = "<asp:Repeater ID='rptPager' runat='server' OnItemCommand='rptPager_ItemCommand'>" +
                                                                             "<ItemTemplate>" + HtmlliPageNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";
                                                                             
                                            HtmlNode AspxulPagerNode = AspxDoc.DocumentNode.Descendants("ul").FirstOrDefault(x => x.Id.ToLower() == "ulpager");
                                            AspxulPagerNode.InnerHtml = AspxulPagerNode.InnerHtml.Replace(AspxulPagerNode.InnerHtml, liPageData);
                                            AspxDoc.Save(OrderHistoryFile.FullName);

                                            HtmlNode HtmlHeaderTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divheadertemplate");
                                            HtmlNode HtmlFooterTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divfootertemplate");

                                            string divOrdersData = "<asp:Repeater ID='rptOrders' runat='server' OnItemCommand='rptOrders_ItemCommand' OnItemDataBound='rptOrders_ItemDataBound'>" +
                                                                             "<HeaderTemplate>" + HtmlHeaderTemplateNode.InnerHtml +
                                                                             "</HeaderTemplate>" +
                                                                             "<ItemTemplate>" + HtmlFooterTemplateNode.InnerHtml +
                                                                             "</ItemTemplate>" +
                                                                             "<SeparatorTemplate></SeparatorTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivOrdersNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divorders");
                                            AspxdivOrdersNode.InnerHtml = AspxdivOrdersNode.InnerHtml.Replace(AspxdivOrdersNode.InnerHtml, divOrdersData);
                                            AspxDoc.Save(OrderHistoryFile.FullName);

                                            #endregion
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                break;
                            case "orderitem":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace Order Item page

                                DirectoryInfo OrderItemDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Orders");
                                FileInfo[] OrderItemFileInfo = OrderItemDirInfo.GetFiles();
                                string OrderItemPagePath = "";

                                foreach (FileInfo OrderItemFile in OrderItemFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(OrderItemFile.FullName).ToLower())
                                    {
                                        case "orderitem":
                                            #region Replace Order Item page

                                            OrderItemPagePath = OrderItemFile.FullName;

                                            HtmlNode htmlOrderItemDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(OrderItemFile.FullName);
                                            HtmlNode AspxOrderItemNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxOrderItemNode.InnerHtml = AspxOrderItemNode.InnerHtml.Replace(AspxOrderItemNode.InnerHtml, htmlOrderItemDataNode.InnerHtml);
                                            AspxDoc.Save(OrderItemFile.FullName);

                                            HtmlNode HtmldivTrackingNumberNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divtrackingnumber");

                                            string divTrackingNumberData = "<asp:Repeater ID='rptTrackingNumber' runat='server'>" +
                                                                             "<ItemTemplate>" + HtmldivTrackingNumberNode.InnerHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivTrackingNumberNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divtrackingnumber");
                                            AspxdivTrackingNumberNode.InnerHtml = AspxdivTrackingNumberNode.InnerHtml.Replace(AspxdivTrackingNumberNode.InnerHtml, divTrackingNumberData);
                                            AspxDoc.Save(OrderItemFile.FullName);

                                            HtmlNode HtmlHeaderTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divheadertemplate");
                                            HtmlNode HtmlFooterTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divfootertemplate");

                                            string divProductItemData = "<asp:Repeater ID='rptProductItem' runat='server' OnItemDataBound='rptProductItem_ItemDataBound' OnItemCommand='rptProductItem_ItemCommand'>" +
                                                                             "<HeaderTemplate>" + HtmlHeaderTemplateNode.InnerHtml +
                                                                             "</HeaderTemplate>" +
                                                                             "<ItemTemplate>" + HtmlFooterTemplateNode.InnerHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivProductItemNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divproductitem");
                                            AspxdivProductItemNode.InnerHtml = AspxdivProductItemNode.InnerHtml.Replace(AspxdivProductItemNode.InnerHtml, divProductItemData);
                                            AspxDoc.Save(OrderItemFile.FullName);

                                            #endregion
                                            break;
                                        case "order_modalajax":
                                            #region Replace Order Modal Ajax page

                                            HtmlNode htmlmodalTrackingDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "modaltracking");
                                            AspxDoc.Load(OrderItemFile.FullName);
                                            HtmlNode AspxOrderModalAjaxNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxOrderModalAjaxNode.InnerHtml = AspxOrderModalAjaxNode.InnerHtml.Replace(AspxOrderModalAjaxNode.InnerHtml, htmlmodalTrackingDataNode.InnerHtml);
                                            AspxDoc.Save(OrderItemFile.FullName);

                                            HtmlNode HtmlOrderModalAjaxHeaderTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divheadertemplate");
                                            HtmlNode HtmlOrderModalAjaxFooterTemplateNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divfootertemplate");

                                            string divModalItemData = "<asp:Repeater ID='rptModalItem' runat='server' OnItemDataBound='rptModalItem_ItemDataBound'>" +
                                                                             "<HeaderTemplate>" + HtmlOrderModalAjaxHeaderTemplateNode.InnerHtml +
                                                                             "</HeaderTemplate>" +
                                                                             "<ItemTemplate>" + HtmlOrderModalAjaxFooterTemplateNode.InnerHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivModalItemNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmodalitem");
                                            AspxdivModalItemNode.InnerHtml = AspxdivModalItemNode.InnerHtml.Replace(AspxdivModalItemNode.InnerHtml, divModalItemData);
                                            AspxDoc.Save(OrderItemFile.FullName);
                                            #endregion

                                            break;
                                        default:
                                            break;
                                    }
                                }

                                AspxDoc.Load(OrderItemPagePath);

                                HtmlNode AspxModalTrackingDeleteNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "modaltracking");
                                AspxModalTrackingDeleteNode.InnerHtml = string.Empty;
                                AspxDoc.Save(OrderItemPagePath);

                                #endregion

                                break;
                            case "register":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace Register page

                                DirectoryInfo RegisterDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Login");
                                FileInfo[] RegisterFileInfo = RegisterDirInfo.GetFiles();

                                foreach (FileInfo RegisterFile in RegisterFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(RegisterFile.FullName).ToLower())
                                    {
                                        case "register":
                                            #region Replace Register page

                                            HtmlNode htmlRegisterDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(RegisterFile.FullName);
                                            HtmlNode AspxRegisterNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxRegisterNode.InnerHtml = AspxRegisterNode.InnerHtml.Replace(AspxRegisterNode.InnerHtml, htmlRegisterDataNode.InnerHtml);
                                            AspxDoc.Save(RegisterFile.FullName);

                                            HtmlNode HtmldivInnerCustomFieldsNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divinnercustomfields");

                                            string divInnerCustomFieldsData = "<asp:Repeater ID='rptCustomFields' runat='server' OnItemDataBound='rptCustomFields_OnItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmldivInnerCustomFieldsNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivCustomFieldsNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divcustomfields");
                                            AspxdivCustomFieldsNode.InnerHtml = AspxdivCustomFieldsNode.InnerHtml.Replace(AspxdivCustomFieldsNode.InnerHtml, divInnerCustomFieldsData);
                                            AspxDoc.Save(RegisterFile.FullName);


                                            HtmlNode HtmldivInnerInvoiceAddressNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divinnerinvoiceaddress");

                                            string divInnerInvoiceAddressData = "<asp:Repeater ID='rptInvoiceAddress' runat='server' OnItemDataBound='rptAddress_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmldivInnerInvoiceAddressNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivInvoiceAddressNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divinvoiceaddress");
                                            AspxdivInvoiceAddressNode.InnerHtml = AspxdivInvoiceAddressNode.InnerHtml.Replace(AspxdivInvoiceAddressNode.InnerHtml, divInnerInvoiceAddressData);
                                            AspxDoc.Save(RegisterFile.FullName);

                                            HtmlNode HtmldivInnerDeliveryAddressNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divinnerdeliveryaddress");

                                            string divInnerDeliveryAddressData = "<asp:Repeater ID='rptDeliveryAddress' runat='server' OnItemDataBound='rptAddress_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmldivInnerDeliveryAddressNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivDeliveryAddressNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divdeliveryaddress");
                                            AspxdivDeliveryAddressNode.InnerHtml = AspxdivDeliveryAddressNode.InnerHtml.Replace(AspxdivDeliveryAddressNode.InnerHtml, divInnerDeliveryAddressData);
                                            AspxDoc.Save(RegisterFile.FullName);

                                            #endregion
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                break;
                            case "login":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace Order History page

                                DirectoryInfo LoginDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\Login");
                                FileInfo[] LoginFileInfo = LoginDirInfo.GetFiles();

                                foreach (FileInfo LoginFile in LoginFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(LoginFile.FullName).ToLower())
                                    {
                                        case "login":
                                            #region Replace Login page

                                            HtmlNode htmlLoginDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(LoginFile.FullName);
                                            HtmlNode AspxLoginyNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxLoginyNode.InnerHtml = AspxLoginyNode.InnerHtml.Replace(AspxLoginyNode.InnerHtml, htmlLoginDataNode.InnerHtml);
                                            AspxDoc.Save(LoginFile.FullName);

                                            HtmlNode HtmldivDynamicCurrencyNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divdynamiccurrency");

                                            string divDynamicCurrencyData = "<asp:Repeater ID='repaterCurrency' runat='server' OnItemDataBound='repaterCurrency_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmldivDynamicCurrencyNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivCurrencyAreaNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divcurrencyarea");
                                            AspxdivCurrencyAreaNode.InnerHtml = AspxdivCurrencyAreaNode.InnerHtml.Replace(AspxdivCurrencyAreaNode.InnerHtml, divDynamicCurrencyData);
                                            AspxDoc.Save(LoginFile.FullName);

                                            #endregion
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                break;
                            case "quickshop":

                                HtmlDoc.Load(HtmlFile.FullName);

                                #region Replace Order Item page

                                DirectoryInfo QuickShopDirInfo = new DirectoryInfo(strTemplatePathWithoutExtension + @"\QuickShop");
                                FileInfo[] QuickShopFileInfo = QuickShopDirInfo.GetFiles();

                                foreach (FileInfo QuickShopFile in QuickShopFileInfo)
                                {
                                    switch (Path.GetFileNameWithoutExtension(QuickShopFile.FullName).ToLower())
                                    {
                                        case "quickshop":
                                            #region Replace QuickShop page

                                            HtmlNode htmlQuickShopDataNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxDoc.Load(QuickShopFile.FullName);
                                            HtmlNode AspxQuickShopNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divmaincontent");
                                            AspxQuickShopNode.InnerHtml = AspxQuickShopNode.InnerHtml.Replace(AspxQuickShopNode.InnerHtml, htmlQuickShopDataNode.InnerHtml);
                                            AspxDoc.Save(QuickShopFile.FullName);

                                            HtmlNode HtmldivItemTemplateCategoryNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divitemtemplatecategory");

                                            string divItemTemplateCategoryData = "<asp:Repeater ID='rptCategory' runat='server' OnItemDataBound='rptCategory_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmldivItemTemplateCategoryNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivInnerCategoryNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divinnercategory");
                                            AspxdivInnerCategoryNode.InnerHtml = AspxdivInnerCategoryNode.InnerHtml.Replace(AspxdivInnerCategoryNode.InnerHtml, divItemTemplateCategoryData);
                                            AspxDoc.Save(QuickShopFile.FullName);


                                            HtmlNode HtmldivInnerParentCategoryNode = HtmlDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divinnerparentcategory");

                                            string HtmldivInnerParentCategoryNodeData = "<asp:Repeater ID='rptParentCategory' runat='server' OnItemDataBound='rptParentCategory_ItemDataBound'>" +
                                                                             "<ItemTemplate>" + HtmldivInnerParentCategoryNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxtblParentCategoryNode = AspxDoc.DocumentNode.Descendants("table").FirstOrDefault(x => x.Id.ToLower() == "tblparentcategory");
                                            AspxtblParentCategoryNode.InnerHtml = AspxtblParentCategoryNode.InnerHtml.Replace(AspxtblParentCategoryNode.InnerHtml, HtmldivInnerParentCategoryNodeData);
                                            AspxDoc.Save(QuickShopFile.FullName);


                                            HtmlNode HtmlHeaderTemplateNode = HtmlDoc.DocumentNode.Descendants("tr").FirstOrDefault(x => x.Id.ToLower() == "trproductsheader");
                                            HtmlNode HtmlItemTemplateNode = HtmlDoc.DocumentNode.Descendants("tr").FirstOrDefault(x => x.Id.ToLower() == "trproductsitem");

                                            string divProductsData = "<asp:Repeater ID='rptProducts' runat='server' OnItemDataBound='rptProducts_ItemDataBound'>" +
                                                                             "<HeaderTemplate>" + HtmlHeaderTemplateNode.OuterHtml +
                                                                             "</HeaderTemplate>" +
                                                                             "<ItemTemplate>" + HtmlItemTemplateNode.OuterHtml +
                                                                             "</ItemTemplate>" +
                                                                             "</asp:Repeater>";

                                            HtmlNode AspxdivProductsNode = AspxDoc.DocumentNode.Descendants("div").FirstOrDefault(x => x.Id.ToLower() == "divproducts");
                                            AspxdivProductsNode.InnerHtml = AspxdivProductsNode.InnerHtml.Replace(AspxdivProductsNode.InnerHtml, divProductsData);
                                            AspxDoc.Save(QuickShopFile.FullName);

                                            #endregion
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion

                                break;
                            default: break;
                        }
                    }

                    if (Convert.ToString(Session["TemplateMode"]) == "edit")
                    {
                        foreach (RepeaterItem item in rptTemplateStores.Items)
                        {
                            CheckBox chkStores = (CheckBox)item.FindControl("chkStores");
                            if (chkStores.Checked)
                            {
                                string strWebsiteHostPath = ConfigurationManager.AppSettings["WebsiteHostPath"];
                                GlobalFunctions.CopyDirectory(strTemplatePathWithoutExtension, strWebsiteHostPath.Trim().Replace("$StoreName$", chkStores.Text), true);
                            }
                        }
                    }

                    if (Convert.ToString(Session["TemplateMode"]) == "create")
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Template added successfully", Convert.ToString(ConfigurationManager.AppSettings["ImgURL"]) + "StoreCreator/ViewTemplate.aspx", AlertType.Success);
                    else if (Convert.ToString(Session["TemplateMode"]) == "edit")
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Template updated successfully", Convert.ToString(ConfigurationManager.AppSettings["ImgURL"]) + "StoreCreator/ViewTemplate.aspx", AlertType.Success);

                    Session["TemplateMode"] = null;
                    Session["TemplateId"] = null;
                    Session["TemplateName"] = null;
                }
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select file", AlertType.Warning);
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            if (Convert.ToString(Session["TemplateMode"]) == "create")
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error in adding template", AlertType.Failure);
            else if (Convert.ToString(Session["TemplateMode"]) == "edit")
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error in updating template", AlertType.Failure);
        }
        finally
        {
            //Session["TemplateMode"] = null;
            //Session["TemplateId"] = null;
            //Session["TemplateName"] = null;
        }
    }

    #endregion
}