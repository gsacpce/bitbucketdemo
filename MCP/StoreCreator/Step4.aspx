﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="Step4.aspx.cs" Inherits="StoreCreator_Step4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="dvStoreLoader" class="preloader" style="display: none;">
        <div class="loader" style="width: 300px;height:140px;">
            <p>
                <img width="46" height="46" alt="loader" src="../Images/UI/ajax-loader.gif">
            </p>
            <p>Store creation is in progress…<br />
                This might take upto 15 minutes</p>
        </div>
    </div>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Create Store</li>
            <li>Step 4</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <h3>Create Store</h3>
                    <p></p>
                    <div class="create_store">
                        <div class="length1"></div>
                        <ul>
                            <li>
                                <div class="wrapBox active">
                                    <span>1</span>
                                    <h4>Information</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox active">
                                    <span>2</span>
                                    <h4>Store Settings</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox  active">
                                    <span>3</span>
                                    <h4>Store Theme</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>4</span>
                                    <h4>Review &amp; Create</h4>
                                </div>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="create_pg4">
                        <div class="col-md-12  button_section">
                            <div class="col-md-6 col-sm-6 text-left">Review Store Details</div>
                            <div class="col-md-6 col-sm-6 text-right">
                                <asp:Button ID="cmdEditStore" runat="server" CssClass="btn" Text="EDIT" ValidationGroup="OnClickSubmit" OnClick="cmdEditStore_Click" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="box">
                            <div class="col-md-3 ">
                                <asp:Image ID="imgTemplate" runat="server" Width="238" Height="238" />
                                <asp:HiddenField ID="hdnTemplateName" runat="server" />
                            </div>
                            <div class="col-md-9">
                                <h6>Information</h6>

                                <div class="review01">
                                    <ul class="detail_title">
                                        <li>
                                            <div class="col-md-6 col-sm-6 lightgray">Corporate name:</div>
                                            <div class="col-md-6  col-sm-6 "><asp:Label ID="lblStoreName" runat="server"></asp:Label></div>
                                        </li>
                                        <li>
                                            <div class="col-md-6  col-sm-6 lightgray">Clone Store: </div>
                                            <div class="col-md-6  col-sm-6 "><asp:Label ID="lblCloneStore" runat="server"></asp:Label></div>
                                        </li>

                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                        </div>
                        <ul class="wrap_block">
                            <li>
                                <div class="col-md-12 no_padding">
                                    <div class="wrap_title">
                                        <div class="icon icon_bg"></div>
                                        <div class="title">Store Setting</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <h6><asp:Label ID="lblProductListingTypeHeading" runat="server"></asp:Label></h6>
                                        <ul class="detail_title">
                                            <asp:Repeater ID="rptDisplayType" runat="server" OnItemDataBound="rptDisplayType_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-6 col-sm-6 lightgray" ><asp:label ID="lblFeature" runat="server"></asp:label></div>
                                                        <div class="col-md-6  col-sm-6 "><asp:label ID="lblFeatureValue" runat="server"></asp:label></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray">Default view </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblDefaultView" runat="server"></asp:Label></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <h6><asp:Label ID="lblPaginationHeading" runat="server"></asp:Label></h6>
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">Pagination </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblPagination" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray " id="dvNoOfProductsPerPage" runat="server">Number of products per page </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblProductsPerPage" runat="server"></asp:Label></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <h6>Default Sorting</h6>
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><asp:Label ID="lblSortingHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblSorting" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray ">Product list default sorting </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblDefaultSorting" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray">Enable quick view: </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblQuickView" runat="server"></asp:Label></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <h6>Default Filter</h6>
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><asp:Label ID="lblFilterHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblFilter" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li style="display:none;">
                                                <div class="col-md-6  col-sm-6 lightgray">Product list default filter </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblDefaultFilter" runat="server"></asp:Label></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <h6>Default Sorting</h6>
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblPrintHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblPrint" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblEmailHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblDownloadImageHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblDownloadImage" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblSocialMediaHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblSocialMedia" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblRecentlyViewedProductsHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblRecentlyViewedProducts" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblYouMayAlsoLikeHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblYouMayAlsoLike" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li style="display:none;">
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblWishListHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblWishList" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblReviewRatingHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblReviewRating" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblSavePDFHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblSavePDF" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblSectionIconsHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblSectionIcons" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-12 no_padding">
                                    <div class="wrap_title">
                                        <div class="icon2 icon_bg"></div>
                                        <div class="title">Language / Currency</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <h6>Language</h6>
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">Available language</div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblLanguages" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray ">Default Language</div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblDefaultLanguage" runat="server"></asp:Label></div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <h6>Currency</h6>
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">Available currency</div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblCurrencies" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">Default currency</div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblDefaultCurrency" runat="server"></asp:Label></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                                <div class="col-md-12 no_padding">
                                    <div class="wrap_title">
                                        <div class="icon3 icon_bg"></div>
                                        <div class="title">Choose Payments</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">

                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblEnablePointsHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEnablePoints" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li id="liPointValue" runat="server">
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    Point value</div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblPointValue" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li id="liPaymentGateway" runat="server">
                                                <div class="col-md-6 col-sm-6 lightgray"><asp:Label id="lblPaymentGatewayHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label id="lblPaymentGateways" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblGiftCertificateHeading" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblGiftCertificate" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01" style="display:none;">

                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray"><b><asp:Label id="lblAlternativePaymentsHeading" runat="server"></asp:Label></b></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label id="lblAlternativePayments" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                                <div class="col-md-12 no_padding">
                                    <div class="wrap_title">
                                        <div class="icon3 icon_bg"></div>
                                        <div class="title">Orders and Checkout</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">

                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><asp:Label ID="lblInternationalOrdersHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblInternationalOrders" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray"><asp:Label ID="lblMessageHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblMessage" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray"><asp:Label ID="lblMOVHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblMOV" runat="server"></asp:Label></div>
                                            </li>
                                            <li style="display:none;">
                                                <div class="col-md-6  col-sm-6 lightgray"><asp:Label ID="lblApproveAllOrderHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblApproveAllOrder" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray"><asp:Label ID="lblAllowBackOrderHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblAllowBackOrder" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray">No Of Days For BackOrder</div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblNoOfDaysForBackOrder" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray">Punchout</div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblPunchout" runat="server"></asp:Label></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>




                                </div>
                            </li>
                        </ul>

                        <div class="clearfix"></div>
                        <ul class="wrap_block">
                            <li>
                                <div class="col-md-12 no_padding">
                                    <div class="wrap_title">
                                        <div class="icon4 icon_bg"></div>
                                        <div class="title">Customer Registration &amp; Account</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <%--<div class="review01">
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><b><asp:Label ID="lblCustomerTypeHeading" runat="server"></asp:Label></b></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblCustomerType" runat="server"></asp:Label>
                                                </div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01" id="dvEmailApproval" runat="server">
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><b> Email Approval</b></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblEmailApproval" runat="server"></asp:Label></div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01" style="display:none;">
                                        <h6>Email Notification</h6>
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">Approval email sent to </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblNotificationEmail" runat="server"></asp:Label></div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01" style="display:none;">
                                        <h6>Validation</h6>
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">Validate users against white list:</div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblWhiteListValidation" runat="server"></asp:Label></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><b><asp:Label ID="lblEnableDomainValidationHeading" runat="server"></asp:Label></b></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEnableDomainValidation" runat="server"></asp:Label>
                                                </div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><b><asp:Label ID="lblEnableEmailValidationHeading" runat="server"></asp:Label></b></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEnableEmailValidation" runat="server"></asp:Label>
                                                </div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><b><asp:Label ID="lblEnableSSOHeading" runat="server"></asp:Label></b></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEnableSSO" runat="server"></asp:Label>
                                                </div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><b><asp:Label ID="lblResetPasswordBlockAccountDurationHeading" runat="server"></asp:Label></b></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblResetPasswordBlockAccountDuration" runat="server"></asp:Label>
                                                </div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><b><asp:Label ID="lblStoreAccessHeading" runat="server"></asp:Label></b></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblStoreAccess" runat="server"></asp:Label>
                                                </div>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>--%>
                                    <div class="review01">
                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblCustomerTypeHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblCustomerType" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li id="dvEmailApproval" runat="server">
                                                <div class="col-md-6 col-sm-6 lightgray">Email Approval</div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEmailApproval" runat="server"></asp:Label></div>
                                            </li>
                                            <li style="display: none;">
                                                <div class="col-md-6 col-sm-6 lightgray">Approval email sent to </div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblNotificationEmail" runat="server"></asp:Label></div>
                                            </li>
                                            <li style="display: none;">
                                                <div class="col-md-6 col-sm-6 lightgray">Validate users against white list:</div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblWhiteListValidation" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblEnableDomainValidationHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEnableDomainValidation" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblEnableEmailValidationHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEnableEmailValidation" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblEnableSSOHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEnableSSO" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li style="display:none;">
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblResetPasswordBlockAccountDurationHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblResetPasswordBlockAccountDuration" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblStoreAccessHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblStoreAccess" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblStoreTypeHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblStoreType" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-12 no_padding">
                                    <div class="wrap_title">
                                        <div class="icon5 icon_bg"></div>
                                        <div class="title">Password Security</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">

                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><asp:Label ID="lblMinimumPasswordLengthHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblMinimumPasswordLength" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray">
                                                    Password must contain both<br>
                                                    letters and numbers:
                                                </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblPasswordExpression" runat="server"></asp:Label></div>
                                            </li>

                                            <li style="display:none;">
                                                <div class="col-md-6  col-sm-6 lightgray">
                                                    Force password to change on<br>
                                                    the first login:
                                                </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblForcePassword" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray">
                                                    Password validity period in<br>
                                                    days: (0 - unlimited)
                                                </div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblPasswordValidity" runat="server"></asp:Label></div>
                                            </li>
                                            <li style="display:none;">
                                                <div class="col-md-6 col-sm-6 lightgray">
                                                    <asp:Label ID="lblEnableCaptchaHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 ">
                                                    <asp:Label ID="lblEnableCaptcha" runat="server"></asp:Label>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>


                                </div>

                                <div class="col-md-12 no_padding">
                                    <div class="wrap_title">
                                        <div class="icon6 icon_bg"></div>
                                        <div class="title">Cookies Law (EU/UK)</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="review01">

                                        <ul class="detail_title">
                                            <li>
                                                <div class="col-md-6 col-sm-6 lightgray"><asp:Label ID="lblEnableCookieHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblEnableCookie" runat="server"></asp:Label></div>
                                            </li>
                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray"><asp:Label ID="lblCookieLifetimeHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblCookieLifetime" runat="server"></asp:Label></div>
                                            </li>

                                            <li>
                                                <div class="col-md-6  col-sm-6 lightgray"><asp:Label ID="lblCookieMessageHeading" runat="server"></asp:Label></div>
                                                <div class="col-md-6  col-sm-6 "><asp:Label ID="lblCookieMessage" runat="server"></asp:Label></div>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>


                                </div>



                            </li>
                        </ul>

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="cmdCreateStore" runat="server" CssClass="btn" Text="Create Store" ValidationGroup="OnClickSubmit" OnClientClick="javascript:$('#dvStoreLoader').show();" OnClick="cmdCreateStore_Click" />
                        </li>
                        <li>
                             <asp:Button ID="cmdCancel" runat="server" CssClass="btn  gray" Text="Cancel" ValidationGroup="OnClickSubmit" OnClick="cmdCancel_Click" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

