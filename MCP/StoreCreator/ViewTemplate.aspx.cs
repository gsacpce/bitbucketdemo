﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class StoreCreator_ViewTemplate : System.Web.UI.Page
{
    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized(HttpContext.Current.Request.Url.Segments[HttpContext.Current.Request.Url.Segments.Length - 1]);
        if (!IsPostBack)
        {
            BindAllTemplates();
        }
    }

    #endregion

    #region Get All Templates

    private void BindAllTemplates()
    {
        try
        {
            List<TemplateBE> TemplateCollection = TemplateBL.GetAllTemplateDetails();
            gvAllTemplates.DataSource = TemplateCollection;
            gvAllTemplates.DataBind();

        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Sorting/Paging Events

    protected void gvAllTemplates_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvAllTemplates.PageIndex = e.NewPageIndex;
            if (Session["TemplateSortedView"] != null)
            {
                gvAllTemplates.DataSource = (List<StoreBE>)Session["TemplateSortedView"];
                gvAllTemplates.DataBind();
            }
            else
            {
                BindAllTemplates();
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void gvAllTemplates_DataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TemplateBE template = (TemplateBE)e.Row.DataItem;

                LinkButton lnkUpload = (LinkButton)e.Row.FindControl("lnkUpload");
                LinkButton lnkDownload = (LinkButton)e.Row.FindControl("lnkDownload");

                lnkUpload.CommandArgument = template.TemplateId + "+" + template.TemplateName;
                lnkDownload.CommandArgument = GlobalFunctions.GetPhysicalFolderPath() + @"\Templates\" + template.TemplateName + ".zip";
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void gvAllTemplates_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            if (direction == SortDirection.Ascending)
                direction = SortDirection.Descending;
            else
                direction = SortDirection.Ascending;

            List<TemplateBE> TemplateCollection = TemplateBL.GetAllTemplateDetails();

            switch (direction)
            {
                case SortDirection.Ascending:
                    switch (e.SortExpression.ToLower())
                    {
                        case "storename": Session["TemplateSortedView"] = TemplateCollection.OrderBy(x => x.TemplateName).ToList();
                            break;
                        case "createddate": Session["TemplateSortedView"] = TemplateCollection.OrderBy(x => x.CreatedDate).ToList();
                            break;
                        default: Session["TemplateSortedView"] = TemplateCollection.OrderBy(x => x.TemplateName).ToList();
                            break;
                    }
                    break;
                case SortDirection.Descending:
                    switch (e.SortExpression.ToLower())
                    {
                        case "storename": Session["TemplateSortedView"] = TemplateCollection.OrderByDescending(x => x.TemplateName).ToList();
                            break;
                        case "createddate": Session["TemplateSortedView"] = TemplateCollection.OrderByDescending(x => x.CreatedDate).ToList();
                            break;
                        default: Session["TemplateSortedView"] = TemplateCollection.OrderByDescending(x => x.TemplateName).ToList();
                            break;
                    }
                    break;
                default:
                    break;
            }

            gvAllTemplates.DataSource = (List<TemplateBE>)Session["TemplateSortedView"];
            gvAllTemplates.DataBind();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    public SortDirection direction
    {
        get
        {
            if (ViewState["directionState"] == null)
            {
                ViewState["directionState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["directionState"];
        }
        set
        {
            ViewState["directionState"] = value;
        }
    }

    protected void gvAllTemplates_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.ToLower() == "downloadtemplate")
            {
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                string[] ZipFilepath = Convert.ToString(e.CommandArgument).Split('\\');
                response.AddHeader("Content-Disposition", "attachment;filename=\"" + ZipFilepath[ZipFilepath.Length - 1] + "\"");
                byte[] data = req.DownloadData(Convert.ToString(e.CommandArgument));
                response.BinaryWrite(data);
                response.End();
            }
            else if (e.CommandName.ToLower() == "uploadtemplate")
            {
                Session["TemplateMode"] = "edit";
                string[] templateSplit = Convert.ToString(e.CommandArgument).Split('+');
                Session["TemplateId"] = templateSplit[0];
                Session["TemplateName"] = templateSplit[1];
                Response.Redirect("~/StoreCreator/AddEditTemplate.aspx");
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion
}