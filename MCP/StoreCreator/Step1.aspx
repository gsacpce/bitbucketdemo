﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Step1.aspx.cs" Inherits="StoreCreator_Step1" MasterPageFile="~/Master/MCPMaster.master" %>


<asp:Content ID="step1Content" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <link href="../CSS/radio.css" rel="stylesheet" />

    <script src="../JS/StoreCreator.js"></script>




    <script type="text/javascript">
        function Cancel() {

            var IsConfirmed = confirm("Do you want to uncheck this currency. If yes, click OK, else fill the mandatory details and click save.")
            if (IsConfirmed) {
                $('.BASYSPopup').modal('hide');
                return true;
            }
            else {
                return false;
            }
        }



        function ShowModalAddCatalogue() {


            $('.BASYSPopup').modal('show');

        }















    </script>

    <script type="text/javascript">

























        $(function () {
            debugger;
            $('#dvCurrencies input[type="checkbox"]').click(function () {
                var value = $(this).parent().next().html();
                $('#<%=hdnCurCurrencyId.ClientID%>').val(value);
                //alert($('#<%=hdnCurCurrencyId.ClientID%>').val());
                $('.BASYSPopup input[type="text"]').attr('value', '');





                if (document.getElementById("rblStoreType_0").checked) {
                    if ($(this).is(":checked") == true) {
                        $('.BASYSPopup').modal('show');
                    }
                }
            });
        });















































    </script>



    <div id="dvStoreLoader" class="preloader" style="display: none;">
        <div class="loader" style="width: 300px; height: 140px;">
            <p>
                <img width="46" height="46" alt="loader" src="../Images/UI/ajax-loader.gif">
            </p>
            <p>
                Store creation is in progress…<br />
                This might take upto 15 minutes
            </p>
        </div>
    </div>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Create Store</li>
            <li>Step 1</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <h3>Create Store</h3>
                    <p></p>
                    <div class="create_store">
                        <div class="length"></div>
                        <ul>
                            <li>
                                <div class="wrapBox">
                                    <span>1</span>
                                    <h4>Information</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>2</span>
                                    <h4>Store Settings</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>3</span>
                                    <h4>Store Theme</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>4</span>
                                    <h4>Review & Create</h4>
                                </div>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="select_data">
                        <div class="row">
                            <div class="col-md-2">
                                <h6>Enter Store Name:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="">
                                    <asp:TextBox ID="txtStoreName" runat="server" MaxLength="100" ClientIDMode="Static" AutoCompleteType="Disabled"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvStoreName" runat="server" ControlToValidate="txtStoreName"
                                        Display="dynamic" Text="(Required)" ErrorMessage="Please enter store name" ForeColor="Red"
                                        ValidationGroup="OnClickSubmit">
                                    </asp:RequiredFieldValidator>
                                    <%--<asp:label ID="lblURL" runat="server" ClientIDMode="Static" Text=""></asp:label>--%>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h6>Store URL:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="">
                                    <asp:Label ID="lblURL" runat="server" ClientIDMode="Static" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">
                                <h6>Description:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="">
                                    <asp:TextBox ID="txtStoreDescription" runat="server" MaxLength="100" TextMode="MultiLine" Rows="3" Columns="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">
                                <h6>Select Store Type:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="radio_data radio radio-danger">
                                    <asp:RadioButtonList ID="rblStoreType" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                        <asp:ListItem Text="BASYS" Selected="True" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="NON-BASYS" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">
                                <h6>Clone Store:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="select-style selectpicker">
                                    <asp:DropDownList ID="ddlCloneStores" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlCloneStores_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">
                                <h6>Select Language:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang" id="dvLanguages" clientidmode="static">
                                    <ul>
                                        <asp:Repeater ID="rptLanguages" runat="server" OnItemDataBound="rptLanguages_ItemDataBound">
                                            <HeaderTemplate>
                                                <li>
                                                    <div class="col-md-3">Language    </div>
                                                    <div class="col-md-2 text-center">Is Default </div>
                                                </li>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <div class="col-md-3">
                                                        <asp:CheckBox ID="chkLanguage" CssClass="chkShow" runat="server" Text='<%#Eval("LanguageName") %>' value='<%#Eval("LanguageId") %>' />
                                                        <asp:HiddenField ID="hdnLanguage" runat="server" />
                                                    </div>
                                                    <div id="dvDefaultLanguage" runat="server" clientidmode="static" class="col-md-2 text-center radio radio-danger radio-storeType">
                                                        <input id="rbDefaultLanguage" runat="server" type="radio" name="DefaultLanguage" clientidmode="static" />
                                                        <label></label>
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <asp:CustomValidator ID="cvLanguages" runat="server" Text="(Required)" ErrorMessage="Please select at least one Language"
                                    ClientValidationFunction="ValidateLanguage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">
                                <h6>Select Currency:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang second" id="dvCurrencies" clientidmode="static">
                                    <ul>
                                        <asp:Repeater ID="rptCurrencies" runat="server" OnItemDataBound="rptCurrencies_ItemDataBound">
                                            <HeaderTemplate>
                                                <li>
                                                    <div class="col_first">Currency    </div>
                                                    <div class="col_second text-center col_default">Is Default </div>
                                                    <div id="dvBASYSDetailHeading" class="col_three BASYSHeading" style="display: none;">
                                                        <table>
                                                            <tr>
                                                                <td>View/Edit</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </li>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <div class="col_first">
                                                        <asp:CheckBox ID="chkCurrency" CssClass="chkShowCurrency" runat="server" Text='<%#Eval("CurrencyName") %>'  />
                                                        <asp:Label ID="lblCurrcurrencyId" runat="server" Text='<%#Eval("CurrencyId") %>' CssClass="vals" Style="display: none;"></asp:Label>
                                                    </div>
                                                    <div id="dvDefaultCurrency" runat="server" clientidmode="static" class="col_second text-center radio radio-danger radio-storeType">
                                                        <input id="rbDefaultCurrency" runat="server" type="radio" name="DefaultCurrency" clientidmode="static" />
                                                        <label></label>

                                                    </div>
                                                    <div id="dvBASYSDetails" runat="server" clientidmode="static" class="radio-storeType col_three" style="display: none;">
                                                        <asp:LinkButton ID="cmdEdit" runat="server" Text="View/Edit" CommandArgument='<%#Eval("CurrencyId") %>' class="btn btn-default BASYSDetail" OnClick="cmdEdit_Click"></asp:LinkButton>
                                                        <%-- <a id="cmdEdit" data-toggle="modal"  value="View/Edit" name='<%#Eval("CurrencyId") %>' class="btn btn-default BASYSDetail">View/Edit</a>--%>
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:HiddenField ID="hdnCurCurrencyId" runat="server" />
                                        <asp:HiddenField ID="hdnDefaultCurrencyId" runat="server" />
                                    </ul>
                                    <div class="clearfix"></div>
                                    <div id="BASYSPopup" runat="server" class="modal fade BASYSPopup" role="dialog" data-backdrop="static" data-keyword="false">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Catalogue Details</h4>
                                                </div>
                                                <div class="modal-body cat-details">
                                                    <table>

                                                        <tr>
                                                            <td>Division name</td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlDivisionName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDivisionName_SelectedIndexChanged"></asp:DropDownList></td>

                                                        </tr>
                                                        <tr>
                                                            <td>Division id<span class="redColor">*</span></td>
                                                            <td>
                                                                <asp:TextBox ID="txtDivisionId" class="span5" MaxLength="10" runat="server" Enabled="false" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvDivisionId" CssClass="rfvDivisionId" runat="server" ControlToValidate="txtDivisionId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                </asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                       












                                                        <tr>
                                                            <td>Catalogue name</td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlCatalogueName" runat="server" OnSelectedIndexChanged="ddlCatalogueName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                                                            <tr>
                                                                <td>Catalogue id <span class="redColor">*</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCatalogueId" AutoCompleteType="Disabled" MaxLength="100" runat="server" Enabled="false" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvCatalogueId" CssClass="rfvCatalogueId" runat="server" ControlToValidate="txtCatalogueId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>SourceCode name</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSourceCodeName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSourceCodeName_SelectedIndexChanged"></asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td>SourceCode id<span class="redColor">*</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSourceCodeId" MaxLength="100" class="span5" runat="server" AutoCompleteType="Disabled" Enabled="false" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvSourceCodeId" CssClass="rfvSourceCodeId" runat="server" ControlToValidate="txtSourceCodeId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Default customer name</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlDefaultCustomerName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDefaultCustomerName_SelectedIndexChanged"></asp:DropDownList>
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <td>Default customer id<span class="redColor">*</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDefaultCustomerId" MaxLength="10" class="span5" runat="server" AutoCompleteType="Disabled" Enabled="false" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvDefaultCustomerId" CssClass="rfvDefaultCustomerId" runat="server" ControlToValidate="txtDefaultCustomerId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Sales person id<span class="redColor">*</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSalesPersonId" MaxLength="10" class="span5" runat="server" Enabled="false" AutoCompleteType="Disabled"  onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvSalesPersonId" CssClass="rfvSalesPersonId" runat="server" ControlToValidate="txtSalesPersonId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr style="display:none">
                                                                <td>Group id<span class="redColor">*</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtGroupId" class="span5" MaxLength="10" runat="server" AutoCompleteType="Disabled"  onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvGroupId" CssClass="rfvGroupId" runat="server" ControlToValidate="txtGroupId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Default customer contact id<span class="redColor">*</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDefault_CUST_CONTACT_ID" MaxLength="10" class="span5" runat="server" Enabled="false" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvtxtDefault_CUST_CONTACT_ID" CssClass="rfvDefaultCompanyId" runat="server" ControlToValidate="txtDefault_CUST_CONTACT_ID" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>

                                                            <tr style="display:none">
                                                                <td>Global cost centre number</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtGlobalCostCentreNumber" class="span5" MaxLength="80" AutoCompleteType="Disabled" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox></td>
                                                            </tr>



                                                            <tr style="display: none;">
                                                                <td>Catalogue alias id<span class="redColor">*</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCatalogueAliasId" MaxLength="50" class="span5" runat="server" AutoCompleteType="Disabled" Enabled="false" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvCatalogueAliasId" CssClass="rfvCatalogueAliasId" runat="server" ControlToValidate="txtCatalogueAliasId" Enabled="false" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr style="display:none">
                                                                <td>Default company id<span class="redColor">*</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDefaultCompanyId" MaxLength="10" class="span5" runat="server" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvDefaultCompanyId" CssClass="rfvDefaultCompanyId" runat="server" ControlToValidate="txtDefaultCompanyId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                             <tr>
                                                            <td>KeyGroup name</td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlKeyGroupName" runat="server" OnSelectedIndexChanged="ddlKeyGroupName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td>KeyGroup id<span class="redColor">*</span></td>
                                                            <td>
                                                                <asp:TextBox ID="txtKeyGroupId" MaxLength="100" class="span5" runat="server" Enabled="false" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvKeyGroupId" CssClass="rfvKeyGroupId" runat="server" ControlToValidate="txtKeyGroupId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                                </asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="modal-footer button_section">
                                                    <asp:Button type="button" runat="server" class="btn BASYSSave" Text="Save" ID="cmdSave" OnClick="btnAddcurrencydetail_Click" ValidationGroup="OnClickSave"></asp:Button>
                                                    <%--<button type="button" class="btn BASYSCancel" >Cancel</button>--%>
                                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="return Cancel();" OnClick="btnCancel_Click"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:CustomValidator ID="cvCurrencies" runat="server" Text="(Required)" ErrorMessage="Please select at least one currency"
                                    ClientValidationFunction="ValidateCurrency" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <ul id="ulSaveDetails" runat="server" class="button_section">
                        <li>
                            <asp:Button ID="cmdSaveClose" runat="server" CssClass="btn" Text="Save & Close" ValidationGroup="OnClickSubmit" CommandName="SaveClose" OnClick="cmdSave_Click" />
                        </li>
                        <li>
                            <asp:Button ID="cmdSaveContinue" runat="server" CssClass="btn" Text="Save & Continue" ValidationGroup="OnClickSubmit" CommandName="SaveContinue" OnClick="cmdSave_Click" />
                        </li>
                        <li>
                            <asp:Button ID="cmdReset" runat="server" CssClass="btn gray" Text="Reset" OnClick="cmdReset_Click" />
                        </li>
                    </ul>
                    <ul id="ulCreateStore" runat="server" class="button_section" style="display: none;">
                        <li>
                            <asp:Button ID="cmdCreateStore" runat="server" CssClass="btn" Text="Create Store" ValidationGroup="OnClickSubmit" CommandName="SaveClose" OnClientClick="javascript:if(Page_ClientValidate('OnClickSubmit')) { $('#dvStoreLoader').show();return true;} else{return false;}" OnClick="cmdCreateStore_Click" />
                        </li>
                        <li>
                            <asp:Button ID="cmdCancel" runat="server" CssClass="btn  gray" Text="Cancel" ValidationGroup="OnClickSubmit" OnClick="cmdCancel_Click" />
                        </li>
                    </ul>
                    <asp:ValidationSummary ID="valsumStore" runat="server" CssClass="ErrorText"
                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="OnClickSubmit" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

