﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="ViewStore.aspx.cs" Inherits="StoreCreator_ViewStore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../CSS/radio.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .select_data .wrap_lang {overflow: hidden;}
        .select_data h6 {font-size: 14px !important;line-height: 14px !important; }
    </style>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Stores</li>
            <li>View Store</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <h3>View Store</h3>
                    <p style="text-align: right">Total Stores:<asp:Label ID="lblTotal" runat="server"></asp:Label></p>
                    <div>
                        <asp:Label ID="lblSearchStore" runat="server" Text="Search Store By Name"></asp:Label>
                        <asp:TextBox ID="txtSearchStore" runat="server"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                        <asp:Label ID="Label1" runat="server" Text="No Store Found With This Name"></asp:Label>
                    </div>
                    <div class="table-responsive table_store">
                        <asp:GridView ID="gvAllStores" runat="server" AutoGenerateColumns="false" CssClass="table table-pagination" OnRowDataBound="gvAllStores_DataBound" data-pagination="true"
                            OnPageIndexChanging="gvAllStores_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" PagerSettings-PageButtonCount="2" PagerSettings-Position="TopAndBottom"
                            AllowSorting="true" OnSorting="gvAllStores_Sorting" OnRowCommand="gvAllStores_RowCommand" PagerStyle-CssClass="abc">
                            <Columns>
                                <asp:TemplateField HeaderText="Store Name" SortExpression="StoreName" HeaderStyle-CssClass="order_data">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStoreName" runat="server" Text='<% #Eval("StoreAlias")%>'></asp:Label>
                                        <asp:HiddenField ID="hdnStoreId" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date Created" SortExpression="CreatedDate" HeaderStyle-CssClass="order_data">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStoreCreatedDate" runat="server" Text='<% #Eval("CreatedDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Default Language">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDefaultLanguage" runat="server"></asp:Label>
                                        <asp:Button ID="cmdLanguageCount" runat="server" data-placement="right" data-html="true" data-popover="true" class="btn btn-primary btn-large showLang"></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Default Currency">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDefaultCurrency" runat="server"></asp:Label>
                                        <asp:Button ID="cmdCurrencyCount" runat="server" data-placement="right" data-html="true" data-popover="true" class="btn btn-primary btn-large showLang"></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStoreStatus" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Created By">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Version" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVersion" runat="server" Text='<% #Eval("Version")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Store Features">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkStoreFeature" runat="server" OnClick="lnkStoreFeature_Click" CommandArgument='<% #Eval("StoreId")%>'>Store Features</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton2" runat="server">Comments</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <div class="action">
                                            <a class="icon" href="#"></a>
                                            <div class="action_hover">
                                                <span></span>
                                                <div class="other_option">
                                                    <%--<a id="lnkStoreDetail" runat="server">Store Detail</a>--%>
                                                    <asp:LinkButton ID="lnkStoreDetail" runat="server" Text="Store Detail" CommandName="StoreDetail"></asp:LinkButton>
                                                    <a id="lnkStoreAdmin" runat="server">Store Admin</a>
                                                    <a id="lnkStoreFrontEnd" runat="server">Store FrontEnd</a>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DTS">
                                    <ItemTemplate>
                                        <div class="action" id="dvRunDTS" runat="server" clientidmode="static">
                                            <asp:ImageButton ID="cmdRunDTS" runat="server" Height="20" Width="20" BorderStyle="None" ImageUrl="~/Images/UI/runicon.png" CommandName="RunDTS" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            var originalLeave = $.fn.popover.Constructor.prototype.leave;
            $.fn.popover.Constructor.prototype.leave = function (obj) {
                var self = obj instanceof this.constructor ?
                    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
                var container, timeout;

                originalLeave.call(this, obj);

                if (obj.currentTarget) {
                    container = $(obj.currentTarget).siblings('.popover')
                    timeout = self.timeout;
                    container.one('mouseenter', function () {
                        //We entered the actual popover – call off the dogs
                        clearTimeout(timeout);
                        //Let's monitor popover content instead
                        container.one('mouseleave', function () {
                            $.fn.popover.Constructor.prototype.leave.call(self, self);
                        });
                    })
                }
            };

            $('body').popover({ selector: '[data-popover]', trigger: 'click hover', placement: 'right', delay: { show: 50, hide: 400 } });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;


            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;

            });
        });
    </script>

    <div class="modal fade" id="ModalTextRegion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog admin_page">
            <div class="modal-content customModal">
                <div class="wrap_container">
                    <div class="content">
                        <div class="customModal modal-header customPanel">
                            <button type="button" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title pageSubSubTitle mainHead" id="myModalLabelUpdate">Store Features</h3>
                        </div>
                        <div class="modal-body select_data">
                            <div class="row details form-group">
                                <div class="col-md-2">
                                    <h6>Search Settings</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="wrap_lang create02 no_borderbtm">
                                        <div class="wrap_creat02">
                                            <div class="col-md-6 col-sm-6">
                                                <h5>Search Settings :</h5>
                                            </div>
                                            <div class="col-md-6 col-sm-6 num_size ">
                                                Is Active:<asp:Label runat="server" ID="LblSearchSettings"></asp:Label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <h5>Product Listing Type:</h5>
                                            <ul>
                                                
                                                <asp:Repeater ID="rptPLDisplayType" runat="server" OnItemDataBound="rptPLDisplayType_ItemDataBound">
                                                    <ItemTemplate>
                                                        <li>
                                                            <div class="col-md-6">
                                                                <asp:Label ID="lblIsActivePL" runat="server"></asp:Label><br />
                                                                <asp:HiddenField ID="hdnPLDisplayTypeStoreFeatureDetailId" runat="server" />

                                                            </div>
                                                            <div class="col-md-2 text-center">Active</div>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li>
                                                     <div class="col-md-6"><asp:Label ID="lblIsDefaultPL" runat="server"></asp:Label></div>
                                                    <div class="col-md-2 text-center">Default:</div>
                                                </li>
                                                
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        
                                        <div class="wrap_creat02">
                                            <h5>Product Pagination:</h5>
                                            <div class="col-md-6">
                                                <asp:Label ID="lblPagination" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-2 text-center">Active</div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <h5>Default Settings:</h5>
                                            <ul>
                                               <li>
                                                   <div class="col-md-6"> 
                                                <asp:Repeater ID="rptPLSorting" runat="server" OnItemDataBound="rptPLSorting_ItemDataBound">
                                                    <ItemTemplate>
                                                                                                          
                                                                <asp:Label ID="lblDefaultSettings" runat="server"></asp:Label>
                                                                <asp:HiddenField ID="hdnPLSortingStoreFeatureDetailId" runat="server" />

                                                            
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                       </div>
                                                <div class="col-md-2 text-center">Active</div>
                                               </li>
                                                <asp:Label ID="LblQuickWatch" runat="server" Text="Label"></asp:Label>
                                                <li>
                                                     <div class="col-md-6">
                                                          <asp:Label ID="lblDefaultSettingsIsDefault" runat="server"></asp:Label>
                                                          <asp:Label ID="lblProductComp" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-md-2 text-center">Default</div>
                                                </li>
                                            </ul>
                                           
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <h5>Filters</h5>
                                            <asp:Repeater ID="rptPLFilters" runat="server" OnItemDataBound="rptPLFilters_ItemDataBound">
                                                <ItemTemplate>
                                                    <li id="liFilter" runat="server">
                                                        <div class="col-md-6">
                                                            <asp:Label ID="chkPLFilters" runat="server"></asp:Label>
                                                            <asp:HiddenField ID="hdnPLFilterStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <h5>Product Details Settings:</h5>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:HiddenField ID="hdnPDPrintStoreFeatureDetailId" runat="server" />
                                                <asp:Label ID="chkPDPrint" runat="server"></asp:Label>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:HiddenField ID="hdnPDDownloadImageStoreFeatureDetailId" runat="server" />
                                                <asp:Label ID="chkPDDownloadImage" runat="server"></asp:Label>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkPDRecentlyViewedProducts" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnPDRecentlyViewedProductsStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkPDWishlist" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnPDWishlistStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkPDSavePDF" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnPDSavePDFStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:HiddenField ID="hdnPDEmailStoreFeatureDetailId" runat="server" />
                                                <asp:Label ID="chkPDEmail" runat="server" TextAlign="Left" Enabled="false"></asp:Label>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkPDSocialMedia" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnPDSocialMediaStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkPDYouMayAlsoLike" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnPDYouMayAlsoLikeStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkPDReviewRating" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnPDReviewRatingStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkPDSectionIcons" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnPDSectionIconsStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <div class="col-md-6 col-sm-6"><span>Store Settings</span></div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkEnablePoints" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnEnablePoints" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <div class="col-md-6 col-sm-6"><span>Home Link:</span></div>
                                            <div class="col-md-6 col-sm-6 num_size " id="Div5" runat="server">
                                                <div class="radio_data radio-danger">
                                                    <asp:Label ID="rdoLinkPosition" runat="server">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <br />
                                            <ul>
                                                <li>
                                                    <div class="col-md-6">Payment Gateway:</div>
                                                    <div class="col-md-2 text-left">Default </div>
                                                </li>
                                                <asp:Repeater ID="rptSCPaymentGateway" runat="server" OnItemDataBound="rptSCPaymentGateway_ItemDataBound">
                                                    <ItemTemplate>
                                                        <li>
                                                            <div class="col-md-6">
                                                                <asp:Label ID="chkSCPaymentGateway" CssClass="chkSCPaymentGateway marright_label2" runat="server"></asp:Label>
                                                                <asp:HiddenField ID="hdnSCPaymentGatewayStoreFeatureDetailId" runat="server" />
                                                            </div>
                                                            <div id="dvSCPaymentGateway" runat="server" clientidmode="static" class="col-md-1 text-center radio radio-danger radio-storeType">
                                                            </div>
                                                            <div class="col-md-3 text-left" id="dvCheckBoxListCurrency" runat="server" clientidmode="Static">
                                                                <asp:ListBox ID="ListBox1" runat="server"></asp:ListBox>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                         </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                               
                                                <li>
                                                     <div class="col-md-6">
                                                         <asp:Label ID="rbSCPaymentGateway" runat="server"></asp:Label>
                                                    </div>

                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        
                                        <div class="col-md-6 col-sm-6">
                                            <asp:Label ID="chkEnableCouponCode" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hdnEnableCouponCode" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 col-sm-6 marright_label1 marginLeft">
                                            <asp:Label ID="chkIsPunchoutRegister" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hdnIsPunchoutReg" runat="server" />

                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="col-md-12 col-sm-6 marright_label1 marginLeft">

                                            <asp:Label ID="chkPunchout" runat="server" Text="Punchout = Enabled"></asp:Label>
                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:Label ID="chkMaxAllowQty" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hdnMaxOrderQtyStoreFeatureDetailId" runat="server" />

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div7" runat="server">
                                            <asp:Label runat="server" ID="lblMaxOrder" Text="Max Allowable Order Qty / Item:" Visible="false"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator1" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter no. products per page"
                                                ClientValidationFunction="ValidateProductsPerPage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                        </div>
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="chkAllowBackOrder" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnODAllowBackOrderStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:Label ID="ChkInvoiceAccountStatus" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hdnInvoiceAccountStatus" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="ChkEmailCopyBasketBtn" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnEmailCopyBasketBtn" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:Label ID="ChkProductEnquiryColor" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnProductEnquiryColor" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                      </div>  
                                   </div>
                                 </div>
                                 <div class="create02 setting_newmargin">
                                        <div class="col-md-2">
                                            <h6>User Settings:</h6>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="wrap_creat02 wrap_lang store_access">
                                                <div class="col-md-6 col-sm-6">
                                                    <span>Store Access:</span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 num_size " id="Div1" runat="server">
                                                     <div class="radio_data radio radio-danger">
                                                        <asp:Label ID="rblCRStoreAccess" runat="server">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                             </div>
                                                <div class="wrap_creat02 wrap_lang store_access">
                                                    <div class="col-md-6 col-sm-6">
                                                        <asp:Label ID="chkCREnableDomainValidation" runat="server" TextAlign="Left" ClientIDMode="Static"></asp:Label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                         </div>
                                </div>
                                 <div class="create02 setting_newmargin">
                                        <div class="col-md-2">
                                            <h6>Password Rules:</h6>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="wrap_creat02 wrap_lang store_access">
                                                <div class="col-md-6 col-sm-6">
                                                    <span>Minimum password length</span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 num_size">
                                                    <asp:Label ID="txtPSMinimumPasswordLength" runat="server" onkeyup="keyUP(event.keyCode)"                              onkeydown="return isNumeric(event.keyCode);"></asp:Label>
                                                </div>
                                                <div class="clearfix"></div>
                                        </div>
                                            <div class="wrap_lang create02 store_access">
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="lblPSPasswordPolicy" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="rdPasswordPolicyType" runat="server">
                                                <asp:ListItem Text="Any Combination"></asp:ListItem>
                                                <asp:ListItem Text="Must contain Alpha Numeric"></asp:ListItem>
                                                <asp:ListItem Text="Must contain Alpha Numeric and Symbol"></asp:ListItem>
                                                    </asp:Label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                  </div>  
                                 <div class="create02 setting_newmargin">
                                     <div class="col-md-2">
                                         <h6>Store Type:</h6>
                                     </div>       
                                     <div class="col-md-10">
                                         <div class="wrap_creat02 wrap_lang store_access">
                                             <div class="col-md-6 col-sm-6">
                                                  <span>Store Type:</span>
                                            </div>       
                                            <div class="col-md-3 col-sm-3 num_size " id="Div2" runat="server">
                                                <div class="radio_data radio radio-danger">
                                                    <asp:Label ID="rblCRStoreType" runat="server" ClientIDMode="Static">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                           
                                        </div>
                                     </div>
                                </div>
                                <div class="create02 setting_newmargin">
                                     <div class="col-md-2">
                                            <h6>Gift Certificate</h6>
                                     </div>
                                      <div class="col-md-10">
                                        <div class="wrap_creat02 wrap_lang store_access">
                                            <div class="col-md-6 col-sm-6">
                                                <span>Gift Certificate:</span>
                                            </div>
                                            <div class="col-md-3 col-sm-3 num_size " id="Div3" runat="server">
                                                <div class="radio_data radio radio-danger">
                                                    <asp:Label ID="rdlIsGiftCertificateAllow" runat="server" ClientIDMode="Static">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                          
                                                <div class="clearfix"></div>
                                            </div>
                                       
                                     </div>
                                 </div> 
                                 <div class="create02 setting_newmargin">
                                     <div class="col-md-2">
                                            <h6>Cookies Law (EU/UK):</h6>
                                     </div>
                                      <div class="col-md-10">
                                        <div class="wrap_creat02 wrap_lang store_access">
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="lblCLEnableCookie" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="chkCLEnableCookie" runat="server" ClientIDMode="Static"></asp:Label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                       
                                     </div>
                                 </div>  
                          </div>
                             <div class="row button_section text-center" style="margin-top: 10px;">
                                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" ClientIDMode="Static" CausesValidation="false" Text="OK" />
                                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>       
    <script type="text/javascript">


        function ShowModal() {
            $('#ModalTextRegion').modal('show')
        }
    </script>
    <style>
        .modal-dialog {width: 790px;}
        .create02 .col-md-6  span{width:100%;}
        .wrap_lang ul li:first-child {margin-bottom: 1px;}
        .wrap_lang .create02 .no_borderbtm, body{font-size:13px !important;}
        .wrap_creat02 {padding: 5px 0 6px 0 !important;}
    </style>

</asp:Content>

