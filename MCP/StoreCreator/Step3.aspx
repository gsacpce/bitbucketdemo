﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="Step3.aspx.cs" Inherits="StoreCreator_Step3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../JS/StoreCreator.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Create Store</li>
            <li>Step 3</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <h3>Create Store</h3>
                    <p></p>
                    <div class="create_store">
                        <div class="length3"></div>
                        <ul>
                            <li>
                                <div class="wrapBox active">
                                    <span>1</span>
                                    <h4>Information</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox active">
                                    <span>2</span>
                                    <h4>Store Settings</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>3</span>
                                    <h4>Store Theme</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>4</span>
                                    <h4>Review &amp; Create</h4>
                                </div>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 create_pg3">
                        <div class="button_section">
                            <h5>Select Template  <span></span></h5>
                            <ul class="button_section">
                                <asp:Button ID="cmdAddTemplate" runat="server" CssClass="btn" Text="Add Template" OnClick="cmdAddTemplate_Click" />
                            </ul>
                        </div>
                        <div class="col-md-12 well">
                            <div class="col-md-6 col-sm-6">
                                <div class="wrap_well"><asp:Literal ID="lblTemplateCount" runat="server"></asp:Literal></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <ul class="themes">
                            <asp:Repeater ID="rptTemplates" runat="server" OnItemDataBound="rptTemplates_ItemDataBound">
                                <ItemTemplate>
                                    <li>
                                        <div class="photo text-center">
                                            <asp:Image ID="imgTemplate" runat="server" Width="238" Height="238" />
                                            <div class="overlay">
                                                <a id="lnkLiveDemo" runat="server" href="#">Live Demo</a>
                                            </div>
                                        </div>
                                        <div class="details">
                                            <div id="dvDefaultTemplate" runat="server" clientidmode="static" class="radio_data  radio radio-danger">
                                                <input id="rdbSelectTemplate" runat="server" type="radio" name="SelectTemplate" checked="true" clientidmode="static" />
                                                <asp:HiddenField ID="hdnTemplateId" runat="server" />
                                                <asp:HiddenField ID="hdnTemplateName" runat="server" />
                                                <asp:Label ID="lblSelectTemplateText" runat="server"></asp:Label>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:CustomValidator ID="cvSelectTemplate" runat="server" Text="(Required)" ErrorMessage="Please select template"
                                ClientValidationFunction="ValidateTemplate" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                        </ul>
                    </div>
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="cmdSaveTemplate" runat="server" CssClass="btn" Text="NEXT STEP" OnClick="cmdSaveTemplate_Click" ValidationGroup="OnClickSubmit"/>
                        </li>
                    </ul>
                    <asp:ValidationSummary ID="valsumStore" runat="server" CssClass="ErrorText"
                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="OnClickSubmit" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

