﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="Step2.aspx.cs" Inherits="StoreCreator_Step2" %>

<asp:Content ID="step2Content" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../CSS/radio.css" rel="stylesheet" />
    <script src="../JS/StoreCreator.js"></script>
    <script>
        $(document).ready(function () {
            var input_field = document.getElementById('txtSCMOV');

            input_field.addEventListener('change', function () {
                var v = parseFloat(this.value);
                if (isNaN(v)) {
                    this.value = '';
                } else {
                    this.value = v.toFixed(2);
                }
            });

            var input_fieldtxtB2CVatPercentage = document.getElementById('txtB2CVatPercentage');

            input_fieldtxtB2CVatPercentage.addEventListener('change', function () {
                var v = parseFloat(this.value);
                if (isNaN(v)) {
                    this.value = '';
                } else {
                    this.value = v.toFixed(2);
                }
            });

            var input_fieldtxtPoints = document.getElementById('txtPoints');

            input_fieldtxtPoints.addEventListener('change', function () {
                var v = parseFloat(this.value);
                if (isNaN(v)) {
                    this.value = '';
                } else {
                    this.value = v.toFixed(4);
                }
            });
        })
    </script>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Create Store</li>
            <li>Step 2</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <h3>Create Store</h3>
                    <p></p>
                    <div class="create_store">
                        <div class="length2"></div>
                        <ul>
                            <li>
                                <div class="wrapBox active">
                                    <span>1</span>
                                    <h4>Information</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>2</span>
                                    <h4>Store Settings</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>3</span>
                                    <h4>Store Theme</h4>
                                </div>
                            </li>
                            <li>
                                <div class="wrapBox">
                                    <span>4</span>
                                    <h4>Review & Create</h4>
                                </div>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <br />
                    <div class="select_data">
                        <div class="row details">
                            <div class="col-md-2">
                                <h6>Store Settings:</h6>
                            </div>
                            <div class="col-md-10">
                             <div class="wrap_lang create02">
                                 <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="ltrSearchSetting" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div6" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rdoSearchSetting" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                  <div class="NewTitle">
                                    <div class="col-md-12 pro_detail"><b>Product Listing Settings:</b> </div>
                                </div>
                                    <div class="wrap_creat02" id="dvDisplayType" clientidmode="static">
                                        <%--<h5>Product Listing:</h5>--%>
                                        <ul>
                                            <li>
                                                <div class="col-md-4">
                                                    <asp:Literal ID="lblPLDisplayTypeHeading" runat="server"></asp:Literal>:
                                                </div>
                                                <div class="col-md-2 text-left">Is Default </div>
                                            </li>
                                            <asp:Repeater ID="rptPLDisplayType" runat="server" OnItemDataBound="rptPLDisplayType_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-4">
                                                            <asp:CheckBox ID="chkPLDisplayType" CssClass="chkPLDisplayType" runat="server" />
                                                            <asp:HiddenField ID="hdnPLDisplayTypeStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                        <div id="dvDefaultDisplayType" runat="server" clientidmode="static" class="col-md-1 text-center radio radio-danger radio-storeType">
                                                            <input id="rbPLDisplayType" runat="server" type="radio" name="DefaultDisplayType" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <asp:CustomValidator ID="cvDisplayType" runat="server" Text="(Required)" ErrorMessage="Please select at least one display type"
                                            ClientValidationFunction="ValidateDisplayType" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                    </div>
                                    <div class="wrap_creat02">
                                        <h5>
                                            <asp:Literal ID="lblPaginationHeading" runat="server"></asp:Literal>:</h5>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblProductPagination" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="dvProductsPerPage" runat="server">
                                            <label>Select number of products per page: </label>
                                            <asp:TextBox ID="txtProductsPerPage" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:CustomValidator ID="cvProductsPerPage" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter no. products per page"
                                                ClientValidationFunction="ValidateProductsPerPage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" id="dvPLSorting" clientidmode="static">
                                        <h5>Default Settings:</h5>
                                        <ul>
                                            <li>
                                                <div class="col-md-4">
                                                    <asp:Literal ID="lblPLSortingHeading" runat="server"></asp:Literal>:
                                                </div>
                                                <div class="col-md-2 text-left">Is Default </div>
                                            </li>
                                            <asp:Repeater ID="rptPLSorting" runat="server" OnItemDataBound="rptPLSorting_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-4">
                                                            <asp:CheckBox ID="chkPLSorting" CssClass="chkPLSorting" runat="server" />
                                                            <asp:HiddenField ID="hdnPLSortingStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                        <div id="dvPLSorting" runat="server" clientidmode="static" class="col-md-1 text-center radio radio-danger radio-storeType">
                                                            <input id="rbPLSorting" runat="server" type="radio" name="PLSorting" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <asp:CustomValidator ID="cvPLSorting" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select atleast one sorting"
                                            ClientValidationFunction="ValidateSorting" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                    </div>
                                    <div class="wrap_creat02" id="dvPLFilter" clientidmode="static">
                                        <ul>
                                            <li>
                                                <div class="col-md-4">
                                                    <asp:Literal ID="lblPLFilterHeading" runat="server"></asp:Literal>:
                                                </div>
                                            </li>
                                            <%--<li>
                                                <div class="col-md-4">
                                                    <asp:CheckBox ID="chkEnableFilter" ClientIDMode="Static" Checked="false" runat="server" Text="Enable/Disable All Filters" />
                                                </div>
                                            </li>--%>
                                            <asp:Repeater ID="rptPLFilter" runat="server" OnItemDataBound="rptPLFilter_ItemDataBound">
                                                <ItemTemplate>
                                                    <li id="liFilter" runat="server">
                                                        <div class="col-md-4">
                                                            <asp:CheckBox ID="chkPLFilter" CssClass="chkPLFilter" runat="server" />
                                                            <asp:HiddenField ID="hdnPLFilterStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                 <div class="wrap_creat02">

                                        <%--<div class="col-md-12 col-sm-12">--%>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkMaxAllowQty" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                            <asp:HiddenField ID="hdnMaxOrderQtyStoreFeatureDetailId" runat="server" />

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div7" runat="server">
                                            <label>Max Allowable Order Qty / Item: </label>
                                            <asp:TextBox ID="txtAllowMaxOrder" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:CustomValidator ID="CustomValidator1" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter no. products per page"
                                                ClientValidationFunction="ValidateProductsPerPage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                        </div>
                                        <%--</div>--%>
                                        <div class="clearfix"></div>
                                    </div>

                                 <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="ltrHomeLink" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div5" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rdoLinkPosition" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkQuickView" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPLQuickViewStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkProductComparison" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnPLProductComparisonStoreFeatureDetailId" runat="server" />
                                            </div>
                                        <div class="clearfix"></div>
                                    </div>
                                 <div class="wrap_creat02">
                                        <h5 class="col-md-12 pro_detail">
                                            <asp:Literal ID="lblCategoryDisplay" runat="server"></asp:Literal>:</h5>
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6 num_size " id="DivCategoryDisplay" runat="server">
                                                <div class="radio_data radio radio-danger">
                                                    <asp:RadioButtonList ID="rblCategoryDisplay" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                 <div class="wrap_creat02 last">
                                        <h5 class="col-md-6 col-sm-6">
                                            <asp:Literal ID="lblBasketPopup" runat="server"></asp:Literal>:</h5>
                                        <div class="col-md-6 col-sm-6 num_size no_borderbtm" id="DivBasketPopup" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblBasketPopup" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                 </div>
                                <div class="NewTitle top_mar">
                                    <div class="col-md-12 pro_detail"><b>Product Details Settings:</b></div>
                                </div>
                                <div class="wrap_lang create02 wrap_product_detail">

                                    <div class="wrap_creat02 pro_detail_check last">



                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDPrint" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDPrintStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDEmail" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDEmailStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDDownloadImage" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDDownloadImageStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDSocialMedia" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDSocialMediaStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDRecentlyViewedProducts" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDRecentlyViewedProductsStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDYouMayAlsoLike" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDYouMayAlsoLikeStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6" style="display:none;">
                                            <asp:CheckBox ID="chkPDWishlist" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDWishlistStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDReviewRating" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDReviewRatingStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDSavePDF" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDSavePDFStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDSectionIcons" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDSectionIconsStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <h6>Choose Payments:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02">
                                    <div class="wrap_creat02" id="dvSCPaymentGateway" clientidmode="static">
                                        <ul>
                                            <li>
                                                <div class="col-md-4">
                                                    <asp:Literal ID="ltrPaymentType" runat="server" Text="Payment Type :"></asp:Literal>
                                                    
                                                </div>
                                            </li>
                                            <li style="margin-bottom:12px;">
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:CheckBox ID="chkEnablePoints" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                                    <asp:HiddenField ID="hdnEnablePoints" runat="server" />
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div id="dvPoints" runat="server" clientidmode="static" class="enable_text">
                                                    1 point :
                                                    <asp:TextBox ID="txtPoints" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                    <asp:Literal ID="ltrSelectedCurrency" runat="server" Text=""></asp:Literal>
                                                    <%--<asp:RequiredFieldValidator ID="rfvPointData" runat="server" ControlToValidate="txtPoints" Text="(Required)" ErrorMessage="Enter point value" Display="Dynamic" ValidationGroup="OnClickSubmit" ForeColor="Red">
                                                    </asp:RequiredFieldValidator>--%>
                                                    <asp:CustomValidator ID="cvPointData" runat="server" Text="(Required)" ErrorMessage="Please enter point value"
                                                ClientValidationFunction="ValidatePointData" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                                </div>
                                            </li>
                                            <div id="liPaymentGatewayHeading" clientidmode="static" runat="server">
                                                <li>
                                                    <div class="col-md-4">
                                                        <asp:Literal ID="lblSCPaymentGatewayHeading" runat="server"></asp:Literal>:
                                                    </div>
                                                    <div class="col-md-2 text-left">Is Default </div>
                                                </li>
                                                <asp:Repeater ID="rptSCPaymentGateway" runat="server" OnItemDataBound="rptSCPaymentGateway_ItemDataBound">
                                                    <ItemTemplate>
                                                        <li>
                                                            <div class="col-md-4">
                                                                <asp:CheckBox ID="chkSCPaymentGateway" CssClass="chkSCPaymentGateway" runat="server" />
                                                                <asp:HiddenField ID="hdnSCPaymentGatewayStoreFeatureDetailId" runat="server" />
                                                            </div>
                                                            <div id="dvSCPaymentGateway" runat="server" clientidmode="static" class="col-md-1 text-center radio radio-danger radio-storeType">
                                                                <input id="rbSCPaymentGateway" runat="server" type="radio" name="SCPaymentGateway" clientidmode="static" />
                                                                <label></label>
                                                            </div>
                                                            <%--<div class="col-md-7" style="display: none;" id="dvPointData" clientidmode="static">
                                                            <asp:TextBox ID="txtPointData" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvPointData" Enabled="false" runat="server" ControlToValidate="txtPointData" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSubmit" ForeColor="Red">
                                                            </asp:RequiredFieldValidator>
                                                        </div>--%>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <%--<li>
                                                <div class="col-md-4">
                                                    <asp:CheckBox ID="chkSCPaymentGateway" CssClass="chkSCPaymentGateway" runat="server" />
                                                    <asp:HiddenField ID="hdnSCPaymentGatewayStoreFeatureDetailId" runat="server" />
                                                </div>
                                                <div id="Div5" runat="server" clientidmode="static" class="col-md-1 text-center radio radio-danger radio-storeType">
                                                    <input id="rbSCPaymentGateway" runat="server" type="radio" name="SCPaymentGateway" clientidmode="static" />
                                                    <label></label>
                                                </div>
                                                <div class="col-md-7" id="dvPointData">
                                                    <asp:TextBox ID="txtPointData" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvPointData" ClientIDMode="Static" runat="server" ControlToValidate="txtPointData" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSubmit" ForeColor="Red">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </li>--%>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <asp:CustomValidator ID="cvSCPaymentGateway" runat="server" Text="(Required)" ErrorMessage="Please select at least one payment gateway"
                                            ClientValidationFunction="ValidateSCPaymentGateway" Display="Dynamic" ForeColor="Red"  ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                    </div>

                                    <%--<div class="wrap_creat02" id="dvSCAlternativePayments" clientidmode="static" style="display:none;">
                                        <ul>
                                            <li>
                                                <div class="col-md-4">
                                                    <asp:Literal ID="lblSCAlternativePaymentsHeading" runat="server"></asp:Literal>:
                                                </div>
                                                <div class="col-md-2 text-left">Is Default </div>
                                            </li>
                                            <asp:Repeater ID="rptSCAlternativePayments" runat="server" OnItemDataBound="rptSCAlternativePayments_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-4">
                                                            <asp:CheckBox ID="chkSCAlternativePayment" CssClass="chkSCAlternativePayment" runat="server" />
                                                            <asp:HiddenField ID="hdnSCAlternativePaymentStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                        <div id="dvSCAlternativePayment" runat="server" clientidmode="static" class="col-md-1 text-center radio radio-danger radio-storeType">
                                                            <input id="rbSCAlternativePayment" runat="server" type="radio" name="SCAlternativePayment" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <asp:CustomValidator ID="cvSCAlternativePayments" runat="server" Text="(Required)" ErrorMessage="Please select at least one alternative payment"
                                            ClientValidationFunction="ValidateSCAlternativePayments" Display="Dynamic" ForeColor="Red"  ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                    </div>--%>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">

                                        <%--<div class="col-md-12 col-sm-12">--%>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkEnableCouponCode" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnEnableCouponCode" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <%--</div>--%>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check  setting_newmargin">
                                        <h5 class="pro_detail">Punchout Settings:</h5>
                                        <br />
                                        <div class="col-md-12 col-sm-6 marright_label1 marginLeft">
                                            <asp:CheckBox ID="chkIsPunchoutRegister" runat="server" Text="IsPunchout Registration" TextAlign="Left" ClientIDMode="static" />
                                            <asp:HiddenField ID="hdnIsPunchoutReg" runat="server" />

                                            <div class="clearfix"></div>

                                        </div>

                                        <div class="col-md-12 col-sm-6 marright_label1 marginLeft">

                                            <asp:CheckBox ID="chkPunchout" runat="server" Text="Allow Punchout" TextAlign="Left" ClientIDMode="static" />
                                            <div class="clearfix"></div>


                                            <div class="col-md-6 col-sm-6" id="divCustomPunchout" style="display: none;" clientidmode="static">
                                                <%--<asp:CheckBox ID="chkCustomPunchout" runat="server" Text="Custom Punchout" TextAlign="Left" />--%>
                                                <asp:RadioButtonList ID="rdoCustomPunchout" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                                <asp:HiddenField ID="hdnCustomPunchout" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <%--</div>--%>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02 last">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="ltGiftCertificate" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="Div3" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rdlIsGiftCertificateAllow" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="div4" clientidmode="Static" runat="server">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h6>Orders and Checkout: </h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02">
                                    <div class="wrap_creat02">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblSCInternationalOrders" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkSCInternationalOrders" runat="server" ClientIDMode="Static"/>
                                            <asp:HiddenField ID="hdnSCInternationalOrdersStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblSCMessage" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:TextBox ID="txtSCMessage" runat="server" TextMode="MultiLine" Rows="2" Columns="25" ClientIDMode="Static"></asp:TextBox>
                                            <asp:HiddenField ID="hdnSCMessageStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblSCMOV" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <%--<asp:TextBox ID="txtSCMOV" runat="server" onkeyup="return checkDec(this);"></asp:TextBox>--%>
                                            <asp:TextBox ID="txtSCMOV" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:HiddenField ID="hdnSCMOVStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkAllowBackOrder" runat="server" TextAlign="Left" ClientIDMode="Static"/>
                                            <asp:HiddenField ID="hdnODAllowBackOrderStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " style="display:none;">
                                            <label>no of days for backorder: </label>
                                            <asp:TextBox ID="txtAllowBackOrder" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <%--<asp:CustomValidator ID="cvAllowBackOrder" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter no. of days for backorder"
                                                ClientValidationFunction="ValidateAllowBackOrder" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>--%>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Email copy of basket Button settings:</h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkEmailCopyBasketBtn" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnEmailCopyBasketBtn" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkSCEnablePunchout" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnSCEnablePunchoutStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <h5 class="col-md-12 pro_detail">Stock Due Date Value:</h5>
                                        <br />
                                        <div class="create02 no_borderbtm store_access noBorder">
                                            <div class="col-md-3 col-sm-3">
                                                <asp:Label ID="lblStockDueDate" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 num_size">
                                                <asp:TextBox ID="txtStockDueDate" runat="server" MaxLength="3" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvStockDueDate" runat="server" ControlToValidate="txtStockDueDate"
                                                    Display="dynamic" ErrorMessage="Please enter stock due date"
                                                    Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:HiddenField ID="hdnStockDueDate" runat="server" />
                                                <asp:RangeValidator ID="rvStockDueDate" runat="server" ControlToValidate="txtStockDueDate" Type="Integer" MinimumValue="0"
                                                    MaximumValue="365" ErrorMessage="Enter a valid value" Text="(Enter value between 0 - 365)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RangeValidator>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <h5 class="col-md-12 pro_detail">Order History Months to Return:</h5>
                                        <br />
                                        <div class="create02 no_borderbtm store_access noBorder">
                                            <div class="col-md-3 col-sm-3">
                                                <asp:Label ID="lblOrderHistoryMonthstoReturn" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 num_size ">
                                                <asp:TextBox ID="txtOrderHistoryMonthstoReturn" runat="server" MaxLength="2" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvOrderHistoryMonthstoReturn" runat="server" ControlToValidate="txtOrderHistoryMonthstoReturn"
                                                    Display="dynamic" ErrorMessage="Please enter stock due date"
                                                    Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:HiddenField ID="hdnOrderHistoryMonthstoReturn" runat="server" />
                                                <asp:RangeValidator ID="rvOrderHistoryMonthstoReturn" runat="server" ControlToValidate="txtOrderHistoryMonthstoReturn" Type="Integer" MinimumValue="1"
                                                    MaximumValue="12" ErrorMessage="Enter a valid value" Text="(Enter value between 1 - 12)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RangeValidator>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <h5 class="col-md-12 pro_detail">Product Enquiry color box:</h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkProductEnquiryColor" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnProductEnquiryColor" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <h5 class="col-md-12 pro_detail">Submit Enquiry [No Order Placing]: </h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkSubmitEnquiry" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnSubmitEnquiry" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02" style="display:none;">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblSCApproveAllOrders" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="radio_data radio radio-danger  ">
                                                <asp:RadioButtonList ID="rblSCApproveAllOrders" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkCOnfirmationEmail" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                                <asp:HiddenField ID="hdnCOemail" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h6>Customer Registration
                  & Account:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02">
                                    <div class="wrap_creat02">
                                        <h5><asp:Label ID="lblCRCustomerTypeHeading" runat="server"></asp:Label></h5>
                                        <div class="col-md-4 col-sm-4 ">
                                            <div class="radio_data  radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRCustomerType" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 type_select  ">
                                            <div class="select-style selectpicker">
                                                <asp:DropDownList ID="ddlCRCustomerHierarchyLevel" runat="server" ClientIDMode="Static">
                                                    <asp:ListItem Text="Select Level" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CustomValidator ID="cvCRCustomerHierarchyLevel" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select hierarchy level"
                                            ClientValidationFunction="ValidateCRCustomerHierarchyLevel" Display="Dynamic" ForeColor="Red"  ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" id="dvApprovalRequired" runat="server" clientidmode="static">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblCRApprovalRequired" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRApprovalRequired" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" style="display:none;">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:Label ID="lblCRNotificationEmail" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 radio radio-danger   ">
                                            <asp:TextBox ID="txtCRNotificationEmail" runat="server" ClientIDMode="Static"></asp:TextBox>
                                            <asp:HiddenField ID="hdnCRNotificationEmailStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" style="display:none;">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblCRWhiteListValidation" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 ">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRWhiteListValidation" runat="server" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkCREnableDomainValidation" runat="server" TextAlign="Left" ClientIDMode="Static"/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 " id="dvCREnableDomainValidation" runat="server" clientidmode="static">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCREnableDomainValidation" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkCREnableEmailValidation" runat="server" TextAlign="Left" ClientIDMode="Static"/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 " id="dvCREnableEmailValidation" runat="server" clientidmode="static">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCREnableEmailValidation" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkCREnableSSO" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnCREnableSSOStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    
                                    <div class="wrap_creat02 num_size" style="display:none;">
                                        <div class="col-md-5 col-sm-6">
                                            <asp:Label ID="lblCRResetPasswordBlockAccountDuration" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 radio radio-danger   ">
                                            <asp:TextBox ID="txtCRResetPasswordBlockAccountDuration" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:HiddenField ID="hdnCRResetPasswordBlockAccountDurationStoreFeatureDetailId" runat="server" />
                                            <%--<asp:CustomValidator ID="cvCRResetPasswordBlockAccountDuration" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter no. of hours to block account"
                                                ClientValidationFunction="ValidateResetPasswordBlockAccountDuration" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>--%>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="lblCRStoreAccessHeading" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div1" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRStoreAccess" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02 last">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="lblCRStoreTypeHeading" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="Div2" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRStoreType" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="divVatPercentage" ClientIDMode="Static" runat="server">
                                            <span style="vertical-align:top;">VAT :  </span>
                                            <asp:TextBox ID="txtB2CVatPercentage" runat="server" ClientIDMode="Static" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            %
                                            <asp:CustomValidator ID="cvB2CVatPercentage" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter VAT percentage"
                                                ClientValidationFunction="ValidateB2CVatPercentage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                            
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h6>Password Security:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02">
                                    <div class="wrap_creat02">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblPSMinimumPasswordLength" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size">
                                            <asp:TextBox ID="txtPSMinimumPasswordLength" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPSMinimumPasswordLength" runat="server" ControlToValidate="txtPSMinimumPasswordLength"
                                                Display="dynamic" ErrorMessage="Please enter minimum password length"
                                                Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:HiddenField ID="hdnPSMinimumPasswordLengthStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" style="display:none">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblPSPasswordExpression" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPSPasswordExpression" runat="server" CssClass="hide"/>
                                            <asp:RadioButtonList ID="rdPwdType" runat="server">
                                                <asp:ListItem Text="Only Alpha" Value="A"></asp:ListItem>
                                                <asp:ListItem Text="Only AlphaNumeric" Value="AN"></asp:ListItem>
                                                <asp:ListItem Text="Only Alpha Numeric and Symbol" Value="ANS"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:HiddenField ID="hdnPSPasswordExpressionStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Code Added By SHRIGANESH SINGH For Password Policy START 09 May 2016 -->
                                    <div class="wrap_creat02">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblPasswordPolicy" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPSPasswordPolicy" runat="server" CssClass="hide" />
                                            <asp:RadioButtonList ID="rdPasswordType" runat="server">
                                           <%--     <asp:ListItem Text="Any Combination"></asp:ListItem>
                                                <asp:ListItem Text="Must Contain Alpha Numeric"></asp:ListItem>
                                                <asp:ListItem Text="Must Contain Alpha Numeric and Symbol"></asp:ListItem>--%>
                                            </asp:RadioButtonList>
                                            <asp:HiddenField ID="hdnPasswordPolicyStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Code Added By SHRIGANESH SINGH For Password Policy END 09 May 2016 -->
                                    <div class="wrap_creat02" style="display:none;">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblPSForcePasswordChange" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPSForcePasswordChange" runat="server" />
                                            <asp:HiddenField ID="hdnPSForcePasswordChangeStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblPSPasswordValidity" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size">
                                            <asp:TextBox ID="txtPSPasswordValidity" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:HiddenField ID="hdnPSPasswordValidityStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" style="display:none;">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkCREnableCaptcha" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnCREnableCaptchaStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h6>Cookies Law (EU/UK):</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02">
                                    <div class="wrap_creat02">
                                        <div class="col-md-4 col-sm-4">
                                             <asp:Label ID="lblCLEnableCookie" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkCLEnableCookie" runat="server" ClientIDMode="Static"/>
                                            <asp:HiddenField ID="hdnCLEnableCookieStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-4">
                                           <asp:Label ID="lblCLCookieLifetime" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 num_size">
                                            <asp:TextBox ID="txtCLCookieLifetime" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            Default is <label id="days" runat="server"></label> days
                                            <asp:CustomValidator ID="cvCLCookieLifetime" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter cookie lifetime"
                                            ClientValidationFunction="ValidateCLCookieLifetime" Display="Dynamic" ForeColor="Red"  ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                            <asp:HiddenField ID="hdnCLCookieLifetimeStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02">
                                        <div class="col-md-4">
                                            <asp:Label ID="lblCLCookieMessage" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:TextBox ID="txtCLCookieMessage" ClientIDMode="Static" runat="server" TextMode="MultiLine" Rows="2" Columns="25"></asp:TextBox>
                                            <asp:CustomValidator ID="cvCLCookieMessage" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter cookie message"
                                            ClientValidationFunction="ValidateCLCookieMessage" Display="Dynamic" ForeColor="Red"  ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                            <asp:HiddenField ID="hdnCookieMessageStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="cmdSaveClose" runat="server" CssClass="btn" Text="Save & Close" ValidationGroup="OnClickSubmit" CommandName="SaveClose" OnClick="cmdSave_Click"/>
                        </li>
                        <li>
                            <asp:Button ID="cmdSaveContinue" runat="server" CssClass="btn" Text="Save & Continue" ValidationGroup="OnClickSubmit" CommandName="SaveContinue" OnClick="cmdSave_Click"/>
                        </li>
                        <li>
                            <asp:Button ID="cmdCancel" runat="server" CssClass="btn gray" Text="Cancel" OnClick="cmdCancel_Click"/>
                        </li>
                        <li>
                            <asp:Button ID="cmdBack" runat="server" CssClass="btn gray" Text="Back" OnClick="cmdBack_Click"/>
                        </li>
                    </ul>
                    <asp:ValidationSummary ID="valsumStore" runat="server" CssClass="ErrorText"
                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="OnClickSubmit" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

