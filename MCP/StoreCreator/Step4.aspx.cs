﻿//using DotNetUtilities.Utilities;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class StoreCreator_Step4 : System.Web.UI.Page
{
    #region Variables

    StoreBE StoreInstance;

    #endregion

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Added by sanchit for authorization

        GlobalFunctions.IsAuthorized("step1.aspx");

        if (Session["PreviousPage"] != null && Session["StoreId"] != null)
        {
            if (Request.QueryString["id"] != null)
            {
                Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);
                if (Convert.ToString(Session["PreviousPage"]) == "step3" && Convert.ToInt16(Session["StoreId"]) == StoreId) { }
                else
                    Response.Redirect("~/UnAuthorized.html");
            }
            else
                Response.Redirect("~/UnAuthorized.html");
        }
        else
            Response.Redirect("~/UnAuthorized.html");

        #endregion

        //if (Request.UrlReferrer != null)
        //{
        //    if (Request.UrlReferrer.Segments.Length > 0)
        //    {
        //        if (Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1].ToLower() != "step3.aspx")
        //        {
        //            HttpContext.Current.Response.Redirect("~/UnAuthorized.html");
        //        }
        //    }
        //}
        if (!IsPostBack)
        {
            ShowStoreFeatures();
        }
    }

    #endregion

    #region Show Store Features

    private void ShowStoreFeatures()
    {
        try
        {
            Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);
            Session["store"] = StoreInstance = StoreBL.GetStoreDetails(StoreId);
            List<TemplateBE> getAllTemplate = TemplateBL.GetAllTemplateDetails();
            if (getAllTemplate.FirstOrDefault(x => x.TemplateId == StoreInstance.TemplateId) != null)
            {
                imgTemplate.ImageUrl = Convert.ToString(GlobalFunctions.GetSetting("CS_TEMPLATEIMAGEPATH")).Replace("@templatename", getAllTemplate.FirstOrDefault(x => x.TemplateId == StoreInstance.TemplateId).TemplateName);
                hdnTemplateName.Value = getAllTemplate.FirstOrDefault(x => x.TemplateId == StoreInstance.TemplateId).TemplateName;
            }
            else
                imgTemplate.ImageUrl = GlobalFunctions.GetVirtualPath() + "Images/UI/no-template.jpg";

            if (StoreInstance != null)
            {
                lblStoreName.Text = StoreInstance.StoreAlias;
                lblCloneStore.Text = !string.IsNullOrEmpty(StoreInstance.CloneStoreName) ? StoreInstance.CloneStoreName : "None";

                #region Product Listing Display Type

                FeatureBE ProductListingTypefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_displaytype");

                if (ProductListingTypefeature != null)
                {
                    lblProductListingTypeHeading.Text = ProductListingTypefeature.FeatureAlias;
                    rptDisplayType.DataSource = ProductListingTypefeature.FeatureValues;
                    rptDisplayType.DataBind();
                    lblDefaultView.Text = ProductListingTypefeature.FeatureValues.FirstOrDefault(x => x.IsDefault == true) != null ?
                        ProductListingTypefeature.FeatureValues.FirstOrDefault(x => x.IsDefault == true).FeatureValue : "None";
                }

                #endregion

                #region Pagination

                FeatureBE Paginationfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_pagination");

                if (Paginationfeature != null)
                {
                    FeatureBE.FeatureValueBE PaginationfeatureValue = Paginationfeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "pagination");
                    lblPaginationHeading.Text = Paginationfeature.FeatureAlias;
                    lblPagination.Text = PaginationfeatureValue.IsEnabled ? "Yes" : "No";
                    if (PaginationfeatureValue.IsEnabled)
                    {
                        dvNoOfProductsPerPage.Style["display"] = "block";
                        lblProductsPerPage.Text = PaginationfeatureValue.FeatureDefaultValue;
                    }
                    else
                        dvNoOfProductsPerPage.Style["display"] = "none";
                }

                #endregion

                #region Sorting

                FeatureBE Sortingfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_sorting");

                if (Sortingfeature != null)
                {
                    lblSortingHeading.Text = Sortingfeature.FeatureAlias;
                    foreach (FeatureBE.FeatureValueBE featureValue in Sortingfeature.FeatureValues)
                    {
                        lblSorting.Text += featureValue.IsEnabled ? featureValue.FeatureValue + "<br>" : string.Empty;
                    }

                    lblSorting.Text = !string.IsNullOrEmpty(lblSorting.Text) ? lblSorting.Text : "None";

                    lblDefaultSorting.Text = Sortingfeature.FeatureValues.FirstOrDefault(x => x.IsDefault == true) != null ?
                        Sortingfeature.FeatureValues.FirstOrDefault(x => x.IsDefault == true).FeatureValue : "None";
                }

                #endregion

                #region Filter

                FeatureBE Filterfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_filter");

                if (Filterfeature != null)
                {
                    lblFilterHeading.Text = Filterfeature.FeatureAlias;
                    foreach (FeatureBE.FeatureValueBE featureValue in Filterfeature.FeatureValues)
                    {
                        lblFilter.Text += featureValue.IsEnabled ? featureValue.FeatureValue + "<br>" : string.Empty;
                    }

                    lblFilter.Text = !string.IsNullOrEmpty(lblFilter.Text) ? lblFilter.Text : "None";

                    lblDefaultFilter.Text = Filterfeature.FeatureValues.FirstOrDefault(x => x.IsDefault == true) != null ?
                        Filterfeature.FeatureValues.FirstOrDefault(x => x.IsDefault == true).FeatureValue : "None";
                }

                #endregion

                #region QuickView

                FeatureBE QuickViewFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_quickview");

                if (QuickViewFeature != null)
                    lblQuickView.Text = QuickViewFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";

                #endregion

                #region Product Detail Print

                FeatureBE PrintFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableprint");

                if (PrintFeature != null)
                {
                    lblPrintHeading.Text = PrintFeature.FeatureAlias;
                    lblPrint.Text = PrintFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail Email

                FeatureBE EmailFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableemail");

                if (EmailFeature != null)
                {
                    lblEmailHeading.Text = EmailFeature.FeatureAlias;
                    lblEmail.Text = EmailFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail Download Image

                FeatureBE DownloadImageFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enabledownloadimage");

                if (DownloadImageFeature != null)
                {
                    lblDownloadImageHeading.Text = DownloadImageFeature.FeatureAlias;
                    lblDownloadImage.Text = DownloadImageFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail Social Media

                FeatureBE SocialMediaFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesocialmedia");

                if (SocialMediaFeature != null)
                {
                    lblSocialMediaHeading.Text = SocialMediaFeature.FeatureAlias;
                    lblSocialMedia.Text = SocialMediaFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail Recently Viewed Products

                FeatureBE RecentlyViewedProductsFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablerecentlyviewedproducts");

                if (RecentlyViewedProductsFeature != null)
                {
                    lblRecentlyViewedProductsHeading.Text = RecentlyViewedProductsFeature.FeatureAlias;
                    lblRecentlyViewedProducts.Text = RecentlyViewedProductsFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail You May Also Like

                FeatureBE YouMayAlsoLikeFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableyoumayalsolike");

                if (YouMayAlsoLikeFeature != null)
                {
                    lblYouMayAlsoLikeHeading.Text = YouMayAlsoLikeFeature.FeatureAlias;
                    lblYouMayAlsoLike.Text = YouMayAlsoLikeFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail WishList

                FeatureBE WishListFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablewishlist");

                if (WishListFeature != null)
                {
                    lblWishListHeading.Text = WishListFeature.FeatureAlias;
                    lblWishList.Text = WishListFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail Review Rating

                FeatureBE ReviewRatingFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablereviewrating");

                if (ReviewRatingFeature != null)
                {
                    lblReviewRatingHeading.Text = ReviewRatingFeature.FeatureAlias;
                    lblReviewRating.Text = ReviewRatingFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail Save PDF

                FeatureBE SavePDFFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesavepdf");

                if (SavePDFFeature != null)
                {
                    lblSavePDFHeading.Text = SavePDFFeature.FeatureAlias;
                    lblSavePDF.Text = SavePDFFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Product Detail Section Icons

                FeatureBE SectionIconsFeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesectionicons");

                if (SectionIconsFeature != null)
                {
                    lblSectionIconsHeading.Text = SectionIconsFeature.FeatureAlias;
                    lblSectionIcons.Text = SectionIconsFeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Languages

                for (int i = 0; i < StoreInstance.StoreLanguages.Count; i++)
                {
                    StoreBE.StoreLanguageBE Language = StoreInstance.StoreLanguages[i];
                    if (i == 0)
                        lblLanguages.Text += Language.LanguageName;
                    else
                        lblLanguages.Text += ", " + Language.LanguageName;
                }

                lblDefaultLanguage.Text = StoreInstance.StoreLanguages.FirstOrDefault(x => x.IsDefault == true).LanguageName;

                #endregion

                #region Currencies

                for (int i = 0; i < StoreInstance.StoreCurrencies.Count; i++)
                {
                    StoreBE.StoreCurrencyBE Currency = StoreInstance.StoreCurrencies[i];
                    if (i == 0)
                        lblCurrencies.Text += Currency.CurrencyName;
                    else
                        lblCurrencies.Text += ", " + Currency.CurrencyName;
                }

                lblDefaultCurrency.Text = StoreInstance.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyName;

                #endregion

                #region Enable Points

                FeatureBE EnablePointsfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_enablepoints");

                if (EnablePointsfeature != null)
                {
                    lblEnablePointsHeading.Text = EnablePointsfeature.FeatureAlias;
                    lblEnablePoints.Text = EnablePointsfeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                if (EnablePointsfeature.FeatureValues[0].IsEnabled)
                {
                    liPointValue.Style["display"] = "";
                    lblPointValue.Text = EnablePointsfeature.FeatureValues[0].FeatureDefaultValue;
                    liPaymentGateway.Style["display"] = "none";
                }
                else
                {
                    liPointValue.Style["display"] = "none";
                    liPaymentGateway.Style["display"] = "";
                }



                #endregion

                #region Payment Gateway

                FeatureBE PaymentGatewayfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_paymentgateway");

                if (PaymentGatewayfeature != null)
                {
                    lblPaymentGatewayHeading.Text = PaymentGatewayfeature.FeatureAlias;
                    foreach (FeatureBE.FeatureValueBE featureValue in PaymentGatewayfeature.FeatureValues)
                    {
                        lblPaymentGateways.Text += featureValue.IsEnabled ? featureValue.FeatureValue + "<br>" : string.Empty;
                    }

                    lblPaymentGateways.Text = !string.IsNullOrEmpty(lblPaymentGateways.Text) ? lblPaymentGateways.Text : "None";
                }

                #endregion

                #region Gift Certificate

                FeatureBE GiftCertificatefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_giftcertificate");

                if (GiftCertificatefeature != null)
                {
                    lblGiftCertificateHeading.Text = GiftCertificatefeature.FeatureAlias;
                    lblGiftCertificate.Text = GiftCertificatefeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue; ;
                }

                #endregion

                #region Alternative Payments

                FeatureBE AlternativePaymentsfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_alternativepayments");

                if (AlternativePaymentsfeature != null)
                {
                    lblAlternativePaymentsHeading.Text = AlternativePaymentsfeature.FeatureAlias;
                    foreach (FeatureBE.FeatureValueBE featureValue in AlternativePaymentsfeature.FeatureValues)
                    {
                        lblAlternativePayments.Text += featureValue.IsEnabled ? featureValue.FeatureValue + "<br>" : string.Empty;
                    }

                    lblAlternativePayments.Text = !string.IsNullOrEmpty(lblAlternativePayments.Text) ? lblAlternativePayments.Text : "None";
                }

                #endregion

                #region International Orders
                FeatureBE InternationalOrdersfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_internationalorders");

                if (InternationalOrdersfeature != null)
                {
                    lblInternationalOrdersHeading.Text = InternationalOrdersfeature.FeatureAlias;
                    lblInternationalOrders.Text = InternationalOrdersfeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Message

                FeatureBE Messagefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_message");

                if (Messagefeature != null)
                {
                    lblMessageHeading.Text = Messagefeature.FeatureAlias;
                    lblMessage.Text = !string.IsNullOrEmpty(Messagefeature.FeatureValues[0].FeatureDefaultValue) ? Messagefeature.FeatureValues[0].FeatureDefaultValue : "None";
                }

                #endregion

                #region Minimum Order Value

                FeatureBE MOVfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_mov");

                if (MOVfeature != null)
                {
                    lblMOVHeading.Text = MOVfeature.FeatureAlias;
                    lblMOV.Text = MOVfeature.FeatureValues[0].IsEnabled ? MOVfeature.FeatureValues[0].FeatureDefaultValue : "None";
                }

                #endregion

                #region Approve All Order

                FeatureBE ApproveAllOrderfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_approveallorders");

                if (ApproveAllOrderfeature != null)
                {
                    lblApproveAllOrderHeading.Text = ApproveAllOrderfeature.FeatureAlias;
                    lblApproveAllOrder.Text = ApproveAllOrderfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null ?
                        ApproveAllOrderfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue : "None";
                }

                #endregion

                #region Allow BackOrder

                FeatureBE AllowBackOrderfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "od_allowbackorder");

                if (AllowBackOrderfeature != null)
                {
                    lblAllowBackOrderHeading.Text = AllowBackOrderfeature.FeatureAlias;
                    lblAllowBackOrder.Text = AllowBackOrderfeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                    lblNoOfDaysForBackOrder.Text = AllowBackOrderfeature.FeatureValues[0].IsEnabled ? AllowBackOrderfeature.FeatureValues[0].FeatureDefaultValue : "None";
                }

                #endregion

                #region Punchout

                lblPunchout.Text = StoreInstance.IsPunchOut ? "Yes" : "No";

                #endregion

                #region Customer Type

                FeatureBE CustomerTypefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_customertype");

                if (CustomerTypefeature != null)
                {
                    lblCustomerTypeHeading.Text = CustomerTypefeature.FeatureAlias;
                    if (CustomerTypefeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "no-hierarchy").IsEnabled == true)
                        lblCustomerType.Text = "No hierarchy";
                    else if (!string.IsNullOrEmpty(CustomerTypefeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "hierarchy").FeatureDefaultValue))
                    {
                        for (int i = 1; i <= Convert.ToByte(CustomerTypefeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "hierarchy").FeatureDefaultValue); i++)
                        {
                            lblCustomerType.Text += "Level " + i + "<br>";
                        }
                    }

                    lblCustomerType.Text = !string.IsNullOrEmpty(lblCustomerType.Text) ? lblCustomerType.Text : "None";
                    dvEmailApproval.Style["display"] = CustomerTypefeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "no-hierarchy").IsEnabled ? "none" : "";
                }

                #endregion

                #region Email Approval

                FeatureBE EmailApprovalfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_approvalrequired");

                if (EmailApprovalfeature != null)
                {
                    lblEmailApproval.Text = EmailApprovalfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null ?
                        EmailApprovalfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue : "None";
                }

                #endregion

                #region Notification Email

                FeatureBE NotificationEmailfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_notificationemail");

                if (NotificationEmailfeature != null)
                    lblNotificationEmail.Text = !string.IsNullOrEmpty(NotificationEmailfeature.FeatureValues[0].FeatureDefaultValue) ? NotificationEmailfeature.FeatureValues[0].FeatureDefaultValue : "None";

                #endregion

                #region WhiteList Validation

                FeatureBE WhiteListValidationfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_whitelistvalidation");

                if (WhiteListValidationfeature != null)
                {
                    lblWhiteListValidation.Text = WhiteListValidationfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null ?
                        WhiteListValidationfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue : "None";
                }

                #endregion

                #region Enable Domain Validation

                FeatureBE EnableDomainValidationfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation");

                if (EnableDomainValidationfeature != null)
                {
                    lblEnableDomainValidationHeading.Text = EnableDomainValidationfeature.FeatureAlias;
                    lblEnableDomainValidation.Text = EnableDomainValidationfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null ?
                        EnableDomainValidationfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue : "No";
                }

                #endregion

                #region Enable Email Validation

                FeatureBE EnableEmailValidationfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation");

                if (EnableEmailValidationfeature != null)
                {
                    lblEnableEmailValidationHeading.Text = EnableEmailValidationfeature.FeatureAlias;
                    lblEnableEmailValidation.Text = EnableEmailValidationfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null ?
                        EnableEmailValidationfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue : "No";
                }

                #endregion

                #region Enable SSO

                FeatureBE EnableSSOfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_enablesso");

                if (EnableSSOfeature != null)
                {
                    lblEnableSSOHeading.Text = EnableSSOfeature.FeatureAlias;
                    lblEnableSSO.Text = EnableSSOfeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region ResetPassword - BlockAccount Duration

                FeatureBE ResetPasswordBlockAccountDurationfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_resetpasswordblockaccountduration");

                if (ResetPasswordBlockAccountDurationfeature != null)
                {
                    lblResetPasswordBlockAccountDurationHeading.Text = ResetPasswordBlockAccountDurationfeature.FeatureAlias;
                    lblResetPasswordBlockAccountDuration.Text = ResetPasswordBlockAccountDurationfeature.FeatureValues[0].IsEnabled ? ResetPasswordBlockAccountDurationfeature.FeatureValues[0].FeatureDefaultValue : "None";
                }

                #endregion

                #region StoreAccess

                FeatureBE StoreAccessfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_storeaccess");

                if (StoreAccessfeature != null)
                {
                    lblStoreAccessHeading.Text = StoreAccessfeature.FeatureAlias;
                    lblStoreAccess.Text = StoreAccessfeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue; ;
                }

                #endregion

                #region Store Type

                FeatureBE StoreTypefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_storetype");

                if (StoreTypefeature != null)
                {
                    lblStoreTypeHeading.Text = StoreTypefeature.FeatureAlias;
                    lblStoreType.Text = StoreTypefeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue; ;
                }

                #endregion

                #region Minimum Password Length

                FeatureBE MinimumPaswordLengthfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength");

                if (MinimumPaswordLengthfeature != null)
                {
                    lblMinimumPasswordLengthHeading.Text = MinimumPaswordLengthfeature.FeatureAlias;
                    lblMinimumPasswordLength.Text = MinimumPaswordLengthfeature.FeatureValues[0].IsEnabled ? MinimumPaswordLengthfeature.FeatureValues[0].FeatureDefaultValue : "NA";
                }

                #endregion

                #region Password Expression

                FeatureBE PasswordExpressionfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordexpression");

                if (PasswordExpressionfeature != null)
                    lblPasswordExpression.Text = PasswordExpressionfeature.FeatureValues[0].IsEnabled ? "Yes" : "No";

                #endregion

                #region Force Password Change

                FeatureBE ForcePasswordChangefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_forcepasswordchange");

                if (ForcePasswordChangefeature != null)
                    lblForcePassword.Text = ForcePasswordChangefeature.FeatureValues[0].IsEnabled ? "Yes" : "No";

                #endregion

                #region Password Validity

                FeatureBE PasswordValidityfeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordvalidity");

                if (PasswordValidityfeature != null)
                    lblPasswordValidity.Text = !string.IsNullOrEmpty(PasswordValidityfeature.FeatureValues[0].FeatureDefaultValue) ? PasswordValidityfeature.FeatureValues[0].FeatureDefaultValue : "NA";

                #endregion

                #region Enable Captcha

                FeatureBE EnableCaptchafeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_enablecaptcha");

                if (EnableCaptchafeature != null)
                {
                    lblEnableCaptchaHeading.Text = EnableCaptchafeature.FeatureAlias;
                    lblEnableCaptcha.Text = EnableCaptchafeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Enable Cookie

                FeatureBE EnableCookiefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_enablecookie");

                if (EnableCookiefeature != null)
                {
                    lblEnableCookieHeading.Text = EnableCookiefeature.FeatureAlias;
                    lblEnableCookie.Text = EnableCookiefeature.FeatureValues[0].IsEnabled ? "Yes" : "No";
                }

                #endregion

                #region Cookie Lifetime

                FeatureBE CookieLifetimefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_cookielifetime");

                if (CookieLifetimefeature != null)
                {
                    lblCookieLifetimeHeading.Text = CookieLifetimefeature.FeatureAlias;
                    lblCookieLifetime.Text = CookieLifetimefeature.FeatureValues[0].IsEnabled ? CookieLifetimefeature.FeatureValues[0].FeatureDefaultValue : "NA";
                }

                #endregion

                #region Cookie Message

                FeatureBE CookieMessagefeature = StoreInstance.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_cookiemessage");

                if (CookieMessagefeature != null)
                {
                    lblCookieMessageHeading.Text = CookieMessagefeature.FeatureAlias;
                    lblCookieMessage.Text = CookieMessagefeature.FeatureValues[0].IsEnabled ? CookieMessagefeature.FeatureValues[0].FeatureDefaultValue : "NA";
                }

                #endregion
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Create Store

    protected void cmdCreateStore_Click(object sender, EventArgs e)
    {
        CreateStore();
    }

    private void CreateStore()
    {
        try
        {
            StoreInstance = (StoreBE)Session["store"];
            string DatabasePassword = string.Empty;
            bool IsStoreCreated = CreateSite(Convert.ToString(ConfigurationManager.AppSettings["DefaultDBName"]), StoreInstance.StoreName.Trim(), ref DatabasePassword);
            Session["AllStores"] = null;
            Session["SortedView"] = null;
            if (IsStoreCreated)
            {
                UserBE user = (UserBE)Session["User"];
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("IsCreated", Convert.ToString(1));
                DictionaryInstance.Add("StatusId", Convert.ToString(Convert.ToByte(StoreStatus.Ready)));
                DictionaryInstance.Add("DBPassword", Convert.ToString(DatabasePassword));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreInstance.StoreId));
                bool IsUpdated = StoreBL.UpdateIsStoreCreated(DictionaryInstance);
                StoreBL.UpdateNewlyCreatedStoreDetails(DictionaryInstance, StoreInstance.StoreName);
                Session["PreviousPage"] = null;
                Session["StoreId"] = null;
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Store created successfully", Convert.ToString(ConfigurationManager.AppSettings["ImgURL"]) + "DashBoard.aspx", AlertType.Success);
            }
            else
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while creating store", AlertType.Failure);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Edit/Cancel Store

    protected void cmdEditStore_Click(object sender, EventArgs e)
    {
        try
        {
            StoreInstance = (StoreBE)Session["store"];
            Session["PreviousPage"] = "step4";
            Session["StoreId"] = StoreInstance.StoreId;
            Response.Redirect("~/StoreCreator/Step1.aspx?m=edit&id=" + StoreInstance.StoreId + "");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Session["PreviousPage"] = null;
            Session["StoreId"] = null;
            Response.Redirect("~/DashBoard.aspx");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Events

    protected void rptDisplayType_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FeatureBE.FeatureValueBE FeatureValueInstance = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                Label lblFeature = (Label)e.Item.FindControl("lblFeature");
                Label lblFeatureValue = (Label)e.Item.FindControl("lblFeatureValue");
                lblFeature.Text = FeatureValueInstance.FeatureValue;
                lblFeatureValue.Text = FeatureValueInstance.IsEnabled ? "Yes" : "No";
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Create Database

    private bool CreateDatabase(string strSiteName, ref string DatabasePassword)
    {
        bool isStoreDetailsTransfered = false;
        DataSet dsGetStoreDetails;
        try
        {
            Dictionary<string, string> CreateDatabaseDictionaryInstance = new Dictionary<string, string>();
            CreateDatabaseDictionaryInstance.Add("BackupDBName", Convert.ToString(ConfigurationManager.AppSettings["BackDBName"]));
            CreateDatabaseDictionaryInstance.Add("BackupFilePath", Convert.ToString(ConfigurationManager.AppSettings["DBBackupPath"]) + Convert.ToString(ConfigurationManager.AppSettings["BackDBName"]) + ".bak");
            CreateDatabaseDictionaryInstance.Add("RestoreDBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strSiteName.Trim().Replace(" ", ""));
            CreateDatabaseDictionaryInstance.Add("RestoreFilePath", Convert.ToString(ConfigurationManager.AppSettings["DBBackupPath"]));

            StoreBL.CreateDatabase(CreateDatabaseDictionaryInstance);
            dsGetStoreDetails = StoreBL.GetMCPStoreDetails(strSiteName);
            Exceptions.WriteInfoLog("database created");
            Exceptions.WriteInfoLog("Before executing script for transfer data");
            isStoreDetailsTransfered = StoreBL.ExecuteScript(dsGetStoreDetails, strSiteName.Trim().Replace(" ", ""));
            Exceptions.WriteInfoLog("After executing script for transfer data");
            DatabasePassword = StoreBL.CreateDBUser(strSiteName.Trim().Replace(" ", ""));
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return isStoreDetailsTransfered;
    }

    #endregion

    #region Create Virtual Directory

    public bool CreateSite(string strBaseSiteName, string strSiteName, ref string DatabasePassword)
    {
        Exceptions.WriteInfoLog("Enter In CreateSite Method");
        bool blnCreateStore = false;
        try
        {
            string strWebsiteHostPath = "";

            strWebsiteHostPath = ConfigurationManager.AppSettings["WebsiteHostPath"];
            //will validate if site or database with specified name exists
            Exceptions.WriteInfoLog("Before Calling IsSiteExists Method");
            if (IsSiteExists(strSiteName, strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName)) == SiteElements.None)
            {
                Exceptions.WriteInfoLog("After Calling IsSiteExists Method");
                //STEP:1 ==> Copy already published site
                Exceptions.WriteInfoLog("Before Calling CopyDirectory Method");
                bool blnCopySite = GlobalFunctions.CopyDirectory(strWebsiteHostPath.Trim().Replace("$StoreName$", strBaseSiteName), strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName), true);
                Exceptions.WriteInfoLog("After Calling CopyDirectory Method, blnCopySite-> " + Convert.ToString(blnCopySite));
                if (blnCopySite == true)
                {
                    Exceptions.WriteInfoLog("Before Calling CopyDirectoryTemmplate Method");
                    //bool blnCopyTemplate = GlobalFunctions.CopyDirectory(GlobalFunctions.GetPhysicalFolderPath() + @"\BAStore", strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName), true);
                    bool blnCopyTemplate = GlobalFunctions.CopyDirectory(GlobalFunctions.GetPhysicalFolderPath() + @"\Templates\" + Path.GetFileNameWithoutExtension(hdnTemplateName.Value), strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName), true);
                    Exceptions.WriteInfoLog("After Calling CopyDirectoryTemmplate Method, blnCopyTemplate-> " + Convert.ToString(blnCopyTemplate));
                    AssignAccessRights(strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName));
                    if (blnCopyTemplate)
                    {
                        //STEP:2 ==> Create DataBase
                        bool blnDatabaseCreated = false;
                        try
                        {
                            Exceptions.WriteInfoLog("Before Calling CreateDatabase Method");
                            blnDatabaseCreated = CreateDatabase(strSiteName, ref DatabasePassword);
                            Exceptions.WriteInfoLog("After Calling CreateDatabase Method");
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            throw ex;
                        }

                        if (blnDatabaseCreated)
                        {
                            //STEP:9 ==> Replace Connection String Of Newly Created Site
                            Exceptions.WriteInfoLog("Before Calling ReplaceConnectionStringInCreatedSite Method");
                            bool blnReplaceConnectionStringInCreatedSite = ReplaceConnectionStringInCreatedSite(strBaseSiteName, strSiteName, DatabasePassword);
                            Exceptions.WriteInfoLog("After Calling ReplaceConnectionStringInCreatedSite Method, blnReplaceConnectionStringInCreatedSite -> " + Convert.ToString(blnReplaceConnectionStringInCreatedSite));

                            if (blnReplaceConnectionStringInCreatedSite)
                            {
                                //STEP:10 ==> Change ApplicationConstants File Of Newly Created Site
                                bool blnUpdateConstantsInCreatedSite = true;// UpdateConstantsInCreatedSite(baseStoreId, strBaseSiteName, strSiteName, createdStoreId, strStoreURL, strCurrencySymbol);
                                if (blnUpdateConstantsInCreatedSite == true)
                                {
                                    //Work Here For VD Or WebSite
                                    string strHostingServer = Convert.ToString(GlobalFunctions.GetSetting("CS_STORESMANAGEMENT_SITEHOSTINGSERVERNAME"));
                                    string strPhysicalPath = strWebsiteHostPath.Replace("$StoreName$", strSiteName);

                                    //STEP : 11 ==> Cteate Virtual Directory Or Web Site In IIS
                                    Exceptions.WriteInfoLog("Before Calling CreateVirtualDirectoryORDWebSiteInIIS Method");
                                    bool blnCreateVirtualDirectoryORDWebSiteInIIS = CreateVirtualDirectoryORDWebSiteInIIS(strHostingServer, strPhysicalPath, strSiteName);
                                    Exceptions.WriteInfoLog("After Calling CreateVirtualDirectoryORDWebSiteInIIS Method, blnCreateVirtualDirectoryORDWebSiteInIIS -> " + Convert.ToString(blnCreateVirtualDirectoryORDWebSiteInIIS));
                                    if (blnCreateVirtualDirectoryORDWebSiteInIIS == true)
                                    {
                                        blnCreateStore = true;
                                    }
                                    else
                                    {
                                        StoreBL.DropDatabase(strSiteName);
                                        DeleteSite(strSiteName);
                                    }
                                }
                            }
                            else
                            {
                                StoreBL.DropDatabase(strSiteName);
                                DeleteSite(strSiteName);
                            }
                        }
                        else
                        {
                            DeleteSite(strSiteName);
                        }
                    }
                }
                else
                {
                    Response.Write("Error while copying the directory base folder");
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }

        return blnCreateStore;
    }

    private SiteElements IsSiteExists(string siteFolderName, string strWebsiteHostPath)
    {
        SiteElements enmSiteElements = SiteElements.None;
        try
        {
            bool blnIsSiteFolderExists = false;
            bool blnIsSiteDatabaseExists = false;

            if (!string.IsNullOrEmpty(strWebsiteHostPath.Trim()) && !string.IsNullOrEmpty(siteFolderName.Trim()))
                blnIsSiteFolderExists = IsSiteFolderExists(strWebsiteHostPath.Trim()) ? true : false;

            blnIsSiteDatabaseExists = StoreBL.IsSiteDatabaseExists(siteFolderName.Trim()) ? true : false;

            if (blnIsSiteFolderExists && blnIsSiteDatabaseExists)
                enmSiteElements = SiteElements.SiteAndDatabase;
            else if (blnIsSiteFolderExists)
                enmSiteElements = SiteElements.Site;
            else if (blnIsSiteDatabaseExists)
                enmSiteElements = SiteElements.Database;
            else
                enmSiteElements = SiteElements.None;

            switch (enmSiteElements)
            {
                case SiteElements.Site:
                    GlobalFunctions.ShowModalAlertMessages(Page, "Folder/Virtual Directory with specified site name already exists, delete the folder and try again", AlertType.Failure);
                    break;
                case SiteElements.Database:
                    GlobalFunctions.ShowModalAlertMessages(Page, "Database with specified site name already exists, delete the database and try again", AlertType.Failure);
                    break;
                case SiteElements.SiteAndDatabase:
                    GlobalFunctions.ShowModalAlertMessages(Page, "Folder/Virtual Directory and Database with specified site name already exists, delete the folder and database and try again", AlertType.Failure);
                    break;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }

        return enmSiteElements;
    }

    private bool IsSiteFolderExists(string strWebsiteHostPath)
    {
        try
        {
            if (Directory.Exists(strWebsiteHostPath) == true)
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }

    }

    private bool ReplaceConnectionStringInCreatedSite(string BaseStoreName, string SiteName, string DatabasePassword)
    {
        bool blnResult = false;

        //will alter connection string in web.config file of just created site.
        StreamWriter objStreamWriter;
        StreamReader objStreamReader;
        string strFileContent = "";
        string strConfigurationFile = "";

        strConfigurationFile = Convert.ToString(ConfigurationManager.AppSettings["WebsiteHostPath"]).Replace("$StoreName$", SiteName) + "\\web.config";
        try
        {
            objStreamReader = new StreamReader(strConfigurationFile);

            //read the file
            if (objStreamReader != null)
                strFileContent = objStreamReader.ReadToEnd();

            objStreamReader.Close();
            objStreamReader = null;

            if (BaseStoreName == "CorporateStoreTemplate")
                BaseStoreName = BaseStoreName.ToLower();

            strFileContent = strFileContent.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + SiteName);
            strFileContent = strFileContent.Replace("$StoreName$", SiteName);
            strFileContent = strFileContent.Replace("$UserName$", SiteName);
            strFileContent = strFileContent.Replace("$Password$", DatabasePassword);

            //code to uncheck file from read only
            FileInfo objFS = new FileInfo(strConfigurationFile);
            bool blnWasReadOnly = false;
            if (objFS.IsReadOnly == true)
            {
                objFS.IsReadOnly = false;
                blnWasReadOnly = true;
            }//end of if (objFS.IsReadOnly == true)
            //*************************************

            objStreamWriter = new StreamWriter(strConfigurationFile);

            objStreamWriter.Write(strFileContent);
            objStreamWriter.Flush();

            objStreamWriter.Close();
            objStreamWriter = null;

            //*************************************
            //code to make file read only, if it was
            if (blnWasReadOnly == true)
            {
                objFS.IsReadOnly = true;
            }//end of if (blnWasReadOnly == true)
            objFS = null;
            //*************************************

            blnResult = true;

        }//end of try
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            blnResult = false;
        }//end of catch
        finally
        {
            objStreamWriter = null;
            objStreamReader = null;
            strFileContent = null;
            strConfigurationFile = null;
        }//end of finally

        return blnResult;
    }

    private bool CreateVirtualDirectoryORDWebSiteInIIS(string serverName, string physicalPath, string siteName)
    {
        Exceptions.WriteInfoLog("Enter in CreateVirtualDirectoryORDWebSiteInIIS Method");
        bool blnCreateVirtualDirectoryORDWebSiteInIIS = false;
        try
        {
            // Create new Website Info object To Represent Instance Of WebSite
            WebSiteInfo objWebSiteInfo = new WebSiteInfo();
            string strSiteUrl = Convert.ToString(GlobalFunctions.GetSetting("CS_STORESMANAGEMENT_DEFAULTDOMAINNAME")) + "/" + siteName;

            // Assign values to object
            objWebSiteInfo.ServerName = serverName;
            objWebSiteInfo.PhysicalPath = physicalPath;

            //Virtual Directory Option Is Selected...                
            string strURL = strSiteUrl.Substring(0, strSiteUrl.IndexOf('/'));
            objWebSiteInfo.ParentWebSiteName = strURL;
            objWebSiteInfo.DirectoryName = siteName;

            // Assign Access rights to New Web Site
            objWebSiteInfo.HasBrowseAccess = true;
            objWebSiteInfo.HasReadAccess = true;
            objWebSiteInfo.HasWriteAccess = false;
            objWebSiteInfo.HasExecuteAccess = true;
            objWebSiteInfo.IsAnonymousAccessAllow = true;
            objWebSiteInfo.IsBasicAuthenticationSet = false;
            objWebSiteInfo.IsNTLMAuthenticationSet = false;
            Exceptions.WriteInfoLog("Before Calling CreateVirtualDirectory Method");
            blnCreateVirtualDirectoryORDWebSiteInIIS = IISHelper.CreateVirtualDirectory(objWebSiteInfo, IISHelper.IISVirsion.IIS6);
            Exceptions.WriteInfoLog("After Calling CreateVirtualDirectory Method");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            blnCreateVirtualDirectoryORDWebSiteInIIS = false;
        }
        return blnCreateVirtualDirectoryORDWebSiteInIIS;
    }

    private bool DeleteSite(string strSiteName)
    {
        bool blnDeleteSite = false;
        try
        {
            string strWebsiteHostPath = Convert.ToString(ConfigurationManager.AppSettings["WebsiteHostPath"]).Replace("$StoreName$", strSiteName);
            string strParentFolderName = Convert.ToString(GlobalFunctions.GetSetting("CS_STORESMANAGEMENT_FULLYQUALIFIEDPARENTFOLDERNAME"));

            if (strWebsiteHostPath.Substring(strWebsiteHostPath.LastIndexOf('\\')).ToLower() == strParentFolderName.ToLower() == false)
                if (Directory.Exists(strWebsiteHostPath))
                {
                    //This will assign the access right to directory
                    DirectorySecurity objDirectorySecurity = null;
                    FileSystemAccessRule objFileSystemAccessRule = null;
                    DirectoryInfo objDirectoryInfo = new DirectoryInfo(strWebsiteHostPath);
                    //*************************************************************
                    //code to add the access level for folder and its contents
                    //this is done so that it can be deleted programmetically
                    objDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
                    objFileSystemAccessRule = new FileSystemAccessRule("everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);

                    objDirectorySecurity = objDirectoryInfo.GetAccessControl(AccessControlSections.Access);
                    objDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
                    objDirectoryInfo.SetAccessControl(objDirectorySecurity);

                    objDirectorySecurity = null;
                    objFileSystemAccessRule = null;
                    //*************************************************************
                    //Remove Read Only Attribute Of ApplicationConstants.Config File
                    string strAppConstFilePath = strWebsiteHostPath + "/" + "applicationConstants.config";
                    if (File.Exists(strAppConstFilePath))
                    {
                        FileInfo objFileInfo = new FileInfo(strAppConstFilePath);
                        if (objFileInfo.IsReadOnly)
                            objFileInfo.IsReadOnly = false;
                    }
                    //Actual Delete
                    Directory.Delete(strWebsiteHostPath, true);
                    blnDeleteSite = true;
                }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return blnDeleteSite;
    }

    private void AssignAccessRights(string WebsitePath)
    {
        try
        {
            string CSSFolderPath = WebsitePath + @"\CSS";
            string HomeFolderPath = WebsitePath + @"\Home";
            string BackupFolderPath = WebsitePath + @"\Backup";
            string ImagesFolderPath = WebsitePath + @"\Images";
            string MasterFolderPath = WebsitePath + @"\Master";
            string CSVFolderPath = WebsitePath + @"\CSV";
            string FontsUploadFolderPath = WebsitePath + @"\FontsUpload";
            string FontsFolderPath = WebsitePath + @"\Fonts";
            string SearchFolderPath = WebsitePath + @"\Search";
            string ExceptionLogsFolderPath = WebsitePath + @"\ExceptionLogs";
            string ProductTranslationFolderPath = WebsitePath + @"\ProductTranslation";
            string AdminImagesFolderPath = WebsitePath + @"\Admin\Images";
            string AdminSettingsFolderPath = WebsitePath + @"\Admin\Settings";
            string XMLFolderPath = WebsitePath + @"\XML";
            string JSFolderPath = WebsitePath + @"\JS";
            string RobotFolderPath = WebsitePath;

            DirectoryInfo objCSSDirectoryInfo = new DirectoryInfo(CSSFolderPath);
            DirectoryInfo objHomeDirectoryInfo = new DirectoryInfo(HomeFolderPath);
            DirectoryInfo objBackupDirectoryInfo = new DirectoryInfo(BackupFolderPath);
            DirectoryInfo objImagesDirectoryInfo = new DirectoryInfo(ImagesFolderPath);
            DirectoryInfo objMasterDirectoryInfo = new DirectoryInfo(MasterFolderPath);
            DirectoryInfo objCSVDirectoryInfo = new DirectoryInfo(CSVFolderPath);
            DirectoryInfo objFontsUploadDirectoryInfo = new DirectoryInfo(FontsUploadFolderPath);
            DirectoryInfo objFontsDirectoryInfo = new DirectoryInfo(FontsFolderPath);
            DirectoryInfo objSearchDirectoryInfo = new DirectoryInfo(SearchFolderPath);
            DirectoryInfo objExceptionLogsDirectoryInfo = new DirectoryInfo(ExceptionLogsFolderPath);
            DirectoryInfo objProductTranslationDirectoryInfo = new DirectoryInfo(ProductTranslationFolderPath);
            DirectoryInfo objAdminImagesDirectoryInfo = new DirectoryInfo(AdminImagesFolderPath);
            DirectoryInfo objAdminSettingsDirectoryInfo = new DirectoryInfo(AdminSettingsFolderPath);
            DirectoryInfo objXMLDirectoryInfo = new DirectoryInfo(XMLFolderPath);
            DirectoryInfo objJSDirectoryInfo = new DirectoryInfo(JSFolderPath);
            DirectoryInfo objRobotDirectoryInfo = new DirectoryInfo(RobotFolderPath);


            DirectorySecurity objCSSDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objHomeDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objBackupDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objImagesDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objMasterDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objCSVDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objFontsUploadDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objFontsDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objSearchDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objExceptionLogsDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objProductTranslationDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objAdminImagesDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objAdminSettingsDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objXMLDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objJSDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objRobotDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();


            FileSystemAccessRule objFileSystemAccessRule = new FileSystemAccessRule("IIS_IUSRS", FileSystemRights.Read | FileSystemRights.ReadAndExecute | FileSystemRights.Write | FileSystemRights.Modify, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);

            objCSSDirectorySecurity = objCSSDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objHomeDirectorySecurity = objHomeDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objBackupDirectorySecurity = objBackupDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objImagesDirectorySecurity = objImagesDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objMasterDirectorySecurity = objMasterDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objCSVDirectorySecurity = objCSVDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objFontsUploadDirectorySecurity = objFontsUploadDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objFontsDirectorySecurity = objFontsDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objSearchDirectorySecurity = objSearchDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objExceptionLogsDirectorySecurity = objExceptionLogsDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objProductTranslationDirectorySecurity = objProductTranslationDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objAdminImagesDirectorySecurity = objAdminImagesDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objAdminSettingsDirectorySecurity = objAdminSettingsDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objXMLDirectorySecurity = objXMLDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objJSDirectorySecurity = objJSDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objRobotDirectorySecurity = objRobotDirectoryInfo.GetAccessControl(AccessControlSections.Access);

            objCSSDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objHomeDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objBackupDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objImagesDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objMasterDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objCSVDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objFontsUploadDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objFontsDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objSearchDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objExceptionLogsDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objProductTranslationDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objAdminImagesDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objAdminSettingsDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objXMLDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objJSDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objRobotDirectorySecurity.AddAccessRule(objFileSystemAccessRule);

            objCSSDirectoryInfo.SetAccessControl(objCSSDirectorySecurity);
            objHomeDirectoryInfo.SetAccessControl(objHomeDirectorySecurity);
            objBackupDirectoryInfo.SetAccessControl(objBackupDirectorySecurity);
            objImagesDirectoryInfo.SetAccessControl(objImagesDirectorySecurity);
            objMasterDirectoryInfo.SetAccessControl(objMasterDirectorySecurity);
            objCSVDirectoryInfo.SetAccessControl(objCSVDirectorySecurity);
            objFontsUploadDirectoryInfo.SetAccessControl(objFontsUploadDirectorySecurity);
            objFontsDirectoryInfo.SetAccessControl(objFontsDirectorySecurity);
            objSearchDirectoryInfo.SetAccessControl(objSearchDirectorySecurity);
            objExceptionLogsDirectoryInfo.SetAccessControl(objExceptionLogsDirectorySecurity);
            objProductTranslationDirectoryInfo.SetAccessControl(objProductTranslationDirectorySecurity);
            objAdminImagesDirectoryInfo.SetAccessControl(objAdminImagesDirectorySecurity);
            objAdminSettingsDirectoryInfo.SetAccessControl(objAdminSettingsDirectorySecurity);
            objXMLDirectoryInfo.SetAccessControl(objXMLDirectorySecurity);
            objJSDirectoryInfo.SetAccessControl(objJSDirectorySecurity);
            objRobotDirectoryInfo.SetAccessControl(objRobotDirectorySecurity);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

}