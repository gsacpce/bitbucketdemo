﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Collections;

public partial class StoreCreator_ViewStore : System.Web.UI.Page
{
    List<UserBE> GetAllUsers;
    //public string StoreVersion = GlobalFunctions.GetSetting("StoreVersion");
    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized(HttpContext.Current.Request.Url.Segments[HttpContext.Current.Request.Url.Segments.Length - 1]);
        if (!IsPostBack)
        {
            BindAllStores();
        }
    }

    #endregion

    #region Get All Stores

    private void BindAllStores()
    {
        try
        {
            GetAllUsers = UserBL.GetMCPUsers();
            List<StoreBE> StoreCollection = StoreBL.GetAllStoreDetails();
            StoreCollection.RemoveAll(x => x.StoreId == 337);
            gvAllStores.DataSource = StoreCollection;
            lblTotal.Text = Convert.ToString(StoreCollection.Count);
            gvAllStores.DataBind();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Sorting/Paging Events

    protected void gvAllStores_DataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                StoreBE store = (StoreBE)e.Row.DataItem;
                Label lblDefaultLanguage = (Label)e.Row.FindControl("lblDefaultLanguage");
                Label lblDefaultCurrency = (Label)e.Row.FindControl("lblDefaultCurrency");
                Label lblStoreStatus = (Label)e.Row.FindControl("lblStoreStatus");
                LinkButton lnkStoreDetail = (LinkButton)e.Row.FindControl("lnkStoreDetail");
                HtmlAnchor lnkStoreAdmin = (HtmlAnchor)e.Row.FindControl("lnkStoreAdmin");
                HtmlAnchor lnkStoreFrontEnd = (HtmlAnchor)e.Row.FindControl("lnkStoreFrontEnd");
                ImageButton cmdRunDTS = (ImageButton)e.Row.FindControl("cmdRunDTS");
                HiddenField hdnStoreId = (HiddenField)e.Row.FindControl("hdnStoreId");
                HtmlGenericControl dvRunDTS = (HtmlGenericControl)e.Row.FindControl("dvRunDTS");
                Label lblCreatedBy = (Label)e.Row.FindControl("lblCreatedBy");
                Label lblVersion = (Label)e.Row.FindControl("lblVersion");
               // lblVersion.Text = StoreVersion;
                hdnStoreId.Value = Convert.ToString(store.StoreId);
                UserBE user = (UserBE)Session["User"];
                if (GetAllUsers != null)
                {
                    if (GetAllUsers.Count > 0)
                    {
                        UserBE userBE = GetAllUsers.FirstOrDefault(x => x.UserId == store.CreatedBy);
                        lblCreatedBy.Text = string.Concat(userBE.FirstName, " ", userBE.LastName);
                    }
                        
                }
                
                if (store.IsCreated)
                {
                    lnkStoreDetail.Style.Add("display", "none");
                    lnkStoreAdmin.Style.Add("display", "");
                    lnkStoreAdmin.Target = "_blank";
                    lnkStoreFrontEnd.Style.Add("display", "");
                    lnkStoreFrontEnd.Target = "_blank";
                    //lnkStoreAdmin.HRef = store.StoreLink + "/Admin/Login/Login.aspx?source=mcp";
                    //UserBE user = (UserBE)Session["User"];
                    Hashtable hsTable = new Hashtable();
                    hsTable.Add("email",user.EmailId);
                    hsTable.Add("StoreId",store.StoreId);
                    lnkStoreAdmin.HRef = GlobalFunctions.EncryptURL(hsTable, store.StoreLink + "/Admin/Login/Login.aspx?");
                    lnkStoreFrontEnd.HRef = store.StoreLink;
                    if (store.IsBASYS)
                        dvRunDTS.Style.Add("display","");
                    else
                        dvRunDTS.Style.Add("display", "none");
                }
                else
                {
                    lnkStoreDetail.Style.Add("display", "");
                    //lnkStoreDetail.HRef = "~/StoreCreator/Step1.aspx?m=edit&id=" + store.StoreId + "";
                    lnkStoreDetail.CommandArgument = Convert.ToString(store.StoreId);
                    lnkStoreAdmin.Style.Add("display", "none");
                    lnkStoreFrontEnd.Style.Add("display", "none");
                    dvRunDTS.Style.Add("display", "none");
                }

                Button cmdLanguageCount = (Button)e.Row.FindControl("cmdLanguageCount");
                cmdLanguageCount.Text = store.StoreLanguages.Count.ToString();
                string languages = "";
                string Header = store.StoreLanguages.Count > 1 ? " Languages" : " Language";
                foreach (StoreBE.StoreLanguageBE item in store.StoreLanguages)
                {
                    languages += item.LanguageName + "<br>";
                }

                cmdLanguageCount.Attributes.Add("title", store.StoreLanguages.Count + Header);
                cmdLanguageCount.Attributes.Add("data-content", languages);

                Button cmdCurrencyCount = (Button)e.Row.FindControl("cmdCurrencyCount");
                cmdCurrencyCount.Text = store.StoreCurrencies.Count.ToString();
                string currencies = "";
                string CurrencyHeader = store.StoreCurrencies.Count > 1 ? " Currencies" : " Currency";
                foreach (StoreBE.StoreCurrencyBE item in store.StoreCurrencies)
                {
                    currencies += item.CurrencyName + "<br>";
                }

                cmdCurrencyCount.Attributes.Add("title", store.StoreCurrencies.Count + CurrencyHeader);
                cmdCurrencyCount.Attributes.Add("data-content", currencies);

                lblDefaultLanguage.Text = store.StoreLanguages.FirstOrDefault(x => x.IsDefault == true).LanguageName;
                lblDefaultCurrency.Text = store.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyName;
                lblStoreStatus.Text = ((StoreStatus)store.StatusId).ToString().Replace("_", " ");
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void gvAllStores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvAllStores.PageIndex = e.NewPageIndex;
            if (Session["SortedView"] != null)
            {
                gvAllStores.DataSource = (List<StoreBE>)Session["SortedView"];
                gvAllStores.DataBind();
            }
            else
            {
                BindAllStores();
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void gvAllStores_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
                GetAllUsers = UserBL.GetMCPUsers();
            if (direction == SortDirection.Ascending)
                direction = SortDirection.Descending;
            else
                direction = SortDirection.Ascending;

            List<StoreBE> StoreCollection = StoreBL.GetAllStoreDetails();
            StoreCollection.RemoveAll(x => x.StoreId == 337);
            switch (direction)
            {
                case SortDirection.Ascending:
                    switch (e.SortExpression.ToLower())
                    {
                        case "storename": Session["SortedView"] = StoreCollection.OrderBy(x => x.StoreName).ToList();
                            break;
                        case "createddate": Session["SortedView"] = StoreCollection.OrderBy(x => x.CreatedDate).ToList();
                            break;
                        default: Session["SortedView"] = StoreCollection.OrderBy(x => x.StoreName).ToList();
                            break;
                    }
                    break;
                case SortDirection.Descending:
                    switch (e.SortExpression.ToLower())
                    {
                        case "storename": Session["SortedView"] = StoreCollection.OrderByDescending(x => x.StoreName).ToList();
                            break;
                        case "createddate": Session["SortedView"] = StoreCollection.OrderByDescending(x => x.CreatedDate).ToList();
                            break;
                        default: Session["SortedView"] = StoreCollection.OrderByDescending(x => x.StoreName).ToList();
                            break;
                    }
                    break;
                default:
                    break;
            }

            gvAllStores.DataSource = (List<StoreBE>)Session["SortedView"];
            gvAllStores.DataBind();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    public SortDirection direction
    {
        get
        {
            if (ViewState["directionState"] == null)
            {
                ViewState["directionState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["directionState"];
        }
        set
        {
            ViewState["directionState"] = value; 
        }
    }

    protected void gvAllStores_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GetAllUsers = UserBL.GetMCPUsers();
        switch (e.CommandName.Trim().ToLower())
        {
            case "rundts":
                UserBE user = (UserBE)Session["User"];
                bool IsTransfered = false;
                try
                {
                    HiddenField hdnStoreId = (HiddenField)((GridViewRow)((ImageButton)e.CommandSource).NamingContainer).FindControl("hdnStoreId");
                    if (hdnStoreId != null)
                    {
                        if (Convert.ToInt16(hdnStoreId.Value) != 0)
                        {
                            int ProductCount = 0;
                            IsTransfered = DTSSchedulerBL.RunDTS(user.UserId, Convert.ToString(hdnStoreId.Value),ref ProductCount);
                            if (IsTransfered)
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "DTS completed successfully", AlertType.Success);
                            else if (!IsTransfered && ProductCount == 0)
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Products are not available for this catalogue", AlertType.Failure);
                            else
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while running DTS", AlertType.Failure);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }

                break;
            case "storedetail":
                try
                {
                    Session["PreviousPage"] = "viewstore";
                    Session["StoreId"] = e.CommandArgument;
                    Response.Redirect("~/StoreCreator/Step1.aspx?m=edit&id=" + e.CommandArgument + "");
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
                break;
            default: break;
        }
    }

    #endregion

    protected void btnReset_Click(object sender, EventArgs e)
    {
        BindAllStores();
        txtSearchStore.Text = string.Empty;
        Label1.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetAllUsers = UserBL.GetMCPUsers();
            List<StoreBE> StoreCollection = StoreBL.GetAllStoreDetails();
            string strText = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSearchStore.Text.Trim());

            StoreCollection = StoreBL.GetStoreDetailsSearch(strText);
            if (StoreCollection != null)
            {
                if (StoreCollection.Count > 0)
                {
                    gvAllStores.DataSource = StoreCollection;
                    gvAllStores.DataBind();
                }
                else
                {
                    Label1.Visible = true;
                }
            }
            else
            {
                Label1.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
    #region Comment
    protected void lnkStoreFeature_Click(object sender, EventArgs e)
    {

        LinkButton btn = (LinkButton)(sender);
        Int16 iStoreId = Convert.ToInt16(btn.CommandArgument);
        ///Step 2: Session the StoreID
        Session["SID"] = iStoreId;
        BindFeatures();
        ClientScript.RegisterStartupScript(this.GetType(), "ShowModal", "<script>javascript:ShowModal();</script>");
    }

    protected void rptPLDisplayType_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
            HiddenField hdnPLDisplayTypeStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLDisplayTypeStoreFeatureDetailId");
            Label lblIsActivePL = (Label)e.Item.FindControl("lblIsActivePL");
            lblIsActivePL.Text = FeatureValue.FeatureValue;
            lblIsDefaultPL.Text = FeatureValue.FeatureValue;
        }
    }
    protected void rptPLSorting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
            HiddenField hdnPLSortingStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLSortingStoreFeatureDetailId");
            Label lblDefaultSettings = (Label)e.Item.FindControl("lblDefaultSettings");
            lblDefaultSettings.Text = FeatureValue.FeatureValue;
        }
    }
    protected void rptPLFilters_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
            HiddenField hdnPLFilterStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLFilterStoreFeatureDetailId");
            Label chkPLFilters = (Label)e.Item.FindControl("chkPLFilters");
            chkPLFilters.Text = FeatureValue.FeatureValue;
        }
    }
    protected void rptSCPaymentGateway_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            Int16 StoreId = 0;
            StoreId = Convert.ToInt16(Session["SID"].ToString());
            StoreBE objStoreBE = StoreBL.GetStoreDetails(StoreId);
            string strConfigurationFile = "";
            string ServerName = objStoreBE.DBServerName;
            string Database = objStoreBE.DBName;
            string User = objStoreBE.DBUserName;
            string password = objStoreBE.DBPassword;

            strConfigurationFile = Convert.ToString(ConfigurationManager.AppSettings["DynamicDatabaseConnection"]).Replace("$DatabaseServer$", objStoreBE.DBServerName).Replace("$DatabaseName$", Database).Replace("$UserName$", User).Replace("$Password$", password);
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                Label chkSCPaymentGateway = (Label)e.Item.FindControl("chkSCPaymentGateway");
                chkSCPaymentGateway.Text = FeatureValue.FeatureValue;
                ListBox ListBox1 = (ListBox)e.Item.FindControl("ListBox1");
                if (chkSCPaymentGateway.Text == "Adflex")
                {
                    List<StoreBE.StoreCurrencyBE> objStoresCur = StoreBL.GetCurrency(strConfigurationFile);
                    ListBox1.DataSource = objStoresCur.FindAll(x => x.ValidForAdflex == true);
                    if (ListBox1.DataSource != null)
                    {
                        ListBox1.DataTextField = "CurrencyCode";
                        ListBox1.DataBind();
                    }
                    else
                    {
                        ListBox1.Visible = false;
                    }
                }
                else
                {
                    ListBox1.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void BindFeatures()
    { ///Step 3: Get all the Store details (tbl_stores) and filter with StoreID
        //StoreBE StoreBE = StoreBL.GetStoreDetails(iStoreId);
        ///Step 6: The new connection will genereate and use the connection string for the featureDetails.
        //Response.Redirect("ViewStoreFeatures.aspx");
        try
        {
            Int16 StoreId = 0;
            StoreId = Convert.ToInt16(Session["SID"].ToString());
            StoreBE objStoreBE = StoreBL.GetStoreDetails(StoreId);
            if (objStoreBE != null)
            {
                string strConfigurationFile = "";
                string ServerName = objStoreBE.DBServerName;
                string Database = objStoreBE.DBName;
                string User = objStoreBE.DBUserName;
                string password = objStoreBE.DBPassword;

                strConfigurationFile = Convert.ToString(ConfigurationManager.AppSettings["DynamicDatabaseConnection"]).Replace("$DatabaseServer$", objStoreBE.DBServerName).Replace("$DatabaseName$", Database).Replace("$UserName$", User).Replace("$Password$", password);
                List<FeatureBE> GetAllFeatures = FeatureBL.GetAllFeatures(StoreId, strConfigurationFile);
                FeatureBE SS_SearchSetting = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "SS_SearchSetting");
                FeatureBE PL_DisplayType = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "PL_DisplayType");
                FeatureBE PL_PaginationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_pagination");
                FeatureBE PL_DisplaytypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_displaytype");
                FeatureBE PL_SortingFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_sorting");
                FeatureBE PL_QuickviewFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_quickview");
                FeatureBE PL_ProductComparisonFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_compareproducts");
                FeatureBE PL_EnableFilterFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_filter");
                FeatureBE PD_EnablePrintFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableprint");
                FeatureBE PD_EnableDownloadImageFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enabledownloadimage");
                FeatureBE PD_EnableRecentlyViewedProductsFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablerecentlyviewedproducts");
                FeatureBE PD_EnableWishlistFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablewishlist");
                FeatureBE PD_EnableSavePDFFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesavepdf");
                FeatureBE PD_EnableEmailFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableemail");
                FeatureBE PD_EnableSocialMediaFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesocialmedia");
                FeatureBE PD_EnableYouMayAlsoLikeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableyoumayalsolike");
                FeatureBE PD_EnableReviewRatingFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablereviewrating");
                FeatureBE PD_EnableSectionIconsFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesectionicons");
                FeatureBE SS_EnablePoints = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_enablepoints");
                FeatureBE SS_HomeLink = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "SS_HomeLink");
                FeatureBE SC_PaymentGatewayFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_paymentgateway");
                FeatureBE SS_EnableCouponCode = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_enablecouponcode");
                FeatureBE SS_IsPunchoutRegistration = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_ispunchoutregistration");
                FeatureBE OD_AllowMaxOrderQtyPerItemFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.Trim() == "SS_MaxAllowQty");
                FeatureBE OD_AllowBackOrderFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "od_allowbackorder");
                FeatureBE IA_InvoiceAccount = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ia_invoiceaccount");
                FeatureBE SC_EmailMeCopyOfBasket = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_email_me_basket");
                FeatureBE PD_ColorProdEnquiry = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_colorprodenquiry");
                FeatureBE CR_StoreAccessFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_storeaccess");
                FeatureBE CR_DomainValidationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation");
                FeatureBE CR_EmailValidationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation");
                FeatureBE PS_MinimumPasswordLengthFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength");
                FeatureBE PS_PasswordPolicyFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy");
                FeatureBE CR_StoreTypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_storetype");
                FeatureBE SC_GiftCertificate = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_giftcertificate");
                FeatureBE CL_EnableCookieFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_enablecookie");
                if (SS_SearchSetting != null)
                {
                    LblSearchSettings.Text = Convert.ToString(SS_SearchSetting.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue);
                }

                if (PL_DisplaytypeFeature != null)
                {
                    lblIsDefaultPL.Text = Convert.ToString(PL_DisplaytypeFeature.FeatureValues.FirstOrDefault(x => x.IsDefault == true).FeatureValue);
                    rptPLDisplayType.DataSource = PL_DisplaytypeFeature.FeatureValues.FindAll(x => x.IsEnabled == true);
                    rptPLDisplayType.DataBind();
                }
                if (PL_PaginationFeature != null)
                {
                    lblPagination.Text = Convert.ToString(PL_PaginationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue);
                }
                if (PL_SortingFeature != null)
                {
                    lblDefaultSettingsIsDefault.Text = Convert.ToString(PL_SortingFeature.FeatureValues.FirstOrDefault(x => x.IsDefault == true).FeatureValue);
                    rptPLSorting.DataSource = PL_SortingFeature.FeatureValues.FindAll(x => x.IsEnabled == true);
                    rptPLSorting.DataBind();
                }
                if (PL_QuickviewFeature != null)
                {
                    LblQuickWatch.Text = PL_QuickviewFeature.FeatureAlias;
                    bool a = PL_QuickviewFeature.FeatureValues[0].IsEnabled;
                    if (a == true)
                    {
                        LblQuickWatch.Visible = true;
                    }
                    else
                    {
                        LblQuickWatch.Visible = false;
                    }
                }
                if (PL_ProductComparisonFeature != null)
                {
                    lblProductComp.Text = PL_ProductComparisonFeature.FeatureAlias;
                    bool b = PL_ProductComparisonFeature.FeatureValues[0].IsEnabled;
                    if (b == true)
                    {
                        lblProductComp.Visible = true;
                    }
                    else
                    {
                        lblProductComp.Visible = false;
                    }
                }
                if (PL_EnableFilterFeature != null)
                {
                    rptPLFilters.DataSource = PL_EnableFilterFeature.FeatureValues.FindAll(x => x.IsEnabled == true);
                    rptPLFilters.DataBind();
                }
                if (PD_EnablePrintFeature != null)
                {
                    chkPDPrint.Text = PD_EnablePrintFeature.FeatureAlias;
                    bool c = PD_EnablePrintFeature.FeatureValues[0].IsEnabled;
                    if (c == true)
                    {
                        chkPDPrint.Visible = true;
                    }
                    else
                    {
                        chkPDPrint.Visible = false;
                    }
                }
                if (PD_EnableDownloadImageFeature != null)
                {
                    chkPDDownloadImage.Text = PD_EnableDownloadImageFeature.FeatureAlias;
                    bool d = PD_EnableDownloadImageFeature.FeatureValues[0].IsEnabled;
                    if (d == true)
                    {
                        chkPDDownloadImage.Visible = true;
                    }
                    else
                    {
                        chkPDDownloadImage.Visible = false;
                    }
                }
                if (PD_EnableRecentlyViewedProductsFeature != null)
                {
                    chkPDRecentlyViewedProducts.Text = PD_EnableRecentlyViewedProductsFeature.FeatureAlias;
                    bool e = PD_EnableRecentlyViewedProductsFeature.FeatureValues[0].IsEnabled;
                    if (e == true)
                    {
                        chkPDRecentlyViewedProducts.Visible = true;
                    }
                    else
                    {
                        chkPDRecentlyViewedProducts.Visible = false;
                    }
                }
                if (PD_EnableWishlistFeature != null)
                {
                    chkPDWishlist.Text = PD_EnableWishlistFeature.FeatureAlias;
                    bool f = PD_EnableWishlistFeature.FeatureValues[0].IsEnabled;
                    if (f == true)
                    {
                        chkPDWishlist.Visible = true;
                    }
                    else
                    {
                        chkPDWishlist.Visible = false;
                    }
                }
                if (PD_EnableSavePDFFeature != null)
                {
                    chkPDSavePDF.Text = PD_EnableSavePDFFeature.FeatureAlias;
                    bool g = PD_EnableSavePDFFeature.FeatureValues[0].IsEnabled;
                    if (g == true)
                    {
                        chkPDSavePDF.Visible = true;
                    }
                    else
                    {
                        chkPDSavePDF.Visible = false;
                    }
                }
                if (PD_EnableEmailFeature != null)
                {
                    chkPDEmail.Text = PD_EnableEmailFeature.FeatureAlias;
                    bool h = PD_EnableEmailFeature.FeatureValues[0].IsEnabled;
                    if (h == true)
                    {
                        chkPDEmail.Visible = true;
                    }
                    else
                    {
                        chkPDEmail.Visible = false;
                    }
                }
                if (PD_EnableSocialMediaFeature != null)
                {
                    chkPDSocialMedia.Text = PD_EnableSocialMediaFeature.FeatureAlias;
                    bool i = PD_EnableSocialMediaFeature.FeatureValues[0].IsEnabled;
                    if (i == true)
                    {
                        chkPDSocialMedia.Visible = true;
                    }
                    else
                    {
                        chkPDSocialMedia.Visible = false;
                    }
                }
                if (PD_EnableYouMayAlsoLikeFeature != null)
                {
                    chkPDYouMayAlsoLike.Text = PD_EnableYouMayAlsoLikeFeature.FeatureAlias;
                    bool j = PD_EnableYouMayAlsoLikeFeature.FeatureValues[0].IsEnabled;
                    if (j == true)
                    {
                        chkPDYouMayAlsoLike.Visible = true;
                    }
                    else
                    {
                        chkPDYouMayAlsoLike.Visible = false;
                    }
                }
                if (PD_EnableReviewRatingFeature != null)
                {
                    chkPDReviewRating.Text = PD_EnableReviewRatingFeature.FeatureAlias;
                    bool k = PD_EnableReviewRatingFeature.FeatureValues[0].IsEnabled;
                    if (k == true)
                    {
                        chkPDReviewRating.Visible = true;
                    }
                    else
                    {
                        chkPDReviewRating.Visible = false;
                    }
                }
                if (PD_EnableSectionIconsFeature != null)
                {
                    chkPDSectionIcons.Text = PD_EnableSectionIconsFeature.FeatureAlias;
                    bool l = PD_EnableSectionIconsFeature.FeatureValues[0].IsEnabled;
                    if (l == true)
                    {
                        chkPDSectionIcons.Visible = true;
                    }
                    else
                    {
                        chkPDSectionIcons.Visible = false;
                    }
                }
                if (chkEnablePoints != null)
                {
                    chkEnablePoints.Text = SS_EnablePoints.FeatureAlias;
                    bool m = SS_EnablePoints.FeatureValues[0].IsEnabled;
                    if (m == true)
                    {
                        chkEnablePoints.Visible = true;
                    }
                    else
                    {
                        chkEnablePoints.Visible = false;
                    }
                }
                if (SS_HomeLink != null)
                {
                    rdoLinkPosition.Text = Convert.ToString(SS_HomeLink.FeatureValues.Find(x => x.IsEnabled == true).FeatureValue);
                }
                if (SC_PaymentGatewayFeature != null)
                {
                    rbSCPaymentGateway.Text = Convert.ToString(SC_PaymentGatewayFeature.FeatureValues.Find(x => x.IsDefault == true).FeatureValue);
                    rptSCPaymentGateway.DataSource = SC_PaymentGatewayFeature.FeatureValues.FindAll(x => x.IsEnabled);
                    rptSCPaymentGateway.DataBind();
                }
                if (SS_EnableCouponCode != null)
                {
                    chkEnableCouponCode.Text = SS_EnableCouponCode.FeatureAlias;
                    bool n = SS_EnableCouponCode.FeatureValues[0].IsEnabled;
                    if (n == true)
                    {
                        chkEnableCouponCode.Visible = true;
                    }
                    else
                    {
                        chkEnableCouponCode.Visible = false;
                    }
                }
                if (SS_IsPunchoutRegistration != null)
                {
                    chkIsPunchoutRegister.Text = SS_IsPunchoutRegistration.FeatureAlias;
                    bool o = SS_IsPunchoutRegistration.FeatureValues[0].IsEnabled;
                    if (o == true)
                    {
                        chkIsPunchoutRegister.Visible = true;
                    }
                    else
                    {
                        chkIsPunchoutRegister.Visible = false;
                    }
                }
                if (objStoreBE.IsPunchOut == true)
                {
                    chkPunchout.Visible = true;
                }
                else
                {
                    chkPunchout.Visible = false;
                }
                if (OD_AllowMaxOrderQtyPerItemFeature != null)
                {
                    chkMaxAllowQty.Text = OD_AllowMaxOrderQtyPerItemFeature.FeatureAlias;
                    bool p = OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].IsEnabled;
                    if (p == true)
                    {
                        chkMaxAllowQty.Visible = true;
                        lblMaxOrder.Visible = true;
                    }
                    else
                    {
                        chkMaxAllowQty.Visible = false;
                    }
                }
                if (OD_AllowBackOrderFeature != null)
                {
                    chkAllowBackOrder.Text = OD_AllowBackOrderFeature.FeatureAlias;
                    bool q = OD_AllowBackOrderFeature.FeatureValues[0].IsEnabled;
                    if (q == true)
                    {
                        chkAllowBackOrder.Visible = true;
                    }
                    else
                    {
                        chkAllowBackOrder.Visible = false;
                    }
                }
                if (IA_InvoiceAccount != null)
                {
                    ChkInvoiceAccountStatus.Text = IA_InvoiceAccount.FeatureAlias;
                    bool r = IA_InvoiceAccount.FeatureValues[0].IsEnabled;
                    if (r == true)
                    {
                        ChkInvoiceAccountStatus.Visible = true;
                    }
                    else
                    {
                        ChkInvoiceAccountStatus.Visible = false;
                    }
                }
                if (SC_EmailMeCopyOfBasket != null)
                {
                    ChkEmailCopyBasketBtn.Text = SC_EmailMeCopyOfBasket.FeatureAlias;
                    bool s = SC_EmailMeCopyOfBasket.FeatureValues[0].IsEnabled;
                    if (s == true)
                    {
                        ChkEmailCopyBasketBtn.Visible = true;
                    }
                    else
                    {
                        ChkEmailCopyBasketBtn.Visible = false;
                    }
                }
                if (PD_ColorProdEnquiry != null)
                {
                    ChkProductEnquiryColor.Text = PD_ColorProdEnquiry.FeatureAlias;
                    bool t = PD_ColorProdEnquiry.FeatureValues[0].IsEnabled;
                    if (t == true)
                    {
                        ChkProductEnquiryColor.Visible = true;
                    }
                    else
                    {
                        ChkProductEnquiryColor.Visible = false;
                    }
                }
                if (CR_StoreAccessFeature != null)
                {
                    rblCRStoreAccess.Text = Convert.ToString(CR_StoreAccessFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue);
                }
                if (CR_DomainValidationFeature != null)
                {
                    chkCREnableDomainValidation.Text = CR_DomainValidationFeature.FeatureAlias;
                    bool u = CR_DomainValidationFeature.FeatureValues[0].IsEnabled;
                    if (u == true)
                    {
                        chkCREnableDomainValidation.Visible = true;
                    }
                    else
                    {
                        chkCREnableDomainValidation.Visible = false;
                    }
                }
                //if(CR_EmailValidationFeature != null)
                //{
                //        chkCREnableEmailValidation.Text = Convert.ToString(CR_EmailValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue);
                //        bool v = CR_EmailValidationFeature.FeatureValues[0].IsEnabled;
                //        if (v == true)
                //        {
                //            chkCREnableEmailValidation.Visible = true;
                //        }
                //        else
                //        {
                //            chkCREnableEmailValidation.Visible = false;
                //        }
                //}
                if (PS_MinimumPasswordLengthFeature != null)
                {
                    txtPSMinimumPasswordLength.Text = PS_MinimumPasswordLengthFeature.FeatureValues[0].FeatureDefaultValue;
                }
                if (PS_PasswordPolicyFeature != null)
                {
                    lblPSPasswordPolicy.Text = PS_PasswordPolicyFeature.FeatureAlias;
                    rdPasswordPolicyType.Text = Convert.ToString(PS_PasswordPolicyFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).FeatureValue);
                }
                if (CR_StoreTypeFeature != null)
                {
                    rblCRStoreType.Text = CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled).FeatureValue;
                }
                if (SC_GiftCertificate != null)
                {
                    rdlIsGiftCertificateAllow.Text = SC_GiftCertificate.FeatureValues.FirstOrDefault(x => x.IsEnabled).FeatureValue;
                }
                if (CL_EnableCookieFeature != null)
                {
                    lblCLEnableCookie.Text = CL_EnableCookieFeature.FeatureAlias;
                    chkCLEnableCookie.Text = CL_EnableCookieFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled).FeatureValue;
                    bool w = CL_EnableCookieFeature.FeatureValues[0].IsEnabled;
                    if (w == true)
                    {
                        chkCLEnableCookie.Text = "YES";
                    }
                    else
                    {
                        chkCLEnableCookie.Text = "NO";
                    }
                }

            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
    #endregion 
    
}