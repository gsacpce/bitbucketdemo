﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

public partial class StoreCreator_test : System.Web.UI.Page
{
    static bool bThreadStatus = false;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCheckDB_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime dtStart = DateTime.Now;
            string connectionString = "server=pz6i500p4d.database.windows.net;database=" + txtDBName.Text.Trim() + ";user id=sa1;password=P@sswd123";
            SqlConnection ConnectionInstance = new SqlConnection(connectionString);
            string strContext = GetSqlContextInfo(ConnectionInstance);
            DateTime dtEnd = DateTime.Now;
            lblUserName.Text = "Time Diff: " + Convert.ToString(dtEnd.Subtract(dtStart));
            lblUserName.Text += " | " + strContext;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    public static string GetSqlContextInfo(SqlConnection conn)
    {
        string sqlContext = string.Empty;

        int sqlMaxRetries = 10;
        int sqlRetrySleep = 30000; // in miliseconds

        for (int retryCount = 0; retryCount <= sqlMaxRetries; retryCount++)
        {
            try
            {
                conn.Open();
                // get the SQL Context and validate the connection is still valid
                using (SqlCommand cmd = new SqlCommand("SELECT CONVERT(NVARCHAR(36), CONTEXT_INFO())", conn))
                {
                    sqlContext = cmd.ExecuteScalar().ToString();
                }
                break;
            }

            catch (SqlException ex)
            {
                conn.Close();
                SqlConnection.ClearPool(conn);

                if (retryCount < sqlMaxRetries)
                {
                    System.Threading.Thread.Sleep(sqlRetrySleep);
                }
            }
        }
        if (sqlContext == null)
        {
            sqlContext = string.Empty;
        }
        return sqlContext;
    }
}