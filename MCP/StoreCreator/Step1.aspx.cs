﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Security.AccessControl;
using System.Text.RegularExpressions;
using System.Data;
using System.Reflection;
//using DotNetUtilities.Utilities;

public partial class StoreCreator_Step1 : System.Web.UI.Page
{
    #region Variables

    StoreBE GetStore;

    #endregion

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Added by sanchit for authorization

        GlobalFunctions.IsAuthorized(HttpContext.Current.Request.Url.Segments[HttpContext.Current.Request.Url.Segments.Length - 1]);

        if (Convert.ToString(Request.QueryString["m"]).ToLower() == "edit")
        {
            if (Session["PreviousPage"] != null && Session["StoreId"] != null)
            {
                if (Request.QueryString["id"] != null)
                {
                    Int16 StoreId = Convert.ToInt16(Request.QueryString["id"]);
                    if ((Convert.ToString(Session["PreviousPage"]) == "step4" || Convert.ToString(Session["PreviousPage"]) == "viewstore" || Convert.ToString(Session["PreviousPage"]) == "step2") && Convert.ToInt16(Session["StoreId"]) == StoreId) { }
                    else
                        Response.Redirect("~/UnAuthorized.html");
                }
                else
                    Response.Redirect("~/UnAuthorized.html");
            }
            else
                Response.Redirect("~/UnAuthorized.html");
        }

        #endregion

        lblURL.Text = Convert.ToString(ConfigurationManager.AppSettings["MainDomainURL"]) + Regex.Replace(txtStoreName.Text.Trim(), "[^a-zA-Z0-9]", "");
        if (!Page.IsPostBack)
        {
            CreateDataTableStructure();
            GetAllStores();
            FillBasicStoreDetails();
            GetAllLanguages();
            GetAllCurrencies();
            BindDivisions();
        }
    }

    #endregion

    #region Get Basic Store/Currency/Language Details

    private void GetAllStores()
    {
        try
        {
            List<StoreBE> GetAllStores = StoreBL.GetAllStoreDetails();
            if (GetAllStores != null)
            {
                ddlCloneStores.DataSource = GetAllStores.FindAll(x => x.IsCreated == true);
                ddlCloneStores.DataTextField = "StoreAlias";
                ddlCloneStores.DataValueField = "StoreId";
                ddlCloneStores.DataBind();
                ddlCloneStores.Items.Insert(0, new ListItem("Select Base Store", "0"));
                ddlCloneStores.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void FillDatatableEditMode(DataTable dtRecords)
    {
        DataTable dtInsert = CreateDataTableStructure();
        //ViewState["datatablecurrency"] = dtInsert;
        for (int i = 0; i < dtRecords.Rows.Count; i++)
        {
            DataRow dr = dtInsert.NewRow();
            dr["CurrencyId"] = dtRecords.Rows[i]["CurrencyId"];
            dr["CatalogueId"] = dtRecords.Rows[i]["CatalogueId"];
            dr["GlobalCostCentre"] = dtRecords.Rows[i]["GlobalCostCentre"];
            dr["GroupId"] = dtRecords.Rows[i]["GroupId"];
            dr["DivisionId"] = dtRecords.Rows[i]["DivisionId"];
            dr["DivisionName"] = dtRecords.Rows[i]["DivisionName"];
            dr["DefaultCustomerId"] = dtRecords.Rows[i]["DefaultCustomerId"];
            dr["CatalogueAlias"] = dtRecords.Rows[i]["CatalogueAlias"];
            dr["SourceCodeId"] = dtRecords.Rows[i]["SourceCodeId"];
            dr["CatalogueName"] = dtRecords.Rows[i]["CatalogueName"];
            dr["KeyGroupId"] = dtRecords.Rows[i]["KeyGroupId"];
            dr["KeyGroupName"] = dtRecords.Rows[i]["KeyGroupName"];
            dr["SourceCodeName"] = dtRecords.Rows[i]["SourceCodeName"];
            dr["SalesPersonId"] = dtRecords.Rows[i]["SalesPersonId"];
            dr["DefaultCompanyId"] = dtRecords.Rows[i]["DefaultCompanyId"];
            dr["Default_CUST_CONTACT_ID"] = dtRecords.Rows[i]["Default_CUST_CONTACT_ID"];
            dtInsert.Rows.Add(dr);
        }
        ViewState["datatablecurrency"] = dtInsert;
    }

    private void FillBasicStoreDetails()
    {
        try
        {
            if ((Convert.ToString(Request.QueryString["m"]).ToLower() == "edit"))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["id"])))
                {
                    GetStore = StoreBL.GetStoreDetails(Convert.ToInt16(Request.QueryString["id"]));
                    /*datatable save in viewstate vikram*/
                    DataTable dt = ToDataTable<StoreBE.StoreCurrencyBE>(GetStore.StoreCurrencies);
                    FillDatatableEditMode(dt);
                    if (GetStore != null)
                    {
                        if (GetStore.IsCreated)
                            txtStoreName.Enabled = false;
                        else
                            txtStoreName.Enabled = true;
                        txtStoreName.Text = GetStore.StoreAlias;
                        lblURL.Text = GetStore.StoreLink;
                        txtStoreDescription.Text = GetStore.Description;
                        //rblStoreType.Items.FindByValue("1").Selected = true;
                        //rblStoreType.Items.FindByValue("1").Selected = GetStore.IsBASYS == true ? true : false;

                        if (GetStore.IsBASYS)
                        {
                            rblStoreType.Items.FindByValue("1").Selected = true;
                            rblStoreType.Items.FindByValue("0").Selected = false;
                        }
                        else
                        {
                            rblStoreType.Items.FindByValue("0").Selected = true;
                            rblStoreType.Items.FindByValue("1").Selected = false;
                        }

                        if (GetStore.CloneStoreId != 0)
                            ddlCloneStores.SelectedValue = Convert.ToString(GetStore.CloneStoreId);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    private void GetAllLanguages()
    {
        try
        {
            List<LanguageBE> GetAllLanguages = LanguageBL.GetAllLanguageDetails();
            rptLanguages.DataSource = GetAllLanguages;
            rptLanguages.DataBind();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    private void GetAllCurrencies()
    {
        try
        {
            List<CurrencyBE> GetAllCurrencies = CurrencyBL.GetAllCurrencyDetails();
            rptCurrencies.DataSource = GetAllCurrencies;
            rptCurrencies.DataBind();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Insert and edit store basic details

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Int16 intStoreId = 0;
        if (SaveBasicStoreDetails(ref intStoreId))
        {
            #region Redirection

            Session["AllStores"] = null;
            Session["SortedView"] = null;

            switch (((Button)sender).CommandName)
            {
                case "SaveClose":
                    Session["PreviousPage"] = null;
                    Session["StoreId"] = null;
                    Response.Redirect("~/DashBoard.aspx");
                    break;
                case "SaveContinue":
                    Session["PreviousPage"] = "step1";
                    Session["StoreId"] = intStoreId;
                    Response.Redirect("~/StoreCreator/Step2.aspx?id=" + intStoreId + "");
                    break;
                default:
                    break;
            }

            #endregion

        }
    }

    private bool SaveBasicStoreDetails(ref Int16 intStoreId)
    {
        try
        {
            UserBE user = (UserBE)Session["User"];
            //Int16 intStoreId = 0;

            if (Convert.ToString(Request.QueryString["m"]).ToLower() == "create")
            {
                #region Create Store

                if (!StoreBL.IsStoreExists(Regex.Replace(txtStoreName.Text.Trim(), "[^a-zA-Z0-9]", "")))
                {
                    InsertBasicStoreDetails(user.UserId, ref intStoreId);
                    InsertLanguageDetails(intStoreId);
                    InsertCurrencyDetails(user.UserId, intStoreId);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Store already exists", AlertType.Warning);
                    return false;
                }

                #endregion
            }
            else if (Convert.ToString(Request.QueryString["m"]).ToLower() == "edit")
            {
                #region Edit Store

                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["id"])))
                    intStoreId = Convert.ToInt16(Request.QueryString["id"]);
                EditBasicStoreDetails(user.UserId, intStoreId);
                InsertLanguageDetails(intStoreId);
                InsertCurrencyDetails(user.UserId, intStoreId);

                #endregion
            }


        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
        return true;
    }

    private void InsertBasicStoreDetails(int UserId, ref Int16 intStoreId)
    {
        try
        {
            string StoreName = Regex.Replace(txtStoreName.Text.Trim(), "[^a-zA-Z0-9]", "");
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            //dictionaryInstance.Add("StoreName", Convert.ToString(txtStoreName.Text));
            dictionaryInstance.Add("StoreName", StoreName.Trim());
            dictionaryInstance.Add("StoreAlias", Convert.ToString(txtStoreName.Text.Trim()));
            dictionaryInstance.Add("StoreLink", Convert.ToString(ConfigurationManager.AppSettings["MainDomainURL"]) + StoreName.Trim());
            //dictionaryInstance.Add("DBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + txtStoreName.Text.Trim().Replace(" ", ""));
            dictionaryInstance.Add("DBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName.Trim());
            dictionaryInstance.Add("DBServerName", Convert.ToString(ConfigurationManager.AppSettings["DBServerName"]));
            dictionaryInstance.Add("DBUserName", Convert.ToString(ConfigurationManager.AppSettings["DBUserName"]));
            dictionaryInstance.Add("DBPassword", Convert.ToString(ConfigurationManager.AppSettings["DBPassword"]));
            if (ddlCloneStores.SelectedIndex > 0)
            {
                StoreBE store = StoreBL.GetStoreDetails(Convert.ToInt16(ddlCloneStores.SelectedValue));
                dictionaryInstance.Add("IsPunchOut", Convert.ToString(store.IsPunchOut));
            }
            else
                dictionaryInstance.Add("IsPunchOut", Convert.ToString(false));
            dictionaryInstance.Add("IsBASYS", Convert.ToString(rblStoreType.SelectedValue));
            dictionaryInstance.Add("CloneStoreId", ddlCloneStores.SelectedIndex > 0 ? Convert.ToString(ddlCloneStores.SelectedValue) : Convert.ToString(0));
            dictionaryInstance.Add("Description", Convert.ToString(txtStoreDescription.Text));
            dictionaryInstance.Add("StatusId", Convert.ToString(Convert.ToByte(StoreStatus.In_Development)));
            dictionaryInstance.Add("CreatedBy", Convert.ToString(UserId));

            intStoreId = StoreBL.InsertStore(dictionaryInstance);//1
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    private void InsertLanguageDetails(Int16 intStoreId)
    {
        try
        {
            foreach (RepeaterItem language in rptLanguages.Items)
            {
                CheckBox chkLanguage = (CheckBox)language.FindControl("chkLanguage");
                if (chkLanguage.Checked)
                {
                    List<LanguageBE> getAllLanguages = LanguageBL.GetAllLanguageDetails();
                    HtmlInputRadioButton rbDefaultLanguage = (HtmlInputRadioButton)language.FindControl("rbDefaultLanguage");
                    Dictionary<string, string> languageDictionaryInstance = new Dictionary<string, string>();
                    languageDictionaryInstance.Add("StoreId", Convert.ToString(intStoreId));
                    languageDictionaryInstance.Add("LanguageId", Convert.ToString(getAllLanguages.FirstOrDefault(x => x.LanguageName == chkLanguage.Text).LanguageId));
                    languageDictionaryInstance.Add("IsDefault", Convert.ToString(rbDefaultLanguage.Checked));
                    StoreBL.InsertStoreLanguage(languageDictionaryInstance);
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    private void InsertCurrencyDetails(int UserId, Int16 intStoreId)
    {
        try
        {
            string ActiveCurrecyIds = "";
            foreach (RepeaterItem currency in rptCurrencies.Items)
            {
                CheckBox chkCurrency = (CheckBox)currency.FindControl("chkCurrency");
                Label lblCurrcurrencyId = (Label)currency.FindControl("lblCurrcurrencyId");
                HtmlInputRadioButton rbDefaultCurrency = (HtmlInputRadioButton)currency.FindControl("rbDefaultCurrency");
                if (rbDefaultCurrency.Checked)
                {
                    hdnDefaultCurrencyId.Value = lblCurrcurrencyId.Text;
                }
                if (chkCurrency.Checked)
                {
                    ActiveCurrecyIds += lblCurrcurrencyId.Text + ",";
                }
            }
            DataTable dt = ViewState["datatablecurrency"] as DataTable;
            List<CurrencyBE> getAllCurrencies = CurrencyBL.GetAllCurrencyDetails();

            if (rblStoreType.SelectedIndex == 0)
            {
                #region BasysStore
                foreach (DataRow dr in dt.Rows)
                {
                    if (ActiveCurrecyIds.Contains(Convert.ToString(dr["CurrencyId"])))
                    {
                        Int16 CurrencyId = Convert.ToInt16(dr["CurrencyId"]);
                        Int32 CatalogueId = Convert.ToInt32(dr["CatalogueId"]);
                        string GlobalCostCentre = Convert.ToString(dr["GlobalCostCentre"]);
                        Int32 GroupId = Convert.ToInt32(dr["GroupId"]);
                        Int32 DivisionId = Convert.ToInt32(dr["DivisionId"]);
                        string DefaultCustomerId = Convert.ToString(dr["DefaultCustomerId"]);
                        string CatalogueAlias = Convert.ToString(dr["CatalogueAlias"]);
                        Int32 KeyGroupId = Convert.ToInt32(dr["KeyGroupId"]);
                        Int32 SourceCodeId = Convert.ToInt32(dr["SourceCodeId"]);
                        string DivisionName = Convert.ToString(dr["DivisionName"]);
                        string CatalogueName = Convert.ToString(dr["CatalogueName"]);
                        string KeyGroupName = Convert.ToString(dr["KeyGroupName"]);
                        string SourceCodeName = Convert.ToString(dr["SourceCodeName"]);
                        Int64 SalesPersonId = Convert.ToInt64(dr["SalesPersonId"]);
                        string DefaultCompanyId = Convert.ToString(dr["DefaultCompanyId"]);
                        string Default_CUST_CONTACT_ID = Convert.ToString(dr["Default_CUST_CONTACT_ID"]);

                        Dictionary<string, string> currencyDictionaryInstance = new Dictionary<string, string>();

                        currencyDictionaryInstance.Add("StoreId", Convert.ToString(intStoreId));
                        currencyDictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                        currencyDictionaryInstance.Add("CatalogueId", Convert.ToString(CatalogueId));
                        currencyDictionaryInstance.Add("GlobalCostCentre", GlobalCostCentre);
                        currencyDictionaryInstance.Add("GroupId", Convert.ToString(GroupId));
                        currencyDictionaryInstance.Add("DivisionId", Convert.ToString(DivisionId));
                        currencyDictionaryInstance.Add("DefaultCustomerId", DefaultCustomerId);
                        currencyDictionaryInstance.Add("CatalogueAlias", Convert.ToString(CatalogueAlias));
                        currencyDictionaryInstance.Add("KeyGroupId", Convert.ToString(KeyGroupId));
                        currencyDictionaryInstance.Add("SourceCodeId", Convert.ToString(SourceCodeId));
                        currencyDictionaryInstance.Add("HelpDeskMailID", "");
                        currencyDictionaryInstance.Add("DivisionName", DivisionName);
                        currencyDictionaryInstance.Add("CatalogueName", CatalogueName);
                        currencyDictionaryInstance.Add("KeyGroupName", KeyGroupName);
                        currencyDictionaryInstance.Add("SourceCodeName", SourceCodeName);
                        currencyDictionaryInstance.Add("SalesPersonId", Convert.ToString(SalesPersonId));
                        currencyDictionaryInstance.Add("DefaultCompanyId", Convert.ToString(DefaultCompanyId));
                        currencyDictionaryInstance.Add("Default_CUST_CONTACT_ID", Default_CUST_CONTACT_ID);
                        if (Convert.ToString(hdnDefaultCurrencyId.Value) == CurrencyId.ToString())
                        {
                            currencyDictionaryInstance.Add("IsDefault", "1");
                        }
                        else
                        {
                            currencyDictionaryInstance.Add("IsDefault", "0");
                        }
                        currencyDictionaryInstance.Add("CreatedBy", Convert.ToString(UserId));
                        StoreBL.InsertStoreCurrency(currencyDictionaryInstance);
                    }
                }
                #endregion
            }
            else
            {
                #region non BasysStore
                foreach (RepeaterItem item in rptCurrencies.Items)
                {
                    CheckBox chkCurrency = (CheckBox)item.FindControl("chkCurrency");
                    Label lblCurrcurrencyId = (Label)item.FindControl("lblCurrcurrencyId");
                    if (chkCurrency.Checked)
                    {
                        Int16 CurrencyId = Convert.ToInt16(lblCurrcurrencyId.Text);
                        Dictionary<string, string> currencyDictionaryInstance = new Dictionary<string, string>();

                        currencyDictionaryInstance.Add("StoreId", Convert.ToString(intStoreId));
                        currencyDictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                        currencyDictionaryInstance.Add("CatalogueId", "");
                        currencyDictionaryInstance.Add("GlobalCostCentre", "");
                        currencyDictionaryInstance.Add("GroupId", "");
                        currencyDictionaryInstance.Add("DivisionId", "");
                        currencyDictionaryInstance.Add("DefaultCustomerId", "");
                        currencyDictionaryInstance.Add("CatalogueAlias", "");
                        currencyDictionaryInstance.Add("KeyGroupId", "");
                        currencyDictionaryInstance.Add("SourceCodeId", "");
                        currencyDictionaryInstance.Add("HelpDeskMailID", "");
                        currencyDictionaryInstance.Add("DivisionName", "");
                        currencyDictionaryInstance.Add("CatalogueName", "");
                        currencyDictionaryInstance.Add("KeyGroupName", "");
                        currencyDictionaryInstance.Add("SourceCodeName", "");
                        currencyDictionaryInstance.Add("SalesPersonId", Convert.ToString(""));
                        currencyDictionaryInstance.Add("DefaultCompanyId", Convert.ToString(""));
                        currencyDictionaryInstance.Add("Default_CUST_CONTACT_ID", "0");
                        if (Convert.ToString(hdnDefaultCurrencyId.Value) == CurrencyId.ToString())
                        {
                            currencyDictionaryInstance.Add("IsDefault", "1");
                        }
                        else
                        {
                            currencyDictionaryInstance.Add("IsDefault", "0");
                        }
                        currencyDictionaryInstance.Add("CreatedBy", Convert.ToString(UserId));
                        StoreBL.InsertStoreCurrency(currencyDictionaryInstance);
                    }
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    private void InsertCurrencyDetails_bkp(int UserId, Int16 intStoreId)
    {
        try
        {
            foreach (RepeaterItem currency in rptCurrencies.Items)
            {
                CheckBox chkCurrency = (CheckBox)currency.FindControl("chkCurrency");
                if (chkCurrency.Checked)
                {
                    List<CurrencyBE> getAllCurrencies = CurrencyBL.GetAllCurrencyDetails();
                    HtmlInputRadioButton rbDefaultCurrency = (HtmlInputRadioButton)currency.FindControl("rbDefaultCurrency");
                    TextBox txtCatalogueId = (TextBox)currency.FindControl("txtCatalogueId");
                    TextBox txtGlobalCostCentreNumber = (TextBox)currency.FindControl("txtGlobalCostCentreNumber");
                    TextBox txtGroupId = (TextBox)currency.FindControl("txtGroupId");
                    TextBox txtDivisionId = (TextBox)currency.FindControl("txtDivisionId");
                    TextBox txtDivisionName = (TextBox)currency.FindControl("txtDivisionName");
                    TextBox txtDefaultCustomerId = (TextBox)currency.FindControl("txtDefaultCustomerId");
                    TextBox txtCatalogueAliasId = (TextBox)currency.FindControl("txtCatalogueAliasId");
                    TextBox txtCatalogueName = (TextBox)currency.FindControl("txtCatalogueName");
                    TextBox txtKeyGroupId = (TextBox)currency.FindControl("txtKeyGroupId");
                    TextBox txtKeyGroupName = (TextBox)currency.FindControl("txtKeyGroupName");
                    TextBox txtSourceCodeId = (TextBox)currency.FindControl("txtSourceCodeId");
                    TextBox txtSourceCodeName = (TextBox)currency.FindControl("txtSourceCodeName");
                    TextBox txtSalesPersonId = (TextBox)currency.FindControl("txtSalesPersonId");
                    TextBox txtDefaultCompanyId = (TextBox)currency.FindControl("txtDefaultCompanyId");
                    TextBox txtDefault_CUST_CONTACT_ID = (TextBox)currency.FindControl("txtDefault_CUST_CONTACT_ID");

                    //TextBox txtHelpDeskEmailId = (TextBox)currency.FindControl("txtHelpDeskEmailId");

                    Dictionary<string, string> currencyDictionaryInstance = new Dictionary<string, string>();
                    currencyDictionaryInstance.Add("StoreId", Convert.ToString(intStoreId));
                    currencyDictionaryInstance.Add("CurrencyId", Convert.ToString(getAllCurrencies.FirstOrDefault(x => x.CurrencyName == chkCurrency.Text).CurrencyId));

                    if (rblStoreType.SelectedValue == "1")
                    {
                        currencyDictionaryInstance.Add("CatalogueId", txtCatalogueId.Text.Trim());
                        currencyDictionaryInstance.Add("GlobalCostCentre", txtGlobalCostCentreNumber.Text.Trim());
                        currencyDictionaryInstance.Add("GroupId", txtGroupId.Text.Trim());
                        currencyDictionaryInstance.Add("DivisionId", txtDivisionId.Text.Trim());
                        currencyDictionaryInstance.Add("DefaultCustomerId", txtDefaultCustomerId.Text.Trim());
                        currencyDictionaryInstance.Add("CatalogueAlias", txtCatalogueAliasId.Text.Trim());
                        currencyDictionaryInstance.Add("KeyGroupId", txtKeyGroupId.Text.Trim());
                        currencyDictionaryInstance.Add("SourceCodeId", txtSourceCodeId.Text);
                        currencyDictionaryInstance.Add("HelpDeskMailID", "");
                        currencyDictionaryInstance.Add("DivisionName", txtDivisionName.Text);
                        currencyDictionaryInstance.Add("CatalogueName", txtCatalogueName.Text.Trim());
                        currencyDictionaryInstance.Add("KeyGroupName", txtKeyGroupName.Text.Trim());
                        currencyDictionaryInstance.Add("SourceCodeName", txtSourceCodeName.Text.Trim());
                        currencyDictionaryInstance.Add("SalesPersonId", txtSalesPersonId.Text.Trim());
                        currencyDictionaryInstance.Add("DefaultCompanyId", txtDefaultCompanyId.Text.Trim());
                        currencyDictionaryInstance.Add("Default_CUST_CONTACT_ID", !string.IsNullOrEmpty(txtDefault_CUST_CONTACT_ID.Text) ? txtDefault_CUST_CONTACT_ID.Text.Trim() : "0");

                    }

                    currencyDictionaryInstance.Add("IsDefault", Convert.ToString(rbDefaultCurrency.Checked));
                    currencyDictionaryInstance.Add("CreatedBy", Convert.ToString(UserId));
                    StoreBL.InsertStoreCurrency(currencyDictionaryInstance);
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    private void EditBasicStoreDetails(int UserId, Int16 intStoreId)
    {
        try
        {
            string StoreName = Regex.Replace(txtStoreName.Text.Trim(), "[^a-zA-Z0-9]", "");
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            //dictionaryInstance.Add("StoreName", Convert.ToString(txtStoreName.Text));
            dictionaryInstance.Add("StoreName", Convert.ToString(StoreName.Trim()));
            dictionaryInstance.Add("StoreAlias", Convert.ToString(txtStoreName.Text.Trim()));
            dictionaryInstance.Add("StoreLink", Convert.ToString(ConfigurationManager.AppSettings["MainDomainURL"]) + StoreName.Trim());
            //dictionaryInstance.Add("DBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + txtStoreName.Text.Trim().Replace(" ", ""));
            dictionaryInstance.Add("DBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName.Trim());
            dictionaryInstance.Add("DBServerName", Convert.ToString(ConfigurationManager.AppSettings["DBServerName"]));
            dictionaryInstance.Add("DBUserName", Convert.ToString(ConfigurationManager.AppSettings["DBUserName"]));
            dictionaryInstance.Add("DBPassword", Convert.ToString(ConfigurationManager.AppSettings["DBPassword"]));
            dictionaryInstance.Add("IsBASYS", Convert.ToString(rblStoreType.SelectedValue));
            dictionaryInstance.Add("CloneStoreId", ddlCloneStores.SelectedIndex > 0 ? Convert.ToString(ddlCloneStores.SelectedValue) : Convert.ToString(0));
            dictionaryInstance.Add("Description", Convert.ToString(txtStoreDescription.Text));
            dictionaryInstance.Add("ModifiedBy", Convert.ToString(UserId));
            dictionaryInstance.Add("StoreId", Convert.ToString(intStoreId));

            StoreBL.UpdateStore(dictionaryInstance);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void cmdReset_Click(object sender, EventArgs e)
    {
        try
        {
            txtStoreName.Text = string.Empty;
            txtStoreDescription.Text = string.Empty;
            rblStoreType.Items.FindByValue("0").Selected = true;
            ddlCloneStores.SelectedIndex = 0;
            GetAllLanguages();
            GetAllCurrencies();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Session["PreviousPage"] = null;
            Session["StoreId"] = null;
            Response.Redirect("~/DashBoard.aspx");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void cmdCreateStore_Click(object sender, EventArgs e)
    {
        try
        {
            Int16 intStoreId = 0;
            SaveBasicStoreDetails(ref intStoreId);
            CreateCloneStore(intStoreId);


        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }


    #endregion

    #region Events

    protected void rptLanguages_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlInputRadioButton rbDefaultLanguage = (HtmlInputRadioButton)e.Item.FindControl("rbDefaultLanguage");
                rbDefaultLanguage.Attributes.Add("onClick", "SetUniqueRadioButton('DefaultLanguage',this);");
                if ((Convert.ToString(Request.QueryString["m"]).ToLower() == "edit"))
                {
                    LanguageBE Language = (LanguageBE)e.Item.DataItem;

                    StoreBE.StoreLanguageBE StoreLanguage = null;
                    if (GetStore != null)
                        StoreLanguage = GetStore.StoreLanguages.FirstOrDefault(x => x.LanguageId == Language.LanguageId);

                    HtmlGenericControl dvDefaultLanguage = (HtmlGenericControl)e.Item.FindControl("dvDefaultLanguage");
                    if (StoreLanguage != null)
                    {
                        CheckBox chkLanguage = (CheckBox)e.Item.FindControl("chkLanguage");
                        chkLanguage.Checked = true;
                        dvDefaultLanguage.Style.Add("display", "block");

                        if (StoreLanguage.IsDefault)
                            rbDefaultLanguage.Checked = StoreLanguage.IsDefault;
                    }
                    else
                        dvDefaultLanguage.Style.Add("display", "none");
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void rptCurrencies_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlInputRadioButton rbDefaultCurrency = (HtmlInputRadioButton)e.Item.FindControl("rbDefaultCurrency");
                rbDefaultCurrency.Attributes.Add("onClick", "SetUniqueRadioButton('DefaultCurrency',this);");
                CurrencyBE Currency = (CurrencyBE)e.Item.DataItem;
                if ((Convert.ToString(Request.QueryString["m"]).ToLower() == "edit"))
                {
                    StoreBE.StoreCurrencyBE StoreCurrency = null;
                    if (GetStore != null)
                        StoreCurrency = GetStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Currency.CurrencyId);

                    HtmlGenericControl dvDefaultCurrency = (HtmlGenericControl)e.Item.FindControl("dvDefaultCurrency");
                    HtmlGenericControl dvBASYSDetails = (HtmlGenericControl)e.Item.FindControl("dvBASYSDetails");
                    if (StoreCurrency != null)
                    {
                        CheckBox chkCurrency = (CheckBox)e.Item.FindControl("chkCurrency");
                        chkCurrency.Checked = true;
                        dvDefaultCurrency.Style.Add("display", "block");

                        if (rblStoreType.SelectedValue == "1")
                        {
                        }
                        else
                            dvBASYSDetails.Style.Add("display", "none");

                        if (StoreCurrency.IsDefault)
                        {
                            rbDefaultCurrency.Checked = StoreCurrency.IsDefault;
                        }
                    }
                    else
                    {
                        dvDefaultCurrency.Style.Add("display", "none");
                        dvBASYSDetails.Style.Add("display", "none");
                    }
                }

                RequiredFieldValidator rfvCatalogueId = (RequiredFieldValidator)e.Item.FindControl("rfvCatalogueId");
                RequiredFieldValidator rfvGroupId = (RequiredFieldValidator)e.Item.FindControl("rfvGroupId");
                RequiredFieldValidator rfvDivisionId = (RequiredFieldValidator)e.Item.FindControl("rfvDivisionId");
                RequiredFieldValidator rfvDefaultCustomerId = (RequiredFieldValidator)e.Item.FindControl("rfvDefaultCustomerId");
                RequiredFieldValidator rfvCatalogueAliasId = (RequiredFieldValidator)e.Item.FindControl("rfvCatalogueAliasId");
                RequiredFieldValidator rfvKeyGroupId = (RequiredFieldValidator)e.Item.FindControl("rfvKeyGroupId");
                RequiredFieldValidator rfvSourceCodeId = (RequiredFieldValidator)e.Item.FindControl("rfvSourceCodeId");
                RequiredFieldValidator rfvSalesPersonId = (RequiredFieldValidator)e.Item.FindControl("rfvSalesPersonId");
                RequiredFieldValidator rfvDefaultCompanyId = (RequiredFieldValidator)e.Item.FindControl("rfvDefaultCompanyId");
                //RequiredFieldValidator rfvHelpDeskEmailId = (RequiredFieldValidator)e.Item.FindControl("rfvHelpDeskEmailId");
                //RegularExpressionValidator revHelpDeskEmailId = (RegularExpressionValidator)e.Item.FindControl("revHelpDeskEmailId");
                Button cmdSave = (Button)e.Item.FindControl("cmdSave");

                rfvCatalogueId.ValidationGroup = rfvGroupId.ValidationGroup = rfvDivisionId.ValidationGroup = rfvDefaultCustomerId.ValidationGroup =
                    rfvCatalogueAliasId.ValidationGroup = rfvKeyGroupId.ValidationGroup = rfvSourceCodeId.ValidationGroup =
                    rfvSalesPersonId.ValidationGroup = rfvDefaultCompanyId.ValidationGroup = cmdSave.ValidationGroup = Currency.CurrencyCode;

                ClearpopupFields();
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void ddlCloneStores_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCloneStores.SelectedIndex == 0)
        {
            ulSaveDetails.Style.Add("display", "");
            ulCreateStore.Style.Add("display", "none");
        }
        else
        {
            ulCreateStore.Style.Add("display", "");
            ulSaveDetails.Style.Add("display", "none");
        }
    }

    #endregion

    #region Create Store

    private void CreateCloneStore(Int16 intStoreId)
    {
        try
        {
            StoreBE StoreInstance = StoreBL.GetStoreDetails(intStoreId);
            StoreBE CloneStoreInstance = StoreBL.GetAllStoreDetails().FirstOrDefault(x => x.StoreId == Convert.ToInt16(ddlCloneStores.SelectedValue));
            string DatabasePassword = string.Empty;
            bool IsStoreCreated = CreateCloneSite(CloneStoreInstance.StoreName, StoreInstance.StoreName.Trim(), ref DatabasePassword);
            Session["AllStores"] = null;
            Session["SortedView"] = null;
            if (IsStoreCreated)
            {

                UserBE user = (UserBE)Session["User"];
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("IsCreated", Convert.ToString(1));
                DictionaryInstance.Add("StatusId", Convert.ToString(Convert.ToByte(StoreStatus.Live)));
                DictionaryInstance.Add("DBPassword", Convert.ToString(DatabasePassword));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(user.UserId));
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreInstance.StoreId));
                bool IsUpdated = StoreBL.UpdateIsStoreCreated(DictionaryInstance);
                StoreBL.UpdateNewlyCreatedStoreDetails(DictionaryInstance, StoreInstance.StoreName);
                Session["PreviousPage"] = null;
                Session["StoreId"] = null;
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Store created successfully", Convert.ToString(ConfigurationManager.AppSettings["ImgURL"]) + "DashBoard.aspx", AlertType.Success);
            }
            else
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while creating store", AlertType.Failure);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion

    #region Create Database

    private bool CreateCloneDatabase(string strBaseSiteName, string strSiteName, ref string DatabasePassword)
    {
        bool isStoreDetailsTransfered = false;
        DataSet dsGetStoreDetails;
        try
        {
            Exceptions.WriteInfoLog("New DBName -> " + Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strSiteName.Trim().Replace(" ", ""));
            Exceptions.WriteInfoLog("Clone DBName -> " + Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strBaseSiteName.Trim().Replace(" ", ""));
            Dictionary<string, string> CreateDatabaseDictionaryInstance = new Dictionary<string, string>();
            //commented by sanchit on 21/12/2015 for NEW BA Server
            //CreateDatabaseDictionaryInstance.Add("DBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strSiteName.Trim().Replace(" ", ""));
            //CreateDatabaseDictionaryInstance.Add("CloneDBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strBaseSiteName.Trim().Replace(" ", ""));
            //StoreBL.CreateCloneDatabase(CreateDatabaseDictionaryInstance);

            CreateDatabaseDictionaryInstance.Add("BackupDBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strBaseSiteName.Trim().Replace(" ", ""));
            CreateDatabaseDictionaryInstance.Add("BackupFilePath", Convert.ToString(ConfigurationManager.AppSettings["DBBackupPath"]) + Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strBaseSiteName.Trim().Replace(" ", "") + ".bak");
            CreateDatabaseDictionaryInstance.Add("RestoreDBName", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strSiteName.Trim().Replace(" ", ""));
            CreateDatabaseDictionaryInstance.Add("RestoreFilePath", Convert.ToString(ConfigurationManager.AppSettings["DBBackupPath"]));
            StoreBL.CreateDatabase(CreateDatabaseDictionaryInstance);

            dsGetStoreDetails = StoreBL.GetCloneMCPStoreDetails(strSiteName);
            Exceptions.WriteInfoLog("database created");
            Exceptions.WriteInfoLog("Before executing script for transfer data");
            isStoreDetailsTransfered = StoreBL.ExecuteCloneDatabaseData(Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strBaseSiteName.Trim().Replace(" ", ""), Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + strSiteName.Trim().Replace(" ", ""));
            Exceptions.WriteInfoLog("After executing script for transfer data");
            DatabasePassword = StoreBL.CreateCloneDBUser(strSiteName.Trim().Replace(" ", ""), strBaseSiteName.Trim().Replace(" ", ""));
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return isStoreDetailsTransfered;
    }

    #endregion

    #region Create Virtual Directory

    public bool CreateCloneSite(string strBaseSiteName, string strSiteName, ref string DatabasePassword)
    {
        Exceptions.WriteInfoLog("Enter In CreateCloneSite Method");
        bool blnCreateStore = false;
        try
        {
            string strWebsiteHostPath = "";

            strWebsiteHostPath = ConfigurationManager.AppSettings["WebsiteHostPath"];
            //will validate if site or database with specified name exists
            Exceptions.WriteInfoLog("Before Calling IsSiteExists Method");
            if (IsSiteExists(strSiteName, strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName)) == SiteElements.None)
            {
                Exceptions.WriteInfoLog("After Calling IsSiteExists Method");
                //STEP:1 ==> Copy already published site
                Exceptions.WriteInfoLog("Before Calling CopyDirectory Method");
                bool blnCopySite = GlobalFunctions.CopyDirectory(strWebsiteHostPath.Trim().Replace("$StoreName$", strBaseSiteName), strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName), true);
                File.Copy(strWebsiteHostPath.Trim().Replace("$StoreName$", Convert.ToString(ConfigurationManager.AppSettings["DefaultDBName"])) + @"\Web.config", strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName) + @"\Web.config", true);
                Exceptions.WriteInfoLog("After Calling CopyDirectory Method, blnCopySite-> " + Convert.ToString(blnCopySite));
                if (blnCopySite == true)
                {
                    //Exceptions.WriteInfoLog("Before Calling CopyDirectoryTemmplate Method");
                    //bool blnCopyTemplate = GlobalFunctions.CopyDirectory(GlobalFunctions.GetPhysicalFolderPath() + @"\BAStore", strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName), true);
                    bool blnCopyTemplate = true;//GlobalFunctions.CopyDirectory(GlobalFunctions.GetPhysicalFolderPath() + @"\Templates\" + Path.GetFileNameWithoutExtension(hdnTemplateName.Value), strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName), true);
                    //Exceptions.WriteInfoLog("After Calling CopyDirectoryTemmplate Method, blnCopyTemplate-> " + Convert.ToString(blnCopyTemplate));
                    AssignAccessRights(strWebsiteHostPath.Trim().Replace("$StoreName$", strSiteName));
                    if (blnCopyTemplate)
                    {
                        //STEP:2 ==> Create DataBase
                        bool blnDatabaseCreated = false;
                        try
                        {
                            Exceptions.WriteInfoLog("Before Calling CreateCloneDatabase Method");
                            blnDatabaseCreated = CreateCloneDatabase(strBaseSiteName, strSiteName, ref DatabasePassword);
                            Exceptions.WriteInfoLog("After Calling CreateCloneDatabase Method");
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            throw ex;
                        }

                        if (blnDatabaseCreated)
                        {
                            //STEP:9 ==> Replace Connection String Of Newly Created Site
                            Exceptions.WriteInfoLog("Before Calling ReplaceConnectionStringInCreatedSite Method");
                            bool blnReplaceConnectionStringInCreatedSite = ReplaceConnectionStringInCreatedSite(strBaseSiteName, strSiteName, DatabasePassword);
                            Exceptions.WriteInfoLog("After Calling ReplaceConnectionStringInCreatedSite Method, blnReplaceConnectionStringInCreatedSite -> " + Convert.ToString(blnReplaceConnectionStringInCreatedSite));

                            if (blnReplaceConnectionStringInCreatedSite)
                            {
                                //STEP:10 ==> Change ApplicationConstants File Of Newly Created Site
                                bool blnUpdateConstantsInCreatedSite = true;// UpdateConstantsInCreatedSite(baseStoreId, strBaseSiteName, strSiteName, createdStoreId, strStoreURL, strCurrencySymbol);
                                if (blnUpdateConstantsInCreatedSite == true)
                                {
                                    //Work Here For VD Or WebSite
                                    string strHostingServer = Convert.ToString(GlobalFunctions.GetSetting("CS_STORESMANAGEMENT_SITEHOSTINGSERVERNAME"));
                                    string strPhysicalPath = strWebsiteHostPath.Replace("$StoreName$", strSiteName);

                                    //STEP : 11 ==> Cteate Virtual Directory Or Web Site In IIS
                                    Exceptions.WriteInfoLog("Before Calling CreateVirtualDirectoryORDWebSiteInIIS Method");
                                    bool blnCreateVirtualDirectoryORDWebSiteInIIS = CreateVirtualDirectoryORDWebSiteInIIS(strHostingServer, strPhysicalPath, strSiteName);
                                    Exceptions.WriteInfoLog("After Calling CreateVirtualDirectoryORDWebSiteInIIS Method, blnCreateVirtualDirectoryORDWebSiteInIIS -> " + Convert.ToString(blnCreateVirtualDirectoryORDWebSiteInIIS));
                                    if (blnCreateVirtualDirectoryORDWebSiteInIIS == true)
                                    {
                                        blnCreateStore = true;
                                    }
                                    else
                                    {
                                        StoreBL.DropDatabase(strSiteName);
                                        DeleteSite(strSiteName);
                                    }
                                }
                            }
                            else
                            {
                                StoreBL.DropDatabase(strSiteName);
                                DeleteSite(strSiteName);
                            }
                        }
                        else
                        {
                            DeleteSite(strSiteName);
                        }
                    }
                }
                else
                {
                    Response.Write("Error while copying the directory base folder");
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        Exceptions.WriteInfoLog(Convert.ToString(blnCreateStore));
        return blnCreateStore;
    }

    private SiteElements IsSiteExists(string siteFolderName, string strWebsiteHostPath)
    {
        SiteElements enmSiteElements = SiteElements.None;
        try
        {
            bool blnIsSiteFolderExists = false;
            bool blnIsSiteDatabaseExists = false;

            if (!string.IsNullOrEmpty(strWebsiteHostPath.Trim()) && !string.IsNullOrEmpty(siteFolderName.Trim()))
                blnIsSiteFolderExists = IsSiteFolderExists(strWebsiteHostPath.Trim()) ? true : false;

            blnIsSiteDatabaseExists = StoreBL.IsSiteDatabaseExists(siteFolderName.Trim()) ? true : false;

            if (blnIsSiteFolderExists && blnIsSiteDatabaseExists)
                enmSiteElements = SiteElements.SiteAndDatabase;
            else if (blnIsSiteFolderExists)
                enmSiteElements = SiteElements.Site;
            else if (blnIsSiteDatabaseExists)
                enmSiteElements = SiteElements.Database;
            else
                enmSiteElements = SiteElements.None;

            switch (enmSiteElements)
            {
                case SiteElements.Site:
                    GlobalFunctions.ShowModalAlertMessages(Page, "Folder/Virtual Directory with specified site name already exists, delete the folder and try again", AlertType.Failure);
                    break;
                case SiteElements.Database:
                    GlobalFunctions.ShowModalAlertMessages(Page, "Database with specified site name already exists, delete the database and try again", AlertType.Failure);
                    break;
                case SiteElements.SiteAndDatabase:
                    GlobalFunctions.ShowModalAlertMessages(Page, "Folder/Virtual Directory and Database with specified site name already exists, delete the folder and database and try again", AlertType.Failure);
                    break;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }

        return enmSiteElements;
    }

    private bool IsSiteFolderExists(string strWebsiteHostPath)
    {
        try
        {
            if (Directory.Exists(strWebsiteHostPath) == true)
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }

    }

    private bool ReplaceConnectionStringInCreatedSite(string BaseStoreName, string SiteName, string DatabasePassword)
    {
        bool blnResult = false;

        //will alter connection string in web.config file of just created site.
        StreamWriter objStreamWriter;
        StreamReader objStreamReader;
        string strFileContent = "";
        string strConfigurationFile = "";

        strConfigurationFile = Convert.ToString(ConfigurationManager.AppSettings["WebsiteHostPath"]).Replace("$StoreName$", SiteName) + "\\web.config";
        try
        {
            objStreamReader = new StreamReader(strConfigurationFile);

            //read the file
            if (objStreamReader != null)
                strFileContent = objStreamReader.ReadToEnd();

            objStreamReader.Close();
            objStreamReader = null;

            if (BaseStoreName == "CorporateStoreTemplate")
                BaseStoreName = BaseStoreName.ToLower();

            strFileContent = strFileContent.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + SiteName);
            strFileContent = strFileContent.Replace("$StoreName$", SiteName);
            strFileContent = strFileContent.Replace("$UserName$", SiteName);
            strFileContent = strFileContent.Replace("$Password$", DatabasePassword);

            //code to uncheck file from read only
            FileInfo objFS = new FileInfo(strConfigurationFile);
            bool blnWasReadOnly = false;
            if (objFS.IsReadOnly == true)
            {
                objFS.IsReadOnly = false;
                blnWasReadOnly = true;
            }//end of if (objFS.IsReadOnly == true)
            //*************************************

            objStreamWriter = new StreamWriter(strConfigurationFile);

            objStreamWriter.Write(strFileContent);
            objStreamWriter.Flush();

            objStreamWriter.Close();
            objStreamWriter = null;

            //*************************************
            //code to make file read only, if it was
            if (blnWasReadOnly == true)
            {
                objFS.IsReadOnly = true;
            }//end of if (blnWasReadOnly == true)
            objFS = null;
            //*************************************

            blnResult = true;

        }//end of try
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            blnResult = false;
        }//end of catch
        finally
        {
            objStreamWriter = null;
            objStreamReader = null;
            strFileContent = null;
            strConfigurationFile = null;
        }//end of finally

        return blnResult;
    }

    private bool CreateVirtualDirectoryORDWebSiteInIIS(string serverName, string physicalPath, string siteName)
    {
        Exceptions.WriteInfoLog("Enter in CreateVirtualDirectoryORDWebSiteInIIS Method");
        bool blnCreateVirtualDirectoryORDWebSiteInIIS = false;
        try
        {
            // Create new Website Info object To Represent Instance Of WebSite
            WebSiteInfo objWebSiteInfo = new WebSiteInfo();
            string strSiteUrl = Convert.ToString(GlobalFunctions.GetSetting("CS_STORESMANAGEMENT_DEFAULTDOMAINNAME")) + "/" + siteName;

            // Assign values to object
            objWebSiteInfo.ServerName = serverName;
            objWebSiteInfo.PhysicalPath = physicalPath;

            //Virtual Directory Option Is Selected...                
            string strURL = strSiteUrl.Substring(0, strSiteUrl.IndexOf('/'));
            objWebSiteInfo.ParentWebSiteName = strURL;
            objWebSiteInfo.DirectoryName = siteName;

            // Assign Access rights to New Web Site
            objWebSiteInfo.HasBrowseAccess = true;
            objWebSiteInfo.HasReadAccess = true;
            objWebSiteInfo.HasWriteAccess = false;
            objWebSiteInfo.HasExecuteAccess = true;
            objWebSiteInfo.IsAnonymousAccessAllow = true;
            objWebSiteInfo.IsBasicAuthenticationSet = false;
            objWebSiteInfo.IsNTLMAuthenticationSet = false;
            Exceptions.WriteInfoLog("Before Calling CreateVirtualDirectory Method");
            blnCreateVirtualDirectoryORDWebSiteInIIS = IISHelper.CreateVirtualDirectory(objWebSiteInfo, IISHelper.IISVirsion.IIS6);
            Exceptions.WriteInfoLog("After Calling CreateVirtualDirectory Method");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            blnCreateVirtualDirectoryORDWebSiteInIIS = false;
        }
        return blnCreateVirtualDirectoryORDWebSiteInIIS;
    }

    private bool DeleteSite(string strSiteName)
    {
        bool blnDeleteSite = false;
        try
        {
            string strWebsiteHostPath = Convert.ToString(ConfigurationManager.AppSettings["WebsiteHostPath"]).Replace("$StoreName$", strSiteName);
            string strParentFolderName = Convert.ToString(GlobalFunctions.GetSetting("CS_STORESMANAGEMENT_FULLYQUALIFIEDPARENTFOLDERNAME"));

            if (strWebsiteHostPath.Substring(strWebsiteHostPath.LastIndexOf('\\')).ToLower() == strParentFolderName.ToLower() == false)
                if (Directory.Exists(strWebsiteHostPath))
                {
                    //This will assign the access right to directory
                    DirectorySecurity objDirectorySecurity = null;
                    FileSystemAccessRule objFileSystemAccessRule = null;
                    DirectoryInfo objDirectoryInfo = new DirectoryInfo(strWebsiteHostPath);
                    //*************************************************************
                    //code to add the access level for folder and its contents
                    //this is done so that it can be deleted programmetically
                    objDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
                    objFileSystemAccessRule = new FileSystemAccessRule("everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);

                    objDirectorySecurity = objDirectoryInfo.GetAccessControl(AccessControlSections.Access);
                    objDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
                    objDirectoryInfo.SetAccessControl(objDirectorySecurity);

                    objDirectorySecurity = null;
                    objFileSystemAccessRule = null;
                    //*************************************************************
                    //Remove Read Only Attribute Of ApplicationConstants.Config File
                    string strAppConstFilePath = strWebsiteHostPath + "/" + "applicationConstants.config";
                    if (File.Exists(strAppConstFilePath))
                    {
                        FileInfo objFileInfo = new FileInfo(strAppConstFilePath);
                        if (objFileInfo.IsReadOnly)
                            objFileInfo.IsReadOnly = false;
                    }
                    //Actual Delete
                    Directory.Delete(strWebsiteHostPath, true);
                    blnDeleteSite = true;
                }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return blnDeleteSite;
    }

    private void AssignAccessRights(string WebsitePath)
    {
        Exceptions.WriteInfoLog("website path: " + WebsitePath);
        Exceptions.WriteInfoLog("Before Calling AssignAccessRights Method");

        try
        {
            string CSSFolderPath = WebsitePath + @"\CSS";
            string HomeFolderPath = WebsitePath + @"\Home";
            string BackupFolderPath = WebsitePath + @"\Backup";
            string ImagesFolderPath = WebsitePath + @"\Images";
            string MasterFolderPath = WebsitePath + @"\Master";
            string CSVFolderPath = WebsitePath + @"\CSV";
            string FontsUploadFolderPath = WebsitePath + @"\FontsUpload";
            string FontsFolderPath = WebsitePath + @"\Fonts";
            string SearchFolderPath = WebsitePath + @"\Search";
            string ExceptionLogsFolderPath = WebsitePath + @"\ExceptionLogs";
            string ProductTranslationFolderPath = WebsitePath + @"\ProductTranslation";
            string AdminImagesFolderPath = WebsitePath + @"\Admin\Images";
            string AdminSettingsFolderPath = WebsitePath + @"\Admin\Settings";
            string XMLFolderPath = WebsitePath + @"\XML";
            string JSFolderPath = WebsitePath + @"\JS";
            string RobotFolderPath = WebsitePath;

            DirectoryInfo objCSSDirectoryInfo = new DirectoryInfo(CSSFolderPath);
            DirectoryInfo objHomeDirectoryInfo = new DirectoryInfo(HomeFolderPath);
            DirectoryInfo objBackupDirectoryInfo = new DirectoryInfo(BackupFolderPath);
            DirectoryInfo objImagesDirectoryInfo = new DirectoryInfo(ImagesFolderPath);
            DirectoryInfo objMasterDirectoryInfo = new DirectoryInfo(MasterFolderPath);
            DirectoryInfo objCSVDirectoryInfo = new DirectoryInfo(CSVFolderPath);
            DirectoryInfo objFontsUploadDirectoryInfo = new DirectoryInfo(FontsUploadFolderPath);
            DirectoryInfo objFontsDirectoryInfo = new DirectoryInfo(FontsFolderPath);
            DirectoryInfo objSearchDirectoryInfo = new DirectoryInfo(SearchFolderPath);
            DirectoryInfo objExceptionLogsDirectoryInfo = new DirectoryInfo(ExceptionLogsFolderPath);
            DirectoryInfo objProductTranslationDirectoryInfo = new DirectoryInfo(ProductTranslationFolderPath);
            DirectoryInfo objAdminImagesDirectoryInfo = new DirectoryInfo(AdminImagesFolderPath);
            DirectoryInfo objAdminSettingsDirectoryInfo = new DirectoryInfo(AdminSettingsFolderPath);
            DirectoryInfo objXMLDirectoryInfo = new DirectoryInfo(XMLFolderPath);
            DirectoryInfo objJSDirectoryInfo = new DirectoryInfo(JSFolderPath);
            DirectoryInfo objRobotDirectoryInfo = new DirectoryInfo(RobotFolderPath);


            DirectorySecurity objCSSDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objHomeDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objBackupDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objImagesDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objMasterDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objCSVDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objFontsUploadDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objFontsDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objSearchDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objExceptionLogsDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objProductTranslationDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objAdminImagesDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objAdminSettingsDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objXMLDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objJSDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
            DirectorySecurity objRobotDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();


            FileSystemAccessRule objFileSystemAccessRule = new FileSystemAccessRule("IIS_IUSRS", FileSystemRights.Read | FileSystemRights.ReadAndExecute | FileSystemRights.Write | FileSystemRights.Modify, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);

            objCSSDirectorySecurity = objCSSDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objHomeDirectorySecurity = objHomeDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objBackupDirectorySecurity = objBackupDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objImagesDirectorySecurity = objImagesDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objMasterDirectorySecurity = objMasterDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objCSVDirectorySecurity = objCSVDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objFontsUploadDirectorySecurity = objFontsUploadDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objFontsDirectorySecurity = objFontsDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objSearchDirectorySecurity = objSearchDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objExceptionLogsDirectorySecurity = objExceptionLogsDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objProductTranslationDirectorySecurity = objProductTranslationDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objAdminImagesDirectorySecurity = objAdminImagesDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objAdminSettingsDirectorySecurity = objAdminSettingsDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objXMLDirectorySecurity = objXMLDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objJSDirectorySecurity = objJSDirectoryInfo.GetAccessControl(AccessControlSections.Access);
            objRobotDirectorySecurity = objRobotDirectoryInfo.GetAccessControl(AccessControlSections.Access);

            objCSSDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objHomeDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objBackupDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objImagesDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objMasterDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objCSVDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objFontsUploadDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objFontsDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objSearchDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objExceptionLogsDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objProductTranslationDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objAdminImagesDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objAdminSettingsDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objXMLDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objJSDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
            objRobotDirectorySecurity.AddAccessRule(objFileSystemAccessRule);

            objCSSDirectoryInfo.SetAccessControl(objCSSDirectorySecurity);
            objHomeDirectoryInfo.SetAccessControl(objHomeDirectorySecurity);
            objBackupDirectoryInfo.SetAccessControl(objBackupDirectorySecurity);
            objImagesDirectoryInfo.SetAccessControl(objImagesDirectorySecurity);
            objMasterDirectoryInfo.SetAccessControl(objMasterDirectorySecurity);
            objCSVDirectoryInfo.SetAccessControl(objCSVDirectorySecurity);
            objFontsUploadDirectoryInfo.SetAccessControl(objFontsUploadDirectorySecurity);
            objFontsDirectoryInfo.SetAccessControl(objFontsDirectorySecurity);
            objSearchDirectoryInfo.SetAccessControl(objSearchDirectorySecurity);
            objExceptionLogsDirectoryInfo.SetAccessControl(objExceptionLogsDirectorySecurity);
            objProductTranslationDirectoryInfo.SetAccessControl(objProductTranslationDirectorySecurity);
            objAdminImagesDirectoryInfo.SetAccessControl(objAdminImagesDirectorySecurity);
            objAdminSettingsDirectoryInfo.SetAccessControl(objAdminSettingsDirectorySecurity);
            objXMLDirectoryInfo.SetAccessControl(objXMLDirectorySecurity);
            objJSDirectoryInfo.SetAccessControl(objJSDirectorySecurity);
            objRobotDirectoryInfo.SetAccessControl(objRobotDirectorySecurity);

            Exceptions.WriteInfoLog("After Calling AssignAccessRights Method");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    #endregion




    #region BindPopupDropDown
    protected void BindCatalogue()
    {
        try
        {
            List<CurrencyBE> GetAllCurrencies = CurrencyBL.GetAllCurrencyDetails();
            string currencyid = hdnCurCurrencyId.Value;
            CurrencyBE currencyBE = GetAllCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(currencyid));
            string currencyCode = currencyBE.CurrencyCode;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            if (Convert.ToInt32(ddlDivisionName.SelectedValue) != 0)
            {

                txtDivisionId.Text = Convert.ToString(ddlDivisionName.SelectedValue); ;

                dictionaryInstance.Add("DIVISION_ID", Convert.ToString(ddlDivisionName.SelectedValue));
                CatalogueBE objLstCatalogueBE = CatalogueBL.getCollectionItem(Constants.USP_GetAllNewCatalogueDetail, dictionaryInstance, false);
                List<CatalogueBE.Catalogues> lstcatalogue = objLstCatalogueBE.lstCatalogues.FindAll(x => x.CURRENCY_COUNTRY == currencyCode);

                //List<CatalogueBE.Key_Groups> lstkeyGroups = objLstCatalogueBE.lstkeyGroups.FindAll(x => x.CURRENCY_COUNTRY == currencyCode);

                if (objLstCatalogueBE.lstKey_Groups.Count > 0)
                {
                    ddlKeyGroupName.Items.Clear();
                    ddlKeyGroupName.DataSource = objLstCatalogueBE.lstKey_Groups;
                    //ddlKeyGroupName.DataSource = lstcatalogue;
                    ddlKeyGroupName.DataTextField = "GROUP_REF";
                    ddlKeyGroupName.DataValueField = "GROUP_ID";
                    ddlKeyGroupName.DataBind();
                    GlobalFunctions.AddDropdownItem(ref ddlKeyGroupName);
                }

                if (objLstCatalogueBE.lstCatalogues.Count > 0)
                {
                    ddlCatalogueName.Items.Clear();
                    //ddlCatalogueName.DataSource = objLstCatalogueBE.lstCatalogues;
                    ddlCatalogueName.DataSource = lstcatalogue;
                    ddlCatalogueName.DataValueField = "CATALOGUE_ID";
                    ddlCatalogueName.DataTextField = "CATALOGUE_REF";
                    ddlCatalogueName.DataBind();
                    GlobalFunctions.AddDropdownItem(ref ddlCatalogueName);
                }
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);

        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void BindKeyGroup()
    {
        try
        {
            if (Convert.ToInt32(ddlKeyGroupName.SelectedValue) != 0)
            {
                txtKeyGroupId.Text = ddlKeyGroupName.SelectedValue;
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void BindSourceCode()
    {
        try
        {

            if (Convert.ToInt32(ddlCatalogueName.SelectedValue) != 0 && ddlCatalogueName.SelectedValue != "")
            {
                ddlSourceCodeName.Items.Clear();
                txtCatalogueId.Text = ddlCatalogueName.SelectedValue;
                CatalogueBE.Source_Codes objSource_Codes = new CatalogueBE.Source_Codes();
                objSource_Codes.CATALOGUE_ID = Convert.ToInt32(ddlCatalogueName.SelectedValue);
                ddlSourceCodeName.DataSource = CatalogueBL.GetSource_CodesByCatId(objSource_Codes);
                ddlSourceCodeName.DataValueField = "SOURCE_CODE_ID";
                ddlSourceCodeName.DataTextField = "SOURCE_CODE";
                ddlSourceCodeName.DataBind();
                GlobalFunctions.AddDropdownItem(ref ddlSourceCodeName);
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void BindCustomers()
    {
        try
        {


            if (Convert.ToInt32(ddlSourceCodeName.SelectedValue) != 0)
            {
                ddlDefaultCustomerName.Items.Clear();
                txtSourceCodeId.Text = ddlSourceCodeName.SelectedValue;
                CatalogueBE.Web_Search_Accounts obj = new CatalogueBE.Web_Search_Accounts();
                obj.SOURCE_CODE_ID = Convert.ToInt32(ddlSourceCodeName.SelectedValue);
                List<CatalogueBE.Web_Search_Accounts> objList = CatalogueBL.GetWeb_Search_AccountsBySourceId(obj);

                ddlDefaultCustomerName.DataSource = objList;
                ddlDefaultCustomerName.DataTextField = "Name";
                ddlDefaultCustomerName.DataValueField = "CUSTOMER_ID";
                ddlDefaultCustomerName.DataBind();
                GlobalFunctions.AddDropdownItem(ref ddlDefaultCustomerName);
            }

            // ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);

        }
        catch (Exception Ex)
        {
            throw;
        }
    }

    protected void BindDefaultCustomer()
    {
        try
        {

            if (Convert.ToInt32(ddlDefaultCustomerName.SelectedValue) != 0)
            {

                txtDefaultCustomerId.Text = ddlDefaultCustomerName.SelectedValue;

                CatalogueBE.Web_Search_Accounts obj = new CatalogueBE.Web_Search_Accounts();
                obj.CUSTOMER_ID = Convert.ToInt32(ddlDefaultCustomerName.SelectedValue);
                List<CatalogueBE.Web_Search_Accounts> objList = CatalogueBL.GetWeb_SearchDatabyCustomerId(obj);
                //objList.RemoveAll(x => x.CUSTOMER_ID != Convert.ToInt32(ddlDefaultCustomerName.SelectedValue) && x.CURRENCY_COUNTRY != ddlCurrency.SelectedValue);

                txtSalesPersonId.Text = Convert.ToString(objList[0].SALESPERSON_ID);
                txtDefaultCustomerId.Text = Convert.ToString(objList[0].CUSTOMER_ID);
                txtDefault_CUST_CONTACT_ID.Text = Convert.ToString(objList[0].Customer_Contact_ID);
                txtGroupId.Text = "8";
                txtDefaultCompanyId.Text = Convert.ToString(objList[0].CUSTOMER_ID);
            }
            // ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }


    private void BindDivisions()
    {
        try
        {

            //Start Binding DivisionName DropDown
            List<CatalogueBE.Division> GetAllDivision = CatalogueBL.GetAllDivision();
            var aDivision = (from d in GetAllDivision
                             select new { d.DIVISION_ID, d.DIVISION_CODE });
            ddlDivisionName.DataSource = aDivision.Distinct();
            ddlDivisionName.DataTextField = "DIVISION_CODE";
            ddlDivisionName.DataValueField = "DIVISION_ID";
            ddlDivisionName.DataBind();
            GlobalFunctions.AddDropdownItem(ref ddlDivisionName);

            //End

            GlobalFunctions.AddDropdownItem(ref ddlKeyGroupName);
            GlobalFunctions.AddDropdownItem(ref ddlCatalogueName);
            GlobalFunctions.AddDropdownItem(ref ddlSourceCodeName);
            GlobalFunctions.AddDropdownItem(ref ddlDefaultCustomerName);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void ddlDivisionName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ClearpopupFields();
            BindCatalogue();
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();$('')", true);

        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
    protected void ddlKeyGroupName_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindKeyGroup();
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);

    }
    protected void ddlCatalogueName_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSourceCode();
        if (ddlCatalogueName.SelectedValue == "0")
        {
            txtDefaultCustomerId.Text = "";
            txtSalesPersonId.Text = "";
            txtGlobalCostCentreNumber.Text = "";
            txtSourceCodeId.Text = "";
            ddlSourceCodeName.DataSource = null;
            ddlSourceCodeName.DataBind();
            ddlDefaultCustomerName.DataSource = null;
            ddlDefaultCustomerName.DataBind();
            txtDefault_CUST_CONTACT_ID.Text = "";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);

    }
    protected void ddlDefaultCustomerName_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDefaultCustomer();
        if (ddlCatalogueName.SelectedValue == "0")
        {
            txtDefaultCustomerId.Text = "";
            txtSalesPersonId.Text = "";
            txtGlobalCostCentreNumber.Text = "";
            txtSourceCodeId.Text = "";
            ddlSourceCodeName.DataSource = null;
            ddlSourceCodeName.DataBind();
            txtDefault_CUST_CONTACT_ID.Text = "";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);
    }
    protected void ddlSourceCodeName_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCustomers();
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);
    }
    #endregion

    #region ClearpopupFields
    protected void ClearpopupFields()
    {
        try
        {
            ddlDivisionName.DataSource = null;
            ddlDivisionName.DataBind();
            ddlKeyGroupName.DataSource = null;
            ddlKeyGroupName.DataBind();
            ddlCatalogueName.DataSource = null;
            ddlCatalogueName.DataSource = null;
            ddlCatalogueName.DataBind();
            ddlSourceCodeName.DataSource = null;
            ddlSourceCodeName.DataBind();
            ddlDefaultCustomerName.DataSource = null;
            ddlDefaultCustomerName.DataBind();
            txtCatalogueAliasId.Text = "";
            txtCatalogueId.Text = "";
            txtDivisionId.Text = "";
            txtGroupId.Text = "8";
            txtSalesPersonId.Text = "";
            txtGlobalCostCentreNumber.Text = "";
            txtSourceCodeId.Text = "";
            txtKeyGroupId.Text = "";
            txtDefaultCompanyId.Text = "";
            txtDefaultCustomerId.Text = "";
            txtDefault_CUST_CONTACT_ID.Text = "";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
    #endregion
    #region CreateTempTableStructure
    protected DataTable CreateDataTableStructure()
    {
        DataTable dtInsert = new DataTable();
        try
        {

            dtInsert.Columns.Add("CurrencyId", typeof(int));
            dtInsert.Columns.Add("CatalogueId", typeof(int));
            dtInsert.Columns.Add("GlobalCostCentre", typeof(string));
            dtInsert.Columns.Add("GroupId", typeof(int));
            dtInsert.Columns.Add("DivisionId", typeof(int));
            dtInsert.Columns.Add("DefaultCustomerId", typeof(int));
            dtInsert.Columns.Add("CatalogueAlias", typeof(string));
            dtInsert.Columns.Add("SourceCodeId", typeof(string));
            dtInsert.Columns.Add("KeyGroupId", typeof(int));
            dtInsert.Columns.Add("DivisionName", typeof(string));
            dtInsert.Columns.Add("CatalogueName", typeof(string));
            dtInsert.Columns.Add("KeyGroupName", typeof(string));
            dtInsert.Columns.Add("SourceCodeName", typeof(string));
            dtInsert.Columns.Add("SalesPersonId", typeof(string));
            dtInsert.Columns.Add("DefaultCompanyId", typeof(string));
            dtInsert.Columns.Add("Default_CUST_CONTACT_ID", typeof(string));
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            dtInsert = null;
        }
        return dtInsert;
    }
    #endregion

    #region TempSaveandUpdatePopupRecords
    protected void btnAddcurrencydetail_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            if (ViewState["datatablecurrency"] != null)
            {
                dt = ViewState["datatablecurrency"] as DataTable;
            }
            else
            {
                dt = CreateDataTableStructure();
            }
            SaveDataTableValues(dt);



            ddlDivisionName.Items.Clear();
            ddlKeyGroupName.Items.Clear();
            ddlCatalogueName.Items.Clear();
            ddlSourceCodeName.Items.Clear();
            ddlDefaultCustomerName.Items.Clear();

            //ddlDivisionName.DataSource = null;
            //ddlDivisionName.DataBind();

            //ddlKeyGroupName.DataSource = null;
            //ddlKeyGroupName.DataBind();

            //ddlCatalogueName.DataSource = null;
            //ddlCatalogueName.DataBind();

            //ddlSourceCodeName.DataSource = null;
            //ddlSourceCodeName.DataBind();

            //ddlDefaultCustomerName.DataSource = null;
            //ddlDefaultCustomerName.DataBind();


            BindDivisions();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
    protected void SaveDataTableValues(DataTable dtInsert)
    {
        try
        {
            string mode = "add";
            DataRow[] drArr = dtInsert.Select("CurrencyID" + "=" + hdnCurCurrencyId.Value);
            // new added
            foreach (DataRow dr in drArr)
            {
                dtInsert.Rows.Remove(dr);
            }
            DataRow drNew = dtInsert.NewRow();
            drNew["CurrencyId"] = hdnCurCurrencyId.Value;
            drNew["CatalogueId"] = ddlCatalogueName.SelectedValue;
            drNew["GlobalCostCentre"] = txtGlobalCostCentreNumber.Text;
            drNew["GroupId"] = txtGroupId.Text;
            drNew["DivisionId"] = txtDivisionId.Text;
            drNew["DivisionName"] = ddlDivisionName.SelectedItem;
            drNew["DefaultCustomerId"] = txtDefaultCustomerId.Text;
            drNew["CatalogueAlias"] = ""; //txtCatalogueAliasId.Text;
            drNew["SourceCodeId"] = txtSourceCodeId.Text;
            drNew["CatalogueId"] = txtCatalogueId.Text;
            drNew["CatalogueName"] = ddlCatalogueName.SelectedItem;
            drNew["KeyGroupId"] = txtKeyGroupId.Text;
            drNew["KeyGroupName"] = ddlKeyGroupName.SelectedItem;
            drNew["SourceCodeName"] = ddlSourceCodeName.SelectedItem;
            drNew["SalesPersonId"] = txtSalesPersonId.Text;
            drNew["DefaultCompanyId"] = txtDefaultCompanyId.Text;
            drNew["Default_CUST_CONTACT_ID"] = txtDefault_CUST_CONTACT_ID.Text;

            dtInsert.Rows.Add(drNew);
            ViewState["datatablecurrency"] = dtInsert;
            //ddlDivisionName.SelectedIndex = 0;//added 23-09-2016
            //  new end


            //if (Convert.ToString(Request.QueryString["m"]).ToLower() == "edit")
            //{
            //    if (drArr.Length == 0)
            //    {
            //        mode = "add";
            //    }
            //    else if (drArr.Count() != 0 && drArr[0]["DivisionId"] != txtDivisionId.Text)
            //    {
            //        if (hdnCurCurrencyId.Value != "" && hdnCurCurrencyId.Value != null)
            //        {
            //            mode = "update";
            //        }
            //    }
            //}
            //else
            //{
            //    if (drArr.Length == 0)
            //    {
            //        mode = "add";
            //    }
            //}

            //if (mode == "add")
            //{
            //    DataRow dr = dtInsert.NewRow();
            //    dr["CurrencyId"] = hdnCurCurrencyId.Value;
            //    dr["CatalogueId"] = ddlCatalogueName.SelectedValue;
            //    dr["GlobalCostCentre"] = txtGlobalCostCentreNumber.Text;
            //    dr["GroupId"] = txtGroupId.Text;
            //    dr["DivisionId"] = txtDivisionId.Text;

            //    dr["DefaultCustomerId"] = txtDefaultCustomerId.Text;
            //    dr["CatalogueAlias"] = ""; //txtCatalogueAliasId.Text;
            //    dr["SourceCodeId"] = txtSourceCodeId.Text;
            //    dr["CatalogueId"] = txtCatalogueId.Text;
            //    dr["CatalogueName"] = ddlCatalogueName.SelectedItem;
            //    dr["KeyGroupId"] = txtKeyGroupId.Text;
            //    dr["KeyGroupName"] = ddlKeyGroupName.SelectedItem;
            //    dr["SourceCodeName"] = ddlSourceCodeName.SelectedItem;
            //    dr["SalesPersonId"] = txtSalesPersonId.Text;
            //    dr["DefaultCompanyId"] = txtDefaultCompanyId.Text;
            //    dr["Default_CUST_CONTACT_ID"] = txtDefault_CUST_CONTACT_ID.Text;
            //    dtInsert.Rows.Add(dr);
            //    ViewState["datatablecurrency"] = dtInsert;
            //}
            //else
            //{

            //    DataRow dr = dtInsert.Rows[0];
            //    dr["CatalogueId"] = ddlCatalogueName.SelectedValue;
            //    dr["GlobalCostCentre"] = txtGlobalCostCentreNumber.Text;
            //    dr["GroupId"] = txtGroupId.Text;
            //    dr["DivisionId"] = txtDivisionId.Text;
            //    dr["DivisionName"] = ddlDivisionName.SelectedItem;

            //    dr["DefaultCustomerId"] = txtDefaultCustomerId.Text;
            //    dr["CatalogueAlias"] = ""; //txtCatalogueAliasId.Text;
            //    dr["SourceCodeId"] = txtSourceCodeId.Text;
            //    dr["CatalogueId"] = txtCatalogueId.Text;
            //    dr["CatalogueName"] = ddlCatalogueName.SelectedItem;
            //    dr["KeyGroupId"] = txtKeyGroupId.Text;
            //    dr["KeyGroupName"] = ddlKeyGroupName.SelectedItem;
            //    dr["SourceCodeName"] = ddlSourceCodeName.SelectedItem;
            //    dr["SalesPersonId"] = txtSalesPersonId.Text;
            //    dr["DefaultCompanyId"] = txtDefaultCompanyId.Text;
            //    dr["Default_CUST_CONTACT_ID"] = txtDefault_CUST_CONTACT_ID.Text;

            //    //ViewState["datatablecurrency"] = dtInsert;
            //}



        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }

    }
    protected void ViewCatalogueDetails(int CurrencyId)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = ViewState["datatablecurrency"] as DataTable;


            DataRow[] drArr = dt.Select("CurrencyID" + "=" + CurrencyId);

            txtCatalogueId.Text = Convert.ToString(drArr[0]["CatalogueId"]);
            txtGlobalCostCentreNumber.Text = Convert.ToString(drArr[0]["GlobalCostCentre"]);
            txtGroupId.Text = Convert.ToString(drArr[0]["GroupId"]);
            txtDivisionId.Text = Convert.ToString(drArr[0]["DivisionId"]);

            ddlDivisionName.SelectedValue = Convert.ToString(drArr[0]["DivisionId"]);
            BindCatalogue();
            BindKeyGroup();
            ddlKeyGroupName.SelectedValue = Convert.ToString(drArr[0]["KeyGroupId"]);

            ddlCatalogueName.SelectedValue = Convert.ToString(drArr[0]["CatalogueId"]);
            BindSourceCode();

            ddlSourceCodeName.SelectedValue = Convert.ToString(drArr[0]["SourceCodeId"]);
            BindCustomers();
            ddlDefaultCustomerName.SelectedValue = Convert.ToString(drArr[0]["DefaultCustomerId"]);
            BindDefaultCustomer();



            txtDefault_CUST_CONTACT_ID.Text = Convert.ToString(drArr[0]["Default_CUST_CONTACT_ID"]);
            txtCatalogueAliasId.Text = Convert.ToString(drArr[0]["CatalogueId"]);

            txtKeyGroupId.Text = Convert.ToString(drArr[0]["KeyGroupId"]);

            txtSourceCodeId.Text = Convert.ToString(drArr[0]["SourceCodeId"]);

            txtSalesPersonId.Text = Convert.ToString(drArr[0]["SalesPersonId"]);

            txtDefaultCompanyId.Text = Convert.ToString(drArr[0]["DefaultCompanyId"]);
            txtDefaultCustomerId.Text = Convert.ToString(drArr[0]["DefaultCustomerId"]);

            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }


    }
    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            int CurrencyId = Convert.ToInt32(lnk.CommandArgument);
            hdnCurCurrencyId.Value = Convert.ToString(CurrencyId);
            ViewCatalogueDetails(CurrencyId);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    public static DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }

    protected void DeleteDataTableRecords()
    {
        try
        {
            DataTable dt = ViewState["datatablecurrency"] as DataTable;


            foreach (RepeaterItem rptitem in rptCurrencies.Items)
            {
                CheckBox chkCurrency = (CheckBox)rptitem.FindControl("chkCurrency");
                Label lblCurrcurrencyId = (Label)rptitem.FindControl("lblCurrcurrencyId");
                LinkButton cmdEdit = (LinkButton)rptitem.FindControl("cmdEdit");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DataRow[] drarr = dt.Select("CurrencyId=" + lblCurrcurrencyId.Text);
                        if (chkCurrency.Checked)
                        {
                        }
                        else
                        {
                            if (drarr.Count() > 0)
                            {
                                foreach (DataRow dr in drarr)
                                {
                                    dt.Rows.Remove(dr);
                                }
                            }
                        }
                        if (drarr.Count() > 0)
                        {
                            chkCurrency.Checked = true;
                            cmdEdit.Visible = true;
                        }
                        else
                        {
                            chkCurrency.Checked = false;
                            cmdEdit.Visible = false;
                        }
                    }
                }
                else
                {
                    chkCurrency.Checked = false;
                    cmdEdit.Visible = false;
                }
            }


            ViewState["datatablecurrency"] = dt;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DeleteDataTableRecords();
        ClearpopupFields();
        ddlDivisionName.Items.Clear();
        ddlKeyGroupName.Items.Clear();
        ddlCatalogueName.Items.Clear();
        ddlSourceCodeName.Items.Clear();
        ddlDefaultCustomerName.Items.Clear();

        BindDivisions();
    }
    #endregion
    //protected void chkCurrency_CheckedChanged(object sender, EventArgs e)
    //{
    //    ClearpopupFields();
    //    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: ShowModalAddCatalogue();", true);

    //    foreach (RepeaterItem rpt in rptCurrencies.Items)
    //    {
    //        CheckBox chkCurrency = (CheckBox)rpt.FindControl("chkCurrency");
    //        if (chkCurrency.Checked)
    //        {
    //            Label lblCurrcurrencyId = (Label)rpt.FindControl("lblCurrcurrencyId");
    //            if (!hdnAllSetCurrency.Value.Contains(lblCurrcurrencyId.Text))
    //            {
    //                hdnCurCurrencyId.Value = lblCurrcurrencyId.Text;
    //            }
    //            hdnAllSetCurrency.Value += lblCurrcurrencyId.Text;
    //        }
    //    }
    //}
}

