﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StoreCreator_RedirectToAddTemplate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GlobalFunctions.IsAuthorized(HttpContext.Current.Request.Url.Segments[HttpContext.Current.Request.Url.Segments.Length - 1]);
        try
        {
            Session["TemplateMode"] = "create";
            Response.Redirect("~/StoreCreator/AddEditTemplate.aspx");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
}