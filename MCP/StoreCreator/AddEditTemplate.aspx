﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MCPMaster.master" AutoEventWireup="true" CodeFile="AddEditTemplate.aspx.cs" Inherits="StoreCreator_AddEditTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../JS/StoreCreator.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function ShowTemplateLoader() {
            if ($.trim($("#txtTemplateName").val()) != "")
            $('#dvLoader').show();

        }
        function HideTemplateLoader() {
            $('#dvLoader').hide();
        }
    </script>
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../DashBoard.aspx">Home</a></li>
            <li>Templates</li>
            <li>Add/Edit Template</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container  ">
            <div class="wrap_container ">
                <div class="content">
                    <h3>Upload Theme</h3>
                    <p></p>
                    <div class="row select_data collapsableMenu ">
                        <div class="form-group clearfix">
                            <div class="col-md-2">
                                <h6>Template Name</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="">
                                    <asp:TextBox ID="txtTemplateName" ClientIDMode="Static" runat="server" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvStoreName" runat="server" ControlToValidate="txtTemplateName"
                                        Display="dynamic" Text="(Required)" ErrorMessage="Please enter template name" ForeColor="Red"
                                        ValidationGroup="OnClickSubmit">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-md-2">
                                <h6>Browse</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="">
                                <asp:FileUpload ID="fuUploadTemplate" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div id="dvTemplateStores" runat="server" class="form-group clearfix">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-10">
                                <div class="">
                                    <div class="wrap_lang" id="dvLanguages" clientidmode="static">
                                        <ul>
                                            <asp:Repeater ID="rptTemplateStores" runat="server">
                                                <HeaderTemplate>
                                                    <li>
                                                        <div class="col-md-3">Template used in Stores    </div>
                                                        <%--<div class="col-md-2 text-center">Is Default </div>--%>
                                                    </li>
                                                    <li>
                                                        <div class="col-md-3">
                                                            <asp:CheckBox ID="chkSelectDeSelectAll" ClientIDMode="Static" Checked="true" runat="server" Text='Select/DeSelect All' />
                                                        </div>
                                                    </li>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-3">
                                                            <asp:CheckBox ID="chkStores" CssClass="chkShow" Checked="true" runat="server" Text='<%#Eval("StoreName") %>' value='<%#Eval("StoreId") %>' />
                                                            <asp:HiddenField ID="hdnStores" runat="server" />
                                                        </div>
                                                        <%--<div id="dvDefaultLanguage" runat="server" clientidmode="static" class="col-md-2 text-center radio radio-danger radio-storeType">
                                                            <input id="rbDefaultLanguage" runat="server" type="radio" name="DefaultLanguage" clientidmode="static" />
                                                            <label></label>
                                                        </div>--%>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-12">
                                <ul class="button_section">
                                    <li>
                                        <asp:Button ID="cmdAddEditTemplate" runat="server" CssClass="btn" Text="Edit Template" OnClientClick="ShowTemplateLoader();" OnClick="cmdAddEditTemplate_Click" ValidationGroup="OnClickSubmit"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

