﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.PaymentTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
namespace Presentation
{
    public partial class Admin_PaymentManagement_PaymentTypeManagement : BasePage//System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.GridView grdwPayments;
        protected global::System.Web.UI.WebControls.Button btnGetPaymentType;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;/*User Type*/
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PopulateLanguage();/*User Type*/
                    btnGetPaymentType.Attributes.Add("onclick", " return confirm('Are you sure,You wants to replace all Payment Types?');");
                    PopulatePaymentTypes();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnGetPaymentType_Click(object sender, EventArgs e)
        {
            try
            {
                SavePaymentTypeXML();
                PopulatePaymentTypes();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void PopulateLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails(true);
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void SavePaymentTypeXML()
        {
            try
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    PaymentTypes objPayTypes = new PaymentTypes();
                    NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    objPayTypes.Credentials = objNetworkCredentials;
                    XmlNode ndPayTypes = objPayTypes.DivisionPaymentTypes(Convert.ToInt32(objStoreBE.StoreCurrencies[0].DivisionId));

                    PaymentTypesBL.SavePaymentTypeNode(ndPayTypes, Convert.ToInt32(objStoreBE.StoreCurrencies[0].DivisionId));
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        public void PopulatePaymentTypes()
        {
            try
            {
                //List<PaymentTypesBE> objPaymentTypesBE = PaymentTypesBL.GetAllPaymentTypesDetails();
                List<PaymentTypesBE> objPaymentTypesBE = PaymentTypesBL.GetAllPaymentTypesDetails(Convert.ToInt16(ddlLanguage.SelectedValue));/*User Type*/
                if (objPaymentTypesBE.Count > 0)
                {
                    grdwPayments.DataSource = objPaymentTypesBE;
                    grdwPayments.DataBind();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void grdwPayments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RadioButton rb = (RadioButton)e.Row.FindControl("rdBtnDefault");
                if (rb.Text == "Y")
                {
                    rb.Text = "";
                    rb.Checked = true;
                }
                else
                {
                    rb.Text = "";
                    rb.Checked = false;
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsUpdated = false;

                if (grdwPayments.Rows.Count > 0)
                {
                    for (int i = 0; i < grdwPayments.Rows.Count; i++)
                    {
                        CheckBox chkShow = (CheckBox)grdwPayments.Rows[i].FindControl("chkIsShow");
                        CheckBox chkInvoiceAccountId = (CheckBox)grdwPayments.Rows[i].FindControl("chkAccID");
                        RadioButton rbDefault = (RadioButton)grdwPayments.Rows[i].FindControl("rdBtnDefault");
                        TextBox txtPaymentType = (TextBox)grdwPayments.Rows[i].FindControl("txtPaymentType");
                        RadioButton rdBtnBudget = (RadioButton)grdwPayments.Rows[i].FindControl("rdBtnBudget");
                        RadioButton rdBtnPoint = (RadioButton)grdwPayments.Rows[i].FindControl("rdBtnPoint");
                        //IsUpdated = PaymentTypesBL.UpdatePaymentTypes(Convert.ToInt32(grdwPayments.DataKeys[i].Values[0]), chkShow.Checked, txtPaymentType.Text.Trim());
                        //IsUpdated = PaymentTypesBL.UpdatePaymentTypes(Convert.ToInt32(grdwPayments.DataKeys[i].Values[0]), chkShow.Checked, txtPaymentType.Text.Trim(), Convert.ToInt16(ddlLanguage.SelectedValue));/*User Type*/
                        IsUpdated = PaymentTypesBL.UpdatePaymentTypes(Convert.ToInt32(grdwPayments.DataKeys[i].Values[0]), chkShow.Checked, txtPaymentType.Text.Trim(), Convert.ToInt16(ddlLanguage.SelectedValue), chkInvoiceAccountId.Checked, rbDefault.Checked, rdBtnBudget.Checked, rdBtnPoint.Checked);
                    }
                    if (IsUpdated)
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Payment Types Submitted Successfully", AlertType.Success);
                        CreateActivityLog("PaymentTypeManagement", "Update", ddlLanguage.SelectedValue);
                    }
                    else
                        GlobalFunctions.ShowModalAlertMessages(Page, "Error while updating Payment Types", AlertType.Failure);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulatePaymentTypes();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}