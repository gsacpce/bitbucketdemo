﻿using PWGlobalEcomm.BusinessLogic;
using System;

public partial class Admin_PaymentManagement_ManagePaymenttype : System.Web.UI.Page
{
    protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                StoreBE objStoreBE = new StoreBE();
                objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    GlobalFunctions.BindAllLanguage(objStoreBE, ref ddlLanguage);
                    BindPaymenttypes();
                }
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    private void BindPaymenttypes()
    {
        try
        {
            //fetch data from tbl_Paymenttype_OASIS as per Language.
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
}