﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Presentation
{
    public partial class Admin_PaymentManagement_ManagePickListItems : BasePage//System.Web.UI.Page
    {
        #region Variables

        protected global::System.Web.UI.WebControls.GridView grdPickList;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage, ddlPaymentTypes;
        protected global::System.Web.UI.WebControls.Button btnSubmit;
        protected global::System.Web.UI.WebControls.HiddenField hdnSelectedPaymentTypeId;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateLanguage();
                BindPaymentTypes();

            }
        }
        private void BindPaymentTypes()
        {
            try
            {
                List<PaymentTypesBE> objPaymentTypesBE = PaymentTypesBL.GetAllPaymentTypesDetails(Convert.ToInt16(ddlLanguage.SelectedValue));
                List<PaymentTypesBE> objVisiblePaymentTypesBE = objPaymentTypesBE.FindAll(x => x.IsShowOnWebsite == true);
                if (objVisiblePaymentTypesBE != null)
                {
                    if (objVisiblePaymentTypesBE.Count > 0)
                    {
                        ddlPaymentTypes.DataSource = objVisiblePaymentTypesBE;
                        ddlPaymentTypes.DataTextField = "DisplayName";//PaymentTypeRef
                        ddlPaymentTypes.DataValueField = "PaymentTypeID";
                        ddlPaymentTypes.DataBind();
                        ddlPaymentTypes.Items.Insert(0, new ListItem("Select Payment Type", "0"));

                        if (Convert.ToString(hdnSelectedPaymentTypeId.Value) != null)
                        {
                            if (Convert.ToString(hdnSelectedPaymentTypeId.Value) != "")
                            {
                                ddlPaymentTypes.SelectedValue = Convert.ToString(hdnSelectedPaymentTypeId.Value);
                            }
                        }
                    }

                }
                else
                {
                    btnSubmit.Visible = false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void PopulateLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails(true);
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        public void PopulatePickListitems()
        {
            try
            {
                //List<PickListItemsBE> objPaymentTypesBE = PaymentTypesBL.GetAllPickListPaymentItems<PickListItemsBE>(Convert.ToInt16(ddlLanguage.SelectedValue), Convert.ToInt16(ddlPaymentTypes.SelectedValue));
                List<PickListItemsBE> objPaymentTypesBE = PaymentTypesBL.GetAllPickListPaymentItemsPID<PickListItemsBE>(Convert.ToInt16(ddlLanguage.SelectedValue), Convert.ToInt16(ddlPaymentTypes.SelectedValue));/*User Type*/

                if (objPaymentTypesBE.Count > 0)
                {
                    grdPickList.DataSource = objPaymentTypesBE;
                    grdPickList.DataBind();
                }
                else
                {
                    grdPickList.DataSource = null;
                    grdPickList.DataBind();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsUpdated = false;

                if (grdPickList.Rows.Count > 0)
                {
                    for (int i = 0; i < grdPickList.Rows.Count; i++)
                    {
                        //CheckBox chkShow = (CheckBox)grdPickList.Rows[i].FindControl("chkIsShow");
                        TextBox txtDisplayName = (TextBox)grdPickList.Rows[i].FindControl("txtDisplayName");

                        IsUpdated = PaymentTypesBL.UpdatePickListPaymentItems(Convert.ToInt32(grdPickList.DataKeys[i].Values[0]), txtDisplayName.Text.Trim());
                    }
                    if (IsUpdated)
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "PickList Updated Successfully", AlertType.Success);
                        CreateActivityLog("ManagePickListItems", "Update", ddlPaymentTypes.DataTextField);
                    }
                    else
                        GlobalFunctions.ShowModalAlertMessages(Page, "Error while updating Payment Types", AlertType.Failure);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnSelectedPaymentTypeId.Value = Convert.ToString(ddlPaymentTypes.SelectedValue);
            PopulatePickListitems();
            BindPaymentTypes();
        }

        protected void ddlPaymentTypes_SelectedIndexChanged(object sender, EventArgs e)
        {

            PopulatePickListitems();
        }
    }
}