﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Presentation
{
    public partial class Admin_PaymentManagement_UDFSManagement : BasePage//System.Web.UI.Page
    {
        #region Variables

        protected global::System.Web.UI.WebControls.DropDownList ddlPaymentTypes;
        protected global::System.Web.UI.WebControls.GridView grdwUDFS, gridCustomRef;
        protected global::System.Web.UI.WebControls.Label lblNoRecords;
        protected global::System.Web.UI.WebControls.Button btnSubmit;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;/*User Type*/
        protected global::System.Web.UI.WebControls.HiddenField hdnSelectedPaymentTypeId;/*User Type*/
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    btnSubmit.Visible = false;
                    PopulateLanguage();/*User Type*/
                    BindPaymentTypes();
                    //PopulateCustRefGrid();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /*User Type*/
        protected void PopulateLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails(true);
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindPaymentTypes()
        {
            try
            {
                //List<PaymentTypesBE> objPaymentTypesBE = PaymentTypesBL.GetAllPaymentTypesDetails();
                List<PaymentTypesBE> objPaymentTypesBE = PaymentTypesBL.GetAllPaymentTypesDetails(Convert.ToInt16(ddlLanguage.SelectedValue));/*User Type*/
                List<PaymentTypesBE> objVisiblePaymentTypesBE = objPaymentTypesBE.FindAll(x => x.IsShowOnWebsite == true);
                if (objVisiblePaymentTypesBE != null)
                {
                    if (objVisiblePaymentTypesBE.Count > 0)
                    {
                        //btnSubmit.Visible = false;
                        ddlPaymentTypes.DataSource = objVisiblePaymentTypesBE;
                        //ddlPaymentTypes.DataTextField = "PaymentTypeRef";
                        ddlPaymentTypes.DataTextField = "DisplayName";/*User Type*/
                        ddlPaymentTypes.DataValueField = "PaymentTypeID";
                        ddlPaymentTypes.DataBind();
                        ddlPaymentTypes.Items.Insert(0, new ListItem("Select Payment Type", "0"));
                    }
                    if (Convert.ToString(hdnSelectedPaymentTypeId.Value) != null)
                    {
                        if (Convert.ToString(hdnSelectedPaymentTypeId.Value) != "")
                        {
                            ddlPaymentTypes.SelectedValue = Convert.ToString(hdnSelectedPaymentTypeId.Value);
                        }
                    }
                }
                else
                {
                    //btnSubmit.Visible = false;
                    lblNoRecords.Text = "There are no Payment type available.";
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void PopulateGrid()
        {
            try
            {
                if (ddlPaymentTypes.SelectedIndex > 0)
                {
                    //List<UDFBE> objUDFBE = PaymentTypesBL.GetAllUDFSDetails<UDFBE>(Convert.ToInt16(ddlLanguage.SelectedValue));
                    List<UDFBE> objUDFBE = PaymentTypesBL.GetAllUDFSDetails<UDFBE>(Convert.ToInt16(ddlLanguage.SelectedValue), Convert.ToInt32(ddlPaymentTypes.SelectedValue));
                    if (objUDFBE != null)
                    {
                        List<UDFBE> PaymentTypeUDF = objUDFBE.FindAll(x => x.PaymentTypeID == Convert.ToInt32(ddlPaymentTypes.SelectedValue));
                        if (PaymentTypeUDF != null)
                        {
                            //btnSubmit.Visible = true;
                            grdwUDFS.DataSource = PaymentTypeUDF;
                            grdwUDFS.DataBind();

                            if (PaymentTypeUDF[0].PaymentIsBudget == true && PaymentTypeUDF[0].PaymentIsPoint == true)
                            {
                                grdwUDFS.Columns[4].Visible = true;
                                grdwUDFS.Columns[5].Visible = true;
                                grdwUDFS.DataSource = PaymentTypeUDF;
                                grdwUDFS.DataBind();
                            }
                            else if (PaymentTypeUDF[0].PaymentIsBudget == true)
                            {
                                grdwUDFS.Columns[5].Visible = false;
                                grdwUDFS.Columns[4].Visible = true;
                                grdwUDFS.DataSource = PaymentTypeUDF;
                                grdwUDFS.DataBind();

                            }
                            else if (PaymentTypeUDF[0].PaymentIsPoint == true)
                            {
                                grdwUDFS.Columns[5].Visible = true;
                                grdwUDFS.Columns[4].Visible = false;
                                grdwUDFS.DataSource = PaymentTypeUDF;
                                grdwUDFS.DataBind();
                            }
                            else
                            {
                                grdwUDFS.Columns[4].Visible = false;
                                grdwUDFS.Columns[5].Visible = false;
                                grdwUDFS.DataSource = PaymentTypeUDF;
                                grdwUDFS.DataBind();
                            }

                        }
                        else
                        {
                            //btnSubmit.Visible = false;
                            lblNoRecords.Text = "There are no UDF's available for this Payment type.";
                        }
                    }
                    else
                    {
                        //btnSubmit.Visible = false;
                        lblNoRecords.Text = "There are no UDF's available for this Payment type.";
                    }
                }
                else
                {
                    grdwUDFS.DataSource = null;
                    grdwUDFS.DataBind();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ddlPaymentTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateGrid();
                PopulateCustRefGrid();
                btnSubmit.Visible = true;

                #region "comments"
                //if (ddlPaymentTypes.SelectedIndex > 0)
                //{
                //    List<UDFBE> objUDFBE = PaymentTypesBL.GetAllUDFSDetails<UDFBE>();

                //    if (objUDFBE != null)
                //    {
                //        List<UDFBE> PaymentTypeUDF = objUDFBE.FindAll(x => x.PaymentTypeID == Convert.ToInt32(ddlPaymentTypes.SelectedValue));
                //        if (PaymentTypeUDF != null)
                //        {
                //            btnSubmit.Visible = true;
                //            grdwUDFS.DataSource = PaymentTypeUDF;
                //            grdwUDFS.DataBind();
                //        }
                //        else
                //        {
                //            btnSubmit.Visible = false;
                //            lblNoRecords.Text = "There are no UDF's available for this Payment type.";
                //        }
                //    }
                //    else
                //    {
                //        btnSubmit.Visible = false;
                //        lblNoRecords.Text = "There are no UDF's available for this Payment type.";
                //    }
                //}
                //else
                //{
                //    grdwUDFS.DataSource = null;
                //    grdwUDFS.DataBind();
                //} 
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void PopulateCustRefGrid()
        {
            try
            {

                //List<UDFBE> objUDFBE = PaymentTypesBL.GetAllUDFSDetails<UDFBE>(Convert.ToInt16(ddlLanguage.SelectedValue));
                List<UDFCustRefBE> objUDFBE = PaymentTypesBL.GetCustRefUDFSDetails<UDFCustRefBE>(Convert.ToInt16(ddlLanguage.SelectedValue), Convert.ToInt32(ddlPaymentTypes.SelectedValue));
                if (objUDFBE != null)
                {
                    gridCustomRef.DataSource = objUDFBE;
                    gridCustomRef.DataBind();


                }
                else
                {
                    gridCustomRef.DataSource = null;
                    gridCustomRef.DataBind();
                }

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsUpdated = false;
                if (grdwUDFS.Rows.Count > 0)
                {
                    for (int i = 0; i < grdwUDFS.Rows.Count; i++)
                    {
                        CheckBox chkIsMandatory = (CheckBox)grdwUDFS.Rows[i].FindControl("chkIsMandatory");
                        CheckBox chkIsVisible = (CheckBox)grdwUDFS.Rows[i].FindControl("chkIsVisible");
                        TextBox txtDisplayName = (TextBox)grdwUDFS.Rows[i].FindControl("txtDisplayName");
                        RadioButton rdBtnPoint = (RadioButton)grdwUDFS.Rows[i].FindControl("rdBtnPoint");
                        RadioButton rdBtnBudget = (RadioButton)grdwUDFS.Rows[i].FindControl("rdBtnBudget");
                        string strDisplayName = txtDisplayName.Text;

                        //IsUpdated = PaymentTypesBL.UpdateUDFSIsMandatory(Convert.ToInt32(grdwUDFS.DataKeys[i].Values[0]), chkIsMandatory.Checked, chkIsVisible.Checked);
                        IsUpdated = PaymentTypesBL.UpdateUDFSIsMandatory(Convert.ToInt32(grdwUDFS.DataKeys[i].Values[0]), chkIsMandatory.Checked, chkIsVisible.Checked, Convert.ToInt16(ddlLanguage.SelectedValue), strDisplayName, rdBtnBudget.Checked, rdBtnPoint.Checked);/*User Type*/
                    }
                    if (IsUpdated)
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "UDFS Submitted Successfully", AlertType.Success);
                        CreateActivityLog("UDFSManagement", "Update", ddlPaymentTypes.DataTextField);
                    }
                    else
                        GlobalFunctions.ShowModalAlertMessages(Page, "Error while updating UDFS", AlertType.Failure);
                }

                if (gridCustomRef.Rows.Count > 0)
                {
                    for (int i = 0; i < gridCustomRef.Rows.Count; i++)
                    {
                        CheckBox chkIsMandatoryCust = (CheckBox)gridCustomRef.Rows[i].FindControl("chkIsMandatoryCust");
                        CheckBox chkIsVisibleCust = (CheckBox)gridCustomRef.Rows[i].FindControl("chkIsVisibleCust");
                        TextBox txtDisplayNameCust = (TextBox)gridCustomRef.Rows[i].FindControl("txtDisplayNameCust");
                        string strDisplayName = txtDisplayNameCust.Text;
                        int PaymentTID = Convert.ToInt32(ddlPaymentTypes.SelectedValue);

                        IsUpdated = PaymentTypesBL.UpdateCustomerRefUDFSIsMandatory(Convert.ToInt32(gridCustomRef.DataKeys[i].Values[0]), chkIsMandatoryCust.Checked, chkIsVisibleCust.Checked, Convert.ToInt16(ddlLanguage.SelectedValue), strDisplayName, PaymentTID);
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hdnSelectedPaymentTypeId.Value = Convert.ToString(ddlPaymentTypes.SelectedValue);
                BindPaymentTypes();
                PopulateGrid();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}