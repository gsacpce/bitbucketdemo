﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using HtmlAgilityPack;
using System.IO;
using System.Text;
using PWGlobalEcomm.BusinessLogic;
using System.Data;
using System.Text.RegularExpressions;
using Microsoft.Security.Application;


namespace Presentation
{
    public partial class Admin_MasterPageManagement_FooterConfigure : System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Button btnSaveLayout;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myCanvas;
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox chkContainer;

        public string Host = GlobalFunctions.GetVirtualPath();
        public string HostAdmin = GlobalFunctions.GetVirtualPathAdmin();

        string strPath = "";
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 18-08-15
        /// Scope   : Page_Load event
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                strPath = Server.MapPath("~/Master/UserControls");
                Session["masterFilePath"] = strPath;
                Session["host"] = Host;

                var webGet = new HtmlWeb();
                string homepageUrl = Host + "Master/userControls/Footer_Modified.ascx";

                HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Master/UserControls/Footer_Modified.ascx")));
                
                homepageUrl = "";
                
                HtmlNode divFooter = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Id == "divFooter");

                
                var newContentTypes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("itemtype"));

                /*Anoop Gupta : 28 01 2016 : Find ROW elements and add checkbox for setting row attribute as full page width or container width*/
                var rowNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class") &&
                                                                             x.Attributes["class"].Value.Contains("container")
                                                                              && x.Name.Equals("div")
                                                                            );
                int checkboxcount = 0;
                foreach (HtmlNode rowNode in rowNodes.ToList())
                {
                    
                    string strContainerWidth = string.Empty;
                    string checkboxValue = string.Empty;
                    if (rowNode.OuterHtml.Contains("containerfullwidth"))
                    {
                        strContainerWidth = "containerfullwidth";
                    }
                    else
                    {
                        strContainerWidth = "container";
                        checkboxValue = "  checked=\"checked\"";
                    }
                    if (checkboxcount == 0)
                    {
                        rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<input type=\"checkbox\" " + checkboxValue + " class=\"chkcontainer\" ;\" onclick=\"javasctipt:funContainerMode(this);\" title=\"Container width\">"));
                        rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<label>&nbsp;&nbsp;Container width</label>"));
                        rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<input type=\"hidden\" class=\"containermode\" value=\"" + strContainerWidth + "\"  >"));
                    }
                        checkboxcount++;
                }


                /*Anoop Gupta : 28 01 2016 */

                HtmlNode[] itemTypeDivElements = newContentTypes.ToArray<HtmlNode>();


                List<HeaderFooterManagementBE> FooterElements = new List<HeaderFooterManagementBE>();
                try
                {
                    FooterElements = HeaderFooterManagementBL.GetAllHeaderFooterElements(false, "footer").Cast<HeaderFooterManagementBE>().ToList();

                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                    //throw;
                }
                
                Int16 counter = 0 ;
                counter = Convert.ToInt16(itemTypeDivElements.Length);


                for (Int16 i = 0; i < counter; i++)
                {
                    string htmlContentId = "";
                    string gmContentId = "";
                    if (itemTypeDivElements[i].Attributes["id"] == null)
                    {
                        htmlContentId = (i + 1).ToString();
                        itemTypeDivElements[i].Attributes.Add("id", "gmcontent" + htmlContentId);
                        itemTypeDivElements[i].Attributes.Add("runat", "server");
                    }
                    else
                    {
                        gmContentId = itemTypeDivElements[i].Attributes["id"].Value;
                        htmlContentId = itemTypeDivElements[i].Attributes["id"].Value.Replace("gmcontent", "");
                        itemTypeDivElements[i].Attributes.Add("runat", "server");
                    }


                    StringBuilder sbDropDown = new StringBuilder();


                    sbDropDown.Append("<select width=\"170px\" class=\"ddlContent\" name=\"ddlContent" + htmlContentId + "\" id=\"ddlContent" + htmlContentId + "\" onchange=\"javascript:funSaveDDLValue('ddlContent" + htmlContentId + "','hdnContent" + htmlContentId + "');\" >");//onchange=\"javascript:funSaveDDLValue('ddlContent" + htmlContentId + "','hdnContent" + htmlContentId + "');\"

                    
                    string strItemType = string.Empty;
                    string strItemValue = string.Empty;
                    if (FooterElements.Count > 0)
                    {
                        HeaderFooterManagementBE currElement = FooterElements.FirstOrDefault(x => x.ConfigSectionName.ToLower().Equals("gmcontent" + htmlContentId));
                        if (currElement != null)
                        {
                                
                            if (currElement.ConfigElementName != "")
                            {
                                strItemType = currElement.ConfigElementName;
                                strItemValue = currElement.ConfigElementValue;

                                if (i < FooterElements.Count)
                                {
                                    itemTypeDivElements[i].Attributes.Add("data-value", Convert.ToString(currElement.ConfigElementID));
                                    itemTypeDivElements[i].Attributes.Add("itemtype", strItemType);
                                }

                                sbDropDown.Append("<option value='0'>-- Select --</option>");

                                if (currElement.ConfigElementName.ToLower() == "footerlogo")
                                    sbDropDown.Append("<option value='Footerlogo' selected=\"selected\">Footer logo</option>");
                                else
                                    sbDropDown.Append("<option value='Footerlogo'>Footer logo</option>");
                                if (currElement.ConfigElementName.ToLower() == "footerlinks")
                                    sbDropDown.Append("<option value='Footerlinks' selected=\"selected\">Footer links</option>");
                                else
                                    sbDropDown.Append("<option value='Footerlinks'>Footer links</option>");
                                if (currElement.ConfigElementName.ToLower() == "footertext")
                                    sbDropDown.Append("<option value='Footertext' selected=\"selected\">Footer text</option>");
                                else
                                    sbDropDown.Append("<option value='Footertext'>Footer text</option>");
                                if (currElement.ConfigElementName.ToLower() == "footercopyright")
                                    sbDropDown.Append("<option value='Footercopyright' selected=\"selected\">Footer copyright</option>");
                                else
                                    sbDropDown.Append("<option value='Footercopyright'>Footer copyright</option>");
                                
                            }
                        }
                        else
                        {
                            itemTypeDivElements[i].Attributes.Add("data-value", "0");
                            sbDropDown.Append("<option value='0' selected=\"selected\">-- Select --</option>");
                            sbDropDown.Append("<option value='Footerlogo'>Footer logo</option>");
                            sbDropDown.Append("<option value='Footerlinks'>Footer links</option>");
                            sbDropDown.Append("<option value='Footercopyright'>Footer copyright</option>");
                            sbDropDown.Append("<option value='Footertext'>Footer text</option>");
                        }
                        sbDropDown.Append("</select><br>");//<span style='color:#702828'>" + strItemType + "</span>
                        
                    }
                    else
                    {
                        string currItem = itemTypeDivElements[i].Attributes["itemType"].Value.ToLower();
                        strItemType = currItem;
                        
                        itemTypeDivElements[i].Attributes.Add("data-value", "0");
                        sbDropDown.Append("<option value='0' selected=\"selected\">-- Select --</option>");
                        sbDropDown.Append("<option value='Footerlogo'>Footer logo</option>");
                        sbDropDown.Append("<option value='Footerlinks'>Footer links</option>");
                        sbDropDown.Append("<option value='Footercopyright'>Footer copyright</option>");
                        sbDropDown.Append("<option value='Footertext'>Footer text</option>");
                        sbDropDown.Append("</select>");
                        
                    }
                   
                    
                    sbDropDown.Append("<input type=\"hidden\" id=\"hdnContent" + htmlContentId + "\" value=\"" + itemTypeDivElements[i].Attributes["itemtype"].Value + "\" >");
                    itemTypeDivElements[i].InnerHtml = sbDropDown.ToString();
                }
                myCanvas.InnerHtml = divFooter.InnerHtml;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }




        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : SavePageLayout method 
        /// </summary>  
        /// param name="PageLayout"
        //[WebMethod]
        //public static string SavePageFooter(string PageLayout, string jsondata)
        [WebMethod]
        public static string SavePageFooter(string PageLayout)
        {
            //string[] message = new string[1];
           //PageLayout = Sanitizer.GetSafeHtmlFragment(PageLayout);
            string message = string.Empty;

            try
            {
                message = "The pagelayout got saved successfully";

                string strPath = HttpContext.Current.Session["masterFilePath"].ToString();

                
                /*Take backup of existing page layout*/
                File.Copy(strPath + "\\Footer.ascx", strPath + "\\Backups\\Footer.ascx." + DateTime.Now.ToString("dd-MM-yy hh-mm-ss"), true);
                
                ProcessPageConfiguration(PageLayout);
                
            }
            catch (Exception ex)
            {
                message = "There is an error saving pagelayout";
                //throw;
            }




            // return new JavaScriptSerializer().Serialize(message);
            return message;
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : SavePageLayout method 
        /// </summary>  
        /// param name="pageConfiguration"
        //private static void ProcessPageConfiguration(string pageConfiguration, string jsondata)
        private static void ProcessPageConfiguration(string pageConfiguration)
        {
            try
            {
                string strPath = HttpContext.Current.Session["masterFilePath"].ToString();


                /*
                Read configuration elements from hidden fields from Home_Configuration.html  
                */


                HtmlDocument newHtmlDocument = new HtmlDocument();

                newHtmlDocument.LoadHtml(pageConfiguration);

                var hdnHtmlNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Id.Contains("hdnContent"));

                /*
                Delete existing layout  
                */
                HeaderFooterManagementBE FooterElement = new HeaderFooterManagementBE();
                HeaderFooterManagementBL.HeaderFooterConfiguration(FooterElement, DBAction.Delete, "footer");
                /*
                Traverse each hidden element and update layout details in database table 
                 */

                //JavaScriptSerializer jss = new JavaScriptSerializer();
                //List<HomePageManagementBE> lstHomePageMgmt = jss.Deserialize<List<HomePageManagementBE>>(jsondata);

                foreach (HtmlNode item in hdnHtmlNodes.ToList())
                {
                    FooterElement = new HeaderFooterManagementBE();
                    FooterElement.ConfigSectionName = item.Id.Replace("hdn", "gm");
                    FooterElement.ConfigElementName = item.Attributes["value"].Value;
                    FooterElement.ConfigElementID = "0";

                    HeaderFooterManagementBL.HeaderFooterConfiguration(FooterElement, DBAction.Insert, "footer");

                }
                //Exception exLog = new Exception("Step 0 -- Post DB Insert records");
                //Exceptions.WriteExceptionLog(exLog);

                /// <summary>
                /// Author  : Sachin Chauhan
                /// Date    : 18-08-15
                /// Scope   : Remove dropdown , hidden & gridview control nodes from final modified document
                

                //exLog = new Exception("Step 1 -- Post SELECT nodes");
                //Exceptions.WriteExceptionLog(exLog);

                foreach (var hdnNode in hdnHtmlNodes.ToList())
                {
                    
                    hdnNode.ParentNode.Attributes["itemtype"].Value = hdnNode.Attributes["value"].Value;
                    hdnNode.ParentNode.RemoveChild(hdnNode);
                }

                var selectNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("select"));

                foreach (var selectNode in selectNodes.ToList())
                {
                    selectNode.ParentNode.RemoveChild(selectNode);
                }

                /*Anoop Gupta: 28-01-2016 */
                var hdnContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                         && x.Attributes["class"].Value.Equals("containermode"));

                var divContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                                         && x.Attributes["class"].Value.Contains("container")
                                                                                         && x.Name.Equals("div")
                                                                                         );

                var newDivContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                                         && x.Attributes["class"].Value.Contains("container")
                                                                                         && x.Name.Equals("div")
                                                                                         );

                HtmlNode[] hdnContainerModes = hdnContainerNodes.ToArray();
                HtmlNode[] newDivContainerModes = newDivContainerNodes.ToArray();

                for (int i = 0; i < hdnContainerModes.Length; i++)
                {
                    if (hdnContainerModes[i].Attributes["value"].Value.Equals("container"))
                    {
                        newDivContainerModes[i].Attributes["class"].Value = "container";
                    }
                    else
                        newDivContainerModes[i].Attributes["class"].Value = "containerfullwidth";

                }

                var labelNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("label"));
                foreach (var labelNode in labelNodes.ToList())
                {
                    labelNode.ParentNode.RemoveChild(labelNode);
                }
                /*Anoop Gupta: 28-01-2016 */

                var hiddenNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("input"));

                //exLog = new Exception("Step 2 -- Post SELECT nodes");
                //Exceptions.WriteExceptionLog(exLog);

                foreach (var hiddenNode in hiddenNodes.ToList())
                {
                    hiddenNode.ParentNode.RemoveChild(hiddenNode);
                }

                newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace("gm-content", "");
                newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace("contenteditable=\"false\"", "");

                //exLog = new Exception("Step 3 -- Clean up");
                //Exceptions.WriteExceptionLog(exLog);
                ///</Summary>


                HtmlDocument oldHomeDocument = new HtmlDocument();
                oldHomeDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Master/Usercontrols/Footer.ascx")));
                HtmlNode divFooter = oldHomeDocument.DocumentNode.SelectSingleNode("//div[@id='divFooter']");

                //exLog = new Exception("Step 4 -- Post Footer original file read: " + oldHomeDocument.DocumentNode.InnerHtml);
                //Exceptions.WriteExceptionLog(exLog);

                
                //exLog = new Exception("Step 4.12 -- Post Footer original file read" + newHtmlDocument.ToString());
                //Exceptions.WriteExceptionLog(exLog);
                //exLog = new Exception("Step 4.11 -- Post Footer original file read" + divFooter.ToString());
                //Exceptions.WriteExceptionLog(exLog);
                
                

                //divFooter.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml; commented by vikram to readd the outer element

                newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace(@"""", "'");
                string sectionTag = "<section id='STANDARD_FOOTER' class='mycanvas container-fluid'>";
                string Footertag = "<footer class='footerBg footerpadding'>";
                string ContainerwidthClassName = "";
                if (newHtmlDocument.DocumentNode.InnerHtml.Contains("containerfullwidth"))
                {
                    ContainerwidthClassName = "containerfullwidth";
                }
                else
                {
                    ContainerwidthClassName = "container";
                }
               
                newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace(sectionTag, "");
                newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace(Footertag, "");
                newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace("<div class='"+ ContainerwidthClassName+"'>","");
                divFooter.InnerHtml = "<section id='STANDARD_FOOTER' class='mycanvas container-fluid'><div class='" + newDivContainerModes[0].Attributes["class"].Value + "'><footer class='footerBg footerpadding'>" + newHtmlDocument.DocumentNode.InnerHtml;
              
                //exLog = new Exception("Step 4.1 -- " + newHtmlDocument.DocumentNode.InnerHtml);
                //Exceptions.WriteExceptionLog(exLog);

                //exLog = new Exception("Step 4.2 -- " + divFooter.InnerHtml );
                //Exceptions.WriteExceptionLog(exLog);

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Master/UserControls/Footer.ascx")));

                //exLog = new Exception("Step 4.2 -- After load of footer ");
                //Exceptions.WriteExceptionLog(exLog);

                HtmlNode dvHomeContainer = doc.DocumentNode.SelectSingleNode("//div[@id='divFooter']");

                //exLog = new Exception("Step 4.3 -- dvhomeContainer ");
                //Exceptions.WriteExceptionLog(exLog);


                //exLog = new Exception("Step 5 -- Footer file read again: " + dvHomeContainer.InnerHtml + " <br> " + divFooter.InnerHtml);
                //Exceptions.WriteExceptionLog(exLog);

                doc.DocumentNode.InnerHtml = doc.DocumentNode.InnerHtml.Replace(dvHomeContainer.InnerHtml, divFooter.InnerHtml);

                //exLog = new Exception("Step 6 -- Footer inner HTML replace");
                //Exceptions.WriteExceptionLog(exLog);

                FileInfo objFS = new FileInfo(strPath + "\\Footer.ascx");
                if (objFS.IsReadOnly == true)
                {
                    objFS.IsReadOnly = false;
                }
                objFS = null;

                //exLog = new Exception("Step 6 -- Before save");
                //Exceptions.WriteExceptionLog(exLog);

                doc.Save(HttpContext.Current.Server.MapPath("~/Master/UserControls/Footer.ascx"));

                //exLog = new Exception("Step 7 -- After save");
                //Exceptions.WriteExceptionLog(exLog);
                //File.WriteAllText(strPath + "\\Home.aspx", doc.DocumentNode.InnerHtml);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);   
                //throw;
            }
            


        }
    }

}
