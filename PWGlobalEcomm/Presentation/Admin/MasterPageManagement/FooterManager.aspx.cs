﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using HtmlAgilityPack;
using System.IO;
using Microsoft.Security.Application;


namespace Presentation
{
    public partial class Admin_MasterPageManagement_FooterManager : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Button btnSaveLayout;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myContainer;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myCanvas;

        public string Host = GlobalFunctions.GetVirtualPath();


        string strPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            /*Sachin Chauhan : 28 08 2015 : Save Home Page Physical Path in Session*/
            strPath = Server.MapPath("~/Master/UserControls");
            Session["masterFilePath"] = strPath;
            /*Sachin Chauhan : 28 08 2015*/

            //var webGet = new HtmlWeb();
            //string homepageUrl = Host + "/Home/Home.aspx";
            //var htmlDocument = webGet.Load(homepageUrl);
            //homepageUrl = "";
            //HtmlNode divHome = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Id == "dvHomeContainer");

            /*Sachin Chauhan */

            HtmlDocument htmlDocument = new HtmlDocument();

            htmlDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Master/UserControls/Footer.ascx")));
            HtmlNode divFooter = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='divFooter']");

            

            try
            {
                HtmlNode imgLogoNode = divFooter.Descendants().First(x => x.Attributes.Contains("src"));

                if (imgLogoNode != null)
                    imgLogoNode.Attributes["src"].Value = imgLogoNode.Attributes["src"].Value.Replace("..images/UI", "../../images/UI");

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            /*Sachin Chauhan : 03 09 2015 : Display existing element names */
            var gmContentNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gmcontent"));
            
            foreach (HtmlNode item in gmContentNodes)
	            {
		            item.InnerHtml = "<br><span style=\"color:#702828\">" + item.Attributes["itemtype"].Value + "</span>" ;
	            }
            
            /*Sachin Chauhan : 03 09 2015*/

            /*Sachin Chauhan : 25 09 2015 : Remove attributes from GMContent DIVs which are not GridManager friendly */
            //Dictionary<string, object> lstGMContentAttributes = new Dictionary<string, object>();


            //foreach (HtmlNode divGMContent in gmContentNodes.ToList())
            //{
            //    lstGMContentAttributes.Add(divGMContent.ChildNodes[1].InnerHtml, new GMAttribute
            //    {
            //        itemtype = divGMContent.Attributes["itemtype"].Value,
            //        datavalue = divGMContent.Attributes["data-value"].Value,
            //        runat = divGMContent.Attributes["runat"].Value,
            //        id = divGMContent.Attributes["id"].Value 
            //    });

            //    divGMContent.Attributes["itemtype"].Remove();
            //    divGMContent.Attributes["data-value"].Remove();
            //    divGMContent.Attributes["runat"].Remove();
            //    divGMContent.Attributes["id"].Remove();
            //}
            //Session["lstGMContentAttributes"] = lstGMContentAttributes;
            /*Sachin Chauhan : 25 09 2015*/

            myCanvas.InnerHtml = divFooter.InnerHtml;
        }

        //[WebMethod]
        //public static string SavePageFooter(string PageLayout)
        //{
        //    //PageLayout = GlobalFunctions.RemoveSanitisedPrefixes( Sanitizer.GetSafeHtmlFragment(PageLayout));

        //    string message;

        //    try
        //    {
        //        message = "The pagelayout got saved successfully";

        //        string strPath = HttpContext.Current.Session["masterFilePath"].ToString();

        //        HtmlDocument htmlDocument = new HtmlDocument();

        //        /*Sachin Chauhan : 28 08 2015 : Start HTML Node Cleanup Process added by GridManager*/
        //        #region Html Elements Cleanup Added by Grid Manager
        //        PageLayout = PageLayout.Replace("<!--gm-editable-region-->", "");
        //        PageLayout = PageLayout.Replace("<!--/gm-editable-region-->", "");


        //        htmlDocument.LoadHtml(PageLayout);

        //        var gmContentNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
        //                                                               && x.Attributes["class"].Value.Contains("gm-content"));


        //        foreach (HtmlNode gmContentNode in gmContentNodes.ToList())
        //        {
        //            if (gmContentNode.InnerHtml.Trim().Equals(String.Empty))
        //                gmContentNode.ParentNode.RemoveChild(gmContentNode);
        //        }


        //        var gmEditableNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
        //                                                               && x.Attributes["class"].Value.Contains("gm-editable-region"));

        //        foreach (var gmEditableNode in gmEditableNodes.ToList())
        //        {
        //            gmEditableNode.ParentNode.RemoveChild(gmEditableNode, true);
        //        }


        //        var gmControls = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
        //                                                               && x.Attributes["id"].Value.Contains("gm-controls"));
        //        foreach (var gmControl in gmControls.ToList())
        //        {
        //            gmControl.ParentNode.RemoveChild(gmControl);
        //        }

        //        HtmlNode gmCanvas = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Attributes.Contains("id")
        //                                                               && x.Attributes["id"].Value.Contains("gm-canvas"));
        //        #endregion

        //        /*Sachin Chauhan : 28 08 2015*/


        //        /*Sachin Chauhan : 28 08 2015 : Add ItemType & Other Attributes */
        //        #region Set Default Attributes For Newly Added Elements
        //        /*Sachin Chauhan : 17 03 2016 : Changed below condition to find DIV elements */
        //        var colNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class") 
        //                                                               && x.Attributes["class"].Value.Contains("column"));
        //        /**/
        //        var existingGmContents = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
        //                                                               && x.Attributes["id"].Value.Contains("gmcontent"));
               
        //        HtmlNode[] maxGmContent = existingGmContents.OrderByDescending(x => x.Attributes["id"].Value).ToArray();

        //        Int16 maxGmcontentID = Convert.ToInt16(existingGmContents.ToList().Count + 1);

        //        /*Sachin Chauhan Start : 11 02 2016 : Changed logic for assigning GMcontentIds to newly added elements*/
        //        if (maxGmContent.Length > 0)
        //        {
        //            List<Int16> lstMaxGmContentId = new List<Int16>();

        //            foreach (HtmlNode node in maxGmContent)
        //            {
        //                Int16 contentId = node.Attributes["id"].Value.Replace("gmcontent", "").To_Int16();
        //                lstMaxGmContentId.Add(contentId);
        //            }
        //            maxGmcontentID = lstMaxGmContentId.Max();
        //            maxGmcontentID += 1;
        //        }

        //        foreach (HtmlNode col in colNodes)
        //        {


        //            if (col.Attributes["itemtype"] != null && col.Attributes["id"] == null)
        //            {
        //                col.InnerHtml = "";
        //                //col.Attributes.Add("itemtype", "select");
        //                col.Attributes.Add("data-value", "0");
        //                col.Attributes.Add("id", "gmcontent" + maxGmcontentID);
        //                col.Attributes.Add("runat", "server");
        //                maxGmcontentID += 1;
        //            }
        //            if (col.Attributes["itemtype"] == null)
        //            {
        //                if (!col.InnerHtml.Contains("row"))
        //                {
        //                    col.Attributes.Add("itemtype", "select");
        //                    col.Attributes.Add("data-value", "0");
        //                    col.Attributes.Add("id", "gmcontent" + maxGmcontentID);
        //                    col.Attributes.Add("runat", "server");
        //                    maxGmcontentID += 1;
        //                }

        //            }

        //        }
        //        #endregion
        //        /*Sachin Chauhan : 28 08 2015*/


        //        /*Sachin Chauhan : 28 08 2015 : Save Footer Layout With Cleaned Up HTML Markup & Original DIV Wrappers Arround It*/
        //        #region Save Cleaned Up HTML Markup for Footer_Modified.Aspx

        //        htmlDocument.KeepChildrenOrder();

        //        PageLayout = "<div id=\"divFooter\" ClientIDMode=\"Static\" runat=\"server\">";
        //        PageLayout += gmCanvas.InnerHtml +
                
        //                     " </div> ";

        //        htmlDocument.LoadHtml(PageLayout);


        //        FileInfo objFS = new FileInfo(strPath + "\\Footer_Modified.ascx");
        //        if (objFS.IsReadOnly == true)
        //        {
        //            objFS.IsReadOnly = false;
        //        }
        //        objFS = null;

        //        File.WriteAllText(strPath + "\\Footer_Modified.ascx", htmlDocument.DocumentNode.InnerHtml);
        //        #endregion
        //        /*Sachin Chauhan : 28 08 2015*/
        //    }
        //    catch (Exception ex)
        //    {
        //        message = "There is an error saving pagelayout";
        //        Exceptions.WriteExceptionLog(ex);
        //    }




        //    return message;
        //}

        [WebMethod]
        public static string SavePageFooter(string PageLayout)
        {
            //PageLayout = GlobalFunctions.RemoveSanitisedPrefixes( Sanitizer.GetSafeHtmlFragment(PageLayout));

            string message;

            try
            {
                message = "The pagelayout got saved successfully";

                string strPath = HttpContext.Current.Session["masterFilePath"].ToString();

                HtmlDocument htmlDocument = new HtmlDocument();

                /*Sachin Chauhan : 28 08 2015 : Start HTML Node Cleanup Process added by GridManager*/
                #region Html Elements Cleanup Added by Grid Manager
                PageLayout = PageLayout.Replace("<!--gm-editable-region-->", "");
                PageLayout = PageLayout.Replace("<!--/gm-editable-region-->", "");


                htmlDocument.LoadHtml(PageLayout);

                var gmContentNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                       && x.Attributes["class"].Value.Contains("gm-content"));


                foreach (HtmlNode gmContentNode in gmContentNodes.ToList())
                {
                    if (gmContentNode.InnerHtml.Trim().Equals(String.Empty))
                        gmContentNode.ParentNode.RemoveChild(gmContentNode);
                }


                var gmEditableNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                       && x.Attributes["class"].Value.Contains("gm-editable-region"));

                foreach (var gmEditableNode in gmEditableNodes.ToList())
                {
                    gmEditableNode.ParentNode.RemoveChild(gmEditableNode, true);
                }


                var gmControls = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gm-controls"));
                foreach (var gmControl in gmControls.ToList())
                {
                    gmControl.ParentNode.RemoveChild(gmControl);
                }

                HtmlNode gmCanvas = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gm-canvas"));
                #endregion

                /*Sachin Chauhan : 28 08 2015*/


                /*Sachin Chauhan : 28 08 2015 : Add ItemType & Other Attributes */
                #region Set Default Attributes For Newly Added Elements
                /*Sachin Chauhan : 17 03 2016 : Changed below condition to find DIV elements */
                var colNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                       && x.Attributes["class"].Value.Contains("column"));
                /**/
                var existingGmContents = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gmcontent"));

                HtmlNode[] maxGmContent = existingGmContents.OrderByDescending(x => x.Attributes["id"].Value).ToArray();

                Int16 maxGmcontentID = Convert.ToInt16(existingGmContents.ToList().Count + 1);

                /*Sachin Chauhan Start : 11 02 2016 : Changed logic for assigning GMcontentIds to newly added elements*/
                if (maxGmContent.Length > 0)
                {
                    List<Int16> lstMaxGmContentId = new List<Int16>();

                    foreach (HtmlNode node in maxGmContent)
                    {
                        Int16 contentId = node.Attributes["id"].Value.Replace("gmcontent", "").To_Int16();
                        lstMaxGmContentId.Add(contentId);
                    }
                    maxGmcontentID = lstMaxGmContentId.Max();
                    maxGmcontentID += 1;
                }

                foreach (HtmlNode col in colNodes)
                {


                    if (col.Attributes["itemtype"] != null && col.Attributes["id"] == null)
                    {
                        col.InnerHtml = "";
                        //col.Attributes.Add("itemtype", "select");
                        col.Attributes.Add("data-value", "0");
                        col.Attributes.Add("id", "gmcontent" + maxGmcontentID);
                        col.Attributes.Add("runat", "server");
                        maxGmcontentID += 1;
                    }
                    if (col.Attributes["itemtype"] == null)
                    {
                        if (!col.InnerHtml.Contains("row"))
                        {
                            col.Attributes.Add("itemtype", "select");
                            col.Attributes.Add("data-value", "0");
                            col.Attributes.Add("id", "gmcontent" + maxGmcontentID);
                            col.Attributes.Add("runat", "server");
                            maxGmcontentID += 1;
                        }

                    }

                }
                #endregion
                /*Sachin Chauhan : 28 08 2015*/


                /*Sachin Chauhan : 28 08 2015 : Save Footer Layout With Cleaned Up HTML Markup & Original DIV Wrappers Arround It*/
                #region Save Cleaned Up HTML Markup for Footer_Modified.Aspx

                htmlDocument.KeepChildrenOrder();

                //PageLayout = "<div id=\"divFooter\" runat=\"server\">";
                //PageLayout += gmCanvas.InnerHtml +

                //             " </div> ";
                //htmlDocument.LoadHtml(PageLayout);



                FileInfo objFS = new FileInfo(strPath + "\\Footer_Modified.ascx");
                if (objFS.IsReadOnly == true)
                {
                    objFS.IsReadOnly = false;
                }
                objFS = null;

                htmlDocument.LoadHtml(gmCanvas.InnerHtml);
                //string section = "<section id=\"STANDARD_FOOTER\" class==\"mycanvas container-fluid\">";
                htmlDocument.DocumentNode.InnerHtml = htmlDocument.DocumentNode.InnerHtml.Replace(@"""", "'");
                string sectionTag = "<section id='STANDARD_FOOTER' class='mycanvas container-fluid'>";
                string Footertag = "<footer class='footerBg footerpadding'>";
                string divFooter = "<div id='divFooter' runat='server'>";
                string ContainerwidthClassName="";
                if (htmlDocument.DocumentNode.InnerHtml.Contains("containerfullwidth"))
                {
                    ContainerwidthClassName = "containerfullwidth";
                }
                else
                {
                    ContainerwidthClassName = "container";
                }
                htmlDocument.DocumentNode.InnerHtml = htmlDocument.DocumentNode.InnerHtml.Replace(sectionTag, "");
                htmlDocument.DocumentNode.InnerHtml = htmlDocument.DocumentNode.InnerHtml.Replace(Footertag, "");
                htmlDocument.DocumentNode.InnerHtml = htmlDocument.DocumentNode.InnerHtml.Replace(divFooter, "");
                htmlDocument.DocumentNode.InnerHtml = htmlDocument.DocumentNode.InnerHtml.Replace("<div class='" + ContainerwidthClassName + "'>", "");
                htmlDocument.DocumentNode.InnerHtml = "<div id='divFooter' runat='server'><section id='STANDARD_FOOTER' class='mycanvas container-fluid'><div class='" + ContainerwidthClassName + "'><footer class='footerBg footerpadding'>" + htmlDocument.DocumentNode.InnerHtml;
                //<div class='container'>
                // htmlDocument.DocumentNode.InnerHtml = htmlDocument.DocumentNode.InnerHtml.Replace("</footer></div></section>", "");

                File.WriteAllText(strPath + "\\Footer_Modified.ascx", htmlDocument.DocumentNode.InnerHtml);
                #endregion
                /*Sachin Chauhan : 28 08 2015*/
            }
            catch (Exception ex)
            {
                message = "There is an error saving pagelayout";
                Exceptions.WriteExceptionLog(ex);
            }




            return message;
        }
    }    
}
