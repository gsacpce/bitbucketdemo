﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using HtmlAgilityPack;
using System.IO;
using System.Text;
using PWGlobalEcomm.BusinessLogic;
using System.Data;
using System.Text.RegularExpressions;
using Microsoft.Security.Application;


namespace Presentation
{
    public partial class Admin_MasterPageManagement_HeaderConfigure : System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Button btnSaveLayout;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myCanvas;
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox chkContainer;

        public string Host = GlobalFunctions.GetVirtualPath();
        public string HostAdmin = GlobalFunctions.GetVirtualPathAdmin();

        string strPath = "";
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 18-08-15
        /// Scope   : Page_Load event
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                strPath = Server.MapPath("~/Master/UserControls");
                Session["masterFilePath"] = strPath;
                Session["host"] = Host;

                var webGet = new HtmlWeb();
                string homepageUrl = Host + "Master/userControls/Header_Modified.ascx";

                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Master/UserControls/Header_Modified.ascx")));

                homepageUrl = "";

                HtmlNode divHeader = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Id == "divHeader");


                var newContentTypes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("itemtype"));

                /*Anoop Gupta : 28 01 2016 : Find ROW elements and add checkbox for setting row attribute as full page width or container width*/
                var rowNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class") &&
                                                                             x.Attributes["class"].Value.Contains("container")
                                                                            );

                foreach (HtmlNode rowNode in rowNodes.ToList())
                {
                    string strContainerWidth = string.Empty;
                    string checkboxValue = string.Empty;
                    if (rowNode.OuterHtml.Contains("containerfullwidth"))
                    {
                        strContainerWidth = "containerfullwidth";
                    }
                    else
                    {
                        strContainerWidth = "container";
                        checkboxValue = "  checked=\"checked\"";
                    }
                    rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<input type=\"checkbox\" " + checkboxValue + " class=\"chkcontainer\" onclick=\"javasctipt:funContainerMode(this);\" title=\"Container width\">"));
                    rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<label>&nbsp;&nbsp;Container width</label>"));
                    rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<input type=\"hidden\" class=\"containermode\" value=\"" + strContainerWidth + "\"  >"));
                }

                var menuRowNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class") &&
                                                                             //x.Name.Equals("nav") &&
                                                                             x.Attributes["class"].Value.Contains("menuBg")
                                                                            );

                foreach (HtmlNode rowNode in menuRowNodes.ToList())
                {
                    string strContainerWidth = string.Empty;
                    string checkboxValue = string.Empty;
                    if (rowNode.OuterHtml.Contains("HEADER_CATEGORIES"))
                    {
                        strContainerWidth = "HEADER_CATEGORIES";
                    }
                    else
                    {
                        strContainerWidth = "nav";
                        checkboxValue = "  checked=\"checked\"";
                    }
                    rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<input type=\"checkbox\" " + checkboxValue + " class=\"chkcontainer\" onclick=\"javasctipt:funInnerContainer(this);\" title=\"Inner Container width\">"));
                    rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<label>&nbsp;&nbsp;Inner Container width</label>"));
                    rowNode.ParentNode.AppendChild(HtmlNode.CreateNode("<input type=\"hidden\" class=\"innercontainermode\" value=\"nav\"  >"));
                }
                /*Anoop Gupta : 28 01 2016 */

                HtmlNode[] itemTypeDivElements = newContentTypes.ToArray<HtmlNode>();


                List<HeaderFooterManagementBE> HeaderElements = new List<HeaderFooterManagementBE>();
                try
                {
                    HeaderElements = HeaderFooterManagementBL.GetAllHeaderFooterElements(false, "header").Cast<HeaderFooterManagementBE>().ToList();

                }
                catch (Exception ex)
                {

                    //throw;
                }

                Int16 counter = 0;
                if (HeaderElements.Count > 0)
                    counter = Convert.ToInt16(HeaderElements.Count);
                else
                    counter = Convert.ToInt16(itemTypeDivElements.Length);


                for (Int16 i = 0; i < counter; i++)
                {
                    string htmlContentId = "";
                    string gmContentId = "";
                    if (itemTypeDivElements[i].Attributes["id"] == null)
                    {
                        htmlContentId = (i + 1).ToString();
                        itemTypeDivElements[i].Attributes.Add("id", "gmcontent" + htmlContentId);
                        itemTypeDivElements[i].Attributes.Add("runat", "server");
                    }
                    else
                    {
                        gmContentId = itemTypeDivElements[i].Attributes["id"].Value;
                        htmlContentId = itemTypeDivElements[i].Attributes["id"].Value.Replace("gmcontent", "");
                        itemTypeDivElements[i].Attributes.Add("runat", "server");
                    }


                    StringBuilder sbDropDown = new StringBuilder();


                    //sbDropDown.Append("<select width=\"170px\" disabled=\"disabled\" class=\"ddlContent\" name=\"ddlContent" + htmlContentId + "\" id=\"ddlContent" + htmlContentId + "\" >");

                    string strItemType = string.Empty;
                    string strItemValue = string.Empty;
                    if (HeaderElements.Count > 0)
                    {
                        HeaderFooterManagementBE currElement = HeaderElements.FirstOrDefault(x => x.ConfigSectionName.ToLower().Equals("gmcontent" + htmlContentId));
                        if (currElement != null)
                        {

                            if (currElement.ConfigElementName != "")
                            {
                                strItemType = currElement.ConfigElementName;
                                strItemValue = currElement.ConfigElementValue;

                                if (i < HeaderElements.Count)
                                {
                                    itemTypeDivElements[i].Attributes.Add("data-value", Convert.ToString(currElement.ConfigElementID));
                                    itemTypeDivElements[i].Attributes.Add("itemtype", strItemType);
                                }

                                /*sbDropDown.Append("<option value='0'>-- Select --</option>");

                                if (currElement.ConfigElementName.ToLower() == "sitelogo")
                                    sbDropDown.Append("<option value='SiteLogo' selected=\"selected\">Site Logo</option>");
                                else
                                    sbDropDown.Append("<option value='SiteLogo'>Site Logo</option>");
                                
                                if (currElement.ConfigElementName.ToLower() == "sitelinks")
                                    sbDropDown.Append("<option value='SiteLinks' selected=\"selected\">Site Links</option>");
                                else
                                    sbDropDown.Append("<option value='SiteLinks'>Site Links</option>");
                                
                                if (currElement.ConfigElementName.ToLower() == "categoriesmenu")
                                    sbDropDown.Append("<option value='CategoriesMenu' selected=\"selected\">Categories Menu</option>");
                                else
                                    sbDropDown.Append("<option value='CategoriesMenu'>Categoreis Menu</option>");
                                
                                if (currElement.ConfigElementName.ToLower() == "sitelanguages")
                                    sbDropDown.Append("<option value='Sitelanguages' selected=\"selected\">Site Languages</option>");
                                else
                                    sbDropDown.Append("<option value='Sitelanguages'>Site Languages</option>");

                                if (currElement.ConfigElementName.ToLower() == "socialmedia")
                                    sbDropDown.Append("<option value='SocialMedia' selected=\"selected\">Social Media</option>");
                                else
                                    sbDropDown.Append("<option value='SocialMedia'>Social Media</option>");*/
                            }
                        }
                        else
                        {
                            //itemTypeDivElements[i].Attributes.Add("data-value", "0");
                            ////tElements[i].Attributes.Add("itemtype", strItemType);
                            //sbDropDown.Append("<option value='0' selected=\"selected\">-- Select --</option>");
                            //sbDropDown.Append("<option value='SiteLogo'>Site Logo</option>");
                            //sbDropDown.Append("<option value='SiteLinks'>Site Links</option>");
                            //sbDropDown.Append("<option value='CategoriesMenu'>Categories Menu</option>");
                            //sbDropDown.Append("<option value='Sitelanguages'>Site Languages</option>");
                            //sbDropDown.Append("<option value='SocialMedia'>SocialMedia</option>");
                        }


                    }
                    else
                    {
                        string currItem = itemTypeDivElements[i].Attributes["itemType"].Value.ToLower();
                        strItemType = currItem;
                        /*if (currItem.Equals("sitelogo"))
                            sbDropDown.Append("<option value='SiteLogo' selected=\"selected\">Site Logo</option>");
                        else
                            sbDropDown.Append("<option value='SiteLogo'>Site Logo</option>");

                        if (currItem.Equals("sitelinks"))
                            sbDropDown.Append("<option value='sitelinks' selected=\"selected\">Site Links</option>");
                        else
                            sbDropDown.Append("<option value='sitelinks'>Site Links</option>");

                        if (currItem.Equals("categoriesmenu"))
                            sbDropDown.Append("<option value='CategoriesMenu' selected=\"selected\">Categories Menu</option>");
                        else
                            sbDropDown.Append("<option value='CategoriesMenu'>Categoreis Menu</option>");

                        if (currItem.Equals("sitelanguages"))
                            sbDropDown.Append("<option value='Sitelanguages' selected=\"selected\">Site Languages</option>");
                        else
                            sbDropDown.Append("<option value='Sitelanguages'>Site Languages</option>");

                        if (currItem.Equals("socialmedia"))
                            sbDropDown.Append("<option value='SocialMedia' selected=\"selected\">Social Media</option>");
                        else
                            sbDropDown.Append("<option value='SocialMedia'>Social Media</option>");*/

                    }
                    //sbDropDown.Append("</select>");
                    //if (strItemType.ToLower().Equals("sitelinks") )
                    //    sbDropDown.Append("&nbsp;&nbsp;<input type=\"Button\" Value=\"Configure Site Links\" > ");

                    sbDropDown.Append("<br><span style='color:#702828'>" + strItemType + "</span>");
                    sbDropDown.Append("<input type=\"hidden\" id=\"hdnContent" + htmlContentId + "\" value=\"" + itemTypeDivElements[i].Attributes["itemtype"].Value + "\" >");
                    itemTypeDivElements[i].InnerHtml = sbDropDown.ToString();
                }
                myCanvas.InnerHtml = divHeader.InnerHtml;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }




        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : SavePageLayout method 
        /// </summary>  
        /// param name="PageLayout"
        [WebMethod]
        //public static string SavePageHeader(string PageLayout, string jsondata)
        public static string SavePageHeader(string PageLayout)
        {
           // PageLayout = Sanitizer.GetSafeHtmlFragment(PageLayout);
            //string[] message = new string[1];
            string message = string.Empty;

            try
            {
                message = "The pagelayout got saved successfully";

                string strPath = HttpContext.Current.Session["masterFilePath"].ToString();

                //File.CreateText(strPath+"\\Home_Modified.aspx") ;
                //FileStream fs = File.OpenWrite(strPath + "\\Home_Modified.aspx");

                /*Take backup of existing page layout*/
                File.Copy(strPath + "\\Header.ascx", strPath + "\\Backups\\Header.ascx." + DateTime.Now.ToString("dd-MM-yy hh-mm-ss"), true);
                /*Sachin Chauhan 17 08 2015 
                 * Commented below line as 2 & 3rd step layout processing is merged
                 * 
                */
                /*Sachin Chauhan*/
                ProcessPageConfiguration(PageLayout);
                /**/

                //File.WriteAllText(strPath + "\\Home_Configuration.html", PageLayout);
            }
            catch (Exception ex)
            {
                message = "There is an error saving pagelayout";
                //throw;
            }




            // return new JavaScriptSerializer().Serialize(message);
            return message;
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : SavePageLayout method 
        /// </summary>  
        /// param name="pageConfiguration"
        //private static void ProcessPageConfiguration(string pageConfiguration, string jsondata)
        private static void ProcessPageConfiguration(string pageConfiguration)
        {
            try
            {
                string strPath = HttpContext.Current.Session["masterFilePath"].ToString();


                /*
                Read configuration elements from hidden fields from Home_Configuration.html  
                */


                HtmlDocument newHtmlDocument = new HtmlDocument();

                newHtmlDocument.LoadHtml(pageConfiguration);

                var hdnHtmlNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Id.Contains("hdnContent"));

                /*
                Delete existing layout  
                */
                HeaderFooterManagementBE headerElement = new HeaderFooterManagementBE();
                HeaderFooterManagementBL.HeaderFooterConfiguration(headerElement, DBAction.Delete, "header");
                /*
                Traverse each hidden element and update layout details in database table 
                 */

                //JavaScriptSerializer jss = new JavaScriptSerializer();
                //List<HomePageManagementBE> lstHomePageMgmt = jss.Deserialize<List<HomePageManagementBE>>(jsondata);

                foreach (HtmlNode item in hdnHtmlNodes.ToList())
                {
                    headerElement = new HeaderFooterManagementBE();
                    headerElement.ConfigSectionName = item.Id.Replace("hdn", "gm");
                    headerElement.ConfigElementName = item.Attributes["value"].Value;
                    headerElement.ConfigElementID = "0";

                    HeaderFooterManagementBL.HeaderFooterConfiguration(headerElement, DBAction.Insert, "header");

                }

                /// <summary>
                /// Author  : Sachin Chauhan
                /// Date    : 18-08-15
                /// Scope   : Remove dropdown , hidden & gridview control nodes from final modified document
                var selectNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("select"));

                foreach (var selectNode in selectNodes.ToList())
                {
                    //divHavingHTML.re
                    //selectNode.ParentNode.ReplaceChild(HtmlNode.CreateNode("<!--Content-->"), selectNode);
                    //selectNode.ParentNode.ReplaceChild(HtmlNode.CreateNode(" "), selectNode);
                    selectNode.ParentNode.RemoveChild(selectNode);
                }

                /*Anoop Gupta: 28-01-2016 */
                var hdnContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                         && x.Attributes["class"].Value.Equals("containermode"));

                var hdnInnerContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                         && x.Attributes["class"].Value.Equals("innercontainermode"));

                var divContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                                         && x.Attributes["class"].Value.Contains("container")
                                                                                         && x.Name.Equals("div")
                                                                                         );

                var newDivContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                                         && x.Attributes["class"].Value.Contains("container")
                                                                                         && x.Name.Equals("div")
                                                                                         );

                var newNavContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Equals("nav"));
                var newHeaderCategoriesContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Id.Equals("HEADER_CATEGORIES"));


                HtmlNode[] hdnContainerModes = hdnContainerNodes.ToArray();
                HtmlNode[] hdnInnerContainerModes = hdnInnerContainerNodes.ToArray();
                HtmlNode[] newDivContainerModes = newDivContainerNodes.ToArray();

                HtmlNode[] newNavContainerModes = newNavContainerNodes.ToArray();
                HtmlNode[] newHeaderCategoriesContainerModes = newHeaderCategoriesContainerNodes.ToArray();

                for (int i = 0; i < hdnContainerModes.Length; i++)
                {
                    if (hdnContainerModes[i].Attributes["value"].Value.Equals("container"))
                    {
                        newDivContainerModes[i].Attributes["class"].Value = "container";
                    }
                    else
                        newDivContainerModes[i].Attributes["class"].Value = "containerfullwidth";
                }

                for (int i = 0; i < hdnInnerContainerModes.Length; i++)
                {
                    newNavContainerModes[i].Attributes.Remove("class");
                    if (hdnInnerContainerModes[i].Attributes["value"].Value.Equals("nav"))
                    {
                        newNavContainerModes[i].Attributes.Add("class", "navbar navbar-inverse menuBg");
                        newHeaderCategoriesContainerModes[i].Attributes.Remove("class");
                    }
                    else
                    {
                        newHeaderCategoriesContainerModes[i].Attributes.Add("class", "menuBg");
                        newNavContainerModes[i].Attributes.Add("class", "navbar navbar-inverse");
                    }
                }

                var labelNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("label"));
                foreach (var labelNode in labelNodes.ToList())
                {
                    labelNode.ParentNode.RemoveChild(labelNode);
                }
                /*Anoop Gupta: 28-01-2016 */

                var hiddenNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("input"));

                foreach (var hiddenNode in hiddenNodes.ToList())
                {
                    //divHavingHTML.re
                    //hiddenNode.ParentNode.ReplaceChild(HtmlNode.CreateNode("<!--Content-->"), hiddenNode);
                    //hiddenNode.ParentNode.ReplaceChild(HtmlNode.CreateNode(" "), hiddenNode);
                    hiddenNode.ParentNode.RemoveChild(hiddenNode);
                }

                newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace("gm-content", "");
                newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace("contenteditable=\"false\"", "");

                ///</Summary>


                HtmlDocument oldHomeDocument = new HtmlDocument();
                oldHomeDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Master/Usercontrols/header.ascx")));
                HtmlNode divHeader = oldHomeDocument.DocumentNode.SelectSingleNode("//div[@id='divHeader']");




                divHeader.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml;


                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Master/UserControls/header.ascx")));
                HtmlNode dvHomeContainer = doc.DocumentNode.SelectSingleNode("//div[@id='divHeader']");



                doc.DocumentNode.InnerHtml = doc.DocumentNode.InnerHtml.Replace(dvHomeContainer.InnerHtml, divHeader.InnerHtml);


                FileInfo objFS = new FileInfo(strPath + "\\Header.ascx");
                if (objFS.IsReadOnly == true)
                {
                    objFS.IsReadOnly = false;
                }
                objFS = null;

                doc.Save(HttpContext.Current.Server.MapPath("~/Master/UserControls/header.ascx"));


                //File.WriteAllText(strPath + "\\Home.aspx", doc.DocumentNode.InnerHtml);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }



        }
    }

}
