﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using HtmlAgilityPack;
using System.IO;
using Microsoft.Security.Application;


namespace Presentation
{
    public partial class Admin_MasterPageManagement_HeaderManager : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Button btnSaveLayout;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myContainer;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myCanvas;

        public string Host = GlobalFunctions.GetVirtualPath();


        string strPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            /*Sachin Chauhan : 28 08 2015 : Save Home Page Physical Path in Session*/
            strPath = Server.MapPath("~/Master/UserControls");
            Session["masterFilePath"] = strPath;
            /*Sachin Chauhan : 28 08 2015*/

            //var webGet = new HtmlWeb();
            //string homepageUrl = Host + "/Home/Home.aspx";
            //var htmlDocument = webGet.Load(homepageUrl);
            //homepageUrl = "";
            //HtmlNode divHome = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Id == "dvHomeContainer");

            /*Sachin Chauhan */

            HtmlDocument htmlDocument = new HtmlDocument();

            htmlDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Master/UserControls/Header.ascx")));
            HtmlNode divHeader = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='divHeader']");

            

            try
            {
                HtmlNode imgLogoNode = divHeader.Descendants().First(x => x.Attributes.Contains("src"));

                if (imgLogoNode != null)
                    imgLogoNode.Attributes["src"].Value = imgLogoNode.Attributes["src"].Value.Replace("..images/UI", "../../images/UI");

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            /*Sachin Chauhan : 03 09 2015 : Display existing element names */
            var gmContentNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gmcontent"));

            var itemTypeNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("itemtype") );
                                                                       //&& x.Attributes["id"].Value.Contains("gmcontent"));

            foreach (HtmlNode item in itemTypeNodes)
	            {
		            item.InnerHtml = "<br><span style=\"color:#702828\">" + item.Attributes["itemtype"].Value + "</span>" ;
	            }

            /*Sachin Chauhan : 25 09 2015 : Remove attributes from GMContent DIVs which are not GridManager friendly */
            
            Dictionary<string, object> lstGMContentAttributes = new Dictionary<string, object>();


            foreach (HtmlNode divGMContent in gmContentNodes.ToList())
            {
                lstGMContentAttributes.Add(divGMContent.Attributes["id"].Value, new GMAttribute
                {
                    itemtype = divGMContent.Attributes["itemtype"].Value,
                    datavalue = divGMContent.Attributes["data-value"].Value,
                    runat = divGMContent.Attributes["runat"].Value
                });

                divGMContent.Attributes["itemtype"].Remove();
                divGMContent.Attributes["data-value"].Remove();
                divGMContent.Attributes["runat"].Remove();
            }
            Session["lstGMContentAttributes"] = lstGMContentAttributes;
            /*Sachin Chauhan : 25 09 2015*/
            /*Sachin Chauhan : 03 09 2015*/

            myCanvas.InnerHtml = divHeader.InnerHtml;
        }

        [WebMethod]
        public static string SavePageHeader(string PageLayout)
        {
           // PageLayout = Sanitizer.GetSafeHtmlFragment(PageLayout);

            string message;

            try
            {
                message = "The pagelayout got saved successfully";

                string strPath = HttpContext.Current.Session["masterFilePath"].ToString();

                HtmlDocument htmlDocument = new HtmlDocument();

                /*Sachin Chauhan : 28 08 2015 : Start HTML Node Cleanup Process added by GridManager*/
                #region Html Elements Cleanup Added by Grid Manager
                PageLayout = PageLayout.Replace("<!--gm-editable-region-->", "");
                PageLayout = PageLayout.Replace("<!--/gm-editable-region-->", "");


                htmlDocument.LoadHtml(PageLayout);

                var gmContentNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                       && x.Attributes["class"].Value.Contains("gm-content"));


                foreach (HtmlNode gmContentNode in gmContentNodes.ToList())
                {
                    if (gmContentNode.InnerHtml.Trim().Equals(String.Empty))
                        gmContentNode.ParentNode.RemoveChild(gmContentNode);
                }


                var gmEditableNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                       && x.Attributes["class"].Value.Contains("gm-editable-region"));

                foreach (var gmEditableNode in gmEditableNodes.ToList())
                {
                    gmEditableNode.ParentNode.RemoveChild(gmEditableNode, true);
                }


                var gmControls = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gm-controls"));
                foreach (var gmControl in gmControls.ToList())
                {
                    gmControl.ParentNode.RemoveChild(gmControl);
                }

                HtmlNode gmCanvas = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gm-canvas"));
                #endregion

                /*Sachin Chauhan : 28 08 2015*/


                /*Sachin Chauhan : 28 08 2015 : Add ItemType & Other Attributes & Default Value SELECT For Newly Added Home Page Elements */
                #region Set Default Attributes For Newly Added Elements
                var colNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("itemtype"));
                                                                       //&& x.Attributes["class"].Value.Contains("col-"));

                var existingGmContents = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gmcontent"));

                Int16 maxGmcontentID = Convert.ToInt16(existingGmContents.ToList().Count + 1);

                /*Sachin Chauhan : 25 09 2015 : Re-Add Attributes to GMContent DIVs which were removed because of GridManager */
                Dictionary<string, object> lstGMContentAttributes = HttpContext.Current.Session["lstGMContentAttributes"] as Dictionary<string, object>;
                var gmContentDivs = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gmcontent"));

                foreach (HtmlNode divGMContent in gmContentDivs.ToList())
                {
                    GMAttribute attribute = lstGMContentAttributes[divGMContent.Attributes["id"].Value] as GMAttribute;
                    if (attribute != null)
                    {
                        divGMContent.Attributes.Add("itemtype", attribute.itemtype);
                        divGMContent.Attributes.Add("data-value", attribute.datavalue);
                        divGMContent.Attributes.Add("runat", attribute.runat);
                    }

                }
                /*Sachin Chauhan : 25 09 2015 */

                foreach (HtmlNode col in colNodes)
                {


                    if (col.Attributes["itemtype"] != null && col.Attributes["id"] == null)
                    {
                        col.InnerHtml = "";
                        //col.Attributes.Add("itemtype", "select");
                        col.Attributes.Add("data-value", "0");
                        col.Attributes.Add("id", "gmcontent" + maxGmcontentID);
                        col.Attributes.Add("runat", "server");
                        maxGmcontentID += 1;
                    }
                    if (col.Attributes["itemtype"] == null)
                    {
                        if (!col.InnerHtml.Contains("row"))
                        {
                            col.Attributes.Add("itemtype", "select");
                            col.Attributes.Add("data-value", "0");
                            col.Attributes.Add("id", "gmcontent" + maxGmcontentID);
                            col.Attributes.Add("runat", "server");
                            maxGmcontentID += 1;
                        }

                    }

                }
                #endregion
                /*Sachin Chauhan : 28 08 2015*/


                /*Sachin Chauhan : 28 08 2015 : Save Header Layout With Cleaned Up HTML Markup & Original DIV Wrappers Arround It*/
                #region Save Cleaned Up HTML Markup for Header_Modified.Aspx

                PageLayout = "<div id=\"divHeader\" ClientIDMode=\"Static\" runat=\"server\">";
                PageLayout += gmCanvas.InnerHtml +
                
                             " </div> ";

                htmlDocument.LoadHtml(PageLayout);


                FileInfo objFS = new FileInfo(strPath + "\\Header_Modified.ascx");
                if (objFS.IsReadOnly == true)
                {
                    objFS.IsReadOnly = false;
                }
                objFS = null;

                File.WriteAllText(strPath + "\\Header_Modified.ascx", htmlDocument.DocumentNode.InnerHtml);
                #endregion
                /*Sachin Chauhan : 28 08 2015*/
            }
            catch (Exception ex)
            {
                message = "There is an error saving pagelayout";
                Exceptions.WriteExceptionLog(ex);
            }




            return message;
        }
    }    
}
