﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Settings_ManageMetaContent : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.TextBox txtPageTitle, txtMetaKeywords, txtMetaDescription;
        protected global::System.Web.UI.WebControls.DropDownList ddlPageName;
        protected global::System.Web.UI.WebControls.HiddenField hdnMetaContentId;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateLanguageDropDownList();
                ReadData();
            }
        }
        protected void ReadData()
        {
            try
            {
                MetaTags.PageName = Convert.ToString(ddlPageName.SelectedItem.Text);
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                MetaTags.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);

                MetaTags = StoreBL.GetMetaTagContents(MetaTags);


                if (MetaTags != null)
                {
                    txtPageTitle.Text = Convert.ToString(MetaTags.MetaContentTitle);
                    hdnMetaContentId.Value = Convert.ToString(MetaTags.MetaContentId);
                    txtMetaDescription.Text = Convert.ToString(MetaTags.MetaDescription);
                    txtMetaKeywords.Text = Convert.ToString(MetaTags.MetaKeyword);
                }
                else
                {
                    hdnMetaContentId.Value = "";
                    txtMetaDescription.Text = "";
                    txtMetaKeywords.Text = "";
                    txtPageTitle.Text = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : it populates language dropdown
        /// </summary>
        private void PopulateLanguageDropDownList()
        {
            List<LanguageBE> languages = new List<LanguageBE>();
            languages = LanguageBL.GetAllLanguageDetails(true);

            if (languages != null)
            {
                if (languages.Count() > 0)
                {
                    ddlLanguage.DataSource = languages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                }
            }

        }

        protected void ddlPageName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReadData();
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ReadData();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                UserBE lst = Session["StoreUser"] as UserBE;
                MetaTags.MetaKeyword = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtMetaKeywords.Text.Trim()));
                MetaTags.MetaDescription = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtMetaDescription.Text.Trim()));
                MetaTags.MetaContentTitle = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtPageTitle.Text.Trim()));
                MetaTags.PageName = Convert.ToString(Sanitizer.GetSafeHtmlFragment(ddlPageName.SelectedItem.Text.Trim()));
                MetaTags.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                // Content Title and PageName will be same
                MetaTags.ModifiedBy = lst.UserId;

                if (Convert.ToString(hdnMetaContentId.Value) == "")
                {
                    MetaTags.Action = Convert.ToInt16(DBAction.Insert);
                    bool res = StoreBL.InsertUpdateMetaTagContents(MetaTags);
                    if (res)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Meta Tags are Added Successfully", "ManageMetaContent.aspx", AlertType.Success);
                        CreateActivityLog("ManageMetaContent btnSave", "Insert", ddlPageName.SelectedValue);
                    }
                }
                else
                {
                    MetaTags.MetaContentId = Convert.ToInt16(hdnMetaContentId.Value);
                    MetaTags.Action = Convert.ToInt16(DBAction.Update);
                    bool res = StoreBL.InsertUpdateMetaTagContents(MetaTags);
                    if (res)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Meta Tags are Updated Successfully", AlertType.Success);
                        CreateActivityLog("ManageMetaContent btnSave", "Update", ddlPageName.SelectedValue);
                    }

                }
                HttpRuntime.Cache["MetaTags"] = null;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}