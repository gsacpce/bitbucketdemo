﻿using OfficeOpenXml;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Settings_CustomFieldsSettings : BasePage//System.Web.UI.Page
    {
        #region Controls
        protected global::System.Web.UI.WebControls.DropDownList ddlUserHierarchies, ddlLanguage, ddlUserTypeNames;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvAddCustomField, dvUserTypesDetails, ModalFileUpload;
        protected global::System.Web.UI.WebControls.GridView gvCustomFields;
        protected global::System.Web.UI.WebControls.FileUpload flupldCustomFieldsExcel, flCustomFieldValues;
        protected global::System.Web.UI.WebControls.HiddenField hdnRegFieldConfigID;
        #endregion
        #region Variables
        Int16 LanguageId;
        string originalFileName, ExportFileName;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (!IsPostBack)
                {
                    BindLanguage(objStoreBE);
                    // Commented by SHRIGANESH on 22 June 2016 /*User Type*/
                    //GetUserHierarchies();
                    GetUserTypes();
                }
                Page.Title = "Manage Custom Fields";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region Code Commented by SHRIGANESH SINGH 27 June 2016 as Hierarchy is removed from Custom Mapping
        /*
        private void GetUserHierarchies()
        {
            List<UserBE.UserHierarchyBE> GetUserHierarchies = UserBL.GetUserHierarchies();
            ddlUserHierarchies.DataTextField = "HierarchyName";
            ddlUserHierarchies.DataValueField = "UserHierarchyId";
            if (GetUserHierarchies == null)
            {
                ddlUserHierarchies.Items.Add(new ListItem() { Text = "No Hierarchy", Value = "4" });
            }
            else
            {
                ddlUserHierarchies.DataSource = GetUserHierarchies;
                ddlUserHierarchies.DataBind();
            }
            ddlUserHierarchies.Items.Insert(0, new ListItem() { Text = "Select Level", Value = "0" });
        }
        */
        #endregion

        /// <summary>		
        /// Author  : SHRIGANESH SINGH		
        /// Date    : 22 June 2016		
        /// Scope   : Method to Get all Usertypes		
        /// </summary>		
        private void GetUserTypes()
        {
            try
            {
                List<UserTypesDetailsBE> lstUTDBE = new List<UserTypesDetailsBE>();
                lstUTDBE = UserTypesBL.GetAlluserTypeNames(Constants.USP_GetAllUserTypes);
                ddlUserTypeNames.DataTextField = "UserTypeName";
                ddlUserTypeNames.DataValueField = "UserTypeID";
                if (lstUTDBE != null)
                {
                    ddlUserTypeNames.DataSource = lstUTDBE;
                    ddlUserTypeNames.DataBind();
                }
                ddlUserTypeNames.Items.Insert(0, new ListItem() { Text = "Please Select", Value = "0" });
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region Code Commented by SHRIGANESH SINGH 27 June 2016 as User Hierarchy is removed from Custom Mapping
        /*
        protected void ddlUserHierarchies_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlUserHierarchies.SelectedIndex > 0)
            {
                dvUserTypesDetails.Style["display"] = "";
                dvAddCustomField.Style["display"] = "";
                GetCustomFieldsData(Convert.ToByte(ddlUserHierarchies.SelectedValue));
            }
            else
            {
                dvUserTypesDetails.Style["display"] = "none";
                dvAddCustomField.Style["display"] = "none";
            }
        }
         */
        #endregion

        /// <summary>		
        /// Author  :   SHRIGANESH SINGH		
        /// Date    :   22 June 2016		
        /// Scope   :   To get all Custom Fields for Selected User Type ID		
        /// </summary>		
        /// <param name="sender"></param>		
        /// <param name="e"></param>		
        protected void ddlUserTypeNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlUserTypeNames.SelectedValue != "-1")
                {
                    dvUserTypesDetails.Style["display"] = "";
                    dvAddCustomField.Style["display"] = "";
                    GetCustomFieldsDataByUserTypeID(ddlUserTypeNames.SelectedValue);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select a User Type !! ", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region "gvCustomFields Events bk"
        #region "gvCustomFields_RowDataBound_bk"
        /* protected void gvCustomFields_RowDataBound_bk(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {
                RegistrationCustomFieldBE objRegistrationCustomFieldBE = (RegistrationCustomFieldBE)e.Row.DataItem;
                DropDownList ddlFieldTypesEdit = (DropDownList)e.Row.FindControl("ddlFieldTypesEdit");
                CheckBox chkIsVisibleEdit = (CheckBox)e.Row.FindControl("chkIsVisibleEdit");
                CheckBox chkIsMandatoryEdit = (CheckBox)e.Row.FindControl("chkIsMandatoryEdit");
                TextBox txtCustomFieldDataEdit = (TextBox)e.Row.FindControl("txtCustomFieldDataEdit");
                RequiredFieldValidator rfvCustomFieldDataEdit = (RequiredFieldValidator)e.Row.FindControl("rfvCustomFieldDataEdit");
                ddlFieldTypesEdit.DataSource = RegistrationCustomFieldBL.GetFieldTypeMasterDetails();
                ddlFieldTypesEdit.DataTextField = "FieldTypeName";
                ddlFieldTypesEdit.DataValueField = "FieldTypeId";
                ddlFieldTypesEdit.DataBind();
                ddlFieldTypesEdit.SelectedValue = Convert.ToString(objRegistrationCustomFieldBE.FieldType);
                chkIsVisibleEdit.Checked = objRegistrationCustomFieldBE.IsVisible;
                chkIsMandatoryEdit.Checked = objRegistrationCustomFieldBE.IsMandatory;

                if (ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "checkbox")
                {
                    txtCustomFieldDataEdit.Visible = true;
                    rfvCustomFieldDataEdit.Enabled = true;
                    string CustomData = "";
                    int count = 0;
                    foreach (RegistrationCustomFieldBE.RegistrationFeildDataMasterBE item in objRegistrationCustomFieldBE.CustomFieldData)
                    {
                        if (count == 0)
                            CustomData += item.RegistrationFieldDataValue;
                        else
                            CustomData += "|" + item.RegistrationFieldDataValue;
                        count++;
                    }
                    txtCustomFieldDataEdit.Text = CustomData;
                }
                else
                    rfvCustomFieldDataEdit.Enabled = false;
            }

            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                RegistrationCustomFieldBE objRegistrationCustomFieldBE = (RegistrationCustomFieldBE)e.Row.DataItem;
                Label lblCustomFieldData = (Label)e.Row.FindControl("lblCustomFieldData");
                if (objRegistrationCustomFieldBE.FieldTypeName.ToLower().Trim() == "dropdown" || objRegistrationCustomFieldBE.FieldTypeName.ToLower().Trim() == "checkbox")
                {
                    string CustomData = "";
                    int count = 0;
                    foreach (RegistrationCustomFieldBE.RegistrationFeildDataMasterBE item in objRegistrationCustomFieldBE.CustomFieldData)
                    {
                        if (count == 0)
                            CustomData += item.RegistrationFieldDataValue;
                        else
                            CustomData += "|" + item.RegistrationFieldDataValue;
                        count++;
                    }
                    lblCustomFieldData.Text = CustomData;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlFieldTypes = (DropDownList)e.Row.FindControl("ddlFieldTypes");
                ddlFieldTypes.DataSource = RegistrationCustomFieldBL.GetFieldTypeMasterDetails();
                ddlFieldTypes.DataTextField = "FieldTypeName";
                ddlFieldTypes.DataValueField = "FieldTypeId";
                ddlFieldTypes.DataBind();

            }
        }
        */
        #endregion
        #region "gvCustomFields_RowEditing_bk"
        /*protected void gvCustomFields_RowEditing_bk(object sender, GridViewEditEventArgs e)
        {
            gvCustomFields.EditIndex = e.NewEditIndex;
            byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
            GetCustomFieldsData(HierarchyLevel);
        }*/
        #endregion
        #region "gvCustomFields_RowUpdating_bk"
        /*protected void gvCustomFields_RowUpdating_bk(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Int16 RegistrationFieldsConfigurationId = Convert.ToInt16(gvCustomFields.DataKeys[e.RowIndex].Values["RegistrationFieldsConfigurationId"]);
                UserBE user = (UserBE)Session["StoreUser"];
                TextBox txtCustomFieldEdit = (TextBox)gvCustomFields.Rows[e.RowIndex].FindControl("txtCustomFieldEdit");
                DropDownList ddlFieldTypesEdit = (DropDownList)gvCustomFields.Rows[e.RowIndex].FindControl("ddlFieldTypesEdit");
                CheckBox chkIsVisibleEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsVisibleEdit");
                CheckBox chkIsMandatoryEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsMandatoryEdit");

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LabelTitle", Convert.ToString(txtCustomFieldEdit.Text.Trim()));
                dictionaryInstance.Add("FieldType", Convert.ToString(ddlFieldTypesEdit.SelectedValue));
                dictionaryInstance.Add("IsVisible", Convert.ToString(chkIsVisibleEdit.Checked));
                dictionaryInstance.Add("IsMandatory", Convert.ToString(chkIsMandatoryEdit.Checked));
                dictionaryInstance.Add("UserHierarchyLevel", Convert.ToString(ddlUserHierarchies.SelectedValue));
                if (ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "checkbox")
                {
                    TextBox txtCustomFieldDataEdit = (TextBox)gvCustomFields.Rows[e.RowIndex].FindControl("txtCustomFieldDataEdit");
                    dictionaryInstance.Add("CustomFieldData", Convert.ToString(txtCustomFieldDataEdit.Text.Trim()));
                }
                dictionaryInstance.Add("UserId", Convert.ToString(user.UserId));
                dictionaryInstance.Add("RegistrationFieldsConfigurationId", Convert.ToString(RegistrationFieldsConfigurationId));
                dictionaryInstance.Add("LanguageIdToUpdate", Convert.ToString(ddlLanguage.SelectedValue));
                dictionaryInstance.Add("Flag", Convert.ToString('E'));

                bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);

                if (Result)
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field updated successfully", AlertType.Success);
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating. Please try again", AlertType.Failure);

                gvCustomFields.EditIndex = -1;
                byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
                GetCustomFieldsData(HierarchyLevel);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }*/
        #endregion
        #region "gvCustomFields_RowDeleting_bk"
        /*protected void gvCustomFields_RowDeleting_bk(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Int16 RegistrationFieldsConfigurationId = Convert.ToInt16(gvCustomFields.DataKeys[e.RowIndex].Values["RegistrationFieldsConfigurationId"]);

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RegistrationFieldsConfigurationId", Convert.ToString(RegistrationFieldsConfigurationId));
                dictionaryInstance.Add("Flag", Convert.ToString('D'));

                bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);

                if (Result)
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field deleted successfully", AlertType.Success);
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while deleting. Please try again", AlertType.Failure);

                byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
                GetCustomFieldsData(HierarchyLevel);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }*/
        #endregion
        #region "gvCustomFields_RowCommand_bk"
        /*protected void gvCustomFields_RowCommand_bk(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                try
                {
                    UserBE user = (UserBE)Session["StoreUser"];
                    TextBox txtCustomField = (TextBox)gvCustomFields.FooterRow.FindControl("txtCustomField");
                    DropDownList ddlFieldTypes = (DropDownList)gvCustomFields.FooterRow.FindControl("ddlFieldTypes");
                    CheckBox chkIsVisible = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsVisible");
                    CheckBox chkIsMandatory = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsMandatory");

                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("LabelTitle", Convert.ToString(txtCustomField.Text.Trim()));
                    dictionaryInstance.Add("FieldType", Convert.ToString(ddlFieldTypes.SelectedValue));
                    dictionaryInstance.Add("IsVisible", Convert.ToString(chkIsVisible.Checked));
                    dictionaryInstance.Add("IsMandatory", Convert.ToString(chkIsMandatory.Checked));
                    dictionaryInstance.Add("UserHierarchyLevel", Convert.ToString(ddlUserHierarchies.SelectedValue));
                    if (ddlFieldTypes.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypes.SelectedItem.Text.ToLower().Trim() == "checkbox")
                    {
                        TextBox txtCustomFieldData = (TextBox)gvCustomFields.FooterRow.FindControl("txtCustomFieldData");
                        dictionaryInstance.Add("CustomFieldData", Convert.ToString(txtCustomFieldData.Text.Trim()));
                    }
                    dictionaryInstance.Add("UserId", Convert.ToString(user.UserId));
                    dictionaryInstance.Add("Flag", Convert.ToString('A'));

                    bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);

                    if (Result)
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field inserted successfully", AlertType.Success);
                    else
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while inserting. Please try again", AlertType.Failure);

                    byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
                    GetCustomFieldsData(HierarchyLevel);

                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            else
            {

            }
        }*/
        #endregion
        #region "gvCustomFields_RowCancelingEdit_bk"
        /*protected void gvCustomFields_RowCancelingEdit_bk(object sender, GridViewCancelEditEventArgs e)
        {
            gvCustomFields.EditIndex = -1;
            byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
            GetCustomFieldsData(HierarchyLevel);
        }*/
        #endregion
        #endregion
        #region "gvCustomFields Events New  /*User Type*/"
        protected void gvCustomFields_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {
                #region
                RegistrationCustomFieldBE objRegistrationCustomFieldBE = (RegistrationCustomFieldBE)e.Row.DataItem;
                DropDownList ddlFieldTypesEdit = (DropDownList)e.Row.FindControl("ddlFieldTypesEdit");
                CheckBox chkIsVisibleEdit = (CheckBox)e.Row.FindControl("chkIsVisibleEdit");
                CheckBox chkIsMandatoryEdit = (CheckBox)e.Row.FindControl("chkIsMandatoryEdit");
                CheckBox chkIsIDFieldEdit = (CheckBox)e.Row.FindControl("chkIsIDFieldEdit");
                CheckBox chkIsLoginFieldEdit = (CheckBox)e.Row.FindControl("chkIsLoginFieldEdit");
                CheckBox chkIsEditableFieldEdit = (CheckBox)e.Row.FindControl("chkIsEditableFieldEdit");
                LinkButton lbImportEdit = (LinkButton)e.Row.FindControl("lbImportEdit") as LinkButton;
                LinkButton lbExportEdit = (LinkButton)e.Row.FindControl("lbExportEdit") as LinkButton;
                List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE> objCurr = new List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE>();
                objCurr = RegistrationCustomFieldBL.GetFieldTypeMasterDetails();
                objCurr.RemoveAll(x => x.FieldTypeId == 4);
                // Commented by SHRIGANESH on 22 June 2016
                //ddlFieldTypesEdit.DataSource = RegistrationCustomFieldBL.GetFieldTypeMasterDetails();
                ddlFieldTypesEdit.DataSource = objCurr;
                ddlFieldTypesEdit.DataTextField = "FieldTypeName";
                ddlFieldTypesEdit.DataValueField = "FieldTypeId";
                ddlFieldTypesEdit.DataBind();
                ddlFieldTypesEdit.SelectedValue = Convert.ToString(objRegistrationCustomFieldBE.FieldType);
                chkIsVisibleEdit.Checked = objRegistrationCustomFieldBE.IsVisible;
                chkIsMandatoryEdit.Checked = objRegistrationCustomFieldBE.IsMandatory;
                chkIsIDFieldEdit.Checked = objRegistrationCustomFieldBE.IsStoreDefault;
                chkIsLoginFieldEdit.Checked = objRegistrationCustomFieldBE.IsLoginField;
                chkIsEditableFieldEdit.Checked = objRegistrationCustomFieldBE.IsUserEditable;
                if (ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "checkbox" || chkIsIDFieldEdit.Checked)
                {
                    lbImportEdit.Visible = true;
                    lbExportEdit.Visible = true;
                }
                else
                {
                    lbImportEdit.Visible = false;
                    lbExportEdit.Visible = false;
                    // Commented by SHRIGANESH 23 June 2016
                    //rfvCustomFieldDataEdit.Enabled = false;
                }
                #endregion
            }

            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                #region
                RegistrationCustomFieldBE objRegistrationCustomFieldBE = (RegistrationCustomFieldBE)e.Row.DataItem;
                //Label lblCustomFieldData = (Label)e.Row.FindControl("lblCustomFieldData");
                if (objRegistrationCustomFieldBE.FieldTypeName.ToLower().Trim() == "dropdown" || objRegistrationCustomFieldBE.FieldTypeName.ToLower().Trim() == "checkbox")
                {

                }
                #endregion

                gvCustomFields.Columns[7].Visible = false; // 21 July 2016
                gvCustomFields.Columns[8].Visible = false; // 21 July 2016
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                #region
                DropDownList ddlFieldTypes = (DropDownList)e.Row.FindControl("ddlFieldTypes");
                List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE> objCurr = new List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE>();
                objCurr = RegistrationCustomFieldBL.GetFieldTypeMasterDetails();
                objCurr.RemoveAll(x => x.FieldTypeId == 4);
                // Commented by SHRIGANESH on 22 June 2016
                //ddlFieldTypes.DataSource = RegistrationCustomFieldBL.GetFieldTypeMasterDetails();
                ddlFieldTypes.DataSource = objCurr;
                ddlFieldTypes.DataTextField = "FieldTypeName";
                ddlFieldTypes.DataValueField = "FieldTypeId";
                ddlFieldTypes.DataBind();
                #endregion
            }
        }
        protected void gvCustomFields_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvCustomFields.EditIndex = e.NewEditIndex;
            GetCustomFieldsDataByUserTypeID(ddlUserTypeNames.SelectedValue);
            gvCustomFields.Columns[7].Visible = true;
            gvCustomFields.Columns[8].Visible = true;
            // Commented by SHRIGANESH on 22 June 2016
            //byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
            //GetCustomFieldsData(HierarchyLevel);
        }
        protected void gvCustomFields_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                #region RegistrationFieldsConfigurationId added to pass value to CheckForExistingCustomLoginId() - By Snehal Jadhav 07 Sep 2016

                Int16 RegistrationFieldsConfigurationId = Convert.ToInt16(gvCustomFields.DataKeys[e.RowIndex].Values["RegistrationFieldsConfigurationId"]);

                #endregion

                UserBE user = (UserBE)Session["StoreUser"];
                TextBox txtCustomFieldEdit = (TextBox)gvCustomFields.Rows[e.RowIndex].FindControl("txtCustomFieldEdit");
                DropDownList ddlFieldTypesEdit = (DropDownList)gvCustomFields.Rows[e.RowIndex].FindControl("ddlFieldTypesEdit");
                CheckBox chkIsIDFieldEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsIDFieldEdit");
                CheckBox chkIsLoginFieldEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsLoginFieldEdit");
                CheckBox chkIsEditableFieldEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsEditableFieldEdit");

                #region Validate Excel file if File Type is 2 or 3 Commented on 22 July 2016 SHRIGANESH SINGH
                //if (ddlFieldTypesEdit.SelectedValue == "2" || ddlFieldTypesEdit.SelectedValue == "3" || chkIsIDFieldEdit.Checked)
                //{
                //    if (flupldCustomFieldsExcel.HasFiles)
                //    {
                //        bool status = false;
                //        string ErrorFileName = "";
                //        int FailedRecords = 0;
                //        List<ProductBE> lstProductBE = new List<ProductBE>();
                //        string xlsFile = flupldCustomFieldsExcel.FileName;  //fileImport is the file upload control
                //        string ext = System.IO.Path.GetExtension(xlsFile);
                //        if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
                //        {
                //            GlobalFunctions.ShowModalAlertMessages(this.Page, "Only Excel Files are allowed for Product Import.", AlertType.Warning);
                //            return;
                //        }
                //        //lstAnonymousObjs.Add(new
                //    }
                //    else
                //    {
                //        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select an Excel file to Import", AlertType.Warning);
                //    }
                //}
                #endregion

                CheckBox chkIsVisibleEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsVisibleEdit");
                CheckBox chkIsMandatoryEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsMandatoryEdit");
               
                #region Check if Login Field Already Exists for Current User Type SNEHAL JADHAV 07 Sep 2016

                if (chkIsLoginFieldEdit.Checked)
                {
                    bool IsResult = RegistrationCustomFieldBL.CheckForExistingCustomLoginId(ddlUserTypeNames.SelectedValue, RegistrationFieldsConfigurationId);

                    if (IsResult)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Login Id has already assigned to the selected User Type.", AlertType.Failure);
                        return;
                    }
                }

                #endregion

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LabelTitle", Convert.ToString(txtCustomFieldEdit.Text.Trim()));
                dictionaryInstance.Add("FieldType", Convert.ToString(ddlFieldTypesEdit.SelectedValue));
                dictionaryInstance.Add("IsVisible", Convert.ToString(chkIsVisibleEdit.Checked));
                dictionaryInstance.Add("IsMandatory", Convert.ToString(chkIsMandatoryEdit.Checked));
                dictionaryInstance.Add("IsStoreDefault", Convert.ToString(chkIsIDFieldEdit.Checked));
                dictionaryInstance.Add("IsLoginField", Convert.ToString(chkIsLoginFieldEdit.Checked));
                //dictionaryInstance.Add("IsUserEditableEdit", Convert.ToString(chkIsUserEditableEdit.Checked));
                // Commented by SHRIGANESH on 22 June 2016
                //dictionaryInstance.Add("UserHierarchyLevel", Convert.ToString(ddlUserHierarchies.SelectedValue));
                dictionaryInstance.Add("UserTypeID", ddlUserTypeNames.SelectedValue);
                // if (ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "checkbox")
                // {
                // Commented by SHRIGANESH on 24 June 2016
                //TextBox txtCustomFieldDataEdit = (TextBox)gvCustomFields.Rows[e.RowIndex].FindControl("txtCustomFieldDataEdit");
                //dictionaryInstance.Add("CustomFieldData", Convert.ToString(txtCustomFieldDataEdit.Text.Trim()));
                // }
                dictionaryInstance.Add("UserId", Convert.ToString(user.UserId));
                dictionaryInstance.Add("RegistrationFieldsConfigurationId", Convert.ToString(RegistrationFieldsConfigurationId));
                dictionaryInstance.Add("LanguageIdToUpdate", Convert.ToString(ddlLanguage.SelectedValue));
                dictionaryInstance.Add("Flag", Convert.ToString('E'));
                dictionaryInstance.Add("IsUserEditable", Convert.ToString(chkIsEditableFieldEdit.Checked));

                #region CODE COMMENTED BY SHRIGANESH SINGH 22 July 2016
                //UserBE lst = Session["StoreUser"] as UserBE;
                //DataTable table = new DataTable();
                //table.Columns.Add("RegistrationFieldsConfigurationId", typeof(Int16));
                //table.Columns.Add("RegistrationFieldDataValue", typeof(string));
                //table.Columns.Add("CreatedBy", typeof(Int16));
                //table.Columns.Add("ModifiedBy", typeof(Int16));

                //if (ddlFieldTypesEdit.SelectedValue == "2" || ddlFieldTypesEdit.SelectedValue == "3" || chkIsIDFieldEdit.Checked)
                //{
                //    string strFileName = flupldCustomFieldsExcel.FileName;
                //    FileInfo fi = new FileInfo(strFileName);
                //    string strFilExt = fi.Extension;
                //    //strFileName = "UserTypesCustomFieldsData" + strFilExt;
                //    string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "\\Excel\\Custom Fields Data\\" + strFileName;

                //    if (File.Exists(strFileUploadPath))
                //    {
                //        File.Delete(strFileUploadPath);
                //    }

                //    flupldCustomFieldsExcel.SaveAs(strFileUploadPath);
                //    if (File.Exists(strFileUploadPath))
                //    {
                //        DataTable dt = new DataTable();
                //        dt = ConvertExcelToDataTable(strFileUploadPath);
                //        foreach (DataColumn col in dt.Columns)
                //        {
                //            foreach (DataRow row in dt.Rows)
                //            {
                //                DataRow dr = table.NewRow();
                //                dr["RegistrationFieldsConfigurationId"] = RegistrationFieldsConfigurationId;
                //                dr["RegistrationFieldDataValue"] = row[col.ColumnName].ToString();
                //                dr["CreatedBy"] = Convert.ToInt16(lst.UserId);
                //                dr["ModifiedBy"] = Convert.ToInt16(lst.UserId);

                //                table.Rows.Add(dr);
                //            }
                //        }
                //    }
                //} 
                #endregion

                #region Commented Code by SHRIGANESH SINGH 29 June 2016
                //DataTable dt = new DataTable();
                //string path = string.Concat(Server.MapPath("~/Excel/" + flupldCustomFieldsExcel.FileName));
                //dt = ConvertExcelToDataTable(path);
                //bool bres = UserTypesBL.DATATYPETEST(table);
                // Commented by SHRIGANESH on 22 June 2016
                //bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);
                //bool Result = RegistrationCustomFieldBL.AEDUserTypeRegistrationCustomFields(dictionaryInstance,table); 
                #endregion

                bool Result = UserTypesBL.UpdateUserTypeRegistrationCustomFields(dictionaryInstance);

                if (Result)
                {
                   // IsLoginCheckField();
                    #region Commented code by SHRIGANESH SINGH 29 June 2016
                    //try
                    //{
                    //    string path1 = string.Concat(Server.MapPath("~/Excel/" + flupldCustomFieldsExcel.FileName));
                    //    flupldCustomFieldsExcel.SaveAs(path1);
                    //    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path1);
                    //    OleDbConnection connection = new OleDbConnection();
                    //    connection.ConnectionString = excelConnectionString;
                    //    OleDbCommand command = new OleDbCommand("select * from [ProductData$]", connection);
                    //    connection.Open();
                    //    DbDataReader dr = command.ExecuteReader();
                    //    string sqlConnectionString = Constants.strStoreConnectionString; ;
                    //    SqlBulkCopy bulkInsert = new SqlBulkCopy(sqlConnectionString);
                    //    bulkInsert.DestinationTableName = "tbl_bulkupload";
                    //    bulkInsert.WriteToServer(dr);
                    //    //MsgAlert.Text = "Product uploaded successfully";
                    //    connection.Close();
                    //}
                    //catch (Exception ex)
                    //{
                    //    Exceptions.WriteExceptionLog(ex);
                    //} 
                    #endregion

                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field updated successfully", AlertType.Success);
                    CreateActivityLog("CustomFieldsSettings gvCustomFields RowUpdating", "Update", txtCustomFieldEdit.Text);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating. Please try again", AlertType.Failure);
                }
                gvCustomFields.EditIndex = -1;
                GetCustomFieldsDataByUserTypeID(ddlUserTypeNames.SelectedValue);
                // Commented by SHRIGANESH on 22 June 2016
                //byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
                //GetCustomFieldsData(HierarchyLevel);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void gvCustomFields_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Int16 RegistrationFieldsConfigurationId = Convert.ToInt16(gvCustomFields.DataKeys[e.RowIndex].Values["RegistrationFieldsConfigurationId"]);

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RegistrationFieldsConfigurationId", Convert.ToString(RegistrationFieldsConfigurationId));
                dictionaryInstance.Add("Flag", Convert.ToString('D'));

                // Commented by SHRIGANESH on 22 June 2016
                //bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);
                bool Result = RegistrationCustomFieldBL.AEDUserTypeRegistrationCustomFields(dictionaryInstance);

                if (Result)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field deleted successfully", AlertType.Success);
                    CreateActivityLog("CustomFieldsSettings gvCustomFields RowDeleting", "Delete", Convert.ToString(RegistrationFieldsConfigurationId));
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while deleting. Please try again", AlertType.Failure);

                GetCustomFieldsDataByUserTypeID(ddlUserTypeNames.SelectedValue);
                // Commented by SHRIGANESH on 22 June 2016
                //byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
                //GetCustomFieldsData(HierarchyLevel);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void gvCustomFields_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
            HiddenField hdnRegFieldConfigID = row.FindControl("hdnRegFieldConfigID") as HiddenField;

            #region ADD Section
            if (e.CommandName.ToLower() == "addnew")
            {
                try
                {
                    #region RegistrationFieldsConfigurationId added to pass value to CheckForExistingCustomLoginId() - By Snehal Jadhav 07 Sep 2016

                   //int rowIndex = Convert.ToInt32(e.CommandArgument);
                    //Int16 RegistrationFieldsConfigurationId = Convert.ToInt16(gvCustomFields.DataKeys[rowIndex].Values["RegistrationFieldsConfigurationId"]);

                    #endregion

                    DropDownList ddlFieldTypes = (DropDownList)gvCustomFields.FooterRow.FindControl("ddlFieldTypes");
                    UserBE user = (UserBE)Session["StoreUser"];
                    TextBox txtCustomField = (TextBox)gvCustomFields.FooterRow.FindControl("txtCustomField");
                    CheckBox chkIsVisible = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsVisible");
                    CheckBox chkIsMandatory = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsMandatory");
                    CheckBox chkIsIDField = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsIDField");
                    CheckBox chkIsLoginField = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsLoginField");
                    CheckBox chkIsEditableField = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsEditableField");

                    #region Check if Custom Field Name Already Exists SHRIGANESH SINGH 30 June 2016

                    bool bResult = RegistrationCustomFieldBL.CheckForExistingCustomFieldName(txtCustomField.Text.Trim(), ddlUserTypeNames.SelectedValue);

                    if (bResult)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field Name already exists against the selected User Type. Please enter a unique Custom Field Name", AlertType.Failure);
                        return;
                    }

                    #endregion

                    #region Check if Login Field Already Exists for Current User Type SNEHAL JADHAV 07 Sep 2016

                    if (chkIsLoginField.Checked)
                    {
                        //bool IsResult = RegistrationCustomFieldBL.CheckForExistingCustomLoginId(ddlUserTypeNames.SelectedValue, RegistrationFieldsConfigurationId);

                       // if (IsResult)
                        //{
                          //  GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Login Id has already assigned to the selected User Type.", AlertType.Failure);
                           // return;
                        //}
                    }

                    #endregion


                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("LabelTitle", Convert.ToString(txtCustomField.Text.Trim()));
                    dictionaryInstance.Add("FieldType", Convert.ToString(ddlFieldTypes.SelectedValue));
                    dictionaryInstance.Add("IsVisible", Convert.ToString(chkIsVisible.Checked));
                    dictionaryInstance.Add("IsMandatory", Convert.ToString(chkIsMandatory.Checked));
                    dictionaryInstance.Add("IsStoreDefault", Convert.ToString(chkIsIDField.Checked));
                    dictionaryInstance.Add("IsLoginField", Convert.ToString(chkIsLoginField.Checked));
                    dictionaryInstance.Add("IsUserEditable", Convert.ToString(chkIsEditableField.Checked));
                    // Commented by SHRIGANESH on 22 June 2016
                    //dictionaryInstance.Add("UserHierarchyLevel", Convert.ToString(ddlUserHierarchies.SelectedValue));
                    dictionaryInstance.Add("UserTypeID", ddlUserTypeNames.SelectedValue);
                    if (ddlFieldTypes.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypes.SelectedItem.Text.ToLower().Trim() == "checkbox")
                    {
                        //TextBox txtCustomFieldData = (TextBox)gvCustomFields.FooterRow.FindControl("txtCustomFieldData");
                        dictionaryInstance.Add("CustomFieldData", "");
                    }
                    dictionaryInstance.Add("UserId", Convert.ToString(user.UserId));
                    dictionaryInstance.Add("Flag", Convert.ToString('A'));

                    // Commented by SHRIGANESH on 22 June 2016
                    //bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);

                    bool Result = RegistrationCustomFieldBL.AEDUserTypeRegistrationCustomFields(dictionaryInstance);

                    if (Result)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field inserted successfully", AlertType.Success);
                        CreateActivityLog("CustomFieldsSettings gvCustomFields RowCommand", "Insert", txtCustomField.Text);
                    }

                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while inserting. Please try again", AlertType.Failure);
                    }
                    GetCustomFieldsDataByUserTypeID(ddlUserTypeNames.SelectedValue);
                    // Commented by SHRIGANESH on 22 June 2016
                    //byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
                    //GetCustomFieldsData(HierarchyLevel);
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            #endregion

            #region IMPORT Section
            if (e.CommandName.ToLower() == "import")
            {
                try
                {
                    //Int16 RegistrationFieldsConfigurationId = Convert.ToInt16(gvCustomFields.DataKeys[e.RowIndex].Values["RegistrationFieldsConfigurationId"]);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:ShowFileUploader();", true);
                    //ModalFileUpload.Style.Add("display", "block");

                    CheckBox chkIsIDFieldEdit = row.FindControl("chkIsIDFieldEdit") as CheckBox;
                    FileUpload flCustomFieldValues = row.FindControl("flCustomFieldValues") as FileUpload;
                    DropDownList ddlFieldTypesEdit = row.FindControl("ddlFieldTypesEdit") as DropDownList;
                    int RegFieldConfigID = Convert.ToInt16(hdnRegFieldConfigID.Value);

                    if (chkIsIDFieldEdit.Checked)
                    {
                        if (flCustomFieldValues.HasFiles)
                        {
                            bool status = false;
                            string ErrorFileName = "";
                            int FailedRecords = 0;
                            List<ProductBE> lstProductBE = new List<ProductBE>();
                            string xlsFile = flCustomFieldValues.FileName;  //fileImport is the file upload control
                            string ext = System.IO.Path.GetExtension(xlsFile);
                            if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Only Excel Files are allowed for Import.", AlertType.Warning);
                                return;
                            }

                            UserBE lst = Session["StoreUser"] as UserBE;
                            DataTable table = new DataTable();
                            table.Columns.Add("RegistrationFieldsConfigurationId", typeof(Int16));
                            table.Columns.Add("RegistrationFieldDataValue", typeof(string));
                            table.Columns.Add("CreatedBy", typeof(Int16));
                            table.Columns.Add("ModifiedBy", typeof(Int16));

                            if (ddlFieldTypesEdit.SelectedValue == "1" || ddlFieldTypesEdit.SelectedValue == "2" || ddlFieldTypesEdit.SelectedValue == "3" || chkIsIDFieldEdit.Checked)
                            {
                                string strFileName = flCustomFieldValues.FileName;
                                FileInfo fi = new FileInfo(strFileName);
                                string strFilExt = fi.Extension;
                                //strFileName = "UserTypesCustomFieldsData" + strFilExt;
                                string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "\\Excel\\Custom Fields Data\\" + strFileName;

                                if (File.Exists(strFileUploadPath))
                                {
                                    File.Delete(strFileUploadPath);
                                }

                                flCustomFieldValues.SaveAs(strFileUploadPath);
                                if (File.Exists(strFileUploadPath))
                                {
                                    DataTable dt = new DataTable();
                                    dt = ConvertExcelToDataTable(strFileUploadPath);
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        foreach (DataRow row1 in dt.Rows)
                                        {
                                            DataRow dr = table.NewRow();
                                            dr["RegistrationFieldsConfigurationId"] = RegFieldConfigID;
                                            dr["RegistrationFieldDataValue"] = row1[col.ColumnName].ToString();
                                            dr["CreatedBy"] = Convert.ToInt16(lst.UserId);
                                            dr["ModifiedBy"] = Convert.ToInt16(lst.UserId);

                                            table.Rows.Add(dr);
                                        }
                                    }
                                }

                                status = UserTypesBL.InsertCustomFieldValues(table, RegFieldConfigID);
                                if (status)
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field Values Imported successfully", AlertType.Success);
                                }
                                else
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while Importing values. Please try again", AlertType.Failure);
                                }
                            }
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select an Excel file to Import and try again", AlertType.Warning);
                        }
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please check the Is ID Field control", AlertType.Warning);
                    }

                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            #endregion

            #region EXPORT Section
            if (e.CommandName.ToLower() == "export")
            {
                try
                {
                    if (ExportValidation())
                    {
                        int RegFieldConfigID = Convert.ToInt16(hdnRegFieldConfigID.Value);
                        TextBox txtCustomFieldEdit = (TextBox)row.FindControl("txtCustomFieldEdit");

                        LinkButton lbImportEdit = (LinkButton)row.Cells[4].FindControl("lbImportEdit") as LinkButton;
                        LinkButton lbExportEdit = row.FindControl("lbExportEdit") as LinkButton;
                        lbImportEdit.Visible = true;
                        lbExportEdit.Visible = true;

                        DataSet dsData = new DataSet();
                        DataTable dtExportToExcel = new DataTable();
                        List<UserTypeCustomFieldDataValue> lstCustomFieldData = new List<UserTypeCustomFieldDataValue>();
                        lstCustomFieldData = UserTypesBL.GetAllCustomFieldDataByFieldID(RegFieldConfigID);

                        if (lstCustomFieldData != null)
                        {
                            List<object> lstAnonymousObjs = new List<object>();
                            ExportFileName = txtCustomFieldEdit.Text.Trim() + "_CustomFieldData";

                            foreach (UserTypeCustomFieldDataValue CustomDataValue in lstCustomFieldData)
                            {
                                lstAnonymousObjs.Add(new
                                {
                                    RegistrationFieldDataValue = CustomDataValue.RegistrationFieldDataValue
                                });
                            }

                            dsData = CreateDataSet(lstAnonymousObjs);
                            dtExportToExcel = dsData.Tables[0];

                            #region Commented code as on 28 June 2016 by SHRIGANESH SINGH
                            //if (dsData == null)
                            //{
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "No Custom Field Data Found.", AlertType.Warning);
                            //return;
                            // Commented by SHRIGANESH on 22 June 2016
                            //dlStatus.Visible = true;
                            //dlStatus.Attributes.Add("class", "Errormsg");
                            //ltrMessage.Text = Exceptions.GetException("SystemError/GeneralMessage");
                            //}

                            //GenerateExcel(dtExportToExcel); 
                            #endregion
                        }
                        else
                        {
                            DataColumn colCustomFieldHeader = new System.Data.DataColumn(txtCustomFieldEdit.Text.Trim(), typeof(string));
                            colCustomFieldHeader.DefaultValue = txtCustomFieldEdit.Text.Trim();
                            dtExportToExcel.Columns.Add(colCustomFieldHeader);

                            ExportFileName = txtCustomFieldEdit.Text.Trim() + "_CustomFieldData";

                            #region Commented Code by SHRIGANESH SINGH 29 June 2016
                            //HyperLink hplImportEdit = (HyperLink)row.Cells[4].FindControl("hplImportEdit") as HyperLink;
                            //LinkButton lbExportEdit = row.FindControl("lbExportEdit") as LinkButton;
                            //hplImportEdit.Visible = true;
                            //lbExportEdit.Visible = true;

                            //List<object> lstAnonymousObjs = new List<object>();
                            //lstAnonymousObjs.Add(new
                            //{
                            //    //RegistrationFieldDataValue = txtCustomField.Text + "_Value"
                            //});
                            //dsData = CreateDataSet(lstAnonymousObjs);
                            //dtExportToExcel = dsData.Tables[0];
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "No Products Found.", AlertType.Warning); 
                            #endregion
                        }
                        using (ExcelPackage pck = new ExcelPackage())
                        {
                            //Create the worksheet
                            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("CustomFieldData");
                            // DataTable NewTable = tbl.DefaultView.ToTable(false, "ProductCode","LanguageId","ProductName","ProductDescription", "FurtherDescription", "");

                            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                            ws.Cells["A1"].LoadFromDataTable(dtExportToExcel, true);

                            Response.Clear();
                            //Write it back to the client
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;  filename=" + ExportFileName + ".xlsx");
                            Response.BinaryWrite(pck.GetAsByteArray());

                            Response.End();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            #endregion

            #region EDIT ID FIELD SETTING Added on 21 July 2016
            if (e.CommandName.Equals("EditIdField"))
            {
                try
                {
                    GridViewRow row2 = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    HiddenField hdnRegFieldConfigID2 = row2.FindControl("hdnRegFieldConfigID") as HiddenField;

                    gvCustomFields.Columns[7].Visible = true;
                    gvCustomFields.Columns[8].Visible = true; // 21 July 2016
                    int index = Convert.ToInt32(e.CommandArgument);
                    //GridViewRow row1 = gvCustomFields.Rows[4];
                    //HiddenField hdnRegFieldConfigIDField1 = row1.FindControl("hdnRegFieldConfigIDField") as HiddenField;

                    int RegFieldConfigIDField = Convert.ToInt16(hdnRegFieldConfigID2.Value);
                    CheckBox chkIsIDFieldEdit = (CheckBox)row2.FindControl("chkIsIDFieldEdit");
                    DropDownList ddlFieldTypesEdit = (DropDownList)row.FindControl("ddlFieldTypesEdit");
                    LinkButton lbImportEdit = (LinkButton)row.FindControl("lbImportEdit") as LinkButton;
                    LinkButton lbExportEdit = (LinkButton)row.FindControl("lbExportEdit") as LinkButton;
                    //RegistrationCustomFieldBE objRegistrationCustomFieldBE = (RegistrationCustomFieldBE)row.DataItem;
                    //chkIsIDFieldEdit.Visible = true;
                    //chkIsIDFieldEdit.Checked = 

                    if (ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "checkbox" || chkIsIDFieldEdit.Checked)
                    //if (ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "checkbox")
                    {
                        lbImportEdit.Visible = true;
                        lbExportEdit.Visible = true;
                    }
                    else
                    {
                        lbImportEdit.Visible = false;
                        lbExportEdit.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            #endregion
        }
        protected void gvCustomFields_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvCustomFields.EditIndex = -1;
                GetCustomFieldsDataByUserTypeID(ddlUserTypeNames.SelectedValue);
                // Commented by SHRIGANESH on 22 June 2016
                //byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
                //GetCustomFieldsData(HierarchyLevel);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                GetCustomFieldsDataByUserTypeID(ddlUserTypeNames.SelectedValue);
                // Commented by SHRIGANESH on 22 June 2016
                //byte HierarchyLevel = Convert.ToByte(ddlUserHierarchies.SelectedValue);
                //GetCustomFieldsData(HierarchyLevel);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindLanguage(StoreBE objStoreBE)
        {
            try
            {
                if (objStoreBE.StoreLanguages.Count > 0)
                {
                    ddlLanguage.DataSource = objStoreBE.StoreLanguages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                    ddlLanguage.SelectedValue = Convert.ToString(objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId);
                    LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                }
                else
                {
                    LanguageId = objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #region "GetCustomFieldsData_bk"
        /*
        private void GetCustomFieldsData_bk(byte HierarchyLevel)
        {
            LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
            List<RegistrationCustomFieldBE> GetRegistrationCustomField = RegistrationCustomFieldBL.GetAllCustomFields();
            List<RegistrationCustomFieldBE> GetHierarchyLevelRegistrationCustomField = null;
            if (GetRegistrationCustomField != null)
            {
                if (GetRegistrationCustomField.Count > 0)
                {
                    switch (HierarchyLevel)
                    {
                        case 1:
                        case 2:
                        case 3:
                            GetHierarchyLevelRegistrationCustomField = GetRegistrationCustomField.FindAll(x => x.LanguageId == LanguageId && x.UserHierarchyId == HierarchyLevel);
                            break;
                        case 4:
                            GetHierarchyLevelRegistrationCustomField = GetRegistrationCustomField.FindAll(x => x.LanguageId == LanguageId && x.UserHierarchyId == 0);
                            break;
                        default:
                            break;
                    }
                }
            }

            if (GetHierarchyLevelRegistrationCustomField != null)
            {
                if (GetHierarchyLevelRegistrationCustomField.Count > 0)
                {
                    gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                    gvCustomFields.DataBind();
                }
                else
                {
                    GetHierarchyLevelRegistrationCustomField = new List<RegistrationCustomFieldBE>();
                    RegistrationCustomFieldBE objRegistrationCustomFieldBE = new RegistrationCustomFieldBE();
                    objRegistrationCustomFieldBE.DisplayOrder = 0;
                    objRegistrationCustomFieldBE.FieldType = 1;
                    objRegistrationCustomFieldBE.LabelTitle = "temp";
                    objRegistrationCustomFieldBE.FieldTypeName = "temp";
                    GetHierarchyLevelRegistrationCustomField.Add(objRegistrationCustomFieldBE);
                    gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                    gvCustomFields.DataBind();
                    gvCustomFields.Rows[0].Visible = false;
                }
            }
            else
            {
                GetHierarchyLevelRegistrationCustomField = new List<RegistrationCustomFieldBE>();
                RegistrationCustomFieldBE objRegistrationCustomFieldBE = new RegistrationCustomFieldBE();
                objRegistrationCustomFieldBE.DisplayOrder = 0;
                objRegistrationCustomFieldBE.FieldType = 1;
                objRegistrationCustomFieldBE.LabelTitle = "temp";
                objRegistrationCustomFieldBE.FieldTypeName = "temp";
                GetHierarchyLevelRegistrationCustomField.Add(objRegistrationCustomFieldBE);
                gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                gvCustomFields.DataBind();
                gvCustomFields.Rows[0].Visible = false;
            }
        }
        */
        #endregion
        private void GetCustomFieldsData(byte HierarchyLevel)
        {
            try
            {
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                List<RegistrationCustomFieldBE> GetRegistrationCustomField = RegistrationCustomFieldBL.GetAllCustomFields();
                List<RegistrationCustomFieldBE> GetHierarchyLevelRegistrationCustomField = null;
                if (GetRegistrationCustomField != null)
                {
                    if (GetRegistrationCustomField.Count > 0)
                    {
                        #region
                        switch (HierarchyLevel)
                        {
                            case 1:
                            case 2:
                            case 3:
                                GetHierarchyLevelRegistrationCustomField = GetRegistrationCustomField.FindAll(x => x.LanguageId == LanguageId && x.UserHierarchyId == HierarchyLevel);
                                break;
                            case 4:
                                GetHierarchyLevelRegistrationCustomField = GetRegistrationCustomField.FindAll(x => x.LanguageId == LanguageId && x.UserHierarchyId == 0);
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
                }

                if (GetHierarchyLevelRegistrationCustomField != null)
                {
                    #region
                    if (GetHierarchyLevelRegistrationCustomField.Count > 0)
                    {
                        gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                        gvCustomFields.DataBind();
                    }
                    else
                    {
                        GetHierarchyLevelRegistrationCustomField = new List<RegistrationCustomFieldBE>();
                        RegistrationCustomFieldBE objRegistrationCustomFieldBE = new RegistrationCustomFieldBE();
                        objRegistrationCustomFieldBE.DisplayOrder = 0;
                        objRegistrationCustomFieldBE.FieldType = 1;
                        objRegistrationCustomFieldBE.LabelTitle = "temp";
                        objRegistrationCustomFieldBE.FieldTypeName = "temp";
                        GetHierarchyLevelRegistrationCustomField.Add(objRegistrationCustomFieldBE);
                        gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                        gvCustomFields.DataBind();
                        gvCustomFields.Rows[0].Visible = false;
                    }
                    #endregion
                }
                else
                {
                    #region
                    GetHierarchyLevelRegistrationCustomField = new List<RegistrationCustomFieldBE>();
                    RegistrationCustomFieldBE objRegistrationCustomFieldBE = new RegistrationCustomFieldBE();
                    objRegistrationCustomFieldBE.DisplayOrder = 0;
                    objRegistrationCustomFieldBE.FieldType = 1;
                    objRegistrationCustomFieldBE.LabelTitle = "temp";
                    objRegistrationCustomFieldBE.FieldTypeName = "temp";
                    GetHierarchyLevelRegistrationCustomField.Add(objRegistrationCustomFieldBE);
                    gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                    gvCustomFields.DataBind();
                    gvCustomFields.Rows[0].Visible = false;
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   22 June 2016
        /// Scope   :   To get All Custom Fileds for Given Usertype
        /// </summary>
        /// <param name="UsertypeID"></param>
        private void GetCustomFieldsDataByUserTypeID(string UsertypeID)
        {
            try
            {
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserTypeID", UsertypeID);
                dictionaryInstance.Add("LanguageID", Convert.ToString(LanguageId));
                List<RegistrationCustomFieldBE> GetRegistrationCustomField = RegistrationCustomFieldBL.GetAllCustomFieldsByUserTypeID(dictionaryInstance);
                //List<RegistrationCustomFieldBE> GetHierarchyLevelRegistrationCustomField = null;

                if (GetRegistrationCustomField != null)
                {
                    gvCustomFields.DataSource = GetRegistrationCustomField;
                    gvCustomFields.DataBind();
                }
                else
                {
                    List<RegistrationCustomFieldBE> GetHierarchyLevelRegistrationCustomField = new List<RegistrationCustomFieldBE>();
                    RegistrationCustomFieldBE objRegistrationCustomFieldBE = new RegistrationCustomFieldBE() ;
                    objRegistrationCustomFieldBE.DisplayOrder = 0;
                    objRegistrationCustomFieldBE.FieldType = 1;
                    objRegistrationCustomFieldBE.LabelTitle = "temp";
                    objRegistrationCustomFieldBE.FieldTypeName = "temp";
                    GetHierarchyLevelRegistrationCustomField.Add(objRegistrationCustomFieldBE);
                    gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                    gvCustomFields.DataBind();
                    gvCustomFields.Rows[0].Visible = false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void ddlFieldTypesEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = (DropDownList)sender;
                GridViewRow row = (GridViewRow)ddl.Parent.Parent;
                int idx = row.RowIndex;

                #region "bk"
                //TextBox txtCustomFieldDataEdit = (TextBox)row.Cells[4].FindControl("txtCustomFieldDataEdit");
                //RequiredFieldValidator rfvCustomFieldDataEdit = (RequiredFieldValidator)row.Cells[4].FindControl("rfvCustomFieldDataEdit");
                //if (ddl.SelectedItem.Text.ToLower() == "dropdown" || ddl.SelectedItem.Text.ToLower() == "checkbox")
                //{
                //    txtCustomFieldDataEdit.Visible = true;
                //    rfvCustomFieldDataEdit.Enabled = true;
                //}
                //else
                //{
                //    txtCustomFieldDataEdit.Visible = false;
                //    rfvCustomFieldDataEdit.Enabled = false;
                //} 
                #endregion
                LinkButton lbImportEdit = (LinkButton)row.Cells[4].FindControl("lbImportEdit") as LinkButton;
                LinkButton lbExportEdit = (LinkButton)row.Cells[4].FindControl("lbExportEdit") as LinkButton;

                if (ddl.SelectedItem.Text.ToLower() == "dropdown" || ddl.SelectedItem.Text.ToLower() == "checkbox")
                {
                    lbImportEdit.Visible = true;
                    lbExportEdit.Visible = true;
                }
                else
                {
                    lbImportEdit.Visible = false;
                    lbExportEdit.Visible = false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #region "ddlFieldTypes_SelectedIndexChanged_bk"
        /*protected void ddlFieldTypes_SelectedIndexChanged_bk(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl.Parent.Parent;
            int idx = row.RowIndex;

            TextBox txtCustomFieldData = (TextBox)row.Cells[4].FindControl("txtCustomFieldData");
            RequiredFieldValidator rfvCustomFieldData = (RequiredFieldValidator)row.Cells[4].FindControl("rfvCustomFieldData");
            if (ddl.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddl.SelectedItem.Text.ToLower().Trim() == "checkbox")
            {
                txtCustomFieldData.Visible = true;
                rfvCustomFieldData.Enabled = true;
            }
            else
            {
                txtCustomFieldData.Visible = false;
                rfvCustomFieldData.Enabled = false;
            }
        }*/
        #endregion
        protected void ddlFieldTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = (DropDownList)sender;
                GridViewRow row = (GridViewRow)ddl.Parent.Parent;
                int idx = row.RowIndex;
                #region Commented Code by SHRIGANESH SINGH 29 June 2016
                //LinkButton lbExportEdit = (LinkButton)row.Cells[4].FindControl("lbExportEdit") as LinkButton;
                // Commented by SHRIGANESH 23 June 2016
                //TextBox txtCustomFieldData = (TextBox)row.Cells[4].FindControl("txtCustomFieldData");
                //RequiredFieldValidator rfvCustomFieldData = (RequiredFieldValidator)row.Cells[4].FindControl("rfvCustomFieldData"); 
                #endregion

                if (ddl.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddl.SelectedItem.Text.ToLower().Trim() == "checkbox")
                {
                    #region Code Commented by SHRIGANESH SINGH 29 June 2016
                    //ImpLink.Visible = true;
                    //cmdImport.Visible = true;
                    //cmdExport.Visible = true;
                    // Commented by SHRIGANESH 23 June 2016
                    //txtCustomFieldData.Visible = true;
                    //rfvCustomFieldData.Enabled = true;
                    //lbExportEdit.Visible = true; 
                    #endregion
                }
                else
                {
                    #region Code Commented by SHRIGANESH SINGH 29 June 2016
                    //ImpLink.Visible = false;
                    //cmdImport.Visible = false;
                    //cmdExport.Visible = false;
                    // Commented by SHRIGANESH 23 June 2016
                    //txtCustomFieldData.Visible = false;
                    //rfvCustomFieldData.Enabled = false;
                    //lbExportEdit.Visible = false; 
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   24 June 2016
        /// Scope   :   To convert the given data in Excel to DataTable
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns> return DataTable dtResult </returns>
        public static DataTable ConvertExcelToDataTable(string FileName)
        {
            DataTable dtResult = null;
            try
            {
                int totalSheet = 0; //No of sheets on excel file  
                using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;
                        sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                    }
                    cmd.Connection = objConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, "excelData");
                    dtResult = ds.Tables["excelData"];
                    objConn.Close();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return dtResult; //Returning DataTable 
        }

        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   24 June 2016
        /// Scope   :   To Validate the Export Command before exporting excel file
        /// </summary>
        /// <param name="RegistrationFieldConfigurationID"></param>
        /// <returns> return boolean bResult</returns>
        protected bool ExportValidation()
        {
            bool bResult = false;
            try
            {
                if (ddlUserTypeNames.SelectedValue.ToString() != "-1")
                {
                    if (ddlLanguage.SelectedValue.ToString() != "0")
                    {
                        bResult = true;
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Language.", AlertType.Warning);
                        bResult = false;
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select User Type Name.", AlertType.Warning);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return bResult;
        }

        public DataSet CreateDataSet<T>(List<T> list)
        {
            //it does so create the dataset and table
            var dataSet = new DataSet();
            var dataTable = new DataTable();
            try
            {
                //list is nothing or has nothing, return nothing (or add exception handling)
                if (list == null || list.Count == 0) { return null; }

                //get the type of the first obj in the list
                var obj = list[0].GetType();

                //now grab all properties
                var properties = obj.GetProperties();

                //make sure the obj has properties, return nothing (or add exception handling)
                if (properties.Length == 0) { return null; }



                //now build the columns from the properties
                var columns = new DataColumn[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    columns[i] = new DataColumn(properties[i].Name, properties[i].PropertyType);
                }

                //add columns to table
                dataTable.Columns.AddRange(columns);

                //now add the list values to the table
                foreach (var item in list)
                {
                    //create a new row from table
                    var dataRow = dataTable.NewRow();

                    //now we have to iterate thru each property of the item and retrieve it's value for the corresponding row's cell
                    var itemProperties = item.GetType().GetProperties();

                    for (int i = 0; i < itemProperties.Length; i++)
                    {
                        dataRow[i] = itemProperties[i].GetValue(item, null);
                    }

                    //now add the populated row to the table
                    dataTable.Rows.Add(dataRow);
                }

                //add table to dataset
                dataSet.Tables.Add(dataTable);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            //return dataset
            return dataSet;
        }

        protected void chkIsIDFieldEdit_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = ((GridViewRow)((CheckBox)sender).NamingContainer);
                int index = row.RowIndex;
                LinkButton lbImportEdit = ((LinkButton)row.FindControl("lbImportEdit"));
                LinkButton lbExportEdit = ((LinkButton)row.FindControl("lbExportEdit"));
                CheckBox chkIsStoreDefault = ((CheckBox)row.FindControl("chkIsIDFieldEdit"));

                if (chkIsStoreDefault.Checked)
                {
                    lbExportEdit.Visible = true;
                    lbImportEdit.Visible = true;
                }
                else
                {
                    lbExportEdit.Visible = false;
                    lbImportEdit.Visible = false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}