﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Settings_Robot : BasePage//System.Web.UI.Page
    {
        public string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        protected global::System.Web.UI.WebControls.TextBox txtNoSearchText;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Read();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void Write()
        {
            // First We create the backup then delete existing file and Re-create it. 
            try
            {
                string FilePath = GlobalFunctions.GetPhysicalFolderPath() + "robots.txt";
                string FileName = Path.GetFileName(FilePath);
                string BackUpFolderPath = GlobalFunctions.GetPhysicalFolderPathAdmin() + @"\Robot File backup";
                if (FilePath != "")
                {
                    if (txtNoSearchText.Text != string.Empty)
                    {
                        if (!Directory.Exists(BackUpFolderPath))
                            Directory.CreateDirectory(BackUpFolderPath);
                        File.Copy(FilePath, BackUpFolderPath + "\\" + FileName, true);
                        if (!File.Exists(FilePath))
                        {
                            File.Create(FilePath + "\\" + FileName);
                        }
                        File.WriteAllText(FilePath, txtNoSearchText.Text);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated successfully.", AlertType.Success);
                        CreateActivityLog("Robot btnSave", "Update", "");

                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Text can not be blank.", AlertType.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Write();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void Read()
        {
            try
            {
                string FilePath = GlobalFunctions.GetPhysicalFolderPath() + "robots.txt";
                if (FilePath != "")
                {
                    txtNoSearchText.Text = File.ReadAllText(FilePath);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "File not Found.", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}