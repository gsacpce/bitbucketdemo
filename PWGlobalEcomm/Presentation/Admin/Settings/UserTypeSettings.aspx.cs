﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Settings_UserTypeSettings : BasePage//System.Web.UI.Page
    {
        #region Variables

        public string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        Int16 StoreId = 0;
        Int16 intUserId = 1;

        protected global::System.Web.UI.WebControls.Label lbl, lblCRCustomerTypeHeading, lblCRApprovalRequired;
        protected global::System.Web.UI.WebControls.HiddenField hdnStoreId;
        protected global::System.Web.UI.WebControls.RadioButtonList rblCRCustomerType, rblCRApprovalRequired, rblUserType1, rblUserType2, rblUserType3;
        protected global::System.Web.UI.WebControls.DropDownList ddlCRCustomerHierarchyLevel, ddlUserType1CatalogueIds, ddlUserType2CatalogueIds, ddlUserType3CatalogueIds;
        protected global::System.Web.UI.WebControls.TextBox txtCRNotificationEmail, txtUserType1, txtUserType2, txtUserType3;
        protected global::System.Web.UI.WebControls.CheckBox chkUserType1, chkUserType2, chkUserType3;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvApprovalRequired, dvUserTypesDetails, dvUserType1, dvUserType2, dvUserType3;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvUserType1, rfvUserType2, rfvUserType3;
        protected global::System.Web.UI.WebControls.CustomValidator cvUserType1CatalogueIds, cvUserType2CatalogueIds, cvUserType3CatalogueIds;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    GetStoreFeatures();
                }
                Page.Title = "Manage User Types";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GetStoreFeatures()
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                hdnStoreId.Value = Convert.ToString(objStoreBE.StoreId);
                StoreId = objStoreBE.StoreId;
                List<FeatureBE> GetAllFeatures = FeatureBL.GetAllFeatures(StoreId, true);

                #region Get Customer Type

                FeatureBE CR_CustomerTypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_customertype");
                if (CR_CustomerTypeFeature != null)
                {
                    lblCRCustomerTypeHeading.Text = CR_CustomerTypeFeature.FeatureAlias;
                    rblCRCustomerType.DataSource = CR_CustomerTypeFeature.FeatureValues;
                    rblCRCustomerType.DataTextField = "FeatureValue";
                    rblCRCustomerType.DataValueField = "StoreFeatureDetailId";
                    rblCRCustomerType.DataBind();
                    if (CR_CustomerTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rblCRCustomerType.SelectedValue = Convert.ToString(CR_CustomerTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    else
                        rblCRCustomerType.SelectedValue = Convert.ToString(CR_CustomerTypeFeature.FeatureValues[1].StoreFeatureDetailId);

                    FeatureBE.FeatureValueBE FeatureValueBEInstance = CR_CustomerTypeFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "hierarchy");
                    if (FeatureValueBEInstance.IsEnabled)
                    {
                        ddlCRCustomerHierarchyLevel.Enabled = true;
                        FeatureBE CR_CustomerHierarchyLevelFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_customerhierarchylevel");
                        ddlCRCustomerHierarchyLevel.SelectedValue = FeatureValueBEInstance.FeatureDefaultValue;
                        dvApprovalRequired.Style["display"] = "";
                    }
                    else
                    {
                        ddlCRCustomerHierarchyLevel.Enabled = false;
                        ddlCRCustomerHierarchyLevel.SelectedIndex = 0;
                        dvApprovalRequired.Style["display"] = "none";
                    }




                }

                #endregion

                #region Get Email Approval Required

                FeatureBE CR_ApprovalRequiredFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_approvalrequired");
                if (CR_ApprovalRequiredFeature != null)
                {
                    lblCRApprovalRequired.Text = CR_ApprovalRequiredFeature.FeatureAlias;
                    rblCRApprovalRequired.DataSource = CR_ApprovalRequiredFeature.FeatureValues;
                    rblCRApprovalRequired.DataTextField = "FeatureValue";
                    rblCRApprovalRequired.DataValueField = "StoreFeatureDetailId";
                    rblCRApprovalRequired.DataBind();
                    if (CR_ApprovalRequiredFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rblCRApprovalRequired.SelectedValue = Convert.ToString(CR_ApprovalRequiredFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    else
                        rblCRApprovalRequired.SelectedValue = Convert.ToString(CR_ApprovalRequiredFeature.FeatureValues[1].StoreFeatureDetailId);
                }

                #endregion

                #region Get All Hierarchy

                if (rblCRCustomerType.SelectedIndex == 0)
                {
                    dvUserTypesDetails.Style["display"] = "";
                    List<UserBE.UserHierarchyBE> GetUserHierarchies = UserBL.GetUserHierarchies();
                    if (GetUserHierarchies.Count > 0)
                    {
                        switch (Convert.ToInt16(GetUserHierarchies.Count))
                        {
                            case 1:
                                dvUserType1.Style["display"] = "";
                                dvUserType2.Style["display"] = "none";
                                dvUserType3.Style["display"] = "none";
                                rfvUserType1.Enabled = true;
                                rfvUserType2.Enabled = false;
                                rfvUserType3.Enabled = false;
                                txtUserType1.Text = GetUserHierarchies[0].HierarchyName.Trim();
                                chkUserType1.Checked = GetUserHierarchies[0].IncludeVAT;
                                break;
                            case 2:
                                dvUserType1.Style["display"] = "";
                                dvUserType2.Style["display"] = "";
                                dvUserType3.Style["display"] = "none";
                                rfvUserType1.Enabled = true;
                                rfvUserType2.Enabled = true;
                                rfvUserType3.Enabled = false;
                                txtUserType1.Text = GetUserHierarchies[0].HierarchyName.Trim();
                                chkUserType1.Checked = GetUserHierarchies[0].IncludeVAT;
                                txtUserType2.Text = GetUserHierarchies[1].HierarchyName.Trim();
                                chkUserType2.Checked = GetUserHierarchies[1].IncludeVAT;
                                break;
                            case 3:
                                dvUserType1.Style["display"] = "";
                                dvUserType2.Style["display"] = "";
                                dvUserType3.Style["display"] = "";
                                rfvUserType1.Enabled = true;
                                rfvUserType2.Enabled = true;
                                rfvUserType3.Enabled = true;
                                txtUserType1.Text = GetUserHierarchies[0].HierarchyName.Trim();
                                chkUserType1.Checked = GetUserHierarchies[0].IncludeVAT;
                                txtUserType2.Text = GetUserHierarchies[1].HierarchyName.Trim();
                                chkUserType2.Checked = GetUserHierarchies[1].IncludeVAT;
                                txtUserType3.Text = GetUserHierarchies[2].HierarchyName.Trim();
                                chkUserType3.Checked = GetUserHierarchies[2].IncludeVAT;
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                    dvUserTypesDetails.Style["display"] = "none";

                

                #endregion
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(hostAdmin + "Dashboard/Dashboard.aspx");
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                UserBE objUserBE = new UserBE();
                objUserBE = Session["StoreUser"] as UserBE;

                intUserId = objUserBE.UserId;
                StoreId = Convert.ToInt16(hdnStoreId.Value);

                #region Update Customer Type

                foreach (ListItem item in rblCRCustomerType.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    if (item.Text.ToLower() == "hierarchy")
                        DictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(ddlCRCustomerHierarchyLevel.SelectedValue));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Update Email Approval Required

                foreach (ListItem item in rblCRApprovalRequired.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Update Level 1 Hierarchy

                //if (string.IsNullOrEmpty(dvUserType1.Style["display"]))
                if (rblCRCustomerType.SelectedIndex == 0 && ddlCRCustomerHierarchyLevel.SelectedIndex >= 1)
                {
                    if (!UserBL.IsUserHierarchyLevelExists(1))
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.HierarchyName = txtUserType1.Text.Trim();
                        objUserHierarchyBE.IncludeVAT = chkUserType1.Checked;
                        objUserHierarchyBE.CreatedBy = objUserBE.UserId;
                        UserBL.InsertUserHierarchy(objUserHierarchyBE);
                    }
                    else
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.UserHierarchyId = 1;
                        objUserHierarchyBE.HierarchyName = txtUserType1.Text.Trim();
                        objUserHierarchyBE.IncludeVAT = chkUserType1.Checked;
                        objUserHierarchyBE.ModifiedBy = objUserBE.UserId;
                        UserBL.UpdateUserHierarchy(objUserHierarchyBE,1,ddlCRCustomerHierarchyLevel.SelectedIndex);
                    }
                }
                else
                {
                    if (UserBL.IsUserHierarchyLevelExists(1))
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.UserHierarchyId = 1;
                        objUserHierarchyBE.ModifiedBy = objUserBE.UserId;
                        UserBL.UpdateUserHierarchy(objUserHierarchyBE, 2, rblCRCustomerType.SelectedIndex == 0 ? ddlCRCustomerHierarchyLevel.SelectedIndex : 0);
                    }
                }

                #endregion

                #region Update Level 2 Hierarchy

                //if (string.IsNullOrEmpty(dvUserType2.Style["display"]))
                if (rblCRCustomerType.SelectedIndex == 0 && ddlCRCustomerHierarchyLevel.SelectedIndex >= 2)
                {
                    if (!UserBL.IsUserHierarchyLevelExists(2))
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.HierarchyName = txtUserType2.Text.Trim();
                        objUserHierarchyBE.IncludeVAT = chkUserType2.Checked;
                        objUserHierarchyBE.CreatedBy = objUserBE.UserId;
                        objUserHierarchyBE.ParentHierarchy = 1;
                        UserBL.InsertUserHierarchy(objUserHierarchyBE);
                    }
                    else
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.UserHierarchyId = 2;
                        objUserHierarchyBE.HierarchyName = txtUserType2.Text.Trim();
                        objUserHierarchyBE.IncludeVAT = chkUserType2.Checked;
                        objUserHierarchyBE.ModifiedBy = objUserBE.UserId;
                        UserBL.UpdateUserHierarchy(objUserHierarchyBE, 1, ddlCRCustomerHierarchyLevel.SelectedIndex);
                    }
                }
                else
                {
                    if (UserBL.IsUserHierarchyLevelExists(2))
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.UserHierarchyId = 2;
                        objUserHierarchyBE.ModifiedBy = objUserBE.UserId;
                        UserBL.UpdateUserHierarchy(objUserHierarchyBE, 2, rblCRCustomerType.SelectedIndex == 0 ? ddlCRCustomerHierarchyLevel.SelectedIndex : 0);
                    }
                }

                #endregion

                #region Update Level 3 Hierarchy

                //if (string.IsNullOrEmpty(dvUserType3.Style["display"]))
                if (rblCRCustomerType.SelectedIndex == 0 && ddlCRCustomerHierarchyLevel.SelectedIndex == 3)
                {
                    if (!UserBL.IsUserHierarchyLevelExists(3))
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.HierarchyName = txtUserType3.Text.Trim();
                        objUserHierarchyBE.IncludeVAT = chkUserType3.Checked;
                        objUserHierarchyBE.CreatedBy = objUserBE.UserId;
                        objUserHierarchyBE.ParentHierarchy = 2;
                        UserBL.InsertUserHierarchy(objUserHierarchyBE);
                    }
                    else
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.UserHierarchyId = 3;
                        objUserHierarchyBE.HierarchyName = txtUserType3.Text.Trim();
                        objUserHierarchyBE.IncludeVAT = chkUserType3.Checked;
                        objUserHierarchyBE.ModifiedBy = objUserBE.UserId;
                        UserBL.UpdateUserHierarchy(objUserHierarchyBE, 1, ddlCRCustomerHierarchyLevel.SelectedIndex);
                    }
                }
                else
                {
                    if (UserBL.IsUserHierarchyLevelExists(3))
                    {
                        UserBE.UserHierarchyBE objUserHierarchyBE = new UserBE.UserHierarchyBE();
                        objUserHierarchyBE.UserHierarchyId = 3;
                        objUserHierarchyBE.ModifiedBy = objUserBE.UserId;
                        UserBL.UpdateUserHierarchy(objUserHierarchyBE, 2, rblCRCustomerType.SelectedIndex == 0 ? ddlCRCustomerHierarchyLevel.SelectedIndex : 0);
                    }
                }

                #endregion

                HttpRuntime.Cache.Remove("StoreDetails");

                GlobalFunctions.ShowModalAlertMessages(this.Page, "Settings has been updated successfully.", AlertType.Success);
                CreateActivityLog("UserTypeSettings cmdSave", "Update", Convert.ToString(intUserId));
                GetStoreFeatures();
                if (rblCRCustomerType.SelectedIndex == 0)
                    dvUserTypesDetails.Style["display"] = "";
                else
                    dvUserTypesDetails.Style["display"] = "none";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        protected void ddlCRCustomerHierarchyLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCRCustomerHierarchyLevel.SelectedIndex > 0)
            {
                dvUserTypesDetails.Style["display"] = "";
                //txtUserType1.Text = string.Empty;
                //txtUserType2.Text = string.Empty;
                //txtUserType3.Text = string.Empty;
                //ddlUserType1CatalogueIds.SelectedIndex = 0;
                //ddlUserType2CatalogueIds.SelectedIndex = 0;
                //ddlUserType3CatalogueIds.SelectedIndex = 0;
                
                switch (Convert.ToInt16(ddlCRCustomerHierarchyLevel.SelectedValue))
                {
                    case 1:
                        dvUserType1.Style["display"] = "";
                        dvUserType2.Style["display"] = "none";
                        dvUserType3.Style["display"] = "none";
                        rfvUserType1.Enabled = true;
                        rfvUserType2.Enabled = false;
                        rfvUserType3.Enabled = false;
                        //cvUserType1CatalogueIds.Enabled = true;
                        //cvUserType2CatalogueIds.Enabled = false;
                        //cvUserType3CatalogueIds.Enabled = false;
                        break;
                    case 2:
                        dvUserType1.Style["display"] = "";
                        dvUserType2.Style["display"] = "";
                        dvUserType3.Style["display"] = "none";
                        rfvUserType1.Enabled = true;
                        rfvUserType2.Enabled = true;
                        rfvUserType3.Enabled = false;
                        //cvUserType1CatalogueIds.Enabled = true;
                        //cvUserType2CatalogueIds.Enabled = true;
                        //cvUserType3CatalogueIds.Enabled = false;
                        break;
                    case 3:
                        dvUserType1.Style["display"] = "";
                        dvUserType2.Style["display"] = "";
                        dvUserType3.Style["display"] = "";
                        rfvUserType1.Enabled = true;
                        rfvUserType2.Enabled = true;
                        rfvUserType3.Enabled = true;
                        //cvUserType1CatalogueIds.Enabled = true;
                        //cvUserType2CatalogueIds.Enabled = true;
                        //cvUserType3CatalogueIds.Enabled = true;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                rfvUserType1.Enabled = false;
                rfvUserType2.Enabled = false;
                rfvUserType3.Enabled = false;
                //cvUserType1CatalogueIds.Enabled = false;
                //cvUserType2CatalogueIds.Enabled = false;
                //cvUserType3CatalogueIds.Enabled = false;
                dvUserTypesDetails.Style["display"] = "none";
                dvUserType1.Style["display"] = "none";
                dvUserType2.Style["display"] = "none";
                dvUserType3.Style["display"] = "none";
            }

            ddlCRCustomerHierarchyLevel.Enabled = true;
        }
    }
}