﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using PWGlobalEcomm.BusinessLogic;

namespace Presentation
{
    public class Admin_Settings_UpdateGA : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptCodes;
        string host = GlobalFunctions.GetVirtualPath();
        static Int16 storeId;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    if (objStoreBE != null && objStoreBE.StoreName != null)
                    {
                        storeId = objStoreBE.StoreId;
                        List<StoreBE.StoreCurrencyBE> objStoreCurrencies = objStoreBE.StoreCurrencies.Where(x => x.IsActive == true).OrderBy(x => x.CurrencyId).Take(1).ToList();
                        // List<StoreBE.StoreCurrencyBE> oStoreBE = objStoreBE.StoreCurrencies.Where(x => x.IsActive == true).OrderBy(x => x.CurrencyId).ToList();
                        if (objStoreCurrencies != null)
                        {
                            rptCodes.DataSource = objStoreCurrencies;
                            rptCodes.DataBind();
                        }
                        else
                        { rptCodes.DataSource = null; rptCodes.DataBind(); }

                    }
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }
        protected void rptCodes_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox txtAccountKey = (TextBox)e.Item.FindControl("txtAccountKey");
                StoreBE.StoreCurrencyBE storeCurrency = (StoreBE.StoreCurrencyBE)e.Item.DataItem;
                txtAccountKey.Text = storeCurrency.GACode;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (RepeaterItem item in rptCodes.Items)
                {
                    TextBox txt = (TextBox)item.FindControl("txtAccountKey");
                    HiddenField hdn = (HiddenField)item.FindControl("hdnCurrencyId");
                    // if (txt.Text == "")  
                    //{ 
                    bool b = StoreBL.UpdateGACode(storeId, Convert.ToInt16(hdn.Value), txt.Text);
                    //}
                }
                GlobalFunctions.ShowModalAlertMessages(this.Page, "GA Code has been updated.", AlertType.Success);
                CreateActivityLog("UpdateGA", "btnSubmit", "");
                HttpRuntime.Cache.Remove("StoreDetails");
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }
    }
}
