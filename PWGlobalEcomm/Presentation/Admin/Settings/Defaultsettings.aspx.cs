﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessEntity;

namespace Presentation
{

    public partial class Admin_Settings_Defaultsettings : BasePage//System.Web.UI.Page
    {

        #region variables

        protected global::System.Web.UI.WebControls.HiddenField hdnStoreId;
        protected global::System.Web.UI.WebControls.Repeater rptCurrencies, rptLanguages;

        Int16 intUserId = 1;
        Int16 StoreId = 0;
        #endregion

        public string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    if (objStoreBE != null)
                    {
                        hdnStoreId.Value = Convert.ToString(objStoreBE.StoreId);
                        StoreId = objStoreBE.StoreId;
                        GetStoreCurrencyDetails();
                        GetStoreLanguageDetails();
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void GetStoreCurrencyDetails()
        {
            try
            {
                List<StoreBE.StoreCurrencyBE> GetStoreCurrencyDetails = StoreCurrencyBL.GetStoreCurrencyDetails();
                rptCurrencies.DataSource = GetStoreCurrencyDetails;
                rptCurrencies.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void rptCurrencies_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    StoreBE.StoreCurrencyBE StoreCurrency = (StoreBE.StoreCurrencyBE)e.Item.DataItem;
                    HiddenField hdnCurrencyId = (HiddenField)e.Item.FindControl("hdnCurrencyId");
                    hdnCurrencyId.Value = Convert.ToString(StoreCurrency.CurrencyId);


                    HtmlInputRadioButton rbDefaultCurrency = (HtmlInputRadioButton)e.Item.FindControl("rbDefaultCurrency");
                    rbDefaultCurrency.Attributes.Add("onClick", "SetUniqueRadioButton('DefaultCurrency',this);");
                    if (StoreCurrency.IsDefault == true)
                    {
                        rbDefaultCurrency.Checked = StoreCurrency.IsDefault;
                    }
                    else
                    {
                        rbDefaultCurrency.Checked = StoreCurrency.IsDefault;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        private void GetStoreLanguageDetails()
        {
            try
            {
                List<StoreBE.StoreLanguageBE> GetStoreLanguages = StoreLanguageBL.GetStoreLanguageDetails();
                rptLanguages.DataSource = GetStoreLanguages;
                rptLanguages.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        protected void rptLanguages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    StoreBE.StoreLanguageBE StoreLanguages = (StoreBE.StoreLanguageBE)e.Item.DataItem;
                    HiddenField hdnlanguageId = (HiddenField)e.Item.FindControl("hdnlanguageId");
                    hdnlanguageId.Value = Convert.ToString(StoreLanguages.LanguageId);
                    CheckBox chkActiveLanguage = (CheckBox)e.Item.FindControl("chkActiveLanguage");
                    HtmlInputRadioButton rbDefaultLanguage = (HtmlInputRadioButton)e.Item.FindControl("rbDefaultLanguage");
                    rbDefaultLanguage.Attributes.Add("onClick", "SetUniqueRadioButton('DefaultLanguage',this);");

                    HtmlInputRadioButton rbIsBasysDefault = (HtmlInputRadioButton)e.Item.FindControl("rbIsBasysDefault");
                    rbIsBasysDefault.Attributes.Add("onClick", "SetUniqueRadioButton('IsDefaultBasys',this);");

                    chkActiveLanguage.Checked = StoreLanguages.IsActive;
                    rbDefaultLanguage.Checked = StoreLanguages.IsDefault;
                    rbIsBasysDefault.Checked = StoreLanguages.IsBASYSDefault;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                UserBE objUserBE = new UserBE();
                objUserBE = Session["StoreUser"] as UserBE;

                intUserId = objUserBE.UserId;
                StoreId = Convert.ToInt16(hdnStoreId.Value);

                bool IsUpdated = false;
                bool isUpdateMcp = false;
                int tempctr = 0;
                int BASYStempctr = 0;

                #region To Check Currency Default Count
                foreach (RepeaterItem item in rptCurrencies.Items)
                {
                    HtmlInputRadioButton rbDefaultCurrency = (HtmlInputRadioButton)item.FindControl("rbDefaultCurrency");
                    if (rbDefaultCurrency.Checked)
                    {
                        tempctr++;
                    }
                }
                if (tempctr > 1)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, only one currency can be default on the store.", AlertType.Warning);
                    return;
                }
                else
                {
                    if (tempctr == 0)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, atleast one currency should be default on the store.", AlertType.Warning);
                        return;
                    }
                    else
                    {
                        tempctr = 0;
                    }
                }
                #endregion

                #region To Check Language Default Count
                foreach (RepeaterItem item in rptLanguages.Items)
                {
                    HtmlInputRadioButton rbDefaultLanguage = (HtmlInputRadioButton)item.FindControl("rbDefaultLanguage");
                    HtmlInputRadioButton rbIsBasysDefault = (HtmlInputRadioButton)item.FindControl("rbIsBasysDefault");
                    CheckBox chkActiveLanguage = (CheckBox)item.FindControl("chkActiveLanguage");
                    if (rbDefaultLanguage.Checked)
                    {
                        if (!chkActiveLanguage.Checked)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, default language should be active on the store.", AlertType.Warning);
                            return;
                        }
                        tempctr++;
                    }

                    if (rbIsBasysDefault.Checked)
                    {
                        if (!chkActiveLanguage.Checked)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, default BASYS should be active on the store.", AlertType.Warning);
                            return;
                        }
                        BASYStempctr++;
                    }

                }
                if (tempctr > 1)
                {
                    //GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException(""), AlertType.Warning);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, only one language can be default on the store.", AlertType.Warning);
                    return;
                }
                else
                {
                    if (tempctr == 0)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, atleast one language should be default on the store.", AlertType.Warning);
                        return;
                    }
                    else
                    {
                        tempctr = 0;
                    }
                }
                if (BASYStempctr > 1)
                {
                    //GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException(""), AlertType.Warning);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, only one BASYS can be default on the store.", AlertType.Warning);
                    return;
                }
                else
                {
                    if (BASYStempctr == 0)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, atleast one BASYS should be default on the store.", AlertType.Warning);
                        return;
                    }
                    else
                    {
                        BASYStempctr = 0;
                    }
                }
                #endregion

                #region To Update Store Currency

                foreach (RepeaterItem item in rptCurrencies.Items)
                {
                    HtmlInputRadioButton rbDefaultCurrency = (HtmlInputRadioButton)item.FindControl("rbDefaultCurrency");
                    HiddenField hdnCurrencyId = (HiddenField)item.FindControl("hdnCurrencyId");

                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("CurrencyId", Convert.ToString(hdnCurrencyId.Value));
                    DictionaryInstance.Add("IsDefault", Convert.ToString(rbDefaultCurrency.Checked));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreCurrencyBL.UpdateDefaultStoreCurrency(DictionaryInstance, true);
                }
                #endregion

                #region Update Store language
                foreach (RepeaterItem item in rptLanguages.Items)
                {
                    HtmlInputRadioButton rbDefaultLanguage = (HtmlInputRadioButton)item.FindControl("rbDefaultLanguage");
                    HtmlInputRadioButton rbIsBASYSDefault = (HtmlInputRadioButton)item.FindControl("rbIsBasysDefault");
                    HiddenField hdnlanguageId = (HiddenField)item.FindControl("hdnlanguageId");
                    CheckBox chkActiveLanguage = (CheckBox)item.FindControl("chkActiveLanguage");

                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    Dictionary<string, string> DictionaryInstanceMCP = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("Languageid", Convert.ToString(hdnlanguageId.Value));
                    DictionaryInstance.Add("IsDefault", Convert.ToString(rbDefaultLanguage.Checked));
                    DictionaryInstance.Add("IsActive", Convert.ToString(chkActiveLanguage.Checked));
                    DictionaryInstance.Add("IsBASYSDefault", Convert.ToString(rbIsBASYSDefault.Checked));
                    DictionaryInstanceMCP.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstanceMCP.Add("Languageid", Convert.ToString(hdnlanguageId.Value));
                    DictionaryInstanceMCP.Add("IsDefault", Convert.ToString(rbDefaultLanguage.Checked));
                    DictionaryInstanceMCP.Add("IsActive", Convert.ToString(chkActiveLanguage.Checked));
                    IsUpdated = StoreLanguageBL.UpdateDefaultStoreLanguageMapping(DictionaryInstance, true);
                    isUpdateMcp = StoreLanguageBL.UpdateDefaultStoreLanguageMappingMCP(DictionaryInstanceMCP, false);
                    CreateActivityLog("DefaultSettings cmdUpdate", "Update", hdnlanguageId.Value);
                }

                if (IsUpdated)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Default settings updated sucessfully.", AlertType.Success);
                    return;
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while updating default settings.", AlertType.Failure);
                    return;
                }
                #endregion

                GetStoreCurrencyDetails();
                GetStoreLanguageDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(hostAdmin + "Dashboard/Dashboard.aspx");
        }

    }
}