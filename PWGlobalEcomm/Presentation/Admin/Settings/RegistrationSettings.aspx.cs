﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Settings_RegistrationSettings : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.DropDownList ddl_FilterType;
        protected global::System.Web.UI.WebControls.FileUpload fuWhiteList, fuBlackList;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvWhiteList, dvBlackList;
        UserBE objUserBE;
        public int UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["StoreUser"] != null)
            {
                objUserBE = Session["StoreUser"] as UserBE;
                UserId = objUserBE.UserId;
            }
        }

        public DataTable CsvFileToDatatable(string path, bool IsFirstRowHeader)
        {
            string header = "No";
            string sql = string.Empty;
            DataTable dataTable = null;
            string pathOnly = string.Empty;
            string fileName = string.Empty;
            try
            {
                pathOnly = Path.GetDirectoryName(path);
                fileName = Path.GetFileName(path);
                sql = @"SELECT * FROM [" + fileName + "]";
                if (IsFirstRowHeader)
                {
                    header = "Yes";
                }
                using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathOnly +
                ";Extended Properties=\"Text;HDR=" + header + "\""))
                {
                    using (OleDbCommand command = new OleDbCommand(sql, connection))
                    {
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
                        {
                            dataTable = new DataTable();
                            dataTable.Locale = CultureInfo.CurrentCulture;
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
            }
            return dataTable;
        }

        public DataTable ConvertToDataTable<T>(List<T> data)
        {

            DataTable table = new DataTable();
            try
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                string type = typeof(T).ToString();
                PropertyInfo property = Type.GetType(type).GetProperty("ProductId", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo[] prope = Type.GetType(type).GetProperties();
                foreach (PropertyDescriptor prop in properties)

                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

                foreach (T item in data)
                {

                    DataRow row = table.NewRow();

                    foreach (PropertyDescriptor prop in properties)

                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;

                    table.Rows.Add(row);

                }
            }
            catch(Exception ex)
            {
                table = null;
                Exceptions.WriteExceptionLog(ex);
            }

            return table;

        }

        protected void ddl_FilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddl_FilterType.SelectedValue == "1")
            {
                dvWhiteList.Visible = true;
                dvBlackList.Visible = false;
            }
            else if(ddl_FilterType.SelectedValue == "2")
            {
                dvBlackList.Visible = true;
                dvWhiteList.Visible = false;
            }
            else
            {
                dvBlackList.Visible = false;
                dvWhiteList.Visible = false;
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select a Registration Filter", AlertType.Warning);
            }
        }

        protected void btnWhiteListSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (fuWhiteList.HasFiles)
                {
                    if (fuWhiteList.PostedFile.ContentLength > 0)
                    {
                        if (GlobalFunctions.IsFileSizeValid(fuWhiteList, Convert.ToInt64(GlobalFunctions.GetSetting("RegistrationFilter_FileSize"))) == false)
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Font file size is not valid", AlertType.Warning);
                            SetFocus(fuWhiteList.ClientID);
                            return;
                        }

                        //Chk for File Extension
                        string[] strFileTypes = { ".csv" };
                        string[] strMimeTypes = { "excel/csv", "application/vnd.ms-excel" };
                        bool bChkFileType = false;
                        bChkFileType = GlobalFunctions.CheckFileExtension(fuWhiteList, strFileTypes);
                        bool bChkFileMimeType = false;
                        bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(fuWhiteList, strMimeTypes);
                        if (!(bChkFileType && bChkFileMimeType))
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
                            SetFocus(fuWhiteList.ClientID);
                            return;
                        }
                        string strFileName = fuWhiteList.FileName;
                        FileInfo fi = new FileInfo(strFileName);
                        string strFilExt = fi.Extension;
                        strFileName = "Whitelist" + strFilExt;
                        string strWhiteListUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "\\CSV\\Registration\\"+strFileName;
                        try
                        {
                            if (File.Exists(strWhiteListUploadPath))
                            {
                                File.Delete(strWhiteListUploadPath);
                            }

                            fuWhiteList.SaveAs(strWhiteListUploadPath);
                            if(File.Exists(strWhiteListUploadPath))
                            {
                                DataTable dtWhiteList = CsvFileToDatatable(strWhiteListUploadPath, true);
                                if(dtWhiteList!=null)
                                {
                                    if(dtWhiteList.Rows.Count>0)
                                    {
                                        DataColumn colCreatedBy = new System.Data.DataColumn("CreatedBy", typeof(Int32));
                                        colCreatedBy.DefaultValue = UserId;
                                        dtWhiteList.Columns.Add(colCreatedBy);
                                    }
                                    bool result = UserBL.InsertRegistrationFilterList(RegistrationFilter.WhiteList, dtWhiteList);
                                    if (result)
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Registration White List Inserted Successfully.", AlertType.Success);
                                        CreateActivityLog("RegistrationSettings btnWhiteListSubmit", "Insert", "");
                                    }
                                    else
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                            return;
                        }

                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Upload File", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
            }
        }

        protected void btnBlackListSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (fuBlackList.HasFiles)
                {
                    if (fuBlackList.PostedFile.ContentLength > 0)
                    {
                        if (GlobalFunctions.IsFileSizeValid(fuBlackList, Convert.ToInt64(GlobalFunctions.GetSetting("RegistrationFilter_FileSize"))) == false)
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Font file size is not valid", AlertType.Warning);
                            SetFocus(fuBlackList.ClientID);
                            return;
                        }

                        //Chk for File Extension
                        string[] strFileTypes = { ".csv" };
                    //    string[] strMimeTypes = { "text/csv", "application/vnd.ms-excel" };
                        string[] strMimeTypes = { "excel/csv", "application/vnd.ms-excel" };
                        
                        bool bChkFileType = false;
                        bChkFileType = GlobalFunctions.CheckFileExtension(fuBlackList, strFileTypes);
                        bool bChkFileMimeType = false;
                        bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(fuBlackList, strMimeTypes);
                        if (!(bChkFileType && bChkFileMimeType))
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
                            SetFocus(fuBlackList.ClientID);
                            return;
                        }
                        string strFileName = fuBlackList.FileName;
                        FileInfo fi = new FileInfo(strFileName);
                        string strFilExt = fi.Extension;
                        strFileName = "Blacklist" + strFilExt;
                        string strBlackListUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "\\CSV\\Registration\\" + strFileName;
                        try
                        {
                            if (File.Exists(strBlackListUploadPath))
                            {
                                File.Delete(strBlackListUploadPath);
                            }

                            fuBlackList.SaveAs(strBlackListUploadPath);
                            if (File.Exists(strBlackListUploadPath))
                            {
                                DataTable dtBlackList = CsvFileToDatatable(strBlackListUploadPath, true);
                                if (dtBlackList != null)
                                {
                                    if (dtBlackList.Rows.Count > 0)
                                    {
                                        DataColumn colCreatedBy = new System.Data.DataColumn("CreatedBy", typeof(Int32));
                                        colCreatedBy.DefaultValue = UserId;
                                        dtBlackList.Columns.Add(colCreatedBy);
                                    }
                                    bool result = UserBL.InsertRegistrationFilterList(RegistrationFilter.BlackList, dtBlackList);
                                    if (result)
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Registration Black List Inserted Successfully.", AlertType.Success);
                                        CreateActivityLog("RegistrationSettings btnBlackListSubmit", "Insert", "");
                                    }
                                    else
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                            return;
                        }

                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Upload File", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
            }
        }

        protected void lnkDownloadWhiteList_Click(object sender, EventArgs e)
        {
            try
            {
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=Whitelist.csv");
                byte[] data = req.DownloadData(Convert.ToString(GlobalFunctions.GetPhysicalFolderPath() + "CSV\\Registration\\Whitelist.csv"));
                response.BinaryWrite(data);
                response.End();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lnkDownloadBlackList_Click(object sender, EventArgs e)
        {
            try
            {
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=Blacklist.csv");
                byte[] data = req.DownloadData(Convert.ToString(GlobalFunctions.GetPhysicalFolderPath() + "CSV\\Registration\\Blacklist.csv"));
                response.BinaryWrite(data);
                response.End();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}