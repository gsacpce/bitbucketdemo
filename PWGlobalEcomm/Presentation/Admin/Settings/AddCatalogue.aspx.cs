﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Settings_AddCatalogue : BasePage//System.Web.UI.Page
    {
        #region
        public string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.Panel pnlBASYSDetails;
        protected global::System.Web.UI.WebControls.Repeater rptCatalogue;
        protected global::System.Web.UI.WebControls.DropDownList ddlCurrency;
        protected global::System.Web.UI.WebControls.DropDownList ddlKeyGroupName;
        protected global::System.Web.UI.HtmlControls.HtmlTable tblNoRecord;
        protected global::System.Web.UI.WebControls.DropDownList ddlDivision, ddlCatalogueName, ddlSourceCodeName, ddlDefaultCustomerName;
        protected global::System.Web.UI.WebControls.Label lblDivisionID, lblDivisionName, lblKeyGroupId, lblCatalogueID, lblSourceCodeId;
        protected global::System.Web.UI.WebControls.TextBox txtCatalogueAliasId, txtDefaultCompanyId, txtDefault_CUST_CONTACT_ID, txtSalesPersonId, txtCustomerId, txtGroupId;
        // txtGlobalCostCentreNumber, ,

        CatalogueBE.Division objDivision = new CatalogueBE.Division();
        Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["StoreUser"] == null)
                {
                    Response.Redirect("~/Login/Login.aspx", true);
                    return;
                }

                HttpRuntime.Cache.Remove("StoreDetails");
                StoreBE objStoreBE = StoreBL.GetStoreDetails();

                if (!IsPostBack)
                {
                    if (objStoreBE.StoreCurrencies.Count > 0)
                    {
                        dictionaryInstance.Add("DIVISION_ID", Convert.ToString(objStoreBE.StoreCurrencies[0].DivisionId));
                    }
                    CatalogueBE objLstCatalogueBE = CatalogueBL.getCollectionItem(Constants.USP_GetAllNewCatalogueDetail, dictionaryInstance, false);
                    if (objLstCatalogueBE.lstDivision.Count > 0)
                    {
                        lblDivisionID.Text = Convert.ToString(objLstCatalogueBE.lstDivision[0].DIVISION_ID);
                        lblDivisionName.Text = Convert.ToString(objLstCatalogueBE.lstDivision[0].DIVISION_CODE);
                    }

                    if (objLstCatalogueBE.lstKey_Groups.Count > 0)
                    {
                        ddlKeyGroupName.DataSource = objLstCatalogueBE.lstKey_Groups;
                        ddlKeyGroupName.DataTextField = "GROUP_REF";
                        ddlKeyGroupName.DataValueField = "GROUP_ID";
                        ddlKeyGroupName.DataBind();
                        GlobalFunctions.AddDropdownItem(ref ddlKeyGroupName);
                    }
                    #region Currency
                    List<CurrencyBE> GetAllCurrencies = CurrencyBL.GetAllStoreCurrencyDetails();
                    List<CurrencyBE> objCur = new List<CurrencyBE>();
                    objCur = (from a in GetAllCurrencies.AsEnumerable()
                              where objLstCatalogueBE.lstCatalogues.Any(xc => xc.CURRENCY_COUNTRY.Contains(a.CurrencyCode))
                              select a).ToList();

                    var aCurrency = (from y in objCur
                                     select new { y.CurrencyCode, y.CurrencyName });

                    ddlCurrency.DataSource = aCurrency.Distinct();
                    ddlCurrency.DataTextField = "CurrencyName";
                    ddlCurrency.DataValueField = "CurrencyCode";
                    ddlCurrency.DataBind();
                    GlobalFunctions.AddDropdownItem(ref ddlCurrency);

                    GlobalFunctions.AddDropdownItem(ref ddlCatalogueName);
                    GlobalFunctions.AddDropdownItem(ref ddlSourceCodeName);
                    GlobalFunctions.AddDropdownItem(ref ddlDefaultCustomerName);
                    #endregion

                    pnlBASYSDetails.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pnlBASYSDetails.Visible = false;
                if (ddlCurrency.SelectedIndex > 0)
                {
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    List<StoreBE.StoreCurrencyBE> objLstStoreCurrencyBE = objStoreBE.StoreCurrencies.FindAll(x => x.CurrencyCode == ddlCurrency.SelectedValue).ToList();

                    if (objLstStoreCurrencyBE.Count > 0)
                    {
                        List<StoreBE.StoreCurrencyBE> objLstStoreCurrencyBE1 = objLstStoreCurrencyBE.FindAll(x => x.IsActive == true).ToList();

                        rptCatalogue.Visible = true;
                        rptCatalogue.DataSource = objLstStoreCurrencyBE1;
                        rptCatalogue.DataBind();
                        tblNoRecord.Visible = false;
                    }
                    else
                    {
                        rptCatalogue.Visible = false;
                        tblNoRecord.Visible = true;
                    }

                    if (objStoreBE.StoreCurrencies.Count > 0)
                    {
                        dictionaryInstance.Add("DIVISION_ID", Convert.ToString(objStoreBE.StoreCurrencies[0].DivisionId));
                    }
                    CatalogueBE objLstCatalogueBE = CatalogueBL.getCollectionItem(Constants.USP_GetAllNewCatalogueDetail, dictionaryInstance, false);
                    if (objLstCatalogueBE.lstCatalogues.Count > 0)
                    {
                        var a = objLstCatalogueBE.lstCatalogues.RemoveAll(x => x.CURRENCY_COUNTRY != ddlCurrency.SelectedValue);
                        ddlCatalogueName.DataSource = objLstCatalogueBE.lstCatalogues;//a;
                        ddlCatalogueName.DataValueField = "CATALOGUE_ID";
                        ddlCatalogueName.DataTextField = "CATALOGUE_REF";
                        ddlCatalogueName.DataBind();
                        GlobalFunctions.AddDropdownItem(ref ddlCatalogueName);
                    }
                }
                else
                {
                    rptCatalogue.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlCurrency.SelectedIndex > 0)
                {
                    pnlBASYSDetails.Visible = true;
                    rptCatalogue.Visible = false;
                    tblNoRecord.Visible = false;
                    CreateActivityLog("AddCatalogue btnAdd", "Add", ddlCurrency.SelectedValue);
                }
                else
                {
                    pnlBASYSDetails.Visible = false;
                    rptCatalogue.Visible = true;
                    tblNoRecord.Visible = false;
                    GlobalFunctions.ShowModalAlertMessages(this, "Please select a currency for adding catalogue. !!", AlertType.Warning);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void OnClickSave(object sender, EventArgs e)
        {
            try
            {
                UserBE lst = Session["StoreUser"] as UserBE;
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                int bRes = InsertCurrencyDetails(lst.UserId, objStoreBE.StoreId);
                int bres1 = InsertCurrencyDetailsMCP(lst.UserId, objStoreBE.StoreId);
                if (bRes == 1 || bres1==1)
                {
                    ClearInput();
                    HttpRuntime.Cache.Remove("StoreDetails");
                    objStoreBE = StoreBL.GetStoreDetails();
                    List<StoreBE.StoreCurrencyBE> objLstStoreCurrencyBE = objStoreBE.StoreCurrencies.FindAll(x => x.CurrencyCode == ddlCurrency.SelectedValue).ToList();
                    if (objLstStoreCurrencyBE.Count > 0)
                    {
                        rptCatalogue.Visible = true;
                        rptCatalogue.DataSource = objLstStoreCurrencyBE;
                        rptCatalogue.DataBind();
                        pnlBASYSDetails.Visible = false;
                        tblNoRecord.Visible = false;
                    }
                    else
                    {
                        rptCatalogue.Visible = false;
                        pnlBASYSDetails.Visible = true;
                        tblNoRecord.Visible = false;
                    }
                    CreateActivityLog("AddCatalogue OnClickSave", "Insert", Convert.ToString(lblDivisionID) + "_" + lblDivisionName.Text + "_" + ddlKeyGroupName.SelectedValue);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "This Catalogue already Exists. !!", AlertType.Warning);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        private void GetAllCurrencies()
        {
            try
            {
                List<CurrencyBE> GetAllCurrencies = CurrencyBL.GetAllStoreCurrencyDetails();
                var aCurrency = (from y in GetAllCurrencies
                                 select new { y.CurrencyCode, y.CurrencyName });
                ddlCurrency.DataSource = aCurrency.Distinct();
                ddlCurrency.DataTextField = "CurrencyName";
                ddlCurrency.DataValueField = "CurrencyCode";
                ddlCurrency.DataBind();
                GlobalFunctions.AddDropdownItem(ref ddlCurrency);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private int InsertCurrencyDetails(int UserId, Int16 intStoreId)
        {
            try
            {
                List<CurrencyBE> getAllCurrencies = CurrencyBL.GetAllCurrencyDetails();
                Dictionary<string, string> currencyDictionaryInstance = new Dictionary<string, string>();
                currencyDictionaryInstance.Add("StoreId", Convert.ToString(intStoreId));
                currencyDictionaryInstance.Add("CurrencyId", Convert.ToString(getAllCurrencies.FirstOrDefault(x => x.CurrencyCode == ddlCurrency.SelectedValue).CurrencyId));

                currencyDictionaryInstance.Add("GlobalCostCentre", ""); // txtGlobalCostCentreNumber.Text.Trim()
                currencyDictionaryInstance.Add("GroupId", txtGroupId.Text.Trim());
                currencyDictionaryInstance.Add("DivisionId", lblDivisionID.Text.Trim());
                currencyDictionaryInstance.Add("DefaultCustomerId", ddlDefaultCustomerName.SelectedValue);
                currencyDictionaryInstance.Add("CatalogueAlias", txtCatalogueAliasId.Text.Trim());
                currencyDictionaryInstance.Add("KeyGroupId", lblKeyGroupId.Text.Trim());

                currencyDictionaryInstance.Add("HelpDeskMailID", "");
                currencyDictionaryInstance.Add("DivisionName", lblDivisionName.Text);
                currencyDictionaryInstance.Add("CatalogueName", ddlCatalogueName.SelectedItem.Text.Trim());
                currencyDictionaryInstance.Add("CatalogueId", ddlCatalogueName.SelectedValue);
                currencyDictionaryInstance.Add("KeyGroupName", ddlKeyGroupName.SelectedItem.Text);
                currencyDictionaryInstance.Add("SourceCodeName", ddlSourceCodeName.SelectedItem.Text);
                currencyDictionaryInstance.Add("SourceCodeId", ddlSourceCodeName.SelectedValue);
                currencyDictionaryInstance.Add("SalesPersonId", txtSalesPersonId.Text.Trim());
                currencyDictionaryInstance.Add("DefaultCompanyId", txtCustomerId.Text.Trim());
                currencyDictionaryInstance.Add("Default_CUST_CONTACT_ID", !string.IsNullOrEmpty(txtDefault_CUST_CONTACT_ID.Text) ? txtDefault_CUST_CONTACT_ID.Text.Trim() : "0");
                currencyDictionaryInstance.Add("IsDefault", "false");
                currencyDictionaryInstance.Add("CreatedBy", Convert.ToString(UserId));
                return StoreBL.InsertStoreCurrencyCatalogue(currencyDictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }

        }
        private int InsertCurrencyDetailsMCP(int UserId, Int16 intStoreId)
        {
            try
            {
                List<CurrencyBE> getAllCurrenciesMCP = CurrencyBL.GetAllCurrencyDetails();
                Dictionary<string, string> currencyDictionaryInstanceMCP = new Dictionary<string, string>();
                currencyDictionaryInstanceMCP.Add("StoreId", Convert.ToString(intStoreId));
                currencyDictionaryInstanceMCP.Add("CurrencyId", Convert.ToString(getAllCurrenciesMCP.FirstOrDefault(x => x.CurrencyCode == ddlCurrency.SelectedValue).CurrencyId));
                currencyDictionaryInstanceMCP.Add("GlobalCostCentre", ""); // txtGlobalCostCentreNumber.Text.Trim()
                currencyDictionaryInstanceMCP.Add("GroupId", txtGroupId.Text.Trim());
                currencyDictionaryInstanceMCP.Add("DivisionId", lblDivisionID.Text.Trim());
                currencyDictionaryInstanceMCP.Add("DefaultCustomerId", ddlDefaultCustomerName.SelectedValue);
                currencyDictionaryInstanceMCP.Add("CatalogueAlias", txtCatalogueAliasId.Text.Trim());
                currencyDictionaryInstanceMCP.Add("KeyGroupId", lblKeyGroupId.Text.Trim());
                currencyDictionaryInstanceMCP.Add("HelpDeskMailID", "");
                currencyDictionaryInstanceMCP.Add("DivisionName", lblDivisionName.Text);
                currencyDictionaryInstanceMCP.Add("CatalogueName", ddlCatalogueName.SelectedItem.Text.Trim());
                currencyDictionaryInstanceMCP.Add("CatalogueId", ddlCatalogueName.SelectedValue);
                currencyDictionaryInstanceMCP.Add("KeyGroupName", ddlKeyGroupName.SelectedItem.Text);
                currencyDictionaryInstanceMCP.Add("SourceCodeName", ddlSourceCodeName.SelectedItem.Text);
                currencyDictionaryInstanceMCP.Add("SourceCodeId", ddlSourceCodeName.SelectedValue);
                currencyDictionaryInstanceMCP.Add("SalesPersonId", txtSalesPersonId.Text.Trim());
                currencyDictionaryInstanceMCP.Add("DefaultCompanyId", txtCustomerId.Text.Trim());
                currencyDictionaryInstanceMCP.Add("Default_CUST_CONTACT_ID", !string.IsNullOrEmpty(txtDefault_CUST_CONTACT_ID.Text) ? txtDefault_CUST_CONTACT_ID.Text.Trim() : "0");
                currencyDictionaryInstanceMCP.Add("IsDefault", "false");
                currencyDictionaryInstanceMCP.Add("CreatedBy", Convert.ToString(UserId));
                return StoreBL.InsertStoreCurrencyCatalogueMCP(currencyDictionaryInstanceMCP, false);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }

        }
        private void ClearInput()
        {
            try
            {
                ddlCatalogueName.SelectedValue = "0";
                //txtGlobalCostCentreNumber.Text = "";
                txtGroupId.Text = "8";//sripal
                txtCatalogueAliasId.Text = "";
                lblKeyGroupId.Text = "";
                lblSourceCodeId.Text = "";
                lblCatalogueID.Text = "";
                ddlKeyGroupName.SelectedValue = "0";
                ddlSourceCodeName.SelectedValue = "0";
                ddlDefaultCustomerName.SelectedValue = "0";
                //txtDefaultCompanyId.Text = "";
                txtCustomerId.Text = "";
                txtSalesPersonId.Text = "";
                //txtDefaultCompanyId.Text = "";
                txtDefault_CUST_CONTACT_ID.Text = "";
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindDivision()
        {
            try
            {
                ddlDivision.DataSource = "";
                ddlDivision.DataTextField = "";
                ddlDivision.DataValueField = "";
                ddlDivision.DataBind();
                GlobalFunctions.AddDropdownItem(ref ddlDivision);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void ddlKeyGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblKeyGroupId.Text = ddlKeyGroupName.SelectedValue;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ddlCatalogueName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblCatalogueID.Text = ddlCatalogueName.SelectedValue;
                CatalogueBE.Source_Codes objSource_Codes = new CatalogueBE.Source_Codes();
                objSource_Codes.CATALOGUE_ID = Convert.ToInt32(ddlCatalogueName.SelectedValue);
                ddlSourceCodeName.DataSource = CatalogueBL.GetSource_CodesByCatId(objSource_Codes);
                ddlSourceCodeName.DataValueField = "SOURCE_CODE_ID";
                ddlSourceCodeName.DataTextField = "SOURCE_CODE";
                ddlSourceCodeName.DataBind();
                GlobalFunctions.AddDropdownItem(ref ddlSourceCodeName);

                #region Code Added By SHRIGANESH SINGH 30 June 2016 to reset all the below fields to their default state
                if (ddlCatalogueName.SelectedValue == "0")
                {
                    lblCatalogueID.Text = "";
                    ddlSourceCodeName.SelectedValue = "0";
                    lblSourceCodeId.Text = "";
                    ddlDefaultCustomerName.SelectedValue = "0";
                    txtCustomerId.Text = "";
                    txtSalesPersonId.Text = "";
                    //txtDefaultCompanyId.Text = "";
                    txtDefault_CUST_CONTACT_ID.Text = "";
                }
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ddlSourceCodeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblSourceCodeId.Text = ddlSourceCodeName.SelectedValue;
                CatalogueBE.Web_Search_Accounts obj = new CatalogueBE.Web_Search_Accounts();
                obj.SOURCE_CODE_ID = Convert.ToInt32(ddlSourceCodeName.SelectedValue);
                List<CatalogueBE.Web_Search_Accounts> objList = CatalogueBL.GetWeb_Search_AccountsBySourceId(obj);

                if (objList != null)
                {
                    objList.RemoveAll(x => x.SOURCE_CODE_ID != Convert.ToInt32(ddlSourceCodeName.SelectedValue) && x.CURRENCY_COUNTRY != ddlCurrency.SelectedValue);
                }
                ddlDefaultCustomerName.DataSource = objList;
                ddlDefaultCustomerName.DataTextField = "Name";
                ddlDefaultCustomerName.DataValueField = "CUSTOMER_ID";
                ddlDefaultCustomerName.DataBind();
                GlobalFunctions.AddDropdownItem(ref ddlDefaultCustomerName);



                //ddlSalesPersonID.DataSource = objList;
                //ddlSalesPersonID.DataTextField = "SALESPERSON_ID";
                //ddlSalesPersonID.DataValueField = "SALESPERSON_ID";
                //ddlSalesPersonID.DataBind();
                //GlobalFunctions.AddDropdownItem(ref ddlSalesPersonID);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ddlDefaultCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CatalogueBE.Web_Search_Accounts obj = new CatalogueBE.Web_Search_Accounts();
                obj.CUSTOMER_ID = Convert.ToInt32(ddlDefaultCustomerName.SelectedValue);
                List<CatalogueBE.Web_Search_Accounts> objList = CatalogueBL.GetWeb_SearchDatabyCustomerId(obj);
                objList.RemoveAll(x => x.CUSTOMER_ID != Convert.ToInt32(ddlDefaultCustomerName.SelectedValue) && x.CURRENCY_COUNTRY != ddlCurrency.SelectedValue);

                txtCustomerId.Text = Convert.ToString(objList[0].CUSTOMER_ID);
                txtSalesPersonId.Text = Convert.ToString(objList[0].SALESPERSON_ID);
                txtDefault_CUST_CONTACT_ID.Text = Convert.ToString(objList[0].Customer_Contact_ID);

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        //private void AddItem(ref DropDownList ddl)
        //{
        //    try
        //    {
        //        ddl.Items.Insert(0, new ListItem("Please select", "0"));
        //    }
        //    catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        //}
    }
}