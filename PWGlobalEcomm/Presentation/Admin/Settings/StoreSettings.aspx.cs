﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;

namespace Presentation
{
    public class Admin_Settings_StoreSettings : BasePage //System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Repeater rptPLDisplayType, rptPLSorting, rptPLFilters, rptSCPaymentGateway, rptPLCurrencyPoint, rptProjectedSale;

        protected global::System.Web.UI.WebControls.RadioButtonList rblProductPagination, rblCREnableDomainValidation, rblCREnableEmailValidation, rblCRStoreAccess, rdoLinkPosition,
            rblCRStoreType, rdlIsGiftCertificateAllow, rdPwdType, rdPasswordPolicyType, rdoSearchSetting, rdoCustomPunchout, rblCategoryDisplay, rblBasketPopup, rdlAllowFreightSrcCountry;

        protected global::System.Web.UI.WebControls.TextBox txtProductsPerPage, txtPSMinimumPasswordLength, txtPSPasswordValidity, txtB2CVatPercentage,
            txtAllowBackOrder, txtPoints, txtCLCookieLifetime, txtCLCookieMessage, txtAllowMaxOrder, txtStockDueDate, txtOrderHistoryMonthstoReturn, txtPointValue, txtInvoiceID, txtProjected, txtBirthdayPoint, txtAnnvPoint, txtCommunigatorGroupId;

        protected global::System.Web.UI.WebControls.CheckBox chkQuickView, chkEnableFilter, chkEnablePriceFilter, chkEnableCategoryFilter, chkEnableColorFilter,
            chkEnableSectionFilter, chkProductComparison, chkPDPrint, chkPDEmail, chkPDDownloadImage, chkPDSocialMedia, chkPDRecentlyViewedProducts,
            chkPDYouMayAlsoLike, chkPDWishlist, chkPDReviewRating, chkPDSavePDF, chkPDSectionIcons, chkPCustomRequestForm, chkEnablePoints,
            chkEnableCouponCode, chkStoreType, chkPSForcePasswordChange, chkCREnableCaptcha, chkCREnableDomainValidation, chkPunchout, chkCustomPunchout, chkMaxAllowQty,
            chkCREnableEmailValidation, chkAllowBackOrder, chkCLEnableCookie, ChkInvoiceAccountStatus, ChkEmailCopyBasketBtn,
            chkSocial, chkLogin, chkBasketSocial, chkBasketLogin, chkRegisterSocial, chkRegisterLogin, chkIsPunchoutRegister, ChkProductEnquiryColor, ChkSubmitEnquiry, ChkCOnfirmationEmail,
            chkHideCurrDD, ChkHidePO_OrderDetails, ChkEnableTotalNet, chkCustomUDFSRegData;
        //chkPSPasswordExpression;

        protected global::System.Web.UI.WebControls.DropDownList ddlCountries;
        protected global::System.Web.UI.WebControls.CheckBoxList CheckBoxListCurrency;
        protected global::System.Web.UI.WebControls.HiddenField hdnPLQuickViewStoreFeatureDetailId, hdnStoreId, hdnPLEnableFilterStoreFeatureDetailId,
            hdnPLEnablePriceFilterStoreFeatureDetailId, hdnMaxOrderQtyStoreFeatureDetailId, hdnPLEnableCategoryFilterStoreFeatureDetailId, hdnPLEnableColorFilterStoreFeatureDetailId,
            hdnPLEnableSectionFilterStoreFeatureDetailId, hdnPLProductComparisonStoreFeatureDetailId, hdnPDPrintStoreFeatureDetailId,
            hdnPDEmailStoreFeatureDetailId, hdnPDDownloadImageStoreFeatureDetailId, hdnPDSocialMediaStoreFeatureDetailId,
            hdnPDRecentlyViewedProductsStoreFeatureDetailId, hdnPDYouMayAlsoLikeStoreFeatureDetailId, hdnPDWishlistStoreFeatureDetailId,
            hdnPDReviewRatingStoreFeatureDetailId, hdnPDSavePDFStoreFeatureDetailId, hdnPDSectionIconsStoreFeatureDetailId, hdnPCustomRequestForm,
            hdnEnablePoints, hdnEnableCouponCode, hdnStoreType, hdnPSMinimumPasswordLengthStoreFeatureDetailId, hdnPSPasswordExpressionStoreFeatureDetailId,
            hdnPSForcePasswordChangeStoreFeatureDetailId, hdnPSPasswordValidityStoreFeatureDetailId, hdnCREnableCaptchaStoreFeatureDetailId,
             hdnODAllowBackOrderStoreFeatureDetailId, hdnCLCookieLifetimeStoreFeatureDetailId, hdnCookieMessageStoreFeatureDetailId, hdnCLEnableCookieStoreFeatureDetailId, hdnInvoiceAccountStatus, hdnEmailCopyBasketBtn, hdnCustomPunchout,
             hdnSocialId, hdnLoginId, hdnBasketSocialLogin, hdnBasketLogin, hdnRegisterSocialLogin, hdnRegisterLogin, hdnIsPunchoutReg, hdnStockDueDate, hdnOrderHistoryMonthstoReturn, hdnProductEnquiryColor, hdnSubmitEnquiry, hdnProjCurrId, hdnBirthday, hdnAnniversary, hdnCOemail, hdnHideCurrDD, hdnCommunigatorGroupId
             , hdnHidePO_OrderDetails, hdnEnableTotalNet, hdnCustomUDFSRegData;

        protected global::System.Web.UI.WebControls.Literal lblPLDisplayTypeHeading, lblPaginationHeading, lblPLSortingHeading, lblPLFilterHeading,
            lblCRStoreAccessHeading, lblCRStoreTypeHeading, ltGiftCertificate, lblSCPaymentGatewayHeading, ltrHomeLink, ltrSelectedCurrency, ltrSearchSetting, lblCategoryDisplay, lblBasketPopup, ltrPLPointValue, ltrPLInvoiceID, ltrlAllowFreightSrcCountry;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMessage, dvCREnableDomainValidation, dvCREnableEmailValidation, dvPoints,
            divVatPercentage, liPaymentGatewayHeading, days;

        protected global::System.Web.UI.WebControls.Label lblPSMinimumPasswordLength, lblPSPasswordExpression, lblPSForcePasswordChange, lblPSPasswordValidity,
            lblPSPasswordPolicy, lblCLCookieLifetime, lblCLCookieMessage, lblCLEnableCookie, lblStockDueDate, lblOrderHistoryMonthstoReturn, lblCurrencySymbol,
            lblPointValueHeader, lblCurrencyHeader, lblInvoiceIDHeader, lblProjCurrSymbol, lblCommunigatorGroupId;
        //protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvB2CVatPercentage;
        protected global::System.Web.UI.WebControls.CustomValidator cvSCPaymentGateway, cvPointData;

        public string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        Int16 intUserId = 1;
        Int16 StoreId = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    GetStoreFeatures();
                }
                Page.Title = "Update Store Settings";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        //protected void chkEnablePoints_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkEnablePoints.Checked == true)
        //    {
        //        rblCRStoreAccess.SelectedIndex = 0;
        //        rblCRStoreAccess.Items[1].Enabled = false;
        //        rblCRStoreAccess.Items[0].Enabled = true;
        //    }
        //    else
        //    {
        //        rblCRStoreAccess.SelectedIndex = 1;
        //        rblCRStoreAccess.Items[0].Enabled = true;
        //        rblCRStoreAccess.Items[1].Enabled = true;
        //    }
        //}


        private void GetStoreFeatures()
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                hdnStoreId.Value = Convert.ToString(objStoreBE.StoreId);
                StoreId = objStoreBE.StoreId;
                List<FeatureBE> GetAllFeatures = FeatureBL.GetAllFeatures(StoreId, true);

                #region Get Allow Punchout - Added By Snehal 22 09 2016
                chkPunchout.Checked = objStoreBE.IsPunchOut;
                #endregion

                #region Get Display Type

                FeatureBE PL_DisplaytypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_displaytype");
                if (PL_DisplaytypeFeature != null)
                {
                    lblPLDisplayTypeHeading.Text = PL_DisplaytypeFeature.FeatureAlias;
                    rptPLDisplayType.DataSource = PL_DisplaytypeFeature.FeatureValues;
                    rptPLDisplayType.DataBind();
                }

                #endregion

                #region Get Pagination

                FeatureBE PL_PaginationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_pagination");
                if (PL_PaginationFeature != null)
                {
                    lblPaginationHeading.Text = PL_PaginationFeature.FeatureAlias;
                    rblProductPagination.DataSource = PL_PaginationFeature.FeatureValues;
                    rblProductPagination.DataTextField = "FeatureValue";
                    rblProductPagination.DataValueField = "StoreFeatureDetailId";
                    rblProductPagination.DataBind();
                    if (PL_PaginationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rblProductPagination.SelectedValue = Convert.ToString(PL_PaginationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    FeatureBE.FeatureValueBE FeatureValueBEInstance = PL_PaginationFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "pagination");
                    if (FeatureValueBEInstance.IsEnabled)
                    {
                        txtProductsPerPage.Enabled = true;
                        txtProductsPerPage.Text = FeatureValueBEInstance.FeatureDefaultValue;
                    }
                    else
                        txtProductsPerPage.Enabled = false;

                }

                #endregion

                #region Get Sorting

                FeatureBE PL_SortingFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_sorting");
                if (PL_SortingFeature != null)
                {
                    lblPLSortingHeading.Text = PL_SortingFeature.FeatureAlias;
                    rptPLSorting.DataSource = PL_SortingFeature.FeatureValues;
                    rptPLSorting.DataBind();
                }

                #endregion

                #region Get QuickView

                FeatureBE PL_QuickviewFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_quickview");
                if (PL_QuickviewFeature != null)
                {
                    chkQuickView.Text = PL_QuickviewFeature.FeatureAlias;
                    chkQuickView.Checked = PL_QuickviewFeature.FeatureValues[0].IsEnabled;
                    hdnPLQuickViewStoreFeatureDetailId.Value = Convert.ToString(PL_QuickviewFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Product Detail Print

                FeatureBE PD_EnablePrintFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableprint");
                if (PD_EnablePrintFeature != null)
                {
                    chkPDPrint.Text = PD_EnablePrintFeature.FeatureAlias;
                    chkPDPrint.Checked = PD_EnablePrintFeature.FeatureValues[0].IsEnabled;
                    hdnPDPrintStoreFeatureDetailId.Value = Convert.ToString(PD_EnablePrintFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Google API Login
                #region Get Login Page

                FeatureBE SS_LoginPanel = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_loginpanel");
                if (SS_LoginPanel != null)
                {
                    chkSocial.Checked = SS_LoginPanel.FeatureValues[0].IsEnabled;
                    chkLogin.Checked = SS_LoginPanel.FeatureValues[1].IsEnabled;
                    hdnSocialId.Value = Convert.ToString(SS_LoginPanel.FeatureValues[0].StoreFeatureDetailId);
                    hdnLoginId.Value = Convert.ToString(SS_LoginPanel.FeatureValues[1].StoreFeatureDetailId);

                }
                #endregion

                #region Get Basket Login Page

                FeatureBE SS_BasketLoginPanel = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_basketloginpanel");
                if (SS_BasketLoginPanel != null)
                {
                    chkBasketSocial.Checked = SS_BasketLoginPanel.FeatureValues[0].IsEnabled;
                    chkBasketLogin.Checked = SS_BasketLoginPanel.FeatureValues[1].IsEnabled;
                    hdnBasketSocialLogin.Value = Convert.ToString(SS_BasketLoginPanel.FeatureValues[0].StoreFeatureDetailId);
                    hdnBasketLogin.Value = Convert.ToString(SS_BasketLoginPanel.FeatureValues[1].StoreFeatureDetailId);

                }
                #endregion

                #region Get Register Login Page

                FeatureBE SS_RegisterLoginPanel = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_registerloginpanel");
                if (SS_RegisterLoginPanel != null)
                {
                    chkRegisterSocial.Checked = SS_RegisterLoginPanel.FeatureValues[0].IsEnabled;
                    chkRegisterLogin.Checked = SS_RegisterLoginPanel.FeatureValues[1].IsEnabled;
                    hdnRegisterSocialLogin.Value = Convert.ToString(SS_RegisterLoginPanel.FeatureValues[0].StoreFeatureDetailId);
                    hdnRegisterLogin.Value = Convert.ToString(SS_RegisterLoginPanel.FeatureValues[1].StoreFeatureDetailId);

                }
                #endregion

                #endregion

                #region Get Product Detail Email

                FeatureBE PD_EnableEmailFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableemail");
                if (PD_EnableEmailFeature != null)
                {
                    chkPDEmail.Text = PD_EnableEmailFeature.FeatureAlias;
                    chkPDEmail.Checked = PD_EnableEmailFeature.FeatureValues[0].IsEnabled;
                    hdnPDEmailStoreFeatureDetailId.Value = Convert.ToString(PD_EnableEmailFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Product Detail download image

                FeatureBE PD_EnableDownloadImageFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enabledownloadimage");
                if (PD_EnableDownloadImageFeature != null)
                {
                    chkPDDownloadImage.Text = PD_EnableDownloadImageFeature.FeatureAlias;
                    chkPDDownloadImage.Checked = PD_EnableDownloadImageFeature.FeatureValues[0].IsEnabled;
                    hdnPDDownloadImageStoreFeatureDetailId.Value = Convert.ToString(PD_EnableDownloadImageFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Product Detail social media

                FeatureBE PD_EnableSocialMediaFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesocialmedia");
                if (PD_EnableSocialMediaFeature != null)
                {
                    chkPDSocialMedia.Text = PD_EnableSocialMediaFeature.FeatureAlias;
                    chkPDSocialMedia.Checked = PD_EnableSocialMediaFeature.FeatureValues[0].IsEnabled;
                    hdnPDSocialMediaStoreFeatureDetailId.Value = Convert.ToString(PD_EnableSocialMediaFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Product Detail recently viewed products

                FeatureBE PD_EnableRecentlyViewedProductsFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablerecentlyviewedproducts");
                if (PD_EnableRecentlyViewedProductsFeature != null)
                {
                    chkPDRecentlyViewedProducts.Text = PD_EnableRecentlyViewedProductsFeature.FeatureAlias;
                    chkPDRecentlyViewedProducts.Checked = PD_EnableRecentlyViewedProductsFeature.FeatureValues[0].IsEnabled;
                    hdnPDRecentlyViewedProductsStoreFeatureDetailId.Value = Convert.ToString(PD_EnableRecentlyViewedProductsFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Product Detail you may also like

                FeatureBE PD_EnableYouMayAlsoLikeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableyoumayalsolike");
                if (PD_EnableYouMayAlsoLikeFeature != null)
                {
                    chkPDYouMayAlsoLike.Text = PD_EnableYouMayAlsoLikeFeature.FeatureAlias;
                    chkPDYouMayAlsoLike.Checked = PD_EnableYouMayAlsoLikeFeature.FeatureValues[0].IsEnabled;
                    hdnPDYouMayAlsoLikeStoreFeatureDetailId.Value = Convert.ToString(PD_EnableYouMayAlsoLikeFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Product Detail wishlist

                FeatureBE PD_EnableWishlistFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablewishlist");
                if (PD_EnableWishlistFeature != null)
                {
                    chkPDWishlist.Text = PD_EnableWishlistFeature.FeatureAlias;
                    chkPDWishlist.Checked = PD_EnableWishlistFeature.FeatureValues[0].IsEnabled;
                    hdnPDWishlistStoreFeatureDetailId.Value = Convert.ToString(PD_EnableWishlistFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Product Detail review rating

                FeatureBE PD_EnableReviewRatingFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablereviewrating");
                if (PD_EnableReviewRatingFeature != null)
                {
                    chkPDReviewRating.Text = PD_EnableReviewRatingFeature.FeatureAlias;
                    chkPDReviewRating.Checked = PD_EnableReviewRatingFeature.FeatureValues[0].IsEnabled;
                    hdnPDReviewRatingStoreFeatureDetailId.Value = Convert.ToString(PD_EnableReviewRatingFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Added By ravi gohil for Hide Order Details for Punchout Orders
                FeatureBE SS_HideOrderDetailsPunchout = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_hideorderdetailspunchout");
                if (SS_HideOrderDetailsPunchout != null)
                {
                    ChkHidePO_OrderDetails.Text = SS_HideOrderDetailsPunchout.FeatureAlias;
                    ChkHidePO_OrderDetails.Checked = SS_HideOrderDetailsPunchout.FeatureValues[0].IsEnabled;
                    hdnHidePO_OrderDetails.Value = Convert.ToString(SS_HideOrderDetailsPunchout.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region Get Product Detail save pdf

                FeatureBE PD_EnableSavePDFFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesavepdf");
                if (PD_EnableSavePDFFeature != null)
                {
                    chkPDSavePDF.Text = PD_EnableSavePDFFeature.FeatureAlias;
                    chkPDSavePDF.Checked = PD_EnableSavePDFFeature.FeatureValues[0].IsEnabled;
                    hdnPDSavePDFStoreFeatureDetailId.Value = Convert.ToString(PD_EnableSavePDFFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Product Detail Section Icons

                FeatureBE PD_EnableSectionIconsFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enablesectionicons");
                if (PD_EnableSectionIconsFeature != null)
                {
                    chkPDSectionIcons.Text = PD_EnableSectionIconsFeature.FeatureAlias;
                    chkPDSectionIcons.Checked = PD_EnableSectionIconsFeature.FeatureValues[0].IsEnabled;
                    hdnPDSectionIconsStoreFeatureDetailId.Value = Convert.ToString(PD_EnableSectionIconsFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region For displaying total net on payment page
                FeatureBE SS_EnableTotalNet = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_enabletotalnet");
                if (SS_EnableTotalNet != null)
                {
                    ChkEnableTotalNet.Text = SS_EnableTotalNet.FeatureAlias;
                    ChkEnableTotalNet.Checked = SS_EnableTotalNet.FeatureValues[0].IsEnabled;
                    hdnEnableTotalNet.Value = Convert.ToString(SS_EnableTotalNet.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region Reg Data for Custom UDFS DANSKE
                FeatureBE SS_CustomUDFSRegData = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_customudfsregdata");
                if (SS_CustomUDFSRegData != null)
                {
                    chkCustomUDFSRegData.Text = SS_CustomUDFSRegData.FeatureAlias;
                    chkCustomUDFSRegData.Checked = SS_CustomUDFSRegData.FeatureValues[0].IsEnabled;
                    hdnCustomUDFSRegData.Value = Convert.ToString(SS_CustomUDFSRegData.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region Get Product Custom Request form

                FeatureBE P_CustomRequestFormFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "p_customrequestform");
                if (P_CustomRequestFormFeature != null)
                {
                    chkPCustomRequestForm.Text = P_CustomRequestFormFeature.FeatureAlias;
                    chkPCustomRequestForm.Checked = P_CustomRequestFormFeature.FeatureValues[0].IsEnabled;
                    hdnPCustomRequestForm.Value = Convert.ToString(P_CustomRequestFormFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get EnableFilter

                FeatureBE PL_EnableFilterFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_filter");
                if (PL_EnableFilterFeature != null)
                {
                    lblPLFilterHeading.Text = PL_EnableFilterFeature.FeatureAlias;

                    //if (PL_EnableFilterFeature.FeatureValues.Count == PL_EnableFilterFeature.FeatureValues.FindAll(x => x.IsEnabled == true).Count)
                    //    chkEnableFilter.Checked = true;
                    //else
                    //    chkEnableFilter.Checked = false;

                    rptPLFilters.DataSource = PL_EnableFilterFeature.FeatureValues;
                    rptPLFilters.DataBind();
                }

                #endregion

                #region Get Compare Products

                FeatureBE PL_ProductComparisonFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pl_compareproducts");
                if (PL_ProductComparisonFeature != null)
                {
                    chkProductComparison.Text = PL_ProductComparisonFeature.FeatureAlias;
                    chkProductComparison.Checked = PL_ProductComparisonFeature.FeatureValues[0].IsEnabled;
                    hdnPLProductComparisonStoreFeatureDetailId.Value = Convert.ToString(PL_ProductComparisonFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Enable Points

                FeatureBE SS_EnablePoints = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_enablepoints");
                if (SS_EnablePoints != null)
                {
                    chkEnablePoints.Text = SS_EnablePoints.FeatureAlias;
                    chkEnablePoints.Checked = SS_EnablePoints.FeatureValues[0].IsEnabled;
                    hdnEnablePoints.Value = Convert.ToString(SS_EnablePoints.FeatureValues[0].StoreFeatureDetailId);
                    txtPoints.Text = Convert.ToString(SS_EnablePoints.FeatureValues[0].FeatureDefaultValue);

                    StoreBE objStores = StoreBL.GetStoreDetails();
                    if (objStores != null)
                    {
                        ltrSelectedCurrency.Text = objStores.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencySymbol;
                    }

                    if (chkEnablePoints.Checked)
                    {
                        // dvPoints.Style["display"] = "";
                        // liPaymentGatewayHeading.Style["display"] = "none";
                        cvSCPaymentGateway.Enabled = false;
                        cvPointData.Enabled = true;
                        rblCRStoreAccess.SelectedIndex = 0;
                    }
                    else
                    {
                        // dvPoints.Style["display"] = "none";
                        // liPaymentGatewayHeading.Style["display"] = "";
                        cvSCPaymentGateway.Enabled = true;
                        cvPointData.Enabled = false;
                        rblCRStoreAccess.SelectedIndex = 1;

                    }
                }

                #endregion

                #region Get Home Link Added By Snehal

                FeatureBE SS_HomeLink = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "SS_HomeLink");
                if (SS_HomeLink != null)
                {
                    ltrHomeLink.Text = SS_HomeLink.FeatureAlias;
                    rdoLinkPosition.DataSource = SS_HomeLink.FeatureValues;
                    rdoLinkPosition.DataTextField = "FeatureValue";
                    rdoLinkPosition.DataValueField = "StoreFeatureDetailId";
                    rdoLinkPosition.DataBind();
                    if (SS_HomeLink.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rdoLinkPosition.SelectedValue = Convert.ToString(SS_HomeLink.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    else
                        rdoLinkPosition.SelectedValue = Convert.ToString(SS_HomeLink.FeatureValues[1].StoreFeatureDetailId);
                }

                #endregion

                #region Added by Ravi gohil For communigatorid
                FeatureBE CommunigatorGroupId = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "communigatorgroupid");
                if (CommunigatorGroupId != null)
                {
                    lblCommunigatorGroupId.Text = CommunigatorGroupId.FeatureAlias;
                    txtCommunigatorGroupId.Text = CommunigatorGroupId.FeatureValues[0].FeatureDefaultValue;
                    hdnCommunigatorGroupId.Value = Convert.ToString(CommunigatorGroupId.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region Get Search Setting Added By Snehal 15 Sep 2016

                FeatureBE SS_SearchSetting = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "SS_SearchSetting");
                if (SS_SearchSetting != null)
                {
                    ltrSearchSetting.Text = SS_SearchSetting.FeatureAlias;
                    rdoSearchSetting.DataSource = SS_SearchSetting.FeatureValues;
                    rdoSearchSetting.DataTextField = "FeatureValue";
                    rdoSearchSetting.DataValueField = "StoreFeatureDetailId";
                    rdoSearchSetting.DataBind();
                    if (SS_SearchSetting.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rdoSearchSetting.SelectedValue = Convert.ToString(SS_SearchSetting.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    else
                        rdoSearchSetting.SelectedValue = Convert.ToString(SS_SearchSetting.FeatureValues[1].StoreFeatureDetailId);
                }

                #endregion

                #region Get Payment Gateway

                FeatureBE SC_PaymentGatewayFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_paymentgateway");

                if (SC_PaymentGatewayFeature != null)
                {
                    lblSCPaymentGatewayHeading.Text = SC_PaymentGatewayFeature.FeatureAlias;
                    rptSCPaymentGateway.DataSource = SC_PaymentGatewayFeature.FeatureValues;
                    rptSCPaymentGateway.DataBind();
                }

                #endregion

                #region Get Enable Coupon Code

                FeatureBE SS_EnableCouponCode = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_enablecouponcode");
                if (SS_EnableCouponCode != null)
                {
                    chkEnableCouponCode.Text = SS_EnableCouponCode.FeatureAlias;
                    chkEnableCouponCode.Checked = SS_EnableCouponCode.FeatureValues[0].IsEnabled;
                    hdnEnableCouponCode.Value = Convert.ToString(SS_EnableCouponCode.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get IsPunchout Registration - Added By Snehal 20 12 2016

                FeatureBE SS_IsPunchoutRegistration = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_ispunchoutregistration");
                if (SS_IsPunchoutRegistration != null)
                {
                    //chkIsPunchoutRegister.Text = SS_IsPunchoutRegistration.FeatureAlias;
                    chkIsPunchoutRegister.Checked = SS_IsPunchoutRegistration.FeatureValues[0].IsEnabled;
                    hdnIsPunchoutReg.Value = Convert.ToString(SS_IsPunchoutRegistration.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Password Length

                FeatureBE PS_MinimumPasswordLengthFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength");
                if (PS_MinimumPasswordLengthFeature != null)
                {
                    lblPSMinimumPasswordLength.Text = PS_MinimumPasswordLengthFeature.FeatureAlias;
                    txtPSMinimumPasswordLength.Text = PS_MinimumPasswordLengthFeature.FeatureValues[0].FeatureDefaultValue;
                    hdnPSMinimumPasswordLengthStoreFeatureDetailId.Value = Convert.ToString(PS_MinimumPasswordLengthFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Allow Back Order

                FeatureBE OD_AllowBackOrderFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "od_allowbackorder");
                if (OD_AllowBackOrderFeature != null)
                {
                    chkAllowBackOrder.Text = OD_AllowBackOrderFeature.FeatureAlias;
                    chkAllowBackOrder.Checked = OD_AllowBackOrderFeature.FeatureValues[0].IsEnabled;
                    hdnODAllowBackOrderStoreFeatureDetailId.Value = Convert.ToString(OD_AllowBackOrderFeature.FeatureValues[0].StoreFeatureDetailId);

                    if (OD_AllowBackOrderFeature.FeatureValues[0].IsEnabled)
                    {
                        txtAllowBackOrder.Enabled = true;
                        txtAllowBackOrder.Text = OD_AllowBackOrderFeature.FeatureValues[0].FeatureDefaultValue;
                    }
                    else
                        txtAllowBackOrder.Enabled = false;

                }

                #endregion

                #region UDFS in Order confirmation email

                FeatureBE OC_UDFEmail = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "oc_udfemail");

                if (OC_UDFEmail != null)
                {
                    ChkCOnfirmationEmail.Text = OC_UDFEmail.FeatureAlias;
                    ChkCOnfirmationEmail.Checked = OC_UDFEmail.FeatureValues[0].IsEnabled;
                    hdnCOemail.Value = Convert.ToString(OC_UDFEmail.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Max Order Qty per Item - Added By Snehal 23 09 2016
                FeatureBE OD_AllowMaxOrderQtyPerItemFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.Trim() == "SS_MaxAllowQty");
                if (OD_AllowMaxOrderQtyPerItemFeature != null)
                {
                    chkMaxAllowQty.Text = OD_AllowMaxOrderQtyPerItemFeature.FeatureAlias;
                    chkMaxAllowQty.Checked = OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].IsEnabled;
                    hdnMaxOrderQtyStoreFeatureDetailId.Value = Convert.ToString(OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].StoreFeatureDetailId);
                    if (OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].IsEnabled)
                    {
                        txtAllowMaxOrder.Enabled = true;
                        txtAllowMaxOrder.Text = OD_AllowMaxOrderQtyPerItemFeature.FeatureValues[0].FeatureDefaultValue;
                    }
                    else
                        txtAllowMaxOrder.Enabled = false;
                }
                #endregion

                #region Get Password Expression Commented by SHRIGANESH SINGH for Password Policy 18 May 2016
                //try
                //{
                //    FeatureBE PS_PasswordExpressionFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordexpression");
                //    if (PS_PasswordExpressionFeature != null)
                //    {
                //        lblPSPasswordExpression.Text = PS_PasswordExpressionFeature.FeatureAlias;
                //        //chkPSPasswordExpression.Checked = PS_PasswordExpressionFeature.FeatureValues[0].IsEnabled;
                //        //hdnPSPasswordExpressionStoreFeatureDetailId.Value = Convert.ToString(PS_PasswordExpressionFeature.FeatureValues[0].StoreFeatureDetailId);
                //        rdPwdType.DataSource = PS_PasswordExpressionFeature.FeatureValues;
                //        rdPwdType.DataTextField = "FeatureValue";
                //        rdPwdType.DataValueField = "StoreFeatureDetailId";
                //        rdPwdType.DataBind();

                //        rdPwdType.SelectedValue = Convert.ToString(PS_PasswordExpressionFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                //    }
                //}
                //catch (Exception) { }

                #endregion

                #region Get Password Policy Added by SHRIGANESH SINGH 06 May 2016 For Password Policy
                try
                {
                    FeatureBE PS_PasswordPolicyFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy");
                    if (PS_PasswordPolicyFeature != null)
                    {
                        lblPSPasswordPolicy.Text = PS_PasswordPolicyFeature.FeatureAlias;
                        rdPasswordPolicyType.DataSource = PS_PasswordPolicyFeature.FeatureValues;
                        rdPasswordPolicyType.DataTextField = "FeatureValue";
                        rdPasswordPolicyType.DataValueField = "StoreFeatureDetailId";
                        rdPasswordPolicyType.DataBind();

                        rdPasswordPolicyType.SelectedValue = Convert.ToString(PS_PasswordPolicyFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    }
                }
                catch (Exception)
                {

                }
                #endregion

                #region Get Force Password Change

                FeatureBE PS_ForcePasswordChangeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_forcepasswordchange");
                if (PS_ForcePasswordChangeFeature != null)
                {
                    lblPSForcePasswordChange.Text = PS_ForcePasswordChangeFeature.FeatureAlias;
                    chkPSForcePasswordChange.Checked = PS_ForcePasswordChangeFeature.FeatureValues[0].IsEnabled;
                    hdnPSForcePasswordChangeStoreFeatureDetailId.Value = Convert.ToString(PS_ForcePasswordChangeFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Password Validity

                FeatureBE PS_PasswordValidityFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordvalidity");
                if (PS_PasswordValidityFeature != null)
                {
                    lblPSPasswordValidity.Text = PS_PasswordValidityFeature.FeatureAlias;
                    txtPSPasswordValidity.Text = PS_PasswordValidityFeature.FeatureValues[0].FeatureDefaultValue;
                    hdnPSPasswordValidityStoreFeatureDetailId.Value = Convert.ToString(PS_PasswordValidityFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Enable Captcha

                FeatureBE CR_EnableCaptchaFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_enablecaptcha");
                if (CR_EnableCaptchaFeature != null)
                {
                    chkCREnableCaptcha.Text = CR_EnableCaptchaFeature.FeatureAlias;
                    chkCREnableCaptcha.Checked = CR_EnableCaptchaFeature.FeatureValues[0].IsEnabled;
                    hdnCREnableCaptchaStoreFeatureDetailId.Value = Convert.ToString(CR_EnableCaptchaFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Domain Validation

                FeatureBE CR_DomainValidationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation");
                if (CR_DomainValidationFeature != null)
                {
                    chkCREnableDomainValidation.Text = CR_DomainValidationFeature.FeatureAlias;
                    chkCREnableDomainValidation.Text = CR_DomainValidationFeature.FeatureAlias;
                    rblCREnableDomainValidation.DataSource = CR_DomainValidationFeature.FeatureValues;
                    rblCREnableDomainValidation.DataTextField = "FeatureValue";
                    rblCREnableDomainValidation.DataValueField = "StoreFeatureDetailId";
                    rblCREnableDomainValidation.DataBind();
                    if (CR_DomainValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    {
                        chkCREnableDomainValidation.Checked = true;
                        dvCREnableDomainValidation.Style["display"] = "";
                        rblCREnableDomainValidation.SelectedValue = Convert.ToString(CR_DomainValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    }
                    else
                    {
                        chkCREnableDomainValidation.Checked = false;
                        dvCREnableDomainValidation.Style["display"] = "none";
                        //rblCREnableDomainValidation.SelectedValue = Convert.ToString(CR_DomainValidationFeature.FeatureValues[1].StoreFeatureDetailId);
                    }
                }

                #endregion

                #region Get Email Validation

                FeatureBE CR_EmailValidationFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation");
                if (CR_EmailValidationFeature != null)
                {
                    chkCREnableEmailValidation.Text = CR_EmailValidationFeature.FeatureAlias;
                    chkCREnableEmailValidation.Text = CR_EmailValidationFeature.FeatureAlias;
                    rblCREnableEmailValidation.DataSource = CR_EmailValidationFeature.FeatureValues;
                    rblCREnableEmailValidation.DataTextField = "FeatureValue";
                    rblCREnableEmailValidation.DataValueField = "StoreFeatureDetailId";
                    rblCREnableEmailValidation.DataBind();
                    if (CR_EmailValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    {
                        chkCREnableEmailValidation.Checked = true;
                        dvCREnableEmailValidation.Style["display"] = "";
                        rblCREnableEmailValidation.SelectedValue = Convert.ToString(CR_EmailValidationFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    }
                    else
                    {
                        chkCREnableEmailValidation.Checked = false;
                        dvCREnableEmailValidation.Style["display"] = "none";
                        //rblCREnableDomainValidation.SelectedValue = Convert.ToString(CR_DomainValidationFeature.FeatureValues[1].StoreFeatureDetailId);
                    }
                }

                #endregion

                #region Get Store Access

                FeatureBE CR_StoreAccessFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_storeaccess");
                if (CR_StoreAccessFeature != null)
                {
                    lblCRStoreAccessHeading.Text = CR_StoreAccessFeature.FeatureAlias;
                    rblCRStoreAccess.DataSource = CR_StoreAccessFeature.FeatureValues;
                    rblCRStoreAccess.DataTextField = "FeatureValue";
                    rblCRStoreAccess.DataValueField = "StoreFeatureDetailId";
                    rblCRStoreAccess.DataBind();
                    if (CR_StoreAccessFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rblCRStoreAccess.SelectedValue = Convert.ToString(CR_StoreAccessFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    else
                        rblCRStoreAccess.SelectedValue = Convert.ToString(CR_StoreAccessFeature.FeatureValues[1].StoreFeatureDetailId);
                }

                #endregion

                #region Get Store Type

                FeatureBE CR_StoreTypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_storetype");
                if (CR_StoreTypeFeature != null)
                {
                    lblCRStoreTypeHeading.Text = CR_StoreTypeFeature.FeatureAlias;
                    rblCRStoreType.DataSource = CR_StoreTypeFeature.FeatureValues;
                    rblCRStoreType.DataTextField = "FeatureValue";
                    rblCRStoreType.DataValueField = "StoreFeatureDetailId";
                    rblCRStoreType.DataBind();
                    if (CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rblCRStoreType.SelectedValue = Convert.ToString(CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    else
                        rblCRStoreType.SelectedValue = Convert.ToString(CR_StoreTypeFeature.FeatureValues[1].StoreFeatureDetailId);

                    FeatureBE.FeatureValueBE StoreTypeB2CFeatureValueBEInstance = CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "b2c");
                    FeatureBE.FeatureValueBE StoreTypeB2BFeatureValueBEInstance = CR_StoreTypeFeature.FeatureValues.FirstOrDefault(x => x.FeatureValue.ToLower() == "b2b");

                    if ((StoreTypeB2CFeatureValueBEInstance.IsEnabled == false && StoreTypeB2BFeatureValueBEInstance.IsEnabled == false))
                    {
                        divVatPercentage.Style["display"] = "";
                        txtB2CVatPercentage.Text = StoreTypeB2CFeatureValueBEInstance.FeatureDefaultValue;
                    }
                    else
                    {
                        if (StoreTypeB2CFeatureValueBEInstance.IsEnabled)
                        {
                            divVatPercentage.Style["display"] = "";
                            txtB2CVatPercentage.Text = StoreTypeB2CFeatureValueBEInstance.FeatureDefaultValue;
                        }
                        else
                        {
                            divVatPercentage.Style["display"] = "none";
                        }
                    }
                }

                #endregion

                #region  Gift Certificate
                FeatureBE SC_GiftCertificate = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_giftcertificate");
                if (SC_GiftCertificate != null)
                {
                    ltGiftCertificate.Text = SC_GiftCertificate.FeatureAlias;
                    rdlIsGiftCertificateAllow.DataSource = SC_GiftCertificate.FeatureValues;
                    rdlIsGiftCertificateAllow.DataTextField = "FeatureValue";
                    rdlIsGiftCertificateAllow.DataValueField = "StoreFeatureDetailId";
                    rdlIsGiftCertificateAllow.DataBind();
                    if (SC_GiftCertificate.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    { rdlIsGiftCertificateAllow.SelectedValue = Convert.ToString(SC_GiftCertificate.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId); }
                    else
                    { rdlIsGiftCertificateAllow.SelectedValue = Convert.ToString(SC_GiftCertificate.FeatureValues[1].StoreFeatureDetailId); }
                }
                #endregion

                #region Get Enable Cookie

                FeatureBE CL_EnableCookieFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_enablecookie");
                if (CL_EnableCookieFeature != null)
                {
                    lblCLEnableCookie.Text = CL_EnableCookieFeature.FeatureAlias;
                    chkCLEnableCookie.Checked = CL_EnableCookieFeature.FeatureValues[0].IsEnabled;
                    if (chkCLEnableCookie.Checked)
                    {
                        txtCLCookieLifetime.Enabled = true;
                        txtCLCookieMessage.Enabled = true;
                    }
                    else
                    {
                        txtCLCookieLifetime.Enabled = false;
                        txtCLCookieMessage.Enabled = false;
                    }
                    hdnCLEnableCookieStoreFeatureDetailId.Value = Convert.ToString(CL_EnableCookieFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Cookie Lifetime

                FeatureBE CL_CookieLifetimeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_cookielifetime");
                if (CL_CookieLifetimeFeature != null)
                {
                    lblCLCookieLifetime.Text = CL_CookieLifetimeFeature.FeatureAlias;
                    days.InnerText = CL_CookieLifetimeFeature.FeatureValues[0].DefaultValue;
                    txtCLCookieLifetime.Text = CL_CookieLifetimeFeature.FeatureValues[0].FeatureDefaultValue;
                    hdnCLCookieLifetimeStoreFeatureDetailId.Value = Convert.ToString(CL_CookieLifetimeFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Cookie Message

                FeatureBE CL_CookieMessageFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_cookiemessage");
                if (CL_CookieMessageFeature != null)
                {
                    lblCLCookieMessage.Text = CL_CookieMessageFeature.FeatureAlias;
                    txtCLCookieMessage.Text = CL_CookieMessageFeature.FeatureValues[0].FeatureDefaultValue;
                    hdnCookieMessageStoreFeatureDetailId.Value = Convert.ToString(CL_CookieMessageFeature.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Invoice Account

                FeatureBE IA_InvoiceAccount = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ia_invoiceaccount");
                if (IA_InvoiceAccount != null)
                {
                    ChkInvoiceAccountStatus.Text = IA_InvoiceAccount.FeatureAlias;
                    ChkInvoiceAccountStatus.Checked = IA_InvoiceAccount.FeatureValues[0].IsEnabled;
                    hdnInvoiceAccountStatus.Value = Convert.ToString(IA_InvoiceAccount.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region "From Swapnil for Currency Invoice"
                List<CurrencyBE> objcurrencyBE = CurrencyBL.GetCurrencyPointInvoiceDetails();

                if (objcurrencyBE != null)
                {
                    rptPLCurrencyPoint.DataSource = objcurrencyBE;
                    rptPLCurrencyPoint.DataBind();
                }
                #endregion

                #region Get Enable Email Me Basket Button
                /*Added By Hardik "04/Oct/2016" */
                FeatureBE SC_EmailMeCopyOfBasket = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_email_me_basket");
                if (SC_EmailMeCopyOfBasket != null)
                {
                    ChkEmailCopyBasketBtn.Text = SC_EmailMeCopyOfBasket.FeatureAlias;
                    ChkEmailCopyBasketBtn.Checked = SC_EmailMeCopyOfBasket.FeatureValues[0].IsEnabled;
                    hdnEmailCopyBasketBtn.Value = Convert.ToString(SC_EmailMeCopyOfBasket.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region "Custom Punchout - Added by HARDIK"
                FeatureBE CustomPunchout = GetAllFeatures.FirstOrDefault(x => x.FeatureName == "C_punchout");
                if (CustomPunchout != null)
                {
                    //ltrHomeLink.Text = SS_HomeLink.FeatureAlias;
                    rdoCustomPunchout.DataSource = CustomPunchout.FeatureValues;
                    rdoCustomPunchout.DataTextField = "FeatureValue";
                    rdoCustomPunchout.DataValueField = "StoreFeatureDetailId";
                    rdoCustomPunchout.DataBind();
                    if (CustomPunchout.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    {
                        rdoCustomPunchout.SelectedValue = Convert.ToString(CustomPunchout.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    }
                    else if (CustomPunchout.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    {
                        rdoCustomPunchout.SelectedValue = Convert.ToString(CustomPunchout.FeatureValues[1].StoreFeatureDetailId);
                    }
                    else
                    {
                        rdoCustomPunchout.SelectedValue = Convert.ToString(CustomPunchout.FeatureValues[0].StoreFeatureDetailId);
                    }

                }
                #endregion

                #region Get Product Stock Due Date(Add Days)
                FeatureBE PD_StockDueDate = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_stockduedate");
                if (PD_StockDueDate != null)
                {
                    lblStockDueDate.Text = PD_StockDueDate.FeatureAlias;
                    txtStockDueDate.Text = PD_StockDueDate.FeatureValues[0].FeatureDefaultValue;
                    hdnStockDueDate.Value = Convert.ToString(PD_StockDueDate.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region Enable text field on Product enquiry page for Color
                FeatureBE PD_ColorProdEnquiry = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_colorprodenquiry");
                if (PD_ColorProdEnquiry != null)
                {
                    ChkProductEnquiryColor.Text = PD_ColorProdEnquiry.FeatureAlias;
                    ChkProductEnquiryColor.Checked = PD_ColorProdEnquiry.FeatureValues[0].IsEnabled;
                    hdnProductEnquiryColor.Value = Convert.ToString(PD_ColorProdEnquiry.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region To Enable Only Submit Enquiry on store
                FeatureBE SubmitEnquiry = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "sc_submit_enquiry");
                if (SubmitEnquiry != null)
                {
                    ChkSubmitEnquiry.Text = SubmitEnquiry.FeatureAlias;
                    ChkSubmitEnquiry.Checked = SubmitEnquiry.FeatureValues[0].IsEnabled;
                    hdnSubmitEnquiry.Value = Convert.ToString(SubmitEnquiry.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region "Get Projected Sales - Snehal 22 03 2017"
                List<CurrencyBE> objcurrBE = CurrencyBL.GetProjectedSaleDetails();

                if (objcurrBE != null)
                {
                    rptProjectedSale.DataSource = objcurrBE;
                    rptProjectedSale.DataBind();
                }
                #endregion

                #region Get Birthday Points - Snehal 22 03 2017

                FeatureBE SS_IndeedBirthdayPoint = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_indeedbirthdaypoint");
                if (SS_IndeedBirthdayPoint != null)
                {
                    txtBirthdayPoint.Text = SS_IndeedBirthdayPoint.FeatureValues[0].FeatureDefaultValue;
                    hdnBirthday.Value = Convert.ToString(SS_IndeedBirthdayPoint.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Anniversary Points - Snehal 22 03 2017

                FeatureBE SS_IndeedAnniversaryPoint = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ss_indeedannivesarypoint");
                if (SS_IndeedAnniversaryPoint != null)
                {
                    txtAnnvPoint.Text = SS_IndeedAnniversaryPoint.FeatureValues[0].FeatureDefaultValue;
                    hdnAnniversary.Value = Convert.ToString(SS_IndeedAnniversaryPoint.FeatureValues[0].StoreFeatureDetailId);
                }

                #endregion

                #region Get Category Display Option
                FeatureBE CR_CategoryDisplayFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_categorydisplay");
                if (CR_CategoryDisplayFeature != null)
                {
                    lblCategoryDisplay.Text = CR_CategoryDisplayFeature.FeatureAlias;
                    rblCategoryDisplay.DataSource = CR_CategoryDisplayFeature.FeatureValues;
                    rblCategoryDisplay.DataTextField = "FeatureValue";
                    rblCategoryDisplay.DataValueField = "StoreFeatureDetailId";
                    rblCategoryDisplay.DataBind();
                    if (CR_CategoryDisplayFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rblCategoryDisplay.SelectedValue = Convert.ToString(CR_CategoryDisplayFeature.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    else
                        rblCategoryDisplay.SelectedValue = Convert.ToString(CR_CategoryDisplayFeature.FeatureValues[1].StoreFeatureDetailId);
                }
                #endregion

                #region Get Basket Popup Option
                FeatureBE CR_BasketPopup = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_basketpopup");
                if (CR_BasketPopup != null)
                {
                    lblBasketPopup.Text = CR_BasketPopup.FeatureAlias;
                    rblBasketPopup.DataSource = CR_BasketPopup.FeatureValues;
                    rblBasketPopup.DataTextField = "FeatureValue";
                    rblBasketPopup.DataValueField = "StoreFeatureDetailId";
                    rblBasketPopup.DataBind();
                    if (CR_BasketPopup.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                        rblBasketPopup.SelectedValue = Convert.ToString(CR_BasketPopup.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId);
                    else
                        rblBasketPopup.SelectedValue = Convert.ToString(CR_BasketPopup.FeatureValues[1].StoreFeatureDetailId);
                }
                #endregion

                #region "To Hide Currency dropdown for punchout"
                FeatureBE SS_chkHideCurrDD = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "hidecurrdropdown");
                if (SS_chkHideCurrDD != null)
                {
                    chkHideCurrDD.Checked = SS_chkHideCurrDD.FeatureValues[0].IsEnabled;
                    hdnHideCurrDD.Value = Convert.ToString(SS_chkHideCurrDD.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region Get Order History months
                FeatureBE OrderHistoryMonthstoReturn = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "orderhistorymonthstoreturn");
                if (OrderHistoryMonthstoReturn != null)
                {
                    lblOrderHistoryMonthstoReturn.Text = OrderHistoryMonthstoReturn.FeatureAlias;
                    txtOrderHistoryMonthstoReturn.Text = OrderHistoryMonthstoReturn.FeatureValues[0].FeatureDefaultValue;
                    hdnOrderHistoryMonthstoReturn.Value = Convert.ToString(OrderHistoryMonthstoReturn.FeatureValues[0].StoreFeatureDetailId);
                }
                #endregion

                #region  Allow Multiple or Single Country for Freight Source country
                FeatureBE FreightSrcCountry = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "freightsrccountry");
                if (FreightSrcCountry != null)
                {
                    ltrlAllowFreightSrcCountry.Text = FreightSrcCountry.FeatureAlias;
                    rdlAllowFreightSrcCountry.DataSource = FreightSrcCountry.FeatureValues;
                    rdlAllowFreightSrcCountry.DataTextField = "FeatureValue";
                    rdlAllowFreightSrcCountry.DataValueField = "StoreFeatureDetailId";
                    rdlAllowFreightSrcCountry.DataBind();
                    if (FreightSrcCountry.FeatureValues.FirstOrDefault(x => x.IsEnabled == true) != null)
                    { rdlAllowFreightSrcCountry.SelectedValue = Convert.ToString(FreightSrcCountry.FeatureValues.FirstOrDefault(x => x.IsEnabled == true).StoreFeatureDetailId); }
                    else
                    { rdlAllowFreightSrcCountry.SelectedValue = Convert.ToString(FreightSrcCountry.FeatureValues[1].StoreFeatureDetailId); }
                    Session["DataTest"] = rdlAllowFreightSrcCountry.SelectedValue;
                    List<CountryBE> ObjLstCountryBE = new List<CountryBE>();
                    ObjLstCountryBE = CountryBL.GetAllCountries();
                    ddlCountries.DataSource = ObjLstCountryBE;
                    ddlCountries.DataTextField = "CountryName";
                    ddlCountries.DataValueField = "CountryId";
                    ddlCountries.DataBind();
                    if (FreightSrcCountry.FeatureValues[1].FeatureDefaultValue == null || FreightSrcCountry.FeatureValues[1].FeatureDefaultValue == "")
                    {
                        ddlCountries.SelectedValue = Convert.ToString(ObjLstCountryBE.FirstOrDefault(x => x.CountryId == 12).CountryId);
                    }
                    else
                    {
                        ddlCountries.SelectedValue = FreightSrcCountry.FeatureValues[1].FeatureDefaultValue;
                    }
                }
                #endregion
            }
        }

        protected void rptPLCurrencyPoint_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CurrencyBE CurrencyPointInvoice = (CurrencyBE)e.Item.DataItem;
                HiddenField hdnCurrencyId = (HiddenField)e.Item.FindControl("hdnCurrencyId");
                hdnCurrencyId.Value = Convert.ToString(CurrencyPointInvoice.CurrencyId);
                //hdnPointValue.Value = Convert.ToString(CurrencyPointInvoice.PointValue);
                TextBox txtPointValue = (TextBox)e.Item.FindControl("txtPointValue");
                txtPointValue.Text = Convert.ToString(CurrencyPointInvoice.PointValue);

                Label lblCurrencySymbol = (Label)e.Item.FindControl("lblCurrencySymbol");
                // changes by vivek as Per the converstaion by the client Harley

                lblCurrencySymbol.Text = Convert.ToString(CurrencyPointInvoice.CurrencyCode);

                TextBox txtInvoiceID = (TextBox)e.Item.FindControl("txtInvoiceID");
                txtInvoiceID.Text = Convert.ToString(CurrencyPointInvoice.InvoiceID);

                #region "Get Currency-Point-Invoice Details"
                if (chkEnablePoints.Checked == true && ChkInvoiceAccountStatus.Checked == false)
                {
                    lblPointValueHeader.Attributes.Add("class", "Display");
                    lblCurrencyHeader.Attributes.Add("class", "Display");
                    lblInvoiceIDHeader.Attributes.Add("class", "notDisplay");
                    txtPointValue.Attributes.Add("class", "Display");
                    lblCurrencySymbol.Attributes.Add("class", "Display");
                    txtInvoiceID.Attributes.Add("class", "notDisplay");
                }
                else if (chkEnablePoints.Checked == false && ChkInvoiceAccountStatus.Checked == true)
                {
                    lblPointValueHeader.Attributes.Add("class", "notDisplay");
                    lblCurrencyHeader.Attributes.Add("class", "Display");
                    lblInvoiceIDHeader.Attributes.Add("class", "Display");
                    txtPointValue.Attributes.Add("class", "notDisplay");
                    lblCurrencySymbol.Attributes.Add("class", "Display");
                    txtInvoiceID.Attributes.Add("class", "Display");
                }
                else if (chkEnablePoints.Checked == true && ChkInvoiceAccountStatus.Checked == true)
                {
                    lblPointValueHeader.Attributes.Add("class", "Display");
                    lblCurrencyHeader.Attributes.Add("class", "Display");
                    lblInvoiceIDHeader.Attributes.Add("class", "Display");
                    txtPointValue.Attributes.Add("class", "Display");
                    lblCurrencySymbol.Attributes.Add("class", "Display");
                    txtInvoiceID.Attributes.Add("class", "Display");
                }
                else if (chkEnablePoints.Checked == false && ChkInvoiceAccountStatus.Checked == false)
                {
                    lblPointValueHeader.Attributes.Add("class", "notDisplay");
                    lblCurrencyHeader.Attributes.Add("class", "notDisplay");
                    lblInvoiceIDHeader.Attributes.Add("class", "notDisplay");
                    txtPointValue.Attributes.Add("class", "notDisplay");
                    lblCurrencySymbol.Attributes.Add("class", "notDisplay");
                    txtInvoiceID.Attributes.Add("class", "notDisplay");
                }
                #endregion
            }
        }

        protected void rptPLDisplayType_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                HiddenField hdnPLDisplayTypeStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLDisplayTypeStoreFeatureDetailId");
                hdnPLDisplayTypeStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

                CheckBox chkPLDisplayType = (CheckBox)e.Item.FindControl("chkPLDisplayType");
                chkPLDisplayType.Text = FeatureValue.FeatureValue;
                chkPLDisplayType.Checked = FeatureValue.IsEnabled;
                HtmlInputRadioButton rbPLDisplayType = (HtmlInputRadioButton)e.Item.FindControl("rbPLDisplayType");
                rbPLDisplayType.Attributes.Add("onClick", "SetUniqueRadioButton('DefaultDisplayType',this);");

                if (chkPLDisplayType.Checked)
                {
                    rbPLDisplayType.Checked = FeatureValue.IsDefault;
                    HtmlGenericControl dvDefaultDisplayType = (HtmlGenericControl)e.Item.FindControl("dvDefaultDisplayType");
                    dvDefaultDisplayType.Style.Add("display", "block");
                }
            }
        }

        protected void rptPLSorting_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                HiddenField hdnPLSortingStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLSortingStoreFeatureDetailId");
                hdnPLSortingStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

                CheckBox chkPLSorting = (CheckBox)e.Item.FindControl("chkPLSorting");
                chkPLSorting.Text = FeatureValue.FeatureValue;
                chkPLSorting.Checked = FeatureValue.IsEnabled;

                HtmlInputRadioButton rbPLSorting = (HtmlInputRadioButton)e.Item.FindControl("rbPLSorting");
                rbPLSorting.Attributes.Add("onClick", "SetUniqueRadioButton('PLSorting',this);");
                if (chkPLSorting.Checked)
                {
                    rbPLSorting.Checked = FeatureValue.IsDefault;
                    HtmlGenericControl dvPLSorting = (HtmlGenericControl)e.Item.FindControl("dvPLSorting");
                    dvPLSorting.Style.Add("display", "block");
                }

            }
        }

        protected void rptPLFilters_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.ItemIndex == 0)
                {
                    HtmlGenericControl liFilter = (HtmlGenericControl)e.Item.FindControl("liFilter");
                    liFilter.Style["display"] = "none";
                }
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                HiddenField hdnPLFilterStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPLFilterStoreFeatureDetailId");
                hdnPLFilterStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

                CheckBox chkPLFilters = (CheckBox)e.Item.FindControl("chkPLFilters");
                chkPLFilters.Text = FeatureValue.FeatureValue;
                chkPLFilters.Checked = FeatureValue.IsEnabled;
            }
        }

        protected void rptSCPaymentGateway_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                    HiddenField hdnSCPaymentGatewayStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnSCPaymentGatewayStoreFeatureDetailId");
                    hdnSCPaymentGatewayStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);

                    CheckBox chkSCPaymentGateway = (CheckBox)e.Item.FindControl("chkSCPaymentGateway");
                    chkSCPaymentGateway.Text = FeatureValue.FeatureValue;
                    chkSCPaymentGateway.Checked = FeatureValue.IsEnabled;

                    HtmlInputRadioButton rbSCPaymentGateway = (HtmlInputRadioButton)e.Item.FindControl("rbSCPaymentGateway");
                    rbSCPaymentGateway.Attributes.Add("onClick", "SetUniqueRadioButton('SCPaymentGateway',this);");
                    if (chkSCPaymentGateway.Checked)
                    {
                        rbSCPaymentGateway.Checked = FeatureValue.IsDefault;
                        HtmlGenericControl dvSCPaymentGateway = (HtmlGenericControl)e.Item.FindControl("dvSCPaymentGateway");
                        dvSCPaymentGateway.Style.Add("display", "block");
                    }

                    if (chkSCPaymentGateway.Text == "Adflex")
                    {
                        CheckBoxList CheckBoxListCurrency = (CheckBoxList)e.Item.FindControl("CheckBoxListCurrency");
                        // StoreBE objStores = StoreBL.GetStoreDetails();

                        List<StoreBE.StoreCurrencyBE> objStoresCur = StoreBL.GetCurrency();

                        CheckBoxListCurrency.DataSource = objStoresCur;
                        CheckBoxListCurrency.DataTextField = "CurrencyCode";
                        CheckBoxListCurrency.DataValueField = "CurrencyId";
                        CheckBoxListCurrency.DataBind();
                        var objCurrency = objStoresCur.FindAll(x => x.ValidForAdflex == true);
                        foreach (StoreBE.StoreCurrencyBE oCurrecny in objCurrency)
                        {
                            if (oCurrecny.ValidForAdflex)
                            {
                                for (int i = 0; i < CheckBoxListCurrency.Items.Count; i++)
                                {
                                    if (CheckBoxListCurrency.Items[i].Value.To_Int16() == oCurrecny.CurrencyId)
                                    {
                                        CheckBoxListCurrency.Items[i].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void rptProjectedSale_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CurrencyBE CurrencyProjectedSale = (CurrencyBE)e.Item.DataItem;
                HiddenField hdnProjCurrId = (HiddenField)e.Item.FindControl("hdnProjCurrId");
                hdnProjCurrId.Value = Convert.ToString(CurrencyProjectedSale.CurrencyId);

                TextBox txtProjected = (TextBox)e.Item.FindControl("txtProjected");
                txtProjected.Text = Convert.ToString(CurrencyProjectedSale.ProjectedSale);

                Label lblProjCurrSymbol = (Label)e.Item.FindControl("lblProjCurrSymbol");
                lblProjCurrSymbol.Text = Convert.ToString(CurrencyProjectedSale.CurrencySymbol);

            }
        }


        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(hostAdmin + "Dashboard/Dashboard.aspx");
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {

                UserBE objUserBE = new UserBE();
                objUserBE = Session["StoreUser"] as UserBE;

                intUserId = objUserBE.UserId;
                StoreId = Convert.ToInt16(hdnStoreId.Value);

                #region Update DisplayType

                foreach (RepeaterItem item in rptPLDisplayType.Items)
                {
                    CheckBox chkPLDisplayType = (CheckBox)item.FindControl("chkPLDisplayType");
                    HtmlInputRadioButton rbPLDisplayType = (HtmlInputRadioButton)item.FindControl("rbPLDisplayType");
                    HiddenField hdnPLDisplayTypeStoreFeatureDetailId = (HiddenField)item.FindControl("hdnPLDisplayTypeStoreFeatureDetailId");

                    if (rbPLDisplayType.Checked)
                    {
                        if (chkPLDisplayType.Checked == false)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Default product listing type should be active.", AlertType.Warning);
                            return;
                        }
                    }

                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLDisplayTypeStoreFeatureDetailId.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(chkPLDisplayType.Checked));
                    DictionaryInstance.Add("IsDefault", Convert.ToString(rbPLDisplayType.Checked));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Update Allow Punchout - Added By Snehal 22 09 2016
                Dictionary<string, string> SC_StorePunchout = new Dictionary<string, string>();
                SC_StorePunchout.Add("StoreId", Convert.ToString(StoreId));
                SC_StorePunchout.Add("IsPunchout", Convert.ToString(chkPunchout.Checked));

                StoreBL.UpdateStorePunchout(SC_StorePunchout, true);
                #endregion

                #region Update Pagination

                foreach (ListItem item in rblProductPagination.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    if (item.Text.ToLower() == "pagination")
                        DictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtProductsPerPage.Text.Trim()));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Update Sorting

                foreach (RepeaterItem item in rptPLSorting.Items)
                {
                    CheckBox chkPLSorting = (CheckBox)item.FindControl("chkPLSorting");
                    HtmlInputRadioButton rbPLSorting = (HtmlInputRadioButton)item.FindControl("rbPLSorting");
                    HiddenField hdnPLSortingStoreFeatureDetailId = (HiddenField)item.FindControl("hdnPLSortingStoreFeatureDetailId");

                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLSortingStoreFeatureDetailId.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(chkPLSorting.Checked));
                    DictionaryInstance.Add("IsDefault", Convert.ToString(rbPLSorting.Checked));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Update QuickView

                Dictionary<string, string> QuickViewDictionaryInstance = new Dictionary<string, string>();
                QuickViewDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                QuickViewDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLQuickViewStoreFeatureDetailId.Value));
                QuickViewDictionaryInstance.Add("IsEnabled", Convert.ToString(chkQuickView.Checked));
                QuickViewDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(QuickViewDictionaryInstance, true);

                #endregion

                #region Update Product Detail Print

                Dictionary<string, string> PrintDictionaryInstance = new Dictionary<string, string>();
                PrintDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                PrintDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDPrintStoreFeatureDetailId.Value));
                PrintDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDPrint.Checked));
                PrintDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(PrintDictionaryInstance, true);

                #endregion

                #region Update Product Detail Email

                Dictionary<string, string> EmailDictionaryInstance = new Dictionary<string, string>();
                EmailDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                EmailDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDEmailStoreFeatureDetailId.Value));
                EmailDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDEmail.Checked));
                EmailDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(EmailDictionaryInstance, true);

                #endregion

                #region For displaying total net on payment page
                Dictionary<string, string> EnableTotalNet = new Dictionary<string, string>();
                EnableTotalNet.Add("StoreId", Convert.ToString(StoreId));
                EnableTotalNet.Add("StoreFeatureDetailId", Convert.ToString(hdnEnableTotalNet.Value));
                EnableTotalNet.Add("IsEnabled", Convert.ToString(ChkEnableTotalNet.Checked));
                EnableTotalNet.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(EnableTotalNet, true);
                #endregion

                #region Update Product Detail download image

                Dictionary<string, string> DownloadImageDictionaryInstance = new Dictionary<string, string>();
                DownloadImageDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DownloadImageDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDDownloadImageStoreFeatureDetailId.Value));
                DownloadImageDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDDownloadImage.Checked));
                DownloadImageDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(DownloadImageDictionaryInstance, true);

                #endregion

                #region Update Reg Data for Custom UDFS DANSKE
                Dictionary<string, string> CustomUDFSRegData = new Dictionary<string, string>();
                CustomUDFSRegData.Add("StoreId", Convert.ToString(StoreId));
                CustomUDFSRegData.Add("StoreFeatureDetailId", Convert.ToString(hdnCustomUDFSRegData.Value));
                CustomUDFSRegData.Add("IsEnabled", Convert.ToString(chkCustomUDFSRegData.Checked));
                CustomUDFSRegData.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(CustomUDFSRegData, true);
                #endregion

                #region Update Product Detail social media

                Dictionary<string, string> SocialMediaDictionaryInstance = new Dictionary<string, string>();
                SocialMediaDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                SocialMediaDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDSocialMediaStoreFeatureDetailId.Value));
                SocialMediaDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDSocialMedia.Checked));
                SocialMediaDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(SocialMediaDictionaryInstance, true);

                #endregion

                #region Update Product Detail recently viewed products

                Dictionary<string, string> RecentlyViewedProductsDictionaryInstance = new Dictionary<string, string>();
                RecentlyViewedProductsDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                RecentlyViewedProductsDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDRecentlyViewedProductsStoreFeatureDetailId.Value));
                RecentlyViewedProductsDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDRecentlyViewedProducts.Checked));
                RecentlyViewedProductsDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(RecentlyViewedProductsDictionaryInstance, true);

                #endregion

                #region Update Product Detail you may also like

                Dictionary<string, string> YouMayAlsoLikeDictionaryInstance = new Dictionary<string, string>();
                YouMayAlsoLikeDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                YouMayAlsoLikeDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDYouMayAlsoLikeStoreFeatureDetailId.Value));
                YouMayAlsoLikeDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDYouMayAlsoLike.Checked));
                YouMayAlsoLikeDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(YouMayAlsoLikeDictionaryInstance, true);

                #endregion

                #region Update Product Detail wishlist

                Dictionary<string, string> WishListDictionaryInstance = new Dictionary<string, string>();
                WishListDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                WishListDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDWishlistStoreFeatureDetailId.Value));
                WishListDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDWishlist.Checked));
                WishListDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(WishListDictionaryInstance, true);

                #endregion

                #region Added By ravi gohil for Hide Order Details for Punchout Orders
                Dictionary<string, string> HidePO_OrderDetails = new Dictionary<string, string>();
                HidePO_OrderDetails.Add("StoreId", Convert.ToString(StoreId));
                HidePO_OrderDetails.Add("StoreFeatureDetailId", Convert.ToString(hdnHidePO_OrderDetails.Value));
                HidePO_OrderDetails.Add("IsEnabled", Convert.ToString(ChkHidePO_OrderDetails.Checked));
                HidePO_OrderDetails.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(HidePO_OrderDetails, true);
                #endregion

                #region Update Product Detail review rating

                Dictionary<string, string> ReviewRatingDictionaryInstance = new Dictionary<string, string>();
                ReviewRatingDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                ReviewRatingDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDReviewRatingStoreFeatureDetailId.Value));
                ReviewRatingDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDReviewRating.Checked));
                ReviewRatingDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(ReviewRatingDictionaryInstance, true);

                #endregion

                #region Update Product Detail save PDF

                Dictionary<string, string> SavePDFDictionaryInstance = new Dictionary<string, string>();
                SavePDFDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                SavePDFDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDSavePDFStoreFeatureDetailId.Value));
                SavePDFDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDSavePDF.Checked));
                SavePDFDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(SavePDFDictionaryInstance, true);

                #endregion

                #region Update Product Detail section icons

                Dictionary<string, string> SectionIconsDictionaryInstance = new Dictionary<string, string>();
                SectionIconsDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                SectionIconsDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPDSectionIconsStoreFeatureDetailId.Value));
                SectionIconsDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPDSectionIcons.Checked));
                SectionIconsDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(SectionIconsDictionaryInstance, true);

                #endregion

                #region Update Product Custom Request Form

                Dictionary<string, string> ProductCustomRequestFormDictionaryInstance = new Dictionary<string, string>();
                ProductCustomRequestFormDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                ProductCustomRequestFormDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPCustomRequestForm.Value));
                ProductCustomRequestFormDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPCustomRequestForm.Checked));
                ProductCustomRequestFormDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(ProductCustomRequestFormDictionaryInstance, true);

                #endregion

                #region Update EnableFilter

                int filterCount = 0;
                foreach (RepeaterItem item in rptPLFilters.Items)
                {
                    if (item.ItemIndex == 0)
                        continue;

                    if (!((CheckBox)item.FindControl("chkPLFilters")).Checked)
                        filterCount += 1;
                }

                foreach (RepeaterItem item in rptPLFilters.Items)
                {
                    CheckBox chkPLFilters = (CheckBox)item.FindControl("chkPLFilters");

                    HiddenField hdnPLFilterStoreFeatureDetailId = (HiddenField)item.FindControl("hdnPLFilterStoreFeatureDetailId");

                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLFilterStoreFeatureDetailId.Value));
                    if (item.ItemIndex == 0)
                    {
                        if (filterCount == 4)
                            DictionaryInstance.Add("IsEnabled", Convert.ToString(false));
                        else
                            DictionaryInstance.Add("IsEnabled", Convert.ToString(true));
                    }
                    else
                        DictionaryInstance.Add("IsEnabled", Convert.ToString(chkPLFilters.Checked));

                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                //Dictionary<string, string> EnableFilterDictionaryInstance = new Dictionary<string, string>();
                //EnableFilterDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                //EnableFilterDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLEnableFilterStoreFeatureDetailId.Value));
                //EnableFilterDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnableFilter.Checked));
                //EnableFilterDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                //StoreBL.UpdateStoreDetail(EnableFilterDictionaryInstance, true);

                #endregion

                #region update communigator group id
                Dictionary<string, string> CommunigatorGroupId = new Dictionary<string, string>();
                CommunigatorGroupId.Add("StoreId", Convert.ToString(StoreId));
                CommunigatorGroupId.Add("StoreFeatureDetailId", Convert.ToString(hdnCommunigatorGroupId.Value));
                CommunigatorGroupId.Add("IsEnabled", Convert.ToString(1));
                CommunigatorGroupId.Add("FeatureDefaultValue", Convert.ToString(txtCommunigatorGroupId.Text.Trim()));
                CommunigatorGroupId.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(CommunigatorGroupId, true);
                #endregion

                #region "Comment"
                //#region Update EnableFilter

                //Dictionary<string, string> EnablePriceFilterDictionaryInstance = new Dictionary<string, string>();
                //EnablePriceFilterDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                //EnablePriceFilterDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLEnablePriceFilterStoreFeatureDetailId.Value));
                //EnablePriceFilterDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnablePriceFilter.Checked));
                //EnablePriceFilterDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                //StoreBL.UpdateStoreDetail(EnablePriceFilterDictionaryInstance, true);

                //#endregion

                //#region Update EnableFilter

                //Dictionary<string, string> EnableCategoryFilterDictionaryInstance = new Dictionary<string, string>();
                //EnableCategoryFilterDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                //EnableCategoryFilterDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLEnableCategoryFilterStoreFeatureDetailId.Value));
                //EnableCategoryFilterDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnableCategoryFilter.Checked));
                //EnableCategoryFilterDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                //StoreBL.UpdateStoreDetail(EnableCategoryFilterDictionaryInstance, true);

                //#endregion

                //#region Update EnableFilter

                //Dictionary<string, string> EnableColorFilterDictionaryInstance = new Dictionary<string, string>();
                //EnableColorFilterDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                //EnableColorFilterDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLEnableColorFilterStoreFeatureDetailId.Value));
                //EnableColorFilterDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnableColorFilter.Checked));
                //EnableColorFilterDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                //StoreBL.UpdateStoreDetail(EnableColorFilterDictionaryInstance, true);

                //#endregion

                //#region Update EnableFilter

                //Dictionary<string, string> EnableSectionFilterDictionaryInstance = new Dictionary<string, string>();
                //EnableSectionFilterDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                //EnableSectionFilterDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLEnableSectionFilterStoreFeatureDetailId.Value));
                //EnableSectionFilterDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnableSectionFilter.Checked));
                //EnableSectionFilterDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                //StoreBL.UpdateStoreDetail(EnableSectionFilterDictionaryInstance, true);

                //#endregion 
                #endregion

                #region Update QuickView

                Dictionary<string, string> ProductComparisonDictionaryInstance = new Dictionary<string, string>();
                ProductComparisonDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                ProductComparisonDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLProductComparisonStoreFeatureDetailId.Value));
                ProductComparisonDictionaryInstance.Add("IsEnabled", Convert.ToString(chkProductComparison.Checked));
                ProductComparisonDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(ProductComparisonDictionaryInstance, true);

                #endregion

                #region Update points settings

                Dictionary<string, string> SS_EnablePointsDictionaryInstance = new Dictionary<string, string>();
                SS_EnablePointsDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                SS_EnablePointsDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnEnablePoints.Value));
                SS_EnablePointsDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnablePoints.Checked));
                SS_EnablePointsDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));

                SS_EnablePointsDictionaryInstance.Add("FeatureDefaultValue", txtPoints.Text);
                StoreBL.UpdateStoreDetail(SS_EnablePointsDictionaryInstance, true);

                #endregion

                #region Update Payment Gateway

                foreach (RepeaterItem item in rptSCPaymentGateway.Items)
                {
                    CheckBox chkSCPaymentGateway = (CheckBox)item.FindControl("chkSCPaymentGateway");
                    HtmlInputRadioButton rbSCPaymentGateway = (HtmlInputRadioButton)item.FindControl("rbSCPaymentGateway");
                    HiddenField hdnSCPaymentGatewayStoreFeatureDetailId = (HiddenField)item.FindControl("hdnSCPaymentGatewayStoreFeatureDetailId");

                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnSCPaymentGatewayStoreFeatureDetailId.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(chkSCPaymentGateway.Checked));
                    DictionaryInstance.Add("IsDefault", Convert.ToString(rbSCPaymentGateway.Checked));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);

                    if (chkSCPaymentGateway.Checked == true)
                    {
                        if (chkSCPaymentGateway.Text == "Adflex")
                        {
                            CheckBoxList CheckBoxListCurrency = (CheckBoxList)item.FindControl("CheckBoxListCurrency");
                            foreach (ListItem Lstitem in CheckBoxListCurrency.Items)
                            {
                                Dictionary<string, string> CurrencyDictionaryInstance = new Dictionary<string, string>();
                                CurrencyDictionaryInstance.Add("ValidForAdflex", Convert.ToString(Lstitem.Selected));
                                CurrencyDictionaryInstance.Add("CurrencyId", Convert.ToString(Lstitem.Value));

                                StoreBL.UpdateStoreCurrencyAdflexDetail(CurrencyDictionaryInstance, true);
                            }
                        }
                    }



                    //Dictionary<string, string> CurrencyDictionaryInstance = new Dictionary<string, string>();
                    //CurrencyDictionaryInstance.Add("ValidForAdflex", Convert.ToString(CheckBoxListCurrency.SelectedItem));

                    //StoreBL.UpdateStoreDetail(CurrencyDictionaryInstance, true);
                }

                #endregion

                #region Update Coupon code settings

                Dictionary<string, string> SC_EnableCouponCodeDictionaryInstance = new Dictionary<string, string>();
                SC_EnableCouponCodeDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                SC_EnableCouponCodeDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnEnableCouponCode.Value));
                SC_EnableCouponCodeDictionaryInstance.Add("IsEnabled", Convert.ToString(chkEnableCouponCode.Checked));
                SC_EnableCouponCodeDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(SC_EnableCouponCodeDictionaryInstance, true);

                #endregion

                #region update Order confirmation email

                Dictionary<string, string> OCEmailInstance = new Dictionary<string, string>();
                OCEmailInstance.Add("StoreId", Convert.ToString(StoreId));
                OCEmailInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCOemail.Value));
                OCEmailInstance.Add("IsEnabled", Convert.ToString(ChkCOnfirmationEmail.Checked));
                OCEmailInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(OCEmailInstance, true);

                #endregion

                #region Update Punchout Registration settings - Added By Snehal 20 12 2016

                Dictionary<string, string> SC_PunchoutRegDictionaryInstance = new Dictionary<string, string>();
                SC_PunchoutRegDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                SC_PunchoutRegDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnIsPunchoutReg.Value));
                SC_PunchoutRegDictionaryInstance.Add("IsEnabled", Convert.ToString(chkIsPunchoutRegister.Checked));
                SC_PunchoutRegDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(SC_PunchoutRegDictionaryInstance, true);

                #endregion

                #region Update Minimum Password Length

                Dictionary<string, string> MinimumPasswordLengthDictionaryInstance = new Dictionary<string, string>();
                MinimumPasswordLengthDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                MinimumPasswordLengthDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPSMinimumPasswordLengthStoreFeatureDetailId.Value));
                MinimumPasswordLengthDictionaryInstance.Add("IsEnabled", Convert.ToString(1));
                MinimumPasswordLengthDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtPSMinimumPasswordLength.Text.Trim()));
                MinimumPasswordLengthDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(MinimumPasswordLengthDictionaryInstance, true);

                #endregion

                #region Update Password Expression Commented by SHRIGANESH SINGH for Password Policy 18 May 2016
                //foreach (ListItem item in rdPwdType.Items)
                //{
                //    Dictionary<string, string> PasswordExpressionDictionaryInstance = new Dictionary<string, string>();
                //    PasswordExpressionDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                //    PasswordExpressionDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                //    PasswordExpressionDictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                //    PasswordExpressionDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                //    StoreBL.UpdateStoreDetail(PasswordExpressionDictionaryInstance, true);
                //}


                //Dictionary<string, string> PasswordExpressionDictionaryInstance = new Dictionary<string, string>();
                //PasswordExpressionDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                //PasswordExpressionDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPSPasswordExpressionStoreFeatureDetailId.Value));
                //PasswordExpressionDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPSPasswordExpression.Checked));
                //PasswordExpressionDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                //StoreBL.UpdateStoreDetail(PasswordExpressionDictionaryInstance, true);

                #endregion

                #region Update Password Policy Added by SHRIGANESH SINGH 09 May 2016

                foreach (ListItem item in rdPasswordPolicyType.Items)
                {
                    Dictionary<string, string> PasswordPolicyDictionaryInstance = new Dictionary<string, string>();
                    PasswordPolicyDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    PasswordPolicyDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    PasswordPolicyDictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    PasswordPolicyDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(PasswordPolicyDictionaryInstance, true);
                }
                #endregion

                #region Update Force Password

                Dictionary<string, string> ForcePasswordDictionaryInstance = new Dictionary<string, string>();
                ForcePasswordDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                ForcePasswordDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPSForcePasswordChangeStoreFeatureDetailId.Value));
                ForcePasswordDictionaryInstance.Add("IsEnabled", Convert.ToString(chkPSForcePasswordChange.Checked));
                ForcePasswordDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(ForcePasswordDictionaryInstance, true);

                #endregion

                #region Update Password Validity

                Dictionary<string, string> PasswordValidityDictionaryInstance = new Dictionary<string, string>();
                PasswordValidityDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                PasswordValidityDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPSPasswordValidityStoreFeatureDetailId.Value));
                PasswordValidityDictionaryInstance.Add("IsEnabled", Convert.ToString(1));
                PasswordValidityDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtPSPasswordValidity.Text.Trim()));
                PasswordValidityDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(PasswordValidityDictionaryInstance, true);

                #endregion

                #region Update Enable Captcha

                Dictionary<string, string> EnableCaptchaDictionaryInstance = new Dictionary<string, string>();
                EnableCaptchaDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                EnableCaptchaDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCREnableCaptchaStoreFeatureDetailId.Value));
                EnableCaptchaDictionaryInstance.Add("IsEnabled", Convert.ToString(chkCREnableCaptcha.Checked));
                EnableCaptchaDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(EnableCaptchaDictionaryInstance, true);

                #endregion

                #region Update Domain Validation

                foreach (ListItem item in rblCREnableDomainValidation.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    if (chkCREnableDomainValidation.Checked)
                        DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    else
                        DictionaryInstance.Add("IsEnabled", Convert.ToString(false));

                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Update Email Validation

                foreach (ListItem item in rblCREnableEmailValidation.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    if (chkCREnableEmailValidation.Checked)
                        DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    else
                        DictionaryInstance.Add("IsEnabled", Convert.ToString(false));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Update Allow BackOrder
                Dictionary<string, string> AllowBackOrderDictionaryInstance = new Dictionary<string, string>();
                AllowBackOrderDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                AllowBackOrderDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnODAllowBackOrderStoreFeatureDetailId.Value));
                AllowBackOrderDictionaryInstance.Add("IsEnabled", Convert.ToString(chkAllowBackOrder.Checked));
                AllowBackOrderDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtAllowBackOrder.Text.Trim()));
                AllowBackOrderDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(AllowBackOrderDictionaryInstance, true);
                #endregion

                #region Update Allow Max Order Qty Per Item - Added By Snehal 23 09 2016
                Dictionary<string, string> AllowMaxOrderQtyPerItemDictionaryInstance = new Dictionary<string, string>();
                AllowMaxOrderQtyPerItemDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                AllowMaxOrderQtyPerItemDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnMaxOrderQtyStoreFeatureDetailId.Value));
                AllowMaxOrderQtyPerItemDictionaryInstance.Add("IsEnabled", Convert.ToString(chkMaxAllowQty.Checked));
                AllowMaxOrderQtyPerItemDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtAllowMaxOrder.Text.Trim()));
                AllowMaxOrderQtyPerItemDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(AllowMaxOrderQtyPerItemDictionaryInstance, true);
                #endregion

                #region Update Store Access
                foreach (ListItem item in rblCRStoreAccess.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }
                #endregion

                #region Update Home Link Position
                foreach (ListItem item in rdoLinkPosition.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }
                #endregion

                #region Google API Login Setting - Added By Snehal 23 11 2016

                #region Login Page
                Dictionary<string, string> DictionaryInstanceLogin = new Dictionary<string, string>();
                DictionaryInstanceLogin.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstanceLogin.Add("StoreFeatureDetailId", Convert.ToString(hdnLoginId.Value));
                DictionaryInstanceLogin.Add("IsEnabled", Convert.ToString(chkLogin.Checked));
                DictionaryInstanceLogin.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(DictionaryInstanceLogin, true);
                #endregion
                #region Social Page
                Dictionary<string, string> DictionaryInstanceSocial = new Dictionary<string, string>();
                DictionaryInstanceSocial.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstanceSocial.Add("StoreFeatureDetailId", Convert.ToString(hdnSocialId.Value));
                DictionaryInstanceSocial.Add("IsEnabled", Convert.ToString(chkSocial.Checked));
                DictionaryInstanceSocial.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(DictionaryInstanceSocial, true);
                #endregion

                #region Basket Login Page
                Dictionary<string, string> DictionaryInstanceBasketLogin = new Dictionary<string, string>();
                DictionaryInstanceBasketLogin.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstanceBasketLogin.Add("StoreFeatureDetailId", Convert.ToString(hdnBasketLogin.Value));
                DictionaryInstanceBasketLogin.Add("IsEnabled", Convert.ToString(chkBasketLogin.Checked));
                DictionaryInstanceBasketLogin.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(DictionaryInstanceBasketLogin, true);
                #endregion
                #region Basket Social Page
                Dictionary<string, string> DictionaryInstanceBasketSocial = new Dictionary<string, string>();
                DictionaryInstanceBasketSocial.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstanceBasketSocial.Add("StoreFeatureDetailId", Convert.ToString(hdnBasketSocialLogin.Value));
                DictionaryInstanceBasketSocial.Add("IsEnabled", Convert.ToString(chkBasketSocial.Checked));
                DictionaryInstanceBasketSocial.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(DictionaryInstanceBasketSocial, true);
                #endregion

                #region Register Login Page
                Dictionary<string, string> DictionaryInstanceRegisterLogin = new Dictionary<string, string>();
                DictionaryInstanceRegisterLogin.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstanceRegisterLogin.Add("StoreFeatureDetailId", Convert.ToString(hdnBasketLogin.Value));
                DictionaryInstanceRegisterLogin.Add("IsEnabled", Convert.ToString(chkRegisterLogin.Checked));
                DictionaryInstanceRegisterLogin.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(DictionaryInstanceRegisterLogin, true);
                #endregion
                #region Register Social Page
                Dictionary<string, string> DictionaryInstanceRegisterSocial = new Dictionary<string, string>();
                DictionaryInstanceRegisterSocial.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstanceRegisterSocial.Add("StoreFeatureDetailId", Convert.ToString(hdnBasketSocialLogin.Value));
                DictionaryInstanceRegisterSocial.Add("IsEnabled", Convert.ToString(chkRegisterSocial.Checked));
                DictionaryInstanceRegisterSocial.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(DictionaryInstanceRegisterSocial, true);
                #endregion

                #endregion

                #region Update Search Setting
                foreach (ListItem item in rdoSearchSetting.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }
                #endregion

                #region Update Store Type

                foreach (ListItem item in rblCRStoreType.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    if (item.Selected && item.Text.ToLower() == "b2c")
                        DictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtB2CVatPercentage.Text.Trim()));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Gift Certificate

                foreach (ListItem item in rdlIsGiftCertificateAllow.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }

                #endregion

                #region Update Enable Cookie

                Dictionary<string, string> EnableCookieDictionaryInstance = new Dictionary<string, string>();
                EnableCookieDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                EnableCookieDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCLEnableCookieStoreFeatureDetailId.Value));
                EnableCookieDictionaryInstance.Add("IsEnabled", Convert.ToString(chkCLEnableCookie.Checked));
                EnableCookieDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(EnableCookieDictionaryInstance, true);

                #endregion

                #region Update Cookie Lifetime

                Dictionary<string, string> CookieLifetimeDictionaryInstance = new Dictionary<string, string>();
                CookieLifetimeDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                CookieLifetimeDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCLCookieLifetimeStoreFeatureDetailId.Value));
                CookieLifetimeDictionaryInstance.Add("IsEnabled", Convert.ToString(chkCLEnableCookie.Checked));
                CookieLifetimeDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtCLCookieLifetime.Text.Trim()));
                CookieLifetimeDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(CookieLifetimeDictionaryInstance, true);

                #endregion

                #region Update Cookie Message

                Dictionary<string, string> CookieMessageDictionaryInstance = new Dictionary<string, string>();
                CookieMessageDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                CookieMessageDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnCookieMessageStoreFeatureDetailId.Value));
                CookieMessageDictionaryInstance.Add("IsEnabled", Convert.ToString(chkCLEnableCookie.Checked));
                CookieMessageDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtCLCookieMessage.Text.Trim()));
                CookieMessageDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(CookieMessageDictionaryInstance, true);

                #endregion

                #region Update Invoice Account

                Dictionary<string, string> IA_EnableInvoiceAccountDictionaryInstance = new Dictionary<string, string>();
                IA_EnableInvoiceAccountDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                IA_EnableInvoiceAccountDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnInvoiceAccountStatus.Value));
                IA_EnableInvoiceAccountDictionaryInstance.Add("IsEnabled", Convert.ToString(ChkInvoiceAccountStatus.Checked));
                IA_EnableInvoiceAccountDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(IA_EnableInvoiceAccountDictionaryInstance, true);

                #endregion

                #region Update Enable Email Me a copy btn SC
                /*Added By Hardik "04/Oct/2016" */
                Dictionary<string, string> SC_btnEmailMeCopyOfBasketInstance = new Dictionary<string, string>();
                SC_btnEmailMeCopyOfBasketInstance.Add("StoreId", Convert.ToString(StoreId));
                SC_btnEmailMeCopyOfBasketInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnEmailCopyBasketBtn.Value));
                SC_btnEmailMeCopyOfBasketInstance.Add("IsEnabled", Convert.ToString(ChkEmailCopyBasketBtn.Checked));
                SC_btnEmailMeCopyOfBasketInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(SC_btnEmailMeCopyOfBasketInstance, true);
                #endregion

                #region To Enable custom Punchout [eg. NissanEmployees]
                /*Added By Hardik "24/Nov/2016" */
                if (chkPunchout.Checked == true)
                {
                    foreach (ListItem item in rdoCustomPunchout.Items)
                    {
                        Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                        DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                        DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                        DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                        DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                        StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                    }
                }
                #endregion

                #region Update Stock Due Date for Product Details page
                Dictionary<string, string> StockDueDateDictionaryInstance = new Dictionary<string, string>();
                StockDueDateDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                StockDueDateDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnStockDueDate.Value));
                StockDueDateDictionaryInstance.Add("IsEnabled", Convert.ToString(1));
                StockDueDateDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtStockDueDate.Text.Trim()));
                StockDueDateDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(StockDueDateDictionaryInstance, true);
                #endregion

                #region Update Order History Months To Return
                Dictionary<string, string> OrderHistoryMonthstoReturn = new Dictionary<string, string>();
                OrderHistoryMonthstoReturn.Add("StoreId", Convert.ToString(StoreId));
                OrderHistoryMonthstoReturn.Add("StoreFeatureDetailId", Convert.ToString(hdnOrderHistoryMonthstoReturn.Value));
                OrderHistoryMonthstoReturn.Add("IsEnabled", Convert.ToString(1));
                OrderHistoryMonthstoReturn.Add("FeatureDefaultValue", Convert.ToString(txtOrderHistoryMonthstoReturn.Text.Trim()));
                OrderHistoryMonthstoReturn.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(OrderHistoryMonthstoReturn, true);
                #endregion

                #region To Add text field for color on product enquiry page
                Dictionary<string, string> PD_ColorProdEnquiryInstance = new Dictionary<string, string>();
                PD_ColorProdEnquiryInstance.Add("StoreId", Convert.ToString(StoreId));
                PD_ColorProdEnquiryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnProductEnquiryColor.Value));
                PD_ColorProdEnquiryInstance.Add("IsEnabled", Convert.ToString(ChkProductEnquiryColor.Checked));
                PD_ColorProdEnquiryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(PD_ColorProdEnquiryInstance, true);
                #endregion

                #region To Enable Only Submit Enquiry on store
                Dictionary<string, string> SS_SubmitEnquiry = new Dictionary<string, string>();
                SS_SubmitEnquiry.Add("StoreId", Convert.ToString(StoreId));
                SS_SubmitEnquiry.Add("StoreFeatureDetailId", Convert.ToString(hdnSubmitEnquiry.Value));
                SS_SubmitEnquiry.Add("IsEnabled", Convert.ToString(ChkSubmitEnquiry.Checked));
                SS_SubmitEnquiry.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(SS_SubmitEnquiry, true);
                #endregion

                #region Update Category Display feature
                foreach (ListItem item in rblCategoryDisplay.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }
                #endregion

                #region Update Basket Popup Option
                foreach (ListItem item in rblBasketPopup.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                }
                #endregion

                #region "To Update Currency Point Invoice Details"
                foreach (RepeaterItem item in rptPLCurrencyPoint.Items)
                {
                    TextBox txtPointValue = (TextBox)item.FindControl("txtPointValue");
                    TextBox txtInvoiceID = (TextBox)item.FindControl("txtInvoiceID");
                    HiddenField hdnCurrencyId = (HiddenField)item.FindControl("hdnCurrencyId");

                    Dictionary<string, string> PL_UpdateCurrencyPointInvoice = new Dictionary<string, string>();
                    PL_UpdateCurrencyPointInvoice.Add("CurrencyId", Convert.ToString(hdnCurrencyId.Value));
                    PL_UpdateCurrencyPointInvoice.Add("PointValue", Convert.ToString(txtPointValue.Text.Trim()));
                    PL_UpdateCurrencyPointInvoice.Add("InvoiceID", Convert.ToString(txtInvoiceID.Text.Trim()));
                    StoreBL.UpdateCurrencyPointInvoice(PL_UpdateCurrencyPointInvoice, true);
                }
                #endregion

                #region Update Projected Sales Details - Snehal 22 03 2017
                foreach (RepeaterItem item in rptProjectedSale.Items)
                {
                    TextBox txtProjected = (TextBox)item.FindControl("txtProjected");
                    HiddenField hdnProjCurrId = (HiddenField)item.FindControl("hdnProjCurrId");

                    Dictionary<string, string> PL_UpdateCurrencyProjectedSale = new Dictionary<string, string>();
                    PL_UpdateCurrencyProjectedSale.Add("CurrencyId", Convert.ToString(hdnProjCurrId.Value));
                    PL_UpdateCurrencyProjectedSale.Add("ProjectedSale", Convert.ToString(txtProjected.Text.Trim()));
                    StoreBL.UpdateProjectedSale(PL_UpdateCurrencyProjectedSale, true);
                }
                #endregion

                #region Update Birthday Points - Snehal 23 03 2017

                Dictionary<string, string> BirthdayPointsDictionaryInstance = new Dictionary<string, string>();
                BirthdayPointsDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                BirthdayPointsDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnBirthday.Value));
                BirthdayPointsDictionaryInstance.Add("IsEnabled", Convert.ToString(1));
                BirthdayPointsDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtBirthdayPoint.Text.Trim()));
                BirthdayPointsDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(BirthdayPointsDictionaryInstance, true);

                #endregion

                #region Update Anniversary Points - Snehal 23 03 2017

                Dictionary<string, string> AnniversaryPointsDictionaryInstance = new Dictionary<string, string>();
                AnniversaryPointsDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                AnniversaryPointsDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnAnniversary.Value));
                AnniversaryPointsDictionaryInstance.Add("IsEnabled", Convert.ToString(1));
                AnniversaryPointsDictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtAnnvPoint.Text.Trim()));
                AnniversaryPointsDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(AnniversaryPointsDictionaryInstance, true);

                #endregion

                #region Hide Currency dropdown for Punchout
                Dictionary<string, string> HideCurrDDDictionaryInstance = new Dictionary<string, string>();
                HideCurrDDDictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                HideCurrDDDictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnHideCurrDD.Value));
                HideCurrDDDictionaryInstance.Add("IsEnabled", Convert.ToString(chkHideCurrDD.Checked));
                HideCurrDDDictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(HideCurrDDDictionaryInstance, true);
                #endregion

                #region Update Allow Multiple or Single Country for Freight Source country
                foreach (ListItem item in rdlAllowFreightSrcCountry.Items)
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(item.Value));
                    DictionaryInstance.Add("IsEnabled", Convert.ToString(item.Selected));
                    DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                    if (item.Selected == true)
                    {
                        if (rdlAllowFreightSrcCountry.SelectedIndex == 0)
                        {
                            DictionaryInstance.Add("FeatureDefaultValue", null);
                        }
                        else
                        {
                            DictionaryInstance.Add("FeatureDefaultValue", ddlCountries.SelectedValue);
                        }
                    }
                    StoreBL.UpdateStoreDetail(DictionaryInstance, true);
                    if (Session["DataTest"] != Convert.ToString(rdlAllowFreightSrcCountry.SelectedValue))
                    {
                        FreightManagementBL.TruncateTables();
                    }

                }
                #endregion

                CreateActivityLog("Store Settings", "Updated", "");
                HttpRuntime.Cache.Remove("StoreDetails");
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Settings has been updated successfully.", AlertType.Success);
                GetStoreFeatures();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}