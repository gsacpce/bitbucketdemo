﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Settings_ManageRelatedProducts : BasePage//System.Web.UI.Page
    {
        public static StoreBE objStoreBE;
        protected global::System.Web.UI.WebControls.Repeater rptPLDisplayType;

        protected int intLanguageId = 0;
        Int16 StoreId = 0;
        int intUserId = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Read();
            }
        }
        protected void Read()
        {
            objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                StoreId = objStoreBE.StoreId;
                List<FeatureBE> GetAllFeatures = FeatureBL.GetAllFeatures(StoreId, true);

                #region Get Display Type

                FeatureBE PL_DisplaytypeFeature = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableyoumayalsolike");
                if (PL_DisplaytypeFeature != null)
                {
                    rptPLDisplayType.DataSource = PL_DisplaytypeFeature.FeatureValues;
                    rptPLDisplayType.DataBind();
                }
                #endregion


            }
        }


        protected void rptPLDisplayType_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FeatureBE.FeatureValueBE FeatureValue = (FeatureBE.FeatureValueBE)e.Item.DataItem;
                HiddenField hdnPLDisplayTypeStoreFeatureDetailId = (HiddenField)e.Item.FindControl("hdnPDDisplayTypeStoreFeatureDetailId");
                hdnPLDisplayTypeStoreFeatureDetailId.Value = Convert.ToString(FeatureValue.StoreFeatureDetailId);
                Literal ltrRelatedType = (Literal)e.Item.FindControl("ltrRelatedType");
                ltrRelatedType.Text = Convert.ToString(FeatureValue.FeatureValue);
                CheckBox chkPDDisplayType = (CheckBox)e.Item.FindControl("chkPDDisplayType");
                chkPDDisplayType.Checked = FeatureValue.IsEnabled;
                TextBox txtPrice = (TextBox)e.Item.FindControl("txtPrice");
                Label lblpercentage = (Label)e.Item.FindControl("lblpercentage");

                if (ltrRelatedType.Text == "Related By Price")
                {
                    if (chkPDDisplayType.Checked)
                    {
                        RequiredFieldValidator RftPrice = (RequiredFieldValidator)e.Item.FindControl("RftPrice");
                        RftPrice.Visible = true;
                        txtPrice.Attributes.Add("style", "display:block");
                        lblpercentage.Attributes.Add("style", "display:block");
                        lblpercentage.Text = "%";
                    }
                }
                else
                {
                    txtPrice.Attributes.Add("style", "display:none");
                    lblpercentage.Attributes.Add("style", "display:none");
                    lblpercentage.Text = "";
                }
            }
        }

        protected void cmdSaveClose_Click(object sender, EventArgs e)
        {
            SaveData();
            GlobalFunctions.ShowModalAlertMessages(this.Page, "Releated Products Updated successfully.", AlertType.Success);
            CreateActivityLog("ManageRelatedProducts", "Update", "");
        }
        protected void SaveData()
        {
            UserBE objUserBE = new UserBE();
            objUserBE = Session["StoreUser"] as UserBE;

            intUserId = objUserBE.UserId;
            foreach (RepeaterItem item in rptPLDisplayType.Items)
            {
                CheckBox chkPLDisplayType = (CheckBox)item.FindControl("chkPDDisplayType");
                HiddenField hdnPLDisplayTypeStoreFeatureDetailId = (HiddenField)item.FindControl("hdnPDDisplayTypeStoreFeatureDetailId");

                TextBox txtPrice = (TextBox)item.FindControl("txtPrice");

                Literal ltrRelatedType = (Literal)item.FindControl("ltrRelatedType");
                if (ltrRelatedType.Text == "Related By Price")
                {
                    RequiredFieldValidator RftPrice = (RequiredFieldValidator)item.FindControl("RftPrice");
                    RftPrice.Visible = true;
                    RftPrice.Enabled = true;
                }
                Label lblpercentage = (Label)item.FindControl("lblpercentage");

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                DictionaryInstance.Add("StoreFeatureDetailId", Convert.ToString(hdnPLDisplayTypeStoreFeatureDetailId.Value));
                DictionaryInstance.Add("IsEnabled", Convert.ToString(chkPLDisplayType.Checked));
                DictionaryInstance.Add("FeatureDefaultValue", Convert.ToString(txtPrice.Text));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                StoreBL.UpdateStoreDetail(DictionaryInstance, true);
            }
            HttpRuntime.Cache.Remove("StoreDetails");

        }
    }
}