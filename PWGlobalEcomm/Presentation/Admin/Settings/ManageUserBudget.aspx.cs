﻿using Excel;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.Text.RegularExpressions;

namespace Presentation
{
    public partial class Admin_Settings_ManageUserBudget : BasePage
    {
        protected global::System.Web.UI.WebControls.GridView gvUserBudget;
        protected global::System.Web.UI.WebControls.Button btnUpdate;
        protected global::System.Web.UI.WebControls.TextBox txtEmail, txtUPoints;
        protected global::System.Web.UI.WebControls.FileUpload fileuploadBudget;
        protected global::System.Web.UI.HtmlControls.HtmlContainerControl divPointsUpdate, divImport;
        protected global::System.Web.UI.WebControls.LinkButton LinkRegBackup, LinkUnRegBackup; // Added By snehal 22 09 2016

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindBudgetData();
            }
        }

        private void BindBudgetData()
        {
            List<UserBE.UserBudgetBE> lstUsers = new List<UserBE.UserBudgetBE>();
            lstUsers = UserBL.GetUserBudgetDetails();
            if (lstUsers != null)
            {
                if (lstUsers.Count > 0)
                {
                    gvUserBudget.DataSource = lstUsers;
                    gvUserBudget.DataBind();
                }
                else
                {
                    gvUserBudget.DataSource = null;
                    gvUserBudget.DataBind();
                }
            }
            else
            {
                gvUserBudget.DataSource = null;
                gvUserBudget.DataBind();
            }
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileuploadBudget.HasFiles)
                {
                    bool status = false;           
                 
                    string xlsFile = fileuploadBudget.FileName;  //fileImport is the file upload control
                    string ext = System.IO.Path.GetExtension(xlsFile);
                    if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Only Excel Files are allowed for Import.", AlertType.Warning);
                        return;
                    }
                    DataTable table = new DataTable();
                    table.Columns.Add("EmailId", typeof(string));
                    table.Columns.Add("BudgetCode", typeof(string));
                    //table.Columns.Add("CurrencySymbol", typeof(string));
                   

                    string strFileName = fileuploadBudget.FileName;
                    string fileName = "UsersBudgetdata" + "_" + DateTime.Now.ToString("MMddyyyyHHmmss") + ".xls"; //Changes Done by snehal 22 09 2016

                    FileAttributes attributes = File.GetAttributes(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS");
                    if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        File.SetAttributes(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS", attributes & ~FileAttributes.ReadOnly);

                    if (!Directory.Exists(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\BUDGET"))
                    {
                        Directory.CreateDirectory(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\BUDGET");
                    }
                    //if (!Directory.Exists(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\BUDGET\BACKUP"))
                    //{
                    //    Directory.CreateDirectory(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\BUDGET\BACKUP");
                    //}
                    string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\BUDGET\" + strFileName;
                    //string strFileUploadPathbackup = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\BUDGET\BACKUP\" + fileName;

                    if (File.Exists(strFileUploadPath))
                    {
                        File.Delete(strFileUploadPath);
                    }
                    //string[] filePaths = Directory.GetFiles(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\BUDGET\BACKUP\");
                    //foreach (string filePath in filePaths)
                    //    File.Delete(filePath);

                    fileuploadBudget.SaveAs(strFileUploadPath);

                    if (File.Exists(strFileUploadPath))
                    {
                        DataTable dt = new DataTable();
                        dt = GlobalFunctions.ConvertExcelToDataTable(strFileUploadPath);

                        DataRow[] rowArray = dt.Select();
                        foreach (DataRow row in rowArray)
                        {
                            table.ImportRow(row);
                        }
                    }
                  // ExportToExcelAndSave(dtbackup, strFileUploadPathbackup);
                    status = UserBL.UpdateUserBudget(table);

                    if (status)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "User Budget Imported successfully", AlertType.Success);
                        BindBudgetData();

                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while Importing User Budget. Please try again", AlertType.Failure);
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select an Excel file to Import and try again", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                List<UserBE.UserBudgetBE> lstUsers = UserBL.GetUserBudgetDetails();
                DataTable dt = new DataTable();
                
                if (lstUsers != null)
                {
                    dt.Columns.Add("EmailId");
                    dt.Columns.Add("BudgetCode");
                  //  dt.Columns.Add("CurrencySymbol");

                    foreach (UserBE.UserBudgetBE User in lstUsers)
                    {
                        DataRow drow = dt.NewRow();
                        drow["EmailId"] = User.EmailId;
                        drow["BudgetCode"] = User.BudgetCode;
                        //drow["CurrencySymbol"] = User.CurrencySymbol;
                        dt.Rows.Add(drow);
                    }
                }
                else
                {
                    DataColumn colEmail = new System.Data.DataColumn("EmailId", typeof(string));
                    colEmail.DefaultValue = "EmailId";
                    DataColumn colBudgetCode = new System.Data.DataColumn("BudgetCode", typeof(string));
                    colBudgetCode.DefaultValue = "BudgetCode";
                    //DataColumn colCurrencySymbol = new System.Data.DataColumn("CurrencySymbol", typeof(string));
                    //colCurrencySymbol.DefaultValue = "CurrencySymbol";
                    dt.Columns.Add(colEmail);
                    dt.Columns.Add(colBudgetCode);
                    //dt.Columns.Add(colCurrencySymbol);
                }
                using (ExcelPackage pck = new ExcelPackage())
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("UsersBudget");
                    // DataTable NewTable = tbl.DefaultView.ToTable(false, "ProductCode","LanguageId","ProductName","ProductDescription", "FurtherDescription", "");

                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                    ws.Cells["A1"].LoadFromDataTable(dt, true);

                    Response.Clear();
                    //Write it back to the client
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=UsersBudget.xlsx");
                    Response.BinaryWrite(pck.GetAsByteArray());

                    Response.End();
                }
          
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static void ExportToExcelAndSave(DataTable dt, string path)
        {
            try
            {
                StreamWriter writer = new StreamWriter(path);
                int colCount = dt.Columns.Count;
                for (int i = 0; i < colCount; i++)
                {
                    writer.Write(dt.Columns[i]);
                    if (i < colCount - 1)
                    {
                        writer.Write("\t");
                    }
                }
                writer.Write(writer.NewLine);
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < colCount; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            writer.Write(dr[i].ToString());
                        }
                        if (i < colCount - 1)
                        {
                            writer.Write("\t");
                        }
                    }
                    writer.Write(writer.NewLine);
                }
                writer.Close();
                writer.Dispose();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void DeleteRecord(object sender, EventArgs e)
        {
            LinkButton lnkRemove = (LinkButton)sender;
            bool isDeleted = UserBL.DeleteBudgetRecord(lnkRemove.CommandArgument);
            if(isDeleted==true)
            {
                BindBudgetData();
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error while deleting record", AlertType.Failure);
            }
        }
    }
}