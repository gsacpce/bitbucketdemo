﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Settings_ApplyJS : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.TextBox txtJS;
        string FilePath = GlobalFunctions.GetPhysicalFolderPath() + @"\JS\" + "StoreMaster.js";
        string BackUpFolderPath = GlobalFunctions.GetPhysicalFolderPath() + @"\JS\" + "Backup";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReadJS(Path.GetFileName(FilePath));
            }
        }

        protected void ReadJS(string FileName)
        {
            try
            {
                if (FilePath != "")
                {
                    txtJS.Text = File.ReadAllText(FilePath);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "File not Found.", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void btnRestore_Click(object sender, EventArgs e)
        {
            Restore(Path.GetFileName(FilePath));
            CreateActivityLog("ApplyJS btnRestore", "Restore", txtJS.Text);
        }

        protected void btnBackup_Click(object sender, EventArgs e)
        {
            Backup(Path.GetFileName(FilePath));
            CreateActivityLog("ApplyJS btnBackup", "Backup", txtJS.Text);
        }

        protected void Backup(string FileName)
        {
            try
            {
                string ExistingFile = string.Empty;
                string NewFileName = Path.GetFileNameWithoutExtension(FileName);
                if (txtJS.Text != string.Empty)
                {
                    if (!Directory.Exists(BackUpFolderPath))
                        Directory.CreateDirectory(BackUpFolderPath);
                    DirectoryInfo di = new DirectoryInfo(BackUpFolderPath);
                    di.Attributes &= ~FileAttributes.ReadOnly;
                    FileInfo[] TXTFiles = di.GetFiles(NewFileName + "_*");
                    if (TXTFiles.Length > 0)
                    {
                        foreach (var fi in TXTFiles)
                        {
                            FileAttributes attributes = File.GetAttributes(BackUpFolderPath + "\\" + fi);
                            if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                                File.SetAttributes(BackUpFolderPath + "\\" + fi, attributes & ~FileAttributes.ReadOnly);
                            if (fi.ToString().Contains("_"))
                            {
                                ExistingFile = fi.ToString().Substring(0, fi.ToString().IndexOf("_")).ToString();
                            }
                            if (NewFileName == ExistingFile)
                            {
                                NewFileName = NewFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".js";
                                File.Delete(BackUpFolderPath + "\\" + fi.ToString());
                                File.Copy(FilePath, BackUpFolderPath + "\\" + NewFileName, true);
                                GlobalFunctions.ShowModalAlertMessages(this, "Backup has been done for file " + FileName + "  !!", AlertType.Success);

                            }
                        }
                    }
                    else
                    {
                        NewFileName = NewFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".js";
                        File.Copy(FilePath, BackUpFolderPath + "\\" + NewFileName, true);
                        GlobalFunctions.ShowModalAlertMessages(this, "Backup has been done for file " + FileName + "  !!", AlertType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void Restore(string FileName)
        {
            try
            {
                string NewFileName = Path.GetFileNameWithoutExtension(FileName);
                DirectoryInfo di = new DirectoryInfo(BackUpFolderPath);
                di.Attributes &= ~FileAttributes.ReadOnly;
                FileInfo[] TXTFiles = di.GetFiles(NewFileName + "_*");
                //     FileInfo[] TXTFiles = di.GetFiles("*.css");
                if (TXTFiles.Length > 0)
                {
                    foreach (var fi in TXTFiles)
                    {
                        string FinalFile = fi.ToString();
                        FileAttributes attributes = File.GetAttributes(FilePath);
                        if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            File.SetAttributes(FilePath, attributes & ~FileAttributes.ReadOnly);
                        File.Copy(BackUpFolderPath + "\\" + FinalFile, FilePath, true);
                        ReadJS(Path.GetFileName(FilePath));
                        GlobalFunctions.ShowModalAlertMessages(this, "Restored file " + FileName + "  !!", AlertType.Success);

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Write(Path.GetFileName(FilePath));
                CreateActivityLog("ApplyJS btnSave", "Insert", txtJS.Text);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void Write(string FileName)
        {
            // First We create the backup then delete existing file and Re-create it. 
            try
            {

                if (FilePath != "")
                {
                    if (txtJS.Text != string.Empty)
                    {
                        if (!Directory.Exists(BackUpFolderPath))
                            Directory.CreateDirectory(BackUpFolderPath);
                        File.Copy(FilePath, BackUpFolderPath + "\\" + FileName, true);
                        if (!File.Exists(FilePath))
                        {
                            File.Create(FilePath + "\\" + FileName);
                        }
                        File.WriteAllText(FilePath, txtJS.Text);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "JS Updated successfully.", AlertType.Success);
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Text can not be blank.", AlertType.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}