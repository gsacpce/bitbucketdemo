﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;

namespace Presentation
{
    public partial class Admin_Settings_ManageCSS : BasePage //System.Web.UI.Page
    {
        public string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        protected global::System.Web.UI.WebControls.FileUpload fuUploadTemplate;
        protected global::System.Web.UI.WebControls.Button cmdUploadCSS, cmdDownloadCSS, btnShowFont, btnBackup, btnRestore;
        protected global::System.Web.UI.WebControls.TextBox txtCSSContent;
        protected global::System.Web.UI.WebControls.Repeater rptCssList;
        protected global::System.Web.UI.WebControls.LinkButton lnkFileName;
        protected global::System.Web.UI.WebControls.HiddenField hdnFileName;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    HideOldVersion();
                    BindAllCSSFileName();
                    //Read();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void HideOldVersion()
        {
            try
            {
                fuUploadTemplate.Visible = false;
                cmdDownloadCSS.Visible = false;
                cmdUploadCSS.Visible = false;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #region Old Version Download and Upload CSS of Zip Files will not be used further but Kept for Future Requirement if asked
        private void DownloadCSS()
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    DirectoryInfo CSSDirInfo = new DirectoryInfo(GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\");
                    FileInfo[] CSSFileInfo = CSSDirInfo.GetFiles();
                    foreach (FileInfo cssFile in CSSFileInfo)
                    {
                        switch (Path.GetFileNameWithoutExtension(cssFile.FullName).ToLower())
                        {
                            case "style":
                            case "bootstrap-theme": zip.AddFile(cssFile.FullName, "CSS");
                                break;
                            default:
                                break;
                        }
                    }

                    Response.Clear();
                    Response.BufferOutput = false;
                    string dateTimeFormat = DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + "_"
                                                + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();

                    string zipName = String.Format("CSSZip_{0}.zip", dateTimeFormat);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        private void UploadCSS()
        {
            try
            {
                if (fuUploadTemplate.HasFile)
                {
                    //Chk for File Extension
                    string[] strFileTypes = { ".zip" };
                    bool bChkFileType = false;
                    bChkFileType = GlobalFunctions.CheckFileExtension(fuUploadTemplate, strFileTypes);
                    if (!bChkFileType)
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
                        SetFocus(fuUploadTemplate.ClientID);
                        return;
                    }

                    string filename = Path.GetFileName(fuUploadTemplate.FileName);
                    string strTemplatePathWithExtension = GlobalFunctions.GetPhysicalFolderPath() + @"CSS\" + filename;
                    string strTemplatePathWithoutExtension = GlobalFunctions.GetPhysicalFolderPath() + @"CSS\" + Path.GetFileNameWithoutExtension(filename);

                    if (File.Exists(strTemplatePathWithExtension))
                    {
                        File.Delete(strTemplatePathWithExtension);
                        Directory.Delete(strTemplatePathWithoutExtension, true);
                    }

                    fuUploadTemplate.SaveAs(strTemplatePathWithExtension);
                    System.IO.Compression.ZipFile.ExtractToDirectory(strTemplatePathWithExtension, GlobalFunctions.GetPhysicalFolderPath() + @"CSS\");
                    DirectoryInfo UploadedCSSDirInfo = new DirectoryInfo(GlobalFunctions.GetPhysicalFolderPath() + @"CSS\CSS");
                    FileInfo[] CSSFileInfo = UploadedCSSDirInfo.GetFiles();
                    foreach (FileInfo file in CSSFileInfo)
                    {
                        File.Copy(file.FullName, GlobalFunctions.GetPhysicalFolderPath() + @"CSS\" + file.Name, true);
                    }
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "CSS uploaded successfully.", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select file", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while uploading CSS.", AlertType.Failure);
            }
        }
        protected void cmdDownloadCSS_Click(object sender, EventArgs e)
        {
            try
            {
                DownloadCSS();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void cmdUploadCSS_Click(object sender, EventArgs e)
        {
            try
            {
                UploadCSS();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion

        protected void Write(string FileName)
        {
            // First We create the backup then delete existing file and Re-create it. 
            try
            {
                CreateActivityLog("Manage CSS", "Updated", "1");
                string FilePath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + FileName;
                //  string FilePath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + "bootstrap-theme.css";
                //string FileName = Path.GetFileName(FilePath);
                CreateActivityLog("Manage CSS", "FilePath", FilePath);
                CreateActivityLog("Manage CSS", "Updated", "2");
                string BackUpFolderPath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + "backup";
                CreateActivityLog("Manage CSS", "BackUpFolderPath", BackUpFolderPath);
                if (FilePath != "")
                {
                    if (txtCSSContent.Text != string.Empty)
                    {
                        if (!Directory.Exists(BackUpFolderPath))
                            Directory.CreateDirectory(BackUpFolderPath);


                        //FileAttributes attributes = File.GetAttributes(BackUpFolderPath + "\\" + FileName);
                        //if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        //    File.SetAttributes(BackUpFolderPath + "\\" + FileName, attributes & ~FileAttributes.ReadOnly);


                        #region copy existing file into backup
                        File.Copy(FilePath, BackUpFolderPath + "\\" + FileName, true);
                        CreateActivityLog("Manage CSS", "copy existing file into backup", FileName);
                        #endregion

                        if (!File.Exists(FilePath))
                        {
                            File.Create(FilePath + "\\" + FileName);
                            CreateActivityLog("Manage CSS", "Create new file", FilePath + "\\" + FileName);
                        }

                        FileAttributes attributes1 = File.GetAttributes(FilePath);
                        if ((attributes1 & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            File.SetAttributes(FilePath, attributes1 & ~FileAttributes.ReadOnly);

                        File.WriteAllText(FilePath, txtCSSContent.Text);


                        CreateActivityLog("Manage CSS", "Updated", FileName);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "CSS Updated successfully.", AlertType.Success);
                    }
                    else
                    {
                        CreateActivityLog("Manage CSS", "6", FilePath + "\\" + FileName);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "CSS can not be blank.", AlertType.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                CreateActivityLog("Manage CSS", "7", ex.Message);
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Write(Convert.ToString(hdnFileName.Value));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btnBackup_Click(object sender, EventArgs e)
        {
            Backup(Convert.ToString(hdnFileName.Value));
        }

        /// <summary>
        /// take back up of selected CSS file and store in Backup Folder by appending datetime to css file
        /// </summary>
        /// <param name="FileName"></param>
        protected void Backup(string FileName)
        {
            try
            {
                string ExistingFile = string.Empty;
                string FilePath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + FileName;
                string NewFileName = Path.GetFileNameWithoutExtension(FileName);
                string BackUpFolderPath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + "backup";
                if (txtCSSContent.Text != string.Empty)
                {
                    if (!Directory.Exists(BackUpFolderPath))
                        Directory.CreateDirectory(BackUpFolderPath);
                    DirectoryInfo di = new DirectoryInfo(BackUpFolderPath);
                    di.Attributes &= ~FileAttributes.ReadOnly;
                    FileInfo[] TXTFiles = di.GetFiles(NewFileName + "_*");
                    //     FileInfo[] TXTFiles = di.GetFiles("*.css");
                    if (TXTFiles.Length > 0)
                    {
                        foreach (var fi in TXTFiles)
                        {
                            FileAttributes attributes = File.GetAttributes(BackUpFolderPath + "\\" + fi);
                            if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                                File.SetAttributes(BackUpFolderPath + "\\" + fi, attributes & ~FileAttributes.ReadOnly);
                            if (fi.ToString().Contains("_"))
                            {
                                ExistingFile = fi.ToString().Substring(0, fi.ToString().IndexOf("_")).ToString();
                                if (NewFileName == ExistingFile)
                                {
                                    NewFileName = NewFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".css";
                                    // NewFileName = NewFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss").Replace(" ", "").Replace("-", "").Replace(":", "").Replace("/", "") + ".css";
                                    File.Delete(BackUpFolderPath + "\\" + fi.ToString());
                                    File.Copy(FilePath, BackUpFolderPath + "\\" + NewFileName, true);
                                    GlobalFunctions.ShowModalAlertMessages(this, "Backup has been done for file " + FileName + "  !!", AlertType.Success);
                                }
                            }
                        }

                    }
                    else
                    {
                        NewFileName = NewFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".css";
                        //NewFileName = NewFileName + "_" + DateTime.Now.ToString().Replace(" ", "").Replace("-", "").Replace(":", "").Replace("/", "") + ".css";
                        File.Copy(FilePath, BackUpFolderPath + "\\" + NewFileName, true);
                        GlobalFunctions.ShowModalAlertMessages(this, "Backup has been done for file " + FileName + "  !!", AlertType.Success);

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }


        }


        protected void btnRestore_Click(object sender, EventArgs e)
        {
            Restore(Convert.ToString(hdnFileName.Value));
        }

        /// <summary>
        ///  Restore selected CSS file from backup folder(latest datetime appended css file) and store CSS folder
        /// </summary>
        /// <param name="FileName"></param>
        protected void Restore(string FileName)
        {
            try
            {
                string FilePath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + FileName;
                string NewFileName = Path.GetFileNameWithoutExtension(FileName);
                string BackUpFolderPath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + "backup";
                DirectoryInfo di = new DirectoryInfo(BackUpFolderPath);
                di.Attributes &= ~FileAttributes.ReadOnly;
                FileInfo[] TXTFiles = di.GetFiles(NewFileName + "_*");
                //     FileInfo[] TXTFiles = di.GetFiles("*.css");
                if (TXTFiles.Length > 0)
                {
                    foreach (var fi in TXTFiles)
                    {
                        string FinalFile = fi.ToString();
                        FileAttributes attributes = File.GetAttributes(FilePath);
                        if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            File.SetAttributes(FilePath, attributes & ~FileAttributes.ReadOnly);
                        File.Copy(BackUpFolderPath + "\\" + FinalFile, FilePath, true);
                        ReadCSS(Path.GetFileName(FilePath));
                        GlobalFunctions.ShowModalAlertMessages(this, "Restored file " + FileName + "  !!", AlertType.Success);

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        protected void btnShowFont_Click(object sender, EventArgs e)
        {
            Response.Redirect(hostAdmin + "settings/FontSettings.aspx");
        }

        protected void ReadCSS(string FileName)
        {
            try
            {
                hdnFileName.Value = FileName;
                string FilePath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + FileName;
                // string FilePath = GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\" + "bootstrap-theme.css";
                if (FilePath != "")
                {
                    txtCSSContent.Text = File.ReadAllText(FilePath);
                    if (txtCSSContent.Text != "")
                    {
                        btnShowFont.Visible = true;
                    }
                    else
                    {
                        btnShowFont.Visible = false;
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "File not Found.", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindAllCSSFileName()
        {
            DirectoryInfo CSSDirInfo = new DirectoryInfo(GlobalFunctions.GetPhysicalFolderPath() + @"\CSS\");
            FileInfo[] CSSFileInfo = CSSDirInfo.GetFiles();
            List<string> lstCssFileName = new List<string>();
            string FilesNotAllowed = GlobalFunctions.GetSetting("FileDoNotAllowtoUpdate");
            //string FilesNotAllowed = "Font.css,bootstrap-datetimepicker.css,CategoryListing.css,common-login.css,font-awesome.css,product-listing.css";
            foreach (FileInfo cssFile in CSSFileInfo)
            {
                if (Path.GetExtension(cssFile.Name).Contains(".css"))
                {
                    if (!FilesNotAllowed.Contains(Convert.ToString(cssFile.Name)))
                        lstCssFileName.Add(cssFile.Name);
                }
            }
            if (lstCssFileName.Count > 0)
            {
                rptCssList.DataSource = from c in lstCssFileName select new { FileName = c }; //lstCssFileName;
                rptCssList.DataBind();
            }
            else
            {
                rptCssList.DataSource = null;
                rptCssList.DataBind();
            }
        }

        protected void rptCssList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            ReadCSS(Convert.ToString(e.CommandArgument));
        }
    }
}