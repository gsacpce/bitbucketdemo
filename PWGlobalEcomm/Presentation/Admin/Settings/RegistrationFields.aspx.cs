﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.Security.Application;
using System.Threading;


namespace Presentation
{
    public partial class Admin_Settings_RegistrationFields : BasePage
    {
        public string Hostadmin = GlobalFunctions.GetVirtualPathAdmin();

        protected global::System.Web.UI.WebControls.Repeater rptRegisterContent;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMessage;

        UserRegistrationBE.RegistrationLanguagesBE objRegistrationLanguagesBE = new UserRegistrationBE.RegistrationLanguagesBE();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Exceptions.WriteInfoLog("test");
                    PopulateLanguageDropDownList();
                    PopulateRegData();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void rptRegisterContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateRegData();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        private void PopulateLanguageDropDownList()
        {
            List<LanguageBE> languages = new List<LanguageBE>();
            languages = LanguageBL.GetAllLanguageDetails(true);

            if (languages != null)
            {
                if (languages.Count() > 0)
                {
                    ddlLanguage.DataSource = languages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                }
            }
        }
        private void PopulateRegData()
        {
            try
            {
                List<UserRegistrationBE.RegistrationLanguagesBE> lstResourceData = new List<UserRegistrationBE.RegistrationLanguagesBE>();
                DataTable dtResourceData = null;
                //RegistrationCustomFieldBE [USP_ManageRegistrationData_SAED]
                objRegistrationLanguagesBE.Action = Convert.ToInt16(DBAction.Select);
                objRegistrationLanguagesBE.LabelTitle = "";
                objRegistrationLanguagesBE.CreatedBy = 1;
                lstResourceData = UserRegistrationBL.GetRegistrationAllLanguages<UserRegistrationBE.RegistrationLanguagesBE>(objRegistrationLanguagesBE);
                if (lstResourceData != null && lstResourceData.Count > 0)
                {
                    lstResourceData = lstResourceData.FindAll(x => x.LanguageId == Convert.ToInt16(ddlLanguage.SelectedValue));
                    if (lstResourceData != null && lstResourceData.Count > 0)
                    {
                        rptRegisterContent.DataSource = lstResourceData;
                        rptRegisterContent.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Hostadmin + "Dashboard/Dashboard.aspx", true);
            }
            catch (ThreadAbortException) { }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int Modifiedby = 0;
                UserBE objUSerBE = Session["StoreUser"] as UserBE;
                if (objUSerBE.UserId > 0)
                {
                    Modifiedby = objUSerBE.UserId;
                }
                foreach (RepeaterItem item in rptRegisterContent.Items)
                {
                    TextBox txtContent = (TextBox)item.FindControl("txtContent");
                    HtmlInputHidden hdnResourceLanguageId = (HtmlInputHidden)item.FindControl("hdnResourceLanguageId");
                    objRegistrationLanguagesBE.Action = Convert.ToInt16(DBAction.Update);
                    objRegistrationLanguagesBE.CreatedBy = Convert.ToInt16(Modifiedby);
                    objRegistrationLanguagesBE.LabelTitle = txtContent.Text;
                    objRegistrationLanguagesBE.RegistrationLanguageId = Convert.ToInt16(hdnResourceLanguageId.Value);
                    UserRegistrationBL.AEDRegistrationData(objRegistrationLanguagesBE);
                }
                CreateActivityLog("Update Registration Field", "Updated", "");
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated successfully.", AlertType.Success);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}