﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;

namespace Presentation
{
    public partial class Admin_Settings_ThemeConfigurator : BasePage //System.Web.UI.Page
    {
        #region HTML Select
        protected global::System.Web.UI.HtmlControls.HtmlSelect customAccordionBgColor, customAccordionColor, customAccordionTextActiveColor, customAccordionTextColor,
            customAccordionTextFF, customAccordionTextFS, customActionBtnBg, customActionBtnColor, customActionBtnHoverBg, customActionBtnHoverColor, customButtonBg,
            customButtonColor, customButtonHoverBg, customButtonHoverColor, customCurrencyFS, customCurrencyIconColor, customCurrencyIconColorFF,
            customCurrencyIconColorHover, customCurrencyIconFS, customCurrencyTextColor, customCurrencyTextColorHover, customCurrencyTextFF
            , customInputBorderColor, customInputColor, customInputFF, customInputFS, customLabelColor, customLabelFF, customLabelFS, customPanelBg,
            customPanelColor, customRatingsContainerColor, customRatingsStarColor, customResetBtnBg, customResetBtnColor, customResetBtnHoverBg,
            customResetBtnHoverColor, customTable, customTableHeadColor, customTableHeadFF, customTableHeadFS, customTableHeadingTextColor, customTableHeadingTextFF,
            customTableHeadingTextFS, customTableTextColor, customTableTextFF, customTableTextFS, customTabsAActiveBg, customTabsAActiveColor, customTabsAActiveHoverBg,
            customTabsAActiveHoverColor, customTabsABg, customTabsAColor, customTabsAFF, customTabsAFS, customTabsAHoverBg, customTabsAHoverColor, customToggleBgColor,
            customToggleBgHoverColor, customToggleColor, customToggleHoverColor, FooterBg, footerLinkActiveFF, footerLinkActiveFS, FooterLinkcolor,
            FooterLinkFF, FooterLinkFS, FooterLinkHoverColor, FooterTextColor, FooterTextFF, FooterTextFS, HeaderBg, HeaderLinkActiveColor, HeaderLinkColor,
             HeaderLinkFF, HeaderLinkFS, HeaderLinkHoverColor, HyperLnkColor, HyperLnkFF, HyperLnkFS, HyperLnkHoverColor, liDropDownMenuActiveColor,
            liDropDownMenuColor, liDropDownMenuHoverBg, liDropDownMenuHoverColor, MenuBg, MenuLinkColor, MenuLinkFF, MenuLinkFS, MenuLinkHoverColor, OwlButtonsBg,
            OwlButtonsColor, OwlButtonsHoverBg, OwlButtonsHoverColor, OwlCarouselBg, OwlCarouselBorderColor, PageHeadingColor, PageHeadingFF, PageHeadingFS,
            pageHighlightTextColor, pageHighlightTextFF, pageHighlightTextFS, pageHighlightTextTT, PageSubTitleColor, PageSubTitleFF, PageSubTitleFS, PageSubTitleTT,
            PageTextColor, PageTextFF, PageTextFS, PageTitleBg, PageTitleColor, PageTitleFF, PageTitleFS, ProductBorder, ProductBorderHover, ProductBorderHoverProductNameColor,
            ProductCodeColor, ProductCodeFF, ProductCodeFS, productDescriptionColor, productDescriptionFF, oductDescriptionFS, ProductNameColor, ProductNameFF,
            ProductNameFS, ProductPriceColor, ProductPriceFF, ProductPriceFS, ProductSmlNameColor, ProductSmlNameFF, ProductSmlNameFS, ProductTitleColor, ProductTitleFF,
            ProductTitleFS, TabBg, productDescriptionFS, BreadcrumbBgColor, BreadcrumbColor, BreadcrumbHoverColor, BreadcrumbActiveColor, HyperLnkSmlColor, HyperLnkSmlHoverColor,
            PageHeading1Color, PageHeading2Color, PageHeading3Color, PageSmlTextColor, PageSubSubTitleColor, PageScrollLinksColor, PageScrollLinksHoverColor,
            customButtonBorderColor, customActionBtnBorderColor, customButtonBorderHoverColor, customActionBtnBorderHoverColor, PageHeadingBg, customTableBorder,
            customResetBtnBorderColor, customResetBtnHoverBorderColor, customTableHeaderTitleBgColor, customTableHeaderTitleColor, customTabsActiveColor,
            customTabsActiveBg, customPanelHeadingBgColor, customPanelHeadingColor, customPanelBorderColor, customRatingsContainerBgColor, liDropDownMenFS,
            BeradcrumbFS, HyperLnkSmlFS, PageHeading1FS, PageHeading2FS, PageHeading3FS, PageSmlTextFS, PageSubSubTitleFS, PageScrollLinksFS, customTableHeaderTitleFS,
            NavActiveColor, NavActiveBgColor, liDropDownMenuActiveBgColor, customCloseColorBg, customCloseFS, customCloseHoverColorBg,
            //footerLinkActiveColor,
            /*Sachin Chauhan Start : 11 01 2015*/
            HeaderLinkPanelDropDownBg, ProductCategoryDropDownMenuPanelBg
            , HeaderLinkDropDownMenuColor, HeaderLinkDropDownMenuHoverBg, HeaderLinkDropDownMenuHoverColor, HeaderLinkDropDownMenuActiveColor,
            HeaderLinkDropDownMenuActiveBgColor, liDropDownBg, HeaderLinkDropDownMenFS, HeaderLinkMenuLinkFW, liDropDownMenFW
            , pdHyperLnkColor, pdHyperLnkHoverColor, pdHyperLnkFS,
            //HeaderLinkDropDownBg, //uncommeneted by vikram to disable highlight on myacounnt;
             HeaderLinkPanelDropDownActiveItemBg,
             SearchIconTxtColor, SearchIconFS, SearchIconHoverColorBg, BasketIconTxtColor, BasketIconFS, BasketIconHoverColorBg, StrikePriceTxtColor ,BasketCountTxtColor, BasketCountBgColor;

        protected global::System.Web.UI.WebControls.CheckBox HeaderLinkDropdownFStyle, HeaderLinkDropdownFD, liDropDownMenFStyle, liDropDownMenFD;
        /*Sachin Chauhan End : 11 01 2015*/
        #endregion


        #region Font Deco
        protected global::System.Web.UI.WebControls.CheckBox customAccordionTextFD, customCurrencyFD, customCurrencyIconFD, customLabelFD, customTableHeaderTitleFD,
            customTableHeadFD, customTableHeadingTextFD, customTableTextFD, customTabsAFD, footerLinkActiveFD, FooterLinkFD, FooterTextFD, HeaderLinkFD,
            HyperLnkFD, MenuLinkFD, PageHeadingFD, pageHighlightTextFD, PageSubTitleFD, PageTextFD, PageTitleFD, ProductCodeFD, productDescriptionFD,
            ProductNameFD, ProductPriceFD, ProductSmlNameFD, ProductTitleFD, HyperLnkSmlHoverFD, pdHyperLnkHoverFD, pdHyperLnkFD, BreadcrumbHoverFD, BeradcrumbFD,
            HyperlinkHoverFD, HyperLnkSmlFD, PageHeading1FD, PageHeading2FD, PageHeading3FD, PageSmlTextFD, PageSubSubTitleFD, PageScrollLinksHoverFD, PageScrollLinksFD;
        #endregion

        protected global::System.Web.UI.WebControls.Button btnSave;

        #region Font Weight
        protected global::System.Web.UI.HtmlControls.HtmlSelect customAccordionTextFW, customCurrencyFW, customCurrencyIconFW, customInputFW, customLabelFW, customTableHeaderTitleFW,
            customTableHeadFW, customTableHeadingTextFW, customTableTextFW, customTabsAFW, footerLinkActiveFW, FooterLinkFW, FooterTextFW,
            HeaderLinkFW, HyperLnkFW, MenuLinkFW, PageHeadingFW, pageHighlightTextFW, PageSubTitleFW, PageTextFW, PageTitleFW, ProductCodeFW
            , productDescriptionFW, ProductNameFW, ProductPriceFW, ProductSmlNameFW, ProductTitleFW, pdHyperLnkFW, HyperLnkSmlFW
            , PageHeading1FW, PageHeading2FW, PageHeading3FW, PageSmlTextFW, PageSubSubTitleFW, PageScrollLinksFW
            , BeradcrumbFW;
        #endregion

        #region FStyle
        protected global::System.Web.UI.WebControls.CheckBox customAccordionTextFStyle, customCurrencyFStyle, customCurrencyIconFStyle, customLabelFStyle,
            customTableHeaderTitleFStyle, customTableHeadFStyle, customTableHeadingTextFStyle, customTableTextFStyle, customTabsAFStyle,
            footerLinkActiveFStyle, FooterLinkFFStyle, FooterTextFFStyle, HeaderLinkFStyle, HyperLnkFFStyle, MenuLinkFFStyle,
            PageHeadingFStyle, pageHighlightTextFStyle, PageSubTitleFStyle, PageTextFStyle, PageTitleFStyle, ProductCodeFStyle,
            productDescriptionFStyle, ProductNameFStyle, ProductPriceFStyle, ProductSmlNameFStyle, ProductTitleFStyle, pdHyperLnkFFStyle, BeradcrumbFStyle,
            HyperLnkSmlFFStyle, PageHeading1FStyle, PageHeading2FStyle, PageHeading3FStyle, PageSmlTextFStyle, PageSubSubTitleFStyle, PageScrollLinksFStyle;
        #endregion



        public static string host = GlobalFunctions.GetVirtualPathAdmin();
        public static BrandColorManagementBE objBrandColorManagementBE;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    CallBindBrandColorDropDown();
                    BindThemeConfigurator();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void CallBindBrandColorDropDown()
        {
            try
            {
                #region
                List<HtmlSelect> lstBrandColorDDL = new List<HtmlSelect>();
                #region "Header"
                lstBrandColorDDL.Add(HeaderLinkColor);
                lstBrandColorDDL.Add(HeaderLinkHoverColor);
                lstBrandColorDDL.Add(HeaderLinkActiveColor);
                #endregion

                lstBrandColorDDL.Add(customAccordionBgColor);
                lstBrandColorDDL.Add(customAccordionColor);
                lstBrandColorDDL.Add(customAccordionTextActiveColor);
                lstBrandColorDDL.Add(customAccordionTextColor);
                lstBrandColorDDL.Add(customActionBtnColor);
                lstBrandColorDDL.Add(customActionBtnHoverColor);
                lstBrandColorDDL.Add(customButtonColor);
                lstBrandColorDDL.Add(customButtonHoverColor);
                lstBrandColorDDL.Add(customCurrencyIconColor);
                lstBrandColorDDL.Add(liDropDownMenuActiveBgColor);
                lstBrandColorDDL.Add(HeaderLinkDropDownMenuActiveBgColor);
                lstBrandColorDDL.Add(customCloseColorBg);
                lstBrandColorDDL.Add(customCloseHoverColorBg);
                                
                //lstBrandColorDDL.Add(NavActiveColor);
                //lstBrandColorDDL.Add(NavActiveBgColor);
                //lstBrandColorDDL.Add(customCurrencyIconColorFF);
                lstBrandColorDDL.Add(customCurrencyIconColorHover);
                lstBrandColorDDL.Add(customCurrencyTextColor);
                lstBrandColorDDL.Add(customCurrencyTextColorHover);
                lstBrandColorDDL.Add(customInputBorderColor);
                lstBrandColorDDL.Add(customInputColor);
                lstBrandColorDDL.Add(customLabelColor);
                lstBrandColorDDL.Add(customPanelColor);
                lstBrandColorDDL.Add(customRatingsContainerColor);
                lstBrandColorDDL.Add(customRatingsStarColor);
                lstBrandColorDDL.Add(customResetBtnColor);
                lstBrandColorDDL.Add(customResetBtnHoverColor);
                lstBrandColorDDL.Add(customTableHeadColor);
                lstBrandColorDDL.Add(customTableHeadingTextColor);
                lstBrandColorDDL.Add(customTableTextColor);
                //lstBrandColorDDL.Add(customTabsAActiveColor);Commented by Sripal as  it is commented and no control in aspx
                //lstBrandColorDDL.Add(customTabsAActiveHoverColor);Commented by Sripal as  it is commented and no control in aspx
                lstBrandColorDDL.Add(customTabsAColor);
                lstBrandColorDDL.Add(customTabsAHoverColor);
                lstBrandColorDDL.Add(customToggleBgColor);
                lstBrandColorDDL.Add(customToggleBgHoverColor);
                lstBrandColorDDL.Add(customToggleColor);
                lstBrandColorDDL.Add(customToggleHoverColor);
                //lstBrandColorDDL.Add(footerLinkActiveColor);
                lstBrandColorDDL.Add(FooterLinkcolor);
                lstBrandColorDDL.Add(FooterLinkHoverColor);
                lstBrandColorDDL.Add(FooterTextColor);
                lstBrandColorDDL.Add(HyperLnkColor);
                lstBrandColorDDL.Add(pdHyperLnkColor);
                lstBrandColorDDL.Add(HyperLnkHoverColor);
                lstBrandColorDDL.Add(pdHyperLnkHoverColor);
                lstBrandColorDDL.Add(liDropDownMenuActiveColor);
                lstBrandColorDDL.Add(HeaderLinkDropDownMenuActiveColor);
                lstBrandColorDDL.Add(liDropDownMenuColor);
                lstBrandColorDDL.Add(HeaderLinkDropDownMenuColor);
                lstBrandColorDDL.Add(liDropDownMenuHoverColor);
                lstBrandColorDDL.Add(HeaderLinkDropDownMenuHoverColor);
                lstBrandColorDDL.Add(MenuLinkColor);
                lstBrandColorDDL.Add(MenuLinkHoverColor);
                lstBrandColorDDL.Add(OwlButtonsColor);
                lstBrandColorDDL.Add(OwlButtonsHoverColor);
                lstBrandColorDDL.Add(OwlCarouselBorderColor);
                lstBrandColorDDL.Add(PageHeadingColor);
                lstBrandColorDDL.Add(pageHighlightTextColor);
                lstBrandColorDDL.Add(PageSubTitleColor);
                lstBrandColorDDL.Add(PageTextColor);
                lstBrandColorDDL.Add(PageTitleColor);
                lstBrandColorDDL.Add(ProductBorderHoverProductNameColor);
                lstBrandColorDDL.Add(ProductCodeColor);
                lstBrandColorDDL.Add(productDescriptionColor);
                lstBrandColorDDL.Add(ProductNameColor);
                lstBrandColorDDL.Add(ProductPriceColor);
                lstBrandColorDDL.Add(ProductSmlNameColor);
                lstBrandColorDDL.Add(ProductTitleColor);
                lstBrandColorDDL.Add(HeaderBg);
                //lstBrandColorDDL.Add(HeaderLinkDropDownBg); //uncommeneted by vikram to disable highlight on myacounnt
                lstBrandColorDDL.Add(HeaderLinkPanelDropDownActiveItemBg);
                ///////////////////////////
                lstBrandColorDDL.Add(liDropDownBg);
                lstBrandColorDDL.Add(liDropDownMenuHoverBg);
                lstBrandColorDDL.Add(HeaderLinkDropDownMenuHoverBg);
                lstBrandColorDDL.Add(BreadcrumbBgColor);
                lstBrandColorDDL.Add(BreadcrumbColor);
                lstBrandColorDDL.Add(BreadcrumbHoverColor);
                lstBrandColorDDL.Add(BreadcrumbActiveColor);
                lstBrandColorDDL.Add(HyperLnkSmlColor);
                lstBrandColorDDL.Add(MenuBg);
                lstBrandColorDDL.Add(HyperLnkSmlHoverColor);
                lstBrandColorDDL.Add(PageHeading1Color);
                lstBrandColorDDL.Add(PageHeading2Color);
                lstBrandColorDDL.Add(PageHeading3Color);
                lstBrandColorDDL.Add(PageTitleBg);
                lstBrandColorDDL.Add(PageSmlTextColor);
                lstBrandColorDDL.Add(PageSubSubTitleColor);
                lstBrandColorDDL.Add(PageScrollLinksColor);
                lstBrandColorDDL.Add(PageScrollLinksHoverColor);
                lstBrandColorDDL.Add(customButtonBg);
                lstBrandColorDDL.Add(customButtonHoverBg);
                lstBrandColorDDL.Add(customButtonBorderColor);
                lstBrandColorDDL.Add(customButtonBorderHoverColor);
                lstBrandColorDDL.Add(customActionBtnBg);
                lstBrandColorDDL.Add(customActionBtnHoverBg);
                lstBrandColorDDL.Add(customActionBtnBorderHoverColor);
                lstBrandColorDDL.Add(customResetBtnBg);
                lstBrandColorDDL.Add(PageHeadingBg);
                lstBrandColorDDL.Add(ProductBorder);
                lstBrandColorDDL.Add(ProductBorderHover);
                lstBrandColorDDL.Add(customTableBorder);
                lstBrandColorDDL.Add(TabBg);
                lstBrandColorDDL.Add(customTabsAHoverBg);
                lstBrandColorDDL.Add(customTabsABg);
                lstBrandColorDDL.Add(customPanelBg);
                lstBrandColorDDL.Add(OwlCarouselBg);
                lstBrandColorDDL.Add(OwlButtonsBg);
                lstBrandColorDDL.Add(OwlButtonsHoverBg);
                lstBrandColorDDL.Add(FooterBg);
                lstBrandColorDDL.Add(BasketCountBgColor);
                lstBrandColorDDL.Add(customActionBtnBorderColor);
                lstBrandColorDDL.Add(customResetBtnHoverBorderColor);
                lstBrandColorDDL.Add(customTableHeaderTitleBgColor);
                lstBrandColorDDL.Add(customTableHeaderTitleColor);
                lstBrandColorDDL.Add(customTabsActiveColor);
                lstBrandColorDDL.Add(customTabsActiveBg);
                lstBrandColorDDL.Add(customPanelHeadingBgColor);
                lstBrandColorDDL.Add(customPanelHeadingColor);
                lstBrandColorDDL.Add(customPanelBorderColor);
                lstBrandColorDDL.Add(customRatingsContainerBgColor);
                lstBrandColorDDL.Add(customResetBtnBorderColor);
                lstBrandColorDDL.Add(customResetBtnHoverBg);

                /*Sachin Chauhan Start : 11 01 2015 : Added for Dropdown BG color*/
                lstBrandColorDDL.Add(HeaderLinkPanelDropDownBg);
                lstBrandColorDDL.Add(ProductCategoryDropDownMenuPanelBg);
                /*Sachin Chauhan End : 11 01 2015*/

                #region "Added by Sripal"
                lstBrandColorDDL.Add(HeaderLinkDropDownMenuActiveBgColor);
                lstBrandColorDDL.Add(liDropDownBg);

                lstBrandColorDDL.Add(SearchIconTxtColor);
                lstBrandColorDDL.Add(BasketIconTxtColor);
                lstBrandColorDDL.Add(SearchIconHoverColorBg);
                lstBrandColorDDL.Add(BasketIconHoverColorBg);
                lstBrandColorDDL.Add(StrikePriceTxtColor);
                lstBrandColorDDL.Add(BasketCountTxtColor);
                #endregion

                for (int i = 0; i < lstBrandColorDDL.Count; i++)
                {
                    BindBrandColorDropDown(lstBrandColorDDL[i]);
                }
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindBrandColorDropDown(HtmlSelect ControlName)
        {
            List<BrandColorManagementBE> lstBrandColorManagementBE = new List<BrandColorManagementBE>();
            try
            {
                lstBrandColorManagementBE = BrandColorManagementBL.GetAllBrandColors();
                lstBrandColorManagementBE = lstBrandColorManagementBE.FindAll(x => x.IsActive == true);
                if (lstBrandColorManagementBE != null)
                {
                    if (lstBrandColorManagementBE.Count > 0)
                    {
                        ControlName.DataSource = lstBrandColorManagementBE;
                        ControlName.DataTextField = "ColorName";
                        ControlName.DataValueField = "ColorHexCode";
                        ControlName.DataBind();
                        foreach (ListItem myItem in ControlName.Items)
                        {
                            //Do some things to determine the color of the item
                            //Set the item background-color like so:
                            myItem.Attributes.Add("style", "background-color:" + myItem.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindThemeConfigurator()
        {
            ThemeConfiguratorBE objThemeConfiguratorBE = new ThemeConfiguratorBE();
            List<ThemeConfiguratorBE> lstThemeConfiguratorBE = new List<ThemeConfiguratorBE>();
            try
            {
                lstThemeConfiguratorBE = ThemeConfiguratorBL.GetAllThemeConfiguratorElemnts();
                if (lstThemeConfiguratorBE != null)
                {
                    if (lstThemeConfiguratorBE.Count > 0)
                    {

                        #region "Header"
                        HeaderLinkColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkColor@").KeyValue;
                        HeaderLinkHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkHoverColor@").KeyValue;
                        HeaderLinkActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkActiveColor@").KeyValue;
                        HeaderLinkFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkFF@").KeyValue;
                        HeaderLinkFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkFS@").KeyValue;
                        HeaderLinkFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkFW@").KeyValue;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkFStyle@").KeyValue.ToLower() == "italic")
                            HeaderLinkFStyle.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkFD@").KeyValue.ToLower() == "underline")
                            HeaderLinkFD.Checked = true;

                        #region "Header-> Dropdown Menu
                        HeaderLinkDropDownMenuColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropDownMenuColor@").KeyValue;
                        HeaderLinkDropDownMenuHoverBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropDownMenuHoverBg@").KeyValue;
                        HeaderLinkDropDownMenuHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropDownMenuHoverColor@").KeyValue;
                        HeaderLinkDropDownMenuActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropDownMenuActiveColor@").KeyValue;
                        HeaderLinkDropDownMenuActiveBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropDownMenuActiveBgColor@").KeyValue;
                        HeaderLinkDropDownMenFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropDownMenFS@").KeyValue;
                        HeaderLinkMenuLinkFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkMenuLinkFW@").KeyValue;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropdownFStyle@").KeyValue.ToLower() == "italic")
                            HeaderLinkDropdownFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropdownFD@").KeyValue.ToLower() == "underline")
                            HeaderLinkDropdownFD.Checked = true;
                        HeaderLinkPanelDropDownBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName.Equals("@HeaderLinkPanelDropDownBg@")).KeyValue;

                        #endregion
                        #endregion

                        customResetBtnBorderColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customResetBtnBorderColor@").KeyValue;

                        customActionBtnBorderColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customActionBtnBorderColor@").KeyValue;
                        liDropDownMenuActiveBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenuActiveBgColor@").KeyValue;

                        customCloseColorBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCloseColorBg@").KeyValue;
                        customCloseFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCloseFS@").KeyValue;
                        customCloseHoverColorBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCloseHoverColorBg@").KeyValue;
                                                 
                        customResetBtnHoverBorderColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customResetBtnHoverBorderColor@").KeyValue;
                        customTableHeaderTitleBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeaderTitleBgColor@").KeyValue;
                        customTableHeaderTitleColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeaderTitleColor@").KeyValue;
                        customTabsActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsActiveColor@").KeyValue;
                        customTabsActiveBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsActiveBg@").KeyValue;
                        customPanelHeadingBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customPanelHeadingBgColor@").KeyValue;
                        customPanelHeadingColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customPanelHeadingColor@").KeyValue;
                        customPanelBorderColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customPanelBorderColor@").KeyValue;
                        customRatingsContainerBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customRatingsContainerBgColor@").KeyValue;

                        customTableBorder.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableBorder@").KeyValue;
                        PageHeadingBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeadingBg@").KeyValue;
                        customActionBtnBorderHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customActionBtnBorderHoverColor@").KeyValue;
                        customActionBtnBorderColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customActionBtnBorderColor@").KeyValue;
                        customButtonBorderHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customButtonBorderHoverColor@").KeyValue;
                        customButtonBorderColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customButtonBorderColor@").KeyValue;
                        PageScrollLinksHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageScrollLinksHoverColor@").KeyValue;
                        PageScrollLinksColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageScrollLinksColor@").KeyValue;
                        PageSubSubTitleColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubSubTitleColor@").KeyValue;
                        PageSmlTextColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSmlTextColor@").KeyValue;
                        PageHeading3Color.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading3Color@").KeyValue;
                        PageHeading2Color.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading2Color@").KeyValue;
                        PageHeading1Color.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading1Color@").KeyValue;
                        HyperLnkSmlHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkSmlHoverColor@").KeyValue;
                        HyperLnkSmlColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkSmlColor@").KeyValue;
                        customTableHeaderTitleFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeaderTitleFS@").KeyValue;
                        BreadcrumbActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BreadcrumbActiveColor@").KeyValue;
                        BreadcrumbHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BreadcrumbHoverColor@").KeyValue;
                        BreadcrumbColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BreadcrumbColor@").KeyValue;
                        BreadcrumbBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BreadcrumbBgColor@").KeyValue;
                        customAccordionBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionBgColor@").KeyValue;
                        customAccordionColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionColor@").KeyValue;
                        customAccordionTextActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionTextActiveColor@").KeyValue;
                        customAccordionTextColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionTextColor@").KeyValue;
                        customAccordionTextFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionTextFF@").KeyValue;
                        customAccordionTextFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionTextFS@").KeyValue;
                        liDropDownMenFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenFS@").KeyValue;
                        liDropDownMenFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenFW@").KeyValue;
                        BeradcrumbFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BeradcrumbFS@").KeyValue;
                        HyperLnkSmlFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkSmlFS@").KeyValue;
                        PageHeading1FS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading1FS@").KeyValue;
                        PageHeading2FS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading2FS@").KeyValue;
                        PageHeading3FS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading3FS@").KeyValue;
                        PageSmlTextFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSmlTextFS@").KeyValue;
                        PageSubSubTitleFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubSubTitleFS@").KeyValue;
                        PageScrollLinksFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageScrollLinksFS@").KeyValue;
                        customActionBtnBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customActionBtnBg@").KeyValue;
                        customActionBtnColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customActionBtnColor@").KeyValue;
                        customActionBtnHoverBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customActionBtnHoverBg@").KeyValue;
                        customActionBtnHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customActionBtnHoverColor@").KeyValue;
                        customButtonBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customButtonBg@").KeyValue;
                        customButtonColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customButtonColor@").KeyValue;
                        customButtonHoverBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customButtonHoverBg@").KeyValue;
                        customButtonHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customButtonHoverColor@").KeyValue;
                        customCurrencyFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyFS@").KeyValue;
                        customCurrencyIconColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyIconColor@").KeyValue;
                        customCurrencyIconColorFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyIconColorFF@").KeyValue;
                        customCurrencyIconColorHover.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyIconColorHover@").KeyValue;
                        customCurrencyIconFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyIconFS@").KeyValue;
                        customCurrencyTextColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyTextColor@").KeyValue;
                        customCurrencyTextColorHover.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyTextColorHover@").KeyValue;
                        customCurrencyTextFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyTextFF@").KeyValue;
                        customInputBorderColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customInputBorderColor@").KeyValue;
                        customInputColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customInputColor@").KeyValue;
                        customInputFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customInputFF@").KeyValue;
                        customInputFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customInputFS@").KeyValue;
                        customLabelColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customLabelColor@").KeyValue;
                        customLabelFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customLabelFF@").KeyValue;
                        customLabelFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customLabelFS@").KeyValue;
                        customPanelBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customPanelBg@").KeyValue;
                        customPanelColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customPanelColor@").KeyValue;
                        customRatingsContainerColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customRatingsContainerColor@").KeyValue;
                        customRatingsStarColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customRatingsStarColor@").KeyValue;
                        customResetBtnBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customResetBtnBg@").KeyValue;
                        customResetBtnColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customResetBtnColor@").KeyValue;
                        customResetBtnHoverBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customResetBtnHoverBg@").KeyValue;
                        customResetBtnHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customResetBtnHoverColor@").KeyValue;
                        customTableHeadColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadColor@").KeyValue;
                        customTableHeadFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadFF@").KeyValue;
                        customTableHeadFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadFS@").KeyValue;
                        customTableHeadingTextColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadingTextColor@").KeyValue;
                        customTableHeadingTextFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadingTextFF@").KeyValue;
                        customTableHeadingTextFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadingTextFS@").KeyValue;
                        customTableTextColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableTextColor@").KeyValue;
                        customTableTextFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableTextFF@").KeyValue;
                        customTableTextFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableTextFS@").KeyValue;

                        customTabsABg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsABg@").KeyValue;
                        customTabsAColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAColor@").KeyValue;
                        customTabsAFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAFF@").KeyValue;
                        customTabsAFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAFS@").KeyValue;
                        customTabsAHoverBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAHoverBg@").KeyValue;
                        customTabsAHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAHoverColor@").KeyValue;
                        customToggleBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customToggleBgColor@").KeyValue;
                        customToggleBgHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customToggleBgHoverColor@").KeyValue;
                        customToggleColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customToggleColor@").KeyValue;
                        customToggleHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customToggleHoverColor@").KeyValue;
                        FooterBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterBg@").KeyValue;
                        BasketCountBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BasketCountBgColor@").KeyValue;		

                        FooterLinkcolor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterLinkcolor@").KeyValue;
                        FooterLinkFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterLinkFF@").KeyValue;
                        FooterLinkFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterLinkFS@").KeyValue;
                        FooterLinkHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterLinkHoverColor@").KeyValue;
                        FooterTextColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterTextColor@").KeyValue;
                        FooterTextFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterTextFF@").KeyValue;
                        FooterTextFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterTextFS@").KeyValue;
                        HeaderBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderBg@").KeyValue;
                        liDropDownBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownBg@").KeyValue;
                        HyperLnkColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkColor@").KeyValue;
                        pdHyperLnkColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pdHyperLnkColor@").KeyValue;
                        HyperLnkFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkFF@").KeyValue;
                        HyperLnkFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkFS@").KeyValue;
                        pdHyperLnkFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pdHyperLnkFS@").KeyValue;

                        HyperLnkHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkHoverColor@").KeyValue;
                        pdHyperLnkHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pdHyperLnkHoverColor@").KeyValue;
                        liDropDownMenuActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenuActiveColor@").KeyValue;
                        liDropDownMenuColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenuColor@").KeyValue;
                        liDropDownMenuHoverBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenuHoverBg@").KeyValue;
                        liDropDownMenuHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenuHoverColor@").KeyValue;
                        MenuBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@MenuBg@").KeyValue;
                        MenuLinkColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@MenuLinkColor@").KeyValue;
                        MenuLinkFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@MenuLinkFF@").KeyValue;
                        MenuLinkFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@MenuLinkFS@").KeyValue;
                        MenuLinkHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@MenuLinkHoverColor@").KeyValue;
                        OwlButtonsBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@OwlButtonsBg@").KeyValue;
                        OwlButtonsColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@OwlButtonsColor@").KeyValue;
                        OwlButtonsHoverBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@OwlButtonsHoverBg@").KeyValue;
                        OwlButtonsHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@OwlButtonsHoverColor@").KeyValue;
                        OwlCarouselBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@OwlCarouselBg@").KeyValue;
                        OwlCarouselBorderColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@OwlCarouselBorderColor@").KeyValue;
                        PageHeadingColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeadingColor@").KeyValue;
                        PageHeadingFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeadingFF@").KeyValue;
                        PageHeadingFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeadingFS@").KeyValue;
                        pageHighlightTextColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pageHighlightTextColor@").KeyValue;
                        pageHighlightTextFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pageHighlightTextFF@").KeyValue;
                        pageHighlightTextFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pageHighlightTextFS@").KeyValue;

                        PageSubTitleColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubTitleColor@").KeyValue;
                        PageSubTitleFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubTitleFF@").KeyValue;
                        PageSubTitleFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubTitleFS@").KeyValue;

                        PageTextColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTextColor@").KeyValue;
                        PageTextFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTextFF@").KeyValue;
                        PageTextFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTextFS@").KeyValue;
                        PageTitleBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTitleBg@").KeyValue;
                        PageTitleColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTitleColor@").KeyValue;
                        PageTitleFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTitleFF@").KeyValue;
                        PageTitleFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTitleFS@").KeyValue;
                        ProductBorder.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductBorder@").KeyValue;
                        ProductBorderHover.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductBorderHover@").KeyValue;
                        ProductBorderHoverProductNameColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductBorderHoverProductNameColor@").KeyValue;
                        ProductCodeColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductCodeColor@").KeyValue;
                        ProductCodeFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductCodeFF@").KeyValue;
                        ProductCodeFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductCodeFS@").KeyValue;
                        productDescriptionColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@productDescriptionColor@").KeyValue;
                        productDescriptionFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@productDescriptionFF@").KeyValue;
                        productDescriptionFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@productDescriptionFS@").KeyValue;
                        ProductNameColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductNameColor@").KeyValue;
                        ProductNameFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductNameFF@").KeyValue;
                        ProductNameFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductNameFS@").KeyValue;
                        ProductPriceColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductPriceColor@").KeyValue;
                        ProductPriceFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductPriceFF@").KeyValue;
                        ProductPriceFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductPriceFS@").KeyValue;
                        ProductSmlNameColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductSmlNameColor@").KeyValue;
                        ProductSmlNameFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductSmlNameFF@").KeyValue;
                        ProductSmlNameFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductSmlNameFS@").KeyValue;
                        ProductTitleColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductTitleColor@").KeyValue;
                        ProductTitleFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductTitleFF@").KeyValue;
                        ProductTitleFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductTitleFS@").KeyValue;
                        TabBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@TabBg@").KeyValue;
                        customAccordionTextFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionTextFW@").KeyValue;
                        customCurrencyFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyFW@").KeyValue;
                        customCurrencyIconFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyIconFW@").KeyValue;
                        customInputFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customInputFW@").KeyValue;
                        customLabelFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customLabelFW@").KeyValue;
                        customTableHeaderTitleFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeaderTitleFW@").KeyValue;
                        customTableHeadFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadFW@").KeyValue;
                        customTableHeadingTextFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadingTextFW@").KeyValue;
                        customTableTextFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableTextFW@").KeyValue;
                        customTabsAFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAFW@").KeyValue;

                        FooterLinkFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterLinkFW@").KeyValue;
                        FooterTextFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterTextFW@").KeyValue;

                        HyperLnkFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkFW@").KeyValue;
                        HyperLnkSmlFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkSmlFW@").KeyValue;

                        pdHyperLnkFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pdHyperLnkFW@").KeyValue;
                        BeradcrumbFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BeradcrumbFW@").KeyValue;

                        MenuLinkFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@MenuLinkFW@").KeyValue;
                        PageHeadingFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeadingFW@").KeyValue;
                        PageHeading1FW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading1FW@").KeyValue;
                        PageHeading2FW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading2FW@").KeyValue;
                        PageHeading3FW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading3FW@").KeyValue;
                        string sss = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSmlTextFW@").KeyValue;
                        PageSmlTextFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSmlTextFW@").KeyValue;
                        PageSubSubTitleFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubSubTitleFW@").KeyValue;
                        PageScrollLinksFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageScrollLinksFW@").KeyValue;
                        pageHighlightTextFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pageHighlightTextFW@").KeyValue;
                        PageSubTitleFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubTitleFW@").KeyValue;
                        PageTextFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTextFW@").KeyValue;
                        PageTitleFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTitleFW@").KeyValue;
                        ProductCodeFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductCodeFW@").KeyValue;
                        productDescriptionFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@productDescriptionFW@").KeyValue;
                        ProductNameFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductNameFW@").KeyValue;
                        ProductPriceFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductPriceFW@").KeyValue;
                        ProductSmlNameFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductSmlNameFW@").KeyValue;
                        ProductTitleFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductTitleFW@").KeyValue;
                        ProductCategoryDropDownMenuPanelBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName.Equals("@ProductCategoryDropDownMenuPanelBg@")).KeyValue;

                        SearchIconTxtColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName.Equals("@SearchIconTxtColor@")).KeyValue;
                        BasketIconTxtColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName.Equals("@BasketIconTxtColor@")).KeyValue;
                        StrikePriceTxtColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName.Equals("@StrikePriceTxtColor@")).KeyValue;
                        BasketCountTxtColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName.Equals("@BasketCountTxtColor@")).KeyValue;
                        SearchIconFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@SearchIconFS@").KeyValue;
                        BasketIconFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BasketIconFS@").KeyValue;
                        SearchIconHoverColorBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@SearchIconHoverColorBg@").KeyValue;
                        BasketIconHoverColorBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BasketIconHoverColorBg@").KeyValue;

                        #region Italic
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionTextFStyle@").KeyValue.ToLower() == "italic")
                            customAccordionTextFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyFStyle@").KeyValue.ToLower() == "italic")
                            customCurrencyFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyIconFStyle@").KeyValue.ToLower() == "italic")
                            customCurrencyIconFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customLabelFStyle@").KeyValue.ToLower() == "italic")
                            customLabelFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeaderTitleFStyle@").KeyValue.ToLower() == "italic")
                            customTableHeaderTitleFStyle.Checked = true;

                        //HeaderLinkDropdownFStyle, HeaderLinkDropdownFD, liDropDownMenFStyle, liDropDownMenFD;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenFStyle@").KeyValue.ToLower() == "italic")
                            liDropDownMenFStyle.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@liDropDownMenFD@").KeyValue.ToLower() == "underline")
                            liDropDownMenFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadFStyle@").KeyValue.ToLower() == "italic")
                            customTableHeadFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadingTextFStyle@").KeyValue.ToLower() == "italic")
                            customTableHeadingTextFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableTextFStyle@").KeyValue.ToLower() == "italic")
                            customTableTextFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAFStyle@").KeyValue.ToLower() == "italic")
                            customTabsAFStyle.Checked = true;
                        //if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@footerLinkActiveFStyle@").KeyValue.ToLower() == "italic")
                        //  footerLinkActiveFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterLinkFFStyle@").KeyValue.ToLower() == "italic")
                            FooterLinkFFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterTextFFStyle@").KeyValue.ToLower() == "italic")
                            FooterTextFFStyle.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BeradcrumbFStyle@").KeyValue.ToLower() == "italic")
                            BeradcrumbFStyle.Checked = true;


                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkFFStyle@").KeyValue.ToLower() == "italic")
                            HyperLnkFFStyle.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkSmlFFStyle@").KeyValue.ToLower() == "italic")
                            HyperLnkSmlFFStyle.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pdHyperLnkFFStyle@").KeyValue.ToLower() == "italic")
                            pdHyperLnkFFStyle.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@MenuLinkFFStyle@").KeyValue.ToLower() == "italic")
                            MenuLinkFFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeadingFStyle@").KeyValue.ToLower() == "italic")
                            PageHeadingFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading1FStyle@").KeyValue.ToLower() == "italic")
                            PageHeading1FStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading2FStyle@").KeyValue.ToLower() == "italic")
                            PageHeading2FStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading3FStyle@").KeyValue.ToLower() == "italic")
                            PageHeading3FStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSmlTextFStyle@").KeyValue.ToLower() == "italic")
                            PageSmlTextFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubSubTitleFStyle@").KeyValue.ToLower() == "italic")
                            PageSubSubTitleFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageScrollLinksFStyle@").KeyValue.ToLower() == "italic")
                            PageScrollLinksFStyle.Checked = true;


                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pageHighlightTextFStyle@").KeyValue.ToLower() == "italic")
                            pageHighlightTextFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubTitleFStyle@").KeyValue.ToLower() == "italic")
                            PageSubTitleFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTextFStyle@").KeyValue.ToLower() == "italic")
                            PageTextFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTitleFStyle@").KeyValue.ToLower() == "italic")
                            PageTitleFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductCodeFStyle@").KeyValue.ToLower() == "italic")
                            ProductCodeFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@productDescriptionFStyle@").KeyValue.ToLower() == "italic")
                            productDescriptionFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductNameFStyle@").KeyValue.ToLower() == "italic")
                            ProductNameFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductPriceFStyle@").KeyValue.ToLower() == "italic")
                            ProductPriceFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductSmlNameFStyle@").KeyValue.ToLower() == "italic")
                            ProductSmlNameFStyle.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductTitleFStyle@").KeyValue.ToLower() == "italic")
                            ProductTitleFStyle.Checked = true;

                        #endregion

                        #region Underline
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkSmlHoverFD@").KeyValue.ToLower() == "underline")
                            HyperLnkSmlHoverFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BreadcrumbHoverFD@").KeyValue.ToLower() == "underline")
                            BreadcrumbHoverFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageScrollLinksHoverFD@").KeyValue.ToLower() == "underline")
                            PageScrollLinksHoverFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperlinkHoverFD@").KeyValue.ToLower() == "underline")
                            HyperlinkHoverFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pdHyperLnkHoverFD@").KeyValue.ToLower() == "underline")
                            pdHyperLnkHoverFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customAccordionTextFD@").KeyValue.ToLower() == "underline")
                            customAccordionTextFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyFD@").KeyValue.ToLower() == "underline")
                            customCurrencyFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customCurrencyIconFD@").KeyValue.ToLower() == "underline")
                            customCurrencyIconFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customLabelFD@").KeyValue.ToLower() == "underline")
                            customLabelFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeaderTitleFD@").KeyValue.ToLower() == "underline")
                            customTableHeaderTitleFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadFD@").KeyValue.ToLower() == "underline")
                            customTableHeadFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableHeadingTextFD@").KeyValue.ToLower() == "underline")
                            customTableHeadingTextFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTableTextFD@").KeyValue.ToLower() == "underline")
                            customTableTextFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAFD@").KeyValue.ToLower() == "underline")
                            customTabsAFD.Checked = true;
                        //if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@footerLinkActiveFD@").KeyValue.ToLower() == "underline")
                        //  footerLinkActiveFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterLinkFD@").KeyValue.ToLower() == "underline")
                            FooterLinkFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@FooterTextFD@").KeyValue.ToLower() == "underline")
                            FooterTextFD.Checked = true;


                        string ss1 = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BeradcrumbFD@").KeyValue;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@BeradcrumbFD@").KeyValue.ToLower() == "underline")
                            BeradcrumbFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkFD@").KeyValue.ToLower() == "underline")
                            HyperLnkFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HyperLnkSmlFD@").KeyValue.ToLower() == "underline")
                            HyperLnkSmlFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pdHyperLnkFD@").KeyValue.ToLower() == "underline")
                            pdHyperLnkFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@MenuLinkFD@").KeyValue.ToLower() == "underline")
                            MenuLinkFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeadingFD@").KeyValue.ToLower() == "underline")
                            PageHeadingFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading1FD@").KeyValue.ToLower() == "underline")
                            PageHeading1FD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading2FD@").KeyValue.ToLower() == "underline")
                            PageHeading2FD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageHeading3FD@").KeyValue.ToLower() == "underline")
                            PageHeading3FD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSmlTextFD@").KeyValue.ToLower() == "underline")
                            PageSmlTextFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubSubTitleFD@").KeyValue.ToLower() == "underline")
                            PageSubSubTitleFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageScrollLinksFD@").KeyValue.ToLower() == "underline")
                            PageScrollLinksFD.Checked = true;

                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pageHighlightTextFD@").KeyValue.ToLower() == "underline")
                            pageHighlightTextFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubTitleFD@").KeyValue.ToLower() == "underline")
                            PageSubTitleFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTextFD@").KeyValue.ToLower() == "underline")
                            PageTextFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageTitleFD@").KeyValue.ToLower() == "underline")
                            PageTitleFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductCodeFD@").KeyValue.ToLower() == "underline")
                            ProductCodeFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@productDescriptionFD@").KeyValue.ToLower() == "underline")
                            productDescriptionFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductNameFD@").KeyValue.ToLower() == "underline")
                            ProductNameFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductPriceFD@").KeyValue.ToLower() == "underline")
                            ProductPriceFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductSmlNameFD@").KeyValue.ToLower() == "underline")
                            ProductSmlNameFD.Checked = true;
                        if (lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@ProductTitleFD@").KeyValue.ToLower() == "underline")
                            ProductTitleFD.Checked = true;

                        #endregion

                        #region "Comments"
                        //NavActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@NavActiveColor@").KeyValue;
                        //NavActiveBgColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@NavActiveBgColor@").KeyValue;
                        //HeaderLinkDropdownFStyle, HeaderLinkDropdownFD, liDropDownMenFStyle, liDropDownMenFD;
                        //customTable.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTable@").KeyValue;
                        //customTabsAActiveBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAActiveBg@").KeyValue;
                        //customTabsAActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAActiveColor@").KeyValue;
                        //customTabsAActiveHoverBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAActiveHoverBg@").KeyValue;
                        //customTabsAActiveHoverColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@customTabsAActiveHoverColor@").KeyValue;
                        //footerLinkActiveColor.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@footerLinkActiveColor@").KeyValue;
                        //footerLinkActiveFF.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@footerLinkActiveFF@").KeyValue;
                        //footerLinkActiveFS.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@footerLinkActiveFS@").KeyValue;
                        #region uncommeneted by vikram to disable highlight on myacounnt
                        //HeaderLinkDropDownBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkDropDownBg@").KeyValue;
                        HeaderLinkPanelDropDownActiveItemBg.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@HeaderLinkPanelDropDownActiveItemBg@").KeyValue;
                        #endregion
                        // pageHighlightTextTT.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@pageHighlightTextTT@").KeyValue;
                        // PageSubTitleTT.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@PageSubTitleTT@").KeyValue;
                        //footerLinkActiveFW.Value = lstThemeConfiguratorBE.FirstOrDefault(x => x.KeyName == "@footerLinkActiveFW@").KeyValue;
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowGeneralAlertMessages(this.Page, "Error Updating Theme");
            }
            finally
            {
                lstThemeConfiguratorBE = null;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region Update Theme Configurator Elements

                #region "Header"
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkColor", HeaderLinkColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkHoverColor", HeaderLinkHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkActiveColor", HeaderLinkActiveColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkFF", HeaderLinkFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkFS", HeaderLinkFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkFW", HeaderLinkFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkFStyle", HeaderLinkFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkFD", HeaderLinkFD.Checked == true ? "underline" : "none");
                #region "Dropdown"
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropDownMenuColor", HeaderLinkDropDownMenuColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropDownMenuHoverBg", HeaderLinkDropDownMenuHoverBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropDownMenuHoverColor", HeaderLinkDropDownMenuHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkPanelDropDownBg", HeaderLinkPanelDropDownBg.Value);
                #endregion
                #endregion
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenFS", liDropDownMenFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropDownMenFS", HeaderLinkDropDownMenFS.Value);//Added by Sripal
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkMenuLinkFW", HeaderLinkMenuLinkFW.Value);//Added by Sripal
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenFW", liDropDownMenFW.Value);//Added by Sripal

                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BeradcrumbFS", BeradcrumbFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkSmlFS", HyperLnkSmlFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading1FS", PageHeading1FS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading2FS", PageHeading2FS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading3FS", PageHeading3FS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSmlTextFS", PageSmlTextFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubSubTitleFS", PageSubSubTitleFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageScrollLinksFS", PageScrollLinksFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeaderTitleFS", customTableHeaderTitleFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenuActiveBgColor", liDropDownMenuActiveBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropDownMenuActiveBgColor", HeaderLinkDropDownMenuActiveBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCloseColorBg", customCloseColorBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCloseFS", customCloseFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCloseHoverColorBg", customCloseHoverColorBg.Value);                                
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customResetBtnBorderColor", customResetBtnBorderColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customActionBtnBorderColor", customActionBtnBorderColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customResetBtnHoverBorderColor", customResetBtnHoverBorderColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeaderTitleBgColor", customTableHeaderTitleBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeaderTitleColor", customTableHeaderTitleColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsActiveColor", customTabsActiveColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsActiveBg", customTabsActiveBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customPanelHeadingBgColor", customPanelHeadingBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customPanelHeadingColor", customPanelHeadingColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customPanelBorderColor", customPanelBorderColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customRatingsContainerBgColor", customRatingsContainerBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableBorder", customTableBorder.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeadingBg", PageHeadingBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customActionBtnBorderHoverColor", customActionBtnBorderHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customActionBtnBorderColor", customActionBtnBorderColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customButtonBorderHoverColor", customButtonBorderHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customButtonBorderColor", customButtonBorderColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageScrollLinksHoverColor", PageScrollLinksHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageScrollLinksColor", PageScrollLinksColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubSubTitleColor", PageSubSubTitleColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSmlTextColor", PageSmlTextColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading3Color", PageHeading3Color.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading2Color", PageHeading2Color.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading1Color", PageHeading1Color.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkSmlHoverColor", HyperLnkSmlHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkSmlColor", HyperLnkSmlColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BreadcrumbActiveColor", BreadcrumbActiveColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BreadcrumbHoverColor", BreadcrumbHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BreadcrumbColor", BreadcrumbColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BreadcrumbBgColor", BreadcrumbBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionTextFW", customAccordionTextFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyFW", customCurrencyFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyIconFW", customCurrencyIconFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customInputFW", customInputFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customLabelFW", customLabelFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeaderTitleFW", customTableHeaderTitleFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadFW", customTableHeadFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadingTextFW", customTableHeadingTextFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableTextFW", customTableTextFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAFW", customTabsAFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterLinkFW", FooterLinkFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterTextFW", FooterTextFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkFW", HyperLnkFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkSmlFW", HyperLnkFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pdHyperLnkFW", pdHyperLnkFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BeradcrumbFW", BeradcrumbFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("MenuLinkFW", MenuLinkFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeadingFW", PageHeadingFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading1FW", PageHeading1FW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading2FW", PageHeading2FW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading3FW", PageHeading3FW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSmlTextFW", PageSmlTextFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubSubTitleFW", PageSubSubTitleFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageScrollLinksFW", PageScrollLinksFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pageHighlightTextFW", pageHighlightTextFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubTitleFW", PageSubTitleFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTextFW", PageTextFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTitleFW", PageTitleFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductCodeFW", ProductCodeFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("productDescriptionFW", productDescriptionFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductNameFW", ProductNameFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductPriceFW", ProductPriceFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductSmlNameFW", ProductSmlNameFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductTitleFW", ProductTitleFW.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionBgColor", customAccordionBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionColor", customAccordionColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionTextActiveColor", customAccordionTextActiveColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionTextColor", customAccordionTextColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionTextFF", customAccordionTextFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionTextFS", customAccordionTextFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customActionBtnBg", customActionBtnBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customActionBtnColor", customActionBtnColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customActionBtnHoverBg", customActionBtnHoverBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customActionBtnHoverColor", customActionBtnHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customButtonBg", customButtonBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customButtonColor", customButtonColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customButtonHoverBg", customButtonHoverBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customButtonHoverColor", customButtonHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyFS", customCurrencyFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyIconColor", customCurrencyIconColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyIconColorFF", customCurrencyIconColorFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyIconColorHover", customCurrencyIconColorHover.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyIconFS", customCurrencyIconFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyTextColor", customCurrencyTextColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyTextColorHover", customCurrencyTextColorHover.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyTextFF", customCurrencyTextFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customInputBorderColor", customInputBorderColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customInputColor", customInputColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customInputFF", customInputFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customInputFS", customInputFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customLabelColor", customLabelColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customLabelFF", customLabelFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customLabelFS", customLabelFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customPanelBg", customPanelBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customPanelColor", customPanelColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customRatingsContainerColor", customRatingsContainerColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customRatingsStarColor", customRatingsStarColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customResetBtnBg", customResetBtnBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customResetBtnColor", customResetBtnColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customResetBtnHoverBg", customResetBtnHoverBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customResetBtnHoverColor", customResetBtnHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadColor", customTableHeadColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadFF", customTableHeadFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadFS", customTableHeadFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadingTextColor", customTableHeadingTextColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadingTextFF", customTableHeadingTextFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadingTextFS", customTableHeadingTextFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableTextColor", customTableTextColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableTextFF", customTableTextFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableTextFS", customTableTextFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsABg", customTabsABg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAColor", customTabsAColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAFF", customTabsAFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAFS", customTabsAFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAHoverBg", customTabsAHoverBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAHoverColor", customTabsAHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customToggleBgColor", customToggleBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customToggleBgHoverColor", customToggleBgHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customToggleColor", customToggleColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customToggleHoverColor", customToggleHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterBg", FooterBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BasketCountBgColor", BasketCountBgColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterLinkcolor", FooterLinkcolor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterLinkFF", FooterLinkFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterLinkFS", FooterLinkFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterLinkHoverColor", FooterLinkHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterTextColor", FooterTextColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterTextFF", FooterTextFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterTextFS", FooterTextFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderBg", HeaderBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownBg", liDropDownBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkColor", HyperLnkColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pdHyperLnkColor", pdHyperLnkColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkFF", HyperLnkFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkFS", HyperLnkFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pdHyperLnkFS", pdHyperLnkFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkHoverColor", HyperLnkHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pdHyperLnkHoverColor", pdHyperLnkHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenuActiveColor", liDropDownMenuActiveColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropDownMenuActiveColor", HeaderLinkDropDownMenuActiveColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenuColor", liDropDownMenuColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenuHoverBg", liDropDownMenuHoverBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenuHoverColor", liDropDownMenuHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("MenuBg", MenuBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("MenuLinkColor", MenuLinkColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("MenuLinkFF", MenuLinkFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("MenuLinkFS", MenuLinkFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("MenuLinkHoverColor", MenuLinkHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("OwlButtonsBg", OwlButtonsBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("OwlButtonsColor", OwlButtonsColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("OwlButtonsHoverBg", OwlButtonsHoverBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("OwlButtonsHoverColor", OwlButtonsHoverColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("OwlCarouselBg", OwlCarouselBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("OwlCarouselBorderColor", OwlCarouselBorderColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeadingColor", PageHeadingColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeadingFF", PageHeadingFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeadingFS", PageHeadingFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pageHighlightTextColor", pageHighlightTextColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pageHighlightTextFF", pageHighlightTextFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pageHighlightTextFS", pageHighlightTextFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubTitleColor", PageSubTitleColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubTitleFF", PageSubTitleFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubTitleFS", PageSubTitleFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTextColor", PageTextColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTextFF", PageTextFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTextFS", PageTextFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTitleBg", PageTitleBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTitleColor", PageTitleColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTitleFF", PageTitleFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTitleFS", PageTitleFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductBorder", ProductBorder.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductBorderHover", ProductBorderHover.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductBorderHoverProductNameColor", ProductBorderHoverProductNameColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductCodeColor", ProductCodeColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductCodeFF", ProductCodeFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductCodeFS", ProductCodeFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("productDescriptionColor", productDescriptionColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("productDescriptionFF", productDescriptionFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("productDescriptionFS", productDescriptionFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductNameColor", ProductNameColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductNameFF", ProductNameFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductNameFS", ProductNameFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductPriceColor", ProductPriceColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductPriceFF", ProductPriceFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductPriceFS", ProductPriceFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductSmlNameColor", ProductSmlNameColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductSmlNameFF", ProductSmlNameFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductSmlNameFS", ProductSmlNameFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductTitleColor", ProductTitleColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductTitleFF", ProductTitleFF.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductTitleFS", ProductTitleFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("TabBg", TabBg.Value);

                /*Sachin Chauhan Start: 11 01 2015 : Added for Dropdown BG color*/
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductCategoryDropDownMenuPanelBg", ProductCategoryDropDownMenuPanelBg.Value);
                /*Sachin Chauhan End: 11 01 2015 */
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionTextFStyle", customAccordionTextFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyFStyle", customCurrencyFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyIconFStyle", customCurrencyIconFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customLabelFStyle", customLabelFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeaderTitleFStyle", customTableHeaderTitleFStyle.Checked == true ? "italic" : "normal");
                //HeaderLinkDropdownFStyle, HeaderLinkDropdownFD, liDropDownMenFStyle, liDropDownMenFD;
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropdownFStyle", HeaderLinkDropdownFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenFStyle", liDropDownMenFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropdownFD", HeaderLinkDropdownFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("liDropDownMenFD", liDropDownMenFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadFStyle", customTableHeadFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadingTextFStyle", customTableHeadingTextFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableTextFStyle", customTableTextFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAFStyle", customTabsAFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterLinkFFStyle", FooterLinkFFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterTextFFStyle", FooterTextFFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BeradcrumbFStyle", BeradcrumbFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkFFStyle", HyperLnkFFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkSmlFFStyle", HyperLnkSmlFFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pdHyperLnkFFStyle", pdHyperLnkFFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("MenuLinkFFStyle", MenuLinkFFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeadingFStyle", PageHeadingFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading1FStyle", PageHeading1FStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading2FStyle", PageHeading2FStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading3FStyle", PageHeading3FStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSmlTextFStyle", PageSmlTextFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubSubTitleFStyle", PageSubSubTitleFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageScrollLinksFStyle", PageScrollLinksFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pageHighlightTextFStyle", pageHighlightTextFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubTitleFStyle", PageSubTitleFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTextFStyle", PageTextFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTitleFStyle", PageTitleFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductCodeFStyle", ProductCodeFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("productDescriptionFStyle", productDescriptionFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductNameFStyle", ProductNameFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductPriceFStyle", ProductPriceFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductSmlNameFStyle", ProductSmlNameFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductTitleFStyle", ProductTitleFStyle.Checked == true ? "italic" : "normal");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customAccordionTextFD", customAccordionTextFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyFD", customCurrencyFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customCurrencyIconFD", customCurrencyIconFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customLabelFD", customLabelFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeaderTitleFD", customTableHeaderTitleFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadFD", customTableHeadFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableHeadingTextFD", customTableHeadingTextFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTableTextFD", customTableTextFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAFD", customTabsAFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterLinkFD", FooterLinkFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("FooterTextFD", FooterTextFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BeradcrumbFD", BeradcrumbFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkFD", HyperLnkFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkSmlFD", HyperLnkSmlFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pdHyperLnkFD", pdHyperLnkFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("MenuLinkFD", MenuLinkFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeadingFD", PageHeadingFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading1FD", PageHeading1FD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading2FD", PageHeading2FD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageHeading3FD", PageHeading3FD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSmlTextFD", PageSmlTextFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubSubTitleFD", PageSubSubTitleFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageScrollLinksHoverFD", PageScrollLinksHoverFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageScrollLinksFD", PageScrollLinksFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pageHighlightTextFD", pageHighlightTextFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubTitleFD", PageSubTitleFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTextFD", PageTextFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageTitleFD", PageTitleFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductCodeFD", ProductCodeFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("productDescriptionFD", productDescriptionFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductNameFD", ProductNameFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductPriceFD", ProductPriceFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductSmlNameFD", ProductSmlNameFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("ProductTitleFD", ProductTitleFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperLnkSmlHoverFD", HyperLnkSmlHoverFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BreadcrumbHoverFD", BreadcrumbHoverFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HyperlinkHoverFD", HyperlinkHoverFD.Checked == true ? "underline" : "none");
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pdHyperLnkHoverFD", pdHyperLnkHoverFD.Checked == true ? "underline" : "none");

                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("SearchIconTxtColor", SearchIconTxtColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BasketIconTxtColor", BasketIconTxtColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("StrikePriceTxtColor", StrikePriceTxtColor.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BasketCountTxtColor", BasketCountTxtColor.Value);                  
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("SearchIconFS", SearchIconFS.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BasketIconFS", BasketIconFS.Value);
                
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("SearchIconHoverColorBg", SearchIconHoverColorBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("BasketIconHoverColorBg", BasketIconHoverColorBg.Value);
                
                #region comments
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAActiveBg", customTabsAActiveBg.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAActiveColor", customTabsAActiveColor.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAActiveHoverBg", customTabsAActiveHoverBg.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTabsAActiveHoverColor", customTabsAActiveHoverColor.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("footerLinkActiveFD", footerLinkActiveFD.Checked == true ? "underline" : "none");
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("footerLinkActiveFStyle", footerLinkActiveFStyle.Checked == true ? "italic" : "normal");
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("PageSubTitleTT", PageSubTitleTT.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("pageHighlightTextTT", pageHighlightTextTT.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("footerLinkActiveColor", footerLinkActiveColor.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("footerLinkActiveFF", footerLinkActiveFF.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("footerLinkActiveFS", footerLinkActiveFS.Value);
                #region  commeneted by vikram to disable highlight on myacounnt
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkDropDownBg", HeaderLinkDropDownBg.Value);
                ThemeConfiguratorBL.UpdateThemeConfiguratorElements("HeaderLinkPanelDropDownActiveItemBg", HeaderLinkPanelDropDownActiveItemBg.Value);
                #endregion
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("customTable", customTable.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("footerLinkActiveFW", footerLinkActiveFW.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("NavActiveColor", NavActiveColor.Value);
                //ThemeConfiguratorBL.UpdateThemeConfiguratorElements("NavActiveBgColor", NavActiveBgColor.Value);
                #endregion

                UpdateModifiedCSS();
                #endregion
                CreateActivityLog("TheamConfigurator", "Updated", "");
                BindThemeConfigurator();                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error Updating Theme", AlertType.Failure);
            }
        }

        private void UpdateModifiedCSS()
        {
            ThemeConfiguratorBE objThemeConfiguratorBE = new ThemeConfiguratorBE();
            List<ThemeConfiguratorBE> lstThemeConfiguratorBE = new List<ThemeConfiguratorBE>();
            try
            {
                lstThemeConfiguratorBE = ThemeConfiguratorBL.GetAllThemeConfiguratorElemnts();
                string strFileContent = "";
                string strFileNameModifiedCSS = "modified.css"; //FileName to be backed up and changed
                string strFileNameModifiedMasterCSS = "modifiedMaster.css"; // Filename from content needs to be read and chnaged 
                string strBackupFileName = "modified" + "_" + DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second;
                string strFileExtension = "";
                string strFilePathModifiedCSS = GlobalFunctions.GetPhysicalFolderPath() + "CSS/" + strFileNameModifiedCSS;
                string strFilePathModifiedMasterCSS = GlobalFunctions.GetPhysicalFolderPath() + "CSS/" + strFileNameModifiedMasterCSS;
                if (File.Exists(strFilePathModifiedCSS))
                {
                    strFileExtension = Path.GetExtension(strFilePathModifiedCSS);
                    string strBackupTargetPath = GlobalFunctions.GetPhysicalFolderPath() + "Backup/ModifiedCSS/" + strBackupFileName + strFileExtension;
                    try
                    {
                        //File.Move(strFilePathModifiedCSS, strBackupTargetPath);
                        File.Copy(strFilePathModifiedCSS, strBackupTargetPath, true);
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error Updating Theme", AlertType.Failure);
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Modified.css file does not exists.", AlertType.Failure);
                    return;
                }
                if (File.Exists(strFilePathModifiedMasterCSS))
                {
                    strFileContent = File.ReadAllText(strFilePathModifiedMasterCSS);
                    for (int i = 0; i < lstThemeConfiguratorBE.Count; i++)
                    {
                        strFileContent = strFileContent.Replace(lstThemeConfiguratorBE[i].KeyName, lstThemeConfiguratorBE[i].KeyValue);
                    }
                    if (File.Exists(strFilePathModifiedCSS))
                    {
                        try
                        {
                            File.Delete(strFilePathModifiedCSS);
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while deleting file Modified.css", AlertType.Failure);
                        }
                    }
                    if (!File.Exists(strFilePathModifiedCSS))
                    {
                        try
                        {
                            File.WriteAllText(strFilePathModifiedCSS, strFileContent);
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Error Updating Theme", AlertType.Failure);
                            return;
                        }
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Modified.css file already exists.", AlertType.Failure);
                        return;
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "ModifiedMaster.css file does not exists.", AlertType.Warning);
                    return;
                }
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Theme Saved Successfully", GlobalFunctions.GetVirtualPathAdmin() + "Settings/ThemeConfigurator.aspx", AlertType.Success);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error Updating Theme", AlertType.Failure);
            }
        }

        [WebMethod]
        public static string GetAllBrandColors()
        {
            List<BrandColorManagementBE> lstBrandColorManagementBE = new List<BrandColorManagementBE>();
            JavaScriptSerializer objJavaScriptSerializer = new JavaScriptSerializer();
            string strBrandColors;
            try
            {
                lstBrandColorManagementBE = BrandColorManagementBL.GetAllBrandColors();

                lstBrandColorManagementBE = lstBrandColorManagementBE.Select(x => new BrandColorManagementBE() { ColorName = x.ColorName, ColorHexCode = x.ColorHexCode }).ToList();

                strBrandColors = objJavaScriptSerializer.Serialize(lstBrandColorManagementBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                strBrandColors = null;
            }
            return strBrandColors;
        }
    }
}