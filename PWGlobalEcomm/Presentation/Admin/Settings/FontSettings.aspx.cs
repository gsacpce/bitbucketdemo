﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO.Compression;
using System.Configuration;
using System.Web.Configuration;
using System.Net;
using System.Data;

namespace Presentation
{
    public partial class Admin_Settings_FontSettings : BasePage //System.Web.UI.Page
    {
        public string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.FileUpload fuFonts, fuFontCSS;
        protected global::System.Web.UI.WebControls.DropDownList ddlFontSettingType;
        protected global::System.Web.UI.WebControls.TextBox txtTypekit;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvTypeKit, dvFonts;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvFontCSS;
        protected global::System.Web.UI.WebControls.Button btnTypekitCSS, btnShowCSS;
        protected global::System.Web.UI.WebControls.GridView grdView;
        protected global::System.Web.UI.WebControls.Label lblFileList, lblFailedStatus, lblUploadStatus;
        // Both Typekit or uploaded Fonts will be applicable it will decided by client which one will be used

        // Either Typekit or uploaded Fonts will be applicable not both



        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RemoveUploadedFonts()
        {
            #region Remove Font uploaded
            string strNewFileName = "FontUpload" + ".zip";
            string strFontUploadPath = GlobalFunctions.GetPhysicalFontsPath() + strNewFileName;
            if (File.Exists(strFontUploadPath))
            {
                File.Delete(strFontUploadPath);
            }
            #endregion
            #region Remove Existing Font
            string FontDonotDelete = Convert.ToString(ConfigurationManager.AppSettings["FontDonotDelete"]);
            DirectoryInfo d = new DirectoryInfo(GlobalFunctions.GetPhysicalFontsPath());
            #region For MAC and Other System (Implemented for all OS)
            DirectoryInfo [] darry = d.GetDirectories();
            foreach(DirectoryInfo dinfo in darry)
            {
                if (dinfo.GetFiles().Count() > 1)
                {
                    FileInfo[] f = dinfo.GetFiles();
                    foreach (FileInfo AllFiles in f)
                    {
                        File.Delete(AllFiles.FullName.ToString());
                    }
                }
                dinfo.Delete();
            }
            #endregion
            FileInfo[] infos = d.GetFiles();
            string[] values = FontDonotDelete.Split(',');


            foreach (FileInfo f in infos)
            {
                bool res = false;
                foreach (string filesdonotdelete in values)
                {
                    if (!res)
                    {
                        if (f.Name.ToString().ToLower().Contains(filesdonotdelete))
                        {
                            res = true;
                        }
                    }
                }
                if (!res)
                {
                    File.Delete(f.FullName.ToString());
                }

            }

            #endregion
        }

        protected void btnTypekitSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string strTypekit = txtTypekit.Text;

                //Helps to open the Root level web.config file.
                //Configuration webConfigApp = WebConfigurationManager.OpenWebConfiguration(GlobalFunctions.GetPhysicalFolderPath() + "settings.config");
                //Modifying the Typekit
                //webConfigApp.AppSettings.Settings["Typekit"].Value = strTypekit;
                //Save the Modified settings of AppSettings.
                //webConfigApp.Save(ConfigurationSaveMode.Minimal);
                if (File.Exists(GlobalFunctions.GetPhysicalFontsPath() + "Typekit.txt"))
                {
                    FileInfo fi = new FileInfo(GlobalFunctions.GetPhysicalFontsPath() + "Typekit.txt");
                    using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                    {
                        txtWriter.Write(strTypekit);
                    }
                    //RemoveUploadedFonts();
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Typekit Path Not Found", AlertType.Warning);
                }
                CreateActivityLog("Font Setting", "Updated", "");
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Typekit Path Saved Successfully", AlertType.Success);
                btnTypekitCSS.Visible = true;

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while saving Typekit path, Please try again", AlertType.Failure);
            }
        }

        protected void CreateDataTableStructure()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FileName", typeof(string));
            dt.Columns.Add("FilePath", typeof(string));
            ViewState["griddata"] = dt;
        }
        protected void btnFontSubmit_Click(object sender, EventArgs e)
        {
            #region Fonts Upload using extration of Zip
            //try
            //{
            //    if (fuFonts.HasFiles)
            //    {
            //        if (fuFonts.PostedFile.ContentLength > 0)
            //        {

            //            string[] strFileTypes = { ".zip" };
            //            string[] strMimeTypes = { "application/zip", "application/octet-stream", "application/x-zip-compressed" };
            //            bool bChkFileType = false;
            //            bChkFileType = GlobalFunctions.CheckFileExtension(fuFonts, strFileTypes);
            //            bool bChkFileMimeType = false;
            //            bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(fuFonts, strMimeTypes);
            //            if (!(bChkFileType && bChkFileMimeType))
            //            {
            //                GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
            //                SetFocus(fuFonts.ClientID);
            //                return;
            //            }
            //            string strFileName = fuFonts.FileName;
            //            FileInfo fi = new FileInfo(strFileName);
            //            string strFilExt = fi.Extension;
            //            string strNewFileName = "FontUpload" + strFilExt;
            //            string strFontUploadPath = GlobalFunctions.GetPhysicalFontsUploadPath() + strNewFileName;
            //            string strUploadFolderName = Path.GetFileNameWithoutExtension(strFontUploadPath);
            //            #region CreateFontBackup
            //            //string BackupFolderPath = GlobalFunctions.GetPhysicalFontsPath()+"Backup";
            //            //if(!Directory.Exists(BackupFolderPath))
            //            //{
            //            //    Directory.CreateDirectory(BackupFolderPath);
            //            //}
            //            //Array.ForEach(Directory.GetFiles(GlobalFunctions.GetPhysicalFontsPath() + strUploadFolderName + "_" + DateTime.Now.ToString()), File.Encrypt);
            //            //ZipFile.CreateFromDirectory(GlobalFunctions.GetPhysicalFontsPath(),BackupFolderPath);
            //            //Directory.Delete(GlobalFunctions.GetPhysicalFontsPath() + strUploadFolderName);
            //            #endregion

            //            try
            //            {
            //                RemoveUploadedFonts();
            //                #region Save Zip
            //                fuFonts.SaveAs(strFontUploadPath);
            //                #endregion
            //                if (File.Exists(strFontUploadPath))
            //                {
            //                    //if (Directory.Exists(GlobalFunctions.GetPhysicalFontsPath() + strUploadFolderName))
            //                    //{
            //                    //    Array.ForEach(Directory.GetFiles(GlobalFunctions.GetPhysicalFontsPath() + strUploadFolderName), File.Delete);
            //                    //}
            //                    ZipFile.ExtractToDirectory(GlobalFunctions.GetPhysicalFontsUploadPath() + strNewFileName, GlobalFunctions.GetPhysicalFontsPath());
            //                }
            //                #region Remove TypeKit
            //                if (File.Exists(GlobalFunctions.GetPhysicalFontsPath() + "Typekit.txt"))
            //                {
            //                    FileInfo fileinfo = new FileInfo(GlobalFunctions.GetPhysicalFontsPath() + "Typekit.txt");
            //                    using (TextWriter txtWriter = new StreamWriter(fileinfo.Open(FileMode.Truncate)))
            //                    {
            //                        txtWriter.Write("");
            //                    }
            //                }
            //                #endregion
            //            }
            //            catch (Exception ex)
            //            {
            //                Exceptions.WriteExceptionLog(ex);
            //                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while uploading Fonts, Please try again", AlertType.Failure);
            //                return;
            //            }
            //            ////dvFontCSS.Visible = true;
            //            CreateActivityLog("Font Setting", "Uploaded", "");
            //            GlobalFunctions.ShowModalAlertMessages(this.Page, "Fonts Uploaded Successfully", AlertType.Success);
            //            btnShowCSS.Visible = true;
            //        }
            //    }
            //    else
            //    {
            //        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Upload File", AlertType.Warning);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Exceptions.WriteExceptionLog(ex);
            //    //dvFontCSS.Visible = false;
            //    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while uploading Fonts, Please try again", AlertType.Failure);
            //}
            #endregion

            if (fuFonts.HasFile)     // CHECK IF ANY FILE HAS BEEN SELECTED.
            {
                int iUploadedCnt = 0;
                int iFailedCnt = 0;
                HttpFileCollection hfc = Request.Files;
                lblFileList.Text = "Select <b>" + hfc.Count + "</b> file(s)";

                if (hfc.Count <= 100)    // 10 FILES RESTRICTION.
                {
                    for (int i = 0; i <= hfc.Count - 1; i++)
                    {
                        HttpPostedFile hpf = hfc[i];
                        if (hpf.ContentLength > 0)
                        {
                            if (!File.Exists(Server.MapPath("Fonts\\") + Path.GetFileName(hpf.FileName)))
                            {
                                // DirectoryInfo objDir = new DirectoryInfo(Server.MapPath("Fonts\\"));
                                DirectoryInfo objDir = new DirectoryInfo(GlobalFunctions.GetPhysicalFontsPath());
                                string sFileName = Path.GetFileName(hpf.FileName);
                                string sFileExt = Path.GetExtension(hpf.FileName);

                                // CHECK FOR DUPLICATE FILES.
                                //FileInfo[] objFI =  objDir.GetFiles(sFileName.Replace(sFileExt, "") + ".*");
                                FileInfo[] objFI = objDir.GetFiles(sFileName);




                                if (objFI.Length > 0)
                                {
                                    // CHECK IF FILE WITH THE SAME NAME EXISTS

                                    foreach (FileInfo file in objFI)
                                    {
                                        string sFileName1 = objFI[0].Name;
                                        string sFileExt1 = Path.GetExtension(objFI[0].Name);

                                        if (sFileName1 == sFileName)
                                        {
                                            iFailedCnt += 1;        // NOT ALLOWING DUPLICATE.
                                            break;
                                        }
                                        //if (sFileName1.Replace(sFileExt1, "") == sFileName.Replace(sFileExt, ""))
                                        //{
                                        //    iFailedCnt += 1;        // NOT ALLOWING DUPLICATE.
                                        //    break;
                                        //}
                                    }
                                }
                                else
                                {
                                    // SAVE THE FILE IN A FOLDER.
                                    string AllAllowedFontType = GlobalFunctions.GetSetting("AllowedFontType");
                                    string FileExtension = Convert.ToString(Path.GetExtension(hpf.FileName).Replace(".", ""));
                                    if (AllAllowedFontType.Contains(FileExtension))
                                    {
                                        string Physicalpath = GlobalFunctions.GetPhysicalFontsPath();//Server.MapPath("~/Fonts/");
                                        hpf.SaveAs(Physicalpath + Path.GetFileName(hpf.FileName));
                                        string Virtualpath = Host + "/fonts/" + Path.GetFileName(hpf.FileName);
                                        DataTable dt = ViewState["griddata"] as DataTable;
                                        DataRow dr = dt.NewRow();
                                        dr["FileName"] = Path.GetFileName(hpf.FileName);
                                        dr["FilePath"] = Virtualpath;
                                        dt.Rows.Add(dr);
                                        ViewState["griddata"] = dt;
                                        iUploadedCnt += 1;
                                    }
                                }
                            }
                        }
                    }
                    CreateActivityLog("Font Setting", "Uploaded", "");
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "" + iUploadedCnt + " Files Uploaded Successfully", AlertType.Success);
                    //lblUploadStatus.Text = "<b>" + iUploadedCnt + "</b> file(s) Uploaded.";
                    lblFailedStatus.Text = "<b>" + iFailedCnt + "</b> duplicate file(s) could not be uploaded.";
                }
                else lblUploadStatus.Text = "Max. 10 files allowed.";
            }
            else lblUploadStatus.Text = "No files selected.";

            BindGrid(ViewState["griddata"] as DataTable);
        }

        protected void ddlFontSettingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFontSettingType.SelectedValue == "1")
            {
                dvTypeKit.Visible = true;
                string FilePath = GlobalFunctions.GetPhysicalFontsPath() + "Typekit.txt";
                if (FilePath != "")
                {
                    txtTypekit.Text = File.ReadAllText(FilePath);
                    if (txtTypekit.Text != "")
                    {
                        btnTypekitCSS.Visible = true;
                    }
                    else
                    {
                        btnTypekitCSS.Visible = false;
                    }
                }
                dvFonts.Visible = false;
            }
            else if (ddlFontSettingType.SelectedValue == "2")
            {
                dvTypeKit.Visible = false;
                dvFonts.Visible = true;
                //string strNewFileName = "FontUpload" + ".zip";
                // string strFontUploadPath = GlobalFunctions.GetPhysicalFontsUploadPath() + strNewFileName;

                // No zip will be used now
                DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Fonts"));
                FileInfo[] infos = d.GetFiles();

                //string[] filePaths = Directory.GetFiles(Host+"fonts/");
                if (infos.Length > 0)
                {
                    CreateDataTableStructure();
                    ReadUploadedFontFiles();
                    btnShowCSS.Visible = true;
                }
                else
                {
                    btnShowCSS.Visible = false;
                }
            }
            else
            {
                dvTypeKit.Visible = false;
                dvFonts.Visible = false;
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select a Font Setting.", AlertType.Warning);
            }
        }

        #region OldVersion Donwload Option
        //protected void lnkFontCSS_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        WebClient req = new WebClient();
        //        HttpResponse response = HttpContext.Current.Response;
        //        response.Clear();
        //        response.ClearContent();
        //        response.ClearHeaders();
        //        response.Buffer = true;
        //        response.AddHeader("Content-Disposition", "attachment;filename=Font.css");
        //        byte[] data = req.DownloadData(Convert.ToString(GlobalFunctions.GetPhysicalFolderPath() + "CSS\\Font.css"));
        //        response.BinaryWrite(data);
        //        response.End();
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //}
        #endregion
        #region OldVersion Upload Option
        //protected void btnFontCSS_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (fuFontCSS.HasFiles)
        //        {
        //            if (fuFontCSS.PostedFile.ContentLength > 0)
        //            {
        //                if (GlobalFunctions.IsFileSizeValid(fuFontCSS, Convert.ToInt64(GlobalFunctions.GetSetting("Font_FileSize"))) == false)
        //                {
        //                    GlobalFunctions.ShowModalAlertMessages(Page, "Font CSS file size is not valid", AlertType.Warning);
        //                    SetFocus(fuFontCSS.ClientID);
        //                    return;
        //                }

        //                //Chk for File Extension
        //                string[] strFileTypes = { ".css" };
        //                string[] strMimeTypes = { "text/css", "application/x-pointplus" };
        //                bool bChkFileType = false;
        //                bChkFileType = GlobalFunctions.CheckFileExtension(fuFontCSS, strFileTypes);
        //                bool bChkFileMimeType = false;
        //                bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(fuFontCSS, strMimeTypes);
        //                if (!(bChkFileType && bChkFileMimeType))
        //                {
        //                    GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
        //                    SetFocus(fuFontCSS.ClientID);
        //                    return;
        //                }
        //                string strFileName = fuFontCSS.FileName;
        //                string strFontCSSPath = GlobalFunctions.GetPhysicalFolderPath() + "CSS//"+ strFileName;
        //                try
        //                {
        //                    if (File.Exists(strFontCSSPath))
        //                    {
        //                        File.Copy(strFontCSSPath, GlobalFunctions.GetPhysicalFolderPath() + "Backup//FontCSS//Backup_" + DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + "_" + strFileName, true);
        //                        File.Delete(strFontCSSPath);
        //                    }

        //                    fuFontCSS.SaveAs(strFontCSSPath);
        //                }
        //                catch (Exception ex)
        //                {
        //                    Exceptions.WriteExceptionLog(ex);
        //                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while uploading Font CSS, Please try again", AlertType.Failure);
        //                    return;
        //                }
        //                GlobalFunctions.ShowModalAlertMessages(this.Page, "Font CSS Uploaded Successfully", AlertType.Success);
        //            }
        //        }
        //        else
        //        {
        //            GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Upload File", AlertType.Failure);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while uploading Font CSS, Please try again", AlertType.Failure);
        //    }
        //}
        #endregion


        protected void ReadUploadedFontFiles()
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Fonts"));
                FileInfo[] files = d.GetFiles();
                string FontDonotDelete = GlobalFunctions.GetSetting("FontDonotDelete");

                string[] values = FontDonotDelete.Split(',');
                DataTable dt = ViewState["griddata"] as DataTable;
                //List<ListItem> files = new List<ListItem>();
                foreach (FileInfo file in files)
                {
                    if (!values.Contains(Path.GetFileNameWithoutExtension(Convert.ToString(file)).ToLower()))
                    {
                        DataRow dr = dt.NewRow();
                        dr["FileName"] = Path.GetFileName(file.Name);
                        dr["FilePath"] = Host + "fonts/" + file;
                        dt.Rows.Add(dr);
                    }
                    //files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                }
                ViewState["griddata"] = dt;
                grdView.DataSource = dt;
                grdView.DataBind();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btnShowCSS_Click(object sender, EventArgs e)
        {
            Response.Redirect(Host + "Admin/settings/ManageCSS.aspx");
        }

        protected void BindGrid(DataTable dt)
        {
            try
            {
                grdView.DataSource = dt;
                grdView.DataBind();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            string fileName = "";
            try
            {
                fileName = (sender as LinkButton).CommandArgument;
                string FilewithPath = GlobalFunctions.GetPhysicalFontsPath() + fileName;
                //Response.Write(FilewithPath);
                File.Delete(FilewithPath);
                CreateDataTableStructure();
                ReadUploadedFontFiles();
                GlobalFunctions.ShowModalAlertMessages(this.Page, fileName + " has been deleted", AlertType.Success);
            }
            catch (Exception ex)
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, fileName + " has been deleted", AlertType.Failure);
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}