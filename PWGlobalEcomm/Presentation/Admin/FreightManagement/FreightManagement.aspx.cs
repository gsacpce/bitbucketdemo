﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_FreightManagement_FreightManagement : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.RadioButtonList rblFreightRegions, rblFreightModes, rblFreightMethods;
        protected global::System.Web.UI.WebControls.DropDownList ddlCarrierService;
        //protected global::System.Web.UI.HtmlControls.HtmlGenericControl divFreightConfiguration;Commented by Sripal


        #region "Added by Sripal"
        protected global::System.Web.UI.WebControls.Button btnSave, btnDelete;

        protected global::System.Web.UI.WebControls.TextBox txtTransitTime;//txtCarrierServiceText,txtCarrierServiceId,
        //protected global::System.Web.UI.WebControls.TextBox txtVAA, txtVAB,txtMOV;

        protected global::System.Web.UI.WebControls.RadioButtonList rblPerBoxPerOrder;
        protected global::System.Web.UI.WebControls.CheckBox chkDisallowOrder;

        protected global::System.Web.UI.WebControls.RegularExpressionValidator rgxTransitTime;//rgxCarrierServiceText,rgxCarrierServiceId
        //protected global::System.Web.UI.WebControls.RegularExpressionValidator rgxMOV, rgxVAA, rgxVAB;

        protected global::System.Web.UI.WebControls.Label lblVAA, lblCarrierServiceId;

        protected global::System.Web.UI.WebControls.Repeater rptValueAppliedAbove, rptMinimumOrderValue, rptValueAppliedBelow;
        public List<FreightManagementBE> lstAllFreightSettings;
        public List<FreightManagementBE.FreightMultiplerCurrency> lstAllFreightMultiplerCurrency;
        public List<FreightManagementBE.FreightMultiplerCurrency> lstMinimumOrderValue;
        public List<FreightManagementBE.FreightMultiplerCurrency> lstValueAppliedAbove;
        public List<FreightManagementBE.FreightMultiplerCurrency> lstValueAppliedBelow;

        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvVAA, rfvPerBoxPerOrder;
        #endregion

        #region "Variables"
        Int16 RegionId = 0;
        Int16 ModeId = 0;
        Int16 MethodId = 0;

        string strCurrencyMultipler = string.Empty;
        FreightManagementBE CurrFreightConfig = new FreightManagementBE();
        #endregion

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : Page_Load event
        /// </summary>
        /// Bind Radio Button List with respective master list of Freight Zone,Mode & method
        /// Build Cache objects from the master list for reusability
        /// Add Fregiht configuration user control to div
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    List<FreightManagementBE.FreightRegion> LstFreightRegions = new List<FreightManagementBE.FreightRegion>();
                    List<FreightManagementBE.FreightMode> LstFreightModes = new List<FreightManagementBE.FreightMode>();
                    List<FreightManagementBE.FreightMethod> LstFreightMethods = new List<FreightManagementBE.FreightMethod>();

                    LstFreightRegions = FreightManagementBL.GetAllFreightRegions<FreightManagementBE.FreightRegion>();
                    LstFreightModes = FreightManagementBL.GetAllFreightModes<FreightManagementBE.FreightMode>();
                    LstFreightMethods = FreightManagementBL.GetAllFreightMethods<FreightManagementBE.FreightMethod>();

                    rblFreightRegions.DataSource = LstFreightRegions;
                    rblFreightRegions.DataTextField = "FreightRegionName";
                    rblFreightRegions.DataValueField = "FreightRegionId";
                    rblFreightRegions.DataBind();
                    rblFreightRegions.SelectedIndex = 0;

                    rblFreightModes.DataSource = LstFreightModes;
                    rblFreightModes.DataTextField = "FreightModeName";
                    rblFreightModes.DataValueField = "FreightModeId";
                    rblFreightModes.DataBind();
                    rblFreightModes.SelectedIndex = 0;

                    rblFreightMethods.DataSource = LstFreightMethods;
                    rblFreightMethods.DataTextField = "FreightMethodName";
                    rblFreightMethods.DataValueField = "FreightMethodId";
                    rblFreightMethods.DataBind();
                    rblFreightMethods.SelectedIndex = 0;

                    Session["CurrFreightRegionId"] = rblFreightRegions.SelectedValue;
                    Session["CurrFreightModeId"] = rblFreightModes.SelectedValue;
                    Session["CurrFreightMethodId"] = rblFreightMethods.SelectedValue;

                    /*Sachin Chauhan : 11 10 15 : Disable Radio button list selection for existing freight configuration */
                    if (Cache["FreightConfigurations"] != null)
                    {
                        lstAllFreightSettings = Cache["FreightConfigurations"] as List<FreightManagementBE>;
                        #region "Commented by Sripal"
                        //FreightManagementBE CurrFreightConfig = lstAllFreightSettings.FirstOrDefault(x => x.FreightModeId.Equals(Session["CurrFreightModeId"].To_Int16()) &&
                        //                                                                                x.FreightRegionId.Equals(Session["CurrFreightRegionId"].To_Int16()) &&
                        //                                                                                x.FreightMethodId.Equals(Session["CurrFreightMethodId"].To_Int16())
                        //                                                                               );
                        //if (CurrFreightConfig != null)
                        //{
                        //    #region "Comments"
                        //    //rblFreightRegions.SelectedValue = CurrFreightConfig.FreightRegionId.ToString();
                        //    //rblFreightModes.SelectedValue = CurrFreightConfig.FreightModeId.ToString();
                        //    //rblFreightMethods.SelectedValue = CurrFreightConfig.FreightMethodId.ToString();

                        //    //rblFreightRegions.Enabled = false;
                        //    //rblFreightModes.Enabled = false;
                        //    //rblFreightMethods.Enabled = false; 
                        //    #endregion
                        //} 
                        #endregion
                    }
                    else
                    {
                        lstAllFreightSettings = FreightManagementBL.GetAllFreightConfiguration<FreightManagementBE>();
                    }
                    Int16 FreightMehodId = Convert.ToInt16(rblFreightMethods.SelectedValue);

                    lstAllFreightMultiplerCurrency = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(FreightMehodId, "CM");
                    lstValueAppliedBelow = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(FreightMehodId, "VAB");

                    lstValueAppliedAbove = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(FreightMehodId, "VAA");
                    lstMinimumOrderValue = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(FreightMehodId, "MOV");

                    #region "added from Configuration"
                    if (lstAllFreightSettings != null)
                    {
                        //CurrFreightConfig = lstAllFreightSettings.FirstOrDefault(x => x.FreightModeId.Equals(Session["CurrFreightModeId"].To_Int16()) &&
                        //                                                                                  x.FreightRegionId.Equals(Session["CurrFreightRegionId"].To_Int16()) &&
                        //                                                                                  x.FreightMethodId.Equals(Session["CurrFreightMethodId"].To_Int16())
                        //                                                                                 );
                        CurrFreightConfig = lstAllFreightSettings.FirstOrDefault(x => x.FreightModeId.Equals(Session["CurrFreightModeId"].To_Int16()) &&
                                                                                                         x.FreightRegionId.Equals(Session["CurrFreightRegionId"].To_Int16())
                                                                                                         );
                        if (CurrFreightConfig != null)
                        {
                            FillCurrFreightSetting(CurrFreightConfig);
                        }
                    }

                    //rgxCarrierServiceId.ValidationExpression = ValidRegEx.RegEx_ValidMultiDigitNo(3);
                    //rgxCarrierServiceText.ValidationExpression = ValidRegEx.RegEx_ValidAlpanumericStringWithSafeSpecialChars;
                    rgxTransitTime.ValidationExpression = ValidRegEx.RegEx_ValidAlpanumerichyphen;// ValidRegEx.RegEx_ValidSingleDigitNumber;

                    #region vikram
                    //rgxMOV.ValidationExpression = ValidRegEx.RegEx_ValidDecimalWithTwoDigit;
                    //rgxVAA.ValidationExpression = ValidRegEx.RegEx_ValidDecimalWithTwoDigit;
                    //rgxVAB.ValidationExpression = ValidRegEx.RegEx_ValidDecimalWithTwoDigit;
                    #endregion
                    #endregion
                    #region "Commented by Sripal"
                    //Admin_FreightManagement_UserControls_FreightConfiguration ctrlFreightConfig = (Admin_FreightManagement_UserControls_FreightConfiguration)Page.LoadControl("UserControls/FreightConfiguration.ascx");
                    //divFreightConfiguration.Controls.Add(ctrlFreightConfig);
                    #endregion

                    #region "Modifying the Controls behaviour"
                    #region commneted by Vikram
                    //txtVAA.Visible = true;
                    //rfvVAA.Enabled = true;
                    #endregion

                    BindCurrency();
                    if (CurrFreightConfig != null)
                    {
                        rblFreightMethods.SelectedValue = Convert.ToString(CurrFreightConfig.FreightMethodId);
                    }
                    if (rblFreightMethods.SelectedItem.Value.Equals("2"))
                    {
                        lblVAA.Text = "% Applied Above";
                    }
                    else if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                    {
                        lblVAA.Text = "Multiplier On Cost Applied Above";
                    }
                    else//Added by Sripal 
                    {
                        lblVAA.Text = "Value Applied Above";
                    }

                    /*Sachin Chauhan : 10 10 2015 : Freight to be applicable per box or per order is only for Fixed Rate
                     * Freight Method and not applicable for % uplit & freight matrix methods
                     */
                    if (rblFreightMethods.SelectedValue.Equals("2") || rblFreightMethods.SelectedValue.Equals("3"))
                    {
                        //RadioButtonList rblPerBoxPerOrder = ctrlFreightConfig.FindControl("rblPerBoxPerOrder") as RadioButtonList;Commented by Sripal
                        rblPerBoxPerOrder.Visible = false;
                        rfvPerBoxPerOrder.Enabled = false;
                    }
                    else { rblPerBoxPerOrder.Visible = true; rfvPerBoxPerOrder.Enabled = true; }
                    #endregion
                    #region "Assign Value to Session"
                    RegionId = rblFreightRegions.SelectedValue.To_Int16();
                    ModeId = rblFreightModes.SelectedValue.To_Int16();
                    MethodId = rblFreightMethods.SelectedValue.To_Int16();

                    Session["CurrFreightRegionId"] = RegionId;
                    Session["CurrFreightModeId"] = ModeId;
                    Session["CurrFreightMethodId"] = MethodId;
                    #endregion

                    BindCarrierService();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : RadibuttonList selected index change event
        /// </summary>
        /// Add Fregiht configuration user control to div when each of the radi button list has selected index not equal to -1
        protected void rbl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                RegionId = rblFreightRegions.SelectedValue.To_Int16();
                if (RegionId == 3)
                {
                    rblFreightModes.Items[0].Enabled = false;
                    var bli = rblFreightModes.Items[0];
                    bli.Attributes.Add("visibility", "hidden");

                    rblFreightModes.Items[1].Selected = true;
                    ModeId = 2;
                }
                else
                {
                    ModeId = rblFreightModes.SelectedValue.To_Int16();
                    rblFreightModes.Items[0].Enabled = true;
                    var bli = rblFreightModes.Items[0];
                    bli.Attributes.Add("visibility", "visible");
                }
                MethodId = rblFreightMethods.SelectedValue.To_Int16();
                RadioButtonList rdList = (RadioButtonList)sender;

                /*Sachin Chauhan : 09 10 2015 : Saving above values in Current Session So that the selected 
                 *                              ids can be used to to pass in FreightConfiguration currently
                 *                              being added or edited
                 */
                Session["CurrFreightRegionId"] = RegionId;
                Session["CurrFreightModeId"] = ModeId;
                Session["CurrFreightMethodId"] = MethodId;
                /**/

                if (RegionId != -1 && ModeId != -1 && ModeId != -1)
                {
                    #region "Comments"
                    #region "Commented by Sripal"
                    //Admin_FreightManagement_UserControls_FreightConfiguration ctrlFreightConfig = (Admin_FreightManagement_UserControls_FreightConfiguration)Page.LoadControl("UserControls/FreightConfiguration.ascx");
                    //divFreightConfiguration.Controls.Clear();
                    //divFreightConfiguration.Controls.Add(ctrlFreightConfig);
                    #endregion
                    /*Sachin Chauhan : 09 10 2015 : Commented as Fregit Matrix too can consider Minimum Order Value*/
                    /*
                    Panel pnlNonFreightMatrixControls = ctrlFreightConfig.FindControl("pnlNonFreightMatrixControls") as Panel;

                    if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                    {
                        ShowHideNonFreightMatrixControls(pnlNonFreightMatrixControls,false);
                    }
                    else
                    {
                        ShowHideNonFreightMatrixControls(pnlNonFreightMatrixControls, true);
                    }*/

                    /*Sachin Chauhan : 10 10 2015 : Change label title as per Freight Method Choosen
                     */
                    //Label lblVAA = ctrlFreightConfig.FindControl("lblVAA") as Label;Commented by Sripal 
                    #endregion

                    CurrFreightConfig = new FreightManagementBE();
                    #region "Added by Sripal"
                    if (Cache["FreightConfigurations"] != null)
                    {
                        lstAllFreightSettings = Cache["FreightConfigurations"] as List<FreightManagementBE>;
                    }
                    else
                    {
                        lstAllFreightSettings = FreightManagementBL.GetAllFreightConfiguration<FreightManagementBE>();
                    }
                    Int16 FreightMehodId = Convert.ToInt16(rblFreightMethods.SelectedValue);
                    lstAllFreightMultiplerCurrency = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(FreightMehodId, "CM");
                    lstValueAppliedBelow = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(FreightMehodId, "VAB");

                    lstValueAppliedAbove = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(FreightMehodId, "VAA");
                    lstMinimumOrderValue = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(FreightMehodId, "MOV");

                    if (lstAllFreightSettings != null)
                    {
                        if (rdList.ID.ToLower() == "rblfreightmethods")
                        {
                            CurrFreightConfig = lstAllFreightSettings.FirstOrDefault(x => x.FreightModeId.Equals(Session["CurrFreightModeId"].To_Int16()) &&
                                                                                                          x.FreightRegionId.Equals(Session["CurrFreightRegionId"].To_Int16()) &&
                                                                                                          x.FreightMethodId.Equals(Session["CurrFreightMethodId"].To_Int16())
                                                                                                         );
                        }
                        else
                        {
                            CurrFreightConfig = lstAllFreightSettings.FirstOrDefault(x => x.FreightModeId.Equals(Session["CurrFreightModeId"].To_Int16()) &&
                                                                                                             x.FreightRegionId.Equals(Session["CurrFreightRegionId"].To_Int16())
                                                                                                            );
                        }
                        if (CurrFreightConfig != null)
                        {
                            FillCurrFreightSetting(CurrFreightConfig);
                        }
                        else
                        {
                            ClearInputs();
                        }
                    }
                    else
                    {
                        ClearInputs();
                    }
                    #endregion
                    #region "Modifying the Controls behaviour"
                    #region by vikram
                    //txtVAA.Visible = true;
                    //rfvVAA.Enabled = true;
                    #endregion
                    BindCurrency();
                    if (CurrFreightConfig != null)
                    {
                        rblFreightMethods.SelectedValue = Convert.ToString(CurrFreightConfig.FreightMethodId);
                    }
                    if (rblFreightMethods.SelectedItem.Value.Equals("2"))
                    {
                        lblVAA.Text = "% Applied Above";
                    }
                    else if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                    {
                        lblVAA.Text = "Multiplier On Cost Applied Above";
                    }
                    else//Added by Sripal 
                    {
                        lblVAA.Text = "Value Applied Above";
                    }

                    /*Sachin Chauhan : 10 10 2015 : Freight to be applicable per box or per order is only for Fixed Rate
                     * Freight Method and not applicable for % uplit & freight matrix methods
                     */
                    if (rblFreightMethods.SelectedValue.Equals("2") || rblFreightMethods.SelectedValue.Equals("3"))
                    {
                        //RadioButtonList rblPerBoxPerOrder = ctrlFreightConfig.FindControl("rblPerBoxPerOrder") as RadioButtonList;Commented by Sripal
                        rblPerBoxPerOrder.Visible = false;
                        rfvPerBoxPerOrder.Enabled = false;
                    }
                    else { rblPerBoxPerOrder.Visible = true; rfvPerBoxPerOrder.Enabled = true; }
                    #endregion

                    #region "Assign Value to Session"
                    RegionId = rblFreightRegions.SelectedValue.To_Int16();
                    ModeId = rblFreightModes.SelectedValue.To_Int16();
                    MethodId = rblFreightMethods.SelectedValue.To_Int16();

                    Session["CurrFreightRegionId"] = RegionId;
                    Session["CurrFreightModeId"] = ModeId;
                    Session["CurrFreightMethodId"] = MethodId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : Function to Show or Hide Non Freight Matrix Controls
        /// </summary>
        /// Bind Radio Button List with respective master list of Freight Zone,Mode & method
        /// Add Fregiht configuration user control to div
        //private static void ShowHideNonFreightMatrixControls(Panel pnlNonFreightMatrixControls, bool isVisible)
        //{
        //    foreach (var ctrl in pnlNonFreightMatrixControls.Controls)
        //    {
        //        if (ctrl.GetType().Equals(typeof(Label)) || ctrl.GetType().Equals(typeof(TextBox)))
        //        {
        //            ((WebControl)ctrl).Visible = isVisible;
        //        }
        //    }
        //}

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 14-09-15
        /// Scope   : Fill Freight Config Details if a particular Freight Configuration is already exisitng
        /// </summary>
        private void FillCurrFreightSetting(FreightManagementBE CurrFreightConfig)
        {
            try
            {
                lblCarrierServiceId.Text = CurrFreightConfig.CarrierServiceId.ToString();
                ddlCarrierService.SelectedValue = CurrFreightConfig.CarrierServiceId.ToString();
                txtTransitTime.Text = CurrFreightConfig.TransitTime.ToString();

                if (!rblFreightMethods.SelectedValue.Equals("2") && !rblFreightMethods.SelectedValue.Equals("3"))
                {
                    if (rblPerBoxPerOrder != null && !string.IsNullOrEmpty(CurrFreightConfig.PerBoxPerOrder))
                    { rblPerBoxPerOrder.SelectedValue = CurrFreightConfig.PerBoxPerOrder; }
                }
                if (chkDisallowOrder != null)
                { chkDisallowOrder.Checked = Convert.ToBoolean(CurrFreightConfig.DisAllowOrderBelowMOV); }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptMinimumOrderValue_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int iFreightConfigID = CurrFreightConfig.FreightConfigurationId;
                    int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));
                    FreightManagementBE.FreightMultiplerCurrency MinimumOrderValue = lstMinimumOrderValue.FirstOrDefault(x => x.FreightConfigurationId == iFreightConfigID && x.CurrencyID == iCurrency);
                    TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
                    if (MinimumOrderValue != null)
                        txtCurrencyValue.Text = Convert.ToString(MinimumOrderValue.CValue);
                    else
                        txtCurrencyValue.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptValueAppliedAbove_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int iFreightConfigID = CurrFreightConfig.FreightConfigurationId;
                    int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));

                    if (lstValueAppliedAbove != null)
                    {
                        FreightManagementBE.FreightMultiplerCurrency ValueAppliedAbove = lstValueAppliedAbove.FirstOrDefault(x => x.FreightConfigurationId == iFreightConfigID &&
                          x.CurrencyID == iCurrency);
                        TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
                        if (ValueAppliedAbove != null)
                        {
                            txtCurrencyValue.Text = Convert.ToString(ValueAppliedAbove.CValue);
                        }
                        //else
                        //{ txtCurrencyValue.Text = "0"; }
                    }
                    if (lstAllFreightMultiplerCurrency != null)
                    {
                        FreightManagementBE.FreightMultiplerCurrency AllFreightMultiplerCurrency = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == iFreightConfigID &&
                         x.CurrencyID == iCurrency);
                        TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
                        if (AllFreightMultiplerCurrency != null)
                        {
                            txtCurrencyValue.Text = Convert.ToString(AllFreightMultiplerCurrency.CValue);
                        }
                        //else { txtCurrencyValue.Text = "0"; }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptValueAppliedBelow_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int iFreightConfigID = CurrFreightConfig.FreightConfigurationId;
                    int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));
                    FreightManagementBE.FreightMultiplerCurrency ValueAppliedBelow = lstValueAppliedBelow.FirstOrDefault(x => x.FreightConfigurationId == iFreightConfigID &&
                          x.CurrencyID == iCurrency);

                    TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
                    if (ValueAppliedBelow != null)
                    {
                        txtCurrencyValue.Text = Convert.ToString(ValueAppliedBelow.CValue);
                    }
                    else
                    { txtCurrencyValue.Text = "0"; }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected string GetAssignedMinimumOrderValues()
        {
            string MOV = string.Empty;
            try
            {
                foreach (RepeaterItem rptitem in rptMinimumOrderValue.Items)
                {
                    Label lblCurrency = rptitem.FindControl("lblCurrency") as Label;
                    HiddenField hdfCurrencyID = rptitem.FindControl("hdfCurrencyID") as HiddenField;
                    TextBox txtMOV = (TextBox)rptitem.FindControl("txtCurrencyValue");
                    RegularExpressionValidator regExp = (RegularExpressionValidator)rptitem.FindControl("rgxMOV");
                    Boolean IsValidMOV = Regex.IsMatch(txtMOV.Text, ValidRegEx.RegEx_ValidDecimalWithFourDigit);
                    if (IsValidMOV)
                    {
                        if (MOV == "")
                            MOV = MOV + hdfCurrencyID.Value + "-" + txtMOV.Text.Trim();
                        else
                            MOV = MOV + "|" + hdfCurrencyID.Value + "-" + txtMOV.Text.Trim();
                    }
                    else
                    {
                        MOV = "";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return MOV;
        }

        protected string GetAssignedValueAppliedBelow()
        {
            string VAB = string.Empty;
            try
            {
                foreach (RepeaterItem rptitem in rptValueAppliedBelow.Items)
                {
                    Label lblCurrency = rptitem.FindControl("lblCurrency") as Label;
                    HiddenField hdfCurrencyID = rptitem.FindControl("hdfCurrencyID") as HiddenField;
                    TextBox txtVAB = (TextBox)rptitem.FindControl("txtCurrencyValue");
                    Boolean IsValidVAB = Regex.IsMatch(txtVAB.Text, ValidRegEx.RegEx_ValidDecimalWithFourDigit);
                    if (IsValidVAB)
                    {
                        if (VAB == "")
                            VAB = VAB + hdfCurrencyID.Value + "-" + txtVAB.Text.Trim();
                        else
                            VAB = VAB + "|" + hdfCurrencyID.Value + "-" + txtVAB.Text.Trim();
                    }
                    else
                    {
                        VAB = "";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return VAB;
        }

        protected string GetAssignedValueAppliedAbove()
        {
            string VAA = string.Empty;
            try
            {
                foreach (RepeaterItem rptitem in rptValueAppliedAbove.Items)
                {
                    Label lblCurrency = rptitem.FindControl("lblCurrency") as Label;
                    HiddenField hdfCurrencyID = rptitem.FindControl("hdfCurrencyID") as HiddenField;
                    TextBox txtVAB = (TextBox)rptitem.FindControl("txtCurrencyValue");
                    Boolean IsValidVAA = Regex.IsMatch(txtVAB.Text, ValidRegEx.RegEx_ValidDecimalWithFourDigit);
                    if (IsValidVAA)
                    {
                        if (VAA == "")
                            VAA = VAA + hdfCurrencyID.Value + "-" + txtVAB.Text.Trim();
                        else
                            VAA = VAA + "|" + hdfCurrencyID.Value + "-" + txtVAB.Text.Trim();
                    }
                    else
                    {
                        VAA = "";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return VAA;
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 12-09-15
        /// Scope   : Btn Save Freight Config event
        /// </summary>
        /// Validate User input against Regex present on client side
        /// If user is some how able to skip the front end validation, server side validation will
        /// ensure input sanitization
        protected void btnSaveFreightConfig_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean IsValidCarrierServiceId = Regex.IsMatch(ddlCarrierService.SelectedValue, ValidRegEx.RegEx_ValidMultiDigitNo(3));
                Boolean IsValidCarrierServiceText = Regex.IsMatch(ddlCarrierService.SelectedItem.Text, ValidRegEx.RegEx_ValidAlpanumericStringWithSafeSpecialChars);
                Boolean IsValidTransitTime = Regex.IsMatch(txtTransitTime.Text, ValidRegEx.RegEx_ValidAlpanumerichyphen);// ValidRegEx.RegEx_ValidSingleDigitNumber);
                string MinimumOrderValue = GetAssignedMinimumOrderValues();
                string ValueAppliedAbove = GetAssignedValueAppliedAbove();
                string ValueAppliedBelow = GetAssignedValueAppliedBelow();
                if (MinimumOrderValue == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Minimum Order Value can be applied upto 4 decimal only.", AlertType.Warning);
                }
                else if (ValueAppliedAbove == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Value applied above can be applied upto 4 decimal only.", AlertType.Warning);
                }
                else if (ValueAppliedBelow == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Value applied below can be applied upto 4 decimal only.", AlertType.Warning);
                }
                else
                {
                    if (IsValidCarrierServiceId && IsValidCarrierServiceText && IsValidTransitTime)
                    {
                        FreightManagementBE CurrFreightConfig = new FreightManagementBE();

                        CurrFreightConfig.CarrierServiceId = ddlCarrierService.SelectedValue.To_Int16();
                        CurrFreightConfig.CarrierServiceText = ddlCarrierService.SelectedItem.Text;
                        //CurrFreightConfig.TransitTime = txtTransitTime.Text.To_Int16();
                        CurrFreightConfig.TransitTime = txtTransitTime.Text;
                        CurrFreightConfig.MinOrderValueCM = MinimumOrderValue;
                        CurrFreightConfig.ValueAppliedAboveCM = ValueAppliedAbove; //!string.IsNullOrEmpty(txtVAA.Text.Trim()) ? txtVAA.Text.Trim().To_Float() : 0;
                        CurrFreightConfig.ValueAppliedBelowCM = ValueAppliedBelow;

                        CurrFreightConfig.FreightModeId = Session["CurrFreightModeId"].To_Int16();
                        CurrFreightConfig.FreightRegionId = Session["CurrFreightRegionId"].To_Int16();
                        CurrFreightConfig.FreightMethodId = Session["CurrFreightMethodId"].To_Int16();

                        if (!rblFreightMethods.SelectedValue.Equals("2") && !rblFreightMethods.SelectedValue.Equals("3"))
                        {
                            if (rblPerBoxPerOrder != null)
                                CurrFreightConfig.PerBoxPerOrder = rblPerBoxPerOrder.SelectedValue;
                        }
                        else { CurrFreightConfig.PerBoxPerOrder = ""; }

                        if (chkDisallowOrder != null)
                            CurrFreightConfig.DisAllowOrderBelowMOV = chkDisallowOrder.Checked.Equals(true) ? true : false;


                        #region Added by Sripal
                        if (Cache["FreightConfigurations"] != null)
                        {
                            lstAllFreightSettings = Cache["FreightConfigurations"] as List<FreightManagementBE>;
                            if (lstAllFreightSettings != null)
                            {
                                FreightManagementBL.FreightManagementSAED(CurrFreightConfig, DBAction.Insert);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowSuccess('Freight Configuration setting saved successfully.');", true);
                                CreateActivityLog("FreightManagement", "Saved", Convert.ToString(ddlCarrierService.SelectedItem.Text));
                            }
                            else
                            {
                                FreightManagementBL.FreightManagementSAED(CurrFreightConfig, DBAction.Insert);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowSuccess('Freight Configuration setting saved successfully.');", true);
                                CreateActivityLog("FreightManagement", "Saved", Convert.ToString(ddlCarrierService.SelectedItem.Text));
                            }
                        }
                        else
                        {
                            FreightManagementBL.FreightManagementSAED(CurrFreightConfig, DBAction.Insert);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowSuccess('Freight Configuration setting saved successfully.');", true);
                            CreateActivityLog("FreightManagement", "Saved", Convert.ToString(ddlCarrierService.SelectedItem.Text));
                        }
                        #endregion


                        HttpContext.Current.Cache.Remove("FreightConfigurations");
                        lstAllFreightSettings = FreightManagementBL.GetAllFreightConfiguration<FreightManagementBE>();
                        #region "added from Configuration"
                        if (lstAllFreightSettings != null)
                        {
                            FreightManagementBE CurrFreightConfig1 = lstAllFreightSettings.FirstOrDefault(x => x.FreightModeId.Equals(Session["CurrFreightModeId"].To_Int16()) &&
                                                                                                               x.FreightRegionId.Equals(Session["CurrFreightRegionId"].To_Int16()) &&
                                                                                                               x.FreightMethodId.Equals(Session["CurrFreightMethodId"].To_Int16())
                                                                                                              );
                            if (CurrFreightConfig1 != null)
                            {
                                FillCurrFreightSetting(CurrFreightConfig1);
                            }
                        }


                        rgxTransitTime.ValidationExpression = ValidRegEx.RegEx_ValidAlpanumerichyphen;// ValidRegEx.RegEx_ValidSingleDigitNumber;
                    }


                        #endregion
                    //List<FreightManagementBE> lstAllFreightSettings = FreightManagementBL.GetAllFreightConfiguration<FreightManagementBE>();
                    //HttpContext.Current.Cache.Insert("FreightConfigurations", lstAllFreightSettings);Commented by Sripal
                }
            }
            catch (Exception ex)
            {
                //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error saving Freight Configuration setting.", AlertType.Failure);
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowFailure('There is an error saving Freight Configuration setting.');", true);
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnDeleteFreightConfig_OnClick(object sender, EventArgs e)
        {
            try
            {
                #region Added by Sripal
                FreightManagementBE CurrFreightConfig = new FreightManagementBE();
                if (Cache["FreightConfigurations"] != null)
                {
                    lstAllFreightSettings = Cache["FreightConfigurations"] as List<FreightManagementBE>;

                    CurrFreightConfig = lstAllFreightSettings.FirstOrDefault(x => x.FreightModeId.Equals(Session["CurrFreightModeId"].To_Int16()) &&
                                                                                                 x.FreightRegionId.Equals(Session["CurrFreightRegionId"].To_Int16()) &&
                                                                                                 x.FreightMethodId.Equals(Session["CurrFreightMethodId"].To_Int16())
                                                                                                );

                    if (CurrFreightConfig != null)
                    {
                        CurrFreightConfig.FreightConfigurationId = CurrFreightConfig.FreightConfigurationId;
                        FreightManagementBL.FreightManagementSAED(CurrFreightConfig, DBAction.Delete);
                        lstAllFreightSettings.Remove(CurrFreightConfig);
                        Cache["FreightConfigurations"] = lstAllFreightSettings;
                        ClearInputs();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowSuccess('Freight Configuration setting deleted successfully.');", true);
                        //GlobalFunctions.ShowModalAlertMessages(this.Page, "Freight Configuration setting deleted successfully.", AlertType.Success);
                        CreateActivityLog("FreightManagement", "Delete", Convert.ToString(CurrFreightConfig.FreightConfigurationId));
                    }
                }
                else
                {
                    //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error while deleting Freight Configuration setting.", AlertType.Failure);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowFailure('There is an error while deleting Freight Configuration setting.');", true);
                }
                #endregion
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowFailure('There is an error while deleting Freight Configuration setting.');", true);
                //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error while deleting Freight Configuration setting.", AlertType.Failure);
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void ClearInputs()
        {
            try
            {
                lblCarrierServiceId.Text = "";
                ddlCarrierService.SelectedValue = "0";
                txtTransitTime.Text = "";
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        private void BindCurrency()
        {
            try
            {
                #region "Repeater Currency"
                rptValueAppliedAbove.Visible = true;
                rptMinimumOrderValue.Visible = true;
                rptValueAppliedBelow.Visible = true;
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                rptValueAppliedAbove.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValueAppliedAbove.DataBind();

                rptMinimumOrderValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptMinimumOrderValue.DataBind();

                rptValueAppliedBelow.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValueAppliedBelow.DataBind();

                #endregion
                //txtVAA.Visible = false;
                //rfvVAA.Enabled = false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        void BindCarrierService()
        {
            try
            {
                List<Freight_Carrier_ServiceBE> lstFreightCarrierService = new List<Freight_Carrier_ServiceBE>();
                lstFreightCarrierService = FreightManagementBL.GetAllFreightCarrierService();
                ddlCarrierService.DataSource = lstFreightCarrierService;
                ddlCarrierService.DataTextField = "CARRIER_SERVICE_TEXT";
                ddlCarrierService.DataValueField = "CARRIER_SERVICE_ID";
                ddlCarrierService.DataBind();

                ddlCarrierService.Items.Insert(0, new ListItem("Please Select", "0"));
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}
