﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using PWGlobalEcomm.BusinessLogic;

namespace Presentation
{
    public class Admin_FineUploader_UploadCatalog : System.Web.UI.Page
    {
        string CategoryId = string.Empty;
        string ImageSrc = string.Empty;
        string ActionType = string.Empty;
        string LanguageId = string.Empty;

        public string BannerImageWidth = GlobalFunctions.GetSetting("BANNERWIDTH_CATEGORY");
        public string BannerImageHeight = GlobalFunctions.GetSetting("BANNERHEIGHT_CATEGORY");

        public string MenuImageWidth = GlobalFunctions.GetSetting("IMAGEWIDTH_MENU");
        public string MenuImageHeight = GlobalFunctions.GetSetting("IMAGEHEIGHT_MENU");


        

        public string ImageWidth = GlobalFunctions.GetSetting("IMAGEWIDTH_CATEGORY");
        public string ImageHeight = GlobalFunctions.GetSetting("IMAGEHEIGHT_CATEGORY");

        #region CSRF
        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        //private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        //private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        //private string _antiXsrfTokenValue;
        ///*Sachin Chauhan End : 22 02 2016 */

        ///// <summary>
        ///// Author  : Anoop Gupta
        ///// Date    : 18-08-15
        ///// Scope   : Page_PreInit event of the page
        ///// Modified by : Sachin Chauhan
        ///// Modified Date : 22 02 2016
        ///// Modify Scope : Added CSRF attack prevention code
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        ///// <returns></returns>
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        //    #region "CSRF prevention code"
        //    try
        //    {
        //        //First, check for the existence of the Anti-XSS cookie
        //        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        //        Guid requestCookieGuidValue;

        //        //If the CSRF cookie is found, parse the token from the cookie.
        //        //Then, set the global page variable and view state user
        //        //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
        //        //method.
        //        if (requestCookie != null
        //        && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        //        {
        //            //Set the global token variable so the cookie value can be
        //            //validated against the value in the view state form field in
        //            //the Page.PreLoad method.
        //            _antiXsrfTokenValue = requestCookie.Value;

        //            //Set the view state user key, which will be validated by the
        //            //framework during each request
        //            Page.ViewStateUserKey = _antiXsrfTokenValue;
        //        }
        //        //If the CSRF cookie is not found, then this is a new session.
        //        else
        //        {
        //            //Generate a new Anti-XSRF token
        //            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

        //            //Set the view state user key, which will be validated by the
        //            //framework during each request
        //            Page.ViewStateUserKey = _antiXsrfTokenValue;

        //            //Create the non-persistent CSRF cookie
        //            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
        //            {
        //                //Set the HttpOnly property to prevent the cookie from
        //                //being accessed by client side script
        //                HttpOnly = true,

        //                //Add the Anti-XSRF token to the cookie value
        //                Value = _antiXsrfTokenValue
        //            };

        //            //If we are using SSL, the cookie should be set to secure to
        //            //prevent it from being sent over HTTP connections
        //            if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
        //                responseCookie.Secure = true;

        //            //Add the CSRF cookie to the response
        //            Response.Cookies.Set(responseCookie);
        //        }

        //        Page.PreLoad += Page_PreLoad;
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
        //        Exceptions.WriteExceptionLog(ex);
        //        //throw;
        //    }
        //    #endregion
        //    /*Sachin Chauhan End : 22 02 2016*/
        //}

        ///// <summary>
        ///// Author  : Sachin Chauhan
        ///// Date : 22 02 2016
        ///// Scope : Added CSRF attack prevention code
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        ///// <returns></returns>
        //protected void Page_PreLoad(object sender, EventArgs e)
        //{
        //    //During the initial page load, add the Anti-XSRF token and user
        //    //name to the ViewState
        //    if (!IsPostBack)
        //    {
        //        //Set Anti-XSRF token
        //        ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

        //        //If a user name is assigned, set the user name
        //        //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        //        if (Session["ContextUserGUID"] == null)
        //            Session["ContextuserGUID"] = Guid.NewGuid().ToString();

        //        ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
        //    }
        //    //During all subsequent post backs to the page, the token value from
        //    //the cookie should be validated against the token in the view state
        //    //form field. Additionally user name should be compared to the
        //    //authenticated users name
        //    else
        //    {
        //        //Validate the Anti-XSRF token
        //        //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
        //        if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())

        //            Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
        //    }
        //}
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            string Type = "";
            /************************************************************************
             * Email configuration
             * **********************************************************************/
            //bool send_email = false;                //enable email notification
            //string main_receiver = "albanx@gmail.com";   //who receive the email
            //string cc = "";                   //other receivers in cc
            //string from = "from@ajaxupload.com";//who appear in the from field
            /*****************************************************************************/
            try
            {

                CategoryBL objCatalogBL = new CategoryBL();
                CategoryId = Convert.ToString(Request.QueryString["categoryid"]);
                ActionType = Convert.ToString(Request.QueryString["type"]);
                if (Convert.ToString(Request.QueryString["updLanguageid"]) != null)
                    LanguageId = Convert.ToString(Request.QueryString["updLanguageid"]);

                if (Request.QueryString["action"] == "remove")
                {
                    string FileUploadPath = string.Empty;
                    if (ActionType == "categorymouseover")
                    {
                        FileUploadPath = Server.MapPath("~/Admin/Images/Category/MouseOver") + "\\";
                    }
                    if (ActionType == "categorymenu")
                    {
                        FileUploadPath = Server.MapPath("~/Admin/Images/Category/categoryMenu") + "\\";
                    }
                    if (ActionType == "category")
                    {
                        FileUploadPath = Server.MapPath("~/Admin/Images/Category") + "\\";
                    }
                    if (ActionType == "banner")
                    {
                        FileUploadPath = Server.MapPath("~/Admin/Images/Category/Banner") + "\\";
                    }
                    if (ActionType == "Section")
                    {
                        FileUploadPath = Server.MapPath("~/Admin/Images/Section/Icon") + "\\";
                    }
                    if (ActionType == "SectionBanner")
                    {
                        FileUploadPath = Server.MapPath("~/Admin/Images/Section/Banner") + "\\";
                    }

                    if (ActionType == "LandingImage")
                    {
                        FileUploadPath = Server.MapPath("~/Images/LandingPage") + "\\";
                    }

                    if (Directory.Exists(FileUploadPath))
                    {
                        #region
                        //if (ActionType == "categorymenu")
                        //{
                        //    DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                        //    FileInfo[] files = di.GetFiles(CategoryId + "*.*");
                        //    foreach (FileInfo file in files)
                        //        try
                        //        {
                        //            file.Attributes = FileAttributes.Normal;
                        //            File.Delete(file.FullName);
                        //        }
                        //        catch { }
                        //}
                        //if (ActionType == "categorymouseover")
                        //{
                        //    DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                        //    FileInfo[] files = di.GetFiles(CategoryId + "*.*");
                        //    foreach (FileInfo file in files)
                        //        try
                        //        {
                        //            file.Attributes = FileAttributes.Normal;
                        //            File.Delete(file.FullName);
                        //        }
                        //        catch { }
                        //}
                        //if (ActionType == "category")
                        //{
                        //    DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                        //    FileInfo[] files = di.GetFiles(CategoryId + "*.*");
                        //    foreach (FileInfo file in files)
                        //        try
                        //        {
                        //            file.Attributes = FileAttributes.Normal;
                        //            File.Delete(file.FullName);
                        //        }
                        //        catch { }
                        //}
                        //if (ActionType == "banner")
                        //{
                        //    DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                        //    FileInfo[] files = di.GetFiles(CategoryId + "*.*");
                        //    foreach (FileInfo file in files)
                        //        try
                        //        {
                        //            file.Attributes = FileAttributes.Normal;
                        //            File.Delete(file.FullName);
                        //        }
                        //        catch { }
                        //}
                        //if (ActionType == "Section")
                        //{
                        //    DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                        //    FileInfo[] files = di.GetFiles(CategoryId + "*.*");
                        //    //FileInfo[] files = di.GetFiles(Convert.ToString(Session["SectionPageId"]) + "*.*");
                        //    foreach (FileInfo file in files)
                        //        try
                        //        {
                        //            file.Attributes = FileAttributes.Normal;
                        //            File.Delete(file.FullName);
                        //        }
                        //        catch { }
                        //}
                        //if (ActionType == "SectionBanner")
                        //{
                        //    DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                        //    FileInfo[] files = di.GetFiles(CategoryId + "*.*");
                        //    foreach (FileInfo file in files)
                        //        try
                        //        {
                        //            file.Attributes = FileAttributes.Normal;
                        //            File.Delete(file.FullName);
                        //        }
                        //        catch { }
                        //}
                        if (ActionType == "LandingImage")
                        {
                            DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                            FileInfo[] files = di.GetFiles();
                            foreach (FileInfo file in files)
                                try
                                {
                                    file.Attributes = FileAttributes.Normal;
                                    if (file.FullName.Contains("LandingPage"))
                                    {
                                        File.Delete(file.FullName);
                                    }
                                }
                                catch { }
                        }
                        #endregion
                    }
                    return;
                }
                //else
                //{

                /***********************************************************************************************************
                 * RECOMMENDED CONFIGURATION HERE
                 * The following parameters can be changed, and is reccomended to change them from here for security reason
                 ***********************************************************************************************************/
                string upload_path = string.Empty; //Server.MapPath(string.IsNullOrEmpty(Request.QueryString["ax-file-path"]) ? "" : Request.QueryString["ax-file-path"]);
                string max_file_size = string.IsNullOrEmpty(Request.QueryString["ax-maxFileSize"]) ? "1024M" : Request.QueryString["ax-maxFileSize"];
                string allow_ext_req = string.IsNullOrEmpty(Request.QueryString["ax-allow-ext"]) ? "" : Request.QueryString["ax-allow-ext"];
                string[] allow_ext = (allow_ext_req.Length > 0) ? allow_ext_req.Split('|') : new string[0];
                /**********************************************************************************************************/

                HttpPostedFile Postedfile = Request.Files[0];

                #region
                #region Added by Sripal
                if (ActionType == "LandingImage")
                {
                    Type = "FULandingPage";
                    ImageWidth = GlobalFunctions.GetSetting("IMAGEWIDTH_LandingPage");
                    ImageHeight = GlobalFunctions.GetSetting("IMAGEHEIGHT_LandingPage");

                    if (Postedfile.ContentLength > 0)
                    {
                        if (!checkImageDimensionLandingPageConfig(Postedfile, Convert.ToInt16(ImageWidth), Convert.ToInt16(ImageHeight)))
                        {
                            Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) + @""",""type"":""" + Type + @""",""status"":""-1"",""info"":""Invalid image dimension.""}");
                            return;
                        }
                    }
                    upload_path = Server.MapPath("~/Images/LandingPage") + "\\";

                }
                #endregion
                if (ActionType == "categorymenu")
                {
                    Type = "FUCategoryMenu";
                    if (Postedfile.ContentLength > 0)
                    {
                        if (!checkImageDimension(Postedfile, Convert.ToInt16(MenuImageWidth), Convert.ToInt16(MenuImageHeight)))
                     
                        {
                            Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) + @""",""type"":""" + Type + @""",""status"":""-1"",""info"":""Invalid image dimension.""}");
                            return;
                        }
                    }
                    upload_path = Server.MapPath("~/Admin/Images/Category/categoryMenu") + "\\";
                }
                if (ActionType == "categorymouseover")
                {
                    Type = "FUCategoryMouseOver";
                    if (Postedfile.ContentLength > 0)
                    {
                        if (!checkImageDimension(Postedfile, Convert.ToInt16(MenuImageWidth), Convert.ToInt16(MenuImageHeight)))
                        {
                            Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) + @""",""type"":""" + Type + @""",""status"":""-1"",""info"":""Invalid image dimension.""}");
                            return;
                          }
                    }
                    upload_path = Server.MapPath("~/Admin/Images/Category/MouseOver") + "\\";
                }
                if (ActionType == "category")
                {
                    Type = "FUCategory";
                    if (Postedfile.ContentLength > 0)
                    {
                        if (!checkImageDimension(Postedfile, Convert.ToInt16(ImageWidth), Convert.ToInt16(ImageHeight)))
                        {
                            Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) + @""",""type"":""" + Type + @""",""status"":""-1"",""info"":""Invalid image dimension.""}");
                            return;
                            //Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(full_path) + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""Generi error:" + ex.Message + @"""}");
                            //Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) +  @""",""status"":""-1"",""info"":""Invalid image dimension.""}");
                            //return;
                        }
                    }
                    upload_path = Server.MapPath("~/Admin/Images/Category") + "\\";
                    // ImageSrc = "Admin/Images/Templates/" + CategoryId + "/" + CategoryId + "." + Path.GetExtension(Request.Files[0].FileName);
                }
                if (ActionType == "banner")
                {
                    Type = "FUBanner";
                    if (Postedfile.ContentLength > 0)
                    {
                        //if (!checkImageDimension(Postedfile, Convert.ToInt16(BannerImageWidth), Convert.ToInt16(BannerImageHeight)))
                        //{
                        //    Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) + @""",""type"":""" + Type + @""",""status"":""-1"",""info"":""Invalid image .""}");
                        //    return;
                        //}
                    }
                    upload_path = Server.MapPath("~/Admin/Images/Category/Banner") + "\\";
                    // ImageSrc = "Admin/Images/Templates/" + CategoryId + "/" + CategoryId + "." + Path.GetExtension(Request.Files[0].FileName);
                }
                if (ActionType == "Section")
                {
                    Type = "FUIcon";
                    /*if (!checkImageDimension(Postedfile, Convert.ToInt16(ImageWidth), Convert.ToInt16(ImageHeight)))
                    {
                        Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) + @""",""type"":""" + Type + @""",""status"":""-1"",""info"":""Invalid image dimension.""}");
                        return;
                    }*/
                    upload_path = Server.MapPath("~/Admin/Images/Section/Icon") + "\\";
                }
                if (ActionType == "SectionBanner")
                {
                    Type = "FUBann";
                    /*if (!checkImageDimension(Postedfile, Convert.ToInt16(BannerImageWidth), Convert.ToInt16(BannerImageHeight)))
                    {
                        Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) + @""",""type"":""" + Type + @""",""status"":""-1"",""info"":""Invalid image dimension.""}");
                        return;
                    }*/
                    upload_path = Server.MapPath("~/Admin/Images/Section/Banner") + "\\";
                }
                #endregion

                /************************************************************************************************************
                * Settings for thumbnail generation, can be changed here or from js
                ************************************************************************************************************/
                //int thumb_height = string.IsNullOrEmpty(Request.QueryString["ax-thumbHeight"]) ? 0 : Convert.ToInt32(Request.QueryString["ax-thumbHeight"]);
                //int thumb_width = string.IsNullOrEmpty(Request.QueryString["ax-thumbWidth"]) ? 0 : Convert.ToInt32(Request.QueryString["ax-thumbWidth"]);
                //string thumb_post_fix = string.IsNullOrEmpty(Request.QueryString["ax-thumbPostfix"]) ? "_thumb" : Request.QueryString["ax-thumbPostfix"];
                //string thumb_path = string.IsNullOrEmpty(Request.QueryString["ax-thumbPath"]) ? "" : Request.QueryString["ax-thumbPath"];
                //string thumb_format = string.IsNullOrEmpty(Request.QueryString["ax-thumbFormat"]) ? "png" : Request.QueryString["ax-thumbFormat"];
                /**********************************************************************************************************/

                /********************************************************************************************************
                * HTML5 UPLOAD PARAMETERS, NOT TO CHANGE 
                ********************************************************************************************************/
                string file_name = string.IsNullOrEmpty(Request.QueryString["qqfilename"]) ? "" : Request.QueryString["qqfilename"];
                int curr_byte = string.IsNullOrEmpty(Request.QueryString["qqchunksize"]) ? 0 : Convert.ToInt32(Request.QueryString["qqchunksize"]);
                int full_size = string.IsNullOrEmpty(Request.QueryString["qqtotalfilesize"]) ? 0 : Convert.ToInt32(Request.QueryString["qqtotalfilesize"]);

                int part_index = string.IsNullOrEmpty(Request.QueryString["qqpartindex"]) ? 0 : Convert.ToInt32(Request.QueryString["qqpartindex"]);
                int total_parts = string.IsNullOrEmpty(Request.QueryString["qqtotalparts"]) ? 0 : Convert.ToInt32(Request.QueryString["qqtotalparts"]);
                string is_last = string.Empty;
                if (part_index == (total_parts - 1))
                {
                    is_last = "true";
                }
                else
                    is_last = "false";
                //string is_last = string.IsNullOrEmpty(Request.QueryString["qqpartindex"]) ? "true" : Request.QueryString["isLast"];
                //bool is_ajax = (string.IsNullOrEmpty(Request.QueryString["isLast"]) && string.IsNullOrEmpty(Request.QueryString["ax-start-byte"]));
                bool is_ajax;
                if (Convert.ToBoolean(Request.QueryString["isLast"]) == false || Convert.ToInt32(Request.QueryString["qqchunksize"]) > 0)
                {
                    is_ajax = true;
                }
                else
                {
                    is_ajax = false;
                }

                /**********************************************************************************************************/
                /*
                * Create upload path if do not exits
                */
                if (!System.IO.File.Exists(upload_path))
                {
                    System.IO.Directory.CreateDirectory(upload_path);
                }

                #region "Comments"
                /*
                * Create thumb path if do not exits
                */
                //if (!System.IO.File.Exists(thumb_path) && thumb_path.Length > 0)
                //{
                //    System.IO.Directory.CreateDirectory(thumb_path);
                //}
                //else
                //{
                //    thumb_path = upload_path;
                //} 
                #endregion

                //Start upload controls
                // file_name = string.IsNullOrEmpty(file_name) ? Request.Files[0].FileName : file_name;
                file_name = string.IsNullOrEmpty(file_name) ? Path.GetFileName(Request.Files[0].FileName) : file_name;

                if (ActionType == "category")
                {
                    ImageSrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Category/" + CategoryId + Path.GetExtension(file_name);
                }
                if (ActionType == "categorymouseover")
                {
                    ImageSrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Category/MouseOver" + CategoryId + Path.GetExtension(file_name);
                }
                if (ActionType == "categorymenu")
                {
                    ImageSrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Category/categoryMenu" + CategoryId + Path.GetExtension(file_name);
                }
                if (ActionType == "banner")
                {
                    ImageSrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Category/Banner/" + CategoryId + Path.GetExtension(file_name);
                }
                if (ActionType == "Section")
                {
                    ImageSrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Section/Icon/" + CategoryId + Path.GetExtension(file_name);
                }
                if (ActionType == "SectionBanner")
                {
                    ImageSrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Section/Banner/" + CategoryId + Path.GetExtension(file_name);
                }
                if (ActionType == "LandingImage")
                {
                    ImageSrc = GlobalFunctions.GetVirtualPath() + "Images/LandingPage/LandingPage" + Path.GetExtension(file_name);
                }
                full_size = (full_size > 0) ? full_size : Request.Files[0].ContentLength;
                //check file size
                if (!checkSize(full_size, max_file_size))
                {
                    Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""Max file size execced""}");
                    return;
                }

                //check file name
                string tmp_fn = file_name;
                file_name = checkName(file_name);
                if (file_name.Length == 0)
                {
                    Response.Write(@"{""name"":""" + tmp_fn + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""File name not allowed""}");
                    return;
                }

                //check file ext
                if (!checkExt(file_name, allow_ext))
                {
                    Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""File extension not allowed""}");
                    return;
                }

                string full_path = "";

                is_last = "true";

                try
                {

                    #region Checking Security Feature
                    file_name = file_name.Replace(" ", "").Trim();
                    string ShowDivOnInvalid = "";

                   
                        // do not check for category image existance
                   full_path = checkFileExists(file_name, upload_path);
                  
                    HttpPostedFile file = Request.Files[0];
                    if (ActionType == "LandingImage")
                    {
                        file.SaveAs(full_path);
                    }
                    else if (ActionType == "categorymenu")
                    {
                        // here image will be uploaded as category_langaugeid
                        string newimgsrc = string.Empty;
                        string NewcategoryId = string.Empty;
                        if (LanguageId != null && LanguageId!="0")
                        {
                            string[] filenamewithext = file_name.Split('.');
                            string Newfilename = Convert.ToString(CategoryId);
                            string extension = Convert.ToString(filenamewithext[1]);
                            full_path = upload_path + Newfilename + "_" + LanguageId + "." + extension;
                            newimgsrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Category/categoryMenu/" + Newfilename + "_" + LanguageId + "." + extension;
                            NewcategoryId = CategoryId + "_" + LanguageId;
                             
                            file.SaveAs(full_path);
                            ShowDivOnInvalid = "FUCategoryMenu";
                            ImageSrc = newimgsrc;
                        }
                        else
                        {
                            #region To add image for multiple language
                            StoreBE objstore = (StoreBE)HttpContext.Current.Cache["StoreDetails"];
                            List<StoreBE.StoreLanguageBE> lstStoreLanguageBE = objstore.StoreLanguages;
                          
                            for (int i = 0; i < objstore.StoreLanguages.Count; i++)
                            {

                                string[] filenamewithext = file_name.Split('.');
                                //string Newfilename = Convert.ToString(filenamewithext[0]);
                                string Newfilename = Convert.ToString(CategoryId);
                                string extension = Convert.ToString(filenamewithext[1]);

                                if (lstStoreLanguageBE.Count > i)
                                {
                                    full_path = upload_path + Newfilename + "_" + lstStoreLanguageBE[i].LanguageId.ToString() + "." + extension;
                                    // by default image for first language shown and others will get uploaded along with first
                                    if (i == 0)
                                    {
                                        newimgsrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Category/categoryMenu/" + Newfilename + "_" + lstStoreLanguageBE[i].LanguageId.ToString() + "." + extension;
                                        NewcategoryId = CategoryId + "_" + lstStoreLanguageBE[i].LanguageId.ToString();
                                    }
                                }
                                else
                                {
                                    full_path = upload_path + " " + Newfilename + "_";
                                }
                                file.SaveAs(full_path);
                                ShowDivOnInvalid = "FUCategoryMenu";
                            }
                            ImageSrc = newimgsrc;
                            CategoryId = NewcategoryId;
                            #endregion
                        }
                    }
                    else if (ActionType == "categorymouseover")
                    {
                        // here image will be uploaded as category_MouseOver_langaugeid
                         string newimgsrc = string.Empty;
                        string NewcategoryId = string.Empty;
                        if (LanguageId != null && LanguageId != "0")
                        {
                            string[] filenamewithext = file_name.Split('.');
                            string Newfilename = Convert.ToString(CategoryId)+"_"+"MouseOver";
                            string extension = Convert.ToString(filenamewithext[1]);
                            full_path = upload_path + Newfilename + "_" + LanguageId + "." + extension;
                            newimgsrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Category/MouseOver/" + Newfilename + "_" + LanguageId + "." + extension;
                            NewcategoryId = CategoryId + "_" + LanguageId;

                            file.SaveAs(full_path);
                            ShowDivOnInvalid = "FUCategoryMouseOver";
                            ImageSrc = newimgsrc;
                        }
                        else
                        {
                            #region To add image for multiple language
                            StoreBE objstore = (StoreBE)HttpContext.Current.Cache["StoreDetails"];
                            List<StoreBE.StoreLanguageBE> lstStoreLanguageBE = objstore.StoreLanguages;

                            for (int i = 0; i < objstore.StoreLanguages.Count; i++)
                            {

                                string[] filenamewithext = file_name.Split('.');
                                //string Newfilename = Convert.ToString(filenamewithext[0]);
                                string Newfilename = Convert.ToString(CategoryId) + "_" + "MouseOver";
                                string extension = Convert.ToString(filenamewithext[1]);

                                if (lstStoreLanguageBE.Count > i)
                                {
                                    full_path = upload_path + Newfilename + "_" + lstStoreLanguageBE[i].LanguageId.ToString() + "." + extension;
                                    // by default image for first language shown and others will get uploaded along with first
                                    if (i == 0)
                                    {
                                        newimgsrc = GlobalFunctions.GetVirtualPath() + "Admin/Images/Category/MouseOver/" + Newfilename + "_" + lstStoreLanguageBE[i].LanguageId.ToString() + "." + extension;
                                        NewcategoryId = CategoryId + "_" + lstStoreLanguageBE[i].LanguageId.ToString();
                                    }
                                }
                                else
                                {
                                    full_path = upload_path + " " + Newfilename + "_";
                                }
                                file.SaveAs(full_path);
                                ShowDivOnInvalid = "FUCategoryMouseOver";
                            }
                            ImageSrc = newimgsrc;
                            CategoryId = NewcategoryId;
                            #endregion
                        }
                    }
                    else if (ActionType == "category")
                    {
                        file.SaveAs(full_path);
                        ShowDivOnInvalid = "FUCategory";
                    }
                    else if (ActionType == "banner")
                    {
                        file.SaveAs(full_path);
                        ShowDivOnInvalid = "FUBanner";
                    }
                    else if (ActionType == "Section")
                    {
                        file.SaveAs(full_path);
                        ShowDivOnInvalid = "FUIcon";
                    }
                    else if (ActionType == "SectionBanner")
                    {
                        file.SaveAs(full_path);
                        ShowDivOnInvalid = "";
                    }
                    else
                    {
                        file.SaveAs(full_path);
                    }
                    //Grab the content type of your file
                    //var actualType = GlobalFunctions.GetContentType(full_path);
                    //Check if it has one of the appropriate types
                    if (GlobalFunctions.IsValidMIMEImageType(full_path))
                    {
                        //It is a valid  file - do work here
                        Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""type"":""" + ShowDivOnInvalid + @""",""status"":""1"",""info"":""Uploaded.""}");
                    }
                    else
                    {
                        Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""type"":""" + ShowDivOnInvalid + @""",""status"":""-1"",""info"":""Invalid File Type.""}");
                        File.Delete(full_path);
                        return;
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(full_path) + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""Generi error:" + ex.Message + @"""}");
                    return;
                }
                //}
                Response.Clear();
                Response.Write("{\"success\":true,\"type\":\"" + ActionType + "\",\"src\":\"" + ImageSrc + "\",\"CategoryId\":\"" + CategoryId + "\"}");
            }
            catch (Exception ex)
            {
                //Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(Postedfile.FileName) + @""",""type"":""" + Type + @""",""status"":""-1"",""info"":""Invalid image dimension.""}");
                Exceptions.WriteExceptionLog(ex);
            }
            #region "Comment"
            //Response.Write("true|" + ActionType);

            //if (is_last.Equals("true"))
            //{
            //    createThumb(full_path, thumb_path, thumb_post_fix, thumb_width, thumb_height, thumb_format);
            //    Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(full_path) + @""",""size"":""" + full_size.ToString() + @""",""status"":""1"",""info"":""File uploaded""}");
            //}
            //} 
            #endregion
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image image, Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }

            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }


        /**
         * 
         * Check if file size is allowed
         * @param unknown_type $size
         * @param unknown_type $max_file_size
         */
        public bool checkSize(int file_size, string max_file_size)
        {
            string mult = max_file_size.Substring(Math.Max(0, max_file_size.Length - 1));
            int msize = Convert.ToInt32(max_file_size.Replace(mult, ""));
            int max_size;

            switch (mult)
            {
                case "T":
                    max_size = msize * 1024 * 1024 * 1024 * 1024; break;
                case "G":
                    max_size = msize * 1024 * 1024 * 1024; break;
                case "M":
                    max_size = msize * 1024 * 1024; break;
                case "K":
                    max_size = msize * 1024; break;
                default:
                    max_size = 4 * 1024 * 1024; break;
            }

            if (file_size > max_size)
            {
                return false;
            }
            return true;
        }

        /**
         * Check if filename is allowed
         */
        public string checkName(string filename)
        {
            string[] windowsReserved = new string[] { "CON", "PRN", "AUX", "NUL", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9" };
            string[] badWinChars = new string[] { "<", ">", ":", @"\", "/", "|", "?", "*" };

            for (int i = 0; i < badWinChars.Length; i++)
            {
                filename.Replace(badWinChars[i], "");
            }
            //check if legal windows file name
            if (Array.IndexOf(windowsReserved, filename) >= 0)
            {
                return "";
            }
            return filename;
        }

        public bool checkExt(string filename, string[] allow_ext)
        {
            string file_ext = System.IO.Path.GetExtension(filename).Replace(".", "");
            file_ext = file_ext.ToLower();

            string[] deny_ext = new string[] { "php", "php3", "php4", "php5", "phtml", "exe", "pl", "cgi", "html", "htm", "js", "asp", "aspx", "bat", "sh", "cmd" };

            if (Array.IndexOf(deny_ext, file_ext) >= 0)
            {
                return false;
            }

            if (Array.IndexOf(allow_ext, file_ext) < 0 && allow_ext.Length > 0)
            {
                return false;
            }

            return true;
        }

        public string checkFileExists(string filename, string upload_path)
        {
            string file_ext = System.IO.Path.GetExtension(filename).Replace(".", "");
            string file_base = System.IO.Path.GetFileNameWithoutExtension(filename);
            string full_path = string.Empty;

            if (ActionType == "category")
            {
                full_path = upload_path + CategoryId + "." + file_ext;
            }
            if (ActionType == "categorymouseover")
            {
                full_path = upload_path + CategoryId + "." + file_ext;
            }
            if (ActionType == "categorymenu")
            {
                full_path = upload_path + CategoryId + "." + file_ext;
            }
            if (ActionType == "banner")
            {
                full_path = upload_path + CategoryId + "." + file_ext;
            }
            if (ActionType == "Section")
            {
                full_path = upload_path + CategoryId + "." + file_ext;
            }
            if (ActionType == "SectionBanner")
            {
                full_path = upload_path + CategoryId + "." + file_ext;
            }
            if (ActionType == "LandingImage")
            {
                if (Session["LanguageId"] != null)
                {
                    full_path = upload_path + "LandingPage" + "." + file_ext;
                    Session["LandingImageName"] = "LandingPage" + "." + file_ext;
                }
                else
                {
                    full_path = upload_path + "LandingPage." + file_ext;
                    Session["LandingImageName"] = "LandingPage." + file_ext;
                }
                if (File.Exists(full_path))
                {
                    File.Delete(full_path);
                }
            }



            //avoid file override, check if file exists and generate another name
            //to override file with same name just disable this while
            //int c = 0;
            //while (System.IO.File.Exists(full_path))
            //{
            //    c++;
            //    filename = file_base + "(" + c.ToString() + ")." + file_ext;
            //    full_path = upload_path + filename;
            //}
            return full_path;
        }
        public bool checkImageDimension(HttpPostedFile FileId, Int16 ImageValidWidth, Int16 ImageValidHeight)
        {

            System.Drawing.Image ObjUploadedImageFile = null;
            bool blnResult = false;

            //Will capture uploaded image file for validation demensions

            ObjUploadedImageFile = System.Drawing.Image.FromStream(FileId.InputStream);
            if ((ObjUploadedImageFile.Width <= ImageValidWidth) && (ObjUploadedImageFile.Height <= ImageValidHeight))
                blnResult = true;
            else
                blnResult = false;
            ObjUploadedImageFile = null;
            return blnResult;
        }

        //Added by Sripal
        public bool checkImageDimensionLandingPageConfig(HttpPostedFile FileId, Int16 ImageValidWidth, Int16 ImageValidHeight)
        {

            System.Drawing.Image ObjUploadedImageFile = null;
            bool blnResult = false;

            //Will capture uploaded image file for validation demensions

            ObjUploadedImageFile = System.Drawing.Image.FromStream(FileId.InputStream);
            if ((ObjUploadedImageFile.Width >= ImageValidWidth) && (ObjUploadedImageFile.Height >= ImageValidHeight))
                blnResult = true;
            else
                blnResult = false;
            ObjUploadedImageFile = null;
            return blnResult;
        }

    }

}

