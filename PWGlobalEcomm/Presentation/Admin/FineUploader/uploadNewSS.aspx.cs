﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

namespace Presentation
{
    public class Admin_FineUploader_uploadNewSS : System.Web.UI.Page
    {
        string CatalogId = string.Empty;
        string ImageSrc = string.Empty;

        public string thumbSrc = string.Empty;
        string ActionType = string.Empty;
        string activeThumb = string.Empty;

        string webOrderBy = string.Empty;
        string Alttext = string.Empty;
        string LinkVal = string.Empty;
        string Title = string.Empty;
        string SubTitle = string.Empty;
        string SlideShowDetailId = string.Empty;

        string NewWindow = "False";
        string SlideShowImageId = string.Empty;
        public string TempSlideShowImageId = "";

        #region CSRF
        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        /*Sachin Chauhan End : 22 02 2016 */

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 18-08-15
        /// Scope   : Page_PreInit event of the page
        /// Modified by : Sachin Chauhan
        /// Modified Date : 22 02 2016
        /// Modify Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void Page_Init(object sender, EventArgs e)
        {
            /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
            #region "CSRF prevention code"
            try
            {
                //First, check for the existence of the Anti-XSS cookie
                var requestCookie = Request.Cookies[AntiXsrfTokenKey];
                Guid requestCookieGuidValue;

                //If the CSRF cookie is found, parse the token from the cookie.
                //Then, set the global page variable and view state user
                //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
                //method.
                if (requestCookie != null
                && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
                {
                    //Set the global token variable so the cookie value can be
                    //validated against the value in the view state form field in
                    //the Page.PreLoad method.
                    _antiXsrfTokenValue = requestCookie.Value;

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;
                }
                //If the CSRF cookie is not found, then this is a new session.
                else
                {
                    //Generate a new Anti-XSRF token
                    _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;

                    //Create the non-persistent CSRF cookie
                    var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                    {
                        //Set the HttpOnly property to prevent the cookie from
                        //being accessed by client side script
                        HttpOnly = true,

                        //Add the Anti-XSRF token to the cookie value
                        Value = _antiXsrfTokenValue
                    };

                    //If we are using SSL, the cookie should be set to secure to
                    //prevent it from being sent over HTTP connections
                    if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                        responseCookie.Secure = true;

                    //Add the CSRF cookie to the response
                    Response.Cookies.Set(responseCookie);
                }

                Page.PreLoad += Page_PreLoad;
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            #endregion
            /*Sachin Chauhan End : 22 02 2016*/
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date : 22 02 2016
        /// Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            //During the initial page load, add the Anti-XSRF token and user
            //name to the ViewState
            if (!IsPostBack)
            {
                //Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

                //If a user name is assigned, set the user name
                //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                if (Session["ContextUserGUID"] == null)
                    Session["ContextuserGUID"] = Guid.NewGuid().ToString();

                ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
            }
            //During all subsequent post backs to the page, the token value from
            //the cookie should be validated against the token in the view state
            //form field. Additionally user name should be compared to the
            //authenticated users name
            else
            {
                //Validate the Anti-XSRF token
                //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())

                    Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                /************************************************************************
                 * Email configuration
                 * **********************************************************************/
                //bool send_email = false;                //enable email notification
                //string main_receiver = "albanx@gmail.com";   //who receive the email
                //string cc = "";                   //other receivers in cc
                //string from = "from@ajaxupload.com";//who appear in the from field
                /*****************************************************************************/

                TempSlideShowImageId = Guid.NewGuid().ToString();
                ActionType = Convert.ToString(Request.QueryString["type"]);
                SlideShowImageId = TempSlideShowImageId;
                activeThumb = Convert.ToString(Request.QueryString["activeThumb"]);

                webOrderBy = Convert.ToString(Request.QueryString["position"]);

                Alttext = Convert.ToString(Request.QueryString["Alttext"]);
                LinkVal = Convert.ToString(Request.QueryString["LinkVal"]);
                Title = Convert.ToString(Request.QueryString["Title"]);
                SubTitle = Convert.ToString(Request.QueryString["SubTitle"]);

                SlideShowDetailId = Convert.ToString(Request.QueryString["SlideShowDetailId"]);


                if (Request.QueryString["NewWindow"] != "")
                    NewWindow = Convert.ToString(Request.QueryString["NewWindow"]);

                if (Request.QueryString["action"] == "remove")
                {
                    string tempFileName = string.Empty;

                    string FileUploadPath = string.Empty;
                    if (ActionType == "slideshow")
                    {
                        FileUploadPath = Server.MapPath("~/Admin/Images/SlideShow") + "\\";
                    }

                    if (Directory.Exists(FileUploadPath))
                    {
                        if (ActionType == "slideshow")
                        {
                            DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                            FileInfo[] files = di.GetFiles(SlideShowImageId + "*.*");
                            foreach (FileInfo file1 in files)
                                try
                                {
                                    file1.Attributes = FileAttributes.Normal;
                                    File.Delete(file1.FullName);
                                }
                                catch { }
                        }

                    }

                    Dictionary<string, string> lstSlideShowDetailIdList = new Dictionary<string, string>();
                    if (Session["SlideShowDetailIdList"] != null)
                    {
                        lstSlideShowDetailIdList = (Dictionary<string, string>)Session["SlideShowDetailIdList"];
                    }

                    List<ImageSlideShowBE.SlideShowImage> SlideShowBOListtemp = new List<ImageSlideShowBE.SlideShowImage>();
                    if (Session["SlideShowList"] != null)
                    {
                        SlideShowBOListtemp = (List<ImageSlideShowBE.SlideShowImage>)Session["SlideShowList"];
                    }
                    ImageSlideShowBE.SlideShowImage tempObj = new ImageSlideShowBE.SlideShowImage();
                    if (SlideShowBOListtemp != null)
                    {
                        foreach (ImageSlideShowBE.SlideShowImage element in SlideShowBOListtemp)
                        {
                            if (element.ActiveThumb == activeThumb)
                            {
                                tempFileName = element.TempImageName;
                                tempObj = element;
                                if (SlideShowDetailId != "")
                                {
                                    if (!lstSlideShowDetailIdList.ContainsKey(SlideShowDetailId))
                                    {
                                        lstSlideShowDetailIdList.Add(SlideShowDetailId, tempFileName);
                                        Session["SlideShowDetailIdList"] = lstSlideShowDetailIdList;
                                    }
                                }
                            }
                        }
                    }

                    if (tempObj != null)
                    {
                        List<ImageSlideShowBE.SlideShowImage> SlideShowBOListRemove = (List<ImageSlideShowBE.SlideShowImage>)Session["SlideShowList"];
                        SlideShowBOListRemove.Remove(tempObj);
                        Session["SlideShowList"] = SlideShowBOListRemove;
                    }
                    return;
                }






                /***********************************************************************************************************
                 * RECOMMENDED CONFIGURATION HERE
                 * The following parameters can be changed, and is reccomended to change them from here for security reason
                 ***********************************************************************************************************/
                string file_name = string.IsNullOrEmpty(Request.QueryString["qqfilename"]) ? "" : Request.QueryString["qqfilename"];
                string upload_path = string.Empty; //Server.MapPath(string.IsNullOrEmpty(Request.QueryString["ax-file-path"]) ? "" : Request.QueryString["ax-file-path"]);
                string max_file_size = string.IsNullOrEmpty(Request.QueryString["ax-maxFileSize"]) ? "1024M" : Request.QueryString["ax-maxFileSize"];
                string allow_ext_req = string.IsNullOrEmpty(Request.QueryString["ax-allow-ext"]) ? "" : Request.QueryString["ax-allow-ext"];
                string[] allow_ext = (allow_ext_req.Length > 0) ? allow_ext_req.Split('|') : new string[0];
                /**********************************************************************************************************/
                HttpPostedFile Postedfile = Request.Files[0];
                file_name = string.IsNullOrEmpty(file_name) ? SlideShowImageId + Path.GetExtension(Postedfile.FileName) : file_name;
                upload_path = Server.MapPath("~/Images/SlideShow/Temp");
                if (ActionType == "slideshow")
                {
                    ImageSrc = GlobalFunctions.GetVirtualPath() + "Images/SlideShow/Temp/" + SlideShowImageId + Path.GetExtension(file_name);

                }
                // ImageSrc = "../Images/SlideShow/Temp" + "/" + file_name;


                /************************************************************************************************************
                * Settings for thumbnail generation, can be changed here or from js
                ************************************************************************************************************/
                //int thumb_height = string.IsNullOrEmpty(Request.QueryString["ax-thumbHeight"]) ? 0 : Convert.ToInt32(Request.QueryString["ax-thumbHeight"]);
                //int thumb_width = string.IsNullOrEmpty(Request.QueryString["ax-thumbWidth"]) ? 0 : Convert.ToInt32(Request.QueryString["ax-thumbWidth"]);
                //string thumb_post_fix = string.IsNullOrEmpty(Request.QueryString["ax-thumbPostfix"]) ? "_thumb" : Request.QueryString["ax-thumbPostfix"];
                //string thumb_path = string.IsNullOrEmpty(Request.QueryString["ax-thumbPath"]) ? "" : Request.QueryString["ax-thumbPath"];
                //string thumb_format = string.IsNullOrEmpty(Request.QueryString["ax-thumbFormat"]) ? "png" : Request.QueryString["ax-thumbFormat"];
                /**********************************************************************************************************/

                /********************************************************************************************************
                * HTML5 UPLOAD PARAMETERS, NOT TO CHANGE 
                ********************************************************************************************************/

                int curr_byte = string.IsNullOrEmpty(Request.QueryString["qqchunksize"]) ? 0 : Convert.ToInt32(Request.QueryString["qqchunksize"]);
                int full_size = string.IsNullOrEmpty(Request.QueryString["qqtotalfilesize"]) ? 0 : Convert.ToInt32(Request.QueryString["qqtotalfilesize"]);

                int part_index = string.IsNullOrEmpty(Request.QueryString["qqpartindex"]) ? 0 : Convert.ToInt32(Request.QueryString["qqpartindex"]);
                int total_parts = string.IsNullOrEmpty(Request.QueryString["qqtotalparts"]) ? 0 : Convert.ToInt32(Request.QueryString["qqtotalparts"]);
                string is_last = string.Empty;

                if (part_index == (total_parts - 1))
                {
                    is_last = "true";
                }
                else
                    is_last = "false";
                //string is_last = string.IsNullOrEmpty(Request.QueryString["qqpartindex"]) ? "true" : Request.QueryString["isLast"];
                //bool is_ajax = (string.IsNullOrEmpty(Request.QueryString["isLast"]) && string.IsNullOrEmpty(Request.QueryString["ax-start-byte"]));
                bool is_ajax;
                if (Convert.ToBoolean(Request.QueryString["isLast"]) == false || Convert.ToInt32(Request.QueryString["qqchunksize"]) > 0)
                {
                    is_ajax = true;
                }
                else
                {
                    is_ajax = false;
                }

                /*
                * Create upload path if do not exits
                */
                if (!System.IO.File.Exists(upload_path))
                {
                    System.IO.Directory.CreateDirectory(upload_path);
                }

                /*
                * Create thumb path if do not exits
                */
                file_name = string.IsNullOrEmpty(file_name) ? SlideShowImageId + Path.GetExtension(Postedfile.FileName) : file_name;


                //ImageSrc = "" + "\\" + file_name;

                full_size = (full_size > 0) ? full_size : Request.Files[0].ContentLength;
                //check file size
                if (!checkSize(full_size, max_file_size))
                {
                    Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""Max file size execced""}");
                    return;
                }


                //check file name
                string tmp_fn = file_name;
                file_name = checkName(file_name);
                if (file_name.Length == 0)
                {
                    Response.Write(@"{""name"":""" + tmp_fn + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""File name not allowed""}");
                    return;
                }

                //check file ext
                if (!checkExt(file_name, allow_ext))
                {
                    Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""File extension not allowed""}");
                    return;
                }

                HttpPostedFile file = Request.Files[0];
                System.Drawing.Image myImage = System.Drawing.Image.FromStream(file.InputStream);
                //if (!checkFileDimensions(myImage))
                //{
                //  Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""type"":""" + ActionType + @""",""activeThumb"":""" + activeThumb + @""",""info"":""File dimension not allowed""}");
                //   return;
                // }

                string full_path = "";
                string thumbNailFileName = string.Empty;
                is_last = "true";
                full_path = checkFileExists(file_name, upload_path);
                try
                {
                    #region  Security Test Feature


                    file_name = file_name.Replace(" ", "").Trim();

                    string ShowDivOnInvalid = "";

                    full_path = checkFileExists(file_name, upload_path);

                   // HttpPostedFile file = Request.Files[0];
                    if (ActionType == "slideshow")
                    {
                        file.SaveAs(full_path);
                        //ShowDivOnInvalid = "FUSlideShow";
                    }
                    

                    //Grab the content type of your file
                    //var actualType = GlobalFunctions.GetContentType(full_path);
                    //Check if it has one of the appropriate types
                    if (GlobalFunctions.IsValidMIMEImageType(full_path))
                    {
                        //It is a valid  file - do work here
                        
                    }
                    else
                    {
                        Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""Invalid File Type.""}");
                       // Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""type"":""" + ShowDivOnInvalid + @""",""status"":""-1"",""info"":""Invalid File Type.""}");
                        File.Delete(full_path);
                        return;
                    }





                    #endregion End
                    file.SaveAs(full_path);

                    if (ActionType == "slideshow")
                    {
                        string file_ext = System.IO.Path.GetExtension(file_name).Replace(".", "");
                        string f_name = file_name.Replace("." + file_ext, "");
                        thumbNailFileName = f_name + "_thumb." + file_ext;
                        System.Drawing.Image original = System.Drawing.Image.FromFile(upload_path + "\\" + file_name);
                        System.Drawing.Image resized = ResizeImage(original, new Size(152, 60), false);
                        MemoryStream memStream = new MemoryStream();
                        resized.Save(memStream, System.Drawing.Imaging.ImageFormat.Png);

                        FileStream file1 = new FileStream(upload_path + "\\" + f_name + "_thumb." + file_ext, FileMode.Create, FileAccess.Write);
                        memStream.WriteTo(file1);
                        file1.Close();
                        memStream.Close();
                        original.Dispose();
                        resized.Dispose();
                    }

                }
                catch (Exception ex)
                {
                    Response.Write(@"{""name"":""" + file_name + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""Invalid File Type.""}");
                    Exceptions.WriteExceptionLog(ex);
                    //Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(full_path) + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""Generi error:" + ex.Message + @"""}");
                    return;
                }




                Response.Clear();

                if (ActionType == "slideshow")
                    thumbSrc = GlobalFunctions.GetVirtualPath() + "Images/SlideShow/Temp/" + SlideShowImageId + "_thumb" + Path.GetExtension(file_name);
                Response.Write("{\"success\":true,\"thumbSrc\":\"" + thumbSrc + "\",\"position\":\"" + webOrderBy + "\",\"type\":\"" + ActionType + "\",\"src\":\"" + ImageSrc + "\",\"activeThumb\":\"" + activeThumb + "\",\"originalFileName\":\"" + file_name + "\",\"thumbNailFileName\":\"" + thumbNailFileName + "\"}");
                return;
            }
            catch (Exception ex)
            {
                
                Exceptions.WriteExceptionLog(ex);
                //Response.Write(@"{""name"":""" + System.IO.Path.GetFileName(full_path) + @""",""size"":""" + full_size.ToString() + @""",""status"":""-1"",""info"":""Generi error:" + ex.Message + @"""}");
                return;
            }

        }

        public bool checkFileDimensions(System.Drawing.Image myImage)
        {
            int height = 0, width = 0;

            if (ActionType == "slideshow")
            {
                height = Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_SLIDESHOW"));
                width = Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_SLIDESHOW"));
                //if (myImage.Height == height && myImage.Width == width) return true;
                //else return false;
            }


            //if (myImage.Height <= height && myImage.Width <= width) return true;
            //else return false;
            if (myImage.Height <= height && myImage.Width <= width) return true;
            else return false;
        }


        public static System.Drawing.Image ResizeImage(System.Drawing.Image image, Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {

                newWidth = size.Width;
                newHeight = size.Height;
            }

            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }


        /**
         * 
         * Check if file size is allowed
         * @param unknown_type $size
         * @param unknown_type $max_file_size
         */
        public bool checkSize(int file_size, string max_file_size)
        {
            string mult = max_file_size.Substring(Math.Max(0, max_file_size.Length - 1));
            int msize = Convert.ToInt32(max_file_size.Replace(mult, ""));
            int max_size;

            switch (mult)
            {
                case "T":
                    max_size = msize * 1024 * 1024 * 1024 * 1024; break;
                case "G":
                    max_size = msize * 1024 * 1024 * 1024; break;
                case "M":
                    max_size = msize * 1024 * 1024; break;
                case "K":
                    max_size = msize * 1024; break;
                default:
                    max_size = 4 * 1024 * 1024; break;
            }

            if (file_size > max_size)
            {
                return false;
            }
            return true;
        }

        /**
         * Check if filename is allowed
         */
        public string checkName(string filename)
        {
            string[] windowsReserved = new string[] { "CON", "PRN", "AUX", "NUL", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9" };
            string[] badWinChars = new string[] { "<", ">", ":", @"\", "/", "|", "?", "*" };

            for (int i = 0; i < badWinChars.Length; i++)
            {
                filename.Replace(badWinChars[i], "");
            }
            //check if legal windows file name
            if (Array.IndexOf(windowsReserved, filename) >= 0)
            {
                return "";
            }
            return filename;
        }

        public bool checkExt(string filename, string[] allow_ext)
        {
            string file_ext = System.IO.Path.GetExtension(filename).Replace(".", "");
            file_ext = file_ext.ToLower();

            string[] deny_ext = new string[] { "php", "php3", "php4", "php5", "phtml", "exe", "pl", "cgi", "html", "htm", "js", "asp", "aspx", "bat", "sh", "cmd" };

            if (Array.IndexOf(deny_ext, file_ext) >= 0)
            {
                return false;
            }

            if (Array.IndexOf(allow_ext, file_ext) < 0 && allow_ext.Length > 0)
            {
                return false;
            }

            return true;
        }


        public string checkFileExists(string filename, string upload_path)
        {
            string file_ext = System.IO.Path.GetExtension(filename).Replace(".", "");
            string file_base = System.IO.Path.GetFileNameWithoutExtension(filename);
            string full_path = string.Empty;

            full_path = upload_path + "\\" + filename;

            if (File.Exists(full_path))
            {
                File.Delete(full_path);
            }

            return full_path;
        }



    }
}
