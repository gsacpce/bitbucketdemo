﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    
    public class Admin_OrderTrackingManagement_OrderListing : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.TextBox txtCustomerSearch;
        protected global::System.Web.UI.WebControls.GridView gvOrders;

        public string host = GlobalFunctions.GetVirtualPath();
        List<UserBE> Users = null;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void gvOrders_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<string> PopulateUsers(string term)
        {
            var Users = UserBL.GetUserDetails();
            //Users = Users.Where(u => u.EmailId.ToLower().Contains(term.ToLower()) && u.RoleId == 2).ToList();
            Users = Users.Where(u => u.EmailId.ToLower().Contains(term.ToLower())).ToList();
            List<string> EmailIds = Users.Select(u => u.EmailId).ToList();
            return EmailIds;


        }
        protected void SearchOrders()
        {
            string EmailId = txtCustomerSearch.Text;
            List<OrderTrackingManagmentBE> Orders = new List<OrderTrackingManagmentBE>();
            Orders = OrderTrackingBL.GetOrdersByEmailId(EmailId);

            if (Orders != null)
            {
                if (Orders.Count > 0)
                {
                    CustomerOrderBE CustomerOrder = new CustomerOrderBE();

                    CustomerOrder.LanguageId = GlobalFunctions.GetLanguageId();
                    CustomerOrder.OrderedBy = Orders.Select(o => o.OrderedBy).FirstOrDefault();
                    Session["Orders"] = CustomerOrder;

                    gvOrders.DataSource = Orders;
                    gvOrders.DataBind();

                }
            }


        }

        protected void btn_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "orders":
                    CustomerOrderBE CustomerOrder = new CustomerOrderBE();

                    if (Session["Orders"] != null)
                    {
                        CustomerOrder = (CustomerOrderBE)Session["Orders"];
                        CustomerOrder.CustomerOrderId = Convert.ToInt32(e.CommandArgument);
                        Session["Orders"] = CustomerOrder;
                    }


                    Response.Redirect(host + "Admin/OrderTrackingManagement/OrderTrackingManagement.aspx"); break;
                case "search": SearchOrders(); break;
            }
        }


    }
}
