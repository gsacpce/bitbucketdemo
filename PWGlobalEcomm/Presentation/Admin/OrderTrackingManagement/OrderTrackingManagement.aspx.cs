﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Data;
namespace Presentation
{
    public class Admin_OrderTrackingManagement_OrderTrackingManagement : BasePage//System.Web.UI.Page
    {

        protected global::System.Web.UI.WebControls.HiddenField hdnOrderId;
        protected global::System.Web.UI.WebControls.GridView gvOrders;
      
      


        bool IsOrderTrackingDetailsExists = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateCustomerOrders();
            }

        }


        public void PopulateCustomerOrders()
        {

            CustomerOrderBE CustomerOrder = new CustomerOrderBE();
            if (Session["Orders"] != null)
            {
                CustomerOrder = (CustomerOrderBE)Session["Orders"];
                hdnOrderId.Value = Convert.ToString(CustomerOrder.CustomerOrderId);

                List<OrderTrackingManagmentBE> Orders = new List<OrderTrackingManagmentBE>();
                CustomerOrder.LanguageId = GlobalFunctions.GetLanguageId();
                Orders = OrderTrackingBL.GetOrderTrackingList(CustomerOrder.OrderedBy, CustomerOrder.LanguageId, CustomerOrder.CustomerOrderId);

                if (Orders != null)
                {
                    if (Orders.Count > 0)
                    {
                        IsOrderTrackingDetailsExists = true;
                        gvOrders.DataSource = Orders;
                        gvOrders.DataBind();
                    }
                }
                else
                {
                    CustomerOrder = ShoppingCartBL.CustomerOrder_SAE(CustomerOrder);
                    var CustomerOrders = CustomerOrder.CustomerOrderProducts;
                    if (CustomerOrders != null)
                    {
                        if (CustomerOrders.Count > 0)
                        {
                            IsOrderTrackingDetailsExists = false;
                            gvOrders.DataSource = CustomerOrders;
                            gvOrders.DataBind();
                        }

                    }
                }

            }
            else
            {
                Response.Redirect(GlobalFunctions.GetVirtualPath() + "Admin/OrderTrackingManagement/OrderTracking.aspx");
            }




        }

        protected void gvOrders_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int NewIndex = e.NewPageIndex;
            gvOrders.PageIndex = NewIndex;
            PopulateCustomerOrders();

        }
        protected void btnUpdate_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "update": UpdateOrders(); break;
                case "cancel": Response.Redirect(GlobalFunctions.GetVirtualPath() + "Admin/OrderTrackingManagement/OrderTracking.aspx"); break;
            }

        }
        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 24-09-15
        /// Scope   : to update order tracking data
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void UpdateOrders()
        {
            Int16 UserId = 0;
            if (Session["StoreUser"] == null)
                Response.Redirect(GlobalFunctions.GetVirtualPathAdmin() + "Login/Login.aspx", true);
            else
            {
                UserBE objUserBE = new UserBE();
                objUserBE = Session["StoreUser"] as UserBE;
                UserId = objUserBE.UserId;
            }
            try
            {
                List<OrderTrackingManagmentBE> Orders = new List<OrderTrackingManagmentBE>();
                foreach (GridViewRow gvPointRow in gvOrders.Rows)
                {
                    Literal ltlProductId = (Literal)gvPointRow.Cells[0].FindControl("ltlProductId");
                    TextBox txtDispatchId = (TextBox)gvPointRow.Cells[0].FindControl("txtDispatchId");
                    TextBox txtTrackingUrl = (TextBox)gvPointRow.Cells[0].FindControl("txtTrackingUrl");
                    TextBox txtConsignmentNo = (TextBox)gvPointRow.Cells[0].FindControl("txtConsignmentNo");
                    TextBox txtDeliveredQuantity = (TextBox)gvPointRow.Cells[0].FindControl("txtDeliveredQuantity");
                    TextBox txtShippingMethod = (TextBox)gvPointRow.Cells[0].FindControl("txtShippingMethod");


                    Orders.Add(new OrderTrackingManagmentBE()
                    {
                        OrderId = Convert.ToInt16(hdnOrderId.Value),
                        ProductSKUId = Convert.ToInt16(ltlProductId.Text),
                        DispatchId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtDispatchId.Text.Trim().Replace('\'', '@')),
                        TrackingUrl = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTrackingUrl.Text.Trim().Replace('\'', '@')),
                        ConsignmentNo = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtConsignmentNo.Text.Trim().Replace('\'', '@')),
                        DeliveredQty = Convert.ToInt16(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtDeliveredQuantity.Text.Trim().Replace('\'', '@'))),
                        ShippingMethod = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtShippingMethod.Text.Trim().Replace('\'', '@')
                        )

                    });
                    // Response.Write(" User Id " + hdnUserId.Value + " and point is " + txtPoints.Text);


                }
                DataTable dtOrders = GetDataIntoTable(Orders);
                bool IsOrdersUpdated = OrderTrackingBL.InsertOrderTrackingManagement(dtOrders);
                if (IsOrdersUpdated)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Order Tracking details saved succesfully", AlertType.Success);
                    CreateActivityLog("Order Tracking Management", "Update/Insert", Convert.ToString(hdnOrderId.Value));
                 }
                else
                {

                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while saving...", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected DataTable GetDataIntoTable(List<OrderTrackingManagmentBE> returnList)
        {

            DataTable dt = GlobalFunctions.To_DataTable(returnList);
            dt.Columns["OrderId"].ColumnName = "OrderId";
            dt.Columns["ProductSKUId"].ColumnName = "ProductSKUId";
            dt.Columns["DispatchId"].ColumnName = "DispatchId";
            dt.Columns["TrackingUrl"].ColumnName = "TrackingUrl";
            dt.Columns["ConsignmentNo"].ColumnName = "ConsignmentNo";
            dt.Columns["DeliveredQty"].ColumnName = "DeliveredQty";
            dt.Columns["ShippingMethod"].ColumnName = "ShippingMethod";
            // dt.Columns["CreatedBy"].ColumnName = "CreatedBy";
            dt.Columns.Remove("OrderedBy");
            dt.Columns.Remove("ProductName");
            dt.Columns.Remove("CatalogueId");
            dt.Columns.Remove("LanguageId");
            dt.Columns.Remove("ModifiedBy");
            dt.Columns.Remove("ModifiedDate");
            dt.Columns.Remove("CreatedBy");
            dt.Columns.Remove("CreatedDate");
            return dt;

        }


        protected void gvOrders_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (IsOrderTrackingDetailsExists)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Literal ltlProductId = (Literal)e.Row.FindControl("ltlProductId");
                    Literal ltlProductName = (Literal)e.Row.FindControl("ltlProductName");
                    TextBox txtDispatchId = (TextBox)e.Row.FindControl("txtDispatchId");
                    TextBox txtTrackingUrl = (TextBox)e.Row.FindControl("txtTrackingUrl");
                    TextBox txtConsignmentNo = (TextBox)e.Row.FindControl("txtConsignmentNo");
                    TextBox txtDeliveredQuantity = (TextBox)e.Row.FindControl("txtDeliveredQuantity");
                    TextBox txtShippingMethod = (TextBox)e.Row.FindControl("txtShippingMethod");


                    ltlProductId.Text = DataBinder.Eval(e.Row.DataItem, "ProductSKUId").ToString();
                    ltlProductName.Text = DataBinder.Eval(e.Row.DataItem, "ProductName").ToString();
                    txtDispatchId.Text = DataBinder.Eval(e.Row.DataItem, "DispatchId").ToString();
                    txtTrackingUrl.Text = DataBinder.Eval(e.Row.DataItem, "TrackingUrl").ToString();
                    txtConsignmentNo.Text = DataBinder.Eval(e.Row.DataItem, "ConsignmentNo").ToString();
                    txtDeliveredQuantity.Text = DataBinder.Eval(e.Row.DataItem, "DeliveredQty").ToString();
                    txtShippingMethod.Text = DataBinder.Eval(e.Row.DataItem, "ShippingMethod").ToString();
                }
            }
            else
            {
                return;
            }

        }
    }
}
