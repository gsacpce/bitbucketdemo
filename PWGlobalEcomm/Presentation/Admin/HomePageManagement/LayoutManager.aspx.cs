﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using HtmlAgilityPack;
using System.IO;
using PWGlobalEcomm.BusinessEntity;
using System.Text;
using Microsoft.Security.Application;

namespace Presentation
{ 
    public class Admin_HomePageManagement_LayoutManager : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Button btnSaveLayout;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myContainer;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myCanvas;
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox chkContainer;

        public string Host = GlobalFunctions.GetVirtualPath();

        
        string strPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            /*Sachin Chauhan : 28 08 2015 : Save Home Page Physical Path in Session*/
            strPath = Server.MapPath("~/Home");
            Session["homeFilePath"] = strPath;
            /*Sachin Chauhan : 28 08 2015*/

            //var webGet = new HtmlWeb();
            //string homepageUrl = Host + "/Home/Home.aspx";
            //var htmlDocument = webGet.Load(homepageUrl);
            //homepageUrl = "";
            //HtmlNode divHome = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Id == "divHomeContainer");

            /*Sachin Chauhan */

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Home/home.aspx")));
            //HtmlNode divHome = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='divHomeContainer']");
            HtmlNode divHome = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='divHomeContainer']");

            //if (divHome.InnerHtml.Contains("container"))
            //{
            //    chkContainer.Checked = true;
            //}
            
            try
            {
                HtmlNode firstSliderImgNode = divHome.Descendants().First(x => x.Attributes.Contains("src"));

                if (firstSliderImgNode != null)
                    firstSliderImgNode.Attributes["src"].Value = firstSliderImgNode.Attributes["src"].Value.Replace("..images/UI", "../../images/UI");

                HtmlNode lastImgNode = divHome.Descendants().Last(x => x.Attributes.Contains("src"));
                if (lastImgNode != null)
                    lastImgNode.Attributes["src"].Value = lastImgNode.Attributes["src"].Value.Replace("..images/UI", "../../images/UI");

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

           
            myCanvas.InnerHtml = divHome.InnerHtml;


        }
       
        
        [WebMethod]
        public static string SavePageLayout(string PageLayout)
        {

            //PageLayout = Sanitizer.GetSafeHtmlFragment(PageLayout);

            string message;
        
            try
            {
                message = "The pagelayout got saved successfully";
                
                string strPath = HttpContext.Current.Session["homeFilePath"].ToString();

                HtmlDocument htmlDocument = new HtmlDocument();

                /*Sachin Chauhan : 28 08 2015 : Start HTML Node Cleanup Process added by GridManager*/
                #region Html Elements Cleanup Added by Grid Manager
                PageLayout = PageLayout.Replace("<!--gm-editable-region-->", "");
                PageLayout = PageLayout.Replace("<!--/gm-editable-region-->", "");


                htmlDocument.LoadHtml(PageLayout);

               
                var gmEditableNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                       && x.Attributes["class"].Value.Contains("gm-editable-region"));

                foreach (var gmEditableNode in gmEditableNodes.ToList())
                {
                    gmEditableNode.ParentNode.RemoveChild(gmEditableNode, true);
                }



                

                var gmControls = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gm-controls"));
                foreach (var gmControl in gmControls.ToList())
                {
                    gmControl.ParentNode.RemoveChild(gmControl);
                }

                HtmlNode gmCanvas = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gm-canvas"));

                //var divContainerNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                //                                                                    && x.Attributes["class"].Value.Contains("container")
                //                                                                    && x.Name.Equals("div")
                //                                                                    );
                //foreach (HtmlNode divContainerNode in divContainerNodes.ToList() )
                //{
                //    if (divContainerNode.ChildNodes.Count == 1 && divContainerNode.ChildNodes[0].NodeType.Equals(HtmlNodeType.Text)  )
                //        divContainerNode.ParentNode.RemoveChild(divContainerNode);
                //}

                #endregion
                
                /*Sachin Chauhan : 28 08 2015*/


                /*Sachin Chauhan : 28 08 2015 : Add ItemType & Other Attributes & Default Value SELECT For Newly Added Home Page Elements */
                #region Set Default Attributes For Newly Added Elements
                
                var existingGmContents = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("id")
                                                                       && x.Attributes["id"].Value.Contains("gmcontent"));

                HtmlNode[] maxGmContent = existingGmContents.OrderByDescending(x => x.Attributes["id"].Value).ToArray();
                /*Sachin Chauhan : 10 09 2015 : Modified below logic to identify Max GMContent ID*/
                Int16 maxGmcontentID = 1;

                var itemTypeNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("itemtype"));

                foreach (HtmlNode itemType in itemTypeNodes)
                {
                    if ( itemType.Attributes["id"] == null )
                    {
                        itemType.InnerHtml = "";
                        itemType.Attributes.Add("id", "gmcontent" + maxGmcontentID);
                        itemType.Attributes.Add("runat", "server");
                        itemType.Attributes.Add("data-value", "0");
                        itemType.Attributes.Add("class","gmcontent");
                        maxGmcontentID += 1;
                    }
                    if ( !itemType.Attributes["class"].Value.Contains("gmcontent"))
                    {
                        itemType.Attributes["class"].Value += " gmcontent";
                    }
                }


                /*Sachin Chauhan Start : 11 02 2016 : Changed logic for assigning GMcontentIds to newly added elements*/
                if (maxGmContent.Length > 0)
                {
                    List<Int16> lstMaxGmContentId = new List<Int16>();

                    foreach (HtmlNode node in maxGmContent)
                    {
                        Int16 contentId = node.Attributes["id"].Value.Replace("gmcontent", "").To_Int16();
                        lstMaxGmContentId.Add(contentId);
                    }
                    maxGmcontentID = lstMaxGmContentId.Max();
                    maxGmcontentID += 1;
                }

                var colNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                       && x.Attributes["class"].Value.Contains("col-"));
                /*Only for newly added row & column divs setting GMContent Id*/
                foreach (HtmlNode  col in colNodes)
                {
                    if (col.Attributes["itemtype"] == null)
                    {
                        if ( !col.InnerHtml.Contains("row") && !col.InnerHtml.Contains("gmcontent") )
                        {
                            col.InnerHtml = "<div id=\"gmcontent"+ maxGmcontentID +"\" data-value=\"0\" runat=\"server\" itemtype=\"\" class=\"gmcontent\" >" + col.InnerHtml + "</div>" ;
                            maxGmcontentID += 1;
                        }
                            
                    }

                }
                /*Sachin Chauhan End : 11 02 2016*/
                #endregion

                
              


                /*Sachin Chauhan : 28 08 2015 : Save Page Layout With Cleaned Up HTML Markup & Original DIV Wrappers Arround It*/
                #region Save Cleaned Up HTML Markup for Home_Modified.Aspx
                
                htmlDocument.KeepChildrenOrder() ;

                var rowNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                       && x.Attributes["class"].Value.Contains("row")
                                                                       && !(x.ParentNode.Attributes["class"].Value.Contains("column"))
                                                                       );

                foreach (HtmlNode row in rowNodes.ToList())
                {
                    HtmlNode divContentNode = row.ChildNodes.FirstOrDefault( x => x.Attributes.Contains("class") 
                                                                            && !x.Attributes["class"].Value.Contains("container") ) as HtmlNode;
                  
                    if (divContentNode != null )
                    {
                        if (!divContentNode.Attributes["class"].Value.Contains("container"))
                        {
                            HtmlNode newDivContainerNode = HtmlNode.CreateNode("<div class=\"containerfullwidth homepageRowPadding\"></div>");
                            foreach (HtmlNode childNode in row.ChildNodes.ToList())
                            {
                                newDivContainerNode.AppendChild(childNode);
                                row.RemoveChild(childNode);
                            }
                            row.AppendChild(newDivContainerNode);

                        }
                    }
                    
                }

                StringBuilder sbPageLayout = new StringBuilder();

                foreach (HtmlNode newRowNode in rowNodes)
                {
                    sbPageLayout.Append(newRowNode.OuterHtml.Replace("<div class=\"gm-content\"></div>",""));
                }

                PageLayout = "<div id=\"divHomeContainer\" ClientIDMode=\"Static\" runat=\"server\">" ;
                PageLayout += sbPageLayout.ToString() +
                               " </div> ";
                 

                htmlDocument.LoadHtml(PageLayout);


                FileInfo objFS = new FileInfo(strPath + "\\Home_Modified.aspx");
                if (objFS.IsReadOnly == true)
                {
                    objFS.IsReadOnly = false;
                }
                objFS = null;

                File.WriteAllText(strPath + "\\Home_Modified.aspx", PageLayout);
                #endregion
                /*Sachin Chauhan : 28 08 2015*/
            }
            catch (Exception ex)
            {                
                message = "There is an error saving pagelayout";
                Exceptions.WriteExceptionLog(ex);
            }

            
            

            return message;
        }

        
    }

    public static class HtmlAgilityFix
    {
        public static void KeepChildrenOrder(this HtmlDocument html)
        {
            var divMainTag = html.DocumentNode.SelectNodes("//div[@id='divMainContent']");
            
            if (divMainTag != null)
            {
                foreach (var div in divMainTag)
                {
                    if (!div.HasChildNodes)
                    {
                        div.ParentNode.RemoveChild(div);

                        continue;
                    }

                    int count = div.ChildNodes.Count;

                    for (var i = count - 1; i >= 0; i--)
                    {
                        var child = div.ChildNodes[i];

                        div.ParentNode.InsertAfter(child, div);
                    }

                    div.ParentNode.RemoveChild(div);
                }
            }
        }
    }

   

}