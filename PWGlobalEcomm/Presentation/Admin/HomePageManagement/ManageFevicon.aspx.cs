﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_HomePageManagement_ManageFevicon : System.Web.UI.Page
{
    protected global::System.Web.UI.WebControls.FileUpload fluFevicon;
    protected global::System.Web.UI.WebControls.Image imgFevicon;

    public string FilePath = GlobalFunctions.GetVirtualPath();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        LoadFevicon();
    }


    protected void Write()
    {
        string filepath = string.Empty;
        // First We create the backup then delete existing file and Re-create it. 
        try
        {
            string extension = Path.GetExtension(fluFevicon.PostedFile.FileName);
            if (extension == ".ico")
            {
                filepath = GlobalFunctions.GetVirtualPath() + "//Favicon.ico";
                File.Delete(Server.MapPath("~/") + "Favicon.png");
            }
            else
            {
                filepath = GlobalFunctions.GetVirtualPath() + "//Favicon.png";
                File.Delete(Server.MapPath("~/") + "Favicon.ico");
            }
            if (extension != null && extension == ".ico" || extension==".png")
            {
                if (filepath != null)
                {
                    File.Delete(Server.MapPath("~/") + "Favicon" + extension);
                }
                if (FilePath != string.Empty)
                {
                    //if (!Directory.Exists(GlobalFunctions.GetPhysicalFolderPath()))
                    //    Directory.CreateDirectory(Server.MapPath("~/Images/Favicon"));
                   // else
                    //{
                        //fluFevicon.SaveAs(Server.MapPath("~/Images/Favicon/") + "Favicon" + extension);
                        fluFevicon.SaveAs(Server.MapPath("~/")+"Favicon" + extension);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated successfully.", AlertType.Success);
                        LoadFevicon();
                    //}
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select the File.", AlertType.Warning);
                }
            }
            else { GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Upload .ico or .png type image", AlertType.Warning); }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void btnupload_Click(object sender, EventArgs e)
    {
        try
        {
            Write();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }




    protected void LoadFevicon()
    {
        try
        {
            string filepath = string.Empty;
            //DirectoryInfo d = new DirectoryInfo(GlobalFunctions.GetVirtualPath());
            //FileInfo[] infos = d.GetFiles("Favicon");
            filepath = GlobalFunctions.GetVirtualPath() + "//Favicon.ico";
            //foreach (FileInfo f in infos)
            if (filepath!=null)
            {
                if (File.Exists(Server.MapPath("~/") + "Favicon.ico"))
                {
                    imgFevicon.ImageUrl = GlobalFunctions.GetVirtualPath() + "//Favicon.ico";
                }
                else
                {
                    imgFevicon.ImageUrl = GlobalFunctions.GetVirtualPath() + "//Favicon.png";
                }
                //if (f.Name.ToString().Remove(f.Name.ToString().LastIndexOf(".")).Replace(".", "") == "Favicon")
                //{
                //        //imgFevicon.ImageUrl = Server.MapPath("~/Images/Favicon/") + f.Name;
                //        imgFevicon.ImageUrl = "" + f.Name;
                //}
            }
            if (FilePath != "")
            {

            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "File not Found.", AlertType.Warning);
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
}