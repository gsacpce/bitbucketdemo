﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using HtmlAgilityPack;
using System.IO;
using System.Text;
using PWGlobalEcomm.BusinessLogic;
using System.Data;
using System.Text.RegularExpressions;
using Microsoft.Security.Application;


namespace Presentation
{
    public class Admin_HomePageManagement_LayoutConfigure : System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Button btnSaveLayout;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl myCanvas;
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox chkContainer; 

        public string Host = GlobalFunctions.GetVirtualPath();
        public string HostAdmin = GlobalFunctions.GetVirtualPathAdmin();

        string strPath = "";
        #endregion

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : Page_Load event
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                strPath = Server.MapPath("~/Home");
                Session["homeFilePath"] = strPath;
                Session["host"] = Host;

                var webGet = new HtmlWeb();
                string homepageUrl = Host + "/Home/Home_Modified.aspx";
                var htmlDocument = webGet.Load(homepageUrl);
                homepageUrl = "";
                HtmlNode divHome = htmlDocument.DocumentNode.Descendants().SingleOrDefault(x => x.Id == "divHomeContainer");

                //if (divHome.InnerHtml.Contains("id=\"dvMainContent\" class=\"container\""))
                //{
                //    chkContainer.Checked = true;
                //}

              
                var newContentTypes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("itemtype") );
                   
                /*Sachin Chauhan : 11 09 2015 : Find ROW elements and add checkbox for setting row attribute as full page width or container width*/
                var rowNodes = htmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class") &&
                                                                             x.Attributes["class"].Value.Contains("container") 
                                                                            //&& !(x.ParentNode.Attributes["class"].Value.Contains("column"))
                                                                            );

                



                foreach (HtmlNode rowNode in rowNodes.ToList())
                {
                    HtmlNode chkRowContainer = HtmlNode.CreateNode("<br />");
                    

                    if (rowNode.OuterHtml.Contains("containerfullwidth") )
                    {
                        chkRowContainer = HtmlNode.CreateNode("<input type=\"checkbox\" class=\"chkcontainer\" onclick=\"javasctipt:funContainerMode(this);\" title=\"Container width\">");
                        var hdnRowContainer = HtmlNode.CreateNode("<input type=\"hidden\" class=\"containermode\" value=\"containerfullwidth\"  >");
                        var lblRowContainer = HtmlNode.CreateNode("<label>&nbsp;&nbsp;Container width</label>");
                        rowNode.ParentNode.AppendChild(chkRowContainer);
                        rowNode.ParentNode.AppendChild(lblRowContainer);
                        rowNode.ParentNode.AppendChild(hdnRowContainer);

                    }
                    else
                    {
                        chkRowContainer = HtmlNode.CreateNode("<input type=\"checkbox\" checked=\"checked\" class=\"chkcontainer\" onclick=\"javasctipt:funContainerMode(this);\" title=\"Container width\">");
                        var hdnRowContainer = HtmlNode.CreateNode("<input type=\"hidden\" class=\"containermode\" value=\"container\"  >");
                        var lblRowContainer = HtmlNode.CreateNode("<label>&nbsp;&nbsp;Container width</label>");
                        rowNode.ParentNode.AppendChild(chkRowContainer);
                        rowNode.ParentNode.AppendChild(lblRowContainer);
                        rowNode.ParentNode.AppendChild(hdnRowContainer);

                    }
                        

                    

                }

                /*Sachin Chauhan : 11 09 2015*/
   
                HtmlNode[] itemTypeDivElements = newContentTypes.ToArray<HtmlNode>();


                List<object> HomePageElement = new List<object>();
                HomePageElement = HomePageManagementBL.GetAllHomePageElements(false);

                if (HomePageElement != null)
                Exceptions.WriteInfoLog(" HP Elem Cnt = =" + HomePageElement.Count);
                
                List<HomePageManagementBE> lstHomePageElements = new List<HomePageManagementBE>();
                if (HomePageElement != null && HomePageElement.Count > 0)
                    lstHomePageElements = HomePageElement.Cast<HomePageManagementBE>().ToList();

                Int16 counter = Convert.ToInt16( itemTypeDivElements.Length );
                

                for (Int16 i = 0; i < counter ; i++)
                {
                    string htmlContentId = "" ;
                    string gmContentId = "";
                    if (itemTypeDivElements[i].Attributes["id"] == null)
                    {
                        htmlContentId = (i + 1).ToString();
                        itemTypeDivElements[i].Attributes.Add("id", "gmcontent" + htmlContentId);
                        itemTypeDivElements[i].Attributes.Add("runat", "server");
                    }
                    else
                    {
                        gmContentId = itemTypeDivElements[i].Attributes["id"].Value;
                        htmlContentId = itemTypeDivElements[i].Attributes["id"].Value.Replace("gmcontent", "");
                        itemTypeDivElements[i].Attributes.Add("runat", "server");
                    }
                        

                    StringBuilder sbDropDown = new StringBuilder();
                    sbDropDown.Append("<select width=\"170px\" class=\"ddlContent\" name=\"ddlContent" + htmlContentId + "\" id=\"ddlContent" + htmlContentId + "\" onchange=\"javascript:funSaveDDLValue('ddlContent" + htmlContentId + "','hdnContent" + htmlContentId + "');\">");

                    
                    string strItemType = string.Empty;
                    string strItemValue = string.Empty;
                    if (lstHomePageElements.Count > 0  )
                    {
                        HomePageManagementBE currElement = lstHomePageElements.FirstOrDefault(x => x.ConfigSectionName.ToLower().Equals("gmcontent" + htmlContentId));
                        if (currElement != null)
                        {
                            if (currElement.ConfigElementName != "")
                            {
                                strItemType = currElement.ConfigElementName;
                                strItemValue = currElement.ConfigElementValue;

                                if ( i < lstHomePageElements.Count )
                                {
                                    itemTypeDivElements[i].Attributes.Add("data-value", Convert.ToString(currElement.ConfigElementID));
                                    itemTypeDivElements[i].Attributes.Add("itemtype", strItemType);
                                }

                                sbDropDown.Append("<option value='0'>-- Select --</option>");

                                if (currElement.ConfigElementName.ToLower() == "slideshow")
                                    sbDropDown.Append("<option value='SlideShow' selected=\"selected\">Slide Show</option>");
                                else
                                    sbDropDown.Append("<option value='SlideShow'>Slide Show</option>");
                                if (currElement.ConfigElementName.ToLower() == "staticimage")
                                    sbDropDown.Append("<option value='StaticImage' selected=\"selected\">Static Image</option>");
                                else
                                    sbDropDown.Append("<option value='StaticImage'>Static Image</option>");
                                if (currElement.ConfigElementName.ToLower() == "statictext")
                                    sbDropDown.Append("<option value='StaticText' selected=\"selected\">Static Text</option>");
                                else
                                    sbDropDown.Append("<option value='StaticText'>Static Text</option>");
                                if (currElement.ConfigElementName.ToLower() == "video")
                                    sbDropDown.Append("<option value='Video' selected=\"selected\">Video</option>");
                                else
                                    sbDropDown.Append("<option value='Video'>Video</option>");

                                if (currElement.ConfigElementName.ToLower() == "sections")
                                    sbDropDown.Append("<option value='Sections' selected=\"selected\">Sections</option>");
                                else
                                    sbDropDown.Append("<option value='Sections'>Sections</option>");

                               
                            }
                        }
                        else 
                        {
                            itemTypeDivElements[i].Attributes.Add("data-value", "0");
                            //tElements[i].Attributes.Add("itemtype", strItemType);
                            sbDropDown.Append("<option value='0' selected=\"selected\">-- Select --</option>");
                            sbDropDown.Append("<option value='SlideShow'>Slide Show</option>");
                            sbDropDown.Append("<option value='StaticText'>Static Text</option>");
                            sbDropDown.Append("<option value='StaticImage'>Static Image</option>");
                            sbDropDown.Append("<option value='Video'>Video</option>");
                            sbDropDown.Append("<option value='Sections'>Sections</option>");
                            
                        }
                        
                    }
                    else
                    {
                        string currItem = itemTypeDivElements[i].Attributes["itemType"].Value.ToLower() ;
                        string dataValue = itemTypeDivElements[i].Attributes["data-value"].Value.ToLower();

                        if (dataValue.Equals("0"))
                        {
                            sbDropDown.Append("<option value='0' selected=\"selected\">-- Select --</option>");
                            sbDropDown.Append("<option value='SlideShow'>Slide Show</option>");
                            sbDropDown.Append("<option value='StaticText'>Static Text</option>");
                            sbDropDown.Append("<option value='StaticImage'>Static Image</option>");
                            sbDropDown.Append("<option value='Video'>Video</option>");
                            sbDropDown.Append("<option value='Sections'>Sections</option>");
                        }
                        else
                        {
                            if (currItem.Equals("slideshow") )
                                sbDropDown.Append("<option value='SlideShow' selected=\"selected\">Slide Show</option>");
                            else
                                sbDropDown.Append("<option value='SlideShow'>Slide Show</option>");

                            if (currItem.Equals("staticimage") )
                                sbDropDown.Append("<option value='StaticImage' selected=\"selected\">Static Image</option>");
                            else
                                sbDropDown.Append("<option value='StaticImage'>Static Image</option>");

                            if (currItem.Equals("statictext") )
                                sbDropDown.Append("<option value='StaticText' selected=\"selected\">Static Text</option>");
                            else
                                sbDropDown.Append("<option value='StaticText'>Static Text</option>");

                            if (currItem.Equals("video") )
                                sbDropDown.Append("<option value='Video' selected=\"selected\">Video</option>");
                            else
                                sbDropDown.Append("<option value='Video'>Video</option>");

                            if (currItem.Equals("sections") )
                                sbDropDown.Append("<option value='Sections' selected=\"selected\">Sections</option>");
                            else
                                sbDropDown.Append("<option value='Sections'>Sections</option>");
                        }

                        

                        
                    }
                    sbDropDown.Append("</select>");
                    sbDropDown.Append("<br><span style='color:#702828'>" + strItemValue + "</span>");
                    sbDropDown.Append("<input type=\"hidden\" id=\"hdnContent" + htmlContentId + "\" value=\"" + itemTypeDivElements[i].Attributes["itemtype"].Value + "\" >");
                    itemTypeDivElements[i].InnerHtml = sbDropDown.ToString();
                }
                myCanvas.InnerHtml = divHome.InnerHtml;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : SavePageLayout method 
        /// </summary>  
        /// param name="PageLayout"
        [WebMethod]
        public static string SavePageLayout(string PageLayout, string jsondata, bool isPreview )
        {
            //string[] message = new string[1];
            //PageLayout = Sanitizer.GetSafeHtmlFragment(PageLayout);
            string message = string.Empty;

            try
            {
                message = "The pagelayout got saved successfully";

                string strPath = HttpContext.Current.Session["homeFilePath"].ToString();

                //File.CreateText(strPath+"\\Home_Modified.aspx") ;
                //FileStream fs = File.OpenWrite(strPath + "\\Home_Modified.aspx");

                /*Take backup of existing page layout*/
                /*Sachin Chauhan : 29 12 15*/
                //if (isPreview)
                //    File.Copy(strPath + "\\HomePreview.aspx", strPath + "\\Backups\\HomePreview.aspx." + DateTime.Now.ToString("dd-MM-yy hh-mm-ss"), true);
                //else
                    File.Copy(strPath + "\\Home.aspx", strPath + "\\Backups\\Home.aspx." + DateTime.Now.ToString("dd-MM-yy hh-mm-ss"), true);
                /*Sachin Chauhan 17 08 2015 
                 * Commented below line as 2 & 3rd step layout processing is merged
                 * 
                */
                ProcessPageConfiguration(PageLayout, jsondata, isPreview );
                //File.WriteAllText(strPath + "\\Home_Configuration.html", PageLayout);
            }
            catch (Exception ex)
            {
                message = "There is an error saving pagelayout";
                //throw;
            }




            // return new JavaScriptSerializer().Serialize(message);
            return message;
        }

        //private static DataTable jsonToDataTable(string jsonString)
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {

        //        string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
        //        List<string> ColumnsName = new List<string>();
        //        foreach (string jSA in jsonStringArray)
        //        {
        //            string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
        //            foreach (string ColumnsNameData in jsonStringData)
        //            {
        //                try
        //                {
        //                    int idx = ColumnsNameData.IndexOf(":");
        //                    string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
        //                    if (!ColumnsName.Contains(ColumnsNameString))
        //                    {
        //                        ColumnsName.Add(ColumnsNameString);
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
        //                }
        //            }
        //            break;
        //        }
        //        foreach (string AddColumnName in ColumnsName)
        //        {
        //            dt.Columns.Add(AddColumnName);
        //        }
        //        foreach (string jSA in jsonStringArray)
        //        {
        //            string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
        //            DataRow nr = dt.NewRow();
        //            foreach (string rowData in RowData)
        //            {
        //                try
        //                {
        //                    int idx = rowData.IndexOf(":");
        //                    string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
        //                    string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
        //                    nr[RowColumns] = RowDataString;
        //                }
        //                catch (Exception ex)
        //                {
        //                    continue;
        //                }
        //            }
        //            dt.Rows.Add(nr);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //    return dt;
        //}

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-08-15
        /// Scope   : SavePageLayout method 
        /// </summary>  
        /// param name="pageConfiguration"
        private static void ProcessPageConfiguration(string pageConfiguration, string jsondata, bool isPreview)
        {
            try
            {

            
            string strPath = HttpContext.Current.Session["homeFilePath"].ToString();
      
            HtmlDocument newHtmlDocument = new HtmlDocument();

            newHtmlDocument.LoadHtml(pageConfiguration);

            var hdnHtmlNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Id.Contains("hdnContent"));

            /*
            Delete existing layout  
            */
            HomePageManagementBE HomePageElement = new HomePageManagementBE(); ;
            HomePageManagementBL.HomePageConfiguration_SAED(HomePageElement, DBAction.Delete);
            /*
            Traverse each hidden element and update layout details in database table 
             */

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<HomePageManagementBE> lstHomePageMgmt = jss.Deserialize<List<HomePageManagementBE>>(jsondata);

            foreach (var item in lstHomePageMgmt)
            {
                HomePageElement = new HomePageManagementBE();
                HomePageElement.ConfigSectionName = item.ConfigSectionName;
                HomePageElement.ConfigElementName = item.ConfigElementName;
                HomePageElement.ConfigElementID = item.ConfigElementID;

                HomePageManagementBL.HomePageConfiguration_SAED(HomePageElement, DBAction.Insert);

            }

            /// <summary>
            /// Author  : Sachin Chauhan
            /// Date    : 18-08-15
            /// Scope   : Remove dropdown , hidden & gridview control nodes from final modified document
            var selectNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("select"));

            foreach (var selectNode in selectNodes.ToList())
            {
                 selectNode.ParentNode.RemoveChild(selectNode);
            }

            var hdnContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                                     && x.Attributes["class"].Value.Equals("containermode"));

            var divContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                                     && x.Attributes["class"].Value.Contains("container")
                                                                                     && x.Name.Equals("div") 
                                                                                     );


            var newDivContainerNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class")
                                                                                     && x.Attributes["class"].Value.Contains("container")
                                                                                     && x.Name.Equals("div")
                                                                                     );

            HtmlNode[] hdnContainerModes = hdnContainerNodes.ToArray();
            HtmlNode[] newDivContainerModes = newDivContainerNodes.ToArray();

            for (int i = 0; i < hdnContainerModes.Length; i++)
            {
                if (hdnContainerModes[i].Attributes["value"].Value.Equals("container"))
                {
                    newDivContainerModes[i].Attributes["class"].Value = "container homepageRowPadding";
                }
                else
                    newDivContainerModes[i].Attributes["class"].Value = "containerfullwidth homepageRowPadding";

            }


            var hiddenNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("input"));

            foreach (var hiddenNode in hiddenNodes.ToList())
            {
                hiddenNode.ParentNode.RemoveChild( hiddenNode);
            }

            var labelNodes = newHtmlDocument.DocumentNode.Descendants().Where(x => x.Name.Contains("label"));

            foreach (var labelNode in labelNodes.ToList())
            {
                labelNode.ParentNode.RemoveChild(labelNode);
            }

            

             newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace("contenteditable=\"false\"","");

            newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace("<p class=\"chkContainer\">","");
            newHtmlDocument.DocumentNode.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml.Replace("</p>", "");
            ///</Summary>

            newHtmlDocument.KeepChildrenOrder();

            HtmlDocument oldHomeDocument = new HtmlDocument();
            oldHomeDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Home/home.aspx")));
            HtmlNode divHome = oldHomeDocument.DocumentNode.SelectSingleNode("//div[@id='divHomeContainer']");

            //oldHomeDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Home/home.aspx")));
            //HtmlNode divMainContent = oldHomeDocument.DocumentNode.SelectSingleNode("//div[@id='divMainContent']");
         
            
            
            divHome.InnerHtml = newHtmlDocument.DocumentNode.InnerHtml;



            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/Home/home.aspx")));
            HtmlNode divHomeContainer = doc.DocumentNode.SelectSingleNode("//div[@id='divHomeContainer']");

            
            /*Sachin Chauhan Start : 08 02 2015 : Commented below line of code as it created an issue 
             *                                    when complete home page is recreated after deleteing existing elements. 
             */
            //doc.DocumentNode.InnerHtml = doc.DocumentNode.InnerHtml.Replace(divHomeContainer.InnerHtml, divHome.InnerHtml);

            if ( divHomeContainer.InnerHtml.Trim().Equals(String.Empty) )
                divHomeContainer.InnerHtml =  divHome.InnerHtml;
            else
                divHomeContainer.InnerHtml = divHomeContainer.InnerHtml.Replace(divHomeContainer.InnerHtml, divHome.InnerHtml);
            
            /*Sachin Chauhan End : 08 02 2015*/

            /*Sachin Chauhan : 29 12 15*/
            /*if (isPreview)
            {
                FileInfo objFS = new FileInfo(strPath + "\\HomePreview.aspx");
                if (objFS.IsReadOnly == true)
                    objFS.IsReadOnly = false;
                objFS = null;
                doc.Save(HttpContext.Current.Server.MapPath("~/Home/HomePreview.aspx"));
                string strHomePreview = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Home/HomePreview.aspx"));
                strHomePreview = strHomePreview.Replace("MasterPage.master", "MasterPagePreview.master");
                File.WriteAllText(strPath + "\\HomePreview.aspx", strHomePreview);
            }
            else
            {*/
                FileInfo objFS = new FileInfo(strPath + "\\Home.aspx");
                if (objFS.IsReadOnly == true)
                    objFS.IsReadOnly = false;
                objFS = null;
                doc.Save(HttpContext.Current.Server.MapPath("~/Home/Home.aspx"));
            //}

            

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            //File.WriteAllText(strPath + "\\Home.aspx", doc.DocumentNode.InnerHtml);
           

        }
    }

}