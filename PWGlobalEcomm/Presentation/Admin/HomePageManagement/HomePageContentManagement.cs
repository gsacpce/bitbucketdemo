﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using CKEditor.NET;
using Microsoft.Security.Application;


namespace Presentation
{
    public class Admin_HomePageContent : BasePage //System.Web.UI.Page
    {
        public List<HomePageManagementBE> lstHomePageContents { get; set; }

        protected global::System.Web.UI.WebControls.TextBox txtTitle, txtAltName, txtNavigateUrl, txtVideoLink, txtCopyRight, txtWidth, txtHeight, txtVideoWidth,
            txtVideoHeight, txtImageWidth, txtImageHeight, txtStaticContents, txtFooters;
        //protected global::CKEditor.NET.CKEditorControl txtStaticContent, txtFooter;


        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnStaticImageSrc;
        protected global::System.Web.UI.HtmlControls.HtmlContainerControl divStaticImage, divVideo, divStaticText, divLang, divSave, divSiteLogo, divFooterText, divCopyRightInfo, divUploadDocument;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liAlt, ulTitle, txtWidthError, txtHeightError, txtVideoWidthError, txtImageHeightError, txtImageWidthError, txtVideoHeightError, mobileimage;


        protected global::System.Web.UI.WebControls.Button btnSave;
        protected global::System.Web.UI.WebControls.Button btnCancel;
        protected global::System.Web.UI.WebControls.DropDownList ddlContentType, ddlLanguage, ddlWidthUnit, ddlHeightUnit, ddlVideoWidthUnit, ddlVideoHeightUnit, ddlImageWidthUnit, ddlImageHeightUnit;
        protected global::System.Web.UI.WebControls.FileUpload pdfFileUploader;

        public string host = GlobalFunctions.GetVirtualPath();

        public string ImageSize = GlobalFunctions.GetSetting("CategoryImageSize");
        public string ImageExtension = GlobalFunctions.GetSetting("CategoryFormatExtension");
        public string ImageHtml { get; set; }
        public string SiteLogoImageHtml { get; set; }
        public string mobileLogoImageHtml { get; set; }
        public string TempStaticImageId = "";

        public string Logo = "";
        public string MobileLogo = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            TempStaticImageId = Guid.NewGuid().ToString();
            if (!IsPostBack)
            {

                ddlContentType.SelectedValue = "1";

                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = true;
                ulTitle.Visible = true;
                liAlt.Visible = true;
                divSave.Visible = true;
                divSiteLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;


                PopulateLanguageDropDownList();
                BindData();
                if (Request.QueryString["id"] != null)
                {

                    editlogo();


                }
                Session["ContentID"] = null;
                Session["TttleStaticImage"] = null;
                //txtStaticContent.config.toolbar = new object[]
                //{
                //    new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
                //    new object[] { "NumberedList", "BulletedList"},
                //    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
                //    "/",
                //    new object[] { "Styles", "Format", "Font", "FontSize" },
                //    new object[] { "TextColor", "BGColor" },
                //    "/",
                //                    new object[] { "Link", "Unlink"},
                //    new object[] { "Table", "HorizontalRule"}
                //};

                #region Commented by Sripal Replace the .net Txtbox Control
                //txtStaticContent.config.toolbar = new object[]
                //{
                //    new object[] { "Source", "Bold", "Italic", "Underline", "Strike"},				  
                //    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
                //    "/",
                //    new object[] { "Font", "FontSize" },
                //    new object[] { "TextColor", "BGColor" },
                //    "/",
                //                    new object[] { "Link", "Unlink"},
                //    new object[] { "HorizontalRule"}
                //};
                //txtFooter.config.toolbar = new object[]
                //{
                //   new object[] { "Source", "Bold", "Italic", "Underline", "Strike"},				  
                //    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
                //    "/",
                //    new object[] { "Font", "FontSize" },
                //    new object[] { "TextColor", "BGColor" },
                //    "/",
                //                    new object[] { "Link", "Unlink"},
                //    new object[] { "HorizontalRule"}
                //};

                #endregion

                //txtFooter.config.toolbar = new object[]
                //{
                //    new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
                //    new object[] { "NumberedList", "BulletedList"},
                //    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
                //    "/",
                //    new object[] { "Styles", "Format", "Font", "FontSize" },
                //    new object[] { "TextColor", "BGColor" },
                //    "/",
                //                    new object[] { "Link", "Unlink"},
                //    new object[] { "Table", "HorizontalRule"}
                //};


            }
            else
            {
                // assigning image src from hidden variable
                if (hdnStaticImageSrc.Value != "")
                {
                    ImageHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + hdnStaticImageSrc.Value + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                    // hdnStaticImageSrc.Value = hdnStaticImageSrc.Value + "?1=1376400078917";

                }
            }

        }

        private void editlogo()
        {
            Logo = "SiteLogo";
            MobileLogo = "MobileLogo";
            mobileimage.Visible = true;
            divStaticText.Visible = false;
            divVideo.Visible = false;
            divStaticImage.Visible = false;
            ulTitle.Visible = false;
            liAlt.Visible = false;
            divSave.Visible = false;
            divSiteLogo.Visible = true;
            divFooterText.Visible = false;
            divCopyRightInfo.Visible = false;
            divUploadDocument.Visible = false;
            ddlContentType.SelectedValue = "4";

            /*Desktop Logo*/
            string FilePath = Server.MapPath("~/Images/SiteLogo") + "\\";
            string fileName = "";
            string _fileName = "";
            DirectoryInfo di = new DirectoryInfo(FilePath);
            FileInfo[] files = di.GetFiles("SiteLogo" + "*.*");
            foreach (FileInfo file in files)
                try
                {
                    file.Attributes = FileAttributes.Normal;
                    fileName = file.Name + "?1=" + DateTime.Now.TimeOfDay;
                    _fileName = file.Name.Replace(Path.GetExtension(file.Name), "");

                }
                catch { }
            if (fileName != "")
                SiteLogoImageHtml = "<li class=\" qq-upload-success\"><img  style=\"background-color:gray\" class=\"uploadedimage\" src=\"" + host + "Images/SiteLogo/" + fileName + "\"><a id=SiteLogo class=\"cloaseuplod1\" onclick=\"RemoveFile(this.id)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";

            FilePath = host + "Images/SiteLogo/" + _fileName;
            string[] fileNameParts = FilePath.Split('_');
            if (fileNameParts.Length > 1)
            {
                txtWidth.Text = fileNameParts[1];
                ddlWidthUnit.SelectedValue = fileNameParts[2] == "px" ? "2" : "1";
                txtHeight.Text = fileNameParts[3];
                ddlHeightUnit.SelectedValue = fileNameParts[4] == "px" ? "2" : "1";
            }


            /*Mobile Logo*/

            string FilePathmobile = Server.MapPath("~/Images/SiteLogo/Mobile") + "\\";
            string fileNamemobile = "";
            string _fileNamemobile = "";
            DirectoryInfo dimobile = new DirectoryInfo(FilePathmobile);
            FileInfo[] filesmobile = dimobile.GetFiles("mobileLogo" + "*.*");
            foreach (FileInfo file in filesmobile)
                try
                {
                    file.Attributes = FileAttributes.Normal;
                    fileNamemobile = file.Name + "?1=" + DateTime.Now.TimeOfDay;
                    _fileNamemobile = file.Name.Replace(Path.GetExtension(file.Name), "");

                }
                catch { }
            if (fileNamemobile != "")
                mobileLogoImageHtml = "<li class=\" qq-upload-success\"><img  style=\"background-color:gray\" class=\"uploadedimage\" src=\"" + host + "Images/SiteLogo/Mobile/" + fileNamemobile + "\"><a id=mobileLogo class=\"cloaseuplod1\" onclick=\"RemoveFile(this.id)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
            else mobileLogoImageHtml = "";

            FilePathmobile = host + "Images/SiteLogo/Mobile/" + _fileNamemobile;
            string[] fileNamePart = FilePathmobile.Split('_');
            if (fileNamePart.Length > 1)
            {
                txtWidth.Text = fileNamePart[1];
                ddlWidthUnit.SelectedValue = fileNamePart[2] == "px" ? "2" : "1";
                txtHeight.Text = fileNamePart[3];
                ddlHeightUnit.SelectedValue = fileNamePart[4] == "px" ? "2" : "1";
            }

        }

        private void BindData()
        {
            if (Session["EditHomePageContent"] != null)
            {
                HomePageManagementBE EditHomePageContent = Session["EditHomePageContent"] as HomePageManagementBE;
                ddlContentType.Enabled = false;

                txtTitle.Text = EditHomePageContent.Title;
                divLang.Visible = true;


                if (EditHomePageContent.ContentType == "StaticImage")
                {
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = true;
                    ulTitle.Visible = true;
                    liAlt.Visible = true;
                    divSave.Visible = true;
                    divSiteLogo.Visible = false;
                    divFooterText.Visible = false;
                    divCopyRightInfo.Visible = false;
                    divUploadDocument.Visible = false;

                    ddlContentType.SelectedValue = "1";
                    txtTitle.Text = EditHomePageContent.Title.Trim();
                    txtAltName.Text = EditHomePageContent.Content.Trim();
                    txtNavigateUrl.Text = EditHomePageContent.URL.Trim();

                    //txtImageWidth.Text = Convert.ToString(EditHomePageContent.Width);
                    //txtImageHeight.Text = Convert.ToString(EditHomePageContent.Height);

                    //if (EditHomePageContent.WidthUnit == "%")
                    //{
                    //    ddlImageWidthUnit.SelectedValue = "1";
                    //}
                    //else
                    //{
                    //    ddlImageWidthUnit.SelectedValue = "2";
                    //}

                    //if (EditHomePageContent.HeightUnit == "%")
                    //{
                    //    ddlImageHeightUnit.SelectedValue = "1";
                    //}
                    //else
                    //{
                    //    ddlImageHeightUnit.SelectedValue = "2";
                    //}



                    //ddlImageWidthUnit.SelectedValue = Convert.ToString(EditHomePageContent.WidthUnit);
                    hdnStaticImageSrc.Value = host + "Images/StaticImage/" + EditHomePageContent.HomePageStaticContentId + EditHomePageContent.ImageExtension.ToString();

                    if (hdnStaticImageSrc.Value != "")
                    {
                        ImageHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + hdnStaticImageSrc.Value + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                        //  hdnStaticImageSrc.Value = hdnStaticImageSrc.Value + "?1=1376400078917";

                    }
                }
                else if (EditHomePageContent.ContentType == "Video")
                {

                    divStaticText.Visible = false;
                    divVideo.Visible = true;
                    divStaticImage.Visible = false;
                    ulTitle.Visible = true;
                    liAlt.Visible = true;
                    divSave.Visible = true;
                    divSiteLogo.Visible = false;
                    divFooterText.Visible = false;
                    divCopyRightInfo.Visible = false;
                    divUploadDocument.Visible = false;

                    //txtVideoWidth.Text = Convert.ToString(EditHomePageContent.Width);
                    txtVideoHeight.Text = Convert.ToString(EditHomePageContent.Height);

                    //if (EditHomePageContent.WidthUnit == "%")
                    //{
                    //    ddlVideoWidthUnit.SelectedValue = "1";
                    //}
                    //else
                    //{
                    //    ddlVideoWidthUnit.SelectedValue = "2";
                    //}

                    //if (EditHomePageContent.HeightUnit == "%")
                    //{
                    //    ddlVideoHeightUnit.SelectedValue = "1";
                    //}
                    //else
                    //{
                    //    ddlVideoHeightUnit.SelectedValue = "2";
                    //}

                    ddlContentType.SelectedValue = "2";
                    txtAltName.Text = EditHomePageContent.Content.Trim();
                    txtVideoLink.Text = EditHomePageContent.URL.Trim();

                }
                else if (EditHomePageContent.ContentType == "StaticText")
                {
                    divStaticText.Visible = true;
                    divVideo.Visible = false;
                    divStaticImage.Visible = false;
                    ulTitle.Visible = true;
                    liAlt.Visible = false;
                    divSave.Visible = true;
                    divSiteLogo.Visible = false;
                    divFooterText.Visible = false;
                    divCopyRightInfo.Visible = false;
                    divUploadDocument.Visible = false;

                    ddlContentType.SelectedValue = "3";
                    //txtStaticContent.Text = GlobalFunctions.RemoveSanitisedPrefixes(Server.HtmlDecode(EditHomePageContent.Content.Trim()));
                    txtStaticContents.Text = GlobalFunctions.RemoveSanitisedPrefixes(Server.HtmlDecode(EditHomePageContent.Content.Trim()));

                }
                else if (EditHomePageContent.ContentType.ToLower() == "sitelogo")
                {
                    Logo = "SiteLogo";
                    MobileLogo = "MobileLogo";
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = false;
                    ulTitle.Visible = false;
                    liAlt.Visible = false;
                    divSave.Visible = false;
                    divSiteLogo.Visible = true;
                    divFooterText.Visible = false;
                    divCopyRightInfo.Visible = false;
                    divUploadDocument.Visible = false;
                }
                else if (EditHomePageContent.ContentType.ToLower() == "footerlogo")
                {
                    Logo = "FooterLogo";
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = false;
                    ulTitle.Visible = false;
                    liAlt.Visible = false;
                    divSave.Visible = false;
                    divSiteLogo.Visible = true;
                    divFooterText.Visible = false;
                    divCopyRightInfo.Visible = false;
                    divUploadDocument.Visible = false;
                }
                else if (EditHomePageContent.ContentType.ToLower() == "footertext")
                {
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = false;
                    liAlt.Visible = false;
                    divSiteLogo.Visible = false;
                    divSiteLogo.Visible = false;

                    ddlContentType.SelectedValue = "6";

                    divFooterText.Visible = true;
                    //txtFooter.Visible = true;
                    txtFooters.Visible = true;
                    divCopyRightInfo.Visible = false;
                    divUploadDocument.Visible = false;

                    //txtFooter.Text = Server.HtmlDecode(EditHomePageContent.Content);
                    txtFooters.Text = Server.HtmlDecode(EditHomePageContent.Content);
                    ulTitle.Visible = false;
                }
                else if (EditHomePageContent.ContentType.ToLower() == "footercopyright")
                {
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = false;
                    liAlt.Visible = false;
                    divSiteLogo.Visible = false;
                    divSiteLogo.Visible = false;

                    ddlContentType.SelectedValue = "7";

                    divFooterText.Visible = false;

                    divCopyRightInfo.Visible = true;
                    divUploadDocument.Visible = false;
                    ulTitle.Visible = false;
                    txtCopyRight.Text = Server.HtmlDecode(EditHomePageContent.Content);
                }
                else if (EditHomePageContent.ContentType.ToLower() == "uploaddocument")
                {
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = false;
                    liAlt.Visible = false;
                    divSiteLogo.Visible = false;
                    divSiteLogo.Visible = false;
                    divFooterText.Visible = false;
                    divCopyRightInfo.Visible = false;
                    divUploadDocument.Visible = true;
                    ulTitle.Visible = false;

                    ddlContentType.SelectedValue = "8";
                }
            }
            else
            {
                var lstHomePageContentsObj = HomePageManagementBL.GetAllHomePageContentDetails().Where(x => x.LanguageId == GlobalFunctions.GetLanguageId()).ToList();

                lstHomePageContentsObj.ToList().ForEach(x => HttpUtility.HtmlDecode(x.Content));

                lstHomePageContents = lstHomePageContentsObj;
                lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "footertext").ToList();

                if (lstHomePageContents != null)
                {
                    if (lstHomePageContents.Count() > 0)
                    {
                        Session["FooterText"] = lstHomePageContents;
                        txtFooters.Text = Convert.ToString(lstHomePageContents[0].Content);
                        //txtFooter.Text = Convert.ToString(lstHomePageContents[0].Content);
                    }
                }
                lstHomePageContents = lstHomePageContentsObj;
                lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "footercopyright").ToList();
                if (lstHomePageContents != null)
                {
                    if (lstHomePageContents.Count() > 0)
                    {
                        Session["FooterCopyRight"] = lstHomePageContents;
                        txtCopyRight.Text = lstHomePageContents[0].Content;
                    }
                }
                ddlContentType.Enabled = true;
            }
        }

        protected void Btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            switch (btn.CommandArgument)
            {
                case "Save":
                    {
                        SaveHomePageContentDetails();
                        break;
                    }
                case "Cancel":
                    {
                        Response.Redirect(host + "Admin/HomePageManagement/HomePageContentListing.aspx");
                        break;
                    }
                default:
                    break;
            }
        }




        private bool SaveHomePageContentDetails()
        {
            bool flgSuccess = true;
            try
            {

                #region Validation
                if (ddlContentType.SelectedValue == "1")
                {
                    if (txtTitle.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Title can not be empty !!", AlertType.Warning);
                        return false;
                    }
                    if (txtAltName.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Alt name can not be empty !!", AlertType.Warning);
                        return false;
                    }
                    if (txtNavigateUrl.Text.Trim() != "")
                    {
                        if (!txtNavigateUrl.Text.ToLower().StartsWith("http://") && !txtNavigateUrl.Text.ToLower().StartsWith("https://"))
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Image navigate url must contain 'http://' or 'https://' !!", AlertType.Warning);
                            return false;
                        }

                    }

                }
                else if (ddlContentType.SelectedValue == "2")
                {

                    if (txtTitle.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Title can not be empty !!", AlertType.Warning);
                        return false;
                    }
                    if (txtAltName.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Alt name can not be empty !!", AlertType.Warning);
                        return false;
                    }
                    if (txtVideoLink.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Video link can not be empty !!", AlertType.Warning);
                        return false;
                    }

                    if (txtVideoHeight.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Video height can not be empty !!", AlertType.Warning);
                        return false;
                    }

                }
                else if (ddlContentType.SelectedValue == "3")
                {
                    if (txtTitle.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Title can not be empty !!", AlertType.Warning);
                        return false;
                    }
                    #region Commenetd by Sripal
                    //if (txtStaticContent.Text.Trim() == "")
                    //{
                    //    GlobalFunctions.ShowModalAlertMessages(this.Page, "Static text content can not be empty !!", AlertType.Warning);
                    //    return false;
                    //} 
                    #endregion
                    if (txtStaticContents.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Static text content can not be empty !!", AlertType.Warning);
                        return false;
                    }


                }

                else if (ddlContentType.SelectedValue == "6")
                {
                    //if (txtFooter.Text.Trim() == "")
                    //{
                    //    GlobalFunctions.ShowModalAlertMessages(this.Page, "Footer text can not be empty !!", AlertType.Warning);
                    //    return false;
                    //}
                    if (txtFooters.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Footer text can not be empty !!", AlertType.Warning);
                        return false;
                    }
                }
                else if (ddlContentType.SelectedValue == "7")
                {
                    if (txtCopyRight.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Copyright info can not be empty !!", AlertType.Warning);
                        return false;
                    }


                }
                #endregion

                /*Sachin Chauhan : 28 09 2015 : Rectified below written code as Language Id getting passed should be of drop down list*/
                //var lstHomePageContentsObj = HomePageManagementBL.GetAllHomePageContentDetails().Where(x => x.LanguageId == GlobalFunctions.GetLanguageId()).ToList();

                var lstHomePageContentsObj = HomePageManagementBL.GetAllHomePageContentDetails().Where(x => x.LanguageId.ToString() == ddlLanguage.SelectedValue).ToList();
                lstHomePageContentsObj.ToList().ForEach(x => HttpUtility.HtmlDecode(x.Content));
                HomePageManagementBE EditHomePageContent = Session["EditHomePageContent"] as HomePageManagementBE;

                if (EditHomePageContent == null)
                {
                    EditHomePageContent = new HomePageManagementBE();
                }

                if (Session["EditHomePageContent"] != null && Session["EditOperation"].ToString().Equals("Update"))
                {


                    EditHomePageContent.HomePageStaticContentId = (Session["EditHomePageContent"] as HomePageManagementBE).HomePageStaticContentId;

                    if (EditHomePageContent.Title != Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@')))
                    {

                        if (!CheckHomePageContentTitleExist(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@')), EditHomePageContent.ContentType))
                        {
                            EditHomePageContent.Title = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);

                            if (ddlContentType.SelectedValue == "1")
                            {
                                #region StaticImage Update
                                EditHomePageContent.ContentType = "StaticImage";
                                EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtAltName.Text.Trim().Replace('\'', '@'));
                                EditHomePageContent.URL = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtNavigateUrl.Text.Trim().Replace('\'', '@'));
                                EditHomePageContent.ImageExtension = SetExtension(hdnStaticImageSrc.Value);
                                HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                                #endregion
                            }

                            else if (ddlContentType.SelectedValue == "2")
                            {
                                #region Video Update
                                EditHomePageContent.ContentType = "Video";
                                EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtAltName.Text.Trim().Replace('\'', '@'));
                                EditHomePageContent.URL = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtVideoLink.Text.Trim().Replace('\'', '@'));


                                EditHomePageContent.Height = Convert.ToInt16(txtVideoHeight.Text.Trim());

                                EditHomePageContent.HeightUnit = "px";
                                HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                                #endregion
                            }
                            else if (ddlContentType.SelectedValue == "3")
                            {
                                #region StaticText Update
                                EditHomePageContent.ContentType = "StaticText";
                                //EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtStaticContent.Text.Trim().Replace('\'', '@')));
                                //  EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtStaticContents.Text.Trim().Replace('\'', '@')));

                                /*Sachin Chauhan Start : 23-11-15*/
                                EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                                /*Sachin Chauhan End : */
                                #endregion
                            }
                            else if (ddlContentType.SelectedValue == "4")
                            {
                                #region Sitelogo
                                EditHomePageContent.ContentType = "SiteLogo";
                                EditHomePageContent.Width = Convert.ToInt16(txtWidth.Text.Trim());
                                #endregion
                            }
                            else if (ddlContentType.SelectedValue == "5")
                            {
                                #region FooterLogo
                                EditHomePageContent.ContentType = "FooterLogo";
                                EditHomePageContent.Width = Convert.ToInt16(txtWidth.Text.Trim());
                                #endregion
                            }
                            else if (ddlContentType.SelectedValue == "6")
                            {
                                #region FooterText Update
                                EditHomePageContent.ContentType = "FooterText";
                                //EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtFooter.Text.Trim().Replace('\'', '@')));
                                EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtFooters.Text.Trim().Replace('\'', '@')));

                                lstHomePageContents = lstHomePageContentsObj;
                                lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "footertext").ToList();

                                if (lstHomePageContents != null)
                                {
                                    EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                                }
                                else
                                {
                                    EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                                }
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                #endregion
                            }
                            else if (ddlContentType.SelectedValue == "7")
                            {
                                #region FooterCopyRight Update
                                EditHomePageContent.ContentType = "FooterCopyRight";
                                EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtCopyRight.Text.Trim().Replace('\'', '@')));
                                lstHomePageContents = lstHomePageContentsObj;
                                lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "footertext").ToList();

                                if (lstHomePageContents != null)
                                {
                                    EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                                }
                                else
                                {
                                    EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                                }
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                #endregion
                            }
                            else if (ddlContentType.SelectedValue == "8")
                            {
                                #region File Upload
                                EditHomePageContent.ContentType = "uploaddocument";
                                EditHomePageContent.Content = "";
                                lstHomePageContents = lstHomePageContentsObj;
                                lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "document").ToList();

                                if (lstHomePageContents != null)
                                {
                                    EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                                }
                                else
                                {
                                    EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                                }
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "File uploaded successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                #endregion
                            }


                            if (EditHomePageContent.HomePageStaticContentId > 0)
                            {

                                if (EditHomePageContent.ContentType == "StaticImage")
                                {
                                    SetImageName(EditHomePageContent.ImageExtension, "staticimage", EditHomePageContent.HomePageStaticContentId);
                                }
                                Session["ContentType"] = EditHomePageContent.ContentType;
                                //GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", AlertType.Success);
                            }
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Title already taken!", AlertType.Warning);
                        }

                    }
                    else
                    {

                        EditHomePageContent.Title = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@'));
                        EditHomePageContent.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                        if (ddlContentType.SelectedValue == "1")
                        {
                            #region StaticImage Set Parameter for Add
                            EditHomePageContent.ContentType = "StaticImage";
                            EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtAltName.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.URL = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtNavigateUrl.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.ImageExtension = SetExtension(hdnStaticImageSrc.Value);
                            EditHomePageContent.Title = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@'));
                            #endregion
                        }

                        else if (ddlContentType.SelectedValue == "2")
                        {
                            #region Video Set Parameter for Add
                            EditHomePageContent.ContentType = "Video";
                            EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtAltName.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.URL = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtVideoLink.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.Title = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.Height = Convert.ToInt16(txtVideoHeight.Text.Trim());
                            #endregion
                        }
                        else if (ddlContentType.SelectedValue == "3")
                        {
                            #region StaticText Set Parameter for Add
                            EditHomePageContent.ContentType = "StaticText";
                            //EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtStaticContent.Text.Trim().Replace('\'', '@'));
                            //EditHomePageContent.Content = Server.HtmlEncode(txtStaticContent.Text.Trim());Commented by Sripal
                            EditHomePageContent.Content = Server.HtmlEncode(txtStaticContents.Text.Trim());
                            EditHomePageContent.Title = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@'));
                            #endregion
                        }
                        else if (ddlContentType.SelectedValue == "4")
                        {
                            #region SiteLogo Set Parameter for Add
                            EditHomePageContent.ContentType = "SiteLogo";
                            EditHomePageContent.Width = Convert.ToInt16(txtWidth.Text.Trim());
                            #endregion
                        }
                        else if (ddlContentType.SelectedValue == "5")
                        {
                            #region FooterLogo Set Parameter for Add
                            EditHomePageContent.ContentType = "FooterLogo";
                            EditHomePageContent.Width = Convert.ToInt16(txtWidth.Text.Trim());
                            #endregion
                        }
                        else if (ddlContentType.SelectedValue == "6")
                        {
                            #region FooterText Update and Add
                            EditHomePageContent.ContentType = "FooterText";
                            //EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(txtFooter.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(txtFooters.Text.Trim().Replace('\'', '@'));
                            if (lstHomePageContents != null)
                            {
                                EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                            }
                            else
                            {
                                EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                            }
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", "HomePageContentListing.aspx", AlertType.Success);
                            #endregion
                        }
                        else if (ddlContentType.SelectedValue == "7")
                        {
                            #region FooterCopyRight Update and Add
                            EditHomePageContent.ContentType = "FooterCopyRight";
                            EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(txtCopyRight.Text.Trim().Replace('\'', '@'));
                            if (lstHomePageContents != null)
                            {
                                EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                            }
                            else
                            {
                                EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                            }
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", "HomePageContentListing.aspx", AlertType.Success);
                            #endregion
                        }

                        EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                        if (EditHomePageContent.HomePageStaticContentId > 0)
                        {

                            if (EditHomePageContent.ContentType == "StaticImage")
                            {
                                string saveFilePath = Server.MapPath("~/Images/StaticImage");
                                string[] newUploadedImage = hdnStaticImageSrc.Value.Split('/');
                                EditHomePageContent.ThumbNail = EditHomePageContent.HomePageStaticContentId + EditHomePageContent.ImageExtension;
                                /*Sachin Chauhan Start : 22 02 2016 : Checked if file already exists then the code should not try reuploading image*/
                                if (!EditHomePageContent.ThumbNail.ToLower().Equals(newUploadedImage[newUploadedImage.Length - 1]))
                                {
                                    File.Delete(saveFilePath + "\\" + EditHomePageContent.ThumbNail);
                                    File.Copy(saveFilePath + "\\" + newUploadedImage[newUploadedImage.Length - 1], saveFilePath + "\\" + EditHomePageContent.ThumbNail, true);
                                }



                                SetImageName(EditHomePageContent.ImageExtension, "staticimage", EditHomePageContent.HomePageStaticContentId);
                            }
                            Session["ContentType"] = EditHomePageContent.ContentType;
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", "HomePageContentListing.aspx", AlertType.Success);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", AlertType.Success);
                        }
                    }



                }
                else
                {

                    if (ddlContentType.SelectedValue == "1")
                    {
                        EditHomePageContent.ContentType = "StaticImage";
                        EditHomePageContent.Title = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@'));
                        EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtAltName.Text.Trim().Replace('\'', '@'));
                        EditHomePageContent.URL = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtNavigateUrl.Text.Trim().Replace('\'', '@'));
                        EditHomePageContent.ImageExtension = SetExtension(hdnStaticImageSrc.Value);


                    }
                    else if (ddlContentType.SelectedValue == "2")
                    {
                        EditHomePageContent.ContentType = "Video";
                        EditHomePageContent.ImageExtension = "";
                        EditHomePageContent.Height = Convert.ToInt16(txtVideoHeight.Text.Trim());
                        EditHomePageContent.HeightUnit = "px";
                        EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtAltName.Text.Trim().Replace('\'', '@'));
                        EditHomePageContent.URL = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtVideoLink.Text.Trim().Replace('\'', '@'));
                        EditHomePageContent.Title = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@'));
                    }
                    else if (ddlContentType.SelectedValue == "3")
                    {
                        EditHomePageContent.ContentType = "StaticText";
                        EditHomePageContent.ImageExtension = "";
                        EditHomePageContent.URL = "#";

                        //EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtStaticContent.Text.Trim().Replace('\'', '@'));Commented by sripal
                        //EditHomePageContent.Content = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtStaticContents.Text.Trim().Replace('\'', '@'));
                        EditHomePageContent.Content = Server.HtmlEncode(txtStaticContents.Text.Trim());
                        EditHomePageContent.Title = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text.Trim().Replace('\'', '@'));
                    }

                    else if (ddlContentType.SelectedValue == "4")
                    {
                        EditHomePageContent.ContentType = "SiteLogo";
                        EditHomePageContent.Width = Convert.ToInt16(txtWidth.Text.Trim());

                    }
                    else if (ddlContentType.SelectedValue == "5")
                    {
                        EditHomePageContent.ContentType = "FooterLogo";
                        EditHomePageContent.Width = Convert.ToInt16(txtWidth.Text.Trim());





                    }
                    else if (ddlContentType.SelectedValue == "6")
                    {
                        lstHomePageContents = lstHomePageContentsObj;
                        lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "footertext").ToList();
                        //lstHomePageContents[0].Title = Microsoft.Security.Application.Encoder.HtmlEncode(txtFooter.Text.Trim().Replace('\'', '@'));
                        //lstHomePageContents[0].Content = Microsoft.Security.Application.Encoder.HtmlEncode(txtFooter.Text.Trim().Replace('\'', '@'));
                        if (lstHomePageContents.Count > 0)
                        {
                            //lstHomePageContents[0].Title = Microsoft.Security.Application.Encoder.HtmlEncode(txtFooters.Text.Trim().Replace('\'', '@'));
                            lstHomePageContents[0].Title = Microsoft.Security.Application.Encoder.HtmlEncode("FooterText");
                            lstHomePageContents[0].Content = Microsoft.Security.Application.Encoder.HtmlEncode(txtFooters.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent = lstHomePageContents[0];
                            if (lstHomePageContents != null)
                            {
                                if (lstHomePageContents.Count() > 0)
                                {
                                    EditHomePageContent.HomePageStaticContentId = lstHomePageContents[0].HomePageStaticContentId;

                                    /*Sachin Chauhan : 28 09 2015 : Rectified belwo code wrong entity getting passed in update operation*/
                                    //EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                                    EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(lstHomePageContents[0], DBAction.Update);

                                    /*Sachin Chauhan : 28 09 2015*/
                                    if (EditHomePageContent.HomePageStaticContentId > 0)
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                    }
                                }
                                else
                                {
                                    EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                                    if (EditHomePageContent.HomePageStaticContentId > 0)
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                    }
                                }
                            }
                            else
                            {
                                EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                                if (EditHomePageContent.HomePageStaticContentId > 0)
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                }
                            }
                        }
                        else
                        {
                            //EditHomePageContent.Title = Microsoft.Security.Application.Encoder.HtmlEncode(txtFooters.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.Title = Microsoft.Security.Application.Encoder.HtmlEncode("FooterText");
                            EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(txtFooters.Text.Trim().Replace('\'', '@'));
                            EditHomePageContent.ContentType = "FooterText";
                            EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);

                            if (EditHomePageContent.HomePageStaticContentId > 0)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", "HomePageContentListing.aspx", AlertType.Success);
                            }
                        }
                    }
                    else if (ddlContentType.SelectedValue == "7")
                    {
                        EditHomePageContent.ContentType = "FooterCopyRight";
                        EditHomePageContent.Title = EditHomePageContent.Content;
                        EditHomePageContent.Content = Microsoft.Security.Application.Encoder.HtmlEncode(txtCopyRight.Text.Trim().Replace('\'', '@'));


                        lstHomePageContents = lstHomePageContentsObj;
                        lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "footercopyright").ToList();


                        if (lstHomePageContents != null)
                        {
                            if (lstHomePageContents.Count() > 0)
                            {
                                EditHomePageContent.HomePageStaticContentId = lstHomePageContents[0].HomePageStaticContentId;
                                EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Update);
                                if (EditHomePageContent.HomePageStaticContentId > 0)
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                }
                            }
                            else
                            {
                                EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                                if (EditHomePageContent.HomePageStaticContentId > 0)
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                }
                            }

                        }
                        else
                        {
                            EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                            if (EditHomePageContent.HomePageStaticContentId > 0)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", "HomePageContentListing.aspx", AlertType.Success);
                            }
                        }




                    }
                    if (ddlContentType.SelectedValue != "7" && ddlContentType.SelectedValue != "6")
                    {
                        if (!CheckHomePageContentTitleExist(EditHomePageContent.Title, EditHomePageContent.ContentType))
                        {
                            EditHomePageContent.HomePageStaticContentId = HomePageManagementBL.HomePageStaticContentAED(EditHomePageContent, DBAction.Insert);
                            Session["ContentID"] = EditHomePageContent.HomePageStaticContentId;
                            CreateActivityLog("HomePageContentManagement", "Insert", ddlContentType.SelectedItem.Text);
                            if (EditHomePageContent.HomePageStaticContentId > 0)
                            {

                                if (EditHomePageContent.ContentType == "StaticImage")
                                {
                                    SetImageName(EditHomePageContent.ImageExtension, "staticimage", EditHomePageContent.HomePageStaticContentId);
                                }
                                Session["ContentType"] = EditHomePageContent.ContentType;
                                Session["EditHomePageContent"] = EditHomePageContent;
                                Session["EditOperation"] = "Update";
                                //GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                if (EditHomePageContent.ContentType == "StaticImage" || EditHomePageContent.ContentType == "StaticText")
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", AlertType.Success);
                                }
                                else
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                }
                            }
                            else
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Details saved successfully!", "HomePageContentListing.aspx", AlertType.Success);
                            }

                            /*Open File required for invalidating cache in WRITE mode*/
                            /*Write Current Time Stamp in file*/
                            /*Close File*/

                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Title already exist for another Image!", AlertType.Warning);
                        }
                        //ClearControls();
                    }


                }




            }
            catch (Exception ex)
            {
                flgSuccess = false;
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error saving Homepage content details!", AlertType.Failure);
                //throw;
            }
            finally
            {

            }
        Exit:
            return flgSuccess;
        }

        private void ClearControls()
        {

            txtAltName.Text = "";
            txtTitle.Text = "";
            txtVideoLink.Text = "";
            txtNavigateUrl.Text = "";
            //txtStaticContent.Text = "";
            txtStaticContents.Text = "";
            hdnStaticImageSrc.Value = "";


        }

        protected void ddlContentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlContentType.SelectedValue == "1")
            {
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = true;
                ulTitle.Visible = true;
                liAlt.Visible = true;
                divSave.Visible = true;
                divSiteLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
            }
            else if (ddlContentType.SelectedValue == "2")
            {
                divStaticText.Visible = false;
                divVideo.Visible = true;
                divStaticImage.Visible = false;
                ulTitle.Visible = true;
                liAlt.Visible = true;
                divSave.Visible = true;
                divSiteLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
            }
            else if (ddlContentType.SelectedValue == "3")
            {
                divStaticText.Visible = true;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                ulTitle.Visible = true;
                liAlt.Visible = false;
                divSave.Visible = true;
                divSiteLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
            }

            else if (ddlContentType.SelectedValue == "4")
            {
                Logo = "SiteLogo";
                MobileLogo = "MobileLogo";
                mobileimage.Visible = true;
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                ulTitle.Visible = false;
                liAlt.Visible = false;
                divSave.Visible = false;
                divSiteLogo.Visible = true;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;

                /*Desktop Logo*/
                string FilePath = Server.MapPath("~/Images/SiteLogo") + "\\";
                string fileName = "";
                string _fileName = "";
                DirectoryInfo di = new DirectoryInfo(FilePath);
                FileInfo[] files = di.GetFiles("SiteLogo" + "*.*");
                foreach (FileInfo file in files)
                    try
                    {
                        file.Attributes = FileAttributes.Normal;
                        fileName = file.Name + "?1=" + DateTime.Now.TimeOfDay;
                        _fileName = file.Name.Replace(Path.GetExtension(file.Name), "");

                    }
                    catch { }
                if (fileName != "")
                    SiteLogoImageHtml = "<li class=\" qq-upload-success\"><img  style=\"background-color:gray\" class=\"uploadedimage\" src=\"" + host + "Images/SiteLogo/" + fileName + "\"><a id=SiteLogo class=\"cloaseuplod1\" onclick=\"RemoveFile(this.id)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";

                FilePath = host + "Images/SiteLogo/" + _fileName;
                string[] fileNameParts = FilePath.Split('_');
                if (fileNameParts.Length > 1)
                {
                    txtWidth.Text = fileNameParts[1];
                    ddlWidthUnit.SelectedValue = fileNameParts[2] == "px" ? "2" : "1";
                    txtHeight.Text = fileNameParts[3];
                    ddlHeightUnit.SelectedValue = fileNameParts[4] == "px" ? "2" : "1";
                }


                /*Mobile Logo*/

                string FilePathmobile = Server.MapPath("~/Images/SiteLogo/Mobile") + "\\";
                string fileNamemobile = "";
                string _fileNamemobile = "";
                DirectoryInfo dimobile = new DirectoryInfo(FilePathmobile);
                FileInfo[] filesmobile = dimobile.GetFiles("mobileLogo" + "*.*");
                foreach (FileInfo file in filesmobile)
                    try
                    {
                        file.Attributes = FileAttributes.Normal;
                        fileNamemobile = file.Name + "?1=" + DateTime.Now.TimeOfDay;
                        _fileNamemobile = file.Name.Replace(Path.GetExtension(file.Name), "");

                    }
                    catch { }
                if (fileNamemobile != "")
                    mobileLogoImageHtml = "<li class=\" qq-upload-success\"><img  style=\"background-color:gray\" class=\"uploadedimage\" src=\"" + host + "Images/SiteLogo/Mobile/" + fileNamemobile + "\"><a id=mobileLogo class=\"cloaseuplod1\" onclick=\"RemoveFile(this.id)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                else mobileLogoImageHtml = "";

                FilePathmobile = host + "Images/SiteLogo/Mobile/" + _fileNamemobile;
                string[] fileNamePart = FilePathmobile.Split('_');
                if (fileNamePart.Length > 1)
                {
                    txtWidth.Text = fileNamePart[1];
                    ddlWidthUnit.SelectedValue = fileNamePart[2] == "px" ? "2" : "1";
                    txtHeight.Text = fileNamePart[3];
                    ddlHeightUnit.SelectedValue = fileNamePart[4] == "px" ? "2" : "1";
                }




            }
            else if (ddlContentType.SelectedValue == "5")
            {
                Logo = "FooterLogo";
                divStaticText.Visible = false;
                mobileimage.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                ulTitle.Visible = false;
                liAlt.Visible = false;
                divSave.Visible = false;
                divSiteLogo.Visible = true;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
                string FilePath = Server.MapPath("~/Images/FooterLogo") + "\\";
                string fileName = "";
                string _fileName = "";
                DirectoryInfo di = new DirectoryInfo(FilePath);
                FileInfo[] files = di.GetFiles("FooterLogo" + "*.*");
                foreach (FileInfo file in files)
                    try
                    {
                        file.Attributes = FileAttributes.Normal;
                        fileName = file.Name + "?1=" + DateTime.Now.TimeOfDay;
                        _fileName = file.Name.Replace(Path.GetExtension(file.Name), "");

                    }
                    catch { }
                if (fileName != "")
                    SiteLogoImageHtml = "<li class=\" qq-upload-success\"><img style=\"background-color:gray\" class=\"uploadedimage\" src=\"" + host + "Images/FooterLogo/" + fileName + "\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                FilePath = host + "Images/FooterLogo/" + _fileName;
                string[] fileNameParts = FilePath.Split('_');
                if (fileNameParts.Length > 1)
                {
                    txtWidth.Text = fileNameParts[1];
                    ddlWidthUnit.SelectedValue = fileNameParts[2] == "px" ? "2" : "1";
                    txtHeight.Text = fileNameParts[3];
                    ddlHeightUnit.SelectedValue = fileNameParts[4] == "px" ? "2" : "1";
                }
            }
            else if (ddlContentType.SelectedValue == "6")
            {

                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                ulTitle.Visible = false;
                liAlt.Visible = false;
                divSave.Visible = true;
                divSiteLogo.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
                divFooterText.Visible = true;

            }
            else if (ddlContentType.SelectedValue == "7")
            {

                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                ulTitle.Visible = false;
                liAlt.Visible = false;
                divSave.Visible = true;
                divSiteLogo.Visible = false;
                divCopyRightInfo.Visible = true;
                divUploadDocument.Visible = false;
                divFooterText.Visible = false;


            }
            else if (ddlContentType.SelectedValue == "8")
            {
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                ulTitle.Visible = false;
                liAlt.Visible = false;
                divSave.Visible = false;
                divSiteLogo.Visible = false;
                divCopyRightInfo.Visible = false;
                divFooterText.Visible = false;
                divUploadDocument.Visible = true;
            }
        }



        /// <summary>
        /// to get the extension of file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string SetExtension(string name)
        {
            string FileExt = string.Empty;
            try
            {

                string BackLogoExt = string.Empty;
                if (name != "")
                {
                    if (name.IndexOf("?") > 0)
                    {
                        FileExt = name.Substring(name.LastIndexOf("."), (name.IndexOf("?") - name.LastIndexOf(".")));
                    }
                    else
                    {
                        FileExt = name.Substring(name.LastIndexOf("."));
                    }
                }


            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }
            return FileExt;
        }
        /// <summary>
        /// To set the image name
        /// </summary>
        /// <param name="FileExt"></param>
        /// <param name="type"></param>
        /// <param name="CategoryId"></param>
        private void SetImageName(string FileExt, string type, int Cid)
        {
            if (FileExt != "")
            {
                //Rename logo file with category id
                try
                {
                    if (type == "staticimage")
                    {
                        string FileName = hdnStaticImageSrc.Value;
                        FileName = FileName.Replace(host, "~/");

                        if (File.Exists(Server.MapPath("~/Images/StaticImage/") + Cid + FileExt))
                        {


                        }
                        else if (File.Exists(Server.MapPath(FileName)))
                        {
                            File.Copy(Server.MapPath(FileName), Server.MapPath("~/Images/StaticImage/") + Cid + FileExt);
                            if (File.Exists(Server.MapPath(FileName)))
                            {
                                File.Delete(Server.MapPath(FileName));
                                hdnStaticImageSrc.Value = host + "Images/StaticImage/" + Cid + FileExt;

                                if (hdnStaticImageSrc.Value != "")
                                {
                                    ImageHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + hdnStaticImageSrc.Value + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                                }
                            }
                        }
                    }


                }
                catch (Exception ex1) { }
            }

        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 19-08-15
        /// Scope   : to check whether the title exist in the db or not
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        private bool CheckHomePageContentTitleExist(string Title, string ContentType)
        {
            List<HomePageManagementBE> HomePageContentList = HomePageManagementBL.GetAllHomePageContentDetails().Where(g => g.ContentType == ContentType).ToList();
            if (HomePageContentList != null)
            {
                if (HomePageContentList.Count() > 0)
                {
                    int count = HomePageContentList.Count(x => x.Title == Title);
                    return count > 0 ? true : false;
                }
            }
            return false;
        }

        private void PopulateLanguageDropDownList()
        {
            List<LanguageBE> languages = new List<LanguageBE>();
            languages = LanguageBL.GetAllLanguageDetails(true);

            if (languages != null)
            {
                if (languages.Count() > 0)
                {
                    ddlLanguage.DataSource = languages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                }
            }
        }

        #region "Added By Hardik on 18/Nov/2016"
        protected void btnUploadDocument_Click(object sender, EventArgs e)
        {
            if (pdfFileUploader.PostedFiles.Count > 0)
            {
                HttpFileCollection hfc = Request.Files;

                string Physicalpath = GlobalFunctions.GetPhysicalFolderPath() + "\\Images\\FilesUpload\\";//Server.MapPath("~/Fonts/");
                if (!System.IO.File.Exists(Physicalpath))
                {
                    System.IO.Directory.CreateDirectory(Physicalpath);
                }
                // pdfFileUploader.PostedFile.SaveAs(MapPath("~") + "/Uploads/" + pdfFileUploader.PostedFile.FileName);
                int iUploadedCnt = 0;
                int iFailedCnt = 0;

                if (hfc.Count <= 100)    // 100 FILES RESTRICTION.
                {
                    for (int i = 0; i <= hfc.Count - 1; i++)
                    {
                        HttpPostedFile hpf = hfc[i];
                        string MIMEType = hpf.ContentType;
                        if (MIMEType == "application/pdf")
                        {
                            if (hpf.ContentLength > 0)
                            {
                                if (!File.Exists(Server.MapPath("FilesUpload\\") + Path.GetFileName(hpf.FileName)))
                                {
                                    // DirectoryInfo objDir = new DirectoryInfo(Server.MapPath("Fonts\\"));
                                    DirectoryInfo objDir = new DirectoryInfo(Physicalpath);
                                    string sFileName = Path.GetFileName(hpf.FileName);
                                    string sFileExt = Path.GetExtension(hpf.FileName);

                                    // CHECK FOR DUPLICATE FILES.
                                    //FileInfo[] objFI =  objDir.GetFiles(sFileName.Replace(sFileExt, "") + ".*");
                                    FileInfo[] objFI = objDir.GetFiles(sFileName);

                                    if (objFI.Length > 0)
                                    {
                                        // CHECK IF FILE WITH THE SAME NAME EXISTS

                                        foreach (FileInfo file in objFI)
                                        {
                                            string sFileName1 = objFI[0].Name;
                                            string sFileExt1 = Path.GetExtension(objFI[0].Name);

                                            if (sFileName1 == sFileName)
                                            {
                                                iFailedCnt += 1;        // NOT ALLOWING DUPLICATE.
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // SAVE THE FILE IN A FOLDER.
                                        string FileExtension = Convert.ToString(Path.GetExtension(hpf.FileName).Replace(".", ""));
                                        hpf.SaveAs(Physicalpath + Path.GetFileName(hpf.FileName));
                                        iUploadedCnt += 1;
                                    }
                                }
                            }
                        }
                        /*
                         * MIMEType == "application/vnd.ms-excel" for .xls, .xlt, .xla
                         * MIMEType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" for .xlsx
                         */
                        if (MIMEType == "application/vnd.ms-excel" || MIMEType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        {
                            if (hpf.ContentLength > 0)
                            {

                                if (!File.Exists(Server.MapPath("FilesUpload\\") + Path.GetFileName(hpf.FileName)))
                                {
                                    // DirectoryInfo objDir = new DirectoryInfo(Server.MapPath("Fonts\\"));
                                    DirectoryInfo objDir = new DirectoryInfo(Physicalpath);
                                    string sFileName = Path.GetFileName(hpf.FileName);
                                    string sFileExt = Path.GetExtension(hpf.FileName);

                                    // CHECK FOR DUPLICATE FILES.
                                    //FileInfo[] objFI =  objDir.GetFiles(sFileName.Replace(sFileExt, "") + ".*");
                                    FileInfo[] objFI = objDir.GetFiles(sFileName);

                                    if (objFI.Length > 0)
                                    {
                                        // CHECK IF FILE WITH THE SAME NAME EXISTS

                                        foreach (FileInfo file in objFI)
                                        {
                                            string sFileName1 = objFI[0].Name;
                                            string sFileExt1 = Path.GetExtension(objFI[0].Name);

                                            if (sFileName1 == sFileName)
                                            {
                                                iFailedCnt += 1;        // NOT ALLOWING DUPLICATE.
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // SAVE THE FILE IN A FOLDER.
                                        string FileExtension = Convert.ToString(Path.GetExtension(hpf.FileName).Replace(".", ""));
                                        hpf.SaveAs(Physicalpath + Path.GetFileName(hpf.FileName));
                                        iUploadedCnt += 1;
                                    }
                                }
                            }
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "" + iUploadedCnt + "Excel", AlertType.Success);
                        }
                        /*
                         * MIMEType == "application/msword" for .doc, .dot
                         * MIMEType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" for .docx
                         */
                        if (MIMEType == "application/msword" || MIMEType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                        {
                            if (hpf.ContentLength > 0)
                            {

                                if (!File.Exists(Server.MapPath("FilesUpload\\") + Path.GetFileName(hpf.FileName)))
                                {
                                    // DirectoryInfo objDir = new DirectoryInfo(Server.MapPath("Fonts\\"));
                                    DirectoryInfo objDir = new DirectoryInfo(Physicalpath);
                                    string sFileName = Path.GetFileName(hpf.FileName);
                                    string sFileExt = Path.GetExtension(hpf.FileName);

                                    // CHECK FOR DUPLICATE FILES.
                                    //FileInfo[] objFI =  objDir.GetFiles(sFileName.Replace(sFileExt, "") + ".*");
                                    FileInfo[] objFI = objDir.GetFiles(sFileName);

                                    if (objFI.Length > 0)
                                    {
                                        // CHECK IF FILE WITH THE SAME NAME EXISTS

                                        foreach (FileInfo file in objFI)
                                        {
                                            string sFileName1 = objFI[0].Name;
                                            string sFileExt1 = Path.GetExtension(objFI[0].Name);

                                            if (sFileName1 == sFileName)
                                            {
                                                iFailedCnt += 1;        // NOT ALLOWING DUPLICATE.
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // SAVE THE FILE IN A FOLDER.
                                        string FileExtension = Convert.ToString(Path.GetExtension(hpf.FileName).Replace(".", ""));
                                        hpf.SaveAs(Physicalpath + Path.GetFileName(hpf.FileName));
                                        iUploadedCnt += 1;
                                    }
                                }
                            }
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "" + iUploadedCnt + "Word", AlertType.Success);
                        }
                    }
                }
                if (iUploadedCnt >= 1)
                {
                    CreateActivityLog("FilesUpload", "Uploaded", "");
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "" + "Files Uploaded Successfully", host + "Admin/HomePageManagement/HomePageContentListing.aspx", AlertType.Success);
                    Session["ContentType"] = "Upload Document";
                    //Response.Redirect(host + "Admin/HomePageManagement/HomePageContentListing.aspx");
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "" + "Please upload pdf,word,excel files only.", host + "Admin/HomePageManagement/HomePageContentManagement.aspx", AlertType.Failure);
                }
            }
        }
        #endregion


        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["EditHomePageContent"] != null)
            {
                HomePageManagementBE EditHomePageContent = Session["EditHomePageContent"] as HomePageManagementBE;
                ddlContentType.Enabled = false;
                txtTitle.Text = EditHomePageContent.Title;

                List<HomePageManagementBE> HomePageContentList = HomePageManagementBL.GetAllHomePageContentDetails();
                HomePageContentList = HomePageContentList.Where(x => x.LanguageId == Convert.ToInt16(ddlLanguage.SelectedValue)).ToList();
                EditHomePageContent = HomePageContentList.Where(x => x.HomePageStaticContentId == EditHomePageContent.HomePageStaticContentId).FirstOrDefault();

                if (EditHomePageContent.ContentType.ToLower() == "Staticimage")
                {
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = true;
                    divFooterText.Visible = false;
                    liAlt.Visible = true;

                    ddlContentType.SelectedValue = "1";
                    txtAltName.Text = EditHomePageContent.Content.Trim();
                    txtNavigateUrl.Text = EditHomePageContent.URL.Trim();

                    hdnStaticImageSrc.Value = host + "Images/StaticImage/" + EditHomePageContent.HomePageStaticContentId + EditHomePageContent.ImageExtension.ToString();

                    if (hdnStaticImageSrc.Value != "")
                    {
                        ImageHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + hdnStaticImageSrc.Value + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                        //  hdnStaticImageSrc.Value = hdnStaticImageSrc.Value + "?1=1376400078917";
                    }
                }
                else if (EditHomePageContent.ContentType.ToLower() == "video")
                {
                    divStaticText.Visible = false;
                    divVideo.Visible = true;
                    divStaticImage.Visible = false;
                    divFooterText.Visible = false;
                    liAlt.Visible = true;

                    ddlContentType.SelectedValue = "2";
                    txtAltName.Text = EditHomePageContent.Content.Trim();
                    txtVideoLink.Text = EditHomePageContent.URL.Trim();
                }
                else if (EditHomePageContent.ContentType.ToLower() == "statictext")
                {
                    divStaticText.Visible = true;
                    divVideo.Visible = false;
                    divStaticImage.Visible = false;
                    divFooterText.Visible = false;
                    liAlt.Visible = false;

                    ddlContentType.SelectedValue = "3";
                    //txtStaticContent.Text = EditHomePageContent.Content.Trim();
                    txtStaticContents.Text = HttpUtility.HtmlDecode(EditHomePageContent.Content.Trim());

                }
                else if (EditHomePageContent.ContentType.ToLower() == "footertext")
                {
                    divFooterText.Visible = true;
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = false;
                    liAlt.Visible = false;

                    ddlContentType.SelectedValue = "6";
                    //txtFooter.Text = EditHomePageContent.Content.Trim();
                    txtFooters.Text = Server.HtmlDecode(EditHomePageContent.Content.Trim());
                }
            }
            else
            {
                ddlContentType.Enabled = true;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetLogoDimensions(string width, string widthUnit, string height, string heightUnit, string ActionType)
        {
            string FileUploadPath = string.Empty;
            string FileName = string.Empty;
            if (ActionType.ToLower() == "site logo")
            {
                FileUploadPath = HttpContext.Current.Server.MapPath("~/Images/SiteLogo") + "\\";
                DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                FileInfo[] files = di.GetFiles("SiteLogo" + "*.*");

                if (files.Length == 0)
                {
                    files = di.GetFiles("*.*");
                    foreach (FileInfo file in files)
                    {
                        if (!file.Name.Contains("Thumbs.db"))
                        {
                            file.CopyTo("SiteLogo" + file.Extension);
                        }
                    }
                }

                if (files.Length > 0)
                {

                    foreach (FileInfo file in files)
                        try
                        {

                            file.Attributes = FileAttributes.Normal;
                            if (!file.Name.Contains("Thumbs.db"))
                            {
                                int ExtIndex = file.Name.IndexOf('.');
                                string fileExt = file.Name.Substring(ExtIndex, 4);
                                string fileName = file.Name.Replace(fileExt, "");
                                fileName = fileName + "_" + DateTime.Now.ToString("dd-MM-yy hh-mm-ss") + fileExt;
                                FileName = fileName;
                                if (!Directory.Exists(FileUploadPath + "BackUp"))
                                {
                                    Directory.CreateDirectory(FileUploadPath + "BackUp");

                                }
                                File.Copy(FileUploadPath + file.Name, FileUploadPath + "BackUp\\" + fileName, true);

                            }
                            File.Copy(file.FullName, HttpContext.Current.Server.MapPath("~/Images/SiteLogo") + "\\" + "/sitelogo" + "_" + width + "_" + widthUnit + "_" + height + "_" + heightUnit + Path.GetExtension(FileName));
                            File.Delete(file.FullName);

                        }
                        catch (Exception ex) { }
                }
            }
            if (ActionType.ToLower() == "footer logo")
            {
                FileUploadPath = HttpContext.Current.Server.MapPath("~/Images/FooterLogo") + "\\";
                DirectoryInfo di = new DirectoryInfo(FileUploadPath);
                FileInfo[] files = di.GetFiles("FooterLogo" + "*.*");
                if (files.Length > 0)
                {

                    foreach (FileInfo file in files)
                        try
                        {

                            file.Attributes = FileAttributes.Normal;
                            if (!file.Name.Contains("Thumbs.db"))
                            {
                                int ExtIndex = file.Name.IndexOf('.');
                                string fileExt = file.Name.Substring(ExtIndex, 4);
                                string fileName = file.Name.Replace(fileExt, "");
                                fileName = fileName + "_" + DateTime.Now.ToString("dd-MM-yy hh-mm-ss") + fileExt;
                                FileName = fileName;
                                if (!Directory.Exists(FileUploadPath + "BackUp"))
                                {
                                    Directory.CreateDirectory(FileUploadPath + "BackUp");

                                }
                                File.Copy(FileUploadPath + file.Name, FileUploadPath + "BackUp\\" + fileName, true);

                            }

                            File.Copy(file.FullName, HttpContext.Current.Server.MapPath("~/Images/FooterLogo") + "\\" + "footerlogo" + "_" + width + "_" + widthUnit + "_" + height + "_" + heightUnit + Path.GetExtension(FileName));
                            File.Delete(file.FullName);
                        }
                        catch (Exception ex) { }
                }
            }




            return "Updated Successfully";

        }

    }
}

