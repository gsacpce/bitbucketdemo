﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using System.IO;
using System.Data;
namespace Presentation
{
    public class Admin_HomePageContentListing : BasePage//System.Web.UI.Page
    {
        public string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.GridView gvStaticImage, gvVideo, gvStaticText, Gv_imgs, grdUploadedDocuments;

        protected global::System.Web.UI.WebControls.DropDownList ddlContentType;

        protected global::System.Web.UI.HtmlControls.HtmlContainerControl divStaticImage, divVideo, divStaticText, divSiteLogo, divFooterLogo, divFooterText, divCopyRightInfo, divUploadDocument;

        protected global::System.Web.UI.HtmlControls.HtmlImage ImgSiteLogo, ImgFooterLogo, ImgMobileLogo;

        protected global::System.Web.UI.WebControls.HiddenField hdnFooterTextId, hdnCopyRightId;

        protected global::System.Web.UI.WebControls.Literal ltlFooterText, ltlCopyRight;


        protected global::System.Web.UI.WebControls.LinkButton btnFooterUpdate, btnCopyRightUpdate;

        public Int16 DefaultLanguage = 1;

        public string host = GlobalFunctions.GetVirtualPath();
        protected void Page_Load(object sender, EventArgs e)
        {




            if (!IsPostBack)
            {
                Session["EditHomePageContent"] = null;
                Session["EditOperation"] = null;

                if (Session["ContentType"] != null)
                {
                    string ContentType = Session["ContentType"].ToString();
                    if (ContentType == "StaticImage")
                    {
                        divStaticText.Visible = false;
                        divVideo.Visible = false;
                        divStaticImage.Visible = true;
                        divSiteLogo.Visible = false;
                        divFooterLogo.Visible = false;
                        divFooterText.Visible = false;
                        divCopyRightInfo.Visible = false;
                        divUploadDocument.Visible = false;
                        ddlContentType.SelectedValue = "1";
                    }
                    else if (ContentType == "Video")
                    {
                        divStaticText.Visible = false;
                        divVideo.Visible = true;
                        divStaticImage.Visible = false;
                        divSiteLogo.Visible = false;
                        divFooterLogo.Visible = false;
                        divFooterText.Visible = false;
                        divCopyRightInfo.Visible = false;
                        divUploadDocument.Visible = false;
                        ddlContentType.SelectedValue = "2";
                    }
                    else if (ContentType == "StaticText")
                    {
                        divStaticText.Visible = true;
                        divVideo.Visible = false;
                        divSiteLogo.Visible = false;
                        divStaticImage.Visible = false;
                        divFooterLogo.Visible = false;
                        divFooterText.Visible = false;
                        divCopyRightInfo.Visible = false;
                        divUploadDocument.Visible = false;
                        ddlContentType.SelectedValue = "3";
                    }
                    else if (ContentType == "SiteLogo")
                    {
                        divStaticText.Visible = false;
                        divVideo.Visible = false;
                        divStaticImage.Visible = false;
                        divSiteLogo.Visible = true;
                        divFooterLogo.Visible = false;
                        divFooterText.Visible = false;
                        divCopyRightInfo.Visible = false;
                        divUploadDocument.Visible = false;
                        ddlContentType.SelectedValue = "4";
                    }
                    else if (ContentType == "FooterLogo")
                    {
                        divStaticText.Visible = false;
                        divVideo.Visible = false;
                        divStaticImage.Visible = false;
                        divSiteLogo.Visible = false;
                        divFooterLogo.Visible = true;
                        divFooterText.Visible = false;
                        divCopyRightInfo.Visible = false;
                        divUploadDocument.Visible = false;
                        ddlContentType.SelectedValue = "5";
                    }
                    else if (ContentType == "FooterText")
                    {
                        divStaticText.Visible = false;
                        divVideo.Visible = false;
                        divStaticImage.Visible = false;
                        divSiteLogo.Visible = false;
                        divFooterLogo.Visible = false;
                        divFooterText.Visible = true;
                        divCopyRightInfo.Visible = false;
                        divUploadDocument.Visible = false;
                        ddlContentType.SelectedValue = "6";
                        btnFooterUpdate.Visible = true;
                    }
                    else if (ContentType == "FooterCopyRight")
                    {
                        divStaticText.Visible = false;
                        divVideo.Visible = false;
                        divStaticImage.Visible = false;
                        divSiteLogo.Visible = false;
                        divFooterLogo.Visible = false;
                        divFooterText.Visible = false;
                        divCopyRightInfo.Visible = true;
                        divUploadDocument.Visible = false;
                        ddlContentType.SelectedValue = "7";
                        btnCopyRightUpdate.Visible = true;
                    }
                    else if(ContentType=="Upload Document")
                    {
                        divUploadDocument.Visible = true;
                        divStaticText.Visible = false;
                        divVideo.Visible = false;
                        divStaticImage.Visible = false;
                        divSiteLogo.Visible = false;
                        divFooterLogo.Visible = false;
                        divFooterText.Visible = false;
                        divCopyRightInfo.Visible = false;
                        ddlContentType.SelectedValue = "8";
                    }
                }
                else
                {
                    divStaticText.Visible = false;
                    divVideo.Visible = false;
                    divStaticImage.Visible = true;
                    divSiteLogo.Visible = false;
                    divFooterLogo.Visible = false;
                    divFooterText.Visible = false;
                    divCopyRightInfo.Visible = false;
                    divUploadDocument.Visible = false;
                }


                GetAllHomePageContent();

            }

        }

      

        private void GetAllHomePageContent()
        {
            List<HomePageManagementBE> lstHomePageContents = new List<HomePageManagementBE>();

            //var lstHomePageContentsObj = HomePageManagementBL.GetAllHomePageContentDetails().ToList();
            var lstHomePageContentsObj = HomePageManagementBL.GetAllHomePageContentDetails().Where(x => x.LanguageId == DefaultLanguage).ToList();

            lstHomePageContentsObj.ToList().ForEach(x => HttpUtility.HtmlDecode(x.Content));
            if (lstHomePageContentsObj != null)
            {
                if (lstHomePageContentsObj.Count() > 0)
                {
                    lstHomePageContents = lstHomePageContentsObj;
                    lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "staticimage").ToList();

                    if (lstHomePageContents != null)
                    {
                        if (lstHomePageContents.Count() > 0)
                        {
                            gvStaticImage.DataSource = lstHomePageContents;
                            Session["StaticImage"] = lstHomePageContents;
                            gvStaticImage.DataBind();
                        }
                    }

                    lstHomePageContents = lstHomePageContentsObj;
                    lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "video").ToList();
                    if (lstHomePageContents != null)
                    {
                        if (lstHomePageContents.Count() > 0)
                        {

                            gvVideo.DataSource = lstHomePageContents;
                            Session["Video"] = lstHomePageContents;
                            gvVideo.DataBind();
                        }
                    }

                    lstHomePageContents = lstHomePageContentsObj;
                    lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "statictext").ToList();

                    if (lstHomePageContents != null)
                    {
                        if (lstHomePageContents.Count() > 0)
                        {

                            gvStaticText.DataSource = lstHomePageContents;
                            Session["StaticText"] = lstHomePageContents;
                            gvStaticText.DataBind();
                        }
                    }
                    lstHomePageContents = lstHomePageContentsObj;
                    lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "footertext").ToList();

                    if (lstHomePageContents != null)
                    {
                        if (lstHomePageContents.Count() > 0)
                        {
                            Session["FooterText"] = lstHomePageContents;
                            hdnFooterTextId.Value = Convert.ToString(lstHomePageContents[0].HomePageStaticContentId);
                            ltlFooterText.Text = Server.HtmlDecode(lstHomePageContents[0].Content);
                            btnFooterUpdate.Visible = true;
                        }
                        else
                        {

                            btnFooterUpdate.Visible = false;
                            ltlFooterText.Text = "No Record found";
                        }
                    }

                    lstHomePageContents = lstHomePageContentsObj;
                    lstHomePageContents = lstHomePageContents.Where(g => g.ContentType.ToLower() == "footercopyright").ToList();
                    if (lstHomePageContents != null)
                    {
                        if (lstHomePageContents.Count() > 0)
                        {
                            Session["FooterCopyRight"] = lstHomePageContents;
                            hdnCopyRightId.Value = Convert.ToString(lstHomePageContents[0].HomePageStaticContentId);
                            ltlCopyRight.Text = lstHomePageContents[0].Content;
                            btnCopyRightUpdate.Visible = true;
                        }
                        else
                        {

                            btnCopyRightUpdate.Visible = false;
                            ltlCopyRight.Text = "No Record found";
                        }
                    }


                    List<ListItem> Imgs = new List<ListItem>();

                    string FilePath = Server.MapPath("~/Images/SiteLogo") + "\\";
                    string fileName = "";
                    DirectoryInfo di = new DirectoryInfo(FilePath);
                    FileInfo[] files = di.GetFiles("SiteLogo" + "*.*");
                    foreach (FileInfo file in files)
                        try
                        {
                            file.Attributes = FileAttributes.Normal;
                            fileName = file.Name;
                            // ImgSiteLogo.Src = host + "Images/SiteLogo/" + fileName;
                            // ViewState["desktoplogopath"] = host + "Images/SiteLogo/" + fileName;
                            Imgs.Add(new ListItem(fileName, host + "Images/SiteLogo/" + fileName));
                        }
                        catch { }


                    FilePath = Server.MapPath("~/Images/SiteLogo/Mobile") + "\\";
                    di = new DirectoryInfo(FilePath);
                    files = di.GetFiles("mobileLogo" + "*.*");

                    foreach (FileInfo file in files)
                        try
                        {
                            file.Attributes = FileAttributes.Normal;
                            fileName = file.Name;

                            //ImgMobileLogo.Src = host + "Images/SiteLogo/Mobile/" + fileName;
                            Imgs.Add(new ListItem(fileName, host + "Images/SiteLogo/Mobile/" + fileName));
                        }
                        catch { }


                    Gv_imgs.DataSource = Imgs;
                    Gv_imgs.DataBind();


                    FilePath = Server.MapPath("~/Images/FooterLogo") + "\\";
                    di = new DirectoryInfo(FilePath);
                    files = di.GetFiles("FooterLogo" + "*.*");

                    foreach (FileInfo file in files)
                        try
                        {
                            file.Attributes = FileAttributes.Normal;
                            fileName = file.Name;
                            ImgFooterLogo.Src = host + "Images/FooterLogo/" + fileName;
                        }
                        catch { }


                    #region ReadUploadedPDF

                    try
                    {
                        DataTable dt= CreateTableStructure();
                        DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Images/FilesUpload"));
                        FileInfo[] uploadedFiles = d.GetFiles();
                        foreach (FileInfo file in uploadedFiles)
                        {   
                                DataRow dr = dt.NewRow();
                                dr["FileName"] = Path.GetFileName(file.Name);
                                dr["FilePath"] =  Host + "Images/FilesUpload/" + file;
                                dt.Rows.Add(dr);
                        }
                        if (dt != null)
                        {
                            grdUploadedDocuments.DataSource = dt;
                            grdUploadedDocuments.DataBind();
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                    #endregion



                }
                // gvStaticImage.DataSource = lstHomePageContents.Count() > 0 ? lstHomePageContents.Where(g => g.ContentType == "Image") : null;






            }

        }
        protected DataTable CreateTableStructure()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FileName", typeof(string));
            dt.Columns.Add("FilePath", typeof(string));
            return dt;
        }

        protected void Btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            LinkButton lnkBtn = sender as LinkButton;

            if (btn != null)
                switch (btn.CommandName.ToLower())
                {
                    case "add":
                        {
                            Session["EditHomePageContent"] = null;
                            Response.Redirect("HomePageContentManagement.aspx");
                            break;
                        }
                    case "cancel":
                        {
                            break;
                        }
                    default:
                        break;
                }
            if (lnkBtn != null)
                switch (lnkBtn.CommandName.ToLower())
                {
                    case "update":
                        {
                            HomePageManagementBE EditHomePageContent = new HomePageManagementBE();

                            if (lnkBtn.Attributes["rel"] == "StaticImage")
                            {
                                if (Session["StaticImage"] != null)
                                {
                                    foreach (var item in (Session["StaticImage"] as List<HomePageManagementBE>))
                                    {
                                        if (item.HomePageStaticContentId == Convert.ToInt16(lnkBtn.CommandArgument))
                                        {
                                            EditHomePageContent = item;

                                        }
                                    }
                                    Session["EditHomePageContent"] = EditHomePageContent;
                                    Session["EditOperation"] = lnkBtn.CommandName;
                                    Response.Redirect("HomePageContentManagement.aspx");
                                }

                            }
                            else if (lnkBtn.Attributes["rel"] == "Video")
                            {

                                if (Session["Video"] != null)
                                {
                                    foreach (var item in (Session["Video"] as List<HomePageManagementBE>))
                                    {
                                        if (item.HomePageStaticContentId == Convert.ToInt16(lnkBtn.CommandArgument))
                                        {
                                            EditHomePageContent = item;
                                        }
                                    }
                                    Session["EditHomePageContent"] = EditHomePageContent;
                                    Session["EditOperation"] = lnkBtn.CommandName;
                                    Response.Redirect("HomePageContentManagement.aspx");
                                }
                            }
                            else if (lnkBtn.Attributes["rel"] == "StaticText")
                            {
                                if (Session["StaticText"] != null)
                                {
                                    foreach (var item in (Session["StaticText"] as List<HomePageManagementBE>))
                                    {
                                        if (item.HomePageStaticContentId == Convert.ToInt16(lnkBtn.CommandArgument))
                                        {
                                            EditHomePageContent = item;
                                        }
                                    }
                                    Session["EditHomePageContent"] = EditHomePageContent;
                                    Session["EditOperation"] = lnkBtn.CommandName;
                                    Response.Redirect("HomePageContentManagement.aspx");
                                }
                            }
                            else if (lnkBtn.Attributes["rel"] == "FooterText")
                            {
                                if (Session["FooterText"] != null)
                                {
                                    foreach (var item in (Session["FooterText"] as List<HomePageManagementBE>))
                                    {
                                        if (item.HomePageStaticContentId == Convert.ToInt16(hdnFooterTextId.Value))
                                        {
                                            EditHomePageContent = item;
                                        }
                                    }
                                    Session["EditHomePageContent"] = EditHomePageContent;
                                    Session["EditOperation"] = lnkBtn.CommandName;
                                    Response.Redirect("HomePageContentManagement.aspx");
                                }
                            }
                            else if (lnkBtn.Attributes["rel"] == "FooterCopyRight")
                            {
                                if (Session["FooterCopyRight"] != null)
                                {
                                    foreach (var item in (Session["FooterCopyRight"] as List<HomePageManagementBE>))
                                    {
                                        if (item.HomePageStaticContentId == Convert.ToInt16(hdnCopyRightId.Value))
                                        {
                                            EditHomePageContent = item;
                                        }
                                    }
                                    Session["EditHomePageContent"] = EditHomePageContent;
                                    Session["EditOperation"] = lnkBtn.CommandName;
                                    Response.Redirect("HomePageContentManagement.aspx");
                                }
                            }

                            break;
                        }
                    case "delete":
                        {
                            HomePageManagementBE EditHomePageContent = new HomePageManagementBE();

                            if (lnkBtn.Attributes["rel"] == "StaticImage")
                            {
                                if (Session["StaticImage"] != null)
                                {
                                    foreach (var item in (Session["StaticImage"] as List<HomePageManagementBE>))
                                    {
                                        if (item.HomePageStaticContentId == Convert.ToInt16(lnkBtn.CommandArgument))
                                        {
                                            EditHomePageContent = item;
                                        }
                                    }
                                    Session["EditHomePageContent"] = EditHomePageContent;
                                    Session["EditOperation"] = lnkBtn.CommandName;

                                }

                            }
                            else if (lnkBtn.Attributes["rel"] == "Video")
                            {

                                if (Session["Video"] != null)
                                {
                                    foreach (var item in (Session["Video"] as List<HomePageManagementBE>))
                                    {
                                        if (item.HomePageStaticContentId == Convert.ToInt16(lnkBtn.CommandArgument))
                                        {
                                            EditHomePageContent = item;
                                        }
                                    }
                                    Session["EditHomePageContent"] = EditHomePageContent;
                                    Session["EditOperation"] = lnkBtn.CommandName;

                                }
                            }
                            else if (lnkBtn.Attributes["rel"] == "StaticText")
                            {
                                if (Session["StaticText"] != null)
                                {
                                    foreach (var item in (Session["StaticText"] as List<HomePageManagementBE>))
                                    {
                                        if (item.HomePageStaticContentId == Convert.ToInt16(lnkBtn.CommandArgument))
                                        {
                                            EditHomePageContent = item;
                                        }
                                    }
                                    Session["EditHomePageContent"] = EditHomePageContent;
                                    Session["EditOperation"] = lnkBtn.CommandName;

                                }
                            }

                            if (Session["EditHomePageContent"] != null && Session["EditOperation"].ToString().Equals("Delete"))
                            {
                                HomePageManagementBE objHomePageContent = Session["EditHomePageContent"] as HomePageManagementBE;
                                if (HomePageManagementBL.HomePageStaticContentAED(objHomePageContent, DBAction.Delete) > 0)
                                {
                                    Session["ContentType"] = objHomePageContent.ContentType;
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Deleted successfully!", "HomePageContentListing.aspx", AlertType.Success);
                                };
                            }





                            break;
                        }

                    case "cancel":
                        {
                            break;
                        }
                    default:
                        break;
                }
        }
        protected void gvStaticImage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int NewIndex = e.NewPageIndex;
            gvStaticImage.PageIndex = NewIndex;
            List<HomePageManagementBE> lstHomePageContents = new List<HomePageManagementBE>();
            lstHomePageContents = HomePageManagementBL.GetAllHomePageContentDetails().Where(x => x.LanguageId == DefaultLanguage).ToList();
            lstHomePageContents = lstHomePageContents.Where(g => g.ContentType == "StaticImage").ToList();
            gvStaticImage.DataSource = lstHomePageContents;
            gvStaticImage.DataBind();



        }
        protected void gvVideo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            List<HomePageManagementBE> lstHomePageContents = new List<HomePageManagementBE>();
            int NewIndex = e.NewPageIndex;
            gvVideo.PageIndex = NewIndex;
            lstHomePageContents = HomePageManagementBL.GetAllHomePageContentDetails().Where(x => x.LanguageId == DefaultLanguage).ToList();
            lstHomePageContents = HomePageManagementBL.GetAllHomePageContentDetails().Where(g => g.ContentType == "Video").ToList();
            gvVideo.DataSource = lstHomePageContents;
            gvVideo.DataBind();

        }
        protected void gvStaticText_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            List<HomePageManagementBE> lstHomePageContents = new List<HomePageManagementBE>();
            int NewIndex = e.NewPageIndex;
            gvStaticText.PageIndex = NewIndex;
            lstHomePageContents = HomePageManagementBL.GetAllHomePageContentDetails().Where(x => x.LanguageId == DefaultLanguage).ToList();
            lstHomePageContents = HomePageManagementBL.GetAllHomePageContentDetails().Where(g => g.ContentType == "StaticText").ToList();
            gvStaticText.DataSource = lstHomePageContents;
            gvStaticText.DataBind();

        }
        protected void gvStaticImage_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Response.Write("Delete handled");
        }
        protected void ddlContentType_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlContentType.SelectedValue == "1")
            {
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = true;
                divSiteLogo.Visible = false;
                divFooterLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
            }
            else if (ddlContentType.SelectedValue == "2")
            {
                divStaticText.Visible = false;
                divVideo.Visible = true;
                divStaticImage.Visible = false;
                divSiteLogo.Visible = false;
                divFooterLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
            }
            else if (ddlContentType.SelectedValue == "3")
            {

                divStaticText.Visible = true;
                divVideo.Visible = false;
                divSiteLogo.Visible = false;
                divStaticImage.Visible = false;
                divFooterLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
            }
            else if (ddlContentType.SelectedValue == "4")
            {
                divStaticText.Visible = true;
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                divSiteLogo.Visible = true;
                divFooterLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;
            }
            else if (ddlContentType.SelectedValue == "5")
            {
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                divSiteLogo.Visible = false;
                divFooterLogo.Visible = true;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;

            }

            else if (ddlContentType.SelectedValue == "6")
            {
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                divSiteLogo.Visible = false;
                divFooterLogo.Visible = false;
                divFooterText.Visible = true;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = false;

            }

            else if (ddlContentType.SelectedValue == "7")
            {
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                divSiteLogo.Visible = false;
                divFooterLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = true;
                divUploadDocument.Visible = false;

            }

            else if (ddlContentType.SelectedValue == "8")
            {
                divStaticText.Visible = false;
                divVideo.Visible = false;
                divStaticImage.Visible = false;
                divSiteLogo.Visible = false;
                divFooterLogo.Visible = false;
                divFooterText.Visible = false;
                divCopyRightInfo.Visible = false;
                divUploadDocument.Visible = true;
            }
        }


        protected void grdUploadedDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkFilename = e.Row.FindControl("lnkFilename") as LinkButton;
                Literal ltrFileName = e.Row.FindControl("ltrFileName") as Literal;
                lnkFilename.Attributes.Add("href", Host + "Images/FilesUpload/" + ltrFileName.Text);
                lnkFilename.Attributes.Add("target", "_blank");
            }
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
           string fileName = "";
            try
            {
                fileName = (sender as LinkButton).CommandName;
                string FilewithPath = GlobalFunctions.GetPhysicalFolderPath() + "Images/FilesUpload/" + fileName;
                File.Delete(FilewithPath);

                    DataTable dt = CreateTableStructure();
                    DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Images/FilesUpload"));
                    FileInfo[] uploadedFiles = d.GetFiles();
                    foreach (FileInfo file in uploadedFiles)
                    {
                        DataRow dr = dt.NewRow();
                        dr["FileName"] = Path.GetFileName(file.Name);
                        dr["FilePath"] = Host + "Images/FilesUpload/" + file;
                        dt.Rows.Add(dr);
                    }
                    grdUploadedDocuments.DataSource = dt;
                    grdUploadedDocuments.DataBind();
                    CreateActivityLog("HomePageContentListing", "Delete", fileName);
                GlobalFunctions.ShowModalAlertMessages(this.Page, fileName + " has been deleted", AlertType.Success);
            }
            catch (Exception ex)
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, fileName + " has been deleted", AlertType.Failure);
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void gvStaticImage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkbtnUpdate = (LinkButton)e.Row.FindControl("lnkbtnUpdate");
                lnkbtnUpdate.Attributes.Add("rel", "StaticImage");

                Image ImgThumbNail = (Image)e.Row.FindControl("ImgThumbNail");

                ImgThumbNail.ImageUrl = GlobalFunctions.GetVirtualPath() + @"Images/StaticImage/" + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Thumbnail")).ToString() + "?" + Guid.NewGuid();

                LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
                lnkbtnDelete.Attributes.Add("rel", "StaticImage");
            }

        }

        protected void gvVideo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkbtnUpdate = (LinkButton)e.Row.FindControl("lnkbtnUpdate");
                lnkbtnUpdate.Attributes.Add("rel", "Video");

                LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
                lnkbtnDelete.Attributes.Add("rel", "Video");
            }
        }
        protected void gvStaticText_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkbtnUpdate = (LinkButton)e.Row.FindControl("lnkbtnUpdate");
                lnkbtnUpdate.Attributes.Add("rel", "StaticText");

                LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
                lnkbtnDelete.Attributes.Add("rel", "StaticText");

                HtmlGenericControl divStaticContent = (HtmlGenericControl)e.Row.FindControl("divStaticContent");
                divStaticContent.InnerHtml = Server.HtmlDecode(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Content")));
            }

        }


    }
}
