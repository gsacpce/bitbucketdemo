﻿using Excel;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.Text.RegularExpressions;



namespace Presentation
{
    public class Admin_Points_PointsUpdate : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.GridView gVPoints, gvUnregisteredUserpoints;
        protected global::System.Web.UI.WebControls.Button btnUpdate;
        protected global::System.Web.UI.WebControls.TextBox txtEmail, txtUPoints;
        protected global::System.Web.UI.WebControls.FileUpload fileuploadExcel, fileuploadUnReg;
        protected global::System.Web.UI.HtmlControls.HtmlContainerControl divPointsUpdate, divImport;
        protected global::System.Web.UI.WebControls.LinkButton LinkRegBackup, LinkUnRegBackup; // Added By snehal 22 09 2016

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divPointsUpdate.Visible = true;
                divImport.Visible = false;
                BindPointsData();
            }

        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtEmail.Text = "";
            BindPointsData();
        }

        private void BindPointsData()
        {
            List<UserBE> lstUsers = new List<UserBE>();
            //List<UserBE> lstUnRegisteredUsers = new List<UserBE>();
            lstUsers = UserBL.GetRegUnregUserDetails();
            //lstUnRegisteredUsers = UserBL.GetUnregisteredUsersDetails();
            if (lstUsers != null)
            {
                if (lstUsers.Count > 0)
                {
                    //Session["Users"] = Users;
                    gVPoints.DataSource = lstUsers;
                    gVPoints.DataBind();
                    //btnUpdate.Visible = true;
                }
                else
                {
                    gVPoints.DataSource = null;
                    gVPoints.DataBind();
                    // btnUpdate.Visible = false;
                }
            }

            //if (lstUnRegisteredUsers != null)
            //{
            //    if (lstUnRegisteredUsers.Count > 0)
            //    {
            //        gvUnregisteredUserpoints.DataSource = lstUnRegisteredUsers;
            //        gvUnregisteredUserpoints.DataBind();
            //    }
            //}
        }

        protected void btn_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "cancel": Response.Redirect(GlobalFunctions.GetVirtualPath() + "Admin/Points/PointsUpdate.aspx"); break;
                case "search": Search(); break;
                case "update": UpdatePoints(); break;
                case "import": Import(); break;
                #region Added By snehal 21 09 2016
                case "importNew": ImportNew(); break;
                case "showimportNew":
                    //divPointsUpdate.Visible = false;
                    divImport.Visible = true; break;
                case "exportNew": ExportNew(); break;
                #endregion
                case "showimport": divPointsUpdate.Visible = false;
                    divImport.Visible = true; break;
                case "export": Export(); break;
            }

        }
        #region Import Data for Registered user - Added by Snehal 22 09 2016
        protected void ImportNew()
        {
            try
            {
                if (fileuploadExcel.HasFile)
                {
                    bool status = false;
                    GetRegisteredUsersBackup();
                    GetUnRegisteredUsersBackup();

                    string xlsFile = fileuploadExcel.FileName;  //fileImport is the file upload control		
                    string ext = System.IO.Path.GetExtension(xlsFile);
                    if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Only Excel Files are allowed for Import.", AlertType.Warning);
                        return;
                    }
                    DataTable table = new DataTable();
                    table.Columns.Add("EmailId", typeof(string));
                    table.Columns.Add("Points", typeof(Int32));

                    string strFileName = fileuploadExcel.FileName;
                    string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\" + strFileName;
                    if (File.Exists(strFileUploadPath))
                    {
                        File.Delete(strFileUploadPath);
                    }
                    fileuploadExcel.SaveAs(strFileUploadPath);

                    DataTable dtErrorCheck = new DataTable();
                    dtErrorCheck = GlobalFunctions.ConvertExcelToDataTable(strFileUploadPath);
                    for (int i = 0; i < dtErrorCheck.Rows.Count; i++)
                    {
                        string emailid = dtErrorCheck.Rows[i]["EmailId"].ToString();
                        string points = dtErrorCheck.Rows[i]["Points"].ToString();
                        bool isvalidemail = IsValidEmail(emailid);
                        if (isvalidemail == false)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Email Id not Valid.", AlertType.Warning);
                            return;
                        }
                        bool isValidNumber = Regex.IsMatch(points, @"^\d+$");
                        if (isValidNumber == false)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Points should be Numeric values only.", AlertType.Warning);
                            return;
                        }
                    }

                    if (File.Exists(strFileUploadPath))
                    {
                        DataTable dt = new DataTable();
                        dt = GlobalFunctions.ConvertExcelToDataTable(strFileUploadPath);
                        DataRow[] rowArray = dt.Select();
                        foreach (DataRow row in rowArray)
                        {
                            table.ImportRow(row);
                        }
                    }
                    status = UserBL.UpdateRegUnregUserPoints(table);
                    if (status)
                    {
                        BindPointsData();
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "User Points Imported successfully", AlertType.Success);
                        // BindPointsData();		
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while Importing User Points. Please try again", AlertType.Failure);
                    }

                    //bool status = false;
                    //string ErrorFileName = "";
                    //int FailedRecords = 0;
                    //List<UserBE> lstRegisteredUsers = new List<UserBE>();
                    //lstRegisteredUsers = UserBL.GetUserDetails();
                    //DataTable dtbackup = new DataTable();

                    //dtbackup.Columns.Add("UserId");
                    //dtbackup.Columns.Add("EmailId");
                    //dtbackup.Columns.Add("FirstName");
                    //dtbackup.Columns.Add("MiddleName");
                    //dtbackup.Columns.Add("LastName");
                    //dtbackup.Columns.Add("Points");
                    //dtbackup.Columns.Add("Flag");
                    //foreach (UserBE User in lstRegisteredUsers)
                    //{
                    //    DataRow drow = dtbackup.NewRow();
                    //    drow["UserId"] = User.UserId;
                    //    drow["EmailId"] = User.EmailId;
                    //    drow["FirstName"] = User.FirstName;
                    //    drow["MiddleName"] = User.MiddleName;
                    //    drow["LastName"] = User.LastName;
                    //    drow["Points"] = User.Points;
                    //    dtbackup.Rows.Add(drow);
                    //}
                    //string xlsFile = fileuploadExcel.FileName;  //fileImport is the file upload control		
                    //string ext = System.IO.Path.GetExtension(xlsFile);
                    //if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
                    //{
                    //    GlobalFunctions.ShowModalAlertMessages(this.Page, "Only Excel Files are allowed for Import.", AlertType.Warning);
                    //    return;
                    //}
                    //DataTable table = new DataTable();
                    //table.Columns.Add("UserId", typeof(int));
                    //table.Columns.Add("EmailId", typeof(string));
                    //table.Columns.Add("FirstName", typeof(string));
                    //table.Columns.Add("MiddleName", typeof(string));
                    //table.Columns.Add("LastName", typeof(string));
                    //table.Columns.Add("Points", typeof(Int32));
                    //table.Columns.Add("Flag", typeof(Int16));
                    //string strFileName = fileuploadExcel.FileName;
                    //string fileName = "registeredUsersdata" + "_" + DateTime.Now.ToString("MMddyyyyHHmmss") + ".xls";
                    //string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\REGISTERED\" + strFileName;
                    //string strFileUploadPathbackup = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\REGISTERED\BACKUP\" + fileName;
                    //if (File.Exists(strFileUploadPath))
                    //{
                    //    File.Delete(strFileUploadPath);
                    //}
                    //string[] filePaths = Directory.GetFiles(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\REGISTERED\BACKUP\");
                    //foreach (string filePath in filePaths)
                    //    File.Delete(filePath);
                    //fileuploadExcel.SaveAs(strFileUploadPath);
                    //if (File.Exists(strFileUploadPath))
                    //{
                    //    DataTable dt = new DataTable();
                    //    dt = GlobalFunctions.ConvertExcelToDataTable(strFileUploadPath);
                    //    DataRow[] rowArray = dt.Select("flag=1 or flag=2");
                    //    foreach (DataRow row in rowArray)
                    //    {
                    //        table.ImportRow(row);
                    //    }
                    //}
                    //ExportToExcelAndSave(dtbackup, strFileUploadPathbackup);
                    //status = UserBL.UpdateRegisteredUsersPoints(table);
                    //if (status)
                    //{
                    //    divPointsUpdate.Visible = true;
                    //    divImport.Visible = false;
                    //    BindPointsData();
                    //    GlobalFunctions.ShowModalAlertMessages(this.Page, "User Points Imported successfully", AlertType.Success);
                    //    // BindPointsData();		
                    //}
                    //else
                    //{
                    //    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while Importing User Points. Please try again", AlertType.Failure);
                    //}
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select an Excel file to Import and try again", AlertType.Warning);
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private bool IsValidEmail(string strEmail)
        {
            string email = strEmail;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
                return true;
            else
                return false;
        }
        #endregion
        #region Export Data for Registered User - Added by Snehal 22 09 2016
        protected void ExportNew()
        {
            try
            {
                List<UserBE> Users = UserBL.GetRegUnregExportUserDetails();
                DataTable dt = new DataTable();
                //dt.Columns.Add("UserId");
                dt.Columns.Add("EmailId");
                dt.Columns.Add("FirstName");
                dt.Columns.Add("MiddleName");
                dt.Columns.Add("LastName");
                dt.Columns.Add("Points");
                //dt.Columns.Add("Flag");
                foreach (UserBE User in Users)
                {
                    DataRow drow = dt.NewRow();
                    //drow["UserId"] = User.UserId;
                    drow["EmailId"] = User.EmailId;
                    drow["FirstName"] = User.FirstName;
                    drow["MiddleName"] = User.MiddleName;
                    drow["LastName"] = User.LastName;
                    drow["Points"] = User.Points;
                    //drow["Flag"] = 0;
                    dt.Rows.Add(drow);
                }
                using (ExcelPackage pck = new ExcelPackage())
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("UsersPoints");
                    // DataTable NewTable = tbl.DefaultView.ToTable(false, "ProductCode","LanguageId","ProductName","ProductDescription", "FurtherDescription", "");

                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                    ws.Cells["A1"].LoadFromDataTable(dt, true);

                    Response.Clear();
                    //Write it back to the client
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=UsersPoints.xlsx");
                    Response.BinaryWrite(pck.GetAsByteArray());

                    Response.End();
                }
                //GlobalFunctions.ExportToExcel(dt, "UsersPoints");
                //dt = null;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion
        #region Backup for Registered user before making changes in Points - Added by Snehal 22 09 2016
        protected void LinkRegBackup_Click(object sender, EventArgs e)
        {
            string FinalFile = string.Empty;
            string BackUpFolderPath = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\REGISTERED\BACKUP";
            DirectoryInfo di = new DirectoryInfo(BackUpFolderPath);
            FileInfo[] TXTFiles = di.GetFiles("registeredUsersdata*");
            if (TXTFiles.Length > 0)
            {
                foreach (var fi in TXTFiles)
                {
                    FinalFile = fi.ToString();
                }
                Response.Clear();
                Response.BufferOutput = false;
                Response.ContentType = "application / vnd.ms - excel";
                string filePath = BackUpFolderPath + "\\" + FinalFile.ToString();
                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + FinalFile + "\"");
                Response.TransmitFile(filePath);
                Response.End();
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "There is no Registered file for Backup.", AlertType.Warning);
            }
        }
        #endregion
        #region Backup for Un-Registered user before making changes in Points - Added by Snehal 22 09 2016
        protected void LinkUnRegBackup_Click(object sender, EventArgs e)
        {
            string FinalFile = string.Empty;
            string BackUpFolderPath = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\UNREGISTERED\BACKUP";
            DirectoryInfo di = new DirectoryInfo(BackUpFolderPath);
            FileInfo[] TXTFiles = di.GetFiles("UnregisteredUsersdata*");
            if (TXTFiles.Length > 0)
            {
                foreach (var fi in TXTFiles)
                {
                    FinalFile = fi.ToString();
                }
                Response.Clear();
                Response.BufferOutput = false;
                Response.ContentType = "application / vnd.ms - excel";
                string filePath = BackUpFolderPath + "\\" + FinalFile.ToString();
                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + FinalFile + "\"");
                Response.TransmitFile(filePath);
                Response.End();
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "There is no Un-Registered file for Backup.", AlertType.Warning);
            }
        }
        #endregion

        protected void Import()
        {
            try
            {
                if (!fileuploadExcel.HasFile)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Excel File.", AlertType.Warning);
                }
                else
                {
                    bool status = false;
                    List<ProductBE> lstProductBE = new List<ProductBE>();
                    string xlsFile = fileuploadExcel.FileName;  //fileImport is the file upload control
                    string ext = System.IO.Path.GetExtension(xlsFile);
                    if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Only Excel Files are allowed for Product Import.", AlertType.Warning);
                        return;
                    }

                    string targetFileName = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\REGISTERED\" + xlsFile;
                    if (System.IO.File.Exists(targetFileName))
                    {
                        System.IO.File.Delete(targetFileName);
                    }
                    fileuploadExcel.PostedFile.SaveAs(targetFileName);
                    FileStream stream = File.Open(targetFileName, FileMode.Open, FileAccess.Read);

                    DataSet dsresult = new DataSet();
                    DataTable dtData = new DataTable();

                    if (ext.ToLower() == ".xlsx")
                    {
                        IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        excelReader.IsFirstRowAsColumnNames = true;
                        dsresult = excelReader.AsDataSet();
                        excelReader.Close();
                        excelReader.Dispose();
                    }
                    else if (ext.ToLower() == ".xls")
                    {
                        IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                        excelReader.IsFirstRowAsColumnNames = true;
                        dsresult = excelReader.AsDataSet();
                        excelReader.Close();
                    }
                    if (ValidateColumnsStructure(dsresult))
                    {
                        if (dsresult.Tables.Count > 0)
                        {
                            bool res = ProductBL.BulkImportExcel(dsresult.Tables[0], "dbo.Tbl_ImportUsers");
                            if (res)
                            {
                                bool Isrecordaffected = UserBL.UpdateImportDataToOriginalTable();
                                if (Isrecordaffected)
                                {
                                    divPointsUpdate.Visible = true;
                                    divImport.Visible = false;
                                    BindPointsData();
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Points are Updated Successfully.", AlertType.Success);
                                    //GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/UpdatedMessage "), AlertType.Success);
                                    CreateActivityLog("PointsUpdate","Update","");
                                }
                            }
                        }
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Proper Data Structure.", AlertType.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected bool ValidateColumnsStructure(DataSet dsExcel)
        {
            int count = 0;
            bool res = false;
            DataTable dt = new DataTable();
            dt.Columns.Add("UserId", typeof(Int16));
            dt.Columns.Add("EmailId", typeof(string));
            dt.Columns.Add("FirstName", typeof(string));
            dt.Columns.Add("MiddleName", typeof(string));
            dt.Columns.Add("LastName", typeof(string));
            dt.Columns.Add("Points", typeof(string));

            for (int i = 0; i < dsExcel.Tables[0].Columns.Count; i++)
            {
                if (dt.Columns[i].ToString() == dsExcel.Tables[0].Columns[i].ColumnName)
                {
                    count++;
                }
                else
                {
                    return false;
                }
            }
            if (count == 6)
                return true;
            else
                return false;
        }

        protected DataTable GetDataIntoTable(List<UserPointBE> returnList)
        {

            DataTable dt = GlobalFunctions.To_DataTable(returnList);
            dt.Columns["UserId"].ColumnName = "UserId";
            dt.Columns["PreviousPoints"].ColumnName = "PreviousPoints";
            dt.Columns["Points"].ColumnName = "Points";
            return dt;
        }

        protected void UpdatePoints()
        {
            try
            {
                List<UserPointBE> Users = new List<UserPointBE>();
                foreach (GridViewRow gvPointRow in gVPoints.Rows)
                {
                    HiddenField hdnUserId = (HiddenField)gvPointRow.Cells[0].FindControl("hdnUserId");
                    TextBox txtPoints = (TextBox)gvPointRow.Cells[0].FindControl("txtPoints");
                    if (txtPoints.Text == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Points can't be blank.", AlertType.Warning);
                        return;
                    }
                    HiddenField hdnPreviousPoints = (HiddenField)gvPointRow.Cells[0].FindControl("hdnPreviousPoints");
                    Users.Add(new UserPointBE() { UserId = Convert.ToInt16(hdnUserId.Value), PreviousPoints = Convert.ToInt32(hdnPreviousPoints.Value), Points = Convert.ToInt32(txtPoints.Text) });
                    // Response.Write(" User Id " + hdnUserId.Value + " and point is " + txtPoints.Text);


                }
                DataTable dtUpdatePoint = GetDataIntoTable(Users);
                bool IsPointUpdated = UserBL.UpdatePoints(dtUpdatePoint);
                if (IsPointUpdated)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Points updated succesfully", AlertType.Success);
                    CreateActivityLog("PointsUpdate", "Update", "");
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void Search()
        {
            List<UserBE> Users = new List<UserBE>();
            Users = UserBL.GetUserPoints(txtEmail.Text.ToString());

            if (Users != null)
            {
                if (Users.Count > 0)
                {
                    //Session["Users"] = Users;
                    gVPoints.DataSource = Users;
                    gVPoints.DataBind();
                }
                else
                {
                    gVPoints.DataSource = null;
                    gVPoints.DataBind();
                }
            }
            else
            {
                gVPoints.DataSource = null;
                gVPoints.DataBind();
            }
        }

        protected void Export()
        {
            try
            {
                List<UserBE> Users = UserBL.GetUserDetails();


                DataTable dt = new DataTable();
                dt.Columns.Add("UserId");
                dt.Columns.Add("EmailId");
                dt.Columns.Add("FirstName");
                dt.Columns.Add("MiddleName");
                dt.Columns.Add("LastName");
                dt.Columns.Add("Points");

                foreach (UserBE User in Users)
                {
                    DataRow drow = dt.NewRow();
                    drow["UserId"] = User.UserId;
                    drow["EmailId"] = User.EmailId;
                    drow["FirstName"] = User.FirstName;
                    drow["MiddleName"] = User.MiddleName;
                    drow["LastName"] = User.LastName;
                    drow["Points"] = User.Points;
                    dt.Rows.Add(drow);
                }

                GlobalFunctions.ExportToExcel(dt, "UsersPoints");
                dt = null;
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void gVPoints_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            int NewIndex = e.NewPageIndex;
            gVPoints.PageIndex = NewIndex;
            BindPointsData();
        }

        protected void btnUExport_Click(object sender, EventArgs e)
        {
            try
            {
                List<UserBE> Users = UserBL.GetUnregisteredUsersDetails();
                DataTable dt = new DataTable();
                dt.Columns.Add("EmailId");
                dt.Columns.Add("Points");
                dt.Columns.Add("Flag");

                foreach (UserBE User in Users)
                {
                    DataRow drow = dt.NewRow();
                    drow["EmailId"] = User.EmailId;
                    drow["Points"] = User.Points;
                    drow["Flag"] = 0;
                    dt.Rows.Add(drow);
                }
                using (ExcelPackage pck = new ExcelPackage())
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("UsersPoints");
                    // DataTable NewTable = tbl.DefaultView.ToTable(false, "ProductCode","LanguageId","ProductName","ProductDescription", "FurtherDescription", "");

                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                    ws.Cells["A1"].LoadFromDataTable(dt, true);

                    Response.Clear();
                    //Write it back to the client
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=UsersPoints.xlsx");
                    Response.BinaryWrite(pck.GetAsByteArray());

                    Response.End();
                }
                //GlobalFunctions.ExportToExcel(dt, "UsersPoints");
                //dt = null;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnUImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileuploadUnReg.HasFiles)
                {
                    bool status = false;
                    string ErrorFileName = "";
                    int FailedRecords = 0;
                    List<UserBE> lstUnRegisteredUsers = new List<UserBE>();
                    lstUnRegisteredUsers = UserBL.GetUnregisteredUsersDetails();
                    DataTable dtbackup = new DataTable();
                    dtbackup.Columns.Add("EmailId");
                    dtbackup.Columns.Add("Points");
                    dtbackup.Columns.Add("Flag");
                    foreach (UserBE User in lstUnRegisteredUsers)
                    {
                        DataRow drow = dtbackup.NewRow();
                        drow["EmailId"] = User.EmailId;
                        drow["Points"] = User.Points;
                        dtbackup.Rows.Add(drow);
                    }

                    string xlsFile = fileuploadUnReg.FileName;  //fileImport is the file upload control
                    string ext = System.IO.Path.GetExtension(xlsFile);
                    if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Only Excel Files are allowed for Import.", AlertType.Warning);
                        return;
                    }
                    DataTable table = new DataTable();
                    table.Columns.Add("EmailId", typeof(string));
                    table.Columns.Add("Points", typeof(Int32));
                    table.Columns.Add("Flag", typeof(Int16));


                    string strFileName = fileuploadUnReg.FileName;
                    string fileName = "UnregisteredUsersdata" + "_" + DateTime.Now.ToString("MMddyyyyHHmmss") + ".xls"; //Changes Done by snehal 22 09 2016
                    string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\UNREGISTERED\" + strFileName;
                    string strFileUploadPathbackup = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\UNREGISTERED\BACKUP\" + fileName;

                    if (File.Exists(strFileUploadPath))
                    {
                        File.Delete(strFileUploadPath);
                    }
                    string[] filePaths = Directory.GetFiles(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\UNREGISTERED\BACKUP\");
                    foreach (string filePath in filePaths)
                        File.Delete(filePath);
                    fileuploadUnReg.SaveAs(strFileUploadPath);

                    if (File.Exists(strFileUploadPath))
                    {
                        DataTable dt = new DataTable();
                        dt = GlobalFunctions.ConvertExcelToDataTable(strFileUploadPath);

                        DataRow[] rowArray = dt.Select("flag=1 or flag=2");
                        foreach (DataRow row in rowArray)
                        {
                            table.ImportRow(row);
                        }
                    }
                    ExportToExcelAndSave(dtbackup, strFileUploadPathbackup);
                    status = UserBL.UpdateUnregisteredUsersPoints(table);

                    if (status)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "User Points Imported successfully", AlertType.Success);
                        BindPointsData();
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while Importing User Points. Please try again", AlertType.Failure);
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select an Excel file to Import and try again", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        public static void ExportToExcelAndSave(DataTable dt, string path)
        {
            try
            {
                StreamWriter writer = new StreamWriter(path);
                int colCount = dt.Columns.Count;
                for (int i = 0; i < colCount; i++)
                {
                    writer.Write(dt.Columns[i]);
                    if (i < colCount - 1)
                    {
                        writer.Write("\t");
                    }
                }
                writer.Write(writer.NewLine);
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < colCount; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            writer.Write(dr[i].ToString());
                        }
                        if (i < colCount - 1)
                        {
                            writer.Write("\t");
                        }
                    }
                    writer.Write(writer.NewLine);
                }
                writer.Close();
                writer.Dispose();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public void backup()
        {
            List<UserBE> Users = UserBL.GetUnregisteredUsersDetails();
            DataTable dt = new DataTable();
            dt.Columns.Add("EmailId");
            dt.Columns.Add("Points");
            dt.Columns.Add("Flag");

            foreach (UserBE User in Users)
            {
                DataRow drow = dt.NewRow();
                drow["EmailId"] = User.EmailId;
                drow["Points"] = User.Points;
                drow["Flag"] = 0;
                dt.Rows.Add(drow);
            }
        }

        #region "From swapnil added on 02-02-2017"
        public void GetRegisteredUsersBackup()
        {
            List<UserBE> lstRegisteredUsers = new List<UserBE>();
            lstRegisteredUsers = UserBL.GetUserDetails();
            DataTable dtbackup = new DataTable();

            //dtbackup.Columns.Add("UserId");
            dtbackup.Columns.Add("EmailId");
            dtbackup.Columns.Add("FirstName");
            dtbackup.Columns.Add("MiddleName");
            dtbackup.Columns.Add("LastName");
            dtbackup.Columns.Add("Points");
            //dtbackup.Columns.Add("Flag");
            foreach (UserBE User in lstRegisteredUsers)
            {
                DataRow drow = dtbackup.NewRow();
                //drow["UserId"] = User.UserId;
                drow["EmailId"] = User.EmailId;
                drow["FirstName"] = User.FirstName;
                drow["MiddleName"] = User.MiddleName;
                drow["LastName"] = User.LastName;
                drow["Points"] = User.Points;
                dtbackup.Rows.Add(drow);
            }

            string fileName = "registeredUsersdata" + "_" + DateTime.Now.ToString("MMddyyyyHHmmss") + ".xls";

            string strFileUploadPathbackup = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\REGISTERED\BACKUP\" + fileName;

            string[] filePaths = Directory.GetFiles(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\REGISTERED\BACKUP\");
            foreach (string filePath in filePaths)
                File.Delete(filePath);

            ExportToExcelAndSave(dtbackup, strFileUploadPathbackup);
        }
        public void GetUnRegisteredUsersBackup()
        {
            List<UserBE> lstUnRegisteredUsers = new List<UserBE>();
            lstUnRegisteredUsers = UserBL.GetUnregisteredUsersDetails();
            DataTable dtbackup = new DataTable();
            dtbackup.Columns.Add("EmailId");
            dtbackup.Columns.Add("Points");
            //dtbackup.Columns.Add("Flag");
            foreach (UserBE User in lstUnRegisteredUsers)
            {
                DataRow drow = dtbackup.NewRow();
                drow["EmailId"] = User.EmailId;
                drow["Points"] = User.Points;
                dtbackup.Rows.Add(drow);
            }

            string fileName = "UnregisteredUsersdata" + "_" + DateTime.Now.ToString("MMddyyyyHHmmss") + ".xls"; //Changes Done by snehal 22 09 2016

            string strFileUploadPathbackup = GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\UNREGISTERED\BACKUP\" + fileName;

            string[] filePaths = Directory.GetFiles(GlobalFunctions.GetPhysicalFolderPath() + @"IMPORTS\POINTS\UNREGISTERED\BACKUP\");
            foreach (string filePath in filePaths)
                File.Delete(filePath);

            ExportToExcelAndSave(dtbackup, strFileUploadPathbackup);
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btnEdit = (Button)sender;
            GridViewRow grow = (GridViewRow)btnEdit.NamingContainer;
            TextBox txtPoints = (TextBox)grow.FindControl("txtPoints");
            Literal ltlEmail = (Literal)grow.FindControl("ltlEmail");
            bool status = false;
            if (btnEdit.Text == "Edit")
            {
                btnEdit.Text = "Save";
                txtPoints.Enabled = true;
            }
            else
            {
                btnEdit.Text = "Edit";
                txtPoints.Enabled = false;

                status = UserBL.UpdateRegUnregUserPointsForEdit(ltlEmail.Text, txtPoints.Text.Trim());
                if (status)
                {
                    BindPointsData();
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "User Points Updated successfully", AlertType.Success);
                    // BindPointsData();		
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while Editing User Points. Please try again", AlertType.Failure);
                }
            }
        }
        //protected void btnUnRegEdit_Click(object sender, EventArgs e)
        //{
        //    Button btnUnRegEdit = (Button)sender;
        //    GridViewRow grow = (GridViewRow)btnUnRegEdit.NamingContainer;
        //    TextBox txtUPoints = (TextBox)grow.FindControl("txtUPoints");
        //    if (btnUnRegEdit.Text == "Edit")
        //    {
        //        btnUnRegEdit.Text = "Save";
        //        txtUPoints.Enabled = true;
        //    }
        //    else
        //    {
        //        btnUnRegEdit.Text = "Edit";
        //        txtUPoints.Enabled = false;
        //    }
        //}

        #endregion
    }
    public class UserPointBE
    {
        public Int16 UserId { get; set; }
        public Int32 PreviousPoints { get; set; }
        public Int32 Points { get; set; }

    }

}
