﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_MaintainNotes_ViewNotes : System.Web.UI.Page
{

    protected global::System.Web.UI.WebControls.HiddenField hdnNoteId;
    protected global::System.Web.UI.WebControls.GridView gvList;
    protected global::System.Web.UI.WebControls.Label lblNote;
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl display;
    #region varaiables
    public Int16 PageIndex = 1;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetNoteDetails();
        }
    }

    private void GetNoteDetails()
    {
        try
        {
            List<NoteBE> lstNoteDetails = NotesBL.GetNoteDetails();
            List<NoteBE> lstNotes = new List<NoteBE>();
            if (lstNoteDetails != null)
            {
                if (lstNoteDetails.Count > 0)
                {
                    UserBE objUserBE = new UserBE();
                    objUserBE.Action = Convert.ToInt16(DBAction.Select);
                    objUserBE.UserId = 0;
                    List<UserBE> lstMCPUserDetails =  UserBL.GetMCPUsers<UserBE>(objUserBE);
                    for (int i = 0; i < lstNoteDetails.Count; i++)
                    {
                        var CreatedBy = lstMCPUserDetails.Where(b => b.UserId == Convert.ToInt32(lstNoteDetails[i].CreatedBy)).ToList();
                        lstNotes.Add(new NoteBE()
                        {
                            NoteId = lstNoteDetails[i].NoteId,
                            NoteNo = lstNoteDetails[i].NoteNo,
                            NoteDate = Convert.ToDateTime(lstNoteDetails[i].NoteDate).ToString("dd/MM/yyyy"),
                            Note = lstNoteDetails[i].Note,
                            Status = lstNoteDetails[i].Status,
                            CreatedBy = CreatedBy[0].FirstName + " " + CreatedBy[0].LastName
                        });
                    }
                }
            }
            
            if (lstNoteDetails != null && lstNoteDetails.Count > 0)
            {
                lblNote.Text = "";
                gvList.DataSource = lstNotes;
                gvList.DataBind();
            }
            else
            {
                gvList.DataSource = null;
                gvList.DataBind();
                lblNote.Text = "No notes to display.";
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvList.PageIndex = e.NewPageIndex;
        PageIndex = Convert.ToInt16(e.NewPageIndex + 1);
        GetNoteDetails();
    }
}