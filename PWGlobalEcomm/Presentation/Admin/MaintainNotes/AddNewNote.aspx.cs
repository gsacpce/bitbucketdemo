﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_MaintainNotes_AddNewNote : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.TextBox txtNoteDate, txtNote, txtNoteNo;
        protected global::System.Web.UI.WebControls.HiddenField hdnNoteNo;

        Int16 intUserId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    GetNoteNo();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                #region Datevalidation Not Required
                //if (txtNoteDate.Text == "")
                //{
                //    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select note date.", AlertType.Warning);
                //    txtNoteDate.Focus();
                //    return;
                //}
                //else
                //{
                //    DateTime dateTime;
                //    if (!DateTime.TryParse(txtNoteDate.Text, out dateTime))
                //    {
                //        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please enter a valid date.", AlertType.Warning);
                //        txtNoteDate.Text = "";
                //        txtNoteDate.Focus();
                //        return;
                //    }
                //}
                #endregion
                if (txtNote.Text == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please enter the note", AlertType.Warning);
                    txtNote.Focus();
                    return;
                }

                UserBE objUserBE = new UserBE();
                objUserBE = Session["StoreUser"] as UserBE;
                intUserId = objUserBE.UserId;

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("NoteId", hdnNoteNo.Value);
                DictionaryInstance.Add("NoteNo", hdnNoteNo.Value);
                DictionaryInstance.Add("Note", txtNote.Text.Trim());
                DictionaryInstance.Add("Status", "1");
                DictionaryInstance.Add("CreatedBy", Convert.ToString(intUserId));
                DictionaryInstance.Add("CreatedDate", DateTime.Now.ToString("MM/dd/yyyy"));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(intUserId));
                DictionaryInstance.Add("ModifiedDate", DateTime.Now.ToString("MM/dd/yyyy"));
                DictionaryInstance.Add("InsUpdFlag", "I");
                bool IsSaved = NotesBL.InsertUpdateNotes(DictionaryInstance, true);

                if (IsSaved)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Note saved successfully", AlertType.Success);
                    CreateActivityLog("AddNewNote", "Insert", Convert.ToString(hdnNoteNo));
                    ClearFields();
                    GetNoteNo();
                    return;
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while saving note", AlertType.Failure);
                    return;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void ClearFields()
        {
            try
            {
                txtNoteNo.Text = "";
                txtNoteDate.Text = "";
                txtNote.Text = "";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GetNoteNo()
        {
            try
            {
                string strNoteNo = NotesBL.GetNoteNo();
                txtNoteNo.Text = strNoteNo;
                hdnNoteNo.Value = strNoteNo;
                txtNoteNo.Enabled = false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            try
            {
                ClearFields();
                GetNoteNo();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}