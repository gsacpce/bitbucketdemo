﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI;

namespace Presentation
{
    public class Admin_SliderListing : System.Web.UI.Page
    {
        
        
        protected global::System.Web.UI.WebControls.Button btnAddNew;
        protected global::System.Web.UI.WebControls.GridView gvSlideShow;

        protected global::System.Web.UI.WebControls.LinkButton lnkbtnUpdate;
        protected global::System.Web.UI.WebControls.LinkButton lnkbtnDelete;

        public string Host = GlobalFunctions.GetVirtualPath();
        List<ImageSlideShowBE> lstSlideShows = new List<ImageSlideShowBE>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAllSlideShows();
            }
            
        }

        private void GetAllSlideShows()
        {
            lstSlideShows = ImageSlideShowBL.GetAllImageSliders();
            Session["SlideShows"] = lstSlideShows;
            gvSlideShow.DataSource = lstSlideShows;
            gvSlideShow.DataBind();
        }

        protected void Btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            
            LinkButton lnkBtn    = sender as LinkButton;

            if (btn != null)
            switch (btn.CommandName.ToLower() )
            {
                case "add" :
                    {
                        Session["EditSlideShow"] = null;
                        Response.Redirect("SliderManagement.aspx");
                        break;
                    }
                case "cancel":
                    {
                        break;
                    }
                default:
                    break;
            }
            if (lnkBtn != null)
            switch (lnkBtn.CommandName.ToLower() )
            {
                case "update":
                    {
                        ImageSlideShowBE EditSlideShow = new ImageSlideShowBE();
                        
                        //(Session["SlideShows"] as List<object>).Find(x => (x as ImageSlideShowBE).SlideShowId == Convert.ToInt16(lnkBtn.CommandArgument));
                        if (Session["SlideShows"] != null)
                        {
                            foreach (var item in (Session["SlideShows"] as List<ImageSlideShowBE>))
                            {
                                if (((ImageSlideShowBE)item).SlideShowId == Convert.ToInt16(lnkBtn.CommandArgument))
                                {
                                    EditSlideShow = ((ImageSlideShowBE)item);
                                }
                            }
                            Session["EditSlideShow"] = EditSlideShow;
                            Session["EditOperation"] = lnkBtn.CommandName;
                            Response.Redirect("SliderManagement.aspx");
                        }
                        
                        break;
                    }
                case "delete":
                    {
                        ImageSlideShowBE EditSlideShow = new ImageSlideShowBE();
                        foreach (var item in (Session["SlideShows"] as List<ImageSlideShowBE>))
                        {
                            if (((ImageSlideShowBE)item).SlideShowId == Convert.ToInt16(lnkBtn.CommandArgument))
                            {
                                EditSlideShow = ((ImageSlideShowBE)item);
                            }
                        }
                        Session["EditSlideShow"] = EditSlideShow;
                        Session["EditOperation"] = lnkBtn.CommandName;

                        if (Session["EditSlideShow"] != null && Session["EditOperation"].ToString().Equals("Delete"))
                        {
                            ImageSlideShowBE objImageSlideShow = Session["EditSlideShow"] as ImageSlideShowBE;
                            if (ImageSlideShowBL.ManageSlideShow_SAED(objImageSlideShow, DBAction.Delete) > 0)
                            {
                                GlobalFunctions.ShowGeneralAlertMessages(this.Page, "Slideshow details saved successfully!");
                                GlobalFunctions.ShowGeneralAlertMessages(this.Page, "Slideshow details updated successfully!", "SliderListing.aspx");
                            }

                        }
                        
                        break;
                    }
                case "manage slides":
                    {
                        ImageSlideShowBE EditSlideShow = new ImageSlideShowBE();
                        if (Session["SlideShows"] != null)
                        {

                            foreach (var item in (Session["SlideShows"] as List<ImageSlideShowBE>))
                            {
                                if (((ImageSlideShowBE)item).SlideShowId == Convert.ToInt16(lnkBtn.CommandArgument))
                                {
                                    EditSlideShow = ((ImageSlideShowBE)item);
                                }
                            }
                            Session["EditSlideShow"] = EditSlideShow;
                            Response.Redirect("SlideShowImages.aspx?Id=" + Convert.ToInt16(lnkBtn.CommandArgument));
                        }
                        break;
                    }
                case "cancel":
                    {
                        break;
                    }
                default:
                    break;
            }
        }

        
        protected void gvSlideShow_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
            int NewIndex = e.NewPageIndex;
            gvSlideShow.PageIndex = NewIndex;
            gvSlideShow.DataSource = ImageSlideShowBL.GetAllImageSliders();
            gvSlideShow.DataBind();
        }
        protected void gvSlideShow_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Response.Write("Delete handled");
        }
        protected void gvSlideShow_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
                bool IsActive = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsActive"));
                if (IsActive)
                {
                    lnkbtnDelete.Visible = true;
                }
                else
                {
                    lnkbtnDelete.Visible = false;
                }
            }
        }
       
    }
}
