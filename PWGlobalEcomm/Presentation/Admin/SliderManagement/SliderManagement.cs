﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;


namespace Presentation
{
    public class Admin_SliderManagement : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.DropDownList ddlSlideShowMaster, ddlFontStyle, ddlOverlayTextFontSize, ddlOverlayTextPosition;
        //    ddlWidthUnit;
        protected global::System.Web.UI.WebControls.TextBox txtSlideShowName;
        protected global::System.Web.UI.WebControls.TextBox txtSlideDelayTime;
        protected global::System.Web.UI.WebControls.TextBox txtSlideEffectTime;
        protected global::System.Web.UI.WebControls.TextBox txtForeColor;
        protected global::System.Web.UI.WebControls.TextBox txtBgColor, txtHeight, txtOverlayTextHeight, txtOverlayTextForeColor, txtOverlayTextBGColor;
        protected global::System.Web.UI.WebControls.CheckBox chkIsActive, chkIsOverLayText, chkShowPagination;
        //protected global::System.Web.UI.HtmlControls.HtmlGenericControl txtWidthError;
        protected global::System.Web.UI.WebControls.Panel pnlNerveHeight, pnlOverlayTextSettings;

        protected global::System.Web.UI.WebControls.Button btnSave;
        protected global::System.Web.UI.WebControls.Button btnCancel;

        protected string AdminHost = GlobalFunctions.GetVirtualPathAdmin();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                for (int i = 1; i <= 100; i++)
                {
                    ddlOverlayTextFontSize.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }

                PopulateSlideShowMaster();
                PopulateFontStyle();

                if (Session["EditSlideShow"] != null)
                {
                    ImageSlideShowBE EditSlideShow = Session["EditSlideShow"] as ImageSlideShowBE;
                    ddlSlideShowMaster.SelectedValue = EditSlideShow.SlideShowMasterId.ToString();
                    txtSlideShowName.Text = EditSlideShow.SlideShowName;
                    txtSlideDelayTime.Text = EditSlideShow.SlideDelayTime.ToString();
                    txtSlideEffectTime.Text = EditSlideShow.SlideEffectTime.ToString();
                    //chkIsActive.Checked = EditSlideShow.IsActive;
                    chkIsOverLayText.Checked = EditSlideShow.IsOverLayText;
                    chkShowPagination.Checked = EditSlideShow.ShowPagination;
                    /*Nerve slider height attribute*/
                    if (EditSlideShow.SlideShowMasterId.Equals(1))
                    {
                        pnlNerveHeight.Visible = true;
                        txtHeight.Text = EditSlideShow.Height != 0 ? EditSlideShow.Height.ToString() : "";
                    }

                    #region MyRegion CODE ADDED BY SHRIGANESH TO DISPLAY OVERLAY CHECKBOX ON DROPDOWN SELECTION 29 JAN 2016
                    /*OWL slider Setting*/
                    if (EditSlideShow.SlideShowMasterId.Equals(2))
                    {
                        pnlOverlayTextSettings.Visible = true;
                        ddlOverlayTextFontSize.SelectedValue = EditSlideShow.OverlayFontSize.ToString();
                        txtOverlayTextHeight.Text = EditSlideShow.OverlayHeight.ToString();
                        txtOverlayTextForeColor.Text = EditSlideShow.OverlayForeColor.ToString();
                        txtOverlayTextBGColor.Text = EditSlideShow.OverlayBGColor.ToString();
                        ddlOverlayTextPosition.SelectedItem.Text = EditSlideShow.OverlayPosition;
                    } 
                    #endregion
                }
            }
        }

        private void PopulateSlideShowMaster()
        {
            List<object> lstAllSlideShowMaster = ImageSlideShowBL.GetAllSlideShowMaster();

            ddlSlideShowMaster.Items.Add(new ListItem("-- Select --", "0"));

            ddlSlideShowMaster.AppendDataBoundItems = true;

            ddlSlideShowMaster.DataTextField = "SlideShowMasterName";
            ddlSlideShowMaster.DataValueField = "SlideShowMasterId";
            ddlSlideShowMaster.DataSource = lstAllSlideShowMaster;
            ddlSlideShowMaster.DataBind();
        }

        protected void Btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            switch (btn.CommandArgument)
            {
                case "Save":
                    {
                        SaveSlideShow();
                        CreateActivityLog("SliderManagement Btn_Click", "Update", "");
                        break;
                    }
                case "Cancel":
                    {
                        Response.Redirect(AdminHost + "SliderManagement/SliderListing.aspx");
                        break;
                    }
                default:
                    break;
            }
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 06-08-15
        /// Scope   : Populate Dropdown list
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void PopulateFontStyle()
        {
            try
            {
                List<ImageSlideShowBE> Fonts = new List<ImageSlideShowBE>();
                var FontListObj = ImageSlideShowBL.GetAllFonts();

                if (FontListObj != null)
                {
                    if (FontListObj.Count() > 0)
                    {
                        foreach (object FontOBj in FontListObj)
                        {
                            ImageSlideShowBE Font = FontOBj as ImageSlideShowBE;
                            Fonts.Add(Font);

                        }
                    }
                }

                ddlFontStyle.DataSource = Fonts;
                ddlFontStyle.DataTextField = "FontName";
                ddlFontStyle.DataValueField = "FontId";
                Fonts = null;
                ddlFontStyle.DataBind();
                ddlFontStyle.Items.Insert(0, new ListItem() { Text = "--Select--", Value = "0" });
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 06-08-15
        /// Scope   : On Data bound add style font family
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void ddlFontStyle_DataBound(object sender, EventArgs e)
        {
            foreach (ListItem myItem in ddlFontStyle.Items)
            {
                myItem.Attributes.Add("style", "font-family:" + myItem.Text);
            }
        }
        private bool SaveSlideShow()
        {
            bool flgSuccess = true;

            try
            {
                if (ddlSlideShowMaster.SelectedValue.Trim() == "0")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select slide show type !!", AlertType.Warning);
                    return false;
                }
                if (txtSlideShowName.Text.Trim() == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Slide show name can not be empty !!", AlertType.Warning);
                    return false;
                }
                if (txtSlideDelayTime.Text.Trim() == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Slide delay time can not be empty !!", AlertType.Warning);
                    return false;
                }
                if (txtSlideEffectTime.Text.Trim() == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Slide effect can not be empty !!", AlertType.Warning);
                    return false;
                }

                if (ddlSlideShowMaster.SelectedItem.Text.ToLower().Contains("nerve"))
                {
                    if (txtHeight.Text.Trim() == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Nerve Slider Images Height can not be empty !!", AlertType.Warning);
                        return false;
                    }
                    bool isInt;
                    Int32 height;
                    isInt = Int32.TryParse(txtHeight.Text, out height);
                    if (!isInt)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Nerve Slider Images Height can be numbers only !!", AlertType.Warning);
                        return false;
                    }
                }

                #region MyRegion CODE ADDED BY SHRIGANESH TO VALIDATE OVERLAY SETTINGS 29 JAN 2016
                if (ddlSlideShowMaster.SelectedItem.Text.ToLower().Contains("owl"))
                {
                    if (chkIsOverLayText.Checked)
                    {
                        if (ddlOverlayTextFontSize.SelectedValue.Trim() == "0")
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select font size !!", AlertType.Warning);
                            return false;
                        }
                        if (txtOverlayTextHeight.Text.Trim() == "")
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Overlay Height cannot be empty !!", AlertType.Warning);
                            return false;
                        }
                        if (txtOverlayTextForeColor.Text.Trim() == "")
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Overlay Fore Color cannot be empty !!", AlertType.Warning);
                            return false;
                        }
                        if (txtOverlayTextBGColor.Text.Trim() == "")
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Overlay Background Color cannot be empty !!", AlertType.Warning);
                            return false;
                        }
                        if (ddlOverlayTextPosition.SelectedValue.Trim() == "0")
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select Overlay Position !!", AlertType.Warning);
                            return false;
                        }
                    }                   
                } 
                #endregion

                //if (chkIsOverLayText.Checked)
                //{
                //    if (ddlFontStyle.SelectedValue == "0")
                //    {
                //        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select font style !!", AlertType.Warning);
                //        return false;
                //    }
                //    if (txtForeColor.Text.Trim() == "")
                //    {
                //        GlobalFunctions.ShowModalAlertMessages(this.Page, "Fore color can not be empty !!", AlertType.Warning);
                //        return false;
                //    }
                //    if (txtBgColor.Text.Trim() == "")
                //    {
                //        GlobalFunctions.ShowModalAlertMessages(this.Page, "Background color can not be empty !!", AlertType.Warning);
                //        return false;
                //    }

                //}
                if (Page.IsValid)
                {
                    ImageSlideShowBE objImageSlideShow = new ImageSlideShowBE();
                    objImageSlideShow.SlideShowMasterId = Convert.ToInt16(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(ddlSlideShowMaster.SelectedValue.Trim().Replace('\'', '@')));
                    objImageSlideShow.SlideDelayTime = Convert.ToInt16(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSlideDelayTime.Text.Trim().Replace('\'', '@')));
                    objImageSlideShow.SlideEffectTime = Convert.ToInt16(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSlideEffectTime.Text.Trim().Replace('\'', '@')));
                    objImageSlideShow.IsActive = true;
                    objImageSlideShow.IsOverLayText = chkIsOverLayText.Checked;
                    objImageSlideShow.ShowPagination = chkShowPagination.Checked;
                    objImageSlideShow.TextForeColor = "";
                    objImageSlideShow.TextBgColor = "";

                    if (ddlSlideShowMaster.SelectedItem.Text.ToLower().Contains("nerve"))
                        objImageSlideShow.Height = txtHeight.Text.To_Int16();

                    //    objImageSlideShow.Width = Convert.ToInt16(txtWidth.Text);
                    // objImageSlideShow.WidthUnit = ddlWidthUnit.SelectedValue == "1" ? "%" : "px"; ;

                    if (objImageSlideShow.IsOverLayText)
                    {
                        //objImageSlideShow.FontId = Convert.ToInt16(ddlFontStyle.SelectedValue);
                        //objImageSlideShow.TextForeColor = txtForeColor.Text.Trim();
                        //objImageSlideShow.TextBgColor = txtBgColor.Text.Trim();

                        #region MyRegion CODE ADDED BY SHRIGANESH FOR OVERLAY DETAILED SETTING 29 JAN 2016
                        objImageSlideShow.OverlayFontSize = Convert.ToInt16(ddlOverlayTextFontSize.SelectedValue);
                        objImageSlideShow.OverlayHeight = txtOverlayTextHeight.Text.Trim();
                        objImageSlideShow.OverlayForeColor = txtOverlayTextForeColor.Text.Trim();
                        objImageSlideShow.OverlayBGColor = txtOverlayTextBGColor.Text.Trim();
                        objImageSlideShow.OverlayPosition = ddlOverlayTextPosition.SelectedItem.Text.Trim(); 
                        #endregion
                    }

                    if (Session["EditSlideShow"] != null && Session["EditOperation"].ToString().Equals("Update"))
                    {
                        objImageSlideShow.SlideShowName = (Session["EditSlideShow"] as ImageSlideShowBE).SlideShowName;
                        objImageSlideShow.SlideShowId = (Session["EditSlideShow"] as ImageSlideShowBE).SlideShowId;
                        if (objImageSlideShow.SlideShowName == txtSlideShowName.Text.Trim())
                        {
                            if (ImageSlideShowBL.ManageSlideShow_SAED(objImageSlideShow, DBAction.Update) > 0)
                            {
                                //txtWidthError.Attributes["display"] = "display: none";
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Slideshow details updated successfully!", "SliderListing.aspx", AlertType.Success);

                                ClearControls();
                            }
                        }
                        else
                        {
                            objImageSlideShow.SlideShowName = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSlideShowName.Text.Trim().Replace('\'', '@'));
                            if (!CheckSlideShowNameExist(objImageSlideShow.SlideShowName))
                            {

                                if (ImageSlideShowBL.ManageSlideShow_SAED(objImageSlideShow, DBAction.Update) > 0)
                                {
                                    // txtWidthError.Attributes["display"] = "display: none";
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Slideshow details updated successfully!", "SliderListing.aspx", AlertType.Success);
                                    ClearControls();
                                }
                                else
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error updating Slideshow details!", AlertType.Failure);
                                }
                            }
                            else
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Slide Show Name Already Taken", AlertType.Warning);
                            }
                        }
                    }

                    else
                    {
                        objImageSlideShow.SlideShowName = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSlideShowName.Text.Trim().Replace('\'', '@'));
                        if (!CheckSlideShowNameExist(objImageSlideShow.SlideShowName))
                        {

                            if (ImageSlideShowBL.ManageSlideShow_SAED(objImageSlideShow, DBAction.Insert) > 0)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Slideshow details saved successfully!", "SliderListing.aspx", AlertType.Success);
                                ClearControls();
                            }
                            else
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error saving Slideshow details!", AlertType.Failure);
                            }
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Slide Show Name Already Taken", AlertType.Warning);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flgSuccess = false;
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            finally
            {

            }
        Exit:
            return flgSuccess;
        }

        private void ClearControls()
        {
            ddlSlideShowMaster.SelectedValue = "0";
            //ddlFontStyle.SelectedValue = "0";
            txtSlideShowName.Text = "";
            txtSlideDelayTime.Text = "";
            txtSlideEffectTime.Text = "";
            //chkIsActive.Checked = true;
            chkIsOverLayText.Checked = true;
        }


        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 19-08-15
        /// Scope   : to check whether the slide show name exist in the db or not
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        private bool CheckSlideShowNameExist(string SlideShowName)
        {
            var ObjList = ImageSlideShowBL.GetAllImageSliders();
            List<ImageSlideShowBE> SlideShows = new List<ImageSlideShowBE>();
            if (ObjList != null)
            {
                foreach (var item in ObjList)
                {
                    SlideShows.Add((ImageSlideShowBE)item);
                }
            }

            if (SlideShows != null)
            {
                if (SlideShows.Count() > 0)
                {
                    int count = SlideShows.Count(x => x.SlideShowName == SlideShowName);
                    return count > 0 ? true : false;
                }
            }
            return false;
        }

        protected void ddlSlideShowMaster_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSlideShowMaster.SelectedItem.Text.ToLower().Contains("nerve"))
            {
                pnlNerveHeight.Visible = true;
            }
            else
            {
                pnlNerveHeight.Visible = false;
            }

            #region MyRegion CODE ADDED BY SHRIGANESH TO DISPLAY/HIDE OVERLAY SETTINGS 29 JAN 2016
            if (ddlSlideShowMaster.SelectedItem.Text.ToLower().Contains("owl"))
            {
                pnlOverlayTextSettings.Visible = true;

            }
            else
            {
                pnlOverlayTextSettings.Visible = false;
                chkIsOverLayText.Checked = false;
            } 
            #endregion
        }
    }
}

