﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using Microsoft.Security.Application;

namespace Presentation
{
    public class Admin_SliderManagement_SliderListing_ajax : System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Repeater rptSliderListing, rptSectionListing, rptStaticContent;
        public string strItemType, strPlaceholderId = string.Empty;
        List<ImageSlideShowBE> lstSlideShows = new List<ImageSlideShowBE>();
        Int16 intLanguageId = 1;
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 18-08-15
        /// Scope   : Page_Load event
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();

                strItemType = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["itemtype"]));
                strPlaceholderId = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["placeholder"]));

                if (strItemType.ToLower() == "sections")
                {
                    List<SectionBE> lstSBE = new List<SectionBE>();
                    SectionBE objBE = new SectionBE();
                    objBE.LanguageId = Convert.ToInt16(intLanguageId);
                    lstSBE = SectionBL.GetAllDetails(Constants.USP_GetSectionDetails, objBE, "A");
                    if (lstSBE != null)
                    {
                        if (lstSBE.Count > 0)
                        {
                            rptSectionListing.DataSource = lstSBE;
                            rptSectionListing.DataBind();
                        }
                        else
                        {
                            Response.Clear();
                            Response.Write("No records");
                        }
                    }
                    else
                    {
                        Response.Clear();
                        Response.Write("No records");
                    }
                }
                else if (strItemType.ToLower() == "slideshow")
                {
                    lstSlideShows = ImageSlideShowBL.GetAllImageSliders().ToList();
                    if (lstSlideShows != null && lstSlideShows.Count > 0)
                    {
                        List<ImageSlideShowBE> lstActiveSlideShows = new List<ImageSlideShowBE>();
                        lstActiveSlideShows = lstSlideShows.Cast<ImageSlideShowBE>().ToList();
                        lstActiveSlideShows = lstActiveSlideShows.FindAll(x => x.IsActive == true);
                        if (lstActiveSlideShows != null && lstActiveSlideShows.Count > 0)
                        {
                            rptSliderListing.DataSource = lstActiveSlideShows;
                            rptSliderListing.DataBind();
                        }
                        else
                        {
                            Response.Clear();
                            Response.Write("No records");
                        }
                    }
                    else
                    {
                        Response.Clear();
                        Response.Write("No records");
                    }
                }
                else
                {
                    List<HomePageManagementBE> lstHomePageBE = new List<HomePageManagementBE>();
                    lstHomePageBE = HomePageManagementBL.GetAllHomePageContentDetails();
                    
                    if (lstHomePageBE != null && lstHomePageBE.Count > 0)
                    {
                        lstHomePageBE = lstHomePageBE.FindAll(x => x.LanguageId == intLanguageId);
                        if (lstHomePageBE != null && lstHomePageBE.Count > 0)
                        {
                            lstHomePageBE = lstHomePageBE.FindAll(x => x.ContentType.ToLower() == strItemType.ToLower());
                            if (lstHomePageBE != null && lstHomePageBE.Count > 0)
                            {
                                rptStaticContent.DataSource = lstHomePageBE;
                                rptStaticContent.DataBind();
                            }
                            else
                            {
                                Response.Clear();
                                Response.Write("No records");
                            }
                        }
                        else
                        {
                            Response.Clear();
                            Response.Write("No records");
                        }
                    }
                    else
                    {
                        Response.Clear();
                        Response.Write("No records");
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}