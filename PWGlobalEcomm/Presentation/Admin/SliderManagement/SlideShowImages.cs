﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using System.IO;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;
using System.Web;



namespace Presentation
{
    public class Admin_SlideShowImages : System.Web.UI.Page
    {
        public global::System.Web.UI.HtmlControls.HtmlInputHidden hdnLinkText1, hdnLinkText2, hdnLinkText3, hdnLinkText4, hdnLinkText5, hdnLinkText6, hdnLinkText7, hdnLinkText8, hdnLinkText9, hdnLinkText10;
        public global::System.Web.UI.HtmlControls.HtmlInputHidden hdnImageAltText1, hdnImageAltText2, hdnImageAltText3, hdnImageAltText4, hdnImageAltText5, hdnImageAltText6, hdnImageAltText7, hdnImageAltText8, hdnImageAltText9, hdnImageAltText10;
        public global::System.Web.UI.HtmlControls.HtmlInputHidden hdnIsNewWindow1, hdnIsNewWindow2, hdnIsNewWindow3, hdnIsNewWindow4, hdnIsNewWindow5, hdnIsNewWindow6, hdnIsNewWindow7, hdnIsNewWindow8, hdnIsNewWindow9, hdnIsNewWindow10;
        public global::System.Web.UI.HtmlControls.HtmlInputHidden hdnIsActive1, hdnIsActive2, hdnIsActive3, hdnIsActive4, hdnIsActive5, hdnIsActive6, hdnIsActive7, hdnIsActive8, hdnIsActive9, hdnIsActive10;
        public global::System.Web.UI.WebControls.Image id1, id2, id3, id4, id5, id6, id7, id8, id9, id10;
        public global::System.Web.UI.WebControls.Literal ltlSlideShowName1, ltlSlideShowName2, ltlSlideShowName3, ltlSlideShowName4, ltlSlideShowName5, ltlSlideShowName6, ltlSlideShowName7, ltlSlideShowName8, ltlSlideShowName9, ltlSlideShowName10;
        public global::System.Web.UI.HtmlControls.HtmlInputHidden hdnSlideImageLoad, hdnTxtWebLink, hdnTxtAltText, hdnIsNewWindow, hdnSaveData, hdnOverLayText, hdnSlideShowImageId;
        public global::System.Web.UI.HtmlControls.HtmlInputHidden hdnImageOverLayText1, hdnImageOverLayText2, hdnImageOverLayText3, hdnImageOverLayText4, hdnImageOverLayText5, hdnImageOverLayText6, hdnImageOverLayText7, hdnImageOverLayText8, hdnImageOverLayText9, hdnImageOverLayText10;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        protected global::System.Web.UI.HtmlControls.HtmlContainerControl divMessage, divLang, divOverLay;
        protected global::System.Web.UI.HtmlControls.HtmlButton btnEdit;
        protected global::System.Web.UI.WebControls.Literal ltrMessage;
        public global::System.Web.UI.WebControls.TextBox txtEditOverLayText;
        public string SlideShowImageSize = GlobalFunctions.GetSetting("IMAGESIZE_SLIDESHOW");
        public string SlideShowExtension = GlobalFunctions.GetSetting("IMAGEEXTENSION_SLIDESHOW");

        public string host = GlobalFunctions.GetVirtualPath();
        public string SlideShowName = "";
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnSlideShowSrc, hdnActiveSlideWeb, hdnSlideShowSrc1;
        protected global::System.Web.UI.WebControls.GridView GvSlideShowOverLayText;

        

        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!IsPostBack)
            {
                if (Request.QueryString["Id"] != null)
                {
                    Session["CurrSlideId"] = Convert.ToInt16(Request.QueryString["Id"].ToString());
                }
                
                
                PopulateLanguageDropDownList();
                //hdnSlideShowImageId.Value = TempSlideShowImageId;


                if (Request.QueryString.HasKeys())
                {
                    if (Request.QueryString["MethodName"] != null)
                    {
                        if (Request.QueryString["MethodName"] == "Insert")
                        {
                            InsertSlideShowImageDetails();


                        }
                    }
                    else
                    {
                        PopulateSlideShow();
                        GetPageLoadData();
                    }

                }
                else
                {
                    PopulateSlideShow();
                    GetPageLoadData();
                }



            }
        }
        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : to get all slide show images details and populate it
        /// </summary>
        private void PopulateSlideShow()
        {
            try
            {
                List<ImageSlideShowBE.SlideShowImage> SlideImages = new List<ImageSlideShowBE.SlideShowImage>();
                Int16 SlideId = 0;

                if (Request.QueryString.HasKeys())
                {
                    if (Request.QueryString["Id"] != null)
                    {
                        SlideId = Convert.ToInt16(Request.QueryString["Id"].ToString());
                        
                    }
                }
                
                if (SlideId > 0)
                {
                    bool IsOverLayText = false;


                    var lstSlideShowsObject = ImageSlideShowBL.GetAllImageSliders();
                    List<ImageSlideShowBE> lstSlideShows = new List<ImageSlideShowBE>();

                    foreach (var item in lstSlideShowsObject)
                    {
                        lstSlideShows.Add((ImageSlideShowBE)item);
                    }
                    IsOverLayText = lstSlideShows.Where(s => s.SlideShowId == SlideId ).Select(s => s.IsOverLayText).FirstOrDefault();
                    SlideShowName = lstSlideShows.Where(s => s.SlideShowId == SlideId).Select(s => s.SlideShowName).FirstOrDefault();
                    if (IsOverLayText == true)
                    {
                        divOverLay.Attributes["style"] = "display:block";
                    }
                    else
                    {
                        divOverLay.Attributes["style"] = "display:none";

                    }
                    var SlideImagesObj = ImageSlideShowBL.GetAllSliderImagesDetails(SlideId);
                    if (SlideImagesObj != null)
                    {
                        if (SlideImagesObj.Count() > 0)
                        {
                            foreach (var SlideImageObj in SlideImagesObj)
                            {
                                ImageSlideShowBE.SlideShowImage SlideImage = (ImageSlideShowBE.SlideShowImage)SlideImageObj;
                                SlideImages.Add(SlideImage);
                            }

                            if (SlideImages != null)
                            {
                                HtmlInputHidden[] lstHdnLinkText = new HtmlInputHidden[] { hdnLinkText1, hdnLinkText2, hdnLinkText3, hdnLinkText4, hdnLinkText5, hdnLinkText6, hdnLinkText7, hdnLinkText8, hdnLinkText9, hdnLinkText10 };
                                HtmlInputHidden[] lstHdnImageAltText = new HtmlInputHidden[] { hdnImageAltText1, hdnImageAltText2, hdnImageAltText3, hdnImageAltText4, hdnImageAltText5, hdnImageAltText6, hdnImageAltText7, hdnImageAltText8, hdnImageAltText9, hdnImageAltText10 };
                                HtmlInputHidden[] lstHdnIsNewWindow = new HtmlInputHidden[] { hdnIsNewWindow1, hdnIsNewWindow2, hdnIsNewWindow3, hdnIsNewWindow4, hdnIsNewWindow5, hdnIsNewWindow6, hdnIsNewWindow7, hdnIsNewWindow8, hdnIsNewWindow9, hdnIsNewWindow10 };
                                HtmlInputHidden[] lstHdnIsActive = new HtmlInputHidden[] { hdnIsActive1, hdnIsActive2, hdnIsActive3, hdnIsActive4, hdnIsActive5, hdnIsActive6, hdnIsActive7, hdnIsActive8, hdnIsActive9, hdnIsActive10 };
                                HtmlInputHidden[] lsthdnImageOverLayText = new HtmlInputHidden[] { hdnImageOverLayText1, hdnImageOverLayText2, hdnImageOverLayText3, hdnImageOverLayText4, hdnImageOverLayText5, hdnImageOverLayText6, hdnImageOverLayText7, hdnImageOverLayText8, hdnImageOverLayText9, hdnImageOverLayText10 };

                                Image[] lstImages = new Image[] { id1, id2, id3, id4, id5, id6, id7, id8, id9, id10 };
                                Literal[] lstLtlSlideShowNames = new Literal[] { ltlSlideShowName1, ltlSlideShowName2, ltlSlideShowName3, ltlSlideShowName4, ltlSlideShowName5, ltlSlideShowName6, ltlSlideShowName7, ltlSlideShowName8, ltlSlideShowName9, ltlSlideShowName10 };

                                for (int i = 0; i < SlideImages.Count(); i++)
                                {
                                    lstHdnLinkText[i].Value = SlideImages[i].NavigateUrl.Trim();
                                    lsthdnImageOverLayText[i].Value = SlideImages[i].OverLayText.Trim();
                                    lstHdnImageAltText[i].Value = SlideImages[i].AltText.Trim();
                                    lstHdnIsNewWindow[i].Value = Convert.ToString(SlideImages[i].IsNewWindow);
                                    lstHdnIsActive[i].Value = Convert.ToString(SlideImages[i].IsActive);
                                    lstImages[i].ImageUrl = GlobalFunctions.GetVirtualPath() + @"Images/SlideShow/" + SlideImages[i].SlideShowImageId + "_Thumb" + SlideImages[i].ImageExtension;
                                    lstImages[i].Attributes["slideshowimageid"] = Convert.ToString(SlideImages[i].SlideShowImageId);
                                    lstImages[i].Attributes["rel"] = Convert.ToString(SlideImages[i].DisplayOrder);
                                    lstImages[i].Attributes["filename"] = Convert.ToString(SlideImages[i].ImageExtension);
                                    lstImages[i].Attributes["thumbname"] = Convert.ToString(SlideImages[i].SlideShowImageId + SlideImages[i].ImageExtension);
                                  

                                }
                                btnEdit.Attributes["style"] = "display:block";
                                


                            }
                        }
                        else
                        {
                            Response.Redirect(host + "Admin/SliderManagement/SliderListing.aspx");
                        }


                    }
                }
            }




            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btn_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// To set the image name
        /// </summary>
        /// <param name="FileExt"></param>
        /// <param name="type"></param>
        /// <param name="CategoryId"></param>
        private void SetImageName(string FileExt, string OldName, int SlideShowId)
        {
            if (FileExt != "")
            {
                //Rename logo file with category id
                try
                {

                    if (File.Exists(Server.MapPath("~/Images/SlideShow/") + SlideShowId + FileExt))
                    {
                    }
                    else if (File.Exists(Server.MapPath("~/Images/SlideShow/Temp/") + OldName))
                    {
                        //File.Copy(Server.MapPath("~/Images/SlideShow/Temp/") + OldName , Server.MapPath("~/Images/SlideShow/") + SlideShowId + FileExt);
                        File.Move(Server.MapPath("~/Images/SlideShow/Temp/") + OldName, Server.MapPath("~/Images/SlideShow/") + SlideShowId + FileExt);
                    }
                    //try
                    //{
                    //    File.Delete(Server.MapPath("~/Images/SlideShow/Temp/") + OldName);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Exceptions.WriteExceptionLog(ex);
                    //}


                    OldName = OldName.Replace(FileExt, "");
                    OldName = OldName + "_Thumb" + FileExt;

                    if (File.Exists(Server.MapPath("~/Images/SlideShow/") + SlideShowId + "_Thumb" + FileExt))
                    {
                    }
                    else if (File.Exists(Server.MapPath("~/Images/SlideShow/Temp/") + OldName))
                    {
                        //File.Copy(Server.MapPath("~/Images/SlideShow/Temp/") + OldName, Server.MapPath("~/Images/SlideShow/") + SlideShowId + "_Thumb" + FileExt);
                        File.Move(Server.MapPath("~/Images/SlideShow/Temp/") + OldName, Server.MapPath("~/Images/SlideShow/") + SlideShowId + "_Thumb" + FileExt);
                    }
                    //try
                    //{
                    //    File.Delete(Server.MapPath("~/Images/SlideShow/Temp/") + OldName);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Exceptions.WriteExceptionLog(ex);
                    //}

                }
                catch (Exception ex1)
                {
                    Exceptions.WriteExceptionLog(ex1);

                }
            }

        }



        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : to get page load data
        /// </summary>
        public void GetPageLoadData()
        {
            try
            {
                Int16 SlideId = 0;
                ImageSlideShowBE.SlideShowImage SlideShowImage = new ImageSlideShowBE.SlideShowImage();
                if (Request.QueryString.HasKeys())
                {
                    if (Request.QueryString["Id"] != null)
                    {
                        SlideId = Convert.ToInt16(Request.QueryString["Id"].ToString());
                    }
                }
                if (SlideId > 0)
                {
                    List<object> SlideImagesObj = ImageSlideShowBL.GetAllSliderImagesDetails(SlideId);
                    if (SlideImagesObj != null)
                    {

                        SlideShowImage = (ImageSlideShowBE.SlideShowImage)SlideImagesObj.FirstOrDefault();
                        hdnSlideImageLoad.Value = host + "images/slideshow/" + SlideShowImage.SlideShowImageId + SlideShowImage.ImageExtension.Trim();
                        hdnTxtWebLink.Value = SlideShowImage.NavigateUrl.Trim();
                        hdnTxtAltText.Value = SlideShowImage.AltText.Trim();
                        hdnOverLayText.Value = SlideShowImage.OverLayText.Trim();
                        hdnIsNewWindow.Value = SlideShowImage.IsNewWindow.ToString();
                        //ddlLanguage.Attributes.Add("sildeshowimageid",Convert.ToString(SlideShowImage.SlideShowImageId);
                        hdnSlideShowImageId.Value = Convert.ToString(SlideShowImage.SlideShowImageId);
                    }
                }
                else
                {
                    Response.Redirect(host + "Admin/SliderManagement/SliderListing.aspx");
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }

        }


        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : to get insert slide show images data
        /// </summary>

        public string InsertSlideShowImageDetails()
        {
            string Status = "";
            if (Request.Form["W"] != null)
            {
                string wdata = Request.Form["W"].ToString();
                DataTable dt = GetDataIntoTable(wdata);
                bool IsSaved = ImageSlideShowBL.InsertSlideImagesDetails(dt);
                if (IsSaved)
                {
                    Status = "Saved Successfully";
                     var SlideImagesObj = ImageSlideShowBL.GetAllSliderImagesDetails(Convert.ToInt16(Session["CurrSlideId"]) );
                     if (SlideImagesObj != null)
                     {
                         if (SlideImagesObj.Count() > 0)
                         {
                             foreach (var SlideImageObj in SlideImagesObj)
                             {
                                 ImageSlideShowBE.SlideShowImage SlideImage = (ImageSlideShowBE.SlideShowImage)SlideImageObj;
                                 SetImageName(SlideImage.ImageExtension, SlideImage.FilePath, SlideImage.SlideShowImageId);
                             }
                         }
                     }
                    PopulateSlideShow();
                    /*Sachin Chauhan Start: 07 11 2015 : Remove slider id from session */
                    Session["CurrSlideId"] = null;
                    DirectoryInfo drTemp = new DirectoryInfo(Server.MapPath("~/Images/SlideShow/Temp/"));
                    foreach (FileInfo tmpFile in drTemp.GetFiles())
                    {
                        if (!tmpFile.Name.ToLower().Contains("thumb"))
                        {
                            if (tmpFile.IsReadOnly)
                                tmpFile.IsReadOnly = false;
                            tmpFile.Delete();
                        }
                        
                    }
                    HttpRuntime.Cache.Remove("ImageSlideShow");
                    /*Sachin Chauhan End: 07 11 2015*/

                }
                else
                {
                    Status = "Error Occured...";
                }



            }

            return Status;
        }
        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : to get page load data
        /// </summary>
        protected DataTable GetDataIntoTable(string vData)
        {
            List<SlideShowImage> returnList = new List<SlideShowImage>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            vData = "[" + vData.Replace("[", "").Replace("]", "") + "]";
            if (vData != "[]")
            {
                returnList = serializer.Deserialize<List<SlideShowImage>>(vData);


            }
            DataTable dt = new DataTable();
            dt.Columns.Add("SlideShowId", typeof(Int16));
            dt.Columns.Add("DisplayOrder", typeof(Int16));
            dt.Columns.Add("ImageExtension", typeof(string)).MaxLength = 10;
            dt.Columns.Add("NavigateUrl", typeof(string)).MaxLength = 250;
            dt.Columns.Add("IsNewWindow", typeof(bool));
            dt.Columns.Add("IsActive", typeof(bool));
            dt.Columns.Add("OverLayText", typeof(string)).MaxLength = 100;
            dt.Columns.Add("FilePath", typeof(string));
            dt.Columns.Add("AltText", typeof(string)).MaxLength = 30;
            int i = 0;
            foreach (SlideShowImage SlideShow in returnList)
            {
                DataRow dr = dt.NewRow();



                dr["SlideShowId"] = SlideShow.SlideShowId;
                dr["DisplayOrder"] = SlideShow.DisplayOrder;
                dr["ImageExtension"] = GetImageExtension(SlideShow.ImageExtension);
                dr["NavigateUrl"] = Microsoft.Security.Application.Encoder.HtmlEncode(SlideShow.NavigateUrl.Trim().Replace('\'', '@')); 
                dr["IsNewWindow"] = SlideShow.IsNewWindow;
                dr["IsActive"] = SlideShow.IsActive;
                dr["OverLayText"] = Microsoft.Security.Application.Encoder.HtmlEncode(SlideShow.OverLayText.Trim().Replace('\'', '@')) ;
                if (SlideShow.SlideShowImageId > 0)
                {
                    ResetImagesBeforeDelete(SlideShow);
                    dr["FilePath"] = SlideShow.FilePath;
                }
                else
                {
                    dr["FilePath"] = SlideShow.ImageExtension;

                }

                dr["AltText"] = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(SlideShow.AltText.Trim().Replace('\'', '@'));
                dt.Rows.Add(dr);
                i++;
            }

            return dt;

        }
        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : to get image extension
        /// </summary>
        protected string GetImageExtension(string Image)
        {
            Int16 Position = Convert.ToInt16(Image.IndexOf('.'));
            Image = Image.Substring(Position, 4);
            return Image;

        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : to populate language dropdownlist
        /// </summary>
        private void PopulateLanguageDropDownList()
        {
            try
            {
                List<LanguageBE> languages = new List<LanguageBE>();
                languages = LanguageBL.GetAllLanguageDetails(true);

                if (languages != null)
                {
                    if (languages.Count() > 0)
                    {
                        ddlLanguage.DataSource = languages;
                        ddlLanguage.DataTextField = "LanguageName";
                        ddlLanguage.DataValueField = "LanguageId";
                        ddlLanguage.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }


        }


        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : web method to get overlay text in different language for particular slide image
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetOverLayText(string LanguageId, string SlideShowImagId)
        {
            string OverLayText = "";
            try
            {
                OverLayText = ImageSlideShowBL.GetOverLayText(LanguageId, SlideShowImagId);

            }
            catch (Exception ex)
            {


            }
            return OverLayText;

        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : web method to set overlay text in different language for particular slide image
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetOverLayText(string LanguageId, string SlideShowImagId, string OverLayText)
        {
            string Status = "";
            try
            {
                Status = ImageSlideShowBL.SetOverLayText(LanguageId, SlideShowImagId, OverLayText);

            }
            catch (Exception ex)
            {


            }
            return Status;

        }

        public void ResetImagesBeforeDelete(SlideShowImage SlideShowImage)
        {
            string FileExt = SlideShowImage.ImageExtension;
            Int16 SlideId = SlideShowImage.SlideShowImageId;

            if (FileExt != "")
            {
                //Rename logo file with category id
                try
                {
                    string OldName = SlideId + FileExt;
                    if (File.Exists(Server.MapPath("~/Images/SlideShow/") + OldName))
                    {
                        SlideShowImage.FilePath = Guid.NewGuid() + FileExt;
                        File.Copy(Server.MapPath("~/Images/SlideShow/") + SlideId + FileExt, Server.MapPath("~/Images/SlideShow/Temp/" + SlideShowImage.FilePath));
                        File.Delete(Server.MapPath("~/Images/SlideShow/") + OldName);
                    }

                    OldName = SlideShowImage.FilePath.Replace(FileExt, "");
                    OldName = OldName + "_Thumb" + FileExt;

                    if (File.Exists(Server.MapPath("~/Images/SlideShow/") + SlideId + "_Thumb" + FileExt))
                    {
                        File.Copy(Server.MapPath("~/Images/SlideShow/") + SlideId + "_Thumb" + FileExt, Server.MapPath("~/Images/SlideShow/Temp/" + OldName));
                        File.Delete(Server.MapPath("~/Images/SlideShow/") + SlideId + "_Thumb" + FileExt);
                    }



                }
                catch (Exception ex1)
                {
                    Exceptions.WriteExceptionLog(ex1);

                }
            }

        }
    }
}

public class SlideShowImage
{

    public Int16 SlideShowId { get; set; }
    public Int16 SlideShowImageId { get; set; }
    public Int16 DisplayOrder { get; set; }
    public string ImageExtension { get; set; }
    public string NavigateUrl { get; set; }
    public bool IsNewWindow { get; set; }
    public bool IsActive { get; set; }
    public string OverLayText { get; set; }
    public string FilePath { get; set; }
    public string AltText { get; set; }

}
