﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CKEditor.NET;
using Microsoft.Security.Application;

namespace Presentation
{
    public class Admin_EmailManagement_ManageEmail : BasePage //System.Web.UI.Page
    {
        int EmailId = 0;
        int UserId;
        string EmailType;
        string host = GlobalFunctions.GetVirtualPathAdmin();
        protected global::CKEditor.NET.CKEditorControl txtEmailBody;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvLang, dvVariable;
        protected global::System.Web.UI.WebControls.TextBox txtFromEmailId, txtCCEmailId, txtBCCEmailId, txtEmailName, txtSubject;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage, ddlVariable;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtEmailBody.config.toolbar = new object[]
                {
                    new object[] { "Source" },
                    new object[] { "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
                    new object[] { "NumberedList", "BulletedList"},
                    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
                    "/",
                    new object[] { "Styles", "Format", "Font", "FontSize" },
                    new object[] { "TextColor", "BGColor" },
                    "/",
                    new object[] { "Link", "Unlink"},
                    new object[] { "Table", "HorizontalRule"}
                };
                if (!IsPostBack)
                {
                    if (Session["StoreUser"] != null)
                    {
                        UserBE objUserBE = new UserBE();
                        objUserBE = Session["StoreUser"] as UserBE;
                        UserId = objUserBE.UserId;
                    }
                    PopulateLanguage();
                    if (Convert.ToString(Session["EmailType"]) != "" || Convert.ToString(Session["EmailType"]) != null)
                    {
                        EmailType = Convert.ToString(Session["EmailType"]);
                        if (EmailType == "Edit")
                        {
                            if (Convert.ToString(Session["EmailTemplateId"]) != null)
                            {
                                EmailId = Convert.ToInt16(Session["EmailTemplateId"]);
                                PopulateEmail(EmailId); dvLang.Visible = true;
                            }
                            else
                            { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), host + "EmailManagement/EmailListing.aspx", AlertType.Warning); return; }
                        }
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), host + "EmailManagement/EmailListing.aspx", AlertType.Warning); return; }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                EmailId = Convert.ToInt16(Session["EmailTemplateId"]);
                PopulateEmail(EmailId);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSaveExit_Click(object sender, EventArgs e)
        {
            try
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match1 = regex.Match(Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtFromEmailId.Text).Trim()));
                if (!match1.Success)
                { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("ForgotPassword/InvalidEmailId"), AlertType.Warning); return; }
                if (txtCCEmailId.Text.Trim() != "")
                {
                    Match match2 = regex.Match(Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtCCEmailId.Text).Trim()));
                    if (!match2.Success)
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("ForgotPassword/InvalidEmailId"), AlertType.Warning); return; }
                }
                if (txtBCCEmailId.Text.Trim() != "")
                {
                    Match match3 = regex.Match(Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtBCCEmailId.Text).Trim()));
                    if (!match3.Success)
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("ForgotPassword/InvalidEmailId"), AlertType.Warning); return; }
                }
                EmailId = Convert.ToInt16(Session["EmailTemplateId"]);
                EmailManagementBE objBE = new EmailManagementBE();
                objBE.EmailTemplateId = Convert.ToInt16(EmailId);
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                objBE.EmailTemplateName = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtEmailName.Text).Trim());
                objBE.FromEmailId = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtFromEmailId.Text).Trim());
                objBE.CCEmailId = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtCCEmailId.Text).Trim());
                objBE.BCCEmailId = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtBCCEmailId.Text).Trim());
                objBE.Subject = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtSubject.Text).Trim());
                objBE.Body = GlobalFunctions.RemoveSanitisedPrefixes(Convert.ToString(Sanitizer.GetSafeHtmlFragment(Server.HtmlEncode(txtEmailBody.Text)).Trim()));
                objBE.ModifiedBy = Convert.ToInt16(UserId);
                int i = EmailManagementBL.UpdateEmailDetails(Constants.USP_UpdateEmailTemplate, objBE, true);
                if (i != 0)
                {
                    CreateActivityLog("Email Management", "Updated", objBE.EmailTemplateId + "_" + txtEmailName.Text);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/EmailTemplate"), host + "EmailManagement/EmailListing.aspx", AlertType.Success);
                }
                else
                { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); return; }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(host + "EmailManagement/EmailListing.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails();
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }

        protected void PopulateEmail(int EmailId)
        {
            try
            {
                EmailManagementBE lstEmail = new EmailManagementBE();
                EmailManagementBE objBE = new EmailManagementBE();
                objBE.EmailTemplateId = Convert.ToInt16(EmailId);
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                lstEmail = EmailManagementBL.GetSingleEmailObject(Constants.USP_GetEmailTemplate, objBE, true);
                if (lstEmail != null && lstEmail.EmailTemplateName != null)
                {
                    txtEmailName.Text = lstEmail.EmailTemplateName.Trim();
                    txtSubject.Text = lstEmail.Subject.Trim();
                    txtFromEmailId.Text = lstEmail.FromEmailId;
                    txtCCEmailId.Text = lstEmail.CCEmailId;
                    txtBCCEmailId.Text = lstEmail.BCCEmailId;
                    PopulateVariable(lstEmail.SettingValue);
                    txtEmailBody.Text = Server.HtmlDecode(lstEmail.Body.Trim());
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }

        protected void PopulateVariable(string SettingValue)
        {
            try
            {
                ddlVariable.Items.Clear();
                if (SettingValue != null && SettingValue != "")
                {
                    string[] str = SettingValue.Split('|');
                    if (str.Length > 0)
                    {
                        dvVariable.Visible = true;
                        for (int i = 0; i < str.Length; i++)
                        { int x = (i + 1); ddlVariable.Items.Insert(i, new ListItem(str[i].ToString(), x.ToString())); }
                        ddlVariable.Items.Insert(0, new ListItem("Select", "0"));
                    }
                    else
                    { dvVariable.Visible = false; }
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }

    }
}