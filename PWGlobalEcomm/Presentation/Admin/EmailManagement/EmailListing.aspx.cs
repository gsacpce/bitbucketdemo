﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_EmailManagement_EmailListing : System.Web.UI.Page
    {
        int LanguageId;
        string host = GlobalFunctions.GetVirtualPathAdmin();
        protected global::System.Web.UI.WebControls.GridView grdEmailTemplate;
        protected global::System.Web.UI.WebControls.Button btnAddEmailTemplate;
        protected global::System.Web.UI.WebControls.TextBox txtStoreNameEmail;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                StoreBE objStore = new StoreBE();
                objStore = StoreBL.GetStoreDetails();
                EmailManagementBE.FromEmailBE objEmailBE = objStore.FromEmailIdAlias.FirstOrDefault(x => x.Id > 0);

                LanguageId = GlobalFunctions.GetLanguageId();
                if (!IsPostBack)
                {
                    btnAddEmailTemplate.Visible = false;
                    PopulateTemplate();
                    Session["EmailType"] = null;
                    Session["EmailTemplateId"] = null;
                    txtStoreNameEmail.Text = objEmailBE.FromEmailIdAlias;
                }
                #region Meta Data
                Page.Title = "Email Management Listing";
                Page.MetaDescription = "Email Management Listing";
                Page.MetaKeywords = "email template";
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateTemplate()
        {
            try
            {
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objBE = new EmailManagementBE();
                objBE.LanguageId = Convert.ToInt16(LanguageId);
                lstEmail = EmailManagementBL.GetAllTemplates(Constants.USP_GetEmailTemplate, objBE);
                if (lstEmail != null)
                {
                    if (lstEmail.Count > 0)
                    {
                        grdEmailTemplate.DataSource = lstEmail;
                        grdEmailTemplate.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnAddEmailTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(host + "EmailManagement/ManageEmail.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void grdEmailTemplate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEmailTemplate.PageIndex = e.NewPageIndex;
                PopulateTemplate();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lbtnEdit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                Session["EmailTemplateId"] = Convert.ToInt32(e.CommandArgument);
                Session["EmailType"] = "Edit";
                Response.Redirect(host + "EmailManagement/ManageEmail.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSaveUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE.FromEmailBE objBE = new EmailManagementBE.FromEmailBE();
                //objBE.LanguageId = Convert.ToInt16(LanguageId);

                if (txtStoreNameEmail.Text != "")
                {
                    objBE.FromEmailIdAlias = txtStoreNameEmail.Text;
                    bool Email = EmailManagementBL.GetFromEmailIdAlias(Constants.USP_GetFromEmailIdAlias, objBE);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated Successfully.", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please enter Details", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}