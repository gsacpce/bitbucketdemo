﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_EditBreadCrumbManagement : BasePage //System.Web.UI.Page
    {

        Int16 BreadCrumbLanguageId = 0;
        Int16 LangugageId = 0;
        protected global::System.Web.UI.WebControls.TextBox txtBreadCrumbName;
        protected global::System.Web.UI.WebControls.TextBox txtBreadCrumbLink;



        protected global::System.Web.UI.WebControls.Button btnSave;
        protected global::System.Web.UI.WebControls.Button btnCancel;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;

        protected string AdminHost = GlobalFunctions.GetVirtualPathAdmin();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["EditBreadCrumb"] != null)
                {
                    BreadCrumbManagementBE EditBreadCrumb = Session["EditBreadCrumb"] as BreadCrumbManagementBE;
                    txtBreadCrumbName.Text = EditBreadCrumb.BreadCrumbName;
                    txtBreadCrumbLink.Text = EditBreadCrumb.Link;
                }
            }
        }

        protected void Btn_Click(object sender, EventArgs e)
        {
            try
            {

                Button btn = sender as Button;

                switch (btn.CommandArgument)
                {
                    case "Save":
                        {
                            SaveBreadCrumb();
                            break;
                        }
                    case "Cancel":
                        {
                            Response.Redirect(AdminHost + "BreadCrumbManagement/BreadCrumbListing.aspx");
                            break;
                        }
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }




        private bool SaveBreadCrumb()
        {
            bool flgSuccess = true;

            try
            {
                if (Page.IsValid)
                {
                    BreadCrumbManagementBE objBrandColor = new BreadCrumbManagementBE();
                    objBrandColor.BreadCrumbName = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtBreadCrumbName.Text.Trim().Replace('\'', '@'));
                    objBrandColor.Link = txtBreadCrumbLink.Text.Trim();

                    if (Session["StoreUser"] != null)
                    {
                        UserBE User = Session["StoreUser"] as UserBE;
                        objBrandColor.ModifiedBy = User.UserId;
                    }
                    if (Session["EditBreadCrumb"] != null && Session["EditOperation"].ToString().Equals("Update"))
                    {
                        objBrandColor.BreadCrumbName = (Session["EditBreadCrumb"] as BreadCrumbManagementBE).BreadCrumbName;
                        objBrandColor.BreadCrumbId = (Session["EditBreadCrumb"] as BreadCrumbManagementBE).BreadCrumbId;
                        objBrandColor.LanguageId = (Session["EditBreadCrumb"] as BreadCrumbManagementBE).LanguageId;
                        objBrandColor.BreadCrumbLanguageId = (Session["EditBreadCrumb"] as BreadCrumbManagementBE).BreadCrumbLanguageId;

                        if (objBrandColor.BreadCrumbName == Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtBreadCrumbName.Text.Trim().Replace('\'', '@')))
                        {
                            if (BreadCrumbManagementBL.UpdateBreadCrumbDetails(objBrandColor, DBAction.Update))
                            {
                                CreateActivityLog("Bread Crumbs", "Updated", objBrandColor.BreadCrumbId + " || " + Convert.ToString(txtBreadCrumbName.Text));
                                ClearControls();
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Bread Crumb details updated successfully!", "BreadCrumbListing.aspx", AlertType.Success);
                            }
                        }
                        else
                        {
                            if (!CheckBreadCrumbNameExist(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtBreadCrumbName.Text.Trim().Replace('\'', '@'))))
                            {
                                objBrandColor.BreadCrumbName = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtBreadCrumbName.Text.Trim().Replace('\'', '@'));
                                if (BreadCrumbManagementBL.UpdateBreadCrumbDetails(objBrandColor, DBAction.Update))
                                {
                                    CreateActivityLog("Bread Crumbs", "Updated", objBrandColor.BreadCrumbId + " || " + Convert.ToString(txtBreadCrumbName.Text));
                                    ClearControls();
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Bread Crumb details updated successfully!", "BreadCrumbListing.aspx", AlertType.Success);
                                }
                            }
                            else
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Bread Crumb Name already taken!", AlertType.Warning);
                            }
                        }
                    }

                }



            }

            catch (Exception ex)
            {
                flgSuccess = false;
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            finally
            {

            }


        Exit:

            return flgSuccess;
        }

        private void ClearControls()
        {
            txtBreadCrumbLink.Text = "";
            txtBreadCrumbName.Text = "";
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 03-09-15
        /// Scope   : to check whether the BreadCrumb name exist in the db or not
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        private bool CheckBreadCrumbNameExist(string BreadCrumbName)
        {

            if (Session["EditBreadCrumb"] != null)
            {
                LangugageId = (Session["EditBreadCrumb"] as BreadCrumbManagementBE).LanguageId;
            }

            List<BreadCrumbManagementBE> BreadCrumb = new List<BreadCrumbManagementBE>();
            BreadCrumb = BreadCrumbManagementBL.GetAllBreadCrumbDetails(LangugageId);

            if (BreadCrumb != null)
            {
                if (BreadCrumb.Count() > 0)
                {
                    int count = BreadCrumb.Count(x => x.BreadCrumbName == BreadCrumbName);
                    return count > 0 ? true : false;
                }
            }
            return false;
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["EditBreadCrumb"] != null)
                {
                    BreadCrumbLanguageId = (Session["EditBreadCrumb"] as BreadCrumbManagementBE).BreadCrumbLanguageId;
                    LangugageId = (Session["EditBreadCrumb"] as BreadCrumbManagementBE).LanguageId;
                }
                GetBreadCrumbDetails(BreadCrumbLanguageId, LangugageId);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        private void GetBreadCrumbDetails(Int16 BreadCrumbLanguageId, Int16 LanguageId)
        {
            try
            {
                List<BreadCrumbManagementBE> BreadCrumbs = new List<BreadCrumbManagementBE>();
                BreadCrumbs = BreadCrumbManagementBL.GetAllBreadCrumbDetails(LanguageId);


                if (BreadCrumbs != null)
                {
                    if (BreadCrumbs.Count() > 0)
                    {
                        BreadCrumbManagementBE BreadCrumb = BreadCrumbs.First(b => b.BreadCrumbLanguageId == BreadCrumbLanguageId);
                        if (BreadCrumb != null)
                        {
                            txtBreadCrumbLink.Text = BreadCrumb.Link;
                            txtBreadCrumbName.Text = BreadCrumb.BreadCrumbName;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

       
    }

}