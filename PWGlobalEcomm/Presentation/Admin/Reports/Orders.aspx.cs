﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using Microsoft.Security.Application;
using System.Data;
namespace Presentation
{
    public class Admin_Reports_Orders : System.Web.UI.Page
    {
        #region UI Controls
        protected global::System.Web.UI.WebControls.GridView grdProduct;
        protected global::System.Web.UI.WebControls.TextBox txtSearch, txtFromDate, txtToDate, txtFromOrderValue, txtToOrderValue;
        protected global::System.Web.UI.WebControls.Literal ltrNoOfProducts;
        protected global::System.Web.UI.WebControls.DropDownList ddlSearchOption, ddlPaymentStatus;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnIsSearch;
        #endregion

        #region Variables
        List<CustomerOrderBE> lstProductBE;
        public string linkCommand = string.Empty;
        public Int16 LanguageId = 1;
        public Int16 PageIndex = 1;
        protected string HostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        protected string Host = GlobalFunctions.GetVirtualPath();
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 03-11-2015
        /// Scope   : Page_Load of the Orders page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                PageIndex = 1;
                BindProduct(PageIndex, Convert.ToInt16(grdProduct.PageSize), "", "", "", "", "", "");
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 03-11-2015
        /// Scope   : BindProduct of the Orders page
        /// </summary>
        ///<param name="PageIndex"></param>     
        ///<param name="PageSize"></param>
        ///<param name="searchText"></param>
        /// <returns></returns>
        private void BindProduct(Int16 PageIndex, Int16 PageSize, string strFilterType, string FromDate, string ToDate, string FilterValue, string FromOrderValue, string ToOrderValue)
        {
            lstProductBE = new List<CustomerOrderBE>();
            try
            {
                lstProductBE = ReportsBL.GetOrdersList<CustomerOrderBE>(LanguageId, PageIndex, PageSize, strFilterType, FromDate, ToDate, FilterValue, FromOrderValue, ToOrderValue);

                if (lstProductBE != null && lstProductBE.Count > 0)
                {
                    ltrNoOfProducts.Text = lstProductBE[0].RowNumber + " - " + lstProductBE[lstProductBE.Count - 1].RowNumber + " of " + lstProductBE[0].ProductCount.ToString();
                    grdProduct.DataSource = lstProductBE;
                    grdProduct.VirtualItemCount = lstProductBE[0].ProductCount;
                    grdProduct.DataBind();
                }
                else
                {
                    grdProduct.DataSource = null;
                    grdProduct.DataBind();
                    ltrNoOfProducts.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = ReportsBL.GetOrderDataForExportView();
                if (dt.Rows.Count > 0)
                {
                    GlobalFunctions.ExportToExcel(dt, "CustomerOrderData");
                    dt = null;
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 03-11-2015
        /// Scope   : btnSearch_Click of the Orders page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param> 
        /// <returns></returns>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hdnIsSearch.Value = "1";
                grdProduct.PageIndex = 0;
                string strFilterValue = string.Empty;
                string strFromDate = string.Empty;
                string strToDate = string.Empty;
                if (ddlSearchOption.SelectedValue == "status")
                    strFilterValue = ddlPaymentStatus.SelectedValue;
                else if (ddlSearchOption.SelectedValue == "date")
                {
                    strFromDate = String.Format("{0:dd-MM-yyyy}", Convert.ToDateTime(txtFromDate.Text.Trim()));
                    strToDate = String.Format("{0:dd-MM-yyyy}", Convert.ToDateTime(txtToDate.Text.Trim()));
                }
                else
                    strFilterValue = Sanitizer.GetSafeHtmlFragment(txtSearch.Text.Trim());

                BindProduct(1, Convert.ToInt16(grdProduct.PageSize), Sanitizer.GetSafeHtmlFragment(ddlSearchOption.SelectedValue), strFromDate, strToDate,
                        strFilterValue, Sanitizer.GetSafeHtmlFragment(txtFromOrderValue.Text.Trim()), Sanitizer.GetSafeHtmlFragment(txtToOrderValue.Text.Trim()));

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 03-11-2015
        /// Scope   : btnReset_Click of the Orders page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param> 
        /// <returns></returns>
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(HostAdmin + "Reports/Orders.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 03-11-2015
        /// Scope   : grdProduct_PageIndexChanging of the Orders page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param> 
        /// <returns></returns>
        protected void grdProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdProduct.PageIndex = e.NewPageIndex;
                PageIndex = Convert.ToInt16(e.NewPageIndex + 1);

                if (hdnIsSearch.Value == "1")
                {
                    string strFilterValue = string.Empty;
                    string strFromDate = string.Empty;
                    string strToDate = string.Empty;

                    if (ddlSearchOption.SelectedValue == "status")
                        strFilterValue = ddlPaymentStatus.SelectedValue;
                    else if (ddlSearchOption.SelectedValue == "date")
                    {
                        strFromDate = String.Format("{0:dd-MM-yyyy}", Convert.ToDateTime(txtFromDate.Text.Trim()));
                        strToDate = String.Format("{0:dd-MM-yyyy}", Convert.ToDateTime(txtToDate.Text.Trim()));
                    }
                    else
                        strFilterValue = Sanitizer.GetSafeHtmlFragment(txtSearch.Text.Trim());

                    BindProduct(PageIndex, Convert.ToInt16(grdProduct.PageSize), Sanitizer.GetSafeHtmlFragment(ddlSearchOption.SelectedValue), strFromDate, strToDate,
                         strFilterValue, Sanitizer.GetSafeHtmlFragment(txtFromOrderValue.Text.Trim()), Sanitizer.GetSafeHtmlFragment(txtToOrderValue.Text.Trim()));
                }
                else
                {
                    BindProduct(PageIndex, Convert.ToInt16(grdProduct.PageSize), "", "", "", "", "", "");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}