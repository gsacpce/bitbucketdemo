﻿using OfficeOpenXml;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Reports_NewletterSubscription : System.Web.UI.Page
{
    #region UI Controls
    protected global::System.Web.UI.WebControls.GridView gvData;
    protected global::System.Web.UI.WebControls.TextBox txtSearch;
    protected global::System.Web.UI.WebControls.HiddenField hdnproductname;
    protected global::System.Web.UI.WebControls.Label lblTotal;
    #endregion

    List<UserBE> lstUserBE;
    public string linkCommand = string.Empty;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        //grdProduct.PageSize = PageSize;
        if (!IsPostBack)
        {
            GetSubscribers();
        }
    }

    string host = GlobalFunctions.GetVirtualPathAdmin();

    //protected void chkIsActive_CheckedChanged(object sender, EventArgs e)
    //{
    //    ProductBE objProductBE = new ProductBE();
    //    try
    //    {
    //        bool result = false;
    //        CheckBox chk = (CheckBox)sender;
    //        objProductBE.ProductId = Convert.ToInt32(chk.Attributes["rel"]);
    //        objProductBE.IsActive = chk.Checked;
    //        string ProductName = objProductBE.ProductName;
    //        result = ProductBL.UpdateProductStatus(objProductBE);
    //        if (result)
    //        {
    //            //CreateActivityLog("Product List", "Updated", ProductName);
    //            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/UpdatedMessage"), AlertType.Success);
    //        }
    //        else
    //            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/UpdatedMessage"), AlertType.Failure);

    //    }
    //    catch (Exception ex)
    //    {
    //        Exceptions.WriteExceptionLog(ex);
    //    }
    //    finally
    //    {
    //        objProductBE = null;
    //    }
    //}
    /// <summary>  
    /// Bind Product
    /// </summary>
    private List<UserBE> GetSubscribers()
    {
        lstUserBE = new List<UserBE>();
        try
        {
            lstUserBE = UserBL.GetSubsribedUserList(txtSearch.Text);
            if (lstUserBE != null)
            {
                if (lstUserBE.Count > 0)
                {

                    gvData.DataSource = lstUserBE;
                    gvData.DataBind();
                    lblTotal.Text = Convert.ToString(lstUserBE.Count);
                    #region CustomPaging
                    //grdProduct.VirtualItemCount = lstProductBE[0].ProductCount;
                    //ltrNoOfProducts.Text = lstProductBE[0].ProductCount.ToString();
                    //Session["PRODUCTS"] = lstProductBE;
                    #endregion
                }
                else
                {
                    gvData.DataSource = null;
                    gvData.DataBind();
                    lblTotal.Text = "0";
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return lstUserBE;
    }

    protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvData.PageIndex = e.NewPageIndex;
       
        //if (!string.IsNullOrEmpty(hdnproductname.Value.Trim()))
        //{
        //    BindProduct(PageIndex, PageSize, hdnproductname.Value.Trim());
        //}
        //else
        //{
        //    BindProduct(PageIndex, PageSize);
        //}
        GetSubscribers();
    }
    protected void btnSearchName_Click(object sender, EventArgs e)
    {
        try
        {
            GetSubscribers();
            //if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
            //{
            //    GetSubscribers();
            //}
            //else
            //{
            //    GetSubscribers();
            //}
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }

    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            txtSearch.Text = "";
            GetSubscribers();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }

    }

 
    public DataSet CreateDataSet<T>(List<T> list)
    {
        //list is nothing or has nothing, return nothing (or add exception handling)
        if (list == null || list.Count == 0) { return null; }

        //get the type of the first obj in the list
        var obj = list[0].GetType();

        //now grab all properties
        var properties = obj.GetProperties();

        //make sure the obj has properties, return nothing (or add exception handling)
        if (properties.Length == 0) { return null; }

        //it does so create the dataset and table
        var dataSet = new DataSet();
        var dataTable = new DataTable();

        //now build the columns from the properties
        var columns = new DataColumn[properties.Length];
        for (int i = 0; i < properties.Length; i++)
        {
            columns[i] = new DataColumn(properties[i].Name, properties[i].PropertyType);
        }

        //add columns to table
        dataTable.Columns.AddRange(columns);

        //now add the list values to the table
        foreach (var item in list)
        {
            //create a new row from table
            var dataRow = dataTable.NewRow();

            //now we have to iterate thru each property of the item and retrieve it's value for the corresponding row's cell
            var itemProperties = item.GetType().GetProperties();

            for (int i = 0; i < itemProperties.Length; i++)
            {
                dataRow[i] = itemProperties[i].GetValue(item, null);
            }

            //now add the populated row to the table
            dataTable.Rows.Add(dataRow);
        }

        //add table to dataset
        dataSet.Tables.Add(dataTable);

        //return dataset
        return dataSet;
    }

    private void GenerateExcel(DataTable tbl)
    {
        using (ExcelPackage pck = new ExcelPackage())
        {
            //Create the worksheet
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Subscribed Users");
            // DataTable NewTable = tbl.DefaultView.ToTable(false, "ProductCode","LanguageId","ProductName","ProductDescription", "FurtherDescription", "");

            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            ws.Cells["A1"].LoadFromDataTable(tbl, true);

            Response.Clear();
            //Write it back to the client
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=SubscribedUsers.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
    protected void btnExportdata_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            DataTable dtExportToExcel = new DataTable();

            List<UserBE> lstData = GetSubscribers();
            // This is not hardcoded but disabled the Custom Paging without changing the Existing Code

            // ds = CreateDataSet(lstData);

            if (ds != null)
            {
                List<object> lstAnonymousObjs = new List<object>();

                foreach (UserBE Users in lstData)
                {
                    lstAnonymousObjs.Add(new
                    {
                        EmailId = Users.EmailId,
                        FirstName = Users.FirstName,
                        LastName = Users.LastName,
                        SubscribedDate = Users.SubscribedDate,
                        SubscribedStatus = Users.SubscribedStatus,
                    });
                }

                ds = CreateDataSet(lstAnonymousObjs);
                dtExportToExcel = ds.Tables[0];
                GenerateExcel(dtExportToExcel);
            }


        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
}