﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using OfficeOpenXml;

public partial class Admin_Reports_ProductWithoutImages : System.Web.UI.Page
{
    protected global::System.Web.UI.WebControls.TextBox txtSearch;
    protected global::System.Web.UI.WebControls.GridView gvData;
    protected global::System.Web.UI.WebControls.HiddenField hdnFilter;
    protected global::System.Web.UI.WebControls.Button btnExportProducts;
    protected global::System.Web.UI.WebControls.Label lblSearch, lblTotal;

    string CurrId = "", PrevId = "";
    string CurrVariantTypeId = "", PrevVariantTypeId = "";
    public string linkCommand = string.Empty;

    public Int16 LanguageId = 1;
    public Int16 PageIndex;
    public Int16 PageSize = 50;

    List<ProductBE.ProductImage> lstProductBE;

    protected void Page_Load(object sender, EventArgs e)
    {
        //*************************************************************************
        // Purpose : It will fired at the time of loading a page.
        // Inputs  : object sender, EventArgs e
        // Return  : Nothing
        //*************************************************************************
     
        try
        {
            Page.Title = "Report - Product Missing Images";
            Page.MetaDescription = "Report - Product Missing Images";
            Page.MetaKeywords = "report, product missing images";

            if (!IsPostBack)
            {
                //gvData.PageSize = PageSize; // custom pagination
                if (!IsPostBack)
                {
                    PageIndex = 0;
                    GetProductMissingImageList(PageIndex, PageSize,"");
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected List<ProductBE.ProductImage> GetProductMissingImageList(Int16 PageIndex, Int16 PageSize, string searchText)
    {
        //*************************************************************************
        // Purpose : To bind the unassigned products
        // Inputs  : Nothing
        // Return  : Nothing
        //*************************************************************************
        try
        {
            DataSet ds = new DataSet();

            lstProductBE = ProductBL.ProductWithoutImages<ProductBE.ProductImage>(LanguageId, PageIndex, PageSize,searchText);



            if (lstProductBE != null)
            {
                if (lstProductBE.Count > 0)
                {
                   
                        if (hdnFilter.Value != "")
                        {
                            //Session["ProductDataWithoutImage"] = lstProductBE;
                            gvData.DataSource = lstProductBE;


                            lblSearch.Text = Convert.ToString(lstProductBE.Count);
                        }
                        else
                        {
                            //Session["ProductDataWithoutImage"] = lstProductBE;
                            gvData.DataSource = lstProductBE;

                          
                        }
                        gvData.DataBind();

                        btnExportProducts.Visible = true;
                        #region For Custom Pagination
                        // gvData.VirtualItemCount = lstProductBE[0].ProductCount;
                        //lblTotal.Text = Convert.ToString(lstProductBE[0].ProductCount);
                        #endregion
                        lblTotal.Text = Convert.ToString(lstProductBE.Count);
                    
                   
                }
                else
                {
                    lblTotal.Text = "0";
                }
            }
            else
            {
                
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return lstProductBE;
    }
    public DataSet CreateDataSet<T>(List<T> list)
    {
        //list is nothing or has nothing, return nothing (or add exception handling)
        if (list == null || list.Count == 0) { return null; }

        //get the type of the first obj in the list
        var obj = list[0].GetType();

        //now grab all properties
        var properties = obj.GetProperties();

        //make sure the obj has properties, return nothing (or add exception handling)
        if (properties.Length == 0) { return null; }

        //it does so create the dataset and table
        var dataSet = new DataSet();
        var dataTable = new DataTable();

        //now build the columns from the properties
        var columns = new DataColumn[properties.Length];
        for (int i = 0; i < properties.Length; i++)
        {
            columns[i] = new DataColumn(properties[i].Name, properties[i].PropertyType);
        }

        //add columns to table
        dataTable.Columns.AddRange(columns);

        //now add the list values to the table
        foreach (var item in list)
        {
            //create a new row from table
            var dataRow = dataTable.NewRow();

            //now we have to iterate thru each property of the item and retrieve it's value for the corresponding row's cell
            var itemProperties = item.GetType().GetProperties();

            for (int i = 0; i < itemProperties.Length; i++)
            {
                dataRow[i] = itemProperties[i].GetValue(item, null);
            }

            //now add the populated row to the table
            dataTable.Rows.Add(dataRow);
        }

        //add table to dataset
        dataSet.Tables.Add(dataTable);

        //return dataset
        return dataSet;
    }

    protected void gvData_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal ltlName = e.Row.FindControl("ltlName") as Literal;
            Literal ltlVariantTypeName = e.Row.FindControl("ltlVariantTypeName") as Literal;

            Literal ltlThumb = e.Row.FindControl("ltlThumb") as Literal;
            Literal ltlMedium = e.Row.FindControl("ltlMedium") as Literal;
            Literal ltlLarge = e.Row.FindControl("ltlLarge") as Literal;
            Literal ltlHiRes = e.Row.FindControl("ltlHiRes") as Literal;

            LinkButton lnkThumb = e.Row.FindControl("lnkThumb") as LinkButton;
            LinkButton lnkMedium = e.Row.FindControl("lnkMedium") as LinkButton;
            LinkButton lnkLarge = e.Row.FindControl("lnkLarge") as LinkButton;
            LinkButton lnkHiRes = e.Row.FindControl("lnkHiRes") as LinkButton;

            CurrId = DataBinder.Eval(e.Row.DataItem, "ProductId").ToString();
            CurrVariantTypeId = DataBinder.Eval(e.Row.DataItem, "ProductVariantTypeId").ToString();

            if (CurrId != PrevId)
                ltlName.Text = DataBinder.Eval(e.Row.DataItem, "ProductCode").ToString() + "-" + DataBinder.Eval(e.Row.DataItem, "ProductName").ToString();
            else
                ltlName.Text = "";


            if (CurrId != PrevId)
            {
                ltlVariantTypeName.Text = DataBinder.Eval(e.Row.DataItem, "ProductVariantTypeName").ToString();
                //ltlVariantTypeName.Text = DataBinder.Eval(e.Row.DataItem, "VariantType").ToString();
                
            }
            else
            {
                if (CurrVariantTypeId != PrevVariantTypeId)
                    ltlVariantTypeName.Text = DataBinder.Eval(e.Row.DataItem, "ProductVariantTypeName").ToString();
                    //ltlVariantTypeName.Text = DataBinder.Eval(e.Row.DataItem, "VariantType").ToString();
                else
                    ltlVariantTypeName.Text = "";


            }

            PrevId = CurrId;
            CurrId = "";

            PrevVariantTypeId = CurrVariantTypeId;
            CurrVariantTypeId = "";

            string ImageName = DataBinder.Eval(e.Row.DataItem, "ImageName").ToString();

            string ProductCode = DataBinder.Eval(e.Row.DataItem, "ProductCode").ToString();

            Search(GlobalFunctions.GetThumbnailProductImagePath() + ImageName, ImageName, lnkThumb, ltlThumb);
            Search(GlobalFunctions.GetMediumProductImagePath() + ImageName, ImageName, lnkMedium, ltlMedium);
            Search(GlobalFunctions.GetLargeProductImagePath() + ImageName, ImageName, lnkLarge, ltlLarge);


            
        }
    }

    protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvData.PageIndex = e.NewPageIndex;
        PageIndex = Convert.ToInt16(e.NewPageIndex);
        if (!string.IsNullOrEmpty(hdnFilter.Value.Trim()))
        {
            GetProductMissingImageList(PageIndex, PageSize, hdnFilter.Value.Trim());
        }
        else
        {
            GetProductMissingImageList(PageIndex, PageSize,"");
        }
      
    }
    protected void btnExportProducts_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            DataTable dtExportToExcel = new DataTable();

            //List<ProductBE.ProductImage> lstData =  Session["ProductDataWithoutImage"] as  List<ProductBE.ProductImage>;

            List<ProductBE.ProductImage> lstData = GetProductMissingImageList(1, 1, "");

            ds = CreateDataSet(lstData);

            if (ds != null)
            {
                ds.Tables[0].Columns.Remove("ProductId");
                ds.Tables[0].Columns.Remove("ProductVariantTypeId");
                ds.Tables[0].Columns.Remove("IsActive");
                ds.Tables[0].Columns.Remove("ProductCount");
                ds.Tables[0].Columns.Remove("ProductVariantId");
                ds.Tables[0].Columns.Remove("CatalogueId");
                ds.Tables[0].Columns.Remove("LanguageId");
                ds.Tables[0].Columns.Remove("CreatedDate");
                ds.Tables[0].Columns.Remove("ModifiedBy");
                ds.Tables[0].Columns.Remove("ModifiedDate");
                ds.Tables[0].Columns.Remove("CreatedBy");
                ds.Tables[0].Columns.Remove("HiRes");
                dtExportToExcel = ds.Tables[0];
            }
            
            GenerateExcel(dtExportToExcel);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    private void GenerateExcel(DataTable tbl)
    {
        using (ExcelPackage pck = new ExcelPackage())
        {
            //Create the worksheet
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ProductDataWithoutImage");
            // DataTable NewTable = tbl.DefaultView.ToTable(false, "ProductCode","LanguageId","ProductName","ProductDescription", "FurtherDescription", "");

            //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            ws.Cells["A1"].LoadFromDataTable(tbl, true);

            Response.Clear();
            //Write it back to the client
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=ProductData.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }


    private void EnableViewImage(string strFilePath, string strFileName, LinkButton lnkbtnView, Literal ltlMissing)
    {
        ltlMissing.Visible = false;
        lnkbtnView.Visible = true;

        lnkbtnView.Attributes.Add("href", strFilePath);

        //lnkbtnView.Attributes.Add("onmousedown", "PopUpPageWithDifferentName('" + strFilePath + "','" + strFileName + "',450,550);");
        //lnkbtnView.Attributes.Add("onkeyPress", "PopUpPageWithDifferentName('" + strFilePath + "','" + strFileName + "',450,550);");
    }

    private void Search(string strFilePath, string strFileName, LinkButton lnkbtnView, Literal ltlMissing)
    {
        if(File.Exists(Server.MapPath("~/Images/Products/Medium/"+ strFileName)))
        {
        ltlMissing.Visible = false;
        lnkbtnView.Visible = true;
        lnkbtnView.Attributes.Add("href", strFilePath);
        lnkbtnView.Attributes.Add("target", "_blank");
        }
        else
        {
            ltlMissing.Visible = true;
            ltlMissing.Text = "Missing";
            lnkbtnView.Visible = false;
        }
        if (File.Exists(Server.MapPath("~/Images/Products/Thumbnail/" + strFileName)))
        {
            ltlMissing.Visible = false;
            lnkbtnView.Visible = true;
            lnkbtnView.Attributes.Add("href", strFilePath);
            lnkbtnView.Attributes.Add("target", "_blank");
        }
        else
        {
            ltlMissing.Visible = true;
            ltlMissing.Text = "Missing";
            lnkbtnView.Visible = false;
        }
        if (File.Exists(Server.MapPath("~/Images/Products/Large/" + strFileName)))
        {
            ltlMissing.Visible = false;
            lnkbtnView.Visible = true;
            lnkbtnView.Attributes.Add("href", strFilePath);
            lnkbtnView.Attributes.Add("target", "_blank");
        }
        else
        {
            ltlMissing.Visible = true;
            ltlMissing.Text = "Missing";
            lnkbtnView.Visible = false;
        }
    }
   

    protected void gvData_OnSorting(object sender, GridViewSortEventArgs e)
    {
        //Retrieve the table from the session object.
        DataTable dt = Session["ExportData"] as DataTable;

        if (dt != null)
        {
            //Sort the data.
            dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            gvData.DataSource = Session["ExportData"];
            gvData.DataBind();
        }

        linkCommand = e.SortExpression;
        SetSortDirection();
    }

    private string GetSortDirection(string column)
    {

        // By default, set the sort direction to ascending.
        string sortDirection = "ASC";

        // Retrieve the last column that was sorted.
        string sortExpression = ViewState["SortExpression"] as string;

        if (sortExpression != null)
        {
            // Check if the same column is being sorted.
            // Otherwise, the default value can be returned.
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }
        }
        // Save new values in ViewState.
        ViewState["SortDirection"] = sortDirection;
        ViewState["SortExpression"] = column;

        return sortDirection;
    }

    private void SetSortDirection()
    {
        if (linkCommand == "ProductCode")
        {
            if (this.ViewState["SortDirection"] == "ASC")
            {
                gvData.HeaderRow.Cells[0].CssClass = "bgup";
            }
            else if (this.ViewState["SortDirection"] == "DESC")
            {
                gvData.HeaderRow.Cells[0].CssClass = "bgdown";
            }
        }
        if (linkCommand == "VariantType")
        {
            if (this.ViewState["SortDirection"] == "ASC")
            {
                gvData.HeaderRow.Cells[1].CssClass = "bgup";
            }
            else if (this.ViewState["SortDirection"] == "DESC")
            {
                gvData.HeaderRow.Cells[1].CssClass = "bgdown";
            }
        }
        if (linkCommand == "VariantName")
        {
            if (this.ViewState["SortDirection"] == "ASC")
            {
                gvData.HeaderRow.Cells[2].CssClass = "bgup";
            }
            else if (this.ViewState["SortDirection"] == "DESC")
            {
                gvData.HeaderRow.Cells[2].CssClass = "bgdown";
            }
        }
        if (linkCommand == "Thumbnail")
        {
            if (this.ViewState["SortDirection"] == "ASC")
            {
                gvData.HeaderRow.Cells[3].CssClass = "bgup";
            }
            else if (this.ViewState["SortDirection"] == "DESC")
            {
                gvData.HeaderRow.Cells[3].CssClass = "bgdown";
            }
        }
        if (linkCommand == "Medium")
        {
            if (this.ViewState["SortDirection"] == "ASC")
            {
                gvData.HeaderRow.Cells[4].CssClass = "bgup";
            }
            else if (this.ViewState["SortDirection"] == "DESC")
            {
                gvData.HeaderRow.Cells[4].CssClass = "bgdown";
            }
        }

        if (linkCommand == "Large")
        {
            if (this.ViewState["SortDirection"] == "ASC")
            {
                gvData.HeaderRow.Cells[5].CssClass = "bgup";
            }
            else if (this.ViewState["SortDirection"] == "DESC")
            {
                gvData.HeaderRow.Cells[5].CssClass = "bgdown";
            }
        }

        if (linkCommand == "HiRes")
        {
            if (this.ViewState["SortDirection"] == "ASC")
            {
                gvData.HeaderRow.Cells[6].CssClass = "bgup";
            }
            else if (this.ViewState["SortDirection"] == "DESC")
            {
                gvData.HeaderRow.Cells[6].CssClass = "bgdown";
            }
        }
    }

    protected void btnSearchName_Click(object sender, EventArgs e)
    {
        //if (txtSearch.Text.Trim().ToString() != "")
        //    hdnFilter.Value = txtSearch.Text.Trim().ToLower() == "Product Code / Product Name".ToLower() ? "" : txtSearch.Text.Trim();

        if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
        {
            GetProductMissingImageList(PageIndex, PageSize, txtSearch.Text.Trim());
            hdnFilter.Value = txtSearch.Text.Trim();
        }
        else
        {
            GetProductMissingImageList(PageIndex, PageSize,"");
            hdnFilter.Value = "";
        }


    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtSearch.Text = "";
        hdnFilter.Value = "";

        gvData.PageIndex = 0;
        GetProductMissingImageList(PageIndex, PageSize, "");
    }
}