﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using Microsoft.Security.Application;
namespace Presentation
{
    public class Admin_Reports_AbandonedCart : System.Web.UI.Page
    {
        #region UI Controls
        protected global::System.Web.UI.WebControls.GridView grdProduct;
        protected global::System.Web.UI.WebControls.TextBox txtSearch;
        protected global::System.Web.UI.WebControls.Literal ltrNoOfProducts;
        #endregion

        #region Variables
        List<AbandonedShoppingCartBE> lstProductBE;
        public string linkCommand = string.Empty;
        public Int16 LanguageId = 1;
        public Int16 PageIndex = 1;
        public Int16 PageSize = 30;
        protected string HostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        protected string Host = GlobalFunctions.GetVirtualPath();
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 27-10-15
        /// Scope   : Page_Load of the AbandonedCart page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            grdProduct.PageSize = PageSize;
            if (!IsPostBack)
            {
                PageIndex = 1;
                BindProduct(PageIndex, PageSize);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 27-10-15
        /// Scope   : BindProduct of the AbandonedCart page
        /// </summary>
        ///<param name="PageIndex"></param>     
        ///<param name="PageSize"></param>
        ///<param name="searchText"></param>
        /// <returns></returns>
        private void BindProduct(Int16 PageIndex, Int16 PageSize, string searchText = "")
        {
            lstProductBE = new List<AbandonedShoppingCartBE>();
            try
            {
                lstProductBE = ReportsBL.GetProductList<AbandonedShoppingCartBE>(LanguageId, PageIndex, PageSize, searchText);

                if (lstProductBE != null && lstProductBE.Count > 0)
                {
                    ltrNoOfProducts.Text = lstProductBE[0].ProductCount.ToString();
                    grdProduct.DataSource = lstProductBE;
                    grdProduct.VirtualItemCount = lstProductBE[0].ProductCount;
                    grdProduct.DataBind();
                    //Session["PRODUCTS"] = lstProductBE;
                }
                else
                {
                    grdProduct.DataSource = null;
                    grdProduct.DataBind();
                    ltrNoOfProducts.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 27-10-15
        /// Scope   : btnSearch_Click of the AbandonedCart page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param> 
        /// <returns></returns>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grdProduct.PageIndex = 0;                
                BindProduct(1, PageSize, Sanitizer.GetSafeHtmlFragment(txtSearch.Text.Trim()));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 27-10-15
        /// Scope   : btnReset_Click of the AbandonedCart page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param> 
        /// <returns></returns>
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(HostAdmin + "Reports/AbandonedCart.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 27-10-15
        /// Scope   : grdProduct_PageIndexChanging of the AbandonedCart page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param> 
        /// <returns></returns>
        protected void grdProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdProduct.PageIndex = e.NewPageIndex;
                PageIndex = Convert.ToInt16(e.NewPageIndex + 1);
                BindProduct(PageIndex, PageSize, Sanitizer.GetSafeHtmlFragment(txtSearch.Text.Trim()));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}