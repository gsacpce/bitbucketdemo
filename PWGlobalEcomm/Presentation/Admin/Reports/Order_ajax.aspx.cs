﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;

namespace Presentation
{
    public partial class Admin_Reports_Order_ajax : System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Repeater rptBasketListing;
        protected global::System.Web.UI.WebControls.Literal ltrPaymentMethod, ltrDeliveryAddress, ltrBillingAddress, ltrOrderNo, ltGiftCertificateCode, ltGiftCertificateAmount;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divGiftCertificate;

        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        public decimal dSubTotalPrice = 0;
        CustomerOrderBE lstCustomerOrders;
        string strCurrencySymbol = string.Empty;
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 04-11-15
        /// Scope   : Page_Load of the Admin_Reports_Order_ajax page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                if (GlobalFunctions.IsPointsEnbled())
                {
                    strCurrencySymbol = string.Empty;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["cid"]) && !string.IsNullOrEmpty(Request.QueryString["uid"]))
                {
                    if (Session["StoreUser"] != null)
                    {
                        lstCustomerOrders = new CustomerOrderBE();
                        CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();

                        objCustomerOrderBE.CustomerOrderId = Convert.ToInt32(Request.QueryString["cid"]);
                        objCustomerOrderBE.OrderedBy = Convert.ToInt32(Request.QueryString["uid"]);
                        objCustomerOrderBE.LanguageId = 1;

                        lstCustomerOrders = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                        if (lstCustomerOrders != null && lstCustomerOrders.CustomerOrderProducts.Count > 0)
                        {

                            rptBasketListing.DataSource = lstCustomerOrders.CustomerOrderProducts;
                            rptBasketListing.DataBind();
                            ltrPaymentMethod.Text = lstCustomerOrders.PaymentTypeRef;
                            ltrOrderNo.Text = Convert.ToString(lstCustomerOrders.OrderId_OASIS);

                            ltrBillingAddress.Text = lstCustomerOrders.InvoiceContactPerson + "<br>" + lstCustomerOrders.InvoiceCompany + "<br>" +
                               lstCustomerOrders.InvoiceAddress1 + "<br>" + lstCustomerOrders.InvoiceAddress2 + "<br>" + lstCustomerOrders.InvoiceCity + "<br>" +
                               lstCustomerOrders.InvoiceCounty + "<br>" + lstCustomerOrders.InvoicePostalCode + "<br>" + lstCustomerOrders.InvoiceCountryName
                               + "<br>" + lstCustomerOrders.InvoicePhone;

                            ltrDeliveryAddress.Text = lstCustomerOrders.DeliveryContactPerson + "<br>" +
                                lstCustomerOrders.DeliveryAddress1 + "<br>" + lstCustomerOrders.DeliveryAddress2 + "<br>" + lstCustomerOrders.DeliveryCity + "<br>" +
                                lstCustomerOrders.DeliveryCounty + "<br>" + lstCustomerOrders.DeliveryPostalCode + "<br>" + lstCustomerOrders.DeliveryCountryName
                                + "<br>" + lstCustomerOrders.DeliveryPhone;

                            #region "GiftCertificate"
                            if (lstCustomerOrders.CustomerOrderGiftCertificate != null)
                            {
                                if (lstCustomerOrders.CustomerOrderGiftCertificate.Count > 0)
                                {
                                    ltGiftCertificateCode.Text = Convert.ToString(lstCustomerOrders.CustomerOrderGiftCertificate[0].GiftCertificateCode);
                                    ltGiftCertificateAmount.Text = strCurrencySymbol + Convert.ToString(lstCustomerOrders.CustomerOrderGiftCertificate[0].GCAmount);
                                    divGiftCertificate.Visible = true;
                                }
                                else
                                {
                                    divGiftCertificate.Visible = false;
                                }
                            }
                            else
                            {
                                divGiftCertificate.Visible = false;
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    Response.Clear();
                    Response.Write("Invalid order number.");
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 23-09-15
        /// Scope   : rptBasketListing_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptBasketListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl dvUnitPrice = (HtmlGenericControl)e.Item.FindControl("dvUnitPrice");
                    HtmlGenericControl dvTotalPrice = (HtmlGenericControl)e.Item.FindControl("dvTotalPrice");
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)e.Item.FindControl("hdnShoppingCartProductId");

                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";
                    double dPrice = 0;
                    if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode) && (lstCustomerOrders.BehaviourType.ToLower() == "product" || lstCustomerOrders.BehaviourType.ToLower() == "basket"))
                        dPrice = GlobalFunctions.DiscountedAmount(Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price")), lstCustomerOrders.DiscountPercentage);
                    else
                        dPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price"));

                    dPrice = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(dPrice)));
                    dvUnitPrice.InnerHtml = strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dPrice), "", GlobalFunctions.GetLanguageId()) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) + "</span>";
                    dvTotalPrice.InnerHtml = strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(dPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", GlobalFunctions.GetLanguageId()) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) + "</span>";
                    dSubTotalPrice += Convert.ToDecimal(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(dPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", GlobalFunctions.GetLanguageId()));
                }
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlGenericControl dvTotal = (HtmlGenericControl)e.Item.FindControl("dvTotal");
                    HtmlGenericControl dvSubTotal = (HtmlGenericControl)e.Item.FindControl("dvSubTotal");
                    HtmlGenericControl dvShippingCharges = (HtmlGenericControl)e.Item.FindControl("dvShippingCharges");
                    HtmlGenericControl dvTaxes = (HtmlGenericControl)e.Item.FindControl("dvTaxes");
                    HtmlGenericControl dvShippingText = (HtmlGenericControl)e.Item.FindControl("dvShippingText");
                    HtmlGenericControl dvTaxText = (HtmlGenericControl)e.Item.FindControl("dvTaxText");

                    decimal dShippingCharges = Convert.ToDecimal(lstCustomerOrders.StandardCharges == 0 ? lstCustomerOrders.ExpressCharges : lstCustomerOrders.StandardCharges);
                    decimal dTaxes = Convert.ToDecimal(lstCustomerOrders.TotalTax);

                    dShippingCharges = Convert.ToDecimal(GlobalFunctions.ConvertToDecimalPrecision(dShippingCharges));
                    dTaxes = Convert.ToDecimal(GlobalFunctions.ConvertToDecimalPrecision(dTaxes));
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        dShippingCharges = 0;
                        dTaxes = 0;
                        dvShippingCharges.Visible = false;
                        dvTaxes.Visible = false;
                        dvTaxText.Visible = false;
                        dvShippingText.Visible = false;
                    }
                    else
                    {
                        dvShippingCharges.InnerHtml = strCurrencySymbol + "<span>" + dShippingCharges + "</span>";
                        dvTaxes.InnerHtml = strCurrencySymbol + "<span>" + dTaxes + "</span>";
                    }
                    decimal dTotalPrice = dSubTotalPrice + dShippingCharges + dTaxes;
                    dvSubTotal.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(dSubTotalPrice) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) + "</span>";
                    dvTotal.InnerHtml = strCurrencySymbol + "<span>" + dTotalPrice + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) + "</span>";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }
}