﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class StoreCreator_UserControls_Menu : System.Web.UI.UserControl
    {
        string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();

        protected void Page_Load(object sender, EventArgs e)
        {
            MenuDetails();
        }

        private void MenuDetails()
        {
            HtmlGenericControl dvMenuCont = this.FindControl("dvMenuCont") as HtmlGenericControl;
            List<MenuBE> objCategoryBE;
            try
            {

                UserBE objUserBE = new UserBE();
                objUserBE = Session["StoreUser"] as UserBE;
                
                objCategoryBE = MenuBL.GetAllStoreMenuDetails(objUserBE.RoleId,false);

                HtmlGenericControl parentUL = new HtmlGenericControl("ul");
                parentUL.Attributes.Add("class", "main-navigation-menu");

                //#region Home LI
                //HtmlGenericControl homeLI = new HtmlGenericControl("li");
                //HtmlAnchor homeAnchor = new HtmlAnchor();
                //homeAnchor.HRef = "javascript:void(0);";
                //HtmlGenericControl homeIcon = new HtmlGenericControl("i");
                //homeIcon.Attributes.Add("class","")
                //HtmlImage homeImage = new HtmlImage();
                //homeImage.Src = GlobalFunctions.GetVirtualPathAdmin() + "Images/UI/home.jpg";
                //homeImage.Height = 15;
                //homeImage.Width = 19;
                //homeAnchor.Controls.Add(homeImage);
                //homeLI.Controls.Add(homeAnchor);
                //parentUL.Controls.Add(homeLI);

                //#endregion

                List<MenuBE> objParentCategory = objCategoryBE.FindAll(x => x.ParentMenuId == 0);
                foreach (MenuBE parentCategory in objParentCategory)
                {
                    HtmlGenericControl parentLI = new HtmlGenericControl("li");
                    HtmlAnchor parentAnchor = new HtmlAnchor();
                    if (parentCategory.MenuUrl.ToLower().Contains("javascript") || parentCategory.MenuUrl.ToLower().Contains("#") || string.IsNullOrEmpty(parentCategory.MenuUrl))
                    {
                        parentAnchor.HRef = "javascript:void(0);";
                        HtmlGenericControl ArrowIcon = new HtmlGenericControl("i");
                        ArrowIcon.Attributes.Add("class", "icon-arrow");
                        parentAnchor.Controls.Add(ArrowIcon);
                    }
                    else
                        parentAnchor.HRef = hostAdmin + parentCategory.MenuUrl;
                    HtmlGenericControl Icon = new HtmlGenericControl("i");
                    Icon.Attributes.Add("class", parentCategory.IconName);
                    HtmlGenericControl MenuName = new HtmlGenericControl("span");
                    MenuName.Attributes.Add("class", "title");
                    MenuName.InnerText = parentCategory.MenuName;
                    //parentAnchor.InnerText = parentCategory.MenuName;
                    parentAnchor.Controls.Add(Icon);
                    parentAnchor.Controls.Add(MenuName);
                    parentLI.Controls.Add(parentAnchor);

                    List<MenuBE> objChildCategory = objCategoryBE.FindAll(x => x.ParentMenuId == parentCategory.MenuId);
                    if (objChildCategory.Count > 0)
                    {
                        //HtmlGenericControl span = new HtmlGenericControl("span");
                        //span.Attributes.Add("class", "caret");
                        //parentAnchor.Controls.Add(span);
                        HtmlGenericControl childUL = new HtmlGenericControl("ul");
                        childUL.Attributes.Add("class", "sub-menu");

                        foreach (MenuBE childCategory in objChildCategory)
                        {
                            HtmlGenericControl childLI = new HtmlGenericControl("li");
                            HtmlAnchor childAnchor = new HtmlAnchor();

                            if (childCategory.MenuUrl.ToLower().Contains("javascript") || childCategory.MenuUrl.ToLower().Contains("#") || string.IsNullOrEmpty(childCategory.MenuUrl))
                                childAnchor.HRef = "javascript:void(0);";
                            else
                                childAnchor.HRef = hostAdmin + childCategory.MenuUrl;

                            //childAnchor.HRef = hostAdmin + childCategory.MenuUrl;
                            HtmlGenericControl childMenuName = new HtmlGenericControl("span");
                            childMenuName.Attributes.Add("class", "title");
                            childMenuName.InnerText = childCategory.MenuName;
                            //childAnchor.InnerText = childCategory.MenuName;
                            childAnchor.Controls.Add(childMenuName);
                            childLI.Controls.Add(childAnchor);
                            SubMenuDetails(objCategoryBE, childCategory, childLI, childAnchor);
                            childUL.Controls.Add(childLI);
                        }
                        parentLI.Controls.Add(childUL);
                    }

                    parentUL.Controls.Add(parentLI);
                }
                this.Controls.Add(parentUL);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void SubMenuDetails(List<MenuBE> objCategoryBE, MenuBE parentCategory, HtmlGenericControl parentLI, HtmlAnchor parentAnchor)
        {
            List<MenuBE> objChildCategory = objCategoryBE.FindAll(x => x.ParentMenuId == parentCategory.MenuId);
            if (objChildCategory.Count > 0)
            {
                parentLI.Style.Add("z-index", "10");
                parentAnchor.Attributes.Add("class", "abc");
                HtmlGenericControl span = new HtmlGenericControl("span");
                span.Attributes.Add("class", "caret");
                parentAnchor.InnerText += " ";
                parentAnchor.Controls.Add(span);
                HtmlGenericControl childUL = new HtmlGenericControl("ul");
                childUL.Attributes.Add("class", "dropdown-menu");

                foreach (MenuBE childCategory in objChildCategory)
                {
                    HtmlGenericControl childLI = new HtmlGenericControl("li");
                    HtmlAnchor childAnchor = new HtmlAnchor();
                    if (childCategory.MenuUrl.ToLower().Contains("javascript") || childCategory.MenuUrl.ToLower().Contains("#") || string.IsNullOrEmpty(childCategory.MenuUrl))
                        childAnchor.HRef = "javascript:void(0);";
                    else
                        childAnchor.HRef = hostAdmin + childCategory.MenuUrl;
                    //childAnchor.HRef = hostAdmin + childCategory.MenuUrl;
                    childAnchor.InnerText = childCategory.MenuName;
                    childLI.Controls.Add(childAnchor);
                    SubMenuDetails(objCategoryBE, childCategory, childLI, childAnchor);
                    childUL.Controls.Add(childLI);
                }
                parentLI.Controls.Add(childUL);
            }
            else
                parentAnchor.Attributes.Add("class", "xyz");
        }
    }

}