﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Presentation
{
    public class Admin_Master_AdminMaster : System.Web.UI.MasterPage
    {
        //public string StoreVersion = GlobalFunctions.GetSetting("StoreVersion");
        public string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        protected global::System.Web.UI.WebControls.Literal litJS, litCSSnJS, ltrUserName, ltrVersion;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkViewFrontEnd;

        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        /*Sachin Chauhan End : 22 02 2016 */

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 18-08-15
        /// Scope   : Page_PreInit event of the page
        /// Modified by : Sachin Chauhan
        /// Modified Date : 22 02 2016
        /// Modify Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void Page_Init(object sender, EventArgs e)
        {
            /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
            if (hostAdmin.Contains("localhost"))
            {

            }
            else
            {
                #region "CSRF prevention code"
                try
                {
                    //First, check for the existence of the Anti-XSS cookie
                    var requestCookie = Request.Cookies[AntiXsrfTokenKey];
                    Guid requestCookieGuidValue;

                    //If the CSRF cookie is found, parse the token from the cookie.
                    //Then, set the global page variable and view state user
                    //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
                    //method.
                    if (requestCookie != null
                    && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
                    {
                        //Set the global token variable so the cookie value can be
                        //validated against the value in the view state form field in
                        //the Page.PreLoad method.
                        _antiXsrfTokenValue = requestCookie.Value;

                        //Set the view state user key, which will be validated by the
                        //framework during each request
                        Page.ViewStateUserKey = _antiXsrfTokenValue;
                    }
                    //If the CSRF cookie is not found, then this is a new session.
                    else
                    {
                        //Generate a new Anti-XSRF token
                        _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                        //Set the view state user key, which will be validated by the
                        //framework during each request
                        Page.ViewStateUserKey = _antiXsrfTokenValue;

                        //Create the non-persistent CSRF cookie
                        var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                        {
                            //Set the HttpOnly property to prevent the cookie from
                            //being accessed by client side script
                            HttpOnly = true,

                            //Add the Anti-XSRF token to the cookie value
                            Value = _antiXsrfTokenValue
                        };

                        //If we are using SSL, the cookie should be set to secure to
                        //prevent it from being sent over HTTP connections
                        if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                            responseCookie.Secure = true;

                        //Add the CSRF cookie to the response
                        Response.Cookies.Set(responseCookie);
                    }

                    Page.PreLoad += master_Page_PreLoad;
                }
                catch (Exception ex)
                {
                    Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
                    Exceptions.WriteExceptionLog(ex);
                    //throw;
                }
                #endregion
            }
            
            /*Sachin Chauhan End : 22 02 2016*/
           
            try
            {
                //string pageName = this.Page.Request.FilePath;
                //if (pageName != "/Admin/Login/Login.aspx" && pageName != "/Admin/Login/ResetPassword.aspx")
                //{
                    if (Session["StoreUser"] == null)
                    {
                        if (hostAdmin.Contains("localhost"))
                        {
                            StoreBE objStoreBE = new StoreBE();
                            UserBE objBE = new UserBE();
                            UserBE objUser = new UserBE();
                            UserBE objBEs = new UserBE();
                            objBEs.EmailId = "sanchit.patne@powerweave.com";
                            objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                            objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                            objBE.StoreId = objStoreBE.StoreId;
                            objUser = UserBL.GetMCPLoginDetailFromCP(false, objBE);
                        }
                        else
                        {
                            Response.Redirect(hostAdmin + "Login/Login.aspx", true);
                        }                        
                    }
                        
                    else
                    {
                        UserBE objUserBE = new UserBE();
                        objUserBE = Session["StoreUser"] as UserBE;
                        ltrUserName.Text = "Hello " + objUserBE.FirstName;
                        StoreBE objStoreBE = StoreBL.GetStoreDetails();
                        lnkViewFrontEnd.HRef = objStoreBE.StoreLink;
                        lnkViewFrontEnd.InnerText = " View " + objStoreBE.StoreName +" FrontEnd ";
                        ltrVersion.Text = "Version " +objStoreBE.Version;
                    }
                //}

                if (GlobalFunctions.GetLanguageId() == 0 || GlobalFunctions.GetCurrencyId() == 0 || string.IsNullOrEmpty(GlobalFunctions.GetCurrencySymbol()))
                {
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();

                    GlobalFunctions.SetLanguageId(objStoreBE.StoreLanguages.FirstOrDefault(x => x.IsDefault == true).LanguageId);
                    GlobalFunctions.SetCurrencyId(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyId);
                    GlobalFunctions.SetCurrencySymbol(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencySymbol);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date : 22 02 2016
        /// Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            //During the initial page load, add the Anti-XSRF token and user
            //name to the ViewState
            if (!IsPostBack)
            {
                
                //Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

                //If a user name is assigned, set the user name
                //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                if (Session["ContextUserGUID"] == null)
                    Session["ContextuserGUID"] = Guid.NewGuid().ToString();

                ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
            }
            //During all subsequent post backs to the page, the token value from
            //the cookie should be validated against the token in the view state
            //form field. Additionally user name should be compared to the
            //authenticated users name
            else
            {
                //Validate the Anti-XSRF token
                //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
              //  if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())

                    Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                litJS.Text = "  <script src='" + hostAdmin + "JS/jquery.js'></script>" +
                    "<script src='" + hostAdmin + "JS/bootstrap.min.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery.smartmenus.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery.smartmenus.bootstrap.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery-ui.js'></script>" +
                    "<script src='" + hostAdmin + "JS/html5shiv.js'></script>" +
                    "<script src='" + hostAdmin + "FineUploader/jquery.fineuploaderCategory-3.2.js'></script>";

                litCSSnJS.Text = "<title></title><link href='" + hostAdmin + "CSS/bootstrap.min.css' rel='stylesheet' />"
                    + "<link href='" + hostAdmin + "CSS/jquery.smartmenus.bootstrap.css' rel='stylesheet' />"
                    + "<link href='" + hostAdmin + "CSS/common-admin.css' rel='stylesheet'/>"
                    + "<link href='" + hostAdmin + "CSS/radio.css' rel='stylesheet'/>"
                    + "<link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>"
                    + "<link href='http://fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css'>"
                    + "<link href='" + hostAdmin + "FineUploader/fineuploaderCategory-3.2.css' rel='stylesheet' type='text/css'>";

                if (Session["StoreUser"] != null)
                {
                    Response.ClearHeaders();
                    Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                    Response.AddHeader("Pragma", "no-cache");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}
