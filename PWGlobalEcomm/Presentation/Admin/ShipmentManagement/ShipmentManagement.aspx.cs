﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using PWGlobalEcomm.BusinessEntity;

namespace Presentation
{

    public partial class Admin_ShipmentManagement_ShipmentManagement : BasePage
    {
        protected global::System.Web.UI.WebControls.DropDownList ddlCountry;
        protected global::System.Web.UI.WebControls.Label lblCountryCode;
        protected global::System.Web.UI.WebControls.GridView gvShipCountry;
        protected global::System.Web.UI.WebControls.CheckBoxList chkListFreightTable;
        public List<FreightManagementBE.FreightTables> LstFreightTables;
        FreightSourceCountryMasterBE objFreight = new FreightSourceCountryMasterBE();

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divChkFreightTables;

        StoreBE objStoreBE = StoreBL.GetStoreDetails();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateCountryDropDownList();
                GetShipCountry();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ADD();
        }

        private void ADD()
        {
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                objFreight.FreightSourceCountryCode = ddlCountry.SelectedValue.Trim();
                objFreight.FreightSourceCountryName = ddlCountry.SelectedItem.Text.Trim();

                string strVal = string.Empty;
                //if (!objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                //{
                //List<ListItem> selected = chkListFreightTable.Items.Cast<ListItem>().Where(li => li.Selected).ToList();                
                for (int i = 0; i < chkListFreightTable.Items.Count; i++)
                {
                    if (chkListFreightTable.Items[i].Selected)
                    {
                        if (string.IsNullOrEmpty(strVal))
                        {
                            strVal = chkListFreightTable.Items[i].Value;
                        }
                        else
                        {
                            strVal = strVal + "," + chkListFreightTable.Items[i].Value;
                        }
                    }
                }
                objFreight.FreightTableID = strVal;
                //}
                int res = FreightManagementBL.InsertFreightSourceCountry(objFreight, dictParams);
                if (res == 1)
                {
                    GetShipCountry();
                    CreateActivityLog("ShipmentManagement btnAdd", "Insert", ddlCountry.SelectedValue);
                    for (int i = 0; i < chkListFreightTable.Items.Count; i++)
                    {
                        chkListFreightTable.Items[i].Selected = false;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated Successfully", AlertType.Success);
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Added Successfully", AlertType.Success);
                    }
                }
                ddlCountry.SelectedIndex = 0;
                lblCountryCode.Text = string.Empty;
                divChkFreightTables.Visible = false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    divChkFreightTables.Visible = true;
                }

                lblCountryCode.Text = ddlCountry.SelectedValue;

                LstFreightTables = FreightManagementBL.GetAllFreightTables<FreightManagementBE.FreightTables>();

                chkListFreightTable.DataSource = LstFreightTables;
                chkListFreightTable.DataTextField = "TableName";
                chkListFreightTable.DataValueField = "FreightTableId";
                chkListFreightTable.DataBind();

                //for (int i = 0; i < chkListFreightTable.Items.Count; i++)
                //{

                //}
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void PopulateCountryDropDownList()
        {
            try
            {
                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                if (lstCountry.Count > 0 && lstCountry != null)
                {
                    ddlCountry.DataTextField = "CountryName";
                    ddlCountry.DataValueField = "CountryCode";
                    ddlCountry.DataSource = lstCountry;

                    // Modified by Hardik on "18/May/2017" for Freight Phase 2 changes 
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                    {
                        ddlCountry.DataSource = lstCountry.Where(x => x.CountryCode == "DE" || x.CountryCode == "GB").ToList();
                        ddlCountry.DataBind();
                    }
                    else
                    {
                        ddlCountry.DataSource = lstCountry;
                        ddlCountry.DataBind();
                    }

                    ddlCountry.Items.Insert(0, new ListItem("--Select--", ""));

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GetShipCountry()
        {
            try
            {
                List<object> lstFreight = new List<object>();
                lstFreight = FreightManagementBL.GetFreightSourceCountryMasterList();

                gvShipCountry.DataSource = lstFreight;
                gvShipCountry.DataBind();
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    gvShipCountry.Columns[2].Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Added By Hardik - 19/May/2017
        /// To delete FreightSourceCountry from tbl_FreightSourceCountryMaster
        /// </summary>
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkDelete = (LinkButton)sender;
                string strFreightSourceCountryCode = lnkDelete.CommandArgument;

                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                objFreight.FreightSourceCountryCode = strFreightSourceCountryCode;

                int res = FreightManagementBL.DeleteFreightSourceCountry(objFreight, dictParams);
                if (res == 1)
                {
                    GetShipCountry();
                    CreateActivityLog("ShipmentManagement lnkDelete_Click", "Delete", strFreightSourceCountryCode);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Deleted Successfully", AlertType.Success);
                }
                else if(res==2)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Change Default Country And Delete", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rdbIsDefault_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int Success = 0;
                foreach (GridViewRow rows in gvShipCountry.Rows)
                {
                    HiddenField hdnId = (HiddenField)rows.FindControl("hdnFreightSourceCountryId");
                    RadioButton rbdIsDefault = (RadioButton)rows.FindControl("rdbIsDefault");
                    if (rbdIsDefault.Checked == true)
                    {
                        Dictionary<string, string> DInstance = new Dictionary<string, string>();
                        objFreight.IsDefault = rbdIsDefault.Checked;
                        objFreight.ID = Convert.ToInt16(hdnId.Value);
                        Success = FreightManagementBL.UpdateIsDefault(objFreight, DInstance);
                    }
                }
                if (Success == 1)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Is Default Is Successfully Update", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There Is Problem While Updating Default Value", AlertType.Failure);
                }
                GetShipCountry();
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnEdit = (Button)sender;
                string Id = btnEdit.CommandArgument;
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                objFreight = FreightManagementBL.GetFreightSourceCountryMaster(Id);
                ddlCountry.SelectedValue = objFreight.FreightSourceCountryCode;
                lblCountryCode.Text = objFreight.FreightSourceCountryCode;
                if (!objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    divChkFreightTables.Visible = true;
                }
                LstFreightTables = FreightManagementBL.GetAllFreightTables<FreightManagementBE.FreightTables>();

                chkListFreightTable.DataSource = LstFreightTables;
                chkListFreightTable.DataTextField = "TableName";
                chkListFreightTable.DataValueField = "FreightTableId";
                chkListFreightTable.DataBind();
                string[] str = objFreight.FreightTableID.Split(',');
                for(int i=0;i<=str.Length-1;i++)
                {
                    foreach(ListItem item in chkListFreightTable.Items)
                    {
                        if(item.Value==str[i])
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}