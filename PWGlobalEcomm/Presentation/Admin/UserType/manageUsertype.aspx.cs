﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
using Presentation;

public partial class Admin_UserType_manageUsertype : BasePage // System.Web.UI.Page
{
    protected global::System.Web.UI.WebControls.TextBox txtuserTypeName, txtVatRate;
    protected global::System.Web.UI.WebControls.Repeater rptViewUserType;
    protected global::System.Web.UI.WebControls.Panel pnlViewAdd, pnlAdd;
    protected global::System.Web.UI.WebControls.DropDownList ddlLanguage, ddlCurrencies, ddlCatalogues;
    protected global::System.Web.UI.WebControls.CheckBox chkVatDisplay;
    protected global::System.Web.UI.WebControls.GridView gvUsertypes;
    protected global::System.Web.UI.WebControls.HiddenField  hdnfUserTypeID, hdnfCurrencyID, hdnfSelectedCatalogueid;
    protected global::System.Web.UI.WebControls.Button btnAddUserTypes,btnCancel,btnEditUpdate,btnEditCancel,btnEdit,btnDelete;
    protected global::System.Web.UI.WebControls.Label lblHeading;
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divaddFields;

    UserTypesBE objUserTypesBE = new UserTypesBE();
    UserTypesBE objlstUserTypesBE = new UserTypesBE();
    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    List<UserTypesDetailsBE> lstUTDBE = new List<UserTypesDetailsBE>();
    UserTypesDetailsBE objUserTypesDetailsBE = new UserTypesDetailsBE();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //BindData();
                HttpRuntime.Cache.Remove("StoreDetails");
                StoreBE objStoreBE = new StoreBE();
                objStoreBE = StoreBL.GetStoreDetails();
                GlobalFunctions.BindAllLanguage(objStoreBE, ref ddlLanguage);
                BindAllUserTypeDetails();
                BindCurrencies();
                BindCatalogue();
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void BindCurrencies()
    {
        HttpRuntime.Cache.Remove("StoreDetails");
        StoreBE objStoreBE = new StoreBE();
        objStoreBE = StoreBL.GetStoreDetails();
        if (objStoreBE.StoreCurrencies.Count > 0)
        {
            //objCur = (from a in objStoreBE.StoreCurrencies.AsEnumerable()
            //          where objLstCatalogueBE.lstCatalogues.Any(xc => xc.CURRENCY_COUNTRY.Contains(a.CurrencyCode))
            //          select a).ToList();

            List<StoreBE.StoreCurrencyBE> objCur = new List<StoreBE.StoreCurrencyBE>();
            objCur = objStoreBE.StoreCurrencies;

            //var aCurrency = (from y in objCur
            //                 select new { y.CurrencyId, y.CurrencyName });
            var aCurrency = (from y in objCur
                             select new { y.CurrencyCode, y.CurrencyName });

            //ddlCurrencies.DataSource = objStoreBE.StoreCurrencies.Distinct();
            ddlCurrencies.DataSource = aCurrency.Distinct();
            ddlCurrencies.DataTextField = "CurrencyName";
            ddlCurrencies.DataValueField = "CurrencyCode";// "CurrencyID";
            //ddlCurrencies.DataValueField = "CurrencyID";// "CurrencyID";
            ddlCurrencies.DataBind();
            GlobalFunctions.AddDropdownItem(ref ddlCurrencies);
        }
    }

    //private void BindData()
    //{
    //    try
    //    {
    //        UserTypesBE objlstUserTypesBE = new UserTypesBE();
    //        objlstUserTypesBE = UserTypesBL.getCollectionItem(Constants.USP_ManageUserType_SAED, null, true);
    //        if (objlstUserTypesBE != null)
    //        {
    //            rptViewUserType.DataSource = objlstUserTypesBE;
    //            rptViewUserType.DataBind();
    //            visiblePanel(true);
    //        }
    //    }
    //    catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    //}
    //private void visiblePanel(bool bVisible)
    //{
    //    try
    //    {
    //        pnlView.Visible = bVisible;
    //        pnlAdd.Visible = !bVisible;
    //    }
    //    catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    //}

    /// <summary>
    /// Author :    SHRIGANESH SINGH
    /// Scope  :    To clear the Details in Add User Type Section
    /// </summary>
    private void ClearData()
    {
        try
        {
            hdnfUserTypeID.Value = "";
            txtuserTypeName.Text = "";
            ddlCurrencies.SelectedIndex = 0;
            ddlCurrencies.Enabled = true;
            txtVatRate.Text = "";
            ddlCatalogues.SelectedIndex = 0;
            ddlCatalogues.Enabled = true;
            lblHeading.Text = "Add User Type";
            ddlLanguage.Enabled = false;
            divaddFields.Visible = true;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex); 
        }
    }

    /// <summary>
    /// Author  :   SHRIGANESH SINGH
    /// Scope   :   To bind all the User Types and their details for editing
    /// </summary>
    private void BindAllUserTypeDetails()
    {
        try
        {
            int LanguageID = Convert.ToInt16(ddlLanguage.SelectedValue);
            UserTypesBL objUserTypeBL = new UserTypesBL();
            lstUTDBE = UserTypesBL.GetAllUserTypeDetailsList(Constants.USP_GetAllUserTypeDetailForEdit, LanguageID);
            if (lstUTDBE != null)
            {
                gvUsertypes.DataSource = lstUTDBE;
                gvUsertypes.DataBind();
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void UpdateUserTypeName()
    {
        try
        {
           
                    int ires = 0;

                    Dictionary<string, string> dictParams = new Dictionary<string, string>();
                    if (Convert.ToString(hdnfUserTypeID.Value) != null)
                    {
                        dictParams.Add("UserTypeID", hdnfUserTypeID.Value);
                    }
                    else
                    {
                        dictParams.Add("UserTypeID", "0");
                    }

                    dictParams.Add("Name", txtuserTypeName.Text.Trim());
                    dictParams.Add("CatalogueID", "0");
                    
                    if (chkVatDisplay.Checked)
                    {
                        dictParams.Add("IsVatDisplay", "true");
                        dictParams.Add("VatRate","");
                    }
                    else
                    {
                        dictParams.Add("IsVatDisplay", "false");
                    }
                    UserBE objUser = new UserBE();
                    objUser = Session["StoreUser"] as UserBE;
                    dictParams.Add("CreatedBy", objUser.UserId.ToString());
                    dictParams.Add("ModifiedBy", objUser.UserId.ToString());
                    dictParams.Add("LanguageId", ddlLanguage.SelectedValue);

                    ires = UserTypesBL.ManageUserTypes_SAED(objUserTypesBE, dictParams, DBAction.Update);
                                        
                    if (ires > 0)
                    {
                        ClearData();
                        GlobalFunctions.ShowModalAlertMessages(this, "User Type Name Updated Successfully !!", AlertType.Success);
                        BindAllUserTypeDetails();
                    }
                    else if (ires == 3)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this, "User Type Already Exists !!", AlertType.Warning);
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this, "Error while updating User Type !!", AlertType.Warning);
                    }
                }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

   protected void Add()
    {
        try
        {
            if (ddlCurrencies.SelectedIndex > 0)
            {
                if (ddlCatalogues.SelectedIndex > 0)
                {
                    if (chkVatDisplay.Checked)
                    {
                        if (txtVatRate.Text == "" || txtVatRate.Text == null)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this, "Please Enter the Vat Percentage !!", AlertType.Warning);
                            return;
                        }
                    }
                    int ires = 0;

                    #region Commented Code 16 June 2016 SHRIGANESH SINGH
                    //StoreBE objStoreBE = new StoreBE();
                    //objStoreBE = StoreBL.GetStoreDetails();
                    //UserTypesBE objUserTypesBE = new UserTypesBE();
                    //UserTypesBE.UserTypeLanguages objUserTypeLanguages = new UserTypesBE.UserTypeLanguages();
                    //UserTypesBE
                    ////short LanguageID = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                    ////long CatalogueID = Convert.ToInt64(ddlCatalogues.SelectedItem);
                    ////objUserTypesBE.objUserTypeLanguages.LanguageID = LanguageID;
                    ////objUserTypesBE.objUserTypeCatalogue.UserTypeCatalogueID = CatalogueID;
                    //objUserTypeLanguages.LanguageID = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                    //objUserTypesBE.l
                    ////objUserTypesBE.objUserTypeLanguages.UserTypeName = txtuserTypeName.Text.Trim(); 
                    #endregion

                    Dictionary<string, string> dictParams = new Dictionary<string, string>();
                    if(Convert.ToString(hdnfUserTypeID.Value)!=null)
                    {
                        dictParams.Add("UserTypeID", hdnfUserTypeID.Value);
                    }
                    else
                    {
                        dictParams.Add("UserTypeID", "0");
                    }
                    dictParams.Add("Name", txtuserTypeName.Text.Trim());
                    dictParams.Add("CatalogueID", ddlCatalogues.SelectedValue);
                    
                    if (chkVatDisplay.Checked)
                    {
                        dictParams.Add("IsVatDisplay", "true");
                        dictParams.Add("VatRate", txtVatRate.Text.Trim());
                    }
                    else
                    {
                        dictParams.Add("IsVatDisplay", "false");
                    }
                    UserBE objUser = new UserBE();
                    objUser = Session["StoreUser"] as UserBE;
                    dictParams.Add("CreatedBy", objUser.UserId.ToString());
                    dictParams.Add("ModifiedBy", objUser.UserId.ToString());
                  

                    ires = UserTypesBL.ManageUserTypes_SAED(objUserTypesBE, dictParams, DBAction.Insert);
                    
                    if (ires == 1)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this, "User Type Added successfully !!", AlertType.Success);
                        BindAllUserTypeDetails();
                        ClearData();
                    }
                    else if (ires == 3)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this, "User Type Already Exists !!", AlertType.Warning);
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this, "Error while creating User Type !!", AlertType.Warning);
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Please select one catalogue for this User Type !!", AlertType.Warning);
                }
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this, "Please select one currency for this User Type !!", AlertType.Warning);
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }


   protected void btnSubmit_Click(object sender, EventArgs e)
   {
       if(Convert.ToString(hdnfUserTypeID.Value)!="")
       {
           UpdateUserTypeName();
           CreateActivityLog("manageUserType UpdateUserTypeName btnSubmit", "Update", "");
       }
       else
       {
           Add();
           CreateActivityLog("manageUserType Add btnSubmit", "Insert", "");
       }
     
   }

   protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
   {
       BindAllUserTypeDetails();
       ReadData(hdnfUserTypeID, hdnfCurrencyID, Convert.ToInt16(hdnfUserTypeID.Value));
   }

    protected void ddlCurrencies_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            hdnfSelectedCatalogueid.Value = "";
            if (ddlCurrencies.SelectedValue == "0")
            {
               
                ddlCatalogues.SelectedIndex = 0;
                ddlCatalogues.Enabled = false;
            }
            else
            {
                BindCatalogue();
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void BindCatalogue()
    {
        ddlCatalogues.Enabled = true;
        HttpRuntime.Cache.Remove("StoreDetails");
        StoreBE objStoreBE = new StoreBE();
        objStoreBE = StoreBL.GetStoreDetails();

        List<StoreBE.StoreCurrencyBE> objCur1 = new List<StoreBE.StoreCurrencyBE>();
        objCur1 = objStoreBE.StoreCurrencies;

        //Int16 SelectedCurrencyId = Convert.ToInt16(ddlCurrencies.SelectedValue);
       // objCur1.RemoveAll(x => x.CurrencyCode != ddlCurrencies.SelectedValue);
        objCur1 = objCur1.FindAll(x => x.CurrencyCode == ddlCurrencies.SelectedValue);
        ddlCatalogues.DataSource = objCur1;
        ddlCatalogues.DataTextField = "CatalogueId";
        ddlCatalogues.DataValueField = "CatalogueId";
        ddlCatalogues.DataBind();
        if(Convert.ToString(hdnfSelectedCatalogueid.Value)!="")
        {
           ddlCatalogues.SelectedValue = hdnfSelectedCatalogueid.Value;
        }
        GlobalFunctions.AddDropdownItem(ref ddlCatalogues);
    }

    protected void btnAddUserType_OnClick(object sender, EventArgs e)
    {
        try
        {
            int ires = 0;
            UserTypesBE objUserTypesBE = new UserTypesBE();
            //objUserTypesBE.Name = txtuserTypeName.Text.Trim();
            // ires = UserTypesBL.ManageUserTypes_SAED(objUserTypesBE, DBAction.Insert);
            if (ires > 0)
            {
                ClearData();
                //BindData();
                GlobalFunctions.ShowModalAlertMessages(this, "User Type created successfully !!", AlertType.Success);
                CreateActivityLog("manageUserType btnAddUserType", "Insert", "");
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this, "Error while creating User Type !!", AlertType.Warning);
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex); 
        }
    }

    protected void btnCancel_OnClick(object sender, EventArgs e)
    {
        try
        {
            ClearData();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    //protected void btnDisplayEditPanel_OnClick(object sender, EventArgs e)
    //{
    //    pnlEdit.Visible = true;
    //}

   
    protected void gvUsertypes_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
           
            GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
            HiddenField hfUTID = row.FindControl("hfUserTypeID") as HiddenField;
            HiddenField hfCurrID = row.FindControl("hfCurrencyID") as HiddenField;
            Int16 UserTypeID = Convert.ToInt16(e.CommandArgument);
            hdnfUserTypeID.Value = hfUTID.Value;
           // hdnfCurrencyID.Value = hfCurrID.Value;
            if (e.CommandName.ToLower() == "edits")
            {
                ReadData(hfUTID, hfCurrID, UserTypeID);
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    //protected void btnEditUpdate_OnClick(object sender, EventArgs e)
    //{

     //   Save();
        //try
        //{
        //    if (txtuserTypeName.Text != null || txtuserTypeName.Text != "")
        //    {
        //        if (chkVatDisplayEdit.Checked == true)
        //        {
        //            if (txtVatRate.Text == null || txtVatRate.Text == "")
        //            {
        //                GlobalFunctions.ShowModalAlertMessages(this, "Please enter the Vat Rate Value !!", AlertType.Warning);
        //                return;
        //            }
        //        }
        //        int iResult = 0;
        //        Dictionary<string, string> dictParams = new Dictionary<string, string>();
        //        dictParams.Add("UserTypeID", hdnfUserTypeID.Value);
        //        dictParams.Add("UserTypeName", txtuserTypeName.Text.Trim());
        //        dictParams.Add("LanguageID", ddlLanguage.SelectedValue);
        //        if (chkVatDisplayEdit.Checked)
        //        {
        //            dictParams.Add("IsVatDisplay", "true");
        //            dictParams.Add("VatRate", txtuserTypeName.Text.Trim());
        //        }
        //        else
        //        {
        //            dictParams.Add("IsVatDisplay", "false");
        //            dictParams.Add("VatRate", "0");
        //        }
        //        dictParams.Add("CatalogueID", lblcatalogueIDValueEdit.Text);
        //        dictParams.Add("CurrencyID", hdnfCurrencyID.Value);
        //        dictParams.Add("LanguageId", ddlLanguage.SelectedValue);
        //       // iResult = UserTypesBL.UpdateUsertTypeByUserTypeID(Constants.USP_UpdateUserTyepDetailsByUserTypeID, dictParams);
        //        iResult = UserTypesBL.ManageUserTypes_SAED(objUserTypesBE,dictParams,DBAction.Update);
        //        if (iResult == 2)
        //        {
        //            GlobalFunctions.ShowModalAlertMessages(this, "User Type Updated successfully !!", AlertType.Success);
        //        }
        //        else if(iResult == 3)
        //        {
        //            GlobalFunctions.ShowModalAlertMessages(this, "User Type Name already Exists. !!", AlertType.Warning);
        //        }
        //        else
        //        {
        //            GlobalFunctions.ShowModalAlertMessages(this, "Error while updating User Type !!", AlertType.Warning);
        //        }
        //    }
        //    else
        //    {
        //        GlobalFunctions.ShowModalAlertMessages(this, "Please enter the User Type Name !!", AlertType.Warning);
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Exceptions.WriteExceptionLog(ex);
        //}
  //  }

    protected void ReadData(HiddenField hfUTID, HiddenField hfCurrID,Int16 UserTypeId)
    {
        UserTypesBE.UserTypeCatalogue objUserTypeCatalogue = new UserTypesBE.UserTypeCatalogue();
        objUserTypeCatalogue.UserTypeID = UserTypeId;
        string LanguageID = ddlLanguage.SelectedValue;
        hfUTID.Value = Convert.ToString(objUserTypeCatalogue.UserTypeID);
        hdnfUserTypeID.Value = Convert.ToString(objUserTypeCatalogue.UserTypeID);
        objUserTypesDetailsBE = UserTypesBL.GetAllUserTypeDetailsByUserTypeID(Constants.USP_GetAllUserTypeDetailsByUserTypeID, LanguageID, objUserTypeCatalogue);

        if (objUserTypesDetailsBE != null)
        {
            txtuserTypeName.Text = objUserTypesDetailsBE.UserTypeName;
            divaddFields.Visible = false;
            //if (objUserTypesDetailsBE.VatDisplay)
            //{
            //    chkVatDisplay.Checked = true;
            //    txtVatRate.Text = objUserTypesDetailsBE.Vatrate.ToString();
            //}
            //else
            //{
            //    chkVatDisplay.Checked = false;
            //    txtVatRate.Text = objUserTypesDetailsBE.Vatrate.ToString();
            //}
            ////  BindCurrencies();

            //ddlCurrencies.SelectedValue = Convert.ToString(objUserTypesDetailsBE.CurrencyCode);
            //ddlCurrencies.Enabled = false;
            //hdnfSelectedCatalogueid.Value = objUserTypesDetailsBE.UserTypeCatalogueID.ToString();
            //BindCatalogue();
            //// ddlCatalogues.SelectedValue = Convert.ToString(objUserTypesDetailsBE.UserTypeCatalogueID);
            //ddlCatalogues.Enabled = false;

            //// lblcatalogueIDValueEdit.Text = objUserTypesDetailsBE.UserTypeCatalogueID.ToString();
            //// lblCurrencyCodeValueEdit.Text = objUserTypesDetailsBE.CurrencyCode.ToString();
            //hfCurrID.Value = Convert.ToString(objUserTypesDetailsBE.CurrencyId);
           // hdnfCurrencyID.Value = Convert.ToString(objUserTypesDetailsBE.CurrencyId);
            lblHeading.Text = "Edit User Type";
            ddlLanguage.Enabled = true;
        }
    }
    
}