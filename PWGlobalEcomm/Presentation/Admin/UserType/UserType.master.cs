﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;

public partial class Admin_UserType_UserType : System.Web.UI.MasterPage
{
    protected global::System.Web.UI.WebControls.RadioButtonList rdListOptonMaster;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            List<UserTypeMappingBE.UserTypeMasterOptions> obj = UserTypesBL.GetAllUserTypeMasterOptions(Constants.USP_GetAllUserTypeMasterOptions, null, true);
            rdListOptonMaster.DataSource = obj;
            rdListOptonMaster.DataTextField = "TypeName";
            rdListOptonMaster.DataValueField = "UserTypeMappingTypeID";
            rdListOptonMaster.DataBind();
        }
    }
}
