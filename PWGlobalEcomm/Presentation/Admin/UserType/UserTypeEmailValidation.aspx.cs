﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.Globalization;
using System.Net;
using System.Text;
using System.Reflection;
namespace Presentation
{

    public partial class Admin_UserType_UserTypeEmailValidation : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.GridView gvList;
        protected global::System.Web.UI.WebControls.Literal ltrAllRecords;

        UserBE objUserBE;
        public int UserId;
        UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["StoreUser"] != null)
            {
                objUserBE = Session["StoreUser"] as UserBE;
                UserId = objUserBE.UserId;
            }
            if (!IsPostBack)
            {
                BindUserTypeList();
            }
        }

        private int BindUserTypeList()
        {
            int TotalRow = 0;
            try
            {

                List<UserTypesBE.CustomUserTypes> lstUserTypesBE = UserTypesBL.GetListUserTypes<UserTypesBE.CustomUserTypes>(objUserTypesBE);
                if (lstUserTypesBE != null)
                {
                    if (lstUserTypesBE.Count > 0)
                    {
                        gvList.DataSource = lstUserTypesBE;
                        ltrAllRecords.Text = lstUserTypesBE[0].TotalRow.ToString();
                        gvList.DataBind();
                    }
                    else
                    {
                        gvList.DataSource = null;
                        gvList.DataBind();
                        ltrAllRecords.Text = "0";
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return TotalRow;
        }


        protected bool ValidateFile(string Filepath)
        {
            bool Isvalidated = false;

            string csvData = File.ReadAllText(Filepath);

            //if (chkIsWhiteListed.Checked)
            //{
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            List<string> emaillist = new List<string>();
            foreach (string row in csvData.Split('\n'))
            {
                if (!string.IsNullOrEmpty(row))
                {
                    foreach (string cell in row.Split(','))
                    {
                        string data = cell.Replace("\r", "");
                        Match match = regex.Match(data);
                        if (match.Success)
                        {
                            Isvalidated = true;
                        }
                        else
                        {
                            emaillist.Add(data);
                            Isvalidated = false;
                            goto EndFunction;
                        }
                    }
                }
            EndFunction: if (!Isvalidated) break;
            }
            //}
            #region Domain validation
            //else if (RdWhiteList.SelectedValue == "2")
            //{
            //    foreach (string row in csvData.Split('\n'))
            //    {
            //        if (!string.IsNullOrEmpty(row))
            //        {
            //            Regex regex = new Regex("^(?:[-A-Za-z0-9]+\\.)+[A-Za-z]{2,6}$");
            //            foreach (string cell in row.Split(','))
            //            {
            //                string data = cell.Replace("\r", "");
            //                Match match = regex.Match(data);
            //                if (match.Success)
            //                {
            //                    Isvalidated = true;
            //                }
            //                else
            //                {
            //                    Isvalidated = false;
            //                    goto EndFunction;
            //                }
            //            }
            //        }
            //    EndFunction: if (!Isvalidated) break;
            //    }
            //}
            #endregion

            return Isvalidated;
        }

        protected bool IsFileValidated(CheckBox chkIsWhiteListed, string FilePath)
        {
            bool Isvalidated = true;
            try
            {
                Isvalidated = ValidateFile(FilePath);
                if (Isvalidated)
                {
                    Isvalidated = true;
                }
                else
                {
                    Isvalidated = false;
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/InvalidData"), AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Isvalidated;
        }

        public DataTable CsvFileToDatatable(string path, bool IsFirstRowHeader)
        {
            string header = "No";
            string sql = string.Empty;
            DataTable dataTable = null;
            string pathOnly = string.Empty;
            string fileName = string.Empty;
            try
            {
                pathOnly = Path.GetDirectoryName(path);
                fileName = Path.GetFileName(path);
                sql = @"SELECT * FROM [" + fileName + "]";
                if (IsFirstRowHeader)
                {
                    header = "true";
                }
                using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathOnly +
                ";Extended Properties=\"Text;HDR=" + header + "\""))
                {
                    using (OleDbCommand command = new OleDbCommand(sql, connection))
                    {
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
                        {
                            dataTable = new DataTable();
                            dataTable.Locale = CultureInfo.CurrentCulture;
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
            }
            return dataTable;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                Int16 UserTypeId = Convert.ToInt16(btn.CommandArgument);
                foreach (GridViewRow row in gvList.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        int UserTypeID = Convert.ToInt32(gvList.DataKeys[row.RowIndex].Values[0]);
                        FileUpload flImport = (FileUpload)row.FindControl("flImport");
                        CheckBox chk = (CheckBox)row.FindControl("chkIsWhiteListed");
                        if (chk != null && (UserTypeId == UserTypeID))
                        {
                            ImportFile(chk, flImport, UserTypeID);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }

        }

        protected void ImportFile(CheckBox chkIsWhiteListed, FileUpload flImport, int UserTypeId)
        {



            try
            {
                if (flImport.HasFiles)
                {
                    if (flImport.PostedFile.ContentLength > 0)
                    {

                        //Chk for File Extension
                        string[] strFileTypes = { ".csv" };
                        string[] strMimeTypes = { "text/csv", "application/vnd.ms-excel" };
                        bool bChkFileType = false;
                        bChkFileType = GlobalFunctions.CheckFileExtension(flImport, strFileTypes);
                        //bool bChkFileMimeType = false;
                        //bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(flImport, strMimeTypes);
                        //if (!(bChkFileType && bChkFileMimeType))
                        //{
                        //    GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
                        //    SetFocus(flImport.ClientID);
                        //    return;
                        //}
                        if (!bChkFileType)
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Only CSV File allowed to upload.", AlertType.Warning);
                            return;
                        }
                        string strFileName = flImport.FileName;
                        FileInfo fi = new FileInfo(strFileName);
                        string strFilExt = fi.Extension;
                        strFileName = "UserTypesEmailWhiteBlackList" + strFilExt;
                        string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "\\CSV\\User Types\\" + strFileName;
                        try
                        {
                            if (File.Exists(strFileUploadPath))
                            {
                                File.Delete(strFileUploadPath);
                            }

                            flImport.SaveAs(strFileUploadPath);
                            if (File.Exists(strFileUploadPath))
                            {
                                // bool Isvalidfile = IsFileValidated(chkIsWhiteListed, strFileUploadPath);

                                //if (Isvalidfile)
                                //{
                                DataTable dtUserTypeEmailWhiteBlackList = CsvFileToDatatable(strFileUploadPath, true);
                                if (dtUserTypeEmailWhiteBlackList != null)
                                {
                                    if (dtUserTypeEmailWhiteBlackList.Rows.Count > 0)
                                    {
                                        DataColumn colCreatedBy = new System.Data.DataColumn("CreatedBy", typeof(Int32));
                                        colCreatedBy.DefaultValue = UserId;
                                        DataColumn colIsWhiteList = new System.Data.DataColumn("IsWhiteList", typeof(bool));
                                        if (chkIsWhiteListed.Checked)
                                            colIsWhiteList.DefaultValue = 1;
                                        else
                                            colIsWhiteList.DefaultValue = 0;

                                        DataColumn colIsActive = new System.Data.DataColumn("IsActive", typeof(Int32));
                                        colIsActive.DefaultValue = 1;
                                        DataColumn colUserTypeId = new System.Data.DataColumn("UserTypeId", typeof(Int32));
                                        colUserTypeId.DefaultValue = UserTypeId;
                                        DataColumn colDomain = new System.Data.DataColumn("Domain", typeof(string));
                                        colDomain.DefaultValue = "";

                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colDomain);
                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colUserTypeId);
                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colIsWhiteList);
                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colIsActive);
                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colCreatedBy);

                                    }
                                    bool result = UserTypesBL.InsertEmailValidateImportList(dtUserTypeEmailWhiteBlackList, Convert.ToInt16(DBAction.Insert), objUserTypesBE);
                                    if (result)
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "User Types File Import compeleted Successfully.", AlertType.Success);
                                        CreateActivityLog("UserTypeEmailValidation ImportFile", "Import","");
                                    }
                                    else
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                }
                                //}
                                //else
                                //{
                                //  GlobalFunctions.ShowModalAlertMessages(this.Page, "Files contains invalid data.", AlertType.Warning);
                                //}

                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                            return;
                        }

                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Upload File", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
            }

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                Int16 UserTypeId = Convert.ToInt16(btn.CommandArgument);
                objUserTypesBE.UserTypeID = UserTypeId;
                string filename = "UserTypesEmailWhiteBlackList" + DateTime.Now + ".csv";

                //string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "\\CSV\\User Types\\" + filename;
                //if (File.Exists(strFileUploadPath))
                //{
                //    DirectoryInfo d = new DirectoryInfo(GlobalFunctions.GetPhysicalFontsPath());
                //    DirectoryInfo[] darry = d.GetDirectories();
                //    foreach (DirectoryInfo dinfo in darry)
                //    {
                //        if (dinfo.GetFiles().Count() > 0)
                //        {
                //            FileInfo[] f = dinfo.GetFiles();
                //            foreach (FileInfo AllFiles in f)
                //            {
                //                File.Delete(AllFiles.FullName.ToString());
                //            }
                //        }
                //        dinfo.Delete();
                //    }
                //    //File.Delete(strFileUploadPath);
                //}
                //File.Create(strFileUploadPath);

                List<UserTypesBE.CustomUserTypes> lstUserTypesBE = UserTypesBL.GetListUserTypes<UserTypesBE.CustomUserTypes>(objUserTypesBE);
                var csv = new StringBuilder();
                if (lstUserTypesBE != null)
                {
                    csv.Append("EmailId" + Environment.NewLine);
                    foreach (UserTypesBE.CustomUserTypes items in lstUserTypesBE)
                    {
                        csv.Append(items.EmailId + Environment.NewLine);
                    }
                }
                else
                {
                    csv.Append("EmailId" + Environment.NewLine);
                }
                //string attachment = "attachment; filename=" + filename + "";
                HttpContext.Current.Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment;filename=UserTypesEmailWhiteBlackList.csv");
                Response.Output.Write(csv);
                HttpContext.Current.Response.ContentType = "Excel/csv";
                Session["lstUserTypesBE"] = null;
                Response.End();

                //WebClient req = new WebClient();
                //HttpResponse response = HttpContext.Current.Response;
                //response.Clear();
                //response.ClearContent();
                //response.ClearHeaders();
                //response.Buffer = true;
                //response.AddHeader("Content-Disposition", "attachment;filename=UserTypesEmailWhiteBlackList.csv");
                //byte[] data = req.DownloadData(Convert.ToString(GlobalFunctions.GetPhysicalFolderPath() + "CSV\\User Types\\UserTypesEmailWhiteBlackList.csv"));
                //response.BinaryWrite(data);
                //response.End();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void chkIsWhiteListed_CheckedChanged(object sender, EventArgs e)
        {
            UserBE objUserBEnew = Session["User"] as UserBE;
            DataTable dtUserTypeEmailWhiteBlackList = null;

            bool res;

            CheckBox chkIsWhiteListed = (sender as CheckBox);

            GridViewRow row = (sender as CheckBox).NamingContainer as GridViewRow;
            int UserTypeId = Convert.ToInt16(gvList.DataKeys[row.RowIndex].Values["UserTypeId"]);
            string UserTypeName = Convert.ToString(gvList.DataKeys[row.RowIndex].Values["UserTypeName"]);

            objUserTypesBE.UserTypeID = UserTypeId;
            objUserTypesBE.CreatedBy = UserId;
            if ((row.FindControl("chkIsWhiteListed") as CheckBox).Checked)
            {
                objUserTypesBE.IsWhiteList = true;
                res = UserTypesBL.InsertEmailValidateImportList(dtUserTypeEmailWhiteBlackList, Convert.ToInt16(DBAction.Update), objUserTypesBE);
            }
            else
            {
                objUserTypesBE.IsWhiteList = false;
                res = UserTypesBL.InsertEmailValidateImportList(dtUserTypeEmailWhiteBlackList, Convert.ToInt16(DBAction.Update), objUserTypesBE);
            }
            if (res)
            {
                //true
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Email List has been updated successfully.", AlertType.Success);
                CreateActivityLog("UserTypeEmailValidation chkIsWhiteListed", "Update", "");
                // objbase.CreateActivityLog("User Management", "Updated", id1 + "_" + UserName);
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "No Records Exists for this Item.", AlertType.Warning);
            }
            BindUserTypeList();
        }

        protected void gvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvList.PageIndex = e.NewPageIndex;
            BindUserTypeList();
        }


    }
}