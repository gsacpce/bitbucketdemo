﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
	public partial class Admin_UserType_ManageUserType_Updated : BasePage //System.Web.UI.Page
	{
		protected global::System.Web.UI.WebControls.TextBox txtuserTypeName, txtVatRate;
		protected global::System.Web.UI.WebControls.Repeater rptViewUserType;
		protected global::System.Web.UI.WebControls.Panel pnlViewAdd, pnlAdd, pnlEditCatalogueDetails;
		protected global::System.Web.UI.WebControls.DropDownList ddlLanguage, ddlCurrencies, ddlCatalogues;
		protected global::System.Web.UI.WebControls.CheckBox chkVatDisplay, chkEnablePoints, chkEnableCurrency, chkIsUTSSO, chkEnableBudget;
		protected global::System.Web.UI.WebControls.GridView gvUsertypes, gvEditUSerTypeCatalogueDetails;
		protected global::System.Web.UI.WebControls.HiddenField hdnfUserTypeID, hdnfCurrencyID, hdnfSelectedCatalogueid;
		protected global::System.Web.UI.WebControls.Button btnAddUserTypes, btnCancel, btnEditUpdate, btnEditCancel, btnEdit, btnDelete, btnUpdateCatalogueDetails, btnCancelCatalogueDetails;
		protected global::System.Web.UI.WebControls.Label lblHeading;
		protected global::System.Web.UI.WebControls.RadioButton rdbUserTypeCatalaogueName;
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl divaddFields;


		UserTypesBE objUserTypesBE = new UserTypesBE();
		UserTypesBE objlstUserTypesBE = new UserTypesBE();
		Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
		List<UserTypesDetailsBE> lstUTDBE = new List<UserTypesDetailsBE>();
		UserTypesDetailsBE objUserTypesDetailsBE = new UserTypesDetailsBE();
		string UserTypeNameForEditCatalogueDetails;
		Int16 UserTypeIDForEdit;

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (!IsPostBack)
				{
					//BindData();
					HttpRuntime.Cache.Remove("StoreDetails");
					StoreBE objStoreBE = new StoreBE();
					objStoreBE = StoreBL.GetStoreDetails();
					GlobalFunctions.BindAllLanguage(objStoreBE, ref ddlLanguage);
					BindAllUserTypeDetails();
					BindCurrencies();
					BindCatalogue();
				}
			}
			catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
		}
		protected void BindCurrencies()
		{
			HttpRuntime.Cache.Remove("StoreDetails");
			StoreBE objStoreBE = new StoreBE();
			objStoreBE = StoreBL.GetStoreDetails();
			if (objStoreBE.StoreCurrencies.Count > 0)
			{

				List<StoreBE.StoreCurrencyBE> objCur = new List<StoreBE.StoreCurrencyBE>();
				objCur = objStoreBE.StoreCurrencies;
								
				var aCurrency = (from y in objCur
								 select new { y.CurrencyCode, y.CurrencyName });

				ddlCurrencies.DataSource = aCurrency.Distinct();
				ddlCurrencies.DataTextField = "CurrencyName";
				ddlCurrencies.DataValueField = "CurrencyCode";// "CurrencyID";
				ddlCurrencies.DataBind();
				GlobalFunctions.AddDropdownItem(ref ddlCurrencies);
			}
		}
			   
		/// <summary>
		/// Author :    SHRIGANESH SINGH
		/// Scope  :    To clear the Details in Add User Type Section
		/// </summary>
		private void ClearData()
		{
			try
			{
				hdnfUserTypeID.Value = "";
				txtuserTypeName.Text = "";
				ddlCurrencies.SelectedIndex = 0;
				ddlCurrencies.Enabled = true;
				txtVatRate.Text = "";
				ddlCatalogues.SelectedIndex = 0;
				ddlCatalogues.Enabled = true;
				lblHeading.Text = "Add User Type";
				ddlLanguage.Enabled = false;
				divaddFields.Visible = true;
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		/// <summary>
		/// Author  :   SHRIGANESH SINGH
		/// Scope   :   To bind all the User Types and their details for editing
		/// </summary>
		private void BindAllUserTypeDetails()
		{
			try
			{
				int LanguageID = Convert.ToInt16(ddlLanguage.SelectedValue);
				UserTypesBL objUserTypeBL = new UserTypesBL();
				lstUTDBE = UserTypesBL.GetAllUserTypeDetailsList(Constants.USP_GetAllUserTypeDetailForEdit, LanguageID);
				if (lstUTDBE != null)
				{
					gvUsertypes.DataSource = lstUTDBE;
					gvUsertypes.DataBind();
				}
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		protected void UpdateUserTypeName()
		{
			try
			{
				int ires = 0;

				Dictionary<string, string> dictParams = new Dictionary<string, string>();
				if (Convert.ToString(hdnfUserTypeID.Value) != null)
				{
					dictParams.Add("UserTypeID", hdnfUserTypeID.Value);
				}
				else
				{
					dictParams.Add("UserTypeID", "0");
				}

				dictParams.Add("Name", txtuserTypeName.Text.Trim());
				dictParams.Add("CatalogueID", "0");

				if (chkVatDisplay.Checked)
				{
					dictParams.Add("IsVatDisplay", "true");
					dictParams.Add("VatRate", "");
				}
				else
				{
					dictParams.Add("IsVatDisplay", "false");
				}
				UserBE objUser = new UserBE();
				objUser = Session["StoreUser"] as UserBE;
				dictParams.Add("CreatedBy", objUser.UserId.ToString());
				dictParams.Add("ModifiedBy", objUser.UserId.ToString());
				dictParams.Add("LanguageId", ddlLanguage.SelectedValue);

				ires = UserTypesBL.ManageUserTypes_SAED(objUserTypesBE, dictParams, DBAction.Update);

				if (ires > 0)
				{
					ClearData();
					GlobalFunctions.ShowModalAlertMessages(this, "User Type Name Updated Successfully !!", AlertType.Success);
					BindAllUserTypeDetails();
				}
				else if (ires == 3)
				{
					GlobalFunctions.ShowModalAlertMessages(this, "User Type Already Exists !!", AlertType.Warning);
				}
				else
				{
					GlobalFunctions.ShowModalAlertMessages(this, "Error while updating User Type !!", AlertType.Warning);
				}
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		protected void Add()
		{
			try
			{
				if (ddlCurrencies.SelectedIndex > 0)
				{
					if (ddlCatalogues.SelectedIndex > 0)
					{
						if (chkVatDisplay.Checked)
						{
							if (txtVatRate.Text == "" || txtVatRate.Text == null)
							{
								GlobalFunctions.ShowModalAlertMessages(this, "Please Enter the Vat Percentage !!", AlertType.Warning);
								return;
							}
						}
						int ires = 0;                                               

						Dictionary<string, string> dictParams = new Dictionary<string, string>();
						if (Convert.ToString(hdnfUserTypeID.Value) != null)
						{
							dictParams.Add("UserTypeID", hdnfUserTypeID.Value);
						}
						else
						{
							dictParams.Add("UserTypeID", "0");
						}
						dictParams.Add("Name", txtuserTypeName.Text.Trim());
						dictParams.Add("CatalogueID", ddlCatalogues.SelectedValue);

						if (chkVatDisplay.Checked)
						{
							dictParams.Add("IsVatDisplay", "true");
							dictParams.Add("VatRate", txtVatRate.Text.Trim());
						}
						else
						{
							dictParams.Add("IsVatDisplay", "false");
						}
						dictParams.Add("IsSSOUT", Convert.ToString(chkIsUTSSO.Checked));
						dictParams.Add("IsPointsEnabled", Convert.ToString(chkEnablePoints.Checked));//INDEED
						dictParams.Add("IsCurrencyEnabled", Convert.ToString(chkEnableCurrency.Checked));//INDEED
						dictParams.Add("IsBudgetEnabled", Convert.ToString(chkEnableBudget.Checked));

						UserBE objUser = new UserBE();
						objUser = Session["StoreUser"] as UserBE;
						dictParams.Add("CreatedBy", objUser.UserId.ToString());
						dictParams.Add("ModifiedBy", objUser.UserId.ToString());


						ires = UserTypesBL.ManageUserTypes_SAED(objUserTypesBE, dictParams, DBAction.Insert);
						HttpRuntime.Cache.Remove("UserTypesBE");
						if (ires == 1)
						{
							GlobalFunctions.ShowModalAlertMessages(this, "User Type Added successfully !!", AlertType.Success);
							BindAllUserTypeDetails();
							ClearData();
						}
						else if (ires == 3)
						{
							GlobalFunctions.ShowModalAlertMessages(this, "User Type Already Exists !!", AlertType.Warning);
						}
						else
						{
							GlobalFunctions.ShowModalAlertMessages(this, "Error while creating User Type !!", AlertType.Warning);
						}
					}
					else
					{
						GlobalFunctions.ShowModalAlertMessages(this, "Please select one catalogue for this User Type !!", AlertType.Warning);
					}
				}
				else
				{
					GlobalFunctions.ShowModalAlertMessages(this, "Please select one currency for this User Type !!", AlertType.Warning);
				}
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (Convert.ToString(hdnfUserTypeID.Value) != "")
			{
				UpdateUserTypeName();
				HttpRuntime.Cache.Remove("UserTypesBE");
			}
			else
			{
				Add();
			}

		}

		protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
		{
			BindAllUserTypeDetails();
		}

		protected void ddlCurrencies_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				hdnfSelectedCatalogueid.Value = "";
				if (ddlCurrencies.SelectedValue == "0")
				{

					ddlCatalogues.SelectedIndex = 0;
					ddlCatalogues.Enabled = false;
				}
				else
				{
					BindCatalogue();
				}
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		protected void BindCatalogue()
		{
			ddlCatalogues.Enabled = true;
			HttpRuntime.Cache.Remove("StoreDetails");
			StoreBE objStoreBE = new StoreBE();
			objStoreBE = StoreBL.GetStoreDetails();

			List<StoreBE.StoreCurrencyBE> objCur1 = new List<StoreBE.StoreCurrencyBE>();
			objCur1 = objStoreBE.StoreCurrencies;
			objCur1 = objCur1.FindAll(x => x.CurrencyCode == ddlCurrencies.SelectedValue);
			ddlCatalogues.DataSource = objCur1;
			ddlCatalogues.DataTextField = "CatalogueId";
			ddlCatalogues.DataValueField = "CatalogueId";
			ddlCatalogues.DataBind();
			if (Convert.ToString(hdnfSelectedCatalogueid.Value) != "")
			{
				ddlCatalogues.SelectedValue = hdnfSelectedCatalogueid.Value;
			}
			GlobalFunctions.AddDropdownItem(ref ddlCatalogues);
		}

		protected void btnAddUserType_OnClick(object sender, EventArgs e)
		{
			try
			{
				int ires = 0;
				UserTypesBE objUserTypesBE = new UserTypesBE();
				if (ires > 0)
				{
					ClearData();
					GlobalFunctions.ShowModalAlertMessages(this, "User Type created successfully !!", AlertType.Success);
				}
				else
				{
					GlobalFunctions.ShowModalAlertMessages(this, "Error while creating User Type !!", AlertType.Warning);
				}
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		protected void btnCancel_OnClick(object sender, EventArgs e)
		{
			try
			{
				ClearData();
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}            

		protected void gvUsertypes_OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			try
			{
				GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
				HiddenField hfUTID = row.FindControl("hfUserTypeID") as HiddenField;
				HiddenField hfCurrID = row.FindControl("hfCurrencyID") as HiddenField;
				TextBox txtUserTypeName = row.FindControl("txtUserTypeName") as TextBox;
				UserTypeNameForEditCatalogueDetails = txtUserTypeName.Text;

				Int16 UserTypeID = Convert.ToInt16(e.CommandArgument);

				hdnfUserTypeID.Value = hfUTID.Value;

				//hdnfCurrencyID.Value = hfCurrID.Value;
				if (e.CommandName.ToLower() == "edits")
				{
					ReadData(hfUTID, hfCurrID, UserTypeID);

				}

				#region Code Added BySHRIGANESH SINGH 02 July 2016
				if (e.CommandName.ToLower() == "editcatalogues")
				{
					ReadCatalogueData(UserTypeID);
				}
				#endregion
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		protected void gvEditUSerTypeCatalogueDetails_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			try
			{

			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}
			   
		protected void ReadData(HiddenField hfUTID, HiddenField hfCurrID, Int16 UserTypeId)
		{
			UserTypesBE.UserTypeCatalogue objUserTypeCatalogue = new UserTypesBE.UserTypeCatalogue();
			objUserTypeCatalogue.UserTypeID = UserTypeId;
			string LanguageID = ddlLanguage.SelectedValue;
			hfUTID.Value = Convert.ToString(objUserTypeCatalogue.UserTypeID);
			hdnfUserTypeID.Value = Convert.ToString(objUserTypeCatalogue.UserTypeID);
			objUserTypesDetailsBE = UserTypesBL.GetAllUserTypeDetailsByUserTypeID(Constants.USP_GetAllUserTypeDetailsByUserTypeID, LanguageID, objUserTypeCatalogue);

			if (objUserTypesDetailsBE != null)
			{
				txtuserTypeName.Text = objUserTypesDetailsBE.UserTypeName;
				divaddFields.Visible = false;
				
				lblHeading.Text = "Edit User Type";
				ddlLanguage.Enabled = true;
			}
		}

		/// <summary>
		/// Author  :   SHRIGANESH SINGH
		/// Date    :   02 July 2016
		/// Scope   :   To get all store and UserType catalogue details
		/// </summary>
		/// <param name="UserTypeID"></param>
		protected void ReadCatalogueData(Int16 UserTypeID)
		{
			List<StoreAndUserTypeCatalogueDetailsBE> lstStoreAndUserTypeCatalogueDetails = new List<StoreAndUserTypeCatalogueDetailsBE>();

			UserTypesBE.UserTypeCatalogue objUserTypeCatalogue = new UserTypesBE.UserTypeCatalogue();
			objUserTypeCatalogue.UserTypeID = UserTypeID;

			lstStoreAndUserTypeCatalogueDetails = UserTypesBL.GetAllStoreAndUserTypeCatalogueDetailsByUserTypeID(Constants.USP_GetAllStoreAndUserTypeCatalogueDetailsByUserTypeID, UserTypeID);

			if (lstStoreAndUserTypeCatalogueDetails.Count > 0)
			{
				gvEditUSerTypeCatalogueDetails.DataSource = lstStoreAndUserTypeCatalogueDetails;
				gvEditUSerTypeCatalogueDetails.DataBind();
				pnlEditCatalogueDetails.Visible = true;
				btnUpdateCatalogueDetails.Visible = true;
				pnlAdd.Visible = false;
				btnCancelCatalogueDetails.Visible = true;
				rdbUserTypeCatalaogueName.Text = UserTypeNameForEditCatalogueDetails;
			}           
		}

		/// <summary>
		/// Author  :   SHRIGANESH SINGH
		/// Date    :   02 July 2016
		/// Scope   :   To Update all the UserType Catalogue Details
		/// </summary>
		protected void btn_Command(object sender, CommandEventArgs e)
		{
			HttpRuntime.Cache.Remove("UserTypesBE");
			UserTypeIDForEdit = Convert.ToInt16(hdnfUserTypeID.Value);
			switch (e.CommandName)
			{
				case "UpdateCatalogueDetails": UpdateCatalogueDetails(UserTypeIDForEdit); break;
				case "CancelCatalogueDetails":
					pnlAdd.Visible = true;
					pnlEditCatalogueDetails.Visible = false;
					break;

			}
		}

		protected void UpdateCatalogueDetails(Int16 UserTypeIDForEdit)
		{
			try
			{              
				List<StoreAndUserTypeCatalogueDetailsBE> lstUpdateStoreAndUserTypeCatalogueDetails = new List<StoreAndUserTypeCatalogueDetailsBE>();
				foreach (GridViewRow Row in gvEditUSerTypeCatalogueDetails.Rows)
				{
					HiddenField hdnCatalogueId = (HiddenField)Row.Cells[0].FindControl("hdfActiveUserTypeCatalogue");
					CheckBox chkCatalogueID = (CheckBox)Row.Cells[0].FindControl("chkCatalogueID");
					CheckBox chkVatDisplay = (CheckBox)Row.Cells[1].FindControl("chkVatDisplay");
					TextBox txtVatRate = (TextBox)Row.Cells[2].FindControl("txtEditCatalogueVATRate");
					CheckBox chkIsSSOUT = (CheckBox)Row.Cells[3].FindControl("chkEditIsSSOUT");
					  CheckBox chkIsPointsEnabled = (CheckBox)Row.Cells[3].FindControl("chkIsPointsEnabled");//INDEED		
					  CheckBox chkIsCurrencyEnabled = (CheckBox)Row.Cells[4].FindControl("chkIsCurrencyEnabled");//INDEED
					  CheckBox chkIsBudgetEnabled = (CheckBox)Row.Cells[4].FindControl("chkIsBudgetEnabled");
					if (chkCatalogueID.Checked)
					{
						if (chkVatDisplay.Checked)
						{
							if (txtVatRate.Text == null || txtVatRate.Text == "")
							{
								GlobalFunctions.ShowModalAlertMessages(this.Page, "Please assign Vat Rate for Catalog ID " + hdnCatalogueId.Value, AlertType.Warning);
								return;
							}
							else
							{
								lstUpdateStoreAndUserTypeCatalogueDetails.Add
									(new StoreAndUserTypeCatalogueDetailsBE() 
									{ 
									  UserTypeID = UserTypeIDForEdit, 
									  UserTypeCatalogueIDs = Convert.ToInt16(hdnCatalogueId.Value), 
									  IsVatDisplay = true, 
									  Vatrate = Convert.ToDecimal(txtVatRate.Text),
									  IsSSOUT = chkIsSSOUT.Checked,
									  IsPointsEnabled = chkIsPointsEnabled.Checked, 
									  IsCurrencyEnabled = chkIsCurrencyEnabled.Checked, 
									  IsBudgetEnabled=chkIsBudgetEnabled.Checked
									});
							}
						}
						else
						{
							lstUpdateStoreAndUserTypeCatalogueDetails.Add
								(new StoreAndUserTypeCatalogueDetailsBE() 
								{ 
									UserTypeID = UserTypeIDForEdit, 
									UserTypeCatalogueIDs = Convert.ToInt16(hdnCatalogueId.Value), 
									IsVatDisplay = false,
									IsSSOUT = chkIsSSOUT.Checked,
									Vatrate = Convert.ToDecimal("0") , 
									IsPointsEnabled = chkIsPointsEnabled.Checked, 
									IsCurrencyEnabled = chkIsCurrencyEnabled.Checked,
									IsBudgetEnabled = chkIsBudgetEnabled.Checked
								});
						}
					}                    
				}
				DataTable dtUpdateUserTypeCatalogueDetails = ToDataTable(lstUpdateStoreAndUserTypeCatalogueDetails);
				DataTable dt = new DataTable();
				dt.Columns.Add("UserTypeID");
				dt.Columns.Add("UserTypeCatalogueIDs");
				dt.Columns.Add("IsVatDisplay");
				dt.Columns.Add("VatRate");
				dt.Columns.Add("IsSSOUT");
				dt.Columns.Add("IsPointsEnabled");	//INDEED	
				dt.Columns.Add("IsCurrencyEnabled");//INDEED
				dt.Columns.Add("IsBudgetEnabled");
				for (int i = 0; i < dtUpdateUserTypeCatalogueDetails.Rows.Count; i++)
				{
					dt.Rows.Add();
					dt.Rows[i]["UserTypeID"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["UserTypeID"];
					dt.Rows[i]["UserTypeCatalogueIDs"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["UserTypeCatalogueIDs"];
					dt.Rows[i]["IsVatDisplay"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["IsVatDisplay"];
					dt.Rows[i]["VatRate"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["VatRate"];
					dt.Rows[i]["IsSSOUT"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["IsSSOUT"];
					dt.Rows[i]["IsPointsEnabled"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["IsPointsEnabled"];		
					dt.Rows[i]["IsCurrencyEnabled"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["IsCurrencyEnabled"];
					dt.Rows[i]["IsBudgetEnabled"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["IsBudgetEnabled"];
				}


				bool IsCatalogueDetailsUpdated = UserTypesBL.UpdateUserTypeCatalogueDetails(dt);
				if (IsCatalogueDetailsUpdated)
				{
					GlobalFunctions.ShowModalAlertMessages(this.Page, "User Type Catalog Details updated succesfully", AlertType.Success);
					BindAllUserTypeDetails();
				}
				else
				{
					GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating", AlertType.Failure);
				}
				HttpRuntime.Cache.Remove("UserTypesBE");
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}                

		public static DataTable ToDataTable<T>(List<T> items)
		{
			DataTable dataTable = new DataTable(typeof(T).Name);

			//Get all the properties
			PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
			foreach (PropertyInfo prop in Props)
			{
				//Setting column names as Property names
				dataTable.Columns.Add(prop.Name);
			}
			foreach (T item in items)
			{
				var values = new object[Props.Length];
				for (int i = 0; i < Props.Length; i++)
				{
					//inserting property values to datatable rows
					values[i] = Props[i].GetValue(item, null);
				}
				dataTable.Rows.Add(values);
			}
			//put a breakpoint here and check datatable
			return dataTable;
		}
	}
}