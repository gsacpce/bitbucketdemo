﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_UserType_AssignCategories : BasePage//System.Web.UI.Page
    {
        #region Decalarations


        protected global::System.Web.UI.WebControls.HiddenField HIDUserTypeSelectedId, HIDAssignedCategoryIds, HIDUnAssignedCategoryIds, HIDAssignedCategoryNameIds, HIDUnAssignedCategoryNameIds;
        protected global::System.Web.UI.WebControls.Literal ltlHeading;
        protected global::System.Web.UI.WebControls.DropDownList ddlUserTypes;
        protected global::System.Web.UI.WebControls.ListBox lstAssignedCategories, lstUnAssignedCategories;
        protected global::System.Web.UI.WebControls.TextBox txtDescription;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divLang;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgLeftToRight, imgRightToLeft;
        protected global::System.Web.UI.HtmlControls.HtmlContainerControl dvLoader1, divShowListBox;
        protected global::System.Web.UI.WebControls.Button btnSaveExit;


        public string Host = GlobalFunctions.GetVirtualPath();

        int UserId = 0;
        int DefaultLanguage = 1;
        public string AdminHost = GlobalFunctions.GetVirtualPathAdmin();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {


            dvLoader1 = this.Master.FindControl("dvLoader1") as System.Web.UI.HtmlControls.HtmlContainerControl;



            if (!IsPostBack)
            {

                imgLeftToRight.Attributes.Add("onmousedown", "fn_MoveStoreDataLeftToRight();");
                imgLeftToRight.Attributes.Add("onkeypress", "fn_MoveStoreDataLeftToRight();");

                imgRightToLeft.Attributes.Add("onmousedown", "fn_MoveStoreDataRightToLeft();");
                imgRightToLeft.Attributes.Add("onkeypress", "fn_MoveStoreDataRightToLeft();");

                PopulateUserTypeDropDown();
                if (ddlUserTypes.SelectedIndex > 0)
                {
                    lstAssignedCategories.Items.Clear();
                    BindAssignedCategories();
                    BindUnAssignedCategories();
                }
            }





            //RemoveMatchingItemsFromLeft(lstUnAssignedCategories, lstAssignedCategories);

        }

        private void RemoveMatchingItemsFromLeft(ListBox objListBox1, ListBox objListBox2)
        {
            foreach (ListItem item in objListBox2.Items)
            {
                if (objListBox1.Items.Contains(item))
                {
                    objListBox1.Items.Remove(item);
                }
            }
        }

        protected void PopulateUserTypeDropDown()
        {
            try
            {
                UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();
                objUserTypesBE.LanguageId = 1;
                List<UserTypesBE.CustomUserTypes> lstUserTypesBE = UserTypesBL.GetListUserTypes<UserTypesBE.CustomUserTypes>(objUserTypesBE);
                if (lstUserTypesBE != null)
                {
                    if (lstUserTypesBE.Count > 0)
                    {
                        ddlUserTypes.DataSource = lstUserTypesBE;
                        ddlUserTypes.DataTextField = "UserTypeName";
                        ddlUserTypes.DataValueField = "UserTypeID";
                        ddlUserTypes.DataBind();
                        ddlUserTypes.Items.Insert(0, new ListItem("Please Select User Type", "0"));
                        HIDUserTypeSelectedId.Value = "";
                    }
                }
                else
                {
                    ddlUserTypes.DataSource = null;
                    ddlUserTypes.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void RebindLastValue()
        {
            try
            {
                if (HIDAssignedCategoryNameIds.Value.Length != 0)
                {
                    HIDAssignedCategoryIds.Value = "";
                    string[] Items = HIDAssignedCategoryNameIds.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    lstAssignedCategories.Items.Clear();
                    foreach (string item in Items)
                    {
                        if (!lstAssignedCategories.Items.Contains(new ListItem(item.Split(new char[] { '|' })[0], item.Split(new char[] { '|' })[1])))
                        {
                            lstAssignedCategories.Items.Add(new ListItem(item.Split(new char[] { '|' })[0], item.Split(new char[] { '|' })[1]));
                            HIDAssignedCategoryIds.Value += item.Split(new char[] { '|' })[1] + ",";
                        }
                    }
                    HIDAssignedCategoryIds.Value.TrimEnd(new char[] { ',' });
                }
                else
                {
                    lstAssignedCategories.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }



        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            try
            {
                UserBE objUser = new UserBE();
                objUser = Session["StoreUser"] as UserBE;


                List<CategoryBE> Categories = new List<CategoryBE>();
                if (Convert.ToInt32(ddlUserTypes.SelectedValue) > 0)
                {

                    int i = UserTypesBL.AddCategoriesToUserTypes(Convert.ToInt32(ddlUserTypes.SelectedValue), Convert.ToString(Convert.ToInt16(DBAction.Insert)), Convert.ToString(HIDAssignedCategoryIds.Value), Convert.ToInt16(objUser.UserId));
                    if (Convert.ToString(HIDAssignedCategoryIds.Value) != "" && i == 1)
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, "Category has been Assigned to Users Types", AlertType.Success);
                    CreateActivityLog("AssignCatagories", "Assigned", ddlUserTypes.SelectedValue);
                    }
                    else if (Convert.ToString(HIDAssignedCategoryIds.Value) == "" && i == 1)
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, "Category has been Removed to Users Types", AlertType.Success);
                    CreateActivityLog("AssignCatagories", "Removed", ddlUserTypes.SelectedValue);
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, "Category Assigned Failed", AlertType.Warning); }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select User Type", AlertType.Warning);
                }
                lstAssignedCategories.Items.Clear();
                BindAssignedCategories();

                BindUnAssignedCategories();



            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }






        protected void ddlUserTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            RebindLastValue();
            lstAssignedCategories.Items.Clear();
            if (ddlUserTypes.SelectedIndex > 0)
            {
                HIDAssignedCategoryIds.Value = "";
                //HIDUserTypeSelectedId.Value = ddlUserTypes.SelectedValue;
                BindAssignedCategories();
                BindUnAssignedCategories();

            }

        }





        protected void BindUnAssignedCategories()
        {
            try
            {

                lstUnAssignedCategories.Items.Clear();
                List<CategoryBE> Categories = new List<CategoryBE>();
                Categories = UserTypesBL.GetAssignedCategoryToUsers(Convert.ToInt32(ddlUserTypes.SelectedValue), "UC");


                if (ddlUserTypes.SelectedValue != "")
                {
                    if (Categories != null)
                    {
                        if (Categories.Count() > 0)
                        {
                            lstUnAssignedCategories.DataSource = Categories;
                            lstUnAssignedCategories.DataTextField = "CategoryName";
                            lstUnAssignedCategories.DataValueField = "CategoryId";
                            lstUnAssignedCategories.DataBind();
                            RemoveMatchingItemsFromLeft(lstUnAssignedCategories, lstAssignedCategories);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindAssignedCategories()
        {

            try
            {

                List<CategoryBE> Categories = new List<CategoryBE>();
                int ResultA = 0;

                if (HIDUserTypeSelectedId.Value == "")
                    HIDUserTypeSelectedId.Value = "0";

                Categories = UserTypesBL.GetAssignedCategoryToUsers(Convert.ToInt32(ddlUserTypes.SelectedValue), "A");

                HIDAssignedCategoryNameIds.Value = "";
                if (Categories != null)
                {
                    if (Categories.Count() > 0)
                    {
                        int count = 0;
                        foreach (CategoryBE Category in Categories)
                        {
                            if (Categories.Count() == 1)
                                HIDAssignedCategoryIds.Value = Category.CategoryId.ToString();
                            else if (count == 0)
                            {
                                HIDAssignedCategoryIds.Value = Category.CategoryId.ToString();
                                count++;
                            }
                            else
                                HIDAssignedCategoryIds.Value += "," + Category.CategoryId.ToString();

                            lstAssignedCategories.Items.Add(new ListItem() { Text = Category.CategoryName, Value = Convert.ToString(Category.CategoryId) });
                        }
                    }
                    else
                    {
                        lstAssignedCategories.Items.Clear();
                    }
                }
                else
                {
                    lstAssignedCategories.Items.Clear();
                }


            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}