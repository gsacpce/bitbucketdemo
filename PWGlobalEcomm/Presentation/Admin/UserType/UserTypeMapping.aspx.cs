﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Presentation
{
    public partial class Admin_UserType_UserTypeMapping : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.GridView gvList;
        protected global::System.Web.UI.WebControls.Literal ltrAllRecords;
        protected global::System.Web.UI.WebControls.RadioButtonList rdList;
        protected global::System.Web.UI.WebControls.Panel pnlUserSelection, pnlEmail, pnlCountry, pnlCustomMapping;
        protected global::System.Web.UI.WebControls.Repeater rptCountry, rptCustomMapping;
        protected global::System.Web.UI.WebControls.FileUpload flupldCustomFieldsExcel;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        protected global::System.Web.UI.WebControls.TextBox txtCustomFieldName;
        protected global::System.Web.UI.WebControls.Button btnUserSequenceSave;
        protected global::System.Web.UI.WebControls.RadioButton rdbRegAsDef, rdbDecReg;
        UserBE objUserBE;
        public int UserId;
        Int16 LanguageId;
        Int16 DefaultRegCustomFieldID;

        UserTypeCountryMaster objUserTypeCountryMaster = new UserTypeCountryMaster();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["StoreUser"] != null)
                {
                    objUserBE = Session["StoreUser"] as UserBE;
                    UserId = objUserBE.UserId;
                }
                if (!IsPostBack)
                {
                    //pnlUserSelection.Visible = false;
                    pnlEmail.Visible = false;

                    List<UserTypeMappingBE.UserTypeMasterOptions> objUserTypeMasterOptions = UserTypesBL.GetAllUserTypeMasterOptions(Constants.USP_GetAllUserTypeMasterOptions, null, true);
                    rdList.DataSource = objUserTypeMasterOptions;
                    rdList.DataTextField = "TypeName";
                    rdList.DataValueField = "UserTypeMappingTypeID";
                    rdList.DataBind();
                    UserTypeMappingBE.UserTypeMasterOptions objMaster = objUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                    if (objMaster != null)
                    {
                        rdList.SelectedValue = Convert.ToString(objMaster.UserTypeMappingTypeID);
                        rdList_SelectedIndexChanged(sender, null);
                        //BindCustomMappingValidation();// added by SHRIGANESH SINGH 07 JULY 2016
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #region "RadioButton List"
        protected void rdList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //pnlUserSelection.Visible = false;

                if (rdList.SelectedValue.Equals("1"))
                {
                    //pnlUserSelection.Visible = true;
                    pnlEmail.Visible = false;
                    pnlCountry.Visible = false;
                    pnlCustomMapping.Visible = false;
                    btnUserSequenceSave.Visible = false;
                }
                else if (rdList.SelectedValue.Equals("2"))
                {
                    pnlEmail.Visible = true;
                    btnUserSequenceSave.Visible = true;
                    // pnlUserSelection.Visible = false;
                    pnlCountry.Visible = false;
                    pnlCustomMapping.Visible = false;
                    BindUserTypeList();
                }
                else if (rdList.SelectedValue.Equals("3"))
                {
                    pnlEmail.Visible = false;
                    // pnlUserSelection.Visible = false;
                    pnlCountry.Visible = true;
                    pnlCustomMapping.Visible = false;
                    btnUserSequenceSave.Visible = false;
                    BindCountry();
                }
                else if (rdList.SelectedValue.Equals("4"))
                {
                    pnlEmail.Visible = false;
                    //  pnlUserSelection.Visible = false;
                    pnlCountry.Visible = false;
                    btnUserSequenceSave.Visible = false;
                    pnlCustomMapping.Visible = true;
                    BindCustomMappingValidation();
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    GlobalFunctions.BindAllLanguage(objStoreBE, ref ddlLanguage);

                }
                else
                {
                    //  pnlUserSelection.Visible = false;
                    pnlEmail.Visible = false;
                    btnUserSequenceSave.Visible = false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #endregion

        #region "EmailID"

        private int BindUserTypeList()
        {
            int TotalRow = 0;
            try
            {
                UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();
                List<UserTypesBE.CustomUserTypes> lstUserTypesBE = UserTypesBL.GetListUserTypes<UserTypesBE.CustomUserTypes>(objUserTypesBE);
                if (lstUserTypesBE != null)
                {
                    if (lstUserTypesBE.Count > 0)
                    {
                        gvList.DataSource = lstUserTypesBE;
                        ltrAllRecords.Text = lstUserTypesBE[0].TotalRow.ToString();
                        gvList.DataBind();
                    }
                    else
                    {
                        gvList.DataSource = null;
                        gvList.DataBind();
                        ltrAllRecords.Text = "0";
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return TotalRow;
        }

        protected bool ValidateFile(string Filepath)
        {
            bool Isvalidated = false;
            try
            {
                string csvData = File.ReadAllText(Filepath);
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                List<string> emaillist = new List<string>();
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        foreach (string cell in row.Split(','))
                        {
                            string data = cell.Replace("\r", "");
                            Match match = regex.Match(data);
                            if (match.Success)
                            {
                                Isvalidated = true;
                            }
                            else
                            {
                                emaillist.Add(data);
                                Isvalidated = false;
                                goto EndFunction;
                            }
                        }
                    }
                EndFunction: if (!Isvalidated) break;
                }
                #region Domain validation
                //else if (RdWhiteList.SelectedValue == "2")
                //{
                //    foreach (string row in csvData.Split('\n'))
                //    {
                //        if (!string.IsNullOrEmpty(row))
                //        {
                //            Regex regex = new Regex("^(?:[-A-Za-z0-9]+\\.)+[A-Za-z]{2,6}$");
                //            foreach (string cell in row.Split(','))
                //            {
                //                string data = cell.Replace("\r", "");
                //                Match match = regex.Match(data);
                //                if (match.Success)
                //                {
                //                    Isvalidated = true;
                //                }
                //                else
                //                {
                //                    Isvalidated = false;
                //                    goto EndFunction;
                //                }
                //            }
                //        }
                //    EndFunction: if (!Isvalidated) break;
                //    }
                //}
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return Isvalidated;
        }

        protected bool IsFileValidated(CheckBox chkIsWhiteListed, string FilePath)
        {
            bool Isvalidated = true;
            try
            {
                Isvalidated = ValidateFile(FilePath);
                if (Isvalidated)
                {
                    Isvalidated = true;
                }
                else
                {
                    Isvalidated = false;
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/InvalidData"), AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Isvalidated;
        }

        public DataTable CsvFileToDatatable(string path, bool IsFirstRowHeader)
        {
            string header = "No";
            string sql = string.Empty;
            DataTable dataTable = null;
            string pathOnly = string.Empty;
            string fileName = string.Empty;
            try
            {
                pathOnly = Path.GetDirectoryName(path);
                fileName = Path.GetFileName(path);
                sql = @"SELECT * FROM [" + fileName + "]";
                if (IsFirstRowHeader)
                {
                    header = "true";
                }
                using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathOnly +
                ";Extended Properties=\"Text;HDR=" + header + "\""))
                {
                    using (OleDbCommand command = new OleDbCommand(sql, connection))
                    {
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
                        {
                            dataTable = new DataTable();
                            dataTable.Locale = CultureInfo.CurrentCulture;
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return dataTable;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                Int16 UserTypeId = Convert.ToInt16(btn.CommandArgument);
                foreach (GridViewRow row in gvList.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        int UserTypeID = Convert.ToInt32(gvList.DataKeys[row.RowIndex].Values[0]);
                        FileUpload flImport = (FileUpload)row.FindControl("flImport");
                        CheckBox chk = (CheckBox)row.FindControl("chkIsWhiteListed");
                        if (chk != null && (UserTypeId == UserTypeID))
                        {
                            ImportFile(chk, flImport, UserTypeID);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ImportFile(CheckBox chkIsWhiteListed, FileUpload flImport, int UserTypeId)
        {
            try
            {
                UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();
                if (flImport.HasFiles)
                {
                    if (flImport.PostedFile.ContentLength > 0)
                    {

                        //Chk for File Extension
                        string[] strFileTypes = { ".csv" };
                        string[] strMimeTypes = { "Excel/csv", "application/vnd.ms-excel" };
                        bool bChkFileType = false;
                        bChkFileType = GlobalFunctions.CheckFileExtension(flImport, strFileTypes);
                        #region "Comment"
                        bool bChkFileMimeType = false;
                        bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(flImport, strMimeTypes);
                        //if (!(bChkFileType && bChkFileMimeType))
                        //{
                        //    GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
                        //    SetFocus(flImport.ClientID);
                        //    return;
                        //}
                        #endregion
                        if (!bChkFileType)
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Only CSV File allowed to upload.", AlertType.Warning);
                            return;
                        }
                        string strFileName = flImport.FileName;
                        FileInfo fi = new FileInfo(strFileName);
                        string strFilExt = fi.Extension;
                        strFileName = "UserTypesEmailWhiteBlackList" + strFilExt;
                        string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "\\CSV\\User Types\\" + strFileName;
                        try
                        {
                            if (File.Exists(strFileUploadPath))
                            {
                                File.Delete(strFileUploadPath);
                            }

                            flImport.SaveAs(strFileUploadPath);
                            if (File.Exists(strFileUploadPath))
                            {
                                DataTable dtUserTypeEmailWhiteBlackList = CsvFileToDatatable(strFileUploadPath, true);
                                if (dtUserTypeEmailWhiteBlackList != null)
                                {
                                    if (dtUserTypeEmailWhiteBlackList.Rows.Count > 0)
                                    {
                                        DataColumn colCreatedBy = new System.Data.DataColumn("CreatedBy", typeof(Int32));
                                        colCreatedBy.DefaultValue = UserId;
                                        DataColumn colIsWhiteList = new System.Data.DataColumn("IsWhiteList", typeof(bool));
                                        if (chkIsWhiteListed.Checked)
                                            colIsWhiteList.DefaultValue = 1;
                                        else
                                            colIsWhiteList.DefaultValue = 0;

                                        DataColumn colIsActive = new System.Data.DataColumn("IsActive", typeof(Int32));
                                        colIsActive.DefaultValue = 1;
                                        DataColumn colUserTypeId = new System.Data.DataColumn("UserTypeId", typeof(Int32));
                                        colUserTypeId.DefaultValue = UserTypeId;

                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colUserTypeId);
                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colIsWhiteList);
                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colIsActive);
                                        dtUserTypeEmailWhiteBlackList.Columns.Add(colCreatedBy);

                                    }
                                    bool result = UserTypesBL.InsertEmailValidateImportList(dtUserTypeEmailWhiteBlackList, Convert.ToInt16(DBAction.Insert), objUserTypesBE);
                                    if (result)
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "User Types File Import compeleted Successfully.", AlertType.Success);
                                    else
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                            return;
                        }
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Upload File", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();
                Button btn = (Button)sender;
                Int16 UserTypeId = Convert.ToInt16(btn.CommandArgument);
                objUserTypesBE.UserTypeID = UserTypeId;
                string filename = "UserTypesEmailWhiteBlackList" + ".csv";

                List<UserTypesBE.CustomUserTypes> lstUserTypesBE = UserTypesBL.GetListUserTypes<UserTypesBE.CustomUserTypes>(objUserTypesBE);

                #region Export Mutliple Column using Datatable in CSV
                DataTable dtExport = new DataTable();
                if (lstUserTypesBE != null)
                {
                    dtExport = ToDataTable<UserTypesBE.CustomUserTypes>(lstUserTypesBE);
                    dtExport.Columns.Remove("TotalRow");
                    dtExport.Columns.Remove("UserTypeID");
                    dtExport.Columns.Remove("UserTypeName");
                    dtExport.Columns.Remove("IsActive");
                    dtExport.Columns.Remove("LanguageId");
                    dtExport.Columns.Remove("CreatedBy");
                    dtExport.Columns.Remove("IsWhiteList");
                    dtExport.Columns.Remove("UserTypeSequence");
                }
                else
                {
                    DataColumn colEmailId = new System.Data.DataColumn("EmailId", typeof(string));
                    colEmailId.DefaultValue = "EmailId";
                    DataColumn colDomain = new System.Data.DataColumn("Domain", typeof(string));
                    colDomain.DefaultValue = "Domain";
                    dtExport.Columns.Add(colEmailId);
                    dtExport.Columns.Add(colDomain);
                }
                WriteDelimitedData(dtExport, filename, ",");
                #endregion

                #region Do Not Delete
                #region Export only For Single Column using String Bulider in CSV
                //var csv = new StringBuilder();
                //if (lstUserTypesBE != null)
                //{
                //    csv.Append("EmailId" + Environment.NewLine);
                //    foreach (UserTypesBE.CustomUserTypes items in lstUserTypesBE)
                //    {
                //        csv.Append(items.EmailId + "," + items.Domain + Environment.NewLine);
                //    }
                //}
                //else
                //{
                //    csv.Append("EmailId" + "," + "Domain" + Environment.NewLine);
                //}
                ////string attachment = "attachment; filename=" + filename + "";
                //HttpContext.Current.Response.Clear();
                //Response.AddHeader("Content-Disposition", "attachment;filename=UserTypesEmailWhiteBlackList.csv");
                //Response.Output.Write(csv);
                //HttpContext.Current.Response.ContentType = "Excel/csv";
                //Session["lstUserTypesBE"] = null;
                //Response.End();
                #endregion
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void chkIsWhiteListed_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                bool res;
                UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();
                UserBE objUserBEnew = Session["User"] as UserBE;
                DataTable dtUserTypeEmailWhiteBlackList = null;
                CheckBox chkIsWhiteListed = (sender as CheckBox);

                GridViewRow row = (sender as CheckBox).NamingContainer as GridViewRow;
                int UserTypeId = Convert.ToInt16(gvList.DataKeys[row.RowIndex].Values["UserTypeId"]);
                string UserTypeName = Convert.ToString(gvList.DataKeys[row.RowIndex].Values["UserTypeName"]);

                objUserTypesBE.UserTypeID = UserTypeId;
                objUserTypesBE.CreatedBy = UserId;
                if ((row.FindControl("chkIsWhiteListed") as CheckBox).Checked)
                {
                    objUserTypesBE.IsWhiteList = true;
                    res = UserTypesBL.InsertEmailValidateImportList(dtUserTypeEmailWhiteBlackList, Convert.ToInt16(DBAction.Update), objUserTypesBE);
                }
                else
                {
                    objUserTypesBE.IsWhiteList = false;
                    res = UserTypesBL.InsertEmailValidateImportList(dtUserTypeEmailWhiteBlackList, Convert.ToInt16(DBAction.Update), objUserTypesBE);
                }
                if (res)
                {
                    //true
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Email List has been updated successfully.", AlertType.Success);
                    // objbase.CreateActivityLog("User Management", "Updated", id1 + "_" + UserName);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "No Records Exists for this Item.", AlertType.Warning);
                }
                BindUserTypeList();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void gvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvList.PageIndex = e.NewPageIndex;
                BindUserTypeList();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnUserSequenceSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<UserTypesBE.CustomUserTypes> lstSequenceList = new List<UserTypesBE.CustomUserTypes>();
                foreach (GridViewRow gvListRow in gvList.Rows)
                {
                    HiddenField hdnUserTypeID = (HiddenField)gvListRow.Cells[0].FindControl("hdnUserTypeID");
                    TextBox txtSequenceNo = (TextBox)gvListRow.Cells[0].FindControl("txtSequenceNo");
                    if (txtSequenceNo.Text == null || txtSequenceNo.Text == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please enter all the Sequence Values", AlertType.Warning);
                        return;
                    }
                    lstSequenceList.Add(new UserTypesBE.CustomUserTypes() { UserTypeID = Convert.ToInt16(hdnUserTypeID.Value), UserTypeSequence = Convert.ToInt64(txtSequenceNo.Text) });
                }
                DataTable dtUpdateSequence = GetDataIntoTable(lstSequenceList);

                DataTable dt = new DataTable();
                dt.Columns.Add("UserTypeID");
                dt.Columns.Add("UserTypeSequence");

                for (int i = 0; i < dtUpdateSequence.Rows.Count; i++)
                {
                    dt.Rows.Add();
                    dt.Rows[i]["UserTypeID"] = dtUpdateSequence.Rows[i]["UserTypeID"];
                    dt.Rows[i]["UserTypeSequence"] = dtUpdateSequence.Rows[i]["UserTypeSequence"];
                    //dt.Rows[i]["IsVatDisplay"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["IsVatDisplay"];
                    //dt.Rows[i]["VatRate"] = dtUpdateUserTypeCatalogueDetails.Rows[i]["VatRate"];
                }

                bool IsSequenceUpdated = UserTypesBL.UpdateUserTypeSequence(dt);
                if (IsSequenceUpdated)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "User Type Sequence updated succesfully", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating User Type Sequence", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected DataTable GetDataIntoTable(List<UserTypesBE.CustomUserTypes> returnList)
        {

            DataTable dt = GlobalFunctions.To_DataTable(returnList);
            dt.Columns["UserTypeID"].ColumnName = "UserTypeID";
            dt.Columns["UserTypeSequence"].ColumnName = "UserTypeSequence";
            //dt.Columns["Points"].ColumnName = "Points";
            return dt;
        }

        //public static DataTable ToDataTable<T>(List<T> items)
        //{
        //    DataTable dataTable = new DataTable(typeof(T).Name);

        //    //Get all the properties
        //    PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        //    foreach (PropertyInfo prop in Props)
        //    {
        //        //Setting column names as Property names
        //        dataTable.Columns.Add(prop.Name);
        //    }
        //    foreach (T item in items)
        //    {
        //        var values = new object[Props.Length];
        //        for (int i = 0; i < Props.Length; i++)
        //        {
        //            //inserting property values to datatable rows
        //            values[i] = Props[i].GetValue(item, null);
        //        }
        //        dataTable.Rows.Add(values);
        //    }
        //    //put a breakpoint here and check datatable
        //    return dataTable;
        //}

        #endregion

        protected void UpdateUserTypeMasterSelectionOption(string UserTypeMappingId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("IsActive", "1");
                dictionaryInstance.Add("UserTypeMappingTypeID", UserTypeMappingId);

                bool bRes = UserTypesBL.UpdateUserTypeOptionMaster(Constants.USP_UpdateUserTypeMasterOptions, dictionaryInstance, true);
                if (bRes)
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Success !!", AlertType.Success);
                    CreateActivityLog("UserTypeMapping btnUserSelectionSave", "Update", (UserTypeMappingId));
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Error !!", AlertType.Warning);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #region "UserSelection"
        protected void btnUserSelectionSave_Click(object sender, EventArgs e)
        {
            if (rdList.Items.Count > 0)
            {
                if (rdList.Items[0].Selected)
                    UpdateUserTypeMasterSelectionOption(rdList.Items[0].Value);
                else if (rdList.Items[1].Selected)
                    UpdateUserTypeMasterSelectionOption(rdList.Items[1].Value);
                else if (rdList.Items[2].Selected)
                    UpdateUserTypeMasterSelectionOption(rdList.Items[2].Value);
                else if (rdList.Items[3].Selected)
                    UpdateUserTypeMasterSelectionOption(rdList.Items[3].Value);
            }
        }
        #endregion

        #region "Country"
        private void BindCountry()
        {
            try
            {
                Dictionary<string, string> objParam = new Dictionary<string, string>();
                objParam.Add("LanguageID", Convert.ToString(GlobalFunctions.GetLanguageId()));
                objUserTypeCountryMaster = UserTypesBL.GetAllUserTypeCountryMaster(Constants.USP_GetUserTypeCountryMaster, objParam, true);

                rptCountry.DataSource = objUserTypeCountryMaster.lstCountryMasterBE;
                rptCountry.DataBind();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void rptCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblIsActive = e.Item.FindControl("lblIsActive") as Label;
                    DropDownList ddlCountryUserType = e.Item.FindControl("ddlCountryUserType") as DropDownList;

                    string strIsActive = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "IsActive"));
                    lblIsActive.Text = strIsActive;

                    ddlCountryUserType.DataSource = objUserTypeCountryMaster.lstUserTypeLanguagesBE;
                    ddlCountryUserType.DataTextField = "UserTypeName";
                    ddlCountryUserType.DataValueField = "UserTypeID";
                    ddlCountryUserType.DataBind();
                    //GlobalFunctions.AddDropdownItem(ref ddlCountryUserType);
                    ddlCountryUserType.Items.Insert(0, new ListItem("UnAllocate", "0"));

                    ddlCountryUserType.SelectedValue = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "UserTypeID"));
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void btnCountrySave_Click(object sender, EventArgs e)
        {
            try
            {
                UserBE lst = Session["StoreUser"] as UserBE;
                DataTable table = new DataTable();
                table.Columns.Add("CountryId", typeof(Int16));
                table.Columns.Add("UserTypeID", typeof(Int16));
                table.Columns.Add("ModifiedBy", typeof(int));

                foreach (RepeaterItem item in rptCountry.Items)
                {
                    DataRow dr = table.NewRow();
                    HiddenField hdfCountryID = item.FindControl("hdfCountryID") as HiddenField;
                    dr["CountryId"] = Convert.ToInt16(hdfCountryID.Value);

                    DropDownList ddlCountryUserType = item.FindControl("ddlCountryUserType") as DropDownList;
                    dr["UserTypeID"] = ddlCountryUserType.SelectedValue;
                    dr["ModifiedBy"] = Convert.ToInt32(lst.UserId);

                    table.Rows.Add(dr);
                }
                bool bres = false;
                bres = UserTypesBL.SetCountryMaster(table);
                if (bres)
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Success !!", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Error !!", AlertType.Failure);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #endregion

        #region "CustomMapping"
        private void BindCustomMappingValidation()
        {
            try
            {
                List<UserTypeCustomMapping> objUserTypeCustomMapping = new List<UserTypeCustomMapping>();
                Dictionary<string, string> objparam = new Dictionary<string, string>();
                objparam.Add("LanguageID", Convert.ToString(GlobalFunctions.GetLanguageId()));
                objUserTypeCustomMapping = UserTypesBL.GetCustomDetailsValue(Constants.USP_GetDefaultCustomField, objparam, true);
                txtCustomFieldName.Text = objUserTypeCustomMapping[0].LabelTitle;
                rdbRegAsDef.Checked = objUserTypeCustomMapping[0].RegisterAsDefault;
                rdbDecReg.Checked = objUserTypeCustomMapping[0].DeclineRegister;
                rptCustomMapping.DataSource = objUserTypeCustomMapping;
                rptCustomMapping.DataBind();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptCustomMapping_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();
                Label lblUserType = (Label)e.Item.FindControl("lblUserType");
                HiddenField hdfCustommapUserTypeID = (HiddenField)e.Item.FindControl("hdfCustommapUserTypeID");
                HiddenField hdfCustomMapRegistrationFieldsConfigurationId = (HiddenField)e.Item.FindControl("hdfCustomMapRegistrationFieldsConfigurationId");
                DefaultRegCustomFieldID = Convert.ToInt16(hdfCustomMapRegistrationFieldsConfigurationId.Value);

                #region "Export"
                if (e.CommandName.ToLower() == "export")
                {
                    int RegFieldConfigID = Convert.ToInt16(e.CommandArgument);
                    string filename = "UserTypeCustomMapping" + "" + ".csv";

                    List<RegistrationFieldDataMasterValidationBE> lstCustomFieldData = new List<RegistrationFieldDataMasterValidationBE>();

                    Dictionary<string, string> objD = new Dictionary<string, string>();
                    objD.Add("RegistrationFieldsConfigurationId", Convert.ToString(e.CommandArgument));
                    lstCustomFieldData = UserTypesBL.GetListCustomByRegID(Constants.USP_GetAllRegistrationFieldDataMasterValidation, objD, true);
                    DataTable dtExport = new DataTable();
                    if (lstCustomFieldData != null)
                    {

                        dtExport = ToDataTable<RegistrationFieldDataMasterValidationBE>(lstCustomFieldData);
                        #region column to hide at export
                        dtExport.Columns.Remove("RegistrationFieldDataMasterId");
                        dtExport.Columns.Remove("RegistrationFieldsConfigurationId");
                        dtExport.Columns.Remove("RegistrationFieldDataId");
                        dtExport.Columns.Remove("IsActive");
                        dtExport.Columns.Remove("LanguageId");
                        dtExport.Columns.Remove("CreatedBy");
                        dtExport.Columns.Remove("CreatedDate");
                        dtExport.Columns.Remove("ModifiedBy");
                        dtExport.Columns.Remove("ModifiedDate");
                        #endregion
                    }
                    else
                    {
                        DataColumn colRegistrationFieldDataValue = new System.Data.DataColumn("RegistrationFieldDataValue", typeof(string));
                        colRegistrationFieldDataValue.DefaultValue = "RegistrationFieldDataValue";
                        DataColumn colUserTypeID = new System.Data.DataColumn("UserTypeID", typeof(string));
                        colUserTypeID.DefaultValue = "UserTypeID";
                        dtExport.Columns.Add(colRegistrationFieldDataValue);
                        dtExport.Columns.Add(colUserTypeID);
                        dtExport.Rows.Add();
                        dtExport.Rows[0]["RegistrationFieldDataValue"] = "";
                        dtExport.Rows[0]["UserTypeID"] = hdfCustommapUserTypeID.Value;
                        //if (Convert.ToString(hdfCustommapUserTypeID.Value) != "0")
                        //{ dr["UserTypeID"] = "UserTypeID"; }
                        //else
                        //    dr["UserTypeID"] = "UserTypeID";
                        //dtExport.Rows.Add(dr);
                    }

                    WriteDelimitedData(dtExport, filename, ",");


                    #region comment
                    //if (lstCustomFieldData.Count > 0)
                    //{
                    //    List<object> lstAnonymousObjs = new List<object>();

                    //    foreach (RegistrationFieldDataMasterValidationBE CustomDataValue in lstCustomFieldData)
                    //    {
                    //        lstAnonymousObjs.Add(new
                    //        {
                    //            RegistrationFieldDataValue = CustomDataValue.RegistrationFieldDataValue,
                    //            UserTypeID = CustomDataValue.UserTypeID
                    //        });
                    //    }
                    //    dsData = CreateDataSet(lstAnonymousObjs);
                    //    if (dsData == null)
                    //    {
                    //        GlobalFunctions.ShowModalAlertMessages(this.Page, "No Custom Field Data Found.", AlertType.Warning);
                    //        return;
                    //    }
                    //    dtExportToExcel = dsData.Tables[0];

                    //    using (ExcelPackage pck = new ExcelPackage())
                    //    {
                    //        //Create the worksheet
                    //        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ProductData");
                    //        // DataTable NewTable = tbl.DefaultView.ToTable(false, "ProductCode","LanguageId","ProductName","ProductDescription", "FurtherDescription", "");

                    //        //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                    //        ws.Cells["A1"].LoadFromDataTable(dtExportToExcel, true);

                    //        Response.Clear();
                    //        //Write it back to the client
                    //        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //        Response.AddHeader("content-disposition", "attachment;  filename=ProductData.xlsx");
                    //        Response.BinaryWrite(pck.GetAsByteArray());
                    //        Response.End();
                    //    }
                    //}
                    // else
                    // {
                    //     GlobalFunctions.ShowModalAlertMessages(this.Page, "No Products Found.", AlertType.Warning);
                    // }
                    #endregion
                }
                #endregion

                #region "Import"
                if (e.CommandName.ToLower() == "import")
                {
                    FileUpload flCustomImport = (FileUpload)e.Item.FindControl("flCustomImport");

                    if (flCustomImport.HasFiles)
                    {
                        if (flCustomImport.PostedFile.ContentLength > 0)
                        {

                            //Chk for File Extension
                            string[] strFileTypes = { ".csv" };
                            string[] strMimeTypes = { "Excel/csv", "application/vnd.ms-excel" };
                            bool bChkFileType = false;
                            bChkFileType = GlobalFunctions.CheckFileExtension(flCustomImport, strFileTypes);
                            #region "Comment"
                            bool bChkFileMimeType = false;
                            bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(flCustomImport, strMimeTypes);
                            //if (!(bChkFileType && bChkFileMimeType))
                            //{
                            //    GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
                            //    SetFocus(flCustomImport.ClientID);
                            //    return;
                            //}
                            #endregion
                            if (!bChkFileType)
                            {
                                GlobalFunctions.ShowModalAlertMessages(Page, "Only CSV File allowed to upload.", AlertType.Warning);
                                return;
                            }
                            string strFileName = flCustomImport.FileName;
                            FileInfo fi = new FileInfo(strFileName);
                            string strFilExt = fi.Extension;
                            strFileName = "CustomFieldMapping" + strFilExt;
                            string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "\\CSV\\User Types\\" + strFileName;
                            if (File.Exists(strFileUploadPath))
                            {
                                File.Delete(strFileUploadPath);
                            }

                            flCustomImport.SaveAs(strFileUploadPath);
                            if (File.Exists(strFileUploadPath))
                            {
                                DataTable dtUserTypeCustomMappingImport = CsvFileToDatatable(strFileUploadPath, true);
                                if (dtUserTypeCustomMappingImport != null)
                                {
                                    if (dtUserTypeCustomMappingImport.Rows.Count > 0)
                                    {
                                        DataColumn colRegistrationFieldsConfigurationId = new System.Data.DataColumn("RegistrationFieldsConfigurationId", typeof(Int32));
                                        colRegistrationFieldsConfigurationId.DefaultValue = hdfCustomMapRegistrationFieldsConfigurationId.Value;
                                        DataColumn colLanguageId = new System.Data.DataColumn("LanguageID", typeof(Int32));
                                        colLanguageId.DefaultValue = 1;
                                        DataColumn colIsActive = new System.Data.DataColumn("IsActive", typeof(Int32));
                                        colIsActive.DefaultValue = 1;
                                        DataColumn colCreatedBy = new System.Data.DataColumn("CreatedBy", typeof(Int32));
                                        colCreatedBy.DefaultValue = UserId;

                                        dtUserTypeCustomMappingImport.Columns.Add(colRegistrationFieldsConfigurationId);
                                        dtUserTypeCustomMappingImport.Columns.Add(colLanguageId);
                                        dtUserTypeCustomMappingImport.Columns.Add(colIsActive);
                                        dtUserTypeCustomMappingImport.Columns.Add(colCreatedBy);

                                    }
                                    bool result = UserTypesBL.InsertCustomMappingImportFields(dtUserTypeCustomMappingImport, Convert.ToInt16(DBAction.Insert), objUserTypesBE);
                                    if (result)
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "User Types File Import compeleted Successfully.", AlertType.Success);
                                    else
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                }
                            }
                        }
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please select the File to Import!!", AlertType.Warning);
                    }
                }
                #endregion

                #region "EDIT" Added by SHRIGANESH SINGH 06 July 2016
                if (e.CommandName.ToLower() == "edits")
                {
                    try
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:ShowEditModal();", true);
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
                #endregion
        }

        public DataSet CreateDataSet<T>(List<T> list)
        {
            //list is nothing or has nothing, return nothing (or add exception handling)
            if (list == null || list.Count == 0) { return null; }

            //get the type of the first obj in the list
            var obj = list[0].GetType();

            //now grab all properties
            var properties = obj.GetProperties();

            //make sure the obj has properties, return nothing (or add exception handling)
            if (properties.Length == 0) { return null; }

            //it does so create the dataset and table
            var dataSet = new DataSet();
            var dataTable = new DataTable();

            //now build the columns from the properties
            var columns = new DataColumn[properties.Length];
            for (int i = 0; i < properties.Length; i++)
            {
                columns[i] = new DataColumn(properties[i].Name, properties[i].PropertyType);
            }

            //add columns to table
            dataTable.Columns.AddRange(columns);

            //now add the list values to the table
            foreach (var item in list)
            {
                //create a new row from table
                var dataRow = dataTable.NewRow();

                //now we have to iterate thru each property of the item and retrieve it's value for the corresponding row's cell
                var itemProperties = item.GetType().GetProperties();

                for (int i = 0; i < itemProperties.Length; i++)
                {
                    dataRow[i] = itemProperties[i].GetValue(item, null);
                }

                //now add the populated row to the table
                dataTable.Rows.Add(dataRow);
            }

            //add table to dataset
            dataSet.Tables.Add(dataTable);

            //return dataset
            return dataSet;
        }

        protected void btnSaveCustomFieldName_Click(object sender, EventArgs e)
        {
            if (ddlLanguage.SelectedIndex > -1)
            {
                if (txtCustomFieldName.Text != null || txtCustomFieldName.Text != "")
                {
                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("LanguageID", ddlLanguage.SelectedValue);
                    dictionaryInstance.Add("LabelTitle", txtCustomFieldName.Text);
                    dictionaryInstance.Add("RegistrationFieldsConfigurationId", Convert.ToString("11"));
                    if (rdbRegAsDef.Checked)
                    {
                        dictionaryInstance.Add("RegisterAsDefault", "1");
                        dictionaryInstance.Add("DeclineRegister", "0");
                    }
                    else
                    {
                        dictionaryInstance.Add("RegisterAsDefault", "0");
                        dictionaryInstance.Add("DeclineRegister", "1");
                    }

                    bool bResult = UserTypesBL.UpdateCustomFieldNames(dictionaryInstance);
                    if (bResult)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field Name Updated Successfully.", AlertType.Success);
                        CreateActivityLog("UserTypeMapping btnSaveCustomFieldName", "Update", txtCustomFieldName.Text);
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "There was an Error while updating Custom Field Name", AlertType.Failure);
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "Please Enter the Custom Field Name", AlertType.Warning);
                }
            }
        }

        protected void btnCancelCustomFieldName_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:CloseEditModal();", true);
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<UserTypeCustomMapping> objUserTypeCustomMapping = new List<UserTypeCustomMapping>();
            Dictionary<string, string> objparam = new Dictionary<string, string>();
            objparam.Add("LanguageID", Convert.ToString(ddlLanguage.SelectedValue));
            objUserTypeCustomMapping = UserTypesBL.GetCustomDetailsValue(Constants.USP_GetDefaultCustomField, objparam, true);
            txtCustomFieldName.Text = objUserTypeCustomMapping[0].LabelTitle;

            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:ShowEditModal();", true);

        }

        #endregion

        private void WriteDelimitedData(DataTable dt, string fileName, string delimiter)
        {
            //prepare the output stream
            Response.Clear();
            Response.ContentType = "Excel/csv";
            Response.AppendHeader("Content-Disposition",
                string.Format("attachment; filename={0}", fileName));

            string value = "";
            StringBuilder builder = new StringBuilder();

            //write the csv column headers
            for (int i = 0; i < dt.Columns.Count; i++)
            {

                value = dt.Columns[i].ColumnName;
                // Implement special handling for values that contain comma or quote
                // Enclose in quotes and double up any double quotes
                if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                else
                {
                    builder.Append(value);

                }

                Response.Output.Write(value);
                HttpContext.Current.Response.ContentType = "Excel/csv";

                //Response.Write(value);
                Response.Write((i < dt.Columns.Count - 1) ? delimiter : Environment.NewLine);
                builder.Clear();
            }

            //write the data
            foreach (DataRow row in dt.Rows)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    value = row[i].ToString();
                    // Implement special handling for values that contain comma or quote
                    // Enclose in quotes and double up any double quotes

                    if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                        builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                    else
                    {
                        builder.Append(value);

                    }
                    Response.Output.Write(builder);
                    // Response.Write(builder.ToString());
                    Response.Write((i < dt.Columns.Count - 1) ? delimiter : Environment.NewLine);
                    builder.Clear();
                }
            }

            Response.End();
        }

    }

}