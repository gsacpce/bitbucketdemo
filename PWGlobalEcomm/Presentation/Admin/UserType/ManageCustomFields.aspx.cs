﻿using Presentation;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_UserType_ManageCustomFields : BasePage //System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.DropDownList ddlUserTyepNames, ddlLanguages;
        protected global::System.Web.UI.WebControls.GridView gvCustomFields;

        Int16 LanguageId;

        List<UserTypesDetailsBE> lstUTDBE = new List<UserTypesDetailsBE>();

        /// <summary>
        /// Author  : SHRIGANESH SINGH
        /// Date    : 21 June 2016
        /// Scope   : Page Load Events
        /// </summary>
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    StoreBE objStoreBE = new StoreBE();
                    objStoreBE = StoreBL.GetStoreDetails();
                    GlobalFunctions.BindAllLanguage(objStoreBE, ref ddlLanguages);
                    BindUserTypeNames(ddlLanguages.SelectedValue);
                }
                Page.Title = "Manage Custom Fields";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
                
        private void BindUserTypeNames(string selectedValue)
        {
            lstUTDBE = UserTypesBL.GetAllUserTypeDetailsList(Constants.USP_GetAllUserTypeDetailForEdit, Convert.ToInt32(selectedValue));
            if (lstUTDBE != null)
            {
                ddlUserTyepNames.DataSource = lstUTDBE;
                ddlUserTyepNames.DataTextField = "UserTypeName";
                ddlUserTyepNames.DataValueField = "UserTypeID";
                ddlUserTyepNames.DataBind();
            }
        }

        private void GetCustomFieldsData(byte HierarchyLevel)
        {
            LanguageId = Convert.ToInt16(ddlLanguages.SelectedValue);
            List<RegistrationCustomFieldBE> GetRegistrationCustomField = RegistrationCustomFieldBL.GetAllCustomFields();
            List<RegistrationCustomFieldBE> GetHierarchyLevelRegistrationCustomField = null;
            if (GetRegistrationCustomField != null)
            {
                if (GetRegistrationCustomField.Count > 0)
                {
                    switch (HierarchyLevel)
                    {
                        case 1:
                        case 2:
                        case 3:
                            GetHierarchyLevelRegistrationCustomField = GetRegistrationCustomField.FindAll(x => x.LanguageId == LanguageId && x.UserHierarchyId == HierarchyLevel);
                            break;
                        case 4:
                            GetHierarchyLevelRegistrationCustomField = GetRegistrationCustomField.FindAll(x => x.LanguageId == LanguageId && x.UserHierarchyId == 0);
                            break;
                        default:
                            break;
                    }
                }
            }

            if (GetHierarchyLevelRegistrationCustomField != null)
            {
                if (GetHierarchyLevelRegistrationCustomField.Count > 0)
                {
                    gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                    gvCustomFields.DataBind();
                }
                else
                {
                    GetHierarchyLevelRegistrationCustomField = new List<RegistrationCustomFieldBE>();
                    RegistrationCustomFieldBE objRegistrationCustomFieldBE = new RegistrationCustomFieldBE();
                    objRegistrationCustomFieldBE.DisplayOrder = 0;
                    objRegistrationCustomFieldBE.FieldType = 1;
                    objRegistrationCustomFieldBE.LabelTitle = "temp";
                    objRegistrationCustomFieldBE.FieldTypeName = "temp";
                    GetHierarchyLevelRegistrationCustomField.Add(objRegistrationCustomFieldBE);
                    gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                    gvCustomFields.DataBind();
                    gvCustomFields.Rows[0].Visible = false;
                }
            }
            else
            {
                GetHierarchyLevelRegistrationCustomField = new List<RegistrationCustomFieldBE>();
                RegistrationCustomFieldBE objRegistrationCustomFieldBE = new RegistrationCustomFieldBE();
                objRegistrationCustomFieldBE.DisplayOrder = 0;
                objRegistrationCustomFieldBE.FieldType = 1;
                objRegistrationCustomFieldBE.LabelTitle = "temp";
                objRegistrationCustomFieldBE.FieldTypeName = "temp";
                GetHierarchyLevelRegistrationCustomField.Add(objRegistrationCustomFieldBE);
                gvCustomFields.DataSource = GetHierarchyLevelRegistrationCustomField;
                gvCustomFields.DataBind();
                gvCustomFields.Rows[0].Visible = false;
            }
        }

        protected void ddlUserTyepNames_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            GetCustomFieldsData(Convert.ToByte(ddlUserTyepNames.SelectedValue));
        }

        protected void ddlLanguages_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        
        protected void ddlFieldTypesEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl.Parent.Parent;
            int idx = row.RowIndex;

            TextBox txtCustomFieldDataEdit = (TextBox)row.Cells[4].FindControl("txtCustomFieldDataEdit");
            RequiredFieldValidator rfvCustomFieldDataEdit = (RequiredFieldValidator)row.Cells[4].FindControl("rfvCustomFieldDataEdit");
            if (ddl.SelectedItem.Text.ToLower() == "dropdown" || ddl.SelectedItem.Text.ToLower() == "checkbox")
            {
                txtCustomFieldDataEdit.Visible = true;
                rfvCustomFieldDataEdit.Enabled = true;
            }
            else
            {
                txtCustomFieldDataEdit.Visible = false;
                rfvCustomFieldDataEdit.Enabled = false;
            }
        }

        protected void ddlFieldTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl.Parent.Parent;
            int idx = row.RowIndex;

            TextBox txtCustomFieldData = (TextBox)row.Cells[4].FindControl("txtCustomFieldData");
            RequiredFieldValidator rfvCustomFieldData = (RequiredFieldValidator)row.Cells[4].FindControl("rfvCustomFieldData");
            if (ddl.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddl.SelectedItem.Text.ToLower().Trim() == "checkbox")
            {
                txtCustomFieldData.Visible = true;
                rfvCustomFieldData.Enabled = true;
            }
            else
            {
                txtCustomFieldData.Visible = false;
                rfvCustomFieldData.Enabled = false;
            }
        }

        protected void gvCustomFields_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {
                RegistrationCustomFieldBE objRegistrationCustomFieldBE = (RegistrationCustomFieldBE)e.Row.DataItem;
                DropDownList ddlFieldTypesEdit = (DropDownList)e.Row.FindControl("ddlFieldTypesEdit");
                CheckBox chkIsVisibleEdit = (CheckBox)e.Row.FindControl("chkIsVisibleEdit");
                CheckBox chkIsMandatoryEdit = (CheckBox)e.Row.FindControl("chkIsMandatoryEdit");
                TextBox txtCustomFieldDataEdit = (TextBox)e.Row.FindControl("txtCustomFieldDataEdit");
                RequiredFieldValidator rfvCustomFieldDataEdit = (RequiredFieldValidator)e.Row.FindControl("rfvCustomFieldDataEdit");
                ddlFieldTypesEdit.DataSource = RegistrationCustomFieldBL.GetFieldTypeMasterDetails();
                ddlFieldTypesEdit.DataTextField = "FieldTypeName";
                ddlFieldTypesEdit.DataValueField = "FieldTypeId";
                ddlFieldTypesEdit.DataBind();
                ddlFieldTypesEdit.SelectedValue = Convert.ToString(objRegistrationCustomFieldBE.FieldType);
                chkIsVisibleEdit.Checked = objRegistrationCustomFieldBE.IsVisible;
                chkIsMandatoryEdit.Checked = objRegistrationCustomFieldBE.IsMandatory;

                if (ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "checkbox")
                {
                    txtCustomFieldDataEdit.Visible = true;
                    rfvCustomFieldDataEdit.Enabled = true;
                    string CustomData = "";
                    int count = 0;
                    foreach (RegistrationCustomFieldBE.RegistrationFeildDataMasterBE item in objRegistrationCustomFieldBE.CustomFieldData)
                    {
                        if (count == 0)
                            CustomData += item.RegistrationFieldDataValue;
                        else
                            CustomData += "|" + item.RegistrationFieldDataValue;
                        count++;
                    }
                    txtCustomFieldDataEdit.Text = CustomData;
                }
                else
                    rfvCustomFieldDataEdit.Enabled = false;
            }

            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                RegistrationCustomFieldBE objRegistrationCustomFieldBE = (RegistrationCustomFieldBE)e.Row.DataItem;
                Label lblCustomFieldData = (Label)e.Row.FindControl("lblCustomFieldData");
                if (objRegistrationCustomFieldBE.FieldTypeName.ToLower().Trim() == "dropdown" || objRegistrationCustomFieldBE.FieldTypeName.ToLower().Trim() == "checkbox")
                {
                    string CustomData = "";
                    int count = 0;
                    foreach (RegistrationCustomFieldBE.RegistrationFeildDataMasterBE item in objRegistrationCustomFieldBE.CustomFieldData)
                    {
                        if (count == 0)
                            CustomData += item.RegistrationFieldDataValue;
                        else
                            CustomData += "|" + item.RegistrationFieldDataValue;
                        count++;
                    }
                    lblCustomFieldData.Text = CustomData;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlFieldTypes = (DropDownList)e.Row.FindControl("ddlFieldTypes");
                ddlFieldTypes.DataSource = RegistrationCustomFieldBL.GetFieldTypeMasterDetails();
                ddlFieldTypes.DataTextField = "FieldTypeName";
                ddlFieldTypes.DataValueField = "FieldTypeId";
                ddlFieldTypes.DataBind();

            }
        }

        protected void gvCustomFields_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvCustomFields.EditIndex = e.NewEditIndex;
            byte HierarchyLevel = Convert.ToByte(ddlUserTyepNames.SelectedValue);
            GetCustomFieldsData(HierarchyLevel);
        }

        protected void gvCustomFields_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Int16 RegistrationFieldsConfigurationId = Convert.ToInt16(gvCustomFields.DataKeys[e.RowIndex].Values["RegistrationFieldsConfigurationId"]);
                UserBE user = (UserBE)Session["StoreUser"];
                TextBox txtCustomFieldEdit = (TextBox)gvCustomFields.Rows[e.RowIndex].FindControl("txtCustomFieldEdit");
                DropDownList ddlFieldTypesEdit = (DropDownList)gvCustomFields.Rows[e.RowIndex].FindControl("ddlFieldTypesEdit");
                CheckBox chkIsVisibleEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsVisibleEdit");
                CheckBox chkIsMandatoryEdit = (CheckBox)gvCustomFields.Rows[e.RowIndex].FindControl("chkIsMandatoryEdit");

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LabelTitle", Convert.ToString(txtCustomFieldEdit.Text.Trim()));
                dictionaryInstance.Add("FieldType", Convert.ToString(ddlFieldTypesEdit.SelectedValue));
                dictionaryInstance.Add("IsVisible", Convert.ToString(chkIsVisibleEdit.Checked));
                dictionaryInstance.Add("IsMandatory", Convert.ToString(chkIsMandatoryEdit.Checked));
                dictionaryInstance.Add("UserHierarchyLevel", Convert.ToString(ddlUserTyepNames.SelectedValue));
                if (ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypesEdit.SelectedItem.Text.ToLower().Trim() == "checkbox")
                {
                    TextBox txtCustomFieldDataEdit = (TextBox)gvCustomFields.Rows[e.RowIndex].FindControl("txtCustomFieldDataEdit");
                    dictionaryInstance.Add("CustomFieldData", Convert.ToString(txtCustomFieldDataEdit.Text.Trim()));
                }
                dictionaryInstance.Add("UserId", Convert.ToString(user.UserId));
                dictionaryInstance.Add("RegistrationFieldsConfigurationId", Convert.ToString(RegistrationFieldsConfigurationId));
                dictionaryInstance.Add("LanguageIdToUpdate", Convert.ToString(ddlLanguages.SelectedValue));
                dictionaryInstance.Add("Flag", Convert.ToString('E'));

                bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);

                if (Result)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field updated successfully", AlertType.Success);
                    CreateActivityLog("ManageCustomFields gvCustomFields_RowUpdating", "Update", "");
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating. Please try again", AlertType.Failure);

                gvCustomFields.EditIndex = -1;
                byte HierarchyLevel = Convert.ToByte(ddlUserTyepNames.SelectedValue);
                GetCustomFieldsData(HierarchyLevel);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void gvCustomFields_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Int16 RegistrationFieldsConfigurationId = Convert.ToInt16(gvCustomFields.DataKeys[e.RowIndex].Values["RegistrationFieldsConfigurationId"]);

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RegistrationFieldsConfigurationId", Convert.ToString(RegistrationFieldsConfigurationId));
                dictionaryInstance.Add("Flag", Convert.ToString('D'));

                bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);

                if (Result){
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field deleted successfully", AlertType.Success);
                    CreateActivityLog("ManageCustomFields gvCustomFields_RowDeleting", "Delete", Convert.ToString(RegistrationFieldsConfigurationId));
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while deleting. Please try again", AlertType.Failure);

                byte HierarchyLevel = Convert.ToByte(ddlUserTyepNames.SelectedValue);
                GetCustomFieldsData(HierarchyLevel);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void gvCustomFields_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                try
                {
                    UserBE user = (UserBE)Session["StoreUser"];
                    TextBox txtCustomField = (TextBox)gvCustomFields.FooterRow.FindControl("txtCustomField");
                    DropDownList ddlFieldTypes = (DropDownList)gvCustomFields.FooterRow.FindControl("ddlFieldTypes");
                    CheckBox chkIsVisible = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsVisible");
                    CheckBox chkIsMandatory = (CheckBox)gvCustomFields.FooterRow.FindControl("chkIsMandatory");

                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("LabelTitle", Convert.ToString(txtCustomField.Text.Trim()));
                    dictionaryInstance.Add("FieldType", Convert.ToString(ddlFieldTypes.SelectedValue));
                    dictionaryInstance.Add("IsVisible", Convert.ToString(chkIsVisible.Checked));
                    dictionaryInstance.Add("IsMandatory", Convert.ToString(chkIsMandatory.Checked));
                    dictionaryInstance.Add("UserHierarchyLevel", Convert.ToString(ddlUserTyepNames.SelectedValue));
                    if (ddlFieldTypes.SelectedItem.Text.ToLower().Trim() == "dropdown" || ddlFieldTypes.SelectedItem.Text.ToLower().Trim() == "checkbox")
                    {
                        TextBox txtCustomFieldData = (TextBox)gvCustomFields.FooterRow.FindControl("txtCustomFieldData");
                        dictionaryInstance.Add("CustomFieldData", Convert.ToString(txtCustomFieldData.Text.Trim()));
                    }
                    dictionaryInstance.Add("UserId", Convert.ToString(user.UserId));
                    dictionaryInstance.Add("Flag", Convert.ToString('A'));

                    bool Result = RegistrationCustomFieldBL.AEDRegistrationCustomField(dictionaryInstance);

                    if (Result)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Custom Field inserted successfully", AlertType.Success);
                        CreateActivityLog("ManageCustomFields gvCustomFields_RowCommand", "Insert", "");
                    }
                    else
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while inserting. Please try again", AlertType.Failure);

                    byte HierarchyLevel = Convert.ToByte(ddlUserTyepNames.SelectedValue);
                    GetCustomFieldsData(HierarchyLevel);

                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            else
            {

            }
        }

        protected void gvCustomFields_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvCustomFields.EditIndex = -1;
            byte HierarchyLevel = Convert.ToByte(ddlUserTyepNames.SelectedValue);
            GetCustomFieldsData(HierarchyLevel);
        }
    }
}