﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_UserType_UserTypeSetting : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        protected global::System.Web.UI.WebControls.GridView GrdDynamic;
        UserTypesBE objUserTypesBE = new UserTypesBE();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                StoreBE objStoreBE = new StoreBE();
                objStoreBE = StoreBL.GetStoreDetails();
                GlobalFunctions.BindAllLanguage(objStoreBE, ref ddlLanguage);
                objUserTypesBE = UserTypesBL.getCollectionItem(Constants.USP_ManageUserType_SAED, null, true);
            }

            LoadDynamicGridView();
        }
        public void LoadDynamicGridView()
        {
            //Create an instance of DataTable
            DataTable dt = new DataTable();

            BoundField bfield = new BoundField();
            bfield.HeaderText = "User Type ID";
            bfield.DataField = "UserTypeID";
            GrdDynamic.Columns.Add(bfield);

            TemplateField tfield = new TemplateField();
            tfield.HeaderText = "User Type Name";
            GrdDynamic.Columns.Add(tfield);

            tfield = new TemplateField();
            tfield.HeaderText = "Vat Display";
            GrdDynamic.Columns.Add(tfield);

            BindGrid();
        }

        private void BindGrid()
        {
            DataTable dt = new DataTable();
            //dt.Columns.AddRange(new DataColumn[3] { new DataColumn("User Type ID",typeof(int)),
            //                                        new DataColumn("User Type Name", typeof(string)),
            //                                        new DataColumn("Vat Display",typeof(string)) });
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("UserTypeID",typeof(int)),
                                                    new DataColumn("UserTypeName", typeof(string)),
                                                    new DataColumn("IsVatDisplay",typeof(string)) });

            dt.Rows.Add(objUserTypesBE.lstUserTypeDetails[0].UserTypeID, objUserTypesBE.lstUserTypeLanguages[0].UserTypeName.ToString(), objUserTypesBE.lstUserTypeCatalogue[0].IsVatDisplay.ToString());
            dt.Rows.Add(objUserTypesBE.lstUserTypeDetails[1].UserTypeID, objUserTypesBE.lstUserTypeLanguages[1].UserTypeName.ToString(), objUserTypesBE.lstUserTypeCatalogue[1].IsVatDisplay.ToString());
            dt.Rows.Add(objUserTypesBE.lstUserTypeDetails[2].UserTypeID, objUserTypesBE.lstUserTypeLanguages[2].UserTypeName.ToString(), objUserTypesBE.lstUserTypeCatalogue[2].IsVatDisplay.ToString());
            dt.Rows.Add(objUserTypesBE.lstUserTypeDetails[3].UserTypeID, objUserTypesBE.lstUserTypeLanguages[3].UserTypeName.ToString(), objUserTypesBE.lstUserTypeCatalogue[3].IsVatDisplay.ToString());
            GrdDynamic.DataSource = dt;
            GrdDynamic.DataBind();
        }
    }
}