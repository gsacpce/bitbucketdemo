﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Presentation
{
    public partial class Admin_UserType_PaymentTypeManagement : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.GridView gvList;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        protected global::System.Web.UI.WebControls.DropDownList ddlUserTypes;
        protected global::System.Web.UI.WebControls.HiddenField hdnselectedUserTypeId;

        UserBE objUserBE;
        public int UserId;
        UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();
        UserTypesBE.UserTypePaymentDetails objUserTypePaymentDetails = new UserTypesBE.UserTypePaymentDetails();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateLanguage();
                PopulateUserTypeDropDown();
            }
        }
        protected void PopulateGrid(Int16 Languageid)
        {
            try
            {
                objUserTypePaymentDetails.Languageid = Languageid;
                objUserTypePaymentDetails.UserTypeID = Convert.ToInt32(ddlUserTypes.SelectedValue);
                List<UserTypesBE.UserTypePaymentDetails> lstUserTypePaymentDetails = UserTypesBL.GetAssignedPaymentOptionForUserType<UserTypesBE.UserTypePaymentDetails>(objUserTypePaymentDetails);
                if (lstUserTypePaymentDetails != null)
                {
                    if (lstUserTypePaymentDetails.Count > 0)
                    {
                        gvList.DataSource = lstUserTypePaymentDetails;
                        gvList.DataBind();
                    }
                }
                else
                {
                    gvList.DataSource = null;
                    gvList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void PopulateUserTypeDropDown()
        {
            try
            {
                objUserTypesBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                List<UserTypesBE.CustomUserTypes> lstUserTypesBE = UserTypesBL.GetListUserTypes<UserTypesBE.CustomUserTypes>(objUserTypesBE);
                if (lstUserTypesBE != null)
                {
                    if (lstUserTypesBE.Count > 0)
                    {
                        ddlUserTypes.DataSource = lstUserTypesBE;
                        ddlUserTypes.DataTextField = "UserTypeName";
                        ddlUserTypes.DataValueField = "UserTypeID";
                        ddlUserTypes.DataBind();


                        if (Convert.ToString(hdnselectedUserTypeId.Value) != null)
                        {
                            if (Convert.ToString(hdnselectedUserTypeId.Value) != "")
                            {
                                ddlUserTypes.SelectedValue = Convert.ToString(hdnselectedUserTypeId.Value);
                            }
                        }
                    }
                }
                else
                {
                    ddlUserTypes.DataSource = null;
                    ddlUserTypes.DataBind();
                }
                GlobalFunctions.AddDropdownItem(ref ddlUserTypes);
                //ddlUserTypes.Items.Insert(0, new ListItem("Please Select Payment Type", "0"));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails(true);
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void Save()
        {
            try
            {
                DataTable dtUserTypePaymentOption = new DataTable();

                //DataColumn colUserTypeId = new System.Data.DataColumn("UserTypeId ", typeof(Int32));
                //DataColumn colPaymentTypeID = new System.Data.DataColumn("PaymentTypeID", typeof(bool));
                //DataColumn colPaymentTypeRef = new System.Data.DataColumn("PaymentTypeRef", typeof(string));
                //DataColumn colLanguageid = new System.Data.DataColumn("Languageid", typeof(string));
                //DataColumn colIsActive = new System.Data.DataColumn("IsActive ", typeof(Int32));
                //DataColumn colCreatedBy = new System.Data.DataColumn("CreatedBy", typeof(Int32));

                //dtUserTypePaymentOption.Columns.Add(colUserTypeId);
                //dtUserTypePaymentOption.Columns.Add(colPaymentTypeID);
                //dtUserTypePaymentOption.Columns.Add(colPaymentTypeRef);
                //dtUserTypePaymentOption.Columns.Add(colLanguageid);
                //dtUserTypePaymentOption.Columns.Add(colIsActive);
                //dtUserTypePaymentOption.Columns.Add(colCreatedBy);

                dtUserTypePaymentOption.Columns.AddRange(new DataColumn[6] { 
            new DataColumn("UserTypeId", typeof(int)),
            new DataColumn("PaymentTypeID",typeof(int)),
            new DataColumn("PaymentTypeRef",typeof(string)),
            new DataColumn("Languageid",typeof(int)),
            new DataColumn("IsActive",typeof(bool)),
            new DataColumn("CreatedBy",typeof(bool)),
              });

                int i = 0;
                foreach (GridViewRow Row in gvList.Rows)
                {
                    if (dtUserTypePaymentOption != null)
                    {
                        string PaymentTypeID = Convert.ToString(gvList.DataKeys[Row.RowIndex].Values["PaymentTypeID"]);

                        dtUserTypePaymentOption.Rows.Add();
                        dtUserTypePaymentOption.Rows[i]["UserTypeId"] = Convert.ToInt16(ddlUserTypes.SelectedValue);
                        dtUserTypePaymentOption.Rows[i]["PaymentTypeID"] = PaymentTypeID;
                        TextBox txtDisplayName = (TextBox)Row.FindControl("txtDisplayName");
                        dtUserTypePaymentOption.Rows[i]["PaymentTypeRef"] = Convert.ToString(txtDisplayName.Text);
                        dtUserTypePaymentOption.Rows[i]["Languageid"] = Convert.ToInt32(ddlLanguage.SelectedValue);
                        CheckBox chkIsactive = (CheckBox)Row.FindControl("chkIsActive");
                        if (chkIsactive.Checked)
                        {
                            dtUserTypePaymentOption.Rows[i]["IsActive"] = 1;
                        }
                        else
                        {
                            dtUserTypePaymentOption.Rows[i]["IsActive"] = 0;
                        }
                        dtUserTypePaymentOption.Rows[i]["CreatedBy"] = UserId;
                    }
                    i++;

                }


                bool result = UserTypesBL.AEDUserTypePaymentOption(dtUserTypePaymentOption);
                if (result)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Payment Option Saved Successfully.", AlertType.Success);
                    CreateActivityLog("PaymentTypeManagement btnSubmit", "Insert", "");
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ddlUserTypes.SelectedValue != "0")
            {
                Save();
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select User Type", AlertType.Warning);
            }
        }

        protected void gvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnselectedUserTypeId.Value = Convert.ToString(ddlUserTypes.SelectedValue);
            PopulateUserTypeDropDown();
            PopulateGrid(Convert.ToInt16(ddlLanguage.SelectedValue));
        }

        protected void ddlUserTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateGrid(Convert.ToInt16(ddlLanguage.SelectedValue));
        }
    }
}