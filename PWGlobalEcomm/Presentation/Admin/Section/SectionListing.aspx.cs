﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{

    public class Admin_Section_SectionListing : BasePage//System.Web.UI.Page
    {
        int LanguageId;
        public string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.GridView gvSection;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LanguageId = GlobalFunctions.GetLanguageId();
                if (!IsPostBack)
                {
                    PopulateSection();
                    Session["SectionPageType"] = null;
                    Session["SectionPageId"] = null;
                }
                #region Meta Data
                Page.Title = "Section Listing";
                Page.MetaDescription = "Section Listing";
                Page.MetaKeywords = "section";
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateSection()
        {
            try
            {
                List<SectionBE> lstSBE = new List<SectionBE>();   
                SectionBE objBE = new SectionBE();
                objBE.LanguageId = Convert.ToInt16(LanguageId);
                lstSBE = SectionBL.GetAllDetails(Constants.USP_GetSectionDetails, objBE, "A");

                if (lstSBE != null)
                {
                    if (lstSBE.Count > 0)
                    {
                        gvSection.DataSource = lstSBE;
                        gvSection.DataBind();
                    }
                }
                else
                {
                    gvSection.DataSource = null;
                    gvSection.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btn_Click(object sender, EventArgs e)
        {
            try
            {
                Session["SectionPageId"] = 0;
                Response.Redirect(Host + "Admin/Section/AddEditSection.aspx?mode=a");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void gvSection_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSection.PageIndex = e.NewPageIndex;
                PopulateSection();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lbtnEditDelete_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditSection")
                {
                    Session["SectionPageId"] = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect(Host + "Admin/Section/AddEditSection.aspx?mode=e");
                }
                else if (e.CommandName == "DeleteSection")
                {
                    int Result = 0;
                    int SectionId = Convert.ToInt32(e.CommandArgument);
                    SectionBE objBE = new SectionBE();
                    objBE.SectionId = Convert.ToInt16(SectionId);
                    objBE.IsActive = true;
                    objBE.ActionType = Convert.ToString(Convert.ToInt16(DBAction.Delete));
                    Result = SectionBL.InsertUpdateSectionDetails(objBE);
                    if (Result != 0)
                    {
                        DeleteImage(SectionId.ToString(), "Icon");
                        DeleteImage(SectionId.ToString(), "Banner");
                        PopulateSection();
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/SectionDelete"), AlertType.Success);
                        CreateActivityLog("SectionListing DeleteSection", "Delete", Convert.ToString(SectionId));
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); return; }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void DeleteImage(string ImageSectionId, string Type)
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Admin/Images/Section/" + Type + "/"));
                FileInfo[] infos = d.GetFiles();
                foreach (FileInfo f in infos)
                {
                    if (f.Name.ToString().Remove(f.Name.ToString().LastIndexOf(".")).Replace(".", "") == ImageSectionId)
                    {
                        if (File.Exists(f.FullName.ToString()))
                        {
                            File.Delete(f.FullName.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void gvSection_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                string Sectionname, id;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblSecURL = e.Row.FindControl("lblSecURL") as Label;
                    LinkButton lnkbtnDelete = e.Row.FindControl("lnkbtnDelete") as LinkButton;
                    HtmlGenericControl dvAction = e.Row.FindControl("dvAction") as HtmlGenericControl;
                    dvAction.Attributes.Add("class", "view_order other_option");
                    lnkbtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete this section \"" + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SectionName")) + "\"?')");
                    Sectionname = GlobalFunctions.RemoveSpecialCharacters(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SectionName")));
                    id = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SectionId"));
                    lblSecURL.Text = Host + "iconcategory/" +GlobalFunctions.EncodeCategoryURL(Sectionname);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }
}