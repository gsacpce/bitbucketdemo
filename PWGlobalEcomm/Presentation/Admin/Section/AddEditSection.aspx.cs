﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Section_AddEditSection : BasePage//System.Web.UI.Page
    {
        int EditSectionId = 0;
        string SectionType;
        public string Host = GlobalFunctions.GetVirtualPath();
        int CurrencyId;
        int CreatedBy;
        public string TempBannerId = "";
        public string TempSectionId = "";
        public string ImageSize = GlobalFunctions.GetSetting("CategoryImageSize");
        public string BannerSize = GlobalFunctions.GetSetting("CategoryBannerSize");
        public string ImageWidth = "0";
        public string ImageHeight = "0";
        public string BannerHtml = "";
        public string IconHtml { get; set; }
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divUploadImage, divViewDeleteImage, divUploadBanner, divViewDeleteBanner, trPCategory, trPSCategory, trProduct;
        protected global::System.Web.UI.WebControls.TextBox txtPageTitle, txtMetaDesc, txtMetaKey, txtIconAlt, txtName;
        protected global::System.Web.UI.WebControls.Label lblHeading;
        protected global::System.Web.UI.WebControls.RadioButton rdCategory, rdProduct, rdbtnOncePerOrder, rdbtnOncePerItem, rdbtnOncePerUnit;
        protected global::System.Web.UI.WebControls.HiddenField HIDAssignedNameIds, HIDAssignedIds, HIDPSelectedId, HIDUnAssignedIds, HIDUnAssignedNameIds, HIDSectionName;
        protected global::System.Web.UI.WebControls.LinkButton lnkImageView, lnkBannerView;
        protected global::System.Web.UI.WebControls.ListBox lstAssigned, lstUnAssigned;
        protected global::System.Web.UI.WebControls.FileUpload flIcon, flBanner;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;// ddlLvl1, ddlLvl2, ddlLvl3,
        protected global::System.Web.UI.WebControls.Repeater rptPCategory, rptCurrencies;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgLeftToRight, imgRightToLeft;
        protected global::System.Web.UI.WebControls.Panel pnlCategory, pnlProduct;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidSectionBannerSrc, hidSectionSrc, HIDIconId, HIDBannId;
        protected global::System.Web.UI.WebControls.Literal litJS;
        protected global::CKEditor.NET.CKEditorControl txtDescription;
        protected global::System.Web.UI.WebControls.CheckBox chkFeeIsActive;


        string mode = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(Request.QueryString["mode"]) == "a")
                {
                    mode = Request.QueryString["mode"];
                    Session["SectionPageId"] = 0;
                    lblHeading.Text = "Add New Section";
                }
                else
                {
                    mode = Request.QueryString["mode"];
                    EditSectionId = Convert.ToInt16(Session["SectionPageId"]);
                    lblHeading.Text = "Edit Section";

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Page Load Function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CurrencyId = GlobalFunctions.GetCurrencyId();
                trProduct.Visible = true;
                if (!IsPostBack)
                {
                    TempSectionId = Guid.NewGuid().ToString();
                    HIDIconId.Value = TempSectionId;
                    #region Meta Data
                    Page.Title = "Add/Edit Section";
                    Page.MetaDescription = "Add/Edit section";
                    Page.MetaKeywords = "add category, edit section";
                    #endregion
                    PopulateLanguage();
                    #region GetCurrencyDetails
                    GetStoreCurrencyDetails();
                    #endregion

                    //BindDropDownCategory(); Product Search by category Category
                    if (mode == "e")
                    {
                        PopulateSection(EditSectionId);
                        //RemoveMatchingItemsFromLeft(lstUnAssigned, lstAssigned); Product Search by category Categorys
                        BindAssignedProducts();
                    }
                    BindUnAssignedProducts();
                    imgLeftToRight.Attributes.Add("onmousedown", "fn_MoveDataLeftToRight();");
                    imgLeftToRight.Attributes.Add("onkeypress", "fn_MoveDataLeftToRight();");
                    imgRightToLeft.Attributes.Add("onmousedown", "fn_MoveDataRightToLeft();");
                    imgRightToLeft.Attributes.Add("onkeypress", "fn_MoveDataRightToLeft();");
                    if (Session["StoreUser"] != null)
                    {
                        UserBE objUserBE = new UserBE();
                        objUserBE = Session["StoreUser"] as UserBE;
                        CreatedBy = objUserBE.UserId;
                    }

                }
                //litJS.Text = "<script src='" + Host + "Admin/FineUploader/jquery.fineuploaderCategory-3.2.js'></script>" +
                //      "<link href='" + Host + "Admin/FineUploader/fineuploaderCategory-3.2.css' rel='stylesheet'>";
                //txtDescription.config.toolbar = new object[]
                //    {
                //        new object[] { "Source" },
                //        new object[] { "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
                //        new object[] { "NumberedList", "BulletedList"},
                //        new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
                //        "/",
                //        new object[] { "Styles", "Format", "Font", "FontSize" },
                //        new object[] { "TextColor", "BGColor" },
                //        "/",
                //        new object[] { "Link", "Unlink"},
                //        new object[] { "Image", "Table", "HorizontalRule"}
                //    };

                txtDescription.config.toolbar = new object[]
                {
               new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				    new object[] { "NumberedList", "BulletedList"},
				    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				    "/",
				    new object[] { "Styles", "Format", "Font", "FontSize" },
				    new object[] { "TextColor", "BGColor" },
                    "/",
                                    new object[] { "Link", "Unlink"},
                    new object[] { "Table", "HorizontalRule"}
                };
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }



        #region Read
        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Populates Section Details on Edit Page
        /// </summary>
        /// <param name="SectionId"></param>
        /// <returns>Void</returns>
        protected void PopulateLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails(true);
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Populates Section Details on Edit Page
        /// </summary>
        /// <param name="SectionId"></param>
        /// <returns>Void</returns>
        protected void PopulateSection(int SectionId)
        {
            try
            {
                List<SectionBE> lstSBE = new List<SectionBE>();
                SectionBE objBE = new SectionBE();
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                objBE.SectionId = Convert.ToInt16(SectionId);
                //lstSBE = SectionBL.GetAllDetails(Constants.USP_GetSectionDetails, objBE, "S");
                objBE = SectionBL.GetAllSectionRelatedDetails(Constants.USP_GetSectionDetails, objBE, "S");

                if (objBE != null)
                {
                    //HIDSectionId.Value = Convert.ToString(lstSBE[0].SectionId);
                    txtPageTitle.Text = Convert.ToString(objBE.PageTitle);
                    txtMetaDesc.Text = Convert.ToString(objBE.MetaDescription);
                    txtMetaKey.Text = Convert.ToString(objBE.MetaKeyword);
                    txtIconAlt.Text = Convert.ToString(objBE.IconImageAltText);
                    txtName.Text = Convert.ToString(objBE.SectionName);
                    HIDSectionName.Value = Convert.ToString(objBE.SectionName);
                    txtDescription.Text = objBE.SectionDescription != null ? objBE.SectionDescription.ToString() : "";
                    hidSectionSrc.Value = objBE.IconImageExtension;
                    hidSectionBannerSrc.Value = objBE.BannerImageExtension;

                    if (objBE.BannerImageExtension != "")
                    {
                        BannerHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + Host + "Admin/Images/Section/Banner/" + objBE.SectionId +"_"+ddlLanguage.SelectedValue+objBE.IconExt + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\"../images/ui/removeclose.gif\"></a></li>";
                        hidSectionBannerSrc.Value = Host + "Admin/Images/Section/Banner/" + lstSBE[0].SectionId + "_" + ddlLanguage.SelectedValue + objBE.IconExt + "?1=1376400078917";
                        // TempBannerId = Convert.ToString(lstSBE[0].SectionId);
                    }
                    if (objBE.IconImageExtension != "")
                    {
                        IconHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + Host + "Admin/Images/Section/Icon/" + objBE.SectionId + "_" + ddlLanguage.SelectedValue + objBE.IconExt + "?1=13764000\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\"../images/ui/removeclose.gif\"></a></li>";
                        hidSectionSrc.Value = Host + "Admin/Images/Section/Icon/" + objBE.SectionId + "_" + ddlLanguage.SelectedValue + objBE.IconExt + "?1=13764007";
                        //TempSectionId = Convert.ToString(lstSBE[0].SectionId);
                    }

                    #region Get handling section fees
                    if (objBE.HandlingFeesSections != null)
                    {
                        if (objBE.HandlingFeesSections.Count > 0)
                        {
                            chkFeeIsActive.Checked = objBE.HandlingFeesSections.Where(s => s.SectionId == objBE.SectionId).FirstOrDefault().IsActive;

                            foreach (RepeaterItem items in rptCurrencies.Items)
                            {
                                HiddenField hdnCurrencyId = (HiddenField)items.FindControl("hdnCurrencyId");
                                Label lblCurrencyName = (Label)items.FindControl("lblCurrencyName");
                                TextBox txthandlingFees = (TextBox)items.FindControl("txthandlingFees");
                                txthandlingFees.Text = Convert.ToString(objBE.HandlingFeesSections.Where(s => s.CurrencyId == Convert.ToInt32(hdnCurrencyId.Value)).FirstOrDefault().HandlingFees);
                            }
                            int perorderstatus = objBE.HandlingFeesSections.Where(s => s.SectionId == objBE.SectionId).FirstOrDefault().PerOrderStatus;
                            if (perorderstatus == 1)
                            {
                                rdbtnOncePerOrder.Checked = true;
                                rdbtnOncePerItem.Checked = false;
                                rdbtnOncePerUnit.Checked = false;
                            }
                            else if (perorderstatus == 2)
                            {
                                rdbtnOncePerOrder.Checked = false;
                                rdbtnOncePerItem.Checked = true;
                                rdbtnOncePerUnit.Checked = false;
                            }
                            else if (perorderstatus == 3)
                            {
                                rdbtnOncePerOrder.Checked = false;
                                rdbtnOncePerItem.Checked = false;
                                rdbtnOncePerUnit.Checked = true;
                            }
                        }
                    }
                    #endregion
                }

                #region Commented
                //if (lstSBE != null)
                //{
                //    if (lstSBE.Count > 0)
                //    {
                //        //HIDSectionId.Value = Convert.ToString(lstSBE[0].SectionId);
                //        txtPageTitle.Text = Convert.ToString(lstSBE[0].PageTitle);
                //        txtMetaDesc.Text = Convert.ToString(lstSBE[0].MetaDescription);
                //        txtMetaKey.Text = Convert.ToString(lstSBE[0].MetaKeyword);
                //        txtIconAlt.Text = Convert.ToString(lstSBE[0].IconImageAltText);
                //        txtName.Text = Convert.ToString(lstSBE[0].SectionName);
                //        HIDSectionName.Value = Convert.ToString(lstSBE[0].SectionName);
                //        txtDescription.Text = lstSBE[0].SectionDescription != null ? lstSBE[0].SectionDescription.ToString() : "";
                //        hidSectionSrc.Value = lstSBE[0].IconImageExtension;
                //        hidSectionBannerSrc.Value = lstSBE[0].BannerImageExtension;

                //        if (lstSBE[0].BannerImageExtension != "")
                //        {
                //            BannerHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + Host + "Admin/Images/Section/Banner/" + lstSBE[0].SectionId + lstSBE[0].BannerImageExtension + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\"../images/ui/removeclose.gif\"></a></li>";
                //            hidSectionBannerSrc.Value = Host + "Admin/Images/Section/Banner/" + lstSBE[0].SectionId + lstSBE[0].BannerImageExtension + "?1=1376400078917";
                //           // TempBannerId = Convert.ToString(lstSBE[0].SectionId);
                //        }
                //        if (lstSBE[0].IconImageExtension != "")
                //        {
                //            IconHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + Host + "Admin/Images/Section/Icon/" + lstSBE[0].SectionId + lstSBE[0].IconImageExtension + "?1=13764000\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\"../images/ui/removeclose.gif\"></a></li>";
                //            hidSectionSrc.Value = Host + "Admin/Images/Section/Icon/" + lstSBE[0].SectionId + lstSBE[0].IconImageExtension + "?1=13764007";
                //            //TempSectionId = Convert.ToString(lstSBE[0].SectionId);
                //        }
                //        #region
                //        //if (Convert.ToString(lstSBE[0].SectionType) == "P")
                //        //{
                //        //    rdCategory.Checked = false;
                //        //    rdProduct.Checked = true;
                //        //    BindAssignedProducts();
                //        //    if (lstAssigned.Items.Count > 0)
                //        //    {
                //        //        string str = "";
                //        //        for (int i = 0; i < lstAssigned.Items.Count; i++)
                //        //        {
                //        //            if (str == "")
                //        //            { str = lstAssigned.Items[i].Value; }
                //        //            else
                //        //            { str = str + "," + lstAssigned.Items[i].Value; }
                //        //        }
                //        //        HIDAssignedIds.Value = str;
                //        //    }
                //        //}
                //        //else if (Convert.ToString(lstSBE[0].SectionType) == "C")
                //        //{
                //        //    rdCategory.Checked = true;
                //        //    rdProduct.Checked = false;
                //        //}
                //        #endregion
                //    }
                //}
                //lstSBE = null; 
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion
        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Cancel Button Function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Host + "Admin/Section/SectionListing.aspx");
        }

        #region Save and Update
        protected int SaveData()
        {
            SectionBE objBE = new SectionBE();

            SectionBE.HandlingFeesSection objHandlingFeesSectionBE = new SectionBE.HandlingFeesSection();

            int MaxId = 0;
            bool isExist = false;
            int Result = 0;
            string _srcIcon = SetExtension(hidSectionSrc.Value);
            string _srcBanner = SetExtension(hidSectionBannerSrc.Value);
            string oldSectionName = Convert.ToString(HIDSectionName.Value);
            if (Convert.ToString(HIDAssignedIds.Value) != "")
            {
                if (oldSectionName != txtName.Text.ToString())
                {
                    isExist = IsNameExits();
                }
                if (!isExist)
                {
                    objBE.SectionName = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtName.Text.Trim()));
                    objBE.IconImageAltText = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtIconAlt.Text.Trim()).Replace(";", " "));
                    objBE.SectionDescription = GlobalFunctions.RemoveSanitisedPrefixes(Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtDescription.Text.Trim())));
                    objBE.PageTitle = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtPageTitle.Text.Trim()));
                    objBE.MetaDescription = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtMetaDesc.Text.Trim()));
                    objBE.MetaKeyword = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtMetaKey.Text.Trim()));
                    objBE.ProductIdCSV = Convert.ToString(HIDAssignedIds.Value);
                    objBE.IconImageExtension = Convert.ToString(_srcIcon);
                    objBE.BannerImageExtension = Convert.ToString(_srcBanner);
                    objBE.CreatedBy = Convert.ToInt16(CreatedBy);
                    objBE.ModifiedBy = Convert.ToInt16(CreatedBy);
                    objBE.ModifiedBy = Convert.ToInt16(CreatedBy);
                    objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                    objBE.IsActive = true;
                    if (mode == "a")
                    {
                        objBE.SectionId = 0;
                        objBE.ActionType = Convert.ToString(Convert.ToInt16(DBAction.Insert));
                        int b = SectionBL.InsertUpdateSectionDetails(objBE);
                        if (b == 0)
                        { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); }
                        Result = 1;
                        MaxId = int.Parse(b.ToString());

                        #region to add Handling fees
                        if (chkFeeIsActive.Checked)
                        {
                            if (rptCurrencies.Items != null)
                            {
                                if (rptCurrencies.Items.Count > 0)
                                {
                                    foreach (RepeaterItem items in rptCurrencies.Items)
                                    {
                                        HiddenField hdnCurrencyId = (HiddenField)items.FindControl("hdnCurrencyId");
                                        Label lblCurrencyName = (Label)items.FindControl("lblCurrencyName");
                                        TextBox txthandlingFees = (TextBox)items.FindControl("txthandlingFees");

                                        objHandlingFeesSectionBE.SectionId = MaxId;
                                        objHandlingFeesSectionBE.CurrencyId = Convert.ToInt32(hdnCurrencyId.Value);
                                        objHandlingFeesSectionBE.HandlingFees = Convert.ToDecimal(txthandlingFees.Text.ToString());
                                        if (rdbtnOncePerOrder.Checked)
                                        {
                                            objHandlingFeesSectionBE.PerOrderStatus = 1;
                                        }
                                        if (rdbtnOncePerItem.Checked)
                                        {
                                            objHandlingFeesSectionBE.PerOrderStatus = 2;
                                        }
                                        if (rdbtnOncePerUnit.Checked)
                                        {
                                            objHandlingFeesSectionBE.PerOrderStatus = 3;
                                        }
                                        objHandlingFeesSectionBE.IsActive = chkFeeIsActive.Checked;
                                        SectionBL.InsertUpdateHandlingFeesSection(objHandlingFeesSectionBE, CreatedBy, "I");
                                    }
                                }
                            }
                        }

                        #endregion

                    }
                    else
                    {
                        objBE.SectionId = Convert.ToInt16(Session["SectionPageId"]);
                        objBE.ActionType = Convert.ToString(Convert.ToInt16(DBAction.Update));
                        int b = SectionBL.InsertUpdateSectionDetails(objBE);
                        if (b == 0)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning);
                        }
                        else
                        {
                            #region updating add Handling fees

                            if (rptCurrencies.Items != null)
                            {
                                if (rptCurrencies.Items.Count > 0)
                                {
                                    foreach (RepeaterItem items in rptCurrencies.Items)
                                    {
                                        HiddenField hdnCurrencyId = (HiddenField)items.FindControl("hdnCurrencyId");
                                        Label lblCurrencyName = (Label)items.FindControl("lblCurrencyName");
                                        TextBox txthandlingFees = (TextBox)items.FindControl("txthandlingFees");

                                        objHandlingFeesSectionBE.SectionId = b;
                                        objHandlingFeesSectionBE.CurrencyId = Convert.ToInt32(hdnCurrencyId.Value);

                                        if (txthandlingFees.Text != null)
                                        {
                                            if (txthandlingFees.Text.ToString() != "")
                                            {
                                                objHandlingFeesSectionBE.HandlingFees = Convert.ToDecimal(txthandlingFees.Text.ToString());

                                                if (rdbtnOncePerOrder.Checked)
                                                {
                                                    objHandlingFeesSectionBE.PerOrderStatus = 1;
                                                }
                                                if (rdbtnOncePerItem.Checked)
                                                {
                                                    objHandlingFeesSectionBE.PerOrderStatus = 2;
                                                }
                                                if (rdbtnOncePerUnit.Checked)
                                                {
                                                    objHandlingFeesSectionBE.PerOrderStatus = 3;
                                                }
                                                objHandlingFeesSectionBE.IsActive = chkFeeIsActive.Checked;
                                                SectionBL.InsertUpdateHandlingFeesSection(objHandlingFeesSectionBE, CreatedBy, "U");
                                            }
                                        }

                                    }
                                }
                            }
                            #endregion
                        }
                        Result = 2;
                        MaxId = int.Parse(b.ToString());
                    }
                }
                else
                {
                    /* Name already Exists*/
                    Result = 3;
                }
                if (Result > 0 && Result != 3)
                {
                    SaveImage(_srcIcon, "icon", MaxId);
                    #region Functionality Removed Temprary
                    //SaveImage(_srcBanner, "banner", MaxId);
                    #endregion

                    return Result;
                }
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/SectionAddProduct"), AlertType.Warning);
            }
            return Result;

        }

        protected bool IsNameExits()
        {

            bool isExist = SectionBL.IsSectionExists(Sanitizer.GetSafeHtmlFragment(txtName.Text.Trim()));
            return isExist;
        }

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 23-07-16
        /// Scope   : Remane Icon / Banner name
        /// </summary>
        /// <param name="FileExt"></param>
        /// <param name="type"></param>
        /// <param name="SectionId"></param>
        /// <returns>Void</returns>
        private void SaveImage(string FileExt, string type, int SectionId)
        {

            if (FileExt != "")
            {
                try
                {

                    if (type == "icon")
                    {
                        DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Admin/Images/Section/Icon/"));
                        FileInfo[] infos = d.GetFiles();
                        foreach (FileInfo f in infos)
                        {

                            if (f.Name.ToString().Remove(f.Name.ToString().LastIndexOf(".")).Replace(".", "") == Convert.ToString(SectionId))
                            {
                                if (File.Exists(f.FullName.ToString()) && File.Exists(Server.MapPath("~/Admin/Images/Section/Icon/") + HIDIconId.Value + FileExt))
                                {
                                    File.Delete(f.FullName.ToString());//previous file deleted
                                }
                            }
                        }
                        if(mode=="a")
                        {
                            File.Move(Server.MapPath("~/Admin/Images/Section/Icon/") + HIDIconId.Value + FileExt, Server.MapPath("~/Admin/Images/Section/Icon/") + SectionId + "_" + ddlLanguage.SelectedValue + FileExt);
                            for (int i = 1; i <= 15; i++)
                            {
                                if (!File.Exists(Server.MapPath("~/Admin/Images/Section/Icon/") + SectionId + "_" + i + FileExt))
                                {
                                    File.Copy(Server.MapPath("~/Admin/Images/Section/Icon/") + SectionId + "_" + ddlLanguage.SelectedValue + FileExt, Server.MapPath("~/Admin/Images/Section/Icon/") + SectionId + "_" + i + FileExt);
                                }
                            }
                        }
                        if(mode=="e")
                        {
                            string tempFExt = ".jpg,.png,.jpeg";
                            string[] values = null;
                            values = tempFExt.Split(',');
                            for (int i = 0; i <= values.Length-1; i++)
                            {
                                if (File.Exists(Server.MapPath("~/Admin/Images/Section/Icon/") + SectionId + "_" + ddlLanguage.SelectedValue + values[i].ToString()))
                                {
                                    File.Delete(Server.MapPath("~/Admin/Images/Section/Icon/") + SectionId + "_" + ddlLanguage.SelectedValue + values[i].ToString());
                                }
                            }
                            File.Move(Server.MapPath("~/Admin/Images/Section/Icon/") + HIDIconId.Value + FileExt, Server.MapPath("~/Admin/Images/Section/Icon/") + SectionId + "_" + ddlLanguage.SelectedValue + FileExt);
                        }
                    }
                    #region Functionality Removed Temprary
                    //else if (type == "banner")
                    //{ File.Move(Server.MapPath("~/Admin/Images/Section/Banner/") + HIDBannId.Value + FileExt, Server.MapPath("~/Admin/Images/Section/Banner/") + SectionId + FileExt); }
                    #endregion
                }
                catch (Exception ex)
                { Exceptions.WriteExceptionLog(ex); }
            }
        }

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Used to remove matching items from two listbox control
        /// </summary>
        /// <param name="objListBox1"></param>
        /// <param name="objListBox2"></param>
        /// <returns>Void</returns>
        private void RemoveMatchingItemsFromLeft(ListBox objListBox1, ListBox objListBox2)
        {
            try
            {
                foreach (ListItem item in objListBox2.Items)
                {
                    if (objListBox1.Items.Contains(item))
                    {
                        objListBox1.Items.Remove(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Submit Button Function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate("VaildCategory");
                if (!Page.IsValid)
                { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/SectionNameInvalid"), AlertType.Warning); return; }

                #region validation of handling fees
                string strMessage = "";
                bool IsValid = HandlingFeesValidation(out strMessage);
                #endregion

                if (IsValid)
                {
                    int Result = SaveData();
                    if (Result != 0)
                    {
                        Session["SectionPageId"] = null;
                        Button btn = sender as Button;
                        if (btn.ID == "btnSaveExit")
                        {
                            if (Result == 1)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/SectionInsert"), Host + "Admin/Section/SectionListing.aspx", AlertType.Success);
                                CreateActivityLog("AddEditSection btnSaveExit", "Insert", txtName.Text);
                            }
                            else if (Result == 2)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/SectionUpdate"), Host + "Admin/Section/SectionListing.aspx", AlertType.Success);
                                CreateActivityLog("AddEditSection btnSaveExit", "Update", txtName.Text);
                            }
                            else if (Result == 3)
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/SectionNameExists"), AlertType.Warning);
                        }
                        else if (btn.ID == "btnSaveAddAnother")
                        {
                            if (Result == 1)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/SectionInsert"), Host + "Admin/Section/SectionListing.aspx", AlertType.Success);
                                CreateActivityLog("AddEditSection btnSaveAddAnother", "Insert", txtName.Text);
                            }
                            else if (Result == 2)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/SectionUpdate"), Host + "Admin/Section/SectionListing.aspx", AlertType.Success);
                                CreateActivityLog("AddEditSection btnSaveAddAnother", "Update", txtName.Text);
                            }
                            else if (Result == 3)
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/SectionNameExists"), AlertType.Warning);
                        }
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); return; }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strMessage, AlertType.Warning);
                    return;
                }



            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 23-07-16
        /// Scope   : Gets image extension
        /// </summary>
        /// <param name="Name"></param>
        /// <returns>String</returns>
        private string SetExtension(string name)
        {
            string FileExt = string.Empty;
            try
            {
                string BackLogoExt = string.Empty;
                if (name != "")
                {
                    if (name.IndexOf("?") > 0)
                    { FileExt = name.Substring(name.LastIndexOf("."), (name.IndexOf("?") - name.LastIndexOf("."))); }
                    else
                    { FileExt = name.Substring(name.LastIndexOf(".")); }
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return FileExt;
        }

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Icon Delete Button Function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void lnkImageDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Admin/Images/Section/Icon/"));
                FileInfo[] infos = d.GetFiles();
                foreach (FileInfo f in infos)
                {
                    if (f.Name.ToString().Remove(f.Name.ToString().LastIndexOf(".")).Replace(".", "") == Request.QueryString["id"].ToString())
                    {
                        if (File.Exists(f.FullName.ToString()))
                        {
                            File.Delete(f.FullName.ToString());
                        }
                    }
                }
                divUploadImage.Visible = true;
                divViewDeleteImage.Visible = false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #endregion


        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Re-Binds unassigned products to listbox control on edit page
        /// </summary>
        /// <returns>Void</returns>
        protected void BindUnAssignedProducts()
        {
            try
            {

                SectionBE objBE = new SectionBE();
                #region commented by vikram to remove  Search by category
                //if (trPSCategory.Visible)
                //{ objBE.subCategoryname = ddlLvl2.SelectedItem.Text; }
                //objBE.subSubcategoryname = "";
                //// objBE.CategoryId = Convert.ToString(HIDPSelectedId.Value);
                //// objBE.CategoryName = ddlLvl1.SelectedItem.Text;
                #endregion
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);

                objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                if (mode == "a")
                    objBE.SectionId = 0;
                else
                /* For GETTING THE UnAssigned Product WE DONT PASS THE SECTION ID THERFORE IT IS 0.*/
                {
                    objBE.SectionId = 0;//Convert.ToInt16(Session["SectionPageId"]);
                    objBE.ProductIdCSV = HIDAssignedIds.Value;
                }

                objBE.ActionType = Convert.ToString(Convert.ToInt16(DBAction.Select));
                List<SectionBE> lst = SectionBL.GetAllSectionProductsDetails(Constants.USP_GetSectionProducts, objBE);
                // List<SectionBE> lst = SectionBL.GetAllSectionProductsDetails("USP_GetSectionProductByCategory", objBE);

                if (lst != null)
                {
                    lstUnAssigned.Items.Clear();
                    lstUnAssigned.DataSource = lst;
                    lstUnAssigned.DataTextField = "Product";
                    lstUnAssigned.DataValueField = "ProductId";
                    lstUnAssigned.DataBind();
                }
                else
                { lstUnAssigned.Items.Clear(); }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Re-Binds assigned products to listbox control on edit page
        /// </summary>
        /// <returns>Void</returns>
        protected void BindAssignedProducts()
        {
            try
            {
                List<SectionBE> lst = new List<SectionBE>();
                SectionBE objBE = new SectionBE();
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                objBE.ActionType = Convert.ToString(Convert.ToInt16(DBAction.Select));

                if (mode == "a")
                    objBE.SectionId = 0;
                else
                {
                    #region commented by vikram to remove Category By Search to avoid Postaback
                    //if (!IsPostBack)
                    //{
                    #endregion
                    objBE.SectionId = Convert.ToInt16(Session["SectionPageId"]);
                    lst = SectionBL.GetAllSectionProductsDetails(Constants.USP_GetSectionProducts, objBE);
                    // lst = SectionBL.GetAllSectionProductsDetails("USP_GetSectionProductByCategory", objBE);
                    if (lst != null)
                    {
                        HIDAssignedIds.Value = lst[0].ProductIdCSV;
                        #region commented by vikram to remove
                        // trProduct.Visible = true;
                        #endregion
                        lstAssigned.DataSource = lst;
                        lstAssigned.DataTextField = "Product";
                        lstAssigned.DataValueField = "ProductId";
                        lstAssigned.DataBind();
                    }
                    //}
                }
                #region commented by vikram to remove search by category

                //lstAssigned.Items.Clear();
                //string AssignedIds = Convert.ToString(HIDAssignedIds.Value);
                //if (AssignedIds.Length > 0)
                //{
                //    objBE.ProductIdCSV = Convert.ToString(AssignedIds);
                //    List<SectionBE> lstProduct = SectionBL.GetAllSectionProductsDetails(Constants.USP_GetSectionProducts, objBE);
                //    for (int i = 0; i < lstProduct.Count; i++)
                //    {
                //        //HidAlreadyExitsProducts.Value = Convert.ToString(HidAlreadyExitsProducts.Value) + "," + lstProduct[i].ProductId;
                //        lstAssigned.Items.Insert(i, new ListItem(lstProduct[i].Product.ToString(), lstProduct[i].ProductId.ToString()));
                //    }
                //}

                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region Search by Category Disable
        ///// <summary>
        ///// Author  : Prajwal Hegde
        ///// Date    : 21-07-16
        ///// Scope   : Dropdown Selected Index Changed Event
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        ///// <returns>Void</returns>
        //protected void ddlLvl1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    lstUnAssigned.Items.Clear();
        //    try
        //    {
        //        if (ddlLvl1.SelectedIndex > 0)
        //        {
        //            HIDPSelectedId.Value = ddlLvl1.SelectedValue;
        //            SectionBE objBE = new SectionBE();
        //            objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
        //            objBE.CatalogueId = Convert.ToInt16(ddlLvl1.SelectedItem.Value);
        //            List<SectionBE> lst = SectionBL.GetAllSectionCategoriesDetails(Constants.USP_GetSectionCategories, objBE, "C-S", Convert.ToInt16(CurrencyId));
        //            if (lst != null)
        //            {
        //                if (lst.Count > 0)
        //                {
        //                    ddlLvl2.Items.Clear();
        //                    ddlLvl2.DataSource = lst;
        //                    ddlLvl2.DataTextField = "CategoryName";
        //                    ddlLvl2.DataValueField = "CategoryId";
        //                    ddlLvl2.DataBind();
        //                    ddlLvl2.Items.Insert(0, new ListItem("--Select SubCategory--", "0"));
        //                }
        //                trPSCategory.Visible = true;
        //            }
        //            else
        //            {
        //                ddlLvl2.SelectedIndex = -1;
        //                trProduct.Visible = true;
        //                trPSCategory.Visible = false;
        //                BindUnAssignedProducts();
        //            }
        //            BindAssignedProducts();
        //        }
        //        else
        //        {
        //            trPSCategory.Visible = false;
        //            trProduct.Visible = true;
        //            HIDPSelectedId.Value = "";
        //        }
        //        RemoveMatchingItemsFromLeft(lstUnAssigned, lstAssigned);
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //}

        ///// <summary>
        ///// Author  : Prajwal Hegde
        ///// Date    : 21-07-16
        ///// Scope   : Dropdown Selected Index Changed Event
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        ///// <returns>Void</returns>
        //protected void ddlLvl2_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlLvl2.SelectedIndex > 0)
        //        {
        //            HIDPSelectedId.Value = ddlLvl2.SelectedValue;
        //            SectionBE objBE = new SectionBE();
        //            objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
        //            objBE.CatalogueId = Convert.ToInt16(ddlLvl2.SelectedItem.Value);
        //            List<SectionBE> lst = SectionBL.GetAllSectionCategoriesDetails(Constants.USP_GetSectionCategories, objBE, "C-S-S", Convert.ToInt16(CurrencyId));
        //            trProduct.Visible = true;
        //            BindUnAssignedProducts();
        //            BindAssignedProducts();
        //        }
        //        else
        //        {
        //            trProduct.Visible = true;
        //            HIDPSelectedId.Value = ddlLvl1.SelectedValue;
        //        }
        //        RemoveMatchingItemsFromLeft(lstUnAssigned, lstAssigned);
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //}

        //protected void BindDropDownCategory()
        //{
        //    try
        //    {

        //        ddlLvl1.ClearSelection();
        //        SectionBE objBE = new SectionBE();
        //        objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
        //        List<SectionBE> lst = SectionBL.GetAllSectionCategoriesDetails(Constants.USP_GetSectionCategories, objBE, "C-M", Convert.ToInt16(CurrencyId));
        //        if (lst != null)
        //        {
        //            if (lst.Count > 0)
        //            {
        //                ddlLvl1.Items.Clear();
        //                ddlLvl1.DataSource = lst;
        //                ddlLvl1.DataTextField = "CategoryName";
        //                ddlLvl1.DataValueField = "CategoryId";
        //                ddlLvl1.DataBind();
        //                ddlLvl1.Items.Insert(0, new ListItem("--Select Category--", "0"));
        //            }
        //        }

        //        pnlProduct.Visible = true;
        //        trPCategory.Visible = true;
        //        trPSCategory.Visible = false;

        //        trProduct.Visible = false;
        //        HIDPSelectedId.Value = "";

        //    }
        //    catch (Exception ex)
        //    { Exceptions.WriteExceptionLog(ex); }
        //}
        #endregion

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 30-07-16
        /// Scope   : Dropdown Selected Index Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            TempSectionId = Guid.NewGuid().ToString();
            HIDIconId.Value = TempSectionId;
            TempSectionId = Guid.NewGuid().ToString();
            HIDIconId.Value = TempSectionId;
            PopulateSection(EditSectionId);
        }

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 21-07-16
        /// Scope   : Returns the current working directory
        /// </summary>
        /// <param name="attributes"></param>
        /// <param name="attributesToRemove"></param>
        /// <returns>String</returns>
        private static FileAttributes RemoveAttribute(FileAttributes attributes, FileAttributes attributesToRemove)
        {
            return attributes & ~attributesToRemove;
        }


        private void GetStoreCurrencyDetails()
        {
            try
            {
                List<StoreBE.StoreCurrencyBE> GetStoreCurrencyDetails = StoreCurrencyBL.GetStoreCurrencyDetails();
                rptCurrencies.DataSource = GetStoreCurrencyDetails;
                rptCurrencies.DataBind();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        private bool HandlingFeesValidation(out  string strMessage)
        {
            bool IsValid = false;
            strMessage = "";
            try
            {
                if (rptCurrencies.Items != null)
                {
                    if (rptCurrencies.Items.Count > 0)
                    {
                        if (chkFeeIsActive.Checked)
                        {
                            foreach (RepeaterItem items in rptCurrencies.Items)
                            {
                                Label lblCurrencyName = (Label)items.FindControl("lblCurrencyName");
                                TextBox txthandlingFees = (TextBox)items.FindControl("txthandlingFees");

                                if (txthandlingFees.Text.Trim() == "")
                                {
                                    strMessage = "Enter handling fees for " + lblCurrencyName.Text.Trim() + "";
                                    txthandlingFees.Focus();
                                    return IsValid = false;
                                }
                                else
                                {
                                    Double number;
                                    bool result = Double.TryParse(txthandlingFees.Text.Trim(), out number);
                                    if (result == false)
                                    {
                                        strMessage = "Incorrect handling fee for " + lblCurrencyName.Text.Trim() + "";
                                        txthandlingFees.Focus();
                                        return IsValid = false;
                                    }
                                    else
                                    {
                                        Boolean IsValidValFrom = Regex.IsMatch(txthandlingFees.Text.Trim(), ValidRegEx.RegEx_ValidFreightDecimalWithFourDigit);

                                        if (IsValidValFrom)
                                        {
                                            IsValid = true;
                                        }
                                        else
                                        {
                                            strMessage = "Exceeds maximum handling fee limit for " + lblCurrencyName.Text.Trim() + "";
                                            return IsValid = false;
                                        }
                                    }
                                }
                            }
                            if (rdbtnOncePerUnit.Checked == false && rdbtnOncePerOrder.Checked == false && rdbtnOncePerItem.Checked == false)
                            {
                                strMessage = "Select atleast one per order status";
                                return IsValid = false;
                            }
                        }
                        else
                        {
                            IsValid = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }
            return IsValid;
        }
    }
}