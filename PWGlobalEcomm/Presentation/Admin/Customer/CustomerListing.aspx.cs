﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Customer_CustomerListing : BasePage //System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptUsers;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMessage;
        protected global::System.Web.UI.WebControls.Literal ltrMessage;
        protected global::System.Web.UI.WebControls.TextBox txtSearch;
        protected global::System.Web.UI.WebControls.HiddenField hdnUID;
        protected global::System.Web.UI.WebControls.LinkButton lnkActivate;

        protected void Page_Load(object sender, EventArgs e)
        {
            divMessage.Visible = false;
            if (!IsPostBack)
            {
                BindPage();
            }
        }

        private void BindPage()
        {
            try
            {
                List<UserBE> Users = new List<UserBE>();
                //Users = UserBL.GetUserDetails();
                Users = UserBL.GetUserDetails();
                if (Users != null)
                {
                    if (Users.Count > 0)
                    {
                        Users = Users.Where(u => u.RoleId == 2).ToList();
                        if (Users != null)
                        {
                            if (Users.Count > 0)
                            {
                                //Users = UserBL.GetUserDetails().Where(u => u.RoleId == 2).ToList();
                                divMessage.Visible = false;
                                rptUsers.DataSource = Users;
                                rptUsers.DataBind();
                            }
                            else
                            {
                                divMessage.Visible = true;
                                ltrMessage.Text = "No Records found";
                            }
                        }
                        else
                        {
                            divMessage.Visible = true;
                            ltrMessage.Text = "No Records found";
                        }
                    }
                    else
                    {
                        divMessage.Visible = true;
                        ltrMessage.Text = "No Records found";
                    }
                }
                else
                {
                    divMessage.Visible = true;
                    ltrMessage.Text = "No Records found";
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            //*************************************************************************
            // Purpose : To export the category relations.
            // Inputs  : object sender, EventArgs e
            // Return  : Nothing
            //*************************************************************************
            try
            {
                DataTable dt = new DataTable();
                dt = UserBL.GetUserDataForExportView();
                if (dt.Rows.Count > 0)
                {
                    GlobalFunctions.ExportToExcel(dt, "UserData");
                    dt = null;
                }
                else
                {
                    divMessage.Visible = true;
                    ltrMessage.Text = "No Records found";
                }
                //    List<UserBE> Users = new List<UserBE>();
                //    Users = UserBL.GetUserDetailsForExport();
                //    if (Users != null)
                //    {
                //        if (Users.Count > 0)
                //        {
                //            Users = UserBL.GetUserDetailsForExport().Where(u => u.RoleId == 2).ToList();
                //            if (Users.Count > 0)
                //            {
                //                //DataTable dt = new DataTable();
                //                //dt = UserBL.GetUserDataForExportView();

                //             //   dt.Columns.Add("Sr No");
                //             //   dt.Columns.Add("userid");
                //             //   dt.Columns.Add("Active Status");
                //             //   dt.Columns.Add("User Type");
                //             //   dt.Columns.Add("EmailId");
                //             //   dt.Columns.Add("FirstName");
                //             //   dt.Columns.Add("LastName");
                //             //   dt.Columns.Add("Billing Contact");
                //             //   dt.Columns.Add("Billing Company");
                //             //   dt.Columns.Add("Billing Address 1");
                //             //   dt.Columns.Add("Billing Address 2");
                //             //   dt.Columns.Add("Billing Town");
                //             //   dt.Columns.Add("Billing County");
                //             //   dt.Columns.Add("Billing Post Code");
                //             //   dt.Columns.Add("Telephone");   
                //             ////   dt.Columns.Add(Users.IndexOf())
                //             //   int i = 1;
                //             //   foreach (UserBE objUserBE in Users)
                //             //   {
                //             //       DataRow drow = dt.NewRow();
                //             //       drow["Sr No"] = i;
                //             //       drow["userid"] = objUserBE.UserId;
                //             //       drow["Active Status"] = objUserBE.IsActive;
                //             //       drow["User Type"] = objUserBE.UserTypeName;
                //             //       drow["EmailId"] = objUserBE.EmailId;
                //             //       drow["FirstName"] = objUserBE.FirstName;
                //             //       drow["LastName"] = objUserBE.LastName;
                //             //       drow["Billing Contact"] = objUserBE.PredefinedColumn1;
                //             //       drow["Billing Company"] = objUserBE.PredefinedColumn2;
                //             //       drow["Billing Address 1"] = objUserBE.PredefinedColumn3;
                //             //       drow["Billing Address 2"] = objUserBE.PredefinedColumn4;
                //             //       drow["Billing Town"] = objUserBE.PredefinedColumn5;
                //             //       drow["Billing County"] = objUserBE.PredefinedColumn6;
                //             //       drow["Billing Post Code"] = objUserBE.PredefinedColumn7;
                //             //       drow["Telephone"] = objUserBE.PredefinedColumn9;                               
                //             //       dt.Rows.Add(drow);
                //             //       i++;
                //             //   }
                //                GlobalFunctions.ExportToExcel(dt, "UserData");
                //                dt = null;
                //            }
                //            else
                //            {
                //                divMessage.Visible = true;
                //                ltrMessage.Text = "No Records found";
                //            }
                //        }
                //        else
                //        {
                //            divMessage.Visible = true;
                //            ltrMessage.Text = "No Records found";
                //        }
                //    }
                //    else
                //    {
                //        divMessage.Visible = true;
                //        ltrMessage.Text = "No Records found";
                //    }
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string strText = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSearch.Text.Trim());
                List<UserBE> Users = new List<UserBE>();
                Users = UserBL.GetUserDetailsSearch(strText);
                if (Users != null)
                {
                    if (Users.Count > 0)
                    {
                        Users = Users.Where(u => u.RoleId == 2).ToList();
                        if (Users != null)
                        {
                            if (Users.Count > 0)
                            {
                                divMessage.Visible = false;
                                rptUsers.DataSource = Users;
                                rptUsers.DataBind();
                            }
                            else
                            {
                                divMessage.Visible = true;
                                ltrMessage.Text = "No Records found";
                            }
                        }
                        else
                        {
                            divMessage.Visible = true;
                            ltrMessage.Text = "No Records found";
                        }
                    }
                    else
                    {
                        divMessage.Visible = true;
                        ltrMessage.Text = "No Records found";
                    }
                }
                else
                {
                    divMessage.Visible = true;
                    ltrMessage.Text = "No Records found";
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
      
        protected void lnkActivate_Click(object sender, EventArgs e)
        {
            UserBE objUserBE = new UserBE();
            try
            {
                LinkButton btn = (LinkButton)(sender);
                Int16 UID = Convert.ToInt16(btn.CommandArgument);
                bool result = false;
                objUserBE.UserId = UID;
                if(btn.Text=="Active")
                {
                    objUserBE.IsActive = false;
                }
                else
                {
                    objUserBE.IsActive = true;
                }
                result = UserBL.UpdateCustomerStatus(objUserBE);
                if (result)
                {
                    CreateActivityLog("Customer List", "Updated", Convert.ToString(UID));
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/UpdatedMessage"), AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/UpdatedMessage"), AlertType.Failure);
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            BindPage();
        }
      
    }
}