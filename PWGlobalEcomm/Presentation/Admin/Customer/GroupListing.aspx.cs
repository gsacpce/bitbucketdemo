﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Customer_GroupListing : BasePage //System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.GridView gvGroups;
        protected global::System.Web.UI.WebControls.TextBox txtSearch;
        protected global::System.Web.UI.WebControls.Literal ltrNoOfRecords;

        UserBE.CustomerGroupBE objCustomerGroupBE = new UserBE.CustomerGroupBE();
        UserBL objUserBL = new UserBL();
        List<UserBE.CustomerGroupBE> lstCustomerGroup = new List<UserBE.CustomerGroupBE>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        protected void BindData()
        {
            try
            {
                objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Select);
                objCustomerGroupBE.GroupId = 0;

                //List<object> objeCustomerGroup = UserBL.GetCustomerGroup(objCustomerGroupBE);

                lstCustomerGroup = UserBL.GetCustomerGroup(objCustomerGroupBE);

                if (lstCustomerGroup != null)
                {
                    if (lstCustomerGroup.Count > 0)
                    {
                        gvGroups.DataSource = lstCustomerGroup;
                        gvGroups.DataBind();
                        ltrNoOfRecords.Text = lstCustomerGroup.Count.ToString();
                    }
                    else
                    {
                        gvGroups.DataSource = null;
                        gvGroups.DataBind();
                        ltrNoOfRecords.Text = "0";
                    }
                }


            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void SearchGroup()
        {
            try
            {
                objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Select);
                objCustomerGroupBE.GroupId = 0;
                objCustomerGroupBE.GroupName = txtSearch.Text.ToString();
                lstCustomerGroup = UserBL.GetCustomerGroup(objCustomerGroupBE);
                if (lstCustomerGroup != null)
                {
                    if (lstCustomerGroup.Count > 0)
                    {
                        gvGroups.DataSource = lstCustomerGroup;
                        gvGroups.DataBind();
                        ltrNoOfRecords.Text = lstCustomerGroup.Count.ToString();
                    }
                    else
                    {
                        gvGroups.DataSource = null;
                        gvGroups.DataBind();
                        ltrNoOfRecords.Text = "0";
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            Session["AEUserId"] = 0;
            Response.Redirect("AddEditGroups.aspx?mode=a");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SearchGroup();
        }
        protected void lnkEditGroups_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                int GroupId = Convert.ToInt32(lnk.CommandArgument);
                Session["AECustomerGroupId"] = GroupId;
                Response.Redirect("AddEditGroups.aspx?mode=e");
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void chkIsActive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                UserBE lst = Session["StoreUser"] as UserBE;
                int res;

                // CheckBox chkIsActive = (sender as CheckBox);

                GridViewRow row = (sender as CheckBox).NamingContainer as GridViewRow;
                int id1 = Convert.ToInt16(gvGroups.DataKeys[row.RowIndex].Values["GroupId"]);

                string GroupName = Convert.ToString(gvGroups.DataKeys[row.RowIndex].Values["GroupName"]);

                objCustomerGroupBE.GroupId = Convert.ToInt16(id1);
                objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Update);
                objCustomerGroupBE.ModifiedBy = Convert.ToInt16(lst.UserId);
                if ((row.FindControl("chkIsActive") as CheckBox).Checked)
                {
                    objCustomerGroupBE.IsActive = true;
                    res = UserBL.InsertUpdateCustomerGroup(objCustomerGroupBE);
                }
                else
                {
                    objCustomerGroupBE.IsActive = false;
                    res = UserBL.InsertUpdateCustomerGroup(objCustomerGroupBE);
                }
                if (res > 0)
                {
                    CreateActivityLog("Group Management List", "Updated",Convert.ToString(id1+"_"+GroupName));
                    //true
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Group has been Updated successfully.", AlertType.Success);
                    BindData();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }



        protected void gvGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvGroups.PageIndex = e.NewPageIndex;
            BindData();
        }




    }
}