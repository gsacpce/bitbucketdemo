﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_Customer_AddEditGroups : BasePage //System.Web.UI.Page
    {
        string mode;
        UserBE.CustomerGroupBE objCustomerGroupBE = new UserBE.CustomerGroupBE();
        UserBL objUserBL = new UserBL();
        List<UserBE.CustomerGroupBE> lstCustomerGroup = new List<UserBE.CustomerGroupBE>();

        #region Control
        protected global::System.Web.UI.WebControls.TextBox txtGroupName;
        protected global::System.Web.UI.WebControls.TextBox txtBudget;
        protected global::System.Web.UI.WebControls.CheckBox chkIsactive;
        protected global::System.Web.UI.WebControls.TextBox txtValidityStartDate;
        protected global::System.Web.UI.WebControls.TextBox txtValidityEndDate;
        protected global::System.Web.UI.WebControls.Button btnAdd;
        protected global::System.Web.UI.WebControls.Button btnUpdate;
        protected global::System.Web.UI.WebControls.ListBox lstAllUser;
        protected global::System.Web.UI.WebControls.ListBox lstAssignedUser;
        protected global::System.Web.UI.WebControls.Label lblErrorMsg;
        ArrayList arraylist1 = new ArrayList();
        ArrayList arraylist2 = new ArrayList();
        #endregion


        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                mode = Request.QueryString["mode"];
                if (mode == "e")
                {
                    btnAdd.Visible = false;
                    btnUpdate.Visible = true;
                }
                else
                {
                    btnUpdate.Visible = false;
                    btnAdd.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (mode == "a")
                {
                    ClearAllFields();
                    BindLeftList();
                }
                else
                {
                    ReadData();
                }
            }
        }
        protected void BindLeftList()
        {
            try
            {
                if (mode == "e")
                    objCustomerGroupBE.GroupId = Convert.ToInt16(Session["AECustomerGroupId"]);
                else
                    objCustomerGroupBE.GroupId = 0;

                objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Select);
                lstCustomerGroup = UserBL.GetAllUnAssignedCustomerGroup(objCustomerGroupBE);

                if (lstCustomerGroup != null)
                {
                    if (lstCustomerGroup.Count > 0)
                    {
                        lstAllUser.DataSource = lstCustomerGroup;

                        lstAllUser.DataTextField = "EmailId";
                        lstAllUser.DataValueField = "UserId";
                        lstAllUser.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void BindRightList()
        {
            try
            {
                objCustomerGroupBE.GroupId = Convert.ToInt16(Session["AECustomerGroupId"]);
                objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Select);
                lstCustomerGroup = UserBL.GetAssignedCustomerGroupEmail(objCustomerGroupBE);

                if (lstCustomerGroup != null)
                {
                    if (lstCustomerGroup.Count > 0)
                    {
                        lstAssignedUser.DataSource = lstCustomerGroup;
                        lstAssignedUser.DataTextField = "EmailId";
                        lstAssignedUser.DataValueField = "UserId";
                        lstAssignedUser.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ClearAllFields()
        {
            txtBudget.Text = "";
            txtGroupName.Text = "";
            txtValidityStartDate.Text = "";
            txtValidityEndDate.Text = "";
        }

        protected void ReadData()
        {
            try
            {
                if (mode == "e")
                    objCustomerGroupBE.GroupId = Convert.ToInt16(Session["AECustomerGroupId"]);
                else
                    objCustomerGroupBE.GroupId = 0;

                objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Select);
                lstCustomerGroup = UserBL.GetCustomerGroup(objCustomerGroupBE);

                if (lstCustomerGroup != null)
                {
                    if (lstCustomerGroup.Count > 0)
                    {
                        txtGroupName.Text = lstCustomerGroup[0].GroupName;
                        txtBudget.Text = Convert.ToString(lstCustomerGroup[0].Budget);
                        txtValidityStartDate.Text = Convert.ToString(lstCustomerGroup[0].ValidityStartDate.ToShortDateString());
                        txtValidityEndDate.Text = Convert.ToString(lstCustomerGroup[0].ValidityEndDate.ToShortDateString());
                        if (lstCustomerGroup[0].IsActive)
                            chkIsactive.Checked = true;
                        else
                            chkIsactive.Checked = false;

                        BindLeftList();
                        BindRightList();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected int SaveGroup()
        {
            int res = 0;
            try
            {
                UserBE lst = Session["StoreUser"] as UserBE;
                if (mode == "a")
                {
                    objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Insert);
                }
                else
                {
                    objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Update);
                }
                objCustomerGroupBE.GroupName = txtGroupName.Text.ToString();
                objCustomerGroupBE.ValidityStartDate = Convert.ToDateTime(txtValidityStartDate.Text);
                objCustomerGroupBE.ValidityEndDate = Convert.ToDateTime(txtValidityEndDate.Text); ;
                objCustomerGroupBE.Budget = Convert.ToDecimal(txtBudget.Text);
                objCustomerGroupBE.ModifiedBy = lst.UserId;
                if (chkIsactive.Checked)
                    objCustomerGroupBE.IsActive = true;
                else
                    objCustomerGroupBE.IsActive = false;
                if (mode == "a")
                {
                    objCustomerGroupBE.GroupId = 0;
                }
                else
                {
                    objCustomerGroupBE.GroupId = Convert.ToInt16(Session["AECustomerGroupId"]);
                }
                res = UserBL.InsertUpdateCustomerGroup(objCustomerGroupBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }
            return res;
        }
        protected string GetSelectedUserId()
        {
            string strRes = "";
            try
            {
                foreach (ListItem item in lstAssignedUser.Items)
                {
                    int UserId = Convert.ToInt32(item.Value);
                    strRes = strRes + Convert.ToString(UserId);
                    strRes = strRes + ",";
                }
                if (strRes.Length > 0)
                {
                    strRes = strRes.Substring(0, strRes.Length - 1);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return strRes;
        }

        protected bool SaveAssignedUser(int GroupId)
        {
            bool res = false;
            try
            {
                UserBE lst = Session["StoreUser"] as UserBE;
                if (mode == "a")
                {
                    objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Insert);
                    objCustomerGroupBE.GroupId = Convert.ToInt16(GroupId);
                }
                else
                {
                    objCustomerGroupBE.Action = Convert.ToInt16(DBAction.Update);
                    objCustomerGroupBE.GroupId = Convert.ToInt16(Session["AECustomerGroupId"]);
                }
                objCustomerGroupBE.UserIdCSV = GetSelectedUserId();
                objCustomerGroupBE.ModifiedBy = lst.UserId;
                res = UserBL.InsertUpdateCustomerGroupedUser(objCustomerGroupBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
            return res;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        protected void SaveData()
        {
            if (Page.IsValid)
            {
                int GroupId = SaveGroup();
                bool res = SaveAssignedUser(GroupId);
                if (res)
                {
                    if (mode == "a")
                    {
                        if (GroupId == 0)
                        { GlobalFunctions.ShowModalAlertMessages(this.Page, "Group already Exists.", AlertType.Failure); }
                        if (GroupId > 0)
                        {
                            CreateActivityLog("Group Management", "Added", Convert.ToString(GroupId+"_"+txtGroupName.Text));
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Group has been added successfully.", "GroupListing.aspx", AlertType.Success);
                        }
                    }
                    else
                    {
                        CreateActivityLog("Group Management", "Updated", Convert.ToString(GroupId + "_" + txtGroupName.Text));
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Group has been Updated successfully.", "GroupListing.aspx", AlertType.Success);
                    }
                }
                else
                {
                    
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Group Saving Failed.", AlertType.Failure);
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        protected void btnMoveRightAll_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            while (lstAllUser.Items.Count != 0)
            {
                for (int i = 0; i < lstAllUser.Items.Count; i++)
                {
                    lstAssignedUser.Items.Add(lstAllUser.Items[i]);
                    lstAllUser.Items.Remove(lstAllUser.Items[i]);
                }
            }
        }

        protected void btnMoveRight_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            if (lstAllUser.SelectedIndex >= 0)
            {
                for (int i = 0; i < lstAllUser.Items.Count; i++)
                {
                    if (lstAllUser.Items[i].Selected)
                    {
                        if (!arraylist1.Contains(lstAllUser.Items[i]))
                        {
                            arraylist1.Add(lstAllUser.Items[i]);
                        }
                    }
                }
                for (int i = 0; i < arraylist1.Count; i++)
                {
                    if (!lstAssignedUser.Items.Contains(((ListItem)arraylist1[i])))
                    {
                        lstAssignedUser.Items.Add(((ListItem)arraylist1[i]));
                    }
                    lstAllUser.Items.Remove(((ListItem)arraylist1[i]));
                }
                lstAssignedUser.SelectedIndex = -1;
            }
            else
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please select atleast one in lstAllUser to move";
            }
        }

        protected void btnMoveLeft_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            if (lstAssignedUser.SelectedIndex >= 0)
            {
                for (int i = 0; i < lstAssignedUser.Items.Count; i++)
                {
                    if (lstAssignedUser.Items[i].Selected)
                    {
                        if (!arraylist2.Contains(lstAssignedUser.Items[i]))
                        {
                            arraylist2.Add(lstAssignedUser.Items[i]);
                        }
                    }
                }
                for (int i = 0; i < arraylist2.Count; i++)
                {
                    if (!lstAllUser.Items.Contains(((ListItem)arraylist2[i])))
                    {
                        lstAllUser.Items.Add(((ListItem)arraylist2[i]));
                    }
                    lstAssignedUser.Items.Remove(((ListItem)arraylist2[i]));
                }
                lstAllUser.SelectedIndex = -1;
            }
            else
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please select atleast one in Itmes to move";
            }
        }

        protected void btnMoveLeftAll_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            while (lstAssignedUser.Items.Count != 0)
            {
                for (int i = 0; i < lstAssignedUser.Items.Count; i++)
                {
                    lstAllUser.Items.Add(lstAssignedUser.Items[i]);
                    lstAssignedUser.Items.Remove(lstAssignedUser.Items[i]);
                }
            }
        }
    }
}