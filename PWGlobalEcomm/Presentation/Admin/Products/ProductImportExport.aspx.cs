﻿using Excel;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.Reflection;
using PWGlobalEcomm.BusinessEntity;
using System.ComponentModel;

namespace Presentation
{

    public partial class Admin_Products_ProductImportExport : BasePage//System.Web.UI.Page
    {
        #region Controls
        protected global::System.Web.UI.WebControls.GridView gvUploadedHistory;
        protected global::System.Web.UI.WebControls.TextBox txtCouponName;
        protected global::System.Web.UI.WebControls.FileUpload flupldProductExcel, flDownloadError;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dlStatus, divSubCategory;
        protected global::System.Web.UI.WebControls.Literal ltrMessage;
        protected global::System.Web.UI.WebControls.Label lblMaxFileSize;
        protected global::System.Web.UI.WebControls.DropDownList ddlCategory, ddlLanguage, ddlSubCategory; //Changes Done by Snehal 20 09 2016
        protected global::System.Web.UI.WebControls.HiddenField HIDSelectCatId; // Snehal 20 09 2016
        #endregion

        string originalFileName;

        ProductBE.ProductImportHistory objProductImportHistory = new ProductBE.ProductImportHistory();
        List<ProductBE.ProductImportHistory> lstImportHistoty = new List<ProductBE.ProductImportHistory>();
        List<ProductBE> lstProduct;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //PopulateUploadedExcel();
                int MaxFileSizeinbyte = Convert.ToInt32((GlobalFunctions.GetSetting("EXCELSIZE_PRODUCT"))) / 1024;
                lblMaxFileSize.Text = Convert.ToString(MaxFileSizeinbyte / 1024); // MegaByte
                PopulateCategory();
                BindGrid();
                BindDropDownLanguage();
            }
        }

        protected void gvUploadedHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUploadedHistory.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        private void PopulateCategory()
        {
            //*************************************************************************
            // Purpose : Populate parent category
            // Inputs  : Nothing
            // Return  : Nothing
            //*************************************************************************

            try
            {
                List<CategoryBE> lstCat = new List<CategoryBE>();
                lstCat = CategoryBL.GetAllCategoriesbyDefaultLanguage();
                ddlCategory.DataSource = lstCat;
                ddlCategory.DataTextField = "CategoryName";
                ddlCategory.DataValueField = "CategoryId";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("-Select Category-", "0"));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region For Subcategory Added  by Snehal 20 09 2016

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            CategoryBE objCategoryBE = new CategoryBE();
            List<CategoryBE> lstCategoryBE = new List<CategoryBE>();

            try
            {
                if (ddlCategory.SelectedIndex > 0)
                {
                    lstCategoryBE = CategoryBL.GetAllCategories();
                    if (lstCategoryBE != null)
                    {
                        if (lstCategoryBE.Count > 0)
                        {
                            lstCategoryBE = lstCategoryBE.FindAll(x => x.ParentCategoryId == Convert.ToInt16(ddlCategory.SelectedValue));
                            if (lstCategoryBE.Count > 0)
                            {
                                ddlSubCategory.DataSource = lstCategoryBE;
                                ddlSubCategory.DataTextField = "CategoryName";
                                ddlSubCategory.DataValueField = "CategoryId";
                                ddlSubCategory.DataBind();
                                ddlSubCategory.Items.Insert(0, new ListItem("-Select SubCategory-", "0"));

                                divSubCategory.Visible = true;
                            }
                            else
                            {
                                divSubCategory.Visible = false;
                                HIDSelectCatId.Value = ddlCategory.SelectedValue.ToString();

                            }
                        }
                        else
                        {
                            divSubCategory.Visible = false;
                            HIDSelectCatId.Value = ddlCategory.SelectedValue.ToString();

                        }
                    }
                    else
                    {
                        divSubCategory.Visible = false;
                        HIDSelectCatId.Value = ddlCategory.SelectedValue.ToString();

                    }
                }
                else
                {
                    divSubCategory.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objCategoryBE = null;
                lstCategoryBE = null;
            }
        }

        protected void ddlSubCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubCategory.SelectedIndex > 0)
            {
                HIDSelectCatId.Value = ddlSubCategory.SelectedValue.ToString();
            }
        }

        #endregion

        protected void gvUploadedHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkErrorFile = e.Row.FindControl("lnkErrorFile") as LinkButton;

                if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "FailedRecords")) == "0")
                {
                    lnkErrorFile.Visible = false;
                }
                else
                {
                    lnkErrorFile.Visible = true;
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!flupldProductExcel.HasFile)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Excel File.", AlertType.Warning);
                }
                else
                {
                    bool status = false;
                    string ErrorFileName = "";
                    int FailedRecords = 0;
                    List<ProductBE> lstProductBE = new List<ProductBE>();
                    string xlsFile = flupldProductExcel.FileName;  //fileImport is the file upload control
                    string ext = System.IO.Path.GetExtension(xlsFile);
                    if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Only Excel Files are allowed for Product Import.", AlertType.Warning);
                        return;
                    }
                    string a = Convert.ToInt32(GlobalFunctions.GetSetting("EXCELSIZE_PRODUCT")).ToString();
                    if (!GlobalFunctions.IsFileSizeValid(flupldProductExcel, Convert.ToInt32(GlobalFunctions.GetSetting("EXCELSIZE_PRODUCT"))))
                    {
                        return;
                    }
                    originalFileName = xlsFile.Substring(0, xlsFile.LastIndexOf("."));
                    string fileName = originalFileName.Replace(" ", "_") + "_" + DateTime.Now.ToString("MMddyyyyHHmmss");
                    string targetFileName = MapPath("~/ProductTranslation/Import/") + fileName + ext;
                    if (System.IO.File.Exists(targetFileName))
                    {
                        System.IO.File.Delete(targetFileName);
                    }
                    flupldProductExcel.PostedFile.SaveAs(targetFileName);
                    FileStream stream = File.Open(targetFileName, FileMode.Open, FileAccess.Read);

                    DataSet dsresult = new DataSet();
                    DataTable dtData = new DataTable();

                    if (ext.ToLower() == ".xlsx")
                    {
                        IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        excelReader.IsFirstRowAsColumnNames = true;
                        dsresult = excelReader.AsDataSet();
                        excelReader.Close();
                        excelReader.Dispose();
                    }
                    else if (ext.ToLower() == ".xls")
                    {
                        IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                        excelReader.IsFirstRowAsColumnNames = true;
                        dsresult = excelReader.AsDataSet();
                        excelReader.Close();
                    }
                    if (ValidateColumnsStructure(dsresult))
                    {
                        if (dsresult.Tables.Count > 0)
                        {
                            bool res = ProductBL.BulkImportExcel(dsresult.Tables[0], "dbo.Tbl_TempProductData");
                            if (res)
                            {
                                bool Isrecordaffected = ProductBL.UpdateProductTranslation();
                                if (Isrecordaffected)
                                {
                                    List<ProductBE> lstProductUnMatched = new List<ProductBE>();
                                    lstProductUnMatched = ProductBL.GetUnMatchedProductFromImport();
                                    if (lstProductUnMatched != null)
                                    {
                                        ErrorFileName = CreateErrorFile(lstProductUnMatched);
                                        FailedRecords = lstProductUnMatched.Count;
                                    }
                                    else { FailedRecords = 0; }
                                    bool output = InsertImportHistory(fileName, ErrorFileName, FailedRecords);

                                    if (output)
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/UpdatedMessage "), AlertType.Success);
                                        CreateActivityLog("ProductImportExport btnSubmit", "Insert", fileName);
                                        BindGrid();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Proper Data Structure.", AlertType.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected bool ValidateColumnsStructure(DataSet dsExcel)
        {
            int count = 0;
            bool res = false;
            DataTable dt = new DataTable();
            dt.Columns.Add("ProductCode", typeof(string));
            dt.Columns.Add("LanguageId", typeof(Int16));
            dt.Columns.Add("ProductName", typeof(string));
            dt.Columns.Add("ProductDescription", typeof(string));
            dt.Columns.Add("FurtherDescription", typeof(string));

            for (int i = 0; i < dsExcel.Tables[0].Columns.Count; i++)
            {
                if (dt.Columns[i].ToString() == dsExcel.Tables[0].Columns[i].ColumnName)
                {
                    count++;
                }
                else
                {
                    return false;
                }
            }
            if (count == 5)
                return true;
            else
                return false;
        }

        protected void BindDropDownLanguage()
        {
            StoreBE lstStoreDetail = StoreBL.GetStoreDetails();
            int StoreId = lstStoreDetail.StoreId;
            List<StoreBE.StoreLanguageBE> lstLanguageBE = lstStoreDetail.StoreLanguages;
            ddlLanguage.DataSource = lstLanguageBE;
            ddlLanguage.DataTextField = "LanguageName";
            ddlLanguage.DataValueField = "LanguageId";
            ddlLanguage.DataBind();
            ddlLanguage.Items.Insert(0, new ListItem("-Select Language-", "0"));
        }

        protected bool InsertImportHistory(string UploadedFileName, string ErroFileName, int NoofFailedRecords)
        {
            UserBE lst = Session["StoreUser"] as UserBE;
            objProductImportHistory.CreatedBy = lst.UserId;
            objProductImportHistory.FailedRecords = NoofFailedRecords;
            objProductImportHistory.UploadedFileName = UploadedFileName;
            objProductImportHistory.ErrorFileName = ErroFileName;
            return ProductBL.InsertProductImportHistory(objProductImportHistory);
        }

        protected void BindGrid()
        {
            List<ProductBE.ProductImportHistory> lstImportData = new List<ProductBE.ProductImportHistory>();
            lstImportData = ProductBL.GetProductImportHistory();
            if (lstImportData != null && lstImportData.Count > 0)
            {
                gvUploadedHistory.DataSource = lstImportData;
                gvUploadedHistory.DataBind();
            }
            else
            {
                gvUploadedHistory.DataSource = null;
                gvUploadedHistory.DataBind();
            }
        }

        private string CreateErrorFile(List<ProductBE> lstProductBE)
        {
            string ErrorFileName = originalFileName;
            string fileName = originalFileName.Replace(" ", "_") + "_" + DateTime.Now.ToString("MMddyyyyHHmmss");
            string ErrorFilePath = Server.MapPath("~/ProductTranslation/Errors/") + fileName + ".xls";
            DataTable dt = ToDataTable(lstProductBE);
            DataTable NewTable = dt.DefaultView.ToTable(false, "ProductCode", "LanguageId", "ProductName", "ProductDescription", "FurtherDescription");
            ExportToExcelAndSave(NewTable, ErrorFilePath);
            return fileName;
        }

        protected void lnkUploadedFile_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkUploadedFile = (LinkButton)sender;
                LinkButton lnkFile = (LinkButton)lnkUploadedFile.Parent.FindControl("lnkUploadedFile");
                DownloadFile(Server.MapPath("~/ProductTranslation/Import/") + lnkFile.Text + ".xlsx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static void DownloadFile(string fullPhysicalPath)
        {
            string path = fullPhysicalPath; //get file object as FileInfo

            System.IO.FileInfo file = new System.IO.FileInfo(path); //-- if the file exists on the server

            if (File.Exists(path)) //set appropriate headers
            {
                //Image img = Image.FromFile(path);
                //img.Dispose();
                if (file.IsReadOnly == true)
                {
                    File.SetAttributes(fullPhysicalPath, FileAttributes.Normal);
                }
                FileStream MyFileStream = new FileStream(fullPhysicalPath, FileMode.Open);
                int FileSize = Convert.ToInt32(MyFileStream.Length);

                byte[] Buffer = new byte[FileSize];

                MyFileStream.Read(Buffer, 0, FileSize);
                MyFileStream.Close();

                //HttpContext.Current.Response.BufferOutput = true;
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                HttpContext.Current.Response.BinaryWrite(Buffer);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }

            path = null;
            file = null;
        }

        public DataSet CreateDataSet<T>(List<T> list)
        {
            //list is nothing or has nothing, return nothing (or add exception handling)
            if (list == null || list.Count == 0) { return null; }

            //get the type of the first obj in the list
            var obj = list[0].GetType();

            //now grab all properties
            var properties = obj.GetProperties();

            //make sure the obj has properties, return nothing (or add exception handling)
            if (properties.Length == 0) { return null; }

            //it does so create the dataset and table
            var dataSet = new DataSet();
            var dataTable = new DataTable();

            //now build the columns from the properties
            var columns = new DataColumn[properties.Length];
            for (int i = 0; i < properties.Length; i++)
            {
                columns[i] = new DataColumn(properties[i].Name, properties[i].PropertyType);
            }

            //add columns to table
            dataTable.Columns.AddRange(columns);

            //now add the list values to the table
            foreach (var item in list)
            {
                //create a new row from table
                var dataRow = dataTable.NewRow();

                //now we have to iterate thru each property of the item and retrieve it's value for the corresponding row's cell
                var itemProperties = item.GetType().GetProperties();

                for (int i = 0; i < itemProperties.Length; i++)
                {
                    dataRow[i] = itemProperties[i].GetValue(item, null);
                }

                //now add the populated row to the table
                dataTable.Rows.Add(dataRow);
            }

            //add table to dataset
            dataSet.Tables.Add(dataTable);

            //return dataset
            return dataSet;
        }

        protected bool ExportValidation()
        {
            bool res = false;
            if (ddlCategory.SelectedValue.ToString() != "0")
            {
                if (ddlLanguage.SelectedValue.ToString() != "0")
                { res = true; }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Language.", AlertType.Warning);
                    res = false;
                }
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Category.", AlertType.Warning);
            }
            return res;
        }

        protected void btnExportProducts_Click(object sender, EventArgs e)
        {
            try
            {
                if (ExportValidation())
                {
                    #region Product Export
                    DataSet dsData = new DataSet();

                    DataTable dtExportToExcel = new DataTable();

                    //  lstProduct = ProductBL.GetTranslatedProductByCategory(Convert.ToInt16(ddlCategory.SelectedValue), Convert.ToInt16(ddlLanguage.SelectedValue));

                    #region Added by snehal 20 09 2016
                    Int16 catId = Convert.ToInt16(HIDSelectCatId.Value);

                    lstProduct = ProductBL.GetTranslatedProductByCategory(catId, Convert.ToInt16(ddlLanguage.SelectedValue));
                    #endregion

                    if (lstProduct.Count > 0)
                    {
                        List<object> lstAnonymousObjs = new List<object>();

                        foreach (ProductBE product in lstProduct)
                        {
                            lstAnonymousObjs.Add(new
                            {
                                ProductCode = product.ProductCode,
                                LanguageId = product.LanguageId,
                                ProductName = product.ProductName,
                                ProductDescription = product.ProductDescription,
                                FurtherDescription = product.FurtherDescription
                            });
                        }

                        dsData = CreateDataSet(lstAnonymousObjs);

                        if (dsData == null)
                        {
                            dlStatus.Visible = true;
                            dlStatus.Attributes.Add("class", "Errormsg");
                            ltrMessage.Text = Exceptions.GetException("SystemError/GeneralMessage");
                            return;
                        }
                        dtExportToExcel = dsData.Tables[0];
                        GenerateExcel(dtExportToExcel);
                    }

                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "No Products Found.", AlertType.Warning);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GenerateExcel(DataTable tbl)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ProductData");
                // DataTable NewTable = tbl.DefaultView.ToTable(false, "ProductCode","LanguageId","ProductName","ProductDescription", "FurtherDescription", "");

                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                ws.Cells["A1"].LoadFromDataTable(tbl, true);

                Response.Clear();
                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=ProductData.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public static void ExportToExcelAndSave(DataTable dt, string path)
        {
            try
            {
                StreamWriter writer = new StreamWriter(path);

                int colCount = dt.Columns.Count;
                for (int i = 0; i < colCount; i++)
                {
                    writer.Write(dt.Columns[i]);
                    if (i < colCount - 1)
                    {
                        writer.Write("\t");
                    }
                }
                writer.Write(writer.NewLine);
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < colCount; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            writer.Write(dr[i].ToString());
                        }
                        if (i < colCount - 1)
                        {
                            writer.Write("\t");
                        }
                    }
                    writer.Write(writer.NewLine);
                }

                writer.Close();
                writer.Dispose();

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lnkErrorFile_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkErrorFile = (LinkButton)sender;
                LinkButton lnkFile = (LinkButton)lnkErrorFile.Parent.FindControl("lnkErrorFile");
                DownloadFile(Server.MapPath("~/ProductTranslation/Errors/") + lnkFile.Text + ".xls");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}