﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Products_ProductAttributeManagement : System.Web.UI.Page
    {
        #region controls
        protected global::System.Web.UI.WebControls.GridView grdProductAttribute;
        protected global::System.Web.UI.WebControls.Literal ltrNoOfProducts;
        protected global::System.Web.UI.WebControls.TextBox txtSearch;
        protected global::System.Web.UI.WebControls.CheckBox chkAll;
        protected global::System.Web.UI.WebControls.CheckBox chkClear;
        #endregion
        ProductBE objProductBE = new ProductBE();

        public Int16 PageIndex;
        public Int16 PageSize = 50;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PageIndex = 0;
                objProductBE.PropGetAllProductSize = ProductBL.GetAllProductAttributesByType<ProductBE.ProductSize>(ProductAttributeType.Size, 1);
                objProductBE.PropGetAllProductElements = ProductBL.GetAllProductAttributesByType<ProductBE.ProductElements>(ProductAttributeType.Element, 1);
                objProductBE.PropGetAllProductUOM = ProductBL.GetAllProductAttributesByType<ProductBE.ProductUOM>(ProductAttributeType.UOM, 1);

                Session["AllSize"] = objProductBE.PropGetAllProductSize;
                Session["AllElement"] = objProductBE.PropGetAllProductElements;
                Session["AllUOM"] = objProductBE.PropGetAllProductUOM;
                BindGridAllProductBtn(PageIndex, PageSize, "");
            }

        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void BindGridAllProductBtn(Int16 PageIndex, Int16 PageSize, string SearchText)
        {
            try
            {
                List<ProductBE.ProductAttributes> lstPBE = new List<ProductBE.ProductAttributes>();

                List<ProductBE.ProductAttributes> Data = new List<ProductBE.ProductAttributes>();
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("pagesize", Convert.ToString(PageSize));
                dictionaryInstance.Add("pageindex", Convert.ToString(PageIndex));
                dictionaryInstance.Add("searchText", Convert.ToString(SearchText));

                objProductBE.PropGetAllProductSize = Session["AllSize"] as List<ProductBE.ProductSize>; //ProductBL.GetAllProductAttributesByType<ProductBE.ProductSize>(ProductAttributeType.Size, 1);
                objProductBE.PropGetAllProductElements = Session["AllElement"] as List<ProductBE.ProductElements>; //ProductBL.GetAllProductAttributesByType<ProductBE.ProductElements>(ProductAttributeType.Element, 1);
                objProductBE.PropGetAllProductUOM = Session["AllUOM"] as List<ProductBE.ProductUOM>; //ProductBL.GetAllProductAttributesByType<ProductBE.ProductUOM>(ProductAttributeType.UOM, 1);

                lstPBE = ProductBL.GetListProductAttribute(Constants.USP_GetListProductAttribute, dictionaryInstance);

                if (lstPBE != null)
                {
                    if (lstPBE.Count > 0)
                    {

                        DataTable dt = ToDataTable(lstPBE);
                        grdProductAttribute.DataSource = lstPBE;
                        ltrNoOfProducts.Text = lstPBE[0].TotalRow.ToString();
                        grdProductAttribute.VirtualItemCount = lstPBE[0].TotalRow;
                        grdProductAttribute.DataBind();

                        foreach (GridViewRow Row in grdProductAttribute.Rows)
                        {
                            ((DropDownList)Row.FindControl("ddlSize")).SelectedValue = lstPBE[Row.DataItemIndex].SizeId.ToString();
                            ((DropDownList)Row.FindControl("ddlElementA")).SelectedValue = lstPBE[Row.DataItemIndex].ElementId1.ToString();
                            ((DropDownList)Row.FindControl("ddlElementB")).SelectedValue = lstPBE[Row.DataItemIndex].ElementId2.ToString();
                            ((DropDownList)Row.FindControl("ddlUOMA1")).SelectedValue = lstPBE[Row.DataItemIndex].UOMIdA1.ToString();
                            ((DropDownList)Row.FindControl("ddlUOMA2")).SelectedValue = lstPBE[Row.DataItemIndex].UOMIdA2.ToString();
                            ((DropDownList)Row.FindControl("ddlUOMB1")).SelectedValue = lstPBE[Row.DataItemIndex].UOMIdB1.ToString();
                            ((DropDownList)Row.FindControl("ddlUOMB2")).SelectedValue = lstPBE[Row.DataItemIndex].UOMIdB2.ToString();
                            if (lstPBE[Row.DataItemIndex].ValueA1 == "0")
                            {
                                ((TextBox)Row.FindControl("txtValueA1")).Text = "";
                            }
                            if (lstPBE[Row.DataItemIndex].ValueA2 == "0")
                            {
                                ((TextBox)Row.FindControl("txtValueA2")).Text = "";
                            }
                            if (lstPBE[Row.DataItemIndex].ValueB1 == "0")
                            {
                                ((TextBox)Row.FindControl("txtValueB1")).Text = "";
                            }
                            if (lstPBE[Row.DataItemIndex].ValueB2 == "0")
                            {
                                ((TextBox)Row.FindControl("txtValueB2")).Text = "";
                            }
                        }
                    }
                }
                else
                {
                    grdProductAttribute.DataSource = null;
                    grdProductAttribute.DataBind();
                }
            }

            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void BindGridAllProductPg(Int16 PageIndex, Int16 PageSize, string SearchText)
        {
            try
            {
                List<ProductBE.ProductAttributes> lstPBE = new List<ProductBE.ProductAttributes>();

                List<ProductBE.ProductAttributes> Data = new List<ProductBE.ProductAttributes>();
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("pagesize", Convert.ToString(PageSize));
                dictionaryInstance.Add("pageindex", Convert.ToString(PageIndex));
                dictionaryInstance.Add("searchText", Convert.ToString(SearchText));

                lstPBE = ProductBL.GetListProductAttribute(Constants.USP_GetListProductAttribute, dictionaryInstance);

                if (lstPBE != null)
                {
                    if (lstPBE.Count > 0)
                    {
                        DataTable dt = ToDataTable(lstPBE);
                        //Session["Originaldata"] = dt;
                        grdProductAttribute.DataSource = lstPBE;
                        grdProductAttribute.DataBind();
                        foreach (GridViewRow Row in grdProductAttribute.Rows)
                        {
                            ((DropDownList)Row.FindControl("ddlSize")).SelectedValue = lstPBE[Row.DataItemIndex].SizeId.ToString();
                            ((DropDownList)Row.FindControl("ddlElementA")).SelectedValue = lstPBE[Row.DataItemIndex].ElementId1.ToString();
                            ((DropDownList)Row.FindControl("ddlElementB")).SelectedValue = lstPBE[Row.DataItemIndex].ElementId2.ToString();
                            ((DropDownList)Row.FindControl("ddlUOMA1")).SelectedValue = lstPBE[Row.DataItemIndex].UOMIdA1.ToString();
                            ((DropDownList)Row.FindControl("ddlUOMA2")).SelectedValue = lstPBE[Row.DataItemIndex].UOMIdA2.ToString();
                            ((DropDownList)Row.FindControl("ddlUOMB1")).SelectedValue = lstPBE[Row.DataItemIndex].UOMIdB1.ToString();
                            ((DropDownList)Row.FindControl("ddlUOMB1")).SelectedValue = lstPBE[Row.DataItemIndex].UOMIdB2.ToString();
                            if (lstPBE[Row.DataItemIndex].ValueA1 == "0")
                            {
                                ((TextBox)Row.FindControl("txtValueA1")).Text = "";
                            }
                            if (lstPBE[Row.DataItemIndex].ValueA2 == "0")
                            {
                                ((TextBox)Row.FindControl("txtValueA2")).Text = "";
                            }
                            if (lstPBE[Row.DataItemIndex].ValueB1 == "0")
                            {
                                ((TextBox)Row.FindControl("txtValueB1")).Text = "";
                            }
                            if (lstPBE[Row.DataItemIndex].ValueB2 == "0")
                            {
                                ((TextBox)Row.FindControl("txtValueB2")).Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PageIndex = 0;
            BindGridAllProductBtn(PageIndex, PageSize, txtSearch.Text.ToString());
        }

        protected void grdProductAttribute_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                #region ddlSize
                DropDownList ddlSize = (DropDownList)e.Row.FindControl("ddlSize");
                ddlSize.DataSource = objProductBE.PropGetAllProductSize;
                ddlSize.DataTextField = "Size";
                ddlSize.DataValueField = "SizeId";
                ddlSize.DataBind();
                ddlSize.Items.Insert(0, new ListItem("Select Size", "0"));
                #endregion

                #region ddlElement
                #region ddlElementA
                DropDownList ddlElementA = (DropDownList)e.Row.FindControl("ddlElementA");
                ddlElementA.DataSource = objProductBE.PropGetAllProductElements;
                ddlElementA.DataTextField = "Element";
                ddlElementA.DataValueField = "ElementId";
                ddlElementA.DataBind();
                ddlElementA.Items.Insert(0, new ListItem("Select Element", "0"));
                #endregion
                #region ddlElementB
                DropDownList ddlElementB = (DropDownList)e.Row.FindControl("ddlElementB");
                ddlElementB.DataSource = objProductBE.PropGetAllProductElements;
                ddlElementB.DataTextField = "Element";
                ddlElementB.DataValueField = "ElementId";
                ddlElementB.DataBind();
                ddlElementB.Items.Insert(0, new ListItem("Select Element", "0"));
                #endregion
                #endregion

                #region ddlUOM
                #region ddlUOMA
                DropDownList ddlUOMA1 = (DropDownList)e.Row.FindControl("ddlUOMA1");
                ddlUOMA1.DataSource = objProductBE.PropGetAllProductUOM;
                ddlUOMA1.DataTextField = "UOM";
                ddlUOMA1.DataValueField = "UOMId";
                ddlUOMA1.DataBind();
                ddlUOMA1.Items.Insert(0, new ListItem("Select UOM", "0"));
                DropDownList ddlUOMA2 = (DropDownList)e.Row.FindControl("ddlUOMA2");
                ddlUOMA2.DataSource = objProductBE.PropGetAllProductUOM;
                ddlUOMA2.DataTextField = "UOM";
                ddlUOMA2.DataValueField = "UOMId";
                ddlUOMA2.DataBind();
                ddlUOMA2.Items.Insert(0, new ListItem("Select UOM", "0"));
                #endregion
                #region ddlUOMB
                DropDownList ddlUOMB1 = (DropDownList)e.Row.FindControl("ddlUOMB1");
                ddlUOMB1.DataSource = objProductBE.PropGetAllProductUOM;
                ddlUOMB1.DataTextField = "UOM";
                ddlUOMB1.DataValueField = "UOMId";
                ddlUOMB1.DataBind();
                ddlUOMB1.Items.Insert(0, new ListItem("Select UOM", "0"));
                DropDownList ddlUOMB2 = (DropDownList)e.Row.FindControl("ddlUOMB2");
                ddlUOMB2.DataSource = objProductBE.PropGetAllProductUOM;
                ddlUOMB2.DataTextField = "UOM";
                ddlUOMB2.DataValueField = "UOMId";
                ddlUOMB2.DataBind();
                ddlUOMB2.Items.Insert(0, new ListItem("Select UOM", "0"));
                #endregion
                #endregion

            }
        }

        protected void btnSaveAttribute_Click(object sender, EventArgs e)
        {
            int iRes = 0;
            bool flag = ValidateData();
            if (flag)
            {
                iRes = SaveData();
            }
            if (iRes > 0)
            {
                BindGridAllProductBtn(Convert.ToInt16(grdProductAttribute.PageIndex), PageSize, "");    
                GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/AttributeSaved"), AlertType.Success);
            }
        }

        protected int SaveData()
        {
            int res = 0;
            try
            {
                DataTable dt = new DataTable();

                dt = ProductBL.GettableRowProductAttribute();
                dt = SetPrimaryKey(dt);
                dt = InsertUpdatedtAttribute(dt);

                // bool Issuccesseed = objProductBL.BulkInsertProductAttribute(dt); 
                DataSet ds = new DataSet();
                dt.TableName = "Tbl_ProductAttributeManagement";
                ds.Tables.Add(dt);

                int iRes = ProductBL.SaveUpdateProductAttribute(ds);
                //BindGridAllProduct();
                res = 1;

            }
            catch (Exception) { res = 0; }
            return res;
        }

        protected bool ValidateData()
        {
            bool finalres = false;
            string msg;
            foreach (GridViewRow Row in grdProductAttribute.Rows)
            {
                DropDownList ddlSize = (DropDownList)Row.FindControl("ddlSize");
                DropDownList ddlElementA = (DropDownList)Row.FindControl("ddlElementA");
                DropDownList ddlElementB = (DropDownList)Row.FindControl("ddlElementB");
                DropDownList ddlUOMA1 = (DropDownList)Row.FindControl("ddlUOMA1");
                DropDownList ddlUOMA2 = (DropDownList)Row.FindControl("ddlUOMA2");
                DropDownList ddlUOMB1 = (DropDownList)Row.FindControl("ddlUOMB1");
                DropDownList ddlUOMB2 = (DropDownList)Row.FindControl("ddlUOMB2");
                TextBox txtValueA1 = (TextBox)Row.FindControl("txtValueA1");
                TextBox txtValueA2 = (TextBox)Row.FindControl("txtValueA2");
                TextBox txtValueB1 = (TextBox)Row.FindControl("txtValueB1");
                TextBox txtValueB2 = (TextBox)Row.FindControl("txtValueB2");



                if (ddlSize.SelectedValue != "0")
                {
                    #region For Element A
                    if (ddlElementA.SelectedValue == "0")
                    {
                        
                        Row.Cells[4].BackColor = Color.Red;
                        Row.Cells[3].BackColor = Color.FromName("#F8F8F8");
                        Row.Cells[6].BackColor = Color.FromName("#F8F8F8");
                        Row.Cells[5].BackColor = Color.FromName("#F8F8F8");
                        ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Select Element');</script>");
                        Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + ddlElementA.ClientID + "').focus();", true);
                        finalres = false;
                        break;

                    }
                    else if (ddlElementA.SelectedValue != "0" && ddlUOMA1.SelectedValue == "0")
                    {
                        Row.Cells[5].BackColor = Color.Red;
                        Row.Cells[3].BackColor = Color.FromName("#F8F8F8");
                        Row.Cells[6].BackColor = Color.FromName("#F8F8F8");
                        Row.Cells[4].BackColor = Color.FromName("#F8F8F8");
                        ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Select Unit of Measurement');</script>");
                        Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + ddlUOMA1.ClientID + "').focus();", true);
                        finalres = false;
                        break;
                    }
                    else if (ddlUOMA1.SelectedValue != "0" && txtValueA1.Text == "")
                    {
                        Row.Cells[6].BackColor = Color.Red;
                        Row.Cells[5].BackColor = Color.FromName("#F8F8F8");
                        Row.Cells[3].BackColor = Color.FromName("#F8F8F8");
                        Row.Cells[4].BackColor = Color.FromName("#F8F8F8");
                        ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Enter Value');</script>");
                        Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + txtValueA1.ClientID + "').focus();", true);
                        finalres = false;
                        break;
                    }
                    else if (ddlSize.SelectedValue == "0" || ddlElementA.SelectedValue == "0" || ddlUOMA1.SelectedValue == "0" || txtValueA1.Text == "")
                    {
                        ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Size,Element,UOM and Value are compulsory.');</script>");
                        finalres = false;
                        break;
                    }
                    else
                    {
                        Row.BackColor = Color.FromName("#F8F8F8");
                        finalres = true;
                    }
                    #endregion
                    #region commented
                    //#region For Element B

                    //if (ddlElementB.SelectedValue != "0" && ddlUOMB1.SelectedValue == "0")
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Select Unit of Measurement');</script>");
                    //    Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + ddlUOMB1.ClientID + "').focus();", true);
                    //    finalres = false;
                    //    break;

                    //}
                    //else if (ddlUOMB1.SelectedValue != "0" && txtValueB1.Text == "")
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Enter Value');</script>");
                    //    Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + txtValueB1.ClientID + "').focus();", true);
                    //    finalres = false;
                    //    break;
                    //}
                    //else if (ddlElementB.SelectedValue != "0" && ddlUOMB1.SelectedValue == "0" || txtValueB1.Text == "")
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('UOM and Value are compulsory.');</script>");
                    //    //Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + ddlSize.ClientID + "').focus();", true);
                    //    finalres = false;
                    //    break;
                    //}
                    //else
                    //{
                    //    finalres = true;
                    //}

                    //#endregion
                    //#region For secondUOM

                    //if (ddlUOMA2.SelectedValue != "0" && txtValueA2.Text == "")
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Enter Value');</script>");
                    //    Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + txtValueA2.ClientID + "').focus();", true);
                    //    finalres = false;
                    //    break;
                    //}
                    //else if (ddlSize.SelectedValue != "0" || ddlElementA.SelectedValue != "0" || ddlUOMA2.SelectedValue == "0" || txtValueA2.Text == "")
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('UOM and Value are compulsory.');</script>");
                    //    //Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + ddlSize.ClientID + "').focus();", true);
                    //    finalres = false;
                    //    break;
                    //}
                    //else
                    //{
                    //    finalres = true;
                    //}
                    //#endregion
                    //#region For FourthUOM

                    //if (ddlUOMB2.SelectedValue != "0" && txtValueB2.Text == "")
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Enter Value');</script>");
                    //    Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + txtValueB2.ClientID + "').focus();", true);
                    //    finalres = false;
                    //    break;
                    //}
                    //else if (ddlSize.SelectedValue != "0" || ddlElementB.SelectedValue != "0" || ddlUOMB2.SelectedValue == "0" || txtValueB2.Text == "")
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Size,Element,UOM and Value are compulsory.');</script>");
                    //    //Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + ddlSize.ClientID + "').focus();", true);
                    //    finalres = false;
                    //    break;
                    //}
                    //else
                    //{
                    //    finalres = true;
                    //}
                    //#endregion

                    #endregion

                }
                else if (ddlSize.SelectedValue == "0")
                {
                    if (ddlElementA.SelectedValue != "0" || ddlUOMA1.SelectedValue != "0" || txtValueA1.Text != "" || txtValueA1.Text == "0")
                    {
                        Row.Cells[3].BackColor = Color.Red;
                        //Row.BackColor = Color.Red;
                        ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Select Size');</script>");
                        Page.ClientScript.RegisterStartupScript(GetType(), "foc", "document.getElementById('" + ddlSize.ClientID + "').focus();", true);
                        finalres = false;
                        break;
                    }
                    else
                    {
                        Row.BackColor = Color.FromName("#F8F8F8");
                        finalres = true;
                    }

                }
                else
                {
                    finalres = false;
                }
            }
            return finalres;
        }

        #region Pagination

        /// <summary>
        /// //
        /// 
        /// PageIndex = 0 ====>>>> Current Page =1
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnClearAttritbutebottom_Click(object sender, EventArgs e)
        {
            ClearFields();
            int iRes = SaveData();
            if (iRes > 0)
            {
                BindGridAllProductBtn(Convert.ToInt16(grdProductAttribute.PageIndex), PageSize, "");
                GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/AttributeSaved"), AlertType.Success);
            }
        }

        protected void btnClearAttributetop_Click(object sender, EventArgs e)
        {
            ClearFields();
            int iRes = SaveData();
            if (iRes > 0)
            {
                BindGridAllProductBtn(Convert.ToInt16(grdProductAttribute.PageIndex), PageSize, "");
                GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/AttributeSaved"), AlertType.Success);
            }
        }

        protected void ClearFields()
        {
            try
            {
                foreach (GridViewRow Row in grdProductAttribute.Rows)
                {
                    DropDownList ddlSize = (DropDownList)Row.FindControl("ddlSize");
                    DropDownList ddlElementA = (DropDownList)Row.FindControl("ddlElementA");
                    DropDownList ddlElementB = (DropDownList)Row.FindControl("ddlElementB");
                    DropDownList ddlUOMA1 = (DropDownList)Row.FindControl("ddlUOMA1");
                    DropDownList ddlUOMA2 = (DropDownList)Row.FindControl("ddlUOMA2");
                    DropDownList ddlUOMB1 = (DropDownList)Row.FindControl("ddlUOMB1");
                    DropDownList ddlUOMB2 = (DropDownList)Row.FindControl("ddlUOMB2");
                    TextBox txtValueA1 = (TextBox)Row.FindControl("txtValueA1");
                    TextBox txtValueA2 = (TextBox)Row.FindControl("txtValueA2");
                    TextBox txtValueB1 = (TextBox)Row.FindControl("txtValueB1");
                    TextBox txtValueB2 = (TextBox)Row.FindControl("txtValueB2");
                    CheckBox chkClear = (CheckBox)Row.FindControl("chkClear");

                    if (chkClear.Checked)
                    {
                        ddlSize.SelectedValue = "0";
                        ddlElementA.SelectedValue = "0";
                        ddlElementB.SelectedValue = "0";
                        ddlUOMA1.SelectedValue = "0";
                        ddlUOMA2.SelectedValue = "0";
                        ddlUOMB1.SelectedValue = "0";
                        ddlUOMB2.SelectedValue = "0";
                        txtValueA1.Text = "";
                        txtValueA2.Text = "";
                        txtValueB1.Text = "";
                        txtValueB2.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void grdProductAttribute_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //bool flag = ValidateData();
            //if (flag)
            //{
            //    //SaveData();
            //    grdProductAttribute.PageIndex = e.NewPageIndex;

            objProductBE.PropGetAllProductSize = Session["AllSize"] as List<ProductBE.ProductSize>; //ProductBL.GetAllProductAttributesByType<ProductBE.ProductSize>(ProductAttributeType.Size, 1);
            objProductBE.PropGetAllProductElements = Session["AllElement"] as List<ProductBE.ProductElements>; //ProductBL.GetAllProductAttributesByType<ProductBE.ProductElements>(ProductAttributeType.Element, 1);
            objProductBE.PropGetAllProductUOM = Session["AllUOM"] as List<ProductBE.ProductUOM>; //ProductBL.GetAllProductAttributesByType<ProductBE.ProductUOM>(ProductAttributeType.UOM, 1);


            PageIndex = Convert.ToInt16(e.NewPageIndex);
            grdProductAttribute.PageIndex = e.NewPageIndex;
            BindGridAllProductBtn(PageIndex, PageSize, txtSearch.Text.ToString());
        }



        #endregion





        

        private DataTable InsertUpdatedtAttribute(DataTable dtInsert)
        {
            try
            {
                foreach (GridViewRow grRow in grdProductAttribute.Rows)
                {
                    #region "Controls"
                    DropDownList ddlSize = (DropDownList)grdProductAttribute.Rows[grRow.RowIndex].FindControl("ddlSize");
                    DropDownList ddlElementA = (DropDownList)grdProductAttribute.Rows[grRow.RowIndex].FindControl("ddlElementA");
                    DropDownList ddlElementB = (DropDownList)grdProductAttribute.Rows[grRow.RowIndex].FindControl("ddlElementB");
                    DropDownList ddlUOMA1 = (DropDownList)grdProductAttribute.Rows[grRow.RowIndex].FindControl("ddlUOMA1");
                    DropDownList ddlUOMA2 = (DropDownList)grdProductAttribute.Rows[grRow.RowIndex].FindControl("ddlUOMA2");
                    DropDownList ddlUOMB1 = (DropDownList)grdProductAttribute.Rows[grRow.RowIndex].FindControl("ddlUOMB1");
                    DropDownList ddlUOMB2 = (DropDownList)grdProductAttribute.Rows[grRow.RowIndex].FindControl("ddlUOMB2");
                    TextBox txtValueA1 = (TextBox)grdProductAttribute.Rows[grRow.RowIndex].FindControl("txtValueA1");
                    TextBox txtValueA2 = (TextBox)grdProductAttribute.Rows[grRow.RowIndex].FindControl("txtValueA2");
                    TextBox txtValueB1 = (TextBox)grdProductAttribute.Rows[grRow.RowIndex].FindControl("txtValueB1");
                    TextBox txtValueB2 = (TextBox)grdProductAttribute.Rows[grRow.RowIndex].FindControl("txtValueB2");
                    #endregion
                    if (txtValueA1.Text == "") { txtValueA1.Text = "0"; }
                    if (txtValueA2.Text == "") { txtValueA2.Text = "0"; }
                    if (txtValueB1.Text == "") { txtValueB1.Text = "0"; }
                    if (txtValueB2.Text == "") { txtValueB2.Text = "0"; }

                    DataRow[] workRow1;
                    HiddenField hdfProductSKUId = (HiddenField)grdProductAttribute.Rows[grRow.RowIndex].FindControl("hdfProductSKUId");
                    workRow1 = dtInsert.Select("ProductSKUId=" + hdfProductSKUId.Value);
                    if (workRow1.Length > 0)
                    {
                        //update  
                        #region "Assign Values"
                        workRow1[0]["ProductSKUId"] = Convert.ToInt16(grdProductAttribute.DataKeys[grRow.RowIndex].Values[0].ToString());
                        workRow1[0]["SizeId"] = Convert.ToInt16(ddlSize.SelectedValue);
                        workRow1[0]["ElementId1"] = Convert.ToInt16(ddlElementA.SelectedValue);
                        workRow1[0]["UOMIdA1"] = Convert.ToInt16(ddlUOMA1.SelectedValue);
                        workRow1[0]["ValueA1"] = Convert.ToString(txtValueA1.Text.ToString());
                        workRow1[0]["UOMIdA2"] = Convert.ToInt16(ddlUOMA2.SelectedValue);
                        workRow1[0]["ValueA2"] = Convert.ToString(txtValueA2.Text.ToString());
                        workRow1[0]["ElementId2"] = Convert.ToInt16(ddlElementB.SelectedValue);
                        workRow1[0]["UOMIdB1"] = Convert.ToInt16(ddlUOMB1.SelectedValue);
                        workRow1[0]["ValueB1"] = Convert.ToString(txtValueB1.Text.ToString());
                        workRow1[0]["UOMIdB2"] = Convert.ToInt16(ddlUOMB2.SelectedValue);
                        workRow1[0]["ValueB2"] = Convert.ToString(txtValueB2.Text.ToString());
                        workRow1[0]["IsActive"] = 0;
                        workRow1[0]["CreatedBy"] = 1;
                        workRow1[0]["CreatedDate"] = Convert.ToDateTime(DateTime.Today.ToShortDateString());
                        workRow1[0]["ModifiedBy"] = 1;
                        workRow1[0]["ModifiedDate"] = Convert.ToDateTime(DateTime.Today.ToShortDateString());
                        #endregion
                    }
                    else
                    {
                        //Insert
                        DataRow workRow = dtInsert.NewRow();
                        // workRow["ProductAttributeManagementId"] = Row.RowIndex;
                        #region "Assign Values"
                        workRow["ProductSKUId"] = Convert.ToInt16(grdProductAttribute.DataKeys[grRow.RowIndex].Values[0].ToString());
                        workRow["SizeId"] = Convert.ToInt16(ddlSize.SelectedValue);
                        workRow["ElementId1"] = Convert.ToInt16(ddlElementA.SelectedValue);
                        workRow["UOMIdA1"] = Convert.ToInt16(ddlUOMA1.SelectedValue);
                        workRow["ValueA1"] = Convert.ToDecimal(txtValueA1.Text.ToString());
                        workRow["UOMIdA2"] = Convert.ToInt16(ddlUOMA2.SelectedValue);
                        workRow["ValueA2"] = Convert.ToDecimal(txtValueA2.Text.ToString());
                        workRow["ElementId2"] = Convert.ToInt16(ddlElementB.SelectedValue);
                        workRow["UOMIdB1"] = Convert.ToInt16(ddlUOMB1.SelectedValue);
                        workRow["ValueB1"] = Convert.ToDecimal(txtValueB1.Text.ToString());
                        workRow["UOMIdB2"] = Convert.ToInt16(ddlUOMB2.SelectedValue);
                        workRow["ValueB2"] = Convert.ToDecimal(txtValueB2.Text.ToString());
                        workRow["IsActive"] = 0;
                        workRow["CreatedBy"] = 1;
                        workRow["CreatedDate"] = Convert.ToDateTime(DateTime.Today.ToShortDateString());
                        workRow["ModifiedBy"] = 1;
                        workRow["ModifiedDate"] = Convert.ToDateTime(DateTime.Today.ToShortDateString());
                        #endregion

                        dtInsert.Rows.Add(workRow);
                    }
                }
            }
            catch (Exception) { }
            return dtInsert;
        }

        private DataTable SetPrimaryKey(DataTable dtPimary)
        {
            try
            {
                DataColumn[] columns = new DataColumn[1];
                columns[0] = dtPimary.Columns[0];
                dtPimary.PrimaryKey = columns;
                columns[0].AutoIncrement = true;

                if (dtPimary.Rows.Count > 0)
                {
                    columns[0].AutoIncrementSeed = Convert.ToInt32(dtPimary.Rows[dtPimary.Rows.Count - 1][0]) + 1;
                }
                else
                { columns[0].AutoIncrementSeed = 1; }
                columns[0].AutoIncrementStep = 1;
            }
            catch (Exception) { }
            return dtPimary;
        }



    }
}