﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;
using System.Text;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.DataAccess;

namespace Presentation
{
    public partial class Admin_Products_ProductSequence : BasePage//System.Web.UI.Page
    {
        #region Decalarations
        #region "Variables"
        protected global::System.Web.UI.HtmlControls.HtmlTableRow trPSCategory, trPCategory, trPSSCategory, trProduct;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMessage, divNoCategoryMessage;
        protected global::System.Web.UI.WebControls.Literal ltrMessage;
        protected global::System.Web.UI.WebControls.DropDownList ddlPParentCategory, ddlPSubCategory, ddlPSubSubCategory;
        protected global::System.Web.UI.WebControls.Button btnSaveExit, Submit;
        protected global::System.Web.UI.WebControls.TextBox txtFilter;
        protected global::System.Web.UI.WebControls.Repeater rptPrdducts;
        protected global::System.Web.UI.WebControls.HiddenField HIDPSelectedId, hdnSeq, hfRelatedCat;
        #endregion

        DataTable dtCategory;
        private CategoryBL ObjCategoryBL;

        public string Host = GlobalFunctions.GetVirtualPath();
        int CId = 0, SCId = 0, SSCId = 0;
        int UserId = 0;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindCategory();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void ddlPParentCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ////*************************************************************************
            //// Purpose : Event will get fired when product parent category get changed
            //// Inputs  : object sender, EventArgs e
            //// Return  : Nothing
            ////*************************************************************************

            CategoryBE objCategoryBE = new CategoryBE();
            List<CategoryBE> lstCategoryBE = new List<CategoryBE>();

            try
            {
                if (ddlPParentCategory.SelectedIndex > 0)
                {
                    lstCategoryBE = CategoryBL.GetAllCategories();
                    if (lstCategoryBE != null)
                    {
                        if (lstCategoryBE.Count > 0)
                        {
                            lstCategoryBE = lstCategoryBE.FindAll(x => x.ParentCategoryId == Convert.ToInt16(ddlPParentCategory.SelectedValue));
                            if (lstCategoryBE.Count > 0)
                            {
                                ddlPSubCategory.DataSource = lstCategoryBE;
                                ddlPSubCategory.DataTextField = "CategoryName";
                                ddlPSubCategory.DataValueField = "CategoryId";
                                ddlPSubCategory.DataBind();
                                ddlPSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));

                                Submit.Visible = false;
                                btnSaveExit.Visible = false;
                                trPSCategory.Visible = true;
                                trProduct.Visible = false;
                            }
                            else
                            {
                                Submit.Visible = true;
                                btnSaveExit.Visible = true;
                                trProduct.Visible = true;
                                trPSCategory.Visible = false;
                                Bind(ddlPParentCategory.SelectedItem.Value.ToString());
                            }
                        }
                        else
                        {
                            Submit.Visible = true;
                            btnSaveExit.Visible = true;
                            trProduct.Visible = true;
                            trPSSCategory.Visible = false;
                            Bind(ddlPParentCategory.SelectedItem.Value.ToString());
                        }
                    }
                    else
                    {
                        Submit.Visible = true;
                        btnSaveExit.Visible = true;
                        trProduct.Visible = true;
                        trPSSCategory.Visible = false;
                        Bind(ddlPParentCategory.SelectedItem.Value.ToString());
                    }
                }
                else
                {
                    trPSCategory.Visible = false;
                    trPSSCategory.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objCategoryBE = null;
                lstCategoryBE = null;
            }
        }

        protected void ddlPSubCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            CategoryBE objCategoryBE = new CategoryBE();
            List<CategoryBE> lstCategoryBE = new List<CategoryBE>();
            try
            {
                if (ddlPSubCategory.SelectedIndex > 0)
                {
                    lstCategoryBE = CategoryBL.GetAllCategories();
                    if (lstCategoryBE != null)
                    {
                        if (lstCategoryBE.Count > 0)
                        {
                            lstCategoryBE = lstCategoryBE.FindAll(x => x.ParentCategoryId == Convert.ToInt16(ddlPSubCategory.SelectedValue));
                            if (lstCategoryBE.Count > 0)
                            {
                                ddlPSubSubCategory.DataSource = lstCategoryBE;
                                ddlPSubSubCategory.DataTextField = "CategoryName";
                                ddlPSubSubCategory.DataValueField = "CategoryId";
                                ddlPSubSubCategory.DataBind();
                                ddlPSubSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));

                                Submit.Visible = false;
                                btnSaveExit.Visible = false;
                                trPSSCategory.Visible = false;
                                trProduct.Visible = false;
                            }
                            else
                            {
                                Submit.Visible = true;
                                btnSaveExit.Visible = true;
                                trProduct.Visible = true;
                                trPSSCategory.Visible = false;
                                Bind(ddlPSubCategory.SelectedItem.Value.ToString());
                            }
                        }
                        else
                        {
                            trPSSCategory.Visible = false;
                            Submit.Visible = false;
                            btnSaveExit.Visible = false;
                            trProduct.Visible = false;
                        }
                    }
                    else
                    {
                        trPSSCategory.Visible = false;
                        Submit.Visible = false;
                        btnSaveExit.Visible = false;
                        trPSSCategory.Visible = false;
                        trProduct.Visible = false;
                    }
                }
                else
                {
                    trPSSCategory.Visible = false;
                    Submit.Visible = false;
                    btnSaveExit.Visible = false;
                    trPSSCategory.Visible = false;
                    trProduct.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objCategoryBE = null;
                lstCategoryBE = null;
            }
        }

        protected void ddlPSubSubCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //*************************************************************************
            // Purpose : To submit the data.
            // Inputs  : object sender, EventArgs e
            // Return  : Nothing
            //*************************************************************************
            divMessage.Visible = false;
            ltrMessage.Text = "";
            CategoryBL objcatbl = new CategoryBL();
            try
            {
                int catID = 0, subCatID = 0, subSubCatID = 0;
                if (trPCategory.Visible == true && trPSCategory.Visible == true && trPSSCategory.Visible == true)
                {
                    catID = Convert.ToInt32(ddlPParentCategory.SelectedValue);
                    subCatID = Convert.ToInt32(ddlPSubCategory.SelectedValue);
                    subSubCatID = Convert.ToInt32(ddlPSubSubCategory.SelectedValue);
                }
                else if (trPCategory.Visible == true && trPSCategory.Visible == true && trPSSCategory.Visible == false)
                {
                    catID = Convert.ToInt32(ddlPParentCategory.SelectedValue);
                    subCatID = Convert.ToInt32(ddlPParentCategory.SelectedValue);
                    subSubCatID = Convert.ToInt32(ddlPSubCategory.SelectedValue);
                }
                else if (trPCategory.Visible == true && trPSCategory.Visible == false && trPSSCategory.Visible == false)
                {
                    catID = Convert.ToInt32(ddlPParentCategory.SelectedValue);
                    subCatID = Convert.ToInt32(ddlPParentCategory.SelectedValue);
                    subSubCatID = Convert.ToInt32(ddlPParentCategory.SelectedValue);
                }

                CategoryBE objCategoryBE = CategoryBL.GetCategoryDetails(catID, subCatID, subSubCatID);

                if (objCategoryBE != null)
                {
                    hfRelatedCat.Value = Convert.ToString(objCategoryBE.RelatedCategoryId);
                }

                DataTable dt = new DataTable();
                dt.Columns.Add("ProductId", typeof(Int32));
                dt.Columns.Add("RelatedCategoryId", typeof(Int32));
                dt.Columns.Add("Seq", typeof(Int32));

                if (hdnSeq.Value != "0")
                {
                    string[] pid = hdnSeq.Value.ToString().TrimEnd('|').Split('|');
                    for (int i = 0; i < pid.Length; i++)
                    {
                        dt.Rows.Add(Convert.ToInt32(pid[i]), Convert.ToInt32(hfRelatedCat.Value), Convert.ToInt32(i + 1));
                    }
                }
                bool ResultA = false, resOut = false;

                DataSet ds = new DataSet();
                ObjCategoryBL = new CategoryBL();
                resOut = CategoryBL.SetProductSequence(dt, ref ResultA, true, Constants.USP_SetProductSequence);
                //ObjCategoryBL.SetProductSequence(dt, ref ResultA);

                if (resOut != false)
                {
                    if (trPCategory.Visible == true && trPSCategory.Visible == true && trPSSCategory.Visible == false)
                    {
                        Bind(ddlPSubCategory.SelectedItem.Value.ToString());
                    }
                    if (trPCategory.Visible == true && trPSCategory.Visible == false && trPSSCategory.Visible == false)
                    {
                        Bind(ddlPParentCategory.SelectedItem.Value.ToString());
                    }
                    if (trPCategory.Visible == true && trPSCategory.Visible == true && trPSSCategory.Visible == true)
                    {
                        Bind(ddlPSubSubCategory.SelectedItem.Value.ToString());
                    }

                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/DataSaveMessage"), AlertType.Success);
                    CreateActivityLog("ProductSequence", "Update", ddlPParentCategory.SelectedValue + "_" + ddlPSubSubCategory.SelectedValue);

                    hdnSeq.Value = "0";
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/DataSaveMessage"), AlertType.Failure);


                }
            }
            catch (Exception ex)
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/DataSaveMessage"), AlertType.Failure);
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptPrdducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl liProducts = (HtmlGenericControl)e.Item.FindControl("liProducts");
                    int count = Convert.ToInt32(e.Item.ItemIndex) + 1;
                    liProducts.InnerText = count.ToString() + ". " + DataBinder.Eval(e.Item.DataItem, "ProductCode").ToString() + "-" + DataBinder.Eval(e.Item.DataItem, "ProductName").ToString();
                    liProducts.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "ProductId").ToString());
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void Bind(string Catid)
        {
            try
            {
                //DataSet ds = new DataSet();

                ProductBL ObjProductBL = new ProductBL();
                List<ProductBE> Products = ProductBL.GetProductDetailsByCategory_ProdSeq(Convert.ToInt16(Catid.ToString()), GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId());
                if (Products.Count > 0)
                {
                    var productss = (from p1 in Products
                                        select new { p1.ProductCode, p1.ProductId, p1.ProductName }).Distinct();
                     
                    trProduct.Visible = true;
                    rptPrdducts.DataSource = productss.Distinct();
                    rptPrdducts.DataBind();
                }
                else
                {
                    trProduct.Visible = false;
                    btnSaveExit.Visible = false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }

        }
        protected void BindCategory()
        {
            try
            {
                List<CategoryBE> Categories = new List<CategoryBE>();
                Categories = CategoryBL.GetAllCategories();
                if (Categories != null)
                {
                    Categories = Categories.Where(c => c.ParentCategoryId == 0).ToList();
                    ddlPParentCategory.DataSource = Categories;
                    ddlPParentCategory.DataTextField = "CategoryName";
                    ddlPParentCategory.DataValueField = "CategoryId";
                    ddlPParentCategory.DataBind();
                    ddlPParentCategory.Items.Insert(0, new ListItem("-Select-", "0"));

                    if (Categories.Count() > 0)
                    { trPCategory.Visible = true; }
                    else
                    { trPCategory.Visible = false; }

                    trPSCategory.Visible = false;
                    trPSSCategory.Visible = false;
                    trProduct.Visible = false;

                    HIDPSelectedId.Value = "";
                    Categories = null;
                    if (CId != 0)
                    {
                        if (SCId == 0 && SSCId == 0 && CId != 0)
                        {
                            trProduct.Visible = true;
                            ddlPParentCategory.ClearSelection();
                            ddlPParentCategory.SelectedValue = Convert.ToString(CId);
                        }
                        else
                        {
                            ddlPParentCategory_OnSelectedIndexChanged(new object(), new EventArgs());
                        }
                    }
                    //divNoCategoryMessage.Visible = false;
                }
                else
                {
                    //divNoCategoryMessage.Visible = true;
                    btnSaveExit.Visible = false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

    }
}