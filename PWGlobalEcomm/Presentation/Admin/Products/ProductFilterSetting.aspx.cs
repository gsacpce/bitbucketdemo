﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;

namespace Presentation
{
    public class Admin_Products_ProductFilterSetting : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptSettings;
        protected global::System.Web.UI.WebControls.Button btnSubmit;

        Int16 intLanguageId = 1;
        Int16 intCurrencyId = 2;
        protected string HostAdmin = GlobalFunctions.GetVirtualPathAdmin();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    List<ProductBE> lstProducts;
                    lstProducts = new List<ProductBE>();

                    lstProducts = ProductBL.GetProductListingFilterSettings(intLanguageId, intCurrencyId);
                    if (lstProducts != null && lstProducts.Count > 0)
                    {
                        rptSettings.DataSource = lstProducts;
                        rptSettings.DataBind();
                    }
                }
                this.Master.Page.Title = "Product listing filter settings";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string strSettings = string.Empty;

                for (int i = 0; i <= rptSettings.Items.Count - 1; i++)
                {

                    HtmlGenericControl spnName = (HtmlGenericControl)rptSettings.Items[i].FindControl("spnName");
                    HiddenField hdnProductDetailGroupMappingId = (HiddenField)rptSettings.Items[i].FindControl("hdnProductDetailGroupMappingId");
                    CheckBox chkIsShowInFilter = (CheckBox)rptSettings.Items[i].FindControl("chkIsShowInFilter");

                    strSettings += hdnProductDetailGroupMappingId.Value + "|" + spnName.InnerHtml.Trim() + "|" + chkIsShowInFilter.Checked + ";";
                }

                if (!string.IsNullOrEmpty(strSettings))
                {
                    List<ProductBE> lstProducts;
                    lstProducts = new List<ProductBE>();

                    lstProducts = ProductBL.SaveProductListingFilterSettings(strSettings);
                    //GlobalFunctions.ShowAlertMsg(Page, "Settings updated successfully.");
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Settings updated successfully.", AlertType.Success);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                List<object> objProductSearchBE = ProductBL.GetAllProductsForLucene();
                List<ProductSearchBE> lstProductSearchBE = new List<ProductSearchBE>();
                lstProductSearchBE = objProductSearchBE.Cast<ProductSearchBE>().ToList();

                if (lstProductSearchBE != null && lstProductSearchBE.Count > 0)
                {
                    ProductSearchBL.ClearLuceneIndex();
                    ProductSearchBL.AddUpdateLuceneIndex(lstProductSearchBE);
                    IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();
                    searchResults = ProductSearchBL.GetAllIndexRecords();
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Search index was created successfully! Total: " + lstProductSearchBE.Count + " Saved: " + searchResults.Count(), AlertType.Success);

                    //litResult.Text = "Search index was created successfully! Total: " + lstProductSearchBE.Count + " Saved: " + searchResults.Count();
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There is some error while indexing product data. Please try after sometime!", AlertType.Failure);
                    //litResult.Text = "There is some error. Please try after sometime!";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}