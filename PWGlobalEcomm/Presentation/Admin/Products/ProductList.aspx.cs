﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Products_ProductList : BasePage//System.Web.UI.Page
    {
        #region UI Controls
        protected global::System.Web.UI.WebControls.GridView grdProduct;
        protected global::System.Web.UI.WebControls.TextBox txtSearch;
        protected global::System.Web.UI.WebControls.HiddenField hdnproductname;
        protected global::System.Web.UI.WebControls.Literal ltrNoOfProducts;
        #endregion

        List<ProductBE> lstProductBE;
        public string linkCommand = string.Empty;
        public Int16 LanguageId = 1;
        public Int16 PageIndex;
        public Int16 PageSize = 30;

        protected void Page_Load(object sender, EventArgs e)
        {
            grdProduct.PageSize = PageSize;
            if (!IsPostBack)
            {
                PageIndex = 0;
                BindProduct(PageIndex, PageSize);
            }
        }

        protected void chkIsActive_CheckedChanged(object sender, EventArgs e)
        {
            ProductBE objProductBE=new ProductBE();
            try
            {
                bool result = false;
                CheckBox chk = (CheckBox)sender;
                objProductBE.ProductId = Convert.ToInt32(chk.Attributes["rel"]);
                objProductBE.IsActive = chk.Checked;
                result = ProductBL.UpdateProductStatus(objProductBE);
                if (result)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/UpdatedMessage"), AlertType.Success);
                    CreateActivityLog("ProductListing chkIsActive", "Update", Convert.ToString("Product Id =" + chk.Attributes["rel"]));
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/UpdatedMessage"), AlertType.Failure);
                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objProductBE=null;
            }
        }
        /// <summary>  
        /// Bind Product
        /// </summary>
        private void BindProduct(Int16 PageIndex,Int16 PageSize)
        {
            lstProductBE = new List<ProductBE>();
            try
            {
                lstProductBE = ProductBL.GetProductList<ProductBE>(LanguageId,PageIndex,PageSize);
                if (lstProductBE != null)
                {
                    if (lstProductBE.Count > 0)
                    {
                        ltrNoOfProducts.Text = lstProductBE[0].ProductCount.ToString();
                        grdProduct.DataSource = lstProductBE;
                        grdProduct.VirtualItemCount = lstProductBE[0].ProductCount;
                        grdProduct.DataBind();
                        Session["PRODUCTS"] = lstProductBE;
                    }
                    else
                    {
                        grdProduct.DataSource = null;
                        grdProduct.DataBind();
                        ltrNoOfProducts.Text = "0";
                    }
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        /// <summary>  
        /// Bind Product
        /// </summary>
        private void BindProduct(Int16 PageIndex,Int16 PageSize,string searchText)
        {
            lstProductBE = new List<ProductBE>();
            try
            {
                lstProductBE = ProductBL.GetProductList<ProductBE>(LanguageId,PageIndex,PageSize,searchText);
                if (lstProductBE != null)
                {
                    if (lstProductBE.Count > 0)
                    {
                        ltrNoOfProducts.Text = lstProductBE[0].ProductCount.ToString();
                        grdProduct.DataSource = lstProductBE;
                        grdProduct.VirtualItemCount = lstProductBE[0].ProductCount;
                        grdProduct.DataBind();
                        Session["PRODUCTS"] = lstProductBE;
                    }
                    else
                    {
                        grdProduct.DataSource = null;
                        grdProduct.DataBind();
                        ltrNoOfProducts.Text = "0";
                    }
                }
                else
                {
                    grdProduct.DataSource = null;
                    grdProduct.DataBind();
                    ltrNoOfProducts.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

       
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                {
                    BindProduct(PageIndex,PageSize,txtSearch.Text.Trim());
                    hdnproductname.Value = txtSearch.Text.Trim();
                }
                else
                {
                    BindProduct(PageIndex, PageSize);
                    hdnproductname.Value = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtSearch.Text = "";
                hdnproductname.Value = "";
                BindProduct(PageIndex, PageSize);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            
        }

        protected void grdProduct_PageIndexChanging(object sender, GridViewPageEventArgs e) 
        {
            grdProduct.PageIndex = e.NewPageIndex;
            PageIndex = Convert.ToInt16(e.NewPageIndex);
            if(!string.IsNullOrEmpty(hdnproductname.Value.Trim()))
            {
                BindProduct(PageIndex, PageSize,hdnproductname.Value.Trim());
            }
            else
            {
                BindProduct(PageIndex, PageSize);
            }
        }

        protected void grdProduct_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "Edit_Product":
                        Response.Redirect("AddEditProducts.aspx?mode=e&pid=" + int.Parse(e.CommandArgument.ToString()).ToString());
                        break;

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e) 
        {
            Response.Redirect("AddEditProducts.aspx?mode=a");
        }

        protected void grdProduct_OnSorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the list from the session object.
            lstProductBE = new List<ProductBE>();
            try
            {
                if(Session["PRODUCTS"] != null)
                {
                    lstProductBE = Session["PRODUCTS"] as List<ProductBE>;
                }
                else
                {
                    lstProductBE = null;
                }
                if(lstProductBE!=null)
                {
                    if(lstProductBE.Count>0)
                    {
                        
                            //Sort the data.
                        //    dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                        //lstProductBE.Sort(x=>x.)
                        //    grdProduct.DataSource = Session["PRODUCTS"];
                        //    grdProduct.DataBind();
                       

                        //linkCommand = e.SortExpression;
                        //SetSortDirection();
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }
            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        private void SetSortDirection()
        {
            if (linkCommand == "ProductCode")
            {
                if (this.ViewState["SortDirection"] == "ASC")
                {
                    grdProduct.HeaderRow.Cells[1].CssClass = "bgup";
                }
                else if (this.ViewState["SortDirection"] == "DESC")
                {
                    grdProduct.HeaderRow.Cells[1].CssClass = "bgdown";
                }
            }
            if (linkCommand == "ProductName")
            {
                if (this.ViewState["SortDirection"] == "ASC")
                {
                    grdProduct.HeaderRow.Cells[2].CssClass = "bgup";
                }
                else if (this.ViewState["SortDirection"] == "DESC")
                {
                    grdProduct.HeaderRow.Cells[2].CssClass = "bgdown";
                }
            }
        }



    }
}