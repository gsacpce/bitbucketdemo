﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Products_UpdateLuceneIndex : BasePage//System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                List<object> objProductSearchBE = ProductBL.GetAllProductsForLucene();
                List<ProductSearchBE> lstProductSearchBE = new List<ProductSearchBE>();
                lstProductSearchBE = objProductSearchBE.Cast<ProductSearchBE>().ToList();

                if (lstProductSearchBE != null && lstProductSearchBE.Count > 0)
                {
                    ProductSearchBL.ClearLuceneIndex();
                    ProductSearchBL.AddUpdateLuceneIndex(lstProductSearchBE);
                    IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();
                    searchResults = ProductSearchBL.GetAllIndexRecords();
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Search index was created successfully! Total: " + lstProductSearchBE.Count + " Saved: " + searchResults.Count(), AlertType.Success);
                    CreateActivityLog("UpdateLuceneIndex","Insert","");
                    Exceptions.WriteInfoLog("Search index was created successfully! Total: " + lstProductSearchBE.Count + " Saved: " + searchResults.Count());
                }
                else
                    Exceptions.WriteInfoLog("There is some error while indexing product data. Please try after sometime!");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}