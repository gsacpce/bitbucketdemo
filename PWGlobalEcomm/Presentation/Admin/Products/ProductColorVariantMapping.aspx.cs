﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Products_ProductColorVariantMapping : BasePage //System.Web.UI.Page
    {
        #region UI Controls
        protected global::System.Web.UI.WebControls.ListBox lstUnAssignedProducts, lstAssignedProducts;
        protected global::System.Web.UI.WebControls.HiddenField HIDUnAssignedProductIds, HIDAssignedProductIds, HidProductColorName;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgLeftToRight, imgRightToLeft;
        protected global::System.Web.UI.WebControls.Button btnSaveRelatedProducts;
        protected global::System.Web.UI.WebControls.DropDownList ddlProducts, ddlLanguage;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvProductAssignment;
        protected global::System.Web.UI.WebControls.TextBox txtProductColorGroupName;
        #endregion

        #region Declaration
        int ProductId;
        Int16 LanguageId = 1;
        ProductBE objProductBE;
        UserBE objUserBE;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["StoreUser"] != null)
                {
                    objUserBE = new UserBE();
                    objUserBE = Session["StoreUser"] as UserBE;
                }
                if (!IsPostBack)
                {
                    BindProducts();
                    BindLanguage();
                }
                ProductId = Convert.ToInt32(ddlProducts.SelectedValue);
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                BindAssignedProducts();
                imgLeftToRight.Attributes.Add("onmousedown", "MoveOption('" + lstUnAssignedProducts.ClientID + "','" + lstAssignedProducts.ClientID + "');");
                imgLeftToRight.Attributes.Add("onkeypress", "MoveOption('" + lstUnAssignedProducts.ClientID + "','" + lstAssignedProducts.ClientID + "');");
                imgRightToLeft.Attributes.Add("onmousedown", "MoveOption('" + lstAssignedProducts.ClientID + "','" + lstUnAssignedProducts.ClientID + "');");
                imgRightToLeft.Attributes.Add("onkeypress", "MoveOption('" + lstAssignedProducts.ClientID + "','" + lstUnAssignedProducts.ClientID + "');");
                btnSaveRelatedProducts.Attributes.Add("onmousedown", "StoreIds('" + lstUnAssignedProducts.ClientID + "','" + lstAssignedProducts.ClientID + "','" + HIDUnAssignedProductIds.ClientID + "','" + HIDAssignedProductIds.ClientID + "');");
                btnSaveRelatedProducts.Attributes.Add("onkeypress", "StoreIds('" + lstUnAssignedProducts.ClientID + "','" + lstAssignedProducts.ClientID + "','" + HIDUnAssignedProductIds.ClientID + "','" + HIDAssignedProductIds.ClientID + "');");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindLanguage()
        {
            try
            {
                StoreBE objStoreBE;
                objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE.StoreLanguages.Count > 0)
                {
                    ddlLanguage.DataSource = objStoreBE.StoreLanguages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                    ddlLanguage.SelectedValue = Convert.ToString(objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId);
                    LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                }
                else
                {
                    LanguageId = objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindProducts()
        {

            List<ProductBE> lstProductBE = new List<ProductBE>();
            try
            {
                lstProductBE = ProductBL.GetAllProductsData<ProductBE>(LanguageId);
                if (lstProductBE != null)
                {
                    if (lstProductBE.Count > 0)
                    {
                        //ddlProducts.DataSource = lstProductBE;
                        //ddlProducts.DataTextField = "ProductCode"+"ProductName";
                        //ddlProducts.DataValueField = "ProductId";
                        //ddlProducts.DataBind();
                        foreach (ProductBE item in lstProductBE)
                        {
                            ddlProducts.Items.Add(new ListItem(item.ProductCode + " - " + item.ProductName, Convert.ToString(item.ProductId)));
                        }
                        ddlProducts.Items.Insert(0, new ListItem("--Select--", "0"));

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                lstProductBE = null;
            }
        }

        private void BindAssignedProducts()
        {
            //*************************************************************************
            // Purpose : It will Bind products.
            // Inputs  : Nothing
            // Return  : Nothing
            //*************************************************************************
            List<ProductBE> lstProductBE = new List<ProductBE>();
            try
            {

                lstProductBE = ProductBL.GetAssignedProductColorVariantMapping(ProductId);

                lstAssignedProducts.Items.Clear();
                if (lstProductBE != null)
                {
                    if (lstProductBE.Count > 0)
                    {
                        lstAssignedProducts.DataSource = lstProductBE;
                        lstAssignedProducts.DataTextField = "ProductName";
                        lstAssignedProducts.DataValueField = "ProductId";
                        lstAssignedProducts.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindUnAssignedProducts()
        {
            //*************************************************************************
            // Purpose : It will Bind products.
            // Inputs  : Nothing
            // Return  : Nothing
            //*************************************************************************
            List<ProductBE> lstProductBE = new List<ProductBE>();
            try
            {
                lstProductBE = ProductBL.GetUnAssignedProductColorVariantMapping(ProductId);
                lstUnAssignedProducts.Items.Clear();
                if (lstProductBE != null)
                {
                    if (lstProductBE.Count > 0)
                    {
                        lstUnAssignedProducts.DataSource = lstProductBE;
                        lstUnAssignedProducts.DataTextField = "ProductName";
                        lstUnAssignedProducts.DataValueField = "ProductId";
                        lstUnAssignedProducts.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProducts.SelectedIndex != 0)
            {
                try
                {
                    ProductId = Convert.ToInt32(ddlProducts.SelectedValue);
                    HidProductColorName.Value = Convert.ToString(ddlProducts.SelectedItem);
                    dvProductAssignment.Visible = true;
                    txtProductColorGroupName.Text = "";
                    BindAssignedProducts();
                    BindUnAssignedProducts();
                    string strProductColorGroupName = ProductBL.GetProductColorGroupName(ProductId, LanguageId);
                    if (!string.IsNullOrEmpty(strProductColorGroupName))
                        txtProductColorGroupName.Text = strProductColorGroupName;
                    else
                        txtProductColorGroupName.Text = "";
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }

            }
            else
            {
                ProductId = 0;
                dvProductAssignment.Visible = false;
            }
        }
        protected void btnSaveRelated_Click(object sender, EventArgs e)
        {
            //*************************************************************************
            // Purpose : It will fired at the time of clicking on the btnSave button.
            // Inputs  : object sender, EventArgs e
            // Return  : Nothing
            //*************************************************************************
            try
            {
                if (lstUnAssignedProducts.Items.Count == 0 && lstAssignedProducts.Items.Count == 0)
                {
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('No item(s) Assigned/Unassigned');", true);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "No item(s) Assigned/Unassigned", AlertType.Warning);
                    return;
                }

                List<ProductBE> lstProductBE = new List<ProductBE>();
                lstProductBE = ProductBL.GetAssignedProductColorVariantMapping(ProductId);
                //var strOldArray = from c in lstProductBE
                //                  select ProductId;
                var strOldArray = lstProductBE.Select(m => m.ProductId).ToList();

                int result = 0;
                if (HIDAssignedProductIds.Value == "")
                {
                    HIDAssignedProductIds.Value = Convert.ToString(ProductId);
                }
                else
                {
                    HIDAssignedProductIds.Value = HIDAssignedProductIds.Value.Contains(Convert.ToString(ProductId)) ? HIDAssignedProductIds.Value : HIDAssignedProductIds.Value + "," + ProductId;
                    string[] strArr = HIDAssignedProductIds.Value.Split(',');
                    string parentProductID = strArr.FirstOrDefault(x => x.Equals(Convert.ToString(ProductId)));
                    if (string.IsNullOrEmpty(parentProductID)) { HIDAssignedProductIds.Value = HIDAssignedProductIds.Value + "," + Convert.ToString(ProductId); }
                }
                string ProductColorGroupName = txtProductColorGroupName.Text;

                var strNewArray1 = from t in HIDAssignedProductIds.Value.Split(',')
                                   select TryParseInt32(t) into x
                                   where x.HasValue
                                   select x.Value;
                var strNewArray = strNewArray1.ToList();

                string strSelfMapping = string.Empty;
                foreach (var a in strOldArray)
                {
                    string[] strArrq = HIDAssignedProductIds.Value.Split(',');
                    string parentProductID = strArrq.FirstOrDefault(x => x.Equals(Convert.ToString(a)));
                    if (string.IsNullOrEmpty(parentProductID))
                    {
                        strSelfMapping += a + ",";
                    }
                }
                if (!string.IsNullOrEmpty(strSelfMapping))
                {
                    strSelfMapping = strSelfMapping.TrimEnd(',');
                }
                //var NotSentMessages =
                //from msg in strOldArray
                //where !strNewArray.Any(x => x.MsgID == msg.MsgID)
                //select msg;
                //HIDAssignedProductIds.Value;

                result = ProductBL.InsertProductColorVariantMapping(ProductId, HIDAssignedProductIds.Value, ProductColorGroupName, objUserBE.UserId);
                if (result != 0)
                {
                    Int16 aRes = ProductBL.InsertProductColorVariantMappingSelf(ProductId, strSelfMapping, objUserBE.UserId, GlobalFunctions.GetLanguageId());
                    bool IsUpdated = ProductSearchBL.UpdateLucene();
                    if (IsUpdated)
                        Exceptions.WriteInfoLog("Search index was created successfully from ProductColorVariantMapping page!");
                    else
                        Exceptions.WriteInfoLog("There is some error while indexing product data. Please try after sometime from ProductColorVariantMapping page!");
                    BindUnAssignedProducts();
                    BindAssignedProducts();
                    CreateActivityLog("Product Color Varient", "Updated", Convert.ToString(HidProductColorName.Value));
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Exceptions.GetException("SuccessMessage/DataSaveMessage") + "');", true);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/DataSaveMessage"), AlertType.Success);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        public static int? TryParseInt32(string text)
        {
            int value;
            return int.TryParse(text, out value) ? value : (int?)null;
        }
        protected void btnUpdateProductGroupName_Click(object sender, EventArgs e)
        {
            try
            {
                int result = 0;
                string ProductColorGroupName = txtProductColorGroupName.Text;
                result = ProductBL.UpdateProductColorGroupName(ProductId, ProductColorGroupName, LanguageId);
                if (result != 0)
                {
                    BindUnAssignedProducts();
                    BindAssignedProducts();
                    CreateActivityLog("Product Color Varient", "Updated", Convert.ToString(HidProductColorName.Value));
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Exceptions.GetException("SuccessMessage/DataSaveMessage") + "');", true);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Product Color Group Name Updated Successfully", AlertType.Success);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                string strProductColorGroupName = ProductBL.GetProductColorGroupName(ProductId, LanguageId);
                if (!string.IsNullOrEmpty(strProductColorGroupName))
                    txtProductColorGroupName.Text = strProductColorGroupName;
                else
                    txtProductColorGroupName.Text = "";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnDeleteGroupName_Click(object sender, EventArgs e)
        {
            try
            {
                //"USP_DeleteGroupName"
                List<ProductBE> lstProductBE = new List<ProductBE>();
                lstProductBE = ProductBL.GetAssignedProductColorVariantMapping(ProductId);
                string strProductsID = HIDAssignedProductIds.Value;

                if (!string.IsNullOrEmpty(strProductsID)) { strProductsID = "," + strProductsID + ","; }
                foreach (ProductBE objProductBE in lstProductBE)
                {
                    if (!strProductsID.Contains("," + objProductBE.ProductId + ","))
                    {
                        strProductsID += objProductBE.ProductId + ",";
                    }
                }
                strProductsID = strProductsID.TrimStart(',');
                strProductsID = strProductsID.TrimEnd(',');

                if (!string.IsNullOrEmpty(strProductsID))
                {
                    bool res = ProductBL.UpdateColorVariantmapping(strProductsID, GlobalFunctions.GetLanguageId());
                    if (res)
                    {
                        bool IsUpdated = ProductSearchBL.UpdateLucene();
                        if (IsUpdated)
                        { Exceptions.WriteInfoLog("Search index was created successfully from ProductColorVariantMapping page!"); }
                        else
                        { Exceptions.WriteInfoLog("There is some error while indexing product data. Please try after sometime from ProductColorVariantMapping page!"); }
                        txtProductColorGroupName.Text = "";
                        CreateActivityLog("Product Color Varient", "Updated", Convert.ToString(HidProductColorName.Value));
                        BindAssignedProducts();
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Group Name deleted successfully.", AlertType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region comment
        #endregion
    }
}