﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Web.Services;
using AjaxControlToolkit;
using PWGlobalEcomm;
using PWGlobalEcomm.BusinessLogic;
using CKEditor;
using Microsoft.Security.Application;

namespace Presentation
{
    public partial class Admin_AddEditProducts : BasePage //System.Web.UI.Page
    {

        #region UIControls
        protected global::System.Web.UI.WebControls.TextBox txtProductCode;
        protected global::System.Web.UI.WebControls.TextBox txtProductName;
        protected global::System.Web.UI.WebControls.Label lblresult;
        protected global::CKEditor.NET.CKEditorControl txtProductDescription;
        //protected global::CKEditor.NET.CKEditorControl txtFurtherDescription;
        protected global::System.Web.UI.WebControls.TextBox txtGroupId, txtFurtherDescription;
        protected global::System.Web.UI.WebControls.TextBox txtCatalogueAlias;
        protected global::System.Web.UI.WebControls.TextBox txtCatalogueReference;
        protected global::System.Web.UI.WebControls.TextBox txtSupplierId;
        protected global::System.Web.UI.WebControls.TextBox txtTrimColorId;
        protected global::System.Web.UI.WebControls.TextBox txtBaseColorId;
        protected global::System.Web.UI.WebControls.TextBox txtBasysProductId;
        protected global::System.Web.UI.WebControls.TextBox txtVatCode;
        protected global::System.Web.UI.WebControls.TextBox txtDimensionalWeight;
        protected global::System.Web.UI.WebControls.TextBox txtGermanStock;
        protected global::System.Web.UI.WebControls.FileUpload fldPDF;
        protected global::System.Web.UI.WebControls.HyperLink hypPdf;
        protected global::System.Web.UI.WebControls.Button btnDeletePdf, btnSaveRelatedProducts, btnSave, btnSave1, btnCallForPrice;
        protected global::System.Web.UI.WebControls.TextBox txtImageAltTag;
        protected global::System.Web.UI.WebControls.TextBox txtVideoLink, txtProductVirtualSampleLink, txtTagAttributes, txtPageTitle, txtMetaKeyword, txtMetaDescription;
        protected global::System.Web.UI.WebControls.HiddenField hdnProductId, HIDRelatedCategoryIDs, hdnProductCode, HIDUnAssignedProductIds, HIDAssignedProductIds;
        protected global::System.Web.UI.WebControls.GridView grdCategories;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnProductName, divMessage, spnHiResInfoImage, spnHiResInfoImageManully, spnThumnailInfo, spnMediumInfo, spnLargeInfo, spnPDFInfo, DivDropDownProduct;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgLeftToRight, imgRightToLeft;
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox cbEnableStrikePrice;
        protected global::System.Web.UI.HtmlControls.HtmlTableRow trAddNewPrice, trIsregularPrice, trStrikePrice;
        protected global::AjaxControlToolkit.TabContainer tab_Product;
        protected global::AjaxControlToolkit.TabPanel tab_pan_PriceDetail;
        protected global::System.Web.UI.WebControls.DropDownList ddlCurrency, drpVariantType, ddlPParentCategory, ddlPSubCategory, ddlPSubSubCategory, ddlLanguage, ddlProducts;
        protected global::System.Web.UI.WebControls.Literal ltrPageHeading;
        protected global::System.Web.UI.WebControls.ListBox lstUnAssignedProducts, lstAssignedProducts;
        protected global::System.Web.UI.WebControls.GridView grdPriceBreak, gvProductSKU;
        protected global::System.Web.UI.WebControls.CheckBox cbIsRegular, chkIsAvtive, chkUserInput, chkMandatory;
        protected global::System.Web.UI.WebControls.TextBox txtPriceBreakName, txtFobPoints, txtStrikePriceBreakName, txtNote, txtProductionTime, txtMaximumQuantity, txtQuantityUnit, txtPriceUnit;
        protected global::System.Web.UI.WebControls.TextBox txtExpirationDate;
        protected global::System.Web.UI.WebControls.Literal ltrVariantTypeLabel;
        protected global::System.Web.UI.WebControls.TextBox txtVairantType, txtVairantName;
        protected global::System.Web.UI.WebControls.HiddenField hdnImageName, HIDSelectCatId;
        protected global::System.Web.UI.WebControls.RadioButton rbtnAuto, rbtnManually, rbPrice, rbCallForPrice;
        protected global::System.Web.UI.WebControls.FileUpload flupHiResAuto, fldLargeImage, fldMediumImage, fldTumbImage;
        protected global::System.Web.UI.WebControls.LinkButton lnkbtnDeleteLarge, lnkbtnDeleteMedium, lnkbtnDeleteThumb;
        protected global::System.Web.UI.WebControls.CheckBox chkShowVairant, chkDisplayEnquiryForm;
        protected global::System.Web.UI.WebControls.Repeater rptVariantsType;
        protected global::System.Web.UI.HtmlControls.HtmlTable tableManually, tableAutoResize, tblPriceTable;
        protected global::System.Web.UI.HtmlControls.HtmlTableRow trPSCategory, trPSSCategory;
        protected global::System.Web.UI.WebControls.CheckBoxList chkCatelogue;


        #endregion

        #region Declaration
        DataSet ds;
        ProductBE objProductBE;
        CurrencyBE objCurrencyBE;
        CategoryBL objCategoryBL = new CategoryBL();
        UserBE objUserBE;
        string ProductCode = "";
        int productId = 0;
        Int16 CurrencyId;
        Int16 LanguageId;
        string mode = "";
        string HiResImagePath = GlobalFunctions.GetHiResProductImagePath();
        string ThumbnailImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        string LargeImagePath = GlobalFunctions.GetLargeProductImagePath();
        string MediumImagePath = GlobalFunctions.GetMediumProductImagePath();
        string PDFFilePath = GlobalFunctions.GetPhysicalProductPdfsPath();
        string FileExtension = ".jpg";
        public string Host = "";
        public int countCategory = 0;
        public string AdminHost = GlobalFunctions.GetVirtualPathAdmin();
        int UserId;
        #endregion



        //protected void Page_PreRender(object sender, System.EventArgs e)
        //{

        //    //****************************************************
        //    //code below is to load Rich text editor
        //    StringBuilder strbldrRTEScript = null;
        //    strbldrRTEScript = new StringBuilder();
        //    strbldrRTEScript.Append("<script language='javascript' type='text/javascript'>");
        //    strbldrRTEScript.Append("tinyMCE.init({");


        //    //strbldrRTEScript.Append("mode : 'exact',");
        //    strbldrRTEScript.Append("mode : 'textareas',");
        //    strbldrRTEScript.Append("theme :'advanced',");
        //    strbldrRTEScript.Append("plugins : 'contextmenu,table,advhr,preview,advlink',");
        //    strbldrRTEScript.Append("theme_advanced_statusbar_location : 'none',");
        //    strbldrRTEScript.Append("theme_advanced_toolbar_location : 'top',");
        //    strbldrRTEScript.Append("theme_advanced_toolbar_align : 'left',");
        //    strbldrRTEScript.Append("theme_advanced_buttons1_add_before : 'bold,italic,underline,separator,formatselect,fontsizeselect,separator,justifyleft ,justifycenter ,justifyright, justifyfull, separator, bullist , numlist ',");
        //    //strbldrRTEScript.Append("theme_advanced_buttons2_add_before : 'indent,outdent,separator,anchor,image,link,unlink,separator,forecolor ,backcolor,separator, tablecontrols,advhr,preview,code',");
        //    strbldrRTEScript.Append("theme_advanced_disable : 'justifyleft,help,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,bold,italic,underline,strikethrough,bullist,numlist,outdent,indent,undo,redo,link,unlink,cleanup,anchor,code,removeformat,hr,table,sub,sup,visualaid,charmap,image',");
        //    strbldrRTEScript.Append("theme_advanced_resizing : false,");
        //    // strbldrRTEScript.Append("extended_valid_elements : 'a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]");
        //    strbldrRTEScript.Append("table_row_limit : 100,");
        //    strbldrRTEScript.Append("table_col_limit : 20,");
        //    strbldrRTEScript.Append("plugin_preview_width : '500',");
        //    strbldrRTEScript.Append("plugin_preview_height : '600'");
        //    strbldrRTEScript.Append("});");
        //    strbldrRTEScript.Append("</script>");

        //    Page.ClientScript.RegisterStartupScript(GetType(), "RTEScript", strbldrRTEScript.ToString());

        //    strbldrRTEScript = null;
        //    //****************************************************

        //}//end of Page_PreRender

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                mode = Convert.ToString(Request.QueryString["mode"]);
                Host = GlobalFunctions.GetVirtualPath();
                CreateTabs();
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Created By: Jaydeep C
        /// Reason: Show/Hide actions on CKE editor
        /// </summary>
        public void SetTextEidtorButtons()
        {
            txtProductDescription.config.toolbar = new object[]
			{
				//new object[] { "Source", "-", "Preview", "-", "Templates" },
				new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				new object[] { "NumberedList", "BulletedList"},
				new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				"/",
				new object[] { "Styles", "Format", "Font", "FontSize" },
				new object[] { "TextColor", "BGColor" },
				"/",
								new object[] { "Link", "Unlink"},
			};

            //txtFurtherDescription.config.toolbar = new object[]
            //{
            //    //new object[] { "Source", "-", "Preview", "-", "Templates" },
            //    new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
            //    new object[] { "NumberedList", "BulletedList"},
            //    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
            //    "/",
            //    new object[] { "Styles", "Format", "Font", "FontSize" },
            //    new object[] { "TextColor", "BGColor" },
            //    "/",
            //                    new object[] { "Link", "Unlink"},
            //};
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            mode = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.QueryString["mode"]));
            StoreBE objStoreBE = new StoreBE();
            if (Session["StoreUser"] != null)
            {
                objUserBE = new UserBE();
                objUserBE = Session["StoreUser"] as UserBE;
                UserId = objUserBE.UserId;
            }
            objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (!string.IsNullOrEmpty(objStoreBE.StoreName))
                {
                    Page.Title = "Product Management - " + objStoreBE.StoreName;
                }
                else
                {
                    Page.Title = "Product Management";
                }
            }
            else
            {
                Page.Title = "Product Management";
            }

            if (mode == "e")
            {

                //ds = objProductsBL.GetProductDetailGroups(Convert.ToInt32(Request.QueryString["pid"]));
                productId = Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(Request.QueryString["pid"]));
                hdnProductId.Value = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.QueryString["pid"]));
            }
            else
            {
                //ds = objProductsBL.GetProductDetailGroups(0);
            }
            CreateTabsContent();
            if (!IsPostBack)
            {
               
                BindCurrency();
                BindCatalogue();
                BindLanguage(objStoreBE);
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                if (mode == "e")
                {
                    ltrPageHeading.Text = "Edit Product";
                    BindProductBasicDetails(productId);
                    //BindProductColorGrid(productId);
                    BindProductPrice(productId, LanguageId, CurrencyId);
                    //BindProductDetails(productId);
                    BindCatgories(Convert.ToInt32(productId));
                    BindProductVairant();
                    BindHiResAndPdf();
                    BindProductSKU(productId);
                    //Related products starts
                    BindCategory();
                    BindAssignedProducts();
                    BindDropDownProducts();
                    imgLeftToRight.Attributes.Add("onmousedown", "MoveOption('" + lstUnAssignedProducts.ClientID + "','" + lstAssignedProducts.ClientID + "');");
                    imgLeftToRight.Attributes.Add("onkeypress", "MoveOption('" + lstUnAssignedProducts.ClientID + "','" + lstAssignedProducts.ClientID + "');");
                    imgRightToLeft.Attributes.Add("onmousedown", "MoveOption('" + lstAssignedProducts.ClientID + "','" + lstUnAssignedProducts.ClientID + "');");
                    imgRightToLeft.Attributes.Add("onkeypress", "MoveOption('" + lstAssignedProducts.ClientID + "','" + lstUnAssignedProducts.ClientID + "');");
                    btnSaveRelatedProducts.Attributes.Add("onmousedown", "StoreIds('" + lstUnAssignedProducts.ClientID + "','" + lstAssignedProducts.ClientID + "','" + HIDUnAssignedProductIds.ClientID + "','" + HIDAssignedProductIds.ClientID + "');");
                    btnSaveRelatedProducts.Attributes.Add("onkeypress", "StoreIds('" + lstUnAssignedProducts.ClientID + "','" + lstAssignedProducts.ClientID + "','" + HIDUnAssignedProductIds.ClientID + "','" + HIDAssignedProductIds.ClientID + "');");
                    //Related products end
                }
                if (mode == "a")
                {
                    DivDropDownProduct.Attributes.Add("style", "display:none");
                    BindCatgories(0);
                    BindProductDetailGroup();

                }

                BindVariantType();
                //BindImprintMethod();
                //BindChargesType();
                SetTextEidtorButtons();
                //BindProductCharges();
            }
            LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
            CurrencyId = Convert.ToInt16(ddlCurrency.SelectedValue);




            //PRODUCT IMAGES
            //spnHiResInfoImage.InnerText = "[Use Image file with >=" + Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_HIRES")) + " X " + Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_HIRES")) + " and <=" + GlobalFunctions.ConvertSizeFromBytes(Convert.ToInt64(GlobalFunctions.GetSetting("IMAGESIZE_PRODUCT_HIRES"))) + " size only]";
            //spnHiResInfoImageManully.InnerText = spnHiResInfoImage.InnerText;
            spnThumnailInfo.InnerText = "[Use Image file with >=" + Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_THUMB")) + " X " + Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_THUMB")) + " and <=" + GlobalFunctions.ConvertSizeFromBytes(Convert.ToInt64(GlobalFunctions.GetSetting("IMAGESIZE_PRODUCT_THUMB"))) + " size only png]";
            spnMediumInfo.InnerText = "[Use Image file with >=" + Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_MEDIUM")) + " X " + Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_MEDIUM")) + " and <=" + GlobalFunctions.ConvertSizeFromBytes(Convert.ToInt64(GlobalFunctions.GetSetting("IMAGESIZE_PRODUCT_MEDIUM"))) + " size only png]";
            spnLargeInfo.InnerText = "[Use Image file with >=" + Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_LARGE")) + " X " + Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_LARGE")) + " and <=" + GlobalFunctions.ConvertSizeFromBytes(Convert.ToInt64(GlobalFunctions.GetSetting("IMAGESIZE_PRODUCT_LARGE"))) + " size only png]";
            //PRODUCT PDF
            spnPDFInfo.InnerText = " [Size not more than " + GlobalFunctions.ConvertSizeFromBytes(Convert.ToInt64(GlobalFunctions.GetSetting("ProductPDFSize"))) + " ]";



        }
        private void CreateTabsContent()
        {
            if (Request.QueryString["mode"] == "a")
            {
                if (Convert.ToString(hdnProductId.Value) != string.Empty)
                {

                    tab_Product.Tabs[1].Enabled = false;
                    tab_Product.Tabs[2].Enabled = false;
                    tab_Product.Tabs[3].Enabled = false;
                    tab_Product.Tabs[4].Enabled = false;
                    tab_Product.Tabs[5].Enabled = false;
                    tab_Product.Tabs[6].Enabled = false;
                    tab_Product.Tabs[7].Enabled = false;
                }
                else
                {

                    tab_Product.Tabs[1].Enabled = false;
                    tab_Product.Tabs[2].Enabled = false;
                    tab_Product.Tabs[3].Enabled = false;
                    tab_Product.Tabs[4].Enabled = false;
                    tab_Product.Tabs[5].Enabled = false;
                    tab_Product.Tabs[6].Enabled = false;
                    tab_Product.Tabs[7].Enabled = false;
                }
            }

            if (Session["PRODUCTTAB"] != null)
            {
                tab_Product.ActiveTabIndex = Convert.ToInt32(Session["PRODUCTTAB"]);
                Session["PRODUCTTAB"] = null;
            }
        }

        private void BindLanguage(StoreBE objStoreBE)
        {
            if (objStoreBE.StoreLanguages.Count > 0)
            {
                ddlLanguage.DataSource = objStoreBE.StoreLanguages;
                ddlLanguage.DataTextField = "LanguageName";
                ddlLanguage.DataValueField = "LanguageId";
                ddlLanguage.DataBind();
                ddlLanguage.SelectedValue = Convert.ToString(objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId);
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
            }
            else
            {
                LanguageId = objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId;
            }


        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
            if (mode == "e")
            {
                ltrPageHeading.Text = "Edit Product";
                BindProductBasicDetails(productId);
                //BindProductColorGrid(productId);
                BindProductPrice(productId, LanguageId, CurrencyId);
                //BindProductDetails(productId);
                BindCatgories(Convert.ToInt32(productId));
                BindProductVairant();
                BindHiResAndPdf();

            }
            if (mode == "a")
            {
                BindCatgories(0);
                BindProductDetailGroup();

            }

            BindVariantType();
        }
        #region Product SKU

        private void BindProductSKU(int productId)
        {
            objProductBE = new ProductBE();
            try
            {
                objProductBE.PropGetAllProductSKU = ProductBL.GetAllProductSKU(productId);
                if (objProductBE.PropGetAllProductSKU != null)
                {
                    if (objProductBE.PropGetAllProductSKU.Count > 0)
                    {
                        gvProductSKU.DataSource = objProductBE.PropGetAllProductSKU;
                        gvProductSKU.DataBind();
                    }
                    else
                    {
                        objProductBE.PropProductSKU.ProductId = 0;
                        objProductBE.PropProductSKU.SKU = "Temp";
                        objProductBE.PropProductSKU.SKUName = "Temp";
                        objProductBE.PropGetAllProductSKU.Add(objProductBE.PropProductSKU);
                        gvProductSKU.DataSource = objProductBE.PropGetAllProductSKU;
                        gvProductSKU.DataBind();
                        gvProductSKU.Rows[0].Visible = false;
                    }
                }
                else
                {
                    objProductBE.PropProductSKU.ProductId = 0;
                    objProductBE.PropProductSKU.SKU = "Temp";
                    objProductBE.PropProductSKU.SKUName = "Temp";
                    objProductBE.PropGetAllProductSKU.Add(objProductBE.PropProductSKU);
                    gvProductSKU.DataSource = objProductBE.PropGetAllProductSKU;
                    gvProductSKU.DataBind();
                    gvProductSKU.Rows[0].Visible = false;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void gvProductSKU_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvProductSKU.EditIndex = e.NewEditIndex;
            BindProductSKU(productId);

        }

        protected void gvProductSKU_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvProductSKU.EditIndex = -1;
            BindProductSKU(productId);
        }

        protected void gvProductSKU_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                int ProductSKUId = Convert.ToInt32(gvProductSKU.DataKeys[e.RowIndex].Values["ProductSKUId"].ToString());
                TextBox txtProductSKUEdit = (TextBox)gvProductSKU.Rows[e.RowIndex].FindControl("txtProductSKUEdit");
                TextBox txtSKUNameEdit = (TextBox)gvProductSKU.Rows[e.RowIndex].FindControl("txtSKUNameEdit");
                TextBox txtVatCodeEdit = (TextBox)gvProductSKU.Rows[e.RowIndex].FindControl("txtVatCodeEdit");
                TextBox txtWeightEdit = (TextBox)gvProductSKU.Rows[e.RowIndex].FindControl("txtWeightEdit");
                TextBox txtMinOrderEdit = (TextBox)gvProductSKU.Rows[e.RowIndex].FindControl("txtMinOrderEdit");
                TextBox txtMaxOrderEdit = (TextBox)gvProductSKU.Rows[e.RowIndex].FindControl("txtMaxOrderEdit");
                objProductBE = new ProductBE();
                objProductBE.PropProductSKU.ProductSKUId = ProductSKUId;
                objProductBE.PropProductSKU.SKUName = Sanitizer.GetSafeHtmlFragment(txtSKUNameEdit.Text);
                objProductBE.PropProductSKU.SKU = Sanitizer.GetSafeHtmlFragment(txtProductSKUEdit.Text);
                objProductBE.PropProductSKU.VatCode = !string.IsNullOrEmpty(txtVatCodeEdit.Text) ? Sanitizer.GetSafeHtmlFragment(txtVatCodeEdit.Text) : "";
                objProductBE.PropProductSKU.DimensionalWeight = !string.IsNullOrEmpty(txtWeightEdit.Text) ? Convert.ToDecimal(Sanitizer.GetSafeHtmlFragment(txtWeightEdit.Text)) : 0;
                objProductBE.PropProductSKU.MinimumOrderQuantity = !string.IsNullOrEmpty(txtMinOrderEdit.Text) ? Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtMinOrderEdit.Text)) : 0;
                objProductBE.PropProductSKU.MaximumOrderQuantity = !string.IsNullOrEmpty(txtMaxOrderEdit.Text) ? Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtMaxOrderEdit.Text)) : 0;
                objProductBE.PropProductSKU.LanguageId = LanguageId;
                bool Result = ProductBL.AEDProductSKU(objProductBE.PropProductSKU, "E");

                if (Result)
                {
                    //lblresult.ForeColor = Color.Green;
                    //lblresult.Text = "Details updated successfully";
                    CreateActivityLog("Add / Edit Product Managment", "Updated", txtSKUNameEdit.Text);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Details updated successfully", AlertType.Success);
                }
                else
                {
                    //lblresult.ForeColor = Color.Red;
                    //lblresult.Text = "Error occured while updating. Please try again";
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating. Please try again", AlertType.Failure);
                }
                gvProductSKU.EditIndex = -1;
                BindProductSKU(productId);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }


        }

       

        protected void gvProductSKU_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int ProductSKUId = Convert.ToInt32(gvProductSKU.DataKeys[e.RowIndex].Values["ProductSKUId"].ToString());
                string SKUName = Convert.ToString(gvProductSKU.DataKeys[e.RowIndex].Values["SKUName"].ToString());
                objProductBE = new ProductBE();
                objProductBE.PropProductSKU.ProductSKUId = ProductSKUId;
                objProductBE.PropProductSKU.LanguageId = LanguageId;
                bool Result = ProductBL.AEDProductSKU(objProductBE.PropProductSKU, "D");

                if (Result)
                {
                    //lblresult.ForeColor = Color.Green;
                    //lblresult.Text = "Details deleted successfully";
                    CreateActivityLog("Add / Edit Product Management", "Deleted", SKUName);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Details deleted successfully", AlertType.Success);

                }
                else
                {
                    //lblresult.ForeColor = Color.Red;
                    //lblresult.Text = "Error occured while deleting. Please try again";
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating. Please try again", AlertType.Failure);
                }
                BindProductSKU(productId);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void gvProductSKU_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                try
                {
                    TextBox txtProductSKU = (TextBox)gvProductSKU.FooterRow.FindControl("txtProductSKU");
                    TextBox txtSKUName = (TextBox)gvProductSKU.FooterRow.FindControl("txtSKUName");
                    TextBox txtVatCode = (TextBox)gvProductSKU.FooterRow.FindControl("txtVatCode");
                    TextBox txtWeight = (TextBox)gvProductSKU.FooterRow.FindControl("txtWeight");
                    TextBox txtMinOrder = (TextBox)gvProductSKU.FooterRow.FindControl("txtMinOrder");
                    TextBox txtMaxOrder = (TextBox)gvProductSKU.FooterRow.FindControl("txtMaxOrder");
                    objProductBE = new ProductBE();
                    objProductBE.PropProductSKU.ProductId = productId;
                    objProductBE.PropProductSKU.SKU = Sanitizer.GetSafeHtmlFragment(txtProductSKU.Text);
                    objProductBE.PropProductSKU.SKUName = Sanitizer.GetSafeHtmlFragment(txtSKUName.Text);
                    objProductBE.PropProductSKU.VatCode = !string.IsNullOrEmpty(txtVatCode.Text) ? Sanitizer.GetSafeHtmlFragment(txtVatCode.Text) : "";
                    objProductBE.PropProductSKU.DimensionalWeight = !string.IsNullOrEmpty(txtWeight.Text) ? Convert.ToDecimal(Sanitizer.GetSafeHtmlFragment(txtWeight.Text)) : 0;
                    objProductBE.PropProductSKU.MinimumOrderQuantity = !string.IsNullOrEmpty(txtMinOrder.Text) ? Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtMinOrder.Text)) : 0;
                    objProductBE.PropProductSKU.MaximumOrderQuantity = !string.IsNullOrEmpty(txtMaxOrder.Text) ? Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtMaxOrder.Text)) : 0;
                    objProductBE.PropProductSKU.LanguageId = LanguageId;
                    bool Result = ProductBL.AEDProductSKU(objProductBE.PropProductSKU, "A");

                    if (Result)
                    {
                        //lblresult.ForeColor = Color.Green;
                        //lblresult.Text = "Details inserted successfully";
                        CreateActivityLog("Add / Edit Product Management", "Added", txtSKUName.Text);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Details inserted successfully", AlertType.Success);
                    }
                    else
                    {
                        //lblresult.ForeColor = Color.Red;
                        //lblresult.Text = "Error occured while inserting. Please try again";
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured while updating. Please try again", AlertType.Failure);
                    }
                    BindProductSKU(productId);

                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
        }
        #endregion

        #region Product Size Guide
        protected void btnSizeGuideAdd_Click(object sender, EventArgs e)
        {

        }

        protected void lnkbtnSizeGuideEdit_Command(object sender, CommandEventArgs e)
        {

        }

        protected void lnkbtnSizeGuideDelete_Command(object sender, CommandEventArgs e)
        {

        }

        #endregion

        private void BindCurrency()
        {
            List<CurrencyBE> lstCurrencyBE = new List<CurrencyBE>();
            try
            {
                lstCurrencyBE = CurrencyBL.GetAllCurrencyDetails();
                if (lstCurrencyBE != null && lstCurrencyBE.Count > 0)
                {
                    ddlCurrency.DataSource = lstCurrencyBE;
                    ddlCurrency.DataTextField = "CurrencyName";
                    ddlCurrency.DataValueField = "CurrencyId";
                    ddlCurrency.DataBind();
                    ddlCurrency.SelectedValue = Convert.ToString(lstCurrencyBE.FirstOrDefault(x => x.IsDefault == true).CurrencyId);
                    CurrencyId = Convert.ToInt16(ddlCurrency.SelectedValue);

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindCatalogue()
        {
            List<CatalogueBE.CataloguesID> lstCatalogueBE = new List<CatalogueBE.CataloguesID>();
            try
            {
                lstCatalogueBE = CatalogueBL.GetAllCatalogue();
                if(lstCatalogueBE!=null && lstCatalogueBE.Count>0)
                {
                    chkCatelogue.DataSource = lstCatalogueBE;
                    chkCatelogue.DataTextField = "CatalogueName";
                    chkCatelogue.DataValueField = "CatalogueId";
                    chkCatelogue.DataBind();

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Create dynamics Tabs 
        /// </summary>
        private void CreateTabs()
        {

        }

        protected void BindHiResAndPdf()
        {
            if (File.Exists(PDFFilePath + GlobalFunctions.RemoveSpecialCharacters(hdnProductCode.Value.ToString()) + ".pdf"))
            {
                hypPdf.NavigateUrl = GlobalFunctions.GetProductPdfsPath() + GlobalFunctions.RemoveSpecialCharacters(hdnProductCode.Value.ToString()) + ".pdf";
                hypPdf.Visible = true;
                btnDeletePdf.Visible = true;
            }
            else
            {
                hypPdf.Visible = false;
                btnDeletePdf.Visible = false;
            }
        }

        /// <summary>
        /// Creating dynamice tab content
        /// </summary>
        /// <param name="ds"></param>
        //private void CreateTabsContent(DataSet ds) { }

        /// <summary>
        /// To bind Product detail Group
        /// </summary>
        protected void BindProductDetailGroup() { }

        /// <summary>
        /// To bind all categories
        /// </summary>
        protected void BindCatgories(int productId)
        {
            //CategoryBE objCategoryBE = new CategoryBE();
            List<CategoryBE> CategoryBEInstance = new List<CategoryBE>();
            try
            {
                CategoryBEInstance = CategoryBL.GetCategroyTree(productId);
                if (CategoryBEInstance != null && CategoryBEInstance.Count > 0)
                {
                    countCategory = CategoryBEInstance.Count;
                    List<CategoryBE> CategoryBEInstanceFiltered = CategoryBEInstance.FindAll(x => x.ProductCategory == 1);
                    if (CategoryBEInstanceFiltered != null && CategoryBEInstanceFiltered.Count > 0)
                    {
                        string catids = "";
                        for (int i = 0; i < CategoryBEInstanceFiltered.Count; i++)
                        {
                            if (catids == "")
                            {
                                catids = CategoryBEInstanceFiltered[i].RelatedCategoryId.ToString() + ",";
                            }
                            else
                            {
                                catids = catids + CategoryBEInstanceFiltered[i].RelatedCategoryId.ToString() + ",";
                            }

                        }
                        HIDRelatedCategoryIDs.Value = catids;
                    }
                    grdCategories.DataSource = CategoryBEInstance;
                    grdCategories.DataBind();

                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        /// <summary>
        /// This will call on item data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdCategories_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "CategoryCount")) > 0)
                {
                    e.Row.BackColor = System.Drawing.Color.Gray;
                    e.Row.CssClass = "colorwhtit";
                    System.Web.UI.WebControls.CheckBox chkCategoryId = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("chkRelatedCategoryId");
                    System.Web.UI.WebControls.RadioButton chkDefault = (System.Web.UI.WebControls.RadioButton)e.Row.FindControl("chkDefault");
                    chkCategoryId.Visible = false;
                    chkDefault.Visible = false;
                }
            }
        }

        /// <summary>
        /// Bind Category name with product count
        /// </summary>
        /// <param name="CategoryName"></param>
        /// <param name="NoOfProducts"></param>
        /// <returns></returns>
        protected string BindCategoryWithProductCount(string CategoryName, string NoOfProducts)
        {
            return CategoryName + " (" + NoOfProducts + " products ) ";
        }

        /// <summary>
        /// To save product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveDetails_Click(object sender, EventArgs e) { }

        protected void btnCancelProdInfo_Click(object sender, EventArgs e) { }

        /// <summary>
        /// To Save Product code, name,desciption etc 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string[] strFileTypes = { ".pdf" };
            string[] strMimeTypes = { "application/pdf" };
            #region Validate Product Pdf Image
            if (Request.QueryString["mode"] != null)
            {
                if (Request.QueryString["mode"] == "a")
                {

                    if (ProductBL.CheckProductCodeExists(txtProductCode.Text.Trim().ToString()))
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Product Code already exists", AlertType.Failure);
                        SetFocus(txtProductCode.ClientID);
                        return;
                    }
                }
            }

            if (fldPDF.PostedFile.ContentLength > 0)
            {
                //if (!fldPDF.PostedFile.FileName.Contains(".pdf"))
                if (!GlobalFunctions.CheckFileExtension(fldPDF, strFileTypes))
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "Please use pdf file only for product PDF", AlertType.Warning);
                    SetFocus(fldPDF.ClientID);
                    return;
                }
                if (!GlobalFunctions.CheckFileMIMEType(fldPDF, strMimeTypes))
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "Please use valid content type only for product PDF", AlertType.Warning);
                    SetFocus(fldPDF.ClientID);
                    return;
                }
                if (GlobalFunctions.IsFileSizeValid(fldPDF, Convert.ToInt64(GlobalFunctions.GetSetting("ProductPDFSize"))) == false)
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "PDF file size is not valid", AlertType.Warning);
                    SetFocus(fldPDF.ClientID);
                    return;
                }


            }
            #endregion
            try
            {
                objProductBE = new ProductBE();
                int returnproductId = 0;

                if (Request.QueryString["mode"] != null)
                {
                    if (Request.QueryString["mode"] == "e")
                    {
                        objProductBE.ProductId = Convert.ToInt32(hdnProductId.Value);
                        objProductBE.Action = "E";//For Edit 
                        objProductBE.ModifiedBy = UserId;
                    }
                    else
                    {
                        objProductBE.Action = "A";//To Add
                        objProductBE.CreatedBy = UserId;
                    }
                }

                objProductBE.ProductCode = Convert.ToString(txtProductCode.Text.Trim());
                objProductBE.ProductName = Convert.ToString(txtProductName.Text.Trim());
                objProductBE.ProductDescription = Convert.ToString(txtProductDescription.Text.Trim());
                objProductBE.FurtherDescription = Convert.ToString(txtFurtherDescription.Text.Trim());
                objProductBE.PageTitle = Convert.ToString(txtPageTitle.Text.Trim());
                //if (txtCatalogPageNumber.Text.Trim() == string.Empty)
                //    objProductBE.CatalogPageNumber = null;
                //else
                //    objProductBE.CatalogPageNumber = Convert.ToInt32(txtCatalogPageNumber.Text.Trim().ToString());
                objProductBE.VideoLink = Convert.ToString(txtVideoLink.Text.Trim());
                objProductBE.IsActive = true;
                objProductBE.ProductVirtualSampleLink = Convert.ToString(txtProductVirtualSampleLink.Text.Trim());
                objProductBE.TagAttributes = Convert.ToString(txtTagAttributes.Text.Trim());
                objProductBE.MetaDescription = Convert.ToString(txtMetaDescription.Text.Trim());
                objProductBE.MetaKeyword = Convert.ToString(txtMetaKeyword.Text.Trim());
                objProductBE.ImageAltTag = Convert.ToString(txtImageAltTag.Text.Trim());
                objProductBE.LanguageId = LanguageId;
                objProductBE.IsUserInput = chkUserInput.Checked;// USER INPUT BY SNEHAL FOR INDEED
                objProductBE.IsMandatory = chkMandatory.Checked;// USER INPUT BY SNEHAL FOR INDEED

                string CheckedCatalogue = string.Empty;
                foreach(ListItem lstCat in chkCatelogue.Items)
                {
                    if (lstCat.Selected)
                    {
                        CheckedCatalogue += lstCat.Value + ",";
                    }
                }

                objProductBE.Catalogue = CheckedCatalogue.Trim(',');
                //objProductBE.GroupId = (!string.IsNullOrEmpty(txtGroupId.Text)) ? Convert.ToInt16(txtGroupId.Text.Trim()) : Convert.ToInt16(0);
                //objProductBE.CatalogueAlias = Convert.ToString(txtCatalogueAlias.Text.Trim());
                //objProductBE.CatalogueReference = Convert.ToString(txtCatalogueReference.Text.Trim());
                //objProductBE.SupplierId = (!string.IsNullOrEmpty(txtSupplierId.Text)) ? Convert.ToInt16(txtSupplierId.Text.Trim()) : Convert.ToInt16(0);
                //objProductBE.TrimColorId = (!string.IsNullOrEmpty(txtTrimColorId.Text)) ? Convert.ToInt16(txtTrimColorId.Text.Trim()) : Convert.ToInt16(0);
                //objProductBE.BaseColorId = (!string.IsNullOrEmpty(txtBaseColorId.Text)) ? Convert.ToInt16(txtBaseColorId.Text.Trim()) : Convert.ToInt16(0);
                //objProductBE.BasysProductId = (!string.IsNullOrEmpty(txtBasysProductId.Text)) ? Convert.ToInt32(txtBasysProductId.Text.Trim()) : 0;
                //objProductBE.VatCode = Convert.ToString(txtVatCode.Text.Trim());
                //objProductBE.DimensionalWeight = (!string.IsNullOrEmpty(txtDimensionalWeight.Text)) ? Convert.ToDecimal(txtDimensionalWeight.Text.Trim()) : Convert.ToDecimal(0.00);
                //objProductBE.GermanStock = (!string.IsNullOrEmpty(txtGermanStock.Text)) ? Convert.ToInt16(txtGermanStock.Text.Trim()) : Convert.ToInt16(0);

                   int MaxProductId = ProductBL.AEDProductBasicDetails(objProductBE, ref returnproductId);
                   CreateActivityLog("AddEditProducts", "Insert", txtProductCode.Text);
                if (MaxProductId > 0)
                {

                    #region Product PDF file
                    if (fldPDF.PostedFile.ContentLength > 0)
                    {
                        string[] strFileType = fldPDF.PostedFile.FileName.ToString().Split('.');
                        string targetFileName = PDFFilePath + GlobalFunctions.RemoveSpecialCharacters(hdnProductCode.Value) + "." + strFileType[1].ToString();
                        if (System.IO.File.Exists(targetFileName))
                            System.IO.File.Delete(targetFileName);
                        try
                        {
                            fldPDF.PostedFile.SaveAs(targetFileName);
                            fldPDF.PostedFile.InputStream.Dispose();

                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                        }
                        finally
                        {
                            strFileType = null;
                            targetFileName = "";
                        }
                    }
                    #endregion
                    spnProductName.InnerText = Convert.ToString(txtProductCode.Text.Trim().ToString()) + " - " + Convert.ToString(txtProductName.Text.Trim().ToString());
                    hdnProductCode.Value = Convert.ToString(txtProductCode.Text.Trim().ToString());
                    hdnProductId.Value = MaxProductId.ToString();
                    //Active All tabs
                    for (int i = 0; i < tab_Product.Tabs.Count; i++)
                    {
                        if (tab_Product.Tabs[i].Enabled == false)
                            tab_Product.Tabs[i].Enabled = true;
                    }
                    Button senderButton = sender as Button;
                    if (senderButton != null)
                    {
                        #region Save Categories
                        //if (HIDRelatedCategoryIDs.Value.ToString() != "")
                        //{
                        int DefaultCatId = 0;
                        foreach (GridViewRow item in grdCategories.Rows)
                        {
                            System.Web.UI.WebControls.RadioButton chkDefault = (System.Web.UI.WebControls.RadioButton)item.FindControl("chkDefault");
                            if (chkDefault.Checked)
                            {
                                DefaultCatId = Convert.ToInt32(chkDefault.Attributes["rel"]);
                            }
                        }

                        char[] delimiters = new char[] { ',' };
                        string[] seletedcat = HIDRelatedCategoryIDs.Value.ToString().TrimEnd(',').Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (seletedcat.Length > 0)
                        {
                            if (seletedcat.Length == 1 && DefaultCatId == 0)
                            {
                                DefaultCatId = Convert.ToInt32(seletedcat[0]);
                            }
                            else if (seletedcat.Length > 1 && !HIDRelatedCategoryIDs.Value.ToString().Contains(DefaultCatId + ","))
                            {
                                DefaultCatId = Convert.ToInt32(seletedcat[0]);
                            }
                            else if (seletedcat.Length == 1 && Convert.ToInt32(seletedcat[0]) != DefaultCatId && DefaultCatId != 0)
                            {
                                DefaultCatId = Convert.ToInt32(seletedcat[0]);
                            }
                        }

                        if (ProductBL.AssignProductCategories(HIDRelatedCategoryIDs.Value.ToString().TrimEnd(','), Convert.ToInt32(hdnProductId.Value), 1, DefaultCatId))//((LoginBO)HttpContext.Current.Session["UserSession"]).UserId
                        {
                            BindCatgories(Convert.ToInt32(hdnProductId.Value));
                        }
                        //}
                        #endregion

                        //#region Save Sections
                        //if (Convert.ToBoolean(hdnSectionUpdate.Value))
                        //{
                        //    if (hdnSectionIdsWeb.Value.ToString() != "" || hdnSectionIdsMobile.Value.ToString() != "")
                        //    {
                        //        if (objProductsBL.AssignProductSection(hdnSectionIdsWeb.Value.ToString().TrimEnd(','), hdnSectionIdsMobile.Value.ToString().TrimEnd(','), hdnSectionIdsTablet.Value.ToString().TrimEnd(','), Convert.ToInt32(hdnProductId.Value)))
                        //        {
                        //            BindSection(Convert.ToInt32(hdnProductId.Value));
                        //        }
                        //    }
                        //}
                        //#endregion

                        if (Convert.ToString(senderButton.CommandName) == "gotonextpanel")
                        {
                            Session["PRODUCTTAB"] = "1";
                            BindHiResAndPdf();
                            Response.Redirect("AddEditProducts.aspx?mode=e&pid=" + hdnProductId.Value);
                        }
                        else
                        {
                            if (Convert.ToString(senderButton.Text).ToString().ToLower() == "update")
                            {
                                BindHiResAndPdf();
                                GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/UpdatedMessage"), AlertType.Success);
                            }
                            else
                            {
                                BindHiResAndPdf();
                                GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/DataSaveMessage"), AlertType.Success);
                                Response.Redirect("AddEditProducts.aspx?mode=e&pid=" + hdnProductId.Value);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e) { }

        protected void btnSeo_Click(object sender, EventArgs e) { }

        protected Bitmap GetResizedImage(Bitmap source, int width, int height)
        {
            //This function creates the thumbnail image.
            //The logic is to create a blank image and to
            // draw the source image onto it

            Bitmap thumb = new Bitmap(width, height);
            Graphics gr = Graphics.FromImage(thumb);

            gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
            gr.SmoothingMode = SmoothingMode.HighQuality;
            gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
            gr.CompositingQuality = CompositingQuality.HighQuality;

            gr.DrawImage(source, 0, 0, width, height);
            return thumb;
        }

        protected void grdPriceBreak_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "Edit_Price":
                        Session["CWS_PRODUCTPRICEBREAKID"] = int.Parse(e.CommandArgument.ToString());
                        trAddNewPrice.Visible = true;
                        ClearPriceControls();
                        BindPriceBreakDetails(int.Parse(e.CommandArgument.ToString()));
                        break;
                    case "Delete_Price":
                        GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                        ImageButton imgbtnDelete = (ImageButton)gvRow.FindControl("imgbtnDelete");
                        if (Convert.ToBoolean(imgbtnDelete.Attributes["rel"]) && grdPriceBreak.Rows.Count > 1)
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please first assign another price break as Regular", AlertType.Warning);
                            return;
                        }
                        DeletePriceBreak(int.Parse(e.CommandArgument.ToString()));
                        break;

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }
        protected void grdPriceBreak_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkDefault = (CheckBox)e.Row.FindControl("chkDefault");
                    ImageButton imgbtnDelete = (ImageButton)e.Row.FindControl("imgbtnDelete");
                    if (chkDefault.Checked)
                    {
                        imgbtnDelete.Attributes.Add("rel", "True");
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void grdPriceBreak_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPriceBreak.PageIndex = e.NewPageIndex;
            if (Convert.ToString(Request.QueryString["pid"]) != string.Empty)
                BindProductPrice(Convert.ToInt32(Request.QueryString["pid"].ToString()), LanguageId, CurrencyId);
        }
        /// <summary>
        /// To delete Price break 
        /// </summary>
        /// <param name="priceBreakId"></param>
        protected void DeletePriceBreak(int priceBreakId)
        {
            try
            {
                ClearPriceControls();
                objProductBE = new ProductBE();
                int NewPriceBreakId = 0;
                objProductBE.PropPriceBreaks.PriceBreakId = priceBreakId;
                objProductBE.PropPriceBreaks.ProductId = hdnProductId.Value.To_Int32();
                objProductBE.PropPriceBreaks.CurrencyId = CurrencyId;
                objProductBE.PropPriceBreaks.Action = 'D';
                NewPriceBreakId = ProductBL.AEDProductPriceBreak(objProductBE, ref NewPriceBreakId);
                if (NewPriceBreakId > 0)
                {
                    BindProductPrice(hdnProductId.Value.To_Int32(), LanguageId, CurrencyId);
                    GlobalFunctions.ShowModalAlertMessages(Page, "Price break deleted successfully", AlertType.Success);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        /// <summary>
        /// SEO
        /// </summary>
        /// <param name="source"></param>
        /// <param name="which"></param>
        /// <returns></returns>
        private string ShowWordStatictics(string source, string which)
        {
            // For showing OUTPUT as HTML
            string txt = "Statictics";
            txt = "<table CellPadding='5' CellSpacing='0' padding ='1' border='1' style='font-family:verdana;font-size:11px;' >";
            if (which == "site")
                txt += "<tr><td colspan=3 bgcolor='#A4A4A4'>Site Staticstics:</td></tr>";
            else
                txt += "<tr><td colspan=3 bgcolor='#A4A4A4'>Text Staticstics:</td></tr>";

            //Word Doc
            object oMissing = System.Reflection.Missing.Value;
            //var oWord = new Application();
            //oWord.Visible = false;
            //var oDoc = oWord.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            string sourcetxt = source;
            //oDoc.Content.Text = sourcetxt;
            string strStat = "";

            //Words:    //Characters:   //Paragraphs:    //Sentences:   //Sentences per Paragraph: 

            //strStat = " Sentences:" + oDoc.Sentences.Count.ToString() + "<br><br>";

            strStat = strStat + " Characters with space:" + sourcetxt.Length + "<br>";
            txt += "<tr><td>Characters with space:</td><td>" + sourcetxt.Length + "</td><td>&nbsp;</td></tr>";

            //get all statistics
            //1. Words per Sentence             //2. Characters per Word             //3. Passive Sentences
            //4. Flesch Reading Ease             //5. Flesch-Kincaid Grade Level

            string info = "";
            //foreach (ReadabilityStatistic stat in oDoc.ReadabilityStatistics)
            //{
            //    strStat = strStat + " " + stat.Name + ":" + stat.Value + "\r\n";
            //    if (stat.Name == "Flesch-Kincaid Grade Level")
            //        info = "A grade level of 7 means that a seventh grader will be able to understand the text";
            //    else if (stat.Name == "Flesch Reading Ease")
            //        info = "A high score implies an easy text. 0 - 90.";
            //    else
            //        info = "Standands to compare will goes here";

            //    txt += "<tr><td>" + stat.Name + "</td><td>" + stat.Value + "</td><td>" + info + "</td></tr>";
            //}
            txt += "</table>";

            //HttpContext.Current.Response.Redirect(strStat);
            //Close all open obj

            object oFalse = false;
            //oDoc.Close(ref oFalse, ref oMissing, ref oMissing);
            //oDoc = null;
            //oWord.Quit();
            //System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWord);

            return txt;
        }

        /// <summary>
        /// To bind Product basic details
        /// </summary>
        /// <param name="productId"></param>
        protected void BindProductBasicDetails(int productId)
        {
            objProductBE = new ProductBE();
            try
            {
                objProductBE = ProductBL.GetProductDetailsAdmin(productId, LanguageId, CurrencyId);//Language Parameter to be added afterwards.
                if (objProductBE != null)
                {


                    txtProductCode.Text = Convert.ToString(objProductBE.ProductCode);
                    txtProductName.Text = Convert.ToString(objProductBE.ProductName);
                    txtProductDescription.Text = Convert.ToString(objProductBE.ProductDescription);
                    txtFurtherDescription.Text = Convert.ToString(objProductBE.FurtherDescription);
                    txtPageTitle.Text = Convert.ToString(objProductBE.PageTitle);
                    //txtCatalogPageNumber.Text = (Convert.ToString(ds.Tables[0].Rows[0]["CatalogPageNumber"]) == "0") ? "" : Convert.ToString(ds.Tables[0].Rows[0]["CatalogPageNumber"]);
                    txtVideoLink.Text = Convert.ToString(objProductBE.VideoLink);
                    txtProductVirtualSampleLink.Text = Convert.ToString(objProductBE.ProductVirtualSampleLink);

                    chkIsAvtive.Checked = Convert.ToBoolean(objProductBE.IsActive);
                    txtTagAttributes.Text = Convert.ToString(objProductBE.TagAttributes);
                    txtMetaDescription.Text = Convert.ToString(objProductBE.MetaDescription);
                    txtMetaKeyword.Text = Convert.ToString(objProductBE.MetaKeyword);

                    chkUserInput.Checked = Convert.ToBoolean(objProductBE.IsUserInput);// ADDED BY SNEHAL FOR INDEED
                    chkMandatory.Checked = Convert.ToBoolean(objProductBE.IsMandatory);// ADDED BY SNEHAL FOR INDEED


                    spnProductName.InnerText = Convert.ToString(objProductBE.ProductCode) + " - " + Convert.ToString(objProductBE.ProductName);
                    btnSave.Text = "Update";
                    btnSave1.Text = "Update";
                    ProductCode = Convert.ToString(objProductBE.ProductCode.Trim());
                    hdnProductCode.Value = Convert.ToString(objProductBE.ProductCode.Trim());
                    hdnProductId.Value = Convert.ToString(objProductBE.ProductId);
                    string Cat = Convert.ToString(objProductBE.CatalogueId_csv);
                    string[] value = Cat.Split(',');
                    int count=0;
                    for(int i=0;i<value.Length;i++)
                    {
                        for (int j = 0; j <= chkCatelogue.Items.Count - 1; j++)
                        {
                            if (chkCatelogue.Items[j].Value == value[i])
                            {
                                chkCatelogue.Items[j].Selected = true;
                               count++;
                            }
                        }
                    }
                    if(count>0)
                    {
                        chkCatelogue.Enabled = false;
                    }
 //                   int length = s.Length;
 //for (int i = 0; i <= s.Length - 1; i++)
 //{
 //string cntry = s[i];
 //for (int j = 0; j <= CheckBoxList2.Items.Count - 1; j++)
 //{
 //if (CheckBoxList2.Items[j].Text == s[i])
 //{
 //CheckBoxList2.Items[j].Selected = true;
 //break;
 //}
 //}
 //}
 //}




                    //BASYS Fields
                    //txtGroupId.Text = Convert.ToString(objProductBE.GroupId);
                    //txtSupplierId.Text = Convert.ToString(objProductBE.SupplierId);
                    //txtTrimColorId.Text = Convert.ToString(objProductBE.TrimColorId);
                    //txtVatCode.Text = Convert.ToString(objProductBE.VatCode);
                    //txtBaseColorId.Text = Convert.ToString(objProductBE.BaseColorId);
                    //txtBasysProductId.Text = Convert.ToString(objProductBE.BasysProductId);
                    //txtCatalogueAlias.Text = Convert.ToString(objProductBE.CatalogueAlias);
                    //txtCatalogueReference.Text = Convert.ToString(objProductBE.CatalogueReference);
                    //txtDimensionalWeight.Text = Convert.ToString(objProductBE.DimensionalWeight);
                    //txtGermanStock.Text = Convert.ToString(objProductBE.GermanStock);


                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        /// <summary>
        /// To bind Product Price
        /// </summary>
        /// <param name="productId"></param>
        protected void BindProductPrice(int ProductId, Int16 LanguageId, Int16 CurrencyId)
        {

            objProductBE = new ProductBE();
            objProductBE = ProductBL.GetProductDetailsAdmin(ProductId, LanguageId, CurrencyId);
            if (objProductBE != null)
            {
                if (objProductBE.PropGetAllPriceBreak != null && objProductBE.PropGetAllPriceBreak.Count > 0)
                {
                    //Chk for Call For Price. There is only one Price Break for Call for Price
                    if (objProductBE.PropGetAllPriceBreak[0].IsCallForPrice == true)
                    {
                        rbCallForPrice.Checked = true;
                        tblPriceTable.Visible = false;
                        btnCallForPrice.Visible = true;
                        grdPriceBreak.Visible = false;
                    }
                    else
                    {
                        rbPrice.Checked = true;
                        tblPriceTable.Visible = true;
                        btnCallForPrice.Visible = false;
                        grdPriceBreak.Visible = true;
                    }

                    Session["PriceBreakDataCHK"] = objProductBE.PropGetAllPriceBreak;
                    grdPriceBreak.DataSource = objProductBE.PropGetAllPriceBreak;
                    grdPriceBreak.DataBind();

                    if (objProductBE.PropGetAllPriceBreak[0].IsRegularPrice == true)
                    {
                        trIsregularPrice.Visible = false;
                    }
                    else
                    {
                        trIsregularPrice.Visible = true;
                    }

                }
                else
                {
                    grdPriceBreak.Visible = false;
                }
            }

        }

        protected void BindPriceBreakDetails(int pricebreakId)
        {
            objProductBE = new ProductBE();
            try
            {

                objProductBE = ProductBL.GetProductDetailsAdmin(hdnProductId.Value.To_Int32(), LanguageId, CurrencyId);
                if (objProductBE != null && objProductBE.PropGetAllPriceBreakDetails.Count > 0)
                {
                    objProductBE.PropGetAllPriceBreakDetails = objProductBE.PropGetAllPriceBreakDetails.FindAll(x => x.PriceBreakId == pricebreakId);
                    objProductBE.PropGetAllPriceBreak = objProductBE.PropGetAllPriceBreak.FindAll(x => x.PriceBreakId == pricebreakId);
                    if (objProductBE.PropGetAllPriceBreak.Count > 0)
                    {
                        txtPriceBreakName.Text = objProductBE.PropGetAllPriceBreak[0].PriceBreakName;
                        txtFobPoints.Text = objProductBE.PropGetAllPriceBreak[0].FOBPoints;
                        txtNote.Text = objProductBE.PropGetAllPriceBreak[0].NoteText;
                        txtProductionTime.Text = objProductBE.PropGetAllPriceBreak[0].ProductionTime;
                        txtStrikePriceBreakName.Text = objProductBE.PropGetAllPriceBreak[0].StrikePriceBreakName;
                        chkDisplayEnquiryForm.Checked = objProductBE.PropGetAllPriceBreak[0].IsShowPriceAndEnquiry;
                        if (!string.IsNullOrEmpty(Convert.ToString(objProductBE.PropGetAllPriceBreak[0].EnableStrikePrice)))
                        {
                            cbEnableStrikePrice.Checked = objProductBE.PropGetAllPriceBreak[0].EnableStrikePrice;
                            if (objProductBE.PropGetAllPriceBreak[0].EnableStrikePrice)
                                trStrikePrice.Attributes.Remove("style");
                            else
                            {
                                trStrikePrice.Attributes.Remove("style");
                                trStrikePrice.Attributes.Add("style", "display:none;");
                            }
                        }
                        else
                        {
                            cbEnableStrikePrice.Checked = false;
                            trStrikePrice.Attributes.Remove("style");
                            trStrikePrice.Attributes.Add("style", "display:none;");
                        }
                        txtMaximumQuantity.Text = Convert.ToString(objProductBE.PropGetAllPriceBreak[0].MaximumQuantity);
                        txtQuantityUnit.Text = objProductBE.PropGetAllPriceBreak[0].QuantityUnit;
                        txtPriceUnit.Text = objProductBE.PropGetAllPriceBreak[0].PriceUnit;
                        if (!string.IsNullOrEmpty(Convert.ToString(objProductBE.PropGetAllPriceBreak[0].ExpirationDate)))
                            txtExpirationDate.Text = Convert.ToDateTime(objProductBE.PropGetAllPriceBreak[0].ExpirationDate).ToShortDateString();  //Convert.ToDateTime(dt.Rows[0]["ExpirationDate"].ToString()).ToShortDateString();
                        else
                            txtExpirationDate.Text = "";

                        if (!string.IsNullOrEmpty(Convert.ToString(objProductBE.PropGetAllPriceBreak[0].IsRegularPrice)))
                            cbIsRegular.Checked = Convert.ToBoolean(objProductBE.PropGetAllPriceBreak[0].IsRegularPrice);
                        else
                            cbIsRegular.Checked = false;
                    }
                    if (objProductBE.PropGetAllPriceBreakDetails.Count > 0)
                    {

                        for (int i = 0; i < objProductBE.PropGetAllPriceBreakDetails.Count; i++)
                        {
                            TextBox txtBreak = (TextBox)tab_pan_PriceDetail.FindControl("txtBreak" + (i + 1));
                            TextBox txtBreakText = (TextBox)tab_pan_PriceDetail.FindControl("txtBreakText" + (i + 1));
                            TextBox txtStrikePrice = (TextBox)tab_pan_PriceDetail.FindControl("txtStrikePrice" + (i + 1));
                            HiddenField hdnPriceBreakDetailId = (HiddenField)tab_pan_PriceDetail.FindControl("hdnPriceBreakDetailId" + (i + 1));
                            TextBox txtDiscCode = (TextBox)tab_pan_PriceDetail.FindControl("txtDiscCode" + (i + 1));
                            TextBox txtPrice = (TextBox)tab_pan_PriceDetail.FindControl("txtPrice" + (i + 1));
                            TextBox txtQuantityText = (TextBox)tab_pan_PriceDetail.FindControl("txtQuantityText" + (i + 1));
                            //Added By Hardik "22/Sep/2016"		
                            TextBox txtBreakTill = (TextBox)tab_pan_PriceDetail.FindControl("txtBreakTill" + (i + 1));
                            //Added By Hardik "22/Sep/2016"		
                            txtBreakTill.Text = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i].BreakTill);
                            txtBreak.Text = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i].BreakFrom);
                          //  txtBreakText.Text = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i].BreakTill);
                            txtPrice.Text = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i].Price);
                            if (!string.IsNullOrEmpty(Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i].StrikePrice)))
                            {
                                if (Convert.ToDecimal(objProductBE.PropGetAllPriceBreakDetails[i].StrikePrice) == 0)
                                    txtStrikePrice.Text = "";
                                else
                                    txtStrikePrice.Text = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i].StrikePrice);
                            }
                            else
                                txtStrikePrice.Text = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i].StrikePrice);

                            //txtQuantityText.Text = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i]);
                            hdnPriceBreakDetailId.Value = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[i].PriceBreakDetailId);
                            txtDiscCode.Text = objProductBE.PropGetAllPriceBreakDetails[i].DiscountCode;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        /// <summary>
        /// To set default Price
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkDefaultPrice_OnCheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            if (!string.IsNullOrEmpty(chk.Attributes["rel"].ToString()))
            {
                bool Result = false;
                ProductBL.UpdateDefaultPrice(Convert.ToInt32(chk.Attributes["rel"]), Convert.ToInt32(hdnProductId.Value), CurrencyId, ref Result);
                if (Result)
                {
                    BindProductPrice(Convert.ToInt32(hdnProductId.Value), LanguageId, CurrencyId);
                    GlobalFunctions.ShowModalAlertMessages(Page, "Data saved successfully.", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "Data saved failed.", AlertType.Failure);
                }
            }
        }

        /// <summary>
        /// To bind product charges, details etc details
        /// </summary>
        /// <param name="productId"></param>
        protected void BindProductDetails(int productId) { }

        protected void txtProductCode_OnTextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ProductBL.CheckProductCodeExists(txtProductCode.Text.Trim().ToString()))
                {
                    divMessage.Attributes.Remove("class");
                    divMessage.Attributes.Add("class", "Errormsg");
                    divMessage.InnerHtml = "Product code already exists.";
                    divMessage.Visible = true;
                    txtProductCode.Focus();
                }
                else
                {
                    divMessage.Attributes.Remove("class");
                    divMessage.Attributes.Add("style", "display:none");
                    txtProductName.Focus();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        [WebMethod]
        public static bool CheckProductCodeExists(string ProductCode, string ProductId)
        {
            ProductBL objProductsBL = new ProductBL();
            bool IsExist = ProductBL.CheckProductCodeExists(ProductCode.Trim().ToString(), Convert.ToInt32(ProductId));
            return IsExist;
        }


        /// <summary>
        /// To go back product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductDetails.aspx");
        }


        /// <summary>
        /// To add New Price Break
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            ClearPriceControls();
        }

        /// <summary>
        /// To Add new Price Break
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSavePrice_Click(object sender, EventArgs e)
        {

            if (rbPrice.Checked == true)
            {
                //Code for Normal Price Start Here
                Page.Validate("ValidDiscount");
                if (Page.IsValid)
                {

                    objProductBE = new ProductBE();
                    objProductBE.PropGetAllPriceBreak = (List<ProductBE.PriceBreaks>)Session["PriceBreakDataCHK"];

                    if (objProductBE.PropGetAllPriceBreak != null)
                    {
                        if (objProductBE.PropGetAllPriceBreak.Count >= 10)
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "pricebreakchk", "javascript:alert('Price break should not be more than 10.')", true);
                            return;
                        }


                        try
                        {
                            //Chk if Price Type has been changed during edit, then delete existing Price Breaks and its details
                            if (mode == "e")
                            {
                                if (objProductBE.PropGetAllPriceBreak[0].IsCallForPrice != Convert.ToBoolean(Session["IsCallForPrice"]))
                                {
                                    bool result = ProductBL.DeleteProductPriceData(productId, CurrencyId);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            Exceptions.WriteInfoLog("Exception Occured while deleting Price Break and Price Break Details");
                        }
                    }


                    try
                    {
                        int PriceBreakId = 0;
                        objProductBE = new ProductBE();

                        if (grdPriceBreak.Rows.Count == 0)
                        {
                            cbIsRegular.Checked = true;
                        }
                        if (Session["CWS_PRODUCTPRICEBREAKID"] != null)
                        {
                            if (Session["CWS_PRODUCTPRICEBREAKID"] != "")
                            {
                                objProductBE.PropPriceBreaks.PriceBreakId = Convert.ToInt32(Session["CWS_PRODUCTPRICEBREAKID"]);
                                objProductBE.PropPriceBreaks.PriceBreakName = txtPriceBreakName.Text.ToString();
                                objProductBE.PropPriceBreaks.ProductId = hdnProductId.Value.To_Int32();
                                objProductBE.PropPriceBreaks.FOBPoints = txtFobPoints.Text.ToString();
                                objProductBE.PropPriceBreaks.EnableStrikePrice = cbEnableStrikePrice.Checked;
                                objProductBE.PropPriceBreaks.StrikePriceBreakName = txtStrikePriceBreakName.Text.Trim();
                                //objProductBE.PropPriceBreaks.QuantityText = "";
                                objProductBE.PropPriceBreaks.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                                objProductBE.PropPriceBreaks.NoteText = txtNote.Text.ToString();
                                objProductBE.PropPriceBreaks.ProductionTime = txtProductionTime.Text.ToString();
                                objProductBE.PropPriceBreaks.MaximumQuantity = string.IsNullOrEmpty(txtMaximumQuantity.Text) ? 0 : Convert.ToInt32(txtMaximumQuantity.Text);
                                objProductBE.PropPriceBreaks.QuantityUnit = txtQuantityUnit.Text.Trim();
                                objProductBE.PropPriceBreaks.PriceUnit = txtPriceUnit.Text.Trim();
                                //if (!String.IsNullOrEmpty(txtExpirationDate.Text))
                                objProductBE.PropPriceBreaks.ExpirationDate = txtExpirationDate.Text.Trim().Equals(string.Empty) ? Convert.ToDateTime("1900-01-01 00:00:00.000") : Convert.ToDateTime(txtExpirationDate.Text);
                                objProductBE.PropPriceBreaks.IsRegularPrice = cbIsRegular.Checked;
                                objProductBE.PropPriceBreaks.IsShowPriceAndEnquiry = chkDisplayEnquiryForm.Checked;
                                objProductBE.PropPriceBreaks.ModifiedBy = UserId;//((LoginBO)HttpContext.Current.Session["UserSession"]).UserId;
                                objProductBE.PropPriceBreaks.Action = 'E';
                                objProductBE.PropPriceBreaks.CurrencyId = CurrencyId;
                                objProductBE.PropPriceBreaks.IsCallForPrice = false;
                                PriceBreakId = ProductBL.AEDProductPriceBreak(objProductBE, ref PriceBreakId);

                                Session["CWS_PRODUCTPRICEBREAKID"] = "";
                                objProductBE = null;
                            }
                            else
                            {
                                objProductBE.PropPriceBreaks.PriceBreakName = txtPriceBreakName.Text.ToString();
                                objProductBE.PropPriceBreaks.ProductId = hdnProductId.Value.To_Int32();
                                objProductBE.PropPriceBreaks.FOBPoints = txtFobPoints.Text.ToString();
                                objProductBE.PropPriceBreaks.EnableStrikePrice = cbEnableStrikePrice.Checked;
                                objProductBE.PropPriceBreaks.StrikePriceBreakName = txtStrikePriceBreakName.Text.Trim();
                                //objProductBE.PropPriceBreaks.QuantityText = "";

                                objProductBE.PropPriceBreaks.NoteText = txtNote.Text.ToString();
                                objProductBE.PropPriceBreaks.ProductionTime = txtProductionTime.Text.ToString();
                                objProductBE.PropPriceBreaks.MaximumQuantity = string.IsNullOrEmpty(txtMaximumQuantity.Text) ? 0 : Convert.ToInt32(txtMaximumQuantity.Text);
                                objProductBE.PropPriceBreaks.QuantityUnit = txtQuantityUnit.Text.Trim();
                                objProductBE.PropPriceBreaks.PriceUnit = txtPriceUnit.Text.Trim();
                                //if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                                objProductBE.PropPriceBreaks.ExpirationDate = txtExpirationDate.Text.Trim().Equals(string.Empty) ? Convert.ToDateTime("1900-01-01 00:00:00.000") : Convert.ToDateTime(txtExpirationDate.Text);
                                objProductBE.PropPriceBreaks.IsRegularPrice = cbIsRegular.Checked;
                                objProductBE.PropPriceBreaks.IsShowPriceAndEnquiry = chkDisplayEnquiryForm.Checked;

                                objProductBE.PropPriceBreaks.CreatedBy = UserId;
                                objProductBE.PropPriceBreaks.Action = 'A';
                                objProductBE.PropPriceBreaks.CurrencyId = CurrencyId;
                                objProductBE.PropPriceBreaks.IsCallForPrice = false;
                                PriceBreakId = ProductBL.AEDProductPriceBreak(objProductBE, ref PriceBreakId);

                                objProductBE = null;

                            }
                        }
                        else
                        {
                            objProductBE.PropPriceBreaks.PriceBreakName = txtPriceBreakName.Text.ToString();
                            objProductBE.PropPriceBreaks.ProductId = hdnProductId.Value.To_Int32();
                            objProductBE.PropPriceBreaks.FOBPoints = txtFobPoints.Text.ToString();
                            objProductBE.PropPriceBreaks.EnableStrikePrice = cbEnableStrikePrice.Checked;
                            objProductBE.PropPriceBreaks.StrikePriceBreakName = txtStrikePriceBreakName.Text.Trim();
                            //objProductPriceBreakBO.QuantityText = txtQuantityText.Text.Trim();

                            objProductBE.PropPriceBreaks.NoteText = txtNote.Text.ToString();
                            objProductBE.PropPriceBreaks.ProductionTime = txtProductionTime.Text.ToString();
                            objProductBE.PropPriceBreaks.MaximumQuantity = string.IsNullOrEmpty(txtMaximumQuantity.Text) ? 0 : Convert.ToInt32(txtMaximumQuantity.Text);
                            objProductBE.PropPriceBreaks.QuantityUnit = txtQuantityUnit.Text.Trim();
                            objProductBE.PropPriceBreaks.PriceUnit = txtPriceUnit.Text.Trim();
                            //if (!String.IsNullOrEmpty(txtExpirationDate.Text))
                            objProductBE.PropPriceBreaks.ExpirationDate = txtExpirationDate.Text.Trim().Equals(string.Empty) ? Convert.ToDateTime("1900-01-01 00:00:00.000") : Convert.ToDateTime(txtExpirationDate.Text);
                            objProductBE.PropPriceBreaks.IsRegularPrice = cbIsRegular.Checked;
                            objProductBE.PropPriceBreaks.IsShowPriceAndEnquiry = chkDisplayEnquiryForm.Checked;

                            objProductBE.PropPriceBreaks.CreatedBy = UserId;
                            objProductBE.PropPriceBreaks.Action = 'A';
                            objProductBE.PropPriceBreaks.CurrencyId = CurrencyId;
                            objProductBE.PropPriceBreaks.IsCallForPrice = false;
                            PriceBreakId = ProductBL.AEDProductPriceBreak(objProductBE, ref PriceBreakId);

                            objProductBE = null;

                        }

                        if (PriceBreakId > 0)
                        {

                            objProductBE = new ProductBE();
                            int NewpricebreakdetailId = 0;
                            for (int i = 0; i < 10; i++)
                            {
                                HiddenField hdnPriceBreakDetailId = (HiddenField)tab_pan_PriceDetail.FindControl("hdnPriceBreakDetailId" + (i + 1));
                                if (i == 0)
                                {
                                    objProductBE.PropPriceBreakDetails.Action = 'D';
                                    objProductBE.PropPriceBreakDetails.PriceBreakId = Convert.ToInt32(PriceBreakId);
                                    if (hdnPriceBreakDetailId.Value.ToString() != "")
                                        objProductBE.PropPriceBreakDetails.PriceBreakDetailId = Convert.ToInt32(hdnPriceBreakDetailId.Value);
                                    else
                                        objProductBE.PropPriceBreakDetails.PriceBreakDetailId = 0;
                                    NewpricebreakdetailId = ProductBL.AEProductPriceBreakDetails(objProductBE, ref NewpricebreakdetailId);
                                    NewpricebreakdetailId = 0;
                                    objProductBE = new ProductBE();
                                }
                                TextBox txtBreak = (TextBox)tab_pan_PriceDetail.FindControl("txtBreak" + (i + 1));
                                //TextBox txtBreakText = (TextBox)tab_pan_PriceDetail.FindControl("txtBreakText" + (i + 1));

                                TextBox txtDiscCode = (TextBox)tab_pan_PriceDetail.FindControl("txtDiscCode" + (i + 1));
                                TextBox txtPrice = (TextBox)tab_pan_PriceDetail.FindControl("txtPrice" + (i + 1));
                                TextBox txtStrikePrice = (TextBox)tab_pan_PriceDetail.FindControl("txtStrikePrice" + (i + 1));
                                TextBox txtQuantityText = (TextBox)tab_pan_PriceDetail.FindControl("txtQuantityText" + (i + 1));
                                if (txtBreak.Text != "")
                                {

                                    objProductBE.PropPriceBreakDetails.Action = 'A';
                                    objProductBE.PropPriceBreakDetails.CreatedBy = UserId;
                                    objProductBE.PropPriceBreakDetails.BreakFrom = Convert.ToInt32(txtBreak.Text);

                                    if (i < 9)
                                    {
                                        //TextBox txtBreakTill = (TextBox)tab_pan_PriceDetail.FindControl("txtBreak" + (i + 2));
                                        TextBox txtBreakTill = (TextBox)tab_pan_PriceDetail.FindControl("txtBreakTill" + (i + 1));
                                        if (txtBreakTill.Text.ToString() != "")
                                            //objProductBE.PropPriceBreakDetails.BreakTill = Convert.ToInt32(Convert.ToInt32(txtBreakTill.Text) - 1); Edited By Hardik "22/Sep/2016" 
                                            objProductBE.PropPriceBreakDetails.BreakTill = Convert.ToInt32(Convert.ToInt32(txtBreakTill.Text));
                                        else
                                            objProductBE.PropPriceBreakDetails.BreakTill = Convert.ToInt32(txtBreak.Text);
                                    }
                                    else
                                    {
                                        objProductBE.PropPriceBreakDetails.BreakTill = Convert.ToInt32(txtBreak.Text);
                                    }

                                    if (hdnPriceBreakDetailId.Value.ToString() != "")
                                        objProductBE.PropPriceBreakDetails.PriceBreakDetailId = Convert.ToInt32(hdnPriceBreakDetailId.Value);
                                    else
                                        objProductBE.PropPriceBreakDetails.PriceBreakDetailId = 0;

                                    objProductBE.PropPriceBreakDetails.PriceBreakId = Convert.ToInt32(PriceBreakId);
                                    objProductBE.PropPriceBreakDetails.Price = Convert.ToDecimal(txtPrice.Text);
                                    if (txtStrikePrice.Text.Trim() == string.Empty)
                                        objProductBE.PropPriceBreakDetails.StrikePrice = 0;
                                    else
                                        objProductBE.PropPriceBreakDetails.StrikePrice = Convert.ToDecimal(txtStrikePrice.Text);
                                    objProductBE.PropPriceBreakDetails.DiscountCode = Convert.ToString(txtDiscCode.Text.ToUpper());
                                    objProductBE.PropPriceBreakDetails.IsActive = true;
                                    //objProductBE.PropPriceBreakDetails.QuantityText = txtQuantityText.Text.Trim();

                                    NewpricebreakdetailId = ProductBL.AEProductPriceBreakDetails(objProductBE, ref NewpricebreakdetailId);
                                }

                            }

                            Session["PriceBreakDataCHK"] = null;

                            BindProductPrice(hdnProductId.Value.To_Int32(), LanguageId, CurrencyId);
                            ClearPriceControls();
                            if (NewpricebreakdetailId > 0)
                            {
                                //ClearPriceControls();
                                Button btnSave = sender as Button;
                                if (btnSave != null)
                                {
                                    if (Convert.ToString(btnSave.CommandName) == "saveandcontinue")
                                    {
                                        tab_Product.ActiveTabIndex = 6;
                                    }
                                }
                                GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/DataSaveMessage"), AlertType.Success);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }

                }
            }
            else
            {
                //Code for Call For Price Start Here

                objProductBE = new ProductBE();
                objProductBE.PropGetAllPriceBreak = (List<ProductBE.PriceBreaks>)Session["PriceBreakDataCHK"];

                try
                {
                    //Chk if Price Type has been changed during edit, then delete existing Price Breaks and its details
                    if (mode == "e")
                    {
                        if (objProductBE.PropGetAllPriceBreak[0].IsCallForPrice != Convert.ToBoolean(Session["IsCallForPrice"]))
                        {
                            bool result = ProductBL.DeleteProductPriceData(productId, CurrencyId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                    Exceptions.WriteInfoLog("Exception Occured while deleting Price Break and Price Break Details");
                }

                try
                {

                    int PriceBreakId = 0;
                    objProductBE = new ProductBE();

                    objProductBE.PropPriceBreaks.PriceBreakName = "Call for price";
                    objProductBE.PropPriceBreaks.ProductId = hdnProductId.Value.To_Int32();
                    objProductBE.PropPriceBreaks.FOBPoints = "";
                    objProductBE.PropPriceBreaks.EnableStrikePrice = false;
                    objProductBE.PropPriceBreaks.StrikePriceBreakName = "";
                    //objProductPriceBreakBO.QuantityText = txtQuantityText.Text.Trim();

                    objProductBE.PropPriceBreaks.NoteText = "";
                    objProductBE.PropPriceBreaks.ProductionTime = "";
                    objProductBE.PropPriceBreaks.MaximumQuantity = 0;
                    objProductBE.PropPriceBreaks.QuantityUnit = "";
                    objProductBE.PropPriceBreaks.PriceUnit = "";
                    //if (!String.IsNullOrEmpty(txtExpirationDate.Text))
                    objProductBE.PropPriceBreaks.ExpirationDate = txtExpirationDate.Text.Trim().Equals(string.Empty) ? Convert.ToDateTime("1900-01-01 00:00:00.000") : Convert.ToDateTime(txtExpirationDate.Text);
                    objProductBE.PropPriceBreaks.IsRegularPrice = true;

                    objProductBE.PropPriceBreaks.CreatedBy = UserId;
                    objProductBE.PropPriceBreaks.Action = 'A';
                    objProductBE.PropPriceBreaks.CurrencyId = CurrencyId;
                    objProductBE.PropPriceBreaks.IsCallForPrice = true;
                    PriceBreakId = ProductBL.AEDProductPriceBreak(objProductBE, ref PriceBreakId);

                    objProductBE = null;

                    if (PriceBreakId > 0)
                    {

                        objProductBE = new ProductBE();
                        int NewpricebreakdetailId = 0;


                        objProductBE.PropPriceBreakDetails.PriceBreakId = Convert.ToInt32(PriceBreakId);
                        objProductBE.PropPriceBreakDetails.BreakFrom = 0;
                        objProductBE.PropPriceBreakDetails.BreakTill = 0;
                        objProductBE.PropPriceBreakDetails.Price = 0;
                        objProductBE.PropPriceBreakDetails.StrikePrice = 0;
                        objProductBE.PropPriceBreakDetails.IsActive = true;
                        objProductBE.PropPriceBreakDetails.CreatedBy = UserId;
                        objProductBE.PropPriceBreakDetails.Action = 'A';
                        NewpricebreakdetailId = ProductBL.AEProductPriceBreakDetails(objProductBE, ref NewpricebreakdetailId);

                        Session["PriceBreakDataCHK"] = null;
                        if (NewpricebreakdetailId > 0)
                        {
                            //ClearPriceControls();
                            Button btnSave = sender as Button;
                            if (btnSave != null)
                            {
                                if (Convert.ToString(btnSave.CommandName) == "saveandcontinue")
                                {
                                    tab_Product.ActiveTabIndex = 6;
                                }
                            }
                            GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/DataSaveMessage"), AlertType.Success);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }

            }
        }


        protected void rbPriceType_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPrice.Checked == true)
            {
                tblPriceTable.Visible = true;
                btnCallForPrice.Visible = false;
                Session["IsCallForPrice"] = "false";
            }
            else
            {
                tblPriceTable.Visible = false;
                btnCallForPrice.Visible = true;
                Session["IsCallForPrice"] = "true";
            }
        }



        /// <summary>
        /// Clear Price controls
        /// </summary>
        protected void ClearPriceControls()
        {
            txtFobPoints.Text = string.Empty;
            txtPriceBreakName.Text = string.Empty;
            txtNote.Text = string.Empty;
            txtProductionTime.Text = string.Empty;

            for (int i = 0; i < 10; i++)
            {
                TextBox txtBreak = (TextBox)tab_pan_PriceDetail.FindControl("txtBreak" + (i + 1));
                TextBox txtBreakText = (TextBox)tab_pan_PriceDetail.FindControl("txtBreakText" + (i + 1));
                HiddenField hdnPriceBreakDetailId = (HiddenField)tab_pan_PriceDetail.FindControl("hdnPriceBreakDetailId" + (i + 1));
                TextBox txtDiscCode = (TextBox)tab_pan_PriceDetail.FindControl("txtDiscCode" + (i + 1));
                TextBox txtPrice = (TextBox)tab_pan_PriceDetail.FindControl("txtPrice" + (i + 1));
                TextBox txtStrikePrice = (TextBox)tab_pan_PriceDetail.FindControl("txtStrikePrice" + (i + 1));
                TextBox txtQuantityText = (TextBox)tab_pan_PriceDetail.FindControl("txtQuantityText" + (i + 1));
                TextBox txtBreakTill = (TextBox)tab_pan_PriceDetail.FindControl("txtBreakTill" + (i + 1)); //Added By Hardik "22/Sep/2016"
                txtBreakTill.Text = string.Empty;  //Added By Hardik "22/Sep/2016"
                txtStrikePrice.Text = string.Empty;
                txtBreak.Text = string.Empty;
                txtBreakText.Text = string.Empty;
                txtDiscCode.Text = string.Empty;
                txtPrice.Text = string.Empty;
                txtQuantityText.Text = "";
                hdnPriceBreakDetailId.Value = string.Empty;
                // Session["CWS_PRODUCTPRICEBREAKID"] = "";

            }
            cbEnableStrikePrice.Checked = false;
            chkDisplayEnquiryForm.Checked = false;
            cbIsRegular.Checked = false;
            txtStrikePriceBreakName.Text = "";
            //txtQuantityText.Text = "";
            txtMaximumQuantity.Text = "";
            txtQuantityUnit.Text = "";
            txtPriceUnit.Text = "";
            txtExpirationDate.Text = "";
        }

        /// <summary>
        /// To submit Group fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmitGroupfeild_Click(object sender, EventArgs e) { }

        protected void btnSubmitfeild_Click(object sender, EventArgs e) { }

        protected void btnAddNewPrice_OnClick(object sender, EventArgs e)
        {
            ClearPriceControls();
            Session["CWS_PRODUCTPRICEBREAKID"] = null;
            trAddNewPrice.Visible = false;
        }

        /// <summary>
        /// To Get Imprint Method
        /// </summary>
        protected void BindImprintMethod() { }

        /// <summary>
        /// To Get Charges Type
        /// </summary>
        protected void BindChargesType() { }

        protected void drpImrpintMethod_OnSelectedIndexChanged(object sender, EventArgs e) { }

        protected void BindImprintLocations(string ImprintMethodMappingId) { }

        protected void drpImprintLocationCount_OnSelectedIndexChanged(object sender, EventArgs e) { }

        /// <summary>
        /// to add some empty records into datatable so that we can bind directly to datalist even there is no enough data
        /// </summary>
        /// <param name="intTotalRows"></param>
        /// <param name="dttbl"></param>
        private void AddEmptyRows(int intTotalRows, System.Data.DataTable dttbl) { }

        protected void BindImprintColors(CheckBoxList lstColors) { }
        #region Product Images
        protected void BindProductVairant()
        {
            Session["PRODUCT_VARIANTS"] = null;
            Session["PRODUCT_VARIANTS_TYPES"] = null;
            ClearImageControls();
            objProductBE = new ProductBE();
            try
            {
                objProductBE = ProductBL.GetProductVaiants(Convert.ToInt32(hdnProductId.Value), LanguageId);
                if (objProductBE != null)
                {
                    if (objProductBE.PropGetAllVariantType.Count > 0 && objProductBE.PropGetAllProductVariants.Count > 0)
                    {
                        rptVariantsType.Visible = true;
                        Session["PRODUCT_VARIANTS_TYPES"] = objProductBE.PropGetAllVariantType;
                        Session["PRODUCT_VARIANTS"] = objProductBE.PropGetAllProductVariants;

                        string DefaultImageName = "";

                        DefaultImageName = string.IsNullOrEmpty(Convert.ToString(objProductBE.DefaultImageName)) ? "" : objProductBE.DefaultImageName;


                        //rptVariantsType.DataSource = ds.Tables[0].DefaultView.ToTable(true, "ProductVariantTypeId", "ProductVariantTypeName", "ShowVariant", "ShowVariant1");
                        //rptVariantsType.DataBind();
                        rptVariantsType.DataSource = objProductBE.PropGetAllVariantType.GroupBy(x => x.ProductVariantTypeName).Select(x => x.First()).ToList();
                        rptVariantsType.DataBind();

                        for (int i = 0; i < rptVariantsType.Items.Count; i++)
                        {
                            Button btnDeleteVariant = (Button)rptVariantsType.Items[i].FindControl("btnDeleteVariant");
                            Label lblVairantType = (Label)rptVariantsType.Items[i].FindControl("lblVairantType");
                            HiddenField hdnVariantTypeId = (HiddenField)rptVariantsType.Items[i].FindControl("hdnVariantTypeId");
                            TextBox txtRenameVariantName = (TextBox)rptVariantsType.Items[i].FindControl("txtRenameVariantName");

                            System.Web.UI.HtmlControls.HtmlAnchor aRename = (System.Web.UI.HtmlControls.HtmlAnchor)rptVariantsType.Items[i].FindControl("aRename");
                            aRename.Attributes.Add("class", "renamevariant_" + hdnVariantTypeId.Value);

                            Button btnCancelVariantName = (Button)rptVariantsType.Items[i].FindControl("btnCancelVariantName");
                            btnCancelVariantName.Attributes.Add("class", "cancelvariant_" + hdnVariantTypeId.Value);

                            System.Web.UI.HtmlControls.HtmlGenericControl dRename = (System.Web.UI.HtmlControls.HtmlGenericControl)rptVariantsType.Items[i].FindControl("dRename");
                            dRename.Attributes.Add("class", "dRename_" + hdnVariantTypeId.Value);

                            System.Web.UI.HtmlControls.HtmlGenericControl dRenameTextbox = (System.Web.UI.HtmlControls.HtmlGenericControl)rptVariantsType.Items[i].FindControl("dRenameTextbox");
                            dRenameTextbox.Attributes.Add("class", "dRenameTextbox_" + hdnVariantTypeId.Value);

                            lblVairantType.Attributes.Add("class", "lblVairantType_" + hdnVariantTypeId.Value);

                            txtRenameVariantName.Attributes.Add("class", "txtRenameVariantName_" + hdnVariantTypeId.Value);

                            //ds.Tables[1].DefaultView.RowFilter = "ProductVariantTypeId = " + Convert.ToInt32(hdnVariantTypeId.Value);
                            //DataTable dt = new DataTable();
                            //dt = ds.Tables[1].DefaultView.ToTable();
                            ProductBE TempProductBE = new ProductBE();
                            TempProductBE.PropGetAllProductVariants = objProductBE.PropGetAllProductVariants.FindAll(x => x.ProductVariantTypeId == Convert.ToInt32(hdnVariantTypeId.Value));
                            if (TempProductBE.PropGetAllProductVariants.Count > 0)
                            {
                                Repeater rptVariants = (Repeater)rptVariantsType.Items[i].FindControl("rptVariants");
                                rptVariants.DataSource = TempProductBE.PropGetAllProductVariants;
                                rptVariants.DataBind();

                                for (int j = 0; j < rptVariants.Items.Count; j++)
                                {
                                    HiddenField hdnImageName = (HiddenField)rptVariants.Items[j].FindControl("hdnImageName");
                                    //System.Web.UI.WebControls.Image ImgHiRes = (System.Web.UI.WebControls.Image)rptVariants.Items[j].FindControl("ImgHiRes");
                                    System.Web.UI.WebControls.Image ImgLarge = (System.Web.UI.WebControls.Image)rptVariants.Items[j].FindControl("ImgLarge");
                                    System.Web.UI.WebControls.Image ImgMedium = (System.Web.UI.WebControls.Image)rptVariants.Items[j].FindControl("ImgMedium");
                                    System.Web.UI.WebControls.Image Imgthumbnail = (System.Web.UI.WebControls.Image)rptVariants.Items[j].FindControl("Imgthumbnail");

                                    //Label lblHiRes = (Label)rptVariants.Items[j].FindControl("lblHiRes");
                                    Label lblLarge = (Label)rptVariants.Items[j].FindControl("lblLarge");
                                    Label lblThumb = (Label)rptVariants.Items[j].FindControl("lblThumb");
                                    Label lblMedium = (Label)rptVariants.Items[j].FindControl("lblMedium");
                                    CheckBox chkDefault = (CheckBox)rptVariants.Items[j].FindControl("chkDefault");
                                    LinkButton lnkbtnDelete = (LinkButton)rptVariants.Items[j].FindControl("lnkbtnDelete");

                                    if (hdnImageName.Value.ToString().ToLower() == DefaultImageName.ToLower())
                                    {
                                        chkDefault.Checked = true;

                                        lnkbtnDelete.OnClientClick = "javascript: return confirm('Please assign another vairant as Default Image.')";
                                        lnkbtnDelete.Attributes.Add("rel", "True");
                                    }
                                    else
                                    {
                                        chkDefault.Checked = false;
                                        lnkbtnDelete.OnClientClick = "javascript: return confirm('Are you sure,you want to delete selected vairant.')";
                                        lnkbtnDelete.Attributes.Add("rel", "False");
                                    }
                                    //Hi-Res Image
                                    /*if (File.Exists(GlobalFunctions.GetPhysicalHiResProductImagePath() + hdnImageName.Value.ToString()))
                                    {
                                        ImgHiRes.ImageUrl = HiResImagePath.Replace("~/", "") + hdnImageName.Value.ToString();
                                        ImgHiRes.Attributes.Add("onmousedown", "PopUpPageWithDifferentName('" + ImgHiRes.ImageUrl + "','HiRes Image',700,700);");
                                    }
                                    else
                                    {
                                        ImgHiRes.Visible = false;
                                        lblHiRes.Visible = true;
                                    }*/

                                    //Large Image
                                    if (File.Exists(GlobalFunctions.GetPhysicalLargeProductImagePath() + hdnImageName.Value.ToString()))
                                    {
                                        ImgLarge.ImageUrl = LargeImagePath.Replace("~/", "") + hdnImageName.Value.ToString();
                                        ImgLarge.Attributes.Add("onmousedown", "PopUpPageWithDifferentName('" + ImgLarge.ImageUrl + "','Large Image',500,500);");
                                    }
                                    else
                                    {
                                        ImgLarge.Visible = false;
                                        lblLarge.Visible = true;
                                    }

                                    //Medium Image
                                    if (File.Exists(GlobalFunctions.GetPhysicalMediumProductImagePath() + hdnImageName.Value.ToString()))
                                    {
                                        ImgMedium.ImageUrl = MediumImagePath.Replace("~/", "") + hdnImageName.Value.ToString();
                                        ImgMedium.Attributes.Add("onmousedown", "PopUpPageWithDifferentName('" + ImgMedium.ImageUrl + "','Medium Image',300,300);");
                                    }
                                    else
                                    {
                                        ImgMedium.Visible = false;
                                        lblMedium.Visible = true;
                                    }

                                    //Thumbnail Image
                                    if (File.Exists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + hdnImageName.Value.ToString()))
                                    {
                                        Imgthumbnail.ImageUrl = ThumbnailImagePath.Replace("~/", "") + hdnImageName.Value.ToString();
                                        Imgthumbnail.Attributes.Add("onmousedown", "PopUpPageWithDifferentName('" + Imgthumbnail.ImageUrl + "','Thumbnail Image');");
                                    }
                                    else
                                    {
                                        Imgthumbnail.Visible = false;
                                        lblThumb.Visible = true;
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        rptVariantsType.Visible = false;
                    }
                }
                else
                {
                    rptVariantsType.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void BindVariantType()
        {
            objProductBE = new ProductBE();
            objProductBE = ProductBL.GetProductVaiantType(LanguageId);//Land Id needs to be added
            if (objProductBE != null && objProductBE.PropGetAllVariantType != null)
            {
                if (objProductBE.PropGetAllVariantType.Count > 0)
                {
                    drpVariantType.DataSource = objProductBE.PropGetAllVariantType;
                    drpVariantType.DataValueField = "ProductVariantTypeId";
                    drpVariantType.DataTextField = "ProductVariantTypeName";
                    drpVariantType.DataBind();

                    if (objProductBE.PropGetAllVariantType.Count < 5)
                        drpVariantType.Items.Insert(objProductBE.PropGetAllVariantType.Count, new ListItem("Add New Variant Type", "0"));

                    //ltrVariantTypeLabel.Text = "Select Variant Type";
                    txtVairantType.Attributes.Add("style", "display:none");
                }
                else
                {
                    drpVariantType.Items.Insert(objProductBE.PropGetAllVariantType.Count, new ListItem("Add New Variant Type", "0"));
                }
            }
            else
            {
                drpVariantType.Items.Insert(objProductBE.PropGetAllVariantType.Count, new ListItem("Add New Variant Type", "0"));
            }
        }

        protected void btnSaveAddMoreImage_OnClick(object sender, EventArgs e)
        {
            string filename = "";
            string[] strFileTypes = { ".jpg", ".jpeg", ".png" };
            string[] strMimeTypes = { "image/jpeg", "image/png" };
            objProductBE = new ProductBE();
            string fileExt = "";
            try
            {
                if (Request.QueryString["mode"] == "a")
                {
                    filename = GlobalFunctions.RemoveSpecialCharacters(hdnProductCode.Value.ToString().Trim().Replace(" - ", "_").Replace("-", "_").ToString()) + "_" + GlobalFunctions.RemoveSpecialCharacters(txtVairantName.Text.ToString()) + drpVariantType.SelectedItem.Value;
                }
                else if (Request.QueryString["mode"] == "e")
                {
                    if (hdnImageName.Value.ToString() != "")
                    {
                        string[] strFileType = hdnImageName.Value.ToString().Split('.');
                        filename = strFileType[0].ToString();
                    }
                    else
                    {
                        filename = GlobalFunctions.RemoveSpecialCharacters(hdnProductCode.Value.ToString().Trim().Replace(" - ", "_").Replace("-", "_").ToString()) + "_" + GlobalFunctions.RemoveSpecialCharacters(txtVairantName.Text.ToString());
                    }
                }

                if (drpVariantType.SelectedValue.ToString() != "0")
                {
                    if (Session["PRODUCT_VARIANTS"] != null)
                    {
                        objProductBE.PropGetAllProductVariants = (List<ProductBE.ProductVariants>)Session["PRODUCT_VARIANTS"];
                        objProductBE.PropGetAllProductVariants = objProductBE.PropGetAllProductVariants.FindAll(x => x.ProductVariantTypeId == Convert.ToInt16(drpVariantType.SelectedValue));
                        if (objProductBE != null && objProductBE.PropGetAllProductVariants.Count > 0)
                        {
                            for (int i = 0; i < objProductBE.PropGetAllProductVariants.Count; i++)
                            {
                                if (Session["VARIANTID"] != null)
                                {
                                    if (Session["VARIANTID"].ToString() == objProductBE.PropGetAllProductVariants[i].ProductVariantId.ToString()) //dr[i]["ProductVariantId"].ToString())
                                    {

                                    }
                                    else if (txtVairantName.Text.Trim().ToLower() == objProductBE.PropGetAllProductVariants[i].ProductVariantName.ToString().ToLower())
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(Page, "Variant name already exists", AlertType.Warning);
                                        SetFocus(txtVairantName.ClientID);
                                        return;
                                    }
                                }
                                else if (txtVairantName.Text.Trim().ToLower() == objProductBE.PropGetAllProductVariants[i].ProductVariantName.ToString().ToLower())
                                {
                                    GlobalFunctions.ShowModalAlertMessages(Page, "Variant name already exists", AlertType.Warning);
                                    SetFocus(txtVairantName.ClientID);
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (Session["PRODUCT_VARIANTS_TYPES"] != null)
                    {
                        objProductBE.PropGetAllVariantType = (List<ProductBE.ProductVariantType>)Session["PRODUCT_VARIANTS_TYPES"];
                        if (objProductBE != null && objProductBE.PropGetAllVariantType.Count > 0)
                        {
                            for (int i = 0; i < objProductBE.PropGetAllVariantType.Count; i++)
                            {
                                if (txtVairantType.Text.Trim().ToLower() == objProductBE.PropGetAllVariantType[i].ProductVariantTypeName.ToString().Trim().ToLower() && Session["VARIANTID"].ToString() == null)
                                {
                                    GlobalFunctions.ShowModalAlertMessages(Page, "Variant type already exists", AlertType.Warning);
                                    SetFocus(txtVairantType.ClientID);
                                    return;
                                }
                            }
                        }
                    }
                }

                if (rbtnAuto.Checked)
                {
                    #region VALIDATE IMAGE
                    if (Session["VARIANTID"] == null)
                    {
                        if (!flupHiResAuto.HasFile)
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please upload Hi-Res Image", AlertType.Failure);
                            SetFocus(flupHiResAuto.ClientID);
                            return;
                        }
                    }
                    //if (!flupHiResAuto.PostedFile.FileName.Contains(".jpg"))
                    if (!GlobalFunctions.CheckFileExtension(flupHiResAuto, strFileTypes))
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Please use JPG or Png Image only for Image File", AlertType.Failure);
                        SetFocus(flupHiResAuto.ClientID);
                        return;
                    }
                    if (!GlobalFunctions.CheckFileMIMEType(flupHiResAuto, strMimeTypes))
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Please use valid content type for Image File", AlertType.Failure);
                        SetFocus(flupHiResAuto.ClientID);
                        return;
                    }
                    if (GlobalFunctions.IsImageDimensionGreaterValid(flupHiResAuto, Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_HIRES")), Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_HIRES"))) == false)
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Image file dimensions are not valid", AlertType.Failure);
                        SetFocus(flupHiResAuto.ClientID);
                        return;
                    }
                    if (GlobalFunctions.IsFileSizeValid(flupHiResAuto, Convert.ToInt64(GlobalFunctions.GetSetting("IMAGESIZE_PRODUCT_HIRES"))) == false)
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Image file size is not valid", AlertType.Failure);
                        SetFocus(flupHiResAuto.ClientID);
                        return;
                    }

                    fileExt = System.IO.Path.GetExtension(flupHiResAuto.PostedFile.FileName);
                    #endregion
                }
                else
                {

                    #region Validate Large Image
                    if (lnkbtnDeleteLarge.Visible == false)
                    {
                        if (!fldLargeImage.HasFile)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please upload Large Image", AlertType.Failure);
                            SetFocus(fldLargeImage.ClientID);
                            return;
                        }
                    }
                    if (fldLargeImage.PostedFile.ContentLength > 0)
                    {
                        //if (!fldLargeImage.PostedFile.FileName.Contains(".jpg"))
                        if (!GlobalFunctions.CheckFileExtension(fldLargeImage, strFileTypes))
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please use JPG or Png Image only for Large File", AlertType.Failure);
                            SetFocus(fldLargeImage.ClientID);
                            return;
                        }
                        if (!GlobalFunctions.CheckFileMIMEType(fldLargeImage, strMimeTypes))
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please use valid content type for Image File", AlertType.Failure);
                            SetFocus(fldLargeImage.ClientID);
                            return;
                        }
                        if (GlobalFunctions.IsImageDimensionValid(fldLargeImage, Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_LARGE")), Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_LARGE"))) == false)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Large Image file dimensions are not valid", AlertType.Failure);
                            SetFocus(fldLargeImage.ClientID);
                            return;
                        }
                        if (GlobalFunctions.IsFileSizeValid(fldLargeImage, Convert.ToInt64(GlobalFunctions.GetSetting("IMAGESIZE_PRODUCT_LARGE"))) == false)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Large Image file size is not valid", AlertType.Failure);
                            SetFocus(fldLargeImage.ClientID);
                            return;
                        }

                    }
                    #endregion
                    #region Validate Medium Thumbnail Image
                    if (lnkbtnDeleteMedium.Visible == false)
                    {
                        if (!fldMediumImage.HasFile)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please upload Medium Image", AlertType.Failure);
                            SetFocus(fldMediumImage.ClientID);
                            return;
                        }
                    }
                    if (fldMediumImage.PostedFile.ContentLength > 0)
                    {
                        //if (!fldMediumImage.PostedFile.FileName.Contains(".jpg"))
                        if (!GlobalFunctions.CheckFileExtension(fldMediumImage, strFileTypes))
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please use JPG or Png Image only for Medium File", AlertType.Failure);
                            SetFocus(fldMediumImage.ClientID);
                            return;
                        }
                        if (!GlobalFunctions.CheckFileMIMEType(fldMediumImage, strMimeTypes))
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please use valid content type for Image File", AlertType.Failure);
                            SetFocus(fldMediumImage.ClientID);
                            return;
                        }
                        if (GlobalFunctions.IsImageDimensionValid(fldMediumImage, Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_MEDIUM")), Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_MEDIUM"))) == false)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Medium Image file dimensions are not valid", AlertType.Failure);
                            SetFocus(fldMediumImage.ClientID);
                            return;
                        }
                        if (GlobalFunctions.IsFileSizeValid(fldMediumImage, Convert.ToInt64(GlobalFunctions.GetSetting("IMAGESIZE_PRODUCT_MEDIUM"))) == false)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Medium Image file size is not valid", AlertType.Failure);
                            SetFocus(fldMediumImage.ClientID);
                            return;
                        }

                    }
                    #endregion
                    #region Validate Thumbnail Image
                    if (lnkbtnDeleteThumb.Visible == false)
                    {
                        if (!fldTumbImage.HasFile)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please upload Thumbnail Image", AlertType.Failure);
                            SetFocus(fldTumbImage.ClientID);
                            return;
                        }
                    }
                    if (fldTumbImage.PostedFile.ContentLength > 0)
                    {
                        //if (!fldTumbImage.PostedFile.FileName.Contains(".jpg"))
                        if (!GlobalFunctions.CheckFileExtension(fldTumbImage, strFileTypes))
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please use JPG or Png Image only for Thumbnail File", AlertType.Failure);
                            SetFocus(fldTumbImage.ClientID);
                            return;
                        }
                        if (!GlobalFunctions.CheckFileMIMEType(fldTumbImage, strMimeTypes))
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "Please use valid content type for Image File", AlertType.Failure);
                            SetFocus(fldTumbImage.ClientID);
                            return;
                        }
                        if (GlobalFunctions.IsImageDimensionValid(fldTumbImage, Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_THUMB")), Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_THUMB"))) == false)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Thumbnail Image file dimensions are not valid", AlertType.Failure);
                            SetFocus(fldTumbImage.ClientID);
                            return;
                        }
                        if (GlobalFunctions.IsFileSizeValid(fldTumbImage, Convert.ToInt64(GlobalFunctions.GetSetting("IMAGESIZE_PRODUCT_THUMB"))) == false)
                        {
                            ShowHideImageTable();
                            GlobalFunctions.ShowModalAlertMessages(Page, "Thumbnail Image file size is not valid", AlertType.Failure);
                            SetFocus(fldTumbImage.ClientID);
                            return;
                        }

                    }
                    #endregion

                    fileExt = System.IO.Path.GetExtension(fldLargeImage.PostedFile.FileName);
                }


                objProductBE.PropProductVariants.LanguageId = LanguageId;
                if (drpVariantType.SelectedItem.Value == "0")
                {
                    objProductBE.PropProductVariantType.IsActive = true;
                    objProductBE.PropProductVariantType.ProductVariantTypeName = txtVairantType.Text;
                    objProductBE.PropProductVariantType.ShowVariant = chkShowVairant.Checked;
                }
                else
                {
                    objProductBE.PropProductVariantType.IsActive = true;
                    objProductBE.PropProductVariantType.ProductVariantTypeId = Convert.ToInt16(drpVariantType.SelectedItem.Value);
                }

                objProductBE.PropProductVariantType.IsActive = true;

                objProductBE.PropProductVariants.ProductId = Convert.ToInt32(hdnProductId.Value);
                objProductBE.PropProductVariants.ProductVariantName = txtVairantName.Text;
                if (Session["VARIANTIMAGENAME"] != null)
                {
                    objProductBE.PropProductVariants.ImageName = Session["VARIANTIMAGENAME"].ToString();
                    objProductBE.PropProductVariants.ModifiedBy = UserId;
                }
                else
                {
                    objProductBE.PropProductVariants.ImageName = filename + fileExt;
                    objProductBE.PropProductVariants.CreatedBy = UserId;
                }

                objProductBE.PropProductVariants.IsActive = true;

                string ImageName = "";
                if (Session["VARIANTID"] != null)
                {
                    ImageName = Session["VARIANTIMAGENAME"].ToString();
                    objProductBE.PropProductVariants.ProductVariantId = Convert.ToInt32(Session["VARIANTID"]);
                    ImageName = ProductBL.UpdateVairantProductVariant(objProductBE, ref ImageName);
                }
                else
                {
                    ImageName = ProductBL.InsertVairantProductVariant(objProductBE, ref ImageName);
                }
                if (ImageName != "")
                {
                    Session["VARIANTID"] = null;
                    Session["VARIANTIMAGENAME"] = null;
                    filename = ImageName.ToString();
                    if (rbtnAuto.Checked)
                    {
                        if (flupHiResAuto.HasFile)
                        {
                            #region IMAGE FILE

                            // Load the image into Bitmap Object
                            Bitmap uploadedImage = new Bitmap(flupHiResAuto.FileContent);
                            Bitmap uploadedImage2 = new Bitmap(flupHiResAuto.FileContent);
                            Bitmap uploadedImage3 = new Bitmap(flupHiResAuto.FileContent);

                            // Set the maximum width and height here.
                            // You can make this versatile by getting these values from
                            // QueryString or textboxes

                            // Resize the image
                            Bitmap resizedImage = GetResizedImage(uploadedImage, Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_THUMB")), Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_THUMB")));
                            Bitmap resizedImage2 = GetResizedImage(uploadedImage2, Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_MEDIUM")), Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_MEDIUM")));
                            Bitmap resizedImage3 = GetResizedImage(uploadedImage3, Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEWIDTH_PRODUCT_LARGE")), Convert.ToInt32(GlobalFunctions.GetSetting("IMAGEHEIGHT_PRODUCT_LARGE")));

                            //Save the image
                            string[] strFileType = flupHiResAuto.PostedFile.FileName.ToString().Split('.');
                            String virtualPathHiRes = GlobalFunctions.GetPhysicalHiResProductImagePath() + filename.ToString();// +"." + strFileType[1].ToString();
                            String virtualPath = GlobalFunctions.GetPhysicalThumbnailProductImagePath() + filename.ToString();// +"." + strFileType[1].ToString();
                            String virtualPath2 = GlobalFunctions.GetPhysicalMediumProductImagePath() + filename.ToString();// +"." + strFileType[1].ToString();
                            String virtualPath3 = GlobalFunctions.GetPhysicalLargeProductImagePath() + filename.ToString();// +"." + strFileType[1].ToString();


                            fileExt = strFileType[1].ToString();

                            String tempFileNameHiRes = virtualPathHiRes;
                            if (System.IO.File.Exists(tempFileNameHiRes))
                                System.IO.File.Delete(tempFileNameHiRes);

                            String tempFileName = virtualPath;
                            if (System.IO.File.Exists(tempFileName))
                                System.IO.File.Delete(tempFileName);

                            String tempFileName2 = virtualPath2;
                            if (System.IO.File.Exists(tempFileName2))
                                System.IO.File.Delete(tempFileName2);

                            String tempFileName3 = virtualPath3;
                            if (System.IO.File.Exists(tempFileName3))
                                System.IO.File.Delete(tempFileName3);


                            GlobalFunctions.FileUpload(flupHiResAuto, HiResImagePath, filename, GlobalFunctions.FileType.Image, true);

                            resizedImage.Save(tempFileName, uploadedImage.RawFormat);
                            resizedImage2.Save(tempFileName2, uploadedImage2.RawFormat);
                            resizedImage3.Save(tempFileName3, uploadedImage3.RawFormat);

                            #endregion
                        }
                    }
                    else
                    {


                        #region Large Image file

                        if (fldLargeImage.PostedFile.ContentLength > 0)
                        {
                            string[] strFileType = fldLargeImage.PostedFile.FileName.ToString().Split('.');
                            string targetFileName = GlobalFunctions.GetPhysicalLargeProductImagePath() + filename.ToString();// +"." + strFileType[1].ToString();
                            if (System.IO.File.Exists(targetFileName))
                                System.IO.File.Delete(targetFileName);
                            try
                            {
                                fldLargeImage.PostedFile.SaveAs(targetFileName);
                                fldLargeImage.PostedFile.InputStream.Dispose();
                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                            }
                            finally
                            {
                                strFileType = null;
                                targetFileName = "";
                            }
                        }
                        #endregion
                        #region Medium Image file
                        if (fldMediumImage.PostedFile.ContentLength > 0)
                        {
                            string[] strFileType = fldMediumImage.PostedFile.FileName.ToString().Split('.');
                            string targetFileName = GlobalFunctions.GetPhysicalMediumProductImagePath() + filename.ToString();// +"." + strFileType[1].ToString();
                            if (System.IO.File.Exists(targetFileName))
                                System.IO.File.Delete(targetFileName);
                            try
                            {
                                fldMediumImage.PostedFile.SaveAs(targetFileName);
                                fldMediumImage.PostedFile.InputStream.Dispose();
                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                            }
                            finally
                            {
                                strFileType = null;
                                targetFileName = "";
                            }
                        }
                        #endregion
                        #region Thumbnail Image file
                        if (fldTumbImage.PostedFile.ContentLength > 0)
                        {
                            string[] strFileType = fldTumbImage.PostedFile.FileName.ToString().Split('.');
                            string targetFileName = GlobalFunctions.GetPhysicalThumbnailProductImagePath() + filename.ToString();// +"." + strFileType[1].ToString();
                            if (System.IO.File.Exists(targetFileName))
                                System.IO.File.Delete(targetFileName);
                            try
                            {
                                fldTumbImage.PostedFile.SaveAs(targetFileName);
                                fldTumbImage.PostedFile.InputStream.Dispose();
                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                            }
                            finally
                            {
                                strFileType = null;
                                targetFileName = "";
                            }
                        }
                        #endregion
                    }
                    BindProductVairant();
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/DataSaveMessage"), AlertType.Success);
                }

                if (txtVairantType.Text.Trim() != "")
                    BindVariantType();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }



        protected void ShowHideImageTable()
        {
            tableManually.Attributes.Add("style", "display:block;");
            tableAutoResize.Attributes.Add("style", "display:none;");
        }
        protected void btnAddNewImage_OnClick(object sender, EventArgs e)
        {
            rbtnAuto.Checked = true;
            rbtnManually.Checked = false;
            drpVariantType.ClearSelection();
            txtVairantName.Text = string.Empty;
            tableManually.Attributes.Remove("style");
            tableManually.Attributes.Add("style", "display:none;");
            tableAutoResize.Attributes.Add("style", "display:block;");
            Session["VARIANTID"] = null;
            Session["VARIANTIMAGENAME"] = null;
            lnkbtnDeleteThumb.Visible = false;
            lnkbtnDeleteMedium.Visible = false;
            lnkbtnDeleteLarge.Visible = false;
        }
        protected void chkDefault_OnCheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            Repeater rpt = chk.Parent.Parent as Repeater;

            bool IsChk = false;
            foreach (RepeaterItem rptItem in rpt.Items)
            {
                CheckBox chk1 = rptItem.FindControl("chkDefault") as CheckBox;

                if (chk1.Checked)
                    IsChk = true;
            }

            if (!IsChk)
            {
                GlobalFunctions.ShowModalAlertMessages(Page, "Please select atleast one default image.", AlertType.Warning);
                BindProductVairant();
                return;
            }

            if (!string.IsNullOrEmpty(chk.Attributes["rel"].ToString()))
            {
                int Result = 0;
                Result = ProductBL.UpdateDefaultImage(chk.Attributes["rel"].ToString(), Convert.ToInt32(hdnProductId.Value), ref Result);
                if (Result == 1)
                {
                    BindProductVairant();
                    GlobalFunctions.ShowModalAlertMessages(Page, "Data saved successfully.", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "Data saved failed.", AlertType.Failure);
                }
            }
        }

        protected void chkShowVariantVT_OnCheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chk = (CheckBox)sender;
                if (!string.IsNullOrEmpty(chk.Attributes["rel"].ToString()))
                {
                    bool Result = false;
                    ProductBL.UpdateShowVariant(Convert.ToInt32(chk.Attributes["rel"].ToString()), chk.Checked, ref Result);
                    if (Result)
                    {
                        BindProductVairant();
                        GlobalFunctions.ShowModalAlertMessages(Page, "Data saved successfully.", AlertType.Success);
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(Page, "Data saved failed.", AlertType.Failure);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void lnkbtnDeleteAll_OnClick(object sender, CommandEventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                objProductBE = new ProductBE();
                objProductBE.PropProductVariants.ProductVariantId = Convert.ToInt32(e.CommandArgument);
                bool Result = false;
                ProductBL.DeleteVariantByVariantTypeId(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(hdnProductId.Value), ref Result);
                if (Convert.ToBoolean(Result))
                {
                    BindProductVairant();
                    GlobalFunctions.ShowModalAlertMessages(Page, "Variant data deleted successfully.", AlertType.Success);
                    CreateActivityLog("AddEditProducts lnkbtnDeleteAll", "Delete", Convert.ToString("Product Id = " + (e.CommandArgument)));
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "Variant data deletion failed.", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnUpdateVariantTypeName_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;

                RepeaterItem rptItem = btn.Parent.Parent as RepeaterItem;
                TextBox txtRenameVariantName = rptItem.FindControl("txtRenameVariantName") as TextBox;

                int id = Convert.ToInt32(btn.Attributes["rel"].ToString());
                string name = txtRenameVariantName.Text;

                bool Result = false;
                ProductBL.UpdateVariantTypeName(id, name, LanguageId, ref Result);
                if (Convert.ToBoolean(Result))
                {
                    BindVariantType();
                    BindProductVairant();
                    GlobalFunctions.ShowModalAlertMessages(Page, "Variant type name updated successfully.", AlertType.Success);
                    CreateActivityLog("AddEditProducts btnUpdateVariantTypeName", "Update", Convert.ToString("Product Id = " + id));
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "Variant type name update failed.", AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void lnkbtnEdit_OnClick(object sender, CommandEventArgs e)
        {
            Session["PRODUCT_VARIANTS"] = null;
            LinkButton lnkbtnEdit = (LinkButton)sender;
            objProductBE = new ProductBE();
            try
            {
                objProductBE = ProductBL.GetProductVaiants(Convert.ToInt32(hdnProductId.Value), LanguageId);
                Session["PRODUCT_VARIANTS"] = objProductBE.PropGetAllProductVariants;
                if (objProductBE.PropGetAllProductVariants.Count > 0)
                {
                    Session["VARIANTID"] = Convert.ToInt32(e.CommandArgument);

                    //DataRow[] dr = ds.Tables[1].Select("ProductVariantId = " + Convert.ToInt32(e.CommandArgument));
                    objProductBE.PropGetAllProductVariants = objProductBE.PropGetAllProductVariants.FindAll(x => x.ProductVariantId == Convert.ToInt32(e.CommandArgument));

                    if (objProductBE.PropGetAllProductVariants.Count > 0)
                    {
                        //DataRow[] dr1 = ds.Tables[0].Select("ProductVariantTypeId = " + Convert.ToInt32(dr[0]["ProductVariantTypeId"].ToString()));
                        objProductBE.PropGetAllVariantType = objProductBE.PropGetAllVariantType.FindAll(x => x.ProductVariantTypeId == objProductBE.PropGetAllProductVariants[0].ProductVariantTypeId);
                        if (objProductBE.PropGetAllVariantType.Count > 0)
                        {
                            chkShowVairant.Checked = Convert.ToBoolean(objProductBE.PropGetAllVariantType[0].ShowVariant.ToString());
                        }

                        rbtnManually.Checked = true;
                        rbtnAuto.Checked = false;
                        tableAutoResize.Attributes.Add("style", "display:none;");
                        tableManually.Attributes.Add("style", "display:block;");

                        drpVariantType.SelectedValue = objProductBE.PropGetAllProductVariants[0].ProductVariantTypeId.ToString();
                        txtVairantName.Text = objProductBE.PropGetAllProductVariants[0].ProductVariantName.ToString();
                        Session["VARIANTNAME"] = txtVairantName.Text;
                        string imageName = objProductBE.PropGetAllProductVariants[0].ImageName.ToString();
                        Session["VARIANTIMAGENAME"] = imageName;


                        //Large Image
                        if (File.Exists(GlobalFunctions.GetPhysicalLargeProductImagePath() + imageName))
                        {
                            lnkbtnDeleteLarge.Visible = true;
                            lnkbtnDeleteLarge.Attributes.Add("rel", imageName);
                        }

                        //Medium Image
                        if (File.Exists(GlobalFunctions.GetPhysicalMediumProductImagePath() + imageName))
                        {
                            lnkbtnDeleteMedium.Visible = true;
                            lnkbtnDeleteMedium.Attributes.Add("rel", imageName);
                        }

                        //Thumbnail Image
                        if (File.Exists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + imageName))
                        {
                            lnkbtnDeleteThumb.Visible = true;
                            lnkbtnDeleteThumb.Attributes.Add("rel", imageName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }
        protected void lnkbtnDelete_OnClick(object sender, CommandEventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            if (Convert.ToBoolean(btn.Attributes["rel"].ToString()))
            {
                GlobalFunctions.ShowModalAlertMessages(Page, "Please assign another variant as Default Image.", AlertType.Warning);
                SetFocus(btn.ID);
                return;
            }
            else
            {
                objProductBE = new ProductBE();
                objProductBE.PropProductVariants.ProductVariantId = Convert.ToInt32(e.CommandArgument);
                objProductBE.PropProductVariants.ProductId = Convert.ToInt32(hdnProductId.Value);
                string Result = "False";
                Result = ProductBL.DeleteVairant(objProductBE, ref Result);
                if (Convert.ToBoolean(Result))
                {
                    BindProductVairant();
                    GlobalFunctions.ShowModalAlertMessages(Page, "Variant deleted successfully.", AlertType.Success);
                    CreateActivityLog("AddEditProducts lnkbtnDelete", "Delete", Convert.ToString(hdnProductId.Value));
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, "Variant deleted failed.", AlertType.Failure);
                }
            }
        }

        protected void btnDeletePdf_Click(object sender, EventArgs e)
        {
            DeleteImage(PDFFilePath + GlobalFunctions.RemoveSpecialCharacters(hdnProductCode.Value.ToString() + ".pdf").ToString());
            hypPdf.Visible = false;
            btnDeletePdf.Visible = false;
        }
        protected void DeleteImage(string Path)
        {
            if (System.IO.File.Exists(Path))
                System.IO.File.Delete(Path);
        }
        protected void DeleteImage_OnCommand(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkbtn = (LinkButton)sender;
                string imageName = lnkbtn.Attributes["rel"];
                if (imageName != "")
                {
                    string targetFileName = "";
                    if (lnkbtn.CommandName.ToString().ToLower() == "hires")
                    {
                        targetFileName = GlobalFunctions.GetPhysicalHiResProductImagePath() + imageName.ToString();
                    }
                    else if (lnkbtn.CommandName.ToString().ToLower() == "large")
                    {
                        targetFileName = GlobalFunctions.GetPhysicalLargeProductImagePath() + imageName.ToString();
                    }
                    else if (lnkbtn.CommandName.ToString().ToLower() == "medium")
                    {
                        targetFileName = GlobalFunctions.GetPhysicalMediumProductImagePath() + imageName.ToString();
                    }
                    else
                    {
                        targetFileName = GlobalFunctions.GetPhysicalThumbnailProductImagePath() + imageName.ToString();
                    }
                    if (System.IO.File.Exists(targetFileName))
                        System.IO.File.Delete(targetFileName);
                    lnkbtn.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }
        protected void ClearImageControls()
        {
            txtVairantName.Text = string.Empty;
            drpVariantType.ClearSelection();
            //lnkbtnDeleteHiRes.Visible = false;
            lnkbtnDeleteLarge.Visible = false;
            lnkbtnDeleteMedium.Visible = false;
            lnkbtnDeleteThumb.Visible = false;
            Session["VARIANTID"] = null;
            Session["VARIANTIMAGENAME"] = null;
        }
        #endregion

        #region Related Products
        protected void ddlPParentCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ////*************************************************************************
            //// Purpose : Event will get fired when product parent category get changed
            //// Inputs  : object sender, EventArgs e
            //// Return  : Nothing
            ////*************************************************************************

            CategoryBE objCategoryBE = new CategoryBE();
            List<CategoryBE> lstCategoryBE = new List<CategoryBE>();

            try
            {
                if (ddlPParentCategory.SelectedIndex > 0)
                {
                    lstCategoryBE = CategoryBL.GetAllCategories();
                    if (lstCategoryBE != null)
                    {
                        if (lstCategoryBE.Count > 0)
                        {
                            lstCategoryBE = lstCategoryBE.FindAll(x => x.ParentCategoryId == Convert.ToInt16(ddlPParentCategory.SelectedValue));
                            if (lstCategoryBE.Count > 0)
                            {
                                ddlPSubCategory.DataSource = lstCategoryBE;
                                ddlPSubCategory.DataTextField = "CategoryName";
                                ddlPSubCategory.DataValueField = "CategoryId";
                                ddlPSubCategory.DataBind();
                                ddlPSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));

                                trPSCategory.Visible = true;
                                trPSSCategory.Visible = false;
                                lstUnAssignedProducts.Items.Clear();
                                lstAssignedProducts.Items.Clear();
                            }
                            else
                            {
                                trPSCategory.Visible = false;
                                trPSSCategory.Visible = false;
                                HIDSelectCatId.Value = ddlPParentCategory.SelectedValue.ToString();
                                BindUnAssignedProducts();
                                BindAssignedProducts();
                            }
                        }
                        else
                        {
                            trPSCategory.Visible = false;
                            trPSSCategory.Visible = false;
                            HIDSelectCatId.Value = ddlPParentCategory.SelectedValue.ToString();
                            BindUnAssignedProducts();
                            BindAssignedProducts();
                        }

                    }
                    else
                    {

                        trPSCategory.Visible = false;
                        trPSSCategory.Visible = false;
                        HIDSelectCatId.Value = ddlPParentCategory.SelectedValue.ToString();
                        BindUnAssignedProducts();
                        BindAssignedProducts();
                    }
                }
                else
                {
                    lstUnAssignedProducts.Items.Clear();
                    lstAssignedProducts.Items.Clear();
                    trPSCategory.Visible = false;
                    trPSSCategory.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objCategoryBE = null;
                lstCategoryBE = null;
            }

        }
        protected void ddlPSubCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            CategoryBE objCategoryBE = new CategoryBE();
            List<CategoryBE> lstCategoryBE = new List<CategoryBE>();
            try
            {

                if (ddlPSubCategory.SelectedIndex > 0)
                {
                    lstCategoryBE = CategoryBL.GetAllCategories();
                    if (lstCategoryBE != null)
                    {
                        if (lstCategoryBE.Count > 0)
                        {
                            lstCategoryBE = lstCategoryBE.FindAll(x => x.ParentCategoryId == Convert.ToInt16(ddlPSubCategory.SelectedValue));
                            if (lstCategoryBE.Count > 0)
                            {
                                ddlPSubSubCategory.DataSource = lstCategoryBE;
                                ddlPSubSubCategory.DataTextField = "CategoryName";
                                ddlPSubSubCategory.DataValueField = "CategoryId";
                                ddlPSubSubCategory.DataBind();
                                ddlPSubSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));

                                trPSSCategory.Visible = true;
                                lstUnAssignedProducts.Items.Clear();
                                lstAssignedProducts.Items.Clear();
                            }
                            else
                            {
                                lstUnAssignedProducts.Items.Clear();
                                lstAssignedProducts.Items.Clear();
                                trPSSCategory.Visible = false;
                                HIDSelectCatId.Value = ddlPSubCategory.SelectedValue.ToString();
                                BindUnAssignedProducts();
                                BindAssignedProducts();
                            }
                        }
                        else
                        {
                            lstUnAssignedProducts.Items.Clear();
                            lstAssignedProducts.Items.Clear();
                            trPSSCategory.Visible = false;
                            HIDSelectCatId.Value = ddlPSubCategory.SelectedValue.ToString();
                            BindUnAssignedProducts();
                            BindAssignedProducts();
                        }
                    }
                    else
                    {
                        trPSSCategory.Visible = false;
                        HIDSelectCatId.Value = ddlPSubCategory.SelectedValue.ToString();
                        BindUnAssignedProducts();
                        BindAssignedProducts();
                    }
                }
                else
                {
                    lstUnAssignedProducts.Items.Clear();
                    lstAssignedProducts.Items.Clear();
                    trPSSCategory.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objCategoryBE = null;
                lstCategoryBE = null;
            }
        }
        protected void ddlPSubSubCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPSubSubCategory.SelectedIndex > 0)
            {
                HIDSelectCatId.Value = ddlPSubSubCategory.SelectedValue.ToString();
                BindUnAssignedProducts();
                BindAssignedProducts();
            }
            else
            {
                lstUnAssignedProducts.Items.Clear();
                lstAssignedProducts.Items.Clear();
            }
        }
        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrencyId = Convert.ToInt16(ddlCurrency.SelectedValue);
            ClearPriceControls();
            trAddNewPrice.Visible = false;
            if (mode == "e")
                productId = Convert.ToInt32(Request.QueryString["pid"]);
            rbPrice.Checked = false;
            rbCallForPrice.Checked = false;
            tblPriceTable.Visible = false;
            btnCallForPrice.Visible = false;

            BindProductPrice(productId, LanguageId, CurrencyId);
        }
        protected void BindUnAssignedProducts()
        {
            //*************************************************************************
            // Purpose : It will Bind products.
            // Inputs  : Nothing
            // Return  : Nothing
            //*************************************************************************
            Int16 catId = Convert.ToInt16(HIDSelectCatId.Value);
            List<ProductBE> lstProductBE = new List<ProductBE>();
            try
            {
                lstProductBE = ProductBL.GetUnAssignedRelatedProducts(Convert.ToInt32(hdnProductId.Value), catId);
                lstUnAssignedProducts.Items.Clear();
                lstAssignedProducts.Items.Clear();
                if (lstProductBE != null)
                {
                    if (lstProductBE.Count > 0)
                    {
                        lstUnAssignedProducts.DataSource = lstProductBE;
                        lstUnAssignedProducts.DataTextField = "ProductName";
                        lstUnAssignedProducts.DataValueField = "ProductId";
                        lstUnAssignedProducts.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void BindAssignedProducts()
        {
            //*************************************************************************
            // Purpose : It will Bind products.
            // Inputs  : Nothing
            // Return  : Nothing
            //*************************************************************************
            List<ProductBE> lstProductBE = new List<ProductBE>();
            try
            {

                lstProductBE = ProductBL.GetAssignedRelatedProducts(Convert.ToInt32(hdnProductId.Value));
                lstAssignedProducts.Items.Clear();
                if (lstProductBE != null)
                {
                    if (lstProductBE.Count > 0)
                    {
                        lstAssignedProducts.DataSource = lstProductBE;
                        lstAssignedProducts.DataTextField = "ProductName";
                        lstAssignedProducts.DataValueField = "ProductId";
                        lstAssignedProducts.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        private void BindCategory()
        {
            CategoryBE objCategoryBE = new CategoryBE();
            ddlPParentCategory.ClearSelection();
            List<CategoryBE> lstCategoryBE = new List<CategoryBE>();
            try
            {
                lstCategoryBE = CategoryBL.GetAllCategories();
                if (lstCategoryBE != null)
                {
                    if (lstCategoryBE.Count > 0)
                    {
                        lstCategoryBE = lstCategoryBE.FindAll(x => x.ParentCategoryId == 0);
                        if (lstCategoryBE.Count > 0)
                        {
                            ddlPParentCategory.DataSource = lstCategoryBE;
                            ddlPParentCategory.DataTextField = "CategoryName";
                            ddlPParentCategory.DataValueField = "CategoryId";
                            ddlPParentCategory.DataBind();
                            ddlPParentCategory.Items.Insert(0, new ListItem("--Select--", "0"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objCategoryBE = null;
                lstCategoryBE = null;
            }
        }
        protected void btnSaveRelated_Click(object sender, EventArgs e)
        {
            //*************************************************************************
            // Purpose : It will fired at the time of clicking on the btnSave button.
            // Inputs  : object sender, EventArgs e
            // Return  : Nothing
            //*************************************************************************
            try
            {
                if (lstUnAssignedProducts.Items.Count == 0 && lstAssignedProducts.Items.Count == 0)
                {
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('No item(s) Assigned/Unassigned');", true);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "No item(s) Assigned/Unassigned", AlertType.Warning);
                    return;
                }


                int result = 0;
                result = ProductBL.InsertRelatedProductDetails(Convert.ToInt32(hdnProductId.Value), HIDAssignedProductIds.Value, UserId);
                if (result != 0)
                {
                    BindUnAssignedProducts();
                    BindAssignedProducts();
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Exceptions.GetException("SuccessMessage/DataSaveMessage") + "');", true);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/DataSaveMessage"), AlertType.Success);
                    CreateActivityLog("AddEditProducts btnSaveRelated", "Insert", hdnProductCode.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion

        #region Sections
        protected void BindSection(int ProductId) { }
        #endregion


        #region Product Charges
        protected void BindProductCharges() { }

        protected void BindProductChargesByImprintMethod(string ImprintMethodChargeId) { }
        protected void lnkbtnAddCharge_OnClientClick(object sender, EventArgs e) { }
        protected void lnkbtnNewCharge_OnClientClick(object sender, EventArgs e) { }
        protected void lnkbtnSaveAddCharges_lnkbtnSaveAddCharges(object sender, EventArgs e) { }
        protected void lnkbtnSaveNewCharges_lnkbtnSaveAddCharges(object sender, EventArgs e) { }
        protected void lnkbtnCancelAddCharges_lnkbtnSaveAddCharges(object sender, EventArgs e) { }

        protected void btnSaveProductCharges_Click(object sender, EventArgs e) { }

        private void ClearImprintFields() { }
        #endregion

        protected void gvImprint_OnRowCommand(object sender, GridViewCommandEventArgs e) { }

        protected void btnAddNewImprintMethod_OnClick(object sender, EventArgs e) { }

        protected void BindBackgroundColor() { }

        protected void BindDropDownProducts()
        {
            try
            {
                List<ProductBE> lstProductBE = ProductBL.GetProductList<ProductBE>(LanguageId, 0, 0, "");
                if (lstProductBE != null)
                {
                    ddlProducts.DataSource = lstProductBE;
                    ddlProducts.DataTextField = "ProductNameWithCode";
                    ddlProducts.DataValueField = "ProductId";
                    ddlProducts.DataBind();
                    ddlProducts.SelectedValue = Convert.ToString(productId);
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                Int32 ProductId = Convert.ToInt16(ddlProducts.SelectedValue);
                productId = ProductId;
                hdnProductId.Value = Convert.ToString(ProductId);

                if (mode == "e")
                {
                    Response.Redirect("AddEditProducts.aspx?mode=e&pid=" + productId.ToString());
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}