﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Products_ProductReviewManagement : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.GridView grdReviewAndRating;
        List<ProductBE.ReviewRating> lstReviewRating;
        Int16 LanguageId = 1;
        public Int16 PageIndex;
        public Int16 PageSize = 10;
        public UserBE objUserBE;
        public int UserId;
        public static string host = GlobalFunctions.GetVirtualPathAdmin();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageIndex = 0;
                    BindReviewAndRating(PageIndex, PageSize);
                }
                if(Session["StoreUser"] != null)
                {
                    objUserBE = Session["StoreUser"] as UserBE;
                    UserId = objUserBE.UserId;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        private void BindReviewAndRating(Int16 PageIndex, Int16 PageSize)
        {
            lstReviewRating = new List<ProductBE.ReviewRating>();
            try
            {
                lstReviewRating = ProductBL.GetAllProductReviewAndRating<ProductBE.ReviewRating>(LanguageId, PageIndex, PageSize);
                if (lstReviewRating != null)
                {
                    if (lstReviewRating.Count > 0)
                    {
                        grdReviewAndRating.DataSource = lstReviewRating;
                        grdReviewAndRating.VirtualItemCount = lstReviewRating[0].TotalCount;
                        grdReviewAndRating.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void grdReviewAndRating_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdReviewAndRating.PageIndex = e.NewPageIndex;
            PageIndex = Convert.ToInt16(e.NewPageIndex);
            BindReviewAndRating(PageIndex, PageSize);

        }

        protected void grdReviewAndRating_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int ReviewRatingId;
            bool result = false;
            if(e.CommandName.Equals("A"))
            {
                ReviewRatingId = Convert.ToInt32(e.CommandArgument);
                result = ProductBL.UpdateProductReviewStatus(ReviewRatingId,"A", UserId);
                if (result)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/ReviewApproved"), AlertType.Success);
                    CreateActivityLog("ProductReviewManagement Approved", "Update", Convert.ToString(Convert.ToInt32(e.CommandArgument)));
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
            }
            if(e.CommandName.Equals("D"))
            {
                ReviewRatingId = Convert.ToInt32(e.CommandArgument);
                result = ProductBL.UpdateProductReviewStatus(ReviewRatingId, "D", UserId);
                if (result)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/ReviewDisapproved"), AlertType.Success);
                    CreateActivityLog("ProductReviewManagement DisApproved", "Update", Convert.ToString(Convert.ToInt32(e.CommandArgument)));
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
            }
            BindReviewAndRating(PageIndex, PageSize);

        }

        protected void grdReviewAndRating_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HtmlGenericControl spnRating1 = (HtmlGenericControl)e.Row.FindControl("spnRating1");
                    HtmlGenericControl spnRating2 = (HtmlGenericControl)e.Row.FindControl("spnRating2");
                    HtmlGenericControl spnRating3 = (HtmlGenericControl)e.Row.FindControl("spnRating3");
                    HtmlGenericControl spnRating4 = (HtmlGenericControl)e.Row.FindControl("spnRating4");
                    HtmlGenericControl spnRating5 = (HtmlGenericControl)e.Row.FindControl("spnRating5");
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    ProductBE.ReviewRating objReviewRating = (ProductBE.ReviewRating)e.Row.DataItem;
                    if (objReviewRating.Action.ToString().ToLower() == "a")
                        lblStatus.Text = "Approved";
                    else if (objReviewRating.Action.ToString().ToLower() == "d")
                        lblStatus.Text = "Disapproved";
                    else
                        lblStatus.Text = "Pending";
                    switch (Convert.ToInt32(objReviewRating.Rating))
                    {
                        case 5:
                            spnRating5.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            goto case 4;
                        case 4:
                            spnRating4.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            goto case 3;
                        case 3:
                            spnRating3.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            goto case 2;
                        case 2:
                            spnRating2.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            goto case 1;
                        case 1:
                            spnRating1.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}