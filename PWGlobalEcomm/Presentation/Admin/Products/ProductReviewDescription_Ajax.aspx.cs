﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Admin_Products_ProductReviewDescription_Ajax : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Literal ltrReviewTitle, ltrDescription;
        int ReviewRatingId;
        ProductBE.ReviewRating objReviewRating;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ReviewRatingId = Convert.ToInt32(Request.Form["ReviewRatingId"]);
                if(ReviewRatingId != 0)
                {
                    objReviewRating = ProductBL.GetProductReviewByReviewId(ReviewRatingId);
                    if(objReviewRating!=null)
                    {
                        ltrReviewTitle.Text = objReviewRating.ReviewTitle;
                        ltrDescription.Text = objReviewRating.Review;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}