﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CKEditor.NET;
using System.IO;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.DataAccess;

namespace Presentation
{
    public partial class Admin_Landing_LandingPage : BasePage //System.Web.UI.Page
    {
        #region "Variables"
        protected global::System.Web.UI.WebControls.Button btnUpload;
        protected global::CKEditor.NET.CKEditorControl txtLandingPageContent;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;


        #endregion
        public string host = GlobalFunctions.GetVirtualPath();
        public string ImageSize = GlobalFunctions.GetSetting("LandingPageConfigImageSize");
        public string CategoryExtension = GlobalFunctions.GetSetting("LandingPageConfigImageExt");
        public string LandingPageImageHtml = "";
        StoreBE objStoreBE = new StoreBE();
        int LanguageId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    objStoreBE = StoreBL.GetStoreDetails();
                    BindLanguage(objStoreBE);
                    FillPage();
                    txtLandingPageContent.config.toolbar = new object[]
			    {
                    new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				    new object[] { "NumberedList", "BulletedList"},
				    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				    "/",
				    new object[] { "Styles", "Format", "Font", "FontSize" },
				    new object[] { "TextColor", "BGColor" },
                    "/",
                                    new object[] { "Link", "Unlink"},
                    new object[] { "Table", "HorizontalRule"}
			    };
                }
                string[] fileEntries = Directory.GetFiles(GlobalFunctions.GetPhysicalFolderPath() + "Images\\LandingPage\\");
                foreach (string fileName in fileEntries)
                {
                    if (fileName.Contains("LandingPage"))
                    {
                        LandingPageImageHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + host + "Images/LandingPage/LandingPage" + System.IO.Path.GetExtension(fileName) + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                        //imgThumb.ImageUrl = "/Images/LandingPage/" + System.IO.Path.GetFileName(fileName);
                        break;
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                #region
                //if (flUpload.HasFiles)
                //{
                //    if (flUpload.PostedFile.ContentLength > 0)
                //    {
                //        //Chk for File Extension
                //        string[] strFileTypes = { ".jpg", ".jpeg", ".gif", ".png", };
                //        string[] strMimeTypes = { "image/jpeg", "image/gif", "image/png" };
                //        bool bChkFileType = false;
                //        bChkFileType = GlobalFunctions.CheckFileExtension(flUpload, strFileTypes);
                //        bool bChkFileMimeType = false;
                //        bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(flUpload, strMimeTypes);
                //        if (!(bChkFileType && bChkFileMimeType))
                //        {
                //            GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Type or Content Type", AlertType.Warning);
                //            SetFocus(flUpload.ClientID);
                //            return;
                //        }

                //        bool bValidDim = ValidateFileDimensions();
                //        if (!bValidDim)
                //        {
                //            GlobalFunctions.ShowModalAlertMessages(Page, "Invalid File Dimension", AlertType.Warning);
                //            SetFocus(flUpload.ClientID);
                //            return;
                //        }
                //        string strFileName = flUpload.FileName;
                //        string strNewFileName = "LandingPage" + System.IO.Path.GetExtension(flUpload.FileName);

                //        string[] fileEntries = Directory.GetFiles(GlobalFunctions.GetPhysicalFolderPath() + "Images\\LandingPage\\");
                //        string strNewImgPath = "";
                //        foreach (string fileName in fileEntries)
                //        {
                //            strNewImgPath = fileName;
                //            break;
                //        }
                //        strNewImgPath = GlobalFunctions.GetPhysicalFolderPath() + "Images\\LandingPage\\Backup\\" + System.IO.Path.GetFileName(strNewImgPath);
                //        string strImgPath = GlobalFunctions.GetPhysicalFolderPath() + "Images\\LandingPage\\" + System.IO.Path.GetFileName(strNewImgPath);

                //        if (strNewImgPath != "")
                //        {
                //            File.Copy(strImgPath, strNewImgPath + "." + DateTime.Now.ToString("dd-MM-yy hh-mm-ss"), true);
                //        }

                //        strImgPath = GlobalFunctions.GetPhysicalFolderPath() + "Images\\LandingPage\\" + strNewFileName;
                //        flUpload.SaveAs(strImgPath);
                //    }
                //} 
                #endregion

                LandingPageConfigurationBE objLandingPageConfigurationBE = new LandingPageConfigurationBE();
                objLandingPageConfigurationBE.LandingPageText = txtLandingPageContent.Text.Trim();
                objLandingPageConfigurationBE.ImagePath = Convert.ToString(Session["LandingImageName"]);
                objLandingPageConfigurationBE.LanguageID = Convert.ToInt32(Session["LanguageId"]);
                bool bres = LandingPageConfigurationBL.AddLandingPageConfiguration(objLandingPageConfigurationBE, DBAction.Insert);
                if (bres)
                {
                    CreateActivityLog("Landing Page", "Updated", "");
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Configuration setting saved successfully.", AlertType.Success);
                    Session["LandingPageConfigurationID"] = null;
                    Session["LandingImageName"] = null;
                    FillPage();
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error saving Configuration setting.", AlertType.Failure);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue.ToString());
                Session["LanguageId"] = LanguageId;
                FillPage();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        //public bool ValidateFileDimensions()
        //{
        //using (System.Drawing.Image myImage = System.Drawing.Image.FromStream(flUpload.PostedFile.InputStream))
        //{
        //    return (myImage.Height >= 446 && myImage.Width >= 1170);
        //}
        //}

        public void FillPage()
        {
            try
            {
                LandingPageConfigurationBE objLandingPageConfigurationBE = new LandingPageConfigurationBE();
                List<object> lstObject = new List<object>();
                List<LandingPageConfigurationBE> lstLandingPageConfigurationBE = new List<LandingPageConfigurationBE>();

                lstLandingPageConfigurationBE = LandingPageConfigurationBL.GetListLandingPageConfiguration(objLandingPageConfigurationBE);
                if (lstLandingPageConfigurationBE != null)
                {
                    if (lstLandingPageConfigurationBE.Count >= 1)
                    {
                        int iLID = Convert.ToInt32(ddlLanguage.SelectedValue);
                        objLandingPageConfigurationBE = lstLandingPageConfigurationBE.FirstOrDefault(x => x.LanguageID == iLID);
                        if (objLandingPageConfigurationBE != null)
                        {
                            txtLandingPageContent.Text = objLandingPageConfigurationBE.LandingPageText;
                            Session["LandingPageConfigurationID"] = objLandingPageConfigurationBE.ID;
                            Session["LandingImageName"] = objLandingPageConfigurationBE.ImagePath;
                        }
                        else
                        {
                            txtLandingPageContent.Text = "";
                            Session["LandingPageConfigurationID"] = "";
                            Session["LandingImageName"] = "";
                        }
                        //txtLandingPageContent.Text = lstLandingPageConfigurationBE[0].LandingPageText;
                    }
                }

                string[] fileEntries = Directory.GetFiles(GlobalFunctions.GetPhysicalFolderPath() + "Images\\LandingPage\\");
                foreach (string fileName in fileEntries)
                {
                    if (fileName.Contains("LandingPage"))
                    {
                        //LandingPageImageHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + host + "Images/LandingPage/LandingPage" + Convert.ToString(Session["LanguageID"]) + System.IO.Path.GetExtension(fileName) + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                        LandingPageImageHtml = "/Images/LandingPage/" + System.IO.Path.GetFileName(fileName);
                        break;
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        private void BindLanguage(StoreBE objStoreBE)
        {
            try
            {
                if (objStoreBE.StoreLanguages.Count > 0)
                {
                    ddlLanguage.DataSource = objStoreBE.StoreLanguages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                    ddlLanguage.SelectedValue = Convert.ToString(objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId);
                    LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                }
                else
                {
                    LanguageId = objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}