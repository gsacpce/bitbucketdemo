﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Presentation
{

     
    
    public class Admin_Landing_UpdateLandingPage : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.RadioButtonList rbtnDisplayType, rbtnControlType;
        protected global::System.Web.UI.WebControls.Button btnSave,btnCancel;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMessage;
        
        
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
        
            if (!IsPostBack)
            {


                PopulateRadioButton("LP_DisplayType", ref rbtnDisplayType);
                PopulateRadioButton("LP_ControlType", ref rbtnControlType);

            }
        }

        /// <summary>
        /// to bind the data coming from the db
        /// </summary>
        /// <param name="?"></param>
        /// <param name="rbtnList"></param>
        protected void PopulateRadioButton(string Type, ref RadioButtonList rbtnList)
        {
            try
            {
                List<FeatureBE> Features = FeatureBL.GetLandingPageDetails(Type);
                if (Features != null)
                {
                    if (Features.Count() > 0)
                    {
                        foreach (FeatureBE Feature in Features)
                        {
                            if (Feature.IsDefault == true)
                            {
                                rbtnList.SelectedValue = Convert.ToString(Feature.FeatureValueId);
                                break;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                
            }
            
        
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<Int16> TypeIds = new List<Int16>();
                TypeIds.Add(Convert.ToInt16(rbtnDisplayType.SelectedValue));
                TypeIds.Add(Convert.ToInt16(rbtnControlType.SelectedValue));
                bool Status= FeatureBL.UpdateLandingPageData(TypeIds,"Edit");
                if (Status)
                {
                    CreateActivityLog("UpdateLanding", "Updated", "");
                    divMessage.InnerText = "Updated Successfully";
                    divMessage.Visible = true;
                }
                else 
                {
                    divMessage.InnerText = "Error occured please try again.";
                }
                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                
            }
        
        }
    }

}
