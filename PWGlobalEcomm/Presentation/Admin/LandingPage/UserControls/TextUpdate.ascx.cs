﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{

    public class Admin_Landing_UserControls_TextUpdate : System.Web.UI.UserControl
    {
        public global::System.Web.UI.WebControls.Literal ltlTextUpdate;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDisplaySelectedDetails("LP_DisplayType");

            }
        }
        /// <summary>
        /// It will bind the drop down list
        /// </summary>
        private void BindTextUpdate(string Type)
        {
            try
            {
                if (Type == "currency")
                {
                    List<FeatureBE> Currencies = FeatureBL.GetAllCurrencies();


                    if (Currencies != null)
                    {
                        if (Currencies.Count() == 1)
                        {
                            Response.Write("Redirect to login or home page only one currency");
                            if (Currencies.First().FeatureValue.ToLower() == "currency")
                            {
                                Session["Currency"] = Currencies.First().FeatureValue;
                            }
                        }
                        else
                        {
                            foreach (FeatureBE Currency in Currencies)
                            {
                                ltlTextUpdate.Text = ltlTextUpdate.Text + Currency.CurrencyName + "&nbsp;";
                            }

                        }


                    }

                }
                else if (Type == "country")
                {
                    List<FeatureBE> Countries = FeatureBL.GetAllCountries();
                    if (Countries != null)
                    {
                        foreach (FeatureBE Country in Countries)
                        {
                            ltlTextUpdate.Text = ltlTextUpdate.Text + Country.CountryName + "&nbsp;";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }
        }


        /// <summary>
        /// to bind the data coming from the db it will have either currency selected or country
        /// </summary>


        protected void GetDisplaySelectedDetails(string Type)
        {
            try
            {
                List<FeatureBE> Features = FeatureBL.GetLandingPageDetails(Type);
                if (Features != null)
                {

                    foreach (FeatureBE Feature in Features)
                    {

                        if (Feature.IsDefault == true)
                        {
                            if (Feature.FeatureValue.ToLower() == "currency")
                            {
                                BindTextUpdate("currency");
                            }
                            else if (Feature.FeatureValue.ToLower() == "country")
                            {
                                BindTextUpdate("country");

                            }
                            break;
                        }

                    }
                    
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }


        }

    }

}
