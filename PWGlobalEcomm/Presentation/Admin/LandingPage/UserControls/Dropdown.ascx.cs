﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_Landing_UserControls_Dropdown : System.Web.UI.UserControl
    {
        public global::System.Web.UI.WebControls.DropDownList ddlControl;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDisplaySelectedDetails("LP_DisplayType");

            }
        }
        /// <summary>
        /// It will bind the drop down list
        /// </summary>
        private void BindDropDownList(string Type)
        {
            try
            {
                if (Type == "currency")
                {
                    List<FeatureBE> Currencies = FeatureBL.GetAllCurrencies();
                    if (Currencies != null)
                    {
                        if (Currencies.Count() == 1)
                        {
                            Response.Write("Redirect to login or home page only one currency");
                            
                            if (Currencies.First().FeatureValue.ToLower() == "currency")
                            {
                                Session["Currency"] = Currencies.First().FeatureValue;
                            }
                            Response.Redirect("~/Admin/Category/CategoryManagement.aspx");
                        }
                        else
                        {
                            ddlControl.DataSource = Currencies;
                            ddlControl.DataTextField = "CurrencyName";
                            ddlControl.DataValueField = "CurrencyId";
                            ddlControl.DataBind();
                        }

                    }

                }
                else if (Type == "country")
                {
                    List<FeatureBE> Countries = FeatureBL.GetAllCountries();
                    if (Countries != null)
                    {
                        ddlControl.DataSource = Countries;
                        ddlControl.DataTextField = "CountryName";
                        ddlControl.DataValueField = "CountryId";
                        ddlControl.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }
        }


        /// <summary>
        /// to bind the data coming from the db it will have either currency selected or country
        /// based on that dropdownlist is bind
        /// </summary>
        /// <param name="?"></param>
        /// <param name="rbtnList"></param>
        protected void GetDisplaySelectedDetails(string Type)
        {
            try
            {
                List<FeatureBE> Features = FeatureBL.GetLandingPageDetails(Type);
                if (Features != null)
                {


                    if (Features.Count() > 0)
                    {

                        foreach (FeatureBE Feature in Features)
                        {

                            if (Feature.IsDefault == true)
                            {
                                if (Feature.FeatureValue.ToLower() == "currency")
                                {
                                    BindDropDownList("currency");
                                }
                                else if (Feature.FeatureValue.ToLower() == "country")
                                {
                                    BindDropDownList("country");

                                }
                                break;
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }


        }

    }
}
