﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;

namespace Presentation
{
    public class Admin_StaticTextManagement_ManageStaticText : BasePage//System.Web.UI.Page
    {
        int EditStaticId = 0;
        string StaticType;
        int UserId;
        public string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.Label lblHeading;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvLang;
        protected global::System.Web.UI.WebControls.TextBox txtKey, txtValue;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["StoreUser"] != null)
                    {
                        UserBE objUserBE = new UserBE();
                        objUserBE = Session["StoreUser"] as UserBE;
                        UserId = objUserBE.UserId;
                    }
                    #region Meta Data
                    Page.Title = "Add/Edit Static text";
                    Page.MetaDescription = "Add/Edit Static text";
                    Page.MetaKeywords = "add static text, edit static text";
                    #endregion
                    PopulateLanguage();
                }
                if (Convert.ToString(Session["StaticTextType"]) != "" && Convert.ToString(Session["StaticTextType"]) != null)
                {
                    StaticType = Convert.ToString(Session["StaticTextType"]);
                    if (StaticType == "Edit")
                    {
                        if (Convert.ToString(Session["StaticTextId"]) != null)
                        {
                            EditStaticId = Convert.ToInt16(Session["StaticTextId"]);
                            lblHeading.Text = "Edit Static Text";
                            if (!IsPostBack)
                            { PopulateStaticText(EditStaticId); dvLang.Visible = true; }
                        }
                        else
                        { lblHeading.Text = "Add New Static Text"; }
                    }
                }
                else
                { lblHeading.Text = "Add New Static Text"; }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateStaticText(int Id)
        {
            try
            {
                txtKey.Enabled = false;
                StaticTextKeyBE lst = new StaticTextKeyBE();
                StaticTextKeyBE objBE = new StaticTextKeyBE();
                objBE.StaticTextId = Convert.ToInt16(Id);
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                lst = StaticTextKeyBL.GetSingleObjectDetails(Constants.USP_GetStaticTextDetails, true, objBE, "S");
                if (lst != null)
                {
                    if (lst.StaticTextId != 0)
                    {
                        txtKey.Text = lst.StaticTextKey;
                        txtValue.Text = lst.StaticTextValue;
                    }
                }
                lst = null;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails(true);
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int Id = Convert.ToInt16(Session["StaticTextId"]);
                PopulateStaticText(Id);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                StaticType = Convert.ToString(Session["StaticTextType"]);
                EditStaticId = Convert.ToUInt16(Session["StaticTextId"]);
                StaticTextKeyBE objBE = new StaticTextKeyBE();
                objBE.StaticTextId = Convert.ToInt16(EditStaticId);
                objBE.StaticTextKey = Sanitizer.GetSafeHtmlFragment(txtKey.Text.Trim());
                objBE.CreatedBy = Convert.ToInt16(UserId);
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value.ToString());
                objBE.StaticTextValue = Sanitizer.GetSafeHtmlFragment(txtValue.Text.Trim());
                objBE.IsActive = true;

                if (EditStaticId == 0 && StaticType != "Edit")
                {

                    Int16 isExist = StaticTextKeyBL.InsertStaticTextKeyDetails(Constants.USP_InsertUpdateDeleteStaticText, true, objBE, 4);
                    if (isExist == 1)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticKeyExists"), AlertType.Warning);
                        return;
                    }
                }
                int ResultA = 0;
                int MaxId = 0;
                if (StaticType != "Edit")
                {
                    int b = StaticTextKeyBL.InsertStaticTextKeyDetails(Constants.USP_InsertUpdateDeleteStaticText, true, objBE, 1);
                    if (b == 0)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning);
                        return;
                    }
                    ResultA = 1;
                    MaxId = int.Parse(b.ToString());
                }
                else
                {
                    int b = StaticTextKeyBL.InsertStaticTextKeyDetails(Constants.USP_InsertUpdateDeleteStaticText, true, objBE, 2);
                    if (b == 0)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning);
                        return;
                    }
                    ResultA = 2;
                    MaxId = int.Parse(b.ToString());
                }

                if (ResultA != 0)
                {
                    Session["StaticTextType"] = null;
                    Session["StaticTextId"] = null;
                    Button btn = sender as Button;
                    if (btn.ID == "btnSaveExit")
                    {
                        if (ResultA == 1)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticKeyInsert"), Host + "Admin/StaticTextManagement/StaticTextListing.aspx", AlertType.Success);
                            CreateActivityLog("ManageStaticText btnSaveExit", "Update", "");
                        }
                        else if (ResultA == 2)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticKeyUpdate"), Host + "Admin/StaticTextManagement/StaticTextListing.aspx", AlertType.Success);
                            CreateActivityLog("ManageStaticText btnSaveExit", "Update", "");
                        }
                    }
                    else if (btn.ID == "btnSaveAddAnother")
                    {
                        if (ResultA == 1)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticKeyInsert"), Host + "Admin/StaticTextManagement/ManageStaticText.aspx", AlertType.Success);
                            CreateActivityLog("ManageStaticText btnSaveAddAnother", "Update", "");
                        }
                        else if (ResultA == 2)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticKeyUpdate"), Host + "Admin/StaticTextManagement/ManageStaticText.aspx", AlertType.Success);
                            CreateActivityLog("ManageStaticText btnSaveAddAnother", "Update", "");
                        }
                    }
                }
                else
                { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Host + "Admin/StaticTextManagement/StaticTextListing.aspx");
        }
    }
}