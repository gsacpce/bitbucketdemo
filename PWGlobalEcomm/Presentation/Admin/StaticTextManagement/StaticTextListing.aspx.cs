﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_StaticTextManagement_StaticTextListing : BasePage//System.Web.UI.Page
    {
        int UserId;
        int LanguageId;
        public string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.GridView gvStaticText;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LanguageId = GlobalFunctions.GetLanguageId();
                if (!IsPostBack)
                {
                    if (Session["StoreUser"] != null)
                    {
                        UserBE objUserBE = new UserBE();
                        objUserBE = Session["StoreUser"] as UserBE;
                        UserId = objUserBE.UserId;
                    }
                    PopulateStaticText();
                    Session["StaticTextType"] = null;
                    Session["StaticTextId"] = null;
                }
                #region Meta Data
                Page.Title = "Static Text Listing";
                Page.MetaDescription = "Static Text";
                Page.MetaKeywords = "statictext";
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateStaticText()
        {
            try
            {
                List<StaticTextKeyBE> lst = new List<StaticTextKeyBE>();
                StaticTextKeyBE objBE = new StaticTextKeyBE();
                objBE.LanguageId = Convert.ToInt16(LanguageId);
                lst = StaticTextKeyBL.GetAllKeys(Constants.USP_GetStaticTextDetails, objBE, "A");
                if (lst != null)
                {
                    if (lst.Count > 0)
                    {
                        gvStaticText.DataSource = lst;
                        gvStaticText.DataBind();
                    }
                }
                else
                {
                    gvStaticText.DataSource = null;
                    gvStaticText.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(Host + "Admin/StaticTextManagement/ManageStaticText.aspx");
        }

        protected void gvStaticText_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkbtnDelete = e.Row.FindControl("lnkbtnDelete") as LinkButton;
                    HtmlGenericControl dvAction = e.Row.FindControl("dvAction") as HtmlGenericControl;
                    dvAction.Attributes.Add("class", "view_order other_option");
                    lnkbtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete this key \"" + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "StaticTextKey")) + "\"?')");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void gvStaticText_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvStaticText.PageIndex = e.NewPageIndex;
                PopulateStaticText();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lbtnEditDelete_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditKey")
                {
                    Session["StaticTextId"] = Convert.ToInt32(e.CommandArgument);
                    Session["StaticTextType"] = "Edit";
                    Response.Redirect(Host + "Admin/StaticTextManagement/ManageStaticText.aspx");
                }
                else if (e.CommandName == "DeleteKey")
                {
                    int Result = 0;
                    int Id = Convert.ToInt32(e.CommandArgument);
                    StaticTextKeyBE objBE = new StaticTextKeyBE();
                    objBE.LanguageId = Convert.ToInt16(LanguageId);
                    objBE.CreatedBy = Convert.ToInt16(UserId);
                    objBE.StaticTextId = Convert.ToInt16(Id);
                    objBE.IsActive = true;
                    Result = StaticTextKeyBL.DeleteStaticTextKeyDetails(Constants.USP_InsertUpdateDeleteStaticText, true, objBE,Convert.ToInt16(DBAction.Delete));
                    if (Result != 0)
                    {
                        PopulateStaticText();
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticKeyDelete"), AlertType.Success);
                        CreateActivityLog("StaticTextListing","Delete",Convert.ToString(Id));
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}