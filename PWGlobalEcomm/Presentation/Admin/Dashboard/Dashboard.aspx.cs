﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_Dashboard_Dashboard : System.Web.UI.Page
    {
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvDashboard;
        UserBE objUserBE;
        List<MenuBE> lstMenuBE;
        public string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["StoreUser"] == null)
                Response.Redirect(hostAdmin + "Login/Login.aspx", true);
            else
            {
                BindMenu();
            }

        }


        private void BindMenu()
        {
            objUserBE = new UserBE();
            objUserBE = Session["StoreUser"] as UserBE;
            lstMenuBE = MenuBL.GetAllStoreMenuDetails(objUserBE.RoleId,false);
            if(lstMenuBE != null)
            {
                if(lstMenuBE.Count>0)
                {
                    Panel objPanel = null;
                    Literal objLiteral = null;
                    lstMenuBE.Remove(lstMenuBE.Find(x => x.MenuName.ToLower() == "dashboard"));
                    lstMenuBE.Remove(lstMenuBE.Find(x => x.MenuName.ToLower() == "logout"));
                    List<MenuBE> lstParentMenu = new List<MenuBE>();
                    lstParentMenu = lstMenuBE.FindAll(x => x.ParentMenuId == 0);

                    //StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    //MenuBE objViewFrontEnd = new MenuBE();
                    //objViewFrontEnd.MenuId = Convert.ToInt16((lstMenuBE.OrderByDescending(x => x.MenuId).ToList()[0]).MenuId + 1);
                    //objViewFrontEnd.MenuName = "View FrontEnd";
                    //objViewFrontEnd.ParentMenuId = 0;
                    //objViewFrontEnd.MenuUrl = objStoreBE.StoreLink;
                    //objViewFrontEnd.Level = 0;
                    //objViewFrontEnd.IconName = "dashboard-icon";
                    //objViewFrontEnd.Sequence = 1;
                    //objViewFrontEnd.IsVisible = true;
                    //objViewFrontEnd.IsActive = true;
                    //lstParentMenu.Add(objViewFrontEnd);

                    #region Bind Google Analytics
                    //objLiteral = new Literal();
                    //objLiteral.Text = "<span><img src='" + hostAdmin + "Images/UI/googleAnalytic.jpg" + "' alt='Google Analytics'/></span>";
                    ////src=\"" + Host + "Images/googleAnalytic.jpg"
                    //objPanel = new Panel();
                    //objPanel.CssClass = "googlemain block";

                    //objPanel.Controls.Add(objLiteral);
                    //BindGoogleAnalytic(objPanel);
                    //dvDashboard.Controls.Add(objPanel);
                    #endregion
                    if (lstParentMenu!=null)
                    {
                        if(lstParentMenu.Count>0)
                        {
                            foreach (var menuitem in lstParentMenu)
                            {
                                objLiteral = new Literal();
                                if (menuitem.MenuUrl.ToLower().Contains("javascript") || menuitem.MenuUrl.ToLower().Contains("#") || string.IsNullOrEmpty(menuitem.MenuUrl))
                                    objLiteral.Text = "<span class='titledivcase'><a href='javascript:void(0)'>"+menuitem.MenuName+"</a></span>";
                                else
                                    objLiteral.Text = "<span class='titledivcase'><a href="+hostAdmin+menuitem.MenuUrl+">" + menuitem.MenuName + "</a></span>";

                                
                                Literal objChildLiteral = new Literal();
                                List<MenuBE> lstChildMenu = lstMenuBE.FindAll(x => x.ParentMenuId == menuitem.MenuId);
                                StringBuilder sb = new StringBuilder();
                                if(lstChildMenu!=null)
                                {
                                    if(lstChildMenu.Count>0)
                                    {
                                        sb.Append("<ul>");
                                        foreach (var childmenuitem in lstChildMenu)
                                        {
                                            sb.Append("<li>");
                                            sb.Append("<a href=" + hostAdmin + childmenuitem.MenuUrl + ">"+childmenuitem.MenuName+"</a>");
                                            sb.Append("</li>");

                                        }
                                        sb.Append("</ul>");
                                    }
                                }
                                Literal countLiteral = new Literal();
                                if(menuitem.MenuName.ToLower() == "products")
                                    countLiteral.Text="<span class='countsother'>" + Convert.ToString(ProductBL.GetProductCount()) + "</span>";
                                if(menuitem.MenuName.ToLower() == "customers")
                                    countLiteral.Text="<span class='countsother'>" + Convert.ToString(UserBL.GetCustomerCount()) + "</span>";
                                objPanel = new Panel();
                                objPanel.Controls.Add(objLiteral);
                                objChildLiteral.Text=sb.ToString();
                                objPanel.Controls.Add(objChildLiteral);
                                objPanel.Controls.Add(countLiteral);
                                objPanel.CssClass = "block";
                                dvDashboard.Controls.Add(objPanel);
                            }
                        }
                    }
                    #region Bind Google Analytics
                    objLiteral = new Literal();
                    objLiteral.Text = ""; //<span class='titledivcase'><a href='javascript:void(0)'>Admin Comments</a></span>
                    // Not Required commented by vikram to show only one admin comment from database not from hardcoded
                    objPanel = new Panel();
                    //objPanel.CssClass = "block";
                    objPanel.Controls.Add(objLiteral);
                    BindGoogleAnalytic(objPanel);
                    dvDashboard.Controls.Add(objPanel);
                    #endregion
                }
            }
        }

        private void BindGoogleAnalytic(Panel objPanel)
        {
            //throw new NotImplementedException();
        }
    }
}