﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using System.IO;


namespace Presentation
{
    public class Admin_BrandColorManagement : BasePage //System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.DropDownList ddlBrandColorMaster, ddlFontStyle;
        //    ddlWidthUnit;
        protected global::System.Web.UI.WebControls.TextBox txtDisplayText;
        protected global::System.Web.UI.WebControls.TextBox txtHexCode;


        protected global::System.Web.UI.WebControls.CheckBox chkIsActive;
        protected global::System.Web.UI.WebControls.Button btnSave;
        protected global::System.Web.UI.WebControls.Button btnCancel;

        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnColorHexCode;

        protected string AdminHost = GlobalFunctions.GetVirtualPathAdmin();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //  PopulateBrandColorMaster();
                // PopulateFontStyle();
                try
                {

                    if (Session["EditBrandColor"] != null)
                    {
                        BrandColorManagementBE EditBrandColor = Session["EditBrandColor"] as BrandColorManagementBE;

                        txtDisplayText.Text = EditBrandColor.ColorName;
                        txtHexCode.Text = EditBrandColor.ColorHexCode;
                        chkIsActive.Checked = EditBrandColor.IsActive;
                        hdnColorHexCode.Value = EditBrandColor.ColorHexCode;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }

        }


                
        protected void Btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            switch (btn.CommandArgument)
            {
                case "Save":
                    {
                        SaveBrandColor();
                        break;
                    }
                case "Cancel":
                    {
                        Response.Redirect(AdminHost + "BrandColorManagement/BrandColorListing.aspx");
                        break;
                    }
                default:
                    break;
            }
        }




        private bool SaveBrandColor()
        {
            bool flgSuccess = true;

            try
            {
                #region Validate
                if (txtDisplayText.Text.Trim() == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Display Text can not be empty !!", AlertType.Warning);
                    return false;
                }
                if (txtHexCode.Text.Trim() == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Hex colour code can not be empty !!", AlertType.Warning);
                    return false;
                }
                #endregion
                if (Page.IsValid)
                {
                    BrandColorManagementBE objBrandColor = new BrandColorManagementBE();
                    objBrandColor.ColorName = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtDisplayText.Text.Trim().Replace('\'', '@'));
                    objBrandColor.ColorHexCode = txtHexCode.Text.Trim();
                    objBrandColor.IsActive = chkIsActive.Checked;
                    if (Session["StoreUser"] != null)
                    {
                        UserBE User = Session["StoreUser"] as UserBE;
                        objBrandColor.CreatedBy = User.UserId;
                    }
                    if (Session["EditBrandColor"] != null && Session["EditOperation"].ToString().Equals("Update"))
                    {
                        objBrandColor.ColorHexCode = (Session["EditBrandColor"] as BrandColorManagementBE).ColorHexCode;
                        objBrandColor.BrandColorId = (Session["EditBrandColor"] as BrandColorManagementBE).BrandColorId;
                        objBrandColor.ExColorCode = hdnColorHexCode.Value;
                        if (objBrandColor.ColorHexCode == Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtHexCode.Text.Trim().Replace('\'', '@')))
                        {
                            if (BrandColorManagementBL.ManageBrandColor_AED(objBrandColor, DBAction.Update))
                            {
                                UpdateModifiedCSS();
                                CreateActivityLog("Brand Color", "Updated", objBrandColor.BrandColorId + " " + Convert.ToString(txtDisplayText.Text));
                                ClearControls();
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Brand Colour details updated successfully!", "BrandColorListing.aspx", AlertType.Success);
                            }
                        }
                        else
                        {
                            if (!CheckHexColorCodeExist(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtHexCode.Text.Trim().Replace('\'', '@'))))
                            {
                                objBrandColor.ColorHexCode = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtHexCode.Text.Trim().Replace('\'', '@'));
                                if (BrandColorManagementBL.ManageBrandColor_AED(objBrandColor, DBAction.Update))
                                {
                                    UpdateModifiedCSS();
                                    CreateActivityLog("Brand Color", "Updated", objBrandColor.BrandColorId + " " + Convert.ToString(txtDisplayText.Text));
                                    ClearControls();
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Brand Colour details updated successfully!", "BrandColorListing.aspx", AlertType.Success);
                                }
                            }
                            else
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Hex colour code already taken!", AlertType.Failure);
                            }
                        }
                    }
                    else
                    {
                        if (!CheckHexColorCodeExist(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtHexCode.Text.Trim().Replace('\'', '@'))))
                        {
                            objBrandColor.ExColorCode = "";
                            if (BrandColorManagementBL.ManageBrandColor_AED(objBrandColor, DBAction.Insert))
                            {
                                CreateActivityLog("Brand Color", "Added", objBrandColor.BrandColorId + " " + Convert.ToString(txtDisplayText.Text));
                                ClearControls();
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Brand Colour details updated successfully!", "BrandColorListing.aspx", AlertType.Success);

                            }
                        }
                        else
                        {

                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Hex colour code already taken!", AlertType.Failure);
                        }
                    }
                }



            }

            catch (Exception ex)
            {
                flgSuccess = false;
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            finally
            {

            }


        Exit:

            return flgSuccess;
        }

        private void ClearControls()
        {
            txtHexCode.Text = "";
            txtDisplayText.Text = "";
            chkIsActive.Checked = false;


        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 02-09-15
        /// Scope   : to check whether the color hex code exist in the db or not
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        private bool CheckHexColorCodeExist(string HexColorCode)
        {

            List<BrandColorManagementBE> BrandColors = new List<BrandColorManagementBE>();
            BrandColors = BrandColorManagementBL.GetAllBrandColors();

            if (BrandColors != null)
            {
                if (BrandColors.Count() > 0)
                {
                    int count = BrandColors.Count(x => x.ColorHexCode == HexColorCode);
                    return count > 0 ? true : false;
                }
            }
            return false;
        }


        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 02-09-15
        /// Scope   : to check modify the css when the code changes
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        private void UpdateModifiedCSS()
        {
            //create an object of brand color 
            BrandColorManagementBE objBrandColorBE = new BrandColorManagementBE();

            try
            {

                string strFileContent = "";
                string strFileNameModifiedCSS = "modified.css"; //FileName to be backed up and changed

                string strBackupFileName = "modified" + "_" + DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second + "_Edited_from_BrandColorManagement";
                string strFileExtension = "";

                string strFilePathModifiedCSS = GlobalFunctions.GetPhysicalFolderPath() + "CSS/" + strFileNameModifiedCSS;

                if (File.Exists(strFilePathModifiedCSS))
                {

                    strFileExtension = Path.GetExtension(strFilePathModifiedCSS);
                    string strBackupTargetPath = GlobalFunctions.GetPhysicalFolderPath() + "Backup/ModifiedCSS/" + strBackupFileName + strFileExtension;
                    try
                    {
                        //File.Move(strFilePathModifiedCSS, strBackupTargetPath);
                        File.Copy(strFilePathModifiedCSS, strBackupTargetPath, true);
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error Updating Theme", AlertType.Failure);
                    }

                    strFileContent = File.ReadAllText(strFilePathModifiedCSS);
                    strFileContent = strFileContent.Replace(hdnColorHexCode.Value, txtHexCode.Text);

                    if (File.Exists(strFilePathModifiedCSS))
                    {
                        try
                        {
                            File.Delete(strFilePathModifiedCSS);
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Error while deleting file Modified.css", AlertType.Failure);
                        }
                    }
                    try
                    {
                        File.WriteAllText(strFilePathModifiedCSS, strFileContent);
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Error Updating Theme", AlertType.Failure);
                        return;
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Modified.css file does not exists.", AlertType.Failure);
                    return;
                }


                //GlobalFunctions.ShowModalAlertMessages(this.Page, "Theme Saved Successfully", "BrandColorListing.aspx", AlertType.Success);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Error Updating Theme", AlertType.Failure);
            }
        }


    }
}

