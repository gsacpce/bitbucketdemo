﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI;

namespace Presentation
{
    public class Admin_BrandColorListing : BasePage //System.Web.UI.Page
    {
        
        
        protected global::System.Web.UI.WebControls.Button btnAddNew;
        protected global::System.Web.UI.WebControls.GridView gvBrandColor;

        protected global::System.Web.UI.WebControls.LinkButton lnkbtnUpdate;
        protected global::System.Web.UI.WebControls.LinkButton lnkbtnDelete;

        
        

        public string Host = GlobalFunctions.GetVirtualPath();
        List<BrandColorManagementBE> lstBrandColors = new List<BrandColorManagementBE>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAllBrandColors();
            }
            
        }

        private void GetAllBrandColors()
        {
            try
            {
                lstBrandColors = BrandColorManagementBL.GetAllBrandColors();
                Session["BrandColors"] = lstBrandColors;
                gvBrandColor.DataSource = lstBrandColors;
                gvBrandColor.DataBind();
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void Btn_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = sender as Button;

                LinkButton lnkBtn = sender as LinkButton;

                if (btn != null)
                    switch (btn.CommandName.ToLower())
                    {
                        case "add":
                            {
                                Session["EditBrandColor"] = null;
                                Response.Redirect("BrandColorManagement.aspx");
                                break;
                            }
                        case "cancel":
                            {
                                break;
                            }
                        default:
                            break;
                    }
                if (lnkBtn != null)
                    switch (lnkBtn.CommandName.ToLower())
                    {
                        case "update":
                            {
                                #region View for Update
                                BrandColorManagementBE EditBrandColor = new BrandColorManagementBE();

                                //(Session["BrandColors"] as List<object>).Find(x => (x as BrandColorManagementBE).BrandColorId == Convert.ToInt16(lnkBtn.CommandArgument));
                                if (Session["BrandColors"] != null)
                                {
                                    foreach (var item in (Session["BrandColors"] as List<BrandColorManagementBE>))
                                    {
                                        if (item.BrandColorId == Convert.ToInt16(lnkBtn.CommandArgument))
                                        {
                                            EditBrandColor = ((BrandColorManagementBE)item);
                                        }
                                    }
                                    Session["EditBrandColor"] = EditBrandColor;
                                    Session["EditOperation"] = lnkBtn.CommandName;
                                    Response.Redirect("BrandColorManagement.aspx");
                                }

                                break;
                                #endregion
                            }
                        case "delete":
                            {
                                #region Delete
                                BrandColorManagementBE EditBrandColor = new BrandColorManagementBE();
                                foreach (var item in (Session["BrandColors"] as List<BrandColorManagementBE>))
                                {
                                    if (item.BrandColorId == Convert.ToInt16(lnkBtn.CommandArgument))
                                    {
                                        EditBrandColor = ((BrandColorManagementBE)item);
                                    }
                                }
                                Session["EditBrandColor"] = EditBrandColor;
                                Session["EditOperation"] = lnkBtn.CommandName;

                                if (Session["EditBrandColor"] != null && Session["EditOperation"].ToString().Equals("Delete"))
                                {
                                    BrandColorManagementBE objImageBrandColor = Session["EditBrandColor"] as BrandColorManagementBE;
                                    if (BrandColorManagementBL.ManageBrandColor_AED(objImageBrandColor, DBAction.Delete))
                                    {
                                        CreateActivityLog("Brand Color List", "Deleted",EditBrandColor.BrandColorId + " " + EditBrandColor.ColorName);
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "BrandColor deleted successfully!", AlertType.Success);
                                        Response.Redirect("BrandColorListing.aspx");
                                    }

                                }

                                break;
                                #endregion
                            }


                        default:
                            break;
                    }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        

        protected void gvBrandColor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int NewIndex = e.NewPageIndex;
                gvBrandColor.PageIndex = NewIndex;
                gvBrandColor.DataSource = BrandColorManagementBL.GetAllBrandColors();
                gvBrandColor.DataBind();
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void gvBrandColor_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
           // Response.Write("Delete handled");
        }
        protected void gvBrandColor_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkbtnDelete = (LinkButton)e.Row.FindControl("lnkbtnDelete");
                    bool IsActive = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsActive"));
                    if (IsActive)
                    {
                        lnkbtnDelete.Visible = true;
                    }
                    else
                    {
                        lnkbtnDelete.Visible = false;
                    }
                    HtmlInputHidden hdnColorCode = (HtmlInputHidden)e.Row.FindControl("hdnColorCode");
                    HtmlGenericControl SpanColor = (HtmlGenericControl)e.Row.FindControl("SpanColor");
                    SpanColor.Attributes["style"] = "background-color:" + hdnColorCode.Value.Trim();
                    //+ ";margin-right:25%;";
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
       
    }
}
