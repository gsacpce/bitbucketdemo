﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CKEditor.NET;
using System.Threading;
using System.Configuration;

namespace Presentation
{
    public class Admin_Category_AddEditCategory : BasePage //System.Web.UI.Page
    {
        public string ImageSize = GlobalFunctions.GetSetting("CategoryImageSize");
        public string ImageCategoryId = "0";
        public string host = GlobalFunctions.GetVirtualPath();
        public string BannerSize = GlobalFunctions.GetSetting("CategoryBannerSize");
        public string CategoryExtension = GlobalFunctions.GetSetting("CategoryFormatExtension");
        public string BannerExtension = GlobalFunctions.GetSetting("BannerFormatExtension");
        public string TempCategoryId = "";
        public string TempBannerId = "";
        public string TempIdforCategoryMenu = "";
        public string TempIdCategoryMouseOver = "";
        public string BannerHtml = "";
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidCategorySrc,hidcategoryMenusrc, hideBannerSrc,hidcategorymouseover, HIDCatId,HIDCatMenuId,HIDCatMouserover, HIDBanId, hdnCategoryId, hdnActionType;
        protected global::System.Web.UI.WebControls.Literal ltlHeading;
        protected global::System.Web.UI.WebControls.DropDownList ddlParentCategory, ddlLanguage;
        //protected global::System.Web.UI.WebControls.TextBox txtDescription;
        protected global::CKEditor.NET.CKEditorControl txtDescription;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divLang;
        protected global::System.Web.UI.WebControls.Literal ltrMessage;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divCategoryMenuImages;
        protected global::System.Web.UI.WebControls.TextBox txtCategoryName, txtBanner, txtMetaDescription, txtMetaKeyWords, txtPageTitle,txtBannerLink;
        CategoryBE objCategoryBE = new CategoryBE();

        string CategoryKey = Convert.ToString(ConfigurationManager.AppSettings["CategoryKey"]);
        public int updLanguageid;

        #region UI
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    TempCategoryId = Guid.NewGuid().ToString();
                    HIDCatId.Value = TempCategoryId;

                    TempIdforCategoryMenu = Guid.NewGuid().ToString();
                    HIDCatMenuId.Value = TempIdforCategoryMenu;

                    TempIdCategoryMouseOver = Guid.NewGuid().ToString();
                    HIDCatMouserover.Value = TempIdCategoryMouseOver;

                    TempBannerId = Guid.NewGuid().ToString();
                    HIDBanId.Value = TempBannerId;

                    PopulatetDropDownList();
                    PopulateLanguageDropDownList();

                    txtDescription.config.toolbar = new object[]
			    {
                    new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				    new object[] { "NumberedList", "BulletedList"},
				    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				    "/",
				    new object[] { "Styles", "Format", "Font", "FontSize" },
				    new object[] { "TextColor", "BGColor" },
                    "/",
                                    new object[] { "Link", "Unlink"},
                    new object[] { "Table", "HorizontalRule"}
			    };

                    if (Request.QueryString["Type"] != null)
                    {
                        if (Request.QueryString["Type"].ToString() == "A")
                        {
                            ltlHeading.Text = "Add New Category";

                            if (Request.QueryString["id"] != null)
                            {

                                foreach (ListItem lt in ddlParentCategory.Items)
                                {
                                    if (lt.Attributes["rel"] == Request.QueryString["id"].ToString())
                                    {
                                        ddlParentCategory.ClearSelection();
                                        ddlParentCategory.Items.FindByValue(lt.Value).Selected = true;
                                        ddlParentCategory.Enabled = false;
                                    }
                                }
                            }
                        }
                        else if (Request.QueryString["Type"].ToString() == "E")
                        {
                            ltlHeading.Text = "Edit Category";
                            ddlParentCategory.Enabled = false;

                            divLang.Visible = true;

                            if (Request.QueryString["id"] != null && Request.QueryString["language-id"] != null)
                            {
                                Int16 ParentCategoryId = 0;
                                ParentCategoryId = Convert.ToInt16(Request.QueryString["id"]);
                                TempBannerId = Convert.ToString(Request.QueryString["id"]);
                                TempCategoryId = Convert.ToString(Request.QueryString["id"]);
                                
                                if (Request.QueryString["pid"] != null)
                                {
                                    ParentCategoryId = Convert.ToInt16(Request.QueryString["pid"]);
                                    objCategoryBE.ParentCategoryId = ParentCategoryId;
                                    objCategoryBE.CategoryId = Convert.ToInt16(Request.QueryString["id"]);
                                }
                                
                                GetCategoryDetails(Convert.ToInt16(Request.QueryString["id"]), Convert.ToInt16(Convert.ToString(Request.QueryString["language-id"])), ParentCategoryId);
                            }
                        }
                    }
                }
                //IsShowCategoryMenuImageContainer();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void IsShowCategoryMenuImageContainer()
        {
            if (CategoryKey == "Image")
            {
                if (Request.QueryString["pid"] != null)
                {
                    if (Convert.ToInt32(Request.QueryString["pid"]) > 0)
                        divCategoryMenuImages.Attributes.Add("style", "diplay:none;");
                    else
                        divCategoryMenuImages.Attributes.Add("style", "diplay:block;");
                }
                else
                {
                    divCategoryMenuImages.Attributes.Add("style", "diplay:none;");
                }
            }
            else
            {
                divCategoryMenuImages.Attributes.Add("style", "diplay:none;");
            }
        }

        private bool IsRecordsaved(string[] CategoryIds)
        {
            bool res = false;
            try
            {
                CategoryBE category = new CategoryBE();
                category.ImageExtension = SetExtension(hidCategorySrc.Value);
                category.MenuImageExtension = SetExtension(hidcategoryMenusrc.Value);
                category.BannerImageExtension = SetExtension(hideBannerSrc.Value);
                category.MenuMouseoverExtension = SetExtension(hidcategorymouseover.Value);
                category.IsActive = true;
                category.CategoryName = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtCategoryName.Text.Trim());

                if (Request.QueryString["id"] != null)
                {
                    category.CategoryId = Convert.ToInt16(Request.QueryString["id"]);
                    hdnCategoryId.Value = Convert.ToString(category.CategoryId);
                }
                category.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                if (Request.QueryString["pid"] != null)
                {
                    category.ParentCategoryId = Convert.ToInt16(Request.QueryString["pid"]);
                }
                else
                {
                    category.ParentCategoryId = Convert.ToInt16(CategoryIds[1]);
                }

                category.DescriptionText = GlobalFunctions.RemoveSanitisedPrefixes(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtDescription.Text.Trim()));
                category.BannerText = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtBanner.Text.Trim());
                category.BannerLink = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtBannerLink.Text.Trim());
                category.MetaTitle = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtPageTitle.Text.Trim());
                category.MetaDescription = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtMetaDescription.Text.Trim());
                category.MetaKeywords = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtMetaKeyWords.Text.Trim());

                if (ddlParentCategory.SelectedIndex > 0)
                {
                    if (Request.QueryString["pid"] != null)
                    {
                        if (Request.QueryString["id"] != null)
                        {
                            category.RelatedCategoryId = Convert.ToInt16(Request.QueryString["id"]);
                        }
                    }
                    else
                    {
                        category.RelatedCategoryId = Convert.ToInt16(CategoryIds[2]);
                        category.CategoryId = Convert.ToInt16(CategoryIds[0]);
                    }
                }
                int IsExistCount = 0;
                if (CategoryIds[1] == "0")
                {
                    category.RelatedCategoryId = Convert.ToInt16(CategoryIds[2]);
                }
                if (Convert.ToString(Request.QueryString["Type"]) == "E")
                {
                    category.Action = "Edit";
                }
                else
                {
                    category.Action = "Add";
                    IsExistCount = CheckCategoryExists(category.CategoryName, int.Parse(ddlParentCategory.SelectedValue.Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries).AsEnumerable().ElementAt(2)));
                }
                #region "Comment"
                //if (Request.QueryString["Type"] == "E")
                //{
                //    // category exist and checking if its name is same as its in update
                //    if (category.CategoryName == Convert.ToString(Session["CategoryName"]))
                //    {
                //        IsExistCount = 0;
                //    }
                //    else
                //    {
                //        IsExistCount = CheckCategoryExists(category.CategoryName, int.Parse(ddlParentCategory.SelectedValue.Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries).AsEnumerable().ElementAt(2)));
                //    }
                //    if (IsExistCount == 0)
                //    {
                //        if (Request.QueryString["Id"] != null)
                //        {
                //            category.CategoryId = Convert.ToInt16(hdnCategoryId.Value);
                //            category.Action = "Edit";
                //            int Id = CategoryBL.InsertCategory(category);
                //            SetImageName(category.ImageExtension, "category", Id);
                //            SetImageName(category.BannerImageExtension, "banner", Id);
                //            if (Id == -1)
                //            {
                //                divMessage.InnerText = "Please remove the products before you create a subcategory";
                //                divMessage.Visible = true;
                //            }
                //            if (e.CommandName == "btnSaveExit")
                //            {
                //                Response.Redirect(host + "Admin/Category/CategoryManagement.aspx");
                //            }
                //            else if (e.CommandName == "btnSaveAssign")
                //            {
                //                Int16 TempId = Convert.ToInt16(Request.QueryString["Id"]);
                //                List<CategoryBE> categories = CategoryBL.GetAllCategories().ToList();
                //                CategoryBE Category = categories.Where(c => c.CategoryId == TempId).FirstOrDefault();
                //                if (Request.QueryString["pid"] != null)
                //                {
                //                    Int16 ParentId = 0;
                //                    ParentId = Convert.ToInt16(Request.QueryString["pid"]);
                //                    Int16 GrandParentId = categories.Where(c => c.CategoryId == ParentId).FirstOrDefault().ParentCategoryId;
                //                    if (Category.CategoryId == Id && Category.SubCategoryId == Id && Category.SubSubCategoryId == Id)
                //                    {
                //                        Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + Category.CategoryId);
                //                    }
                //                    else if (Category.ParentCategoryId == ParentId && Category.SubCategoryId == ParentId && Category.SubSubCategoryId == Id)
                //                    {
                //                        if (GrandParentId != 0)
                //                        {
                //                            if (Category.ParentCategoryId != GrandParentId && Category.SubCategoryId == ParentId && Category.SubSubCategoryId == Id)
                //                            {
                //                                Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + GrandParentId + "&scid=" + ParentId + "&sscid=" + Id);
                //                            }
                //                        }
                //                        else
                //                        {
                //                            Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + ParentId + "&scid=" + Id);
                //                        }
                //                    }
                //                }
                //                else
                //                {
                //                    Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + Category.CategoryId);
                //                }
                //            }
                //            else if (e.CommandName == "btnSaveAdd")
                //            {
                //                Response.Redirect(host + "Admin/Category/AddEditCategory.aspx");
                //            }
                //        }
                //        else
                //        {
                //            divMessage.InnerText = "Category name already exist";
                //            divMessage.Visible = true;
                //        }
                //    }
                //}
                //else if (Request.QueryString["Type"] == "A")
                //{
                //    if (IsExistCount > 0)
                //    {
                //        divMessage.InnerText = "Category name already exist";
                //        divMessage.Visible = true;
                //    }
                //    else
                //    {
                //        if (hdnCategoryId.Value == "")
                //        {
                //            category.CategoryId = 0;
                //        }
                //        else
                //        {
                //            category.CategoryId = Convert.ToInt16(hdnCategoryId.Value);
                //        }
                //        category.Action = "Add";
                //        int Id = CategoryBL.InsertCategory(category);
                //        SetImageName(category.ImageExtension, "category", Id);
                //        SetImageName(category.BannerImageExtension, "banner", Id);
                //        if (Id == -1)
                //        {
                //            divMessage.InnerText = "Please remove the products before you create a subcategory";
                //            divMessage.Visible = true;
                //        }
                //        else if (Id > 0)
                //        {
                //            Session["CategoryName"] = category.CategoryName;
                //            divMessage.InnerText = "Saved successfully";
                //            if (e.CommandName == "btnSaveExit")
                //            {
                //                Response.Redirect(host + "Admin/Category/CategoryManagement.aspx");
                //            }
                //            else if (e.CommandName == "btnSaveAssign")
                //            {
                //                if (Request.QueryString["Type"] == "A")
                //                {
                //                    Int16 TempId = Convert.ToInt16(Request.QueryString["Id"]);
                //                    List<CategoryBE> categories = CategoryBL.GetAllCategories().ToList();
                //                    CategoryBE Category = categories.Where(c => c.CategoryId == Id).FirstOrDefault();
                //                    Int16 ParentId = Category.ParentCategoryId;
                //                    Int16 GrandParentId = categories.Where(c => c.CategoryId == Category.ParentCategoryId).FirstOrDefault().ParentCategoryId;
                //                    if (Request.QueryString["Id"] != null)
                //                    {
                //                        if (Category.CategoryId == Id && Category.SubCategoryId == Id && Category.SubSubCategoryId == Id)
                //                        {
                //                            Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + Category.CategoryId);
                //                        }
                //                        else if (Category.ParentCategoryId == ParentId && Category.SubCategoryId == ParentId && Category.SubSubCategoryId == Id)
                //                        {
                //                            if (GrandParentId != 0)
                //                            {
                //                                if (Category.ParentCategoryId != GrandParentId && Category.SubCategoryId == ParentId && Category.SubSubCategoryId == Id)
                //                                {
                //                                    Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + GrandParentId + "&scid=" + ParentId + "&sscid=" + Id);
                //                                }
                //                            }
                //                            else
                //                            {
                //                                Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + ParentId + "&scid=" + Id);
                //                            }
                //                        }
                //                    }
                //                }
                //                else if (Request.QueryString["Type"] == null)
                //                {
                //                    Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + Id);
                //                }
                //            }
                //            else if (e.CommandName == "btnSaveAdd")
                //            {
                //                Response.Redirect(host + "Admin/Category/AddEditCategory.aspx");
                //            }
                //        }
                //    }
                //}
                #endregion

                if (IsExistCount == 0)
                {
                    res = SaveData(category);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Category name already exist", AlertType.Warning);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return res;
        }

        protected bool SaveData(CategoryBE objCategoryBE)
        {
            try
            {
                int Id = CategoryBL.InsertCategory(objCategoryBE);

                if (Id == -1)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please remove the products before you create a subcategory", AlertType.Warning);
                }
                if (Id > 0)
                {
                        if (Request.QueryString["Type"] != "E")
                        {
                            SetImageName(objCategoryBE.MenuMouseoverExtension, "CategoryMouseOver", Id);
                            SetImageName(objCategoryBE.MenuImageExtension, "categorymenu", Id);
                            SetImageName(objCategoryBE.ImageExtension, "category", Id);
                            SetImageName(objCategoryBE.BannerImageExtension, "banner", Id);
                        }
                    Session["CategoryName"] = objCategoryBE.CategoryName;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); return false; }
        }

        // private void SaveAndAssignProducts()
        //{


        //                 if (Request.QueryString["Type"] == "E")
        //                 {
        //                     Int16 TempId = Convert.ToInt16(Request.QueryString["Id"]);
        //                     List<CategoryBE> categories = CategoryBL.GetAllCategories().ToList();
        //                     CategoryBE Category = categories.Where(c => c.CategoryId == TempId).FirstOrDefault();


        //                     if (Request.QueryString["pid"] != null)
        //                     {
        //                         Int16 ParentId = 0;
        //                         ParentId = Convert.ToInt16(Request.QueryString["pid"]);
        //                         Int16 GrandParentId = categories.Where(c => c.CategoryId == ParentId).FirstOrDefault().ParentCategoryId;
        //                         if (Category.CategoryId == Id && Category.SubCategoryId == Id && Category.SubSubCategoryId == Id)
        //                         {
        //                             Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + Category.CategoryId);
        //                         }
        //                         else if (Category.ParentCategoryId == ParentId && Category.SubCategoryId == ParentId && Category.SubSubCategoryId == Id)
        //                         {
        //                             if (GrandParentId != 0)
        //                             {
        //                                 if (Category.ParentCategoryId != GrandParentId && Category.SubCategoryId == ParentId && Category.SubSubCategoryId == Id)
        //                                 {
        //                                     Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + GrandParentId + "&scid=" + ParentId + "&sscid=" + Id);
        //                                 }
        //                             }

        //                             else
        //                             {
        //                                 Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + ParentId + "&scid=" + Id);
        //                             }

        //                         }
        //                     }
        //                     else
        //                     {
        //                         Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + Category.CategoryId);
        //                     }



        //                 }
        //                 else if (Request.QueryString["Type"] == "A")
        //                 {
        //                     Int16 TempId = Convert.ToInt16(Request.QueryString["Id"]);
        //                     List<CategoryBE> categories = CategoryBL.GetAllCategories().ToList();
        //                     CategoryBE Category = categories.Where(c => c.CategoryId == Id).FirstOrDefault();
        //                     Int16 ParentId = Category.ParentCategoryId;
        //                     Int16 GrandParentId = categories.Where(c => c.CategoryId == Category.ParentCategoryId).FirstOrDefault().ParentCategoryId;

        //                     if (Request.QueryString["Id"] != null)
        //                     {
        //                         if (Category.CategoryId == Id && Category.SubCategoryId == Id && Category.SubSubCategoryId == Id)
        //                         {
        //                             Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + Category.CategoryId);
        //                         }
        //                         else if (Category.ParentCategoryId == ParentId && Category.SubCategoryId == ParentId && Category.SubSubCategoryId == Id)
        //                         {
        //                             if (GrandParentId != 0)
        //                             {
        //                                 if (Category.ParentCategoryId != GrandParentId && Category.SubCategoryId == ParentId && Category.SubSubCategoryId == Id)
        //                                 {
        //                                     Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + GrandParentId + "&scid=" + ParentId + "&sscid=" + Id);
        //                                 }

        //                             }

        //                             else
        //                             {
        //                                 Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + ParentId + "&scid=" + Id);
        //                             }

        //                         }

        //                     }

        //                 }
        //                 else if (Request.QueryString["Type"] == null)
        //                 {
        //                     Response.Redirect(host + "Admin/Category/AssignProductCategory.aspx?cid=" + Id);
        //                 }


        //}

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int CategoryId, LanguageId;
                if (Request.QueryString["Id"] != null)
                {
                    CategoryId = Convert.ToInt16(Convert.ToString(Request.QueryString["Id"]));
                    LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue.ToString());

                    Int16 ParentCategoryId = 0;
                    ParentCategoryId = Convert.ToInt16(Request.QueryString["id"]);
                    if (Request.QueryString["pid"] != null)
                    {
                        ParentCategoryId = Convert.ToInt16(Request.QueryString["pid"]);
                    }
                    GetCategoryDetails(Convert.ToInt16(Request.QueryString["id"]), LanguageId, ParentCategoryId);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }

        }
        protected void BtnActionCommand(object sender, CommandEventArgs e)
        {
            try
            {
                string[] CategoryIds = new string[3];
                bool res = false;
                if (Page.IsValid)
                {
                    try
                    {
                        string value = ddlParentCategory.SelectedValue.ToString();
                        if (value == "0,0,0")
                        {
                            if (Request.QueryString["id"] != null)
                            {
                                value = Request.QueryString["id"] + ",0," + Request.QueryString["id"];
                            }
                        }
                        CategoryIds = value.Split(',');

                        Page.Validate("VaildCategory");
                        if (!Page.IsValid)
                        {
                            //return;
                        }
                        else
                        {
                            res = IsRecordsaved(CategoryIds);
                        }
                        if (res)
                        {
                            if (e.CommandName == "btnSaveExit")
                            {
                                if (Request.QueryString["Type"] == "E")
                                    CreateActivityLog("AddEditCategory", "Updated", Request.QueryString["id"] + "_" + txtCategoryName.Text);
                                else
                                    CreateActivityLog("AddEditCategory", "Added", txtCategoryName.Text);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Saved successfully", host + "Admin/Category/CategoryManagement.aspx", AlertType.Success);
                            }
                            else if (e.CommandName == "btnSaveAdd")
                            {
                                if (Request.QueryString["Type"] == "E")
                                    CreateActivityLog("AddEditCategory", "Updated", Request.QueryString["id"] + "_" + txtCategoryName.Text);
                                else
                                    CreateActivityLog("AddEditCategory", "Added", txtCategoryName.Text);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Saved successfully", host + "Admin/Category/AddEditCategory.aspx", AlertType.Success);
                                ClearFields();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {

                Response.Redirect(host + "Admin/Category/CategoryManagement.aspx");
            }
            catch (ThreadAbortException Te) { Exceptions.WriteExceptionLog(Te); }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ClearFields()
        {
            try
            {
                txtCategoryName.Text = "";
                txtBanner.Text = "";
                txtPageTitle.Text = "";
                txtMetaDescription.Text = "";
                txtMetaKeyWords.Text = "";
            }
            catch (ThreadAbortException Te) { Exceptions.WriteExceptionLog(Te); }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #endregion

        #region BussinessLogic

        /// <summary>
        /// Author  : Farooq Shaikh 
        /// Date    : 22-07-15
        /// Scope   : get category details 
        /// </summary>
        /// <param name="CategoryId"></param>

        /// <returns>returns CategoryBE object</returns>

        private void GetCategoryDetails(int CategoryId, int LanguageId, int ParentCategoryId)
        {
            try
            {
                CategoryBE category = new CategoryBE();
                category = CategoryBL.GetCategoryDetails(CategoryId, LanguageId);
                // category.ImageExtension = SetExtension(hidCategorySrc.Value);
                //category.BannerImageExtension = SetExtension(hideBannerSrc.Value);
                if (category != null)
                {
                    category.IsActive = true;
                    txtCategoryName.Text = category.CategoryName.Trim();
                    Session["CategoryName"] = txtCategoryName.Text;
                    ddlLanguage.SelectedValue = Convert.ToString(LanguageId);
                    if (category.ParentCategoryId == 0)
                    {
                        ddlParentCategory.Items.Insert(0, new ListItem("New Parent Category", "0,0,0"));
                        ddlParentCategory.SelectedIndex = 0;
                    }
                    else
                    {
                        if (category.ParentCategoryId == ParentCategoryId)
                        {
                            CategoryBE cat = CategoryBL.GetCategoryDetails(ParentCategoryId, LanguageId);
                            ddlParentCategory.SelectedValue = Convert.ToString(cat.CategoryRelation).Trim();
                            cat = null;
                        }
                        else
                        {
                            ddlParentCategory.SelectedValue = Convert.ToString(category.CategoryRelation).Trim();
                        }
                    }

                   
                    hdnCategoryId.Value = Convert.ToString(category.CategoryId);
                    txtDescription.Text = category.DescriptionText.Trim();
                    txtBanner.Text = category.BannerText.Trim();
                    txtBannerLink.Text = category.BannerLink.Trim();
                    txtPageTitle.Text = category.MetaTitle.Trim();
                    txtMetaDescription.Text = category.MetaDescription.Trim();
                    txtMetaKeyWords.Text = category.MetaKeywords.Trim();
                    updLanguageid = Convert.ToInt16(ddlLanguage.SelectedValue); // This language id will be only used for category menu image for language wise update
                    TempIdforCategoryMenu = Convert.ToString(Request.QueryString["id"]);
                    TempIdCategoryMouseOver = Convert.ToString(Request.QueryString["id"]);
                    if (category.BannerImageExtension != null && category.BannerImageExtension != "")
                    {
                        BannerHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + host + "Admin/Images/Category/Banner/" + category.CategoryId + category.BannerImageExtension + "?1=1376400078917\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                        hideBannerSrc.Value = host + "Admin/Images/Category/Banner/" + category.CategoryId + category.BannerImageExtension;
                    }
                    if (category.ImageExtension != null && category.ImageExtension != "")
                    {
                        CategoryHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + host + "Admin/Images/Category/" + category.CategoryId + category.ImageExtension + "?1=13764000\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                        hidCategorySrc.Value = host + "Admin/Images/Category/" + category.CategoryId + category.ImageExtension;
                    }
                    if (category.MenuImageExtension != null && category.MenuImageExtension != "")
                    {
                        //  // here image will be feteched as category_langaugeid
                        CategoryMenuHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + host + "Admin/Images/Category/categoryMenu/" + category.CategoryId + "_" + LanguageId + category.MenuImageExtension + "?1=13764000\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                        hidcategoryMenusrc.Value = host + "Admin/Images/Category/categoryMenu/" + category.CategoryId + "_" + LanguageId + category.MenuImageExtension;
                    }
                    if (category.MenuMouseoverExtension != null && category.MenuMouseoverExtension != "" )
                    {
                        // here image will be uploaded as category_MouseOver_langaugeid
                        CategoryMouseOverHtml = "<li class=\" qq-upload-success\"><img class=\"uploadedimage\" src=\"" + host + "Admin/Images/Category/MouseOver/" + category.CategoryId + "_MouseOver_" + LanguageId  +category.MenuMouseoverExtension + "?1=13764000\"><a class=\"cloaseuplod1\" onclick=\"RemoveFile(this)\" href=\"javascript:void(0);\"><img alt=\"Close\" src=\" " + host + "Admin/images/ui/removeclose.gif\"></a></li>";
                        hidcategorymouseover.Value = host + "Admin/Images/Category/MouseOver/" + category.CategoryId + "_MouseOver_" + LanguageId + category.MenuMouseoverExtension;
                    }
                }
            }
            catch (ThreadAbortException Te) { Exceptions.WriteExceptionLog(Te); }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void PopulateLanguageDropDownList()
        {
            try
            {
                List<LanguageBE> languages = new List<LanguageBE>();
                languages = LanguageBL.GetAllLanguageDetails(true);

                if (languages != null)
                {
                    if (languages.Count() > 0)
                    {
                        ddlLanguage.DataSource = languages;
                        ddlLanguage.DataTextField = "LanguageName";
                        ddlLanguage.DataValueField = "LanguageId";
                        ddlLanguage.DataBind();
                    }
                }
            }
            catch (ThreadAbortException Te) { Exceptions.WriteExceptionLog(Te); }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// to populate the drop downlist for categories and subcategories
        /// </summary>
        private void PopulatetDropDownList()
        {
            try
            {
                List<CategoryBE> Categories;
                try
                {
                    Categories = new List<CategoryBE>();
                    Categories = CategoryBL.ListCategories();

                    List<CategoryBE> ParentCategories = new List<CategoryBE>();
                    List<CategoryBE> SubChildCategories = new List<CategoryBE>();
                    if (Categories != null && Categories.Count > 0)
                    {
                        ParentCategories = Categories.FindAll(x => x.ParentCategoryId == 0);
                    }
                    else
                    {
                        ddlParentCategory.Items.Insert(0, new ListItem("New Parent Category", "0,0,0"));
                        return;
                    }
                    if (ParentCategories.Count() > 0)
                    {
                        foreach (CategoryBE ParentCategory in ParentCategories)
                        {
                            ListItem ParentList = new ListItem(ParentCategory.CategoryName, ParentCategory.CategoryRelation);
                            //ParentList.Attributes.Add("rc", Convert.ToString(ParentCategory.RelatedCategoryId));
                            ddlParentCategory.Items.Add(ParentList);

                            ddlParentCategory.Items.FindByValue(ParentCategory.CategoryRelation.ToString()).Attributes.Add("rel", ParentCategory.CategoryId.ToString());

                            List<CategoryBE> ChildCategories = Categories.FindAll(x => x.ParentCategoryId == ParentCategory.CategoryId);

                            if (ChildCategories.Count() > 0)
                            {
                                foreach (CategoryBE ChildCategory in ChildCategories)
                                {
                                    ListItem ChildList = new ListItem(HttpUtility.HtmlDecode("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + ChildCategory.CategoryName, ChildCategory.CategoryRelation);
                                    //ChildList.Attributes.Add("rc", Convert.ToString(ChildCategory.RelatedCategoryId));

                                    ddlParentCategory.Items.Add(ChildList);
                                    ddlParentCategory.Items.FindByValue(ChildCategory.CategoryRelation.ToString()).Attributes.Add("rel", ChildCategory.SubSubCategoryId.ToString());

                                    #region "comment"
                                    /*
                                    SubChildCategories = Categories.FindAll(x => x.SubCategoryId == ChildCategory.SubSubCategoryId);
                                    foreach (CategoryBE SubChildCategory in SubChildCategories)
                                    {
                                        ddlParentCategory.Items.Add(new ListItem(HttpUtility.HtmlDecode("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + SubChildCategory.CategoryName, SubChildCategory.CategoryRelation));
                                        ddlParentCategory.Items.FindByValue(SubChildCategory.CategoryRelation.ToString()).Attributes.Add("rel", SubChildCategory.CategoryId.ToString());

                                    }*/
                                    #endregion

                                }
                            }
                        }
                    }
                    else
                    {
                        ddlParentCategory.Items.Insert(0, new ListItem("New Parent Category", "0,0,0"));
                    }
                    if (!Request.QueryString.HasKeys())
                    {
                        ddlParentCategory.Items.Insert(0, new ListItem("New Parent Category", "0,0,0"));
                    }
                    Categories = null;
                    ParentCategories = null;
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            catch (ThreadAbortException Te) { Exceptions.WriteExceptionLog(Te); }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }


        /// <summary>
        /// to get the extension of file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string SetExtension(string name)
        {
            string FileExt = string.Empty;
            try
            {

                string BackLogoExt = string.Empty;
                if (name != "")
                {
                    if (name.IndexOf("?") > 0)
                    {
                        FileExt = name.Substring(name.LastIndexOf("."), (name.IndexOf("?") - name.LastIndexOf(".")));
                    }
                    else
                    {
                        FileExt = name.Substring(name.LastIndexOf("."));
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return FileExt;
        }
        /// <summary>
        /// To set the image name
        /// </summary>
        /// <param name="FileExt"></param>
        /// <param name="type"></param>
        /// <param name="CategoryId"></param>
        private void SetImageName(string FileExt, string type, int Cid)
        {
            if (FileExt != "")
            {
                StoreBE objstore = (StoreBE)HttpContext.Current.Cache["StoreDetails"];
                List<StoreBE.StoreLanguageBE> lstStoreLanguageBE = objstore.StoreLanguages;
                //Rename logo file with category id
                try
                {
                    if (type == "category")
                    {
                        DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Admin/Images/Category/"));
                        FileInfo[] infos = d.GetFiles();
                        foreach (FileInfo f in infos)
                        {
                            if (f.Name.ToString().Remove(f.Name.ToString().LastIndexOf(".")).Replace(".", "") == Convert.ToString(Cid))
                            {
                                if (File.Exists(f.FullName.ToString()))
                                {
                                    File.Delete(f.FullName.ToString());
                                }
                            }
                        }
                        File.Move(Server.MapPath("~/Admin/Images/Category/") + HIDCatId.Value + FileExt, Server.MapPath("~/Admin/Images/Category/") + Cid + FileExt);
                    }
                    else if (type == "CategoryMouseOver")
                    {
                        // here image will be uploaded as category_MouseOver_langaugeid
                        if (Request.QueryString["Type"] != null)
                        {
                            if (Request.QueryString["Type"].ToString() == "E")
                            {
                                string Newfilename = HIDCatMouserover.Value + "_" + "MouseOver"; 
                                File.Move(Server.MapPath("~/Admin/Images/Category/MouseOver/") + Newfilename + "_" + Convert.ToString(ddlLanguage.SelectedValue) + FileExt, Server.MapPath("~/Admin/Images/Category/MouseOver/") + Cid + "_MouseOver_" + Convert.ToString(ddlLanguage.SelectedValue) + FileExt);
                            }
                        }
                        else
                        {
                            #region To add image for multiple language
                            for (int i = 0; i < objstore.StoreLanguages.Count; i++)
                            {
                                string Newfilename = HIDCatMouserover.Value + "_" + "MouseOver"; ;
                                File.Move(Server.MapPath("~/Admin/Images/Category/MouseOver/") + Newfilename + "_" + lstStoreLanguageBE[i].LanguageId.ToString() + FileExt, Server.MapPath("~/Admin/Images/Category/MouseOver/") + Cid + "_MouseOver_" + lstStoreLanguageBE[i].LanguageId.ToString() + FileExt);
                            }
                            #endregion
                        }
                    }
                    else if(type=="categorymenu")
                    {
                        // here image will be uploaded as category_langaugeid
                        if (Request.QueryString["Type"] != null)
                        {
                            if (Request.QueryString["Type"].ToString() == "E")
                            {
                                string Newfilename =HIDCatMenuId.Value;
                                File.Move(Server.MapPath("~/Admin/Images/Category/categoryMenu/") + Newfilename + "_" + Convert.ToString(ddlLanguage.SelectedValue) + FileExt, Server.MapPath("~/Admin/Images/Category/categoryMenu/") + Cid + "_" + Convert.ToString(ddlLanguage.SelectedValue) + FileExt);
                            }
                        }
                        else
                        {
                            #region To add image for multiple language
                            for (int i = 0; i < objstore.StoreLanguages.Count; i++)
                            {
                                string Newfilename = HIDCatMenuId.Value;
                                File.Move(Server.MapPath("~/Admin/Images/Category/categoryMenu/") + Newfilename + "_" + lstStoreLanguageBE[i].LanguageId.ToString() + FileExt, Server.MapPath("~/Admin/Images/Category/categoryMenu/") + Cid + "_" + lstStoreLanguageBE[i].LanguageId.ToString() + FileExt);
                            }
                            #endregion
                        }
                    }
                    else if (type == "banner")
                    {
                        DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/Admin/Images/Category/Banner/"));
                        FileInfo[] infos = d.GetFiles();
                        foreach (FileInfo f in infos)
                        {
                            if (f.Name.ToString().Remove(f.Name.ToString().LastIndexOf(".")).Replace(".", "") == Convert.ToString(Cid))
                            {
                                if (File.Exists(f.FullName.ToString()))
                                {
                                    File.Delete(f.FullName.ToString());
                                }
                            }
                        }
                        File.Move(Server.MapPath("~/Admin/Images/Category/Banner/") + HIDBanId.Value + FileExt, Server.MapPath("~/Admin/Images/Category/Banner/") + Cid + FileExt);
                    }
                }
                catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            }
        }

        public int CheckCategoryExists(string CategoryName, int ParentCategoryId)
        {
            int Count = 0;
            try
            {
                Count = CategoryBL.CheckCategoryExists(CategoryName, ParentCategoryId);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Count;
        }
        #endregion

        public string CategoryHtml { get; set; }
        public string CategoryMenuHtml { get; set; }
        public string CategoryMouseOverHtml { get; set; }
    }

}
