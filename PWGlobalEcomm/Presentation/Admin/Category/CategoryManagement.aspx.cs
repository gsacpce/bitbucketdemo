﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
namespace Presentation
{
    public class Admin_Category_CategoryManagement : BasePage //System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptPCategory;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMessage;
        protected global::System.Web.UI.WebControls.Literal ltrMessage;
        protected global::System.Web.UI.WebControls.DropDownList ddlUserTypes;

        #region Declarations

        List<CategoryBE> lstCategories;

        public string Host = GlobalFunctions.GetVirtualPath();
        public Int16 _DefaultLanguage = 1;

        string AdminHost = string.Empty;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                PopulateUserTypeDropDown();
                PopulateCategory();

            }

        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            //*************************************************************************
            // Purpose : To export the category relations.
            // Inputs  : object sender, EventArgs e
            // Return  : Nothing
            //*************************************************************************
            try
            {
                List<CategoryBE> Categories = CategoryBL.GetCategoriesForReporting();


                DataTable dt = new DataTable();
                dt.Columns.Add("RelatedCategoryId");
                dt.Columns.Add("CategoryName");
                dt.Columns.Add("ParentCategoryName");
                dt.Columns.Add("DescriptionText");
                dt.Columns.Add("IsActive");
                dt.Columns.Add("MetaTitle");
                dt.Columns.Add("MetaDescription");
                dt.Columns.Add("MetaKeywords");



                foreach (CategoryBE category in Categories)
                {
                    DataRow drow = dt.NewRow();
                    drow["RelatedCategoryId"] = category.RelatedCategoryId;
                    drow["CategoryName"] = category.CategoryName;
                    drow["ParentCategoryName"] = category.ParentCategoryName;
                    drow["DescriptionText"] = GlobalFunctions.RemoveSpecialCharacters(category.DescriptionText);
                    drow["IsActive"] = category.IsActive;
                    drow["MetaTitle"] = category.MetaTitle;
                    drow["MetaDescription"] = category.MetaDescription;
                    drow["MetaKeywords"] = category.MetaKeywords;
                    dt.Rows.Add(drow);
                }

                GlobalFunctions.ExportToExcel(dt, "CategoryData");
                CreateActivityLog("Category Management List", "Exported in Excel File.", "CategoryData");
                dt = null;


            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (btn.ID == "btnSeq")
                Response.Redirect(AdminHost + "CategorySequence.aspx");
            else if (btn.ID == "btnAddNew")
                Response.Redirect(AdminHost + "AddEditCategory.aspx");
        }
        protected void chk_OnCheckedChanged(object sender, EventArgs e)
        {
            Exception ex = null;
            bool Result = false;
            CheckBox chk = (CheckBox)sender;
            int CategoryId = Convert.ToInt32(chk.Attributes["rel"]);
            string UpdatedCategoryName = "";
            foreach (RepeaterItem item in rptPCategory.Items)
            {
                Repeater rptSCategory = (Repeater)item.FindControl("rptSCategory");
                CheckBox chkStatus = (CheckBox)item.FindControl("chkStatus");
                if (Convert.ToInt32(chkStatus.Attributes["rel"]) == CategoryId)
                {
                    HtmlGenericControl spanCategoryName = (HtmlGenericControl)item.FindControl("spanCategoryName");
                    UpdatedCategoryName = Convert.ToString(spanCategoryName.InnerText);
                    SetCategoryStatus(CategoryId, chkStatus, ref Result);
                    break;
                }
                foreach (RepeaterItem items in rptSCategory.Items)
                {
                    Repeater rptSSCategory = (Repeater)items.FindControl("rptSSCategory");

                    CheckBox chkSubStatus = (CheckBox)items.FindControl("chkStatus");
                    if (Convert.ToInt32(chkSubStatus.Attributes["rel"]) == CategoryId)
                    {
                        HtmlGenericControl spansubCategoryName = (HtmlGenericControl)items.FindControl("spansubCategoryName");
                        UpdatedCategoryName = Convert.ToString(spansubCategoryName.InnerText);
                        SetCategoryStatus(CategoryId, chkSubStatus, ref Result);
                        break;
                    }
                    foreach (RepeaterItem itemss in rptSSCategory.Items)
                    {
                        HtmlGenericControl spansubsubCategoryName = (HtmlGenericControl)item.FindControl("spansubsubCategoryName");
                        // UpdatedCategoryName = Convert.ToString(spansubsubCategoryName.InnerText);
                        CheckBox chkSubSubStatus = (CheckBox)itemss.FindControl("chkStatus");
                        if (Convert.ToInt32(chkSubSubStatus.Attributes["rel"]) == CategoryId)
                        {
                            SetCategoryStatus(CategoryId, chkSubSubStatus, ref Result);
                            break;
                        }
                    }
                }
            }
            if (Result)
            {
                divMessage.Visible = true;
                //divMessage.InnerText = "Updated Successfully";
                // ltrMessage.Text = "Updated Successfully";

                CreateActivityLog("Category Management List", "Updated", Convert.ToString(CategoryId) + "_" + Convert.ToString(UpdatedCategoryName.Trim()));
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated Successfully", AlertType.Success);

                ex = new Exception();

                //Exceptions.WriteExceptionLog(ex);
                PopulateCategory();
            }
            else
            {
                ex = new Exception();
                divMessage.Visible = true;
                divMessage.Attributes.Add("class", "Errormsg");
                Exceptions.WriteExceptionLog(ex);
            }
        }



        /// <summary>
        /// 
        /// </summary>

        protected void PopulateCategory()
        {

            try
            {
                if (Convert.ToInt32(ddlUserTypes.SelectedValue) > 0)
                {
                    lstCategories = new List<CategoryBE>();
                    lstCategories = CategoryBL.GetAllCategoriesList(Convert.ToInt32(ddlUserTypes.SelectedValue));
                    if (lstCategories.Count > 0)
                    {
                        rptPCategory.DataSource = lstCategories.Where(c => c.ParentCategoryId == 0).ToList();
                        rptPCategory.DataBind();
                    }
                    else
                    {
                        rptPCategory.DataSource = null;
                        rptPCategory.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void rptPCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl spanCategoryName = e.Item.FindControl("spanCategoryName") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    Repeater rptSCategory = e.Item.FindControl("rptSCategory") as Repeater;
                    Literal ltlProductCount = e.Item.FindControl("ltlProductCount") as Literal;
                    HtmlTableRow trCategory = e.Item.FindControl("trCategory") as HtmlTableRow;


                    System.Web.UI.HtmlControls.HtmlAnchor aAddProduct = e.Item.FindControl("aAddProduct") as System.Web.UI.HtmlControls.HtmlAnchor;
                    //LinkButton lnkbtnDelete = e.Item.FindControl("lnkbtnDelete") as LinkButton;
                    //lnkbtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete this category \"" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName")) + "\"?')");

                    string catid = DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString();
                    string path = Host + "product-category/" + GlobalFunctions.RemoveSpecialCharacters(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"))) + "/" + catid + "/0/0";

                    HyperLink hrfNewSubCat = e.Item.FindControl("hrfNewSubCat") as HyperLink;
                    hrfNewSubCat.NavigateUrl = AdminHost + "AddEditCategory.aspx?id=" + catid + "&Type=A";
                    hrfNewSubCat.Visible = true;

                    Int16 CategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString());

                    List<CategoryBE> lstSubCategories = new List<CategoryBE>();
                    lstSubCategories = lstCategories.Where(c => c.ParentCategoryId == CategoryId).ToList();


                    if (lstSubCategories.Count() > 0)
                    {
                        spanCategoryName.Attributes.Add("class", "paneltab");
                        aAddProduct.Visible = false;

                        int ProdCnt = 0;
                        foreach (var category in lstSubCategories)
                        {
                            ProdCnt = ProdCnt + Convert.ToInt32(category.ProductCount);

                        }
                        ltlProductCount.Text = ProdCnt.ToString();
                        //lnkbtnDelete.Visible = false;
                        hrfNewSubCat.Visible = true;
                    }


                    else
                    {
                        path = AdminHost + "AssignProductCategory.aspx?cid=" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryId"));

                        #region Link of AddEdit Product from category Level
                        //ltlProductCount.Text = "<a href=\"" + path + "\" target=\"_blank\">" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + "</a>";
                        #endregion
                        ltlProductCount.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount"));



                        if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) == "0")
                        {
                            // lnkbtnDelete.Visible = true;
                            hrfNewSubCat.Visible = true;
                        }
                        else
                        {
                            // lnkbtnDelete.Visible = false;
                            hrfNewSubCat.Visible = false;
                        }
                    }




                    rptSCategory.DataSource = lstSubCategories;
                    rptSCategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater rptSSCategory = e.Item.FindControl("rptSSCategory") as Repeater;
                    Literal ltlProductCount = e.Item.FindControl("ltlProductCount") as Literal;
                    System.Web.UI.HtmlControls.HtmlGenericControl spansubCategoryName = e.Item.FindControl("spansubCategoryName") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    // CheckBox chkW = e.Item.FindControl("chkW") as CheckBox;

                    HtmlTableRow trCategory = e.Item.FindControl("trCategory") as HtmlTableRow;
                    System.Web.UI.HtmlControls.HtmlAnchor aAddProduct = e.Item.FindControl("aAddProduct") as System.Web.UI.HtmlControls.HtmlAnchor;
                    //LinkButton lnkbtnDelete = e.Item.FindControl("lnkbtnDelete") as LinkButton;
                    //lnkbtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete this category \"" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName")) + "\"?')");
                    TextBox txtspcatid = e.Item.FindControl("txtspcatid") as TextBox;

                    HiddenField hdnSubSubCatID = e.Item.FindControl("hdnSubSubCatID") as HiddenField;



                    Repeater innerRepeater = (Repeater)sender;
                    RepeaterItem outerItem = (RepeaterItem)innerRepeater.NamingContainer;
                    TextBox TextBox1 = (TextBox)outerItem.FindControl("txtpcatid");
                    string pcatid = TextBox1.Text;
                    string subcatid = DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString();
                    txtspcatid.Text = pcatid;
                    string path = Host + "product-listing/" + GlobalFunctions.RemoveSpecialCharacters(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"))) + "/" + pcatid + "/" + subcatid + "/0";


                    string catid = DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString();


                    HyperLink hrfNewSubCat = e.Item.FindControl("hrfNewSubCat") as HyperLink;
                    hrfNewSubCat.NavigateUrl = AdminHost + "AddEditCategory.aspx?id=" + hdnSubSubCatID.Value + "&Type=A";
                    //hrfNewSubCat.Visible = true; // To Disable Sub sub category Level





                    Int16 CategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString());

                    List<CategoryBE> lstSubSubCategories = new List<CategoryBE>();
                    lstSubSubCategories = lstCategories.Where(c => c.ParentCategoryId == CategoryId).ToList();

                    if (lstSubSubCategories.Count() > 0)
                    {
                        aAddProduct.Visible = false;
                        int ProdCnt = 0;

                        foreach (var category in lstSubSubCategories)
                        {
                            ProdCnt = ProdCnt + Convert.ToInt32(category.ProductCount);

                        }
                        ltlProductCount.Text = ProdCnt.ToString();
                        //  lnkbtnDelete.Visible = false;
                        //hrfNewSubCat.Visible = true; // To Disable Sub sub category Level
                    }


                    else
                    {


                        path = AdminHost + "AssignProductCategory.aspx?cid=" + pcatid + "&scid=" + subcatid;


                        #region Link of AddEdit Product from sub category Level
                        //ltlProductCount.Text = "<a href=\"" + path + "\" target=\"_blank\">" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + "</a>";
                        #endregion
                        ltlProductCount.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount"));


                        if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) == "0")
                        {
                            //lnkbtnDelete.Visible = true;
                            //hrfNewSubCat.Visible = true; // To Disable Sub sub category Level
                        }
                        else
                        {
                            //lnkbtnDelete.Visible = false;
                            //hrfNewSubCat.Visible = true; // To Disable Sub sub category Level
                        }
                    }


                    rptSSCategory.DataSource = lstSubSubCategories;
                    rptSSCategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }



        protected void rptSSCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Literal ltlProductCount = e.Item.FindControl("ltlProductCount") as Literal;
                    CheckBox chkW = e.Item.FindControl("chkW") as CheckBox;
                    CheckBox chkM = e.Item.FindControl("chkM") as CheckBox;
                    CheckBox chkT = e.Item.FindControl("chkT") as CheckBox;
                    HtmlTableRow trCategory = e.Item.FindControl("trCategory") as HtmlTableRow;

                    // chkW.Attributes.Add("rel", Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryId")));


                    //get id of parent category
                    Repeater innerRepeater = (Repeater)sender;
                    RepeaterItem outerItem = (RepeaterItem)innerRepeater.NamingContainer;
                    TextBox TextBox1 = (TextBox)outerItem.FindControl("txtscatid");
                    TextBox TextBox2 = (TextBox)outerItem.FindControl("txtspcatid");

                    string scatid = TextBox1.Text;
                    string pcatid = TextBox2.Text;
                    string subsubcatid = DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString();

                    string path = Host + "product-listing/" + GlobalFunctions.RemoveSpecialCharacters(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"))) + "/" + pcatid + "/" + scatid + "/" + subsubcatid;

                    //LinkButton lnkbtnDelete = e.Item.FindControl("lnkbtnDelete") as LinkButton;
                    //lnkbtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete this category \"" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName")) + "\"?')");


                    path = AdminHost + "AssignProductCategory.aspx?cid=" + pcatid + "&scid=" + scatid + "&sscid=" + subsubcatid;

                    #region Link of AddEdit Product from sub category Level
                    //ltlProductCount.Text = "<a href=\"" + path + "\" target=\"_blank\">" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + "</a>";
                    #endregion

                    ltlProductCount.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount"));


                    // string DisplayOn = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DisplayOn"));
                    //Common.SetDisplayOn(ref chkW, ref chkM, ref chkT, DisplayOn);

                    //if (DisplayOn.Contains("D"))
                    //{
                    //    chkM.Checked = false;
                    //    chkW.Checked = false;
                    //    chkT.Checked = false;
                    //    trCategory.Attributes.Add("class", "greyrow");
                    //}
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void rptPCategory_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "DisableCategory")
            {
                Int16 CatId = Convert.ToInt16(e.CommandArgument);
                bool status = CategoryBL.DeleteCategories(CatId);
                if (status)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Category deleted successfully.", AlertType.Success);
                    PopulateCategory();
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "The Category cannot be deleted since subcategories or products are assigned to the category.", AlertType.Failure);
                    return;
                }

            }
        }


        protected void SetCategoryStatus(int CategoryId, CheckBox chkStatus, ref bool Result)
        {
            try
            {
                bool IsChecked = false;
                if (chkStatus != null)
                {

                    if (chkStatus.Checked)
                    {
                        IsChecked = true;
                    }
                    else
                    {
                        IsChecked = false;
                    }
                }
                Result = CategoryBL.SetCategoryStatus(CategoryId, IsChecked);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        protected void PopulateUserTypeDropDown()
        {
            try
            {
                UserTypesBE.CustomUserTypes objUserTypesBE = new UserTypesBE.CustomUserTypes();
                objUserTypesBE.LanguageId = 1;
                List<UserTypesBE.CustomUserTypes> lstUserTypesBE = UserTypesBL.GetListUserTypes<UserTypesBE.CustomUserTypes>(objUserTypesBE);
                if (lstUserTypesBE != null)
                {
                    if (lstUserTypesBE.Count > 0)
                    {
                        ddlUserTypes.DataSource = lstUserTypesBE;
                        ddlUserTypes.DataTextField = "UserTypeName";
                        ddlUserTypes.DataValueField = "UserTypeID";
                        ddlUserTypes.DataBind();
                        //ddlUserTypes.Items.Insert(0, new ListItem("Please Select User Type", "0"));
                    }
                }
                else
                {
                    ddlUserTypes.DataSource = null;
                    ddlUserTypes.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void chkAssignUT_CheckedChanged(object sender, EventArgs e)
        {

            try
            {
                Exception ex = null;
                bool Result = false;
                CheckBox chk = (CheckBox)sender;
                int CategoryId = Convert.ToInt32(chk.Attributes["rel"]);
                string UpdatedCategoryName = "";
                foreach (RepeaterItem item in rptPCategory.Items)
                {
                    Repeater rptSCategory = (Repeater)item.FindControl("rptSCategory");
                    CheckBox chkAssignUT = (CheckBox)item.FindControl("chkAssignUT");
                    if (Convert.ToInt32(chkAssignUT.Attributes["rel"]) == CategoryId)
                    {
                        HtmlGenericControl spanCategoryName = (HtmlGenericControl)item.FindControl("spanCategoryName");
                        UpdatedCategoryName = Convert.ToString(spanCategoryName.InnerText);
                        AssignCategoriesToUT(CategoryId, chkAssignUT.Checked);
                        break;
                    }
                    foreach (RepeaterItem items in rptSCategory.Items)
                    {
                        Repeater rptSSCategory = (Repeater)items.FindControl("rptSSCategory");

                        CheckBox chkSubAssignUT = (CheckBox)items.FindControl("chkAssignUT");
                        if (Convert.ToInt32(chkSubAssignUT.Attributes["rel"]) == CategoryId)
                        {
                            HtmlGenericControl spansubCategoryName = (HtmlGenericControl)items.FindControl("spansubCategoryName");
                            UpdatedCategoryName = Convert.ToString(spansubCategoryName.InnerText);
                            AssignCategoriesToUT(CategoryId, chkSubAssignUT.Checked);
                            break;
                        }
                        foreach (RepeaterItem itemss in rptSSCategory.Items)
                        {
                            HtmlGenericControl spansubsubCategoryName = (HtmlGenericControl)item.FindControl("spansubsubCategoryName");
                            // UpdatedCategoryName = Convert.ToString(spansubsubCategoryName.InnerText);
                            CheckBox chkSubSubAssignUT = (CheckBox)itemss.FindControl("chkAssignUT");
                            if (Convert.ToInt32(chkSubSubAssignUT.Attributes["rel"]) == CategoryId)
                            {
                                AssignCategoriesToUT(CategoryId, chkSubSubAssignUT.Checked);
                                break;
                            }
                        }
                    }
                }
                if (Result)
                {
                    divMessage.Visible = true;
                    //divMessage.InnerText = "Updated Successfully";
                    // ltrMessage.Text = "Updated Successfully";

                    CreateActivityLog("Category", Convert.ToString(UpdatedCategoryName.Trim()), "Assigned to User Type" + " " + Convert.ToString(UpdatedCategoryName.Trim()));
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated Successfully", AlertType.Success);

                    ex = new Exception();

                    //Exceptions.WriteExceptionLog(ex);
                    PopulateCategory();
                }
                else
                {
                    ex = new Exception();
                    divMessage.Visible = true;
                    divMessage.Attributes.Add("class", "Errormsg");
                    Exceptions.WriteExceptionLog(ex);
                }



            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void AssignCategoriesToUT(int CatgoryId, bool CheckStatus)
        {
            try
            {
                UserBE objUser = new UserBE();
                objUser = Session["StoreUser"] as UserBE;


                List<CategoryBE> Categories = new List<CategoryBE>();
                if (Convert.ToInt32(ddlUserTypes.SelectedValue) > 0)
                {
                    int i = 0;
                    if (CheckStatus)
                    {
                        i = UserTypesBL.AddCategoriesToUserTypes(Convert.ToInt32(ddlUserTypes.SelectedValue), Convert.ToString(Convert.ToInt16(DBAction.Insert)), Convert.ToString(CatgoryId), Convert.ToInt16(objUser.UserId));
                    }
                    else
                    {
                        i = UserTypesBL.AddCategoriesToUserTypes(Convert.ToInt32(ddlUserTypes.SelectedValue), Convert.ToString(Convert.ToInt16(DBAction.Delete)), Convert.ToString(CatgoryId), Convert.ToInt16(objUser.UserId));
                    }
                    if (i == 1)
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, "Category has been Assigned to Users Types", AlertType.Success); }
                    else if (i == 3)
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, "Category has been Removed to Users Types", AlertType.Success); }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, "Category Assigned Failed", AlertType.Warning); }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select User Type", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlUserTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateCategory();
        }
    }

}

