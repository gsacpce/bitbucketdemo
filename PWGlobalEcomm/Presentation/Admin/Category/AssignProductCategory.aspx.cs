﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Data;
using PWGlobalEcomm;
using System.Text.RegularExpressions;
using System.Text;
using PWGlobalEcomm.BusinessLogic;

namespace Presentation
{
    public class Admin_AssignProductCategory : BasePage //System.Web.UI.Page
    {
        #region Decalarations


        protected global::System.Web.UI.WebControls.HiddenField HIDPSelectedId, HIDAssignedProductIds, HIDUnAssignedProductIds, HIDAssignedProductNameIds, HIDUnAssignedProductNameIds;
        protected global::System.Web.UI.WebControls.Literal ltlHeading;
        protected global::System.Web.UI.WebControls.DropDownList ddlPParentCategory, ddlPSubCategory, ddlPSubSubCategory;
        protected global::System.Web.UI.WebControls.ListBox lstAssignedProducts, lstUnAssignedProducts;
        protected global::System.Web.UI.WebControls.TextBox txtDescription;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMessage, divLang;
        protected global::System.Web.UI.HtmlControls.HtmlTableRow trPSCategory, trPCategory, trPSSCategory, trProduct;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgLeftToRight, imgRightToLeft;
        protected global::System.Web.UI.WebControls.Literal ltrMessage;
        protected global::System.Web.UI.WebControls.TextBox txtCategoryName, txtBanner, txtMetaDescription, txtMetaKeyWords, txtPageTitle, txtFilter;
        protected global::System.Web.UI.HtmlControls.HtmlContainerControl dvLoader1, divNoCategoryMessage;
        protected global::System.Web.UI.WebControls.Button btnSaveExit;


        public string Host = GlobalFunctions.GetVirtualPath();
        int CId = 0, SCId = 0, SSCId = 0;
        int UserId = 0;
        int DefaultLanguage = 1;
        public string AdminHost = GlobalFunctions.GetVirtualPathAdmin();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            divMessage.Visible = false;
            ltrMessage.Text = "";

            dvLoader1 = this.Master.FindControl("dvLoader1") as System.Web.UI.HtmlControls.HtmlContainerControl;



            if (!IsPostBack)
            {
                #region Meta Data

                Page.Title = "Assign Products To Category";
                Page.MetaDescription = "Assign Products To Category";
                Page.MetaKeywords = "Assign Products To Category";
                #endregion

                imgLeftToRight.Attributes.Add("onmousedown", "fn_MoveStoreDataLeftToRight();");
                imgLeftToRight.Attributes.Add("onkeypress", "fn_MoveStoreDataLeftToRight();");

                imgRightToLeft.Attributes.Add("onmousedown", "fn_MoveStoreDataRightToLeft();");
                imgRightToLeft.Attributes.Add("onkeypress", "fn_MoveStoreDataRightToLeft();");

                if (Request.QueryString["cid"] != null)
                {
                    CId = Convert.ToInt16(Convert.ToString(Request.QueryString["cid"]));
                    ddlPParentCategory.Enabled = false;
                }

                if (Request.QueryString["cid"] != null)
                {
                    ddlPParentCategory.Enabled = false;
                    if ((CId == Convert.ToInt16(Convert.ToString(Request.QueryString["scid"]))) && (CId == Convert.ToInt16(Convert.ToString(Request.QueryString["sscid"]))))
                    {
                        SCId = 0;
                    }
                    else
                    {
                        SCId = Convert.ToInt16(Convert.ToString(Request.QueryString["scid"]));
                    }
                }

                if (Request.QueryString["sscid"] != null)
                {
                    ddlPSubCategory.Enabled = false;
                    if ((CId == SCId) && (CId == Convert.ToInt16(Convert.ToString(Request.QueryString["sscid"]))))
                    {
                        SSCId = 0;
                    }
                    else if (CId == SCId)
                    {
                        SCId = Convert.ToInt16(Convert.ToString(Request.QueryString["sscid"]));
                    }
                    else
                    {
                        SSCId = Convert.ToInt16(Convert.ToString(Request.QueryString["sscid"]));
                    }
                }

                BindCategory();
            }

            if (Request.QueryString["sscid"] == null && Request.QueryString["scid"] == null && Request.QueryString["cid"] != null)
            {
                ddlPParentCategory.Enabled = false;
                HIDPSelectedId.Value = Request.QueryString["cid"];
                lstAssignedProducts.Items.Clear();
                BindAssignedProducts();
                BindUnAssignedProducts("");
            }
            else if (Request.QueryString["sscid"] == null && Request.QueryString["scid"] != null && Request.QueryString["cid"] != null)
            {

                ddlPSubCategory.Enabled = false;
                HIDPSelectedId.Value = Request.QueryString["scid"];
                lstAssignedProducts.Items.Clear();
                BindAssignedProducts();
                BindUnAssignedProducts("");
            }
            else if (Request.QueryString["sscid"] != null && Request.QueryString["scid"] != null && Request.QueryString["cid"] != null)
            {
                ddlPSubSubCategory.Enabled = false;
                HIDPSelectedId.Value = Request.QueryString["sscid"];
                lstAssignedProducts.Items.Clear();
                BindAssignedProducts();
                BindUnAssignedProducts("");
            }
            //BindUnAssignedProducts("");
            //BindAssignedProducts();
            //RemoveMatchingItemsFromLeft(lstUnAssignedProducts, lstAssignedProducts);

        }

        private void RebindLastValue()
        {
            try
            {
                if (HIDAssignedProductNameIds.Value.Length != 0)
                {
                    HIDAssignedProductIds.Value = "";
                    string[] Items = HIDAssignedProductNameIds.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    lstAssignedProducts.Items.Clear();
                    foreach (string item in Items)
                    {
                        if (!lstAssignedProducts.Items.Contains(new ListItem(item.Split(new char[] { '|' })[0], item.Split(new char[] { '|' })[1])))
                        {
                            lstAssignedProducts.Items.Add(new ListItem(item.Split(new char[] { '|' })[0], item.Split(new char[] { '|' })[1]));
                            HIDAssignedProductIds.Value += item.Split(new char[] { '|' })[1] + ",";
                        }
                    }
                    HIDAssignedProductIds.Value.TrimEnd(new char[] { ',' });
                }
                else
                {
                    lstAssignedProducts.Items.Clear();
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //*************************************************************************
            // Purpose : To submit the data.
            // Inputs  : object sender, EventArgs e
            // Return  : Nothing
            //*************************************************************************
            divMessage.Visible = false;
            ltrMessage.Text = "";

            try
            {
                int ResultA = 0;

                List<CategoryBE> Categories = new List<CategoryBE>();


                if (lstAssignedProducts.Items.Count == 0 && HIDAssignedProductIds.Value == "")
                {
                    divMessage.Visible = true;
                    ltrMessage.Text = "Please assign category ";
                    //ltrMessage.Text = Exceptions.GetException("RequiredFieldException/QuickView");
                    return;
                }
                if (Request.QueryString["cid"] != null)
                {
                    HIDPSelectedId.Value = Convert.ToString(Request.QueryString["cid"]);
                    if (Request.QueryString["scid"] != null)
                    {
                        HIDPSelectedId.Value = Convert.ToString(Request.QueryString["scid"]);
                    }
                    if (Request.QueryString["sscid"] != null)
                    {
                        HIDPSelectedId.Value = Convert.ToString(Request.QueryString["sscid"]);
                    }


                }
                List<ProductBE> products = CategoryBL.AssignProductToCategory(int.Parse(HIDPSelectedId.Value), HIDAssignedProductIds.Value, "A", DefaultLanguage, ref ResultA, 0);

                if (products.Count() == 2)
                {

                    divMessage.Visible = true;
                    //divMessage.Attributes.Add("class", "sucessfully");
                    //ltrMessage.Text = "Updated Successfully";
                    if (ddlPSubCategory.SelectedValue != "")
                    {
                        if (Convert.ToInt32(ddlPSubCategory.SelectedValue) > 0)
                        {
                            CreateActivityLog("Assign Product Category ", "Updated", Convert.ToString(ddlPSubCategory.SelectedValue) + "_" + Convert.ToString(ddlPSubCategory.SelectedItem.Text));
                        }
                    }
                    else
                    {
                        CreateActivityLog("Assign Product Category ", "Updated", Convert.ToString(ddlPParentCategory.SelectedValue) + "_" + Convert.ToString(ddlPParentCategory.SelectedItem.Text));
                    }
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated Successfully", AlertType.Success); 
                    products = null;
                    bool IsUpdated = ProductSearchBL.UpdateLucene();
                    if (IsUpdated)
                        Exceptions.WriteInfoLog("Search index was created successfully!");
                    else
                        Exceptions.WriteInfoLog("There is some error while indexing product data. Please try after sometime!");
                }

                else
                {
                    ltrMessage.Text = "Error occured while saving";
                    divMessage.Visible = true;

                    // ltrMessage.Text = Exceptions.GetException("FailureMessage/DataSaveMessage");

                }

                lstAssignedProducts.Items.Clear();
                BindAssignedProducts();
                txtFilter.Text = "";
                BindUnAssignedProducts("");
                //RemoveMatchingItemsFromLeft(lstUnAssignedProducts, lstAssignedProducts);


            }
            catch (Exception ex)
            {
                divMessage.Visible = true;

                //ltrMessage.Text = Exceptions.GetException("SystemError/GeneralMessage");
                Exceptions.WriteExceptionLog(ex);
            }

        }

        protected void BindCategory()
        {
            List<CategoryBE> Categories = new List<CategoryBE>();
            Categories = CategoryBL.GetAllCategories();
            if (Categories != null)
            {
                Categories = Categories.Where(c => c.ParentCategoryId == 0).ToList();
                ddlPParentCategory.DataSource = Categories;
                ddlPParentCategory.DataTextField = "CategoryName";
                ddlPParentCategory.DataValueField = "CategoryId";
                ddlPParentCategory.DataBind();
                ddlPParentCategory.Items.Insert(0, new ListItem("-Select-", "0"));

                if (Categories.Count() > 0)
                    trPCategory.Visible = true;
                else
                    trPCategory.Visible = false;

                trPSCategory.Visible = false;
                trPSSCategory.Visible = false;
                trProduct.Visible = false;

                HIDPSelectedId.Value = "";
                Categories = null;
                if (CId != 0)
                {
                    if (SCId == 0 && SSCId == 0 && CId != 0)
                    {
                        trProduct.Visible = true;
                        ddlPParentCategory.ClearSelection();
                        ddlPParentCategory.SelectedValue = Convert.ToString(CId);
                    }
                    else
                    {
                        ddlPParentCategory_OnSelectedIndexChanged(new object(), new EventArgs());
                    }
                    //ddlPParentCategory.Items.FindByValue(CId.ToString()).Selected = true;

                }
                divNoCategoryMessage.Visible = false;
            }
            else
            {
                divNoCategoryMessage.Visible = true;
                btnSaveExit.Visible = false;
            }

        }


        //protected void ddlPParentCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    lstAssignedProducts.Items.Clear();
        //    if (CId != 0)
        //    {
        //        List<CategoryBE> SubCategories = new List<CategoryBE>();
        //        SubCategories = CategoryBL.GetAllCategories();
        //        SubCategories = SubCategories.Where(c => c.ParentCategoryId == Convert.ToInt16(ddlPParentCategory.SelectedValue)).ToList();

        //        if (SubCategories.Count() > 0)
        //        {
        //            trPSCategory.Visible = true;
        //            ddlPSubCategory.Items.Clear();
        //            ddlPSubCategory.DataSource = SubCategories;
        //            ddlPSubCategory.DataTextField = "CategoryName";
        //            ddlPSubCategory.DataValueField = "CategoryId";
        //            ddlPSubCategory.DataBind();
        //            ddlPSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));
        //            ddlPSubCategory.Items.FindByValue(SCId.ToString()).Selected = true;
        //        }
        //        else
        //        {
        //            trPSCategory.Visible = false;
        //            trProduct.Visible = true;
        //            HIDPSelectedId.Value = ddlPParentCategory.SelectedValue;
        //            BindAssignedProducts();
        //            BindUnAssignedProducts("");
        //        }
        //    }

        //}

        //protected void ddlPSubCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}

        protected void ddlPParentCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                RebindLastValue();


                if (CId != 0)
                {
                    ddlPParentCategory.Items.FindByValue(CId.ToString()).Selected = true;
                    HIDPSelectedId.Value = ddlPParentCategory.SelectedValue;

                    List<CategoryBE> Categories = new List<CategoryBE>();
                    Categories = CategoryBL.GetAllCategories();
                    Categories = Categories.Where(c => c.ParentCategoryId == Convert.ToInt16(ddlPParentCategory.SelectedValue)).ToList();
                    trPSSCategory.Visible = false;
                    if (Categories.Count() > 0)
                    {

                        ddlPSubCategory.Items.Clear();
                        ddlPSubCategory.DataSource = Categories;
                        ddlPSubCategory.DataTextField = "CategoryName";
                        ddlPSubCategory.DataValueField = "CategoryId";
                        ddlPSubCategory.DataBind();
                        ddlPSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));
                        ddlPSubCategory.Items.FindByValue(SCId.ToString()).Selected = true;
                        ddlPSubCategory_OnSelectedIndexChanged(new object(), new EventArgs());

                        if (Request.QueryString.HasKeys())
                        {
                            if (SCId != 0)
                            {
                                HIDPSelectedId.Value = Convert.ToString(SCId);
                                trPSCategory.Visible = true;
                                trProduct.Visible = true;
                            }
                            if (SSCId != 0)
                            {
                                HIDPSelectedId.Value = Convert.ToString(SSCId);
                                trPSCategory.Visible = true;
                                trPSSCategory.Visible = true;

                                trProduct.Visible = true;
                            }
                            else if (SSCId == 0 && SSCId == 0 && CId != 0)
                            {
                                HIDPSelectedId.Value = Convert.ToString(CId);
                                trProduct.Visible = true;
                            }
                        }
                        else
                        {
                            trPSCategory.Visible = true;
                        }




                        // lstUnAssignedProducts.Items.Clear();
                    }
                    else
                    {
                        trProduct.Visible = true;
                        trPSCategory.Visible = false;

                    }


                }


                else if (ddlPParentCategory.SelectedIndex > 0)
                {
                    lstAssignedProducts.Items.Clear();
                    if (Request.QueryString["cid"] != null)
                    {
                        CId = Convert.ToInt16(Convert.ToString(Request.QueryString["cid"]));
                    }
                    HIDPSelectedId.Value = ddlPParentCategory.SelectedValue;

                    List<CategoryBE> Categories = new List<CategoryBE>();
                    Categories = CategoryBL.GetAllCategories();
                    Categories = Categories.Where(c => c.ParentCategoryId == Convert.ToInt16(ddlPParentCategory.SelectedValue)).ToList();
                    trPSSCategory.Visible = false;
                    if (Categories.Count() > 0)
                    {

                        ddlPSubCategory.Items.Clear();
                        ddlPSubCategory.DataSource = Categories;
                        ddlPSubCategory.DataTextField = "CategoryName";
                        ddlPSubCategory.DataValueField = "CategoryId";
                        ddlPSubCategory.DataBind();
                        ddlPSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));
                        ddlPSubCategory.Items.FindByValue(SCId.ToString()).Selected = true;
                        ddlPSubCategory_OnSelectedIndexChanged(new object(), new EventArgs());

                        if (Request.QueryString.HasKeys())
                        {


                            if (SCId != 0)
                            {
                                trPSCategory.Visible = true;
                                HIDPSelectedId.Value = Convert.ToString(SCId);
                                lstAssignedProducts.Items.Clear();
                                BindAssignedProducts();
                                trProduct.Visible = true;
                            }
                            if (SSCId != 0)
                            {
                                trPSCategory.Visible = true;
                                HIDPSelectedId.Value = Convert.ToString(SCId);
                                lstAssignedProducts.Items.Clear();
                                BindAssignedProducts();
                                trPSSCategory.Visible = true;
                                trProduct.Visible = true;
                            }

                            else if (CId != 0)
                            {
                                HIDPSelectedId.Value = Convert.ToString(CId);
                                lstAssignedProducts.Items.Clear();
                                BindAssignedProducts();
                                trPSCategory.Visible = true;
                            }
                            else if (SSCId == 0 && SSCId == 0 && CId != 0)
                            {
                                HIDPSelectedId.Value = Convert.ToString(CId);
                                lstAssignedProducts.Items.Clear();
                                BindAssignedProducts();
                                trProduct.Visible = true;
                            }
                        }
                        else
                        {
                            trPSCategory.Visible = true;
                        }




                        // lstUnAssignedProducts.Items.Clear();
                    }
                    else
                    {
                        trProduct.Visible = true;
                        trPSCategory.Visible = false;
                        HIDPSelectedId.Value = ddlPParentCategory.SelectedValue;
                        BindAssignedProducts();
                        BindUnAssignedProducts("");

                    }


                }
                else
                {
                    trPSCategory.Visible = false;
                    trPSSCategory.Visible = false;
                    trProduct.Visible = false;


                    HIDPSelectedId.Value = "";
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlPSubCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //*************************************************************************
            // Purpose : Event will get fired when product sub category get changed
            // Inputs  : object sender, EventArgs e
            // Return  : Nothing
            //*************************************************************************
            //RebindLastValue();
            try
            {
                lstAssignedProducts.Items.Clear();
                divMessage.Visible = false;
                ltrMessage.Text = "";
                List<CategoryBE> Categories = new List<CategoryBE>();

                if (SCId != 0)
                {
                    Categories = CategoryBL.GetAllCategories();
                    Categories = Categories.Where(c => c.ParentCategoryId == Convert.ToInt16(SCId)).ToList();
                    HIDPSelectedId.Value = Convert.ToString(SCId);
                    if (Categories.Count() > 0)
                    {
                        ddlPSubSubCategory.DataSource = Categories;
                        ddlPSubSubCategory.DataTextField = "CategoryName";
                        ddlPSubSubCategory.DataValueField = "CategoryId";
                        ddlPSubSubCategory.DataBind();
                        ddlPSubSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));

                        if (Request.QueryString.HasKeys())
                        {

                            if (SSCId != 0)
                            {
                                ddlPSubSubCategory.Items.FindByValue(SSCId.ToString()).Selected = true;
                                HIDPSelectedId.Value = ddlPSubSubCategory.SelectedValue;
                                trPSSCategory.Visible = true;
                                //trProduct.Visible = true;
                                HIDAssignedProductNameIds.Value = "";
                                HIDAssignedProductIds.Value = "";
                                lstAssignedProducts.Items.Clear();

                                BindAssignedProducts();
                            }

                        }

                        //if (SSCId != 0)
                        //{
                        //    ddlPSubSubCategory.Items.FindByValue(SSCId.ToString()).Selected = true;
                        //    HIDPSelectedId.Value = ddlPSubSubCategory.SelectedValue;
                        //    trProduct.Visible = true;
                        //    HIDAssignedProductNameIds.Value = "";
                        //    HIDAssignedProductIds.Value = "";
                        //    lstAssignedProducts.Items.Clear();
                        //}





                        //  lstUnAssignedProducts.Items.Clear();
                    }
                    else
                    {
                        trProduct.Visible = true;
                        trPSSCategory.Visible = false;
                        HIDPSelectedId.Value = ddlPSubCategory.SelectedValue;
                        BindAssignedProducts();

                        //BindUnAssignedProducts();
                        //BindAssignedProducts();
                    }

                }

                else if (ddlPSubCategory.SelectedIndex > 0)
                {
                    lstAssignedProducts.Items.Clear();
                    HIDPSelectedId.Value = ddlPSubCategory.SelectedValue;

                    //lstAssignedProducts.Items.Clear();

                    Categories = CategoryBL.GetAllCategories();
                    Categories = Categories.Where(c => c.ParentCategoryId == Convert.ToInt16(ddlPSubCategory.SelectedValue)).ToList();

                    if (Categories.Count() > 0)
                    {
                        ddlPSubSubCategory.DataSource = Categories;
                        ddlPSubSubCategory.DataTextField = "CategoryName";
                        ddlPSubSubCategory.DataValueField = "CategoryId";
                        ddlPSubSubCategory.DataBind();
                        ddlPSubSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));

                        trPSSCategory.Visible = true;
                        trProduct.Visible = false;

                        HIDAssignedProductNameIds.Value = "";
                        HIDAssignedProductIds.Value = "";
                        //lstAssignedProducts.Items.Clear();




                        //  lstUnAssignedProducts.Items.Clear();
                    }
                    else
                    {
                        trProduct.Visible = true;
                        trPSSCategory.Visible = false;
                        HIDPSelectedId.Value = ddlPSubCategory.SelectedValue;
                        BindAssignedProducts();
                        BindUnAssignedProducts("");
                        //BindAssignedProducts();
                    }

                }
                else
                {
                    // lstUnAssignedProducts.Items.Clear();
                    trPSSCategory.Visible = false;

                    trProduct.Visible = false;

                    HIDPSelectedId.Value = ddlPParentCategory.SelectedValue;

                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        protected void ddlPSubSubCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            lstAssignedProducts.Items.Clear();

            divMessage.Visible = false;
            ltrMessage.Text = "";

            if (ddlPSubSubCategory.SelectedIndex > 0)
            {
                HIDPSelectedId.Value = ddlPSubSubCategory.SelectedValue;

                trProduct.Visible = true;
                lstAssignedProducts.Items.Clear();
                BindAssignedProducts();
                BindUnAssignedProducts("");
                //BindAssignedProducts();
            }
            else
            {

                trProduct.Visible = false;
                HIDPSelectedId.Value = ddlPSubCategory.SelectedValue;

                //  lstUnAssignedProducts.Items.Clear();
            }

        }

        /*private void RemoveMatchingItemsFromLeft(ListBox objListBox1, ListBox objListBox2)
        {
            foreach (ListItem item in objListBox2.Items)
            {
                if (objListBox1.Items.Contains(item))
                {
                    objListBox1.Items.Remove(item);
                }
            }
        }*/

        protected void BindUnAssignedProducts(string SearchText)
        {
            //*************************************************************************
            // Purpose : It will Bind products.
            // Inputs  : Nothing
            // Return  : Nothing
            //*************************************************************************
            try
            {
                lstUnAssignedProducts.Items.Clear();


                List<CategoryBE> Categories = new List<CategoryBE>();
                Categories = CategoryBL.GetAllCategories();
                int ResultA = 0;

                //AssignProductToCategory(int CategoryId, string ProductIds, string Action, int DefaultLanguage, ref int Result, int UserId)
                //List<ProductBE> Products = CategoryBL.AssignProductToCategory(0, "", "P", DefaultLanguage, ref ResultA, UserId);

                List<ProductBE> Products = CategoryBL.AssignProductToCategory(int.Parse(HIDPSelectedId.Value), "", "P", DefaultLanguage, ref ResultA, UserId);


                if (ddlPSubCategory.SelectedValue != "")
                {
                    Categories = Categories.Where(c => c.ParentCategoryId == Convert.ToInt16(ddlPSubCategory.SelectedValue)).ToList();

                    if (SearchText.ToLower() != "Product Code / Name".ToLower() && SearchText.ToLower() != "")
                    {
                        string strName = SearchText.ToLower();
                        if (strName.Contains(' '))
                        {
                            string[] tokens = strName.Split(' ');
                            string last = tokens[tokens.Length - 1];

                            Products = Products.Where(p => p.ProductCode.ToLower().Contains(strName) || p.ProductName.ToLower().Contains(strName)).ToList();

                        }
                        else
                        {
                            Products = Products.Where(p => p.ProductCode.ToLower().Contains(strName) || p.ProductName.ToLower().Contains(strName)).ToList();

                        }


                    }

                }




                //CategoryBL.AssignProductToCategory(0, "", "P", ref ResultA, 0);

                if (Categories != null)
                {
                    if (Categories.Count() > 0)
                    {

                        if (SearchText.ToLower() != "Product Code / Name".ToLower() && SearchText.ToLower() != "")
                        {
                            string strName = SearchText.ToLower();
                            if (strName.Contains(' '))
                            {
                                string[] tokens = strName.Split(' ');
                                string last = tokens[tokens.Length - 1];

                                Products = Products.Where(p => p.ProductCode.ToLower().Contains(strName) || p.ProductName.ToLower().Contains(strName)).ToList();

                            }
                            else
                            {
                                Products = Products.Where(p => p.ProductCode.ToLower().Contains(strName) || p.ProductName.ToLower().Contains(strName)).ToList();

                            }


                        }
                    }
                }
                if (Products != null)
                {
                    if (Products.Count() > 0)
                    {
                        var objProductBE = Products.OrderBy(y => y.ProductCode).Select(x => new { ProductId = x.ProductId, ProductCodeWithName = x.ProductCode + "-" + x.ProductName }).ToList();
                        lstUnAssignedProducts.DataSource = objProductBE;
                        //lstUnAssignedProducts.DataSource = Products;
                        lstUnAssignedProducts.DataTextField = "ProductCodeWithName";
                        lstUnAssignedProducts.DataValueField = "ProductId";
                        lstUnAssignedProducts.DataBind();


                    }
                    else
                    {
                        ltrMessage.Text = "No Records found";
                        divMessage.Visible = true;

                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindAssignedProducts()
        {
            //*************************************************************************
            // Purpose : It will Bind products.
            // Inputs  : Nothing
            // Return  : Nothing
            //*************************************************************************
            try
            {
                List<ProductBE> Products = new List<ProductBE>();

                int ResultA = 0;

                if (HIDPSelectedId.Value == "")
                    HIDPSelectedId.Value = "0";




                Products = CategoryBL.AssignProductToCategory(int.Parse(HIDPSelectedId.Value), "", "AP", DefaultLanguage, ref ResultA, 0);

                HIDAssignedProductNameIds.Value = "";
                if (Products != null)
                {
                    if (Products.Count() > 0)
                    {
                        int count = 0;
                        foreach (ProductBE product in Products)
                        {
                            HIDAssignedProductNameIds.Value = HIDAssignedProductNameIds.Value == "" ? Convert.ToString(product.ProductCode + " - " + product.ProductName + "|") : HIDAssignedProductNameIds.Value + "," + Convert.ToString(Products[0].Result);
                            if (Products.Count() == 1)
                                HIDAssignedProductIds.Value = product.ProductId.ToString();
                            else if (count == 0)
                            {
                                HIDAssignedProductIds.Value = product.ProductId.ToString();
                                count++;
                            }
                            else
                                HIDAssignedProductIds.Value += "," + product.ProductId.ToString();

                            lstAssignedProducts.Items.Add(new ListItem() { Text = product.ProductCode + "-" + product.ProductName, Value = Convert.ToString(product.ProductId) });


                        }

                    }
                    else
                    {

                        lstAssignedProducts.Items.Clear();



                    }
                }
                else
                {

                    lstAssignedProducts.Items.Clear();

                }

                //RebindLastValue();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                divMessage.Visible = false;
                ltrMessage.Text = "";

                if (txtFilter.Text != "" && txtFilter.Text != "Product Code / Name")
                {
                    BindUnAssignedProducts(txtFilter.Text.Trim());
                    if (ltrMessage.Text == "")
                    {
                        Bind();
                    }

                }
                else
                {
                    divMessage.Visible = true;
                    ltrMessage.Text = "Please enter Product Code / Name";

                    //  divMessage.Attributes.Remove("class");

                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            //ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", "HideLoader()", true);

        }

        protected void Bind()
        {
            List<ProductBE> Products = new List<ProductBE>();

            int ResultA = 0;

            if (HIDPSelectedId.Value == "")
                HIDPSelectedId.Value = "0";
            Products = CategoryBL.AssignProductToCategory(int.Parse(HIDPSelectedId.Value), "", "AP", DefaultLanguage, ref ResultA, 0);
            lstAssignedProducts.Items.Clear();
            HIDAssignedProductNameIds.Value = "";
            if (Products != null)
            {
                if (Products.Count() > 0)
                {
                    foreach (ProductBE product in Products)
                    {
                        lstAssignedProducts.Items.Add(new ListItem() { Text = product.ProductName, Value = Convert.ToString(product.ProductId) });
                    }
                    HIDAssignedProductNameIds.Value = HIDAssignedProductNameIds.Value == "" ? Convert.ToString(Products[0].Result) : HIDAssignedProductNameIds.Value + "," + Convert.ToString(Products[0].Result);


                }

            }
            if (Products.Count() == 0)
            {
                ltrMessage.Text = "No Such Record Found";
                divMessage.Visible = false;
            }
            else
            {
                if (HIDAssignedProductNameIds.Value != "")
                    RebindLastValue();

                //RemoveMatchingItemsFromLeft(lstUnAssignedProducts, lstAssignedProducts);

            }

        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            divMessage.Visible = false;
            txtFilter.Text = "";

            BindUnAssignedProducts("");
            if (ltrMessage.Text != "No Such Record found")
            {
                Bind();
            }
            ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", "HideLoader()", true);
        }
    }
}
