﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Data;
using PWGlobalEcomm;
using System.Web.Script.Serialization;
using PWGlobalEcomm.BusinessLogic;
using System.Web.Services;


namespace Presentation
{
    class CategorySequence
    {
        public string CatId { get; set; }
        public string Sequence { get; set; }
    }
    public class Admin_CategorySequence : BasePage //System.Web.UI.Page
    {
        #region Declarations

        public string Host = GlobalFunctions.GetVirtualPathAdmin();

        #endregion

        #region UI



        protected void Page_Load(object sender, EventArgs e)
        {
            #region Ajax methods

            if (Request.QueryString["MethodName"] == "SaveData")
            {
                SaveData(Request.Form["W"].ToString());
                return;
            }

            #endregion
        }

        protected DataTable GetDataIntoTable(string vData)
        {
            List<CategorySequence> returnList = new List<CategorySequence>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            vData = "[" + vData.Replace("[", "").Replace("]", "") + "]";
            if (vData != "[]")
            {
                returnList = serializer.Deserialize<List<CategorySequence>>(vData);
            }
            DataTable dt = GlobalFunctions.To_DataTable(returnList);
            dt.Columns["CatId"].ColumnName = "CategoryId";
            dt.Columns["Sequence"].ColumnName = "Sequence";

            return dt;
        }



        protected void btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (btn.ID == "btnAddNew")
                Response.Redirect(Host + "Category/AddEditCategory.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //*************************************************************************
            // Purpose : To submit the data.
            // Inputs  : object sender, EventArgs e
            // Return  : Nothing
            //*************************************************************************

            Response.Redirect(Host + "Category/CategoryManagement.aspx");
        }
        #endregion


        #region BussinessLogic

       
   
        protected void SaveData(string W)
        {
            try
            {
                bool IsSequenceSet = CategoryBL.SetCategorySequence(GetDataIntoTable(W));

                Response.Clear();
                if (IsSequenceSet)
                {
                    CreateActivityLog("Category Sequence", "Updated", "");
                    Response.Write('1');

                }
                else
                    Response.Write('0');
                Response.End();
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion
    }

}

