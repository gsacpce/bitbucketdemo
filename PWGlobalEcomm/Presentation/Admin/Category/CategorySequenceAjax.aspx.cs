﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PWGlobalEcomm.BusinessLogic;

namespace Presentation
{
    public class Admin_CategorySequenceAjax : BasePage //System.Web.UI.Page
    {
        List<CategoryBE> Categories;
        protected global::System.Web.UI.WebControls.Repeater rptPCategory;

        protected void Page_Load(object sender, EventArgs e)
        {

            PopulateSequence();
        }

        protected void PopulateSequence()
        {
            //*************************************************************************
            // Purpose : It will populate the grid data.
            // Inputs  : Nothing
            // Return  : Nothing
            //*************************************************************************
            try
            {



                Categories = CategoryBL.GetAllCategories();
                if (Categories.Count() > 0)
                {


                    Categories = Categories.OrderBy(c => c.Sequence).ToList();
                    rptPCategory.DataSource = Categories.Where(c => c.ParentCategoryId == 0).ToList();
                    rptPCategory.DataBind();

                }

            }

            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        #region Website

        protected void rptPCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl liPC = e.Item.FindControl("liPC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    System.Web.UI.HtmlControls.HtmlGenericControl ParentUl = e.Item.FindControl("ParentUl") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    Repeater rptSCategory = e.Item.FindControl("rptSCategory") as Repeater;

                    liPC.Attributes.Add("class", "cat" + DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString() + " category");
                    liPC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString());

                    List<CategoryBE> SubCategories = new List<CategoryBE>();
                    //SubCategories = Categories.Where((c => c.ParentCategoryId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId")) && (c.SubCategoryId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId"))))).ToList();
                    SubCategories = Categories.Where(c => c.SubSubCategoryId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId"))).ToList();
                    SubCategories = SubCategories.OrderBy(c => c.Sequence).ToList();
                    rptSCategory.DataSource = SubCategories;

                    if (SubCategories.Count() != 0)
                    {
                        ParentUl.Attributes.Add("class", "ClickUlmain");
                    }
                    rptSCategory.DataSource = SubCategories;
                    rptSCategory.DataBind();
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl liSC = e.Item.FindControl("liSC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    Repeater rptSSCategory = e.Item.FindControl("rptSSCategory") as Repeater;

                    liSC.Attributes.Add("class", "cat" + DataBinder.Eval(e.Item.DataItem, "ParentCategoryId").ToString() + " subcat" + DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString() + " subcategory");
                    liSC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString());

                    List<CategoryBE> SubSubCategories = new List<CategoryBE>();

                    SubSubCategories = Categories.Where(c => c.ParentCategoryId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId"))).ToList();
                    SubSubCategories = SubSubCategories.OrderBy(c => c.Sequence).ToList();
                    rptSSCategory.DataSource = SubSubCategories;
                    rptSSCategory.DataBind();
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSSCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Literal ltlSSC = e.Item.FindControl("ltlSSC") as Literal;

                    ltlSSC.Text = "<li class='cat" + DataBinder.Eval(e.Item.DataItem, "RelatedCategoryId").ToString() + " subcat" + DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString() + " subsubcategory'  rel='" + DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString() + "'><span class='textIcon'>" + DataBinder.Eval(e.Item.DataItem, "CategoryName").ToString() + "</span></li>";
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #endregion


    }
}
