﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

namespace Presentation
{
    public partial class Admin_RegionFreightManagement_FreightRepeater : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.DropDownList ddlSelectRegion, ddlCarrierService, ddlLanguage, ddlFreightTable, ddlFreightTable1, ddlCarrierService1, ddlSelectFreightSrcCountry;
        protected global::System.Web.UI.WebControls.RadioButtonList rblFreightModes, rblFreightMethods, rblCarriageMode, rblFreightMethods1;
        protected global::System.Web.UI.WebControls.Repeater rptValueAppliedAbove, rptMinimumOrderValue, rptValueAppliedBelow, rptValue, rptValueAppliedAboveBand2, rptMinimumOrderValueBand2, rptValueAppliedBelowBand2, rptValueBand2, rptBands;
        protected global::System.Web.UI.WebControls.TextBox txtTransitTime, txtWeight, txtCustomText, txtWeightFrom, txtWeightTo, txtTransitTime1, txtWeight1, txtCustomText1, txtWeightFrom1, txtWeightTo1;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ulWeight, ulValue, divPerOrder, divFreightTable, divBandEditable, div1, ulWeight1, ulValue1, divFreightTable1, divPerOrder1, collapse1, divFreightSourceCountry;
        protected global::System.Web.UI.WebControls.RadioButton rdoWeightKG, rdoPerOrder, rdoWeightKG1, rdoPerOrder1;
        protected global::System.Web.UI.WebControls.CheckBox chkDisallowOrder, chkDisallowOrder1, checkBandActive1;
        protected global::System.Web.UI.WebControls.Label lblVAA, lblCarrierServiceId, lblVAA1, lblBand1;
        protected global::System.Web.UI.WebControls.Button btnNextBand;
        protected global::System.Web.UI.WebControls.HiddenField HiddenBandId1, BandActiveSession, HiddenRepeaterItemCount, HidenMode;

        List<FreightRegionManagementBE.ShipmentMethodMasterBE> objShipmentMethodMasterBEL = new List<FreightRegionManagementBE.ShipmentMethodMasterBE>();
        public List<FreightManagementBE> lstAllFreightSettings;
        public List<FreightManagementBE> lstAllFreightSettingsMultipleCountry;
        public List<FreightManagementBE> lstAllFreightCreatedBand;
        public List<FreightManagementBE.FreightMultiplerCurrency> lstAllFreightMultiplerCurrency;
        public List<FreightManagementBE.FreightMultiplerCurrency> lstMinimumOrderValue;
        public List<FreightManagementBE.FreightMultiplerCurrency> lstValueAppliedAbove;
        public List<FreightManagementBE.FreightMultiplerCurrency> lstValueAppliedBelow;
        public List<FreightManagementBE.FreightCurrencyBandParameters> lstCurrencyBandParameters;
        public List<FreightManagementBE.FreightWeightBandParameters> lstWeightBandParameters;
        public List<FreightManagementBE.FreightTransitTime> lstTransitTime;
        //  public List<FreightManagementBE.FreightTables> lstFreightTables;
        public List<FreightManagementBE.FreightBand> lstFreightBand;
        public List<FreightManagementBE.FreightAtiveRegionBand> lstFreightActiveRegionBand;

        public List<Freight_Carrier_ServiceBE> lstFreightCarrierService;
        FreightManagementBE CurrFreightConfig = new FreightManagementBE();
        List<FreightManagementBE> objListFreight = new List<FreightManagementBE>();
        public List<FreightManagementBE.FreightMode> LstFreightModes;
        public List<FreightManagementBE.FreightMethod> LstFreightMethods;
        public List<FreightManagementBE.FreightTables> LstFreightTables;
        public List<FreightManagementBE.FreightTables> lstFreightTableID;
        public List<object> lstFreightSrcCountryCode;
        StoreBE objStoreBE = StoreBL.GetStoreDetails();
        FreightSourceCountryMasterBE objFreight = new FreightSourceCountryMasterBE();
        FreightManagementBE.FreightTables objFreightTables = new FreightManagementBE.FreightTables();

        #region "Variables"
        Int16 RegionId = 0;
        Int16 ModeId = 0;
        Int16 MethodId = 0;

        string strCurrencyMultipler = string.Empty;
        #endregion

        Int16 LanguageId;
        public int BandCounter = 0;
        public int FreightId = 0;
        public int BandId = 0;
        public string BandName = string.Empty;
        public Int16 FreightMethodId = 0;
        public int RepeaterId = 0;
        public string host = GlobalFunctions.GetVirtualPath();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsPostBack)
                {
                    if (Request["__EVENTTARGET"].Contains("rblFreightModes") == true && Request["__EVENTARGUMENT"] == "SelectedIndexChanged")
                    {
                        rblMode_SelectedIndexChanged(sender, e);
                    }
                }

                if (!string.IsNullOrEmpty(BandActiveSession.Value))
                {
                    if (divBandEditable.Visible == false)
                    {
                        if (rptBands.Items.Count > 0)
                        {
                            for (int i = 0; i < rptBands.Items.Count; i++)
                            {
                                HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");
                                if (Convert.ToInt32(BandActiveSession.Value) == i)
                                {
                                    divCollapse.Attributes["class"] = "collapse-in";
                                }
                                else
                                {
                                    divCollapse.Attributes["class"] = "collapse";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (rptBands.Items.Count > 0)
                        {
                            for (int i = 0; i < rptBands.Items.Count; i++)
                            {
                                HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");

                                divCollapse.Attributes["class"] = "collapse";

                            }
                        }
                    }
                }
                //StoreBE objStoreBE = StoreBL.GetStoreDetails();
                //rblCarriageMode.Attributes.Add("onclick", "javascript:setTimeout('__doPostBack(\'" + rblCarriageMode.ClientID.Replace("_", "$") + "\',\'\')', 0)");
                InitialiseAllListData();

                if (!IsPostBack)
                {
                    //StoreBE objStoreBE = StoreBL.GetStoreDetails();

                    BindRegionName();
                    BindLanguage(objStoreBE);
                    //rblFreightModes.SelectedIndex = 0;
                    BindFeightMode();
                    BindBandRepeater();

                    if (!objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                    {
                        divFreightSourceCountry.Visible = true;
                        BindFreightSourceCountry();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        // public static string ChangeRadioSelection(Int32 selectedMethod, bool weightPara, bool CurrencyPara)
        [System.Web.Services.WebMethod]
        public static string ChangeRadioSelection(string region, string Mode, string Parameter)
        {
            FreightManagementBE CurrFreightConfig = new FreightManagementBE();
            CurrFreightConfig.FreightRegionId = Convert.ToInt16(region);
            CurrFreightConfig.FreightModeId = Convert.ToInt16(Mode);
            if (Parameter == "Weight")
            {
                CurrFreightConfig.WeightParameter = true;
                CurrFreightConfig.CurrencyParameter = false;
            }
            else
            {
                if (Parameter == "Value")
                {
                    CurrFreightConfig.CurrencyParameter = true;
                    CurrFreightConfig.WeightParameter = false;
                }
            }
            bool IsDelete = FreightManagementBL.DeleteFreightCombination(CurrFreightConfig);
            if (IsDelete == true)
            {


            }

            return "1";

        }

        private void BindFeightMode()
        {
            try
            {
                short FId = 0;
                FId = LstFreightModes.FirstOrDefault(x => x.RegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.Languageid == Convert.ToInt32(ddlLanguage.SelectedValue)).FreightModeId;
                rblFreightModes.DataSource = LstFreightModes.Where(x => x.RegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.Languageid == Convert.ToInt32(ddlLanguage.SelectedValue));
                rblFreightModes.DataTextField = "FreightModeName";
                rblFreightModes.DataValueField = "FreightModeId";
                rblFreightModes.DataBind();

                rblFreightModes.SelectedValue = FId.ToString();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void InitialiseAllListData()
        {
            try
            {
                lstAllFreightSettings = FreightManagementBL.GetAllFreightConfigurationPerBand<FreightManagementBE>();
                lstAllFreightSettingsMultipleCountry = FreightManagementBL.GetAllFreightConfigurationPerBandMultipleCountry<FreightManagementBE>();
                lstAllFreightCreatedBand = FreightManagementBL.GetAllFreightPerBand<FreightManagementBE>();
                lstAllFreightMultiplerCurrency = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(0, "CM");
                lstMinimumOrderValue = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(0, "MOV");
                lstValueAppliedAbove = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(0, "VAA");
                lstValueAppliedBelow = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(0, "VAB");
                lstCurrencyBandParameters = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightCurrencyBandParameters>(0, "CUR");
                lstWeightBandParameters = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightWeightBandParameters>(0, "WEIGHT");
                lstTransitTime = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightTransitTime>(0, "TRANSIT");
                //lstFreightTables = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightTables>(0, "FreightTable");
                lstFreightBand = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightBand>(0, "BAND");
                lstFreightCarrierService = FreightManagementBL.GetAllFreightCarrierService();
                LstFreightModes = FreightManagementBL.GetAllFreightModes<FreightManagementBE.FreightMode>();
                LstFreightMethods = FreightManagementBL.GetAllFreightMethods<FreightManagementBE.FreightMethod>();
                LstFreightTables = FreightManagementBL.GetAllFreightTables<FreightManagementBE.FreightTables>();
                lstFreightActiveRegionBand = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightAtiveRegionBand>(0, "BandActive");
                lstFreightSrcCountryCode = FreightManagementBL.GetFreightSourceCountryMasterList();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rbl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bool method = true;

                if (BandActiveSession.Value == "")
                {
                    RepeaterId = 0;
                    BandActiveSession.Value = "0";
                }
                else
                {
                    RepeaterId = Convert.ToInt32(BandActiveSession.Value);
                }

                if (divBandEditable.Visible == true)
                {
                    if (rblFreightMethods1.SelectedValue.Equals("3"))
                    {
                        divFreightTable1.Style.Add("display", "block");
                    }
                    else
                    {
                        divFreightTable1.Style.Add("display", "none");
                    }

                    if (rblFreightMethods1.SelectedItem.Value.Equals("2"))
                    {
                        lblVAA.Text = "% Applied Above :";
                    }
                    else if (rblFreightMethods1.SelectedItem.Value.Equals("3"))
                    {
                        lblVAA.Text = "Multiplier On Cost Applied Above :";
                    }
                    else
                    {
                        lblVAA.Text = "Value Applied Above :";
                    }

                    //if (rblFreightMethods1.SelectedValue.Equals("2") || rblFreightMethods1.SelectedValue.Equals("3"))
                    //{
                    //    divPerOrder1.Visible = false;
                    //}
                    //else { divPerOrder1.Visible = true; }
                }

                if (lstAllFreightSettings != null)
                {
                    GetAllConfigurationDetailsPerMethod(RepeaterId, method);
                }

                BindFreightTable();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GetAllConfigurationDetailsPerMethod(int RepeaterId, bool method)
        {
            try
            {
                RegionId = ddlSelectRegion.SelectedValue.To_Int16();

                if (Convert.ToInt32(rblFreightModes.SelectedValue) > 0 || rblFreightModes.SelectedValue != "")
                {
                    ModeId = rblFreightModes.SelectedValue.To_Int16();
                    rblFreightModes.Items[0].Enabled = true;
                    var bli = rblFreightModes.Items[0];
                    bli.Attributes.Add("visibility", "visible");
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Carriage Service.", AlertType.Success);
                    return;
                }

                BindRadioBandRepeaterPerMethod(RepeaterId, method);
                RadioButtonList rblFreightMethods = (RadioButtonList)rptBands.Items[RepeaterId].FindControl("rblFreightMethods");
                HtmlGenericControl divFreightTable = (HtmlGenericControl)rptBands.Items[RepeaterId].FindControl("divFreightTable");
                if (divBandEditable.Visible == false)
                {
                    for (int i = 0; i < rptBands.Items.Count; i++)
                    {
                        HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");
                        if (RepeaterId == i)
                        {
                            divCollapse.Attributes["class"] = "collapse-in";
                        }
                        else
                        {
                            divCollapse.Attributes["class"] = "collapse";
                        }
                    }
                }
                if (divBandEditable.Visible == true)
                {
                    if (rblCarriageMode.SelectedValue == "Weight")
                    {
                        ulWeight1.Style.Add("display", "block");
                        ulValue1.Style.Add("display", "none");
                    }
                    else if (rblCarriageMode.SelectedValue == "Value")
                    {
                        ulWeight1.Style.Add("display", "none");
                        ulValue1.Style.Add("display", "block");
                    }
                }
                else
                {
                    HtmlGenericControl ulWeight = (HtmlGenericControl)rptBands.Items[RepeaterId].FindControl("ulWeight");
                    HtmlGenericControl ulValue = (HtmlGenericControl)rptBands.Items[RepeaterId].FindControl("ulValue");
                    if (rblCarriageMode.SelectedValue == "Weight")
                    {
                        ulWeight.Style.Add("display", "block");
                        ulValue.Style.Add("display", "none");
                    }
                    else if (rblCarriageMode.SelectedValue == "Value")
                    {
                        ulWeight.Style.Add("display", "none");
                        ulValue.Style.Add("display", "block");
                    }

                }
                if (divBandEditable.Visible == true)
                {
                    if (rblFreightMethods1.SelectedItem.Value.Equals("2"))
                    {
                        lblVAA.Text = "% Applied Above :";
                    }
                    else if (rblFreightMethods1.SelectedItem.Value.Equals("3"))
                    {
                        lblVAA.Text = "Multiplier On Cost Applied Above :";
                    }
                    else
                    {
                        lblVAA.Text = "Value Applied Above :";
                    }
                }
                else
                {
                    Label lblVAA = (Label)rptBands.Items[RepeaterId].FindControl("lblVAA");
                    if (rblFreightMethods.SelectedItem.Value.Equals("2"))
                    {
                        lblVAA.Text = "% Applied Above :";
                    }
                    else if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                    {
                        lblVAA.Text = "Multiplier On Cost Applied Above :";
                    }
                    else
                    {
                        lblVAA.Text = "Value Applied Above :";
                    }

                }
                if (divBandEditable.Visible == true)
                {
                    if (rblFreightMethods1.SelectedValue.Equals("3"))
                    {
                        divFreightTable1.Style.Add("display", "block");
                    }
                    else
                    {
                        divFreightTable1.Style.Add("display", "none");
                    }
                }
                else
                {
                    if (rblFreightMethods.SelectedValue.Equals("3"))
                    {
                        //  ddlFreightTable.SelectedValue = Convert.ToString(CurrFreightConfig.FreightTableId);
                        divFreightTable.Style.Add("display", "block");
                    }
                    else
                    {
                        divFreightTable.Style.Add("display", "none");
                    }
                }
               
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindRadioBandRepeaterPerMethod(int RepeaterId, bool method)
        {
            try
            {
                if (lstAllFreightSettings != null)
                {
                    if (divBandEditable.Visible == false)
                    {
                        RadioButtonList rblFreightMethods = (RadioButtonList)rptBands.Items[RepeaterId].FindControl("rblFreightMethods");
                        Label lblBand = (Label)rptBands.Items[RepeaterId].FindControl("lblBand");
                        HiddenField HiddenBandId = (HiddenField)rptBands.Items[RepeaterId].FindControl("HiddenBandId");
                        BandName = lblBand.Text;
                        BandId = Convert.ToInt32(HiddenBandId.Value);

                        if (method == true)
                        {
                            FreightMethodId = Convert.ToInt16(rblFreightMethods.SelectedValue);

                            if (rblCarriageMode.SelectedIndex == 0)
                            {
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.WeightParameter == true && x.BandId == Convert.ToInt32(BandId)).ToList();                                
                            }
                            else if (rblCarriageMode.SelectedIndex == 1)
                            {                                
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.CurrencyParameter == true && x.BandId == Convert.ToInt32(BandId)).ToList();
                            }
                            else
                            {
                                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                                {
                                    objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.BandId == Convert.ToInt32(BandId)).ToList();
                                }
                                else
                                {
                                    objListFreight = lstAllFreightSettingsMultipleCountry.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.BandId == Convert.ToInt32(BandId) && x.FreightSourceCountryCode == ddlSelectFreightSrcCountry.SelectedValue).ToList();
                                }                                
                            }
                        }
                        else
                        {
                            if (rblCarriageMode.SelectedIndex == 0)
                            {
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.WeightParameter == true && x.BandId == Convert.ToInt32(BandId)).ToList();                                
                            }
                            else if (rblCarriageMode.SelectedIndex == 1)
                            {                                
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.CurrencyParameter == true && x.BandId == Convert.ToInt32(BandId)).ToList();
                            }
                            else
                            {
                                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                                {
                                    objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.BandId == Convert.ToInt32(BandId)).ToList();
                                }
                                else
                                {
                                    objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.BandId == Convert.ToInt32(BandId) && x.FreightSourceCountryCode == ddlSelectFreightSrcCountry.SelectedValue).ToList();
                                }
                            }
                        }                        

                        if (objListFreight.Count > 0)
                        {
                            //rptBands.DataSource = objListFreight;
                            //rptBands.DataBind();
                            BindCurrentIndexRepeater(RepeaterId, objListFreight);
                            Button btnNextBand = (Button)rptBands.Items[rptBands.Items.Count - 1].FindControl("btnNextBand");
                            btnNextBand.Style.Add("display", "block");

                            divBandEditable.Visible = false;
                        }
                        else
                        {
                            //for (int i = 1; i < rptBands.Items.Count; i++)
                            //{
                            //    HtmlGenericControl dvMainBand = (HtmlGenericControl)rptBands.Items[i].FindControl("dvMainBand");
                            //    dvMainBand.Style.Add("display", "none");
                            //}
                            //Button btnNextBand = (Button)rptBands.Items[rptBands.Items.Count - 1].FindControl("btnNextBand");
                            //btnNextBand.Style.Add("display", "none");

                            HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[RepeaterId].FindControl("divCollapse");
                            divCollapse.Attributes["class"] = "collapse-in";
                            ClearRepeater(RepeaterId, method);
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(rblFreightMethods1.SelectedValue) > 0)
                        {
                            FreightMethodId = Convert.ToInt16(rblFreightMethods1.SelectedValue);
                            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                            {
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods1.SelectedValue) && x.BandId == Convert.ToInt32(HiddenBandId1.Value)).ToList();
                            }
                            else
                            {
                                objListFreight = lstAllFreightSettingsMultipleCountry.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods1.SelectedValue) && x.BandId == Convert.ToInt32(HiddenBandId1.Value) && x.FreightSourceCountryCode == ddlSelectFreightSrcCountry.SelectedValue).ToList();
                            }
                        }
                        else
                        {
                            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                            {
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.BandId == Convert.ToInt32(HiddenBandId1.Value)).ToList();
                            }
                            else
                            {
                                objListFreight = lstAllFreightSettingsMultipleCountry.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.BandId == Convert.ToInt32(HiddenBandId1.Value) && x.FreightSourceCountryCode == ddlSelectFreightSrcCountry.SelectedValue).ToList();
                            }
                        } 

                        if (objListFreight.Count > 0)
                        {
                            //  BindCurrentIndexRepeater(RepeaterId, objListFreight);
                            rptBands.DataSource = objListFreight;
                            rptBands.DataBind();
                            divBandEditable.Visible = false;
                        }
                        else
                        {
                            //ClearRepeater(RepeaterId);
                        }
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rblMode_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (lstAllFreightSettings != null)
                {
                    if (rblCarriageMode.SelectedValue == "Value")
                    {
                        objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.WeightParameter == true).ToList();
                        if (objListFreight.Count > 0)
                        {
                            rblCarriageMode.SelectedValue = "Weight";
                        }
                    }
                    else
                    {
                        if (rblCarriageMode.SelectedValue == "Weight")
                        {
                            objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.CurrencyParameter == true).ToList();
                            if (objListFreight.Count > 0)
                            {
                                rblCarriageMode.SelectedValue = "Value";
                            }
                        }
                    }
                    if (rptBands.Items.Count > 0)
                    {
                        if (divBandEditable.Visible == true)
                        {
                            divBandEditable.Visible = false;
                        }
                    }
                }

                if (BandActiveSession.Value == "")
                {
                    RepeaterId = 0;
                    BandActiveSession.Value = "0";
                }
                else
                {
                    RepeaterId = Convert.ToInt32(BandActiveSession.Value);
                }

                if (lstAllFreightSettings != null)
                {
                    GetAllConfigurationDetails(RepeaterId, false);
                }
                else
                {
                    rptBands.DataSource = null;
                    rptBands.DataBind();
                    divBandEditable.Visible = true;
                    lblBand1.Text = "Band1";
                    HiddenBandId1.Value = "1";
                    collapse1.Attributes["class"] = "collapse.in";
                    GenerateBlankBand();
                }


            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlSelectRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divBandEditable.Visible = false;
                rblCarriageMode.Items[0].Selected = false;
                rblCarriageMode.Items[1].Selected = false;

                BindFeightMode();
                if (BandActiveSession.Value == "")
                {
                    BandActiveSession.Value = "0";
                }
                BindBandRepeater();
                ddlSelectFreightSrcCountry_SelectedIndexChanged(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindFeightMode();
                // divBandEditable.Visible = false;

                if (BandActiveSession.Value == "")
                {
                    BandActiveSession.Value = "0";
                }
                if (lstAllFreightSettings != null)
                {
                    if (divBandEditable.Visible == false)
                    {
                        if (rptBands.Items.Count > 0)
                        {
                            TextBox txtTransitTime = (TextBox)rptBands.Items[Convert.ToInt32(BandActiveSession.Value)].FindControl("txtTransitTime");
                            TextBox txtCustomText = (TextBox)rptBands.Items[Convert.ToInt32(BandActiveSession.Value)].FindControl("txtCustomText");
                            //HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[Convert.ToInt32(BandActiveSession.Value)].FindControl("divCollapse");
                            HiddenField HiddenFreightId = (HiddenField)rptBands.Items[Convert.ToInt32(BandActiveSession.Value)].FindControl("HiddenFreightId");
                            FreightManagementBE.FreightTransitTime objTran = lstTransitTime.FirstOrDefault(x => x.FreightConfigurationId == Convert.ToInt32(HiddenFreightId.Value) && x.LanguageId == Convert.ToInt32(ddlLanguage.SelectedValue));
                            txtTransitTime.Text = objTran != null ? Convert.ToString(objTran.TransitTimeName) : "";
                            txtCustomText.Text = objTran != null ? Convert.ToString(objTran.CustomText) : "";
                            //  divCollapse.Attributes["class"] = "collapse-in";
                            for (int i = 0; i < rptBands.Items.Count; i++)
                            {
                                HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");
                                if (Convert.ToInt32(BandActiveSession.Value) == i)
                                {
                                    divCollapse.Attributes["class"] = "collapse-in";
                                }
                                else
                                {
                                    divCollapse.Attributes["class"] = "collapse";
                                }
                            }

                        }
                    }
                    else
                    {
                        if (rptBands.Items.Count > 0)
                        {
                            TextBox txtTransitTime = (TextBox)rptBands.Items[Convert.ToInt32(BandActiveSession.Value)].FindControl("txtTransitTime");
                            TextBox txtCustomText = (TextBox)rptBands.Items[Convert.ToInt32(BandActiveSession.Value)].FindControl("txtCustomText");
                            //HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[Convert.ToInt32(BandActiveSession.Value)].FindControl("divCollapse");
                            HiddenField HiddenFreightId = (HiddenField)rptBands.Items[Convert.ToInt32(BandActiveSession.Value)].FindControl("HiddenFreightId");
                            FreightManagementBE.FreightTransitTime objTran = lstTransitTime.FirstOrDefault(x => x.FreightConfigurationId == Convert.ToInt32(HiddenFreightId.Value) && x.LanguageId == Convert.ToInt32(ddlLanguage.SelectedValue));
                            txtTransitTime.Text = objTran != null ? Convert.ToString(objTran.TransitTimeName) : "";
                            txtCustomText.Text = objTran != null ? Convert.ToString(objTran.CustomText) : "";
                            //  divCollapse.Attributes["class"] = "collapse-in";
                            for (int i = 0; i < rptBands.Items.Count; i++)
                            {
                                HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");
                                divCollapse.Attributes["class"] = "collapse";

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GetAllConfigurationDetails(int RepeaterId, bool method)
        {
            try
            {
                RegionId = ddlSelectRegion.SelectedValue.To_Int16();

                if (Convert.ToInt32(rblFreightModes.SelectedValue) > 0 || rblFreightModes.SelectedValue != "")
                {
                    ModeId = rblFreightModes.SelectedValue.To_Int16();
                    rblFreightModes.Items[0].Enabled = true;
                    var bli = rblFreightModes.Items[0];
                    bli.Attributes.Add("visibility", "visible");
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Please Select Carriage Service.", AlertType.Success);
                    return;
                }

                BindRadioBandRepeater(RepeaterId, method);
                RadioButtonList rblFreightMethods = (RadioButtonList)rptBands.Items[RepeaterId].FindControl("rblFreightMethods");
                HtmlGenericControl divFreightTable = (HtmlGenericControl)rptBands.Items[RepeaterId].FindControl("divFreightTable");
                if (divBandEditable.Visible == false)
                {
                    for (int i = 0; i < rptBands.Items.Count; i++)
                    {
                        HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");
                        if (RepeaterId == i)
                        {
                            divCollapse.Attributes["class"] = "collapse-in";
                        }
                        else
                        {
                            divCollapse.Attributes["class"] = "collapse";
                        }
                    }
                }
                if (divBandEditable.Visible == true)
                {
                    if (rblCarriageMode.SelectedValue == "Weight")
                    {
                        ulWeight1.Style.Add("display", "block");
                        ulValue1.Style.Add("display", "none");
                    }
                    else if (rblCarriageMode.SelectedValue == "Value")
                    {
                        ulWeight1.Style.Add("display", "none");
                        ulValue1.Style.Add("display", "block");
                    }
                }
                else
                {
                    HtmlGenericControl ulWeight = (HtmlGenericControl)rptBands.Items[RepeaterId].FindControl("ulWeight");
                    HtmlGenericControl ulValue = (HtmlGenericControl)rptBands.Items[RepeaterId].FindControl("ulValue");
                    if (rblCarriageMode.SelectedValue == "Weight")
                    {
                        ulWeight.Style.Add("display", "block");
                        ulValue.Style.Add("display", "none");
                    }
                    else if (rblCarriageMode.SelectedValue == "Value")
                    {
                        ulWeight.Style.Add("display", "none");
                        ulValue.Style.Add("display", "block");
                    }

                }
                if (divBandEditable.Visible == true)
                {
                    if (rblFreightMethods1.SelectedItem.Value.Equals("2"))
                    {
                        lblVAA.Text = "% Applied Above :";
                    }
                    else if (rblFreightMethods1.SelectedItem.Value.Equals("3"))
                    {
                        lblVAA.Text = "Multiplier On Cost Applied Above :";
                    }
                    else
                    {
                        lblVAA.Text = "Value Applied Above :";
                    }
                }
                else
                {
                    Label lblVAA = (Label)rptBands.Items[RepeaterId].FindControl("lblVAA");
                    if (rblFreightMethods.SelectedItem.Value.Equals("2"))
                    {
                        lblVAA.Text = "% Applied Above :";
                    }
                    else if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                    {
                        lblVAA.Text = "Multiplier On Cost Applied Above :";
                    }
                    else
                    {
                        lblVAA.Text = "Value Applied Above :";
                    }

                }
                if (divBandEditable.Visible == true)
                {
                    if (rblFreightMethods1.SelectedValue.Equals("3"))
                    {
                        divFreightTable1.Style.Add("display", "block");
                    }
                    else
                    {
                        divFreightTable1.Style.Add("display", "none");
                    }
                }
                else
                {
                    if (rblFreightMethods.SelectedValue.Equals("3"))
                    {
                        //  ddlFreightTable.SelectedValue = Convert.ToString(CurrFreightConfig.FreightTableId);
                        divFreightTable.Style.Add("display", "block");
                    }
                    else
                    {
                        divFreightTable.Style.Add("display", "none");
                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ClearInputs()
        {
            try
            {
                txtTransitTime1.Text = string.Empty;
                txtCustomText1.Text = string.Empty;
                txtWeight1.Text = string.Empty;
                txtWeightFrom1.Text = string.Empty;
                txtWeightTo1.Text = string.Empty;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptValue_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int iFreightConfigID = FreightId;
                    int iBandId = BandId;
                    int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));

                    FreightManagementBE.FreightCurrencyBandParameters CurrencyBandParameters = lstCurrencyBandParameters.FirstOrDefault(x => x.BandId == iBandId && x.CurrencyId == iCurrency && x.FreightConfigurationId == iFreightConfigID);
                    TextBox txtValFrom = (TextBox)e.Item.FindControl("txtValFrom");
                    TextBox txtValTo = (TextBox)e.Item.FindControl("txtValTo");

                    if (CurrencyBandParameters != null)
                    {
                        txtValFrom.Text = Convert.ToString(CurrencyBandParameters.CurrencyFrom);
                        txtValTo.Text = Convert.ToString(CurrencyBandParameters.CurrencyTo);
                    }
                    else
                    {
                        txtValFrom.Text = "";
                        txtValTo.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptMinimumOrderValue_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int iFreightConfigID = FreightId;
                    int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));
                    FreightManagementBE.FreightMultiplerCurrency MinimumOrderValue = lstMinimumOrderValue.FirstOrDefault(x => x.FreightConfigurationId == iFreightConfigID && x.CurrencyID == iCurrency);
                    TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
                    if (MinimumOrderValue != null)
                        txtCurrencyValue.Text = Convert.ToString(MinimumOrderValue.CValue);
                    else
                        txtCurrencyValue.Text = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptValueAppliedAbove_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int iFreightConfigID = FreightId;
                    int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));

                    if (lstValueAppliedAbove != null)
                    {
                        FreightManagementBE.FreightMultiplerCurrency ValueAppliedAbove = lstValueAppliedAbove.FirstOrDefault(x => x.FreightConfigurationId == iFreightConfigID &&
                          x.CurrencyID == iCurrency);
                        TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
                        if (ValueAppliedAbove != null)
                        {
                            txtCurrencyValue.Text = Convert.ToString(ValueAppliedAbove.CValue);
                        }
                        //else
                        //{ txtCurrencyValue.Text = "0"; }
                    }
                    if (lstAllFreightMultiplerCurrency != null)
                    {
                        FreightManagementBE.FreightMultiplerCurrency AllFreightMultiplerCurrency = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == iFreightConfigID &&
                         x.CurrencyID == iCurrency);
                        TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
                        if (AllFreightMultiplerCurrency != null)
                        {
                            txtCurrencyValue.Text = Convert.ToString(AllFreightMultiplerCurrency.CValue);
                        }
                        //else { txtCurrencyValue.Text = "0"; }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptValueAppliedBelow_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int iFreightConfigID = FreightId;
                    int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));
                    FreightManagementBE.FreightMultiplerCurrency ValueAppliedBelow = lstValueAppliedBelow.FirstOrDefault(x => x.FreightConfigurationId == iFreightConfigID &&
                          x.CurrencyID == iCurrency);

                    TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
                    if (ValueAppliedBelow != null)
                    {
                        txtCurrencyValue.Text = Convert.ToString(ValueAppliedBelow.CValue);
                    }
                    else
                    { txtCurrencyValue.Text = ""; }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindLanguage(StoreBE objStoreBE)
        {
            try
            {
                if (objStoreBE.StoreLanguages.Count > 0)
                {
                    ddlLanguage.DataSource = objStoreBE.StoreLanguages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                    ddlLanguage.SelectedValue = Convert.ToString(objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId);
                    LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                }
                else
                {
                    LanguageId = objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindRegionName()
        {
            try
            {
                FreightRegionManagementBE objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();
                List<FreightManagementBE.FreightMethod> LstFreightMethods = new List<FreightManagementBE.FreightMethod>();


                if (objFreightRegionManagementBE.FreightRegionsBELst.Count > 0)
                {
                    ddlSelectRegion.DataSource = objFreightRegionManagementBE.FreightRegionsBELst.Where(x => x.FreightRegionName != "ALL" && x.FreightRegionName != "Unallocated"); ;
                    ddlSelectRegion.DataTextField = "FreightRegionName";
                    ddlSelectRegion.DataValueField = "FreightRegionId";
                    ddlSelectRegion.DataBind();
                    //ddlSelectRegion.Items.Insert(0, new ListItem("--Select Region--", "0"));

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindCurrency()
        {
            try
            {
                #region "Repeater Currency"
                rptValueAppliedAbove.Visible = true;
                rptMinimumOrderValue.Visible = true;
                rptValueAppliedBelow.Visible = true;
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                rptValueAppliedAbove.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValueAppliedAbove.DataBind();

                rptMinimumOrderValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptMinimumOrderValue.DataBind();

                rptValueAppliedBelow.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValueAppliedBelow.DataBind();

                rptValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValue.DataBind();

                #endregion
                //txtVAA.Visible = false;
                //rfvVAA.Enabled = false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rblCarriageMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lstAllFreightSettings != null)
                {
                    if (rblCarriageMode.SelectedValue == "Value")
                    {
                        objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.WeightParameter == true).ToList();
                        if (objListFreight.Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(string), "confirm",
                 "myTestFunction();", true);

                        }
                    }
                    else
                    {
                        if (rblCarriageMode.SelectedValue == "Weight")
                        {
                            objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.CurrencyParameter == true).ToList();
                            if (objListFreight.Count > 0)
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "confirm", "<script>javascript:myTestFunction();</script>");

                                //           ScriptManager.RegisterStartupScript(this, typeof(string), "confirm",
                                //"myTestFunction();", true);

                            }
                        }
                    }

                }

                if (BandActiveSession.Value == "")
                {
                    RepeaterId = 0;
                    BandActiveSession.Value = "0";
                }
                else
                {
                    RepeaterId = Convert.ToInt32(BandActiveSession.Value);
                }

                if (divBandEditable.Visible == true)
                {
                    if (rblCarriageMode.SelectedValue == "Weight")
                    {
                        ulWeight1.Style.Add("display", "block");
                        ulValue1.Style.Add("display", "none");
                    }
                    else if (rblCarriageMode.SelectedValue == "Value")
                    {
                        ulWeight1.Style.Add("display", "none");
                        ulValue1.Style.Add("display", "block");
                    }

                    GetAllConfigurationDetails(Convert.ToInt32(RepeaterId), false);

                }
                else
                {

                    HtmlGenericControl ulWeight = (HtmlGenericControl)rptBands.Items[Convert.ToInt32(RepeaterId)].FindControl("ulWeight");
                    HtmlGenericControl ulValue = (HtmlGenericControl)rptBands.Items[Convert.ToInt32(RepeaterId)].FindControl("ulValue");

                    if (rblCarriageMode.SelectedValue == "Weight")
                    {
                        ulWeight.Style.Add("display", "block");
                        ulValue.Style.Add("display", "none");
                    }
                    else if (rblCarriageMode.SelectedValue == "Value")
                    {
                        ulWeight.Style.Add("display", "none");
                        ulValue.Style.Add("display", "block");
                    }
                    GetAllConfigurationDetails(Convert.ToInt32(RepeaterId), false);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected string GetAssignedMinimumOrderValues(ref Repeater rptMinimumOrderValue)
        {
            string MOV = string.Empty;
            try
            {
                foreach (RepeaterItem rptitem in rptMinimumOrderValue.Items)
                {
                    Label lblCurrency = rptitem.FindControl("lblCurrency") as Label;
                    HiddenField hdfCurrencyID = rptitem.FindControl("hdfCurrencyID") as HiddenField;
                    TextBox txtMOV = (TextBox)rptitem.FindControl("txtCurrencyValue");
                    RegularExpressionValidator regExp = (RegularExpressionValidator)rptitem.FindControl("rgxMOV");
                    Boolean IsValidMOV = Regex.IsMatch(txtMOV.Text, ValidRegEx.RegEx_ValidDecimalWithFourDigit);
                    if (IsValidMOV)
                    {
                        if (MOV == "")
                            MOV = MOV + hdfCurrencyID.Value + "-" + txtMOV.Text.Trim();
                        else
                            MOV = MOV + "|" + hdfCurrencyID.Value + "-" + txtMOV.Text.Trim();
                    }
                    else
                    {
                        MOV = "";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return MOV;
        }

        protected string GetAssignedCurrencyValues(ref Repeater rptValue)
        {
            string CURR = string.Empty;
            try
            {
                foreach (RepeaterItem rptitem in rptValue.Items)
                {
                    Label lblCurrencyFrom = rptitem.FindControl("lblCurrencyFrom") as Label;
                    HiddenField hdfCurrencyID = rptitem.FindControl("hdfCurrencyID") as HiddenField;
                    TextBox txtValFrom = (TextBox)rptitem.FindControl("txtValFrom");
                    TextBox txtValTo = (TextBox)rptitem.FindControl("txtValTo");

                    Boolean IsValidValFrom = Regex.IsMatch(txtValFrom.Text, ValidRegEx.RegEx_ValidFreightDecimalWithFourDigit);
                    Boolean IsValidValTo = Regex.IsMatch(txtValTo.Text, ValidRegEx.RegEx_ValidFreightDecimalWithFourDigit);

                    if (IsValidValFrom && IsValidValTo)
                    {
                        if (CURR == "")
                            CURR = CURR + hdfCurrencyID.Value + "-" + txtValFrom.Text.Trim() + "-" + txtValTo.Text.Trim();
                        else
                            CURR = CURR + "|" + hdfCurrencyID.Value + "-" + txtValFrom.Text.Trim() + "-" + txtValTo.Text.Trim();
                    }
                    else
                    {
                        CURR = "";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return CURR;
        }

        protected string GetAssignedWeightValues(ref TextBox txtWeightFrom, ref TextBox txtWeightTo)
        {
            string WEIGHT = string.Empty;
            try
            {
                Boolean IsValidValFrom = Regex.IsMatch(txtWeightFrom.Text, ValidRegEx.RegEx_ValidFreightDecimalWithFourDigit);
                Boolean IsValidValTo = Regex.IsMatch(txtWeightTo.Text, ValidRegEx.RegEx_ValidFreightDecimalWithFourDigit);

                if (IsValidValFrom && IsValidValTo)
                {
                    if (WEIGHT == "")
                        WEIGHT = WEIGHT + txtWeightFrom.Text.Trim() + "-" + txtWeightTo.Text.Trim();
                    else
                        WEIGHT = WEIGHT + "|" + txtWeightFrom.Text.Trim() + "-" + txtWeightTo.Text.Trim();
                }
                else
                {
                    WEIGHT = "";
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return WEIGHT;
        }

        protected string GetAssignedWeightValues()
        {
            string WEIGHT = string.Empty;
            try
            {
                Boolean IsValidValFrom = Regex.IsMatch(txtWeightFrom1.Text, ValidRegEx.RegEx_ValidFreightDecimalWithFourDigit);
                Boolean IsValidValTo = Regex.IsMatch(txtWeightTo1.Text, ValidRegEx.RegEx_ValidFreightDecimalWithFourDigit);

                if (IsValidValFrom && IsValidValTo)
                {
                    if (WEIGHT == "")
                        WEIGHT = WEIGHT + txtWeightFrom1.Text.Trim() + "-" + txtWeightTo1.Text.Trim();
                    else
                        WEIGHT = WEIGHT + "|" + txtWeightFrom1.Text.Trim() + "-" + txtWeightTo1.Text.Trim();
                }
                else
                {
                    WEIGHT = "";
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return WEIGHT;
        }

        protected string GetAssignedValueAppliedBelow(ref Repeater rptValueAppliedBelow)
        {
            string VAB = string.Empty;
            try
            {
                foreach (RepeaterItem rptitem in rptValueAppliedBelow.Items)
                {
                    Label lblCurrency = rptitem.FindControl("lblCurrency") as Label;
                    HiddenField hdfCurrencyID = rptitem.FindControl("hdfCurrencyID") as HiddenField;
                    TextBox txtVAB = (TextBox)rptitem.FindControl("txtCurrencyValue");
                    Boolean IsValidVAB = Regex.IsMatch(txtVAB.Text, ValidRegEx.RegEx_ValidDecimalWithFourDigit);
                    if (IsValidVAB)
                    {
                        if (VAB == "")
                            VAB = VAB + hdfCurrencyID.Value + "-" + txtVAB.Text.Trim();
                        else
                            VAB = VAB + "|" + hdfCurrencyID.Value + "-" + txtVAB.Text.Trim();
                    }
                    else
                    {
                        VAB = "";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return VAB;
        }

        protected string GetAssignedValueAppliedAbove(ref Repeater rptValueAppliedAbove)
        {
            string VAA = string.Empty;
            try
            {
                foreach (RepeaterItem rptitem in rptValueAppliedAbove.Items)
                {
                    Label lblCurrency = rptitem.FindControl("lblCurrency") as Label;
                    HiddenField hdfCurrencyID = rptitem.FindControl("hdfCurrencyID") as HiddenField;
                    TextBox txtVAB = (TextBox)rptitem.FindControl("txtCurrencyValue");
                    Boolean IsValidVAA = Regex.IsMatch(txtVAB.Text, ValidRegEx.RegEx_ValidDecimalWithFourDigit);
                    if (IsValidVAA)
                    {
                        if (VAA == "")
                            VAA = VAA + hdfCurrencyID.Value + "-" + txtVAB.Text.Trim();
                        else
                            VAA = VAA + "|" + hdfCurrencyID.Value + "-" + txtVAB.Text.Trim();
                    }
                    else
                    {
                        VAA = "";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return VAA;
        }

        private void BindRadioBandRepeater(int RepeaterId, bool method)
        {
            try
            {
                if (lstAllFreightSettings != null)
                {
                    //if (divBandEditable.Visible == false)
                    //{

                    if (method == true)
                    {
                        if (rptBands.Items.Count > 0)
                        {
                            RadioButtonList rblFreightMethods = (RadioButtonList)rptBands.Items[RepeaterId].FindControl("rblFreightMethods");
                            Label lblBand = (Label)rptBands.Items[RepeaterId].FindControl("lblBand");
                            HiddenField HiddenBandId = (HiddenField)rptBands.Items[RepeaterId].FindControl("HiddenBandId");
                            BandName = lblBand.Text;
                            BandId = Convert.ToInt32(HiddenBandId.Value);
                            FreightMethodId = Convert.ToInt16(rblFreightMethods.SelectedValue);

                            if (rblCarriageMode.SelectedIndex == 0)
                            {
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.WeightParameter == true).ToList();

                                //objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.WeightParameter == true && x.BandId == Convert.ToInt32(BandId)).ToList();
                            }
                            else if (rblCarriageMode.SelectedIndex == 1)
                            {
                                //objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.CurrencyParameter == true && x.BandId == Convert.ToInt32(BandId)).ToList();
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.CurrencyParameter == true).ToList();
                            }
                            else
                            {
                                //objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue) && x.BandId == Convert.ToInt32(BandId)).ToList();
                                objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods.SelectedValue)).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (rblCarriageMode.SelectedIndex == 0)
                        {
                            objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.WeightParameter == true).ToList();
                            // objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.WeightParameter == true && x.BandId == Convert.ToInt32(BandId)).ToList();
                        }
                        else if (rblCarriageMode.SelectedIndex == 1)
                        {
                            // objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.CurrencyParameter == true && x.BandId == Convert.ToInt32(BandId)).ToList();
                            objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.CurrencyParameter == true).ToList();
                        }
                        else
                        {
                            objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue)).ToList();
                            // objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.BandId == Convert.ToInt32(BandId)).ToList();
                        }
                    }
                    //  objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.BandId == Convert.ToInt32(BandId)).ToList();
                    //} 
                    //objListFreight = objListFreight.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue)).ToList();

                    if (objListFreight.Count > 0)
                    {
                        rptBands.DataSource = objListFreight;
                        rptBands.DataBind();
                        // BindCurrentIndexRepeater(RepeaterId, objListFreight);
                        Button btnNextBand = (Button)rptBands.Items[rptBands.Items.Count - 1].FindControl("btnNextBand");
                        btnNextBand.Style.Add("display", "block");

                        divBandEditable.Visible = false;
                    }
                    else
                    {
                        rptBands.DataSource = null;
                        rptBands.DataBind();
                        divBandEditable.Visible = true;
                        lblBand1.Text = "Band1";
                        HiddenBandId1.Value = "1";
                        collapse1.Attributes["class"] = "collapse.in";
                        GenerateBlankBand();
                        //for (int i = 1; i < rptBands.Items.Count; i++)
                        //{
                        //    HtmlGenericControl dvMainBand = (HtmlGenericControl)rptBands.Items[i].FindControl("dvMainBand");
                        //    dvMainBand.Style.Add("display", "none");
                        //}
                        //Button btnNextBand = (Button)rptBands.Items[rptBands.Items.Count - 1].FindControl("btnNextBand");
                        //btnNextBand.Style.Add("display", "none");

                        //HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[0].FindControl("divCollapse");
                        //divCollapse.Attributes["class"] = "collapse-in";
                        //ClearRepeater(RepeaterId,method);
                    }
                    #region Already commented
                    //}
                    //else
                    //{
                    //    if (Convert.ToInt32(rblFreightMethods1.SelectedValue) > 0)
                    //    {
                    //        FreightMethodId = Convert.ToInt16(rblFreightMethods1.SelectedValue);

                    //        objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightMethodId == Convert.ToInt16(rblFreightMethods1.SelectedValue) && x.BandId == Convert.ToInt32(HiddenBandId1.Value)).ToList();

                    //    }
                    //    else
                    //    {
                    //        objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.BandId == Convert.ToInt32(HiddenBandId1.Value)).ToList();
                    //    } //objListFreight = objListFreight.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue)).ToList();

                    //    if (objListFreight.Count > 0)
                    //    {
                    //        //  BindCurrentIndexRepeater(RepeaterId, objListFreight);
                    //        rptBands.DataSource = objListFreight;
                    //        rptBands.DataBind();
                    //        divBandEditable.Visible = false;
                    //    }
                    //    else
                    //    {
                    //        //ClearRepeater(RepeaterId);
                    //    }
                    //} 
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindCurrentIndexRepeater(int inDex, List<FreightManagementBE> objListFreight)
        {
            try
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                FreightManagementBE CurrFreightConfig = new FreightManagementBE();
                CurrFreightConfig = objListFreight.FirstOrDefault(x => x.FreightConfigurationId > 0);
                FreightId = Convert.ToInt32(CurrFreightConfig.FreightConfigurationId);
                BandId = Convert.ToInt32(CurrFreightConfig.BandId);
                Button btnNextBand = (Button)rptBands.Items[inDex].FindControl("btnNextBand");
                DropDownList ddlCarrierService = (DropDownList)rptBands.Items[inDex].FindControl("ddlCarrierService");
                DropDownList ddlFreightTable = (DropDownList)rptBands.Items[inDex].FindControl("ddlFreightTable");
                Repeater rptMinimumOrderValue = (Repeater)rptBands.Items[inDex].FindControl("rptMinimumOrderValue");
                Repeater rptValueAppliedAbove = (Repeater)rptBands.Items[inDex].FindControl("rptValueAppliedAbove");
                Repeater rptValueAppliedBelow = (Repeater)rptBands.Items[inDex].FindControl("rptValueAppliedBelow");
                Repeater rptValue = (Repeater)rptBands.Items[inDex].FindControl("rptValue");
                TextBox txtWeightFrom = (TextBox)rptBands.Items[inDex].FindControl("txtWeightFrom");
                TextBox txtWeightTo = (TextBox)rptBands.Items[inDex].FindControl("txtWeightTo");
                TextBox txtTransitTime = (TextBox)rptBands.Items[inDex].FindControl("txtTransitTime");
                TextBox txtCustomText = (TextBox)rptBands.Items[inDex].FindControl("txtCustomText");
                TextBox txtWeight = (TextBox)rptBands.Items[inDex].FindControl("txtWeight");
                Label lblBand = (Label)rptBands.Items[inDex].FindControl("lblBand");
                Label lblVAA = (Label)rptBands.Items[inDex].FindControl("lblVAA");
                RadioButton rdoWeightKG = (RadioButton)rptBands.Items[inDex].FindControl("rdoWeightKG");
                RadioButton rdoPerOrder = (RadioButton)rptBands.Items[inDex].FindControl("rdoPerOrder");
                RadioButtonList rblFreightMethods = (RadioButtonList)rptBands.Items[inDex].FindControl("rblFreightMethods");
                CheckBox chkDisallowOrder = (CheckBox)rptBands.Items[inDex].FindControl("chkDisallowOrder");
                CheckBox checkBandActive = (CheckBox)rptBands.Items[inDex].FindControl("checkBandActive");
                HtmlGenericControl ulWeight = (HtmlGenericControl)rptBands.Items[inDex].FindControl("ulWeight");
                HtmlGenericControl ulValue = (HtmlGenericControl)rptBands.Items[inDex].FindControl("ulValue");
                HtmlGenericControl divFreightTable = (HtmlGenericControl)rptBands.Items[inDex].FindControl("divFreightTable");
                HtmlGenericControl divPerOrder = (HtmlGenericControl)rptBands.Items[inDex].FindControl("divPerOrder");
                HiddenField hiddenModeId = (HiddenField)rptBands.Items[inDex].FindControl("hiddenModeId");
                HiddenField HiddenFreightId = (HiddenField)rptBands.Items[inDex].FindControl("HiddenFreightId");

                hiddenModeId.Value = Convert.ToString(CurrFreightConfig.FreightModeId);
                HiddenFreightId.Value = Convert.ToString(CurrFreightConfig.FreightConfigurationId);
                rblFreightMethods.SelectedValue = Convert.ToString(CurrFreightConfig.FreightMethodId);


                ddlCarrierService.SelectedValue = Convert.ToString(CurrFreightConfig.CarrierServiceId);

                FreightManagementBE.FreightTransitTime objTran = lstTransitTime.FirstOrDefault(x => x.FreightConfigurationId == Convert.ToInt32(FreightId) && x.LanguageId == Convert.ToInt32(ddlLanguage.SelectedValue));
                txtTransitTime.Text = objTran != null ? Convert.ToString(objTran.TransitTimeName) : "";
                txtCustomText.Text = objTran != null ? Convert.ToString(objTran.CustomText) : "";

                if (Convert.ToBoolean(CurrFreightConfig.WeightParameter) == true)
                {
                    txtWeightFrom.Text = Convert.ToString(lstWeightBandParameters.FirstOrDefault(x => x.FreightConfigurationId == FreightId).WeightFrom);
                    txtWeightTo.Text = Convert.ToString(lstWeightBandParameters.FirstOrDefault(x => x.FreightConfigurationId == FreightId).WeightTo);
                    ulWeight.Style.Add("display", "block");
                    ulValue.Style.Add("display", "none");
                    rblCarriageMode.SelectedValue = "Weight";
                }
                else
                {
                    if (Convert.ToBoolean(CurrFreightConfig.CurrencyParameter) == true)
                    {
                        ulWeight.Style.Add("display", "none");
                        ulValue.Style.Add("display", "block");
                        rblCarriageMode.SelectedValue = "Value";
                    }
                    else
                    {
                        ulWeight.Style.Add("display", "block");
                        ulValue.Style.Add("display", "none");
                    }
                }

                if (rblFreightMethods.SelectedItem.Value.Equals("2"))
                {
                    lblVAA.Text = "% Applied Above :";
                }
                else if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                {
                    lblVAA.Text = "Multiplier On Cost Applied Above :";
                }
                else
                {
                    lblVAA.Text = "Value Applied Above :";
                }


                if (rblFreightMethods.SelectedValue.Equals("3"))
                {
                    divFreightTable.Style.Add("display", "block");
                    ddlFreightTable.SelectedValue = Convert.ToString(LstFreightTables.FirstOrDefault(x => x.FreightTableId == CurrFreightConfig.FreightTableId).FreightTableId);
                    divFreightTable.Style.Add("display", "block");
                }
                else
                {
                    divFreightTable.Style.Add("display", "none");
                }
                if (!rblFreightMethods.SelectedValue.Equals("2") && !rblFreightMethods.SelectedValue.Equals("3"))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(CurrFreightConfig.PerBoxPerOrder)))
                    {
                        if (Convert.ToString(CurrFreightConfig.PerBoxPerOrder) == "PerBox")
                        {
                            rdoWeightKG.Checked = true;
                            txtWeight.Text = Convert.ToString(Convert.ToString(CurrFreightConfig.WeightBand));
                        }
                        else
                        {
                            rdoPerOrder.Checked = true;
                        }
                    }
                }

                //if (rblFreightMethods.SelectedValue.Equals("2") || rblFreightMethods.SelectedValue.Equals("3"))
                //{
                //    divPerOrder.Visible = false;
                //}
                //else { divPerOrder.Visible = true; }



                if (chkDisallowOrder != null)
                { chkDisallowOrder.Checked = Convert.ToBoolean(CurrFreightConfig.DisAllowOrderBelowMOV); }

                if (checkBandActive != null)
                { checkBandActive.Checked = Convert.ToBoolean(lstFreightActiveRegionBand.FirstOrDefault(x => x.BandId == BandId && x.FreightRegionId == Convert.ToInt32(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt32(rblFreightModes.SelectedValue)).IsActive); }

                rptValueAppliedAbove.Visible = true;
                rptMinimumOrderValue.Visible = true;
                rptValueAppliedBelow.Visible = true;
                rptValueAppliedAbove.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValueAppliedAbove.DataBind();

                rptMinimumOrderValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptMinimumOrderValue.DataBind();

                rptValueAppliedBelow.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValueAppliedBelow.DataBind();

                rptValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValue.DataBind();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        private void ClearRepeater(int inDex, bool method)
        {
            try
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();

                Button btnNextBand = (Button)rptBands.Items[inDex].FindControl("btnNextBand");
                DropDownList ddlCarrierService = (DropDownList)rptBands.Items[inDex].FindControl("ddlCarrierService");
                DropDownList ddlFreightTable = (DropDownList)rptBands.Items[inDex].FindControl("ddlFreightTable");
                Repeater rptMinimumOrderValue = (Repeater)rptBands.Items[inDex].FindControl("rptMinimumOrderValue");
                Repeater rptValueAppliedAbove = (Repeater)rptBands.Items[inDex].FindControl("rptValueAppliedAbove");
                Repeater rptValueAppliedBelow = (Repeater)rptBands.Items[inDex].FindControl("rptValueAppliedBelow");
                Repeater rptValue = (Repeater)rptBands.Items[inDex].FindControl("rptValue");
                TextBox txtWeightFrom = (TextBox)rptBands.Items[inDex].FindControl("txtWeightFrom");
                TextBox txtWeightTo = (TextBox)rptBands.Items[inDex].FindControl("txtWeightTo");
                TextBox txtTransitTime = (TextBox)rptBands.Items[inDex].FindControl("txtTransitTime");
                TextBox txtCustomText = (TextBox)rptBands.Items[inDex].FindControl("txtCustomText");
                TextBox txtWeight = (TextBox)rptBands.Items[inDex].FindControl("txtWeight");
                Label lblBand = (Label)rptBands.Items[inDex].FindControl("lblBand");
                Label lblVAA = (Label)rptBands.Items[inDex].FindControl("lblVAA");
                RadioButton rdoWeightKG = (RadioButton)rptBands.Items[inDex].FindControl("rdoWeightKG");
                RadioButton rdoPerOrder = (RadioButton)rptBands.Items[inDex].FindControl("rdoPerOrder");
                RadioButtonList rblFreightMethods = (RadioButtonList)rptBands.Items[inDex].FindControl("rblFreightMethods");
                CheckBox chkDisallowOrder = (CheckBox)rptBands.Items[inDex].FindControl("chkDisallowOrder");
                CheckBox checkBandActive = (CheckBox)rptBands.Items[inDex].FindControl("checkBandActive");
                HtmlGenericControl divFreightTable = (HtmlGenericControl)rptBands.Items[inDex].FindControl("divFreightTable");
                HtmlGenericControl divPerOrder = (HtmlGenericControl)rptBands.Items[inDex].FindControl("divPerOrder");
                HiddenField hiddenModeId = (HiddenField)rptBands.Items[inDex].FindControl("hiddenModeId");
                HiddenField HiddenFreightId = (HiddenField)rptBands.Items[inDex].FindControl("HiddenFreightId");


                BindCarrierService(ref ddlCarrierService);
                BindFreightTable(ref ddlFreightTable);

                rptValueAppliedAbove.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValueAppliedAbove.DataBind();

                rptMinimumOrderValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptMinimumOrderValue.DataBind();

                rptValueAppliedBelow.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptValueAppliedBelow.DataBind();

                if (method == false)
                {
                    txtWeightFrom.Text = string.Empty;
                    txtWeightTo.Text = string.Empty;

                    rptValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                    rptValue.DataBind();
                }


                txtTransitTime.Text = string.Empty;
                txtCustomText.Text = string.Empty;

                chkDisallowOrder.Checked = false;
                txtWeight.Text = string.Empty;
                rdoPerOrder.Checked = false;
                rdoWeightKG.Checked = false;
                hiddenModeId.Value = string.Empty;
                HiddenFreightId.Value = string.Empty;

                if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                {
                    divFreightTable.Style.Add("display", "block");
                }
                else
                {
                    divFreightTable.Style.Add("display", "none");
                }
                //if (rblFreightMethods.SelectedValue.Equals("2") || rblFreightMethods.SelectedValue.Equals("3"))
                //{
                //    divPerOrder.Visible = false;
                //}
                //else { divPerOrder.Visible = true; }

                if (rblFreightMethods.SelectedItem.Value.Equals("2"))
                {
                    lblVAA.Text = "% Applied Above :";
                }
                else if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                {
                    lblVAA.Text = "Multiplier On Cost Applied Above :";
                }
                else
                {
                    lblVAA.Text = "Value Applied Above :";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GenerateBlankBand()
        {
            try
            {
                BindCurrency();
                BindCarrierService();
                BindFreightTable();
                BindFreightMethods();
                ClearInputs();
                if (rblFreightMethods1.SelectedValue.Equals("3"))
                {
                    divFreightTable1.Style.Add("display", "block");
                }
                else
                {
                    divFreightTable1.Style.Add("display", "none");
                }
                //if (rblFreightMethods1.SelectedValue.Equals("2") || rblFreightMethods1.SelectedValue.Equals("3"))
                //{
                //    divPerOrder1.Visible = false;
                //}
                //else { divPerOrder1.Visible = true; }
                rdoWeightKG1.Checked = true;
                if (rblCarriageMode.SelectedValue == "Weight")
                {
                    ulWeight1.Style.Add("display", "block");
                    ulValue1.Style.Add("display", "none");
                }
                else if (rblCarriageMode.SelectedValue == "Value")
                {
                    ulWeight1.Style.Add("display", "none");
                    ulValue1.Style.Add("display", "block");
                }
                else
                {
                    rblCarriageMode.SelectedValue = "Weight";
                    ulWeight1.Style.Add("display", "block");
                    ulValue1.Style.Add("display", "none");
                }
                chkDisallowOrder1.Checked = false;

                //lblBand1.Text = BandName;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindBandRepeater()
        {
            try
            {
                if (lstAllFreightSettings != null)
                {
                    if (rblCarriageMode.SelectedIndex == 0)
                    {
                        objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.WeightParameter == true).ToList();
                    }
                    else if (rblCarriageMode.SelectedIndex == 1)
                    {
                        objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.CurrencyParameter == true).ToList();
                    }
                    else
                    {
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                        {
                            objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue)).ToList();
                        }
                        else
                        {
                            //for Multiple freight source country - added by Hardik on "30/May/2017"
                            objListFreight = lstAllFreightSettingsMultipleCountry.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightSourceCountryCode == ddlSelectFreightSrcCountry.SelectedValue).ToList();
                        }

                    }

                    //objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue)).ToList();
                    // objListFreight = lstAllFreightCreatedBand.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue)).ToList();
                    if (objListFreight.Count > 0)
                    {
                        rptBands.DataSource = objListFreight;
                        rptBands.DataBind();
                        HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[0].FindControl("divCollapse");
                        Button btnNextBand = (Button)rptBands.Items[rptBands.Items.Count - 1].FindControl("btnNextBand");

                        divCollapse.Attributes["class"] = "collapse-in";
                        btnNextBand.Style.Add("display", "block");

                        //for (int i = 0; i < rptBands.Items.Count; i++)
                        //{
                        //    HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");
                        //    if (i == 0)
                        //    {
                        //        divCollapse.Attributes["class"] = "collapse-in";
                        //    }
                        //    else
                        //    {
                        //        divCollapse.Attributes["class"] = "collapse";
                        //    }
                        //}
                        //if (rptBands.Items.Count > 1)
                        //{
                        //    for (int i = 1; i < rptBands.Items.Count; i++)
                        //    {
                        //        ClearRepeater(i);
                        //    }
                        //}
                    }
                    else
                    {
                        rptBands.DataSource = null;
                        rptBands.DataBind();
                        divBandEditable.Visible = true;
                        lblBand1.Text = "Band1";
                        HiddenBandId1.Value = "1";
                        collapse1.Attributes["class"] = "collapse.in";
                        GenerateBlankBand();
                    }
                }
                else
                {
                    rptBands.DataSource = null;
                    rptBands.DataBind();
                    divBandEditable.Visible = true;
                    lblBand1.Text = "Band1";
                    HiddenBandId1.Value = "1";
                    collapse1.Attributes["class"] = "collapse.in";
                    GenerateBlankBand();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        void BindCarrierService(ref DropDownList ddlCarrierService)
        {
            try
            {
                //List<Freight_Carrier_ServiceBE> lstFreightCarrierService = FetchFreightCarrierService();
                ddlCarrierService.DataSource = lstFreightCarrierService;
                ddlCarrierService.DataTextField = "CARRIER_SERVICE_TEXT";
                ddlCarrierService.DataValueField = "CARRIER_SERVICE_ID";
                ddlCarrierService.DataBind();

                ddlCarrierService.Items.Insert(0, new ListItem("Please Select", "0"));
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        void BindFreightTable(ref DropDownList ddlFreightTable)
        {
            try
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    ddlFreightTable.DataSource = LstFreightTables;
                    ddlFreightTable.DataTextField = "TableName";
                    ddlFreightTable.DataValueField = "FreightTableId";
                    ddlFreightTable.DataBind();
                    ddlFreightTable.Items.Insert(0, new ListItem("--Select Freight Table--", "0"));
                }
                else
                {
                    lstFreightTableID = FreightManagementBL.GetAllFreightTableID<FreightManagementBE.FreightTables>(Convert.ToString(Session["CountryCode"]));   
                    ddlFreightTable.DataSource = lstFreightTableID;
                    ddlFreightTable.DataTextField = "TableName";
                    ddlFreightTable.DataValueField = "FreightTableId";
                    ddlFreightTable.DataBind();
                    ddlFreightTable.Items.Insert(0, new ListItem("--Select Freight Table--", "0"));
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        void BindCarrierService()
        {
            try
            {
                ddlCarrierService1.Items.Clear();
                //List<Freight_Carrier_ServiceBE> lstFreightCarrierService = FetchFreightCarrierService();
                ddlCarrierService1.DataSource = lstFreightCarrierService;
                ddlCarrierService1.DataTextField = "CARRIER_SERVICE_TEXT";
                ddlCarrierService1.DataValueField = "CARRIER_SERVICE_ID";
                ddlCarrierService1.DataBind();

                ddlCarrierService1.Items.Insert(0, new ListItem("Please Select", "0"));
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        void BindFreightTable()
        {
            try
            {
                ddlFreightTable1.Items.Clear();

                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    ddlFreightTable1.DataSource = LstFreightTables;
                    ddlFreightTable1.DataTextField = "TableName";
                    ddlFreightTable1.DataValueField = "FreightTableId";
                    ddlFreightTable1.DataBind();
                    ddlFreightTable1.Items.Insert(0, new ListItem("--Select Freight Table--", "0"));
                }
                else
                {
                    lstFreightTableID = FreightManagementBL.GetAllFreightTableID<FreightManagementBE.FreightTables>(Convert.ToString(Session["CountryCode"]));     
                    ddlFreightTable1.DataSource = lstFreightTableID;
                    ddlFreightTable1.DataTextField = "TableName";
                    ddlFreightTable1.DataValueField = "FreightTableId";
                    ddlFreightTable1.DataBind();
                    ddlFreightTable1.Items.Insert(0, new ListItem("--Select Freight Table--", "0"));
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        void BindFreightMethods()
        {
            try
            {
                rblFreightMethods1.DataSource = LstFreightMethods;
                rblFreightMethods1.DataTextField = "FreightMethodName";
                rblFreightMethods1.DataValueField = "FreightMethodId";
                rblFreightMethods1.DataBind();
                if (FreightMethodId > 0)
                {
                    rblFreightMethods1.SelectedValue = Convert.ToString(FreightMethodId);
                }
                else
                {
                    rblFreightMethods1.SelectedValue = "1";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        protected void rptBands_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    HiddenRepeaterItemCount.Value = Convert.ToString(e.Item.ItemIndex);
                    FreightId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "FreightConfigurationId"));
                    BandId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BandId"));
                    Label lblBand = (Label)e.Item.FindControl("lblBand");
                    Label lblVAA = (Label)e.Item.FindControl("lblVAA");
                    lblBand.Text = Convert.ToString(lstFreightBand.FirstOrDefault(x => x.BandId == BandId).BandName);
                    //lblBand.Text = "Band" + Convert.ToInt32(Convert.ToInt32(e.Item.ItemIndex) + 1);
                    TextBox txtWeightFrom = (TextBox)e.Item.FindControl("txtWeightFrom");
                    TextBox txtWeightTo = (TextBox)e.Item.FindControl("txtWeightTo");
                    TextBox txtTransitTime = (TextBox)e.Item.FindControl("txtTransitTime");
                    TextBox txtCustomText = (TextBox)e.Item.FindControl("txtCustomText");
                    TextBox txtWeight = (TextBox)e.Item.FindControl("txtWeight");
                    RadioButton rdoWeightKG = (RadioButton)e.Item.FindControl("rdoWeightKG");
                    RadioButton rdoPerOrder = (RadioButton)e.Item.FindControl("rdoPerOrder");
                    RadioButtonList rblFreightMethods = (RadioButtonList)e.Item.FindControl("rblFreightMethods");
                    DropDownList ddlCarrierService = (DropDownList)e.Item.FindControl("ddlCarrierService");
                    DropDownList ddlFreightTable = (DropDownList)e.Item.FindControl("ddlFreightTable");
                    Repeater rptValueAppliedAbove = (Repeater)e.Item.FindControl("rptValueAppliedAbove");
                    Repeater rptMinimumOrderValue = (Repeater)e.Item.FindControl("rptMinimumOrderValue");
                    Repeater rptValueAppliedBelow = (Repeater)e.Item.FindControl("rptValueAppliedBelow");
                    Repeater rptValue = (Repeater)e.Item.FindControl("rptValue");
                    CheckBox chkDisallowOrder = (CheckBox)e.Item.FindControl("chkDisallowOrder");
                    CheckBox checkBandActive = (CheckBox)e.Item.FindControl("checkBandActive");
                    HtmlGenericControl ulWeight = (HtmlGenericControl)e.Item.FindControl("ulWeight");
                    HtmlGenericControl ulValue = (HtmlGenericControl)e.Item.FindControl("ulValue");
                    HtmlGenericControl divFreightTable = (HtmlGenericControl)e.Item.FindControl("divFreightTable");
                    HtmlGenericControl divPerOrder = (HtmlGenericControl)e.Item.FindControl("divPerOrder");
                    HtmlAnchor aBand = (HtmlAnchor)e.Item.FindControl("aBand");
                    HtmlGenericControl divCollapse = (HtmlGenericControl)e.Item.FindControl("divCollapse");
                    HtmlGenericControl SpnItemId = (HtmlGenericControl)e.Item.FindControl("SpnItemId");
                    HiddenField hiddenModeId = (HiddenField)e.Item.FindControl("hiddenModeId");

                    hiddenModeId.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FreightModeId"));

                    // divCollapse.ID = divCollapse.ClientID + SpnItemId.InnerHtml;
                    aBand.HRef = "#" + divCollapse.ClientID;

                    BindCarrierService(ref ddlCarrierService);
                    BindFreightTable(ref ddlFreightTable);

                    rblFreightMethods.DataSource = LstFreightMethods;
                    rblFreightMethods.DataTextField = "FreightMethodName";
                    rblFreightMethods.DataValueField = "FreightMethodId";
                    rblFreightMethods.DataBind();
                    rblFreightMethods.SelectedValue = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "freightMethodId"));

                    ddlCarrierService.SelectedValue = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CarrierServiceId"));

                    FreightManagementBE.FreightTransitTime objTran = lstTransitTime.FirstOrDefault(x => x.FreightConfigurationId == Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "FreightConfigurationId")) && x.LanguageId == Convert.ToInt32(ddlLanguage.SelectedValue));
                    txtTransitTime.Text = objTran != null ? Convert.ToString(objTran.TransitTimeName) : "";
                    txtCustomText.Text = objTran != null ? Convert.ToString(objTran.CustomText) : "";

                    if (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "WeightParameter")) == true)
                    {
                        txtWeightFrom.Text = Convert.ToString(lstWeightBandParameters.FirstOrDefault(x => x.FreightConfigurationId == FreightId).WeightFrom);
                        txtWeightTo.Text = Convert.ToString(lstWeightBandParameters.FirstOrDefault(x => x.FreightConfigurationId == FreightId).WeightTo);
                        ulWeight.Style.Add("display", "block");
                        ulValue.Style.Add("display", "none");
                    }
                    else
                    {
                        if (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "CurrencyParameter")) == true)
                        {
                            ulWeight.Style.Add("display", "none");
                            ulValue.Style.Add("display", "block");
                        }
                        else
                        {
                            ulWeight.Style.Add("display", "block");
                            ulValue.Style.Add("display", "none");
                        }
                    }

                    //if (lblBand.Text == "Band1")
                    //{
                    rblFreightModes.SelectedValue = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "freightModeId"));
                    if (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "WeightParameter")) == true)
                    {
                        rblCarriageMode.SelectedValue = "Weight";
                    }
                    else
                    {
                        if (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "CurrencyParameter")) == true)
                        {
                            rblCarriageMode.SelectedValue = "Value";
                        }
                    }
                    //}

                    if (rblFreightMethods.SelectedItem.Value.Equals("2"))
                    {
                        lblVAA.Text = "% Applied Above :";
                    }
                    else if (rblFreightMethods.SelectedItem.Value.Equals("3"))
                    {
                        lblVAA.Text = "Multiplier On Cost Applied Above :";
                    }
                    else
                    {
                        lblVAA.Text = "Value Applied Above :";
                    }


                    if (rblFreightMethods.SelectedValue.Equals("3"))
                    {
                        ddlFreightTable.SelectedValue = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FreightTableId"));
                        divFreightTable.Style.Add("display", "block");
                    }
                    else
                    {
                        divFreightTable.Style.Add("display", "none");
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PerBoxPerOrder"))))
                    {
                        if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PerBoxPerOrder")) == "PerBox")
                        {
                            rdoWeightKG.Checked = true;
                            txtWeight.Text = Convert.ToString(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "WeightBand")));
                        }
                        else
                        {
                            rdoPerOrder.Checked = true;
                        }
                    }

                    //if (rblFreightMethods.SelectedValue.Equals("2") || rblFreightMethods.SelectedValue.Equals("3"))
                    //{
                    //    divPerOrder.Visible = false;
                    //}
                    //else { divPerOrder.Visible = true; }

                    if (chkDisallowOrder != null)
                    { chkDisallowOrder.Checked = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "DisAllowOrderBelowMOV")); }

                    if (checkBandActive != null)
                    { checkBandActive.Checked = Convert.ToBoolean(lstFreightActiveRegionBand.FirstOrDefault(x => x.BandId == BandId && x.FreightRegionId == Convert.ToInt32(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt32(rblFreightModes.SelectedValue)).IsActive); }

                    rptValueAppliedAbove.Visible = true;
                    rptMinimumOrderValue.Visible = true;
                    rptValueAppliedBelow.Visible = true;
                    rptValueAppliedAbove.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                    rptValueAppliedAbove.DataBind();

                    rptMinimumOrderValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                    rptMinimumOrderValue.DataBind();

                    rptValueAppliedBelow.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                    rptValueAppliedBelow.DataBind();

                    rptValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                    rptValue.DataBind();


                }

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnNextBandRpt_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnNextBandRpt = (Button)sender;
                Int32 inDex = Convert.ToInt32(btnNextBandRpt.CommandArgument);
                HiddenField HiddenBandId = (HiddenField)rptBands.Items[inDex].FindControl("HiddenBandId");
                HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[inDex].FindControl("divCollapse");
                Int32 NextIndex = inDex + 1;
                int bandExist = lstFreightActiveRegionBand.Where(x => x.BandId == Convert.ToInt32(Convert.ToInt32(HiddenBandId.Value) + 1) && x.FreightRegionId == Convert.ToInt32(ddlSelectRegion.SelectedValue)).ToList().Count();
                if (bandExist > 0)
                {
                    objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.BandId == Convert.ToInt32(Convert.ToInt32(HiddenBandId.Value) + 1)).ToList();
                    if (objListFreight.Count > 0)
                    {
                        BindCurrentIndexRepeater(NextIndex, objListFreight);
                        for (int i = 0; i < rptBands.Items.Count; i++)
                        {
                            HtmlGenericControl div = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");
                            if (NextIndex == i)
                            {
                                div.Attributes["class"] = "collapse.in";
                            }
                            else
                            {
                                div.Attributes["class"] = "collapse";
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < rptBands.Items.Count; i++)
                        {
                            HtmlGenericControl div = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");

                            div.Attributes["class"] = "collapse";
                        }
                        lblBand1.Text = "Band" + Convert.ToInt32(Convert.ToInt32(HiddenBandId.Value) + 1);
                        HiddenBandId1.Value = Convert.ToInt32(Convert.ToInt32(HiddenBandId.Value) + 1).ToString();
                        divBandEditable.Visible = true;
                        GenerateBlankBand();
                        collapse1.Attributes["class"] = "collapse.in";

                    }


                    BandActiveSession.Value = Convert.ToString(Convert.ToInt32(Convert.ToInt32(HiddenBandId.Value) + 1));
                }
                else
                {
                    for (int i = 0; i < rptBands.Items.Count; i++)
                    {
                        HtmlGenericControl div = (HtmlGenericControl)rptBands.Items[i].FindControl("divCollapse");

                        div.Attributes["class"] = "collapse";
                    }
                    lblBand1.Text = "Band" + Convert.ToInt32(Convert.ToInt32(HiddenBandId.Value) + 1);
                    HiddenBandId1.Value = Convert.ToInt32(Convert.ToInt32(HiddenBandId.Value) + 1).ToString();
                    divBandEditable.Visible = true;
                    GenerateBlankBand();
                    collapse1.Attributes["class"] = "collapse.in";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnNextBand_Click(object sender, EventArgs e)
        {
            try
            {
                divBandEditable.Visible = true;
                lblBand1.Text = "Band" + BandId + 1;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSaveFreightConfigRpt_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSaveFreightConfigRpt = (Button)sender;
                Int32 inDex = Convert.ToInt32(btnSaveFreightConfigRpt.CommandArgument);
                bool IsInsert = false;
                #region
                Button btnNextBand = (Button)rptBands.Items[inDex].FindControl("btnNextBand");
                DropDownList ddlCarrierService = (DropDownList)rptBands.Items[inDex].FindControl("ddlCarrierService");
                DropDownList ddlFreightTable = (DropDownList)rptBands.Items[inDex].FindControl("ddlFreightTable");
                Repeater rptMinimumOrderValue = (Repeater)rptBands.Items[inDex].FindControl("rptMinimumOrderValue");
                Repeater rptValueAppliedAbove = (Repeater)rptBands.Items[inDex].FindControl("rptValueAppliedAbove");
                Repeater rptValueAppliedBelow = (Repeater)rptBands.Items[inDex].FindControl("rptValueAppliedBelow");
                Repeater rptValue = (Repeater)rptBands.Items[inDex].FindControl("rptValue");
                TextBox txtWeightFrom = (TextBox)rptBands.Items[inDex].FindControl("txtWeightFrom");
                TextBox txtWeightTo = (TextBox)rptBands.Items[inDex].FindControl("txtWeightTo");
                TextBox txtWeight = (TextBox)rptBands.Items[inDex].FindControl("txtWeight");
                TextBox txtTransitTime = (TextBox)rptBands.Items[inDex].FindControl("txtTransitTime");
                TextBox txtCustomText = (TextBox)rptBands.Items[inDex].FindControl("txtCustomText");
                Label lblBand = (Label)rptBands.Items[inDex].FindControl("lblBand");
                RadioButton rdoWeightKG = (RadioButton)rptBands.Items[inDex].FindControl("rdoWeightKG");
                RadioButton rdoPerOrder = (RadioButton)rptBands.Items[inDex].FindControl("rdoPerOrder");
                RadioButtonList rblFreightMethods = (RadioButtonList)rptBands.Items[inDex].FindControl("rblFreightMethods");
                CheckBox chkDisallowOrder = (CheckBox)rptBands.Items[inDex].FindControl("chkDisallowOrder");
                CheckBox checkBandActive = (CheckBox)rptBands.Items[inDex].FindControl("checkBandActive");
                Button btnNextBand1 = (Button)rptBands.Items[rptBands.Items.Count - 1].FindControl("btnNextBand");

                Boolean IsValidCarrierServiceId = Regex.IsMatch(ddlCarrierService.SelectedValue, ValidRegEx.RegEx_ValidMultiDigitNo(3));
                Boolean IsValidCarrierServiceText = Regex.IsMatch(ddlCarrierService.SelectedItem.Text, ValidRegEx.RegEx_ValidAlpanumericStringWithSafeSpecialChars);

                //  Boolean IsValidTransitTime = Regex.IsMatch(txtTransitTime.Text, ValidRegEx.RegEx_ValidAlpanumerichyphen);// ValidRegEx.RegEx_ValidSingleDigitNumber);
                string MinimumOrderValue = GetAssignedMinimumOrderValues(ref rptMinimumOrderValue);
                string ValueAppliedAbove = GetAssignedValueAppliedAbove(ref rptValueAppliedAbove);
                string ValueAppliedBelow = GetAssignedValueAppliedBelow(ref rptValueAppliedBelow);
                string CurrencyFromToValue = GetAssignedCurrencyValues(ref rptValue);
                string WeightFromToValue = GetAssignedWeightValues(ref txtWeightFrom, ref txtWeightTo);
                if (MinimumOrderValue == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Minimum Order Value can be applied upto 4 decimal only.", AlertType.Warning);
                    return;
                }
                if (ValueAppliedAbove == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Value applied above can be applied upto 4 decimal only.", AlertType.Warning);
                    return;
                }
                if (ValueAppliedBelow == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Value applied below can be applied upto 4 decimal only.", AlertType.Warning);
                    return;
                }

                if (rdoWeightKG.Checked == true)
                {
                    if (txtWeight.Text == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Weight Per KG Should not be empty", AlertType.Warning);
                        return;
                    }
                }


                if (rblCarriageMode.Items[0].Selected)
                {
                    if (WeightFromToValue == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Weight Band Parameter Should not be empty.", AlertType.Warning);
                        return;
                    }
                }
                if (rblCarriageMode.Items[1].Selected)
                {
                    if (CurrencyFromToValue == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Currency Band Parameter Should not be empty.", AlertType.Warning);
                        return;
                    }
                }
                if (IsValidCarrierServiceId && IsValidCarrierServiceText)
                {
                    #region
                    FreightManagementBE CurrFreightConfig = new FreightManagementBE();

                    CurrFreightConfig.CarrierServiceId = ddlCarrierService.SelectedValue.To_Int16();
                    CurrFreightConfig.CarrierServiceText = ddlCarrierService.SelectedItem.Text;

                    //CurrFreightConfig.TransitTime = txtTransitTime.Text.To_Int16();
                    CurrFreightConfig.TransitTime = txtTransitTime.Text;
                    CurrFreightConfig.CustomText = txtCustomText.Text;
                    if (rblCarriageMode.Items[0].Selected)
                    {
                        CurrFreightConfig.WeightParameter = true;
                        CurrFreightConfig.CurrencyParameter = false;
                    }
                    else
                    {
                        CurrFreightConfig.WeightParameter = false;
                        CurrFreightConfig.CurrencyParameter = true;
                    }
                    CurrFreightConfig.WeightFromToValueCM = WeightFromToValue;
                    CurrFreightConfig.CurrencyFromToValueCM = CurrencyFromToValue;
                    CurrFreightConfig.MinOrderValueCM = MinimumOrderValue;
                    CurrFreightConfig.ValueAppliedAboveCM = ValueAppliedAbove; //!string.IsNullOrEmpty(txtVAA.Text.Trim()) ? txtVAA.Text.Trim().To_Float() : 0;
                    CurrFreightConfig.ValueAppliedBelowCM = ValueAppliedBelow;
                    //CurrFreightConfig.BandName = "Band1";
                    CurrFreightConfig.BandName = lblBand.Text;
                    if (checkBandActive.Checked == true)
                    {
                        CurrFreightConfig.IsActive = true;
                    }
                    else
                    { CurrFreightConfig.IsActive = false; }
                    CurrFreightConfig.FreightModeId = Convert.ToInt16(rblFreightModes.SelectedValue);
                    CurrFreightConfig.FreightRegionId = Convert.ToInt16(ddlSelectRegion.SelectedValue);
                    CurrFreightConfig.FreightMethodId = Convert.ToInt16(rblFreightMethods.SelectedValue);
                    CurrFreightConfig.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);

                    if (rdoWeightKG.Checked == true)
                    {
                        CurrFreightConfig.WeightBand = Convert.ToDouble(txtWeight.Text);
                        CurrFreightConfig.PerBoxPerOrder = "PerBox";
                    }
                    else
                    {
                        CurrFreightConfig.PerBoxPerOrder = "PerOrder";
                    }


                    if (ddlFreightTable.SelectedIndex > 0)
                    {
                        CurrFreightConfig.FreightTableId = Convert.ToInt32(ddlFreightTable.SelectedValue);
                    }
                    else
                    { CurrFreightConfig.FreightTableId = 0; }

                    if (chkDisallowOrder != null)
                        CurrFreightConfig.DisAllowOrderBelowMOV = chkDisallowOrder.Checked.Equals(true) ? true : false;

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                    {
                        IsInsert = FreightManagementBL.FreightManagementPerBand(CurrFreightConfig, DBAction.Insert);
                    }
                    else
                    {
                        CurrFreightConfig.FreightSourceCountryCode = ddlSelectFreightSrcCountry.SelectedValue;
                        IsInsert = FreightManagementBL.FreightManagementPerBandMultipleSourceCountry(CurrFreightConfig, DBAction.Insert);
                    }

                    if (IsInsert == true)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Freight Configuration setting saved successfully.", AlertType.Success);
                        btnNextBand1.Style.Add("display", "block");
                        CreateActivityLog("FreightRepeter btnSaveFreightConfigRpt", "Insert", ddlSelectRegion.SelectedValue + "_" + ddlLanguage.SelectedValue);
                    }

                    #endregion

                #endregion
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowFailure('There is an error saving Freight Configuration setting.');", true);
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSaveFreightConfig_Click1(object sender, EventArgs e)
        {
            try
            {
                bool IsInsert = false;
                Boolean IsValidCarrierServiceId = Regex.IsMatch(ddlCarrierService1.SelectedValue, ValidRegEx.RegEx_ValidMultiDigitNo(3));
                Boolean IsValidCarrierServiceText = Regex.IsMatch(ddlCarrierService1.SelectedItem.Text, ValidRegEx.RegEx_ValidAlpanumericStringWithSafeSpecialChars);
                //Boolean IsValidTransitTime = Regex.IsMatch(txtTransitTime1.Text, ValidRegEx.RegEx_ValidAlpanumerichyphen);// ValidRegEx.RegEx_ValidSingleDigitNumber);
                string MinimumOrderValue = GetAssignedMinimumOrderValues(ref rptMinimumOrderValue);
                string ValueAppliedAbove = GetAssignedValueAppliedAbove(ref rptValueAppliedAbove);
                string ValueAppliedBelow = GetAssignedValueAppliedBelow(ref rptValueAppliedBelow);
                string CurrencyFromToValue = GetAssignedCurrencyValues(ref rptValue);
                string WeightFromToValue = GetAssignedWeightValues();
                if (MinimumOrderValue == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Minimum Order Value can be applied upto 4 decimal only.", AlertType.Warning);
                    return;
                }
                if (ValueAppliedAbove == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Value applied above can be applied upto 4 decimal only.", AlertType.Warning);
                    return;

                }
                if (ValueAppliedBelow == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Value applied below can be applied upto 4 decimal only.", AlertType.Warning);
                    return;

                }
                if (divPerOrder1.Visible == true)
                {
                    if (rdoWeightKG1.Checked == true)
                    {
                        if (txtWeight1.Text == "")
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Weight Per KG Should not be empty", AlertType.Warning);
                            return;

                        }
                    }
                }

                if (rblCarriageMode.Items[0].Selected)
                {
                    if (WeightFromToValue == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Weight Band Parameter Should not be empty.", AlertType.Warning);
                        return;

                    }
                }
                if (rblCarriageMode.Items[1].Selected)
                {
                    if (CurrencyFromToValue == "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Currency Band Parameter Should not be empty.", AlertType.Warning);
                        return;

                    }
                }

                if (IsValidCarrierServiceId && IsValidCarrierServiceText)
                {
                    FreightManagementBE CurrFreightConfig = new FreightManagementBE();

                    CurrFreightConfig.CarrierServiceId = ddlCarrierService1.SelectedValue.To_Int16();
                    CurrFreightConfig.CarrierServiceText = ddlCarrierService1.SelectedItem.Text;

                    CurrFreightConfig.TransitTime = txtTransitTime1.Text;
                    CurrFreightConfig.CustomText = txtCustomText1.Text;
                    if (rblCarriageMode.Items[0].Selected)
                    {
                        CurrFreightConfig.WeightParameter = true;
                        CurrFreightConfig.CurrencyParameter = false;
                    }
                    else
                    {
                        CurrFreightConfig.WeightParameter = false;
                        CurrFreightConfig.CurrencyParameter = true;
                    }
                    CurrFreightConfig.WeightFromToValueCM = WeightFromToValue;
                    CurrFreightConfig.CurrencyFromToValueCM = CurrencyFromToValue;
                    CurrFreightConfig.MinOrderValueCM = MinimumOrderValue;
                    CurrFreightConfig.ValueAppliedAboveCM = ValueAppliedAbove; //!string.IsNullOrEmpty(txtVAA.Text.Trim()) ? txtVAA.Text.Trim().To_Float() : 0;
                    CurrFreightConfig.ValueAppliedBelowCM = ValueAppliedBelow;
                    CurrFreightConfig.BandName = lblBand1.Text;
                    if (checkBandActive1.Checked == true)
                    {
                        CurrFreightConfig.IsActive = true;
                    }
                    else
                    { CurrFreightConfig.IsActive = false; }
                    CurrFreightConfig.FreightModeId = Convert.ToInt16(rblFreightModes.SelectedValue);
                    CurrFreightConfig.FreightRegionId = Convert.ToInt16(ddlSelectRegion.SelectedValue);
                    CurrFreightConfig.FreightMethodId = Convert.ToInt16(rblFreightMethods1.SelectedValue);
                    CurrFreightConfig.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);

                    if (rdoWeightKG1.Checked == true)
                    {
                        CurrFreightConfig.WeightBand = Convert.ToDouble(txtWeight1.Text);
                        CurrFreightConfig.PerBoxPerOrder = "PerBox";
                    }
                    else
                    {
                        CurrFreightConfig.PerBoxPerOrder = "PerOrder";
                    }


                    if (ddlFreightTable1.SelectedIndex > 0)
                    {
                        CurrFreightConfig.FreightTableId = Convert.ToInt32(ddlFreightTable1.SelectedValue);
                    }
                    else
                    { CurrFreightConfig.FreightTableId = 0; }

                    if (chkDisallowOrder1 != null)
                        CurrFreightConfig.DisAllowOrderBelowMOV = chkDisallowOrder1.Checked.Equals(true) ? true : false;


                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                    {
                        IsInsert = FreightManagementBL.FreightManagementPerBand(CurrFreightConfig, DBAction.Insert);
                    }
                    else
                    {
                        CurrFreightConfig.FreightSourceCountryCode = ddlSelectFreightSrcCountry.SelectedValue;
                        IsInsert = FreightManagementBL.FreightManagementPerBandMultipleSourceCountry(CurrFreightConfig, DBAction.Insert);
                    }

                    if (IsInsert == true)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Freight Configuration setting saved successfully.", AlertType.Success);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowSuccess('Freight Configuration setting saved successfully.');", true);
                        divBandEditable.Visible = false;
                        InitialiseAllListData();
                        // objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue)).ToList();

                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                        {
                            objListFreight = lstAllFreightSettings.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue)).ToList();
                        }
                        else
                        {
                            objListFreight = lstAllFreightSettingsMultipleCountry.Where(x => x.FreightRegionId == Convert.ToInt16(ddlSelectRegion.SelectedValue) && x.FreightModeId == Convert.ToInt16(rblFreightModes.SelectedValue) && x.FreightSourceCountryCode == ddlSelectFreightSrcCountry.SelectedValue).ToList();
                        }

                        if (objListFreight.Count > 0)
                        {
                            rptBands.DataSource = objListFreight;
                            rptBands.DataBind();
                            HtmlGenericControl divCollapse = (HtmlGenericControl)rptBands.Items[0].FindControl("divCollapse");
                            Button btnNextBand = (Button)rptBands.Items[rptBands.Items.Count - 1].FindControl("btnNextBand");

                            divCollapse.Attributes["class"] = "collapse.in";
                            btnNextBand.Style.Add("display", "block");
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error saving Freight Configuration setting.", AlertType.Failure);
                // ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowFailure('There is an error saving Freight Configuration setting.');", true);
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (rptBands.Items.Count > 0)
            {
                divBandEditable.Visible = false;
            }
            else
            {
                divBandEditable.Visible = true;
            }
        }

        #region Added By Hardik on "22/May/2017"
        private void BindFreightSourceCountry()
        {
            try
            {
                ddlSelectFreightSrcCountry.Items.Clear();
                ddlSelectFreightSrcCountry.DataSource = lstFreightSrcCountryCode;
                ddlSelectFreightSrcCountry.DataTextField = "FreightSourceCountryName";
                ddlSelectFreightSrcCountry.DataValueField = "FreightSourceCountryCode";
                ddlSelectFreightSrcCountry.DataBind();

                //ddlSelectFreightSrcCountry.Items.Insert(0, new ListItem("--Select Freight Source country--", "0"));                
                ddlSelectFreightSrcCountry_SelectedIndexChanged(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlSelectFreightSrcCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strCountryCode = ddlSelectFreightSrcCountry.SelectedValue;
            Session["CountryCode"] = strCountryCode;
            lstFreightTableID = FreightManagementBL.GetAllFreightTableID<FreightManagementBE.FreightTables>(strCountryCode);
            divBandEditable.Visible = false;
            rblCarriageMode.Items[0].Selected = false;
            rblCarriageMode.Items[1].Selected = false;

            BindFeightMode();
            if (BandActiveSession.Value == "")
            {
                BandActiveSession.Value = "0";
            }
            BindBandRepeater();
            BindFreightTable();            
        }
        #endregion
    }
}

