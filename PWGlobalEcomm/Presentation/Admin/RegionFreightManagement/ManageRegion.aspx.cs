﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System.Threading;

namespace Presentation
{
    public partial class Admin_RegionFreightManagement_ManageRegion : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptServices;
        protected global::System.Web.UI.WebControls.DropDownList ddlSelectRegion, ddlLanguage;
        protected global::System.Web.UI.WebControls.TextBox txtRegionName, txtAddRegionName, txtUpdateRegionName;
        protected global::System.Web.UI.WebControls.Button btnSaveRegion, btnSave;
        protected global::System.Web.UI.WebControls.CheckBox chkService;

        Int16 LanguageId;


        List<FreightRegionManagementBE.ShipmentMethodMasterBE> objShipmentMethodMasterBEL = new List<FreightRegionManagementBE.ShipmentMethodMasterBE>();
        StoreBE objStoreBE = new StoreBE();

        protected void Page_Load(object sender, EventArgs e)
        {
            objStoreBE = StoreBL.GetStoreDetails();


            if (!IsPostBack)
            {
                BindLanguage(objStoreBE);
                FreightRegionManagementBE objFreightRegionManagementBE = new FreightRegionManagementBE();
                objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();


                rptServices.DataSource = objFreightRegionManagementBE.FreightShipmentMethodMasterLst;
                rptServices.DataBind();

                BindRegionName();

            }

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ShowModal", "<script>javascript:ShowModal();</script>");
        }
        protected void rptServices_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        private void BindRegionName()
        {
            FreightRegionManagementBE objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();

            if (objFreightRegionManagementBE.FreightRegionsBELst.Count > 0)
            {
                ddlSelectRegion.DataSource = objFreightRegionManagementBE.FreightRegionsBELst;
                ddlSelectRegion.DataTextField = "FreightRegionName";
                ddlSelectRegion.DataValueField = "FreightRegionId";
                ddlSelectRegion.DataBind();
                ddlSelectRegion.Items.Insert(0, new ListItem("--Select Region--", ""));
            }
        }

        private void BindLanguage(StoreBE objStoreBE)
        {
            if (objStoreBE.StoreLanguages.Count > 0)
            {
                ddlLanguage.DataSource = objStoreBE.StoreLanguages;
                ddlLanguage.DataTextField = "LanguageName";
                ddlLanguage.DataValueField = "LanguageId";
                ddlLanguage.DataBind();
                ddlLanguage.SelectedValue = Convert.ToString(objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId);
                LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
            }
            else
            {
                LanguageId = objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId;
            }
        }

        protected void ddlSelectRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSelectRegion.SelectedIndex > 0)
            {
                txtRegionName.Text = ddlSelectRegion.SelectedItem.Text;
                FreightRegionManagementBE objFreight = new FreightRegionManagementBE();
                objFreight = FreightRegionManagementBL.GetShipmentServiceMethod(Convert.ToInt32(ddlSelectRegion.SelectedValue), Convert.ToInt32(ddlLanguage.SelectedValue));
                if (objFreight.FreightShipmentMethodMasterLst.Count > 0)
                {
                    //FreightRegionManagementBE objFreightRegionManagementBE = new FreightRegionManagementBE();
                    //objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();

                    //rptServices.DataSource = objFreightRegionManagementBE.FreightShipmentMethodMasterLst;
                    //rptServices.DataBind();

                    //rptServices.DataSource = objFreight.FreightShipmentMethodMasterLst;
                    //rptServices.DataBind();
                    FreightRegionManagementBE objFreightRegionManagementBE = new FreightRegionManagementBE();
                    objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();

                    rptServices.DataSource = objFreightRegionManagementBE.FreightShipmentMethodMasterLst;
                    rptServices.DataBind();
                    for (int i = 0; i < objFreight.FreightShipmentMethodMasterLst.Count; i++)
                    {
                        foreach (RepeaterItem item in rptServices.Items)
                        {
                            CheckBox chkService1 = (CheckBox)item.FindControl("chkService");
                            HiddenField hdfFreightModeId = (HiddenField)item.FindControl("hdfFreightModeId");
                            TextBox txtFreightModeName = (TextBox)item.FindControl("txtFreightModeName");
                            if (Convert.ToInt16(hdfFreightModeId.Value) == objFreight.FreightShipmentMethodMasterLst[i].FreightModeId)
                            {
                                chkService1.Checked = true;
                                txtFreightModeName.Text = objFreight.FreightShipmentMethodMasterLst[i].FreightModeName;
                            }

                        }
                    }
                }
                else
                {
                    FreightRegionManagementBE objFreightRegionManagementBE = new FreightRegionManagementBE();
                    objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();

                    rptServices.DataSource = objFreightRegionManagementBE.FreightShipmentMethodMasterLst;
                    rptServices.DataBind();
                }
            }
            else
            {
                txtRegionName.Text = string.Empty;
            }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSelectRegion.SelectedIndex > 0)
            {
                txtRegionName.Text = ddlSelectRegion.SelectedItem.Text;
                FreightRegionManagementBE objFreight = new FreightRegionManagementBE();
                objFreight = FreightRegionManagementBL.GetShipmentServiceMethod(Convert.ToInt32(ddlSelectRegion.SelectedValue), Convert.ToInt32(ddlLanguage.SelectedValue));
                if (objFreight.FreightShipmentMethodMasterLst.Count > 0)
                {
                    FreightRegionManagementBE objFreightRegionManagementBE = new FreightRegionManagementBE();
                    objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();

                    rptServices.DataSource = objFreightRegionManagementBE.FreightShipmentMethodMasterLst;
                    rptServices.DataBind();
                    for (int i = 0; i < objFreight.FreightShipmentMethodMasterLst.Count; i++)
                    {

                        foreach (RepeaterItem item in rptServices.Items)
                        {
                            CheckBox chkService = (CheckBox)item.FindControl("chkService");
                            HiddenField hdfFreightModeId = (HiddenField)item.FindControl("hdfFreightModeId");
                            TextBox txtFreightModeName = (TextBox)item.FindControl("txtFreightModeName");
                            if (Convert.ToInt16(hdfFreightModeId.Value) == objFreight.FreightShipmentMethodMasterLst[i].FreightModeId)
                            {
                                chkService.Checked = true;
                                txtFreightModeName.Text = objFreight.FreightShipmentMethodMasterLst[i].FreightModeName;
                            }
                        }
                    }
                }
                else
                {
                    FreightRegionManagementBE objFreightRegionManagementBE = new FreightRegionManagementBE();
                    objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();

                    rptServices.DataSource = objFreightRegionManagementBE.FreightShipmentMethodMasterLst;
                    rptServices.DataBind();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowWarning('Please Select Region..!!');", true);

            }
        }


        protected void btnSaveRegion_Click(object sender, EventArgs e)
        {
            int returnRegionID = 0;

            FreightRegionManagementBE.FreightRegionsBE objFreightRegion = new FreightRegionManagementBE.FreightRegionsBE();

            objFreightRegion.FreightRegionName = Convert.ToString(txtAddRegionName.Text.Trim());
            int MaxRegID = FreightRegionManagementBL.InsertFreightRegion(objFreightRegion, ref returnRegionID);
            if (MaxRegID > 0)
            {
                BindRegionName();
                //ddlSelectRegion.SelectedItem.Text = txtAddRegionName.Text;
                ddlSelectRegion.SelectedValue =Convert.ToString(MaxRegID);
                txtRegionName.Text = txtAddRegionName.Text;
                CreateActivityLog("ManageRegion", "Insert", txtAddRegionName.Text);
                FreightRegionManagementBE objFreightRegionManagementBE = new FreightRegionManagementBE();
                objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();
                rptServices.DataSource = objFreightRegionManagementBE.FreightShipmentMethodMasterLst;
                rptServices.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowWarning('Region Is Already Exist..!!');", true);

                //Already Exist Region Name in DB- MessageBox
            }
        }

        protected void btnFreightSave_Click(object sender, EventArgs e)
        {
            int MaxId = 0;
            int count = 0;
            int MaxRegId = 0;
            FreightRegionManagementBE objBE = new FreightRegionManagementBE();
            if (ddlSelectRegion.SelectedIndex == 0)
            {
                GlobalFunctions.ShowModalAlertMessages(this, "Please select the region first", AlertType.Warning);
            }
            else
            {
                foreach (RepeaterItem item in rptServices.Items)
                {
                    TextBox txtFreightModeName = (TextBox)item.FindControl("txtFreightModeName");
                    CheckBox chkService = (CheckBox)item.FindControl("chkService");
                    HiddenField hdfFreightModeId = (HiddenField)item.FindControl("hdfFreightModeId");

                    FreightRegionManagementBE.ShipmentMethodLanguagesBE objShipmentMethodLanguagesBE = new FreightRegionManagementBE.ShipmentMethodLanguagesBE();
                    FreightRegionManagementBE.RegionServiceShipmentBE objRegionServiceShipmentBE = new FreightRegionManagementBE.RegionServiceShipmentBE();


                    objShipmentMethodLanguagesBE.FreightModeName = txtFreightModeName.Text;
                    objShipmentMethodLanguagesBE.FreightModeId = Convert.ToInt16(hdfFreightModeId.Value);
                    objShipmentMethodLanguagesBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
                    objRegionServiceShipmentBE.RegionId = Convert.ToInt32(ddlSelectRegion.SelectedValue);
                    if (chkService.Checked)
                    {
                        objRegionServiceShipmentBE.IsActive = true;
                    }
                    else
                    {
                        objRegionServiceShipmentBE.IsActive = false;
                    }

                    objBE.FreightShipmentMethodLanguagesBELst.Add(objShipmentMethodLanguagesBE);
                    objBE.FreightRegionServiceShipmentBELst.Add(objRegionServiceShipmentBE);

                    MaxRegId = FreightRegionManagementBL.InserFreightModeLanguage(objBE, count, ref MaxId);
                    count++;
                    MaxRegId += MaxRegId;
                }
                if (MaxRegId > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowSuccess('Freight Services for Selected Region Saved Successfully.');", true);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try { Response.Redirect("~/Admin/Dashboard/Dashboard.aspx"); }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void BtnChangeRegionName_Click(object sender, EventArgs e)
        {
            if (ddlSelectRegion.SelectedIndex != 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ShowModalUpdate", "<script>javascript:ShowModalUpdate();</script>");

                txtUpdateRegionName.Text = txtRegionName.Text;
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this, "Please Select Region", AlertType.Warning);
            }
        }
        protected void BtnUpdateRegion_Click(object sender, EventArgs e)
        {
            bool CheckUpdateName;
            FreightRegionManagementBE.FreightRegionsBE ObjBE = new FreightRegionManagementBE.FreightRegionsBE();
            ObjBE.FreightRegionId = Convert.ToInt32(ddlSelectRegion.SelectedValue);
            ObjBE.FreightRegionName = txtUpdateRegionName.Text;
            ObjBE.OldRegionName = txtRegionName.Text.Trim();
            CheckUpdateName = FreightRegionManagementBL.UpdateFreightRegionName(ObjBE);
            if (CheckUpdateName == true)
            {
                GlobalFunctions.ShowModalAlertMessages(this, "Region Name Updated Successfully", AlertType.Success);
                CreateActivityLog("ManageRegion BtnUpdateRegion", "Update", txtAddRegionName.Text);
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this, "Can't Update Region Name", AlertType.Warning);

            }
            BindRegionName();
            txtRegionName.Text = ddlSelectRegion.SelectedValue;
            foreach (RepeaterItem item in rptServices.Items)
            {
                CheckBox chkService = (CheckBox)item.FindControl("chkService");
                chkService.Checked = false;
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            bool Check;
            FreightRegionManagementBE.FreightRegionsBE ObjBE = new FreightRegionManagementBE.FreightRegionsBE();
            ObjBE.FreightRegionId = Convert.ToInt32(ddlSelectRegion.SelectedValue);
            ObjBE.FreightRegionName = txtRegionName.Text;
            try
            {
                Check = FreightRegionManagementBL.DeleteFreightRegionName(ObjBE);
                BindRegionName();
                if (Check == true)
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Region Deleted Successfully", AlertType.Success);
                    txtRegionName.Text = "";
                    CreateActivityLog("ManageRegion BtnUpdateRegion", "Delete", ddlSelectRegion.SelectedValue);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Please Delete Services Against This Region Or This Region Is Assinged To Some Country", AlertType.Warning);
                    txtRegionName.Text = "";
                    foreach (RepeaterItem item in rptServices.Items)
                    {
                        CheckBox chkService = (CheckBox)item.FindControl("chkService");
                        chkService.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            BindRegionName();

        }
    }
}