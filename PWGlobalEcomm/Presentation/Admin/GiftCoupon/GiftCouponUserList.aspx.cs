﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_GiftCoupon_GiftCouponUserList : System.Web.UI.Page
{
    #region Controls
    protected global::System.Web.UI.WebControls.GridView gvUserList;
    protected global::System.Web.UI.WebControls.Literal ltrNoOfuser;
    #endregion
    #region variables
    public Int16 PageIndex;
    public Int16 PageSize = 50;
    int CouponId;
    string UserType;
    List<CouponBE> lstCBE;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
         CouponId = Convert.ToInt16(Request.QueryString["Id"]);
         //UserType = Request.QueryString["UserType"].ToString();
         gvUserList.PageSize = PageSize;
          if (!IsPostBack)
          {
            PageIndex = 0;
            BindGrid(CouponId,UserType,PageIndex,PageSize);
          }
    }
    private void BindGrid(int CouponId,string UserType,Int16 PageIndex, Int16 PageSize)
    {
        try
        {
            //List<CouponBE> lstCBE;
            CouponBE objCouponBE = new CouponBE();
            objCouponBE.CouponID = CouponId;
           // objCouponBE.UserType = UserType;
            lstCBE = CouponBL.GetUserListCouponMaster(Constants.USP_GetUserCouponList,CouponId);
            Session["lstCBE"] = lstCBE;
            if (lstCBE != null)
            {
                gvUserList.DataSource = lstCBE;
                gvUserList.VirtualItemCount = lstCBE[0].TotalRow;
                ltrNoOfuser.Text = lstCBE[0].TotalRow.ToString();
                gvUserList.DataBind();
            }
            else
            {
                gvUserList.DataSource = null;
                gvUserList.DataBind();
                ltrNoOfuser.Text = "0";
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    protected void gvUserList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUserList.PageIndex = e.NewPageIndex;
        PageIndex = Convert.ToInt16(e.NewPageIndex);
        BindGrid(CouponId,UserType, PageIndex, PageSize);
    }
    protected void btnExportTocsv_Click(object sender, EventArgs e)
    {
        List<CouponBE> lstCBE = Session["lstCBE"] as List<CouponBE>;
        string filename = "Promo Code User List" + ".csv";
        string csvPath = Server.MapPath("~/CSV/Promo Code/") + filename;
        if (File.Exists(csvPath))
        {
            File.Delete(csvPath);
        }
       // File.Create(csvPath);
        var csv = new StringBuilder();
        foreach (CouponBE items in lstCBE)
        {
            csv.Append(items.User + Environment.NewLine);
        }
      

        string attachment = "attachment; filename=" + filename + "";
        HttpContext.Current.Response.Clear();
        Response.AddHeader("Content-Disposition", "attachment;filename=PromoCodeUser.csv");
        Response.Output.Write(csv);
        HttpContext.Current.Response.ContentType = "Excel/csv";
        Session["lstCBE"] = null;
        Response.End();
    }

   

}