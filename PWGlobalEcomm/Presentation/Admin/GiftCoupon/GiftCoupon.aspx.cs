﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace Presentation
{
    public partial class Admin_GiftCoupon : BasePage //System.Web.UI.Page
    {
        #region controls
        protected global::System.Web.UI.WebControls.GridView gvList;
        protected global::System.Web.UI.WebControls.TextBox txtCouponName;
        protected global::System.Web.UI.WebControls.TextBox txtDiscountValue;
        //protected global::System.Web.UI.WebControls.TextBox txtMinOrder;
        //protected global::System.Web.UI.WebControls.TextBox txtMaxOrder;
        protected global::System.Web.UI.WebControls.TextBox txtStartDate;
        protected global::System.Web.UI.WebControls.TextBox txtEndDate;
        protected global::System.Web.UI.WebControls.DropDownList ddlDicount;
        protected global::System.Web.UI.WebControls.RadioButtonList rdStatus;
        protected global::System.Web.UI.WebControls.RadioButtonList rdListNoOfUses;
        protected global::System.Web.UI.WebControls.RadioButtonList rdListShippmentType;
        protected global::System.Web.UI.WebControls.CheckBoxList chkRegion;
        protected global::System.Web.UI.WebControls.HiddenField hdfCouponID;
        protected global::System.Web.UI.WebControls.FileUpload UPlWhiteList;
        protected global::System.Web.UI.WebControls.RadioButtonList RdWhiteList;
        protected global::System.Web.UI.WebControls.TextBox txtSearch;
        protected global::System.Web.UI.WebControls.Literal ltrNoOfCoupon;
        protected global::System.Web.UI.WebControls.LinkButton lnkDownload;
        protected global::System.Web.UI.WebControls.Button btnSearch;
        protected global::System.Web.UI.WebControls.HiddenField hdntotalPage;
        //Added By Hardik "27/Sep/2016"
        protected global::System.Web.UI.WebControls.Repeater rptMinMaxOrderValue, rptMaximumOrderValue;
        public List<CouponBE.CouponCurrencyBE> lstMinMaxOrderValue;
        //public List<CouponBE.CouponCurrencyBE> lstMaximumOrderValue;
        #endregion
        #region variable
        public Int16 PageIndex;
        public Int16 PageSize = 50;

        string UnMatchedRecordPath = "";
        #endregion
        List<CouponBE> lstCBE = new List<CouponBE>();

        CouponBE objCouponBE = new CouponBE();

        protected void Page_Load(object sender, EventArgs e)
        {
            gvList.PageSize = PageSize;
            if (!IsPostBack)
            {
                PageIndex = 0;
                BindCoupon(PageIndex, PageSize, "");
                BindCurrency();
                BindddlDiscountMaster();
                BindCheckboxField();
                Session["lstCouponWhitelistExport"] = null;
            }
            //lnkDownload.Visible = false;
            btnSearch.Attributes.Add("onmousedown", "ValidateTextbox('" + txtSearch.ClientID + "');");
            // btnSearch.Attributes.Add("onchange", "ValidateTextbox('" + txtSearch.ClientID + "');");
        }

        protected void ddlDicount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDicount.SelectedItem.Text.ToString().Contains("Free shipping excluding Freight Value"))
            {
                txtDiscountValue.Text = "100";
                txtDiscountValue.Enabled = false;
            }
            else
            {
                txtDiscountValue.Enabled = true;
            }
        }

        #region validation

        protected bool Validation(string mode, int Couponid)
        {
            bool Isvalidated = true;
            bool isvalidordervalue = true;
            DateTime dtEnd = DateTime.Parse(txtEndDate.Text);
            DateTime dtStart = DateTime.Parse(txtStartDate.Text);
            Double MinOrdervalue = 0;
            Double MaxOrdervalue = 0;

            if  (IsFileValidated(mode, Couponid))
            {
                if (!GlobalFunctions.IsSpecialCharactersCoupon(txtCouponName.Text))
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Only AlpaNumeric are allowed in the Coupon Code.", AlertType.Warning);
                    txtCouponName.Focus();
                    Isvalidated = false;
                }

                if (chkRegion.SelectedIndex == -1)
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/CouponRegionMandatory"), AlertType.Warning);
                    Isvalidated = false;
                }
                if (rdListShippmentType.SelectedIndex == -1)
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/CouponShipmentMandatory"), AlertType.Warning);
                    Isvalidated = false;
                }
                if (dtEnd < dtStart)
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/CompareDate"), AlertType.Warning);
                    Isvalidated = false;
                }



                foreach (RepeaterItem item in rptMinMaxOrderValue.Items)
                {
                    TextBox txtMinOrder = (TextBox)item.FindControl("txtMinOrder");
                    TextBox txtMaxOrder = (TextBox)item.FindControl("txtMaxOrder");
                    MinOrdervalue = Convert.ToDouble(txtMinOrder.Text);

                    if (txtMinOrder.Text != "" && (int.Parse(txtMinOrder.Text) >= 0))
                    {
                        if (txtMaxOrder.Text != "")
                        {
                            MaxOrdervalue = Convert.ToDouble(txtMaxOrder.Text);

                            if (Convert.ToDouble(txtMaxOrder.Text) >= Convert.ToDouble(txtMinOrder.Text))
                            { isvalidordervalue = true; }
                            else
                            {
                                isvalidordervalue = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        isvalidordervalue = false;
                        GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/MinimumOrderCoupon"), AlertType.Warning);
                    }
                }
                if (isvalidordervalue && Isvalidated)
                {
                    Isvalidated = true;
                }
                else
                {
                    Isvalidated = false;
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/MinimumOrderCoupon"), AlertType.Warning);
                }
            }

            else
            {
                Isvalidated = false;
            }
            return Isvalidated;
        }

        protected bool ValidateFile()
        {
            bool Isvalidated = false;
            if (System.IO.Path.GetExtension(UPlWhiteList.PostedFile.FileName) == ".csv")
            {
                if (!Directory.Exists(Server.MapPath("~/CSV/Promo Code/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/CSV/Promo Code/"));
                }
                string Filename = Path.GetFileName(UPlWhiteList.PostedFile.FileName);
                Filename = Filename.Replace(".csv", "");
                string csvPath = Server.MapPath("~/CSV/Promo Code/") + Filename + ".csv";
                UPlWhiteList.SaveAs(csvPath);
                string csvData = File.ReadAllText(csvPath);
                if (RdWhiteList.SelectedValue == "1")
                {
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    List<string> emaillist = new List<string>();
                    foreach (string row in csvData.Split('\n'))
                    {
                        if (!string.IsNullOrEmpty(row))
                        {
                            foreach (string cell in row.Split(','))
                            {
                                string data = cell.Replace("\r", "");
                                if (data.Substring(0, 7).ToString() == "EmailId")
                                {
                                    Isvalidated = true;
                                }
                                else
                                {
                                    Match match = regex.Match(data);
                                    if (match.Success)
                                    {
                                        Isvalidated = true;
                                    }
                                    else
                                    {
                                        emaillist.Add(data);
                                        Isvalidated = false;
                                        goto EndFunction;
                                    }
                                }
                            }
                        }
                    EndFunction: if (!Isvalidated) break;
                    }
                }
                else if (RdWhiteList.SelectedValue == "2")
                {
                    foreach (string row in csvData.Split('\n'))
                    {
                        if (!string.IsNullOrEmpty(row))
                        {
                            Regex regex = new Regex("^(?:[-A-Za-z0-9]+\\.)+[A-Za-z]{2,6}$");
                            foreach (string cell in row.Split(','))
                            {
                                string data = cell.Replace("\r", "");
                                if (data == "DomainId")
                                {
                                    Isvalidated = true;
                                }
                                else
                                {
                                    Match match = regex.Match(data);
                                    if (match.Success)
                                    {
                                        Isvalidated = true;
                                    }
                                    else
                                    {
                                        Isvalidated = false;
                                        goto EndFunction;
                                    }
                                }
                            }
                        }
                    EndFunction: if (!Isvalidated) break;
                    }
                }
            }
            else if(RdWhiteList.SelectedValue=="3")
            {
                Isvalidated = true;
            }
            return Isvalidated;
        }

        protected bool IsFileValidated(string mode, int couponid)
        {
            bool Isvalidated = false;
            try
            {
                string userType=string.Empty;
                if(RdWhiteList.SelectedValue=="1")
                {
                    userType = "email";
                }
                else if(RdWhiteList.SelectedValue=="2")
                {
                    userType = "domain";
                }
                List<CouponBE> lstWhiteList = CouponBL.GetUserListCouponMaster(Constants.USP_GetUserCouponList, couponid,userType);
                if ((mode == "edit") && (UPlWhiteList.HasFile))
                {
                    Isvalidated = checkForFileValidation();
                }
                else if ((mode == "edit") && (!UPlWhiteList.HasFile) && (lstWhiteList == null))
                {
                    Isvalidated = checkForFileValidation();
                }
                else if ((mode == "add"))
                {
                    Isvalidated = checkForFileValidation();
                }
                else
                {
                    Isvalidated = true;
                }


            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Isvalidated;
        }

        #endregion

        #region binding



        //private int BindCoupon(Int16 PageIndex, Int16 PageSize)
        //{
        //    int TotalRow = 0;
        //    try
        //    {
        //        CouponBL objCouponBL = new CouponBL();
        //        CouponBE objCouponBE = new CouponBE();
        //       // lstProductBE = ProductBL.GetProductList<ProductBE>(LanguageId, PageIndex, PageSize, searchText);
        //        lstCBE = CouponBL.GetListCouponMaster(Constants.USP_GetListCouponMaster, gvList.PageSize, "", gvList.PageIndex);
        //        if (lstCBE != null)
        //        {
        //            if (lstCBE.Count > 0)
        //            {
        //                gvList.DataSource = lstCBE;
        //                gvList.DataBind();

        //                TotalRow = Convert.ToInt32(lstCBE[0].TotalRow);
        //                hdntotalPage.Value = lstCBE[0].PageCount.ToString();

        //            }
        //            else
        //            {
        //                gvList.DataSource = null;
        //                gvList.DataBind();
        //                ltrNoOfCoupon.Text = "0";
        //            }
        //        }
        //    }
        //    catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        //    return TotalRow;
        //}

        private int BindCoupon(Int16 PageIndex, Int16 PageSize, string searchText)
        {
            int TotalRow = 0;
            try
            {
                CouponBL objCouponBL = new CouponBL();
                CouponBE objCouponBE = new CouponBE();
                lstCBE = CouponBL.GetListCouponMaster(Constants.USP_GetListCouponMaster, PageSize, searchText, PageIndex);
                if (lstCBE != null)
                {
                    if (lstCBE.Count > 0)
                    {
                        gvList.DataSource = lstCBE;
                        ltrNoOfCoupon.Text = lstCBE[0].TotalRow.ToString();
                        gvList.VirtualItemCount = lstCBE[0].TotalRow;
                        gvList.DataBind();

                    }
                    else
                    {
                        gvList.DataSource = null;
                        gvList.DataBind();
                        ltrNoOfCoupon.Text = "0";
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return TotalRow;
        }

        private void BindddlDiscountMaster()
        {
            try
            {
                CouponBL objCouponBL = new CouponBL();
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                //dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                lstCBE = CouponBL.GetDiscountBehaviour(Constants.USP_GetListDiscountMaster);
                ddlDicount.DataSource = lstCBE;
                ddlDicount.DataTextField = "BehaviourDescription";
                ddlDicount.DataValueField = "BehaviourId";
                ddlDicount.DataBind();
                ddlDicount.Items.Insert(0, new ListItem("Please Select", "0"));
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #endregion

        private void BindCheckboxField()
        {
            List<FreightManagementBE> ObjFreightManagementBE = new List<FreightManagementBE>();
            ObjFreightManagementBE = FreightManagementBL.GetAllFreights();
            chkRegion.DataSource = ObjFreightManagementBE;
            chkRegion.DataTextField = "FreightRegionName";
            chkRegion.DataValueField = "FreightRegionId";
            chkRegion.DataBind();
        }

        protected bool checkForFileValidation()
        {
            bool Isvalidated = false;
            try
            {

                if (!UPlWhiteList.HasFile && Convert.ToInt16(RdWhiteList.SelectedValue) == 0)
                {
                    Isvalidated = true;
                }
                else if (UPlWhiteList.HasFile && !(System.IO.Path.GetExtension(UPlWhiteList.PostedFile.FileName) == ".csv") && Convert.ToInt16(RdWhiteList.SelectedValue) > 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/InvalidUserFileCoupon"), AlertType.Warning);
                }
                else if (UPlWhiteList.HasFile && Convert.ToInt16(RdWhiteList.SelectedValue) == 0 && (System.IO.Path.GetExtension(UPlWhiteList.PostedFile.FileName) == ".csv"))
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/InvalidUserTypeCoupon"), AlertType.Warning);
                }
                else if (!UPlWhiteList.HasFile && Convert.ToInt16(RdWhiteList.SelectedValue) > 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/InvalidUserFileCoupon"), AlertType.Warning);
                }
                else
                {
                    Isvalidated = ValidateFile();
                    if (Isvalidated)
                    {
                        Isvalidated = true;
                    }
                    else
                    {
                        Isvalidated = false;
                        GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/InvalidData"), AlertType.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
            return Isvalidated;
        }
        #region Edit and Delete
        protected void gvList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                CouponBE objCouponMasterBO = new CouponBE();
                objCouponMasterBO.CouponID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.ToLower() == "edits")
                {
                    #region
                    hdfCouponID.Value = Convert.ToString(e.CommandArgument);

                    objCouponBE = CouponBL.GetCouponMasterByCatIDCouponCode(Constants.USP_GetListCouponMasterByCouponID, objCouponMasterBO);

                    if (objCouponBE != null)
                    {
                        Session["lstCouponWhitelistExport"] = objCouponBE;
                        #region set Readonly if order is placed by this coupon
                        if (objCouponBE.IsReadOnly)
                        {
                            txtCouponName.Enabled = false;
                            txtDiscountValue.Enabled = false;
                        }
                        else
                        {
                            txtCouponName.Enabled = true;
                            txtDiscountValue.Enabled = true;
                        }
                        #endregion
                        txtCouponName.Text = objCouponBE.CouponCode;
                        txtDiscountValue.Text = objCouponBE.DiscountPercentage; //Convert.ToString(objCouponBE.DiscountValue.Replace("%", ""));
                        txtEndDate.Text = objCouponBE.EndDate.ToLongDateString();
                        //txtMinOrder.Text = objCouponBE.MinimumOrderValue.ToString();
                        //txtMaxOrder.Text = objCouponBE.MaximumOrderValue.ToString();
                        lstMinMaxOrderValue = CouponBL.GetAllMinMaxValueCoupon<CouponBE.CouponCurrencyBE>(Convert.ToInt16(hdfCouponID.Value));
                        BindCurrency();

                        //Hardik

                        //lstMaximumOrderValue = CouponBL.GetAllMinMaxValueCoupon<CouponBE.CouponCurrencyBE>(Convert.ToInt16(hdfCouponID.Value));
                        //if (objCouponBE.MaximumOrderValue.ToString() == "0")
                        //    txtMaxOrder.Text = "";
                        //else
                        //    txtMaxOrder.Text = objCouponBE.MaximumOrderValue.ToString();
                        txtStartDate.Text = objCouponBE.StartDate.ToLongDateString();
                        if (Convert.ToString(objCouponBE.Usage) == "0")
                        {
                            rdListNoOfUses.SelectedIndex = 1;
                        }
                        else
                        {
                            rdListNoOfUses.SelectedIndex = 0;
                        }
                        ddlDicount.SelectedValue = objCouponBE.BehaviourId.ToString();
                        if (objCouponBE.UserType == "email")
                        {
                            RdWhiteList.SelectedValue = "1";
                            lnkDownload.Text = "Export Email WhiteList";
                            lnkDownload.Visible = true;
                        }
                        else if (objCouponBE.UserType == "domain")
                        {
                            RdWhiteList.SelectedValue = "2";
                            lnkDownload.Text = "Export Domain WhiteList";
                            lnkDownload.Visible = true;
                        }
                        else
                        {
                            RdWhiteList.SelectedValue = "0";
                            lnkDownload.Text = "Export Sample";
                            lnkDownload.Visible = true;
                        }

                        string strRegion = Convert.ToString(objCouponBE.Region);
                        string[] RegionsStr = strRegion.Split(',');
                        if (RegionsStr.Length > 0)
                        {
                            for (int i = 0; i <= RegionsStr.Length - 1; i++)
                            {
                                foreach (ListItem liRegion in chkRegion.Items)
                                {
                                    if (liRegion.Text == RegionsStr[i])
                                    {
                                        liRegion.Selected = true;
                                    }
                                }
                            }
                        }
                        //if (strRegion.Contains("All"))
                        //{
                        //    chkRegion.SelectedValue = "All";
                        //}
                        //else
                        //{
                        //    chkRegion.Items[3].Selected = false;
                        //    if (strRegion.Contains("UK"))
                        //    {
                        //        chkRegion.Items[0].Selected = true;
                        //    }
                        //    if (strRegion.Contains("EU"))
                        //    {
                        //        chkRegion.Items[1].Selected = true;
                        //    }
                        //    if (strRegion.Contains("Other"))
                        //    {
                        //        chkRegion.Items[2].Selected = true;
                        //    }
                        //}
                        Int16 status = 0;
                        if (objCouponBE.IsActive == false)
                        {
                            rdStatus.SelectedValue = status.ToString();
                        }
                        else
                        {
                            status = 1;
                            rdStatus.SelectedValue = status.ToString();
                        }

                        rdListShippmentType.SelectedValue = Convert.ToString(objCouponBE.ShipmentType);
                    }

                    BindCoupon(PageIndex, PageSize, "");
                    #endregion
                }
                else if (e.CommandName.ToLower() == "deletes")
                {
                    #region
                    try
                    {
                        bool bres = CouponBL.DeleteCouponCode(Constants.USP_DeleteCouponbyCouponID, objCouponMasterBO);
                        if (bres)
                        {
                            CreateActivityLog("Gift Coupon", "Deleted", txtCouponName.Text);
                            GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/DeletedMessage"), AlertType.Success);
                        }
                        if (bres)
                        {
                            CreateActivityLog("Gift Coupon", "Deleted", txtCouponName.Text);
                            GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("SuccessMessage/DeletedMessage"), AlertType.Success);
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, "This Coupon Can not be deleted due to order placed by this coupon", AlertType.Failure);
                            //ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Please Not Deleted');</script>");
                        }

                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                    ClearInput();
                    //BindCoupon();
                    #endregion
                    BindCoupon(PageIndex, PageSize, "");
                }
                else if (e.CommandName.ToLower() == "viewuser")
                {
                    #region
                    try
                    {
                        Response.Redirect("~/Admin/GiftCoupon/GiftCouponUserList.aspx?Id=" + e.CommandArgument + "");
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                    #endregion
                }
                else if (e.CommandName.ToLower() == "viewdomain")
                {
                    #region
                    try
                    {
                        Response.Redirect("~/Admin/GiftCoupon/GiftCouponUserList.aspx?Id=" + e.CommandArgument + "");
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #endregion

        #region Search

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //Search();

                try
                {
                    if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                    {
                        BindCoupon(PageIndex, PageSize, txtSearch.Text.Trim());
                        hdfCouponID.Value = txtSearch.Text.Trim();
                    }
                    else
                    {
                        BindCoupon(PageIndex, PageSize, "");
                        hdfCouponID.Value = "";
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
        }

        protected void Search()
        {
            try
            {
                CouponBL objCouponBL = new CouponBL();
                string searchdata = txtSearch.Text;
                lstCBE = CouponBL.GetListCouponMaster(Constants.USP_GetListCouponMaster, gvList.PageSize, searchdata, gvList.PageIndex);
                if (lstCBE != null)
                {
                    if (lstCBE.Count > 0)
                    {
                        gvList.DataSource = lstCBE;
                        gvList.DataBind();
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #endregion

        #region Pagination

        /// <summary>
        /// //
        /// 
        /// PageIndex = 0 ====>>>> Current Page =1
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvList.PageIndex = e.NewPageIndex;
            PageIndex = Convert.ToInt16(e.NewPageIndex);
            if (txtSearch.Text.ToString() == "")
            {
                BindCoupon(PageIndex, PageSize, txtSearch.Text);
            }
            else
            {
                BindCoupon(PageIndex, PageSize, "");
            }
        }
        #endregion


        #region save and update



        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            UserBE lst = Session["StoreUser"] as UserBE;
            if (Page.IsValid)
            {
                try
                {
                    string mode = "";
                    int couponid = 0;
                    if (!string.IsNullOrEmpty(hdfCouponID.Value))
                    {
                        couponid = Convert.ToInt16(hdfCouponID.Value);
                        mode = "edit";
                    }
                    else
                    {
                        couponid = 0;
                        mode = "add";
                    }
                    bool Isvalidated = Validation(mode, couponid);
                    if (!Isvalidated)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "File Data Is Not Valid", AlertType.Warning);
                    }
                    else
                    {
                        CouponBE objCouponMasterBO = new CouponBE();
                        int bres = 0;
                        #region Object Storage
                        //string strShipment = "";

                        string strRegion = "";

                        if (!string.IsNullOrEmpty(hdfCouponID.Value))
                        {
                            objCouponMasterBO.CouponID = Convert.ToInt32(hdfCouponID.Value);
                            //objCouponMasterBO.ModifiedBy = Convert.ToInt16(lst.UserId);
                            objCouponMasterBO.ModifiedBy = 1;
                            //mode = "edit";
                        }
                        else
                        {
                            objCouponMasterBO.CouponID = 0;
                            objCouponMasterBO.CreatedBy = 1;//Convert.ToInt16(lst.UserId);
                            // mode = "add";
                        }
                        objCouponMasterBO.CouponCode = Microsoft.Security.Application.Encoder.HtmlEncode(txtCouponName.Text);
                        objCouponMasterBO.BehaviourType = "";
                        objCouponMasterBO.BehaviourId = Convert.ToInt32(ddlDicount.SelectedValue);
                        objCouponMasterBO.DiscountPercentage = txtDiscountValue.Text;
                        //objCouponMasterBO.MinimumOrderValue = float.Parse(txtMinOrder.Text);
                        //if (txtMaxOrder.Text != "")
                        //{
                        //    objCouponMasterBO.MaximumOrderValue = float.Parse(txtMaxOrder.Text);
                        //}
                        //else
                        //{
                        //    objCouponMasterBO.MaximumOrderValue = 0;
                        //}
                        objCouponMasterBO.Usage = rdListNoOfUses.SelectedValue;

                        objCouponMasterBO.EndDate = Convert.ToDateTime(txtEndDate.Text);
                        objCouponMasterBO.StartDate = Convert.ToDateTime(txtStartDate.Text); ;

                        bool status = false;
                        if (rdStatus.SelectedValue == "0")
                        {
                            status = false;
                        }
                        else
                        {
                            status = true;
                        }
                        objCouponMasterBO.IsActive = status;
                        foreach (ListItem liRegion in chkRegion.Items)
                        {
                            
                            if (liRegion.Selected)
                            {
                                strRegion += liRegion.Text + ",";
                            }
                        }
                        if (strRegion.Contains("UK,EU,Other")) { strRegion = "All"; }
                        if (strRegion.Contains("All")) { strRegion = "All"; }
                        objCouponMasterBO.Region = strRegion;

                        objCouponMasterBO.ShipmentType = rdListShippmentType.SelectedValue;

                        string UserType = "";
                        if (RdWhiteList.SelectedValue == "1")
                            UserType = "email";
                        else if (RdWhiteList.SelectedValue == "2")
                            UserType = "domain";
                        else
                            UserType = "";
                        objCouponMasterBO.UserType = UserType;
                        #endregion
                        bool ischangeUserlist = false;
                        if ((mode == "edit") && (UPlWhiteList.HasFile))
                        {
                            ischangeUserlist = true;
                        }
                        bres = CouponBL.InsertCouponMaster(Constants.USP_InsertUpdateCouponMaster, objCouponMasterBO, mode, ischangeUserlist);

                        if (bres > 0 && bres != 3)
                        {
                            bool res = false;
                            SaveCouponCurrencyDetails(bres);
                            if ((mode == "edit") && (UPlWhiteList.HasFile))
                            {
                                res = UploadFiles(bres);
                            }
                            else if ((mode == "add"))
                            {
                                res = UploadFiles(bres);
                            }
                            else
                            {
                                res = true;
                            }
                            #region
                            if (res)
                            {

                                if (!string.IsNullOrEmpty(hdfCouponID.Value))
                                {
                                    hdfCouponID.Value = "";
                                    lnkDownload.Visible = false;
                                    CreateActivityLog("Gift Coupon", "Updated", txtCouponName.Text);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/CouponUpdated "), AlertType.Success);
                                }
                                else
                                {
                                    lnkDownload.Visible = false;
                                    CreateActivityLog("Gift Coupon", "Added", txtCouponName.Text);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/CouponSaved"), AlertType.Success);
                                }

                            }
                            ClearInput();
                            //BindCoupon();
                            BindCoupon(PageIndex, PageSize, "");
                            #endregion
                        }
                        else if (bres == 3)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Coupon with the same name already exists.", AlertType.Warning);
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/ExistCoupon"), AlertType.Failure);
                        }
                    }
                }
                catch (Exception) { }
            }
        }

        protected bool UploadFiles(int CouponId)
        {
            bool res = false;
            try
            {

                List<string> UnMatchedList = new List<string>();
                if (RdWhiteList.SelectedValue != "0")
                {
                    CouponBL objCouponBL = new CouponBL();

                    if (UPlWhiteList.HasFile)
                    {
                        if (System.IO.Path.GetExtension(UPlWhiteList.PostedFile.FileName) == ".csv")
                        {
                            #region delete Existsing Records for Coupon before bulk insert
                            CouponBE objCouponMasterBO = new CouponBE();
                            objCouponMasterBO.CouponID = Convert.ToInt32(CouponId);
                            bool bres;
                            if (RdWhiteList.SelectedValue == "1")
                            {
                                // email
                                bres = CouponBL.DeleteCouponCode(Constants.USP_DeleteAllowedUser, objCouponMasterBO);
                            }
                            else if (RdWhiteList.SelectedValue == "2")
                            {
                                bres = CouponBL.DeleteCouponCode(Constants.USP_DeleteAllowedDomain, objCouponMasterBO);
                            }
                            else { }
                            #endregion
                            #region Upload and save the file

                            //string Filename = Path.GetFileName(UPlWhiteList.PostedFile.FileName);
                            //Filename = Filename.Replace(".csv", "");
                            //string csvPath = Server.MapPath("~/CSV/Promo Code/") + Filename + ".csv";
                            string strFileName = UPlWhiteList.FileName;
                            FileInfo fi = new FileInfo(strFileName);
                            string strFilExt = fi.Extension;
                            strFileName = "Whitelist Coupon" + strFilExt;
                            string csvPath = GlobalFunctions.GetPhysicalFolderPath() + "\\CSV\\Registration\\" + strFileName;
                            if (File.Exists(csvPath))
                            {
                                File.Delete(csvPath);
                            }
                            UPlWhiteList.SaveAs(csvPath);
                            DataTable dt = new DataTable();
                            if (RdWhiteList.SelectedIndex == 0)
                            {
                                dt.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),
                            new DataColumn("UserId", typeof(int)),
                            new DataColumn("CouponId",typeof(int)),
                            new DataColumn("EmailId",typeof(string)) 
                            });
                            }
                            else
                            {
                                dt.Columns.AddRange(new DataColumn[3] { new DataColumn("Id", typeof(int)),
                            new DataColumn("DomainId", typeof(int)),
                            new DataColumn("CouponId",typeof(int)),
                            });
                            }
                            string csvData = File.ReadAllText(csvPath);
                            csvData = csvData.Replace("EmailId","");
                            csvData = csvData.Replace("DomainId", "");
                            string list = string.Empty;

                            list = csvData.Replace("\r\n", ",");
                            list = list.Replace("'", "''");
                            list = list.Substring(0, list.Length - 1);
                            int count = list.Length;

                            List<string> mylist = new List<string>();

                            DataTable table = new DataTable();


                            if (RdWhiteList.SelectedIndex == 0)
                            {
                                table = CouponBL.GetIdByCSV(Constants.USP_GetUserIdfromEmail, list);
                                UnMatchedList = CouponBL.GetUnMatchedRecords(Constants.USP_GetUnMatcheduserIdfromEmail, list);
                            }
                            else
                            {
                                table = CouponBL.GetIdByCSV(Constants.USP_GetDominIdfromName, list);
                                //UnMatchedList = CouponBL.GetUnMatchedRecords(Constants.USP_GetUnMatcheduserIdfromDomain, list);
                            }

                            int i = 0;
                            #region only UnRegistered User adding
                            if (UnMatchedList.Count > 0)
                            {
                                for (int item = 0; item < UnMatchedList.Count; item++)
                                {
                                    dt.Rows.Add();
                                    dt.Rows[i]["Id"] = i;
                                    dt.Rows[i]["CouponId"] = CouponId;
                                    if (RdWhiteList.SelectedValue == "1")
                                    {
                                        dt.Rows[i]["UserId"] = 0;
                                        dt.Rows[i]["EmailId"] = UnMatchedList[item];
                                    }
                                    i++;
                                }
                            }
                            #endregion

                            foreach (DataRow dr in table.Rows)
                            {
                                //dt.NewRow();
                                dt.Rows.Add();
                                dt.Rows[i]["Id"] = i;
                                dt.Rows[i]["CouponId"] = CouponId;
                                if (RdWhiteList.SelectedValue == "1")
                                {
                                    dt.Rows[i]["UserId"] = dr["UserId"];
                                    dt.Rows[i]["EmailId"] = dr["EmailId"];
                                }
                                if (RdWhiteList.SelectedValue == "2")
                                {
                                    dt.Rows[i]["DomainId"] = dr["Id"];
                                }
                                i++;
                            }

                            string TableName = "";
                            if (RdWhiteList.SelectedValue == "1")
                            {
                                TableName = "Tbl_UserCouponMapping";
                                //dt.Columns.Remove("EmailId");
                            }
                            if (RdWhiteList.SelectedValue == "2")
                            {
                                TableName = "Tbl_DomainCouponMapping";
                            }

                            #endregion
                            
                            res = objCouponBL.BulkInsertWhiteList(dt, TableName);
                            #region Now it is also for Guest User therefore UnMatchedList Disabled
                            //if (UnMatchedList.Count > 0)
                            //{
                            //  UnMatchedRecordPath = ShowUnMachedRecordsNotUploaded(UnMatchedList);
                            //    CreateActivityLog("Gift Coupon", "Uploaded", "UnMatchedRecords.csv");
                            //  lnkDownload.Text = "Some of the records did not found click here to view those records.";
                            //    lnkDownload.Visible = true;
                            //}
                            #endregion
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, Exceptions.GetException("FailureMessage/InvalidUserFileCoupon"), AlertType.Failure);
                            res = false;
                        }
                    }
                }
                else if (RdWhiteList.SelectedValue.ToString() == "0" || !UPlWhiteList.HasFile)
                {
                    res = true;
                }
                else
                {
                    res = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return res;
        }

        protected string ShowUnMachedRecordsNotUploaded(List<string> records)
        {
            string csvPath = "";
            if (records.Count > 0)
            {
                try
                {
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ajax", "<script language='javascript'>confirm('if(confirm('Some of Records did not Found. Click OK See Those Records?')) { document.getElementById('btn').click(); }');</script>", false);
                    csvPath = Server.MapPath("~/CSV/Promo Code/") + "UnMatchedRecords" + ".csv";
                    if (File.Exists(csvPath))
                    {
                        File.Delete(csvPath);
                    }
                    else
                    {
                        UPlWhiteList.SaveAs(csvPath);
                    }
                    //in your loop
                    var csv = new StringBuilder();
                    foreach (string items in records)
                    {
                        csv.Append(items + Environment.NewLine);
                    }
                    //after your loop
                    File.WriteAllText(csvPath, csv.ToString());
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
                return csvPath;
            }
            else
            {
                return "";
            }

        }

        #endregion

        #region Clear

        private void ClearInput()
        {
            txtCouponName.Text = "";
            txtCouponName.Enabled = true;
            txtDiscountValue.Text = "";
            txtEndDate.Text = "";
            //txtMinOrder.Text = "";
            txtStartDate.Text = "";
            txtDiscountValue.Enabled = true; //Added By Hardik to enable txt box after page load(after 100% disc)
            foreach (RepeaterItem item in rptMinMaxOrderValue.Items)
            {
                TextBox txtMinOrder = (TextBox)item.FindControl("txtMinOrder");
                TextBox txtMaxOrder = (TextBox)item.FindControl("txtMaxOrder");

                txtMaxOrder.Enabled = true;
                txtMinOrder.Enabled = true;
                txtMinOrder.Text = "0";
                txtMaxOrder.Text = "0";
            }

            rdListNoOfUses.SelectedIndex = 0;
            ddlDicount.SelectedIndex = 0;

            rdListShippmentType.SelectedValue = "0";

            chkRegion.ClearSelection();
            RdWhiteList.SelectedIndex = -1;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            try
            {
                ClearInput();
                hdfCouponID.Value = "";

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #endregion

        #region Donwload

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                //  Response.Redirect("~/Admin/GiftCoupon/GiftCouponUserList.aspx?Id=" + hdfCouponID.Value + "");

                //string host = GlobalFunctions.GetVirtualPathAdmin();
                //string Url = host + "/GiftCoupon/GiftCouponUserList.aspx?Id=" + hdfCouponID.Value + "";

                //// Page.ClientScript.RegisterStartupScript(Page.GetType(), null, "window.open("+Url+", '_newtab')", true);



                //StringBuilder sb = new StringBuilder();

                //sb.Append("<script type = 'text/javascript'>");

                //sb.Append("window.open('");

                //sb.Append(Url);

                //sb.Append("');");

                //sb.Append("</script>");

                //ClientScript.RegisterStartupScript(this.GetType(), "script", sb.ToString());

                GetAllWhiteListUserDomainList();

                //Response.ContentType = "Application/csv";
                //string csvpath = Server.MapPath("~/CSV/Promo Code/") + "UnMatchedRecords" + ".csv";
                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + "UnMatchedRecords" + ".csv" + "");
                //Response.TransmitFile(csvpath);
                //Response.End();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #endregion

        //Added By Hardik "27/Sep/2016"
        protected void rptMinMaxOrderValue_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    CouponBE oCouponBE = objCouponBE;

                    int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));
                    TextBox txtMinOrder = (TextBox)e.Item.FindControl("txtMinOrder");
                    TextBox txtMaxOrder = (TextBox)e.Item.FindControl("txtMaxOrder");
                    if (lstMinMaxOrderValue != null)
                    {
                        CouponBE.CouponCurrencyBE MinMaxOrderValueBE = lstMinMaxOrderValue.FirstOrDefault(x => x.CurrencyId == iCurrency);
                        if (oCouponBE.IsReadOnly)
                        {
                            txtMinOrder.Enabled = false;
                            txtMaxOrder.Enabled = false;
                        }
                        if (MinMaxOrderValueBE != null)
                        {
                            txtMinOrder.Text = Convert.ToString(MinMaxOrderValueBE.MinimumOrderValue);
                            txtMaxOrder.Text = Convert.ToString(MinMaxOrderValueBE.MaximumOrderValue);
                        }
                        else
                        {
                            txtMinOrder.Text = "0";
                            txtMaxOrder.Text = "0";
                        }
                    }
                    else
                    {
                        txtMinOrder.Text = "0";
                        txtMaxOrder.Text = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        //Added By Hardik "27/Sep/2016"
        //protected void rptMaximumOrderValue_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //        {
        //            int iCurrency = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "CurrencyID"));
        //            CouponBE.CouponCurrencyBE MaximumOrderValueBE = lstMaximumOrderValue.FirstOrDefault(x => x.CurrencyId == iCurrency);
        //            TextBox txtCurrencyValue = (TextBox)e.Item.FindControl("txtCurrencyValue");
        //            if (MaximumOrderValueBE != null)
        //                txtCurrencyValue.Text = Convert.ToString(MaximumOrderValueBE.MaximumOrderValue);
        //            else
        //                txtCurrencyValue.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //}

        //Added By Hardik "27/Sep/2016"

        /*Added By Hardik "27/Sep/2016" */
        private void BindCurrency()
        {
            try
            {
                rptMinMaxOrderValue.Visible = true;
                //  rptMaximumOrderValue.Visible = true;
                StoreBE objStoreBE = StoreBL.GetStoreDetails();

                rptMinMaxOrderValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                rptMinMaxOrderValue.DataBind();

                //rptMaximumOrderValue.DataSource = objStoreBE.StoreCurrencies.FindAll(x => x.IsActive == true);
                //rptMaximumOrderValue.DataBind();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /*Added By Hardik "28/Sep/2016" */
        private bool SaveCouponCurrencyDetails(Int32 CouponId)
        {
            try
            {
                DataTable dtCouponMinMax = new DataTable();

                dtCouponMinMax.Columns.AddRange(new DataColumn[4] { 
                new DataColumn("CouponId",typeof(Int32)),
                new DataColumn("CurrencyId",typeof(Int16)),
                new DataColumn("MinimumOrderValue",typeof(Double)),
                new DataColumn("MaximumOrderValue",typeof(Double))
            });

                int i = 0;
                foreach (RepeaterItem item in rptMinMaxOrderValue.Items)
                {
                    if (dtCouponMinMax != null)
                    {
                        dtCouponMinMax.Rows.Add();
                        dtCouponMinMax.Rows[i]["CouponId"] = Convert.ToInt32(CouponId);
                        HiddenField thdfCurrencyID = (HiddenField)item.FindControl("hdfCurrencyID");
                        TextBox txtMinOrder = (TextBox)item.FindControl("txtMinOrder");
                        TextBox txtMaxOrder = (TextBox)item.FindControl("txtMaxOrder");
                        dtCouponMinMax.Rows[i]["CurrencyId"] = Convert.ToInt16(thdfCurrencyID.Value);
                        dtCouponMinMax.Rows[i]["MinimumOrderValue"] = Convert.ToDouble(txtMinOrder.Text);
                        if (txtMaxOrder.Text != "")
                        {
                            dtCouponMinMax.Rows[i]["MaximumOrderValue"] = Convert.ToDouble(txtMaxOrder.Text);
                        }
                        else
                        {
                            dtCouponMinMax.Rows[i]["MaximumOrderValue"] = 0;
                        }

                    }
                    i++;
                }

                bool result = CouponBL.AED_CouponMinMaxValue(dtCouponMinMax);
                return result;
                //if (result)
                //    GlobalFunctions.ShowModalAlertMessages(this.Page, "Coupon saved Successfully", AlertType.Success);
                //else
                //    GlobalFunctions.ShowModalAlertMessages(this.Page, "SystemError/GeneralMessage", AlertType.Failure);
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        protected void GetAllWhiteListUserDomainList()
        {
            try
            {
                string UsrType = string.Empty;
                CouponBE OCouponBE = Session["lstCouponWhitelistExport"] as CouponBE;
                int CouponId = OCouponBE.CouponID;
                // objCouponBE.UserType = UserType;
                string filename = "PromoCode" + ".csv";
                if (RdWhiteList.SelectedValue == "1")
                {
                    UsrType = "email";
                }
                else
                {
                    UsrType = "domain";
                }
                DataTable dt;
                dt = CouponBL.GetUserListCouponMasterNew(Constants.USP_GetUserCouponList, CouponId,UsrType);
                GlobalFunctions.ExportToCSV(dt, filename);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}