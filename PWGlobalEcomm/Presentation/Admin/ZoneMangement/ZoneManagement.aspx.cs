﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Data;
using System.Threading;

namespace Presentation
{
    public partial class Admin_ZoneManagement_ZoneManagement : BasePage//System.Web.UI.Page
    {
        //CountryBL objCountryBL = new CountryBL();
        protected global::System.Web.UI.WebControls.Repeater rpZoneManagement;
        protected global::System.Web.UI.WebControls.DropDownList ddlZone, ddlSelectRegion;
        protected global::System.Web.UI.WebControls.CheckBox chkUnAllocated;
        protected global::System.Web.UI.WebControls.HiddenField hdfddlZoneVal;
        FreightRegionManagementBE objFreightRegionManagementBE = FreightRegionManagementBL.GetShipmentMethodDetails();

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!IsPostBack)
                {
                    PopulateData();
                    BindRegionName();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        private void BindRegionName()
        {

            if (objFreightRegionManagementBE.FreightRegionsBELst.Count > 0)
            {
                ddlSelectRegion.DataSource = objFreightRegionManagementBE.FreightRegionsBELst;
                ddlSelectRegion.DataTextField = "FreightRegionName";
                ddlSelectRegion.DataValueField = "FreightRegionId";
                ddlSelectRegion.DataBind();
                ddlSelectRegion.SelectedValue = Convert.ToString(objFreightRegionManagementBE.FreightRegionsBELst.FirstOrDefault(x => x.FreightRegionName.ToLower() == "all").FreightRegionId);
                //  ddlSelectRegion.Items.Insert(0, new ListItem("--Select Region--", ""));            
            }
        }

        protected void rpZoneManagement_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    ddlZone = e.Item.FindControl("ddlZone") as DropDownList;
                    chkUnAllocated = e.Item.FindControl("chkUnAllocated") as CheckBox;
                    hdfddlZoneVal = e.Item.FindControl("hdfddlZoneVal") as HiddenField;
                    ddlZone.DataSource = objFreightRegionManagementBE.FreightRegionsBELst.Where(x => x.FreightRegionName != "ALL");
                    ddlZone.DataTextField = "FreightRegionName";
                    ddlZone.DataValueField = "FreightRegionId";
                    ddlZone.DataBind();

                    //Sripal - Snehal 24 10 2016
                    string strFreightRegionName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "RegionCode"));
                    if (strFreightRegionName.ToLower() == "eu" || strFreightRegionName.ToLower() == "uk")
                    {
                        strFreightRegionName = strFreightRegionName + " Region";
                    }
                    FreightRegionManagementBE.FreightRegionsBE objRegion = objFreightRegionManagementBE.FreightRegionsBELst.FirstOrDefault(x => x.FreightRegionName.ToLower() == strFreightRegionName.ToLower());
                    if (objRegion != null)
                    {
                        ddlZone.SelectedValue = Convert.ToString(objRegion.FreightRegionId);
                        hdfddlZoneVal.Value = ddlZone.SelectedValue;
                        if (ddlZone.SelectedValue == "5")
                        {
                            chkUnAllocated.Checked = true;
                            ddlZone.Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                DataTable dtZone = new DataTable();
                dtZone.Columns.Add("CountryId", typeof(int));
                dtZone.Columns.Add("RegionCode", typeof(string));
                dtZone.Columns.Add("UserID", typeof(int));
                dtZone.Columns.Add("DisplayDutyMessage", typeof(bool));

                DataRow drZone;
                //foreach (ListItem item in rpZoneManagement.Items) { }

                UserBE objUser = new UserBE();
                objUser = (UserBE)Session["StoreUser"];
                for (int i = 0; i < rpZoneManagement.Items.Count; i++)
                {
                    drZone = dtZone.NewRow();
                    drZone["CountryId"] = (((HiddenField)rpZoneManagement.Items[i].FindControl("hdfCountryID"))).Value;
                    drZone["RegionCode"] = (((DropDownList)rpZoneManagement.Items[i].FindControl("ddlZone"))).SelectedItem.Text;
                    drZone["UserID"] = objUser.UserId;
                    drZone["DisplayDutyMessage"] = ((CheckBox)rpZoneManagement.Items[i].FindControl("chkDutyMessage")).Checked;
                    dtZone.Rows.Add(drZone);
                }
                List<CountryBE> lstCountryData = new List<CountryBE>();
                lstCountryData = ZoneManagementBL.ManageResourceData_SAED(DBAction.Update, dtZone);
                if (lstCountryData != null && lstCountryData.Count > 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Zones updated successfully.", AlertType.Success);
                    CreateActivityLog("ZoneManagement btnSave", "Update", "");
                    PopulateData();
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There is some problem in updating Zone. Please try after sometime.", AlertType.Failure);
                }
                drZone = null;
                dtZone = null;
                dtZone.Dispose();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void chkUnAllocated_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            var itemC = (RepeaterItem)chk.NamingContainer;
            var chkIndex = itemC.ItemIndex;
            DropDownList DDLzone = (DropDownList)rpZoneManagement.Controls[chkIndex].Controls[0].FindControl("ddlZone");
            HiddenField hdfddlZoneVal = (HiddenField)rpZoneManagement.Controls[chkIndex].Controls[0].FindControl("hdfddlZoneVal");

            if (chk.Checked == true)
            {
                DDLzone.SelectedValue = "5";
                DDLzone.Enabled = false;
            }
            if (chk.Checked == false)
            {
                if (hdfddlZoneVal.Value == "5")
                {
                    DDLzone.SelectedValue = "1";
                    DDLzone.Enabled = true;
                }
                else
                {
                    DDLzone.SelectedValue = Convert.ToString(hdfddlZoneVal.Value);
                    DDLzone.Enabled = true;
                }
            }
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            try { Response.Redirect("~/Admin/Dashboard/Dashboard.aspx"); }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        private void PopulateData()
        {
            try
            {
                rpZoneManagement.DataSource = CountryBL.GetAllCountries();
                rpZoneManagement.DataBind();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void ddlSelectRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                rpZoneManagement.DataSource = CountryBL.GetAllFreightCountry(ddlSelectRegion.SelectedItem.Text);
                rpZoneManagement.DataBind();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnManageRegion_Click(object sender, EventArgs e)
        {
            try { Response.Redirect("~/Admin/RegionFreightManagement/ManageRegion.aspx"); }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}