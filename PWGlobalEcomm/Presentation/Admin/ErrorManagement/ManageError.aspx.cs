﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;

namespace Presentation
{
    public class Admin_ErrorManagement_ManageError : BasePage//System.Web.UI.Page
    {
        static int UserId;
        static string ErrorType;
        static Int16 ErrorId;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        protected global::System.Web.UI.WebControls.TextBox txtErrorName, txtSubject;
        protected global::CKEditor.NET.CKEditorControl txtBody;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvLang;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtBody.config.toolbar = new object[]
                {
                    new object[] { "Source" },
                    new object[] { "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
                    new object[] { "NumberedList", "BulletedList"},
                    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
                    "/",
                    new object[] { "Styles", "Format", "Font", "FontSize" },
                    new object[] { "TextColor", "BGColor" },
                    "/",
                    new object[] { "Link", "Unlink"},
                    new object[] { "Table", "HorizontalRule"}
                };
                if (!IsPostBack)
                {
                    if (Session["StoreUser"] != null)
                    {
                        UserBE objUserBE = new UserBE();
                        objUserBE = Session["StoreUser"] as UserBE;
                        UserId = objUserBE.UserId;
                    }
                    PopulateLanguage();
                    if (Convert.ToString(Session["EmailType"]) != "" || Convert.ToString(Session["EmailType"]) != null)
                    {
                        ErrorType = Convert.ToString(Session["ErrorType"]);
                        if (ErrorType == "Edit")
                        {
                            if (Convert.ToString(Session["ErrorTemplateId"]) != null)
                            {
                                ErrorId = Convert.ToInt16(Session["ErrorTemplateId"]);
                                PopulateError(ErrorId); dvLang.Visible = true;
                            }
                            else
                            { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ErrorListing.aspx", AlertType.Warning); return; }
                        }
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ErrorListing.aspx", AlertType.Warning); return; }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorId = Convert.ToInt16(Session["EmailTemplateId"]);
                PopulateError(ErrorId);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails();
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }

        protected void PopulateError(int Id)
        {
            try
            {
                ErrorManagementBE lst = new ErrorManagementBE();
                ErrorManagementBE objBE = new ErrorManagementBE();
                objBE.ErrorTemplateId = Convert.ToInt16(Id);
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                lst = ErrorManagementBL.GetSingleErrorObject(Constants.USP_GetErrorTemplate, objBE, true);
                if (lst != null && lst.ErrorTemplateName != null)
                {
                    txtErrorName.Text = lst.ErrorTemplateName.Trim();
                    txtSubject.Text = lst.Subject.Trim();
                    txtBody.Text = lst.Body.Trim();
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Sanitizer.GetSafeHtmlFragment(txtBody.Text.Trim()) == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error message content can be empty.", AlertType.Warning);
                    return;
                }
                int _Result = 0;
                ErrorManagementBE objBE = new ErrorManagementBE();
                ErrorType = Convert.ToString(Session["ErrorType"]);
                ErrorId = Convert.ToInt16(Session["ErrorTemplateId"]);
                objBE.ErrorTemplateId = ErrorId;
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                objBE.ErrorTemplateName = Sanitizer.GetSafeHtmlFragment(txtErrorName.Text.Trim());
                objBE.Subject = Sanitizer.GetSafeHtmlFragment(txtSubject.Text.Trim());
                objBE.Body = GlobalFunctions.RemoveSanitisedPrefixes(Sanitizer.GetSafeHtmlFragment(txtBody.Text.Trim()));
                objBE.CreatedBy = UserId;
                objBE.IsHtml = true;
                objBE.IsActive = true;
                if (ErrorType == "Edit")
                {
                    int i = ErrorManagementBL.UpdateErrorDetails(Constants.USP_UpdateErrorTemplate, objBE, true);
                    if (i == 0)
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); return; }
                    _Result = 1;
                }
                else
                {
                    int i = ErrorManagementBL.InsertErrorDetails(Constants.USP_InsertErrorTemplate, objBE, true);
                    if (i == 0)
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); return; }
                    _Result = 2;
                }
                if (_Result != 0)
                {
                    Session["ErrorType"] = null;
                    Session["ErrorTemplateId"] = null;
                    Button btn = sender as Button;
                    if (btn.ID == "btnSaveExit")
                    {
                        if (_Result == 1)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Error template has been updated successfully", GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ErrorListing.aspx", AlertType.Success);
                            CreateActivityLog("Manage Error btnSaveExit", "Update", Convert.ToString(ErrorId + "_" + txtErrorName.Text + "_" + txtSubject.Text));
                        }
                        else if (_Result == 2)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Error template has been added successfully", GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ErrorListing.aspx", AlertType.Success);
                            CreateActivityLog("Manage Error btnSaveExit", "Insert/Added", Convert.ToString(ErrorId + "_" + txtErrorName.Text + "_" + txtSubject.Text));
                        }
                    }
                    else if (btn.ID == "btnSaveAddAnother")
                    {
                        if (_Result == 1)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Error template has been updated successfully", GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ManageError.aspx", AlertType.Success);
                            CreateActivityLog("Manage Error btnSaveAddAnother", "Update", Convert.ToString(ErrorId + "_" + txtErrorName.Text + "_" + txtSubject.Text));
                        }
                        else if (_Result == 2)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Error template has been added successfully", GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ManageError.aspx", AlertType.Success);
                            CreateActivityLog("Manage Error btnSaveAddAnother", "Insert/Added", Convert.ToString(ErrorId + "_" + txtErrorName.Text + "_" + txtSubject.Text));
                        }
                    }
                }
                else
                { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning); return; }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ErrorListing.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}