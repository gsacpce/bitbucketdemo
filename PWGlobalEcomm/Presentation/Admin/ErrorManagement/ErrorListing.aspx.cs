﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_ErrorManagement_ErrorListing : System.Web.UI.Page
    {
        int LanguageId;
        protected global::System.Web.UI.WebControls.Button btnAddErrorTemplate;
        protected global::System.Web.UI.WebControls.GridView grdErrorTemplate;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LanguageId = GlobalFunctions.GetLanguageId();
                if (!IsPostBack)
                {
                    btnAddErrorTemplate.Visible = true;
                    PopulateTemplate();
                    Session["ErrorType"] = null;
                    Session["ErrorTemplateId"] = null;
                }
                #region Meta Data
                Page.Title = "Error Management Listing";
                Page.MetaDescription = "Error Management Listing";
                Page.MetaKeywords = "error template";
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateTemplate()
        {
            try
            {
                List<ErrorManagementBE> lst = new List<ErrorManagementBE>();
                ErrorManagementBE objBE = new ErrorManagementBE();
                objBE.LanguageId = Convert.ToInt16(LanguageId);
                lst = ErrorManagementBL.GetAllTemplates(Constants.USP_GetErrorTemplate, objBE, "A");
                if (lst != null)
                {
                    if (lst.Count > 0)
                    {
                        grdErrorTemplate.DataSource = lst;
                        grdErrorTemplate.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnAddErrorTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ManageError.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void grdErrorTemplate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdErrorTemplate.PageIndex = e.NewPageIndex;
                PopulateTemplate();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lbtnEdit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                Session["ErrorTemplateId"] = Convert.ToInt32(e.CommandArgument);
                Session["ErrorType"] = "Edit";
                Response.Redirect(GlobalFunctions.GetVirtualPathAdmin() + "ErrorManagement/ManageError.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lnkbtnDelete_Command(object sender, CommandEventArgs e)
        {

        }
    }
}