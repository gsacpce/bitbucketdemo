﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_SocialMediaManagement_SocialMediaManagement : BasePage //System.Web.UI.Page
    {
        public string host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.TextBox txtSocialMediaName, txtURL;
        protected global::System.Web.UI.WebControls.CheckBox chkIsActive;
        protected global::System.Web.UI.WebControls.FileUpload flupImage;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgEdit;
        protected global::System.Web.UI.WebControls.Label lblHeading;

        protected global::System.Web.UI.HtmlControls.HtmlAnchor anchorView;
        protected global::System.Web.UI.WebControls.Repeater rptSocialMedia;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liSequence;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["method"] != null)
            {
                if (Request.QueryString["method"] == "savesequence")
                {
                    string SequenceData = Request.Form["W"].ToString();
                    SaveSequenceData(SequenceData);
                    imgEdit.Visible = false;
                }

            }
            if (!IsPostBack)
            {

                BindSocialMedia();
            }
        }
        protected void BindSocialMedia()
        {
            List<SocialMediaManagementBE> SocialMediaPlatforms = SocialMediaManagementBL.ListSocialMediaManagement();
            if (SocialMediaPlatforms != null)
            {
                if (SocialMediaPlatforms.Count > 0)
                {

                    rptSocialMedia.DataSource = SocialMediaPlatforms;
                    rptSocialMedia.DataBind();
                    Session["SocialMediaPlatform"] = SocialMediaPlatforms;
                }
                else
                {
                    liSequence.Visible = false;
                }

            }


        }

        protected void rptSocialMedia_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Image ImageSocialMedia = e.Item.FindControl("ImageSocialMedia") as Image;
                ImageSocialMedia.ImageUrl = host + "Images/SocialMedia/" + DataBinder.Eval(e.Item.DataItem, "SocialMediaId") + DataBinder.Eval(e.Item.DataItem, "IconExtension");
                System.Web.UI.HtmlControls.HtmlGenericControl liPC = e.Item.FindControl("liPC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                liPC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "SocialMediaId").ToString());

                LinkButton lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;
                bool IsActive = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "IsActive").ToString());
                if (!IsActive)
                {
                    lnkDelete.Visible = false;
                }


            }
        }





        protected void btnSave_Command(object sender, CommandEventArgs e)
        {

            switch (e.CommandName)
            {
                case "save": SaveSocialMediaDetails(); break;
                case "clear":
                    {
                        txtSocialMediaName.Text = "";
                        txtURL.Text = "";

                        imgEdit.Attributes["style"] = "display:none";
                        imgEdit.Src = "";
                        chkIsActive.Checked = false;
                       
                        txtURL.Enabled = true;
                        txtSocialMediaName.Enabled = true;
                        //anchorView.Attributes["style"] = "display:none";
                        //anchorView.HRef = "";
                        break;
                    };
            }

        }
        protected void Btn_Click(object sender, EventArgs e)
        {


            LinkButton lnkBtn = sender as LinkButton;



            if (lnkBtn != null)
                switch (lnkBtn.CommandName.ToLower())
                {
                    case "update":
                        {
                            SocialMediaManagementBE SocialMediaPlatform = new SocialMediaManagementBE();

                            //(Session["SlideShows"] as List<object>).Find(x => (x as SocialMediaManagementBE).SocialMediaId == Convert.ToInt16(lnkBtn.CommandArgument));
                            if (Session["SocialMediaPlatform"] != null)
                            {
                                foreach (var item in (Session["SocialMediaPlatform"] as List<SocialMediaManagementBE>))
                                {
                                    if (((SocialMediaManagementBE)item).SocialMediaId == Convert.ToInt16(lnkBtn.CommandArgument))
                                    {
                                        SocialMediaPlatform = ((SocialMediaManagementBE)item);
                                    }
                                }
                                Session["EditSocialMediaPlatform"] = SocialMediaPlatform;
                                Session["EditOperation"] = lnkBtn.CommandName;

                                txtSocialMediaName.Text = SocialMediaPlatform.SocialMediaPlatform;
                                txtURL.Text = SocialMediaPlatform.SocialMediaUrl;
                                txtURL.Enabled = false;
                                txtSocialMediaName.Enabled = false;
                                chkIsActive.Checked = SocialMediaPlatform.IsActive;
                                //anchorView.HRef = host + "Images/SocialMedia/" + SocialMediaPlatform.SocialMediaId + SocialMediaPlatform.IconExtension;
                                //anchorView.Attributes["style"]= "display:block";

                                imgEdit.Src = host + "Images/SocialMedia/" + SocialMediaPlatform.SocialMediaId + SocialMediaPlatform.IconExtension;
                                imgEdit.Attributes.Add("style","display:block") ;

                                lblHeading.Text = "Edit social media";
                            }

                            break;
                        }
                    case "delete":
                        {
                            string[] commandArgs = lnkBtn.CommandArgument.ToString().Split(new char[] { ',' });
                            int SocialMediaId = Convert.ToInt16(commandArgs[0]);
                            string strSocialMediaPlatform = commandArgs[1];

                            SocialMediaManagementBE SocialMediaPlatform = new SocialMediaManagementBE();
                            foreach (var item in (Session["SocialMediaPlatform"] as List<SocialMediaManagementBE>))
                            {
                                if (((SocialMediaManagementBE)item).SocialMediaId == SocialMediaId) //Convert.ToInt16(lnkBtn.CommandArgument))
                                {
                                    SocialMediaPlatform = ((SocialMediaManagementBE)item);
                                }
                            }
                            Session["EditSocialMediaPlatform"] = SocialMediaPlatform;
                            Session["EditOperation"] = lnkBtn.CommandName;

                            if (Session["SocialMediaPlatform"] != null && Session["EditOperation"].ToString().Equals("Delete"))
                            {
                                SocialMediaManagementBE objImageSlideShow = Session["EditSocialMediaPlatform"] as SocialMediaManagementBE;
                                objImageSlideShow.Action = 3;


                                if (SocialMediaManagementBL.ManageSocialMedia(objImageSlideShow) > 0)
                                {
                                    CreateActivityLog("Social Media Management", "Disabled",objImageSlideShow.SocialMediaId +"_"+strSocialMediaPlatform);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Social Media Platform details saved successfully!", AlertType.Success);
                                    BindSocialMedia();
                                }

                            }

                            break;
                        }

                    default:
                        break;
                }
        }



        protected void SaveSocialMediaDetails()
        {
            SocialMediaManagementBE SocialMediaPlatform = new SocialMediaManagementBE();
            int Id = 0;
            string[] imageExtensionArray = new string[] { ".jpeg", ".jpg", ".gif", ".png" };
            if (Page.IsValid)
            {

                string minPass = "0";
                bool IsExists = false;
                if (txtSocialMediaName.Text.Trim() == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Social media name can not be empty !!", AlertType.Warning);
                    return;
                }

                if (txtURL.Text.Trim() == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Social media url can not be empty !!", AlertType.Warning);
                    return;
                }

                if (Session["EditSocialMediaPlatform"] != null)
                {
                    SocialMediaPlatform = (SocialMediaManagementBE)Session["EditSocialMediaPlatform"];
                    if (Convert.ToString(Session["EditOperation"]).ToLower() != "update")
                    {
                        if (SocialMediaPlatform.SocialMediaPlatform != Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSocialMediaName.Text.Trim()))
                        {
                            if (CheckSocialMediaPlatformNameExist(txtSocialMediaName.Text.Trim()))
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Social Media Name Exists", AlertType.Warning);
                                //Response.Write("<script>alert('Social Media Name Exists')</script>");
                                IsExists = true;
                            }
                        }
                        if (SocialMediaPlatform.SocialMediaUrl != Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtURL.Text.Trim()))
                        {
                            if (CheckSocialMediaPlatformNameExist(txtURL.Text.Trim()))
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Social Media Url Exists", AlertType.Warning);
                                // Response.Write("<script>alert('Social Media Url Exists')</script>");
                                IsExists = true;
                            }
                        }
                    }
                    if (!IsExists)
                    {

                        if (!flupImage.HasFile)
                        {

                            if (imgEdit.Src != "")
                            {
                                //flupImage.PostedFile.SaveAs(Server.MapPath("~/Images/SocialMedia/Temp/") + flupImage.FileName);

                                SocialMediaPlatform.Action = 2;
                                SocialMediaPlatform.SocialMediaUrl = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtURL.Text.Trim().Replace('\'', '@'));
                                SocialMediaPlatform.SocialMediaPlatform = txtSocialMediaName.Text.Trim();//Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSocialMediaName.Text.Trim().Replace('\'', '@'));
                                SocialMediaPlatform.IsActive = chkIsActive.Checked;
                                SocialMediaPlatform.IconExtension = Path.GetExtension(imgEdit.Src);
                                Id = SocialMediaManagementBL.ManageSocialMedia(SocialMediaPlatform);
                                if (Id > 0)
                                {
                                    CreateActivityLog("Social Media Management", "Updated", Id + "_" + txtSocialMediaName.Text);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated successfully", AlertType.Success);
                                    // Response.Write("<script>alert('Updated successfully')</script>");
                                    Session["EditSocialMediaPlatform"] = null;
                                    txtSocialMediaName.Text = "";
                                    txtURL.Text = "";
                                    //chkIsActive.Checked = false;
                                    lblHeading.Text = "Add new social media";
                                    imgEdit.Attributes["style"] = "display:none";
                                    imgEdit.Src = "";
                                    //anchorView.Attributes["style"] = "display:none";
                                    //anchorView.HRef = "";
                                    BindSocialMedia();
                                    return;
                                }
                            }
                            else
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Please upload file", AlertType.Failure);

                                // Response.Write("<script>alert('Please upload file')</script>");

                            }

                        }


                        else
                        {
                            int count = 0;
                            foreach (string imageExtension in imageExtensionArray)
                            {
                                if (Path.GetExtension(flupImage.FileName) != imageExtension)
                                {
                                    count++;
                                }

                            }
                            if (count == imageExtensionArray.Length)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Not a valid file", AlertType.Failure);
                                //Response.Write("<script>alert('Not a valid file')</script>");

                            }
                            else
                            {
                                flupImage.PostedFile.SaveAs(Server.MapPath("~/Images/SocialMedia/Temp/") + flupImage.FileName);

                                SocialMediaPlatform.Action = 2;
                                SocialMediaPlatform.SocialMediaUrl = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtURL.Text.Trim().Replace('\'', '@'));
                                SocialMediaPlatform.SocialMediaPlatform = txtSocialMediaName.Text.Trim();//Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSocialMediaName.Text.Trim().Replace('\'', '@'));
                                SocialMediaPlatform.IsActive = chkIsActive.Checked;
                                SocialMediaPlatform.IconExtension = Path.GetExtension(flupImage.FileName);
                                Id = SocialMediaManagementBL.ManageSocialMedia(SocialMediaPlatform);
                                if (Id > 0)
                                {
                                    if (File.Exists(Server.MapPath("~/Images/SocialMedia/") + Id + Path.GetExtension(flupImage.FileName)))
                                    {
                                        File.Delete(Server.MapPath("~/Images/SocialMedia/") + Id + Path.GetExtension(flupImage.FileName));
                                    }
                                    File.Move(Server.MapPath("~/Images/SocialMedia/Temp/") + flupImage.FileName, Server.MapPath("~/Images/SocialMedia/") + Id + Path.GetExtension(flupImage.FileName));
                                    CreateActivityLog("Social Media Management", "Updated", Id +"_"+txtSocialMediaName.Text);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Updated successfully", AlertType.Success);
                                    // Response.Write("<script>alert('Updated successfully')</script>");
                                    Session["EditSocialMediaPlatform"] = null;
                                    lblHeading.Text = "Add new social media";
                                    txtSocialMediaName.Text = "";
                                    txtURL.Text = "";
                                    // chkIsActive.Checked = false;

                                    imgEdit.Attributes["style"] = "display:none";
                                    imgEdit.Src = "";
                                    //anchorView.Attributes["style"] = "display:none";
                                    //anchorView.HRef = "";
                                    BindSocialMedia();
                                }
                            }

                        }


                    }

                }
                else
                {
                    if (CheckSocialMediaPlatformNameExist(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSocialMediaName.Text.Trim().Replace('\'', '@'))))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Social Media Name Exists", AlertType.Warning);
                        // Response.Write("<script>alert('Social Media Name Exists')</script>");
                        IsExists = true;
                    }
                    if (CheckSocialMediaPlatformNameExist(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtURL.Text.Trim().Replace('\'', '@'))))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Social Media Url Exists", AlertType.Warning);
                        //Response.Write("<script>alert('Social Media Url Exists')</script>");
                        IsExists = true;
                    }
                    if (!IsExists)
                    {
                        if (!flupImage.HasFile)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Please upload file", AlertType.Failure);
                            //  Response.Write("<script>alert('Please upload file')</script>");
                        }
                        else
                        {
                            int count = 0;
                            foreach (string imageExtension in imageExtensionArray)
                            {
                                if (Path.GetExtension(flupImage.FileName) != imageExtension)
                                {
                                    count++;
                                }

                            }
                            if (count == imageExtensionArray.Length)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Not a valid file", AlertType.Failure);
                                // Response.Write("<script>alert('Not a valid file')</script>");

                            }
                            else
                            {
                                flupImage.PostedFile.SaveAs(Server.MapPath("~/Images/SocialMedia/Temp/") + flupImage.FileName);
                                SocialMediaPlatform.SocialMediaUrl = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtURL.Text.Trim().Replace('\'', '@'));
                                SocialMediaPlatform.SocialMediaPlatform = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtSocialMediaName.Text.Trim().Replace('\'', '@'));
                                SocialMediaPlatform.IsActive = true;
                                SocialMediaPlatform.IconExtension = Path.GetExtension(flupImage.FileName);
                                SocialMediaPlatform.Action = 1;
                                Id = SocialMediaManagementBL.ManageSocialMedia(SocialMediaPlatform);
                                if (Id > 0)
                                {
                                    if (File.Exists(Server.MapPath("~/Images/SocialMedia/") + Id + Path.GetExtension(flupImage.FileName)))
                                    {
                                        File.Delete(Server.MapPath("~/Images/SocialMedia/") + Id + Path.GetExtension(flupImage.FileName));
                                    }
                                    File.Move(Server.MapPath("~/Images/SocialMedia/Temp/") + flupImage.FileName, Server.MapPath("~/Images/SocialMedia/") + Id + Path.GetExtension(flupImage.FileName));
                                    CreateActivityLog("Social Media Management", "Added",Id+"_"+txtSocialMediaName.Text);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Saved successfully", AlertType.Success);
                                    //Response.Write("<script>alert('Saved successfully')</script>");
                                    BindSocialMedia();
                                }
                            }
                        }

                    }

                }




            }
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 14-09-15
        /// Scope   : to check whether the social media show name exist or not
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        private bool CheckSocialMediaPlatformNameExist(string SocialMediaPlatform)
        {

            List<SocialMediaManagementBE> SocialMediaPlatforms = new List<SocialMediaManagementBE>();
            SocialMediaPlatforms = SocialMediaManagementBL.ListSocialMediaManagement();


            if (SocialMediaPlatforms != null)
            {
                if (SocialMediaPlatforms.Count() > 0)
                {
                    int count = SocialMediaPlatforms.Count(x => x.SocialMediaPlatform.ToLower() == SocialMediaPlatform.ToLower());
                    return count > 0 ? true : false;
                }
            }
            return false;
        }
        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 14-09-15
        /// Scope   : to check whether the social media show url exist or not
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        private bool CheckSocialMediaPlatformUrlExist(string SocialMediaUrl)
        {

            List<SocialMediaManagementBE> SocialMediaPlatforms = new List<SocialMediaManagementBE>();
            SocialMediaPlatforms = SocialMediaManagementBL.ListSocialMediaManagement();


            if (SocialMediaPlatforms != null)
            {
                if (SocialMediaPlatforms.Count() > 0)
                {
                    int count = SocialMediaPlatforms.Count(x => x.SocialMediaUrl.ToLower() == SocialMediaUrl.ToLower());
                    return count > 0 ? true : false;
                }
            }
            return false;
        }



        public void SaveSequenceData(string SequenceData)
        {
            DataTable dtDisplayOrder = GetDataIntoTable(SequenceData);

            bool IsSequenceSet = SocialMediaManagementBL.SetSocialMediaSequence(dtDisplayOrder);

            //Response.Clear();
            if (IsSequenceSet)
            {
                CreateActivityLog("Social Media Management", "Updated", "Sequence");
                Response.Write("1");
            }
            else
                Response.Write("0");
            Response.End();
            //return "Updated Successfully";
        }
        protected DataTable GetDataIntoTable(string vData)
        {
            List<SocialManagementSequenceBE> returnList = new List<SocialManagementSequenceBE>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            vData = "[" + vData.Replace("[", "").Replace("]", "") + "]";
            if (vData != "[]")
            {
                returnList = serializer.Deserialize<List<SocialManagementSequenceBE>>(vData);
            }
            List<SocialManagementSequenceBE> sequenceDataList = new List<SocialManagementSequenceBE>();



            DataTable dt = GlobalFunctions.To_DataTable(returnList);
            dt.Columns["SocialMediaId"].ColumnName = "socialmediaid";
            dt.Columns["DisplayOrder"].ColumnName = "DisplayOrder";

            return dt;

        }

    }
}

