﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using System.Text.RegularExpressions;

namespace Presentation
{
    public class Admin_Login_ResetPassword : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.TextBox txtConfirmPassword, txtPassword, txtForgotEmail, txtCaptcha;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvForgot, dvReset, dvError, dvMsg;
        protected global::System.Web.UI.WebControls.Label lblMsg;
        protected global::System.Web.UI.WebControls.Literal litJS, litCSSnJS;

        Int16 UserId = 0;
        string UserEmail = "";
        Int16 CurrencyId;
        public static string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();


        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (GlobalFunctions.GetLanguageId() == 0 || GlobalFunctions.GetCurrencyId() == 0 || string.IsNullOrEmpty(GlobalFunctions.GetCurrencySymbol()))
                {
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();

                    GlobalFunctions.SetLanguageId(objStoreBE.StoreLanguages.FirstOrDefault(x => x.IsDefault == true).LanguageId);
                    GlobalFunctions.SetCurrencyId(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyId);
                    GlobalFunctions.SetCurrencySymbol(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencySymbol);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                litJS.Text = "  <script src='" + hostAdmin + "JS/jquery.js'></script>" +
                    "<script src='" + hostAdmin + "JS/bootstrap.min.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery.smartmenus.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery.smartmenus.bootstrap.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery-ui.js'></script>" +
                    "<script src='" + hostAdmin + "JS/html5shiv.js'></script>" ;

                litCSSnJS.Text = "<title></title><link href='" + hostAdmin + "CSS/bootstrap.min.css' rel='stylesheet' />"
                    + "<link href='" + hostAdmin + "CSS/jquery.smartmenus.bootstrap.css' rel='stylesheet' />"
                    + "<link href='" + hostAdmin + "CSS/common-admin.css' rel='stylesheet'/>"
                    + "<link href='" + hostAdmin + "CSS/radio.css' rel='stylesheet'/>"
                    + "<link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>"
                    + "<link href='http://fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css'>";


                if (!IsPostBack)
                {
                    if (Convert.ToString(Request.QueryString["Request"]) != "" && Convert.ToString(Request.QueryString["Request"]) != null)
                    {
                        dvForgot.Visible = false;
                        dvReset.Visible = true;
                        string str = GlobalFunctions.Decrypt(Convert.ToString(Request.QueryString["Request"]));
                        if (str != "")
                        { string[] val = str.Split('+'); UserId = val[0].To_Int16(); UserEmail = val[1].ToString(); VerifyRequest(UserEmail); }
                        else
                        { dvForgot.Visible = true; dvReset.Visible = false; dvError.Visible = true; dvMsg.Visible = false; }
                    }
                    else
                    { dvForgot.Visible = true; dvReset.Visible = false; }
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
        }

        protected void VerifyRequest(string UserEmail)
        {
            try
            {
                CurrencyId = GlobalFunctions.GetCurrencyId();
                UserBE lst = new UserBE();
                UserBE objBEs = new UserBE();
                objBEs.EmailId = UserEmail;
                objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                lst = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, false, objBEs);
                if (lst != null || lst.EmailId != null)
                {
                    Session["UserLst"] = lst;
                    DateTime currentDateTime = lst.CurrentTimeStamp;
                    DateTime reqDateTime = lst.ResetTime;
                    reqDateTime = reqDateTime.AddMinutes(30);
                    if (currentDateTime <= reqDateTime)
                    {
                        if (lst.IsActive)
                        {
                            dvReset.Visible = true;
                            dvForgot.Visible = false;
                            dvMsg.Visible = false;
                            dvError.Visible = false;
                            txtPassword.Text = "";
                            txtConfirmPassword.Text = "";
                        }
                        else
                        {
                            dvReset.Visible = false;
                            dvForgot.Visible = true;
                            dvMsg.Visible = false;
                            dvError.Visible = true;
                            txtForgotEmail.Text = "";
                            txtCaptcha.Text = "";
                            UserBE objBE = new UserBE();
                            objBE.UserId = lst.UserId;
                           // int i = UserBL.ExecuteResetDetails(Constants.USP_InsertResetLoginDetails, true, objBE, "E");
                        }
                    }
                    else
                    {
                        dvReset.Visible = false;
                        dvForgot.Visible = true;
                        dvMsg.Visible = false;
                        dvError.Visible = true;
                        txtForgotEmail.Text = "";
                        txtCaptcha.Text = "";
                        UserBE objBE = new UserBE();
                        objBE.UserId = lst.UserId;
                        //int i = UserBL.ExecuteResetDetails(Constants.USP_InsertResetLoginDetails, true, objBE, "E");
                    }
                }
                else
                {
                    dvReset.Visible = false;
                    dvForgot.Visible = true;
                    dvMsg.Visible = false;
                    dvError.Visible = true;
                    txtForgotEmail.Text = "";
                    txtCaptcha.Text = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSubmitForgot_Click(object sender, EventArgs e)
        {
            try
            {
                string theCode = Session["CaptchaText"].ToString();
                if (Sanitizer.GetSafeHtmlFragment(txtCaptcha.Text.ToLower()) != theCode.ToLower())
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("Login/Captcha"), AlertType.Warning);
                    txtCaptcha.Text = "";
                    return;
                }
                string EncryptStr = "";
                UserBE lst = new UserBE();
                UserBE objBEs = new UserBE();
                objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(txtForgotEmail.Text.Trim());
                objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                lst = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, false, objBEs);
                if (lst != null || lst.EmailId != null)
                {
                    EncryptStr = lst.UserId + "+" + lst.EmailId;
                    EncryptStr = GlobalFunctions.Encrypt(EncryptStr);
                    string Subject = "";
                    StringBuilder MailBody = new StringBuilder();
                    string ToAddress = lst.EmailId;
                    string FromAddress = "";
                    string CC = "";
                    string BCC = "";
                    bool IsBodyHTML = true;
                    string url = GlobalFunctions.GetVirtualPath() + "Admin/Login/ResetPassword.aspx?Request=" + EncryptStr;
                    List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                    EmailManagementBE objEmail = new EmailManagementBE();
                    objEmail.EmailTemplateName = "Forgot password";
                    objEmail.LanguageId = GlobalFunctions.GetLanguageId();
                    lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                    if (lstEmail != null && lstEmail.Count > 0)
                    {
                        Subject = lstEmail[0].Subject;
                        FromAddress = lstEmail[0].FromEmailId;
                        CC = lstEmail[0].CCEmailId;
                        BCC = lstEmail[0].BCCEmailId;
                        IsBodyHTML = lstEmail[0].IsHtml;
                        MailBody.Append(lstEmail[0].Body.Replace("@UserName", lst.FirstName + " " + lst.LastName).Replace("@URL", url));
                        GlobalFunctions.SendEmail(ToAddress, FromAddress, "", CC, BCC, Subject, MailBody.ToString(), "", "", IsBodyHTML);
                        UserBE objBE = new UserBE();
                        objBE.UserId = lst.UserId;
                        int i = UserBL.ExecuteResetDetails(Constants.USP_InsertResetLoginDetails, true, objBE, "I");
                        txtCaptcha.Text = "";
                        txtForgotEmail.Text = "";
                        dvMsg.Visible = true;
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/SendEmailFailed"), AlertType.Failure); return; }
                }
                else
                { dvMsg.Visible = true; }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/Login/Login.aspx");
        }

        private bool ValidatePassword(string regExp)
        {
            try
            {
                Regex reg = new Regex(regExp); //(regExp, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                return reg.IsMatch(Sanitizer.GetSafeHtmlFragment(txtPassword.Text.Trim()));
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); return false; }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                string minPass = "5";
                bool flag = false;
                if (flag)
                {
                    string regExp = @"^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[\W_])\S{" + minPass + ",20}$";
                    bool check = ValidatePassword(regExp);
                    if (!check)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/RegistrationPasswordCheck"), AlertType.Warning);
                        return;
                    }
                }

                UserBE lst = (UserBE)Session["UserLst"];
                string SaltPass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(txtPassword.Text.Trim()), "SHA512", null);
                UserBE objBE = new UserBE();
                objBE.UserId = lst.UserId;
                objBE.Password = SaltPass;
                objBE.Action = Convert.ToInt16(DBAction.Update);
                int i = UserBL.ExecuteResetDetails(Constants.USP_ResetMCPPassword, false, objBE, Convert.ToString(objBE.Action));

                string Subject = "";
                StringBuilder MailBody = new StringBuilder();
                string ToAddress = lst.EmailId;
                string FromAddress = "";
                string CC = "";
                string BCC = "";
                bool IsBodyHTML = true;
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = "Change Passwords";
                objEmail.LanguageId = 1;//GlobalFunctions.GetLanguageId();
                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    Subject = lstEmail[0].Subject;
                    FromAddress = lstEmail[0].FromEmailId;
                    CC = lstEmail[0].CCEmailId;
                    BCC = lstEmail[0].BCCEmailId;
                    IsBodyHTML = lstEmail[0].IsHtml;
                    MailBody.Append(lstEmail[0].Body.Replace("@UserName", lst.FirstName + " " + lst.LastName).Replace("@Email", lst.EmailId));
                    GlobalFunctions.SendEmail(ToAddress, FromAddress, "", CC, BCC, Subject, MailBody.ToString(), "", "", IsBodyHTML);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Password Updated Successfully",GlobalFunctions.GetVirtualPath() + "Admin/Login/Login.aspx", AlertType.Success);
                }
               
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }
}