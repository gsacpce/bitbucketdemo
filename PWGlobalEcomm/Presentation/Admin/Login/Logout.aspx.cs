﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Login_Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Abandon();
            Session["StoreUser"] = null;
            Session["BrandColors"] = null;
            Session["EditBrandColor"] = null;
            Session["EditOperation"] = null;
            Session["EditBreadCrumb"] = null;
            Session["BreadCrumb"] = null;
            Session["CategoryName"] = null;
            Session["AECustomerGroupId"] = null;
            Session["AEUserId"] = null;
            Session["EmailType"] = null;
            Session["EmailTemplateId"] = null;
            Session["ErrorType"] = null;
            Session["ErrorTemplateId"] = null;
            Session["LandingImageName"] = null;
            Session["SlideShowDetailIdList"] = null;
            Session["SlideShowList"] = null;
            Session["Width"] = null;
            Session["WidthUnit"] = null;
            Session["Height"] = null;
            Session["HeightUnit"] = null;
            Session["CurrFreightRegionId"] = null;
            Session["CurrFreightModeId"] = null;
            Session["CurrFreightMethodId"] = null;
            Session["lstCBE"] = null;
            Session["ContentType"] = null;
            Session["StaticImage"] = null;
            Session["Video"] = null;
            Session["StaticText"] = null;
            Session["FooterText"] = null;
            Session["FooterCopyRight"] = null;
            Session["EditHomePageContent"] = null;
            Session["EditOperation"] = null;
            Session["homeFilePath"] = null;
            Session["host"] = null;
            Session["LandingPageConfigurationID"] = null;
            Session["CaptchaText"] = null;
            Session["UserLst"] = null;
            Session["masterFilePath"] = null;
            Session["lstGMContentAttributes"] = null;
            Session["Orders"] = null;
            Session["PRODUCTTAB"] = null;
            Session["CWS_PRODUCTPRICEBREAKID"] = null;
            Session["PriceBreakDataCHK"] = null;
            Session["IsCallForPrice"] = null;
            Session["PRODUCT_VARIANTS"] = null;
            Session["PRODUCT_VARIANTS_TYPES"] = null;
            Session["VARIANTID"] = null;
            Session["VARIANTIMAGENAME"] = null;
            Session["VARIANTNAME"] = null;
            Session["AllSize"] = null;
            Session["AllElement"] = null;
            Session["AllUOM"] = null;
            Session["PRODUCTS"] = null;
            Session["SectionPageId"] = null;
            Session["SectionPageType"] = null;
            Session["SlideShows"] = null;
            Session["EditSlideShow"] = null;
            Session["CurrSlideId"] = null;
            Session["SocialMediaPlatform"] = null;
            Session["EditSocialMediaPlatform"] = null;
            Session["StaticPageType"] = null;
            Session["StaticPageId"] = null;
            Session["StaticPageDetail"] = null;
            Session["StaticPageList"] = null;
            Session["StaticTextType"] = null;
            Session["StaticTextId"] = null;
            
            Response.Redirect( GlobalFunctions.GetVirtualPathAdmin() + "Login/Login.aspx");
        }
    }
}