﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using System.Data;


namespace Presentation
{
    public class Admin_Login_Login :BasePage //System.Web.UI.Page
    {
        int LoginWrongAttempts = Convert.ToInt16(ConfigurationManager.AppSettings["LoginWrongAttempts"]);
        int LoginWaitTime = Convert.ToInt16(ConfigurationManager.AppSettings["LoginWaitTime"]);
        protected global::System.Web.UI.WebControls.TextBox txtUserName, txtPassword, txtCaptcha;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCaptcha;
        protected global::System.Web.UI.WebControls.Label lblMsg;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden Hidden1;
        public static string hostAdmin = GlobalFunctions.GetVirtualPathAdmin();
        protected global::System.Web.UI.WebControls.Literal litJS, litCSSnJS;


        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (GlobalFunctions.GetLanguageId() == 0 || GlobalFunctions.GetCurrencyId() == 0 || string.IsNullOrEmpty(GlobalFunctions.GetCurrencySymbol()))
                {
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();

                    GlobalFunctions.SetLanguageId(objStoreBE.StoreLanguages.FirstOrDefault(x => x.IsDefault == true).LanguageId);
                    GlobalFunctions.SetCurrencyId(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyId);
                    GlobalFunctions.SetCurrencySymbol(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencySymbol);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        ///// need session  of current store Id and Pass as table type


        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 28-07-16
        /// Scope   : Page Load Function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                litJS.Text = "  <script src='" + hostAdmin + "JS/jquery.js'></script>" +
                    "<script src='" + hostAdmin + "JS/bootstrap.min.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery.smartmenus.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery.smartmenus.bootstrap.js'></script>" +
                    "<script src='" + hostAdmin + "JS/jquery-ui.js'></script>" +
                    "<script src='" + hostAdmin + "JS/html5shiv.js'></script>";

                litCSSnJS.Text = "<title></title><link href='" + hostAdmin + "CSS/bootstrap.min.css' rel='stylesheet' />"
                    + "<link href='" + hostAdmin + "CSS/jquery.smartmenus.bootstrap.css' rel='stylesheet' />"
                    + "<link href='" + hostAdmin + "CSS/common-admin.css' rel='stylesheet'/>"
                    + "<link href='" + hostAdmin + "CSS/radio.css' rel='stylesheet'/>"
                    + "<link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>"
                    + "<link href='http://fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css'>";


                if (!String.IsNullOrEmpty(Convert.ToString(Request.QueryString["request"])))
                {
                    Exceptions.WriteInfoLog(HttpContext.Current.Request.UrlReferrer.AbsolutePath);

                    string cKey = Convert.ToString(ConfigurationManager.AppSettings["CryptoKey"]);
                    string dValue = GlobalFunctions.DecryptQueryStrings(Convert.ToString(Request.QueryString["request"]), cKey);
                    if (dValue != "")
                    {
                        UserBE lst = new UserBE();
                        UserBE objBE = new UserBE();
                        string[] values = dValue.Split('&');
                        objBE.StoreId = Convert.ToInt16(values[0].Split('=')[1]);
                        objBE.EmailId = values[1].Split('=')[1];
                        objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());

                        //lst = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBE);   //// old 
                        //DataSet dsStoreUserMappingDetails = UserBL.GetStoreUserMappingDetails();             //// New
                        lst = UserBL.GetMCPLoginDetailFromCP(false, objBE);
                        Exceptions.WriteInfoLog("Store Admin Login " + (lst == null).ToString());
                        if (lst != null && lst.EmailId != null)
                        {
                            Session["StoreUser"] = lst;
                            Exceptions.WriteInfoLog("Inside Store Admin Login " + (lst == null).ToString());
                            CreateActivityLog("Control Panel", "Logged In", "");
                            Response.Redirect(hostAdmin + "Dashboard/Dashboard.aspx", true);
                        }
                    }
                }

                if (Session["StoreUser"] != null)
                    Response.Redirect(hostAdmin + "Dashboard/Dashboard.aspx", true);

                //UserBE objUser = new UserBE();
                //UserBE objBEs = new UserBE();
                //objBEs.EmailId = "sanchit.patne@powerweave.com";
                //objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                //objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                //objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                //if (objUser != null || objUser.EmailId != null)
                //{
                //    Session["StoreUser"] = objUser;
                //    Response.Redirect(hostAdmin + "Reports/Orders.aspx");
                //}
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

      

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 28-07-16
        /// Scope   : Submit Button Function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (dvCaptcha.Visible)
                {
                    string theCode = Session["CaptchaText"].ToString();
                    if (Sanitizer.GetSafeHtmlFragment(txtCaptcha.Text.ToLower()) != theCode.ToLower())
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("Login/Captcha"), AlertType.Warning);
                        txtCaptcha.Text = "";
                        return;
                    }
                }
                StoreBE objStoreBE = StoreBL.GetStoreDetails();

                string pass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(txtPassword.Text.Trim()), "SHA512", null);
                UserBE objBE = new UserBE();
                objBE.EmailId = Sanitizer.GetSafeHtmlFragment(txtUserName.Text.Trim());
                objBE.Password = pass;
                objBE.IPAddress = GlobalFunctions.GetIpAddress();
                objBE.StoreId = objStoreBE.StoreId;
                objBE.CurrencyId = GlobalFunctions.GetCurrencyId(); //Hardcode get currencyId selected from splash page
                int validateUser = UserBL.ExecuteAdminLogin(Constants.USP_ValidateAdminLogin, false, objBE, LoginWrongAttempts, LoginWaitTime);
                if (validateUser == 0 || validateUser == 2) // -- User does not exist || -- Incorrect user password
                {
                    dvCaptcha.Visible = true;
                    txtCaptcha.Text = "";
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("Login/UserCheck"), AlertType.Warning);
                    return;
                }
                else if (validateUser == 3) // -- User blocked 
                {
                    dvCaptcha.Visible = true;
                    txtCaptcha.Text = "";
                    string _exp = Exceptions.GetException("Login/UserBlock").Replace("~block~", Convert.ToString(LoginWaitTime));
                    GlobalFunctions.ShowModalAlertMessages(this.Page, _exp, AlertType.Warning);
                    return;
                }
                else if (validateUser == 1) // -- Success
                {

                    UserBE objUser = new UserBE();
                    UserBE objBEs = new UserBE();
                    objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(txtUserName.Text.Trim());
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    objBE.StoreId = objStoreBE.StoreId;
                    objUser = UserBL.GetMCPLoginDetailFromCP(false, objBE);

                    if (objUser != null || objUser.EmailId != null)
                    {
                        Session["StoreUser"] = objUser;
                        CreateActivityLog("Control Panel", "Logged-In", "");
                        Session["CaptchaText"] = null;
                        Response.Redirect(hostAdmin + "Dashboard/Dashboard.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}