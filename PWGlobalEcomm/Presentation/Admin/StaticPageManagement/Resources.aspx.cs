﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Data;
using System.Web.UI.HtmlControls;
using Microsoft.Security.Application;

namespace Presentation
{
    public class Admin_StaticPageManagement_Resources : BasePage//System.Web.UI.Page
    {
       public string Hostadmin = GlobalFunctions.GetVirtualPathAdmin();

        protected global::System.Web.UI.WebControls.Repeater rptStaticContent;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMessage;

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : Page_Load event
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PopulateLanguageDropDownList();

                    PopulateResourceData();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : it populates language dropdown
        /// </summary>
        private void PopulateLanguageDropDownList()
        {
            List<LanguageBE> languages = new List<LanguageBE>();
            languages = LanguageBL.GetAllLanguageDetails(true);

            if (languages != null)
            {
                if (languages.Count() > 0)
                {
                    ddlLanguage.DataSource = languages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                }
            }

        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : it populates Resource Data
        /// </summary>
        private void PopulateResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstResourceData = new List<StaticPageManagementBE>();
                DataTable dtResourceData = null;

                lstResourceData = StaticPageManagementBL.ManageResourceData_SAED(DBAction.Select, dtResourceData);
                if (lstResourceData != null && lstResourceData.Count > 0)
                {
                    lstResourceData = lstResourceData.FindAll(x => x.LanguageId == Convert.ToInt16(ddlLanguage.SelectedValue));
                    if (lstResourceData != null && lstResourceData.Count > 0)
                    {
                        rptStaticContent.DataSource = lstResourceData;
                        rptStaticContent.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : ddlLanguage_SelectedIndexChanged
        /// </summary>
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateResourceData();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : rptStaticContent_ItemDataBound event
        /// </summary>
        protected void rptStaticContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //System.Web.UI.HtmlControls.HtmlGenericControl liPC = e.Item.FindControl("liPC") as System.Web.UI.HtmlControls.HtmlGenericControl;

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : btnSave_Click event
        /// </summary>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
              
                UserBE objUserBE = new UserBE();
                objUserBE = Session["StoreUser"] as UserBE;
                Int16 intUserId = objUserBE.UserId;

                DataTable dtResourceData = new DataTable();
                DataRow dr;
                dtResourceData.Columns.Add("ResourceLanguageId");
                dtResourceData.Columns.Add("ResourceValue");
                dtResourceData.Columns.Add("UserId");
                for (int i = 0; i <= rptStaticContent.Items.Count - 1; i++)
                {
                    HtmlInputHidden hdnResourceLanguageId = (HtmlInputHidden)rptStaticContent.Items[i].FindControl("hdnResourceLanguageId");
                    TextBox txtContent = (TextBox)rptStaticContent.Items[i].FindControl("txtContent");
                    dr = dtResourceData.NewRow();

                    dr["ResourceLanguageId"] = Convert.ToInt16(hdnResourceLanguageId.Value);
                    dr["ResourceValue"] = Convert.ToString(txtContent.Text);//Sanitizer.GetSafeHtmlFragment(Convert.ToString(txtContent.Text));
                    dr["UserId"] = intUserId;

                    dtResourceData.Rows.Add(dr);
                   
                }

                List<StaticPageManagementBE> lstResourceData = new List<StaticPageManagementBE>();

                lstResourceData = StaticPageManagementBL.ManageResourceData_SAED(DBAction.Update, dtResourceData);
                if (lstResourceData != null && lstResourceData.Count > 0)
                {
                    //divMessage.InnerText = "Static Content updated successfully.";
                    //divMessage.Attributes.Add("class", "");
                    //divMessage.Visible = true;
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Static Content updated successfully.", AlertType.Success);
                    CreateActivityLog("Resources btnSave", "Update", "");
                    HttpRuntime.Cache.Remove("AllResourceData");
                    PopulateResourceData();
                }
                else
                {
                    //divMessage.InnerText = "There is some problem in updating Static Content. Please try after sometime.";
                    //divMessage.Attributes.Add("class", "Errormsg");
                    //divMessage.Visible = true;
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There is some problem in updating Static Content. Please try after sometime.", AlertType.Failure);
                }
                dr = null;
                dtResourceData = null;
                dtResourceData.Dispose();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 20-08-15
        /// Scope   : btnCancel_Click event
        /// </summary>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Hostadmin + "Dashboard/Dashboard.aspx",true);
        }
    }
}