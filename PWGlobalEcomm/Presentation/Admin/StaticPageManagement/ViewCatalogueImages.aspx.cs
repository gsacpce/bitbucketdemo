﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.BusinessEntity;
using System.Web.Services;
using System.Web.Script.Services;
using System.Linq;

namespace Presentation
{
    public partial class Admin_StaticPageManagement_Default : BasePage//System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.GridView GridView1;
        public string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.FileUpload fuMultiple;
        protected global::System.Web.UI.WebControls.HiddenField hdnImageName;
        protected global::System.Web.UI.WebControls.Button Button1, Button2;
        AddCatalogueImageBE ObjCatalogueImageBE = new AddCatalogueImageBE();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRepeter();
            }
        }
        public void BindRepeter()
        {
            GridView1.DataSource = CatalogueImageBL.GetAllHomePageContentDetails();
            GridView1.DataBind();
            foreach (GridViewRow row in GridView1.Rows)
            {
                System.Net.WebClient client = new System.Net.WebClient();
                HyperLink HyperLink1 = (HyperLink)row.FindControl("HyperLink1");
                HyperLink1.NavigateUrl = HyperLink1.Text;
                Image ImgThumbNail = (Image)row.FindControl("ImgThumbNail");

                ImgThumbNail.ImageUrl = GlobalFunctions.GetVirtualPath() + @"Admin/Images/CatalogueImages/" + ImgThumbNail.ImageUrl;
            }
            if(GridView1.DataSource==null)
            {
                Button1.Visible = false;
                Button2.Visible = false;
            }
            else
            {
                Button2.Visible = true;
                Button1.Visible = true;
            }

        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {

        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string ImageFolderPath = "";
                ImageFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "Admin\\Images\\CatalogueImages\\";
                if (!Directory.Exists(ImageFolderPath))
                    Directory.CreateDirectory(ImageFolderPath);
                bool Result = false;
                HttpFileCollection hfc = Request.Files;
                int iUploadedCnt = 0;
                int iFailedCnt = 0;
                string Physicalpath = GlobalFunctions.GetPhysicalFolderPath() + "Admin\\Images\\CatalogueImages\\";
                if (!System.IO.File.Exists(Physicalpath))
                {
                    System.IO.Directory.CreateDirectory(Physicalpath);
                }
                
                for (int i = 0; i <= hfc.Count - 1; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    string MIMEType = System.IO.Path.GetExtension(hpf.FileName);
                    if (MIMEType == ".png" || MIMEType == ".jpg" || MIMEType == ".jpeg" || MIMEType == ".gif" || MIMEType == ".svg")
                    {
                        if (hpf.ContentLength > 0)
                        {
                            DirectoryInfo objDir = new DirectoryInfo(Physicalpath);
                            string sFileName = Path.GetFileName(hpf.FileName);
                            string sFileExt = Path.GetExtension(hpf.FileName);
                            FileInfo[] objFI = objDir.GetFiles(sFileName);
                            string FileExtension = Convert.ToString(Path.GetExtension(hpf.FileName).Replace(".", ""));
                            hpf.SaveAs(Physicalpath + Path.GetFileName(hpf.FileName));
                            iUploadedCnt += 1;
                            ObjCatalogueImageBE.ImageExtension = FileExtension;
                            ObjCatalogueImageBE.ImageName = hpf.FileName;
                            ObjCatalogueImageBE.URL = GlobalFunctions.GetVirtualPath() + @"Admin/Images/CatalogueImages/" + Path.GetFileName(hpf.FileName);
                            Result = CatalogueImageBL.InsertCatalogueImage(ObjCatalogueImageBE);
                        }
                    }
                }
                ClientScript.RegisterStartupScript(this.GetType(), "#dvLoader1", "<script>HideLoader();</script>");
                if (Result == true)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Images are uploaded successfully", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Can't Upload Images", AlertType.Warning);
                }
                BindRepeter();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            bool Ststus = false;
            foreach (GridViewRow GRrows in GridView1.Rows)
            {
                CheckBox chkCheck = (CheckBox)GRrows.FindControl("checkbocDelete");
                HyperLink HyperLink1 = (HyperLink)GRrows.FindControl("HyperLink1");
                if (chkCheck.Checked)
                {
                    AddCatalogueImageBE objAddcatalogueBE = new AddCatalogueImageBE();
                    objAddcatalogueBE.URL = HyperLink1.Text;
                    Ststus = CatalogueImageBL.DeleteCatalogueImages(objAddcatalogueBE);
                }
            }
            if (Ststus==true)
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Successfully Deleteted images", AlertType.Success);
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Can't Delete images", AlertType.Failure);
            }
            BindRepeter();
        }
    }
}