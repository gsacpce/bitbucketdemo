﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using CKEditor.NET;

namespace Presentation
{

public partial class Admin_StaticPageManagement_ContactUs : BasePage //System.Web.UI.Page
{
    protected global::System.Web.UI.WebControls.TextBox txtHelpDeskEmail;
    protected global::System.Web.UI.WebControls.TextBox txtHelpDeskPhone;
    protected global::System.Web.UI.WebControls.TextBox txtHelpDeskContactPerson;
   // protected global::System.Web.UI.WebControls.TextBox txtHelpDeskText;
    protected global::System.Web.UI.WebControls.HiddenField hdnContactId;
    protected global::CKEditor.NET.CKEditorControl txtHelpDeskText;
    protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;

   StoreBE.ContactBE lstContactBE = new StoreBE.ContactBE();

    StoreBE.ContactBE objContactBE = new StoreBE.ContactBE();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateLanguageDropDownList();
            ReadContactInfo();
            //txtHelpDeskText.config.toolbar = new object[]
            //    {
            //        new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
            //        new object[] { "NumberedList", "BulletedList"},
            //        new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
            //        "/",
            //        new object[] { "Styles", "Format", "Font", "FontSize" },
            //        new object[] { "TextColor", "BGColor" },
            //        "/",
            //                        new object[] { "Link", "Unlink"},
            //        new object[] { "Table", "HorizontalRule"}
            //    };

            txtHelpDeskText.config.toolbar = new object[]
                {
               new object[] { "Source", "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				    new object[] { "NumberedList", "BulletedList"},
				    new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				    "/",
				    new object[] { "Styles", "Format", "Font", "FontSize" },
				    new object[] { "TextColor", "BGColor" },
                    "/",
                                    new object[] { "Link", "Unlink"},
                    new object[] { "Table", "HorizontalRule"}
                };
        }
    }
    protected void ReadContactInfo()
    {
        try
        {
            objContactBE.Action = Convert.ToInt16(DBAction.Select);
            objContactBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
            lstContactBE = StoreBL.GetContactInfo(objContactBE);
            if (lstContactBE != null)
            {
                txtHelpDeskEmail.Text = Convert.ToString(lstContactBE.HelpDeskEmail);
                txtHelpDeskPhone.Text = Convert.ToString(lstContactBE.HelpDeskPhone);
                txtHelpDeskContactPerson.Text = Convert.ToString(lstContactBE.HelpDeskContactPerson);
                txtHelpDeskText.Text = Convert.ToString(lstContactBE.HelpDeskText);
                hdnContactId.Value = Convert.ToString(lstContactBE.ContactId);
            }
            else
            {
                txtHelpDeskEmail.Text = "";
                txtHelpDeskPhone.Text = "";
                txtHelpDeskContactPerson.Text = "";
                txtHelpDeskText.Text = "";
                hdnContactId.Value = "";
            }
        }
        catch(Exception ex)
        {
            Exceptions.WriteExceptionLog(ex); 
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 12-08-15
    /// Scope   : it populates language dropdown
    /// </summary>
    private void PopulateLanguageDropDownList()
    {
        List<LanguageBE> languages = new List<LanguageBE>();
        languages = LanguageBL.GetAllLanguageDetails(true);

        if (languages != null)
        {
            if (languages.Count() > 0)
            {
                ddlLanguage.DataSource = languages;
                ddlLanguage.DataTextField = "LanguageName";
                ddlLanguage.DataValueField = "LanguageId";
                ddlLanguage.DataBind();
            }
        }

    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 12-08-15
    /// Scope   : ddlLanguage_SelectedIndexChanged
    /// </summary>
    protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ReadContactInfo();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Save();
    }

    protected void Save()
    {
        try
        {
            UserBE lst = Session["StoreUser"] as UserBE;

            if (Convert.ToString(hdnContactId.Value) == "")
            {
                objContactBE.Action = Convert.ToInt16(DBAction.Insert);
                objContactBE.ContactId = 0;// not exists
            }
            else
            {
                objContactBE.Action = Convert.ToInt16(DBAction.Update);
                objContactBE.ContactId = Convert.ToInt16(hdnContactId.Value);
            }
            objContactBE.ContactLanguageId = Convert.ToInt16(ddlLanguage.SelectedValue);
            objContactBE.HelpDeskEmail = Convert.ToString(txtHelpDeskEmail.Text);
            objContactBE.HelpDeskPhone = Convert.ToString(txtHelpDeskPhone.Text);
            objContactBE.HelpDeskContactPerson = Convert.ToString(txtHelpDeskContactPerson.Text);
            objContactBE.HelpDeskText = Convert.ToString(txtHelpDeskText.Text);
            objContactBE.ModifiedBy = lst.UserId;
            bool res = StoreBL.InsertUpdateContactInfo(objContactBE);
            if (res)
            {
                CreateActivityLog("Contact Us", "Updated", "");
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Contacts are Updated", AlertType.Success);
            }
            else
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, "Contacts Update Failed", AlertType.Success);
            }
        }
        catch(Exception ex)
        {
            Exceptions.WriteExceptionLog(ex); 
        }
    }

}
}