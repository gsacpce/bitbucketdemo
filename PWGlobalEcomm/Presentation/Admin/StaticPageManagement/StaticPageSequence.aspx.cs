﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PWGlobalEcomm.BusinessLogic;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

namespace Presentation
{
    public class Admin_StaticPageSequenceAjax : System.Web.UI.Page
    {
        List<StaticPageManagementBE> StaticPages;
        List<StaticPageManagementBE> SubStaticPages;
        protected global::System.Web.UI.WebControls.Repeater rptPStaticPage;

        private string host = GlobalFunctions.GetVirtualPath();
        
        


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["method"] != null)
            {
                if (Request.QueryString["method"] == "savesequence")
                {
                    string SequenceData = Request.Form["W"].ToString();
                    string DisplayLocation = Request.Form["V"].ToString();
                    SaveSequenceData(SequenceData, DisplayLocation);
                }
                
            }
            PopulateSequence();
        }

        //*************************************************************************
        // Purpose : It will populate the grid data.
        // Inputs  : Nothing
        // Return  : Nothing
        //*************************************************************************
        protected void PopulateSequence()
        {
            try
            {
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                StaticPages = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "All");
                if (StaticPages.Count() > 0)
                {
                    //StaticPages = StaticPages.OrderBy(c => c.DisplayOrder).ToList();
                    rptPStaticPage.DataSource = StaticPages.Where(c => c.ParentStaticPageId == 0 && c.IsActive.Equals(true) ).ToList();
                    rptPStaticPage.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        #region Website

        protected void Btn_Command(object sender, CommandEventArgs e)
        {
            switch ((e.CommandName))
            {
                case "addNew" : Response.Redirect(host + "Admin/StaticPageManagement/StaticPageManagement.aspx"); 
                    break;
                case "cancel": Response.Redirect(host + "Admin/StaticPageManagement/StaticPageListing.aspx");
                    break;
                    
            }

        }


        protected void rptPStaticPage_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl liPC = e.Item.FindControl("liPC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                System.Web.UI.HtmlControls.HtmlInputRadioButton rbtnHeader = e.Item.FindControl("rbtnHeader") as System.Web.UI.HtmlControls.HtmlInputRadioButton;
                System.Web.UI.HtmlControls.HtmlInputRadioButton rbtnFooter = e.Item.FindControl("rbtnFooter") as System.Web.UI.HtmlControls.HtmlInputRadioButton;

                if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DisplayLocation")).ToLower() == "header")
                {
                    rbtnHeader.Checked = true;
                    rbtnFooter.Checked = false;
                }
                else if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DisplayLocation")).ToLower() == "footer")
                {
                    rbtnFooter.Checked = true;
                    rbtnHeader.Checked = false;
                    
                }
                    
                Repeater rptSStaticPage = e.Item.FindControl("rptSStaticPage") as Repeater;

                liPC.Attributes.Add("class", "StaticPage");
                liPC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                //liPC.Attributes.Add("parentstaticpageid", DataBinder.Eval(e.Item.DataItem, "parentstaticpageid").ToString());
                int DisplayOrder = e.Item.ItemIndex + 1;
                
               
               // liPC.Attributes.Add("displayorder", DisplayOrder.ToString());
                SubStaticPages = new List<StaticPageManagementBE>();
                //SubCategories = Categories.Where((c => c.ParentStaticPageId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "StaticPageId")) && (c.SubStaticPageId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "StaticPageId"))))).ToList();
                SubStaticPages = StaticPages.Where(c => c.ParentStaticPageId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "StaticPageId")) && Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "ParentStaticPageId")) == 0).ToList();
               // SubStaticPages = SubStaticPages.OrderBy(c => c.DisplayOrder).ToList();
               
                rptSStaticPage.DataSource = SubStaticPages;
               
                rptSStaticPage.DataBind();
            }
        }

        protected void rptSStaticPage_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl liSC = e.Item.FindControl("liSC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                Repeater rptSSStaticPage = e.Item.FindControl("rptSSStaticPage") as Repeater;

                if (rptSSStaticPage != null)
                {
                    // liSC.Attributes.Add("displayorder", DisplayOrder.ToString());
                    
                    List<StaticPageManagementBE> SubSubStaticPages = new List<StaticPageManagementBE>();

                    SubSubStaticPages = StaticPages.Where(c => c.ParentStaticPageId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "StaticPageId"))).ToList();

                  //  SubSubStaticPages = SubSubStaticPages.OrderBy(c => c.DisplayOrder).ToList();
                    rptSSStaticPage.DataSource = SubSubStaticPages;
                    rptSSStaticPage.DataBind();
                }   
                if (liSC != null)
                {
                    //liSC.Attributes.Add("class", "cat" + DataBinder.Eval(e.Item.DataItem, "ParentStaticPageId").ToString() + " subcat" + DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString() + " subStaticPage");
                    liSC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                    liSC.Attributes.Add("class", "StaticPage");
                  //  liSC.Attributes.Add("parentstaticpageid", DataBinder.Eval(e.Item.DataItem, "parentstaticpageid").ToString());
                }
               
                
               // int DisplayOrder = e.Item.ItemIndex + 1;
              
              
            }
        }

        protected void rptSSStaticPage_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                System.Web.UI.HtmlControls.HtmlGenericControl liSSC = e.Item.FindControl("liSSC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                liSSC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                liSSC.Attributes.Add("class", "StaticPage");
                
                
            }
        }


        public void SaveSequenceData(string SequenceData, string DisplayLocation)
        {
            DataTable dtDisplayOrder = GetDataIntoTable(SequenceData);
            dtDisplayOrder.Columns.Remove("DisplayLocation");
            DataTable dtDisplayLocation = GetDataIntoTable(DisplayLocation);
            dtDisplayLocation.Columns.Remove("DisplayOrder");
            bool IsSequenceSet = StaticPageManagementBL.SetStaticPageSequence(dtDisplayOrder, dtDisplayLocation);

            //Response.Clear();
            if (IsSequenceSet)
                Response.Write("1");
            else
                Response.Write("0");
            Response.End();
            //return "Updated Successfully";
        }
        protected DataTable GetDataIntoTable(string vData)
        {
            List<StaticPageManagementSequenceBE> returnList = new List<StaticPageManagementSequenceBE>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            vData = "[" + vData.Replace("[", "").Replace("]", "") + "]";
            if (vData != "[]")
            {
                returnList = serializer.Deserialize<List<StaticPageManagementSequenceBE>>(vData);
            }
            List<StaticPageManagementSequenceBE> sequenceDataList = new List<StaticPageManagementSequenceBE>();



            DataTable dt = GlobalFunctions.To_DataTable(returnList);
            dt.Columns["StaticPageId"].ColumnName = "StaticPageId";
            dt.Columns["DisplayOrder"].ColumnName = "DisplayOrder";
            dt.Columns["DisplayLocation"].ColumnName = "DisplayLocation";
            return dt;

        }    

        #endregion


    }
}
