﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_StaticPageManagement_StaticPageListing : BasePage //System.Web.UI.Page
    {
        int LanguageId;
        string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.Repeater rptPStaticPage;
        protected global::System.Web.UI.WebControls.HiddenField hdnAddEdit;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LanguageId = GlobalFunctions.GetLanguageId();
                if (!IsPostBack)
                {
                    
                    Session["StaticPageType"] = null;
                    Session["StaticPageId"] = null;
                    Session["StaticPageDetail"] = null;
                }
                PopulateGrid();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void PopulateGrid()
        {
            try
            {
                List<StaticPageManagementBE> lst = new List<StaticPageManagementBE>();
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(LanguageId);
                lst = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "All");
                if (lst != null && lst.Count > 0)
                {
                    //lst = lst.Where(x => ( x.IsSystemLink.Equals(true) || ( x.IsSystemLink.Equals(false) && x.IsActive.Equals(true)  ))).ToList();
                    Session["StaticPageDetail"] = lst;
                    List<StaticPageManagementBE> tempLst = new List<StaticPageManagementBE>();
                    for (int i = 0; i < lst.Count; i++)
                    {
                        if (lst[i].ParentStaticPageId == 0)
                        { tempLst.Add(lst[i]); }
                    }
                    if (tempLst.Count > 0)
                    {
                        rptPStaticPage.DataSource = tempLst;
                        rptPStaticPage.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnAddNewStaticPage_Click(object sender, EventArgs e)
        {
            Response.Redirect(Host + "Admin/StaticPageManagement/StaticPageManagement.aspx?Mode=add");
        }

        protected void rptPStaticPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl liPC = e.Item.FindControl("liPC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    Repeater rptSCategory = e.Item.FindControl("rptSStaticPage") as Repeater;
                    Label lblurl = (Label)e.Item.FindControl("lblurl");
                    HtmlTableRow trParent = (HtmlTableRow)e.Item.FindControl("trParent");
                    //HyperLink lnkView = (HyperLink)e.Item.FindControl("lnkView");
                    if (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PageType")) == 2)
                    {
                        if (DataBinder.Eval(e.Item.DataItem, "PageURL").ToString().Contains("#"))
                        {
                            //lnkView.NavigateUrl = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString()) + "/true";
                            lblurl.Text = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString());
                        }
                        else
                        {
                            //lnkView.NavigateUrl = DataBinder.Eval(e.Item.DataItem, "PageURL").ToString();
                            lblurl.Text = DataBinder.Eval(e.Item.DataItem, "PageURL").ToString();
                        }
                    }
                    else
                    {
                        //lnkView.NavigateUrl = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString()) + "/true";
                        lblurl.Text = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString());
                    }

                    LinkButton lnkbtnActive = (LinkButton)e.Item.FindControl("lnkbtnActive");
                    lnkbtnActive.Visible = false;
                    LinkButton lnkbtnEdit = (LinkButton)e.Item.FindControl("lnkbtnEdit");
                    lnkbtnEdit.Visible = true;
                    LinkButton lnkbtnDisable = (LinkButton)e.Item.FindControl("lnkbtnDisable");
                    lnkbtnDisable.Visible = true;
                    LinkButton lnkbtnDisableSystemLink = (LinkButton)e.Item.FindControl("lnkbtnDisableSystemLink");
                    lnkbtnDisableSystemLink.Visible = true;
                    //HyperLink lnkView = (HyperLink)e.Item.FindControl("lnkbtnDisableSystemLink");
                    //lnkView.Visible = true;

                    bool IsActive = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "IsActive"));
                    bool IsSystemLink = Convert.ToBoolean( DataBinder.Eval(e.Item.DataItem,"IsSystemLink") );

                    if ( !IsActive && !IsSystemLink )
                    {
                        //lnkView.Visible = false;
                        lnkbtnActive.Visible = true; 
                        lnkbtnDisable .Visible = false;
                        lnkbtnDisableSystemLink.Visible = false;
                        lnkbtnEdit.Visible = false;

                    }

                    if (IsActive && !IsSystemLink )
                    {
                        
                        lnkbtnActive.Visible = false;
                        lnkbtnDisable.Visible = true;
                        lnkbtnDisableSystemLink.Visible = false;
                        lnkbtnEdit.Visible = true;
                    }

                    if (!IsActive && IsSystemLink)
                    {
                        lnkbtnActive.Visible = true;
                        lnkbtnEdit.Visible = true;
                        lnkbtnDisable.Visible = false;
                        lnkbtnDisableSystemLink.Visible = false;
                    }

                    if (IsActive && IsSystemLink)
                    {
                        lnkbtnActive.Visible = false;
                        lnkbtnDisable.Visible = false;
                        lnkbtnDisableSystemLink.Visible = true;
                        lnkbtnEdit.Visible = true;

                    }



                   
                    liPC.Attributes.Add("class", "1-" + DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                    liPC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                    List<StaticPageManagementBE> lst = (List<StaticPageManagementBE>)Session["StaticPageDetail"];
                    List<StaticPageManagementBE> tempLst = new List<StaticPageManagementBE>();
                    if (lst != null && lst.Count > 0)
                    {
                        for (int i = 0; i < lst.Count; i++)
                        {
                            if (lst[i].ParentStaticPageId.ToString() == DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString())
                            { tempLst.Add(lst[i]); }
                        }
                    }
                    rptSCategory.DataSource = tempLst;
                    rptSCategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptPStaticPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int StaticPageId = Convert.ToInt16(commandArgs[0]);
                string PageName = commandArgs[1];

                if (e.CommandName == "EditStaticPage")
                {
                    Session["StaticPageId"] = StaticPageId; ;
                    hdnAddEdit.Value = Session["StaticPageId"].ToString();
                    Session["StaticPageType"] = "Edit";
                    Response.Redirect(Host + "Admin/StaticPageManagement/StaticPageManagement.aspx");

                }
                else if (e.CommandName == "DisableStaticPage" || e.CommandName == "DisableSystemLink" || e.CommandName == "EnableStaticPage")
                {
                    string type;
                    if (e.CommandName == "DisableStaticPage" || e.CommandName == "DisableSystemLink")
                    { type = "0"; }
                    else { type = "1"; }
                    EnableDisableStaticPage(type, StaticPageId, PageName);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSStaticPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl liSC = e.Item.FindControl("liSC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    Repeater rptSSCategory = e.Item.FindControl("rptSSStaticPage") as Repeater;
                    Label lblurl = (Label)e.Item.FindControl("lblsurl");
                    HtmlTableRow trSubchild = (HtmlTableRow)e.Item.FindControl("trSubchild");
                    //HyperLink lnkView = (HyperLink)e.Item.FindControl("lnkView");
                    if (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PageType")) == 2)
                    {
                        if (DataBinder.Eval(e.Item.DataItem, "PageURL").ToString().Contains("#"))
                        {
                            //lnkView.NavigateUrl = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString()) + "/true";
                            lblurl.Text = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString());
                        }
                        else
                        {
                            //lnkView.NavigateUrl = DataBinder.Eval(e.Item.DataItem, "PageURL").ToString();
                            lblurl.Text = DataBinder.Eval(e.Item.DataItem, "PageURL").ToString();
                        }
                    }
                    else
                    {
                        //lnkView.NavigateUrl = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString());
                        lblurl.Text = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString());
                    }

                    LinkButton lnkbtnActive = (LinkButton)e.Item.FindControl("lnkbtnActive");
                    lnkbtnActive.Visible = false;
                    LinkButton lnkbtnEdit = (LinkButton)e.Item.FindControl("lnkbtnEdit");
                    lnkbtnEdit.Visible = true;
                    LinkButton lnkbtnDisable = (LinkButton)e.Item.FindControl("lnkbtnDisable");
                    lnkbtnDisable.Visible = true;
                    LinkButton lnkbtnDisableSystemLink = (LinkButton)e.Item.FindControl("lnkbtnDisableSystemLink");
                    lnkbtnDisableSystemLink.Visible = true;
                    //HyperLink lnkView = (HyperLink)e.Item.FindControl("lnkbtnDisableSystemLink");
                    //lnkView.Visible = true;

                    bool IsActive = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "IsActive"));
                    bool IsSystemLink = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "IsSystemLink"));

                    if (!IsActive && !IsSystemLink)
                    {
                        //lnkView.Visible = false;
                        lnkbtnActive.Visible = true;
                        lnkbtnDisable.Visible = false;
                        lnkbtnDisableSystemLink.Visible = false;
                        lnkbtnEdit.Visible = false;

                    }

                    if (IsActive && !IsSystemLink)
                    {

                        lnkbtnActive.Visible = false;
                        lnkbtnDisable.Visible = true;
                        lnkbtnDisableSystemLink.Visible = false;
                        lnkbtnEdit.Visible = true;
                    }

                    if (!IsActive && IsSystemLink)
                    {
                        lnkbtnActive.Visible = true;
                        lnkbtnEdit.Visible = true;
                        lnkbtnDisable.Visible = false;
                        lnkbtnDisableSystemLink.Visible = false;
                        
                    }

                    if (IsActive && IsSystemLink)
                    {
                        lnkbtnActive.Visible = false;
                        lnkbtnDisable.Visible = false;
                        lnkbtnDisableSystemLink.Visible = true;
                        lnkbtnEdit.Visible = true;

                    }

                    liSC.Attributes.Add("class", "2-" + DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                    liSC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                    List<StaticPageManagementBE> lst = (List<StaticPageManagementBE>)Session["StaticPageDetail"];
                    List<StaticPageManagementBE> tempLst = new List<StaticPageManagementBE>();
                    if (lst != null && lst.Count > 0)
                    {
                        for (int i = 0; i < lst.Count; i++)
                        {
                            if (lst[i].ParentStaticPageId.ToString() == DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString())
                            { tempLst.Add(lst[i]); }
                        }
                    }
                    rptSSCategory.DataSource = tempLst;
                    rptSSCategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSStaticPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int StaticPageId = Convert.ToInt16(commandArgs[0]);
                string PageName = commandArgs[1];

                if (e.CommandName == "EditStaticPage")
                {
                    Session["StaticPageId"] = Convert.ToInt16(commandArgs[0]);
                    hdnAddEdit.Value = Session["StaticPageId"].ToString();
                    Session["StaticPageType"] = "Edit";
                    Response.Redirect(Host + "Admin/StaticPageManagement/StaticPageManagement.aspx");

                }
                else if (e.CommandName == "DisableStaticPage" || e.CommandName == "DisableSystemLink" || e.CommandName == "EnableStaticPage")
                {
                    string type;
                    if (e.CommandName == "DisableStaticPage" || e.CommandName == "DisableSystemLink")
                    { type = "0"; }
                    else { type = "1"; }
                    EnableDisableStaticPage(type, StaticPageId, PageName);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSSStaticPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl liSSC = e.Item.FindControl("liSSC") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    Label lblurl = (Label)e.Item.FindControl("lblssurl");
                    HtmlTableRow trSubSubchild = (HtmlTableRow)e.Item.FindControl("trSubSubchild");
                    HyperLink lnkView = (HyperLink)e.Item.FindControl("lnkView");
                    if (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PageType")) == 2)
                    {
                        if (DataBinder.Eval(e.Item.DataItem, "PageURL").ToString().Contains("#"))
                        {
                            lnkView.NavigateUrl = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString()) + "/true";
                            lblurl.Text = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString());
                        }
                        else
                        {
                            lnkView.NavigateUrl = DataBinder.Eval(e.Item.DataItem, "PageURL").ToString();
                            lblurl.Text = DataBinder.Eval(e.Item.DataItem, "PageURL").ToString();
                        }
                    }
                    else
                    {
                        lnkView.NavigateUrl = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString());
                        lblurl.Text = GetUrl(DataBinder.Eval(e.Item.DataItem, "PageURL").ToString());
                    }
                    if (!Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "IsActive").ToString()))
                    {
                        LinkButton lnkbtnActive = (LinkButton)e.Item.FindControl("lnkbtnActive");
                        lnkbtnActive.Visible = true;
                        LinkButton lnkbtnEdit = (LinkButton)e.Item.FindControl("lnkbtnEdit");
                        lnkbtnEdit.Visible = false;
                        lnkView.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtnDisable = (LinkButton)e.Item.FindControl("lnkbtnDisable");
                        lnkbtnDisable.Visible = true;
                        LinkButton lnkbtnEdit = (LinkButton)e.Item.FindControl("lnkbtnEdit");
                        lnkbtnEdit.Visible = true;
                        lnkView.Visible = true;
                    }
                    liSSC.Attributes.Add("class", "3-" + DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                    liSSC.Attributes.Add("rel", DataBinder.Eval(e.Item.DataItem, "StaticPageId").ToString());
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSSStaticPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditStaticPage")
                {
                    Session["StaticPageId"] = Convert.ToInt32(e.CommandArgument);
                    hdnAddEdit.Value = Session["StaticPageId"].ToString();
                    Session["StaticPageType"] = "Edit";
                    Response.Redirect(Host + "Admin/StaticPageManagement/StaticPageManagement.aspx");

                }
                else if (e.CommandName == "DisableStaticPage" || e.CommandName == "EnableStaticPage")
                {
                    string type;
                    if (e.CommandName == "DisableStaticPage")
                    { type = "0"; }
                    else { type = "1"; }
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int StaticPageId = Convert.ToInt16(commandArgs[0]);
                    string PageName = commandArgs[1];
                    EnableDisableStaticPage(type, StaticPageId, PageName);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void EnableDisableStaticPage(string Type, int StaticPageID, string PageName)
        {
            try
            {
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(LanguageId);
                objBE.StaticPageId = Convert.ToInt16(StaticPageID);
                objBE.IsActive = Convert.ToBoolean(Convert.ToInt32(Type));
                int i = StaticPageManagementBL.ExecutePageDetails(Constants.usp_ExecuteStaticPageDetails, objBE, "ACTDEC");
                PopulateGrid();
                if (Type == "0")
                {
                    CreateActivityLog("Static Page Listing", "Disabled", PageName);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticPageDisable"), AlertType.Success); 
                }
                else 
                {
                    CreateActivityLog("Static Page Listing", "Enabled", PageName);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticPageEnable"), AlertType.Success); 
                }
            }
            catch { }
        }

        public string GetUrl(string Value)
        {
            string Url = "";
            //Url = Host.TrimEnd('/') + Value;
            Url = Host + "" + Value;
            return Url;
        }
    }
}