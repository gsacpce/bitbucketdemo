﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.BusinessEntity;
using System.Web.Services;
using System.Web.Script.Services;

namespace Presentation
{
    public partial class Admin_StaticPageManagement_UploadCatalogueImages : BasePage//System.Web.UI.Page
    {
        public string Host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.FileUpload fuMultiple;
        protected global::System.Web.UI.WebControls.HiddenField hdnImageName;
        AddCatalogueImageBE ObjCatalogueImageBE = new AddCatalogueImageBE();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string ImageFolderPath = "";
                ImageFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "Admin\\Images\\CatalogueImages\\";
                if (!Directory.Exists(ImageFolderPath))
                    Directory.CreateDirectory(ImageFolderPath);
                //ClientScript.RegisterStartupScript(this.GetType(), "#dvLoader1", "<script>ShowLoader();</script>");
                bool Result = false;
                //foreach (HttpPostedFile postedFile in fuMultiple.PostedFiles)
                //{
                //    string contentType = System.IO.Path.GetExtension(postedFile.FileName);
                //    if (contentType != ".jpg" && contentType != ".png" && contentType != ".jpeg" && contentType != ".svg")
                //    {
                //        GlobalFunctions.ShowModalAlertMessages(this, "Please Upload Only JPG JPEG PNG SVG Files Only", AlertType.Warning);
                //    }
                //    else
                //    {
                //        fuMultiple.SaveAs(GlobalFunctions.GetPhysicalFolderPathAdmin() + "Images\\CatalogueImages\\" + postedFile.FileName);
                //    }
                //}
                HttpFileCollection hfc = Request.Files;
                int iUploadedCnt = 0;
                int iFailedCnt = 0;
                string Physicalpath = GlobalFunctions.GetPhysicalFolderPath() + "Admin\\Images\\CatalogueImages\\";
                if (!System.IO.File.Exists(Physicalpath))
                {
                    System.IO.Directory.CreateDirectory(Physicalpath);
                }
                for (int i = 0; i <= hfc.Count - 1; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    string MIMEType = System.IO.Path.GetExtension(hpf.FileName);
                    if (MIMEType == ".png" || MIMEType == ".jpg" || MIMEType == ".jpeg" || MIMEType == ".gif" || MIMEType == ".svg")
                    {
                        if (hpf.ContentLength > 0)
                        {

                            DirectoryInfo objDir = new DirectoryInfo(Physicalpath);
                            string sFileName = Path.GetFileName(hpf.FileName);
                            string sFileExt = Path.GetExtension(hpf.FileName);
                            FileInfo[] objFI = objDir.GetFiles(sFileName);
                            string FileExtension = Convert.ToString(Path.GetExtension(hpf.FileName).Replace(".", ""));
                            hpf.SaveAs(Physicalpath + Path.GetFileName(hpf.FileName));
                            iUploadedCnt += 1;
                            ObjCatalogueImageBE.ImageExtension = FileExtension;
                            ObjCatalogueImageBE.ImageName = hpf.FileName;
                            ObjCatalogueImageBE.URL = GlobalFunctions.GetVirtualPath() + @"Admin/Images/CatalogueImages/" + Path.GetFileName(hpf.FileName);
                            Result = CatalogueImageBL.InsertCatalogueImage(ObjCatalogueImageBE);
                        }
                    }
                }
                ClientScript.RegisterStartupScript(this.GetType(), "#dvLoader1", "<script>HideLoader();</script>");
                if (Result == true)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Images are uploaded successfully", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Can't Upload Images", AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}