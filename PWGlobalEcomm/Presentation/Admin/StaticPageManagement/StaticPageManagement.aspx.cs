﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using CKEditor.NET;
using System.Text.RegularExpressions;
using Microsoft.Security.Application;

namespace Presentation
{
    public class Admin_StaticPageManagement_StaticPageManagement : System.Web.UI.Page
    {
        int UserId;
        int StaticId;
        string StaticType;
        public string Host = GlobalFunctions.GetVirtualPath();
        //protected global::CKEditor.NET.CKEditorControl txtWebSiteContent;
        protected global::System.Web.UI.WebControls.TextBox txtUserName, txtPassword, txtCaptcha;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage, ddlParentPage, ddlSubParentPage, ddlSubSubParentPage;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvParentPage, dvLang, dvContent, dvSEO, dvSubPageContent, dvNavigation, dvSubSubParentPage, dvSubParentPage, divPageTypes, divLinkTypes;
        protected global::System.Web.UI.WebControls.TextBox txtPageURL, txtPageName, txtPageTitle, txtMetaKeywords, txtMetaDescription, txtWebSiteContent;
        protected global::System.Web.UI.WebControls.CheckBox chkShowPage, chkLogin, chkIsParent, chkIsSystemLink;
        protected global::System.Web.UI.WebControls.RadioButtonList rbtnPageType, rbtnShowPageIn;
        protected global::System.Web.UI.WebControls.Literal ltrPageHeading;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    divPageTypes.Visible = true;
                    if (Session["StoreUser"] != null)
                    {
                        UserBE objUserBE = new UserBE();
                        objUserBE = Session["StoreUser"] as UserBE;
                        UserId = objUserBE.UserId;
                    }
                    BindLanguage();
                    BindStaticPageDropdown(0);


                    if (Convert.ToString(Request.QueryString["Mode"]) != null)
                    {
                        Session["StaticPageType"] = Request.QueryString["Mode"];
                        Session["StaticPageId"] = null;
                    }
                    else
                    {
                        Session["StaticPageType"] = "edit";
                    }

                    if (Convert.ToString(Session["StaticPageType"]) == "edit")
                    {
                        EditStaticPage(Convert.ToInt16(Session["StaticPageId"]));
                    }
                    else
                    {
                        ltrPageHeading.Text = "Add Static Page / Link";
                        ClearControls();
                    }
                }
                Page.Title = "Static Pages Management";
                Page.MetaDescription = "Add/Edit Static Pages Management";
                Page.MetaKeywords = "add static page, edit static page, static page listing";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindStaticPageDropdown(int staticPageId = 0)
        {
            try
            {
                Session["StaticPageList"] = null;
                List<StaticPageManagementBE> lst = new List<StaticPageManagementBE>();
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                lst = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "ParentPages");
                if (lst != null && lst.Count > 0)
                {
                    var source = new Dictionary<int, string>();
                    for (int i = 0; i < lst.Count; i++)
                    {
                        if (lst[i].ParentStaticPageId == 0)
                        { source.Add(Convert.ToInt32(lst[i].StaticPageId), Convert.ToString(lst[i].PageName)); }
                    }
                    ddlParentPage.DataSource = source;
                    ddlParentPage.DataTextField = "value";
                    ddlParentPage.DataValueField = "key";
                    ddlParentPage.DataBind();
                    ddlParentPage.Items.Insert(0, "-- Select Parent Page--");
                    Session["StaticPageList"] = lst;
                }
                else
                {
                    ddlParentPage.Items.Insert(0, "-- Select Parent Page--");
                    dvParentPage.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindLanguage()
        {
            try
            {
                List<LanguageBE> lst = new List<LanguageBE>();
                lst = LanguageBL.GetAllLanguageDetails(true);
                if (lst != null && lst.Count > 0)
                {
                    ddlLanguage.Items.Clear();
                    for (int i = 0; i < lst.Count; i++)
                    { ddlLanguage.Items.Insert(i, new ListItem(lst[i].LanguageName, lst[i].LanguageId.ToString())); }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                StaticId = Convert.ToUInt16(Session["StaticPageId"]);
                EditStaticPage(StaticId);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private bool IsUrlValid(string url)
        {
            try
            {
                string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
                Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                return reg.IsMatch(url);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex); return false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int Result = 0;
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                #region Set Data for Save
                StaticType = Convert.ToString(Session["StaticPageType"]);
                StaticId = Convert.ToUInt16(Session["StaticPageId"]);
                if (Sanitizer.GetSafeHtmlFragment(txtPageName.Text.Trim()) == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageEmpty"), AlertType.Warning);
                    return;
                }
                int pId = 0;
                int IsLoginBased = 0; int NewWindow = 0; int isParent = 0;
                int PageType = 1;
                // this can be either 1 = (PageType 1) or 2 =(PageType 2)
                if (chkIsSystemLink.Checked)
                {
                    PageType = 1; // this will only page type 1 
                    #region System Link

                    #region assign parent static page id
                    if (chkShowPage.Checked)
                    {
                        if (ddlParentPage.SelectedIndex != 0)
                        {
                            if (ddlSubParentPage.SelectedIndex != 0 && ddlSubParentPage.SelectedIndex != -1 && dvSubParentPage.Visible)
                            {
                                if (ddlSubSubParentPage.SelectedIndex != 0 && ddlSubSubParentPage.SelectedIndex != -1 && dvSubSubParentPage.Visible)
                                {
                                    pId = Convert.ToInt16(ddlSubSubParentPage.SelectedValue);
                                }
                                else
                                {
                                    pId = Convert.ToInt16(ddlSubParentPage.SelectedValue);
                                }
                            }
                            else
                            {
                                pId = Convert.ToInt16(ddlParentPage.SelectedValue);
                            }
                        }
                    }
                    else { pId = 0; }
                    #endregion
                    #region set parent or child
                    if (chkIsParent.Checked)
                    { isParent = 0; }
                    else
                    { isParent = 0; }
                    #endregion
                    if (chkLogin.Checked) { IsLoginBased = 1; }
                    if (rbtnShowPageIn.Items[1].Selected) { NewWindow = 1; } else { NewWindow = 0; }
                    #endregion
                }
                else
                {
                    #region Non System Link

                    if (rbtnPageType.Items[0].Selected)
                    {
                        #region PageType1
                        // first radio button
                        if (!chkIsParent.Checked)
                        {
                            if (Sanitizer.GetSafeHtmlFragment(txtWebSiteContent.Text.Trim()) == "")
                            {
                                chkIsParent_CheckedChanged(null, null);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageContent"), AlertType.Warning);
                                return;
                            }
                        }
                        // here auto url will be there for page type1
                        #endregion
                        PageType = 1;
                    }
                    else
                    {
                        #region PageType2
                        if (!chkIsParent.Checked)
                        {
                            if (Request.Form[txtPageURL.UniqueID] == "")
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageURL"), AlertType.Warning);
                                return;
                            }
                        }
                        else
                        { txtPageURL.Text = "#"; }
                        #endregion
                        PageType = 2;
                    }
                    #region Assign Parent static page id
                    if (!chkIsParent.Checked)
                    {
                        if (chkShowPage.Checked)
                        {
                            if (ddlParentPage.SelectedIndex == 0)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageParentPage"), AlertType.Warning);
                                return;
                            }
                            else
                            {
                                if (ddlParentPage.SelectedIndex != 0)
                                {
                                    if (ddlSubParentPage.SelectedIndex != 0 && ddlSubParentPage.SelectedIndex != -1 && dvSubParentPage.Visible)
                                    {
                                        if (ddlSubSubParentPage.SelectedIndex != 0 && ddlSubSubParentPage.SelectedIndex != -1 && dvSubSubParentPage.Visible)
                                        { pId = Convert.ToInt16(ddlSubSubParentPage.SelectedValue); }
                                        else
                                        { pId = Convert.ToInt16(ddlSubParentPage.SelectedValue); }
                                    }
                                    else { pId = Convert.ToInt16(ddlParentPage.SelectedValue); }
                                }
                                else { pId = 0; }
                            }
                        }
                        else { pId = 0; }
                    }
                    #endregion
                    #region set parent or child
                    if (chkIsParent.Checked)
                    { isParent = 1; }
                    else
                    { isParent = 0; }
                    #endregion
                    if (chkLogin.Checked) { IsLoginBased = 1; }
                    if (rbtnShowPageIn.Items[1].Selected) { NewWindow = 1; } else { NewWindow = 0; }

                    #endregion
                }

                StaticPageManagementBE objBEs = new StaticPageManagementBE();
                objBEs.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                objBEs.PageURL = Convert.ToString(Request.Form[txtPageURL.UniqueID]);
                objBEs.NewWindow = Convert.ToInt16(NewWindow);
                objBEs.ParentStaticPageId = Convert.ToInt16(pId);
                objBEs.IsLoginBased = Convert.ToBoolean(IsLoginBased);
                objBEs.IsParent = Convert.ToBoolean(isParent);
                objBEs.PageType = Convert.ToInt16(PageType);
                objBEs.IsActive = true;
                objBEs.CreatedBy = Convert.ToInt16(UserId);
                objBEs.WebsiteContent = txtWebSiteContent.Text;
                objBEs.PageName = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtPageName.Text.Trim()));
                objBEs.PageTitle = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtPageTitle.Text.Trim()));
                objBEs.MetaKeyword = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtMetaKeywords.Text.Trim()));
                objBEs.MetaDescription = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtMetaDescription.Text.Trim()));
                objBEs.StaticPageId = Convert.ToInt16(StaticId);
                #endregion



                if (Session["StaticPageType"] != null || Convert.ToString(Session["StaticPageType"]) != "")
                {
                    if (Session["StaticPageId"] != null)
                    {
                        if (Session["StaticPageId"] != null && Session["StaticPageType"] == "edit")
                        {
                            int StaticPageId = StaticPageManagementBL.InsertUpdatePageDetails(Constants.usp_ExecuteStaticPageDetails, objBEs, "Update");
                            Result = 1;
                        }
                        else
                        {
                            int StaticPageId = StaticPageManagementBL.InsertUpdatePageDetails(Constants.usp_ExecuteStaticPageDetails, objBEs, "Insert");
                            if (StaticPageId == 0)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                return;
                            }
                            Result = 2;
                            Session["StaticPageId"] = StaticPageId;
                            Session["StaticPageName"] = objBEs.PageName;
                            Session["StaticPageType"] = "edit";
                        }
                    }
                    else
                    {
                        if (Convert.ToString(Session["StaticPageName"]).ToLower() != Convert.ToString(objBEs.PageName).ToLower())
                        {
                            objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                            objBE.PageName = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtPageName.Text.Trim()));
                            int i = StaticPageManagementBL.CheckPageDetails(Constants.usp_CheckStaticPageExists, objBE, "PageName");
                            if (i == 1)
                            {
                                txtPageName.Text = "";
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageExists"), AlertType.Warning);
                                return;
                            }
                            else
                            {
                                int StaticPageId = StaticPageManagementBL.InsertUpdatePageDetails(Constants.usp_ExecuteStaticPageDetails, objBEs, "Insert");
                                if (StaticPageId == 0)
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                    return;
                                }
                                Result = 1;
                                Session["StaticPageId"] = StaticPageId;
                                Session["StaticPageName"] = objBEs.PageName;
                                Session["StaticPageType"] = "edit";
                            }
                        }
                        else
                        {
                            int StaticPageId = StaticPageManagementBL.InsertUpdatePageDetails(Constants.usp_ExecuteStaticPageDetails, objBEs, "Update");
                            if (StaticPageId == 0)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                return;
                            }
                            Result = 1;
                            Session["StaticPageId"] = StaticPageId;
                            Session["StaticPageName"] = objBEs.PageName;
                            Session["StaticPageType"] = "edit";
                        }
                    }
                }
                if (Result != 0)
                {
                    if (Result == 1)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticPageUpdate"), AlertType.Success);
                    }
                    else if (Result == 2)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticPageInsert"), AlertType.Success);
                    }
                    EditStaticPage(Convert.ToInt16(Session["StaticPageId"]));
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                    return;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSaveAddAnother_Click(object sender, EventArgs e)
        {
            try
            {
                int Result = 0;
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                #region Set Data for Save
                StaticType = Convert.ToString(Session["StaticPageType"]);
                StaticId = Convert.ToUInt16(Session["StaticPageId"]);
                if (Sanitizer.GetSafeHtmlFragment(txtPageName.Text.Trim()) == "")
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageEmpty"), AlertType.Warning);
                    return;
                }
                int pId = 0;
                int IsLoginBased = 0; int NewWindow = 0; int isParent = 0;
                int PageType = 1;
                if (chkIsSystemLink.Checked)
                {
                    PageType = 1;
                    #region System Link

                    #region assign parent static page id
                    if (chkShowPage.Checked)
                    {
                        if (ddlParentPage.SelectedIndex != 0)
                        {
                            if (ddlSubParentPage.SelectedIndex != 0 && ddlSubParentPage.SelectedIndex != -1 && dvSubParentPage.Visible)
                            {
                                if (ddlSubSubParentPage.SelectedIndex != 0 && ddlSubSubParentPage.SelectedIndex != -1 && dvSubSubParentPage.Visible)
                                {
                                    pId = Convert.ToInt16(ddlSubSubParentPage.SelectedValue);
                                }
                                else
                                {
                                    pId = Convert.ToInt16(ddlSubParentPage.SelectedValue);
                                }
                            }
                            else
                            {
                                pId = Convert.ToInt16(ddlParentPage.SelectedValue);
                            }
                        }
                    }
                    else { pId = 0; }
                    #endregion
                    #region set parent or child
                    if (chkIsParent.Checked)
                    { isParent = 0; }
                    else
                    { isParent = 0; }
                    #endregion
                    if (chkLogin.Checked) { IsLoginBased = 1; }
                    if (rbtnShowPageIn.Items[1].Selected) { NewWindow = 1; } else { NewWindow = 0; }
                    #endregion
                }
                else
                {
                    #region Non System Link

                    if (rbtnPageType.Items[0].Selected)
                    {
                        #region PageType1
                        // first radio button
                        if (!chkIsParent.Checked)
                        {
                            if (Sanitizer.GetSafeHtmlFragment(txtWebSiteContent.Text.Trim()) == "")
                            {
                                chkIsParent_CheckedChanged(null, null);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageContent"), AlertType.Warning);
                                return;
                            }
                        }
                        #endregion
                        PageType = 1;
                    }
                    else
                    {
                        #region PageType2
                        if (!chkIsParent.Checked)
                        {
                            if (Request.Form[txtPageURL.UniqueID] == "")
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageURL"), AlertType.Warning);
                                return;
                            }
                        }
                        else
                        { txtPageURL.Text = "#"; }
                        #endregion
                        PageType = 2;
                    }
                    #region Assign Parent static page id
                    if (!chkIsParent.Checked)
                    {
                        if (chkShowPage.Checked)
                        {
                            if (ddlParentPage.SelectedIndex == 0)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageParentPage"), AlertType.Warning);
                                return;
                            }
                            else
                            {
                                if (ddlParentPage.SelectedIndex != 0)
                                {
                                    if (ddlSubParentPage.SelectedIndex != 0 && ddlSubParentPage.SelectedIndex != -1 && dvSubParentPage.Visible)
                                    {
                                        if (ddlSubSubParentPage.SelectedIndex != 0 && ddlSubSubParentPage.SelectedIndex != -1 && dvSubSubParentPage.Visible)
                                        { pId = Convert.ToInt16(ddlSubSubParentPage.SelectedValue); }
                                        else
                                        { pId = Convert.ToInt16(ddlSubParentPage.SelectedValue); }
                                    }
                                    else { pId = Convert.ToInt16(ddlParentPage.SelectedValue); }
                                }
                                else { pId = 0; }
                            }
                        }
                        else { pId = 0; }
                    }
                    #endregion
                    #region set parent or child
                    if (chkIsParent.Checked)
                    { isParent = 1; }
                    else
                    { isParent = 0; }
                    #endregion
                    if (chkLogin.Checked) { IsLoginBased = 1; }
                    if (rbtnShowPageIn.Items[1].Selected) { NewWindow = 1; } else { NewWindow = 0; }

                    #endregion
                }

                StaticPageManagementBE objBEs = new StaticPageManagementBE();
                objBEs.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                objBEs.PageURL = Convert.ToString(Request.Form[txtPageURL.UniqueID]);
                objBEs.NewWindow = Convert.ToInt16(NewWindow);
                objBEs.ParentStaticPageId = Convert.ToInt16(pId);
                objBEs.IsLoginBased = Convert.ToBoolean(IsLoginBased);
                objBEs.IsParent = Convert.ToBoolean(isParent);
                objBEs.PageType = Convert.ToInt16(PageType);
                objBEs.IsActive = true;
                objBEs.CreatedBy = Convert.ToInt16(UserId);
                objBEs.WebsiteContent = txtWebSiteContent.Text;
                objBEs.PageName = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtPageName.Text.Trim()));
                objBEs.PageTitle = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtPageTitle.Text.Trim()));
                objBEs.MetaKeyword = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtMetaKeywords.Text.Trim()));
                objBEs.MetaDescription = Microsoft.Security.Application.Encoder.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtMetaDescription.Text.Trim()));
                objBEs.StaticPageId = Convert.ToInt16(StaticId);
                #endregion

                #region new code

                if (Convert.ToString(Session["StaticPageType"]) != "")
                {

                    if (Session["StaticPageId"] != null && Session["StaticPageType"] == "edit")
                    {
                        int StaticPageId = StaticPageManagementBL.InsertUpdatePageDetails(Constants.usp_ExecuteStaticPageDetails, objBEs, "Update");
                        Result = 2;
                    }
                    else
                    {
                        if (Convert.ToString(Session["StaticPageName"]).ToLower() != Convert.ToString(objBEs.PageName).ToLower())
                        {
                            objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                            objBE.PageName = Convert.ToString(Sanitizer.GetSafeHtmlFragment(txtPageName.Text.Trim()));
                            int i = StaticPageManagementBL.CheckPageDetails(Constants.usp_CheckStaticPageExists, objBE, "PageName");
                            if (i == 1)
                            {
                                txtPageName.Text = "";
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/StaticPageExists"), AlertType.Warning);
                                return;
                            }
                            else
                            {
                                int StaticPageId = StaticPageManagementBL.InsertUpdatePageDetails(Constants.usp_ExecuteStaticPageDetails, objBEs, "Insert");
                                if (StaticPageId == 0)
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                    return;
                                }
                                Result = 1;

                            }
                        }
                        else
                        {
                            int StaticPageId = StaticPageManagementBL.InsertUpdatePageDetails(Constants.usp_ExecuteStaticPageDetails, objBEs, "Update");
                            if (StaticPageId == 0)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                                return;
                            }
                            Result = 1;

                        }
                    }
                }

                #endregion

                if (Result != 0)
                {
                    if (Result == 1)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticPageUpdate"),AlertType.Success);
                    }
                    else if (Result == 2)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/StaticPageInsert"),AlertType.Success);
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                    return;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            finally
            {
                Session["StaticPageId"] = null;
                Session["StaticPageName"] = null;
                ClearControls();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["StaticPageType"] = null;
            Session["StaticPageId"] = null;
            Response.Redirect(Host + "Admin/StaticPageManagement/StaticPageListing.aspx");
        }

        protected void EditStaticPage(int staticPageId)
        {
            try
            {
                dvLang.Visible = true;
                ltrPageHeading.Text = "Edit Static Page / Link";
                BindStaticPageDropdown(staticPageId);
                List<StaticPageManagementBE> lst = new List<StaticPageManagementBE>();
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                objBE.StaticPageId = Convert.ToInt16(staticPageId);
                lst = StaticPageManagementBL.GetSinglePageDetails(Constants.usp_ExecuteStaticPageDetails, objBE, "Select");

                //StaticPageManagementBE currStaticPage = lst.FirstOrDefault(x => x.StaticPageId.Equals(staticPageId));
                StaticPageManagementBE currStaticPage = lst.ToArray<StaticPageManagementBE>()[0];

                if (currStaticPage != null && !currStaticPage.IsSystemLink.Equals(true))
                {
                    divPageTypes.Visible = true;
                    divLinkTypes.Visible = false;


                    if (Convert.ToInt32(currStaticPage.PageType) == 1)
                    {
                        rbtnPageType.Items[0].Selected = true;
                        rbtnPageType_SelectedIndexChanged(new object(), new EventArgs());
                    }
                    else if (Convert.ToInt32(currStaticPage.PageType) == 2)
                    {
                        rbtnPageType.Items[1].Selected = true;
                        rbtnPageType_SelectedIndexChanged(new object(), new EventArgs());
                    }
                    if (Convert.ToInt32(currStaticPage.IsParent) == 1)
                    {
                        chkIsParent.Checked = true;
                        dvNavigation.Visible = false;
                    }
                    txtPageName.Text = Convert.ToString(Server.HtmlDecode(currStaticPage.PageName));
                    txtPageTitle.Text = currStaticPage.PageTitle;
                    txtMetaKeywords.Text = currStaticPage.MetaKeyword;
                    txtMetaDescription.Text = currStaticPage.MetaDescription;
                    txtWebSiteContent.Text = Convert.ToString(Server.HtmlDecode(currStaticPage.WebsiteContent)).Replace("~/", GlobalFunctions.GetVirtualPath());
                    chkLogin.Checked = Convert.ToBoolean(currStaticPage.IsLoginBased);
                    bool b = false;
                    if (currStaticPage.ParentStaticPageId == 0) { b = false; } else { b = true; }
                    chkShowPage.Checked = Convert.ToBoolean(b);
                    if (Convert.ToInt32(currStaticPage.ParentStaticPageId) == 0) { ddlParentPage.SelectedIndex = 0; }
                    else { dvParentPage.Visible = true; ddlParentPage.SelectedValue = Convert.ToString(currStaticPage.ParentStaticPageId); }
                    if (currStaticPage.NewWindow == 1)
                    { rbtnShowPageIn.Items[1].Selected = true; }
                    else if (currStaticPage.NewWindow == 2)
                    { rbtnShowPageIn.Items[2].Selected = true; }
                    else { rbtnShowPageIn.Items[0].Selected = true; }
                    txtPageURL.Text = currStaticPage.PageURL.ToString();
                }

                if (currStaticPage != null && currStaticPage.IsSystemLink.Equals(true))
                {
                    divPageTypes.Visible = false;
                    divLinkTypes.Visible = true;
                    dvNavigation.Visible = false;
                    dvContent.Visible = true;

                    chkIsSystemLink.Checked = true;
                    chkIsSystemLink.Enabled = false;

                    rbtnPageType.Items[0].Selected = true;

                    if (Convert.ToInt32(currStaticPage.ParentStaticPageId) == 0)
                    {
                        chkIsParent.Checked = true;
                    }
                    else
                    {
                        chkShowPage.Checked = true;
                        ddlParentPage.SelectedValue = Convert.ToString(currStaticPage.ParentStaticPageId);
                    }
                    txtPageName.Text = Convert.ToString(Server.HtmlDecode(currStaticPage.PageName));
                    txtPageTitle.Text = currStaticPage.PageTitle;
                    txtPageURL.Text = currStaticPage.PageURL;
                    txtPageTitle.Text = currStaticPage.PageTitle;
                    txtMetaKeywords.Text = currStaticPage.MetaKeyword;
                    txtMetaDescription.Text = currStaticPage.MetaDescription;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rbtnPageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                dvContent.Visible = true;
                if (rbtnPageType.Items[0].Selected)
                {
                    dvSEO.Visible = true;
                    dvSubPageContent.Visible = true;
                    if (chkIsParent.Checked)
                    { dvNavigation.Visible = false; }
                    else
                    { dvNavigation.Visible = true; }
                    txtPageURL.ReadOnly = true;
                }
                else if (rbtnPageType.Items[1].Selected)
                {
                    dvSEO.Visible = false;
                    dvSubPageContent.Visible = false;
                    if (chkIsParent.Checked)
                    { txtPageURL.ReadOnly = true; dvNavigation.Visible = false; }
                    else
                    { txtPageURL.ReadOnly = false; dvNavigation.Visible = true; }
                }
                StaticType = Convert.ToString(Session["StaticPageType"]);
                StaticId = Convert.ToUInt16(Session["StaticPageId"]);
                if (StaticType != "Edit")
                { ClearControls(); }
                else
                {
                    if (rbtnPageType.Items[0].Selected)
                    {
                        if (txtPageName.Text != "")
                        {
                            if (chkIsParent.Checked) { txtPageURL.Text = "#"; }
                            else { txtPageURL.Text = "/info/" + txtPageURL.Text.Trim().Replace(" ", "_"); }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void chkShowPage_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkShowPage.Checked)
                { dvParentPage.Visible = true; }
                else
                { dvParentPage.Visible = false; }
                chkIsParent_CheckedChanged(new object(), new EventArgs());
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ClearControls()
        {
            txtPageName.Text = "";
            txtPageURL.Text = "";
            txtWebSiteContent.Text = "";
            rbtnShowPageIn.Items[0].Selected = false;
            rbtnShowPageIn.Items[1].Selected = false;
            chkLogin.Checked = false;
            chkShowPage.Checked = false;
            ddlParentPage.SelectedIndex = 0;
            txtPageTitle.Text = "";
            txtMetaDescription.Text = "";
            txtMetaKeywords.Text = "";
        }

        protected void chkIsParent_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkIsParent.Checked)
                {
                    dvNavigation.Visible = false;
                    txtPageURL.ReadOnly = true;
                    txtPageURL.Text = "#";
                }
                else
                {
                    if (rbtnPageType.Items[0].Selected)
                    {
                        dvNavigation.Visible = true; txtPageURL.ReadOnly = true;
                        if (txtPageName.Text.Trim() != "")
                        { txtPageURL.Text = "/info/" + txtPageName.Text.Trim().Replace(" ", "_"); }
                        else { txtPageURL.Text = ""; }
                    }
                    else if (rbtnPageType.Items[1].Selected)
                    {
                        dvNavigation.Visible = true; txtPageURL.ReadOnly = false;
                        //txtPageURL.Text = ""; by vikram on 08082016
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void chkIsSystemLink_CheckedChanged(object sender, EventArgs e)
        {

        }
        protected void ddlParentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                List<StaticPageManagementBE> lst = (List<StaticPageManagementBE>)Session["StaticPageList"];
                if (lst == null)
                {
                    Session["StaticPageList"] = null;
                    StaticPageManagementBE objBE = new StaticPageManagementBE();
                    objBE.LanguageId = Convert.ToInt16(ddlLanguage.SelectedItem.Value);
                    lst = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "ParentPages");
                }
                DropDownList btn = sender as DropDownList;
                if (btn.ID == "ddlParentPage")
                {
                    if (lst != null && lst.Count > 0)
                    {
                        var source = new Dictionary<int, string>();
                        for (int i = 0; i < lst.Count; i++)
                        {
                            if (lst[i].ParentStaticPageId.ToString() == ddlParentPage.SelectedValue)
                            { source.Add(Convert.ToInt32(lst[i].StaticPageId), Convert.ToString(lst[i].PageName)); }
                        }
                        if (source.Count > 0)
                        {
                            ddlSubParentPage.DataSource = source;
                            ddlSubParentPage.DataTextField = "value";
                            ddlSubParentPage.DataValueField = "key";
                            ddlSubParentPage.DataBind();
                            ddlSubParentPage.Items.Insert(0, "-- Select Sub Parent Page--");
                            dvSubParentPage.Visible = true;
                        }
                        else
                        { dvSubParentPage.Visible = false; dvSubSubParentPage.Visible = false; }
                    }
                }
                else if (btn.ID == "ddlSubParentPage")
                {
                    if (lst != null && lst.Count > 0)
                    {
                        var source = new Dictionary<int, string>();
                        for (int i = 0; i < lst.Count; i++)
                        {
                            if (lst[i].ParentStaticPageId.ToString() == ddlSubParentPage.SelectedValue)
                            { source.Add(Convert.ToInt32(lst[i].StaticPageId), Convert.ToString(lst[i].PageName)); }
                        }
                        if (source.Count > 0)
                        {
                            ddlSubSubParentPage.DataSource = source;
                            ddlSubSubParentPage.DataTextField = "value";
                            ddlSubSubParentPage.DataValueField = "key";
                            ddlSubSubParentPage.DataBind();
                            ddlSubSubParentPage.Items.Insert(0, "-- Select Sub Sub Parent Page--");
                            dvSubSubParentPage.Visible = true;
                        }
                        else
                        {
                            dvSubSubParentPage.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }
}