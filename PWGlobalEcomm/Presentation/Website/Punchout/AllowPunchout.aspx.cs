﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml;

namespace Presentation
{
    public partial class Punchout_AllowPunchout : System.Web.UI.Page
    {
        bool res = false;
        private int _catalogueID;
        private string _languageID;
        private string _currencySymbol;

        StoreBE objStoreBE;
        UserBE objUser = new UserBE();
        PunchoutMarkerBE objPunchoutMarkerBE = new PunchoutMarkerBE();
        PunchoutDetailsBE objPunchoutDetailsBE = new PunchoutDetailsBE();

        public string REQURL
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            objPunchoutMarkerBE.Marker = "Default Allow PunchoutPageload enter";
            res = PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
            objStoreBE = StoreBL.GetStoreDetails();
            String strCurrCode = string.Empty;
            String redirectURL = "login-page";
            GetQueryString();
            if (!IsPostBack)
            {
                if (Convert.ToString(REQURL) != "")
                {
                    objPunchoutDetailsBE.PunchoutID = Convert.ToInt32(REQURL);
                    objPunchoutDetailsBE = PunchoutBL.GetPunchoutDetailsByID(objPunchoutDetailsBE);

                    if (objPunchoutDetailsBE != null && objPunchoutDetailsBE.PunchoutID > 0)
                    {
                        Session["cXML_ALLOWPUNCHOUT"] = objPunchoutDetailsBE.PunchoutResponseBody;
                    }
                    if (Session["cXML_ALLOWPUNCHOUT"] != null)
                    {
                        string sReturnStream = Convert.ToString(Session["cXML_ALLOWPUNCHOUT"]);
                        Session["cXML_ALLOWPUNCHOUT"] = null;

                        if (objPunchoutDetailsBE != null && objPunchoutDetailsBE.PunchoutID > 0)
                        {
                            String strStatus = objPunchoutDetailsBE.PunchoutResponse;
                            if (strStatus == "200")
                            {
                                XmlDocument oXMLDoc = new XmlDocument();
                                String strEmail = "";
                                String strCurrency = "";
                              
                                oXMLDoc.LoadXml(objPunchoutDetailsBE.PunchoutRequest);
                                try
                                {
                                    strEmail = oXMLDoc.SelectSingleNode("cXML/Request/PunchOutSetupRequest/Contact/Email").LastChild.Value;
                                }
                                catch { }
                                try
                                {
                                    strCurrency = oXMLDoc.SelectSingleNode("cXML/Request/PunchOutSetupRequest/Contact/Currency").LastChild.Value;
                                    strCurrency = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyCode == strCurrency).CurrencyId);
                                    _currencySymbol = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(strCurrency)).CurrencySymbol;
                                    strCurrCode = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(strCurrency)).CurrencyCode;
                                }
                                catch
                                {
                                    strCurrency = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyId);
                                    _currencySymbol = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencySymbol);
                                    strCurrCode = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(strCurrency)).CurrencyCode;
                                }

                                Session.Add("PunchoutCurrency", strCurrency);
                                _catalogueID = objStoreBE.CatalogueId;

                                UserBL oUsersBL = new UserBL();
                                UserBE obUserBE = new UserBE();
                                Int32 lngStoreId = objStoreBE.StoreId;
                                UserBE objUserBE = new UserBE();

                                _catalogueID = Convert.ToInt32(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(strCurrency)).CatalogueId);

                                Dictionary<string, string> param = new Dictionary<string, string>();
                                param.Add("EmailId", strEmail);
                                param.Add("CatalogueId", Convert.ToString(_catalogueID));
                                obUserBE = UserDA.getUserDetails(Constants.USP_GetUserId, param, true);
                                Int32 lngUserId = obUserBE.UserId;

                                //Int32 lngUserId = Convert.ToInt32(oUsersBL.GetUserId(strEmail, _catalogueID));

                                objPunchoutMarkerBE.Marker = "Default AllowPunchoutSession enter";
                                res = PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);

                                obUserBE.LanguageId = Convert.ToInt16(objStoreBE.StoreLanguages.FirstOrDefault(x => x.StoreId == Convert.ToInt16(lngStoreId)).LanguageId);
                                objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, obUserBE);
                                if (objUser != null || objUser.EmailId != null)
                                {
                                    //UpdateBasketSessionProducts(objUser, 'Y');
                                    Session["User"] = objUser;
                                    Session["CaptchaText"] = null;
                                }

                                if (objStoreBE.StoreId == 462)
                                {
                                    String ReturnToFusionURL = "";
                                    ReturnToFusionURL= oXMLDoc.SelectSingleNode("cXML/Request/PunchOutSetupRequest/BrowserFormPost/URL").LastChild.Value;
                                    Session.Add("S_ReturnToFusionURL",ReturnToFusionURL);
                                }

                                Session.Add("S_ISPUNCHOUT", true);
                                Session.Add("S_PUNCHOUTREQUEST", objPunchoutDetailsBE.PunchoutRequest);
                                Session.Add("S_USERID", lngUserId);
                                Session.Add("S_USEREMAIL", strEmail);
                                Session.Add("S_USERSTOREID", lngStoreId);
                                Session.Add("S_LOGGEDINSTOREID", lngStoreId);
                                Session.Add("S_USERCatalogueId", _catalogueID);
                                Session.Add("catalogueId", _catalogueID);
                                Session.Add("S_USERCurrencySymbol", _currencySymbol);
                                _languageID = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).LanguageId);
                                Session.Add("LanguageId", _languageID);
                                Session.Add("CurrencySymbol", _currencySymbol);
                                Session.Add("CurrencyCode", strCurrCode);

                                objPunchoutMarkerBE.Marker = "Default AllowPunchoutSession End";
                                res = PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);

                                //Added By Hardik "for stiing active currency in punchout and in dropdown"
                                GlobalFunctions.SetCurrencyId(Convert.ToInt16(strCurrency)); 

                                if (lngStoreId != -1)
                                {
                                    if (obUserBE != null)
                                    {
                                        redirectURL = "index";
                                    }
                                }
                            }
                        }
                    }
                }

                objPunchoutMarkerBE.Marker = "Default Allow Punchout pageload End";
                res = PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
                Response.RedirectToRoute(redirectURL);
            }
        }

        private void GetQueryString()
        {
            // Check for encrypted query string
            REQURL = Convert.ToString(Request.QueryString["reqUrl"]);
            string encryptedQueryString = Request.QueryString["request"];
            string decryptedQueryString = string.Empty;
            string cryptoKey = string.Empty;
            if (!string.IsNullOrEmpty(encryptedQueryString))
            {
                // Decrypt query strings
                cryptoKey = Convert.ToString(ConfigurationManager.AppSettings["CryptoKey"]);
                decryptedQueryString = GlobalFunctions.DecryptQueryStrings(Convert.ToString(Request.QueryString["request"]), cryptoKey);

            }
            string[] strArr = decryptedQueryString.Split('&');
            string[] KeyValue = null;
            for (int iArr = 0; iArr < strArr.Length; iArr++)
            {
                KeyValue = strArr[iArr].Split('=');
                if (strArr[iArr].IndexOf('=') > 0)
                {
                    if (Convert.ToString(KeyValue[0]).ToUpper() == "REQURL")
                    {
                        REQURL = Convert.ToString(KeyValue[1]);
                    }
                }
            }
        }
    }
}