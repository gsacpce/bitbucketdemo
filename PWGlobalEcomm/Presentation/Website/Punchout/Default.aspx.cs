﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml;

namespace Presentation
{
    public partial class Punchout_Default : System.Web.UI.Page
    {
        PunchoutMarkerBE objPunchoutMarkerBE = new PunchoutMarkerBE();
        bool res = false;
        string strStatusCode = "";
        string strStatusText = "";
        string strPayloadID = "";
        string strBuyerCookie = "";
        string strResponseURL = string.Empty;
        XmlDocument oXMLPOSR = new XmlDocument();
        String strEmail = "";
        String strFullName;
        String strCurrency = "";
        String strPassedSecret;
        Boolean bValidOperation;
        String strDUNSCredential;
        String strDUNS;
        String strCatalogID;
        String strSupplierID;
        String strUniqueName;
        private int _catalogueID;
        StoreBE objStoreBE;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    objPunchoutMarkerBE.Marker = "Default Page Load";
                    res = PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
                    SetupRequest();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public void SetupRequest()
        {
            objPunchoutMarkerBE.Marker = "Default Setuprequest enter";
            res = false;
            res = PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
            //WriteLog("Punchout_SetupRequest_start");

            objStoreBE = StoreBL.GetStoreDetails();
            Boolean flag = objStoreBE.IsPunchOut;
            //WriteLog("Punchout_SetupRequest_Flag = " + flag + "\r\n");

            if (flag)
            {
                String cXMLSetupRequest = ReturnStream();
                cXMLSetupRequest = TrimXmlString(cXMLSetupRequest);
                //WriteLog("cXMLSetupRequest = " + cXMLSetupRequest + "\r\n");

                cXMLSetupRequest = cXMLSetupRequest.Replace("cXML-urlencoded=", "");
                cXMLSetupRequest = cXMLSetupRequest.Replace("&", "&amp;");
                //Accepts a Punchout Setup Request and returns a Punchout Setup Response Document (cXML)	

                //String strCostCenter;
                //try
                //{
                oXMLPOSR.LoadXml(cXMLSetupRequest);
                //WriteLog("oXMLPOSR.LoadXml(cXMLSetupRequest)\r\n");
                bValidOperation = true;
                //strStatusCode = "200";
                //strStatusText = "Success"; 

                //Edited By Hardik on "25/Nov/2016"
                strStatusCode = Convert.ToString(ConfigurationManager.AppSettings["strStatusCode"]);
                strStatusText = Convert.ToString(ConfigurationManager.AppSettings["strStatusText"]);
                strPayloadID = Convert.ToString(ConfigurationManager.AppSettings["strPayloadID"]);

                //Validate User and retrieve Dictionary (via EMail address??)
                try
                {
                    strEmail = oXMLPOSR.SelectSingleNode("cXML/Request/PunchOutSetupRequest/Contact/Email").LastChild.Value;
                    Session["PunchoutUserEmail"] = strEmail;
                }
                catch { }
                try
                {
                    //strResponseURL = oXMLPOSR.SelectSingleNode("cXML/Response/Status/PunchOutSetupResponse/StartPage").LastChild.Value;
                }
                catch (Exception) { }
                try
                {
                    strFullName = oXMLPOSR.SelectSingleNode("cXML/Request/PunchOutSetupRequest/Contact/Name").LastChild.Value;
                }
                catch
                {
                    bValidOperation = false;
                    strStatusCode = "401";
                    strStatusText = "Unauthorized: No Email or Name specified in the Contact Node";
                }
                try
                {
                    strCurrency = oXMLPOSR.SelectSingleNode("cXML/Request/PunchOutSetupRequest/Contact/Currency").LastChild.Value;
                    strCurrency = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyCode == strCurrency).CurrencyId);
                }
                catch
                {
                    strCurrency = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyId);
                    bValidOperation = false;
                    strStatusCode = "401";
                    strStatusText = "Unauthorized: No Email or Name specified in the Contact Node";
                }
                try
                {
                    strDUNS = oXMLPOSR.SelectSingleNode("cXML/Header/From/Credential").Attributes.GetNamedItem("domain").Value;
                    if (strDUNS != "DUNS")
                    {
                        throw new Exception();
                    }
                }
                catch
                {
                    bValidOperation = false;
                    strStatusCode = "401";
                    strStatusText = "Unauthorized: Invalid domain attribute in the From Credential Node";
                }
                if (bValidOperation)
                {
                    strDUNSCredential = oXMLPOSR.SelectSingleNode("cXML/Header/From/Credential[@domain='DUNS']/Identity").LastChild.Value.Trim();
                    if ((strDUNSCredential.Substring(2)) == "-T")
                    {
                        strDUNSCredential = strDUNSCredential.Substring(strDUNSCredential.Length - 2);
                    }
                    strDUNS = strDUNSCredential;
                }
                try
                {
                    //WriteLog("strBuyerCookie \r\n");
                    strBuyerCookie = oXMLPOSR.SelectSingleNode("cXML/Request/PunchOutSetupRequest/BuyerCookie").LastChild.Value;
                    strBuyerCookie = strBuyerCookie.Replace("+","%2b");
                }
                catch
                {
                    bValidOperation = false;
                    strStatusCode = "401";
                    strStatusText = "Unauthorized: No BuyerCookie specified in the PunchOutSetupRequest Node";
                }

                _catalogueID = objStoreBE.CatalogueId;

                try
                {
                    if (bValidOperation)
                    {
                        UserRegister();
                    }
                }
                catch
                {
                    strStatusCode = "401";
                    strStatusText = "Unauthorized: Error in store user validation";
                }

                String strPunchoutRequest = cXMLSetupRequest;
                String strPunchoutResponse = strStatusCode;

                PunchoutDetailsBE objPunchoutDetailsBE = new PunchoutDetailsBE();
                objPunchoutDetailsBE.BuyerCookie = strBuyerCookie;
                objPunchoutDetailsBE.PunchoutRequest = strPunchoutRequest;
                objPunchoutDetailsBE.PunchoutResponse = strPunchoutResponse;

                res = PunchoutBL.AddPunchoutDetails(objPunchoutDetailsBE, DBAction.Insert);

            }
            else
            {
                strStatusCode = "401";
                strStatusText = "Unauthorized: Punchout Not Allowed For This Store";
            }
            SetupResponse(strBuyerCookie);
            //WriteLog("SetupResponse end \r\n");
            objPunchoutMarkerBE.Marker = "Default setuprequest end";
            PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
        }
        public string ReturnStream()
        {
            objPunchoutMarkerBE.Marker = "Default ReturnStream enter";
            res = PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);

            Stream str = default(Stream);
            string strmContents = null;
            int strLen = 0;
            int strRead = 0;

            // Create a Stream object. 
            str = Request.InputStream;
            // Find number of bytes in stream. 
            strLen = (int)str.Length;
            // Create a byte array. 
            byte[] strArr = new byte[strLen + 1];
            // Read stream into byte array. 
            strRead = str.Read(strArr, 0, strLen);

            strmContents = System.Text.Encoding.UTF8.GetString(strArr);
            objPunchoutMarkerBE.Marker = "Default ReturnStream exit";
            res = PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
            return Server.UrlDecode(strmContents);
        }
        private String TrimXmlString(String strXML)
        {
            try
            {
                int iValuefirstIndex = 0;
                int iValueLastIndex = 0;
                iValuefirstIndex = strXML.IndexOf("<Value>") + 7;
                iValueLastIndex = strXML.IndexOf("</Value>");

                if (iValueLastIndex > 0 && iValuefirstIndex > 0)
                {
                    strXML = strXML.Substring(iValuefirstIndex, (iValueLastIndex - iValuefirstIndex));
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return strXML;
        }
        private void UserRegister()
        {
            try
            {
                UserBE objBE = new UserBE();

                string[] arrEmail = strEmail.Split('@');
                string strPassword = Convert.ToString(arrEmail[0]);

                string SaltPass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(strPassword), "SHA512", null);
                objBE.EmailId = Sanitizer.GetSafeHtmlFragment(strEmail.Trim());
                objBE.Password = SaltPass;
                objBE.IsGuestUser = true;
                objBE.IPAddress = GlobalFunctions.GetIpAddress();
                objBE.IsPunchoutUser = true; // Changes done by Snehal 20 12 2016
                objBE.FirstName = string.Empty;
                objBE.LastName = string.Empty;
                objBE.PredefinedColumn1 = string.Empty;
                objBE.PredefinedColumn2 = string.Empty;
                objBE.PredefinedColumn3 = string.Empty;
                objBE.PredefinedColumn4 = string.Empty;
                objBE.PredefinedColumn5 = string.Empty;
                objBE.PredefinedColumn6 = string.Empty;
                objBE.PredefinedColumn7 = string.Empty;
                objBE.PredefinedColumn8 = "12";
                objBE.PredefinedColumn9 = string.Empty;

                UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                obj.PreDefinedColumn1 = string.Empty;
                obj.PreDefinedColumn2 = string.Empty;
                obj.PreDefinedColumn3 = string.Empty;
                obj.PreDefinedColumn4 = string.Empty;
                obj.PreDefinedColumn5 = string.Empty;
                obj.PreDefinedColumn6 = string.Empty;
                obj.PreDefinedColumn7 = string.Empty;
                obj.PreDefinedColumn8 = "12";
                obj.PreDefinedColumn9 = string.Empty;
                objBE.UserDeliveryAddress.Add(obj);
                objBE.UserTypeID = 1;
                objBE.Points = -1; // Added by ShriGanesh 26 April 2017
                int i = 0;

                objBE.CurrencyId = Convert.ToInt16(strCurrency);// objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyId;
                i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                if (i > 0)
                { }
                else if (i == -1)
                {
                    //GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/EmailIdAlreadyExists"), AlertType.Warning);
                    //return;
                }
                else if (i == -3)
                {
                    //Update i value because if i == -3 then user was already registered as a guest user now when he registers 
                    // then we update his profile from guest user to normal user
                    //i = 1;
                }
                else
                {
                    //GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning);
                    //return;
                }
                if (i > 0)
                {
                    //if (GlobalFunctions.SendRegistrationMail(Sanitizer.GetSafeHtmlFragment(txtEmail.Text.Trim()), Sanitizer.GetSafeHtmlFragment(txtFirstName.Text.Trim()), Sanitizer.GetSafeHtmlFragment(txtLastName.Text.Trim())))
                    //    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/RegistrationSuccess"), GlobalFunctions.GetVirtualPath() + "Login/Login.aspx", AlertType.Success);
                    //else
                    //    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/SendEmailFailed"), AlertType.Failure); return;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        public void SetupResponse(String strBuyerCookie)
        {
            try
            {
                objPunchoutMarkerBE.Marker = "Default setupresponce enter";
                PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
                StreamReader sResponseBody;
                if (strStatusCode == "200")
                { sResponseBody = new StreamReader(Server.MapPath("~/Punchout/ResponseBody.xml")); }
                else
                { sResponseBody = new StreamReader(Server.MapPath("~/Punchout/ResponseBodyInvalid.xml")); }

                if (sResponseBody != null)
                {
                    String strResponseBody = sResponseBody.ReadToEnd();
                    sResponseBody.Close();

                    //strResponseBody = strResponseBody.Replace("$timestamp$", DateTime.Now.ToString()); Edited for Workday
                    strResponseBody = strResponseBody.Replace("$timestamp$", DateTime.Now.ToString(GlobalFunctions.GetSetting("PunchoutDateFormat")));
                    //strResponseBody = strResponseBody.Replace("$payloadID$", "933694607739");
                    //Edited By Hardik on "25/Nov/2016"
                    strResponseBody = strResponseBody.Replace("$payloadID$", strPayloadID);
                    strResponseBody = strResponseBody.Replace("$code$", strStatusCode);
                    strResponseBody = strResponseBody.Replace("$text$", strStatusText);
                    if (strStatusCode == "200")
                    {
                        objPunchoutMarkerBE.Marker = "Default GetPunchoutId enter";
                        PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);

                        PunchoutDetailsBE objPunchoutDetailsBE = new PunchoutDetailsBE();
                        objPunchoutDetailsBE.BuyerCookie = strBuyerCookie;

                        objPunchoutDetailsBE = PunchoutBL.GetPunchoutID(objPunchoutDetailsBE);
                        string strPunchoutID = Convert.ToString(objPunchoutDetailsBE.PunchoutID);
                        objPunchoutMarkerBE.Marker = "Default Get PunchoutID END";
                        PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);

                        String strUrl = "";
                        if (Request.Url.AbsoluteUri.ToLower().Contains("localhost"))
                        { strUrl = GlobalFunctions.GetVirtualPath() + "Punchout/AllowPunchout.aspx?reqUrl=" + strPunchoutID; }
                        else
                        { strUrl = GlobalFunctions.GetVirtualPath() + "Punchout/AllowPunchout.aspx?reqUrl=" + strPunchoutID; }

                        objPunchoutMarkerBE.Marker = strUrl;
                        PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
                        strResponseBody = strResponseBody.Replace("$URL$", strUrl);
                        strResponseURL = strUrl;
                        string cXML_Form = "";
                        //Build The Post Shopping Cart cXML 
                        //cXML_Form = Punchout.CreateCxmlForm(strUrl, strResponseBody);

                        //objPunchoutDetailsBE.PunchoutID=Convert.ToInt32(strPunchoutID);
                        objPunchoutDetailsBE.PunchoutResponseBody = strResponseBody;

                        bool res = PunchoutBL.AddPunchoutDetails(objPunchoutDetailsBE, DBAction.Update);
                        //Punchout.UpdatePunchoutResponse(Convert.ToInt32(strPunchoutID), strResponseBody);

                        Session["cXML_ALLOWPUNCHOUT"] = strResponseBody;
                        //Create the form to post back             
                        Response.Buffer = true;
                        Response.Clear();
                        Response.Write(strResponseBody);
                        //Response.Redirect(strUrl, true);
                        objPunchoutMarkerBE.Marker = "Default Setupresponce end";
                        PunchoutBL.SetPunchoutMarker(objPunchoutMarkerBE, DBAction.Insert);
                    }
                    else
                    {
                        string cXML_Form = "";
                        //Build The Post Shopping Cart cXML 
                        cXML_Form = PunchoutBL.CreateCxmlForm(Request.UrlReferrer.ToString(), strResponseBody);
                        //Create the form to post back             
                        Response.Clear();
                        Response.Write(cXML_Form);
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}