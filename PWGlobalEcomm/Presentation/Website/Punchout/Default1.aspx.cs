﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.IO;
using System.Xml;

namespace Presentation
{
    public partial class Punchout_Default1 : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.TextBox txtcXML;
        protected global::System.Web.UI.WebControls.Button btnSubmit;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StreamReader fp = default(StreamReader);
                String xmlData = "";
                fp = File.OpenText(Server.MapPath("XMLFile.xml"));
                xmlData = fp.ReadToEnd();
                fp.Close();
                txtcXML.Text = xmlData;

                String sReturnStream = ReturnStream().ToString();
                if (sReturnStream != "")
                {
                    XmlDocument oXMLDoc = new XmlDocument();
                    sReturnStream = sReturnStream.Replace("cXML-urlencoded=", "");
                    oXMLDoc.LoadXml(sReturnStream);
                    try
                    {
                        string strStatusText = oXMLDoc.SelectSingleNode("cXML/Response/Status").Attributes.GetNamedItem("text").Value;
                        Response.Write(strStatusText);
                    }
                    catch
                    {
                        Response.Write("Bad Response");
                    }
                }
            }
        }
        public string ReturnStream()
        {

            Stream str = default(Stream);
            string strmContents = null;
            int counter = 0;
            int strLen = 0;
            int strRead = 0;

            // Create a Stream object. 
            str = Request.InputStream;
            // Find number of bytes in stream. 
            strLen = (int)str.Length;
            // Create a byte array. 
            byte[] strArr = new byte[strLen + 1];
            // Read stream into byte array. 
            strRead = str.Read(strArr, 0, strLen);

            strmContents = System.Text.Encoding.UTF8.GetString(strArr);

            if (strLen == 0)
            { return ""; }
            else
            {
                return Server.UrlDecode(strmContents);
            }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            String sURL = GlobalFunctions.GetVirtualPath() + "Punchout/Default.aspx";
            //ConfigurationManager.AppSettings["siteurl"].Replace("/WebContents", "") + "/Punchout/Default.aspx";
            String xmlData = txtcXML.Text.Trim();
            string cXML_Form = "";
            //Build The Post Shopping Cart cXML 
            cXML_Form = PunchoutBL.CreateCxmlForm(sURL, xmlData);

            //Create the form to post back             
            Response.Clear();
            Response.Write(cXML_Form);
        }
    }
}