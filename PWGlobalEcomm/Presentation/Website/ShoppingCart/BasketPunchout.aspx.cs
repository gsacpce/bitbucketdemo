﻿using AHPP.ASP.Net.Lib;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.Stock;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

namespace Presentation
{
    public partial class ShoppingCart_BasketPunchout : BasePage
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Repeater rptBasketListing;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvEmptyBasket, hPageHeading;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidcountyid;

        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        Int32 intUserId = 0;
        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        public decimal dTotalPrice = 0;
        string strUserSessionId = string.Empty;
        public bool bBackOrderAllowed = false;
        public bool bBASysStore = false;
        public string strProductText, strTitleText, strQuantityText, strUnitCost, strLineTotal, strRemoveText, strTotalText, strContinueShoppingText,
            strCheckoutText, strRemoveItemsFromCart, strQuantityNotZero, strMinMaxQuantity, strQuantityNotMoreThanStock,
            strProductRemovedFromCart, strBasketErrorWhileRemovingProduct, strInvalidQuantityMessage, strPageTitle, strPageMetaKeywords,
            EmailId, Password, SignIn_to_Account, Forgot_password, Checkout_as_Guest, Basket_OrderConfirmation, Basket_NewToOurSite, Basket_HaveAnAccount,
            strSelectCountryFailMsg, strPageMetaDescription, strCommonErrorMessageWhileUpdatingBasket = string.Empty;

        public bool bIsPointsEnabled = false;
        public bool bCustomPunchout = false;

        int intCustomerOrderId = 0;
        int countryid = 0;

        //Added By Hardik 8-Aug-2016
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDutyMessage, divMOVMessage;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidStandard, hidExpress, hidStandardTax, hidExpressTax, hidBasketTot, hidSpnShippingPrice,
            hidddlDCountry, hidDeliveryCharge;
        //protected global::System.Web.UI.HtmlControls.HtmlSelect ddlDCountry;
        decimal dTotalWeight = 0;
        public string strCurrencySymbol = string.Empty;
        public static string strShipping = "";
        CustomerOrderBE lstCustomerOrders;
        public string strDutyMessage = string.Empty;// "International duty may apply in your country.";
        //Added By Hardik 8-Aug-2016

        #endregion

        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 4-11-2015
        /// Scope   : Page_Load of the ShoppingCart_BasketPunchout page replica of ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                GlobalFunctions.writePunchoutLog("ShoppingCart_BasketPunchout_Page_Load");
                if (Session["User"] != null)
                {
                    UserBE lst = new UserBE();
                    lst = Session["User"] as UserBE;
                    intUserId = lst.UserId;
                }
                else
                {
                    strUserSessionId = Session.SessionID;
                }

                intCurrencyId = GlobalFunctions.GetCurrencyId();
                intLanguageId = GlobalFunctions.GetLanguageId();
                BindResourceData();
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "C_punchout").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "normal punchout").IsEnabled)
                    {
                        bCustomPunchout = false;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "C_punchout").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "nissan punchout").IsEnabled)
                    {
                        bCustomPunchout = true;
                    }
                    //bCustomPunchout = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "C_punchout").FeatureValues[0].IsEnabled;
                    bBackOrderAllowed = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "OD_AllowBackOrder").FeatureValues[0].IsEnabled;
                    bBASysStore = objStoreBE.IsBASYS;
                }

                if (!IsPostBack)
                {
                    List<object> lstShoppingBE = new List<object>();
                    List<ShoppingCartBE> lstShoppingCartBE = new List<ShoppingCartBE>();

                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId);
                    lstShoppingCartBE = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    //ViewState["VS_SHOPPINGCARTITEMS"] = lstShoppingCartBE;


                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        rptBasketListing.DataSource = lstShoppingBE;
                        rptBasketListing.DataBind();
                        Session["ProductWeight"] = Convert.ToString(dTotalWeight);
                    }
                    else
                    {
                        rptBasketListing.DataSource = null;
                        rptBasketListing.DataBind();
                    }
                }
                this.Page.Title = strPageTitle;
                this.MetaDescription = strPageMetaDescription;
                this.MetaKeywords = strPageMetaKeywords;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 4-11-2015
        /// Scope   : BindResourceData of the ShoppingCart_BasketPunchout page replica of ShoppingCart_Basket page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strDutyMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_duty_message").ResourceValue; //Added By Hardik
                        strSelectCountryFailMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Country_Placeholder_2").ResourceValue; //Added By Hardik
                        hPageHeading.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Page_Heading").ResourceValue;
                        strProductText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Product_Text").ResourceValue;
                        strTitleText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Title_Text").ResourceValue;
                        strQuantityText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Text").ResourceValue;
                        strUnitCost = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Unit_Cost_Text").ResourceValue;
                        strLineTotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Line_Total_Text").ResourceValue;
                        strRemoveText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Remove_Text").ResourceValue;
                        strTotalText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Total_Text").ResourceValue;
                        strContinueShoppingText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Continue_Shopping_Text").ResourceValue;
                        strCheckoutText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Checkout_Text").ResourceValue;
                        dvEmptyBasket.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_NoProductsFound_Message").ResourceValue;
                        strRemoveItemsFromCart = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Remove_Items_From_Cart_Message").ResourceValue;
                        strQuantityNotZero = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Not_Zero_Message").ResourceValue;
                        strMinMaxQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Min_Max_Quantity_Message").ResourceValue;
                        strQuantityNotMoreThanStock = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Not_More_Than_Stock_Message").ResourceValue;
                        strProductRemovedFromCart = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Product_Removed_From_Cart_Message").ResourceValue;
                        strBasketErrorWhileRemovingProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Error_While_Removing_Product_Message").ResourceValue;
                        strInvalidQuantityMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Invalid_Quantity_Message").ResourceValue;
                        strPageTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Page_Title").ResourceValue;
                        strPageMetaKeywords = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Page_Meta_Keywords").ResourceValue;
                        strPageMetaDescription = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Page_Meta_Description").ResourceValue;
                        strCommonErrorMessageWhileUpdatingBasket = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Common_Error_Message_While_Updating_Basket").ResourceValue;
                        EmailId = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailId").ResourceValue;
                        Password = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Password").ResourceValue;
                        SignIn_to_Account = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_SignIntoYourAccount").ResourceValue;
                        Forgot_password = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_ForgotPassword").ResourceValue;
                        Checkout_as_Guest = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_CheckoutAsGuest").ResourceValue;
                        Basket_OrderConfirmation = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_OrderConfirmationMessage").ResourceValue;
                        Basket_NewToOurSite = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_NewToOurSite").ResourceValue;
                        Basket_HaveAnAccount = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_HaveAnAccount").ResourceValue;
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 4-11-2015
        /// Scope   : rptBasketListing_ItemDataBound ShoppingCart_BasketPunchout page replica of ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptBasketListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header)
                {
                    HtmlGenericControl divProductText = (HtmlGenericControl)e.Item.FindControl("divProductText");
                    HtmlGenericControl divTitleText = (HtmlGenericControl)e.Item.FindControl("divTitleText");
                    HtmlGenericControl divQuantityText = (HtmlGenericControl)e.Item.FindControl("divQuantityText");
                    HtmlGenericControl divUnitCostText = (HtmlGenericControl)e.Item.FindControl("divUnitCostText");
                    HtmlGenericControl divLineTotalText = (HtmlGenericControl)e.Item.FindControl("divLineTotalText");
                    HtmlGenericControl divRemoveText = (HtmlGenericControl)e.Item.FindControl("divRemoveText");
                    divProductText.InnerHtml = strProductText;
                    divTitleText.InnerHtml = strTitleText;
                    divQuantityText.InnerHtml = strQuantityText;
                    divUnitCostText.InnerHtml = strUnitCost;
                    divLineTotalText.InnerHtml = strLineTotal;
                    divRemoveText.InnerHtml = strRemoveText;
                }
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl dvUnitPrice = (HtmlGenericControl)e.Item.FindControl("dvUnitPrice");
                    HtmlGenericControl dvBasketRowContainer = (HtmlGenericControl)e.Item.FindControl("dvBasketRowContainer");
                    HtmlGenericControl dvTotalPrice = (HtmlGenericControl)e.Item.FindControl("dvTotalPrice");
                    HtmlAnchor aRemoveProducts = (HtmlAnchor)e.Item.FindControl("aRemoveProducts");
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)e.Item.FindControl("hdnShoppingCartProductId");
                    Literal ltrSKUName = (Literal)e.Item.FindControl("ltrSKUName");
                    Literal ltrProductName = (Literal)e.Item.FindControl("ltrProductName");
                    HtmlInputText txtQuantity = (HtmlInputText)e.Item.FindControl("txtQuantity");
                    HtmlInputHidden hdnIsBackOrderAllowed = (HtmlInputHidden)e.Item.FindControl("hdnIsBackOrderAllowed");
                    HtmlInputHidden hdnMinimumOrderQuantity = (HtmlInputHidden)e.Item.FindControl("hdnMinimumOrderQuantity");
                    HtmlInputHidden hdnMaximumOrderQuantity = (HtmlInputHidden)e.Item.FindControl("hdnMaximumOrderQuantity");

                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";


                    double dPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price"));
                    //string strUnitPrice = GlobalFunctions.GetCurrencySymbol() + "<span>" + dPrice.ToString("##,###,##0.#0") + "</span>";
                    dvUnitPrice.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + dPrice.ToString("##,###,##0.#0") + "</span>";                    
                    dvTotalPrice.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + Convert.ToDecimal(dPrice.ToString("##,###,##0.#0")) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity")) + "</span>";
                    dTotalPrice += Convert.ToDecimal(dPrice.ToString("##,###,##0.#0")) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                    aRemoveProducts.Attributes.Add("rel", Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId")));
                    dvBasketRowContainer.ID = "dvBasketRowContainer" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId"));
                    hdnShoppingCartProductId.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId"));
                    ltrSKUName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SKU"));
                    ltrProductName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductName"));
                    txtQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                    hdnIsBackOrderAllowed.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "IsBackOrderAllowed"));
                    hdnMinimumOrderQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MinimumOrderQuantity"));
                    hdnMaximumOrderQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MaximumOrderQuantity"));
                    //Added By Hardik 8-Aug-2016
                    dTotalWeight += Math.Ceiling(Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "DimensionalWeight")) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity")) * Convert.ToDecimal(0.001));
                    try
                    {
                        if (bBackOrderAllowed == false)
                        {
                            if (bBASysStore)
                            {
                                Int32 intProductid_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BASYSProductId")),
                                    intBaseColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BaseColorId")),
                                    intTrimColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "TrimColorId"));

                                Stock objStock = new Stock();
                                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                                    ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                                objStock.Credentials = objNetworkCredentials;
                                StockDetails objStockDetails = new StockDetails();
                                objStockDetails = objStock.LevelEnquiry(intProductid_OASIS, intBaseColorId_OASIS, intTrimColorId_OASIS);

                                if (objStockDetails != null)
                                {
                                    HtmlInputHidden hdnStockStatus = (HtmlInputHidden)e.Item.FindControl("hdnStockStatus");
                                    hdnStockStatus.Value = Convert.ToString(objStockDetails.StockLevel);
                                }
                            }
                            else
                            {
                                HtmlInputHidden hdnStockStatus = (HtmlInputHidden)e.Item.FindControl("hdnStockStatus");
                                hdnStockStatus.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Inventory"));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                }
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlGenericControl dvTotal = (HtmlGenericControl)e.Item.FindControl("dvTotal");
                    HtmlGenericControl divTotalText = (HtmlGenericControl)e.Item.FindControl("divTotalText");
                    HtmlAnchor aCheckOut = (HtmlAnchor)e.Item.FindControl("aCheckOut");
                    HtmlAnchor aContinueShopping = (HtmlAnchor)e.Item.FindControl("aContinueShopping");
                    HtmlGenericControl spnShippingPrice = (HtmlGenericControl)e.Item.FindControl("spnShippingPrice"); //Added By Hardik

                    strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                    dTotalPrice += Convert.ToDecimal(spnShippingPrice.InnerText);// Added By Hardik - FinalTotal
                    hidBasketTot.Value = Convert.ToString(dTotalPrice);
                    dvTotal.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + Convert.ToString(dTotalPrice) + "</span>";
                    divTotalText.InnerHtml = strTotalText;
                    aContinueShopping.InnerHtml = strContinueShoppingText;
                    aCheckOut.InnerHtml = strCheckoutText;

                    #region Bindd Drop Down country "Hardik"
                    Control FooterTemplate = rptBasketListing.Controls[0].Controls[0];
                    DropDownList ddlDCountry = (DropDownList)e.Item.FindControl("ddlDCountry");
                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        ddlDCountry.DataSource = lstCountry;
                        ddlDCountry.DataTextField = "CountryName";
                        ddlDCountry.DataValueField = "CountryId";
                        ddlDCountry.DataBind();
                        ddlDCountry.Items.Insert(0, new ListItem("--Select--", "0"));
                    }
                    ddlDCountry.SelectedIndex = 0;
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 4-11-2015
        /// Scope   : aCheckOut_ServerClick of the ShoppingCart_BasketPunchout page replica of ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void aCheckOut_ServerClick(object sender, EventArgs e)
        {
            try
            {
                //Added By Hardik For Dropdown validation
                if (bCustomPunchout == true)
                {
                    if (hidddlDCountry.Value != "0")
                    {
                        GlobalFunctions.writePunchoutLog("ShoppingCartPO - PlaceOrder");
                        UpdateCustomerOrderData();
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strSelectCountryFailMsg, AlertType.Failure);

                        DropDownList ddlDCountry = (DropDownList)FindControl("ddlDCountry");
                        ddlDCountry.Visible = true;
                        return;
                    }
                }
                else
                {
                    GlobalFunctions.writePunchoutLog("ShoppingCartPO - PlaceOrder");
                    UpdateCustomerOrderData();
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 4-11-2015
        /// Scope   : RemoveProductsFromBasket webmethod of the ShoppingCart_BasketPunchout page replica of ShoppingCart_Basket page
        /// </summary>
        /// <param name="ShoppingCartProductId"></param>                
        /// <returns name="bool"></returns>
        [System.Web.Services.WebMethod]
        public static bool RemoveProductsFromBasket(Int32 ShoppingCartProductId)
        {
            try
            {
                string strUserSessionId = string.Empty;
                Int32 intUserId = 0;
                if (HttpContext.Current.Session["User"] != null)
                {
                    UserBE lst = new UserBE();
                    lst = HttpContext.Current.Session["User"] as UserBE;
                    intUserId = lst.UserId;
                }
                else
                {
                    strUserSessionId = HttpContext.Current.Session.SessionID;
                }

                List<object> lstShoppingBE = new List<object>();
                lstShoppingBE = ShoppingCartBL.RemoveProductsFromBasket(ShoppingCartProductId, intUserId, strUserSessionId);
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShoppingCart = new List<ShoppingCartBE>();
                    lstNewShoppingCart = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    if (lstNewShoppingCart[0].ShoppingCartProductId != 0)
                        return true;
                    else
                        return false;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 4-11-2015
        /// Scope   : UpdateBasketProducts webmethod of the ShoppingCart_BasketPunchout page replica of ShoppingCart_Basket page
        /// </summary>
        /// <param name="ShoppingCartProductId"></param>                
        /// <returns name="bool"></returns>
        [System.Web.Services.WebMethod]
        public static bool UpdateBasketProducts(string action)
        {
            bool bStatus = false;
            try
            {
                if (action == "y")
                {
                    if (HttpContext.Current.Session["User"] != null)
                    {
                        UserBE lstUser = new UserBE();
                        lstUser = HttpContext.Current.Session["User"] as UserBE;
                        Login_UserLogin.UpdateBasketSessionProducts(lstUser, 'Y');
                        bStatus = true;
                    }
                }
                else if (action == "n")
                {
                    if (HttpContext.Current.Session["User"] != null)
                    {
                        UserBE lstUser = new UserBE();
                        lstUser = HttpContext.Current.Session["User"] as UserBE;
                        Login_UserLogin.UpdateBasketSessionProducts(lstUser, 'N');
                        bStatus = true;
                    }
                    bStatus = true;
                }
                return bStatus;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return bStatus;
            }
        }


        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 4-11-2015
        /// Scope   : Bind UpdateCustomerOrderData of the ShoppingCart_BasketPunchout page replica of ShoppingCart_Basket page
        /// </summary>            
        /// <returns>True</returns>
        protected bool UpdateCustomerOrderData()
        {
            bool bStatus = false;
            try
            {
                GlobalFunctions.writePunchoutLog("UpdateCustomerOrderData start");
                UserBE objUser = new UserBE();
                objUser = (UserBE)Session["User"];

                string strGUID = Funcs.CreateTxRefGUID();
                CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
                objCustomerOrderBE.OrderedBy = objUser.UserId;
                GlobalFunctions.writePunchoutLog("UserID : " + objUser.UserId);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["CustomerOrderId"])))
                {
                    objCustomerOrderBE.action = DBAction.Update;
                    objCustomerOrderBE.CustomerOrderId = Convert.ToInt32(Session["CustomerOrderId"]);
                    GlobalFunctions.writePunchoutLog("Action : Update");
                    GlobalFunctions.writePunchoutLog("CustomerOrderId : " + objCustomerOrderBE.CustomerOrderId);
                }
                else
                {
                    objCustomerOrderBE.CustomerOrderId = 0;
                    objCustomerOrderBE.action = DBAction.Insert;
                    GlobalFunctions.writePunchoutLog("Action : Insert");
                    GlobalFunctions.writePunchoutLog("CustomerOrderId : 0");
                }
                objCustomerOrderBE.ApprovedBy = 0;
                objCustomerOrderBE.InvoiceCompany = "";
                objCustomerOrderBE.InvoiceContactPerson = "";
                objCustomerOrderBE.InvoiceAddress1 = "";
                objCustomerOrderBE.InvoiceAddress2 = "";
                objCustomerOrderBE.InvoiceCity = "";
                objCustomerOrderBE.InvoicePostalCode = "";
                objCustomerOrderBE.InvoiceCountry = "";
                objCustomerOrderBE.InvoiceCounty = "";
                objCustomerOrderBE.InvoicePhone = "";
                objCustomerOrderBE.InvoiceFax = "";
                objCustomerOrderBE.InvoiceEmail = objUser.EmailId;
                objCustomerOrderBE.PaymentMethod = Sanitizer.GetSafeHtmlFragment("0");
                objCustomerOrderBE.OrderRefNo = Convert.ToString(System.DateTime.Now);
                objCustomerOrderBE.AuthorizationId_OASIS = "";
                objCustomerOrderBE.OrderId_OASIS = 0;
                objCustomerOrderBE.TxnRefGUID = Funcs.CreateTxRefGUID();
                objCustomerOrderBE.IsOrderCompleted = false;
                objCustomerOrderBE.DeliveryCompany = "";
                objCustomerOrderBE.DeliveryAddressId = 0;
                objCustomerOrderBE.DeliveryContactPerson = "";
                objCustomerOrderBE.DeliveryAddress1 = "";
                objCustomerOrderBE.DeliveryAddress2 = "";
                objCustomerOrderBE.DeliveryCity = "";
                objCustomerOrderBE.DeliveryPostalCode = "";
                objCustomerOrderBE.DeliveryCountry = "";
                objCustomerOrderBE.DeliveryCounty = "";
                objCustomerOrderBE.DeliveryPhone = "";
                objCustomerOrderBE.DeliveryFax = "";
                objCustomerOrderBE.stCardScheme = "";
                objCustomerOrderBE.SpecialInstructions = "";
                objCustomerOrderBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                objCustomerOrderBE.LanguageId = GlobalFunctions.GetLanguageId();


                if (bCustomPunchout == true)
                {
                    HtmlInputRadioButton rbStandard = (HtmlInputRadioButton)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("rbStandard");
                    HtmlInputRadioButton rbExpress = (HtmlInputRadioButton)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("rbExpress");

                    if (rbStandard.Checked)
                    {
                        objCustomerOrderBE.StandardCharges = Convert.ToDouble(hidStandard.Value);
                        objCustomerOrderBE.ExpressCharges = 0;
                    }
                    if (rbExpress.Checked)
                    {
                        objCustomerOrderBE.StandardCharges = 0;
                        objCustomerOrderBE.ExpressCharges = Convert.ToDouble(hidExpress.Value);
                    }
                }
                #region "For Normal Punchout"
                else
                {
                    objCustomerOrderBE.StandardCharges = 0;
                    objCustomerOrderBE.ExpressCharges = 0;
                }
                #endregion

                objCustomerOrderBE.OrderCarrierId = "0";
                objCustomerOrderBE.OrderCarrierName = "";
                objCustomerOrderBE.TotalTax = 0;
                objCustomerOrderBE.CouponCode = string.Empty;

                #region Freight&TaxForCouponCode
                double OrderItemTotal = 0;
                //string StandardFreightValueAfterDiscount = "0";
                //string ExpressFreightValueAfterDiscount = "0";
                //string StandardTaxValueAfterDiscount = "0";
                //double OrderValueAfterDiscount = 0;
                //string ExpressTaxValueAfterDiscount = "0";
                string strBehaviourType = string.Empty;

                string strMessages = string.Empty;
                string strShoppingCartProductIds = string.Empty;
                for (int i = 0; i < rptBasketListing.Items.Count; i++)
                {
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnShoppingCartProductId");
                    HtmlInputText txtQuantity = (HtmlInputText)rptBasketListing.Items[i].FindControl("txtQuantity");
                    //HtmlGenericControl divProductText = (HtmlGenericControl)rptBasketListing.Items[i].FindControl("divProductText");used internal control
                    Literal ltrSKUName = (Literal)rptBasketListing.Items[i].FindControl("ltrSKUName");
                    Literal ltrProductName = (Literal)rptBasketListing.Items[i].FindControl("ltrProductName");
                    HtmlGenericControl dvUnitPrice = (HtmlGenericControl)rptBasketListing.Items[i].FindControl("dvUnitPrice");
                    if (string.IsNullOrEmpty(txtQuantity.Value.Trim()))
                    {
                        strMessages += strInvalidQuantityMessage + ltrSKUName.Text + "\n";
                    }
                    else
                    {
                        //if (GlobalFunctions.isNumeric(txtQuantity.Value.Trim(), System.Globalization.NumberStyles.Integer)) Commented by Sripal (Quantity validataion only non sign n decimal numbers)
                        Int64 aQty;
                        try
                        {
                            aQty = Convert.ToInt64(txtQuantity.Value.Trim(), System.Globalization.CultureInfo.CurrentCulture);
                            if (aQty > 0)
                            {
                                strShoppingCartProductIds += Sanitizer.GetSafeHtmlFragment(hdnShoppingCartProductId.Value) + "|" + Sanitizer.GetSafeHtmlFragment(txtQuantity.Value) + ";";
                            }
                            else
                            {
                                strMessages += strInvalidQuantityMessage + ltrSKUName.Text + "\n";
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            strMessages += strInvalidQuantityMessage + ltrSKUName.Text + "\n";
                        }
                    }
                }
                List<object> lstShoppingBE = new List<object>();
                lstShoppingBE = ShoppingCartBL.UpdateBasketData(strShoppingCartProductIds, intUserId, strUserSessionId);
                lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), objUser.UserId, "");
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                    lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    ViewState["VS_SHOPPINGCARTITEMS"] = lstNewShopping;
                    for (int i = 0; i < lstShoppingBE.Count; i++)
                    {
                        OrderItemTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                    }
                }

                CouponBE objCouponBE = new CouponBE();
                objCouponBE.OrderValue = float.Parse(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(OrderItemTotal)));
                objCouponBE.CouponCode = "";
                objCouponBE.Region = "GB";
                objCouponBE.ShipmentType = "0"; // 1 for Standard, 2 for express shipping method
                #endregion

                CustomerOrderBE lstShoppingCart = new CustomerOrderBE();
                GlobalFunctions.writePunchoutLog("Before ShoppingCartBL.CustomerOrderPunchout_SAE call start");
                lstShoppingCart = ShoppingCartBL.CustomerOrderPunchout_SAE(objCustomerOrderBE);
                GlobalFunctions.writePunchoutLog("Shopping cart ID :" + lstShoppingCart);
                GlobalFunctions.writePunchoutLog("Before ShoppingCartBL.CustomerOrderPunchout_SAE call end");
                //lstShoppingCart.CustomerOrderProducts = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                if (lstShoppingCart != null && lstShoppingCart.CustomerOrderId != 0)
                {
                    intCustomerOrderId = lstShoppingCart.CustomerOrderId;
                    bStatus = true;
                    SendPunchoutOrderMessage();

                    Hashtable hstParameters = new Hashtable();
                    hstParameters.Add("orderId", intCustomerOrderId);
                    GlobalFunctions.writePunchoutLog("CustomerOrderId : " + intCustomerOrderId);
                    string url = GlobalFunctions.EncryptURL(hstParameters, host + "shoppingcart/OrderDetailsPunchout.aspx?");
                    Response.Redirect(url);
                }

                GlobalFunctions.writePunchoutLog("UpdateCustomerOrderData end");
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return bStatus;
        }


        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 4-11-2015
        /// Scope   : SendPunchoutOrderMessage webmethod of the ShoppingCart_BasketPunchout page new method for Punchout
        /// </summary>
        /// <param name="ShoppingCartProductId"></param>                
        /// <returns name="bool"></returns>
        public void SendPunchoutOrderMessage()
        {
            try
            {
                GlobalFunctions.writePunchoutLog("SendPunchoutOrderMessage call start");
                XmlDocument oXMLDoc = new XmlDocument();
                XmlDocument oXMLOrderMeassage = new XmlDocument();
                StoreBE objStoreBE = new StoreBE();
                GlobalFunctions.writePunchoutLog("GetStoreDetails start");
                objStoreBE = StoreBL.GetStoreDetails();
                GlobalFunctions.writePunchoutLog("GetStoreDetails end");
                HtmlGenericControl spnShippingPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnShippingPrice");
                if (Session["S_PUNCHOUTREQUEST"] != null)
                {
                    #region
                    GlobalFunctions.writePunchoutLog("Session S_PUNCHOUTREQUEST has value");
                    oXMLDoc.LoadXml(Convert.ToString(Session["S_PUNCHOUTREQUEST"]));
                    StreamReader sReadMessageBody = new StreamReader(Server.MapPath("~/Punchout/OrderMessageBody.xml"));
                    String strOMB = sReadMessageBody.ReadToEnd();
                    StreamReader sReadItemBody = new StreamReader(Server.MapPath("~/Punchout/OrderMessageItemBody.xml"));
                    String strOIB = sReadItemBody.ReadToEnd();
                    #region "Added By Hardik"
                    StreamReader sReadFreightBody = new StreamReader(Server.MapPath("~/Punchout/OrderMessageFreightBody.xml"));
                    String strOFB = sReadFreightBody.ReadToEnd();
                    String sFreightDetails = "";
                    #endregion
                    sReadMessageBody.Close();
                    sReadItemBody.Close();
                    String sURL = "";
                    String sCurrency;
                    int intSupplierId_OASIS = 0;
                    try
                    {
                        sCurrency = oXMLDoc.SelectSingleNode("cXML/Request/PunchOutSetupRequest/Contact/Currency").LastChild.Value;
                        sCurrency = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyCode == sCurrency).CurrencyCode);
                    }
                    catch
                    {
                        sCurrency = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyCode);
                    }
                    String stimestamp, spayloadID, sFromIdentity, sToIdentity, sSenderIdentity, sUserAgent, sBuyerCookie = string.Empty;
                    String sTotalCurrency, sTotalMoney, sShipToName, sShipToAddress, sShippingCurrency, sShippingMoney, sShippingDescription, sItemDetails, sQuantity, sSupplierPartID, sCARRIERSERVICEID, sBASECOLOUR, sTRIMCOLOUR, sPRODUCTID, sSUPPLIERID, sItemDetailCurrency, sItemDetailMoney, sItemDetailDescription, sExtrinsic, sTaxCurrency, sTaxMoney, sTaxDescription = string.Empty;
                    String sLEADTIME = "4";

                    String sUnitOfMeasure = "EA";
                    String sClassification = "49101602";// "80141605";
                    if (objStoreBE.StoreId == 462)
                    {
                        sClassification = "8014161603";
                    }

                    sURL = oXMLDoc.SelectSingleNode("cXML/Request/PunchOutSetupRequest/BrowserFormPost/URL").LastChild.Value;
                    if (objStoreBE.StoreId == 462)
                    {
                        sURL = GlobalFunctions.GetSetting("PunchOutOrderURL");
                    }
                    Session["PunchoutSubmitUrl"] = sURL;
                    // sCurrency = "USD";
                    stimestamp = DateTime.Now.ToString();
                    spayloadID = oXMLDoc.SelectSingleNode("cXML").Attributes.GetNamedItem("payloadID").Value;
                    sFromIdentity = oXMLDoc.SelectSingleNode("cXML/Header/To/Credential/Identity").LastChild.Value;
                    sToIdentity = oXMLDoc.SelectSingleNode("cXML/Header/From/Credential/Identity").LastChild.Value;
                    sSenderIdentity = oXMLDoc.SelectSingleNode("cXML/Header/To/Credential/Identity").LastChild.Value;
                    sUserAgent = oXMLDoc.SelectSingleNode("cXML/Header/Sender/UserAgent").LastChild.Value;
                    sBuyerCookie = oXMLDoc.SelectSingleNode("cXML/Request/PunchOutSetupRequest/BuyerCookie").LastChild.Value;
                    sTotalCurrency = sCurrency;
                    sTotalMoney = "0";

                    List<ShoppingCartBE> lstShoppingCartBE = new List<ShoppingCartBE>();
                    lstShoppingCartBE = (List<ShoppingCartBE>)ViewState["VS_SHOPPINGCARTITEMS"];
                    decimal iTotal = 0;
                    decimal TotalMoney = 0;

                    foreach (ShoppingCartBE objShoppingCartBE in lstShoppingCartBE)
                    {
                        iTotal += (objShoppingCartBE.Price * objShoppingCartBE.Quantity);
                        intSupplierId_OASIS = objShoppingCartBE.SupplierId;
                    }

                    #region "For custom Punchout"
                    if (bCustomPunchout == false)
                    {
                        sTotalMoney = iTotal.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        TotalMoney = iTotal + Convert.ToDecimal(hidSpnShippingPrice.Value);
                        sTotalMoney = TotalMoney.ToString(CultureInfo.InvariantCulture);
                    }
                    #endregion

                    sShipToName = oXMLDoc.SelectSingleNode("cXML/Request/PunchOutSetupRequest/ShipTo/Address/Name").LastChild.Value;
                    sShipToAddress = oXMLDoc.SelectSingleNode("cXML/Request/PunchOutSetupRequest/ShipTo/Address").LastChild.OuterXml;
                    sShippingCurrency = sCurrency;
                    sShippingMoney = "";
                    sShippingDescription = "";
                    sItemDetails = "";
                    strOMB = strOMB.Replace("$timestamp$", stimestamp);
                    strOMB = strOMB.Replace("$payloadID$", spayloadID);
                    strOMB = strOMB.Replace("$FromIdentity$", sFromIdentity);
                    strOMB = strOMB.Replace("$ToIdentity$", sToIdentity);
                    strOMB = strOMB.Replace("$SenderIdentity$", sSenderIdentity);
                    strOMB = strOMB.Replace("$UserAgent$", sUserAgent);
                    strOMB = strOMB.Replace("$BuyerCookie$", sBuyerCookie);
                    strOMB = strOMB.Replace("$TotalCurrency$", sTotalCurrency);
                    strOMB = strOMB.Replace("$TotalMoney$", sTotalMoney);
                    strOMB = strOMB.Replace("$ShipToName$", sShipToName);
                    strOMB = strOMB.Replace("$ShipToAddress$", sShipToAddress);
                    strOMB = strOMB.Replace("$ShippingCurrency$", sShippingCurrency);
                    strOMB = strOMB.Replace("$ShippingMoney$", sShippingMoney.Replace("$", "").Replace("€", "").Replace("£", ""));
                    strOMB = strOMB.Replace("$ShippingDescription$", sShippingDescription);

                    foreach (ShoppingCartBE objShoppingCartBE in lstShoppingCartBE)
                    {
                        #region
                        sReadItemBody = new StreamReader(Server.MapPath("~/Punchout/OrderMessageItemBody.xml"));
                        strOIB = sReadItemBody.ReadToEnd();
                        sReadItemBody.Dispose();
                        sReadItemBody.Close();
                        sQuantity = objShoppingCartBE.Quantity.ToString(CultureInfo.InvariantCulture);
                        sCARRIERSERVICEID = "";
                        sItemDetailCurrency = sCurrency;
                        sItemDetailMoney = objShoppingCartBE.Price.ToString(CultureInfo.InvariantCulture);
                        sUnitOfMeasure = "EA";
                        sClassification = "49101602";// "80141605";
                        if (objStoreBE.StoreId == 462)
                        {
                            sClassification = "8014161603";
                        }
                        sExtrinsic = "";
                        sTaxCurrency = sCurrency;
                        sTaxMoney = "";
                        sTaxDescription = "";
                        string sProductName = string.Empty;
                        sItemDetailDescription = "";

                        sSupplierPartID = objShoppingCartBE.SKU; // Modified to SKU from Product Code SHRIGANESH 28 Oct 2016
                        sBASECOLOUR = Convert.ToString(objShoppingCartBE.BaseColorId);
                        sTRIMCOLOUR = Convert.ToString(objShoppingCartBE.TrimColorId);
                        sPRODUCTID = Convert.ToString(objShoppingCartBE.ProductId);
                        sSUPPLIERID = Convert.ToString(intSupplierId_OASIS);
                        sProductName = objShoppingCartBE.FurtherDescription;
                        sLEADTIME = "4";

                        strOIB = strOIB.Replace("$quantity$", sQuantity);
                        strOIB = strOIB.Replace("$SupplierPartID$", sSupplierPartID);
                        strOIB = strOIB.Replace("$CARRIERSERVICEID$", sCARRIERSERVICEID);
                        strOIB = strOIB.Replace("$BASECOLOUR$", sBASECOLOUR);
                        strOIB = strOIB.Replace("$TRIMCOLOUR$", sTRIMCOLOUR);
                        strOIB = strOIB.Replace("$PRODUCTID$", sPRODUCTID);
                        strOIB = strOIB.Replace("$SUPPLIERID$", sSUPPLIERID);
                        strOIB = strOIB.Replace("$LEADTIME$", sLEADTIME);
                        strOIB = strOIB.Replace("$ItemDetailCurrency$", sItemDetailCurrency);
                        strOIB = strOIB.Replace("$ItemDetailMoney$", sItemDetailMoney.Replace("$", "").Replace("€", "").Replace("£", ""));
                        strOIB = strOIB.Replace("$ItemDetailDescription$", Server.HtmlEncode(sProductName));
                        strOIB = strOIB.Replace("$UnitOfMeasure$", sUnitOfMeasure);
                        strOIB = strOIB.Replace("$Classification$", sClassification);
                        strOIB = strOIB.Replace("$Extrinsic$", sExtrinsic);
                        strOIB = strOIB.Replace("$TaxCurrency$", sTaxCurrency);
                        strOIB = strOIB.Replace("$TaxMoney$", sTaxMoney.Replace("$", "").Replace("€", "").Replace("£", ""));
                        strOIB = strOIB.Replace("$TaxDescription$", sTaxDescription);
                        sItemDetails += strOIB.ToString();
                        #endregion

                        if (bCustomPunchout == true)
                        {
                            #region "Added By Hardik to add node in XML for punchout"
                            sFreightDetails = "";
                            string sDeliveryCharge = "";
                            if (hidDeliveryCharge.Value == "1")
                            {
                                sDeliveryCharge = "STANDARD DELIVERY CHARGE";
                            }
                            else
                            {
                                sDeliveryCharge = "EXPRESS DELIVERY CHARGE";
                            }
                            string sFreightCurrency = sCurrency;
                            string sFreightUnitOfMeasure = "EA";
                            //string sFreightMoney = spnShippingPrice.InnerText.Replace("$", "").Replace("€", "").Replace("£", "").Trim();
                            string sFreightMoney = hidSpnShippingPrice.Value;
                            string sFreightClassification = "78102204";
                            strOFB = strOFB.Replace("$quantity$", "1");
                            strOFB = strOFB.Replace("$SupplierPartID$", "DELIVERY");
                            strOFB = strOFB.Replace("$SupplierPartAuxiliaryID$", "BrandAd");
                            strOFB = strOFB.Replace("$FreightCurrency$", sItemDetailCurrency);
                            strOFB = strOFB.Replace("$FreightMoney$", sFreightMoney);
                            //strOFB = strOFB.Replace("$Description$", "STANDARD DELIVERY CHARGE");
                            //strOFB = strOFB.Replace("$Description$", "STANDARD DELIVERY CHARGE");
                            strOFB = strOFB.Replace("$Description$", sDeliveryCharge);
                            strOFB = strOFB.Replace("$UnitOfMeasure$", sFreightUnitOfMeasure);
                            strOFB = strOFB.Replace("$Classification$", sFreightClassification);
                            sFreightDetails += strOFB.ToString();
                            #endregion
                        }
                    }


                    #region "For Normal Punchout"
                    if (bCustomPunchout == false)
                    {
                        strOMB = strOMB.Replace("$ItemDetails$", sItemDetails);
                    }
                    #endregion

                    #region "For Custom Punchout"
                    else
                    {
                        sItemDetails += sFreightDetails;
                        //strOMB = strOMB.Replace("$ItemDetails$", sItemDetails).Replace("$FreightDetails$", sFreightDetails);
                        strOMB = strOMB.Replace("$ItemDetails$", sItemDetails);

                    }
                    #endregion

                    GlobalFunctions.writePunchoutXML(strOMB);
                    //to send punchout order message
                    String sXML = strOMB.ToString();
                    string cXML_Form = "";
                    //Build The Post Shopping Cart cXML
                    cXML_Form = PunchoutBL.CreateCxmlForm(sURL, strOMB);
                    GlobalFunctions.writePunchoutLog(DateTime.Now.ToString());
                    GlobalFunctions.writePunchoutLog(cXML_Form);
                    //Hold it and create the form in the next page. 
                    Session["cXML_Form"] = cXML_Form;
                    Session["cXML_PostURL"] = sURL;
                    GlobalFunctions.writePunchoutLog("URL : " + sURL);

                    GlobalFunctions.writePunchoutLog("SendPunchoutOrderMessage call end");
                    #endregion
                }
                else { GlobalFunctions.writePunchoutLog("Session S_PUNCHOUTREQUEST has NO value"); }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Anoop Gupta  [Edited : Hardik Gohil]
        /// Date    : 8-8-2016
        /// Scope   : CalculateFreightCharges webmethod of the ShoppingCart_Payment page
        /// </summary>
        /// <param name="intCountryId"></param>                
        /// <returns>Freight charges</returns>
        [System.Web.Services.WebMethod]
        public static string CalculateFreightCharges(Int16 intCountryId)
        {
            string jsondata = string.Empty;
            try
            {
                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                lstFreightBE = CalculateFreight(intCountryId);
                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    //string strTotalTax = CalculateTax(intCountryId, lstFreightBE[0].SZPrice, lstFreightBE[0].EZPrice, 0);
                    //if (!string.IsNullOrEmpty(strTotalTax))
                    //{
                    //    string[] strTax = strTotalTax.Split('|');
                    //    if (strTax.Length > 0)
                    //    {
                    //        lstFreightBE[0].StandardZone = strTax[0]; //Total tax for standard zone
                    //        lstFreightBE[0].ExpressZone = strTax[1];//Total tax for express zone
                    //    }
                    //}
                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        if (lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).DisplayDutyMessage)
                            lstFreightBE[0].DisplayDutyMessage = true;
                    }
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsondata = jSearializer.Serialize(lstFreightBE);
                }

                return jsondata;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta [Edited : Hardik Gohil]
        /// Date    : 8-8-2016
        /// Scope   : Calculate freight charges for standard and express zone
        /// </summary>
        /// <param name="intCountryId"></param>
        /// <returns></returns>
        protected static List<FreightManagementBE> CalculateFreight(Int16 intCountryId)
        {
            try
            {
                FreightManagementBE objFreight = new FreightManagementBE();
                objFreight.Weight = objFreight.WeightUnits = float.Parse(Convert.ToString(HttpContext.Current.Session["ProductWeight"]));
                objFreight.WeightPerBox = float.Parse(ConfigurationManager.AppSettings["WeightPerBox"]);
                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    objFreight.CountryCode = lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).CountryCode;
                    string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).RegionCode;

                    if (strRegionCode == "EU")
                        objFreight.FreightRegionName = "EU Region";
                    else if (strRegionCode == "UK")
                        objFreight.FreightRegionName = "UK Region";
                    else
                        objFreight.FreightRegionName = "ROW";
                }
                objFreight.LanguageId = GlobalFunctions.GetLanguageId();

                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    //string FM = Convert.ToString(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_FreightMultiplier").FeatureValues[0].FeatureDefaultValue);
                    //string CM = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(s => s.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyConversionFactor);

                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";
                    //float avarage = float.Parse("0.0", NumberStyles.Any, ci);

                    //objFreight.FreightMultiplier = float.Parse(FM, NumberStyles.Any, ci);




                    objFreight.FreightMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_FreightMultiplier").FeatureValues[0].FeatureDefaultValue), NumberStyles.Any, ci);
                    objFreight.CurrencyMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(s => s.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyConversionFactor), NumberStyles.Any, ci);



                    //objFreight.FreightMultiplier = float.Parse(FM.ToString(CultureInfo.InvariantCulture));
                    //objFreight.CurrencyMultiplier = CM.ToString("##,###,##0.#0");
                    //  objFreight.FreightMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_FreightMultiplier").FeatureValues[0].FeatureDefaultValue));
                    //  objFreight.CurrencyMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(s => s.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyConversionFactor));
                }

                List<FreightManagementBE.FreightMultiplerCurrency> lstAllFreightMultiplerCurrency;
                lstAllFreightMultiplerCurrency = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(0, "CM");
                if (lstAllFreightMultiplerCurrency != null)
                {
                    if (lstAllFreightMultiplerCurrency.Count > 0)
                    {
                        decimal fFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.CurrencyID == GlobalFunctions.GetCurrencyId()).CValue;
                        objFreight.FreightMultiplier = float.Parse(Convert.ToString(fFreightMultiplier));
                    }
                }

                List<object> lstShoppingBE = new List<object>();
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                double OrderItemTotal = 0;
                if (lstUser != null && lstUser.UserId != 0)
                {
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                }
                else
                {
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                }
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                    lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    for (int i = 0; i < lstShoppingBE.Count; i++)
                    {
                        double OrderItemAddTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                        //if (DiscountPercentage == 0)
                        OrderItemTotal += Convert.ToDouble(Convert.ToDecimal(OrderItemAddTotal));
                        //else
                        //    OrderItemTotal = GlobalFunctions.DiscountedAmount(OrderItemTotal, DiscountPercentage);
                    }
                }

                objFreight.OrderValue = OrderItemTotal;
                List<FreightManagementBE> lstFreight = new List<FreightManagementBE>();
                lstFreight = FreightManagementBL.GetFreightDetails(objFreight);


                FreightManagementBE CurrFreightConfig = new FreightManagementBE();

                return lstFreight;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
    }
}