﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Configuration;
using PWGlobalEcomm.Stock;
using Microsoft.Security.Application;
using System.Threading;
using System.Web.Script.Serialization;
using PWGlobalEcomm.BusinessEntity;
using System.IO;
using System.Net.Mail;
using PWGlobalEcomm.GlobalUtilities;
//using PWGlobalEcomm.Stock;

namespace Presentation
{
    public class ShoppingCart_Basket : BasePage
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Repeater rptBasketListing;
        protected global::System.Web.UI.WebControls.Button btnGuestCheckOut;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvEmptyBasket, hPageHeading, divBasketRegisteredLogin, divBasketGuestLogin, divSocialLogin; //Added By Snehal - 04 10 2016
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnBasket_InvalidEmailPassword, hdnBasket_AddPreviousProducts, hdnBasket_ErrorUpdatingBasket, hdnBasket_PreviousProductAdded, hdnMin, hdnMax, hdnFirstPriceBrk;
        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        Int32 intUserId = 0;
        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        public decimal dTotalPrice = 0;
        string strUserSessionId = string.Empty;
        public bool bBackOrderAllowed = false;
        public bool isEmailValid = false; //Added By Snehal on 16 09 2016		
        public bool bBASysStore = false;
        public string strCurrencySymbol = string.Empty;
        public string strProductText, strTitleText, strQuantityText, strUnitCost, strLineTotal, strRemoveText, strTotalText, strContinueShoppingText,
            strCheckoutText, strRemoveItemsFromCart, strQuantityNotZero, strMinMaxQuantity, strQuantityNotMoreThanStock,
            strProductRemovedFromCart, strBasketErrorWhileRemovingProduct, strInvalidQuantityMessage, strPageTitle, strPageMetaKeywords,strGenericPointsText,
            strPageMetaDescription, strCommonErrorMessageWhileUpdatingBasket = string.Empty;
        /* Added By Snehal on 16 09 2016 */
        public string MsgEmailBlank, MsgPasswordBlank, MsgInvalidEmail, MsgInvalidUsenamePass, stror, MinPriceBreak;
        public bool bIsPointsEnabled = false;
        Int32 intUserPoints = 0;

        //-- Added By snehal 23 09 2016
        public int intStoreFeatureDefaultValue;
        public bool IsMaxQtyEnabled = false;
        public string strCheckDefaultQuantityValue = string.Empty;
        // -- End

        //-- Added By snehal 03 10 2016
        public string accessToken = "";
        public string CASE = "";
        public bool Querystring = true;
        public static bool CheckExist = false;
        string message = "";
        // -- End

        //Added By Hardik on "22/FEB/2017"
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divBasketNote;

        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();

        /*Sachin Chauhan Start : 27 03 2016 : varaible declarations for resource languages*/
        public string Basket_HaveAnAccount, Basket_Email, Basket_Password, Basket_BeforeYouCheckout, Basket_SignIntoYourAccount,
                      Basket_ForgotPassword, Basket_NewToOurSite, Basket_OrderConfirmationMessage, Basket_CheckoutAsGuest,
                      Basket_InvalidEmailPassword, Basket_AddPreviousProducts, Basket_ErrorUpdatingBasket, Basket_PreviousProductAdded;
        /*Sachin Chauhan Emd : 27 03 2016*/
        Int16 iUserTypeID = 1;
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 09-09-15
        /// Scope   : Page_Load of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                GoogleEmailBE oUser = new GoogleEmailBE();
                UserTypeMappingBE objUserTypeMappingBE = new UserTypeMappingBE();
                bool validation = true;

                

                #region For Google Plus API - Added By Snehal  03 10 2016
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_BasketLoginPanel").FeatureValues[0].IsEnabled)
                {
                    if (Session["AccessToken"] == null)
                    {
                        if (Request.QueryString["Case"] == null)
                        {
                            Querystring = false;
                        }
                        else
                        {
                            Querystring = true;
                            oUser.email = Request["email"].ToString();
                            oUser.given_name = Request["firstname"].ToString();
                            oUser.family_name = Request["lastname"].ToString();
                            oUser.name = Request["name"].ToString();

                            if (oUser.email != null)
                            {
                                Session["APIEmailId"] = oUser.email;
                                Session["FirstName"] = oUser.given_name;
                                Session["LastName"] = oUser.family_name;
                                Session["FullName"] = oUser.name;
                                Session["AccessToken"] = true;

                                #region ADDED BY SHRIGANESH FOR USER TYPE ID MAPPING

                                string host1 = "";
                                MailAddress address = new MailAddress(oUser.email.Trim());
                                host1 = address.Host;
                                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                                DictionaryInstance.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                                objUserTypeMappingBE = UserTypesBL.getCollectionItemUserTypeMappingbyCId(Constants.USP_UserTypeMappingDetailsByCID, DictionaryInstance, true);
                                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                                if (objUserTypeMasterOptions != null)
                                {
                                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                                    {
                                        #region "Email"
                                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                                        {

                                            UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidation = objUserTypeMappingBE.lstUserTypeEmailValidation.FirstOrDefault(x => x.EmailId.ToLower() == oUser.email.Trim());
                                            UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidationDomain = objUserTypeMappingBE.lstUserTypeEmailValidation.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.Domain.ToLower() == host1.ToLower());
                                            UserTypeMappingBE.UserTypeDetails objRegUserTypeDetails = objUserTypeMappingBE.lstUserTypeDetails.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.IsWhiteList == false);
                                            if (objRegUserTypeEmailValidation != null)
                                            {
                                                validation = false;
                                                if (objRegUserTypeDetails != null)
                                                {
                                                    iUserTypeID = objRegUserTypeDetails.UserTypeID;
                                                }

                                            }
                                            else if (objRegUserTypeEmailValidationDomain != null)
                                            {
                                                validation = false;
                                                if (objRegUserTypeDetails != null)
                                                {
                                                    iUserTypeID = objRegUserTypeDetails.UserTypeID;
                                                }

                                            }

                                            if (objRegUserTypeDetails != null)
                                            {
                                                iUserTypeID = objRegUserTypeDetails.UserTypeID;
                                            }

                                            if (objRegUserTypeEmailValidation != null && objRegUserTypeEmailValidationDomain != null)
                                            {
                                                GlobalFunctions.ShowModalAlertMessages(this.Page, "Invalid Domain/Email ID ", AlertType.Warning);
                                                return;
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion

                                #region Insert Google SignIn User Into DataBase

                                #region
                                int i = 0;
                                int IsExist;
                                try
                                {
                                    UserBE objBE = new UserBE();
                                    UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                                    objBE.EmailId = oUser.email;
                                    IsExist = UserBL.IsExistUser(Constants.USP_IsExistUserAPI, true, objBE);
                                    if (IsExist == 0)
                                    {
                                        objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);
                                        objBE.FirstName = oUser.given_name;
                                        objBE.LastName = oUser.family_name;
                                        objBE.PredefinedColumn1 = oUser.name;
                                        objBE.PredefinedColumn2 = "";
                                        objBE.PredefinedColumn3 = "";
                                        objBE.PredefinedColumn4 = "";
                                        objBE.PredefinedColumn5 = "";
                                        objBE.PredefinedColumn6 = "";
                                        objBE.PredefinedColumn7 = "";
                                        objBE.PredefinedColumn8 = "12";
                                        objBE.PredefinedColumn9 = "";
                                        objBE.UserDeliveryAddress.Add(obj);
                                        objBE.IPAddress = GlobalFunctions.GetIpAddress();
                                        objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                                        objBE.IsGuestUser = true;
                                        objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                                        objBE.IsMarketing = false;
                                        objBE.UserTypeID = iUserTypeID;// Added by SHRIGANESH 22 Sept 2016    
                                        objBE.Points = -1; // Added by ShriGanesh 26 April 2017
                                        Exceptions.WriteInfoLog("Before calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                                        i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                                        Exceptions.WriteInfoLog("After calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                                        objBE.UserId = Convert.ToInt16(i);

                                        UserBE objBEs = new UserBE();
                                        UserBE objUser = new UserBE();
                                        objBEs.EmailId = oUser.email.Trim();
                                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                                        //Below if condition is added according to harley guestcheckout tax calculation July 28,2016
                                        if (objUser != null || objUser.EmailId != null)
                                        {
                                            HttpContext.Current.Session["User"] = objUser;
                                            GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                                            GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                                            Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                            Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                            Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                            Session["GuestUser"] = null;
                                        }
                                    }
                                    else
                                    {
                                        objBE.UserTypeID = iUserTypeID;
                                        i = UserBL.UpdateReferrelUserTypeId(Constants.USP_UpdateUserTypeForReferrelURL, true, objBE);
                                        objBE.UserId = Convert.ToInt16(i);
                                        UserBE objBEs = new UserBE();
                                        UserBE objUser = new UserBE();
                                        objBEs.EmailId = oUser.email.Trim();
                                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                                        if (objUser != null || objUser.EmailId != null)
                                        {
                                            HttpContext.Current.Session["User"] = objUser;
                                            GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                                            GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                                            Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                            Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                            Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                            Session["GuestUser"] = null;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Exceptions.WriteExceptionLog(ex);
                                }
                                #endregion
                                #endregion                               
                            }
                            if (!validation)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, message, "/index", AlertType.Warning);
                            }
                            else
                            {
                                Response.Redirect(host + "Checkout");
                            }
                           
                        }
                    }
                }
                #endregion

                ProductBE objProductBE = new ProductBE();
                if (Session["objCurrentProductBE"] != null)

                    objProductBE = Session["objCurrentProductBE"] as ProductBE;
               // hdnFirstPriceBrk.Value = Convert.ToString(objProductBE.PropGetAllPriceBreakDetails[0].BreakFrom);

                if (IsPostBack)
                {
                    if (Session["PrevLanguageId"] == null)
                    {
                        Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                        BindDetails();
                    }
                    else if (!Session["PrevLanguageId"].ToString().To_Int32().Equals(GlobalFunctions.GetLanguageId().To_Int32()))
                    {
                        Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                        BindDetails();
                    }
                    #region Added by Snehal 16-09-2016 for Email id field validation

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CF_LoginId").FeatureValues[0].IsEnabled)
                    {
                        isEmailValid = false;
                    }
                    else
                    {
                        isEmailValid = true;
                    }
                    #endregion

                    #region Added By snehal 23 09 2016

                    if (objStoreBE != null)
                    {
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_MaxAllowQty").FeatureValues[0].IsEnabled)
                        {
                            IsMaxQtyEnabled = true;
                            int intFeatureId = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_MaxAllowQty").FeatureValues[0].FeatureId;
                            intStoreFeatureDefaultValue = Convert.ToInt32(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureId == intFeatureId).FeatureValues[0].FeatureDefaultValue);
                        }
                        else
                        {
                            IsMaxQtyEnabled = false;
                            intStoreFeatureDefaultValue = 0;
                        }
                    }
                    #endregion

                    #region Added by Snehal 04 10 2016 - To Check Either Social or store Login to make visible

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_BasketLoginPanel").FeatureValues[0].IsEnabled && objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_BasketLoginPanel").FeatureValues[1].IsEnabled) // To check Social Media Login
                    {
                        divSocialLogin.Visible = true;
                        divBasketGuestLogin.Visible = true;
                        divBasketRegisteredLogin.Visible = true;
                    }

                    else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_BasketLoginPanel").FeatureValues[0].IsEnabled) // To check Social Media Login
                    {
                        divSocialLogin.Visible = true;
                        divBasketGuestLogin.Visible = false;
                        divBasketRegisteredLogin.Visible = false;
                    }
                    else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_BasketLoginPanel").FeatureValues[1].IsEnabled) // To check store login
                    {
                        divSocialLogin.Visible = false;
                        divBasketRegisteredLogin.Visible = true;
                        divBasketGuestLogin.Visible = true;
                    }
                    #endregion
                }
                else
                {
                    BindDetails();
                }

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #region Added By Snehal 03 10 2016
        private void CheckUserExist(string emailid)
        {
            int IsExist;
            UserBE objUser = new UserBE();
            UserBE objBE = new UserBE();
            objBE.EmailId = emailid;
            IsExist = UserBL.IsExistUser(Constants.USP_IsExistUserAPI, true, objBE);

            if (IsExist == 1)
            {
                UserBE objBEs = new UserBE();
                objBEs.EmailId = emailid;
                objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                if (objUser != null || objUser.EmailId != null)
                {
                    Login_UserLogin.UpdateBasketSessionProducts(objUser, 'Y');
                    HttpContext.Current.Session["User"] = objUser;
                    ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:Checkout();</script>");
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:LoginAPI();</script>");
            }
        }

        public string GetGoogleUserJSON(string access_token)
        {
            string url = "https://www.googleapis.com/userinfo/email?alt=json";

            WebClient wc = new WebClient();
            wc.Headers.Add("Authorization", "OAuth " + accessToken);
            Stream data = wc.OpenRead(url);
            StreamReader reader = new StreamReader(data);
            string retirnedJson = reader.ReadToEnd();
            data.Close();
            reader.Close();
            return retirnedJson;
        }

        public string GetGoogleUserinfoJSON(string access_token)
        {
            string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json";

            WebClient wc = new WebClient();
            wc.Headers.Add("Authorization", "OAuth " + accessToken);
            Stream data = wc.OpenRead(url);
            StreamReader reader = new StreamReader(data);
            string retirnedJson = reader.ReadToEnd();
            data.Close();
            reader.Close();
            return retirnedJson;
        }
        #endregion


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 20-10-15
        /// Scope   : BindResourceData of the basket page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();

                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        divBasketNote.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SC_divBasketNote").ResourceValue;
                        hPageHeading.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Page_Heading").ResourceValue;
                        strProductText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Product_Text").ResourceValue;
                        strTitleText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Title_Text").ResourceValue;
                        strQuantityText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Text").ResourceValue;
                        strUnitCost = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Unit_Cost_Text").ResourceValue;
                        strLineTotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Line_Total_Text").ResourceValue;
                        strRemoveText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Remove_Text").ResourceValue;
                        strTotalText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Total_Text").ResourceValue;
                        strContinueShoppingText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Continue_Shopping_Text").ResourceValue;
                        strCheckoutText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Checkout_Text").ResourceValue;
                        dvEmptyBasket.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_NoProductsFound_Message").ResourceValue;
                        strRemoveItemsFromCart = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Remove_Items_From_Cart_Message").ResourceValue;
                        strQuantityNotZero = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Not_Zero_Message").ResourceValue;
                        strMinMaxQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Min_Max_Quantity_Message").ResourceValue;
                        strQuantityNotMoreThanStock = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Not_More_Than_Stock_Message").ResourceValue;
                        strProductRemovedFromCart = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Product_Removed_From_Cart_Message").ResourceValue;
                        strBasketErrorWhileRemovingProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Error_While_Removing_Product_Message").ResourceValue;
                        strInvalidQuantityMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Invalid_Quantity_Message").ResourceValue;
                        strPageTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Page_Title").ResourceValue;
                        strPageMetaKeywords = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Page_Meta_Keywords").ResourceValue;
                        strPageMetaDescription = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Page_Meta_Description").ResourceValue;
                        strCommonErrorMessageWhileUpdatingBasket = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Common_Error_Message_While_Updating_Basket").ResourceValue;
                        Basket_HaveAnAccount = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_HaveAnAccount").ResourceValue;
                        Basket_Email = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Email").ResourceValue;
                        Basket_Password = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Password").ResourceValue;
                        Basket_BeforeYouCheckout = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_BeforeYouCheckout").ResourceValue;
                        Basket_SignIntoYourAccount = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_SignIntoYourAccount").ResourceValue;
                        Basket_ForgotPassword = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_ForgotPassword").ResourceValue;
                        Basket_NewToOurSite = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_NewToOurSite").ResourceValue;
                        Basket_OrderConfirmationMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_OrderConfirmationMessage").ResourceValue;
                        Basket_CheckoutAsGuest = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_CheckoutAsGuest").ResourceValue;
                        hdnBasket_InvalidEmailPassword.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_InvalidEmailPassword").ResourceValue;
                        hdnBasket_AddPreviousProducts.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_AddPreviousProducts").ResourceValue;
                        hdnBasket_ErrorUpdatingBasket.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_ErrorUpdatingBasket").ResourceValue;
                        hdnBasket_PreviousProductAdded.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_PreviousProductAdded").ResourceValue;
                        MsgPasswordBlank = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password_Req_Message").ResourceValue;
                        MsgEmailBlank = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Valid_Email_Address_Message").ResourceValue;
                        MsgInvalidEmail = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ValidEmail_Message").ResourceValue;
                        MsgInvalidUsenamePass = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_InvalidUsernamePassword_Message").ResourceValue;
                        stror = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "RecieveCommunication_or_Text").ResourceValue;
                        strGenericPointsText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Points_Text").ResourceValue;
                        strCheckDefaultQuantityValue = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Min_Max_Quantity_Message").ResourceValue;
                        btnGuestCheckOut.Text = Basket_CheckoutAsGuest;
                        MinPriceBreak = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Min_Price_Break_Message").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 10-09-16
        /// Scope   : rptBasketListing_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptBasketListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header)
                {
                    HtmlGenericControl divProductText = (HtmlGenericControl)e.Item.FindControl("divProductText");
                    HtmlGenericControl divTitleText = (HtmlGenericControl)e.Item.FindControl("divTitleText");
                    HtmlGenericControl divQuantityText = (HtmlGenericControl)e.Item.FindControl("divQuantityText");
                    HtmlGenericControl divUnitCostText = (HtmlGenericControl)e.Item.FindControl("divUnitCostText");
                    HtmlGenericControl divLineTotalText = (HtmlGenericControl)e.Item.FindControl("divLineTotalText");
                    HtmlGenericControl divRemoveText = (HtmlGenericControl)e.Item.FindControl("divRemoveText");
                    divProductText.InnerHtml = strProductText;
                    divTitleText.InnerHtml = strTitleText;
                    divQuantityText.InnerHtml = strQuantityText;
                    divUnitCostText.InnerHtml = strUnitCost;
                    divLineTotalText.InnerHtml = strLineTotal;
                    divRemoveText.InnerHtml = strRemoveText;
                }
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl dvUnitPrice = (HtmlGenericControl)e.Item.FindControl("dvUnitPrice");
                    HtmlGenericControl dvBasketRowContainer = (HtmlGenericControl)e.Item.FindControl("dvBasketRowContainer");
                    HtmlGenericControl dvTotalPrice = (HtmlGenericControl)e.Item.FindControl("dvTotalPrice");
                    HtmlAnchor aRemoveProducts = (HtmlAnchor)e.Item.FindControl("aRemoveProducts");
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)e.Item.FindControl("hdnShoppingCartProductId");
                    Literal ltrSKUName = (Literal)e.Item.FindControl("ltrSKUName");
                    Literal ltrProductName = (Literal)e.Item.FindControl("ltrProductName");
                    HtmlInputText txtQuantity = (HtmlInputText)e.Item.FindControl("txtQuantity");
                    HtmlInputHidden hdnIsBackOrderAllowed = (HtmlInputHidden)e.Item.FindControl("hdnIsBackOrderAllowed");
                    HtmlInputHidden hdnMinimumOrderQuantity = (HtmlInputHidden)e.Item.FindControl("hdnMinimumOrderQuantity");
                    HtmlInputHidden hdnMaximumOrderQuantity = (HtmlInputHidden)e.Item.FindControl("hdnMaximumOrderQuantity");
                    LinkButton lnkDeleteProduct = (LinkButton)e.Item.FindControl("lnkDeleteProduct");
                    lnkDeleteProduct.Attributes.Add("onclick", lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Remove_Items_From_Cart_Message").ResourceValue);

                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";

                    double dPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price"));
                    dvUnitPrice.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(Convert.ToDecimal(dPrice.ToString("##,###,##0.#0"))), "", intLanguageId);
                    dvTotalPrice.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(Convert.ToDecimal(dPrice.ToString("##,###,##0.#0")) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", intLanguageId);// +"</span>" + GlobalFunctions.PointsText(intLanguageId);
                    dTotalPrice += Convert.ToDecimal(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(dPrice.ToString("##,###,##0.#0")) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", intLanguageId));
                     aRemoveProducts.Attributes.Add("rel", Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId")));
                    dvBasketRowContainer.ID = "dvBasketRowContainer" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId"));
                    hdnShoppingCartProductId.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId"));
                    ltrSKUName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SKU"));
                    ltrProductName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductName"));
                    txtQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                    hdnIsBackOrderAllowed.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "IsBackOrderAllowed"));
                    hdnMinimumOrderQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MinimumOrderQuantity"));
                    hdnMin.Value = hdnMinimumOrderQuantity.Value; // Added By snehal 23 09 2016
                    hdnMaximumOrderQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MaximumOrderQuantity"));
                    hdnMax.Value = hdnMaximumOrderQuantity.Value;  // Added By snehal 23 09 2016
                    try
                    {
                        if (bBackOrderAllowed == false)
                        {
                            if (bBASysStore)
                            {
                                Int32 intProductid_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BASYSProductId")),
                                    intBaseColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BaseColorId")),
                                    intTrimColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "TrimColorId"));
                                Stock objStock = new Stock();
                                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                                    ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                                objStock.Credentials = objNetworkCredentials;
                                StockDetails objStockDetails = new StockDetails();
                                objStockDetails = objStock.LevelEnquiry(intProductid_OASIS, intBaseColorId_OASIS, intTrimColorId_OASIS);

                                if (objStockDetails != null)
                                {
                                    HtmlInputHidden hdnStockStatus = (HtmlInputHidden)e.Item.FindControl("hdnStockStatus");
                                    hdnStockStatus.Value = Convert.ToString(objStockDetails.StockLevel);
                                }
                            }
                            else
                            {
                                HtmlInputHidden hdnStockStatus = (HtmlInputHidden)e.Item.FindControl("hdnStockStatus");
                                hdnStockStatus.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Inventory"));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                }
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlGenericControl dvTotal = (HtmlGenericControl)e.Item.FindControl("dvTotal");
                    HtmlGenericControl divTotalText = (HtmlGenericControl)e.Item.FindControl("divTotalText");
                    HtmlAnchor aCheckOut = (HtmlAnchor)e.Item.FindControl("aCheckOut");
                    HtmlGenericControl Divor = (HtmlGenericControl)e.Item.FindControl("Divor");
                    HtmlGenericControl DivPoint = (HtmlGenericControl)e.Item.FindControl("DivPoint");
                    HtmlGenericControl Divhidden = (HtmlGenericControl)e.Item.FindControl("Divhidden");
                    HtmlAnchor aContinueShopping = (HtmlAnchor)e.Item.FindControl("aContinueShopping");
                    dvTotal.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + Convert.ToString(dTotalPrice);// + "</span>" + GlobalFunctions.PointsText(intLanguageId);
                    divTotalText.InnerHtml = strTotalText;
                    aContinueShopping.InnerHtml = strContinueShoppingText;
                    aCheckOut.InnerHtml = strCheckoutText;
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled)
                    {
                        Double PointValues = 0;
                        CurrencyBE objCurrencyCD = new CurrencyBE();
                        List<CurrencyBE> objCurrencyBE = null;
                        objCurrencyBE = CurrencyBL.GetCurrencyPointInvoiceDetails();
                        for (int i = 0; i < objCurrencyBE.Count; i++)
                        {
                            if (objCurrencyBE[i].CurrencySymbol.Trim() == GlobalFunctions.GetCurrencySymbol())
                            {
                                PointValues = double.Parse(objCurrencyBE[i].PointValue.ToString());
                            }
                        }
                        Divor.InnerHtml = stror;
                        DivPoint.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dTotalPrice), GlobalFunctions.GetCurrencySymbol(), intLanguageId);
                        Divhidden.InnerHtml = Convert.ToString(PointValues);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 11-09-15
        /// Scope   : aCheckOut_ServerClick of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void aCheckOut_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlAnchor aButton = sender as HtmlAnchor;
                BindResourceData();
                #region Region added for geeting userid
                //Update quantity
                string strMessages = string.Empty;
                string strShoppingCartProductIds = string.Empty;

                if (Session["User"] != null)
                {
                    UserBE lst = new UserBE();
                    lst = Session["User"] as UserBE;
                    intUserId = lst.UserId;
                    intUserPoints = lst.Points;
                }
                else
                {
                    strUserSessionId = Session.SessionID;
                }
                #endregion
                #region "Points"
                if (bIsPointsEnabled && intUserId > 0)
                {
                    List<object> lstObjShoppingBE = new List<object>();
                    lstObjShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId);
                    List<ShoppingCartBE> lstShoppingBE = new List<ShoppingCartBE>();
                    lstShoppingBE = lstObjShoppingBE.Cast<ShoppingCartBE>().ToList();

                    Int32 intPoints = 0;
                    for (int i = 0; i < rptBasketListing.Items.Count; i++)
                    {
                        HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnShoppingCartProductId");
                        HtmlInputText txtQuantity = (HtmlInputText)rptBasketListing.Items[i].FindControl("txtQuantity");
                        Literal ltrSKUName = (Literal)rptBasketListing.Items[i].FindControl("ltrSKUName");
                        Literal ltrProductName = (Literal)rptBasketListing.Items[i].FindControl("ltrProductName");
                        if (string.IsNullOrEmpty(txtQuantity.Value.Trim()))
                        {
                            strMessages += strInvalidQuantityMessage + ltrSKUName.Text + "\n";
                        }
                        else
                        {
                            Int64 aQty;
                            try
                            {
                                aQty = Convert.ToInt64(txtQuantity.Value.Trim(), System.Globalization.CultureInfo.CurrentCulture);
                                if (aQty > 0)
                                {
                                    if (IsMaxQtyEnabled == true)
                                    {
                                        if (aQty > intStoreFeatureDefaultValue)
                                        {
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strCheckDefaultQuantityValue.Replace("{min}", hdnMin.Value).Replace("{max}", intStoreFeatureDefaultValue.ToString()), AlertType.Warning);
                                            return;
                                        }
                                        else
                                        {
                                            intPoints += Convert.ToInt32(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstShoppingBE.FirstOrDefault(x => x.ShoppingCartProductId == Convert.ToInt32(hdnShoppingCartProductId.Value)).Price * Convert.ToInt32(txtQuantity.Value.Trim())), "", intLanguageId));
                                        }
                                    }
                                    else
                                    {
                                        if (aQty > Convert.ToInt32(hdnMax.Value) && aQty < Convert.ToInt32(hdnMin))
                                        {
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strCheckDefaultQuantityValue.Replace("{min}", hdnMin.Value).Replace("{max}", hdnMax.Value), AlertType.Warning);
                                            return;
                                        }
                                        else
                                        {
                                            intPoints += Convert.ToInt32(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstShoppingBE.FirstOrDefault(x => x.ShoppingCartProductId == Convert.ToInt32(hdnShoppingCartProductId.Value)).Price * Convert.ToInt32(txtQuantity.Value.Trim())), "", intLanguageId));
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                                strMessages += strInvalidQuantityMessage + ltrSKUName.Text + "\n";
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(strMessages))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strMessages, AlertType.Warning);
                        return;
                    }
                }
                #endregion
                for (int i = 0; i < rptBasketListing.Items.Count; i++)
                {
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnShoppingCartProductId");
                    HtmlInputText txtQuantity = (HtmlInputText)rptBasketListing.Items[i].FindControl("txtQuantity");
                    Literal ltrSKUName = (Literal)rptBasketListing.Items[i].FindControl("ltrSKUName");
                    Literal ltrProductName = (Literal)rptBasketListing.Items[i].FindControl("ltrProductName");
                    if (string.IsNullOrEmpty(txtQuantity.Value.Trim()))
                    {
                        strMessages += strInvalidQuantityMessage + ltrSKUName.Text + "\n";
                    }
                    else
                    {
                        Int64 aQty;
                        try
                        {
                            aQty = Convert.ToInt64(txtQuantity.Value.Trim(), System.Globalization.CultureInfo.CurrentCulture);
                            if (aQty > 0)
                            {
                                if (IsMaxQtyEnabled == true)
                                {
                                    if (aQty > intStoreFeatureDefaultValue)
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strCheckDefaultQuantityValue.Replace("{min}", hdnMin.Value).Replace("{max}", intStoreFeatureDefaultValue.ToString()), AlertType.Warning);
                                        return;
                                    }
                                    else
                                    {
                                        strShoppingCartProductIds += Sanitizer.GetSafeHtmlFragment(hdnShoppingCartProductId.Value) + "|" + Sanitizer.GetSafeHtmlFragment(txtQuantity.Value) + ";";
                                    }
                                }
                                else
                                {
                                    if (aQty > Convert.ToInt32(hdnMax.Value) && aQty < Convert.ToInt32(hdnMin))
                                    {
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strCheckDefaultQuantityValue.Replace("{min}", hdnMin.Value).Replace("{max}", hdnMax.Value), AlertType.Warning);
                                        return;
                                    }
                                    else
                                    {
                                        strShoppingCartProductIds += Sanitizer.GetSafeHtmlFragment(hdnShoppingCartProductId.Value) + "|" + Sanitizer.GetSafeHtmlFragment(txtQuantity.Value) + ";";
                                    }
                                }
                            }
                            else
                            {
                                strMessages += strInvalidQuantityMessage + ltrSKUName.Text + "\n";
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            strMessages += strInvalidQuantityMessage + ltrSKUName.Text + "\n";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(strMessages))
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strMessages, AlertType.Warning);
                    return;
                }
                else
                {
                    List<object> lstShoppingBE = new List<object>();
                    lstShoppingBE = ShoppingCartBL.UpdateBasketData(strShoppingCartProductIds, intUserId, strUserSessionId);

                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        List<ShoppingCartBE> lstNewShoppingCart = new List<ShoppingCartBE>();
                        lstNewShoppingCart = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                        if (lstNewShoppingCart[0].ShoppingCartProductId != 0)
                        {
                            if (aButton.ID == "aContinueShopping")
                                Response.RedirectToRoute("Index");
                            else
                            {
                                if (intUserId > 0)
                                    Response.Redirect(host + "checkout");
                                else
                                    if (Session["AccessToken"] != null)
                                    {
                                        ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:LoginAPI();</script>");
                                        Querystring = true;
                                    }
                                    else
                                    {
                                        ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:LoginUser();</script>");
                                    }
                            }
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strCommonErrorMessageWhileUpdatingBasket, AlertType.Warning);
                        }
                    }
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 11-09-15
        /// Scope   : RemoveProductsFromBasket webmethod of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="ShoppingCartProductId"></param>                
        /// <returns name="bool"></returns>
        [System.Web.Services.WebMethod]
        public static bool RemoveProductsFromBasket(Int32 ShoppingCartProductId)
        {
            try
            {
                string strUserSessionId = string.Empty;
                Int32 intUserId = 0;
                if (HttpContext.Current.Session["User"] != null)
                {
                    UserBE lst = new UserBE();
                    lst = HttpContext.Current.Session["User"] as UserBE;
                    intUserId = lst.UserId;
                }
                else
                {
                    strUserSessionId = HttpContext.Current.Session.SessionID;
                }

                List<object> lstShoppingBE = new List<object>();
                lstShoppingBE = ShoppingCartBL.RemoveProductsFromBasket(ShoppingCartProductId, intUserId, strUserSessionId);
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShoppingCart = new List<ShoppingCartBE>();
                    lstNewShoppingCart = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    if (lstNewShoppingCart[0].ShoppingCartProductId != 0)
                        return true;
                    else
                        return false;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 27-10-15
        /// Scope   : UpdateBasketProducts webmethod of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="ShoppingCartProductId"></param>                
        /// <returns name="bool"></returns>
        [System.Web.Services.WebMethod]
        public static bool UpdateBasketProducts(string action)
        {
            bool bStatus = false;
            try
            {
                /*Sachin Chauhan Start : 2-12-15 : Commented lines of code & replaced with below lines*/
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                Login_UserLogin.UpdateBasketSessionProducts(lstUser, action.ToUpper().ToCharArray()[0]);
                bStatus = true;
                return bStatus;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return bStatus;
            }
        }

        protected void lnkDeleteProduct_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkDeleteProduct = (LinkButton)sender;
                Int32 ShoppingCartProductId = Convert.ToInt32(lnkDeleteProduct.CommandArgument);
                string strUserSessionId = string.Empty;
                Int32 intUserId = 0;
                if (HttpContext.Current.Session["User"] != null)
                {
                    UserBE lst = new UserBE();
                    lst = HttpContext.Current.Session["User"] as UserBE;
                    intUserId = lst.UserId;
                }
                else
                {
                    strUserSessionId = HttpContext.Current.Session.SessionID;
                }

                List<object> lstShoppingBE = new List<object>();
                lstShoppingBE = ShoppingCartBL.RemoveProductsFromBasket(ShoppingCartProductId, intUserId, strUserSessionId);
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShoppingCart = new List<ShoppingCartBE>();
                    lstNewShoppingCart = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                    BindBasket();
                    if (lstNewShoppingCart[0].ShoppingCartProductId != 0)
                    {
                        strRemoveItemsFromCart = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Product_Removed_From_Cart_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strRemoveItemsFromCart, Request.RawUrl, AlertType.Success);
                    }
                    else
                    {
                        strRemoveItemsFromCart = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Error_While_Removing_Product_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strRemoveItemsFromCart, AlertType.Failure);
                    }
                }
                else
                {
                    strRemoveItemsFromCart = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Error_While_Removing_Product_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strRemoveItemsFromCart, AlertType.Failure);
                }

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "Basket");
                    if (lstMetaTags.Count > 0)
                    {
                        Page.Title = lstMetaTags[0].MetaContentTitle;
                        Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                        Page.MetaDescription = lstMetaTags[0].MetaDescription;
                    }
                    else
                    {
                        Page.Title = "";
                        Page.MetaKeywords = "";
                        Page.MetaDescription = "";
                    }
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindBasket()
        {
            List<object> lstShoppingBE = new List<object>();
            lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId);
            if (lstShoppingBE != null && lstShoppingBE.Count > 0)
            {
                rptBasketListing.DataSource = lstShoppingBE;
                rptBasketListing.DataBind();
            }
            else
            {
                rptBasketListing.DataSource = null;
                rptBasketListing.DataBind();
            }
        }

        protected void BindDetails()
        {

            #region
            Session["Amount"] = null;
            #region "added by Sripal
            if (Session["S_ISPUNCHOUT"] != null)
            {
                if (Convert.ToBoolean(Session["S_ISPUNCHOUT"]))
                {
                    Response.RedirectToRoute("BasketPagePunchout");
                    return;
                }
            }
            #endregion
            if (Session["User"] != null)
            {
                UserBE lst = new UserBE();
                lst = Session["User"] as UserBE;
                intUserId = lst.UserId;
                intUserPoints = lst.Points;
            }
            else
            {
                strUserSessionId = Session.SessionID;
            }
            bIsPointsEnabled = GlobalFunctions.IsPointsEnbled();
            intCurrencyId = GlobalFunctions.GetCurrencyId();
            intLanguageId = GlobalFunctions.GetLanguageId();
            strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();

            if (GlobalFunctions.IsPointsEnbled())
                strCurrencySymbol = "";

            BindResourceData();
            #region
            List<object> lstShoppingBE = new List<object>();
            lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId);

            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                bBackOrderAllowed = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "OD_AllowBackOrder").FeatureValues[0].IsEnabled;
                bBASysStore = objStoreBE.IsBASYS;
            }

            if (lstShoppingBE != null && lstShoppingBE.Count > 0)
            {
                rptBasketListing.DataSource = lstShoppingBE;
                rptBasketListing.DataBind();
            }
            else
            {
                rptBasketListing.DataSource = null;
                rptBasketListing.DataBind();
            }
            #endregion

            ReadMetaTagsData();
            #endregion
        }
    }
}