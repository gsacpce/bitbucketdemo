﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;

namespace Presentation
{
    public partial class ShoppingCart_TelecashResponse : BasePage
    {
        private string OrderId, approval_code="", ccbrand="", cccountry="", chargetotal="", currencycode="", fail_rc="", fail_reason="", hash_algorithm="", ipgTransactionId="",
             oid="", response_hash="", status="", timezone="", txndatetime="", txntype="", txndate_processed="", cardnumber="", bname="";
        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        Int32 intUserId = 0;
        UserBE lst;
        public string host = GlobalFunctions.GetVirtualPath();
        protected void Page_Load(object sender, EventArgs e)
        {

            string[] keys = Request.Form.AllKeys;
            for (int i = 0; i < keys.Length; i++)
            {

                if (keys[i] == "approval_code")
                {
                    approval_code = Request.Form[keys[i]];
                }

                if (keys[i] == "ccbrand")
                {
                    ccbrand = Request.Form[keys[i]];
                }

                if (keys[i] == "cccountry")
                {
                    cccountry = Request.Form[keys[i]];
                }

                if (keys[i] == "chargetotal")
                {
                    chargetotal = Request.Form[keys[i]];
                }

                if (keys[i] == "currency")
                {
                    currencycode = Request.Form[keys[i]];
                }

                if (keys[i] == "fail_rc")
                {
                    fail_rc = Request.Form[keys[i]];
                }


                if (keys[i] == "fail_reason")
                {
                    fail_reason = Request.Form[keys[i]];
                }

                if (keys[i] == "hash_algorithm")
                {
                    hash_algorithm = Request.Form[keys[i]];
                }

                if (keys[i] == "ipgTransactionId")
                {
                    ipgTransactionId = Request.Form[keys[i]];
                }

                if (keys[i] == "oid")
                {
                    oid = Request.Form[keys[i]];
                }

                if (keys[i] == "response_hash")
                {
                    response_hash = Request.Form[keys[i]];
                }


                if (keys[i] == "status")
                {
                    status = Request.Form[keys[i]];
                }


                if (keys[i] == "timezone")
                {
                    timezone = Request.Form[keys[i]];
                }



                if (keys[i] == "txndatetime")
                {
                    txndatetime = Request.Form[keys[i]];
                }

                if (keys[i] == "txntype")
                {
                    txntype = Request.Form[keys[i]];
                }

                if (keys[i] == "txndate_processed")
                {
                    txndate_processed = Request.Form[keys[i]];
                }

                if (keys[i] == "cardnumber")
                {
                    cardnumber = Request.Form[keys[i]];
                }

                if (keys[i] == "bname")
                {
                    bname = Request.Form[keys[i]];
                }


                Exceptions.WriteInfoLog(keys[i] + ": " + Request.Form[keys[i]] + "<br>");



                // Response.Write(keys[i] + ": " + Request.Form[keys[i]] + "<br>");
            }


            if (Session["User"] != null)
            {
                lst = new UserBE();
                lst = Session["User"] as UserBE;
                intUserId = lst.UserId;
            }
            else
            {
                Response.RedirectToRoute("login-page");
                return;
            }


            intCurrencyId = GlobalFunctions.GetCurrencyId();
            intLanguageId = GlobalFunctions.GetLanguageId();


            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            CustomerOrderBE lstCustomerOrders = new CustomerOrderBE();
            CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
            objCustomerOrderBE.OrderedBy = lst.UserId;
            objCustomerOrderBE.LanguageId = intLanguageId;
            lstCustomerOrders = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);



            bool result = ShoppingCartBL.TeleCashTransactions(Convert.ToString(lstCustomerOrders.CustomerOrderId), approval_code, ccbrand, cccountry, chargetotal, currencycode, fail_rc, fail_reason, hash_algorithm, ipgTransactionId, oid, response_hash, status, timezone, txndatetime, txntype, txndate_processed, cardnumber, bname);
            Session["transaction"] = true;

            string lstrOrderURL = host + "ShoppingCart/OrderConfirmation.aspx?storeid=" + objStoreBE.StoreId + "&orderid=" + lstCustomerOrders.CustomerOrderId + "&TxRefGUID=" + lstCustomerOrders.TxnRefGUID;
            Response.Write("<script type=text/javascript> window.parent.location.href ='" + lstrOrderURL + "' </script>");



        }
    }
}