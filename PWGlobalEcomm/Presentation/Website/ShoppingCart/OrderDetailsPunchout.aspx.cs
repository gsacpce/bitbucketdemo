﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class ShoppingCart_OrderDetailsPunchout : System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Repeater rptBasketListing;
        protected global::System.Web.UI.WebControls.Literal ltrOrderNo;


        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;

        private string orderId, storeId;

        public string OrderId
        {
            get
            {
                return orderId;
            }
            set
            {
                orderId = value;
            }
        }

        public string StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }

        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        public decimal dSubTotalPrice = 0;
        CustomerOrderBE lstCustomerOrders;
        //protected static string error1, error2, error3, error4, error5, error6, error7, error8, error9, error10, error11, error12, error13, error14, error15, error16, error17;
        protected static string error4, error5;
        #endregion

        /// <summary>
        /// Author  : Sripal Amballa
        /// Date    : 23-09-15
        /// Scope   : Page_Load of the ShoppingCart_OrderDetails page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {                
                GlobalFunctions.writePunchoutLog("OrderDetailsPunchout_Page_Load start");
                BindResourceData();
                if (!Page.IsPostBack)
                {

                    if (Session["User"] == null)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, "Your order has been placed but there is some error in generating order#. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                        return;
                    }
                    GetQueryString();

                    intCurrencyId = GlobalFunctions.GetCurrencyId();
                    intLanguageId = GlobalFunctions.GetLanguageId();

                    Session["transaction"] = null;
                    UserBE lstUserBE = new UserBE();
                    lstUserBE = Session["User"] as UserBE;
                    
                    lstCustomerOrders = new CustomerOrderBE();
                    CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();

                    objCustomerOrderBE.CustomerOrderId = Convert.ToInt32(OrderId);
                    objCustomerOrderBE.OrderedBy = lstUserBE.UserId;
                    objCustomerOrderBE.LanguageId = intLanguageId;
                    objCustomerOrderBE.CurrencyId = intCurrencyId;

                    lstCustomerOrders = ShoppingCartBL.CustomerOrderPunchout_SAE(objCustomerOrderBE);
                    if (lstCustomerOrders == null)
                    {
                        Response.RedirectToRoute("index");
                        return;
                    }

                    if (lstCustomerOrders != null && lstCustomerOrders.CustomerOrderProducts.Count > 0)
                    {

                        rptBasketListing.DataSource = lstCustomerOrders.CustomerOrderProducts;
                        rptBasketListing.DataBind();

                        ltrOrderNo.Text = Convert.ToString(OrderId);

                        IsConnectedToInternet("http://www.google.com");

                        string cXML_Form = null;
                        cXML_Form = Convert.ToString(Session["cXML_Form"]);

                        string[] PunchoutSubmitUrl = Session["PunchoutSubmitUrl"].ToString().Split('/');
                        string poSubmitURL = PunchoutSubmitUrl[0] + "//" + PunchoutSubmitUrl[2];
                        GlobalFunctions.writePunchoutLog("Connectivity check : " + poSubmitURL);
                        IsConnectedToInternet(poSubmitURL);

                        Response.Write(cXML_Form);
                    }

                }
                this.Page.Title = "Order Confirmation";
                GlobalFunctions.writePunchoutLog("OrderDetailsPunchout_Page_Load end");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 23-09-15
        /// Scope   : setting query string values
        /// </summary>
        private void GetQueryString()
        {
            try
            {
                // Check for encrypted query string
                orderId = Request.QueryString["orderid"];
                storeId = Request.QueryString["storeid"];
                string encryptedQueryString = Request.QueryString["request"];
                string decryptedQueryString = string.Empty;
                string cryptoKey = string.Empty;
                if (!string.IsNullOrEmpty(encryptedQueryString))
                {
                    // Decrypt query strings
                    cryptoKey = System.Web.Configuration.WebConfigurationManager.AppSettings["CryptoKey"];
                    decryptedQueryString = GlobalFunctions.DecryptQueryStrings(encryptedQueryString, cryptoKey);
                }
                string[] strArr = decryptedQueryString.Split('&');
                string[] KeyValue = null;
                for (int iArr = 0; iArr < strArr.Length; iArr++)
                {
                    KeyValue = strArr[iArr].Split('=');
                    if (strArr[iArr].IndexOf('=') > 0)
                    {
                        if (Convert.ToString(KeyValue[0]).ToUpper() == "ORDERID")
                            orderId = Convert.ToString(KeyValue[1]);

                        if (Convert.ToString(KeyValue[0]).ToUpper() == "STOREID")
                            storeId = Convert.ToString(KeyValue[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 23-09-15
        /// Scope   : rptBasketListing_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptBasketListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl dvUnitPrice = (HtmlGenericControl)e.Item.FindControl("dvUnitPrice");
                    HtmlGenericControl dvTotalPrice = (HtmlGenericControl)e.Item.FindControl("dvTotalPrice");
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)e.Item.FindControl("hdnShoppingCartProductId");

                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";
                    double dPrice = 0;
                    if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode) && (lstCustomerOrders.BehaviourType.ToLower() == "product" || lstCustomerOrders.BehaviourType.ToLower() == "basket"))
                        dPrice = GlobalFunctions.DiscountedAmount(Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price")), lstCustomerOrders.DiscountPercentage);
                    else
                        dPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price"));

                    dPrice = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(dPrice)));
                    dvUnitPrice.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + dPrice + "</span>";
                    dvTotalPrice.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + Convert.ToDecimal(dPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity")) + "</span>";
                    dSubTotalPrice += Convert.ToDecimal(dPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                }
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlGenericControl dvTotal = (HtmlGenericControl)e.Item.FindControl("dvTotal");
                    HtmlGenericControl dvSubTotal = (HtmlGenericControl)e.Item.FindControl("dvSubTotal");
                    HtmlGenericControl dvShippingCharges = (HtmlGenericControl)e.Item.FindControl("dvShippingCharges");
                    HtmlGenericControl dvTaxes = (HtmlGenericControl)e.Item.FindControl("dvTaxes");

                    decimal dShippingCharges = Convert.ToDecimal(lstCustomerOrders.StandardCharges == 0 ? lstCustomerOrders.ExpressCharges : lstCustomerOrders.StandardCharges);
                    decimal dTaxes = Convert.ToDecimal(lstCustomerOrders.TotalTax);

                    dShippingCharges = Convert.ToDecimal(GlobalFunctions.ConvertToDecimalPrecision(dShippingCharges));
                    dTaxes = Convert.ToDecimal(GlobalFunctions.ConvertToDecimalPrecision(dTaxes));

                    decimal dTotalPrice = dSubTotalPrice + dShippingCharges + dTaxes;
                    dvSubTotal.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + Convert.ToString(dSubTotalPrice) + "</span>";
                    dvShippingCharges.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + dShippingCharges + "</span>";
                    dvTaxes.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + dTaxes + "</span>";
                    dvTotal.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "<span>" + dTotalPrice + "</span>";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 26-10-15
        /// Scope   : BindResourceData of the order detail page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                GlobalFunctions.writePunchoutLog("BindResourceData start");
                intLanguageId = GlobalFunctions.GetLanguageId();
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        error4 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Error4_Message").ResourceValue;
                        error5 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Error5_Message").ResourceValue;
                    }
                }
                GlobalFunctions.writePunchoutLog("BindResourceData end");
            }
            catch (Exception ex)
            {
                GlobalFunctions.writePunchoutLog("BindResourceData: " + ex.Message);
                Exceptions.WriteExceptionLog(ex);
            }
        }


        public bool IsConnectedToInternet(string urlToPing)
        {
            Uri url = new Uri(urlToPing);
            string pingurl = string.Format("{0}", url.Host);
            string host = pingurl;
            bool result = false;
            Ping p = new Ping();
            try
            {
                PingReply reply = p.Send(host, 2000);
                if (reply.Status == IPStatus.Success)
                {
                    GlobalFunctions.writePunchoutLog("Connectivity Check " + urlToPing + " Success.");
                    return true;
                }
                else
                {
                    GlobalFunctions.writePunchoutLog("Connectivity Check " + urlToPing + " Failure.");
                }


            }
            catch (Exception ex)
            {
                GlobalFunctions.writePunchoutLog("Connectivity Check " + urlToPing + " Failed Due to Exception :" + ex.StackTrace);
            }
            return result;

        }

    }
}