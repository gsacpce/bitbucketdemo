﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class ShoppingCart_TeleCashRequest : BasePage
    {
        private static String DEFAULT_ALGORITHM = "SHA256";
        private static String SHA256 = "SHA256";
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hash_algorithm, storename, mode, oid, timezone, txntype, currency, txndatetime,
            hash, responseSuccessURL, responseFailURL, chargetotal;
        protected global::System.Web.UI.WebControls.Literal ltrForm, ltrScript;
        protected global::System.Web.UI.HtmlControls.HtmlForm form;
        private static ReadOnlyCollection<String> SUPPORTED_ALGORITHMS = new ReadOnlyCollection<String>(new List<String> { DEFAULT_ALGORITHM, SHA256 });
        protected global::System.Web.UI.HtmlControls.HtmlIframe TransFrame;
        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        Int32 intUserId = 0;
        UserBE lst;
        public string host = GlobalFunctions.GetVirtualPath();
        public string TeleCashUrl = GlobalFunctions.GetSetting("TeleCashUrl");
        public string Algorithm = GlobalFunctions.GetSetting("Algorithm");
        public string Username = GlobalFunctions.GetSetting("Username");
        public string Telecash_Mode = GlobalFunctions.GetSetting("Mode");
        public string Telecash_TimeZone = GlobalFunctions.GetSetting("TimeZone");
        public string Telecash_TransactionType = GlobalFunctions.GetSetting("TransactionType");
        public string Telecash_Hash = GlobalFunctions.GetSetting("Hash");
        public string Telecash_ResponseURL = GlobalFunctions.GetSetting("ResponseURL");



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                lst = new UserBE();
                lst = Session["User"] as UserBE;
                intUserId = lst.UserId;
            }
            else
            {
                Response.RedirectToRoute("login-page");
                return;
            }

            intCurrencyId = GlobalFunctions.GetCurrencyId();
            intLanguageId = GlobalFunctions.GetLanguageId();


            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            TeleCashCurrencyBE objTeleCashtrans = new TeleCashCurrencyBE();
            objTeleCashtrans = ShoppingCartBL.GetCurrencynumber(Convert.ToInt16(GlobalFunctions.GetCurrencyId()));
            CustomerOrderBE lstCustomerOrders = new CustomerOrderBE();
            CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
            objCustomerOrderBE.OrderedBy = lst.UserId;
            objCustomerOrderBE.LanguageId = intLanguageId;
            lstCustomerOrders = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
            if (lstCustomerOrders == null)
            {
                Response.RedirectToRoute("index");
                return;
            }



            string total = Convert.ToString(CalculateTotalAmount(lstCustomerOrders.StandardCharges == 0 ? lstCustomerOrders.ExpressCharges : lstCustomerOrders.StandardCharges, lstCustomerOrders.TotalTax, lstCustomerOrders));
            //   string total = "1.00";

            string formScript = "<script language=\"javascript\" type=\"text/javascript\">" +
                               "document.getElementById('{0}').submit();" +
                               "</script>";

            string formName = "form1";
            string formAction = TeleCashUrl;
            this.hash_algorithm.Value = Algorithm;
            this.storename.Value = Username;
            this.mode.Value = Telecash_Mode;
            this.timezone.Value = Telecash_TimeZone;
            this.txntype.Value = Telecash_TransactionType;
            if (objTeleCashtrans != null)
            { this.currency.Value = objTeleCashtrans.TeleCashcurrency; }
            else
            {
                this.currency.Value = "978";
            }
            if (TeleCashUrl.Contains("test"))
            {
                this.chargetotal.Value = "1.00";
            }
            else
            {
                this.chargetotal.Value = total;
            }
            TimeZoneInfo timeZoneInfo;
            DateTime dateTime;
            //Set the time zone information to US Mountain Standard Time 
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time");
            //Get date and time in US Mountain Standard Time 
            dateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);
            //Print out the date and time
            this.txndatetime.Value = dateTime.ToString(@"yyyy:MM:dd-H:mm:ss");
            var StringToHash = this.storename.Value + this.txndatetime.Value + total + this.currency.Value + Telecash_Hash;
            string hash = calculateHashFromString(new StringBuilder(StringToHash), Algorithm);
            this.hash.Value = hash;
            this.responseSuccessURL.Value = host + Telecash_ResponseURL;
            this.responseFailURL.Value = host + Telecash_ResponseURL;
            this.oid.Value = objStoreBE.StoreName + Convert.ToString(DateTime.Now.Ticks.GetHashCode());
           // this.oid.Value =  Convert.ToString(DateTime.Now.Ticks.GetHashCode());

            ltrForm.Text = string.Format("<form id=\"{0}\" name=\"{0}\" method=\"post\" action=\"{1}\">", formName, formAction) + AllInputTagsString() + "</form>";
            ltrScript.Text = string.Format(formScript, formName);

        }



        /// <summary>
        /// Creates input tags of the send form with id and values from fields in the web form
        /// </summary>
        /// <returns>string containing all input tags</returns>
        protected string AllInputTagsString()
        {
            StringBuilder result = new StringBuilder();
            string input = "<input type=\"hidden\" name=\"{0}\" value=\"{1}\"/>";

            foreach (Control control in form.Controls)
            {
                string controlName = control.GetType().Name;

                if (!(controlName.Equals("LiteralControl") || controlName.Equals("CheckBox") || controlName.Equals("Button")))
                {
                    var textProperty = control.GetType().GetProperty("Value");

                    if (textProperty != null)
                    {
                        var text = textProperty.GetGetMethod().Invoke(control, null);
                        result.Append(string.Format(input, control.ID, text));
                    }
                }
            }
            return result.ToString();
        }

        /// <summary>
        /// Determines the character representation for a specific number (digit)
        /// </summary>
        /// <param name="digit"></param>
        /// <param name="radix"></param>
        /// <returns></returns>
        private static char forDigit(int digit, int radix)
        {
            int MIN_RADIX = 2, MAX_RADIX = 36;
            if ((digit >= radix) || (digit < 0))
            {
                return '\0';
            }
            if ((radix < MIN_RADIX) || (radix > MAX_RADIX))
            {
                return '\0';
            }
            if (digit < 10)
            {
                return (char)('0' + digit);
            }
            return (char)('a' - 10 + digit);
        }

        public static String calculateHashFromString(StringBuilder stringValue, String hashAlgorithmName)
        {
            if (!SUPPORTED_ALGORITHMS.Contains(hashAlgorithmName) && hashAlgorithmName.Length > 0)
            {
                throw new System.ArgumentOutOfRangeException("hashAlgorithmName", hashAlgorithmName, "Algorithm '" + hashAlgorithmName + "' not supported");
            }
            HashAlgorithm hashAlgorithm = HashAlgorithm.Create(hashAlgorithmName.Length == 0 ? DEFAULT_ALGORITHM : hashAlgorithmName);

            StringBuilder sb = new StringBuilder();

            byte[] stringValueBytes = Encoding.ASCII.GetBytes(stringValue.ToString());

            int stringValueBytesLength = stringValueBytes.Length;

            for (int i = 0; i < stringValueBytesLength; i++)
            {
                sb.Append(forDigit((stringValueBytes[i] & 240) >> 4, 16));
                sb.Append(forDigit((stringValueBytes[i] & 15), 16));
            }

            stringValueBytes = Encoding.ASCII.GetBytes((new StringBuilder(sb.ToString()).ToString()));

            hashAlgorithm.TransformFinalBlock(stringValueBytes, 0, stringValueBytes.Length);
            byte[] hash = hashAlgorithm.Hash;
            int hashLength = hash.Length;

            return BitConverter.ToString(hash).Replace("-", "").ToLower();
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-09-15
        /// Scope   : CalculateTotalAmount
        /// </summary>            
        /// <returns>double</returns>
        protected double CalculateTotalAmount(double dShippingCharges, double dTax, CustomerOrderBE lstCustomerOrderBE)
        {
            double dTotalAmount = 0;
            try
            {
                List<object> lstShoppingBE = new List<object>();
                List<ShoppingCartBE> lstShoppingCartNewBE = new List<ShoppingCartBE>();
                lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, "", true);
                lstShoppingCartNewBE = lstShoppingBE.Cast<ShoppingCartBE>().ToList();

                if (lstShoppingCartNewBE.Count > 0)
                {
                    for (int i = 0; i < lstShoppingCartNewBE.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(lstCustomerOrderBE.CouponCode) && (lstCustomerOrderBE.BehaviourType.ToLower() == "product" || lstCustomerOrderBE.BehaviourType.ToLower() == "basket"))
                            dTotalAmount += Convert.ToDouble((GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstShoppingCartNewBE[i].Price), lstCustomerOrderBE.DiscountPercentage) * lstShoppingCartNewBE[i].Quantity));
                        else
                            dTotalAmount += Convert.ToDouble((lstShoppingCartNewBE[i].Price * lstShoppingCartNewBE[i].Quantity));
                    }
                    dTotalAmount += dShippingCharges + dTax;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            #region "Added by Sripal for Gift certificate"
            if (dTotalAmount > 0)
            {
                if (Session["Amount"] != null)
                {
                    try
                    {
                        double dGiftCertificateAmount = Convert.ToDouble(Session["Amount"]);
                        dTotalAmount = dTotalAmount - dGiftCertificateAmount;
                        if (dTotalAmount < 0) { dTotalAmount = 0; }
                    }
                    catch (Exception) { }
                }
            }
            #endregion










            return Math.Round(dTotalAmount, 2, MidpointRounding.AwayFromZero);
            //return dTotalAmount;
        }

    }
}