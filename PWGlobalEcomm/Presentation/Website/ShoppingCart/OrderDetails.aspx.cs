﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Configuration;
using Microsoft.Security.Application;
using PWGlobalEcomm.GlobalUtilities;
using System.Globalization;
using System.Net.Mail;
using System.Reflection;


namespace Presentation
{
    public class ShoppingCart_OrderDetails : BasePage// : System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Repeater rptBasketListing, rptCustomMapping, rptUserTypeCustomFields, rptCustomFieldsAll;
        protected global::System.Web.UI.WebControls.Literal ltrPaymentMethod, ltrDeliveryAddress, ltrBillingAddress, ltrOrderNo, ltGiftCertificateCode, ltGiftCertificateAmount, ltrGATrackingEcomm;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divGuestCheckout, divGiftCertificate, divTax, divaddtionalcharges, divUserType, divGuestUserRegistrationDetails, divddlUserType, lblTitle, divCustomFields, divUserTitles, divPreferredLanguage, divPreferredCurrency;

        protected global::System.Web.UI.WebControls.RequiredFieldValidator reqtxtPassword, reqtxtConfirmPassword, reqddlUserType, rfvDdlTitle, reqtxtTitles;
        protected global::System.Web.UI.WebControls.CompareValidator cmpValidatorPwd;
        protected global::System.Web.UI.WebControls.RegularExpressionValidator regexPwdLentxtPassword, regexAlphaNumtxtPassword, regexAlphaNumSymtxtPassword;
        protected global::System.Web.UI.WebControls.Label lblPasswordPolicyMessage, lblCustomFieldName, lblUserTypes, lblPreferredLanguage, lblPreferredCurrency;
        protected global::System.Web.UI.WebControls.HiddenField hdnPasswordPolicyType, hdnMinPasswordLength;
        protected global::System.Web.UI.WebControls.TextBox txtPassword, txtConfirmPassword, txtTitles;
        protected global::System.Web.UI.WebControls.Button btnCreateAccount;
        protected global::System.Web.UI.WebControls.DropDownList ddlUserType, ddlCustomValue, ddlTitles, drpdwnPreferredLanguage, drpdwnPreferredCurrency;

        public Int16 intLanguageId, intCurrencyId = 0, iCount = 0, iUserTypeID = 1;
        public string strAddtionalcharges, strCurrencySymbol = string.Empty;
        private string orderId, storeId;

        public string Generic_Order_Confirmation, Order_Details_Error_Generating_Order_Msg, OrderDetails_Order_Placed_Thankyou_Note, OrderDetails_Order_Confirmation_Email_Note,
                      OrderDetails_SaveDetails_FutureUse_Note, OrderDetails_CheckOut_Faster, Generic_View_Order_History, Generic_WriteReviews,
                      Order_Details_Set_Password_Reset_Text, Register_Password_Text, Register_ConfirmPassword_Text, Generic_Create_Account_Text,
                      Generic_Order_Number_Text, Print_Title, Product_Title, Register_Delivery_Address_Title, Basket_Quantity_Text,
                      Basket_Unit_Cost_Text, Basket_Line_Total_Text, Generic_SubTotal, Shipping, Tax, Total, SiteLinks_GiftCertificateTitle,
                      Generic_Billing_Address, Generic_DeliveryAddress, SC_Payment_Method, Generic_Enter_Text, Generic_Select_Text, strRegistrationDeclinedMessage, Register_Next_Button, strCustomFieldValidationError, strGeneric_Existing_LoginFieldValue_Message, CustomFieldValidationError, HostDomain,
                      strRegister_PreferredCurrency_Message, strRegister_PreferredLanguages_Message, RegTitleMsg, strTitleReqMessage, strPreferredLanguageReqMessage,
                      strPreferredCurrencyReqMessage, strGeneric_Registration_Successfully_Msg, strGeneric_Registration_Failure_Msg, strGeneric_InvalidCaptchaText_Message,
                      strGeneric_Email_Sending_Failed_Msg, strOrderDetails_Error_Processing_Registration_Error_Msg;

        UserTypeMappingBE objUserTypeMappingBE = new UserTypeMappingBE();
        UserTypeMappingBE objTempUserTypeMappingBE = new UserTypeMappingBE();
        UserRegistrationBE objUserRegistrationBE = new UserRegistrationBE();
        UserRegistrationBE objTempUserRegistrationBE = new UserRegistrationBE();
        List<CurrencyBE> lstCurrencies = new List<CurrencyBE>();

        public string OrderId
        {
            get
            {
                return orderId;
            }
            set
            {
                orderId = value;
            }
        }

        public string StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }

        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        public decimal dSubTotalPrice = 0;
        CustomerOrderBE lstCustomerOrders;
        protected static string error4, error5;
        public bool b = false;
        protected string strMinPassLenErrorMsg, strAlphaNumPassReqErrorMsg, strAlphaNumSymPassReqErrorMsg;
        StoreBE lstStoreDetail = new StoreBE();
        string minPasswordLength = "0";
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 23-09-15
        /// Scope   : Page_Load of the ShoppingCart_OrderDetails page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                intLanguageId = GlobalFunctions.GetLanguageId();
                strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                lstStoreDetail = StoreBL.GetStoreDetails();
                BindResourceData();
                if (!Page.IsPostBack)
                {
                    if (Session["transaction"] != null)
                    {
                        if (Session["User"] == null)
                        {
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "Your order has been placed but there is some error in generating order#. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, Order_Details_Error_Generating_Order_Msg, GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                            return;
                        }
                        GetQueryString();

                        if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                        {
                            strCurrencySymbol = string.Empty;
                        }
                        Session["transaction"] = null;
                        Session["sbGift"] = null;
                        UserBE lstUserBE = new UserBE();
                        lstUserBE = Session["User"] as UserBE;

                        if (lstUserBE.IsGuestUser == false)
                        {
                            divGuestCheckout.Visible = false;
                            divUserType.Visible = false;
                            divGuestUserRegistrationDetails.Visible = false;
                            divCustomFields.Visible = false;
                        }
                        else
                        {
                            divGuestCheckout.Visible = true;
                            divGuestUserRegistrationDetails.Visible = true;
                            divCustomFields.Visible = true;
                            divUserTitles.Visible = true;
                            divPreferredLanguage.Visible = true;
                            BindUserTypeMappping();
                            //BindUserTitles();		
                            BindPreferredLanguages();
                            //BindPreferredCurrency(lstUserBE.UserTypeID);		
                            UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 1)
                            {
                                BindPreferredCurrency(lstUserBE.UserTypeID);	
                                divUserType.Visible = true;
                                BindUserSelection();
                            }
                            else if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                            {
                                MailAddress address = new MailAddress(lstUserBE.EmailId.Trim());
                                HostDomain = address.Host;
                                UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidation = objUserTypeMappingBE.lstUserTypeEmailValidation.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.EmailId.ToLower() == lstUserBE.EmailId.ToLower().Trim() && x.IsWhiteList == true);
                                UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidationDomain = objUserTypeMappingBE.lstUserTypeEmailValidation.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.Domain.ToLower() == HostDomain.ToLower() && x.IsWhiteList == true);

                                if (objRegUserTypeEmailValidation != null)
                                {
                                    divddlUserType.Visible = false;
                                    reqddlUserType.Enabled = false;
                                    BindVisibleUserType(objRegUserTypeEmailValidation.UserTypeId);
                                    divPreferredCurrency.Visible = true;
                                    BindPreferredCurrency(objRegUserTypeEmailValidation.UserTypeId);
                                }
                                else if (objRegUserTypeEmailValidationDomain != null)
                                {
                                    divddlUserType.Visible = false;
                                    reqddlUserType.Enabled = false;
                                    BindVisibleUserType(objRegUserTypeEmailValidationDomain.UserTypeId);
                                    divPreferredCurrency.Visible = true;
                                    BindPreferredCurrency(objRegUserTypeEmailValidationDomain.UserTypeId);
                                }
                                else
                                {
                                    UserTypeMappingBE.UserTypeDetails objRegUserTypeDetails = objUserTypeMappingBE.lstUserTypeDetails.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.IsWhiteList == false);
                                    if (objRegUserTypeDetails != null)
                                    {
                                        divddlUserType.Visible = false;
                                        reqddlUserType.Enabled = false;
                                        BindVisibleUserType(objRegUserTypeDetails.UserTypeID);
                                        divPreferredCurrency.Visible = true;
                                        BindPreferredCurrency(objRegUserTypeDetails.UserTypeID);
                                    }
                                    else
                                    {
                                        divddlUserType.Visible = false;
                                        reqddlUserType.Enabled = false;
                                        rptUserTypeCustomFields.Visible = false;
                                    }
                                }
                            }
                            else if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                            {
                                UserTypeMappingBE.Country objRegCountry = objUserTypeMappingBE.lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(lstUserBE.PredefinedColumn8));
                                if (objRegCountry != null)
                                {
                                    if (objRegCountry.UserTypeID > 0)
                                    {
                                        BindVisibleUserType(objRegCountry.UserTypeID);
                                        divPreferredCurrency.Visible = true;
                                        BindPreferredCurrency(objRegCountry.UserTypeID);
                                    }
                                    else
                                    {
                                        divddlUserType.Visible = false;
                                        reqddlUserType.Enabled = false;
                                        rptUserTypeCustomFields.Visible = false;
                                    }
                                }
                                else
                                {
                                    divddlUserType.Visible = false;
                                    reqddlUserType.Enabled = false;
                                    rptUserTypeCustomFields.Visible = false;
                                }
                            }
                            else if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                            {
                                BindPreferredCurrency(lstUserBE.UserTypeID);	
                                BindCustomMapping();
                            }
                        }

                        lstCustomerOrders = new CustomerOrderBE();
                        CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();

                        objCustomerOrderBE.CustomerOrderId = Convert.ToInt32(OrderId);
                        objCustomerOrderBE.OrderedBy = lstUserBE.UserId;
                        objCustomerOrderBE.LanguageId = intLanguageId;
                        objCustomerOrderBE.CurrencyId = intCurrencyId;

                        lstCustomerOrders = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                        if (lstCustomerOrders == null)
                        {
                            Response.RedirectToRoute("index");
                            return;
                        }

                        if (lstCustomerOrders != null && lstCustomerOrders.CustomerOrderProducts.Count > 0)
                        {
                            rptBasketListing.DataSource = lstCustomerOrders.CustomerOrderProducts;
                            rptBasketListing.DataBind();
                            ltrPaymentMethod.Text = lstCustomerOrders.PaymentTypeRef;
                            ltrOrderNo.Text = Convert.ToString(lstCustomerOrders.OrderId_OASIS);

                            ltrBillingAddress.Text = lstCustomerOrders.InvoiceContactPerson + "<br>" + lstCustomerOrders.InvoiceCompany + "<br>" +
                               lstCustomerOrders.InvoiceAddress1 + "<br>" + lstCustomerOrders.InvoiceAddress2 + "<br>" + lstCustomerOrders.InvoiceCity + "<br>" +
                               lstCustomerOrders.InvoiceCounty + "<br>" + lstCustomerOrders.InvoicePostalCode + "<br>" + lstCustomerOrders.InvoiceCountryName
                               + "<br>" + lstCustomerOrders.InvoicePhone;

                            ltrDeliveryAddress.Text = lstCustomerOrders.DeliveryContactPerson + "<br>" + lstCustomerOrders.DeliveryCompany + "<br>" +
                                lstCustomerOrders.DeliveryAddress1 + "<br>" + lstCustomerOrders.DeliveryAddress2 + "<br>" + lstCustomerOrders.DeliveryCity + "<br>" +
                                lstCustomerOrders.DeliveryCounty + "<br>" + lstCustomerOrders.DeliveryPostalCode + "<br>" + lstCustomerOrders.DeliveryCountryName
                                + "<br>" + lstCustomerOrders.DeliveryPhone;


                            #region "GiftCertificate"
                            if (lstCustomerOrders.CustomerOrderGiftCertificate != null)
                            {
                                if (lstCustomerOrders.CustomerOrderGiftCertificate.Count > 0)
                                {
                                    ltGiftCertificateCode.Text = Convert.ToString(lstCustomerOrders.CustomerOrderGiftCertificate[0].GiftCertificateCode);
                                    ltGiftCertificateAmount.Text = strCurrencySymbol + Convert.ToString(lstCustomerOrders.CustomerOrderGiftCertificate[0].GCAmount);
                                    divGiftCertificate.Visible = true;
                                }
                                else
                                {
                                    divGiftCertificate.Visible = false;
                                }
                            }
                            else
                            {
                                divGiftCertificate.Visible = false;
                            }
                            #endregion

                            Session["SelectedEntityDetails"] = null;
                            Session["IsTotalvalueZeroafterGC"] = null;// vikram for legal entity

                        }

                    }
                    else
                    {
                        Response.RedirectToRoute("index");
                    }

                    if (!Request.Url.ToString().ToLower().Contains("localhost"))
                    {
                        GAtracking();

                    }
                }
                this.Page.Title = Generic_Order_Confirmation;
                #region  Added by SHRIGANESH SINGH to set Password Policy features 17 May 2016
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue != null)
                {
                    minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                    hdnMinPasswordLength.Value = minPasswordLength;
                }

                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].FeatureValue;
                }
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].FeatureValue;
                }
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].FeatureValue;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 23-09-15
        /// Scope   : setting query string values
        /// </summary>
        private void GetQueryString()
        {
            try
            {
                // Check for encrypted query string
                orderId = Request.QueryString["orderid"];
                storeId = Request.QueryString["storeid"];
                string encryptedQueryString = Request.QueryString["request"];
                string decryptedQueryString = string.Empty;
                string cryptoKey = string.Empty;
                if (!string.IsNullOrEmpty(encryptedQueryString))
                {
                    // Decrypt query strings
                    cryptoKey = System.Web.Configuration.WebConfigurationManager.AppSettings["CryptoKey"];
                    decryptedQueryString = GlobalFunctions.DecryptQueryStrings(encryptedQueryString, cryptoKey);
                }
                string[] strArr = decryptedQueryString.Split('&');
                string[] KeyValue = null;
                for (int iArr = 0; iArr < strArr.Length; iArr++)
                {
                    KeyValue = strArr[iArr].Split('=');
                    if (strArr[iArr].IndexOf('=') > 0)
                    {
                        if (Convert.ToString(KeyValue[0]).ToUpper() == "ORDERID")
                            orderId = Convert.ToString(KeyValue[1]);

                        if (Convert.ToString(KeyValue[0]).ToUpper() == "STOREID")
                            storeId = Convert.ToString(KeyValue[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 23-09-15
        /// Scope   : rptBasketListing_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptBasketListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl dvUnitPrice = (HtmlGenericControl)e.Item.FindControl("dvUnitPrice");
                    HtmlGenericControl dvTotalPrice = (HtmlGenericControl)e.Item.FindControl("dvTotalPrice");
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)e.Item.FindControl("hdnShoppingCartProductId");
                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                    {
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    }
                    else
                    {
                        imgProduct.Src = host + "Images/Products/default.jpg";
                    }
                    double dPrice = 0;
                    double total = 0;
                    double pointtotal = 0;
                    if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode) && (lstCustomerOrders.BehaviourType.ToLower() == "product" || lstCustomerOrders.BehaviourType.ToLower() == "basket"))
                    {
                        dPrice = GlobalFunctions.DiscountedAmount(Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price")), lstCustomerOrders.DiscountPercentage);
                    }
                    else
                    {
                        dPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price"));
                    }

                    dPrice = Math.Round(dPrice, 2, MidpointRounding.AwayFromZero);
                    #region added by vikram to show to upto 2 decimal
                    double ProductWiseTotal = Convert.ToDouble(Convert.ToDecimal(dPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity")));
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        dvUnitPrice.InnerHtml = strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dPrice, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbol, intLanguageId) + "</span>" + GlobalFunctions.PointsText(intLanguageId);
                        dvTotalPrice.InnerHtml = strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(ProductWiseTotal, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbol, intLanguageId) + "</span>" + GlobalFunctions.PointsText(intLanguageId);
                        Double PointValues = 0;
                        CurrencyBE objCurrencyCD = new CurrencyBE();
                        List<CurrencyBE> objCurrencyBE = null;
                        objCurrencyBE = CurrencyBL.GetCurrencyPointInvoiceDetails();
                        for (int i = 0; i < objCurrencyBE.Count; i++)
                        {
                            if (objCurrencyBE[i].CurrencySymbol.Trim() == GlobalFunctions.GetCurrencySymbol())
                            {
                                PointValues = double.Parse(objCurrencyBE[i].PointValue.ToString());
                            }
                        }
                        double stotal = Convert.ToDouble(dPrice * PointValues);
                        total = Convert.ToDouble(Convert.ToDecimal(stotal) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity")));
                        pointtotal = total;
                        total = Math.Ceiling(total);
                        dvUnitPrice.InnerHtml = strCurrencySymbol + "<span>" + GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dPrice, CultureInfo.InvariantCulture.NumberFormat), GlobalFunctions.GetCurrencySymbol(), intLanguageId)/* + "</span>" + GlobalFunctions.PointsText(intLanguageId)*/;
                        dvTotalPrice.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(total) + " </span>" + GlobalFunctions.PointsText(intLanguageId);
                    }
                    else
                    {
                        dvUnitPrice.InnerHtml = strCurrencySymbol + "<span>" + dPrice.ToString("##,###,##0.#0") + GlobalFunctions.PointsText(intLanguageId);
                        dvTotalPrice.InnerHtml = strCurrencySymbol + "<span>" + ProductWiseTotal.ToString("##,###,##0.#0") + "</span>" + GlobalFunctions.PointsText(intLanguageId);
                    }
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        string value = GlobalFunctions.RemovePointsSpanIntheText(GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(ProductWiseTotal, CultureInfo.InvariantCulture.NumberFormat), GlobalFunctions.GetCurrencySymbol(), intLanguageId));
                        dSubTotalPrice += Convert.ToDecimal(pointtotal);
                    }
                    else
                    {
                        dSubTotalPrice += Convert.ToDecimal(Convert.ToDecimal(dPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity")));
                    }
                    #endregion
                }
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlGenericControl dvTotal = (HtmlGenericControl)e.Item.FindControl("dvTotal");
                    HtmlGenericControl dvSubTotal = (HtmlGenericControl)e.Item.FindControl("dvSubTotal");
                    HtmlGenericControl dvShippingCharges = (HtmlGenericControl)e.Item.FindControl("dvShippingCharges");
                    HtmlGenericControl dvTaxes = (HtmlGenericControl)e.Item.FindControl("dvTaxes");
                    HtmlGenericControl divTax = (HtmlGenericControl)e.Item.FindControl("divTax");

                    decimal dShippingCharges = 0.00M;
                    if (lstCustomerOrders.StandardCharges > 0)
                    {
                        dShippingCharges = Convert.ToDecimal(lstCustomerOrders.StandardCharges);
                    }
                    else if (lstCustomerOrders.ExpressCharges > 0)
                    {
                        dShippingCharges = Convert.ToDecimal(lstCustomerOrders.ExpressCharges);
                    }
                    else if (lstCustomerOrders.PalletCharges > 0)
                    {
                        dShippingCharges = Convert.ToDecimal(lstCustomerOrders.PalletCharges);
                    }
                    else
                    {
                        dShippingCharges = 0.00M;
                    }
                    decimal dTaxes = Convert.ToDecimal(lstCustomerOrders.TotalTax);

                    dShippingCharges = Convert.ToDecimal(dShippingCharges.ToString("##,###,##0.#0"));
                    dTaxes = Convert.ToDecimal(dTaxes.ToString("##,###,##0.#0"));// by vikram

                    string dSh, ddTotalTax = "";
                    decimal dTotalPrice = 0;
                    string dtotalPricepoint;
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        divTax.Visible = false;
                        dSh = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dShippingCharges), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                        ddTotalTax = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dTaxes), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                    }
                    else
                    {
                        dSh = dShippingCharges.ToString("##,###,##0.#0");
                        ddTotalTax = dTaxes.ToString("##,###,##0.#0");
                    }

                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        dTotalPrice = dSubTotalPrice;

                    }
                    else
                    {
                        dTotalPrice = dSubTotalPrice + Convert.ToDecimal(dSh) + Convert.ToDecimal(ddTotalTax);
                    }

                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        dShippingCharges = 0;
                        dSubTotalPrice = Math.Ceiling(dSubTotalPrice);
                        dtotalPricepoint = Convert.ToString(dSubTotalPrice);
                        dvSubTotal.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(dSubTotalPrice) + " </span>" + GlobalFunctions.PointsText(intLanguageId);
                        dvShippingCharges.InnerHtml = strCurrencySymbol + "<span>" + GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dShippingCharges), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()) + "</span>";
                        dvTotal.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(dtotalPricepoint) + " </span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                    }
                    else
                    {
                        dvSubTotal.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(dSubTotalPrice.ToString("##,###,##0.#0")) + "</span>";
                        dvShippingCharges.InnerHtml = strCurrencySymbol + "<span>" + dShippingCharges.ToString("##,###,##0.#0") + "</span>";
                        dvTaxes.InnerHtml = strCurrencySymbol + "<span>" + dTaxes.ToString("##,###,##0.#0") + "</span>";
                        dvTotal.InnerHtml = strCurrencySymbol + "<span>" + dTotalPrice.ToString("##,###,##0.#0") + "</span>";

                    }
                    #region "GIFTCERTIFICATE"
                    if (lstCustomerOrders.CustomerOrderGiftCertificate != null)
                    {
                        if (lstCustomerOrders.CustomerOrderGiftCertificate.Count > 0)
                        {
                            HtmlGenericControl dvGC = (HtmlGenericControl)e.Item.FindControl("dvGC");
                            HtmlGenericControl divGC = (HtmlGenericControl)e.Item.FindControl("divGC");
                            dvGC.InnerHtml = strCurrencySymbol + "<span> -" + lstCustomerOrders.CustomerOrderGiftCertificate[0].GCAmount.ToString("##,###,##0.#0") + "</span>" + GlobalFunctions.PointsText(intLanguageId);
                            divGC.Visible = true;

                            if (Convert.ToDecimal(lstCustomerOrders.CustomerOrderGiftCertificate[0].GCAmount) == dTotalPrice)
                            {
                                dvTotal.InnerHtml = strCurrencySymbol + "<span> 0.00 </span>" + GlobalFunctions.PointsText(intLanguageId);
                            }
                            if (Convert.ToDecimal(lstCustomerOrders.CustomerOrderGiftCertificate[0].GCAmount) < dTotalPrice)
                            {
                                dvTotal.InnerHtml = strCurrencySymbol + "<span>" + (dTotalPrice - Convert.ToDecimal(lstCustomerOrders.CustomerOrderGiftCertificate[0].GCAmount)).ToString("##,###,##0.#0") + "</span>" + GlobalFunctions.PointsText(intLanguageId);
                            }
                            if (Convert.ToDecimal(lstCustomerOrders.CustomerOrderGiftCertificate[0].GCAmount) > dTotalPrice)
                            {
                                dvTotal.InnerHtml = strCurrencySymbol + "<span>" + (Convert.ToDecimal(lstCustomerOrders.CustomerOrderGiftCertificate[0].GCAmount) - dTotalPrice).ToString("##,###,##0.#0") + "</span>" + GlobalFunctions.PointsText(intLanguageId);
                            }

                        }
                        else
                        {
                            HtmlGenericControl divGC = (HtmlGenericControl)e.Item.FindControl("divGC");
                            divGC.Visible = false;
                        }
                    }
                    else
                    {
                        HtmlGenericControl divGC = (HtmlGenericControl)e.Item.FindControl("divGC");
                        divGC.Visible = false;
                    }
                    #endregion
                    #region "HANDLING FEE"
                    if (lstCustomerOrders.handlingfee != null && lstCustomerOrders.handlingfee != 0)
                    {
                        decimal handlingfee = Convert.ToDecimal(lstCustomerOrders.handlingfee.ToString("#######0.#0"));
                        HtmlGenericControl divaddtionalcharges = (HtmlGenericControl)e.Item.FindControl("divaddtionalcharges");
                        HtmlGenericControl dvadditionalcharges = (HtmlGenericControl)e.Item.FindControl("dvadditionalcharges");
                        dvadditionalcharges.InnerHtml = strCurrencySymbol + "<span> " + handlingfee + "</span>";
                        divaddtionalcharges.Visible = true;
                    }
                    else
                    {
                        HtmlGenericControl divGC = (HtmlGenericControl)e.Item.FindControl("divaddtionalcharges");
                        divGC.Visible = false;
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 26-10-15
        /// Scope   : BindResourceData of the order detail page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        error4 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password/ConfirmPassword_Req_Message").ResourceValue;
                        error5 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password/ConfirmPassword_Mismatch_Message").ResourceValue;
                        /*Sachin Chauhan Start : 29 03 2016 : Variable to store resource languages */
                        Order_Details_Error_Generating_Order_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_Details_Error_Generating_Order_Msg").ResourceValue;
                        Generic_Order_Confirmation = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Confirmation").ResourceValue;
                        OrderDetails_Order_Placed_Thankyou_Note = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "OrderDetails_Order_Placed_Thankyou_Note").ResourceValue;
                        OrderDetails_Order_Confirmation_Email_Note = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "OrderDetails_Order_Confirmation_Email_Note").ResourceValue;
                        OrderDetails_SaveDetails_FutureUse_Note = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "OrderDetails_SaveDetails_FutureUse_Note").ResourceValue;
                        OrderDetails_CheckOut_Faster = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "OrderDetails_CheckOut_Faster").ResourceValue;
                        Generic_View_Order_History = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_View_Order_History").ResourceValue;
                        Generic_WriteReviews = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Write_Reviews").ResourceValue;
                        Order_Details_Set_Password_Reset_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_Details_Set_Password_Reset_Text").ResourceValue;
                        Register_Password_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Password_Text").ResourceValue;
                        Register_ConfirmPassword_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_ConfirmPassword_Text").ResourceValue;
                        Generic_Create_Account_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Create_Account_Text").ResourceValue;
                        Generic_Order_Number_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Number_Text").ResourceValue;
                        Print_Title = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Print_Title").ResourceValue;
                        Product_Title = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Title").ResourceValue;
                        Register_Delivery_Address_Title = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Delivery_Address_Title").ResourceValue;
                        Basket_Quantity_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Text").ResourceValue;
                        Basket_Unit_Cost_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Unit_Cost_Text").ResourceValue;
                        Basket_Line_Total_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Line_Total_Text").ResourceValue;
                        Generic_SubTotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_SubTotal").ResourceValue;
                        Shipping = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Shipping").ResourceValue;
                        Tax = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Tax").ResourceValue;
                        Total = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Total_Text").ResourceValue;
                        SiteLinks_GiftCertificateTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_GiftCertificateTitle").ResourceValue;
                        Generic_Billing_Address = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Billing_Address").ResourceValue;
                        Generic_DeliveryAddress = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DeliveryAddress").ResourceValue;
                        SC_Payment_Method = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SC_Payment_Method").ResourceValue;
                        /*Sachin Chauhan End : 29 03 2016*/
                        strAddtionalcharges = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Handlingfee" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #region Code Added by SHRIGANESH SINGH 16 May 2016 for Password Policy
                        #region Minimum Password Length
                        string minPass = "0";
                        lstStoreDetail = StoreBL.GetStoreDetails();
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue != null)
                        {
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 2a");
                            minPass = lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue;
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 3a");
                            Exceptions.WriteInfoLog("Registration:PS_MinimumPasswordLength -" + minPass);
                        }
                        else if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue != null)
                        {
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 2b");
                            minPass = lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue;
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 3b");
                            Exceptions.WriteInfoLog("Registration:PS_MinimumPasswordLength -" + minPass);
                        }
                        #endregion
                        #region Password Policy
                        lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;
                            regexAlphaNumtxtPassword.Enabled = false;
                            regexAlphaNumSymtxtPassword.Enabled = false;
                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
                        }
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;
                            regexAlphaNumtxtPassword.Enabled = true;
                            regexAlphaNumSymtxtPassword.Enabled = false;
                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
                            regexAlphaNumtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]{0,50}";
                            regexAlphaNumtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        }
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;
                            regexAlphaNumtxtPassword.Enabled = false;
                            regexAlphaNumSymtxtPassword.Enabled = true;
                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
                            regexAlphaNumSymtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                            regexAlphaNumSymtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        }
                        #endregion
                        strMinPassLenErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strMinPassLenErrorMsg = strMinPassLenErrorMsg.Replace("@Num@", minPass);
                        strAlphaNumPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strAlphaNumSymPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnCreateAccount.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Create_Account_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_Password_ConfirmPassword_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtConfirmPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_Password_ConfirmPassword_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        cmpValidatorPwd.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_PasswordMisMatch_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegistrationDeclinedMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_registrationdeclined_message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblUserTypes.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_usertype" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblPreferredCurrency.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredCurrency_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblPreferredLanguage.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredLanguages_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_PreferredCurrency_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredCurrency_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_PreferredLanguages_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredLanguages_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Reg_Titles" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        #endregion
                        #region RESOURCES ADDED BY SHRIGANESH FOR GUEST USER REGISTRATION 14 FEB 2017
                        strTitleReqMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ddlTitle_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strPreferredLanguageReqMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredLanguages_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strPreferredCurrencyReqMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredCurrency_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Registration_Successfully_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Registration_Successfully_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Registration_Failure_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Registration_Failure_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_InvalidCaptchaText_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_InvalidCaptchaText_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Email_Sending_Failed_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Email_Sending_Failed_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strOrderDetails_Error_Processing_Registration_Error_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "OrderDetails_Error_Processing_Registration_Error_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtTitles.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ddlTitle_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 26-10-15
        /// Scope   : aCreateAccount_ServerClick of the anchor button
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void btnCreateAccount_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtTitles.Text))
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strTitleReqMessage, AlertType.Warning);
                    return;
                }
                if (drpdwnPreferredLanguage.SelectedIndex == 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPreferredLanguageReqMessage, AlertType.Warning);
                    return;
                }
                if (drpdwnPreferredCurrency.SelectedIndex == 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPreferredCurrencyReqMessage, AlertType.Warning);
                    return;
                }
                #region CODE ADDED BY SHRIGANESH TO REDUCE HITTING TO DATABASE AGAIN
                if (Session["UserTypeMapping"] != null)
                {
                    objUserTypeMappingBE = (UserTypeMappingBE)Session["UserTypeMapping"];
                }
                else
                {
                    BindUserTypeMappping();
                }
                #endregion
                UserBE lstUserBE = new UserBE();
                if (Session["User"] != null)
                {
                    lstUserBE = Session["User"] as UserBE;
                    intLanguageId = GlobalFunctions.GetLanguageId();
                    intCurrencyId = GlobalFunctions.GetCurrencyId();
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    string UserTitle = txtTitles.Text.Trim();

                    #region CODE ADDED BY SHRIGANESH SINGH FOR PASSWORD POLICY 13 MAY 2016
                    if (lstStoreDetail != null)
                    {
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue != null)
                        {
                            minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                        }
                        else
                        {
                            minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].DefaultValue;
                        }
                        if (objStoreBE != null)
                        {
                            lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                            if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                            {
                                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                                {
                                    string password = txtPassword.Text;
                                    minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                    if (password.Length < Convert.ToInt16(minPasswordLength))
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        return;
                                    }
                                }

                                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                                {
                                    string password = txtPassword.Text;
                                    minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                    if (password.Length >= Convert.ToInt16(minPasswordLength))
                                    {
                                        string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]{0,50}";
                                        bool check = true;
                                        check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                        if (!check)
                                        {
                                            string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        return;
                                    }
                                }
                                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                                {
                                    string password = txtPassword.Text;
                                    minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                    if (password.Length >= Convert.ToInt16(minPasswordLength))
                                    {
                                        string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                                        bool check = true;
                                        check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                        if (!check)
                                        {
                                            string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    UserBE objBE = new UserBE();
                    string SaltPass = SaltHash.ComputeHash((txtPassword.Text.Trim()), "SHA512", null);
                    objBE.UserId = lstUserBE.UserId;
                    objBE.FirstName = lstUserBE.FirstName;
                    objBE.LastName = lstUserBE.LastName;
                    objBE.Password = SaltPass;
                    objBE.PredefinedColumn1 = lstUserBE.PredefinedColumn1;
                    objBE.PredefinedColumn2 = lstUserBE.PredefinedColumn2;
                    objBE.PredefinedColumn3 = lstUserBE.PredefinedColumn3;
                    objBE.PredefinedColumn4 = lstUserBE.PredefinedColumn4;
                    objBE.PredefinedColumn5 = lstUserBE.PredefinedColumn5;
                    objBE.PredefinedColumn6 = lstUserBE.PredefinedColumn6;
                    objBE.PredefinedColumn7 = lstUserBE.PredefinedColumn7;
                    objBE.PredefinedColumn8 = lstUserBE.PredefinedColumn8;
                    objBE.PredefinedColumn9 = lstUserBE.PredefinedColumn9;
                    objBE.IPAddress = GlobalFunctions.GetIpAddress();
                    objBE.IsGuestUser = false;
                    objBE.UserTypeID = iUserTypeID;
                    objBE.UserTitle = UserTitle;

                    #region UserType Filtering"
                    //BindUserTypeMappping();		
                    UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);

                    objBE.UserTypeID = 1; // Modified to 1 from 0 by SHRIGANESH SINGH 14 July 2016		
                    #region "Validation according Option Selection"
                    if (objUserTypeMasterOptions != null)
                    {
                        #region "UserSelection"
                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 1)
                        {
                            if (ddlUserType.SelectedValue != "")
                            {
                                objBE.UserTypeID = Convert.ToInt16(ddlUserType.SelectedValue);
                                // Below LOC is added by SHRIGANESH SINGH to display Validation Alert 14 July 2016		
                                CustomFieldValidationError = SetCustomFields(objBE);
                                if (CustomFieldValidationError != "")
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                                    return;
                                }
                            }
                            else
                            {
                                CustomFieldValidationError = SetCustomFields(objBE);
                                if (CustomFieldValidationError != "")
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                                    return;
                                }
                            }
                        }
                        #endregion
                        #region "Email"
                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                        {
                            MailAddress address = new MailAddress(lstUserBE.EmailId.Trim());
                            HostDomain = address.Host;
                            UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidation = objUserTypeMappingBE.lstUserTypeEmailValidation.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.EmailId.ToLower() == lstUserBE.EmailId.ToLower().Trim() && x.IsWhiteList == true);
                            UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidationDomain = objUserTypeMappingBE.lstUserTypeEmailValidation.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.Domain.ToLower() == HostDomain.ToLower() && x.IsWhiteList == true);

                            if (objRegUserTypeEmailValidation != null)
                            {
                                objBE.UserTypeID = objRegUserTypeEmailValidation.UserTypeId;
                            }
                            else if (objRegUserTypeEmailValidationDomain != null)
                            {
                                objBE.UserTypeID = objRegUserTypeEmailValidationDomain.UserTypeId;
                            }
                            else
                            {
                                // Below LOC is commented by SHRIGANESH SINGH 19 Dec 2016		
                                //UserTypeMappingBE.UserTypeDetails objRegUserTypeDetails = objUserTypeMappingBE.lstUserTypeDetails.OrderBy(x => x.UserTypeID).FirstOrDefault(x => x.IsWhiteList == false);		
                                UserTypeMappingBE.UserTypeDetails objRegUserTypeDetails = objUserTypeMappingBE.lstUserTypeDetails.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.IsWhiteList == false);
                                if (objRegUserTypeDetails != null)
                                {
                                    objBE.UserTypeID = objRegUserTypeDetails.UserTypeID;
                                }
                                else
                                {
                                    objBE.UserTypeID = 1;
                                }
                            }
                            // Below LOC is added by SHRIGANESH SINGH to display Validation Alert 14 July 2016		
                            CustomFieldValidationError = SetCustomFields(objBE);
                            if (CustomFieldValidationError != "")
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                                return;
                            }
                        }
                        #endregion
                        #region "Country"
                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                        {
                            UserTypeMappingBE.Country objRegCountry = objUserTypeMappingBE.lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(objBE.PredefinedColumn8));
                            if (objRegCountry != null)
                            {
                                if (objRegCountry.UserTypeID > 0)
                                {
                                    objBE.UserTypeID = objRegCountry.UserTypeID;
                                }
                                else
                                {
                                    //objUser.UserTypeID = 1;//unallocated		
                                    string str = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Country_Register" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    GlobalFunctions.ShowModalAlertMessages(this, str, AlertType.Failure);
                                    return;
                                }
                            }
                            else
                            {
                                //objUser.UserTypeID = 1;//unallocated		
                                string str = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Country_Register" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                GlobalFunctions.ShowModalAlertMessages(this, str, AlertType.Failure);
                                return;
                            }
                            // Below LOC is added by SHRIGANESH SINGH to display Validation Alert 14 July 2016		
                            CustomFieldValidationError = SetCustomFields(objBE);
                            if (CustomFieldValidationError != "")
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                                return;
                            }
                        }
                        #endregion
                        #region "Custom Mapping"
                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                        {
                            //objUser.UserTypeID = Convert.ToInt16(ddlUserType.SelectedValue);		
                            #region "CustomFields For UserType"
                            Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields start.");
                            try
                            {
                                #region rptUserTypeCustomFields
                                foreach (RepeaterItem item in rptCustomMapping.Items)
                                {
                                    TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                                    DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                                    HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                                    HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                                    CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");

                                    PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                    object value = null;
                                    if (fi != null)
                                    {
                                        try
                                        {
                                            #region "customColumns"
                                            if (hdfControl.Value.ToLower() == "textbox")
                                            {
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping textbox.");
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping before set value in customColumns");
                                                if (txtCustomFieldName != null)
                                                {
                                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);
                                                }
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping after set value in customColumns");
                                            }
                                            else if (hdfControl.Value.ToLower() == "dropdown")
                                            {
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping dropdown.");
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping before set value in customColumns");
                                                if (ddlCustomValue != null)
                                                {
                                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                                }
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping after set value in customColumns");
                                            }
                                            else if (hdfControl.Value.ToLower() == "checkbox")
                                            {
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping checkbox.");
                                                if (chkCustomValue != null)
                                                {
                                                    Exceptions.WriteInfoLog("Registration:rptCustomMapping before set value in customColumns");
                                                    #region
                                                    foreach (ListItem chkItem in chkCustomValue.Items)
                                                    {
                                                        if (chkItem.Selected)
                                                        {
                                                            if (iCount == 0)
                                                            {
                                                                value = chkItem.Value;
                                                                iCount++;
                                                            }
                                                            else
                                                            {
                                                                value = value + "|" + chkItem.Value;
                                                                iCount++;
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                                    Exceptions.WriteInfoLog("Registration:rptCustomMapping after set value in customColumns");
                                                }
                                            }
                                            #endregion
                                        }
                                        catch (InvalidCastException) { }
                                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                                        {
                                            b = false;
                                            if (objUserTypeMappingBE != null)
                                            {
                                                Int16 iRegistrationFieldsConfigurationId = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.SystemColumnName == hdfColumnName.Value).RegistrationFieldsConfigurationId;
                                                UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objValRegistrationFieldDataMasterValidationBE = objUserTypeMappingBE.lstRegistrationFieldDataMasterValidationBE.FirstOrDefault(x => x.RegistrationFieldDataValue.ToLower() == Convert.ToString(value).ToLower());

                                                if (objValRegistrationFieldDataMasterValidationBE != null)
                                                {
                                                    b = true;
                                                    objBE.UserTypeID = Convert.ToInt16(objValRegistrationFieldDataMasterValidationBE.UserTypeID);
                                                }
                                                if (b == false)
                                                {
                                                    if (objBE.UserTypeID == 1)
                                                    {
                                                        objBE.UserTypeID = 1;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            b = true;
                                        }
                                        fi.SetValue(objBE, value, null);
                                    }
                                }
                                #endregion
                                CustomFieldValidationError = SetCustomFields(objBE);
                                if (CustomFieldValidationError != "")
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                                    return;
                                }
                            }
                            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                            Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields end.");
                            #endregion
                        }
                        #endregion
                    }
                    #endregion

                    #region "CustomFields For Non UserType"
                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll start.");
                    try
                    {
                        #region rptCustomFieldsAll
                        string strError = "";
                        bool custom = true;
                        foreach (RepeaterItem item in rptCustomFieldsAll.Items)
                        {
                            TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                            DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                            HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                            HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                            CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                            // Below LOC is added by SHRIGANESH SINGH 13 July 2016 to get the value each RegistrationFieldsConfigurationId for Validation		
                            HiddenField hdnRegistrationFieldsConfigurationId = (HiddenField)item.FindControl("hdnRegistrationFieldsConfigurationId");
                            Label lblCustomFieldName = (Label)item.FindControl("lblCustomFieldName");

                            PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            object value = null;
                            if (fi != null)
                            {
                                try
                                {
                                    #region "customColumns"
                                    if (hdfControl.Value.ToLower() == "textbox")
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll textbox.");
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll before set value in customColumns");
                                        if (txtCustomFieldName != null)
                                        {
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);

                                            int bResult = RegistrationCustomFieldBL.ValidateRegistrationCustomFields(value, hdnRegistrationFieldsConfigurationId.Value);

                                            if (bResult == 0)
                                            {
                                                strError = strCustomFieldValidationError + " " + lblCustomFieldName.Text.Replace("*", "");
                                                GlobalFunctions.ShowModalAlertMessages(this.Page, strError, AlertType.Warning);
                                                return;
                                            }
                                            else if (bResult == 3)
                                            {
                                                strError = strError = strGeneric_Existing_LoginFieldValue_Message.Replace("@fieldName", lblCustomFieldName.Text.Replace("*", ""));
                                                GlobalFunctions.ShowModalAlertMessages(this.Page, strError, AlertType.Warning);
                                                return;
                                            }
                                        }
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll after set value in customColumns");
                                    }
                                    else if (hdfControl.Value.ToLower() == "dropdown")
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll dropdown.");
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll before set value in customColumns");
                                        if (ddlCustomValue != null)
                                        {
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                        }
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll after set value in customColumns");
                                    }
                                    else if (hdfControl.Value.ToLower() == "checkbox")
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll checkbox.");
                                        if (chkCustomValue != null)
                                        {
                                            Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll before set value in customColumns");
                                            #region
                                            foreach (ListItem chkItem in chkCustomValue.Items)
                                            {
                                                if (chkItem.Selected)
                                                {
                                                    if (iCount == 0)
                                                    {
                                                        value = chkItem.Value;
                                                        iCount++;
                                                    }
                                                    else
                                                    {
                                                        value = value + "|" + chkItem.Value;
                                                        iCount++;
                                                    }
                                                }
                                            }
                                            #endregion
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                            Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll after set value in customColumns");
                                        }
                                    }
                                    #endregion
                                }
                                catch (InvalidCastException) { }

                                b = false;
                                if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                                {
                                    if (objUserTypeMappingBE != null)
                                    {
                                        Int16 iRegistrationFieldsConfigurationId = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.SystemColumnName == hdfColumnName.Value).RegistrationFieldsConfigurationId;
                                        List<UserTypeMappingBE.RegistrationFieldDataMasterValidationBE> objValRegistrationFieldDataMasterValidationBELst = objUserTypeMappingBE.lstRegistrationFieldDataMasterValidationBE.FindAll(x => x.RegistrationFieldsConfigurationId == iRegistrationFieldsConfigurationId);
                                        if (objValRegistrationFieldDataMasterValidationBELst != null)
                                        {
                                            UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objUserTypeMappingBERegistrationFieldDataMasterValidationBE = objValRegistrationFieldDataMasterValidationBELst.FirstOrDefault(x => x.RegistrationFieldDataValue.ToLower().Equals(Convert.ToString(value)));
                                            if (objUserTypeMappingBERegistrationFieldDataMasterValidationBE != null)
                                            {
                                                b = true;
                                                objBE.UserTypeID = Convert.ToInt16(objUserTypeMappingBERegistrationFieldDataMasterValidationBE.UserTypeID);
                                            }
                                        }
                                        if (b == false)
                                        {
                                            if (objBE.UserTypeID == 1)
                                            {
                                                objBE.UserTypeID = 1;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    b = true;
                                }

                                fi.SetValue(objBE, value, null);
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll end.");
                    #endregion
                    #endregion
                    int i = UserBL.ExecuteProfileDetails(Constants.USP_InsertUserProfileDetails, true, objBE);
                    UserPreferredBE objUserPreferredBE = new UserPreferredBE();
                    objUserPreferredBE.UserId = objBE.UserId;
                    objUserPreferredBE.PreferredLanguageId = Convert.ToInt16(drpdwnPreferredLanguage.SelectedValue);
                    objUserPreferredBE.PreferredCurrencyId = Convert.ToInt16(drpdwnPreferredCurrency.SelectedValue);
                    UserPreferredBL.InsertUpdateUserPreference(Constants.usp_InsertUpdateUserPreferred, true, objUserPreferredBE);
                    if (i > 0)
                    {
                        lstCurrencies = CurrencyBL.GetAllCurrencyDetails();
                        GlobalFunctions.SetLanguageId(Convert.ToInt16(objUserPreferredBE.PreferredLanguageId));
                        GlobalFunctions.SetCurrencyId(Convert.ToInt16(objUserPreferredBE.PreferredCurrencyId));
                        GlobalFunctions.SetCurrencySymbol(lstCurrencies.First(x => x.CurrencyId == objUserPreferredBE.PreferredCurrencyId).CurrencySymbol);

                        UserBE objUser = new UserBE();
                        UserBE objBEs = new UserBE();                                              
                        objBEs.EmailId = lstUserBE.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;                            
                        }

                        if (GlobalFunctions.SendRegistrationMail(lstUserBE.EmailId, lstUserBE.FirstName, lstUserBE.LastName))
                        {
                            //Session.Abandon();
                            Session["IsAnonymous"] = null;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strGeneric_Registration_Successfully_Msg, host + "index", AlertType.Success);
                        }
                        else
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strGeneric_Email_Sending_Failed_Msg, AlertType.Failure);
                    }
                    else
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strOrderDetails_Error_Processing_Registration_Error_Msg, AlertType.Failure);
                }
                else
                {
                    Response.RedirectToRoute("login");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Code add for E-commerce GA tracking
        /// </summary>
        private void GAtracking()
        {
            //GA tracking code implementation 
            string strGAtracking = "";
            decimal dSubTotalPriceGA = 0;
            if (!string.IsNullOrEmpty(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).GACode))
            {
                strGAtracking = "<script type=\"text/javascript\"> " +
                  "var _gaq = _gaq || [];" +
                  "_gaq.push(['_setAccount', '" + lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).GACode + "']);" +
                  "_gaq.push(['_trackPageview']);";

                if (lstCustomerOrders.CustomerOrderProducts != null)
                {
                    string strGAtrackingProducts = "";
                    for (int i = 0; i < lstCustomerOrders.CustomerOrderProducts.Count; i++)
                    {
                        strGAtrackingProducts = strGAtrackingProducts +
                                  "_gaq.push(['_addItem'," +
                                  "'" + Convert.ToString(Guid.NewGuid()) + "'," +  // transaction ID - required
                                  "'" + Convert.ToString(lstCustomerOrders.CustomerOrderProducts[i].SKU) + "'," +        // SKU/code - required
                                  "'" + Convert.ToString(lstCustomerOrders.CustomerOrderProducts[i].SKUName) + "'," +        // product name
                                   "''," +                                                  // category or variation
                                  "'" + Convert.ToString(lstCustomerOrders.CustomerOrderProducts[i].Price) + "'," +       // unit price - required
                                  "'" + Convert.ToString(lstCustomerOrders.CustomerOrderProducts[i].Quantity) + "'" +     // quantity - required
                                  "]);";
                        dSubTotalPriceGA += Convert.ToDecimal(Convert.ToDecimal(lstCustomerOrders.CustomerOrderProducts[i].Price) * Convert.ToInt32(lstCustomerOrders.CustomerOrderProducts[i].Quantity));
                    }
                    decimal dShippingCharges = 0.00M;
                    if (lstCustomerOrders.StandardCharges > 0)
                    {
                        dShippingCharges = Convert.ToDecimal(lstCustomerOrders.StandardCharges);
                    }
                    else if (lstCustomerOrders.ExpressCharges > 0)
                    {
                        dShippingCharges = Convert.ToDecimal(lstCustomerOrders.ExpressCharges);
                    }
                    else if (lstCustomerOrders.PalletCharges > 0)
                    {
                        dShippingCharges = Convert.ToDecimal(lstCustomerOrders.PalletCharges);
                    }
                    else
                    {
                        dShippingCharges = 0.00M;
                    }
                    decimal dTaxes = Convert.ToDecimal(lstCustomerOrders.TotalTax);
                    decimal grandtotal = dSubTotalPrice + dShippingCharges + dTaxes;

                    strGAtracking = strGAtracking +
                        "_gaq.push(['_addTrans'," +
                        "'" + Convert.ToString(Guid.NewGuid()) + "'," +
                        "'" + lstStoreDetail.StoreName + "', " +                      // affiliation or store name
                        "'" + Convert.ToDecimal(grandtotal.ToString("##,###,##0.#0")) + "',  " +       // total - required Does not include Tax and Shipping. lblfinaltotal 
                        "'" + Convert.ToDecimal(lstCustomerOrders.TotalTax.ToString("##,###,##0.#0")) + "'," +           // tax
                        "'" + dShippingCharges + "', " +      // shipping
                        "'" + lstCustomerOrders.DeliveryCity + "'," +                    // city
                        "'" + lstCustomerOrders.DeliveryCounty + "', " +                  // state or province
                        "'" + lstCustomerOrders.DeliveryCountryName + "' " +                 // country
                      "]);";
                    strGAtracking = strGAtracking + strGAtrackingProducts;
                    strGAtracking = strGAtracking + "_gaq.push(['_trackTrans']);";
                    strGAtracking = strGAtracking + "(function() { " +
                        "var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;" +
                        "ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';" +
                        "var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);" +
                        "})();";

                    strGAtracking = strGAtracking + "</script>";
                    ltrGATrackingEcomm.Text = strGAtracking;
                }
            }
        }

        #region "UserType"
        /*User Type*/
        private string SetCustomFields(UserBE obj)
        {
            string strError = "";
            try
            {
                #region rptUserTypeCustomFields
                foreach (RepeaterItem item in rptUserTypeCustomFields.Items)
                {
                    TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                    DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                    HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                    HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                    CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                    Label lblCustomFieldName = (Label)item.FindControl("lblCustomFieldName");
                    HiddenField hdnRegistrationFieldsConfigurationId = (HiddenField)item.FindControl("hdnRegistrationFieldsConfigurationId");
                    PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    object value = null;
                    if (fi != null)
                    {
                        try
                        {
                            #region "customColumns"
                            if (hdfControl.Value.ToLower() == "textbox")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields textbox.");
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                if (txtCustomFieldName != null)
                                {
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);
                                    int bResult = RegistrationCustomFieldBL.ValidateRegistrationCustomFields(value, hdnRegistrationFieldsConfigurationId.Value);

                                    if (bResult == 0)
                                    {
                                        strError = strCustomFieldValidationError + " " + lblCustomFieldName.Text.Replace("*", "");
                                    }
                                    else if (bResult == 3)
                                    {
                                        strError = strGeneric_Existing_LoginFieldValue_Message.Replace("@fieldName", lblCustomFieldName.Text.Replace("*", ""));
                                    }
                                }
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                            }
                            else if (hdfControl.Value.ToLower() == "dropdown")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields dropdown.");
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                if (ddlCustomValue != null)
                                {
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                }
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                            }
                            else if (hdfControl.Value.ToLower() == "checkbox")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields checkbox.");
                                if (chkCustomValue != null)
                                {
                                    Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                    #region
                                    foreach (ListItem chkItem in chkCustomValue.Items)
                                    {
                                        if (chkItem.Selected)
                                        {
                                            if (iCount == 0)
                                            {
                                                value = chkItem.Value;
                                                iCount++;
                                            }
                                            else
                                            {
                                                value = value + "|" + chkItem.Value;
                                                iCount++;
                                            }
                                        }
                                    }
                                    #endregion
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                    Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                                }
                            }
                            #endregion
                            fi.SetValue(obj, value, null);
                        }
                        catch (InvalidCastException) { }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return strError;
        }

        protected void BindUserTypeMappping()
        {
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                objUserTypeMappingBE = UserTypesBL.getCollectionItemUserTypeMappingbyCId(Constants.USP_UserTypeMappingDetailsByCID, DictionaryInstance, true);
                Session["UserTypeMapping"] = objUserTypeMappingBE;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindUserSelection()
        {
            if (objUserTypeMappingBE != null)
            {
                List<UserTypeMappingBE.CustomUserTypes> objCustomUserTypes = objUserTypeMappingBE.lstCustomUserTypes.FindAll(x => x.LanguageID == GlobalFunctions.GetLanguageId());
                List<UserTypeMappingBE.CustomUserTypes> objCS = objCustomUserTypes.FindAll(x => x.UserTypeID != 1);
                if (objCS.Count > 0)
                {
                    ddlUserType.DataSource = objCS;// objCustomUserTypes.FindAll(x => x.UserTypeID != 1);		
                    ddlUserType.DataTextField = "UserTypeName";
                    ddlUserType.DataValueField = "UserTypeID";
                    ddlUserType.DataBind();
                    GlobalFunctions.AddDropdownItem(ref ddlUserType);
                }
                else
                {
                    ddlUserType.Visible = false;
                    lblUserTypes.Visible = false;
                    reqddlUserType.Enabled = false;
                }
            }
            #region "Default Custom Fields"
            List<UserTypeMappingBE.RegistrationFieldsConfigurationBE> objReggobjRegistrationFieldsConfigurationBEL = new List<UserTypeMappingBE.RegistrationFieldsConfigurationBE>();
            objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping = objUserTypeMappingBE.lstUserTypeCustomFieldMapping.FindAll(x => x.UserTypeID == 1);

            objTempUserTypeMappingBE.lstRegistrationLanguagesBE = objUserTypeMappingBE.lstRegistrationLanguagesBE.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId > 10);

            foreach (UserTypeMappingBE.UserTypeCustomFieldMapping obj in objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping)
            {
                UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE1 = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == obj.RegistrationFieldsConfigurationId && x.IsVisible == true && x.IsStoreDefault == false);
                if (objRegistrationFieldsConfigurationBE1 != null)
                {
                    UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserTypeMappingBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                    objReggobjRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE);
                }
            }
            if (objReggobjRegistrationFieldsConfigurationBEL.Count > 0)
            {
                divUserType.Visible = true;
            }
            rptCustomFieldsAll.DataSource = objReggobjRegistrationFieldsConfigurationBEL;
            rptCustomFieldsAll.DataBind();
            #endregion
        }

        private void BindCustomFields()
        {
            try
            {
                if (objUserTypeMappingBE != null)
                {
                    List<UserTypeMappingBE.CustomUserTypes> objCustomUserTypes = objUserTypeMappingBE.lstCustomUserTypes.FindAll(x => x.LanguageID == GlobalFunctions.GetLanguageId());
                    List<UserTypeMappingBE.CustomUserTypes> objCS = objCustomUserTypes.FindAll(x => x.UserTypeID != 1);
                    if (objCS.Count > 0)
                    {
                        UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                        #region
                        if (objUserTypeMasterOptions != null)
                        {
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2 || objUserTypeMasterOptions.UserTypeMappingTypeID == 3 || objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                            {
                            }
                            else
                            {
                                divUserType.Visible = true;
                            }
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                            {
                            }
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                            {
                            }
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 1)
                            {
                                ddlUserType.Attributes.Add("onchange", "javascript: ShowLoader();");
                            }

                            if (objUserTypeMasterOptions.UserTypeMappingTypeID != 2)
                            {
                            }
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID != 3)
                            {
                            }
                        }
                        #endregion
                        #region "rptUserTypeCustomFields"
                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                        {
                            BindCustomMapping();
                        }
                        #endregion
                    }
                    else
                    {                        
                        ddlUserType.Visible = false;
                        lblUserTypes.Visible = false;
                        reqddlUserType.Enabled = false;
                    }
                }
                else
                {
                    GlobalFunctions.AddDropdownItem(ref ddlUserType);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindVisibleUserType(Convert.ToInt16(ddlUserType.SelectedValue));
                BindPreferredCurrency(Convert.ToInt16(ddlUserType.SelectedValue));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptUserTypeCustomFields_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    #region
                    Int16 iRegId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                    UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserTypeMappingBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);
                    UserTypeMappingBE.RegistrationLanguagesBE objLang = objUserTypeMappingBE.lstRegistrationLanguagesBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId & x.LanguageId == GlobalFunctions.GetLanguageId());

                    HiddenField hdfColumnName = (HiddenField)e.Item.FindControl("hdfColumnName");
                    HiddenField hdfControl = (HiddenField)e.Item.FindControl("hdfControl");
                    Label lblCustomFieldName = (Label)e.Item.FindControl("lblCustomFieldName");
                    TextBox txtCustomFieldName = (TextBox)e.Item.FindControl("txtCustomFieldName");
                    DropDownList ddlCustomValue = (DropDownList)e.Item.FindControl("ddlCustomValue");
                    CheckBoxList chkCustomValue = (CheckBoxList)e.Item.FindControl("chkCustomValue");
                    HiddenField hdnRegistrationFieldsConfigurationId = (HiddenField)e.Item.FindControl("hdnRegistrationFieldsConfigurationId");
                    hdnRegistrationFieldsConfigurationId.Value = Convert.ToString(iRegId);
                    RequiredFieldValidator reqtxtCustomFieldName = (RequiredFieldValidator)e.Item.FindControl("reqtxtCustomFieldName");
                    RequiredFieldValidator reqddlCustomValue = (RequiredFieldValidator)e.Item.FindControl("reqddlCustomValue");

                    if (hdfColumnName != null)
                    {
                        hdfColumnName.Value = objRegistrationFieldsConfigurationBE.SystemColumnName;
                    }
                    if (objRegistrationFieldsConfigurationBE.IsMandatory)
                    {
                        lblCustomFieldName.Text = "*" + objLang.LabelTitle;
                    }
                    else
                    {
                        lblCustomFieldName.Text = objLang.LabelTitle;
                    }

                    if (objRegistrationFieldsConfigurationBE.FieldType == 2)
                    {
                        #region "DropDownList"
                        hdfControl.Value = "dropdown";
                        ddlCustomValue.Visible = true;
                        Int16 oRegistrationFieldsConfigurationId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                        ddlCustomValue.DataSource = objUserTypeMappingBE.lstRegistrationFieldDataMasterBE.FindAll(x => x.RegistrationFieldsConfigurationId == oRegistrationFieldsConfigurationId);// && x.LanguageID == GlobalFunctions.GetLanguageId());		
                        ddlCustomValue.DataTextField = "RegistrationFieldDataValue";
                        ddlCustomValue.DataValueField = "RegistrationFieldDataMasterId";
                        ddlCustomValue.DataBind();

                        txtCustomFieldName.Visible = false;
                        if (reqtxtCustomFieldName != null)
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqddlCustomValue != null)
                            {
                                reqddlCustomValue.Visible = true;
                                reqddlCustomValue.Enabled = true;
                                reqddlCustomValue.ErrorMessage = Generic_Select_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 1)
                    {
                        #region "TextBox"
                        hdfControl.Value = "textbox";
                        ddlCustomValue.Visible = false;
                        txtCustomFieldName.Visible = true;

                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)//! remove		
                        {
                            if (reqtxtCustomFieldName != null)
                            {
                                reqtxtCustomFieldName.Visible = true;
                                reqtxtCustomFieldName.Enabled = true;
                                reqtxtCustomFieldName.ErrorMessage = Generic_Enter_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 3)
                    {
                        #region "CheckBox
                        hdfControl.Value = "checkbox";
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = true;
                            Int16 oRegistrationFieldsConfigurationId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                            chkCustomValue.DataSource = objUserTypeMappingBE.lstRegistrationFieldDataMasterBE.FindAll(x => x.RegistrationFieldsConfigurationId == oRegistrationFieldsConfigurationId);// && x.LanguageID == GlobalFunctions.GetLanguageId());		
                            chkCustomValue.DataTextField = "RegistrationFieldDataValue";
                            chkCustomValue.DataValueField = "RegistrationFieldDataMasterId";
                            chkCustomValue.DataBind();
                        }
                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (txtCustomFieldName != null)
                        {
                            txtCustomFieldName.Visible = false;
                        }
                        if (ddlCustomValue != null)
                        {
                            ddlCustomValue.Visible = false;
                        }

                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (reqtxtCustomFieldName != null)
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 4)//Pending Custom Mapping		
                    { }
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindVisibleUserType(Int16 iSUertype)
        {
            try
            {
                if (Session["UserTypeMapping"] != null)
                {
                    objUserTypeMappingBE = (UserTypeMappingBE)Session["UserTypeMapping"];
                }
                else
                {
                    BindUserTypeMappping();
                }

                #region "rptUserTypeCustomFields"
                List<UserTypeMappingBE.RegistrationFieldsConfigurationBE> objReggobjRegistrationFieldsConfigurationBEL = new List<UserTypeMappingBE.RegistrationFieldsConfigurationBE>();
                objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping = objUserTypeMappingBE.lstUserTypeCustomFieldMapping.FindAll(x => x.UserTypeID != 1 && x.UserTypeID == iSUertype);

                objTempUserTypeMappingBE.lstRegistrationLanguagesBE = objUserTypeMappingBE.lstRegistrationLanguagesBE.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId > 10);

                foreach (UserTypeMappingBE.UserTypeCustomFieldMapping obj in objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping)
                {
                    UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE1 = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == obj.RegistrationFieldsConfigurationId && x.IsVisible == true);
                    if (objRegistrationFieldsConfigurationBE1 != null)
                    {
                        UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserTypeMappingBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                        objReggobjRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE);
                    }
                }
                if (objReggobjRegistrationFieldsConfigurationBEL.Count > 0)
                {
                    rptUserTypeCustomFields.Visible = true;
                }
                rptUserTypeCustomFields.DataSource = objReggobjRegistrationFieldsConfigurationBEL;
                rptUserTypeCustomFields.DataBind();
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptInvoiceAddress_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlRegisterCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList drp = (DropDownList)sender;
                RepeaterItem itm = (RepeaterItem)drp.Parent;

                HiddenField hdfColumnName = (HiddenField)itm.FindControl("hdfColumnName");
                if (hdfColumnName != null)
                {
                    if (hdfColumnName.Value.ToLower() == "predefinedcolumn8")
                    {
                        //BindUserTypeMappping();		
                        UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                        if (objUserTypeMasterOptions != null)
                        {
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                            {
                                #region "Country"
                                if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                                {
                                    UserTypeMappingBE.Country objRegCountry = objUserTypeMappingBE.lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(drp.SelectedValue));
                                    if (objRegCountry != null)
                                    {
                                        if (objRegCountry.UserTypeID > 0)
                                        {
                                            BindVisibleUserType(objRegCountry.UserTypeID);
                                        }
                                        else
                                        {
                                            rptUserTypeCustomFields.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        rptUserTypeCustomFields.Visible = false;
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowPnl2();", true);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindCustomMapping()
        {
            try
            {
                List<UserTypeMappingBE.RegistrationFieldsConfigurationBE> objReg = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FindAll(x => x.RegistrationFieldsConfigurationId == 11);
                rptCustomMapping.DataSource = objReg;// objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FindAll(x => x.IsStoreDefault == true);		
                rptCustomMapping.DataBind();

                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                if (objUserTypeMasterOptions != null)
                {
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                    {
                        foreach (RepeaterItem item in rptCustomMapping.Items)
                        {
                            TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                            txtCustomFieldName.AutoPostBack = true;
                            txtCustomFieldName.TextChanged += txtCustomFieldName_TextChanged;
                            txtCustomFieldName.Attributes.Add("onchange", "javascript: ShowLoader();");
                        }
                    }
                }
                if (objReg.Count > 0)
                {
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void txtCustomFieldName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                if (objUserTypeMasterOptions != null)
                {
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                    {
                        #region "CustomFields For UserType"
                        Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields start.");
                        try
                        {
                            #region rptCustomMapping
                            foreach (RepeaterItem item in rptCustomMapping.Items)
                            {
                                TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                                DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                                HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                                HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                                CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                                Label lblCustomFieldName = (Label)item.FindControl("lblCustomFieldName");

                                PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                object value = null;
                                if (fi != null)
                                {
                                    try
                                    {
                                        #region "customColumns"
                                        if (hdfControl.Value.ToLower() == "textbox")
                                        {
                                            if (txtCustomFieldName != null)
                                            {
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);
                                            }
                                        }
                                        else if (hdfControl.Value.ToLower() == "dropdown")
                                        {
                                            if (ddlCustomValue != null)
                                            {
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                            }
                                        }
                                        else if (hdfControl.Value.ToLower() == "checkbox")
                                        {
                                            if (chkCustomValue != null)
                                            {
                                                #region
                                                foreach (ListItem chkItem in chkCustomValue.Items)
                                                {
                                                    if (chkItem.Selected)
                                                    {
                                                        if (iCount == 0)
                                                        {
                                                            value = chkItem.Value;
                                                            iCount++;
                                                        }
                                                        else
                                                        {
                                                            value = value + "|" + chkItem.Value;
                                                            iCount++;
                                                        }
                                                    }
                                                }
                                                #endregion
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                            }
                                        }
                                        #endregion
                                    }
                                    catch (InvalidCastException) { }
                                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                                    {
                                        if (objUserTypeMappingBE != null)
                                        {
                                            Int16 iRegistrationFieldsConfigurationId = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.SystemColumnName == hdfColumnName.Value).RegistrationFieldsConfigurationId;
                                            UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objValRegistrationFieldDataMasterValidationBE = objUserTypeMappingBE.lstRegistrationFieldDataMasterValidationBE.FirstOrDefault(x => x.RegistrationFieldDataValue.ToLower() == Convert.ToString(value).ToLower());
                                            if (objValRegistrationFieldDataMasterValidationBE != null)
                                            {
                                                iUserTypeID = Convert.ToInt16(objValRegistrationFieldDataMasterValidationBE.UserTypeID);
                                            }
                                            else
                                            {
                                                UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objCustomFieldValidation = UserTypesBL.GetAllCustomFieldValidationRule(iRegistrationFieldsConfigurationId);
                                                if (objCustomFieldValidation.RegisterAsDefault)
                                                {
                                                    iUserTypeID = 1;
                                                }
                                                else if (objCustomFieldValidation.DeclineRegister)
                                                {
                                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strRegistrationDeclinedMessage + " " + lblCustomFieldName.Text, AlertType.Warning);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            BindVisibleUserType(iUserTypeID);
                            divPreferredCurrency.Visible = true;
                            BindPreferredCurrency(iUserTypeID);
                            #endregion
                        }
                        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                        #endregion
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #endregion

        private void BindPreferredLanguages()
        {
            try
            {
                List<StoreBE.StoreLanguageBE> GetStoreLanguages = StoreLanguageBL.GetActiveStoreLanguageDetails();

                if (GetStoreLanguages != null)
                {
                    if (GetStoreLanguages.Count > 0)
                    {
                        lblPreferredLanguage.Visible = true;
                        drpdwnPreferredLanguage.Visible = true;
                        drpdwnPreferredLanguage.DataSource = GetStoreLanguages;
                        drpdwnPreferredLanguage.DataValueField = "LanguageId";
                        drpdwnPreferredLanguage.DataTextField = "LanguageName";
                        drpdwnPreferredLanguage.DataBind();
                        GlobalFunctions.AddDropdownItem(ref drpdwnPreferredLanguage);
                        if (GetStoreLanguages.Count == 1)
                        {
                            lblPreferredLanguage.Visible = false;
                            drpdwnPreferredLanguage.Visible = false;
                            drpdwnPreferredLanguage.SelectedValue = Convert.ToString(GetStoreLanguages[0].LanguageId);
                        }
                    }
                    else
                    {
                        lblPreferredLanguage.Visible = false;
                        drpdwnPreferredLanguage.Visible = false;
                    }
                }
                else
                {
                    lblPreferredLanguage.Visible = false;
                    drpdwnPreferredLanguage.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindPreferredCurrency(int UserTypeId)
        {
            try
            {
                List<UserPreferredBE.UserTypeCurrency> GetCurrencyUserTypeWise = UserPreferredBL.GetCurrencyUserTypeWise(UserTypeId);
                if (GetCurrencyUserTypeWise != null)
                {
                    if (GetCurrencyUserTypeWise.Count > 0)
                    {
                        divPreferredCurrency.Visible = true;
                        lblPreferredCurrency.Visible = true;
                        drpdwnPreferredCurrency.Visible = true;
                        drpdwnPreferredCurrency.DataSource = GetCurrencyUserTypeWise;
                        drpdwnPreferredCurrency.DataValueField = "CurrencyId";
                        drpdwnPreferredCurrency.DataTextField = "CurrencyName";
                        drpdwnPreferredCurrency.DataBind();
                        GlobalFunctions.AddDropdownItem(ref drpdwnPreferredCurrency);
                        if (GetCurrencyUserTypeWise.Count == 1)
                        {
                            lblPreferredCurrency.Visible = false;
                            drpdwnPreferredCurrency.Visible = false;
                            drpdwnPreferredCurrency.SelectedValue = Convert.ToString(GetCurrencyUserTypeWise[0].CurrencyId);
                        }
                    }
                    else
                    {
                        lblPreferredCurrency.Visible = false;
                        drpdwnPreferredCurrency.Visible = false;
                    }
                }
                else
                {
                    lblPreferredCurrency.Visible = false;
                    drpdwnPreferredCurrency.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>		
        /// Name: Added By Hardik 		
        /// Date: 19 JAN 2017		
        /// </summary>		
        private void BindUserTitles()
        {
            try
            {
                #region "ddlTitles"
                List<UserBE.Titles> lstTitles = UserBL.GetTitles(Convert.ToInt16(GlobalFunctions.GetLanguageId()));
                ddlTitles.DataSource = lstTitles;
                ddlTitles.DataTextField = "Title";
                ddlTitles.DataValueField = "TitleMasterID";
                ddlTitles.DataBind();
                GlobalFunctions.AddDropdownItem(ref ddlTitles);
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
    }
}