﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHPP.ASP.Net.Lib;
using PWGlobalEcomm.BusinessLogic;
using System.Configuration;

namespace Presentation
{
    public class ShoppingCart_CreditCardPayment : System.Web.UI.Page
    {
        #region variables
        protected global::System.Web.UI.HtmlControls.HtmlIframe TransFrame;

        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        Int32 intUserId = 0;
        UserBE lst;
        public double charges = 0;
        public string host = GlobalFunctions.GetVirtualPath();

        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 23-09-15
        /// Scope   : Page_Load of the ShoppingCart_CreditCardPayment page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["User"] != null)
                {
                    lst = new UserBE();
                    lst = Session["User"] as UserBE;
                    intUserId = lst.UserId;
                }
                else
                {
                    Response.RedirectToRoute("login-page");
                    return;
                }

                intCurrencyId = GlobalFunctions.GetCurrencyId();
                intLanguageId = GlobalFunctions.GetLanguageId();

                Message transaction = new Message("3.3.2");
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                CustomerOrderBE lstCustomerOrders = new CustomerOrderBE();
                CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
                objCustomerOrderBE.OrderedBy = lst.UserId;
                objCustomerOrderBE.LanguageId = intLanguageId;
                lstCustomerOrders = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                if (lstCustomerOrders == null)
                {
                    Response.RedirectToRoute("index");
                    return;
                }

                transaction.TxRefGUID = Funcs.CreateTxRefGUID();
                //transaction.TransactionType = 4; //Commented By Hardik as per Harley's request received on "25/Aug/2016"
                transaction.TransactionType = 9; // changed from 4 to 9 By Hardik on "17/Nov/2016" 
                transaction.Language = objStoreBE.StoreLanguages.FirstOrDefault(x => x.LanguageId == GlobalFunctions.GetLanguageId()).CultureCode;
                transaction.CurrencyCode = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).AdflexCurrencyCode;

                if (lstCustomerOrders.StandardCharges != 0)
                { charges = lstCustomerOrders.StandardCharges; }
                else if (lstCustomerOrders.ExpressCharges != 0)
                {charges = lstCustomerOrders.ExpressCharges; }
                else if (lstCustomerOrders.PalletCharges != 0)
                { charges = lstCustomerOrders.PalletCharges;}
                else
                { charges = 0.0;}


                transaction.Amount = Convert.ToString(CalculateTotalAmount(charges, lstCustomerOrders.TotalTax, lstCustomerOrders));
                // transaction.Amount = "22.99";
                transaction.TransactionRef = "P" + Convert.ToString(Guid.NewGuid()).Replace("-", "").Substring(0, 11);
                transaction.Version = Convert.ToString(ConfigurationManager.AppSettings["Version"]);
                transaction.AuthenticationToken = Convert.ToString(ConfigurationManager.AppSettings["PSPToken"]);
                transaction.MessageType = (int)MsgType.MerchantSubmitTransaction;
                transaction.TransactionFlags = 0;
                transaction.TransactionDescription = "Goods Sold";
                transaction.NotificationURLID = 4; // 1 for LIVE ENVIRONMENT
                //transaction.NotificationURLID = 2; // BETA environment "Added By Hardik on 17/Nov/2016"
                // transaction.NotificationURLID = 1; // 1 for TEST ENVIRONMENT
                transaction.SupportedCardsURLID = 0;
                transaction.CardHolderName = lst.FirstName.Trim() + " " + lst.LastName.Trim();
                transaction.TxRefGUID = lstCustomerOrders.TxnRefGUID;
                transaction.CardHolderAddress1 = lstCustomerOrders.DeliveryAddress1;
                transaction.CardHolderAddress2 = lstCustomerOrders.DeliveryAddress2;
                transaction.CardHolderCity = lstCustomerOrders.DeliveryCity;
                transaction.CardHolderStateCounty = lstCustomerOrders.DeliveryCounty;
                transaction.CardHolderPostcode = lstCustomerOrders.DeliveryPostalCode;
                transaction.CustomerPhone = lstCustomerOrders.DeliveryPhone;

                string lstrOrderURL = host + "ShoppingCart/OrderConfirmation.aspx?storeid=" + objStoreBE.StoreId + "&orderid=" + lstCustomerOrders.CustomerOrderId + "&TxRefGUID=" + lstCustomerOrders.TxnRefGUID;
                //string lstrOrderURL = host + "ShoppingCart/OrderConfirmation.aspx?storeid=" + objStoreBE.StoreId + "&orderid=" + lstCustomerOrders.CustomerOrderId;
                transaction.UserData1 = lstrOrderURL.Replace("/", ",").Replace("?", "@").Replace("&", "_").Replace("=", "(");
                transaction.CustomerEmail = lst.EmailId.Trim();
                Session["transaction"] = transaction;
                TransFrame.Visible = true;

                this.Page.Title = "Payment";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-09-15
        /// Scope   : CalculateTotalAmount
        /// </summary>            
        /// <returns>double</returns>
        protected double CalculateTotalAmount(double dShippingCharges, double dTax, CustomerOrderBE lstCustomerOrderBE)
        {
            double dTotalAmount = 0;
            try
            {
                List<object> lstShoppingBE = new List<object>();
                List<ShoppingCartBE> lstShoppingCartNewBE = new List<ShoppingCartBE>();
                lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, "", true);
                lstShoppingCartNewBE = lstShoppingBE.Cast<ShoppingCartBE>().ToList();

                if (lstShoppingCartNewBE.Count > 0)
                {
                    for (int i = 0; i < lstShoppingCartNewBE.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(lstCustomerOrderBE.CouponCode) && (lstCustomerOrderBE.BehaviourType.ToLower() == "product" || lstCustomerOrderBE.BehaviourType.ToLower() == "basket"))
                            dTotalAmount += Convert.ToDouble(Math.Round((GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstShoppingCartNewBE[i].Price), lstCustomerOrderBE.DiscountPercentage) * lstShoppingCartNewBE[i].Quantity), 2, MidpointRounding.AwayFromZero));
                        else
                            dTotalAmount += Convert.ToDouble(Math.Round(lstShoppingCartNewBE[i].Price * lstShoppingCartNewBE[i].Quantity, 2, MidpointRounding.AwayFromZero));
                    }
                    dTotalAmount += dShippingCharges + dTax;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            #region "Added by Sripal for Gift certificate"
            if (dTotalAmount > 0)
            {
                if (Session["Amount"] != null)
                {
                    try
                    {
                        double dGiftCertificateAmount = Convert.ToDouble(Session["Amount"]);
                        dTotalAmount = dTotalAmount - dGiftCertificateAmount;
                        if (dTotalAmount < 0) { dTotalAmount = 0; }
                    }
                    catch (Exception) { }
                }
            }
            #endregion










            return Math.Round(dTotalAmount, 2, MidpointRounding.AwayFromZero);
            //return dTotalAmount;
        }




    }
}