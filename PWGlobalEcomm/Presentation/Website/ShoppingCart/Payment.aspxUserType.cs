﻿using AHPP.ASP.Net.Lib;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace Presentation
{
    public partial class ShoppingCart_Payment : BasePage
    {
        #region variables
        protected global::System.Web.UI.WebControls.Repeater rptBasketListing;
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtDContact_Name, txtDPhone, txtDAddress1,
            txtDAddress2, txtDTown, txtDState__County, txtDPostal_Code, txtAddressTitle, txtContact_Name, txtPhone, txtAddress1,
            txtAddress2, txtTown, txtState__County, txtPostal_Code, txtCompany, txtCouponCode, txtDCompany_Name;
        protected global::System.Web.UI.WebControls.TextBox txtGiftCertificateCode;
        //protected global::System.Web.UI.WebControls.Button aProceed;
        protected global::System.Web.UI.WebControls.DropDownList ddlDCountry, ddlDAddressTitle, ddlPaymentTypes, ddlCountry;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidDeliveryAddressId, hidStandard, hidExpress, hidStandardTax, hidExpressTax, hidCouponCode;
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox addressCheckbox, chkIsDefault;
        protected global::System.Web.UI.HtmlControls.HtmlInputRadioButton rbStandard, rbExpress;
        protected global::System.Web.UI.WebControls.Literal ltrDContact_Name, ltrDPhone, ltrDAddress1, ltrDAddress2, ltrDTown, ltrdState__County, ltrDPostalCode,
            ltrDCountry, ltrContact_Name, ltrPhone, ltrAddress1, ltrAddress2, ltrTown, ltrState__County, ltrPostalCode, ltrCountry, ltrCompany, ltrColumnName, ltrDCompany_Name;
        protected global::System.Web.UI.HtmlControls.HtmlTextArea txtInstruction;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnstandard, spnexpress, spnStandardText, spnExpressText, divStandardCont, divExpressCont,
            divMOVMessage, divDutyMessage, divStatictext, header2, header2_space, dvApplyCode, spnRegisterAddress, spnDelivery_Address, spnDelivery_AddressTitle
            , spnPageTitle, spnShipping_Method, lblSetDefault, spnPaymentMethod, lblGC, lblInstruction, lblUploadFile, lblCouponCode, lblSucCoupon
            , spnReviewNConfirm, spnBsktSubTotal, lblChkAddress, dvApplyGiftCertificate;
        protected global::System.Web.UI.WebControls.Table tblUDFS;
        protected global::System.Web.UI.WebControls.FileUpload fuDoc;

        protected global::System.Web.UI.HtmlControls.HtmlInputButton btnRemoveCoupon, btnApplyCoupon;

        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        Int32 intUserId = 0;
        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        public decimal dTotalPrice = 0;
        public bool bDeliveryAddress = false;
        string strUserSessionId = string.Empty;
        UserBE lstUser;
        CustomerOrderBE lstCustomerOrders;
        Int32 intCustomerOrderId = 0;
        string TxRefGUID = string.Empty;
        decimal dTotalWeight = 0;
        public bool bBackOrderAllowed = false;
        public bool bBASysStore = false;
        public string strFreightMOVMessage = string.Empty;// "You can not proceed because your order value is not fullfilling the criteria.";
        public string strDutyMessage = string.Empty;// "International duty may apply in your country.";
        public string strCurrencySymbol = string.Empty;
        public string strPointsText = string.Empty;
        public bool IsPointsEnbled = false;

        public string strPTax, strPShip, strPTotal = string.Empty;
        decimal dPTotal = 0;

        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
        string strSC_InSufficient_Point = string.Empty;
        public string strSC_Payment_Types, strSC_UDF_Minimum_Maximum_Range, strSC_UDF_Range2, strSC_UDF_Range3, strSC_Numeric, strSC_NumberMinDecimal, strNew_Delivery_Address
             , strSC_Error_Processing_Payment, strSC_Updating_Order_Details, strSC_Error_Update_Delivery_Address, strSC_UDF_Max_Character
             , strUppercase, strRequired, strFile_Extension, stValid, str_SessionGCAmount = string.Empty;
        public static string strSC_Invalid_Coupon_Code, strSC_Coupon_Code_Maximum_Order_Value, strSC_Coupon_Code_Minimum_Order_Value,
             strSC_Coupon_Code_Minimum_Maximum_Range, strSC_Invalid_Coupon_Delivery_Country, strSC_Invalid_Coupon_Shipping_Method,
             strSuccess, strRegister_RegisteredAddress_Text, strRegister_DeliveryAddress_Text, strValid, strBasket_Title_Text, strSC_Special_Instruction,
             strBasket_Quantity_Not_More_Than_Stock_Message, strSC_Reapply_Coupon, strSC_Require_Coupon, strSC_Place_Order, strSC_Enter_Card_Details,
             strTotal_Text = "", strTax = "", strGiftCertificateTitle = "", strShipping = "", strsc_basket_subtotal = "", strsc_insufficient_couponcode_balance = "";

        protected global::System.Web.UI.WebControls.HiddenField hdfGC;

        //string strShippingStd, strShippingExp = "";

        StoreBE objStoreBE = new StoreBE();
        Int16 iUserTypeID = 0;/*User Type*/
        #endregion


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 09-09-15
        /// Scope   : Page_Load of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                #region
                #region "added by Sripal Punchout"
                if (Session["S_ISPUNCHOUT"] != null)
                {
                    if (Convert.ToBoolean(Session["S_ISPUNCHOUT"]))
                    {
                        Response.RedirectToRoute("BasketPagePunchout");
                        return;
                    }
                }
                #endregion

                #region Code Added 28 July 2016 to set the session of discountPercentage to NULL
                if (string.IsNullOrEmpty(hidCouponCode.Value))
                {
                    Session["discountPercentage"] = null;
                }
                #endregion

                if (Session["User"] != null)
                {
                    lstUser = new UserBE();
                    lstUser = Session["User"] as UserBE;
                    intUserId = lstUser.UserId;
                    iUserTypeID = lstUser.UserTypeID;/*User Type*/
                }
                else
                {
                    if (Session["GuestUser"] == null)//If condition for Guest User
                    {
                        Response.RedirectToRoute("login-page");
                        return;
                    }
                }
                BindResourceData();

                intCurrencyId = GlobalFunctions.GetCurrencyId();
                intLanguageId = GlobalFunctions.GetLanguageId();
                if (!GlobalFunctions.IsPointsEnbled())
                {
                    strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                }
                #region "Commented by Sripal"
                if (GlobalFunctions.IsPointsEnbled())
                {
                    strCurrencySymbol = string.Empty;
                    //header2.Visible = false;
                    //header2_space.Visible = false;
                    //IsPointsEnbled = true;
                    strPointsText = GlobalFunctions.PointsText(intLanguageId);
                }
                #endregion
                objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    #region
                    bBackOrderAllowed = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "OD_AllowBackOrder").FeatureValues[0].IsEnabled;
                    bBASysStore = objStoreBE.IsBASYS;
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_EnableCouponCode").FeatureValues[0].IsEnabled)
                        dvApplyCode.Visible = true;
                    else
                        dvApplyCode.Visible = false;


                    //added by Sripal
                    if (Request.Url.ToString().Contains("local"))
                    {
                        hdfGC.Value = "1";
                        dvApplyGiftCertificate.Visible = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_GiftCertificate").FeatureValues[0].IsEnabled)
                    {
                        hdfGC.Value = "1";
                        dvApplyGiftCertificate.Visible = true;
                    }
                    else
                    {
                        hdfGC.Value = "0";
                        dvApplyGiftCertificate.Visible = false;
                    }
                    List<FeatureBE> objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                    if (objFeatures != null)
                    {
                        List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;

                        if (objLstFeatureValueBE != null)
                        {
                            FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                            if (objFeatureValueBE != null)
                            {
                                ddlPaymentTypes.Attributes.Add("onchange", "paymentChanged()");
                            }
                            else
                            {
                                RemovePaymentDropdown();
                            }
                        }
                        else
                        {
                            RemovePaymentDropdown();
                        }
                    }
                    else
                    {
                        RemovePaymentDropdown();
                    }
                    #endregion
                }
                divMOVMessage.Attributes.Add("style", "display:none");
                divMOVMessage.InnerHtml = strFreightMOVMessage;

                divDutyMessage.Attributes.Add("style", "display:none");
                divDutyMessage.InnerHtml = strDutyMessage;

                if (Session["PrevLanguageId"] == null)
                {
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                }

                if (!IsPostBack || !Session["PrevLanguageId"].ToString().To_Int32().Equals(GlobalFunctions.GetLanguageId().To_Int32()))
                {
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                    Session["Amount"] = null;
                    #region
                    Session["CustomerOrderId"] = null;
                    hidDeliveryAddressId.Value = "";

                    List<object> lstShoppingBE = new List<object>();
                    if (Session["GuestUser"] == null)
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId, true);
                    else
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, Session.SessionID, true);

                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        #region
                        BindDeliveryAddress();
                        BindPaymentTypes();
                        BindCountryDropDown();

                        if (Session["GuestUser"] == null)
                        {
                            lstCustomerOrders = new CustomerOrderBE();
                            CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
                            objCustomerOrderBE.OrderedBy = lstUser.UserId;
                            objCustomerOrderBE.LanguageId = intLanguageId;
                            // Commented below LOC by SHRIGANESH 27 July 2016 Earlier added by Sripal to show last failed order address 
                            // || lstCustomerOrders.CustomerOrderId == 0
                            //lstCustomerOrders = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                            #region
                            if (lstCustomerOrders == null)//Added by Sripal OR condition
                            {
                                Session["CustomerOrderId"] = null;
                                ddlDCountry.ClearSelection();
                                if (ddlDCountry.Items.FindByValue(Convert.ToString(lstUser.PredefinedColumn8)) != null)
                                {
                                    ddlDCountry.Items.FindByValue(Convert.ToString(lstUser.PredefinedColumn8)).Selected = true;
                                }
                            }
                            else
                            {
                                #region
                                Session["CustomerOrderId"] = Convert.ToString(lstCustomerOrders.CustomerOrderId);
                                txtInstruction.Value = lstCustomerOrders.SpecialInstructions;
                                ddlPaymentTypes.ClearSelection();

                                Int16 intPaymentTypeId = Convert.ToInt16(lstCustomerOrders.PaymentMethod);
                                List<PaymentTypesBE> lstPaymentTypesNew = new List<PaymentTypesBE>();
                                List<object> lstPaymentTypes = new List<object>();
                                lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypes();

                                if (lstPaymentTypes.Count > 0)
                                {
                                    #region
                                    lstPaymentTypesNew = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();
                                    try
                                    {
                                        string strPaymentMethod = lstPaymentTypesNew.FirstOrDefault(x => x.PaymentTypeID == intPaymentTypeId).PaymentMethod;
                                        //string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                                        ddlPaymentTypes.Items.FindByValue(strPaymentMethod).Selected = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        Exceptions.WriteExceptionLog(ex);
                                    }
                                    #endregion
                                }
                                try
                                {
                                    #region
                                    if (lstCustomerOrders.DeliveryAddressId == 0)
                                    {
                                        addressCheckbox.Checked = true;
                                        bDeliveryAddress = false;
                                    }
                                    else
                                    {
                                        bDeliveryAddress = true;
                                        addressCheckbox.Checked = false;
                                        chkIsDefault.Checked = lstUser.UserDeliveryAddress.FirstOrDefault(x => x.DeliveryAddressId == lstCustomerOrders.DeliveryAddressId).IsDefault;
                                        ddlDAddressTitle.ClearSelection();
                                        ddlDAddressTitle.Items.FindByValue(Convert.ToString(lstCustomerOrders.DeliveryAddressId)).Selected = true;
                                        txtAddressTitle.Value = ddlDAddressTitle.SelectedItem.Text;
                                    }
                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    Exceptions.WriteExceptionLog(ex);
                                }
                                //Bind Promocode details
                                #region Bind Promocode details
                                if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode))
                                {
                                    hidCouponCode.Value = txtCouponCode.Value = lstCustomerOrders.CouponCode;
                                }
                                #endregion
                                #endregion
                            }
                            #endregion
                        }
                        rptBasketListing.DataSource = lstShoppingBE;
                        rptBasketListing.DataBind();
                        Session["ProductWeight"] = Convert.ToString(dTotalWeight);
                        //Bind User Address
                        BindUserAddress();

                        //Calculate freight and display value
                        SetFreightValues();

                        BindPriceDetails();
                        #endregion
                    }
                    else
                    {
                        Response.RedirectToRoute("Index");
                    }
                    try
                    {
                        Control FooterTemplate = rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0];
                        HtmlGenericControl divGiftCertificate = FooterTemplate.FindControl("divGiftCertificate") as HtmlGenericControl;
                        divGiftCertificate.Visible = false;
                        txtGiftCertificateCode.Text = "";
                        Session["Amount"] = null;
                        str_SessionGCAmount = "0";
                    }
                    catch (Exception) { }
                    #endregion
                }


                BindUDFFields(!IsPostBack);
                this.Page.Title = "Checkout";
                BindStaticText();
                #endregion
                if (GlobalFunctions.IsPointsEnbled())
                {
                    dvApplyGiftCertificate.Visible = false;
                    dvApplyCode.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindPriceDetails()
        {
            try
            {
                HtmlGenericControl spnShippingPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnShippingPrice");
                HtmlGenericControl spnTaxPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTaxPrice");
                #region corrected by Vikram for Total Price start
                HtmlGenericControl spnTotalPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTotalPrice");


                #endregion end
                HtmlGenericControl dvTotal = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("dvTotal");
                string str;
                str = dvTotal.InnerText;
                if (!GlobalFunctions.IsPointsEnbled())
                {
                    str = str.Replace(GlobalFunctions.GetCurrencySymbol() + "<span>", "");
                    str = str.Replace("</span>", "");
                }
                else
                {
                    str = str.Replace(GlobalFunctions.GetCurrencySymbol() + "<span>", "");
                    str = str.Replace("</span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                }
                //str = strPTotal;
                //decimal FinalTotal = Convert.ToDecimal(spnTotalPrice.InnerText) + Convert.ToDecimal(spnShippingPrice.InnerText) + Convert.ToDecimal(spnTaxPrice.InnerText);
                decimal FinalTotal = Convert.ToDecimal(str) + Convert.ToDecimal(spnShippingPrice.InnerText) + Convert.ToDecimal(spnTaxPrice.InnerText);
                spnTotalPrice.InnerText = FinalTotal.ToString("##,###,##0.#0");


                #region Comments
                //str = dvTotal.InnerText.Replace(strCurrencySymbol, "");
                //str = str.Replace("<span>", "");
                //str = str.Replace("</span>", "");
                //if (!GlobalFunctions.IsPointsEnbled())
                //{ 
                #endregion
                if (rbStandard.Checked)
                {
                    strPTax = hidStandardTax.Value;
                    strPShip = spnstandard.InnerHtml;

                    #region "Comments"
                    //spnTaxPrice.InnerHtml = hidStandardTax.Value;
                    //spnShippingPrice.InnerHtml = spnstandard.InnerHtml;
                    //spnTaxPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(hidStandardTax.Value, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                    //spnstandard.InnerHtml = spnstandard.InnerHtml.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "").Trim();
                    //spnstandard.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(spnstandard.InnerHtml, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                    //spnTaxPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(hidStandardTax.Value, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

                    #endregion
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        spnstandard.InnerHtml = spnstandard.InnerHtml.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "").Trim();
                    }
                    spnTaxPrice.InnerHtml = hidStandardTax.Value;

                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        spnShippingPrice.InnerHtml = spnstandard.InnerHtml + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                        spnstandard.InnerHtml = spnstandard.InnerHtml + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                    }
                    else
                    {
                        spnShippingPrice.InnerHtml = spnstandard.InnerHtml;
                    }
                }
                else
                {
                    strPTax = hidExpressTax.Value;
                    strPShip = spnShippingPrice.InnerHtml;
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        spnexpress.InnerHtml = spnexpress.InnerHtml.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "").Trim();
                    }

                    #region "Comments"
                    //spnTaxPrice.InnerHtml = hidExpressTax.Value;
                    //spnShippingPrice.InnerHtml = spnexpress.InnerHtml;
                    //spnexpress.InnerHtml = spnexpress.InnerHtml.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "").Trim();
                    //spnexpress.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(spnexpress.InnerHtml, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                    //spnexpress.InnerHtml = spnexpress.InnerHtml.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "").Trim();
                    //spnTaxPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(hidExpressTax.Value, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

                    #endregion
                    spnTaxPrice.InnerHtml = hidExpressTax.Value;
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        spnShippingPrice.InnerHtml = spnexpress.InnerHtml + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                        spnexpress.InnerHtml = spnexpress.InnerHtml + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                    }
                    else
                    {
                        spnShippingPrice.InnerHtml = spnexpress.InnerHtml;
                    }
                }

                //spnstandard.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(strShippingStd, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                //spnexpress.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(strShippingExp, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

                double dShpCartPrice = 0;
                if (GlobalFunctions.IsPointsEnbled())
                {
                    string strTot = GlobalFunctions.DisplayPriceOrPoints(str, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                    //string strTPTax = GlobalFunctions.DisplayPriceOrPoints(strPTax, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                    string strTPTax = strPTax;
                    dShpCartPrice = Convert.ToDouble(strTot) + Convert.ToDouble(strTPTax) + Convert.ToDouble(strPShip.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), ""));
                }
                else
                {
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(strPTax) + Convert.ToDouble(strPShip.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), ""));
                    }
                    else
                    {
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(strPTax) + Convert.ToDouble(strPShip);
                    }
                    strPTotal = Convert.ToString(dShpCartPrice.ToString("f"));
                }
                //double dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(spnTaxPrice.InnerText) + Convert.ToDouble(spnShippingPrice.InnerText);

                if (GlobalFunctions.IsPointsEnbled())
                {
                    spnTotalPrice.InnerText = Convert.ToString(dShpCartPrice);// +GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                    //spnTotalPrice.InnerText = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dShpCartPrice.ToString("f")), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                }
                else
                {
                    spnTotalPrice.InnerText = Convert.ToString(dShpCartPrice.ToString("f"));
                }
                if (!GlobalFunctions.IsPointsEnbled())
                {
                    #region MyRegion
                    double dDiff = 0;
                    double dGCAmt = Session["Amount"] != null ? Convert.ToDouble(Session["Amount"]) : 0;
                    dDiff = dShpCartPrice - dGCAmt;
                    if (dDiff <= 0)
                    {
                        ListItem lItem = ddlPaymentTypes.Items.FindByValue("1|I");
                        if (lItem != null)
                        {
                            ddlPaymentTypes.Items.Clear();
                            ddlPaymentTypes.Items.Add(lItem);
                            ddlPaymentTypes.Attributes.Add("class", "form-control input-sm hidden");
                        }
                    }
                    else
                    {
                        ddlPaymentTypes.Attributes.Add("class", "form-control input-sm");
                    }

                    if (dGCAmt < dShpCartPrice)
                    {
                        dDiff = dShpCartPrice - dGCAmt;
                        //if (GlobalFunctions.IsPointsEnbled())
                        //{
                        //    spnTotalPrice.InnerText = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dDiff.ToString("f")), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                        //}
                        //else
                        //{
                        spnTotalPrice.InnerText = Convert.ToString(dDiff.ToString("f"));
                        //}

                        if (Session["Amount"] != null)
                        { Session["Amount"] = dGCAmt.ToString("f"); }
                    }
                    else if (dGCAmt > dShpCartPrice)
                    {
                        dDiff = dGCAmt - dShpCartPrice;
                        spnTotalPrice.InnerText = "0.00";

                        if (Session["Amount"] != null) { Session["Amount"] = dShpCartPrice.ToString("f"); }
                    }
                    else if (dGCAmt == dShpCartPrice)
                    {
                        dDiff = 0;
                        spnTotalPrice.InnerText = "0.00";
                        if (Session["Amount"] != null) { Session["Amount"] = dGCAmt.ToString("f"); }
                    }
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 02-11-15
        /// Scope   : BindStaticText
        /// </summary>            
        /// <returns></returns>
        protected void BindStaticText()
        {
            try
            {
                List<StaticPageManagementBE> lst = new List<StaticPageManagementBE>();
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBE.PageName = "/info/checkoutcontent";
                lst = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "PageUrl");
                lst = lst.FindAll(x => x.LanguageId == Convert.ToInt16(GlobalFunctions.GetLanguageId()));
                if (lst != null && lst.Count > 0)
                {
                    string strContent = Convert.ToString(lst[0].WebsiteContent).Replace("~/", GlobalFunctions.GetVirtualPath()).Replace("$host$", GlobalFunctions.GetVirtualPath());

                    List<StaticTextKeyBE> lstKey = new List<StaticTextKeyBE>();
                    StaticTextKeyBE objBEs = new StaticTextKeyBE();
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    lstKey = StaticTextKeyBL.GetAllKeys(Constants.USP_GetStaticTextDetails, objBEs, "A");
                    if (lstKey != null)
                    {
                        if (lstKey.Count > 0)
                        {
                            for (int i = 0; i < lstKey.Count; i++)
                            {
                                strContent = strContent.Replace("[" + Convert.ToString(lstKey[i].StaticTextKey) + "]", Convert.ToString(lstKey[i].StaticTextValue));
                            }
                        }
                    }
                    if (divStatictext != null)
                        divStatictext.InnerHtml = strContent;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind BindCountryDropDown
        /// </summary>            
        /// <returns></returns>
        protected void BindCountryDropDown()
        {
            try
            {
                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                ddlCountry.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlDCountry.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlCountry.AppendDataBoundItems = true;
                ddlDCountry.AppendDataBoundItems = true;
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    lstCountry = lstCountry.FindAll(x => x.RegionCode != "UA");
                    ddlCountry.DataTextField = "CountryName";
                    ddlCountry.DataValueField = "CountryId";
                    ddlCountry.DataSource = lstCountry;
                    ddlCountry.DataBind();

                    ddlDCountry.DataTextField = "CountryName";
                    ddlDCountry.DataValueField = "CountryId";
                    ddlDCountry.DataSource = lstCountry;
                    ddlDCountry.DataBind();
                }
                ddlCountry.SelectedIndex = 0;
                ddlDCountry.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 10-09-16
        /// Scope   : rptBasketListing_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptBasketListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl dvUnitPrice = (HtmlGenericControl)e.Item.FindControl("dvUnitPrice");
                    HtmlGenericControl dvBasketRowContainer = (HtmlGenericControl)e.Item.FindControl("dvBasketRowContainer");
                    HtmlGenericControl dvTotalPrice = (HtmlGenericControl)e.Item.FindControl("dvTotalPrice");
                    //HtmlAnchor aRemoveProducts = (HtmlAnchor)e.Item.FindControl("aRemoveProducts");
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)e.Item.FindControl("hdnShoppingCartProductId");
                    HtmlInputHidden hdnInventory = (HtmlInputHidden)e.Item.FindControl("hdnInventory");

                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";

                    //string strProductPrice = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Price")));

                    //GlobalFunctions.ConvertToDecimalPrecision Removed due to multi Language currency (example: 0,6300.00)
                    string strProductPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Price")).ToString("#######0.#0");

                    //dvUnitPrice.InnerHtml = "@ " + strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(strProductPrice), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId) + "</span>";
                    //dvTotalPrice.InnerHtml = " = " + strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId) + "</span>";

                    //GlobalFunctions.ConvertToDecimalPrecision Removed due to multi Language currency (example: 0,6300.00)

                    dvUnitPrice.InnerHtml = "@ " + strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(strProductPrice), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId) + "</span>";
                    dvTotalPrice.InnerHtml = " = " + strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId) + "</span>";

                    dPTotal += Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"));

                    dTotalPrice += Convert.ToDecimal(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", intLanguageId));
                    //aRemoveProducts.Attributes.Add("rel", Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId")));
                    dvBasketRowContainer.ID = "dvBasketRowContainer" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId"));
                    hdnShoppingCartProductId.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId"));
                    hdnInventory.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Inventory"));
                    dTotalWeight += Math.Ceiling(Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "DimensionalWeight")) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity")) * Convert.ToDecimal(0.001));

                    if (bBackOrderAllowed == false && bBASysStore)
                    // for testing purpose
                    //if (bBackOrderAllowed == false)
                    {
                        Int32 intProductid_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BASYSProductId")),
                            intBaseColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BaseColorId")),
                            intTrimColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "TrimColorId"));

                        PWGlobalEcomm.Stock.Stock objStock = new PWGlobalEcomm.Stock.Stock();
                        NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                            ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                        objStock.Credentials = objNetworkCredentials;
                        PWGlobalEcomm.Stock.StockDetails objStockDetails = new PWGlobalEcomm.Stock.StockDetails();
                        objStockDetails = objStock.LevelEnquiry(intProductid_OASIS, intBaseColorId_OASIS, intTrimColorId_OASIS);

                        if (objStockDetails != null)
                        {
                            HtmlInputHidden hdnStockStatus = (HtmlInputHidden)e.Item.FindControl("hdnStockStatus");
                            hdnStockStatus.Value = Convert.ToString(objStockDetails.StockLevel);
                        }
                    }
                }
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        strPTotal = dPTotal.ToString();
                    }
                    else
                    {
                        //strPTotal = dPTotal.ToString("##,###,##0.#0");
                        strPTotal = dPTotal.ToString("#######0.#0");
                    }
                    HtmlGenericControl dvTotal = (HtmlGenericControl)e.Item.FindControl("dvTotal");
                    HtmlGenericControl spnTotalPrice = (HtmlGenericControl)e.Item.FindControl("spnTotalPrice");
                    spnTotalPrice.InnerText = Convert.ToString(dTotalPrice);
                    if (IsPointsEnbled)
                    {
                        dvTotal.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(dTotalPrice) + "</span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());

                        HtmlGenericControl divShipping = (HtmlGenericControl)e.Item.FindControl("divShipping");
                        HtmlGenericControl divTax = (HtmlGenericControl)e.Item.FindControl("divTax");

                        divShipping.Attributes.Add("style", "display:none");
                        divTax.Attributes.Add("style", "display:none");
                        if (dTotalPrice > lstUser.Points)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_InSufficient_Point, AlertType.Warning);
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "You do not have sufficient points for this order.", AlertType.Warning);
                        }
                    }
                    else
                    {
                        if (GlobalFunctions.IsPointsEnbled())
                        {
                            dvTotal.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(dTotalPrice) + "</span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                        }
                        else
                        {
                            //dvTotal.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(GlobalFunctions.ConvertToDecimalPrecision(dTotalPrice)) + "</span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());

                            //GlobalFunctions.ConvertToDecimalPrecision Removed due to multi Language currency (example: 0,6300.00)                            
                            //dvTotal.InnerHtml = strCurrencySymbol + "<span>" + dTotalPrice.ToString("##,###,##0.#0") + "</span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                            dvTotal.InnerHtml = strCurrencySymbol + "<span>" + dTotalPrice.ToString("#######0.#0") + "</span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                        }
                    }

                    Button aProceed = (Button)e.Item.FindControl("aProceed");
                    aProceed.Text = strSC_Place_Order;

                    HtmlGenericControl spnBsktSubTotal = (HtmlGenericControl)e.Item.FindControl("spnBsktSubTotal");
                    spnBsktSubTotal.InnerText = strsc_basket_subtotal;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }



        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 15-09-15
        /// Scope   : Bind User Addresses
        /// </summary>            
        /// <returns></returns>
        protected void BindUserAddress()
        {
            try
            {
                UserBE UserdetailLanguageWise = new UserBE();
                if (Session["User"] != null)
                {
                    UserdetailLanguageWise = Session["User"] as UserBE;
                    UserdetailLanguageWise.EmailId = UserdetailLanguageWise.EmailId;
                }
                else if (Session["GuestUser"] != null)
                {
                    string GuestEmailId = Convert.ToString(Session["GuestUser"]);
                    UserdetailLanguageWise.EmailId = GuestEmailId;
                }
                UserdetailLanguageWise.LanguageId = GlobalFunctions.GetLanguageId();
                UserdetailLanguageWise.CurrencyId = GlobalFunctions.GetCurrencyId();
                UserdetailLanguageWise = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, UserdetailLanguageWise);
                if (Session["GuestUser"] == null)
                {
                    #region
                    if (UserdetailLanguageWise.UserPreDefinedColumns.Count > 0)
                    {
                        UserdetailLanguageWise.UserPreDefinedColumns = UserdetailLanguageWise.UserPreDefinedColumns.FindAll(x => x.IsVisible == true);
                        for (int i = 0; i < lstUser.UserPreDefinedColumns.Count; i++)
                        {
                            if (UserdetailLanguageWise.UserPreDefinedColumns[i].ParentFieldGroup == 0)
                            {
                                ltrColumnName.Text = "<p id=\"pBillingTitle\" class=\"pageText\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</p>";
                            }
                            else
                            {
                                bool bMandatory = UserdetailLanguageWise.UserPreDefinedColumns[i].IsMandatory;
                                string strColumnName = UserdetailLanguageWise.UserPreDefinedColumns[i].SystemColumnName.ToLower();
                                // Removed OR Condition by SHRIGANESH 27 July 2016 Earlier added by Sripal to show last failed order address 
                                // || lstCustomerOrders.CustomerOrderId <= 0
                                if (lstCustomerOrders == null || lstCustomerOrders.CustomerOrderId <= 0) //Added by Sripal Or condition
                                {
                                    #region Bind From UserTable
                                    switch (strColumnName)
                                    {
                                        case "predefinedcolumn1":
                                            {
                                                ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                                txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtContact_Name.Value = (!string.IsNullOrEmpty(UserdetailLanguageWise.PredefinedColumn1)) ? UserdetailLanguageWise.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                                break;
                                            }
                                        case "predefinedcolumn2":
                                            {
                                                #region Section Added by SHRIGANESH to diaplay CompanyName in delivery address 27 July 2016
                                                ltrDCompany_Name.Text = "<label id=\"lblDCompany_Name\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                                txtDCompany_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDCompany_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                #endregion

                                                ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtCompany.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtCompany.Value = UserdetailLanguageWise.PredefinedColumn2;
                                                break;


                                            }
                                        case "predefinedcolumn3":
                                            {
                                                ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtAddress1.Value = UserdetailLanguageWise.PredefinedColumn3;
                                                break;
                                            }
                                        case "predefinedcolumn4":
                                            {
                                                ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtAddress2.Value = UserdetailLanguageWise.PredefinedColumn4;
                                                break;
                                            }
                                        case "predefinedcolumn5":
                                            {
                                                ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtTown.Value = UserdetailLanguageWise.PredefinedColumn5;
                                                break;
                                            }
                                        case "predefinedcolumn6":
                                            {
                                                ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtState__County.Value = UserdetailLanguageWise.PredefinedColumn6;
                                                break;
                                            }
                                        case "predefinedcolumn7":
                                            {
                                                ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtPostal_Code.Value = UserdetailLanguageWise.PredefinedColumn7;
                                                break;
                                            }
                                        case "predefinedcolumn8":
                                            {
                                                ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlDCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlCountry.ClearSelection();
                                                ddlDCountry.ClearSelection();
                                                try
                                                {
                                                    ddlCountry.Items.FindByValue(UserdetailLanguageWise.PredefinedColumn8).Selected = true;
                                                    ddlDCountry.Items.FindByValue(UserdetailLanguageWise.PredefinedColumn8).Selected = true;
                                                }
                                                catch (Exception) { }
                                                break;
                                            }
                                        case "predefinedcolumn9":
                                            {
                                                ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtPhone.Value = lstUser.PredefinedColumn9;
                                                break;
                                            }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Bind From CustomerOrder Table
                                    switch (strColumnName)
                                    {
                                        case "predefinedcolumn1":
                                            {

                                                ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDContact_Name.Value = lstCustomerOrders.DeliveryContactPerson;
                                                txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtContact_Name.Value = lstCustomerOrders.InvoiceContactPerson;
                                                break;
                                            }
                                        case "predefinedcolumn2":
                                            {
                                                #region Section Added by SHRIGANESH to diaplay CompanyName in delivery address 27 July 2016
                                                ltrDCompany_Name.Text = "<label id=\"lblDCompany_Name\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                                txtDCompany_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDCompany_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                #endregion

                                                ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtCompany.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtCompany.Value = lstCustomerOrders.InvoiceCompany;
                                                break;
                                            }
                                        case "predefinedcolumn3":
                                            {
                                                ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress1.Value = lstCustomerOrders.DeliveryAddress1;
                                                txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress1.Value = lstCustomerOrders.InvoiceAddress1;
                                                txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn4":
                                            {
                                                ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress2.Value = lstCustomerOrders.DeliveryAddress2;
                                                txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress2.Value = lstCustomerOrders.InvoiceAddress2;
                                                txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn5":
                                            {
                                                ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDTown.Value = lstCustomerOrders.DeliveryCity;
                                                txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtTown.Value = lstCustomerOrders.InvoiceCity;
                                                txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn6":
                                            {
                                                ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDState__County.Value = lstCustomerOrders.DeliveryCounty;
                                                txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtState__County.Value = lstCustomerOrders.InvoiceCounty;
                                                txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn7":
                                            {
                                                ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPostal_Code.Value = lstCustomerOrders.DeliveryPostalCode;
                                                txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPostal_Code.Value = lstCustomerOrders.InvoicePostalCode;
                                                txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn8":
                                            {
                                                ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlDCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlDCountry.ClearSelection();

                                                ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlCountry.ClearSelection();
                                                try
                                                {
                                                    //    ddlDCountry.Items.FindByValue(lstCustomerOrders.DeliveryCountry).Selected = true;
                                                    //    ddlCountry.Items.FindByValue(lstCustomerOrders.InvoiceCountry).Selected = true;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Exceptions.WriteExceptionLog(ex);
                                                }
                                                break;
                                            }
                                        case "predefinedcolumn9":
                                            {
                                                ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPhone.Value = lstCustomerOrders.DeliveryPhone;
                                                txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPhone.Value = lstCustomerOrders.InvoicePhone;
                                                txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                    }
                                    #endregion
                                }

                                #region " Added by SHRIGANESH so taht Address Fields are populated from the DataBase 27 July 2016"
                                #region Bind From UserTable
                                switch (strColumnName)
                                {
                                    case "predefinedcolumn1":
                                        {
                                            ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                            txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtContact_Name.Value = (!string.IsNullOrEmpty(UserdetailLanguageWise.PredefinedColumn1)) ? UserdetailLanguageWise.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                            break;
                                        }
                                    case "predefinedcolumn2":
                                        {
                                            #region Section Added by SHRIGANESH to diaplay CompanyName in delivery address 27 July 2016
                                            ltrDCompany_Name.Text = "<label id=\"lblDCompany_Name\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                            txtDCompany_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDCompany_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            #endregion

                                            ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtCompany.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtCompany.Value = UserdetailLanguageWise.PredefinedColumn2;
                                            break;


                                        }
                                    case "predefinedcolumn3":
                                        {
                                            ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtAddress1.Value = UserdetailLanguageWise.PredefinedColumn3;
                                            break;
                                        }
                                    case "predefinedcolumn4":
                                        {
                                            ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtAddress2.Value = UserdetailLanguageWise.PredefinedColumn4;
                                            break;
                                        }
                                    case "predefinedcolumn5":
                                        {
                                            ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtTown.Value = UserdetailLanguageWise.PredefinedColumn5;
                                            break;
                                        }
                                    case "predefinedcolumn6":
                                        {
                                            ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtState__County.Value = UserdetailLanguageWise.PredefinedColumn6;
                                            break;
                                        }
                                    case "predefinedcolumn7":
                                        {
                                            ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtPostal_Code.Value = UserdetailLanguageWise.PredefinedColumn7;
                                            break;
                                        }
                                    case "predefinedcolumn8":
                                        {
                                            ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            ddlDCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            ddlCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            ddlCountry.ClearSelection();
                                            ddlDCountry.ClearSelection();
                                            try
                                            {
                                                ddlCountry.Items.FindByValue(UserdetailLanguageWise.PredefinedColumn8).Selected = true;
                                                ddlDCountry.Items.FindByValue(UserdetailLanguageWise.PredefinedColumn8).Selected = true;
                                            }
                                            catch (Exception) { }
                                            break;
                                        }
                                    case "predefinedcolumn9":
                                        {
                                            ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtPhone.Value = lstUser.PredefinedColumn9;
                                            break;
                                        }
                                }
                                #endregion
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    UserRegistrationBE objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                    UserRegistrationBE objTempUserRegistrationBE = new UserRegistrationBE();
                    objTempUserRegistrationBE.UserRegistrationLanguagesLst = objUserRegistrationBE.UserRegistrationLanguagesLst.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId <= 10);

                    #region "Comments"
                    //foreach (UserRegistrationBE.RegistrationLanguagesBE obj in objTempUserRegistrationBE.UserRegistrationLanguagesLst)
                    //{
                    //    UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                    //    objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                    //    objRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE);
                    //} 
                    #endregion

                    for (int i = 0; i < objTempUserRegistrationBE.UserRegistrationLanguagesLst.Count; i++)
                    {
                        //short ias= objTempUserRegistrationBE.RegistrationFieldsConfigurationBE[i+1].RegistrationFieldsConfigurationId;

                        UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].RegistrationFieldsConfigurationId);

                        bool bMandatory = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst[i + 1].IsMandatory;
                        //bool bMandatory = false;//objTempUserRegistrationBE.RegistrationFieldsConfigurationBE[i]..IsMandatory;
                        string strColumnName = objRegistrationFieldsConfigurationBE.SystemColumnName.ToLower();

                        #region "Invoice Address"
                        #region Bind Address
                        switch (strColumnName)
                        {
                            case "predefinedcolumn1":
                                {
                                    ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                    txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtContact_Name.Value = (!string.IsNullOrEmpty(lstUser.PredefinedColumn1)) ? lstUser.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                    break;
                                }
                            case "predefinedcolumn2":
                                {
                                    #region Section Added by SHRIGANESH to diaplay CompanyName in delivery address 27 July 2016
                                    ltrDCompany_Name.Text = "<label id=\"lblDCompany_Name\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                    txtDCompany_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDCompany_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    #endregion

                                    ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtCompany.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtCompany.Value = lstUser.PredefinedColumn2;
                                    break;
                                }
                            case "predefinedcolumn3":
                                {
                                    ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtAddress1.Value = lstUser.PredefinedColumn3;
                                    break;
                                }
                            case "predefinedcolumn4":
                                {
                                    ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtAddress2.Value = lstUser.PredefinedColumn4;
                                    break;
                                }
                            case "predefinedcolumn5":
                                {
                                    ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtTown.Value = lstUser.PredefinedColumn5;
                                    break;
                                }
                            case "predefinedcolumn6":
                                {
                                    ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtState__County.Value = lstUser.PredefinedColumn6;
                                    break;
                                }
                            case "predefinedcolumn7":
                                {
                                    ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtPostal_Code.Value = lstUser.PredefinedColumn7;
                                    break;
                                }
                            case "predefinedcolumn8":
                                {
                                    ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlDCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //ddlCountry.ClearSelection();
                                    //ddlDCountry.ClearSelection();
                                    //try
                                    //{
                                    //    ddlCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //    ddlDCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //}
                                    //catch (Exception) { }
                                    ddlCountry.SelectedValue = "12";
                                    ddlDCountry.SelectedValue = "12";
                                    break;
                                }
                            case "predefinedcolumn9":
                                {
                                    ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtPhone.Value = lstUser.PredefinedColumn9;
                                    break;
                                }
                        }
                        #endregion
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 15-09-15
        /// Scope   : Bind User Addresses
        /// </summary>            
        /// <returns></returns>
        protected void BindUserAddress_bkp()
        {
            try
            {
                if (Session["GuestUser"] == null)
                {
                    #region
                    if (lstUser.UserPreDefinedColumns.Count > 0)
                    {
                        lstUser.UserPreDefinedColumns = lstUser.UserPreDefinedColumns.FindAll(x => x.IsVisible == true);
                        for (int i = 0; i < lstUser.UserPreDefinedColumns.Count; i++)
                        {
                            if (lstUser.UserPreDefinedColumns[i].ParentFieldGroup == 0)
                            {
                                ltrColumnName.Text = "<p id=\"pBillingTitle\" class=\"pageText\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</p>";
                            }
                            else
                            {
                                bool bMandatory = lstUser.UserPreDefinedColumns[i].IsMandatory;
                                string strColumnName = lstUser.UserPreDefinedColumns[i].SystemColumnName.ToLower();
                                if (lstCustomerOrders == null || lstCustomerOrders.CustomerOrderId <= 0) //Added by Sripal Or condition
                                {
                                    #region Bind From UserTable
                                    switch (strColumnName)
                                    {
                                        case "predefinedcolumn1":
                                            {
                                                ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                                txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDContact_Name.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtContact_Name.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtContact_Name.Value = (!string.IsNullOrEmpty(lstUser.PredefinedColumn1)) ? lstUser.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                                break;
                                            }
                                        case "predefinedcolumn2":
                                            {
                                                ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtCompany.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtCompany.Value = lstUser.PredefinedColumn2;
                                                break;
                                            }
                                        case "predefinedcolumn3":
                                            {
                                                ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress1.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress1.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtAddress1.Value = lstUser.PredefinedColumn3;
                                                break;
                                            }
                                        case "predefinedcolumn4":
                                            {
                                                ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress2.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress2.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtAddress2.Value = lstUser.PredefinedColumn4;
                                                break;
                                            }
                                        case "predefinedcolumn5":
                                            {
                                                ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDTown.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtTown.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtTown.Value = lstUser.PredefinedColumn5;
                                                break;
                                            }
                                        case "predefinedcolumn6":
                                            {
                                                ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDState__County.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtState__County.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtState__County.Value = lstUser.PredefinedColumn6;
                                                break;
                                            }
                                        case "predefinedcolumn7":
                                            {
                                                ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPostal_Code.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPostal_Code.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtPostal_Code.Value = lstUser.PredefinedColumn7;
                                                break;
                                            }
                                        case "predefinedcolumn8":
                                            {
                                                ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlDCountry.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlCountry.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlCountry.ClearSelection();
                                                ddlDCountry.ClearSelection();
                                                try
                                                {
                                                    ddlCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                                    ddlDCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                                }
                                                catch (Exception) { }
                                                break;
                                            }
                                        case "predefinedcolumn9":
                                            {
                                                ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPhone.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPhone.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtPhone.Value = lstUser.PredefinedColumn9;
                                                break;
                                            }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Bind From CustomerOrder Table
                                    switch (strColumnName)
                                    {
                                        case "predefinedcolumn1":
                                            {

                                                ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDContact_Name.Value = lstCustomerOrders.DeliveryContactPerson;
                                                txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDContact_Name.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtContact_Name.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtContact_Name.Value = lstCustomerOrders.InvoiceContactPerson;
                                                break;
                                            }
                                        case "predefinedcolumn2":
                                            {
                                                ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtCompany.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtCompany.Value = lstCustomerOrders.InvoiceCompany;
                                                break;
                                            }
                                        case "predefinedcolumn3":
                                            {
                                                ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress1.Value = lstCustomerOrders.DeliveryAddress1;
                                                txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress1.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress1.Value = lstCustomerOrders.InvoiceAddress1;
                                                txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress1.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn4":
                                            {
                                                ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress2.Value = lstCustomerOrders.DeliveryAddress2;
                                                txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress2.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress2.Value = lstCustomerOrders.InvoiceAddress2;
                                                txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress2.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn5":
                                            {
                                                ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDTown.Value = lstCustomerOrders.DeliveryCity;
                                                txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDTown.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtTown.Value = lstCustomerOrders.InvoiceCity;
                                                txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtTown.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn6":
                                            {
                                                ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDState__County.Value = lstCustomerOrders.DeliveryCounty;
                                                txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDState__County.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtState__County.Value = lstCustomerOrders.InvoiceCounty;
                                                txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtState__County.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn7":
                                            {
                                                ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPostal_Code.Value = lstCustomerOrders.DeliveryPostalCode;
                                                txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPostal_Code.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPostal_Code.Value = lstCustomerOrders.InvoicePostalCode;
                                                txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPostal_Code.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn8":
                                            {
                                                ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlDCountry.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlDCountry.ClearSelection();

                                                ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlCountry.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlCountry.ClearSelection();
                                                try
                                                {
                                                    ddlDCountry.Items.FindByValue(lstCustomerOrders.DeliveryCountry).Selected = true;
                                                    ddlCountry.Items.FindByValue(lstCustomerOrders.InvoiceCountry).Selected = true;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Exceptions.WriteExceptionLog(ex);
                                                }
                                                break;
                                            }
                                        case "predefinedcolumn9":
                                            {
                                                ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPhone.Value = lstCustomerOrders.DeliveryPhone;
                                                txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPhone.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPhone.Value = lstCustomerOrders.InvoicePhone;
                                                txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPhone.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    UserRegistrationBE objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                    UserRegistrationBE objTempUserRegistrationBE = new UserRegistrationBE();
                    objTempUserRegistrationBE.UserRegistrationLanguagesLst = objUserRegistrationBE.UserRegistrationLanguagesLst.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId <= 10);

                    #region "Comments"
                    //foreach (UserRegistrationBE.RegistrationLanguagesBE obj in objTempUserRegistrationBE.UserRegistrationLanguagesLst)
                    //{
                    //    UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                    //    objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                    //    objRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE);
                    //} 
                    #endregion

                    for (int i = 0; i < objTempUserRegistrationBE.UserRegistrationLanguagesLst.Count; i++)
                    {
                        //short ias= objTempUserRegistrationBE.RegistrationFieldsConfigurationBE[i+1].RegistrationFieldsConfigurationId;

                        UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].RegistrationFieldsConfigurationId);

                        bool bMandatory = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst[i + 1].IsMandatory;
                        //bool bMandatory = false;//objTempUserRegistrationBE.RegistrationFieldsConfigurationBE[i]..IsMandatory;
                        string strColumnName = objRegistrationFieldsConfigurationBE.SystemColumnName.ToLower();

                        #region "Invoice Address"
                        #region Bind Address
                        switch (strColumnName)
                        {
                            case "predefinedcolumn1":
                                {
                                    ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                    txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtContact_Name.Value = (!string.IsNullOrEmpty(lstUser.PredefinedColumn1)) ? lstUser.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                    break;
                                }
                            case "predefinedcolumn2":
                                {
                                    ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtCompany.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtCompany.Value = lstUser.PredefinedColumn2;
                                    break;
                                }
                            case "predefinedcolumn3":
                                {
                                    ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtAddress1.Value = lstUser.PredefinedColumn3;
                                    break;
                                }
                            case "predefinedcolumn4":
                                {
                                    ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtAddress2.Value = lstUser.PredefinedColumn4;
                                    break;
                                }
                            case "predefinedcolumn5":
                                {
                                    ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtTown.Value = lstUser.PredefinedColumn5;
                                    break;
                                }
                            case "predefinedcolumn6":
                                {
                                    ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtState__County.Value = lstUser.PredefinedColumn6;
                                    break;
                                }
                            case "predefinedcolumn7":
                                {
                                    ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtPostal_Code.Value = lstUser.PredefinedColumn7;
                                    break;
                                }
                            case "predefinedcolumn8":
                                {
                                    ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlDCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //ddlCountry.ClearSelection();
                                    //ddlDCountry.ClearSelection();
                                    //try
                                    //{
                                    //    ddlCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //    ddlDCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //}
                                    //catch (Exception) { }
                                    ddlCountry.SelectedValue = "12";
                                    ddlDCountry.SelectedValue = "12";
                                    break;
                                }
                            case "predefinedcolumn9":
                                {
                                    ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtPhone.Value = lstUser.PredefinedColumn9;
                                    break;
                                }
                        }
                        #endregion
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind User delivery Address
        /// </summary>            
        /// <returns></returns>
        protected void BindPaymentTypes()
        {
            try
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                List<FeatureBE> objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                if (objFeatures != null)
                {
                    List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;

                    if (objLstFeatureValueBE != null)
                    {
                        FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                        if (objFeatureValueBE != null)
                        {
                            List<object> lstPaymentTypes = new List<object>();
                            List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                            
                            //lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypes();
                            lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypesByLanguageID(GlobalFunctions.GetLanguageId(), iUserTypeID);/*User Type*/

                            if (lstPaymentTypes.Count > 0)
                            {
                                ddlPaymentTypes.DataTextField = "PaymentTypeRef";
                                ddlPaymentTypes.DataValueField = "PaymentMethod";
                                ddlPaymentTypes.DataSource = lstPaymentTypes;
                                ddlPaymentTypes.DataBind();

                                ListItem litem = new ListItem();
                                litem.Text = strSC_Payment_Types;// "Please select";
                                litem.Value = "0";
                                ddlPaymentTypes.Items.Insert(0, litem);
                            }
                        }
                        else
                        {
                            RemovePaymentDropdown();
                        }
                    }
                    else
                    {
                        RemovePaymentDropdown();
                    }
                }
                else
                {
                    RemovePaymentDropdown();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        private void RemovePaymentDropdown()
        {
            try
            {
                ddlPaymentTypes.Visible = false;
                ddlPaymentTypes.Attributes.Remove("onchange");

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 7-10-15
        /// Scope   : Bind UDF Fields
        /// </summary>            
        /// <returns></returns>
        protected void BindUDFFields(bool bSetValues)
        {
            try
            {
                #region BindUDF Fields
                List<object> lstPaymentTypes = new List<object>();
                List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                //lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypes();
                lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypesByLanguageID(GlobalFunctions.GetLanguageId(), iUserTypeID);/*User Type*/

                if (lstPaymentTypes.Count > 0)
                {
                    lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();
                    List<object> lstObj = new List<object>();
                    lstObj = ShoppingCartBL.GetUDFS(0);

                    if (lstObj != null && lstObj.Count > 0)
                    {
                        List<UDFBE> lstUDF = new List<UDFBE>();
                        List<UDFBE> lstUDFFilterBE = new List<UDFBE>();
                        lstUDF = lstObj.Cast<UDFBE>().ToList();

                        tblUDFS.Controls.Clear();
                        tblUDFS.Visible = true;
                        TableRow trUDFS = null;
                        TableCell tcUDFS = null;
                        TextBox txtUDFS = null;
                        Label lblUDFS = null;
                        RegularExpressionValidator revNumeric = null;
                        RegularExpressionValidator revCase = null;
                        RequiredFieldValidator rfvUDF = null;
                        RangeValidator rvRange = null;
                        DropDownList ddlPickListItems = null;
                        CustomerOrderBE.UDFValuesBE objUDFValues;

                        for (int i = 0; i < lstPaymentTypesBE.Count; i++)
                        {
                            lstUDFFilterBE = new List<UDFBE>();
                            //lstUDFFilterBE = lstUDF.FindAll(x => x.PaymentTypeID == lstPaymentTypesBE[i].PaymentTypeID);
                            lstUDFFilterBE = lstUDF.FindAll(x => x.PaymentTypeID == lstPaymentTypesBE[i].PaymentTypeID && x.LanguageId == GlobalFunctions.GetLanguageId());/*User Type*/

                            for (int j = 0; j < lstUDFFilterBE.Count; j++)
                            {
                                if (lstUDFFilterBE[j].IsVisible)
                                {
                                    string strUDFValue = string.Empty;
                                    if (lstCustomerOrders != null && lstCustomerOrders.UDFValues != null && lstCustomerOrders.UDFValues.Count > 0 && bSetValues)
                                    {
                                        objUDFValues = new CustomerOrderBE.UDFValuesBE();
                                        objUDFValues = lstCustomerOrders.UDFValues.FirstOrDefault(x => x.UDFOasisID == lstUDFFilterBE[j].UDFOasisID);
                                        if (objUDFValues != null)
                                            strUDFValue = objUDFValues.UDFValue;
                                    }
                                    trUDFS = new TableRow();
                                    tcUDFS = new TableCell();
                                    lblUDFS = new Label();

                                    lblUDFS.ID = "lbl" + lstUDFFilterBE[j].UDFOasisID;
                                    lblUDFS.Text = lstUDFFilterBE[j].Caption + " :" + (lstUDFFilterBE[j].IsMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    lblUDFS.CssClass = "LabelText customLabel";
                                    tcUDFS.HorizontalAlign = HorizontalAlign.Left;
                                    tcUDFS.Width = Unit.Percentage(32);
                                    tcUDFS.Controls.Add(lblUDFS);
                                    trUDFS.Cells.Add(tcUDFS);
                                    trUDFS.Attributes.Add("class", "paymentid paymentid" + lstUDFFilterBE[j].PaymentTypeID);
                                    if (lstUDFFilterBE[j].RuleType == "A")
                                    {
                                        tcUDFS = new TableCell();
                                        txtUDFS = new TextBox();
                                        txtUDFS.ID = "txt" + lstUDFFilterBE[j].UDFOasisID;
                                        if (Convert.ToString(lstUDFFilterBE[j].MaximumLength) != "-1")
                                        {
                                            txtUDFS.MaxLength = lstUDFFilterBE[j].MaximumLength;
                                        }
                                        if (lstUDFFilterBE[j].CaseRule == "Y")
                                        {
                                            revCase = new RegularExpressionValidator();
                                            revCase.ID = "rev1" + txtUDFS.ID.ToString();
                                            revCase.ControlToValidate = txtUDFS.ID.ToString();
                                            revCase.ValidationExpression = "^[A-Z]*$";
                                            revCase.ErrorMessage = strUppercase;// "Text can only be in Uppercase";
                                            revCase.Display = ValidatorDisplay.Dynamic;
                                            revCase.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                            revCase.CssClass = "errortext";
                                            tcUDFS.Controls.Add(revCase);
                                        }
                                        if (lstUDFFilterBE[j].IsMandatory)
                                        {
                                            rfvUDF = new RequiredFieldValidator();
                                            rfvUDF.ID = "rfv1" + txtUDFS.ID.ToString();
                                            rfvUDF.ControlToValidate = txtUDFS.ID.ToString();
                                            rfvUDF.ErrorMessage = strRequired;// "Required";
                                            rfvUDF.Display = ValidatorDisplay.Dynamic;
                                            rfvUDF.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                            rfvUDF.CssClass = "errortext";
                                            tcUDFS.Controls.Add(rfvUDF);
                                        }
                                        txtUDFS.CssClass = "InputField form-control input-sm customInput";
                                        tcUDFS.Controls.Add(txtUDFS);
                                        tcUDFS.Width = Unit.Percentage(62);
                                        trUDFS.Cells.Add(tcUDFS);
                                        if (bSetValues)
                                            txtUDFS.Text = strUDFValue;
                                    }
                                    else if (lstUDFFilterBE[j].RuleType == "B")
                                    {
                                        tcUDFS = new TableCell();
                                        txtUDFS = new TextBox();
                                        rvRange = new RangeValidator();
                                        revNumeric = new RegularExpressionValidator();
                                        txtUDFS.ID = "txt" + lstUDFFilterBE[j].UDFOasisID;
                                        rvRange.ControlToValidate = txtUDFS.ID.ToString();
                                        if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1" || Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                        {
                                            if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1" && Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                            {
                                                rvRange.MinimumValue = Convert.ToString(lstUDFFilterBE[j].MinimumValue);
                                                rvRange.MaximumValue = Convert.ToString(lstUDFFilterBE[j].MaximumValue);
                                                strSC_UDF_Minimum_Maximum_Range = strSC_UDF_Minimum_Maximum_Range.Replace("@min", Convert.ToString(lstUDFFilterBE[j].MinimumValue));
                                                strSC_UDF_Minimum_Maximum_Range = strSC_UDF_Minimum_Maximum_Range.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumValue));
                                                rvRange.ErrorMessage = strSC_UDF_Minimum_Maximum_Range;// "Enter value between " + lstUDFFilterBE[j].MinimumValue + " and " + lstUDFFilterBE[j].MaximumValue;
                                            }
                                            else if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1")
                                            {
                                                rvRange.MinimumValue = Convert.ToString(lstUDFFilterBE[j].MinimumValue);
                                                strSC_UDF_Range2 = strSC_UDF_Range2.Replace("@min", Convert.ToString(lstUDFFilterBE[j].MinimumValue));
                                                rvRange.ErrorMessage = strSC_UDF_Range2;// "Enter value more or equal to " + lstUDFFilterBE[j].MinimumValue;
                                            }
                                            else if (Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                            {
                                                rvRange.MaximumValue = Convert.ToString(lstUDFFilterBE[j].MaximumValue);
                                                strSC_UDF_Range3 = strSC_UDF_Range3.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumValue));
                                                rvRange.ErrorMessage = strSC_UDF_Range3;// "Enter value less or equal to " + lstUDFFilterBE[j].MaximumValue;
                                            }
                                        }
                                        revNumeric.ControlToValidate = txtUDFS.ID;
                                        if (Convert.ToString(lstUDFFilterBE[j].DecimalPlaces) == "-1")
                                        {
                                            revNumeric.ValidationExpression = @"\d+";
                                            revNumeric.Text = strSC_Numeric;// "Enter only numbers";
                                        }
                                        else
                                        {
                                            revNumeric.ValidationExpression = @"^\d+(?:\.\d{" + lstUDFFilterBE[j].DecimalPlaces + "})?$";
                                            strSC_NumberMinDecimal = strSC_NumberMinDecimal.Replace("@num", Convert.ToString(lstUDFFilterBE[j].DecimalPlaces));
                                            revNumeric.Text = strSC_NumberMinDecimal;// "Enter only numbers with " + lstUDFFilterBE[j].DecimalPlaces + " decimal places";
                                        }
                                        revNumeric.SetFocusOnError = true;
                                        revNumeric.Display = ValidatorDisplay.None;
                                        rvRange.Display = ValidatorDisplay.None;
                                        revNumeric.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                        rvRange.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                        txtUDFS.CssClass = "InputField form-control input-sm customInput";
                                        revNumeric.CssClass = "errortext";
                                        rvRange.CssClass = "errortext";
                                        tcUDFS.Controls.Add(txtUDFS);
                                        tcUDFS.Controls.Add(revNumeric);
                                        tcUDFS.Controls.Add(rvRange);
                                        tcUDFS.Width = Unit.Pixel(475);
                                        trUDFS.Cells.Add(tcUDFS);
                                        if (bSetValues)
                                            txtUDFS.Text = strUDFValue;
                                    }
                                    else if (lstUDFFilterBE[j].RuleType == "E")
                                    {
                                        List<object> lstObjPicklist = new List<object>();
                                        List<PickListItemsBE> lstPicklist = new List<PickListItemsBE>();
                                        lstObjPicklist = ShoppingCartBL.GetUDFSValues(lstUDFFilterBE[j].PaymentTypeID, lstUDFFilterBE[j].SequenceNo);
                                        if (lstObjPicklist != null && lstObjPicklist.Count > 0)
                                        {
                                            lstPicklist = lstObjPicklist.Cast<PickListItemsBE>().ToList();
                                            tcUDFS = new TableCell();
                                            ddlPickListItems = new DropDownList();
                                            ddlPickListItems.ID = "ddl" + lstUDFFilterBE[j].UDFOasisID;
                                            ddlPickListItems.DataSource = lstPicklist;
                                            ddlPickListItems.DataTextField = "PayValue";
                                            ddlPickListItems.DataValueField = "ID";
                                            ddlPickListItems.CssClass = "InputField form-control input-sm customInput";
                                            ddlPickListItems.DataBind();
                                            lstPicklist = lstPicklist.FindAll(x => x.IsDefault == "Y");
                                            if (lstPicklist.Count > 0)
                                            {
                                                ddlPickListItems.SelectedValue = Convert.ToString(lstPicklist[0].ID);
                                            }
                                            ddlPickListItems.CssClass = "InputField form-control input-sm customInput ";
                                            tcUDFS.Controls.Add(ddlPickListItems);
                                            tcUDFS.Width = Unit.Pixel(475);
                                            trUDFS.Cells.Add(tcUDFS);
                                            try
                                            {
                                                if (bSetValues && !string.IsNullOrEmpty(strUDFValue))
                                                    ddlPickListItems.Items.FindByText(strUDFValue).Selected = true;
                                            }
                                            catch (Exception) { }
                                        }
                                    }
                                    tblUDFS.Rows.Add(trUDFS);
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind User delivery Address
        /// </summary>            
        /// <returns></returns>
        protected void BindDeliveryAddress()
        {
            try
            {
                if (Session["GuestUser"] == null)
                {
                    if (lstUser.UserDeliveryAddress.Count > 0)
                    {
                        ddlDAddressTitle.DataTextField = "AddressTitle";
                        ddlDAddressTitle.DataValueField = "DeliveryAddressId";
                        ddlDAddressTitle.DataSource = lstUser.UserDeliveryAddress;
                        ddlDAddressTitle.DataBind();
                    }
                    ListItem litem = new ListItem();
                    litem.Text = strNew_Delivery_Address;// "Add New";
                    litem.Value = "0";
                    ddlDAddressTitle.Items.Insert(0, litem);
                }
                else
                {
                    ddlDAddressTitle.DataTextField = "AddressTitle";
                    ddlDAddressTitle.DataValueField = "DeliveryAddressId";

                    ListItem litem = new ListItem();
                    litem.Text = strNew_Delivery_Address;// "Add New";
                    litem.Value = "0";
                    ddlDAddressTitle.Items.Insert(0, litem);

                    #region Comments
                    //litem = new ListItem();
                    //litem.Text = "Default";// "Add New";
                    //litem.Value = "1";
                    //ddlDAddressTitle.Items.Insert(1, litem); 
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 15-09-15
        /// Scope   : aCheckOut_ServerClick of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void aProceed_ServerClick(object sender, EventArgs e)
        {
            try
            {
                /*Added by sripal for non payment*/
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                List<FeatureBE> objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                if (objFeatures != null)
                {
                    List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;

                    if (objLstFeatureValueBE != null)
                    {
                        FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                        if (objFeatureValueBE == null)
                        {
                            if (!string.IsNullOrEmpty(txtCouponCode.Value) || !string.IsNullOrEmpty(hidCouponCode.Value))
                            {
                                #region
                                string str = hidCouponCode.Value;
                                string strShippmentType = (rbStandard.Checked) ? "1" : "2";
                                string strCoupon = CalculateCoupon(hidCouponCode.Value, strShippmentType, Convert.ToInt16(ddlCountry.SelectedValue));

                                string strShipmentType = strCoupon.Split(',')[7].Split(':')[1].ToString();
                                strShipmentType = strShipmentType.Replace("}]", "").Trim('\"');

                                //"[{\"Message\":\"" + strMessage + "\",\"OrderValueAfterDiscount\":\"" + OrderValueAfterDiscount + "\",\"StandardFreightValueAfterDiscount\":\"" +
                                //      StandardFreightValueAfterDiscount + "\",\"ExpressFreightValueAfterDiscount\":\"" + ExpressFreightValueAfterDiscount +
                                //      "\",\"StandardTaxValueAfterDiscount\":\"" + StandardTaxValueAfterDiscount + "\",\"ExpressTaxValueAfterDiscount\":\"" +
                                //      ExpressTaxValueAfterDiscount + "\",\"BehaviourType\":\"" + strBehaviourType.ToLower() + "\",\"ShipmentType\":\"" + strShipmentType.ToLower() + "\"}]";

                                if (!string.IsNullOrEmpty(strCoupon))
                                {
                                    string strStatus = strCoupon.Split(',')[0].Split(':')[1].ToString();
                                    strStatus = strStatus.Trim('\"');

                                    string strGoodsTotal = strCoupon.Split(',')[1].Split(':')[1].ToString();
                                    strGoodsTotal = strGoodsTotal.Trim('\"');

                                    if (!string.Equals(strStatus.ToLower(), "success"))
                                    {
                                        //GlobalFunctions.ShowModalAlertMessages(this, strStatus, AlertType.Failure);
                                        return;
                                    }
                                    else
                                    {
                                        #region
                                        string SZPrice = "";
                                        string EZPrice = "";
                                        string SZtax = "";
                                        string EZtax = "";
                                        if (strShipmentType == "0")
                                        {
                                            SZPrice = strCoupon.Split(',')[2].Split(':')[1].ToString();
                                            SZPrice = SZPrice.Trim('\"');

                                            EZPrice = strCoupon.Split(',')[3].Split(':')[1].ToString();
                                            EZPrice = EZPrice.Trim('\"');

                                            SZtax = strCoupon.Split(',')[4].Split(':')[1].ToString();
                                            SZtax = SZtax.Trim('\"');

                                            EZtax = strCoupon.Split(',')[5].Split(':')[1].ToString();
                                            EZtax = EZtax.Trim('\"');

                                            //$('#hidStandardTax').val(SZtax);
                                            //$('#hidExpressTax').val(EZtax);
                                            //$('#hidStandard').val(SZPrice);
                                            //$('#hidExpress').val(EZPrice);
                                            //$('#spnstandard').html(SZPrice);
                                            //$('#spnexpress').html(EZPrice);
                                        }
                                        else if (strShipmentType == "1")
                                        {
                                            SZPrice = strCoupon.Split(',')[2].Split(':')[1].ToString();
                                            SZPrice = SZPrice.Trim('\"');

                                            SZtax = strCoupon.Split(',')[4].Split(':')[1].ToString();
                                            SZtax = SZtax.Trim('\"');

                                            //$('#hidStandardTax').val(SZtax);
                                            //$('#hidStandard').val(SZPrice);
                                            //$('#spnstandard').html(SZPrice);
                                        }
                                        else if (strShipmentType == "2")
                                        {
                                            EZPrice = strCoupon.Split(',')[3].Split(':')[1].ToString();
                                            EZPrice = EZPrice.Trim('\"');

                                            EZtax = strCoupon.Split(',')[5].Split(':')[1].ToString();
                                            EZtax = EZtax.Trim('\"');

                                            //$('#hidExpressTax').val(EZtax);
                                            //$('#hidExpress').val(EZPrice);
                                            //$('#spnexpress').html(EZPrice);
                                        }
                                        double dTax = 0, dFreightPrice = 0;
                                        if (!rbStandard.Checked)
                                        {
                                            //$('#spnShippingPrice').html(EZPrice);
                                            //$('#spnTaxPrice').html(EZtax);
                                            dTax = Convert.ToDouble(EZtax);
                                            dFreightPrice = Convert.ToDouble(EZPrice);
                                        }
                                        else
                                        {
                                            //$('#spnShippingPrice').html(SZPrice);
                                            //$('#spnTaxPrice').html(SZtax);
                                            dTax = Convert.ToDouble(SZtax);
                                            dFreightPrice = Convert.ToDouble(SZPrice);
                                        }
                                        //totalPrice = dvTotal
                                        double dGoodsTotal = Convert.ToDouble(strGoodsTotal);
                                        double dFullTotal = 0;
                                        dFullTotal = dGoodsTotal + dTax + dFreightPrice;
                                        //totalPrice = (parseFloat(totalPrice) + aspnTaxPrice + spnShippingPrice).toFixed(2);
                                        if (dFullTotal > 0)
                                        {
                                            GlobalFunctions.ShowModalAlertMessages(this, strsc_insufficient_couponcode_balance, AlertType.Failure);
                                            return;
                                        }
                                        else { }
                                        #endregion
                                    }

                                }
                                #endregion
                            }
                            else
                            {
                                GlobalFunctions.ShowModalAlertMessages(this, strSC_Require_Coupon, AlertType.Failure);
                                return;
                            }
                        }
                    }
                }



                if (!ValidateDetails())
                {
                    return;
                }
                else
                {
                    #region
                    //add/update delivery address
                    if (SaveAddressDetails())
                    {
                        #region
                        //add/update order data
                        if (UpdateCustomerOrderData())
                        {
                            #region

                            objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                            if (objFeatures != null)
                            {
                                List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;

                                if (objLstFeatureValueBE != null)
                                {
                                    FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                                    if (objFeatureValueBE == null)
                                    {
                                        if (!string.IsNullOrEmpty(hidCouponCode.Value))
                                        {
                                            SaveUDFValues(Convert.ToInt32(1));
                                            //For other payment methods 
                                            Session["transaction"] = "true";
                                            Response.RedirectToRoute("OrderConfirmation");
                                        }
                                        else
                                        {
                                            string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                                            string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                                            if (strSplitPaymentMethod.Length > 1)
                                            {
                                                #region
                                                //If payment method is credit card
                                                if (strSplitPaymentMethod[1] == "C")
                                                    Response.RedirectToRoute("checkoutPagePurchase");
                                                //For testing purpose
                                                //Response.RedirectToRoute("checkoutPage");
                                                else
                                                {
                                                    SaveUDFValues(Convert.ToInt32(strSplitPaymentMethod[0]));
                                                    //For other payment methods 
                                                    Session["transaction"] = "true";
                                                    Response.RedirectToRoute("OrderConfirmation");
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Error_Processing_Payment, AlertType.Failure);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                                        string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                                        if (strSplitPaymentMethod.Length > 1)
                                        {
                                            #region
                                            //If payment method is credit card
                                            if (strSplitPaymentMethod[1] == "C")
                                                Response.RedirectToRoute("checkoutPagePurchase");
                                            //For testing purpose
                                            //Response.RedirectToRoute("checkoutPage");
                                            else
                                            {
                                                SaveUDFValues(Convert.ToInt32(strSplitPaymentMethod[0]));
                                                //For other payment methods 
                                                Session["transaction"] = "true";
                                                Response.RedirectToRoute("OrderConfirmation");
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Error_Processing_Payment, AlertType.Failure);
                                        }
                                    }
                                }
                            }



                            #region Commented by Sripal
                            //string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                            //string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                            //if (strSplitPaymentMethod.Length > 1)
                            //{
                            //    #region
                            //    //If payment method is credit card
                            //    if (strSplitPaymentMethod[1] == "C")
                            //        Response.RedirectToRoute("checkoutPagePurchase");
                            //    //For testing purpose
                            //    //Response.RedirectToRoute("checkoutPage");
                            //    else
                            //    {
                            //        SaveUDFValues(Convert.ToInt32(strSplitPaymentMethod[0]));
                            //        //For other payment methods 
                            //        Session["transaction"] = "true";
                            //        Response.RedirectToRoute("OrderConfirmation");
                            //    }
                            //    #endregion
                            //}
                            //else
                            //{
                            //    GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Error_Processing_Payment, AlertType.Failure);
                            //    //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is some error while processing payment.", AlertType.Failure);
                            //} 
                            #endregion
                            #endregion
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Updating_Order_Details, AlertType.Failure);
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is some error while updating order details.", AlertType.Failure);
                        }
                        #endregion
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Error_Update_Delivery_Address, AlertType.Failure);
                        //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is some error while updating delivery Address details.", AlertType.Failure);
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void PlaceOrder()
        {
            try
            {

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 06-10-15
        /// Scope   : Save UDF values into DB
        /// </summary>            
        /// <returns>True/False</returns>
        protected bool SaveUDFValues(Int32 intPaymentTypeId)
        {
            bool bStatus = false;
            try
            {
                string strErrormessage = string.Empty;
                List<object> lstObj = new List<object>();
                lstObj = ShoppingCartBL.GetUDFS(0);

                List<UDFBE> lstUDF = new List<UDFBE>();
                List<UDFBE> lstUDFFilterBE = new List<UDFBE>();
                lstUDF = lstObj.Cast<UDFBE>().ToList();

                lstUDFFilterBE = new List<UDFBE>();
                lstUDFFilterBE = lstUDF.FindAll(x => x.PaymentTypeID == intPaymentTypeId);

                StringBuilder sbUDFValues = new StringBuilder();
                for (int j = 0; j < lstUDFFilterBE.Count; j++)
                {
                    if (lstUDFFilterBE[j].IsVisible)
                    {

                        if (lstUDFFilterBE[j].RuleType == "A")
                        {
                            TextBox txtUDFS = (TextBox)tblUDFS.FindControl("txt" + lstUDFFilterBE[j].UDFOasisID);
                            if (Convert.ToString(lstUDFFilterBE[j].MaximumLength) != "-1")
                            {
                                if (txtUDFS.Text.Trim().Length > lstUDFFilterBE[j].MaximumLength)
                                {
                                    strSC_UDF_Max_Character = strSC_UDF_Max_Character.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumLength));
                                    strErrormessage += lstUDFFilterBE[j].Caption + strSC_UDF_Max_Character + "<br>";//: can not be more than " + lstUDFFilterBE[j].MaximumLength + " characters.<br>";
                                }
                            }
                            if (lstUDFFilterBE[j].CaseRule == "Y")
                            {
                                Regex rg = new Regex("^[A-Z]*$");
                                if (rg.IsMatch(txtUDFS.Text.Trim()) == false)
                                    strErrormessage += lstUDFFilterBE[j].Caption + ": " + strUppercase + " <br>";//Text can only be in Uppercase.<br>";
                            }
                            if (lstUDFFilterBE[j].IsMandatory)
                            {
                                if (GlobalFunctions.IsEmpty(txtUDFS.Text.Trim()))
                                    strErrormessage += lstUDFFilterBE[j].Caption + ": " + strRequired + "<br>";
                            }
                            sbUDFValues.Append(lstUDFFilterBE[j].UDFOasisID + "|" + Sanitizer.GetSafeHtmlFragment(txtUDFS.Text.Trim()) + ";");
                        }
                        else if (lstUDFFilterBE[j].RuleType == "B")
                        {
                            TextBox txtUDFS = (TextBox)tblUDFS.FindControl("txt" + lstUDFFilterBE[j].UDFOasisID);
                            if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1" || Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                            {
                                if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1" && Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                {
                                    if (Convert.ToInt32(txtUDFS.Text.Trim()) < Convert.ToInt32(lstUDFFilterBE[j].MinimumValue) &&
                                        Convert.ToInt32(txtUDFS.Text.Trim()) > Convert.ToInt32(lstUDFFilterBE[j].MaximumValue))
                                    {
                                        strSC_UDF_Minimum_Maximum_Range = strSC_UDF_Minimum_Maximum_Range.Replace("@min", Convert.ToString(lstUDFFilterBE[j].MinimumValue));
                                        strSC_UDF_Minimum_Maximum_Range = strSC_UDF_Minimum_Maximum_Range.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumValue));
                                        strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_UDF_Minimum_Maximum_Range + "<br>";
                                        //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter value between " + lstUDFFilterBE[j].MinimumValue + " and " + lstUDFFilterBE[j].MaximumValue + "<br>";
                                    }
                                }
                                else if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1")
                                {
                                    if (Convert.ToInt32(txtUDFS.Text.Trim()) < Convert.ToInt32(lstUDFFilterBE[j].MinimumValue))
                                    {
                                        strSC_UDF_Range2 = strSC_UDF_Range2.Replace("@min", Convert.ToString(lstUDFFilterBE[j].MinimumValue));
                                        strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_UDF_Range2 + "<br>";
                                        //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter value more or equal to " + lstUDFFilterBE[j].MinimumValue + "<br>";
                                    }
                                }
                                else if (Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                {
                                    if (Convert.ToInt32(txtUDFS.Text.Trim()) > Convert.ToInt32(lstUDFFilterBE[j].MaximumValue))
                                    {
                                        strSC_UDF_Range3 = strSC_UDF_Range3.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumValue));
                                        strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_UDF_Range3 + "<br>";
                                        //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter value less or equal to " + lstUDFFilterBE[j].MaximumValue + "<br>";
                                    }
                                }
                            }
                            if (Convert.ToString(lstUDFFilterBE[j].DecimalPlaces) == "-1")
                            {
                                Regex rg = new Regex("^\\d+$");
                                if (rg.IsMatch(txtUDFS.Text.Trim()) == false)
                                {
                                    strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_Numeric + "<br>";
                                    //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter only numbers.<br>";
                                }
                            }
                            else
                            {
                                Regex rg = new Regex(@"^\d+(?:\.\d{" + lstUDFFilterBE[j].DecimalPlaces + "})?$");
                                if (rg.IsMatch(txtUDFS.Text.Trim()) == false)
                                {
                                    strSC_NumberMinDecimal = strSC_NumberMinDecimal.Replace("@num", Convert.ToString(lstUDFFilterBE[j].DecimalPlaces));
                                    strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_NumberMinDecimal + "<br>";
                                    //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter only numbers with " + lstUDFFilterBE[j].DecimalPlaces + " decimal places<br>";
                                }
                            }
                            sbUDFValues.Append(lstUDFFilterBE[j].UDFOasisID + "|" + Sanitizer.GetSafeHtmlFragment(txtUDFS.Text.Trim()) + ";");
                        }
                        else if (lstUDFFilterBE[j].RuleType == "E")
                        {
                            DropDownList ddlPickListItems = (DropDownList)tblUDFS.FindControl("ddl" + lstUDFFilterBE[j].UDFOasisID);
                            sbUDFValues.Append(lstUDFFilterBE[j].UDFOasisID + "|" + Sanitizer.GetSafeHtmlFragment(ddlPickListItems.SelectedItem.Text.Trim()) + ";");
                        }
                    }
                }

                if (!string.IsNullOrEmpty(strErrormessage))
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strErrormessage, AlertType.Failure);
                    bStatus = false;
                }
                else
                {
                    if (!string.IsNullOrEmpty(sbUDFValues.ToString()))
                    {
                        ShoppingCartBL.UpdateUDFValues(sbUDFValues.ToString(), intCustomerOrderId);
                    }
                    bStatus = true;
                }
                return bStatus;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 30-09-15
        /// Scope   : Validate address and other Details
        /// </summary>            
        /// <returns>True/False</returns>
        protected bool ValidateDetails()
        {
            bool bStatus = false;
            try
            {
                string strErrormessage = string.Empty;
                string strDeliveryErrorMsg = "", strInvoiceErrorMsg = "";
                //strRequired
                //string strBillingtitle = strRegister_RegisteredAddress_Text;//  "Registered Address";
                //string strDeliveryTitle = strRegister_DeliveryAddress_Text;// "Delivery Address";
                //Validate billing contact name
                #region "strInvoiceErrorMsg"
                if (txtContact_Name.Attributes["rel"] == "m")
                {
                    if (GlobalFunctions.IsEmpty(txtContact_Name.Value))
                        strInvoiceErrorMsg += txtContact_Name.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtContact_Name.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtContact_Name.Value, 100))
                            strInvoiceErrorMsg += strValid + " " + txtContact_Name.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtContact_Name.Attributes["data-value"] + "<br>";
                }
                else if (GlobalFunctions.IsEmpty(txtContact_Name.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtContact_Name.Value, 100))
                    {
                        strInvoiceErrorMsg += strValid + " " + txtContact_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtContact_Name.Attributes["data-value"] + "<br>";
                    }
                //Validate billing txtCompany name
                if (txtCompany.Attributes["rel"] == "m")
                {
                    if (GlobalFunctions.IsEmpty(txtCompany.Value))
                        strInvoiceErrorMsg += txtCompany.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtCompany.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtCompany.Value, 100))
                            strInvoiceErrorMsg += strValid + " " + txtCompany.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtCompany.Attributes["data-value"] + "<br>";
                }
                else if (GlobalFunctions.IsEmpty(txtCompany.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtCompany.Value, 100))
                        strInvoiceErrorMsg += strValid + " " + txtCompany.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtCompany.Attributes["data-value"] + "<br>";

                //Validate billing Address line 1
                if (txtAddress1.Attributes["rel"] == "m")
                {
                    if (GlobalFunctions.IsEmpty(txtAddress1.Value))
                        strInvoiceErrorMsg += txtAddress1.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtAddress1.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtAddress1.Value, 200))
                            strInvoiceErrorMsg += strValid + " " + txtAddress1.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtAddress1.Attributes["data-value"] + "<br>";
                }
                else if (!GlobalFunctions.IsEmpty(txtAddress1.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtAddress1.Value, 200))
                        strInvoiceErrorMsg += strValid + " " + txtAddress1.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtAddress1.Attributes["data-value"] + "<br>";

                //Validate billing Address line 2
                if (txtAddress2.Attributes["rel"] == "m")
                {
                    if (GlobalFunctions.IsEmpty(txtAddress2.Value))
                        strInvoiceErrorMsg += txtAddress2.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtAddress2.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtAddress2.Value, 200))
                            strInvoiceErrorMsg += strValid + " " + txtAddress2.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtAddress2.Attributes["data-value"] + "<br>";
                }
                else if (!GlobalFunctions.IsEmpty(txtAddress2.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtAddress2.Value, 200))
                        strInvoiceErrorMsg += strValid + " " + txtAddress2.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtAddress2.Attributes["data-value"] + "<br>";

                //Validate billing Town
                if (txtTown.Attributes["rel"] == "m")
                {
                    if (GlobalFunctions.IsEmpty(txtTown.Value))
                        strInvoiceErrorMsg += txtTown.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtTown.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtTown.Value, 100))
                            strInvoiceErrorMsg += strValid + " " + txtTown.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtTown.Attributes["data-value"] + "<br>";
                }
                else if (!GlobalFunctions.IsEmpty(txtTown.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtTown.Value, 100))
                        strInvoiceErrorMsg += strValid + " " + txtTown.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtTown.Attributes["data-value"] + "<br>";

                //Validate billing State/County
                if (txtState__County.Attributes["rel"] == "m")
                {
                    if (GlobalFunctions.IsEmpty(txtState__County.Value))
                        strInvoiceErrorMsg += txtState__County.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtState__County.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtState__County.Value, 100))
                            strInvoiceErrorMsg += strValid + " " + txtState__County.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtState__County.Attributes["data-value"] + "<br>";
                }
                else if (!GlobalFunctions.IsEmpty(txtState__County.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtState__County.Value, 100))
                        strInvoiceErrorMsg += strValid + " " + txtState__County.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtState__County.Attributes["data-value"] + "<br>";

                //Validate billing Postal Code
                if (txtPostal_Code.Attributes["rel"] == "m")
                {
                    if (GlobalFunctions.IsEmpty(txtPostal_Code.Value))
                        strInvoiceErrorMsg += txtPostal_Code.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtPostal_Code.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsValidPostCode(txtPostal_Code.Value, 20))
                            strInvoiceErrorMsg += strValid + " " + txtPostal_Code.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtPostal_Code.Attributes["data-value"] + "<br>";
                }
                else if (!GlobalFunctions.IsEmpty(txtPostal_Code.Value))
                    if (!GlobalFunctions.IsValidPostCode(txtPostal_Code.Value, 20))
                        strInvoiceErrorMsg += strValid + " " + txtPostal_Code.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtPostal_Code.Attributes["data-value"] + "<br>";

                //Validate billing Country
                if (ddlDCountry.Attributes["rel"] == "m")
                    if (ddlDCountry.SelectedValue == "0")
                        strInvoiceErrorMsg += ddlDCountry.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please choose " + ddlDCountry.Attributes["data-value"] + "<br>";

                //Validate billing Phone
                if (txtPhone.Attributes["rel"] == "m")
                {
                    if (GlobalFunctions.IsEmpty(txtPhone.Value))
                        strInvoiceErrorMsg += txtPhone.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtPhone.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsValidPhone(txtPhone.Value, 15))
                            strInvoiceErrorMsg += strValid + " " + txtPhone.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtPhone.Attributes["data-value"] + "<br>";
                }
                else if (!GlobalFunctions.IsEmpty(txtPhone.Value))
                    if (!GlobalFunctions.IsValidPhone(txtPhone.Value, 15))
                        strInvoiceErrorMsg += strValid + " " + txtPhone.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtPhone.Attributes["data-value"] + "<br>";

                #endregion

                if (!string.IsNullOrEmpty(strInvoiceErrorMsg))
                {
                    strErrormessage = strRegister_RegisteredAddress_Text + ": <br>" + strInvoiceErrorMsg + "<br>";
                }

                //validate delivery address if it is not same as billing address
                #region Delivery
                if (addressCheckbox.Checked == false)
                {
                    //Validate delivery adress title
                    if (ddlDAddressTitle.SelectedValue == "0")
                    {
                        if (GlobalFunctions.IsEmpty(txtAddressTitle.Value))
                            strDeliveryErrorMsg += strBasket_Title_Text + " <br>";
                        //strErrormessage += "Please enter delivery address title <br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtAddressTitle.Value, 50))
                                strDeliveryErrorMsg += strValid + " " + strBasket_Title_Text + " <br>";
                        //strErrormessage += "Please enter valid delivery address title <br>";
                    }

                    //Validate delivery contact name
                    if (txtDContact_Name.Attributes["rel"] == "m")
                    {
                        if (GlobalFunctions.IsEmpty(txtDContact_Name.Value))
                            strDeliveryErrorMsg += txtDContact_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDContact_Name.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDContact_Name.Value, 100))
                                strDeliveryErrorMsg += strValid + " " + txtDContact_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDContact_Name.Attributes["data-value"] + "<br>";
                    }
                    else if (GlobalFunctions.IsEmpty(txtDContact_Name.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDContact_Name.Value, 100))
                            strDeliveryErrorMsg += strValid + " " + txtDContact_Name.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDContact_Name.Attributes["data-value"] + "<br>";

                    #region Code Added by SHRIGANESH to Validate Company Name in Delivery Address section 27 July 2016

                    //Validate delivery contact name
                    if (txtDCompany_Name.Attributes["rel"] == "m")
                    {
                        if (GlobalFunctions.IsEmpty(txtDCompany_Name.Value))
                            strDeliveryErrorMsg += txtDCompany_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDCompany_Name.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDCompany_Name.Value, 100))
                                strDeliveryErrorMsg += strValid + " " + txtDCompany_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDCompany_Name.Attributes["data-value"] + "<br>";
                    }
                    else if (GlobalFunctions.IsEmpty(txtDCompany_Name.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDCompany_Name.Value, 100))
                            strDeliveryErrorMsg += strValid + " " + txtDCompany_Name.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDCompany_Name.Attributes["data-value"] + "<br>";

                    #endregion

                    //Validate delivery Address line 1
                    if (txtDAddress1.Attributes["rel"] == "m")
                    {
                        if (GlobalFunctions.IsEmpty(txtDAddress1.Value))
                            strDeliveryErrorMsg += txtDAddress1.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDAddress1.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDAddress1.Value, 200))
                                strDeliveryErrorMsg += strValid + " " + txtDAddress1.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDAddress1.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDAddress1.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDAddress1.Value, 200))
                            strDeliveryErrorMsg += strValid + " " + txtDAddress1.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDAddress1.Attributes["data-value"] + "<br>";

                    //Validate delivery Address line 2
                    if (txtDAddress2.Attributes["rel"] == "m")
                    {
                        if (GlobalFunctions.IsEmpty(txtDAddress2.Value))
                            strDeliveryErrorMsg += txtDAddress2.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDAddress2.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDAddress2.Value, 200))
                                strDeliveryErrorMsg += strValid + " " + txtDAddress2.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDAddress2.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDAddress2.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDAddress2.Value, 200))
                            strDeliveryErrorMsg += strValid + " " + txtDAddress2.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDAddress2.Attributes["data-value"] + "<br>";

                    //Validate delivery Town
                    if (txtDTown.Attributes["rel"] == "m")
                    {
                        if (GlobalFunctions.IsEmpty(txtDTown.Value))
                            strDeliveryErrorMsg += txtDTown.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDTown.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDTown.Value, 100))
                                strDeliveryErrorMsg += strValid + " " + txtDTown.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDTown.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDTown.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDTown.Value, 100))
                            strDeliveryErrorMsg += strValid + " " + txtDTown.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDTown.Attributes["data-value"] + "<br>";

                    //Validate delivery State/County
                    if (txtDState__County.Attributes["rel"] == "m")
                    {
                        if (GlobalFunctions.IsEmpty(txtDState__County.Value))
                            strDeliveryErrorMsg += txtDState__County.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDState__County.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDState__County.Value, 100))
                                strDeliveryErrorMsg += strValid + " " + txtDState__County.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDState__County.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDState__County.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDState__County.Value, 100))
                            strDeliveryErrorMsg += strValid + " " + txtDState__County.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDState__County.Attributes["data-value"] + "<br>";

                    //Validate delivery Postal Code
                    if (txtDPostal_Code.Attributes["rel"] == "m")
                    {
                        if (GlobalFunctions.IsEmpty(txtDPostal_Code.Value))
                            strDeliveryErrorMsg += txtDPostal_Code.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDPostal_Code.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsValidPostCode(txtDPostal_Code.Value, 20))
                                strDeliveryErrorMsg += strValid + " " + txtDPostal_Code.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDPostal_Code.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDPostal_Code.Value))
                        if (!GlobalFunctions.IsValidPostCode(txtDPostal_Code.Value, 20))
                            strDeliveryErrorMsg += strValid + " " + txtDPostal_Code.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDPostal_Code.Attributes["data-value"] + "<br>";

                    //Validate delivery Country
                    if (ddlDCountry.Attributes["rel"] == "m")
                        if (ddlDCountry.SelectedValue == "0")
                            strDeliveryErrorMsg += ddlDCountry.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please choose " + ddlDCountry.Attributes["data-value"] + "<br>";

                    //Validate delivery Phone
                    if (txtDPhone.Attributes["rel"] == "m")
                    {
                        if (GlobalFunctions.IsEmpty(txtDPhone.Value))
                            strDeliveryErrorMsg += txtDPhone.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDPhone.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsValidPhone(txtDPhone.Value, 15))
                                strDeliveryErrorMsg += strValid + " " + txtDPhone.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDPhone.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDPhone.Value))
                        if (!GlobalFunctions.IsValidPhone(txtDPhone.Value, 15))
                            strDeliveryErrorMsg += strValid + " " + txtDPhone.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDPhone.Attributes["data-value"] + "<br>";
                }
                #endregion
                if (!string.IsNullOrEmpty(strDeliveryErrorMsg))
                {
                    strErrormessage += strRegister_DeliveryAddress_Text + ": <br>" + strDeliveryErrorMsg + "<br>";
                }

                //Check for payment method
                if (ddlPaymentTypes.SelectedValue == "0")
                    strDeliveryErrorMsg += strSC_Payment_Types;
                //strErrormessage += "Please select payment method.<br>";

                //check for the special instructions
                if (!GlobalFunctions.IsEmpty(txtInstruction.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtInstruction.Value, 200))
                        strErrormessage += strValid + " " + strSC_Special_Instruction + "<br>";
                //strErrormessage += "Please enter valid special instructions. <br>";

                //Check for the valid shipping method
                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                lstFreightBE = CalculateFreight(Convert.ToInt16(ddlDCountry.SelectedValue));

                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    if (rbStandard.Checked && lstFreightBE[0].SZDisallowOrder)
                    {
                        strErrormessage += strFreightMOVMessage;
                    }
                    else if (lstFreightBE[0].EZDisallowOrder)
                    {
                        strErrormessage += strFreightMOVMessage;
                    }
                }
                //Check for the inventory of the products
                if (bBackOrderAllowed == false)
                {
                    #region
                    for (var i = 0; i < rptBasketListing.Items.Count; i++)
                    {
                        //HtmlInputText txtQuantity = (HtmlInputText)rptBasketListing.Items[i].FindControl("txtQuantity");//Commented by Sripal
                        HtmlGenericControl txtQuantity = (HtmlGenericControl)rptBasketListing.Items[i].FindControl("txtQuantity");//Added by Sripal
                        HtmlInputHidden hdnInventory = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnInventory");
                        HtmlInputHidden hdnStockStatus = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnStockStatus");
                        HtmlGenericControl dvProductName = (HtmlGenericControl)rptBasketListing.Items[i].FindControl("dvProductName");

                        if (bBASysStore == false)
                        {
                            //if (Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtQuantity.Value)) > Convert.ToInt32(hdnInventory.Value))
                            //    strErrormessage += dvProductName.InnerHtml + ": " + " Can not order more than " + hdnInventory.Value;

                            if (Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtQuantity.InnerText)) > Convert.ToInt32(hdnInventory.Value))
                            {
                                strErrormessage += dvProductName.InnerHtml + ": " + strBasket_Quantity_Not_More_Than_Stock_Message + hdnInventory.Value + "<br>";
                                //strErrormessage += dvProductName.InnerHtml + ": " + " Can not order more than " + hdnInventory.Value;
                            }
                        }
                        else
                        {
                            //if (Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtQuantity.Value)) > Convert.ToInt32(hdnStockStatus.Value))
                            //    strErrormessage += dvProductName.InnerHtml + ": " + " Can not order more than " + hdnStockStatus.Value;
                            if (Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtQuantity.InnerText)) > Convert.ToInt32(hdnStockStatus.Value))
                                strErrormessage += dvProductName.InnerHtml + ": " + strBasket_Quantity_Not_More_Than_Stock_Message + hdnStockStatus.Value + "<br>";
                            //strErrormessage += dvProductName.InnerHtml + ": " + " Can not order more than " + hdnStockStatus.Value;
                        }
                    }
                    #endregion
                }
                if (IsPointsEnbled)
                {
                    #region
                    List<object> lstObjShoppingBE = new List<object>();
                    lstObjShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId);
                    List<ShoppingCartBE> lstShoppingBE = new List<ShoppingCartBE>();
                    lstShoppingBE = lstObjShoppingBE.Cast<ShoppingCartBE>().ToList();

                    Int32 intPoints = 0;
                    for (int i = 0; i < rptBasketListing.Items.Count; i++)
                    {
                        HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnShoppingCartProductId");

                        //HtmlInputText txtQuantity = (HtmlInputText)rptBasketListing.Items[i].FindControl("txtQuantity");//Commented by Sripal
                        HtmlGenericControl txtQuantity = (HtmlGenericControl)rptBasketListing.Items[i].FindControl("txtQuantity");//Added by Sripal

                        //if (GlobalFunctions.isNumeric(txtQuantity.Value.Trim(), System.Globalization.NumberStyles.Integer))
                        //{
                        //    intPoints += Convert.ToInt32(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstShoppingBE.FirstOrDefault(x => x.ShoppingCartProductId == Convert.ToInt32(hdnShoppingCartProductId.Value)).Price * Convert.ToInt32(txtQuantity.Value.Trim())), "", intLanguageId));
                        //}
                        if (GlobalFunctions.isNumeric(txtQuantity.InnerText.Trim(), System.Globalization.NumberStyles.Integer))
                        {
                            intPoints += Convert.ToInt32(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstShoppingBE.FirstOrDefault(x => x.ShoppingCartProductId == Convert.ToInt32(hdnShoppingCartProductId.Value)).Price * Convert.ToInt32(txtQuantity.InnerText.Trim())), "", intLanguageId));
                        }
                    }
                    if (intPoints > lstUser.Points)
                    {
                        strErrormessage += strSC_InSufficient_Point;// "You do not have sufficient points for this order.";
                    }
                    #endregion
                }
                //Check uploaded file extension
                #region CheckUploaded File Extension
                if (fuDoc.HasFiles)
                {
                    if (fuDoc.PostedFile.ContentLength > 0)
                    {
                        //Chk for File Extension
                        string[] strFileTypes = { ".jpg", ".jpeg", ".gif", ".png", ".ai", ".pdf", ".eps" };
                        string[] strMimeTypes = { "image/jpeg", "image/gif", "image/png", "image/bmp", "image/tiff", "image/x-icon", "application/postscript", "application/pdf" };
                        bool bChkFileType = false;
                        bChkFileType = GlobalFunctions.CheckFileExtension(fuDoc, strFileTypes);
                        bool bChkFileMimeType = false;
                        bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(fuDoc, strMimeTypes);
                        if (!(bChkFileType && bChkFileMimeType))
                        {
                            strErrormessage += strFile_Extension + "<br>";// "Invalid File Type or Content Type.<br>";
                        }
                    }
                }
                #endregion

                if (!string.IsNullOrEmpty(strErrormessage))
                {
                    string strMs = "<font color:red>" + strRequired + "</font>" + strErrormessage;
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strMs, AlertType.Failure);
                    bStatus = false;
                }
                else
                    bStatus = true;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
            return bStatus;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind UpdateCustomerOrderData
        /// </summary>            
        /// <returns>True</returns>
        protected bool UpdateCustomerOrderData()
        {
            bool bStatus = false;
            try
            {
                TxRefGUID = Funcs.CreateTxRefGUID();
                CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
                objCustomerOrderBE.OrderedBy = lstUser.UserId;

                #region Region Commented on 28 July 2016 so that every time the order details should be Inserted into the DataBase
                /*
                if (!string.IsNullOrEmpty(Convert.ToString(Session["CustomerOrderId"])))
                {
                    objCustomerOrderBE.action = DBAction.Update;
                    objCustomerOrderBE.CustomerOrderId = Convert.ToInt32(Session["CustomerOrderId"]);
                }
                else
                {
                    objCustomerOrderBE.CustomerOrderId = 0;
                    objCustomerOrderBE.action = DBAction.Insert;
                }
                 */
                #endregion

                #region Code Added by SHRIGANESH 28 July 2016 to insert the Latest Order details into DataBase.
                objCustomerOrderBE.CustomerOrderId = 0;
                objCustomerOrderBE.action = DBAction.Insert;
                #endregion

                objCustomerOrderBE.ApprovedBy = 0;
                objCustomerOrderBE.InvoiceCompany = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                objCustomerOrderBE.InvoiceContactPerson = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                objCustomerOrderBE.InvoiceAddress1 = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                objCustomerOrderBE.InvoiceAddress2 = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                objCustomerOrderBE.InvoiceCity = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                objCustomerOrderBE.InvoicePostalCode = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                objCustomerOrderBE.InvoiceCountry = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                objCustomerOrderBE.InvoiceCounty = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                objCustomerOrderBE.InvoicePhone = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                objCustomerOrderBE.InvoiceFax = "";
                objCustomerOrderBE.InvoiceEmail = lstUser.EmailId;

                /*Added by sripal for non payment*/
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                List<FeatureBE> objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                if (objFeatures != null)
                {
                    List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;

                    if (objLstFeatureValueBE != null)
                    {
                        FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                        if (objFeatureValueBE == null)
                        {
                            objCustomerOrderBE.PaymentMethod = "1";
                        }
                        else
                        {
                            string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                            string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                            if (strSplitPaymentMethod.Length > 1)
                            {
                                objCustomerOrderBE.PaymentMethod = Sanitizer.GetSafeHtmlFragment(strSplitPaymentMethod[0]);
                            }
                        }
                    }
                    else
                    {
                        string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                        string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                        if (strSplitPaymentMethod.Length > 1)
                        {
                            objCustomerOrderBE.PaymentMethod = Sanitizer.GetSafeHtmlFragment(strSplitPaymentMethod[0]);
                        }
                    }
                }
                else
                {
                    string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                    string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                    if (strSplitPaymentMethod.Length > 1)
                    {
                        objCustomerOrderBE.PaymentMethod = Sanitizer.GetSafeHtmlFragment(strSplitPaymentMethod[0]);
                    }
                }
                //string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                //string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                //if (strSplitPaymentMethod.Length > 1)
                //{
                //    objCustomerOrderBE.PaymentMethod = Sanitizer.GetSafeHtmlFragment(strSplitPaymentMethod[0]);
                //}
                objCustomerOrderBE.OrderRefNo = Convert.ToString(System.DateTime.Now);
                objCustomerOrderBE.AuthorizationId_OASIS = "";
                objCustomerOrderBE.OrderId_OASIS = 0;
                objCustomerOrderBE.TxnRefGUID = TxRefGUID;
                objCustomerOrderBE.IsOrderCompleted = false;
                objCustomerOrderBE.DeliveryCompany = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                if (addressCheckbox.Checked == true)
                    objCustomerOrderBE.DeliveryAddressId = 0;
                else if (hidDeliveryAddressId.Value == "")
                    objCustomerOrderBE.DeliveryAddressId = Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(ddlDAddressTitle.SelectedValue));
                else
                    objCustomerOrderBE.DeliveryAddressId = Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(hidDeliveryAddressId.Value.ToString()));
                objCustomerOrderBE.DeliveryContactPerson = Sanitizer.GetSafeHtmlFragment(txtDContact_Name.Value.Trim());
                objCustomerOrderBE.DeliveryCompany = Sanitizer.GetSafeHtmlFragment(txtDCompany_Name.Value.Trim()); // Added By SHRIGANESH 27 July 2016 for Delivery Company
                objCustomerOrderBE.DeliveryAddress1 = Sanitizer.GetSafeHtmlFragment(txtDAddress1.Value.Trim());
                objCustomerOrderBE.DeliveryAddress2 = Sanitizer.GetSafeHtmlFragment(txtDAddress2.Value.Trim());
                objCustomerOrderBE.DeliveryCity = Sanitizer.GetSafeHtmlFragment(txtDTown.Value.Trim());
                objCustomerOrderBE.DeliveryPostalCode = Sanitizer.GetSafeHtmlFragment(txtDPostal_Code.Value.Trim());
                objCustomerOrderBE.DeliveryCountry = Sanitizer.GetSafeHtmlFragment(ddlDCountry.SelectedValue.Trim());
                objCustomerOrderBE.DeliveryCounty = Sanitizer.GetSafeHtmlFragment(txtDState__County.Value.Trim());
                objCustomerOrderBE.DeliveryPhone = Sanitizer.GetSafeHtmlFragment(txtDPhone.Value.Trim());
                objCustomerOrderBE.DeliveryFax = "";
                objCustomerOrderBE.stCardScheme = "";
                objCustomerOrderBE.SpecialInstructions = Sanitizer.GetSafeHtmlFragment(txtInstruction.Value.Trim());
                objCustomerOrderBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                objCustomerOrderBE.LanguageId = GlobalFunctions.GetLanguageId();

                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                lstFreightBE = CalculateFreight(Convert.ToInt16(Sanitizer.GetSafeHtmlFragment(ddlDCountry.SelectedValue)));

                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    string sfSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                    string sfEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                    if (rbStandard.Checked)
                    {
                        //objCustomerOrderBE.StandardCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].SZPrice)));
                        objCustomerOrderBE.StandardCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(sfSZPrice)));
                        objCustomerOrderBE.ExpressCharges = 0;
                        objCustomerOrderBE.OrderCarrierId = lstFreightBE[0].SZCarrierServiceId;
                        objCustomerOrderBE.OrderCarrierName = lstFreightBE[0].SZCarrierServiceText;
                    }
                    else if (rbExpress.Checked)
                    {
                        objCustomerOrderBE.StandardCharges = 0;
                        //objCustomerOrderBE.ExpressCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].EZPrice)));
                        objCustomerOrderBE.ExpressCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(sfEZPrice)));
                        objCustomerOrderBE.OrderCarrierId = lstFreightBE[0].EZCarrierServiceId;
                        objCustomerOrderBE.OrderCarrierName = lstFreightBE[0].EZCarrierServiceText;
                    }
                    else
                    {
                        objCustomerOrderBE.OrderCarrierId = string.IsNullOrEmpty(lstFreightBE[0].SZCarrierServiceId) ? lstFreightBE[0].EZCarrierServiceId : lstFreightBE[0].SZCarrierServiceId;
                        objCustomerOrderBE.OrderCarrierName = string.IsNullOrEmpty(lstFreightBE[0].SZCarrierServiceText) ? lstFreightBE[0].EZCarrierServiceText : lstFreightBE[0].SZCarrierServiceText;
                    }

                    double discountedPercentage = 0.0;
                    if (Session["discountPercentage"] != null)
                        discountedPercentage = Convert.ToDouble(Session["discountPercentage"]);

                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";

                    string strSZPrice = "0", strEZPrice = "0"; ;
                    if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                    {
                        strSZPrice = Convert.ToString(float.Parse(lstFreightBE[0].SZPrice, NumberStyles.Any, ci), CultureInfo.InvariantCulture);
                    }
                    if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                    {
                        strEZPrice = Convert.ToString(float.Parse(lstFreightBE[0].EZPrice, NumberStyles.Any, ci), CultureInfo.InvariantCulture);
                    }
                    string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), strSZPrice, strEZPrice, discountedPercentage);
                    //string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), lstFreightBE[0].SZPrice, lstFreightBE[0].EZPrice, discountedPercentage);

                    if (!string.IsNullOrEmpty(strTotalTax))
                    {
                        string[] strTax = strTotalTax.Split('|');
                        if (strTax.Length > 0)
                        {
                            if (rbStandard.Checked)
                                objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[0]))); //Total tax for standard zone
                            else
                                objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[1]))); //Total tax for express zone
                        }
                    }

                    objCustomerOrderBE.CouponCode = string.Empty; ;

                    if (!string.IsNullOrEmpty(hidCouponCode.Value.Trim()))
                    {
                        #region Freight&TaxForCouponCode
                        double OrderItemTotal = 0;
                        string StandardFreightValueAfterDiscount = "0";
                        string ExpressFreightValueAfterDiscount = "0";
                        string StandardTaxValueAfterDiscount = "0";
                        //double OrderValueAfterDiscount = 0;
                        string ExpressTaxValueAfterDiscount = "0";
                        string strBehaviourType = string.Empty;
                        List<object> lstShoppingBE = new List<object>();
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                        if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                        {
                            List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                            lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                            for (int i = 0; i < lstShoppingBE.Count; i++)
                            {
                                OrderItemTotal += Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                            }
                        }

                        CouponBE objCouponBE = new CouponBE();
                        List<CouponBE> lstCouponBE = new List<CouponBE>();
                        List<CountryBE> lstCountry = new List<CountryBE>();
                        objCouponBE.OrderValue = float.Parse(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(OrderItemTotal)));
                        objCouponBE.CouponCode = hidCouponCode.Value.Trim();
                        lstCountry = CountryBL.GetAllCountries();
                        if (lstCountry != null && lstCountry.Count > 0)
                        {
                            string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(ddlDCountry.SelectedValue)).RegionCode;
                            objCouponBE.Region = strRegionCode == "ROW" ? "Other" : strRegionCode;
                        }
                        objCouponBE.ShipmentType = rbStandard.Checked == true ? "1" : "2"; // 1 for Standard, 2 for express shipping method
                        lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, lstUser, GlobalFunctions.GetCurrencyId());
                        if (lstCouponBE[0].IsActive && lstCouponBE[0].IsExpired == false && lstCouponBE[0].IsValidOrderValue && lstCouponBE[0].IsValidRegion && lstCouponBE[0].IsValidShipmentType)
                        {
                            string strFSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                            string strFEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                            strBehaviourType = lstCouponBE[0].BehaviourType;
                            if (lstCouponBE[0].BehaviourType.ToLower() != "product")
                            {
                                double dDiscountPercentage = 0;
                                //Apply to goods only, excluding Freight Value 
                                if (lstCouponBE[0].BehaviourType.ToLower() == "basket")
                                {
                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone

                                    dDiscountPercentage = Convert.ToDouble(lstCouponBE[0].DiscountPercentage);
                                }
                                //Free shipping excluding Freight Value
                                else if (lstCouponBE[0].BehaviourType.ToLower() == "freeshipping")
                                {
                                    StandardFreightValueAfterDiscount = "0"; //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = "0"; //Total tax for express zone
                                    dDiscountPercentage = 0;
                                }
                                //Discounted Shipping
                                else if (lstCouponBE[0].BehaviourType.ToLower() == "shipping")
                                {
                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    dDiscountPercentage = 0;
                                }

                                string strTotalTaxForCoupon = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, dDiscountPercentage);
                                if (!string.IsNullOrEmpty(strTotalTaxForCoupon))
                                {
                                    string[] strTax = strTotalTaxForCoupon.Split('|');
                                    if (strTax.Length > 0)
                                    {
                                        StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                        ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                    }
                                }

                                if (rbStandard.Checked)
                                {
                                    objCustomerOrderBE.StandardCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(StandardFreightValueAfterDiscount)));
                                    objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(StandardTaxValueAfterDiscount))); //Total tax for standard zone
                                    objCustomerOrderBE.ExpressCharges = 0;
                                }
                                else if (rbExpress.Checked)
                                {
                                    objCustomerOrderBE.StandardCharges = 0;
                                    objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ExpressTaxValueAfterDiscount))); //Total tax for express zone
                                    objCustomerOrderBE.ExpressCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ExpressFreightValueAfterDiscount)));
                                }
                            }
                            //Add coupon code in session and use it on the creditcard payment and Order xml.
                            objCustomerOrderBE.CouponCode = hidCouponCode.Value.Trim();
                        }
                        #endregion
                    }
                }

                CustomerOrderBE lstShoppingCart = new CustomerOrderBE();
                lstShoppingCart = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                if (lstShoppingCart != null && lstShoppingCart.CustomerOrderId != 0)
                {
                    intCustomerOrderId = lstShoppingCart.CustomerOrderId;
                    bStatus = true;

                    //Save uploaded file extension
                    #region Save Uploaded File
                    if (fuDoc.HasFiles)
                    {
                        if (fuDoc.PostedFile.ContentLength > 0)
                        {
                            try
                            {
                                string strFileUploadPath = GlobalFunctions.GetPhysicalFolderPath() + "Images\\CustomerOrders\\" + intCustomerOrderId + "\\";
                                string strImgPath = strFileUploadPath + fuDoc.FileName;
                                if (!System.IO.Directory.Exists(strFileUploadPath))
                                    System.IO.Directory.CreateDirectory(strFileUploadPath);

                                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(strFileUploadPath);
                                System.IO.FileInfo[] files = di.GetFiles("*.*");
                                foreach (System.IO.FileInfo file in files)
                                    try
                                    {
                                        file.Attributes = System.IO.FileAttributes.Normal;
                                        System.IO.File.Delete(file.FullName);
                                    }
                                    catch { }

                                fuDoc.SaveAs(strImgPath);
                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return bStatus;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind SaveAddressDetails
        /// </summary>            
        /// <returns>True</returns>
        protected bool SaveAddressDetails()
        {
            bool bStatus = false;
            try
            {
                if (Session["GuestUser"] != null)
                {
                    Session["IsLogin"] = true;
                    #region "Insert into database and BASYS"
                    #region
                    int i = 0;
                    try
                    {
                        UserBE objBE = new UserBE();
                        UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                        objBE.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["GuestUser"]).Trim());
                        objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);

                        Exceptions.WriteInfoLog("textBox = " + txtContact_Name.Value);
                        #region "Region for the First name and last Name"
                        List<string> objLs = SplitName(txtContact_Name.Value);
                        if (objLs.Count == 2)
                        {
                            objBE.FirstName = objLs[0];
                            objBE.LastName = objLs[1];
                        }

                        #region "1"
                        //string[] strArr = txtContact_Name.Value.Trim().Split(' ');
                        //if (strArr.Count() >= 2)
                        //{
                        //    Exceptions.WriteInfoLog("Arr 0 = " + strArr[0]);
                        //    objBE.FirstName = strArr[0];
                        //    int ij = 0;
                        //    string strLName = "";
                        //    foreach (string str in strArr)
                        //    {
                        //        if (ij != 0)
                        //        {
                        //            strLName = strLName + " " + str;
                        //        }
                        //        ij++;
                        //    }
                        //    objBE.LastName = !string.IsNullOrEmpty(strLName) ? strLName.Trim() : ".";
                        //    Exceptions.WriteInfoLog("objBE.LastName = " + objBE.LastName);
                        //    Exceptions.WriteInfoLog("LastName = " + strLName.Trim());
                        //    objBE.PredefinedColumn1 = strArr[0].Trim();
                        //    objBE.PredefinedColumn2 = !string.IsNullOrEmpty(strLName) ? strLName.Trim() : "."; ;
                        //}
                        //else
                        //{
                        //    Exceptions.WriteInfoLog("Fname = " + txtContact_Name.Value);
                        //    objBE.FirstName = txtContact_Name.Value;
                        //    objBE.LastName = ".";
                        //    objBE.PredefinedColumn1 = txtContact_Name.Value;
                        //    objBE.PredefinedColumn2 = txtCompany.Value;
                        //} 
                        #endregion
                        #region "2"
                        //try
                        //{
                        //    if (txtContact_Name.Value.Trim().IndexOf(' ') > 0)
                        //    {
                        //        objBE.FirstName = txtContact_Name.Value.Trim().Substring(0, txtContact_Name.Value.Trim().IndexOf(' '));
                        //        objBE.LastName = txtContact_Name.Value.Trim().Substring(txtContact_Name.Value.Trim().IndexOf(' ') + 1, (txtContact_Name.Value.Trim().Length - (txtContact_Name.Value.Trim().IndexOf(' ') + 1)));
                        //    }
                        //    else
                        //    {
                        //        objBE.FirstName = txtContact_Name.Value.Trim();
                        //        objBE.LastName = ".";
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    objBE.FirstName = txtContact_Name.Value.Trim();
                        //    objBE.LastName = ".";
                        //} 
                        #endregion

                        #endregion
                        objBE.PredefinedColumn1 = txtContact_Name.Value;
                        objBE.PredefinedColumn2 = txtCompany.Value;
                        objBE.PredefinedColumn3 = txtAddress1.Value;
                        objBE.PredefinedColumn4 = txtAddress2.Value;
                        objBE.PredefinedColumn5 = txtTown.Value;
                        objBE.PredefinedColumn6 = txtState__County.Value;
                        objBE.PredefinedColumn7 = txtPostal_Code.Value;
                        objBE.PredefinedColumn8 = ddlCountry.SelectedValue;
                        objBE.PredefinedColumn9 = txtPhone.Value;

                        if (addressCheckbox.Checked)
                        {
                            obj.AddressTitle = "Default";
                            obj.PreDefinedColumn1 = txtContact_Name.Value;
                            obj.PreDefinedColumn2 = txtCompany.Value;
                            obj.PreDefinedColumn3 = txtAddress1.Value;
                            obj.PreDefinedColumn4 = txtAddress2.Value;
                            obj.PreDefinedColumn5 = txtTown.Value;
                            obj.PreDefinedColumn6 = txtState__County.Value;
                            obj.PreDefinedColumn7 = txtPostal_Code.Value;
                            obj.PreDefinedColumn8 = ddlCountry.SelectedValue;
                            obj.PreDefinedColumn9 = txtPhone.Value;
                        }
                        else
                        {
                            obj.AddressTitle = !string.IsNullOrEmpty(txtAddressTitle.Value) ? txtAddressTitle.Value : "Default";
                            obj.PreDefinedColumn1 = txtDContact_Name.Value;
                            obj.PreDefinedColumn2 = txtDCompany_Name.Value; // Added by SHRIGANESH for Delivery Comapny Name 27 July 2016
                            obj.PreDefinedColumn3 = txtDAddress1.Value;
                            obj.PreDefinedColumn4 = txtDAddress2.Value;
                            obj.PreDefinedColumn5 = txtDTown.Value;
                            obj.PreDefinedColumn6 = txtDState__County.Value;
                            obj.PreDefinedColumn7 = txtDPostal_Code.Value;
                            obj.PreDefinedColumn8 = ddlDCountry.SelectedValue;
                            obj.PreDefinedColumn9 = txtDPhone.Value;
                        }

                        objBE.UserDeliveryAddress.Add(obj);
                        objBE.IPAddress = GlobalFunctions.GetIpAddress();
                        objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objBE.IsGuestUser = true;
                        objBE.LanguageId = Convert.ToInt16(intLanguageId);
                        objBE.IsMarketing = false;


                        i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                        objBE.UserId = Convert.ToInt16(i);
                        if (i > 0 || i == -2)
                        {
                            if (i > 0)
                            {
                                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                                if (objStoreBE.IsBASYS)
                                {
                                    for (int x = 0; x < objStoreBE.StoreCurrencies.Count(); x++)
                                    {
                                        if (objStoreBE.StoreCurrencies[x].IsActive)
                                        {
                                            InsertBASYSId(objStoreBE.StoreCurrencies[x].CurrencyId, Convert.ToString(Session["GuestUser"]).Trim(), objBE.UserId);
                                        }
                                    }
                                }
                            }

                            //Customer already registered as a guest user
                            UserBE objBEs = new UserBE();
                            UserBE objUser = new UserBE();
                            objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["GuestUser"]).Trim());
                            objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                            objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                            objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                            //Below if condition is added according to harley guestcheckout tax calculation July 28,2016
                            if (objUser != null || objUser.EmailId != null)
                            {
                                HttpContext.Current.Session["User"] = objUser;
                                Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                Session["GuestUser"] = null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                    #endregion
                    #endregion
                }
                UserBE objUserBE = new UserBE();
                UserBE.UserDeliveryAddressBE objUserDeliveryAddress = new UserBE.UserDeliveryAddressBE();
                if (Session["User"] != null)
                {
                    lstUser = Session["User"] as UserBE;
                    objUserBE.UserId = lstUser.UserId;
                    if (string.IsNullOrEmpty(lstUser.BASYS_CustomerContactId))
                    {
                        InsertBASYSId(GlobalFunctions.GetCurrencyId(), lstUser.EmailId, lstUser.UserId);
                        UserBE objBE = new UserBE();
                        objBE.EmailId = Sanitizer.GetSafeHtmlFragment(lstUser.EmailId);
                        objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());

                        UserBE objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBE);
                        Session["User"] = objUser;
                    }
                }

                //else
                //{
                //    Response.RedirectToRoute("login-page");
                //    return false;
                //}

                if (addressCheckbox.Checked)
                {
                    txtDContact_Name.Value = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                    txtDCompany_Name.Value = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim()); // Added by SHRIGANESH 27 July 2016 for Delivery Company Name
                    txtDAddress1.Value = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                    txtDAddress2.Value = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                    txtDTown.Value = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                    txtDState__County.Value = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                    txtDPostal_Code.Value = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                    ddlDCountry.SelectedValue = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                    txtDPhone.Value = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                    bStatus = true;
                }
                else
                {
                    #region "UserDeliveryAddress"
                    objUserDeliveryAddress.PreDefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtDContact_Name.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtDCompany_Name.Value.Trim()); // Added by SHRIGANESH 27 July 2016 for Delivery Company Name
                    objUserDeliveryAddress.PreDefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtDAddress1.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtDAddress2.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtDTown.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtDState__County.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtDPostal_Code.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlDCountry.SelectedValue.Trim());
                    objUserDeliveryAddress.PreDefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtDPhone.Value.Trim());
                    objUserDeliveryAddress.IsDefault = chkIsDefault.Checked;
                    objUserDeliveryAddress.AddressTitle = Sanitizer.GetSafeHtmlFragment(txtAddressTitle.Value);
                    //objUserBE.UserDeliveryAddress.Add(objUserDeliveryAddress);
                    DBAction type;

                    if (ddlDAddressTitle.SelectedIndex == 0)
                    {
                        type = DBAction.Insert;
                    }
                    else
                    {
                        type = DBAction.Update;
                        objUserDeliveryAddress.DeliveryAddressId = Convert.ToInt16(Sanitizer.GetSafeHtmlFragment(ddlDAddressTitle.SelectedValue));
                    }
                    objUserBE.UserDeliveryAddress.Add(objUserDeliveryAddress);
                    Int32 intDeliveryId = UserBL.ExecuteLoginDetails(objUserBE, type);
                    if (intDeliveryId > 0)
                    {
                        UserBE objUser = new UserBE();
                        UserBE objBEs = new UserBE();
                        objBEs.EmailId = lstUser.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                        }
                        hidDeliveryAddressId.Value = Convert.ToString(intDeliveryId);
                        bStatus = true;
                    }
                    #endregion
                }

                #region Update billing address in case of Guest user checkout
                lstUser = new UserBE();
                lstUser = Session["User"] as UserBE;
                if (lstUser.IsGuestUser == true)
                {
                    try
                    {
                        //Update billing address
                        UserBE objBE = new UserBE();
                        objBE.UserId = Convert.ToInt16(lstUser.UserId);
                        objBE.FirstName = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                        objBE.LastName = lstUser.LastName;
                        objBE.Password = lstUser.Password;
                        objBE.PredefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                        objBE.PredefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                        objBE.PredefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                        objBE.PredefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                        objBE.PredefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                        objBE.PredefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                        objBE.PredefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                        objBE.PredefinedColumn8 = ddlCountry.SelectedValue;
                        objBE.PredefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                        objBE.IPAddress = GlobalFunctions.GetIpAddress();
                        objBE.IsGuestUser = lstUser.IsGuestUser;
                        //int i = UserBL.ExecuteProfileDetails(Constants.USP_InsertUserProfileDetails, true, objBE);

                        UserBE objUser = new UserBE();
                        UserBE objBEs = new UserBE();
                        objBEs.EmailId = lstUser.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                        }
                        //Update billing address ends here                      
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                }
                #endregion

                #region "For SSO"
                if (Session["IsSSO"] != null)
                {
                    int iRes = UpdateRegistration();
                    UserBE objUser = new UserBE();
                    UserBE objBEs = new UserBE();
                    objBEs.EmailId = lstUser.EmailId;
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                    if (string.IsNullOrEmpty(objUser.BASYS_CustomerContactId))
                    {
                        //Need to Added condition
                        //StoreBE objStoreBE = StoreBL.GetStoreDetails();Commented by Sripal OnPageLoad()
                        if (objStoreBE.IsBASYS)
                        {
                            for (int x = 0; x < objStoreBE.StoreCurrencies.Count(); x++)
                            {
                                if (objStoreBE.StoreCurrencies[x].IsActive)
                                {
                                    InsertBASYSId(objStoreBE.StoreCurrencies[x].CurrencyId, lstUser.EmailId.Trim(), lstUser.UserId);
                                }
                            }
                        }

                        objBEs.EmailId = lstUser.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                        }
                    }
                    //else
                    //{
                    //    Session["User"] = objUser;
                    //}
                }
                #endregion
                return bStatus;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return bStatus;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 25-09-15
        /// Scope   : Calculate freight charges for standard and express zone
        /// </summary>            
        /// <returns></returns>
        protected static List<FreightManagementBE> CalculateFreight(Int16 intCountryId)
        {
            try
            {
                FreightManagementBE objFreight = new FreightManagementBE();
                objFreight.Weight = objFreight.WeightUnits = float.Parse(Convert.ToString(HttpContext.Current.Session["ProductWeight"]));
                objFreight.WeightPerBox = float.Parse(ConfigurationManager.AppSettings["WeightPerBox"]);
                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    objFreight.CountryCode = lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).CountryCode;
                    string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).RegionCode;

                    if (strRegionCode == "EU")
                        objFreight.FreightRegionName = "EU Region";
                    else if (strRegionCode == "UK")
                        objFreight.FreightRegionName = "UK Region";
                    else
                        objFreight.FreightRegionName = "ROW";
                }
                objFreight.LanguageId = GlobalFunctions.GetLanguageId();

                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    //string FM = Convert.ToString(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_FreightMultiplier").FeatureValues[0].FeatureDefaultValue);
                    //string CM = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(s => s.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyConversionFactor);

                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";
                    //float avarage = float.Parse("0.0", NumberStyles.Any, ci);

                    //objFreight.FreightMultiplier = float.Parse(FM, NumberStyles.Any, ci);




                    objFreight.FreightMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_FreightMultiplier").FeatureValues[0].FeatureDefaultValue), NumberStyles.Any, ci);
                    objFreight.CurrencyMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(s => s.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyConversionFactor), NumberStyles.Any, ci);



                    //objFreight.FreightMultiplier = float.Parse(FM.ToString(CultureInfo.InvariantCulture));
                    //objFreight.CurrencyMultiplier = CM.ToString("##,###,##0.#0");
                    //  objFreight.FreightMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_FreightMultiplier").FeatureValues[0].FeatureDefaultValue));
                    //  objFreight.CurrencyMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(s => s.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyConversionFactor));
                }

                List<FreightManagementBE.FreightMultiplerCurrency> lstAllFreightMultiplerCurrency;
                lstAllFreightMultiplerCurrency = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(0, "CM");
                if (lstAllFreightMultiplerCurrency != null)
                {
                    if (lstAllFreightMultiplerCurrency.Count > 0)
                    {
                        decimal fFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.CurrencyID == GlobalFunctions.GetCurrencyId()).CValue;
                        objFreight.FreightMultiplier = float.Parse(Convert.ToString(fFreightMultiplier));
                    }
                }

                List<object> lstShoppingBE = new List<object>();
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                double OrderItemTotal = 0;
                if (lstUser != null && lstUser.UserId != 0)
                {
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                }
                else
                {
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                }
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                    lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    for (int i = 0; i < lstShoppingBE.Count; i++)
                    {
                        double OrderItemAddTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                        //if (DiscountPercentage == 0)
                        OrderItemTotal += Convert.ToDouble(Convert.ToDecimal(OrderItemAddTotal));
                        //else
                        //    OrderItemTotal = GlobalFunctions.DiscountedAmount(OrderItemTotal, DiscountPercentage);
                    }
                }


                objFreight.OrderValue = OrderItemTotal;
                List<FreightManagementBE> lstFreight = new List<FreightManagementBE>();
                lstFreight = FreightManagementBL.GetFreightDetails(objFreight);

                FreightManagementBE CurrFreightConfig = new FreightManagementBE();

                return lstFreight;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 24-09-15
        /// Scope   : Calculate freight charges for standard and express zone
        /// </summary>            
        /// <returns></returns>
        protected void SetFreightValues()
        {
            try
            {
                HtmlGenericControl spnShippingPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnShippingPrice");
                spnShippingPrice.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                HtmlGenericControl spnTaxPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTaxPrice");

                if (IsPointsEnbled)
                {
                    spnShippingPrice.InnerHtml = "0";
                    spnTaxPrice.InnerHtml = "0";
                    return;
                }
                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                lstFreightBE = CalculateFreight(Convert.ToInt16(ddlDCountry.SelectedValue));

                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    if (!string.Equals(ddlDCountry.SelectedValue, "0"))
                    {
                        if (lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(ddlDCountry.SelectedValue)).DisplayDutyMessage)
                        { divDutyMessage.Attributes.Add("style", "display:''"); }
                    }
                    //if (string.Equals ddlDCountry.SelectedValue)

                }

                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    //hidStandard.Value = lstFreightBE[0].SZPrice;
                    //hidExpress.Value = lstFreightBE[0].EZPrice;
                    hidStandard.Value = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                    hidExpress.Value = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                    spnStandardText.InnerHtml = lstFreightBE[0].StandardMethodName + " (" + lstFreightBE[0].SZTransitTime + ")";
                    spnExpressText.InnerHtml = lstFreightBE[0].ExpressMethodName + " (" + lstFreightBE[0].EZTransitTime + ")";
                    if (lstFreightBE[0].IsStandard)
                    {
                        divStandardCont.Attributes.Add("style", "display:''");
                    }
                    else
                    {
                        divStandardCont.Attributes.Add("style", "display:none");
                    }
                    if (lstFreightBE[0].IsExpress)
                    {
                        divExpressCont.Attributes.Add("style", "display:''");
                    }
                    else
                    {
                        divExpressCont.Attributes.Add("style", "display:none");
                    }
                    //divStandardCont.Attributes.Add("style", "display:''");
                    //divExpressCont.Attributes.Add("style", "display:''");

                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        spnstandard.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstFreightBE[0].SZPrice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                        spnexpress.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstFreightBE[0].EZPrice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                    }
                    else
                    {
                        //spnstandard.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].SZPrice));// +GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                        //spnexpress.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].EZPrice));// +GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());

                        //GlobalFunctions.ConvertToDecimalPrecision Removed due to multi Language currency (example: 0,6300.00)
                        if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                        {
                            spnstandard.InnerHtml = Convert.ToDecimal(lstFreightBE[0].SZPrice).ToString("##,###,##0.#0");
                        }
                        else { spnstandard.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0"); }
                        if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                        {
                            spnexpress.InnerHtml = Convert.ToDecimal(lstFreightBE[0].EZPrice).ToString("##,###,##0.#0");
                        }
                        else
                        {
                            spnexpress.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                        }
                    }

                    //if (Convert.ToDouble(lstFreightBE[0].SZPrice) == 0)
                    //{
                    //    rbStandard.Disabled = true;
                    //}
                    //else
                    //{
                    if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                    {
                        if (Convert.ToDouble(lstFreightBE[0].SZPrice) >= 0)
                        {
                            rbStandard.Disabled = false;
                            rbStandard.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].SZPrice));
                        }
                    }
                    //if (Convert.ToDouble(lstFreightBE[0].EZPrice) == 0)
                    //{
                    //    rbExpress.Disabled = true;
                    //}
                    //else
                    if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                    {
                        if (Convert.ToDouble(lstFreightBE[0].EZPrice) >= 0)
                        {
                            rbExpress.Disabled = false;
                            if (!rbStandard.Checked)
                            {
                                rbExpress.Checked = true;
                                spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].EZPrice));
                            }
                        }
                    }

                    if (!(lstCustomerOrders == null || (lstCustomerOrders.StandardCharges == 0 && lstCustomerOrders.ExpressCharges == 0)))
                    {
                        if (lstCustomerOrders.StandardCharges != 0)
                        {
                            rbStandard.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.StandardCharges));
                        }
                        else
                        {
                            rbExpress.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.ExpressCharges));
                        }
                        spnTaxPrice.InnerHtml = Convert.ToString(lstCustomerOrders.TotalTax);
                    }
                    spnTaxPrice.InnerHtml = "0";
                    #region corrected by vikram to get proper tax start
                    // string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue),lstFreightBE[0].SZPrice, lstFreightBE[0].EZPrice, 0);
                    decimal dSZPrice = 0, dEZPrice = 0;
                    if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                    {
                        dSZPrice = Convert.ToDecimal(lstFreightBE[0].SZPrice);
                    }
                    if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                    {
                        dEZPrice = Convert.ToDecimal(lstFreightBE[0].EZPrice);
                    }
                    string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), dSZPrice.ToString(CultureInfo.InvariantCulture), dEZPrice.ToString(CultureInfo.InvariantCulture), 0);
                    //string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), Convert.ToDecimal(lstFreightBE[0].SZPrice).ToString(CultureInfo.InvariantCulture), Convert.ToDecimal(lstFreightBE[0].EZPrice).ToString(CultureInfo.InvariantCulture), 0);
                    #endregion end
                    if (!string.IsNullOrEmpty(strTotalTax))
                    {
                        string[] strTax = strTotalTax.Split('|');
                        if (strTax.Length > 0)
                        {

                            if (GlobalFunctions.IsPointsEnbled())
                            {
                                hidStandardTax.Value = GlobalFunctions.DisplayPriceOrPoints(strTax[0], GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                                hidExpressTax.Value = GlobalFunctions.DisplayPriceOrPoints(strTax[1], GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                            }
                            else
                            {
                                hidStandardTax.Value = strTax[0];
                                hidExpressTax.Value = strTax[1];
                            }
                            if (rbStandard.Checked)
                            {
                                //spnTaxPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[0]));
                                spnTaxPrice.InnerHtml = Convert.ToDecimal(strTax[0]).ToString("##,###,##0.#0");
                            }
                            else
                            {
                                //spnTaxPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[1]));
                                spnTaxPrice.InnerHtml = Convert.ToDecimal(strTax[1]).ToString("##,###,##0.#0");
                            }
                        }
                    }
                    if (!(lstCustomerOrders == null || lstCustomerOrders.TotalTax == 0))
                    {
                        spnTaxPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.TotalTax));
                    }

                    #region Show when the error msg appears.
                    divStandardCont.Visible = true;
                    divExpressCont.Visible = true;
                    #endregion
                    if (rbStandard.Checked && lstFreightBE[0].SZDisallowOrder)
                    {
                        divMOVMessage.Attributes.Add("style", "display:''");
                        #region Hide when the error msg appears.
                        divStandardCont.Visible = false;
                        divExpressCont.Visible = false;
                        #endregion
                        GlobalFunctions.ShowModalAlertMessages(this.Page, divMOVMessage.InnerHtml, AlertType.Warning);
                    }
                    else if (lstFreightBE[0].EZDisallowOrder)
                    {
                        divMOVMessage.Attributes.Add("style", "display:''");
                        #region Hide when the error msg appears.
                        divStandardCont.Visible = false;
                        divExpressCont.Visible = false;
                        #endregion
                        GlobalFunctions.ShowModalAlertMessages(this.Page, divMOVMessage.InnerHtml, AlertType.Warning);
                    }
                }
                else
                {
                    divExpressCont.Attributes.Add("style", "display:none");
                    divStandardCont.Attributes.Add("style", "display:none");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 25-09-15
        /// Scope   : CalculateTax from BASyS
        /// </summary>            
        /// <returns></returns>
        protected static string CalculateTax(Int16 intDeliveryCountryId, string strStandardFreightCharges, string strExpressFreightCharges, double DiscountPercentage)
        {
            double dStandardTotalTax = 0;
            double dExpressTotalTax = 0;
            try
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                UserBE lstUser = new UserBE();
                string strCustomerContactId = string.Empty;
                if (objStoreBE != null && objStoreBE.IsBASYS)
                {

                    if (HttpContext.Current.Session["User"] != null)
                    {
                        lstUser = HttpContext.Current.Session["User"] as UserBE;
                        strCustomerContactId = lstUser.BASYS_CustomerContactId;
                    }
                    else
                    {
                        if (HttpContext.Current.Session["GuestUser"] == null)
                            return "";
                        else
                        {
                            StoreBE.StoreCurrencyBE objCurrency = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId());
                            strCustomerContactId = Convert.ToString(objCurrency.Default_CUST_CONTACT_ID);
                        }
                    }
                    if (string.IsNullOrEmpty(strCustomerContactId))
                    {
                        StoreBE.StoreCurrencyBE objCurrency = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId());
                        strCustomerContactId = Convert.ToString(objCurrency.Default_CUST_CONTACT_ID);
                    }

                    string strCountryCode = string.Empty;
                    string strCountryName = string.Empty;
                    Int32 intCustomerOrderId = Convert.ToInt32(HttpContext.Current.Session["CustomerOrderId"]);
                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        strCountryCode = lstCountry.FirstOrDefault(x => x.CountryId == intDeliveryCountryId).CountryCode;
                        strCountryName = lstCountry.FirstOrDefault(x => x.CountryId == intDeliveryCountryId).CountryName;
                    }

                    Webservice_OASIS wsTax = new Webservice_OASIS();
                    DataTable dtTax = new DataTable();
                    ArrayList arrProducts = new ArrayList();

                    List<object> lstShoppingBE = new List<object>();
                    if (HttpContext.Current.Session["GuestUser"] == null)
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                    else
                    {
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                    }
                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                        lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();

                        double OrderItemTotal = 0.0;
                        string strBASYSProductId = string.Empty;
                        for (int i = 0; i < lstShoppingBE.Count; i++)
                        {
                            strBASYSProductId = Convert.ToString(lstNewShopping[i].BASYSProductId);
                            arrProducts.Add(strBASYSProductId);
                            double price = DiscountPercentage > 0 ?
                                            Convert.ToDouble(lstNewShopping[i].Price) - (Convert.ToDouble(lstNewShopping[i].Price) * DiscountPercentage * 0.01) : Convert.ToDouble(lstNewShopping[i].Price);
                            OrderItemTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * (price);
                            #region corrected by vikram
                            //  OrderItemTotal = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(OrderItemTotal)));

                            #endregion
                            //if (DiscountPercentage == 0)

                            //arrProducts.Add(Convert.ToString(OrderItemTotal));
                            arrProducts.Add(OrderItemTotal.ToString(CultureInfo.InvariantCulture));
                            // arrProducts.Add(OrderItemTotal.ToString("##,###,##0.#0"));

                            //else
                            //    arrProducts.Add(Convert.ToString(GlobalFunctions.DiscountedAmount(OrderItemTotal, DiscountPercentage)));
                            arrProducts.Add("");
                        }

                        //Calculate tax for standard freight charges .ToString("#######0.#0")

                        //dtTax = wsTax.getTaxDetails(strCustomerContactId, "", HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strStandardFreightCharges, CultureInfo.InvariantCulture).ToString(), arrProducts, intCustomerOrderId);
                        dtTax = wsTax.getTaxDetails(strCustomerContactId, "", HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strStandardFreightCharges, CultureInfo.InvariantCulture).ToString("##,###,##0.#0").Replace(',', '.'), arrProducts, intCustomerOrderId);
                        			 

                        if (dtTax != null)
                        {
                            if (dtTax.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtTax.Rows.Count; j++)
                                {
                                    dStandardTotalTax += Convert.ToDouble(dtTax.Rows[j]["TotalSalesTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                                }
                                dStandardTotalTax += Convert.ToDouble(dtTax.Rows[0]["FreightTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                            }
                        }

                        //Calculate tax for express freight charges .ToString("#######0.#0")
                        dtTax = null;
                        //dtTax = wsTax.getTaxDetails(strCustomerContactId, "", HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strExpressFreightCharges, CultureInfo.InvariantCulture).ToString(), arrProducts, intCustomerOrderId);
                        dtTax = wsTax.getTaxDetails(strCustomerContactId, "", HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strExpressFreightCharges, CultureInfo.InvariantCulture).ToString("##,###,##0.#0").Replace(',', '.'), arrProducts, intCustomerOrderId);

                        if (dtTax != null)
                        {
                            if (dtTax.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtTax.Rows.Count; j++)
                                {
                                    dExpressTotalTax += Convert.ToDouble(dtTax.Rows[j]["TotalSalesTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                                }
                                dExpressTotalTax += Convert.ToDouble(dtTax.Rows[0]["FreightTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                            }
                        }
                        HttpContext.Current.Session["TaxDetails"] = dtTax;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
            return dStandardTotalTax.ToString("##,###,##0.#0") + "|" + dExpressTotalTax.ToString("##,###,##0.#0");
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 14-10-15
        /// Scope   : ApplyCoupon of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="CouponCode"></param>        
        /// <param name="ShipmentType"></param>        
        /// <param name="CountryId"></param> 
        /// <returns>json data</returns>
        [System.Web.Services.WebMethod]
        public static string ApplyCoupon_bkp(string CouponCode, string ShipmentType, Int16 CountryId)
        {
            try
            {
                string strMessage = string.Empty;
                double OrderItemTotal = 0;
                double OrderValueAfterDiscount = 0;
                string StandardFreightValueAfterDiscount = "0";
                string ExpressFreightValueAfterDiscount = "0";
                string StandardTaxValueAfterDiscount = "0";
                string ExpressTaxValueAfterDiscount = "0";
                string strShipmentType = "0";
                string strBehaviourType = string.Empty;
                List<object> lstShoppingBE = new List<object>();
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                if (lstUser != null && lstUser.UserId != 0)
                {
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                        lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                        for (int i = 0; i < lstShoppingBE.Count; i++)
                        {
                            //OrderItemTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                            //OrderItemTotal = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(OrderItemTotal)));

                            //Added by Vikram
                            double ProductTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                            ProductTotal = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ProductTotal)));
                            OrderItemTotal = OrderItemTotal + ProductTotal;
                        }
                    }

                    CouponBE objCouponBE = new CouponBE();
                    List<CouponBE> lstCouponBE = new List<CouponBE>();
                    objCouponBE.OrderValue = float.Parse(Convert.ToString(OrderItemTotal));
                    objCouponBE.CouponCode = CouponCode.Trim();

                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == CountryId).RegionCode;
                        objCouponBE.Region = strRegionCode == "ROW" ? "Other" : strRegionCode;
                    }
                    objCouponBE.ShipmentType = ShipmentType; // 1 for Standard, 2 for express shipping method
                    lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, lstUser, GlobalFunctions.GetCurrencyId());
                    if (lstCouponBE != null && lstCouponBE.Count > 0)
                    {
                        if (lstCouponBE[0].IsActive == false)
                        {
                            strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                            //return strMessage;
                        }
                        else // vikram
                            if (lstCouponBE[0].IsExpired == true && string.IsNullOrEmpty(strMessage))
                            {
                                //strMessage = "Coupon code has been expired.";
                                strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                                //return strMessage;
                            }
                            else // vikram
                                if (lstCouponBE[0].IsValidOrderValue == false && string.IsNullOrEmpty(strMessage))
                                {
                                    if (lstCouponBE[0].MinimumOrderValue == 0)
                                    {
                                        strSC_Coupon_Code_Maximum_Order_Value = strSC_Coupon_Code_Maximum_Order_Value.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                                        strMessage = strSC_Coupon_Code_Maximum_Order_Value;
                                        //strMessage = "Coupon code is valid only for maximum order value " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue;
                                    }
                                    else if (lstCouponBE[0].MaximumOrderValue == 0)
                                    {
                                        strSC_Coupon_Code_Minimum_Order_Value = strSC_Coupon_Code_Minimum_Order_Value.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                                        strMessage = strSC_Coupon_Code_Minimum_Order_Value;
                                        //strMessage = "Coupon code is valid only for minimum order value " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue;
                                    }
                                    else
                                    {
                                        strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                                        strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                                        strMessage = strSC_Coupon_Code_Minimum_Maximum_Range;// "Coupon code is valid only for order value between " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue + " and " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue;
                                        //return strMessage;
                                    }
                                }
                                else // vikram
                                    if (lstCouponBE[0].IsValidRegion == false && string.IsNullOrEmpty(strMessage))
                                    {
                                        strMessage = strSC_Invalid_Coupon_Delivery_Country;// "Coupon code is not valid for the selected delivery country.";
                                        //return strMessage;
                                    }
                                    else // vikram
                                        if (lstCouponBE[0].IsValidShipmentType == false && string.IsNullOrEmpty(strMessage))
                                        {
                                            strMessage = strSC_Invalid_Coupon_Shipping_Method;// "Coupon code is not valid for the selected shipping method.";
                                            //return strMessage;
                                        }

                        if (string.IsNullOrEmpty(strMessage))
                        {
                            strBehaviourType = lstCouponBE[0].BehaviourType;
                            //Apply to goods only, excluding Freight Value 
                            if (lstCouponBE[0].BehaviourType.ToLower() == "product")
                            {
                                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                                lstFreightBE = CalculateFreight(CountryId);

                                if (lstFreightBE != null && lstFreightBE.Count > 0)
                                {
                                    StandardFreightValueAfterDiscount = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0"; //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0"; //Total tax for express zone

                                    string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));

                                    if (!string.IsNullOrEmpty(strTotalTax))
                                    {
                                        string[] strTax = strTotalTax.Split('|');
                                        if (strTax.Length > 0)
                                        {
                                            StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                            ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                        }
                                    }
                                }
                                OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                                strMessage = "Success";
                                //return strMessage;
                            }
                            //Apply to goods only, excluding Freight Value 
                            else if (lstCouponBE[0].BehaviourType.ToLower() == "basket")
                            {
                                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                                lstFreightBE = CalculateFreight(CountryId);

                                if (lstFreightBE != null && lstFreightBE.Count > 0)
                                {
                                    string saSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                    string saEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(saSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(saEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone

                                    string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));

                                    if (!string.IsNullOrEmpty(strTotalTax))
                                    {
                                        string[] strTax = strTotalTax.Split('|');
                                        if (strTax.Length > 0)
                                        {
                                            StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                            ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                        }
                                    }
                                }
                                OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                                strMessage = "Success";
                                //return strMessage;
                            }
                            //Free shipping excluding Freight Value
                            else if (lstCouponBE[0].BehaviourType.ToLower() == "freeshipping")
                            {
                                StandardFreightValueAfterDiscount = "0"; //Total tax for standard zone
                                ExpressFreightValueAfterDiscount = "0"; //Total tax for express zone
                                string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, 0);
                                if (!string.IsNullOrEmpty(strTotalTax))
                                {
                                    string[] strTax = strTotalTax.Split('|');
                                    if (strTax.Length > 0)
                                    {
                                        StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                        ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                    }
                                }

                                OrderValueAfterDiscount = OrderItemTotal;
                                strMessage = "Success";
                            }
                            //Discounted Shipping
                            else if (lstCouponBE[0].BehaviourType.ToLower() == "shipping")
                            {
                                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                                lstFreightBE = CalculateFreight(CountryId);

                                if (lstFreightBE != null && lstFreightBE.Count > 0)
                                {
                                    string sbSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                    string sbEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sbSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sbEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone

                                    string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, 0);

                                    if (!string.IsNullOrEmpty(strTotalTax))
                                    {
                                        string[] strTax = strTotalTax.Split('|');
                                        if (strTax.Length > 0)
                                        {
                                            StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                            ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                        }
                                    }
                                }
                                OrderValueAfterDiscount = OrderItemTotal;
                                strMessage = strSuccess;// "Success";
                            }
                        }
                        strShipmentType = lstCouponBE[0].ShipmentType;
                    }
                    else
                    {
                        strMessage = strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
                    }
                }
                else
                {
                    strMessage = strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
                }
                return "[{\"Message\":\"" + strMessage + "\",\"OrderValueAfterDiscount\":\"" + OrderValueAfterDiscount + "\",\"StandardFreightValueAfterDiscount\":\"" +
                    StandardFreightValueAfterDiscount + "\",\"ExpressFreightValueAfterDiscount\":\"" + ExpressFreightValueAfterDiscount +
                    "\",\"StandardTaxValueAfterDiscount\":\"" + StandardTaxValueAfterDiscount + "\",\"ExpressTaxValueAfterDiscount\":\"" +
                    ExpressTaxValueAfterDiscount + "\",\"BehaviourType\":\"" + strBehaviourType.ToLower() + "\",\"ShipmentType\":\"" + strShipmentType.ToLower() + "\"}]";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 14-10-15
        /// Scope   : ApplyCoupon of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="CouponCode"></param>        
        /// <param name="ShipmentType"></param>        
        /// <param name="CountryId"></param> 
        /// <returns>json data</returns>
        [System.Web.Services.WebMethod]
        public static string ApplyCoupon(string CouponCode, string ShipmentType, Int16 CountryId)
        {
            try
            {
                return CalculateCoupon(CouponCode, ShipmentType, CountryId);
                #region
                //string CouponShipmnet = "";
                //string strMessage = string.Empty;
                //double OrderItemTotal = 0;
                //double OrderValueAfterDiscount = 0;
                //string strShipmentType = "0";
                //string StandardFreightValueAfterDiscount = "0";
                //string ExpressFreightValueAfterDiscount = "0";
                //string StandardTaxValueAfterDiscount = "0";
                //string ExpressTaxValueAfterDiscount = "0";
                //string strBehaviourType = string.Empty;
                //List<object> lstShoppingBE = new List<object>();
                //UserBE lstUser = new UserBE();
                //lstUser = HttpContext.Current.Session["User"] as UserBE;
                //// if (lstUser != null && lstUser.UserId != 0)
                //// {
                //// lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);

                //if (lstUser != null && lstUser.UserId != 0)
                //{
                //    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                //}
                //else
                //{
                //    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                //}


                //if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                //{
                //    List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                //    lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                //    for (int i = 0; i < lstShoppingBE.Count; i++)
                //    {
                //        //OrderItemTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                //        //OrderItemTotal = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(OrderItemTotal)));

                //        //Added by Vikram
                //        double ProductTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                //        ProductTotal = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ProductTotal)));
                //        OrderItemTotal = OrderItemTotal + ProductTotal;
                //    }
                //}

                //CouponBE objCouponBE = new CouponBE();
                //List<CouponBE> lstCouponBE = new List<CouponBE>();
                //objCouponBE.OrderValue = float.Parse(Convert.ToString(OrderItemTotal));
                //objCouponBE.CouponCode = CouponCode.Trim();

                //List<CountryBE> lstCountry = new List<CountryBE>();
                //lstCountry = CountryBL.GetAllCountries();
                //if (lstCountry != null && lstCountry.Count > 0)
                //{
                //    string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == CountryId).RegionCode;
                //    objCouponBE.Region = strRegionCode == "ROW" ? "Other" : strRegionCode;
                //}
                //objCouponBE.ShipmentType = ShipmentType; // 1 for Standard, 2 for express shipping method

                //if (lstUser != null && lstUser.UserId != 0)
                //{
                //    lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, lstUser, GlobalFunctions.GetCurrencyId());
                //}
                //else
                //{
                //    UserBE objUserBE = new UserBE();
                //    objUserBE.UserId = 0;
                //    objUserBE.EmailId = Convert.ToString(HttpContext.Current.Session["GuestUser"]);
                //    lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, objUserBE, GlobalFunctions.GetCurrencyId());
                //}
                //if (lstCouponBE != null && lstCouponBE.Count > 0)
                //{
                //    #region validate coupon
                //    if (lstCouponBE[0].IsActive == false)
                //    {
                //        strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                //        //return strMessage;
                //    }
                //    else if (lstCouponBE[0].IsExpired == true && string.IsNullOrEmpty(strMessage))
                //    {
                //        //strMessage = "Coupon code has been expired.";
                //        strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                //        //return strMessage;
                //    }
                //    else if (lstCouponBE[0].IsValidOrderValue == false && string.IsNullOrEmpty(strMessage))
                //    {
                //        if (lstCouponBE[0].MinimumOrderValue == 0)
                //        {
                //            strSC_Coupon_Code_Maximum_Order_Value = strSC_Coupon_Code_Maximum_Order_Value.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                //            strMessage = strSC_Coupon_Code_Maximum_Order_Value;
                //            //strMessage = "Coupon code is valid only for maximum order value " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue;
                //        }
                //        else if (lstCouponBE[0].MaximumOrderValue == 0)
                //        {
                //            strSC_Coupon_Code_Minimum_Order_Value = strSC_Coupon_Code_Minimum_Order_Value.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                //            strMessage = strSC_Coupon_Code_Minimum_Order_Value;
                //            //strMessage = "Coupon code is valid only for minimum order value " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue;
                //        }
                //        else
                //        {
                //            strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                //            strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                //            strMessage = strSC_Coupon_Code_Minimum_Maximum_Range;// "Coupon code is valid only for order value between " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue + " and " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue;
                //            //return strMessage;
                //        }
                //    }
                //    else if (lstCouponBE[0].IsValidRegion == false && string.IsNullOrEmpty(strMessage))
                //    {
                //        strMessage = strSC_Invalid_Coupon_Delivery_Country;// "Coupon code is not valid for the selected delivery country.";
                //        //return strMessage;
                //    }
                //    else if (lstCouponBE[0].IsValidShipmentType == false && string.IsNullOrEmpty(strMessage))
                //    {
                //        strMessage = strSC_Invalid_Coupon_Shipping_Method;// "Coupon code is not valid for the selected shipping method.";
                //        //return strMessage;
                //    }
                //    #endregion

                //    if (string.IsNullOrEmpty(strMessage))
                //    {
                //        strBehaviourType = lstCouponBE[0].BehaviourType;

                //        strShipmentType = lstCouponBE[0].ShipmentType;

                //        double discountPercentage = Convert.ToDouble(lstCouponBE[0].DiscountPercentage);
                //        //CouponShipmnet = lstCouponBE[0].ShipmentType;
                //        bool isvalid = false;
                //        #region      //Apply to goods only, excluding Freight Value
                //        if (lstCouponBE[0].BehaviourType.ToLower() == "product")
                //        {
                //            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                //            lstFreightBE = CalculateFreight(CountryId);
                //            isvalid = true;
                //            if (lstFreightBE != null && lstFreightBE.Count > 0)
                //            {
                //                StandardFreightValueAfterDiscount = Convert.ToString(lstFreightBE[0].SZPrice); //Total tax for standard zone
                //                ExpressFreightValueAfterDiscount = Convert.ToString(lstFreightBE[0].EZPrice); //Total tax for express zone

                //            }
                //            OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                //            strMessage = "Success";

                //            //return strMessage;
                //        }
                //        #endregion
                //        #region   //Apply to goods only, excluding Freight Value
                //        else if (lstCouponBE[0].BehaviourType.ToLower() == "basket")
                //        {
                //            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                //            lstFreightBE = CalculateFreight(CountryId);
                //            isvalid = true;
                //            if (lstFreightBE != null && lstFreightBE.Count > 0)
                //            {
                //                if (strShipmentType.Equals("1"))
                //                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //                if (strShipmentType.Equals("2"))
                //                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //                if (strShipmentType.Equals("0"))
                //                {
                //                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //                }

                //            }
                //            OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                //            strMessage = "Success";
                //            //return strMessage;
                //        }
                //        #endregion
                //        #region //Free shipping excluding Freight Value
                //        else if (lstCouponBE[0].BehaviourType.ToLower() == "freeshipping")
                //        {
                //            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                //            lstFreightBE = CalculateFreight(CountryId);
                //            isvalid = true;
                //            /*if (CouponShipmnet == "1")
                //            {
                //                StandardFreightValueAfterDiscount = "0"; //Total tax for standard zone
                //                ExpressFreightValueAfterDiscount = lstFreightBE[0].EZPrice;
                //            }
                //            else if (CouponShipmnet == "2")
                //            {
                //                ExpressFreightValueAfterDiscount = "0"; //Total tax for express zone
                //                StandardFreightValueAfterDiscount = lstFreightBE[0].SZPrice;
                //            }
                //            else
                //            {
                //                StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //                ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //            }*/
                //            if (strShipmentType.Equals("1"))
                //                StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //            if (strShipmentType.Equals("2"))
                //                ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //            if (strShipmentType.Equals("0"))
                //            {
                //                StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //                ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //            }
                //            OrderValueAfterDiscount = OrderItemTotal;
                //            discountPercentage = 0.0;
                //            strMessage = "Success";
                //        }
                //        #endregion
                //        #region  //Discounted Shipping
                //        else if (lstCouponBE[0].BehaviourType.ToLower() == "shipping")
                //        {
                //            isvalid = true;
                //            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                //            lstFreightBE = CalculateFreight(CountryId);

                //            if (lstFreightBE != null && lstFreightBE.Count > 0)
                //            {
                //                /*if (CouponShipmnet == "1")
                //                {
                //                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //                    ExpressFreightValueAfterDiscount = lstFreightBE[0].EZPrice;
                //                }
                //                else if (CouponShipmnet == "2")
                //                {
                //                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //                    StandardFreightValueAfterDiscount = lstFreightBE[0].SZPrice;
                //                }
                //                else
                //                {
                //                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //                }*/
                //                if (strShipmentType.Equals("1"))
                //                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //                if (strShipmentType.Equals("2"))
                //                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //                if (strShipmentType.Equals("0"))
                //                {
                //                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                //                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                //                }
                //                discountPercentage = 0.0;

                //            }
                //            OrderValueAfterDiscount = OrderItemTotal;
                //            strMessage = strSuccess;// "Success";
                //        }
                //        #endregion
                //        #region Tax Calculation after discount
                //        if (isvalid)
                //        {
                //            string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, discountPercentage);
                //            HttpContext.Current.Session["discountPercentage"] = discountPercentage;
                //            if (!string.IsNullOrEmpty(strTotalTax))
                //            {
                //                string[] strTax = strTotalTax.Split('|');
                //                if (strTax.Length > 0)
                //                {
                //                    StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                //                    ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                //                }
                //            }
                //        }
                //        #endregion
                //    }
                //}
                //else
                //{
                //    strMessage = strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
                //}
                //// }
                //// else
                //// {
                ////    strMessage = strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
                ////}
                //return "[{\"Message\":\"" + strMessage + "\",\"OrderValueAfterDiscount\":\"" + OrderValueAfterDiscount + "\",\"StandardFreightValueAfterDiscount\":\"" +
                //    StandardFreightValueAfterDiscount + "\",\"ExpressFreightValueAfterDiscount\":\"" + ExpressFreightValueAfterDiscount +
                //    "\",\"StandardTaxValueAfterDiscount\":\"" + StandardTaxValueAfterDiscount + "\",\"ExpressTaxValueAfterDiscount\":\"" +
                //    ExpressTaxValueAfterDiscount + "\",\"BehaviourType\":\"" + strBehaviourType.ToLower() + "\",\"ShipmentType\":\"" + strShipmentType.ToLower() + "\"}]"; 
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
            }
        }

        private static string CalculateCoupon(string CouponCode, string ShipmentType, Int16 CountryId)
        {
            string strMessage = string.Empty;
            double OrderItemTotal = 0;
            double OrderValueAfterDiscount = 0;
            string strShipmentType = "0";
            string StandardFreightValueAfterDiscount = "0";
            string ExpressFreightValueAfterDiscount = "0";
            string StandardTaxValueAfterDiscount = "0";
            string ExpressTaxValueAfterDiscount = "0";
            string strBehaviourType = string.Empty;
            List<object> lstShoppingBE = new List<object>();
            UserBE lstUser = new UserBE();

            try
            {
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                if (lstUser != null && lstUser.UserId != 0)
                {
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                }
                else
                {
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                }
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                    lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    for (int i = 0; i < lstShoppingBE.Count; i++)
                    {
                        double ProductTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                        ProductTotal = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ProductTotal)));
                        OrderItemTotal = OrderItemTotal + ProductTotal;
                    }
                }
                CouponBE objCouponBE = new CouponBE();
                List<CouponBE> lstCouponBE = new List<CouponBE>();
                objCouponBE.OrderValue = float.Parse(Convert.ToString(OrderItemTotal));
                objCouponBE.CouponCode = CouponCode.Trim();

                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == CountryId).RegionCode;
                    objCouponBE.Region = strRegionCode == "ROW" ? "Other" : strRegionCode;
                }
                objCouponBE.ShipmentType = ShipmentType; // 1 for Standard, 2 for express shipping method

                if (lstUser != null && lstUser.UserId != 0)
                {
                    lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, lstUser, GlobalFunctions.GetCurrencyId());
                }
                else
                {
                    UserBE objUserBE = new UserBE();
                    objUserBE.UserId = 0;
                    objUserBE.EmailId = Convert.ToString(HttpContext.Current.Session["GuestUser"]);
                    lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, objUserBE, GlobalFunctions.GetCurrencyId());
                }
                if (lstCouponBE != null && lstCouponBE.Count > 0)
                {
                    #region validate coupon
                    if (lstCouponBE[0].IsActive == false)
                    {
                        strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                    }
                    else if (lstCouponBE[0].IsExpired == true && string.IsNullOrEmpty(strMessage))
                    {
                        strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                    }
                    else if (lstCouponBE[0].IsValidOrderValue == false && string.IsNullOrEmpty(strMessage))
                    {
                        if (lstCouponBE[0].MinimumOrderValue == 0)
                        {
                            strSC_Coupon_Code_Maximum_Order_Value = strSC_Coupon_Code_Maximum_Order_Value.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                            strMessage = strSC_Coupon_Code_Maximum_Order_Value;
                        }
                        else if (lstCouponBE[0].MaximumOrderValue == 0)
                        {
                            strSC_Coupon_Code_Minimum_Order_Value = strSC_Coupon_Code_Minimum_Order_Value.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                            strMessage = strSC_Coupon_Code_Minimum_Order_Value;
                        }
                        else
                        {
                            strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                            strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                            strMessage = strSC_Coupon_Code_Minimum_Maximum_Range;// "Coupon code is valid only for order value between " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue + " and " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue;
                        }
                    }
                    else if (lstCouponBE[0].IsValidRegion == false && string.IsNullOrEmpty(strMessage))
                    {
                        strMessage = strSC_Invalid_Coupon_Delivery_Country;// "Coupon code is not valid for the selected delivery country.";
                    }
                    else if (lstCouponBE[0].IsValidShipmentType == false && string.IsNullOrEmpty(strMessage))
                    {
                        strMessage = strSC_Invalid_Coupon_Shipping_Method;// "Coupon code is not valid for the selected shipping method.";
                    }
                    #endregion

                    if (string.IsNullOrEmpty(strMessage))
                    {
                        strBehaviourType = lstCouponBE[0].BehaviourType;
                        strShipmentType = lstCouponBE[0].ShipmentType;

                        double discountPercentage = Convert.ToDouble(lstCouponBE[0].DiscountPercentage);
                        bool isvalid = false;
                        #region      //Apply to goods only, excluding Freight Value
                        if (lstCouponBE[0].BehaviourType.ToLower() == "product")
                        {
                            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                            lstFreightBE = CalculateFreight(CountryId);
                            isvalid = true;
                            if (lstFreightBE != null && lstFreightBE.Count > 0)
                            {
                                string scSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                string scEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                                //StandardFreightValueAfterDiscount = Convert.ToString(lstFreightBE[0].SZPrice); //Total tax for standard zone
                                //ExpressFreightValueAfterDiscount = Convert.ToString(lstFreightBE[0].EZPrice); //Total tax for express zone
                                StandardFreightValueAfterDiscount = Convert.ToString(scSZPrice); //Total tax for standard zone
                                ExpressFreightValueAfterDiscount = Convert.ToString(scEZPrice); //Total tax for express zone
                            }
                            OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                            strMessage = "Success";
                        }
                        #endregion
                        #region   "Apply to goods only, excluding Freight Value"
                        else if (lstCouponBE[0].BehaviourType.ToLower() == "basket")
                        {
                            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                            lstFreightBE = CalculateFreight(CountryId);
                            isvalid = true;
                            if (lstFreightBE != null && lstFreightBE.Count > 0)
                            {
                                string sdSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                string sdEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                                if (strShipmentType.Equals("1"))
                                {
                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                }
                                if (strShipmentType.Equals("2"))
                                {
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                }
                                if (strShipmentType.Equals("0"))
                                {
                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone

                                }
                            }
                            OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                            strMessage = "Success";
                        }
                        #endregion
                        #region //Free shipping excluding Freight Value
                        else if (lstCouponBE[0].BehaviourType.ToLower() == "freeshipping")
                        {
                            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                            lstFreightBE = CalculateFreight(CountryId);
                            isvalid = true;
                            string seSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                            string seEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                            if (strShipmentType.Equals("1"))
                            {
                                //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                            }
                            if (strShipmentType.Equals("2"))
                            {
                                //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                            }
                            if (strShipmentType.Equals("0"))
                            {
                                //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                            }
                            OrderValueAfterDiscount = OrderItemTotal;
                            discountPercentage = 0.0;
                            strMessage = "Success";
                        }
                        #endregion
                        #region  //Discounted Shipping
                        else if (lstCouponBE[0].BehaviourType.ToLower() == "shipping")
                        {
                            isvalid = true;
                            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                            lstFreightBE = CalculateFreight(CountryId);

                            if (lstFreightBE != null && lstFreightBE.Count > 0)
                            {
                                string sfSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                string sfEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                                if (strShipmentType.Equals("1"))
                                {
                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                }
                                if (strShipmentType.Equals("2"))
                                {
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                }
                                if (strShipmentType.Equals("0"))
                                {
                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                }
                                discountPercentage = 0.0;

                            }
                            OrderValueAfterDiscount = OrderItemTotal;
                            strMessage = strSuccess;// "Success";
                        }
                        #endregion
                        #region Tax Calculation after discount
                        if (isvalid)
                        {
                            string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, discountPercentage);
                            HttpContext.Current.Session["discountPercentage"] = discountPercentage;
                            if (!string.IsNullOrEmpty(strTotalTax))
                            {
                                string[] strTax = strTotalTax.Split('|');
                                if (strTax.Length > 0)
                                {
                                    StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                    ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                }
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    strMessage = strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }

            return "[{\"Message\":\"" + strMessage + "\",\"OrderValueAfterDiscount\":\"" + OrderValueAfterDiscount + "\",\"StandardFreightValueAfterDiscount\":\"" +
                  StandardFreightValueAfterDiscount + "\",\"ExpressFreightValueAfterDiscount\":\"" + ExpressFreightValueAfterDiscount +
                  "\",\"StandardTaxValueAfterDiscount\":\"" + StandardTaxValueAfterDiscount + "\",\"ExpressTaxValueAfterDiscount\":\"" +
                  ExpressTaxValueAfterDiscount + "\",\"BehaviourType\":\"" + strBehaviourType.ToLower() + "\",\"ShipmentType\":\"" + strShipmentType.ToLower() + "\"}]";


        }
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 11-09-15
        /// Scope   : changeaddress webmethod of the ShoppingCart_Payment page
        /// </summary>
        /// <param name="DeliveryAddressId"></param>                
        /// <returns>Delivery Address</returns>
        [System.Web.Services.WebMethod]
        public static string changeaddress(Int32 DeliveryAddressId)
        {
            string jsondata = string.Empty;
            try
            {
                UserBE lstUser = new UserBE();
                List<UserBE.UserDeliveryAddressBE> lstNew = new List<UserBE.UserDeliveryAddressBE>();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                if (lstUser != null && lstUser.UserDeliveryAddress.Count > 0)
                {
                    lstNew = lstUser.UserDeliveryAddress.FindAll(x => x.DeliveryAddressId == DeliveryAddressId);
                    if (lstNew.Count > 0)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        jsondata = jSearializer.Serialize(lstNew);
                    }
                    else
                    {
                        List<UserBE.UserDeliveryAddressBE> lstBlank = new List<UserBE.UserDeliveryAddressBE>();
                        System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        jsondata = jSearializer.Serialize(lstBlank);
                    }
                }
                return jsondata;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 24-09-15
        /// Scope   : CalculateFreightCharges webmethod of the ShoppingCart_Payment page
        /// </summary>
        /// <param name="intCountryId"></param>                
        /// <returns>Freight charges</returns>
        [System.Web.Services.WebMethod]
        public static string CalculateFreightCharges(Int16 intCountryId)
        {
            string jsondata = string.Empty;
            try
            {
                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                lstFreightBE = CalculateFreight(intCountryId);
                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    lstFreightBE[0].SZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                    lstFreightBE[0].EZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                    string strTotalTax = CalculateTax(intCountryId, lstFreightBE[0].SZPrice, lstFreightBE[0].EZPrice, 0);

                    if (!string.IsNullOrEmpty(strTotalTax))
                    {
                        string[] strTax = strTotalTax.Split('|');
                        if (strTax.Length > 0)
                        {
                            lstFreightBE[0].StandardZone = strTax[0]; //Total tax for standard zone
                            lstFreightBE[0].ExpressZone = strTax[1];//Total tax for express zone
                        }
                    }
                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        if (lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).DisplayDutyMessage)
                            lstFreightBE[0].DisplayDutyMessage = true;
                    }
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsondata = jSearializer.Serialize(lstFreightBE);
                }
                return jsondata;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
        }

        //Added by Sripal For SSO the data enter in the BSYS table
        private void InsertBASYSId(Int16 CurrencyIdx, string strEmailId, Int16 iUserID)
        {
            try
            {
                StoreBE objStore = StoreBL.GetStoreDetails();
                int intCustomerID_OASIS;
                UserBE objUser = new UserBE();
                #region "Commented by Sripal for not fetching data
                //UserBE objBE = new UserBE();
                //objBE.EmailId = Sanitizer.GetSafeHtmlFragment(strEmailId.Trim());
                //objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                //objBE.CurrencyId = Convert.ToInt16(CurrencyIdx);
                //objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBE);
                #endregion
                #region "Assign the data to object from the inputs"
                objUser.EmailId = Sanitizer.GetSafeHtmlFragment(strEmailId.Trim());
                objUser.UserId = iUserID;
                List<string> objLs = SplitName(txtContact_Name.Value);
                if (objLs.Count == 2)
                {
                    objUser.FirstName = objLs[0];
                    objUser.LastName = objLs[1];
                }
                //objUser.FirstName = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                //objUser.LastName = lstUser.LastName;
                objUser.PredefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                objUser.PredefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                objUser.PredefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                objUser.PredefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                objUser.PredefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                objUser.PredefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                objUser.PredefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                objUser.PredefinedColumn8 = ddlCountry.SelectedValue;
                objUser.CountryName = ddlCountry.SelectedItem.Text;
                objUser.PredefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                objUser.IPAddress = GlobalFunctions.GetIpAddress();
                #endregion
                objUser.DefaultCustId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId; //1281850; // objStore.StoreCurrencies[i].DefaultCompanyId;
                objUser.SourceCodeId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).SourceCodeId; //5281; //objStore.StoreCurrencies[i].SourceCodeId;

                intCustomerID_OASIS = RegisterCustomer_OASIS.BuildCustContact_OASIS(objUser);
                if (intCustomerID_OASIS > 0)
                {
                    UserBL.ExecuteBASYSDetails(Constants.USP_InsertUserBASYSDetails, true, objUser.UserId, CurrencyIdx, intCustomerID_OASIS);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        private int UpdateRegistration()
        {
            int i = 0;
            try
            {
                UserBE objRegBE = new UserBE();
                string SaltPass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["FirstName"]) + " " + Convert.ToString(Session["LastName"])), "SHA512", null);
                objRegBE.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["UserName"]));
                objRegBE.Password = SaltPass;
                objRegBE.FirstName = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["FirstName"]));
                objRegBE.LastName = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["LastName"]));

                objRegBE.PredefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                objRegBE.PredefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                objRegBE.PredefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                objRegBE.PredefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                objRegBE.PredefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                objRegBE.PredefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                objRegBE.PredefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                objRegBE.PredefinedColumn8 = ddlCountry.SelectedValue;
                objRegBE.PredefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                UserBE.UserDeliveryAddressBE objReg = new UserBE.UserDeliveryAddressBE();
                objReg.PreDefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtDContact_Name.Value.Trim());
                objReg.PreDefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtDCompany_Name.Value.Trim()); // Added by SHRIGANESH 27 July 2016 for Delivery Company Name
                objReg.PreDefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtDAddress1.Value.Trim());
                objReg.PreDefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtDAddress2.Value.Trim());
                objReg.PreDefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtDTown.Value.Trim());
                objReg.PreDefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtDState__County.Value.Trim());
                objReg.PreDefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtDPostal_Code.Value.Trim());
                objRegBE.IsGuestUser = false;
                objReg.PreDefinedColumn8 = ddlDCountry.SelectedValue;
                objReg.PreDefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtDPhone.Value.Trim());
                objRegBE.UserDeliveryAddress.Add(objReg);
                objRegBE.IPAddress = GlobalFunctions.GetIpAddress();
                objRegBE.CurrencyId = Convert.ToInt16(intCurrencyId);
                i = UserBL.ExecuteRegisterDetailsSSO(Constants.USP_UpdateTbl_UsersSSO, true, objRegBE, "StoreCustomer");
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return i;
        }

        protected void btnApplyGiftCertificate_Click(object sender, EventArgs e)
        {
            try
            {
                BindPriceDetails();
                GenerateGiftCertificateXML();
                Control FooterTemplate = rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0];
                HtmlGenericControl divGiftCertificate = FooterTemplate.FindControl("divGiftCertificate") as HtmlGenericControl;

                HtmlGenericControl spnGiftCertificateAmount = FooterTemplate.FindControl("spnGiftCertificateAmount") as HtmlGenericControl;
                HtmlGenericControl dvTotal = (HtmlGenericControl)FooterTemplate.FindControl("dvTotal");
                HtmlGenericControl spnShippingPrice = (HtmlGenericControl)FooterTemplate.FindControl("spnShippingPrice");
                HtmlGenericControl spnTaxPrice = (HtmlGenericControl)FooterTemplate.FindControl("spnTaxPrice");
                HtmlGenericControl spnTotalPrice = (HtmlGenericControl)FooterTemplate.FindControl("spnTotalPrice");
                string str;
                str = dvTotal.InnerText.Replace(strCurrencySymbol, "");
                str = str.Replace("<span>", "");
                str = str.Replace("</span>", "");

                divGiftCertificate.Visible = true;
                //Session["Amount"] = "10";
                if (Session["Amount"] != null)
                {
                    str_SessionGCAmount = Convert.ToString(Session["Amount"]);
                    double dDiff, dShpCartPrice = 0;
                    double dGCAmt = Convert.ToDouble(Session["Amount"]);
                    if (rbStandard.Checked)
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(hidStandardTax.Value) + Convert.ToDouble(spnstandard.InnerText);// Convert.ToInt64(spnTotalPrice.InnerText);
                    else
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(hidExpressTax.Value) + Convert.ToDouble(spnexpress.InnerText);

                    if (dGCAmt < dShpCartPrice)
                    {
                        dDiff = dShpCartPrice - dGCAmt;
                        spnTotalPrice.InnerText = Convert.ToString(dDiff.ToString("f"));
                        spnGiftCertificateAmount.InnerText = "-" + Convert.ToString(dGCAmt.ToString("f"));
                    }
                    else if (dGCAmt > dShpCartPrice)
                    {
                        dDiff = dGCAmt - dShpCartPrice;
                        spnTotalPrice.InnerText = "0.00";
                        spnGiftCertificateAmount.InnerText = "-" + Convert.ToString(dShpCartPrice.ToString("f"));
                    }
                    else if (dGCAmt == dShpCartPrice)
                    {
                        dDiff = 0;
                        spnTotalPrice.InnerText = "0.00";
                        spnGiftCertificateAmount.InnerText = "-" + Convert.ToString(dShpCartPrice.ToString("f"));
                    }
                    BindPriceDetails();
                    //spnGiftCertificateAmount.InnerText = Convert.ToString(Session["Amount"]);
                }
                else
                {
                    spnGiftCertificateAmount.InnerText = "0.00"; ;
                }
                //BindPriceDetails();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void btnCancelGiftCertificate_OnClick(object sender, EventArgs e)
        {
            try
            {
                Session["Amount"] = null;
                Control FooterTemplate = rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0];
                HtmlGenericControl divGiftCertificate = FooterTemplate.FindControl("divGiftCertificate") as HtmlGenericControl;
                divGiftCertificate.Visible = false;

                txtGiftCertificateCode.Text = "";
                BindPaymentTypes();
                BindPriceDetails();
                ListItem lItem = ddlPaymentTypes.Items.FindByValue("1|I");
                if (lItem != null)
                {
                    ddlPaymentTypes.SelectedValue = "1|I";
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private int GenerateGiftCertificateXML()
        {
            int intOrderID = 0;
            try
            {
                StoreBE lstStoreDetail = new StoreBE();
                lstStoreDetail = StoreBL.GetStoreDetails();

                //txtGiftCertificateCode.Text = "0324-D49C-2615-88A2";
                #region CheckBalance
                XElement xCheckBalanceElement;

                if (Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode).ToLower() != "gbp")
                {
                    xCheckBalanceElement = new XElement("CheckBalance", new XElement("CertificateReference", txtGiftCertificateCode.Text.Trim())
                                                              , new XElement("Currency", Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode))
                                                              , new XElement("DivisionID", Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId)));
                }
                else
                {
                    xCheckBalanceElement = new XElement("CheckBalance", new XElement("CertificateReference", txtGiftCertificateCode.Text.Trim())
                                                             , new XElement("Currency", "")
                                                             , new XElement("DivisionID", Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId)));
                }
                #endregion
                //XElement xOrderElement;
                //xOrderElement = new XElement("GiftOrder", xOrderHeaderElement, xOrderLineElement);
                XDocument xOrderXML = new XDocument(xCheckBalanceElement);
                XmlNode OrderXMLNode = new XmlDocument().ReadNode(xOrderXML.Root.CreateReader());

                string str = "";
                if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
                {
                    PWGlobalEcomm.GiftCertificateServiceLive.GiftCertificateService giftcertificateservice = new PWGlobalEcomm.GiftCertificateServiceLive.GiftCertificateService();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                       ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    giftcertificateservice.Credentials = objNetworkCredentials;
                    str = giftcertificateservice.CheckBalance(OrderXMLNode);
                }
                else
                {
                    PWGlobalEcomm.GiftCertificateService.GiftCertificateService giftcertificateservice = new PWGlobalEcomm.GiftCertificateService.GiftCertificateService();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                        ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    giftcertificateservice.Credentials = objNetworkCredentials;
                    str = giftcertificateservice.CheckBalance(OrderXMLNode);
                }

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(str);

                XmlNodeList elemList = xd.GetElementsByTagName("GiftCertificateCheckResult");
                double dRGCAmount = 0;
                for (int i = 0; i < elemList.Count; i++)
                {
                    string attrVal = elemList[i].Attributes["Valid"].Value;
                    if (attrVal.ToLower() == "true")
                    {
                        XmlNodeList elemList1 = xd.GetElementsByTagName("Amount");
                        if (elemList1.Count > 0)
                        {
                            string strAmount = elemList1[0].InnerText;
                            //Session["Amount"] = strAmount;
                            dRGCAmount += Convert.ToDouble(strAmount);
                            StringBuilder sbGift = new StringBuilder();
                            sbGift.Append(txtGiftCertificateCode.Text.Trim() + "|*|" + strAmount);
                            Session["sbGift"] = sbGift;
                        }
                    }
                }
                if (dRGCAmount == 0) { Session["Amount"] = null; }
                else { Session["Amount"] = dRGCAmount; }
                //Session["Amount"] = 600;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return intOrderID;
        }

        protected void BindResourceData()
        {
            try
            {
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strFreightMOVMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_freight_mov_message").ResourceValue;
                        strDutyMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_duty_message").ResourceValue;
                        strSC_InSufficient_Point = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_insufficient_point").ResourceValue;
                        strSC_Payment_Types = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_payment_types").ResourceValue);
                        strSC_UDF_Minimum_Maximum_Range = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_udf_minimum_maximum_range").ResourceValue;
                        strSC_UDF_Range2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_udf_range2").ResourceValue;
                        strSC_UDF_Range3 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_udf_range3").ResourceValue;
                        strSC_Numeric = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_numeric").ResourceValue;
                        strSC_NumberMinDecimal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_numbermindecimal").ResourceValue;
                        strNew_Delivery_Address = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "new_delivery_address").ResourceValue;
                        strSC_Error_Processing_Payment = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_error_processing_payment").ResourceValue;
                        strSC_Updating_Order_Details = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_updating_order_details").ResourceValue;
                        strSC_Error_Update_Delivery_Address = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_error_update_delivery_address").ResourceValue;
                        strSC_Invalid_Coupon_Code = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_invalid_coupon_code").ResourceValue);
                        strSC_Coupon_Code_Maximum_Order_Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_coupon_code_maximum_order_value").ResourceValue;
                        strSC_Coupon_Code_Minimum_Order_Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_coupon_code_minimum_order_value").ResourceValue;
                        strSC_Coupon_Code_Minimum_Maximum_Range = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_coupon_code_minimum_maximum_range").ResourceValue;
                        strSC_Invalid_Coupon_Delivery_Country = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_invalid_coupon_delivery_country").ResourceValue;
                        strSC_Invalid_Coupon_Shipping_Method = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_invalid_coupon_shipping_method").ResourceValue;
                        strSuccess = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Success").ResourceValue;
                        strSC_UDF_Max_Character = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_udf_max_character").ResourceValue;
                        strUppercase = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "uppercase").ResourceValue;
                        strRequired = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "required").ResourceValue);
                        strFile_Extension = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "file_extension").ResourceValue;
                        strRegister_RegisteredAddress_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_registeredaddress_text").ResourceValue;
                        strRegister_DeliveryAddress_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_deliveryaddress_text").ResourceValue;
                        strValid = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "valid").ResourceValue;
                        stValid = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "valid").ResourceValue);
                        strBasket_Title_Text = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "basket_title_text").ResourceValue);
                        strSC_Special_Instruction = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_special_instruction").ResourceValue);
                        strBasket_Quantity_Not_More_Than_Stock_Message = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "basket_quantity_not_more_than_stock_message").ResourceValue);
                        strSC_Reapply_Coupon = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_reapply_coupon").ResourceValue);

                        strSC_Require_Coupon = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_require_coupon").ResourceValue);
                        strSC_Place_Order = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_place_order").ResourceValue);

                        //Button aProceed = (Button)rptBasketListing.FindControl("aProceed");
                        //aProceed.Text = strSC_Place_Order;

                        foreach (RepeaterItem item in rptBasketListing.Items)
                        {
                            if (item.ItemType == ListItemType.Footer)
                            {
                                Button aProceed = (Button)item.FindControl("aProceed");
                                aProceed.Text = strSC_Place_Order;

                                HtmlGenericControl spnBsktSubTotal = (HtmlGenericControl)item.FindControl("spnBsktSubTotal");
                                spnBsktSubTotal.InnerText = strsc_basket_subtotal;
                            }
                        }

                        strSC_Enter_Card_Details = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_enter_card_details").ResourceValue);
                        spnRegisterAddress.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_section_2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnDelivery_Address.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_deliveryaddress_text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnDelivery_AddressTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_delivery_address_title" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        spnPageTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_payment_page_title" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnShipping_Method.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_shipping_method" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblSetDefault.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_set_as_default" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnPaymentMethod.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_payment_method" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblGC.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_code" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        lblChkAddress.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_same_as_delivery_address" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        lblInstruction.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_instructions" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblUploadFile.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_upload_file" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblCouponCode.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_couponcode" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblSucCoupon.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_applied_coupon_success" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnApplyCoupon.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "apply" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnRemoveCoupon.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "cancel" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnReviewNConfirm.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_review_and_confirm" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        strsc_basket_subtotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_basket_subtotal" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strTotal_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_total_text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strShipping = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "shipping" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGiftCertificateTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sitelinks_giftcertificatetitle" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strTax = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "tax" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        strsc_insufficient_couponcode_balance = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_insufficient_couponcode_balance" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private List<string> SplitName(string strName)
        {
            List<string> objLstStr = new List<string>();
            try
            {
                if (strName.Trim().IndexOf(' ') > 0)
                {
                    objLstStr.Add(strName.Trim().Substring(0, txtContact_Name.Value.Trim().IndexOf(' ')));
                    objLstStr.Add(strName.Trim().Substring(strName.Trim().IndexOf(' ') + 1, (strName.Trim().Length - (strName.Trim().IndexOf(' ') + 1))));
                }
                else
                {
                    objLstStr.Add(strName.Trim());
                    objLstStr.Add(".");
                }
            }
            catch (Exception)
            {
                objLstStr.Add(strName.Trim());
                objLstStr.Add(".");
            }
            return objLstStr;
        }
    }
}