﻿using AHPP.ASP.Net.Lib;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
//using PWGlobalEcomm.GiftCertificateService;
using PWGlobalEcomm.GlobalUtilities;
//using PWGlobalEcomm.Stock;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace Presentation
{
    public partial class ShoppingCart_Payment : BasePage
    {
        #region variables
        protected global::System.Web.UI.WebControls.Repeater rptBasketListing;
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtDContact_Name, txtDPhone, txtDAddress1,
            txtDAddress2, txtDTown, txtDState__County, txtDPostal_Code, txtAddressTitle, txtContact_Name, txtPhone, txtAddress1, txtTaxNo,
            txtAddress2, txtTown, txtState__County, txtPostal_Code, txtCompany, txtCouponCode, txtDCompany_Name;
        protected global::System.Web.UI.WebControls.TextBox txtGiftCertificateCode;
        //protected global::System.Web.UI.WebControls.Button aProceed;
        protected global::System.Web.UI.WebControls.DropDownList ddlDCountry, ddlDAddressTitle, ddlPaymentTypes, ddlCountry, ddlInvoiceCountry, ddlLegalEntity;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidDeliveryAddressId, hidService1, hidService2, hidService3, hidService1Tax, hidService2Tax, hidService3Tax, hidCouponCode, hidePrice, hdncustom2, hdncustom1, hdncustom3, hdisLegalEntity;
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox addressCheckbox, chkIsDefault;
        protected global::System.Web.UI.HtmlControls.HtmlInputRadioButton rbService1, rbService2, rbService3;
        protected global::System.Web.UI.WebControls.Literal ltrDContact_Name, ltrDPhone, ltrDAddress1, ltrDAddress2, ltrDTown, ltrdState__County, ltrDPostalCode,
            ltrDCountry, ltrContact_Name, ltrPhone, ltrTaxNo, ltrAddress1, ltrAddress2, ltrTown, ltrState__County, ltrPostalCode, ltrCountry, ltrCompany, ltrColumnName, ltrDCompany_Name, ltrInvoiceAddress, ltrInvoiceCountry, ltrLegalEntity;
        protected global::System.Web.UI.HtmlControls.HtmlTextArea txtInstruction;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnService1, spnService2, spnService3, spnService1Text, spnService2Text, spnService3Text, divService1Cont, divService2Cont, divService3Cont,
            divMOVMessage, divDutyMessage, divStatictext, divaddtionalcharges, header2, header2_space, dvApplyCode, spnRegisterAddress, spnDelivery_Address, spanBillingTitle, spnDelivery_AddressTitle, spnItemPrice, spnPostagePacking, spnTotalBeforeVAT, spnVAT, spnTotal
            , spnPageTitle, spnShipping_Method, lblSetDefault, spnPaymentMethod, lblGC, lblInstruction, lblUploadFile, lblCouponCode, lblSucCoupon
            , spnReviewNConfirm, spnBsktSubTotal, spnadditionaltext, spnadditionalcharge, lblChkAddress, dvApplyGiftCertificate, divTax, divIncludVAT, divHeader_Invoice, divCountry, divTaxNo, spncustom2text, spncustom1text, spncustom3text;
        protected global::System.Web.UI.WebControls.Table tblUDFS, tblCustRefUDFS;
        protected global::System.Web.UI.WebControls.FileUpload fuDoc;
        protected global::System.Web.UI.WebControls.Label lblItemPrice, lblPostagePacking, lblTotalBeforeVAT, lblVAT, lblTotal;
        protected global::System.Web.UI.HtmlControls.HtmlInputButton btnRemoveCoupon, btnApplyCoupon;
        //protected global::System.Web.UI.WebControls.Button aEmail;
        public bool IsTotalvalueZeroafterGC = false;// vikram for legal entity
        public string strEmailMeaCopyOfMyBasket;
        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        Int32 intUserId = 0;
        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        public decimal dTotalPrice = 0;
        public bool bDeliveryAddress = false;
        string strUserSessionId = string.Empty;
        UserBE lstUser;
        double totalnet = 0;
        CustomerOrderBE lstCustomerOrders;
        Int32 intCustomerOrderId = 0;
        string TxRefGUID = string.Empty;
        decimal dTotalWeight = 0;
        public bool bBackOrderAllowed = false;
        public bool bBASysStore = false;
        public string selected = string.Empty;
        public bool IsDropdownvalue = false;
        public string strFreightMOVMessage = string.Empty;// "You can not proceed because your order value is not fullfilling the criteria.";
        public string strDutyMessage = string.Empty;// "International duty may apply in your country.";
        public string strCurrencySymbol = string.Empty;
        public string strPointsText = string.Empty;
        public bool IsPointsEnbled = false;
        public bool IsPointsEnbledPoints = true;
        public bool pIsPointsEnbled = false;
        public bool pIsPointsSymmEnbled = true;
        public bool IsInvoiceAccountEnabled = false;
        public bool IsMultifreight = false;
        //  public bool IsB2C = false;
        List<InvoiceBE> lstInvoice;
        public string strPTax, strPShip, strProductPriceEmail, strDeliveryAddress, strShippingMethodEmail, strPTotal = string.Empty;
        decimal dPTotal = 0;
        public int eCounter = 0; //Added By Hardik "14/10/2016"
        string strSiteLogoDir = string.Empty;
        public string strAddtionalcharges = string.Empty;
        public string strtotalnet = string.Empty;
        decimal additionalcharge = 0;
        public string hostispointvalue = string.Empty;
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
        List<CustomUDFRegDataDanske> lstRegData_Danskebank = new List<CustomUDFRegDataDanske>();
        string strSC_InSufficient_Point = string.Empty;
        public string strSC_Payment_Types, strSC_UDF_Minimum_Maximum_Range, strSC_UDF_Range2, strSC_UDF_Range3, strSC_Numeric, strSC_NumberMinDecimal, strNew_Delivery_Address
             , strSC_Error_Processing_Payment, strSC_Updating_Order_Details, strSC_Error_Update_Delivery_Address, strSC_UDF_Max_Character, Generic_Select_Text, strGeneric_Invoice_others
             , strUppercase, strRequired, strFile_Extension, stValid, str_SessionGCAmount = string.Empty, strPayment_UDF_InvalidRegNum;
        public static string strSC_Invalid_Coupon_Code, strSC_Coupon_Code_Maximum_Order_Value, strSC_Coupon_Code_Minimum_Order_Value,
             strSC_Coupon_Code_Minimum_Maximum_Range, strSC_Invalid_Coupon_Delivery_Country, strSC_Invalid_Coupon_Shipping_Method,
             strSuccess, strRegister_RegisteredAddress_Text, strRegister_DeliveryAddress_Text, strValid, strBasket_Title_Text, strSC_Special_Instruction,
             strBasket_Quantity_Not_More_Than_Stock_Message, strSC_Reapply_Coupon, strSC_Require_Coupon, strSC_Place_Order, strSC_Enter_Card_Details, strSC_Email_Me_Basket,
             strTotal_Text = "", strTax = "", strGiftCertificateTitle = "", strShipping = "", strRegister_txtSplChar_Message = "", strsc_basket_subtotal = "", strsc_insufficient_couponcode_balance = "", str_Order_Total_Include_VAT = "",
             str_See_Details = "", strGeneric_Items = "", str_Generic_Postage_Packing = "", str_Generic_Total_Before_VAT = "", str_Generic_VAT = "", str_Generic_Order_Summary = "", str_Generic_Order_Total = "", str_SC_Email_Me_Copy = "";
        public string Status_Title, strUnitCost, strProductTitle, strStockTitle, Culturedata = "", strGenericPointsText, strAvailableBudget;
        protected global::System.Web.UI.WebControls.HiddenField hdfGC;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgProduct;
        //string strShippingStd, strShippingExp = "";

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divAddressDetails, divShipPayMethod;

        string strProductDetails = "";
        //string strProductPriceEmail = "";


        /*User Type*/
        StoreBE objStoreBE = new StoreBE();
        Int16 iUserTypeID = 1;
        string IsBudget;
        string Budgetvalue, Approver_name, Approver_email, Approver_employeeid, isapproved, team, BudgetCurrencySymbol, strBudgetErrorMsg;

        Int16 DivisionID, iUserDeliveryCountryID;
        #endregion

        public bool IsCustomerRef = false; // Added By Snehal 06 01 2017

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 09-09-15
        /// Scope   : Page_Load of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Exceptions.WriteInfoLog("Payment Page Load Start");
                #region
                #region "added by Sripal Punchout"
                if (Session["S_ISPUNCHOUT"] != null)
                {
                    Exceptions.WriteInfoLog("Inside Session[S_ISPUNCHOUT] != null");
                    if (Convert.ToBoolean(Session["S_ISPUNCHOUT"]))
                    {
                        Exceptions.WriteInfoLog("Before Redirecting to BasketPagePunchout");
                        Response.RedirectToRoute("BasketPagePunchout");
                        return;
                    }
                }
                #endregion

                if (Session["User"] != null)
                {
                    Exceptions.WriteInfoLog("Inside Session[User] != null");
                    lstUser = new UserBE();
                    lstUser = Session["User"] as UserBE;
                    intUserId = lstUser.UserId;
                    iUserTypeID = lstUser.UserTypeID;/*User Type*/
                    IsBudget = lstUser.CustomColumn2;
                }
                else
                {
                    if (Session["GuestUser"] == null)//If condition for Guest User
                    {
                        Exceptions.WriteInfoLog("Inside Session[guestuser] != null");
                        Response.RedirectToRoute("login-page");
                        return;
                    }
                }

               

                #region Code Added 28 July 2016 to set the session of discountPercentage to NULL
                if (string.IsNullOrEmpty(hidCouponCode.Value))
                {
                    Session["discountPercentage"] = null;
                }
                #endregion

                Exceptions.WriteInfoLog("Before calling BindResourceData()");
                BindResourceData();
                Exceptions.WriteInfoLog("After calling BindResourceData()");

                Exceptions.WriteInfoLog("Before calling BindlstRegData_Danskebank()");
                BindlstRegData_Danskebank();
                Exceptions.WriteInfoLog("After calling BindlstRegData_Danskebank()");

                intCurrencyId = GlobalFunctions.GetCurrencyId();
                intLanguageId = GlobalFunctions.GetLanguageId();

               

                Exceptions.WriteInfoLog("Before calling GetStoreDetails()");
                objStoreBE = StoreBL.GetStoreDetails();
                // vikram for legal entity
                Culturedata = objStoreBE.StoreLanguages.FirstOrDefault(x => x.LanguageId == GlobalFunctions.GetLanguageId()).CultureCode;
                CultureInfo ci = new CultureInfo(Culturedata);
                // vikram for legal entity
                Exceptions.WriteInfoLog("After calling GetStoreDetails()");
                if (objStoreBE != null)
                {
                    Exceptions.WriteInfoLog("InSide objStoreBE != null condition");
                    #region
                    bBackOrderAllowed = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "OD_AllowBackOrder").FeatureValues[0].IsEnabled;
                    bBASysStore = objStoreBE.IsBASYS;
                    DivisionID = objStoreBE.StoreCurrencies[0].DivisionId;
                    IndeedSalesBE.BA_Budget objCurr = new IndeedSalesBE.BA_Budget();

                    if (IsBudget != null)
                    {
                        if (IsBudget.Length > 0)
                        {
                            objCurr = CurrencyBL.USP_GetBudgetdetails(IsBudget, DivisionID);
                            if (objCurr != null)
                            {
                                if (objCurr.Budget_Code != "0")
                                {
                                    Budgetvalue = objCurr.Budget_Code;
                                }
                                else { Budgetvalue = "NA"; }
                                team = objCurr.Team;
                                Approver_name = objCurr.Name;
                                Approver_email = objCurr.work_email;
                                Approver_employeeid = objCurr.Reports_To_Employee_ID;
                            }
                            else
                            {
                                BudgetCode();
                            }
                        }
                        else
                        {
                            BudgetCode();
                        }
                    }
                    else
                    {
                        BudgetCode();
                    }
       
                    Exceptions.WriteInfoLog("Before assigning bBackOrderAllowed");
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_EnableCouponCode").FeatureValues[0].IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Inside setting dvApplyCode.Visible = true");
                        dvApplyCode.Visible = true;
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Inside setting dvApplyCode.Visible = false");
                        dvApplyCode.Visible = false;
                    }

                    //added by Sripal
                    if (Request.Url.ToString().Contains("local"))
                    {
                        hdfGC.Value = "1";
                        dvApplyGiftCertificate.Visible = true;
                        Exceptions.WriteInfoLog("Inside setting dvApplyGiftCertificate.Visible = true for local");
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_GiftCertificate").FeatureValues[0].IsEnabled)
                    {
                        hdfGC.Value = "1";
                        dvApplyGiftCertificate.Visible = true;
                        Exceptions.WriteInfoLog("Inside setting dvApplyGiftCertificate.Visible = true");
                    }
                    else
                    {
                        hdfGC.Value = "0";
                        dvApplyGiftCertificate.Visible = false;
                        Exceptions.WriteInfoLog("Inside setting dvApplyGiftCertificate.Visible = false");
                    }
                    List<FeatureBE> objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                    Exceptions.WriteInfoLog("After fetching sc_paymentgateway feature");
                    if (objFeatures != null)
                    {
                        Exceptions.WriteInfoLog("Inside objFeatures != null");
                        List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;
                        Exceptions.WriteInfoLog("After fetching objLstFeatureValueBE value");
                        if (objLstFeatureValueBE != null)
                        {
                            Exceptions.WriteInfoLog("Inside objLstFeatureValueBE != null");
                            FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                            Exceptions.WriteInfoLog("After fetching objFeatureValueBE value");
                            if (objFeatureValueBE != null)
                            {
                                ddlPaymentTypes.Attributes.Add("onchange", "paymentChanged()");
                            }
                            else
                            {
                                Exceptions.WriteInfoLog("before calling RemovePaymentDropdown()");
                                RemovePaymentDropdown();
                                Exceptions.WriteInfoLog("After calling RemovePaymentDropdown()");
                            }
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("before calling RemovePaymentDropdown()");
                            RemovePaymentDropdown();
                            Exceptions.WriteInfoLog("After calling RemovePaymentDropdown()");
                        }
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("before calling RemovePaymentDropdown()");
                        RemovePaymentDropdown();
                        Exceptions.WriteInfoLog("After calling RemovePaymentDropdown()");
                    }
                    #endregion
                }

                #region Changes For the Indeed Points Tripti
                if (!GlobalFunctions.IsPointsEnbled())
                {
                    strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                    paymentselection();
                }
                if (GlobalFunctions.IsPointsEnbled())
                {
                    strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                    pIsPointsEnbled = true;
                    hostispointvalue = GlobalFunctions.PointsText(intLanguageId);
                    Session["ispoint"] = 1;
                    paymentselection();
                }
                #endregion

                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                {
                    IsInvoiceAccountEnabled = true;
                    #region vikram for legal entity
                    ddlPaymentTypes.Attributes.Add("onchange", "javascript: ShowLoader();");
                    ddlPaymentTypes.AutoPostBack = true;
                    #endregion
                }
                else
                {
                    IsInvoiceAccountEnabled = false;
                }
                divMOVMessage.Attributes.Add("style", "display:none");
                divMOVMessage.InnerHtml = strFreightMOVMessage;

                divDutyMessage.Attributes.Add("style", "display:none");
                divDutyMessage.InnerHtml = strDutyMessage;

                if (Session["PrevLanguageId"] == null)
                {
                    Exceptions.WriteInfoLog("Inside Session[PrevLanguageId] == null");
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                }

                if (!IsPostBack || !Session["PrevLanguageId"].ToString().To_Int32().Equals(GlobalFunctions.GetLanguageId().To_Int32()) || Session["ispoint"] != null)
                {
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                    Session["Amount"] = null;
                    Session["IsTotalvalueZeroafterGC"] = null;
                    #region
                    Session["CustomerOrderId"] = null;
                    hidDeliveryAddressId.Value = "";

                    List<object> lstShoppingBE = new List<object>();
                    if (Session["GuestUser"] == null)
                    {
                        Exceptions.WriteInfoLog("Inside Session[GuestUser] == null");
                        Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData() for Reg User");
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId, true);
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData() for GuestUser User");
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, Session.SessionID, true);
                        Exceptions.WriteInfoLog("After calling ShoppingCartBL.GetBasketProductsData() for GuestUser User");
                    }

                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        Exceptions.WriteInfoLog("Inside lstShoppingBE != null && lstShoppingBE.Count > 0 condition");
                        #region
                        Exceptions.WriteInfoLog("Before calling BindCountryDropDown()");
                        if (!IsPostBack)
                        {
                            
                            BindCountryDropDown();

                            Exceptions.WriteInfoLog("After calling BindCountryDropDown()");
                            Exceptions.WriteInfoLog("Before calling BindDeliveryAddress()");
                            BindDeliveryAddress();
                            Exceptions.WriteInfoLog("After calling BindDeliveryAddress()");
                            Exceptions.WriteInfoLog("Before calling BindPaymentTypes()");
                            BindPaymentTypes();
                        }
                        Exceptions.WriteInfoLog("After calling BindPaymentTypes()");
                        if (Session["GuestUser"] == null)
                        {
                            Exceptions.WriteInfoLog("Inside Session[GuestUser] == null of page load()");
                            lstCustomerOrders = new CustomerOrderBE();
                            CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
                            objCustomerOrderBE.OrderedBy = lstUser.UserId;
                            objCustomerOrderBE.LanguageId = intLanguageId;
                            #region
                            if (lstCustomerOrders == null)//Added by Sripal OR condition
                            {
                                Session["CustomerOrderId"] = null;
                                ddlDCountry.ClearSelection();
                                if (ddlDCountry.Items.FindByValue(Convert.ToString(lstUser.PredefinedColumn8)) != null)
                                {
                                    ddlDCountry.Items.FindByValue(Convert.ToString(lstUser.PredefinedColumn8)).Selected = true;
                                }
                            }
                            #endregion
                        }

                        rptBasketListing.DataSource = lstShoppingBE;
                        rptBasketListing.DataBind();
                        //Session["strProductBasketdetails"] = strbuilderProductDetails;
                        ViewState["strProductBasketdetails"] = strProductDetails;
                        ViewState["strProductPriceEmail"] = strProductPriceEmail;

                        Session["ProductWeight"] = Convert.ToString(dTotalWeight);

                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                        {
                            IsInvoiceAccountEnabled = true;
                            divHeader_Invoice.Visible = true;
                            divCountry.Visible = false;

                            //BindInvoiceCountry();

                            //BindInvoiceUserAddress(null);

                            #region vikram for legal entity

                            List<object> lstPaymentTypes = new List<object>();
                            List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();

                            string strPaymentMethod = Convert.ToString(ddlPaymentTypes.SelectedValue);
                            if (strPaymentMethod.Contains("|"))
                            {
                                string[] items = strPaymentMethod.Split('|');
                                Int16 iPaymentTypeId = Convert.ToInt16(items[0]);
                                lstPaymentTypes = ShoppingCartBL.GetPaymentdetailsById(iPaymentTypeId, GlobalFunctions.GetLanguageId());
                                lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                                if (Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                                {
                                    
                                    divCountry.Visible = true;
                                    divHeader_Invoice.Visible = false;
                                    BindInvoiceCountry();
                                    BindUserAddress();
                                }
                                else
                                {
                                    if (!lstPaymentTypesBE[0].InvoiceAccountId)
                                    {
                                        BindInvoiceCountry();
                                        divCountry.Visible = true;
                                        divHeader_Invoice.Visible = false;
                                        BindUserAddress();
                                    }
                                    else
                                    {
                                        divCountry.Visible = false;
                                        BindInvoiceCountry();
                                        BindInvoiceUserAddress(null);
                                    }
                                }
                            }
                            else
                            {
                                divCountry.Visible = true;
                                divHeader_Invoice.Visible = false;
                                BindInvoiceCountry();
                                BindUserAddress();
                            }

                            #endregion

                            SetFreightValues();
                            bDeliveryAddress = true;
                            addressCheckbox.Checked = false;
                        }
                        else
                        {
                            bDeliveryAddress = false;
                            //Bind User Address
                            divHeader_Invoice.Visible = false;
                            Exceptions.WriteInfoLog("Before Calling BindUserAddress() of page load()");
                            divCountry.Visible = true;
                            if (!IsPostBack)
                            {
                                BindUserAddress();
                            }

                            Exceptions.WriteInfoLog("After Calling BindUserAddress() of page load()");
                            Exceptions.WriteInfoLog("Before Calling SetFreightValues() of page load()");
                            SetFreightValues();
                            Exceptions.WriteInfoLog("After Calling SetFreightValues() of page load()");
                        }
                        //Calculate freight and display value                        
                        Exceptions.WriteInfoLog("Before Calling BindPriceDetails() of page load()");
                        BindPriceDetails();
                        Exceptions.WriteInfoLog("After Calling BindPriceDetails() of page load()");
                        #endregion
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Before Redirecting to Index Page of page load()");
                        Response.RedirectToRoute("Index");
                    }
                    try
                    {
                        Control FooterTemplate = rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0];
                        HtmlGenericControl divGiftCertificate = FooterTemplate.FindControl("divGiftCertificate") as HtmlGenericControl;
                        divGiftCertificate.Visible = false;
                        txtGiftCertificateCode.Text = "";
                        Session["Amount"] = null;
                        str_SessionGCAmount = "0";
                    }
                    catch (Exception) { }
                    #endregion
                }
                Exceptions.WriteInfoLog("Before Calling BindUDFFields() of page load()");
                BindUDFFields(!IsPostBack);
                BindCustomerRefUDFFields();

                Exceptions.WriteInfoLog("After Calling BindUDFFields() of page load()");
                this.Page.Title = "Checkout";
                Exceptions.WriteInfoLog("Before Calling BindStaticText() of page load()");
                BindStaticText();
                Exceptions.WriteInfoLog("After Calling BindStaticText() of page load()");
                #endregion
                if (GlobalFunctions.IsPointsEnbled())
                {
                    dvApplyGiftCertificate.Visible = false;
                    dvApplyCode.Visible = false;
                }
                #region Added By Hardik on 21/FEB/2017
                if (GlobalFunctions.SubmitEnquiry() == true)
                {
                    HtmlGenericControl divShipping = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("divShipping");
                    HtmlGenericControl divTax = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("divTax");
                    Button aProceed = (Button)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("aProceed");
                    divAddressDetails.Visible = false;
                    divShipPayMethod.Visible = false;
                    divShipping.Visible = false;
                    divTax.Visible = false;
                    aProceed.Visible = false;
                }
                else
                {
                    HtmlGenericControl divShipping = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("divShipping");
                    HtmlGenericControl divTax = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("divTax");
                    Button aProceed = (Button)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("aProceed");
                    divAddressDetails.Visible = true;
                    divShipPayMethod.Visible = true;
                    divShipping.Visible = true;
                    divTax.Visible = true;
                    aProceed.Visible = true;
                }
                #endregion
                #region for Legal entity
                if (Session["IsTotalvalueZeroafterGC"] != null)
                {
                    IsTotalvalueZeroafterGC = Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]);//vikam for legal entity
                }
                #endregion

                if (Session["Budget"] == null)
                {
                    HtmlGenericControl spnbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnbudget");
                    HtmlGenericControl dvbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("dvbudget");

                    spnbudget.InnerText = "";// need to add resource by tripti
                    dvbudget.InnerHtml = "";
                }
                                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BudgetCode()
        {
            try
            {
                if (Session["GuestUser"] == null)
                {
                    if (lstUser != null)
                    {
                        UserBE.UserBudgetBE objUB = new UserBE.UserBudgetBE();

                        objUB = CurrencyBL.USP_GetUserBudgetdetailsPayment(lstUser.EmailId, GlobalFunctions.GetCurrencyId());
                        if (objUB != null)
                        {
                            if (objUB.BudgetCode != "")
                            {
                                Budgetvalue = objUB.BudgetCode;
                            }
                            else { Budgetvalue = "NA"; }
                            // BudgetCurrencySymbol = objUB.CurrencySymbol;
                            BudgetCurrencySymbol = objUB.CurrencySymbol;
                        }
                    }
                }
                else
                {
                    UserBE objBE = new UserBE();
                    objBE.EmailId = Convert.ToString(Session["GuestUser"]).Trim();
                    UserBE.UserBudgetBE objUB = new UserBE.UserBudgetBE();

                    objUB = CurrencyBL.USP_GetUserBudgetdetailsPayment(objBE.EmailId, GlobalFunctions.GetCurrencyId());
                    if (objUB != null)
                    {
                        if (objUB.BudgetCode != "")
                        {
                            Budgetvalue = objUB.BudgetCode;
                        }
                        else { Budgetvalue = "NA"; }
                         BudgetCurrencySymbol = objUB.CurrencySymbol;
                       // BudgetCurrencySymbol = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(GlobalFunctions.GetCurrencyId())).CurrencyCode;
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindPriceDetails()
        {
            try
            {
                Exceptions.WriteInfoLog("Inside BindPriceDetails()");
                HtmlGenericControl spnShippingPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnShippingPrice");
                HtmlGenericControl spnTaxPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTaxPrice");
                #region corrected by Vikram for Total Price start
                HtmlGenericControl spnTotalPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTotalPrice");
                #endregion end
                HtmlGenericControl dvTotal = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("dvTotal");
                HtmlGenericControl spnadditionalcharge = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnadditionalcharge");
                HtmlGenericControl spntotalnet = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spntotalnet");

                #region budget
                if (Session["budget"] != null)
                {
                    HtmlGenericControl spnbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnbudget");
                    HtmlGenericControl dvbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("dvbudget");
                    spnbudget.InnerText = strAvailableBudget;//   add resource by Vivek		
                    dvbudget.InnerHtml =GlobalFunctions.GetCurrencySymbol() + Convert.ToString(Session["budget"]);
                }
                #endregion

                string str;
                str = dvTotal.InnerText;
                decimal vacant = dTotalPrice;
                if (!GlobalFunctions.IsPointsEnbled())
                {
                    str = str.Replace(GlobalFunctions.GetCurrencySymbol() + "<span>", "");
                    str = str.Replace("</span>", "");
                }
                else
                {
                    if (Convert.ToString(Session["ispoint"]) == "0")
                    {
                        str = str.Replace(GlobalFunctions.GetCurrencySymbol() + "<span>", "");
                        str = str.Replace("</span>", "");
                    }
                    else
                    {
                        str = str.Replace("<span>", "");
                        str = str.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                    }
                }
                decimal FinalTotal = 0;
                string Final;
                decimal finalprice = 0;
                if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                {
                    strCurrencySymbol = string.Empty;

                    Final = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(spnShippingPrice.InnerText, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbol, intLanguageId).Replace(strGenericPointsText, "");
                    Final = Final.Replace("<span>", "");

                    FinalTotal = Convert.ToDecimal(str, CultureInfo.InvariantCulture.NumberFormat) + Convert.ToDecimal(Final, CultureInfo.InvariantCulture.NumberFormat);

                    spnTotalPrice.InnerText = Convert.ToString(Math.Round(FinalTotal)) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());

                }
                else
                {
                    FinalTotal = Convert.ToDecimal(str, CultureInfo.InvariantCulture.NumberFormat)
                    + Convert.ToDecimal(spnShippingPrice.InnerText, CultureInfo.InvariantCulture.NumberFormat)
                    + Convert.ToDecimal(spnTaxPrice.InnerText, CultureInfo.InvariantCulture.NumberFormat);

                    spnTotalPrice.InnerText = FinalTotal.ToString("##,###,##0.#0");
                    strPTax = spnTaxPrice.InnerText;

                }

                // spnTotalPrice.InnerText = FinalTotal.ToString("##,###,##0.#0");
                finalprice = Convert.ToDecimal(spnadditionalcharge.InnerText, CultureInfo.InvariantCulture.NumberFormat);
                if (rbService1.Checked)
                {
                    Exceptions.WriteInfoLog("Inside rbService1.Checked in BindPriceDetails()");
                    strPTax = GlobalFunctions.RemovePointsSpanIntheText(hidService1Tax.Value);
                    strPShip = Convert.ToString(spnService1.InnerHtml);

                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        strPShip = "0.00";
                        strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();

                        spnService1.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(strPShip, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbol, intLanguageId);
                        spnShippingPrice.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(strPShip, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbol, intLanguageId)/* + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId())*/;
                        //spnService1.InnerHtml = Final;
                        strPShip = GlobalFunctions.RemovePointsSpanIntheText(spnShippingPrice.InnerHtml);
                    }
                    else
                    {
                        spnTaxPrice.InnerHtml = GlobalFunctions.RemovePointsSpanIntheText(hidService1Tax.Value);
                        spnService1.InnerHtml = strPShip;
                        spnShippingPrice.InnerHtml = Convert.ToString(GlobalFunctions.RemovePointsSpanIntheText(spnService1.InnerHtml));
                    }

                    Exceptions.WriteInfoLog("Exiting rbService1.Checked in BindPriceDetails()");
                }
                else if (rbService2.Checked)
                {
                    Exceptions.WriteInfoLog("Inside else of rbService1.Checked in BindPriceDetails()");
                    strPTax = hidService2Tax.Value;
                    strPShip = spnShippingPrice.InnerHtml;
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        spnService2.InnerHtml = spnService2.InnerHtml.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "").Trim();
                    }

                    spnTaxPrice.InnerHtml = hidService2Tax.Value;
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        spnShippingPrice.InnerHtml = spnService2.InnerHtml + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                        spnService2.InnerHtml = spnService2.InnerHtml + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                    }
                    else
                    {
                        spnShippingPrice.InnerHtml = Convert.ToString(spnService2.InnerHtml);
                    }
                    Exceptions.WriteInfoLog("Exiting else of rbService2.Checked in BindPriceDetails()");
                }
                else if (rbService3.Checked)
                {
                    Exceptions.WriteInfoLog("Inside else of rbService3.Checked in BindPriceDetails()");
                    strPTax = hidService3Tax.Value;
                    strPShip = spnShippingPrice.InnerHtml;
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        spnService3.InnerHtml = spnService3.InnerHtml.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "").Trim();
                    }

                    spnTaxPrice.InnerHtml = hidService3Tax.Value;
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        spnShippingPrice.InnerHtml = spnService3.InnerHtml + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                        spnService3.InnerHtml = spnService3.InnerHtml + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                    }
                    else
                    {
                        spnShippingPrice.InnerHtml = Convert.ToString(spnService3.InnerHtml);
                    }
                    Exceptions.WriteInfoLog("Exiting else of rbService3.Checked in BindPriceDetails()");
                }

                double dShpCartPrice = 0;
                double totalnet = 0;
                if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                {
                    string strTot = str;
                    if (strPShip == null)
                    {
                        strPShip = "0.00";
                    }
                    string strTPTax = strPTax;
                    dShpCartPrice = Convert.ToDouble(strTot) + Convert.ToDouble(strPShip.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), ""));
                }
                else
                {
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(strPShip.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), ""));
                    }
                    else
                    {
                        totalnet = Convert.ToDouble(str) + Convert.ToDouble(strPShip) + Convert.ToDouble(finalprice);
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(GlobalFunctions.RemovePointsSpanIntheText(strPTax)) + Convert.ToDouble(strPShip) + Convert.ToDouble(finalprice);
                    }
                    strPTotal = Convert.ToString(dShpCartPrice.ToString("f"));
                }
                Session["dShpCartPrice"] = dShpCartPrice;
                if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                {
                    strCurrencySymbol = string.Empty;
                    spnTotalPrice.InnerText = Convert.ToString(dShpCartPrice) + " " + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                    spntotalnet.InnerText = Convert.ToString(totalnet);


                }
                else
                {
                    spntotalnet.InnerText = Convert.ToString(totalnet.ToString("f"));
                    spnTotalPrice.InnerText = Convert.ToString(dShpCartPrice.ToString("f"));
                }
                if (!GlobalFunctions.IsPointsEnbled())
                {
                    #region MyRegion
                    double dDiff = 0;
                    double dGCAmt = Session["Amount"] != null ? Convert.ToDouble(Session["Amount"]) : 0;
                    dDiff = dShpCartPrice - dGCAmt;
                    if (dDiff <= 0)
                    {
                        #region vikram for legal entity
                        ListItem lItem = new ListItem(); ;
                        if (Session["Amount"] != null)
                        {
                            Session["IsTotalvalueZeroafterGC"] = true;
                            IsTotalvalueZeroafterGC = true;
                            List<ListItem> items = new List<ListItem>();
                            items.Add(new ListItem("Inv to Open Account", "1|I"));
                            ddlPaymentTypes.Items.AddRange(items.ToArray());
                            lItem = ddlPaymentTypes.Items.FindByValue("1|I");
                            ddlPaymentTypes.Items.Clear();
                            ddlPaymentTypes.Items.Add(lItem);
                            ddlPaymentTypes.Attributes.Add("class", "form-control input-sm hidden");
                            ddlPaymentTypes.Attributes.Remove("onchange");
                            tblCustRefUDFS.Controls.Clear();
                            tblCustRefUDFS.Visible = false;
                        }
                        else
                        {
                            lItem = ddlPaymentTypes.Items.FindByValue("1|I");
                        }
                        #endregion
                    }
                    else
                    {
                        ddlPaymentTypes.Attributes.Add("class", "form-control input-sm");
                    }

                    if (dGCAmt < dShpCartPrice)
                    {
                        dDiff = dShpCartPrice - dGCAmt;
                        spnTotalPrice.InnerText = Convert.ToString(dDiff.ToString("f"));

                        if (Session["Amount"] != null)
                        { Session["Amount"] = Convert.ToDouble(dGCAmt); }//dGCAmt.ToString("f"); //vikram for legal entity
                    }
                    else if (dGCAmt > dShpCartPrice)
                    {
                        dDiff = dGCAmt - dShpCartPrice;
                        spnTotalPrice.InnerText = "0.00";

                        if (Session["Amount"] != null) { Session["Amount"] = Convert.ToDouble(dShpCartPrice); } //dShpCartPrice.ToString("f"); //vikram for legal entiy
                    }
                    else if (dGCAmt == dShpCartPrice)
                    {
                        dDiff = 0;
                        spnTotalPrice.InnerText = "0.00";
                        if (Session["Amount"] != null) { Session["Amount"] = Convert.ToDouble(dGCAmt); } //dGCAmt.ToString("f"); vikram for legal entity
                    }
                    #endregion
                }
                Exceptions.WriteInfoLog("Exiting BindPriceDetails()");
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 02-11-15
        /// Scope   : BindStaticText
        /// </summary>            
        /// <returns></returns>
        protected void BindStaticText()
        {
            try
            {
                Exceptions.WriteInfoLog("Inside BindStaticText()");
                List<StaticPageManagementBE> lst = new List<StaticPageManagementBE>();
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBE.PageName = "/info/checkoutcontent";
                Exceptions.WriteInfoLog("Before calling StaticPageManagementBL.GetAllDetails in BindStaticText()");
                lst = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "PageUrl");
                Exceptions.WriteInfoLog("After calling StaticPageManagementBL.GetAllDetails in BindStaticText()");
                lst = lst.FindAll(x => x.LanguageId == Convert.ToInt16(GlobalFunctions.GetLanguageId()));
                if (lst != null && lst.Count > 0)
                {
                    Exceptions.WriteInfoLog("Inside lst != null && lst.Count > 0 in BindStaticText()");
                    string strContent = Convert.ToString(lst[0].WebsiteContent).Replace("~/", GlobalFunctions.GetVirtualPath()).Replace("$host$", GlobalFunctions.GetVirtualPath());

                    List<StaticTextKeyBE> lstKey = new List<StaticTextKeyBE>();
                    StaticTextKeyBE objBEs = new StaticTextKeyBE();
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    Exceptions.WriteInfoLog("Before calling StaticTextKeyBL.GetAllKeys in BindStaticText()");
                    lstKey = StaticTextKeyBL.GetAllKeys(Constants.USP_GetStaticTextDetails, objBEs, "A");
                    Exceptions.WriteInfoLog("After calling StaticTextKeyBL.GetAllKeys in BindStaticText()");
                    if (lstKey != null)
                    {
                        if (lstKey.Count > 0)
                        {
                            for (int i = 0; i < lstKey.Count; i++)
                            {
                                strContent = strContent.Replace("[" + Convert.ToString(lstKey[i].StaticTextKey) + "]", Convert.ToString(lstKey[i].StaticTextValue));
                            }
                        }
                    }
                    if (divStatictext != null)
                        divStatictext.InnerHtml = strContent;
                }
                Exceptions.WriteInfoLog("Exiting BindStaticText()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindInvoiceCountry()
        {
            try
            {
                Exceptions.WriteInfoLog(" Inside BindInvoiceCountry()");
                List<CountryBE> lstCountry = new List<CountryBE>();
                // lstCountry = CountryBL.GetAllInvoiceCountries(); // Code Comented by SHRIGANESH 05 December 2016 for Harley's Email on 30th November 2016
                lstCountry = CountryBL.GetAllCountries(); //Code Added by SHRIGANESH 05 December 2016 for Harley's Email on 30th November 2016
                ddlInvoiceCountry.Items.Insert(0, new ListItem("--" + Generic_Select_Text + "--", "0"));
                ddlLegalEntity.Items.Insert(0, new ListItem("--" + Generic_Select_Text + "--", "0"));

                ddlInvoiceCountry.AppendDataBoundItems = true;

                if (lstCountry != null && lstCountry.Count > 0)
                {
                    //ddlInvoiceCountry.DataTextField = "Country"; // Code Comented by SHRIGANESH 05 December 2016 for Harley's Email on 30th November 2016
                    ddlInvoiceCountry.DataTextField = "CountryName"; //Code Added by SHRIGANESH 05 December 2016 for Harley's Email on 30th November 2016
                    ddlInvoiceCountry.DataValueField = "CountryId";
                    Exceptions.WriteInfoLog("Before assingning DataSource to ddlDCountry in BindInvoiceCountry()");

                    if (Session["IndeedSSO_User"] != null || Session["IndeedEmployeeSSO_User"] != null)
                    {
                        if (Session["Ship_To_Location"] != null)
                        {
                            ddlInvoiceCountry.DataSource = lstCountry.FindAll(x => x.CountryCode == Session["Ship_To_Location"].ToString());
                        }
                        else if (Session["IndeedEmployeeCountry"] != null)
                        {
                            ddlInvoiceCountry.DataSource = lstCountry.FindAll(x => x.CountryName == Session["IndeedEmployeeCountry"].ToString());
                        }
                    }
                    else
                    {
                        ddlInvoiceCountry.DataSource = lstCountry;
                    }
                    ddlInvoiceCountry.DataBind();
                }
                ddlInvoiceCountry.SelectedIndex = 0;

                Exceptions.WriteInfoLog("Exiting BindInvoiceCountry()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void AddressClear()
        {
            txtContact_Name.Value = string.Empty;
            txtCompany.Value = string.Empty;
            txtAddress1.Value = string.Empty;
            txtAddress2.Value = string.Empty;
            txtPhone.Value = string.Empty;
            txtPostal_Code.Value = string.Empty;
            txtTown.Value = string.Empty;
            txtTaxNo.Value = string.Empty;
            txtState__County.Value = string.Empty;
        }

        protected void ddlInvoiceCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlLegalEntity.Items.Clear();
            AddressClear();
            BindInvoiceUserAddress(null); // Snehal's Code 21 Sept 16
            List<CountryBE> lstUDFS = new List<CountryBE>();
            // SetFreightValues_Invoice();
            string currency = Convert.ToString(GlobalFunctions.GetCurrencyId()); // Snehal's Code 21 Sept 16
            lstUDFS = CountryBL.GetDisplayUDFS(ddlInvoiceCountry.SelectedItem.Text, currency);
            ddlLegalEntity.Items.Insert(0, new ListItem("--" + Generic_Select_Text + "--", "0"));
            ddlLegalEntity.AppendDataBoundItems = true;
            if (lstUDFS != null && lstUDFS.Count > 0)
            {
                ddlLegalEntity.DataTextField = "Name";
                ddlLegalEntity.DataValueField = "id";
                ddlLegalEntity.DataSource = lstUDFS;
                ddlLegalEntity.DataBind();
                if (Session["IndeedSSO_User"] == null)
                {
                    ddlLegalEntity.Items.Insert(lstUDFS.Count + 1, new ListItem(strGeneric_Invoice_others, "Others"));
                }
            }

            #region Region Code Added by SHRIGANESH 05 December 2016 for Harley's Email on 30th November 2016
            else
            {
                if (Session["IndeedSSO_User"] == null)
                {
                    ddlLegalEntity.DataTextField = "Name";
                    ddlLegalEntity.DataValueField = "id";
                    ddlLegalEntity.Items.Insert(1, new ListItem(strGeneric_Invoice_others, "Others"));
                }
            }
            #endregion

            ddlLegalEntity.SelectedIndex = 0;
            #region vikram for legal entity 03032017
            if (Session["Amount"] != null)
            {
                ApplyGiftCertificate();
            }
            #endregion
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<InvoiceBE> lstInvoice = new List<InvoiceBE>();
            lstInvoice = new List<InvoiceBE>();
            lstInvoice = InvoiceBL.GetdisplayUDFSDetails(ddlLegalEntity.SelectedValue);
            Session["SelectedEntityDetails"] = lstInvoice;
            BindInvoiceUserAddress(lstInvoice);
            SetFreightValues();
            #region vikram for legal entity 03032017
            if (Session["Amount"] != null)
            {
                ApplyGiftCertificate();
            }
            #endregion
            //Session["SelectedEntityDetails"] = lstInvoice;// Added by SHRIGANESH
            //Session.Add("SelectedEntityDetails", lstInvoice);
        }

        /// <summary>
        /// Author  : snehal
        /// Date    : 13-09-16
        /// Scope   : Bind invoice User Addresses
        /// </summary>            
        /// <returns></returns>
        protected void BindInvoiceUserAddress(List<InvoiceBE> lstInvoice)
        {
            try
            {
                Exceptions.WriteInfoLog("Inside BindUserAddress()");
                UserBE UserdetailLanguageWise = new UserBE();
                if (Session["User"] != null)
                {
                    Exceptions.WriteInfoLog("Inside Session[User] != null in BindUserAddress()");
                    UserdetailLanguageWise = Session["User"] as UserBE;
                    UserdetailLanguageWise.EmailId = UserdetailLanguageWise.EmailId;
                    Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.EmailId in BindUserAddress()");
                }
                else if (Session["GuestUser"] != null)
                {
                    Exceptions.WriteInfoLog("Inside Session[GuestUser] != null in BindUserAddress()");
                    string GuestEmailId = Convert.ToString(Session["GuestUser"]);
                    UserdetailLanguageWise.EmailId = GuestEmailId;
                    Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.EmailId in BindUserAddress()");
                }
                Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.LanguageId in BindUserAddress()");
                UserdetailLanguageWise.LanguageId = GlobalFunctions.GetLanguageId();
                Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.CurrencyId in BindUserAddress()");
                UserdetailLanguageWise.CurrencyId = GlobalFunctions.GetCurrencyId();
                Exceptions.WriteInfoLog("Before Calling UserBL.GetSingleObjectDetails in BindUserAddress()");
                UserdetailLanguageWise = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, UserdetailLanguageWise);

                if (Session["GuestUser"] == null)
                {
                    Exceptions.WriteInfoLog("Inside Session[GuestUser] == null in BindUserAddress()");
                    #region
                    if (UserdetailLanguageWise.UserPreDefinedColumns.Count > 0)
                    {
                        Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.UserPreDefinedColumns in BindUserAddress()");
                        UserdetailLanguageWise.UserPreDefinedColumns = UserdetailLanguageWise.UserPreDefinedColumns.FindAll(x => x.IsVisible == true);
                        for (int i = 0; i < lstUser.UserPreDefinedColumns.Count; i++)
                        {
                            if (UserdetailLanguageWise.UserPreDefinedColumns[i].ParentFieldGroup == 0)
                            {
                                ltrColumnName.Text = "<p id=\"pBillingTitle\" class=\"pageText\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</p>";
                            }
                            else
                            {
                                bool bMandatory = UserdetailLanguageWise.UserPreDefinedColumns[i].IsMandatory;
                                string strColumnName = UserdetailLanguageWise.UserPreDefinedColumns[i].SystemColumnName.ToLower();
                                // Removed OR Condition by SHRIGANESH 27 July 2016 Earlier added by Sripal to show last failed order address 
                                // || lstCustomerOrders.CustomerOrderId <= 0                                

                                #region " Added by SNEHAL so that Address Fields are populated from the DataBase 14 Sep 2016"
                                #region Bind From UserTable
                                switch (strColumnName)
                                {
                                    case "predefinedcolumn1":
                                        {
                                            ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                            txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            //txtContact_Name.Value = (!string.IsNullOrEmpty(UserdetailLanguageWise.PredefinedColumn1)) ? UserdetailLanguageWise.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                            if (ddlLegalEntity.Items.Count > 0)
                                            {
                                                if (ddlLegalEntity.SelectedItem.Value == "Others")
                                                {
                                                    txtContact_Name.Value = "";
                                                    txtContact_Name.Attributes.Remove("readonly");
                                                }
                                                else
                                                {
                                                    if (lstInvoice != null)
                                                    {
                                                        txtContact_Name.Value = HttpUtility.HtmlDecode(lstInvoice[0].Name.Trim());
                                                        txtContact_Name.Attributes.Add("readonly", "readonly");
                                                    }
                                                    else
                                                    {
                                                        txtContact_Name.Value = "";
                                                        txtContact_Name.Attributes.Add("readonly", "readonly");
                                                    }
                                                }
                                            }
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn1: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn2":
                                        {
                                            #region Section Added by SHRIGANESH to diaplay CompanyName in delivery address 27 July 2016
                                            ltrDCompany_Name.Text = "<label id=\"lblDCompany_Name\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                            txtDCompany_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDCompany_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            #endregion

                                            ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtCompany.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            //txtCompany.Value = UserdetailLanguageWise.PredefinedColumn2;
                                            //txtCompany.Value = HttpUtility.HtmlDecode(UserdetailLanguageWise.PredefinedColumn2);
                                            if (ddlLegalEntity.Items.Count > 0)
                                            {
                                                if (ddlLegalEntity.SelectedItem.Value == "Others")
                                                {
                                                    txtCompany.Value = "";
                                                    txtCompany.Attributes.Remove("readonly");
                                                }
                                                else
                                                {
                                                    if (lstInvoice != null)
                                                    {
                                                        txtCompany.Value = HttpUtility.HtmlDecode(lstInvoice[0].Name.Trim());
                                                        txtCompany.Attributes.Add("readonly", "readonly");
                                                    }
                                                    else
                                                    {
                                                        txtCompany.Value = "";
                                                        txtCompany.Attributes.Add("readonly", "readonly");
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    case "predefinedcolumn3":
                                        {
                                            ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            if (ddlLegalEntity.Items.Count > 0)
                                            {
                                                if (ddlLegalEntity.SelectedItem.Value == "Others")
                                                {
                                                    txtAddress1.Value = "";
                                                    txtAddress1.Attributes.Remove("readonly");
                                                }
                                                else
                                                {
                                                    if (lstInvoice != null)
                                                    {
                                                        txtAddress1.Value = lstInvoice[0].Address1.Trim();
                                                        txtAddress1.Attributes.Add("readonly", "readonly");
                                                    }
                                                    else
                                                    {
                                                        txtAddress1.Value = "";
                                                        txtAddress1.Attributes.Add("readonly", "readonly");
                                                    }
                                                }
                                            }
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn3: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn4":
                                        {
                                            ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            if (ddlLegalEntity.Items.Count > 0)
                                            {
                                                if (ddlLegalEntity.SelectedItem.Value == "Others")
                                                {
                                                    txtAddress2.Value = "";
                                                    txtAddress2.Attributes.Remove("readonly");
                                                }
                                                else
                                                {
                                                    if (lstInvoice != null)
                                                    {
                                                        txtAddress2.Value = lstInvoice[0].Address2.Trim();
                                                        txtAddress2.Attributes.Add("readonly", "readonly");
                                                    }
                                                    else
                                                    {
                                                        txtAddress2.Value = "";
                                                        txtAddress2.Attributes.Add("readonly", "readonly");
                                                    }
                                                }
                                            }
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn4: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn5":
                                        {
                                            ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            if (ddlLegalEntity.Items.Count > 0)
                                            {
                                                if (ddlLegalEntity.SelectedItem.Value == "Others")
                                                {
                                                    txtTown.Value = "";
                                                    txtTown.Attributes.Remove("readonly");
                                                }
                                                else
                                                {
                                                    if (lstInvoice != null)
                                                    {
                                                        txtTown.Value = lstInvoice[0].Town.Trim();
                                                        txtTown.Attributes.Add("readonly", "readonly");
                                                    }
                                                    else
                                                    {
                                                        txtTown.Value = "";
                                                        txtTown.Attributes.Add("readonly", "readonly");
                                                    }
                                                }
                                            }
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn5: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn6":
                                        {
                                            ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            if (ddlLegalEntity.Items.Count > 0)
                                            {
                                                if (ddlLegalEntity.SelectedItem.Value == "Others")
                                                {
                                                    txtState__County.Value = "";
                                                    txtState__County.Attributes.Remove("readonly");
                                                }
                                                else
                                                {
                                                    if (lstInvoice != null)
                                                    {
                                                        txtState__County.Value = lstInvoice[0].Country.Trim();
                                                        txtState__County.Attributes.Add("readonly", "readonly");
                                                    }
                                                    else
                                                    {
                                                        txtState__County.Value = "";
                                                        txtState__County.Attributes.Add("readonly", "readonly");
                                                    }
                                                }
                                            }
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn6: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn7":
                                        {
                                            ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            if (ddlLegalEntity.Items.Count > 0)
                                            {
                                                if (ddlLegalEntity.SelectedItem.Value == "Others")
                                                {
                                                    txtPostal_Code.Value = "";
                                                    txtPostal_Code.Attributes.Remove("readonly");
                                                }
                                                else
                                                {
                                                    if (lstInvoice != null)
                                                    {
                                                        txtPostal_Code.Value = lstInvoice[0].Post_Code.Trim();
                                                        txtPostal_Code.Attributes.Add("readonly", "readonly");
                                                    }
                                                    else
                                                    {
                                                        txtPostal_Code.Value = "";
                                                        txtPostal_Code.Attributes.Add("readonly", "readonly");
                                                    }
                                                }
                                            }
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn7: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn8":
                                        {
                                            ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            ddlDCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            ddlCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            ddlCountry.ClearSelection();
                                            ddlDCountry.ClearSelection();
                                            try
                                            {
                                                ddlCountry.Items.FindByValue(UserdetailLanguageWise.PredefinedColumn8).Selected = true;
                                                ddlDCountry.Items.FindByValue(UserdetailLanguageWise.PredefinedColumn8).Selected = true;
                                            }
                                            catch (Exception) { }
                                            break;
                                        }
                                    case "predefinedcolumn9":
                                        {
                                            ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            //txtPhone.Value = lstUser.PredefinedColumn9;
                                            if (ddlLegalEntity.Items.Count > 0)
                                            {
                                                if (ddlLegalEntity.SelectedItem.Value == "Others")
                                                {
                                                    txtPhone.Value = "";
                                                    txtPhone.Attributes.Remove("readonly");
                                                }
                                                else
                                                {
                                                    txtPhone.Value = "";
                                                    txtPhone.Attributes.Add("readonly", "readonly");
                                                }
                                            }
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn9: in BindUserAddress()");
                                            break;
                                        }
                                }
                                #endregion
                                #endregion
                            }
                        }

                        if (ddlLegalEntity.Items.Count > 0)
                        {
                            if (ddlLegalEntity.SelectedItem.Value == "Others")
                            {
                                divTaxNo.Visible = true;
                            }
                            else
                            {
                                divTaxNo.Visible = false;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    Exceptions.WriteInfoLog("Inside Else of Session[GuestUser] == null in BindUserAddress()");
                    UserRegistrationBE objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                    Exceptions.WriteInfoLog("After calling UserRegistrationBL.GetUserRegistrationDetails() in BindUserAddress()");
                    UserRegistrationBE objTempUserRegistrationBE = new UserRegistrationBE();
                    objTempUserRegistrationBE.UserRegistrationLanguagesLst = objUserRegistrationBE.UserRegistrationLanguagesLst.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId <= 10);
                    Exceptions.WriteInfoLog("After assigning objTempUserRegistrationBE.UserRegistrationLanguagesLst in BindUserAddress()");

                    for (int i = 0; i < objTempUserRegistrationBE.UserRegistrationLanguagesLst.Count; i++)
                    {
                        UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].RegistrationFieldsConfigurationId);

                        bool bMandatory = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst[i + 1].IsMandatory;
                        //bool bMandatory = false;//objTempUserRegistrationBE.RegistrationFieldsConfigurationBE[i]..IsMandatory;
                        string strColumnName = objRegistrationFieldsConfigurationBE.SystemColumnName.ToLower();

                        #region "Invoice Address"
                        #region Bind Address
                        switch (strColumnName)
                        {
                            case "predefinedcolumn1":
                                {
                                    ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                    txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn1: for Guest User in BindUserAddress()");
                                    //txtContact_Name.Value = (!string.IsNullOrEmpty(lstUser.PredefinedColumn1)) ? lstUser.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;

                                    if (ddlLegalEntity.Items.Count > 0)
                                    {
                                        if (ddlLegalEntity.SelectedItem.Value == "Others")
                                        {
                                            txtContact_Name.Value = "";
                                            txtContact_Name.Attributes.Remove("readonly");
                                        }
                                        else
                                        {
                                            if (lstInvoice != null)
                                            {
                                                txtContact_Name.Value = HttpUtility.HtmlDecode(lstInvoice[0].Name.Trim());
                                                txtContact_Name.Attributes.Add("readonly", "readonly");
                                            }
                                            else
                                            {
                                                txtContact_Name.Value = "";
                                                txtContact_Name.Attributes.Add("readonly", "readonly");
                                            }
                                        }
                                    }

                                    break;
                                }
                            case "predefinedcolumn2":
                                {
                                    #region Section Added by SHRIGANESH to diaplay CompanyName in delivery address 27 July 2016
                                    ltrDCompany_Name.Text = "<label id=\"lblDCompany_Name\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                    txtDCompany_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDCompany_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    #endregion

                                    ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtCompany.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn2: for Guest User in BindUserAddress()");
                                    //txtCompany.Value = lstUser.PredefinedColumn2;
                                    if (ddlLegalEntity.Items.Count > 0)
                                    {
                                        if (ddlLegalEntity.SelectedItem.Value == "Others")
                                        {
                                            txtCompany.Value = "";
                                            txtCompany.Attributes.Remove("readonly");
                                        }
                                        else
                                        {
                                            if (lstInvoice != null)
                                            {
                                                txtCompany.Value = HttpUtility.HtmlDecode(lstInvoice[0].Name.Trim());
                                                txtCompany.Attributes.Add("readonly", "readonly");
                                            }
                                            else
                                            {
                                                txtCompany.Value = "";
                                                txtCompany.Attributes.Add("readonly", "readonly");
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "predefinedcolumn3":
                                {
                                    ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn3: for Guest User in BindUserAddress()");
                                    //txtAddress1.Value = lstUser.PredefinedColumn3;
                                    if (ddlLegalEntity.Items.Count > 0)
                                    {
                                        if (ddlLegalEntity.SelectedItem.Value == "Others")
                                        {
                                            txtAddress1.Value = "";
                                            txtAddress1.Attributes.Remove("readonly");
                                        }
                                        else
                                        {
                                            if (lstInvoice != null)
                                            {
                                                txtAddress1.Value = lstInvoice[0].Address1.Trim();
                                                txtAddress1.Attributes.Add("readonly", "readonly");
                                            }
                                            else
                                            {
                                                txtAddress1.Value = "";
                                                txtAddress1.Attributes.Add("readonly", "readonly");
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "predefinedcolumn4":
                                {
                                    ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn4: for Guest User in BindUserAddress()");
                                    //txtAddress2.Value = lstUser.PredefinedColumn4;
                                    if (ddlLegalEntity.Items.Count > 0)
                                    {
                                        if (ddlLegalEntity.SelectedItem.Value == "Others")
                                        {
                                            txtAddress2.Value = "";
                                            txtAddress2.Attributes.Remove("readonly");
                                        }
                                        else
                                        {
                                            if (lstInvoice != null)
                                            {
                                                txtAddress2.Value = lstInvoice[0].Address2.Trim();
                                                txtAddress2.Attributes.Add("readonly", "readonly");
                                            }
                                            else
                                            {
                                                txtAddress2.Value = "";
                                                txtAddress2.Attributes.Add("readonly", "readonly");
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "predefinedcolumn5":
                                {
                                    ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn5: for Guest User in BindUserAddress()");
                                    //txtTown.Value = lstUser.PredefinedColumn5;
                                    if (ddlLegalEntity.Items.Count > 0)
                                    {
                                        if (ddlLegalEntity.SelectedItem.Value == "Others")
                                        {
                                            txtTown.Value = "";
                                            txtTown.Attributes.Remove("readonly");
                                        }
                                        else
                                        {
                                            if (lstInvoice != null)
                                            {
                                                txtTown.Value = lstInvoice[0].Town.Trim();
                                                txtTown.Attributes.Add("readonly", "readonly");
                                            }
                                            else
                                            {
                                                txtTown.Value = "";
                                                txtTown.Attributes.Add("readonly", "readonly");
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "predefinedcolumn6":
                                {
                                    ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn6: for Guest User in BindUserAddress()");
                                    //txtState__County.Value = lstUser.PredefinedColumn6;
                                    if (ddlLegalEntity.Items.Count > 0)
                                    {
                                        if (ddlLegalEntity.SelectedItem.Value == "Others")
                                        {
                                            txtState__County.Value = "";
                                            txtState__County.Attributes.Remove("readonly");
                                        }
                                        else
                                        {
                                            if (lstInvoice != null)
                                            {
                                                txtState__County.Value = lstInvoice[0].Country.Trim();
                                                txtState__County.Attributes.Add("readonly", "readonly");
                                            }
                                            else
                                            {
                                                txtState__County.Value = "";
                                                txtState__County.Attributes.Add("readonly", "readonly");
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "predefinedcolumn7":
                                {
                                    ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn7: for Guest User in BindUserAddress()");
                                    //txtPostal_Code.Value = lstUser.PredefinedColumn7;
                                    if (ddlLegalEntity.Items.Count > 0)
                                    {
                                        if (ddlLegalEntity.SelectedItem.Value == "Others")
                                        {
                                            txtPostal_Code.Value = "";
                                            txtPostal_Code.Attributes.Remove("readonly");
                                        }
                                        else
                                        {
                                            if (lstInvoice != null)
                                            {
                                                txtPostal_Code.Value = lstInvoice[0].Post_Code.Trim();
                                                txtPostal_Code.Attributes.Add("readonly", "readonly");
                                            }
                                            else
                                            {
                                                txtPostal_Code.Value = "";
                                                txtPostal_Code.Attributes.Add("readonly", "readonly");
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "predefinedcolumn8":
                                {
                                    ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlDCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //ddlCountry.ClearSelection();
                                    //ddlDCountry.ClearSelection();
                                    //try
                                    //{
                                    //    ddlCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //    ddlDCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //}
                                    //catch (Exception) { }
                                    ddlCountry.SelectedValue = "12";
                                    ddlDCountry.SelectedValue = "12";
                                    break;
                                }
                            case "predefinedcolumn9":
                                {
                                    ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn1: for Guest User in BindUserAddress()");
                                    //txtPhone.Value = lstUser.PredefinedColumn9;
                                    if (ddlLegalEntity.Items.Count > 0)
                                    {
                                        if (ddlLegalEntity.SelectedItem.Value == "Others")
                                        {
                                            txtPhone.Value = "";
                                            txtPhone.Attributes.Remove("readonly");
                                        }
                                        else
                                        {
                                            txtPhone.Value = "";
                                            txtPhone.Attributes.Add("readonly", "readonly");
                                        }
                                    }
                                    break;
                                }
                        }
                        #endregion
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind BindCountryDropDown
        /// </summary>            
        /// <returns></returns>
        protected void BindCountryDropDown()
        {
            try
            {
                Exceptions.WriteInfoLog(" Inside BindCountryDropDown()");
                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                ddlCountry.Items.Insert(0, new ListItem("--" + Generic_Select_Text + "--", "0"));
                ddlDCountry.Items.Insert(0, new ListItem("--" + Generic_Select_Text + "--", "0"));
                ddlCountry.AppendDataBoundItems = true;
                ddlDCountry.AppendDataBoundItems = true;
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    if (Session["IndeedSSO_User"] != null || Session["IndeedEmployeeSSO_User"] != null)
                    {
                        lstCountry = lstCountry.FindAll(x => x.RegionCode != "UA");
                        ddlCountry.DataTextField = "CountryName";
                        ddlCountry.DataValueField = "CountryId";
                        Exceptions.WriteInfoLog("Before assingning DataSource to ddlDCountry in BindCountryDropDown()");
                        if (Session["Ship_To_Location"] != null)
                        {
                            ddlCountry.DataSource = lstCountry.FindAll(x => x.CountryCode == Session["Ship_To_Location"].ToString());
                            var objCountry = lstCountry.FindAll(x => x.CountryCode == Session["Ship_To_Location"].ToString());
                            iUserDeliveryCountryID = objCountry.FirstOrDefault().CountryId;
                        }
                        else if (Session["IndeedEmployeeCountry"] != null)
                        {
                            ddlCountry.DataSource = lstCountry.FindAll(x => x.CountryName == Session["IndeedEmployeeCountry"].ToString());
                            var objCountry = lstCountry.FindAll(x => x.CountryName == Session["IndeedEmployeeCountry"].ToString());
                            iUserDeliveryCountryID = objCountry.FirstOrDefault().CountryId;
                        }
                        else
                        {
                            ddlCountry.DataSource = lstCountry;
                        }
                        ddlCountry.DataBind();
                        Exceptions.WriteInfoLog("Before DataBind event of ddlDCountry in BindCountryDropDown()");

                        ddlDCountry.DataTextField = "CountryName";
                        ddlDCountry.DataValueField = "CountryId";
                        if (Session["Ship_To_Location"] != null)
                        {
                            ddlDCountry.DataSource = lstCountry.FindAll(x => x.CountryCode == Session["Ship_To_Location"].ToString());
                        }
                        else if (Session["IndeedEmployeeCountry"] != null)
                        {
                            ddlDCountry.DataSource = lstCountry.FindAll(x => x.CountryName == Session["IndeedEmployeeCountry"].ToString());
                        }
                        else
                        {
                            ddlDCountry.DataSource = lstCountry;
                        }
                        ddlDCountry.DataBind();
                        ddlDCountry.SelectedValue = Convert.ToString(iUserDeliveryCountryID);
                    }
                    else
                    {
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                        {
                            lstCountry = lstCountry.FindAll(x => x.RegionCode != "UA");
                            ddlCountry.DataTextField = "CountryName";
                            ddlCountry.DataValueField = "CountryId";
                            Exceptions.WriteInfoLog("Before assingning DataSource to ddlDCountry in BindCountryDropDown()");
                            ddlCountry.DataSource = lstCountry;
                            ddlCountry.DataBind();
                            Exceptions.WriteInfoLog("Before DataBind event of ddlDCountry in BindCountryDropDown()");

                            ddlDCountry.DataTextField = "CountryName";
                            ddlDCountry.DataValueField = "CountryId";
                            ddlDCountry.DataSource = lstCountry;
                            ddlDCountry.DataBind();
                        }
                        else
                        {
                            if(Session["user"]!=null)
                            {
                                string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(lstUser.PredefinedColumn8)).RegionCode;

                                lstCountry = lstCountry.FindAll(x => x.RegionCode == strRegionCode);
                                ddlCountry.DataTextField = "CountryName";
                                ddlCountry.DataValueField = "CountryId";
                                Exceptions.WriteInfoLog("Before assingning DataSource to ddlDCountry in BindCountryDropDown()");
                                ddlCountry.DataSource = lstCountry;
                                ddlCountry.DataBind();
                                Exceptions.WriteInfoLog("Before DataBind event of ddlDCountry in BindCountryDropDown()");

                                ddlDCountry.DataTextField = "CountryName";
                                ddlDCountry.DataValueField = "CountryId";
                                ddlDCountry.DataSource = lstCountry;
                                ddlDCountry.DataBind();
                                
                              

                            }
                     

                        }


                       
                    }
                }
                else
                {
                    ddlCountry.SelectedIndex = 0;
                }


                Exceptions.WriteInfoLog("Exiting BindCountryDropDown()");
            }
            //}
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 10-09-16
        /// Scope   : rptBasketListing_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptBasketListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Exceptions.WriteInfoLog("Inside rptBasketListing_ItemDataBound");
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl dvUnitPrice = (HtmlGenericControl)e.Item.FindControl("dvUnitPrice");
                    HtmlGenericControl dvBasketRowContainer = (HtmlGenericControl)e.Item.FindControl("dvBasketRowContainer");
                    HtmlGenericControl dvTotalPrice = (HtmlGenericControl)e.Item.FindControl("dvTotalPrice");
                    HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)e.Item.FindControl("hdnShoppingCartProductId");
                    HtmlInputHidden hdnInventory = (HtmlInputHidden)e.Item.FindControl("hdnInventory");
                    HtmlGenericControl divSKUName = (HtmlGenericControl)e.Item.FindControl("divSKUName");
                    HtmlGenericControl divSKUCode = (HtmlGenericControl)e.Item.FindControl("divSKUCode");
                    HtmlGenericControl txtQuantity = (HtmlGenericControl)e.Item.FindControl("txtQuantity");
                    HtmlGenericControl divdvProductName = (HtmlGenericControl)e.Item.FindControl("dvProductName");
                    //ddlPaymentTypes.Attributes.Add("onchange", "javascript: ShowLoader();");
                    //ddlPaymentTypes.AutoPostBack = true;


                    HtmlInputHidden hidePrice = (HtmlInputHidden)e.Item.FindControl("hidePrice");// SNEHAL's Code for Rolls Royce Removed 08 Sept 2016

                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";

                    string strProductPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Price")).ToString("#######0.#0");

                    dvUnitPrice.InnerHtml = "@ " + strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(strProductPrice), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId) + "</span>";
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        dvUnitPrice.InnerHtml = "<span>" + GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(strProductPrice), strCurrencySymbol, intLanguageId) + "</span>";
                        dvTotalPrice.InnerHtml = " = " + "<span>" + GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), strCurrencySymbol, intLanguageId) + "</span>";
                        dPTotal += Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                        dTotalPrice += Convert.ToDecimal(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", intLanguageId));
                    }
                    else
                    {
                        dvUnitPrice.InnerHtml = "@ " + strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(strProductPrice), "", intLanguageId) +/* GlobalFunctions.PointsText(intLanguageId) +*/ "</span>";
                        dvTotalPrice.InnerHtml = " = " + strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", intLanguageId) + /*GlobalFunctions.PointsText(intLanguageId) +*/ "</span>";
                        dPTotal += Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                        dTotalPrice += Convert.ToDecimal(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToDecimal(strProductPrice) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"))), "", intLanguageId));
                    }
                    dvBasketRowContainer.ID = "dvBasketRowContainer" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId"));
                    hdnShoppingCartProductId.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShoppingCartProductId"));
                    hdnInventory.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Inventory"));
                    dTotalWeight += Math.Ceiling(Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "DimensionalWeight")) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity")) * Convert.ToDecimal(0.001));

                    #region Basket Emailcontent
                    strProductDetails += "<tr>";
                    strProductDetails += "<td style='border: 1px solid #ccc; padding:5px;text-align:center'><img src='" + imgProduct.Src + "'/></td>";
                    strProductDetails += "<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + divdvProductName.InnerHtml.Replace("<br>", "") + "</td>";
                    //strProductDetails += "<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + divSKUName.InnerHtml.Replace("<br>", "") + "</td>";
                    strProductDetails += "<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + txtQuantity.InnerHtml.Replace("<br>", "").Replace("@", "") + "</td>";
                    strProductDetails += "<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + dvUnitPrice.InnerHtml.Replace("<br>", "").Replace("@", "") + "</td>";
                    strProductDetails += "<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + dvTotalPrice.InnerHtml.Replace("<br>", "").Replace("=", "") + "</td>";
                    strProductDetails += "</tr><br>";
                    #endregion


                    if (bBackOrderAllowed == false && bBASysStore)
                    // for testing purpose
                    //if (bBackOrderAllowed == false)
                    {
                        Int32 intProductid_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BASYSProductId")),
                            intBaseColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BaseColorId")),
                            intTrimColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "TrimColorId"));

                        PWGlobalEcomm.Stock.Stock objStock = new PWGlobalEcomm.Stock.Stock();
                        NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                            ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                        objStock.Credentials = objNetworkCredentials;
                        PWGlobalEcomm.Stock.StockDetails objStockDetails = new PWGlobalEcomm.Stock.StockDetails();
                        objStockDetails = objStock.LevelEnquiry(intProductid_OASIS, intBaseColorId_OASIS, intTrimColorId_OASIS);

                        if (objStockDetails != null)
                        {
                            HtmlInputHidden hdnStockStatus = (HtmlInputHidden)e.Item.FindControl("hdnStockStatus");
                            hdnStockStatus.Value = Convert.ToString(objStockDetails.StockLevel);
                        }
                    }
                }
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Exceptions.WriteInfoLog("Inside e.Item.ItemType == ListItemType.Footer rptBasketListing_ItemDataBound");
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        strPTotal = dPTotal.ToString();
                    }
                    else
                    {
                        //strPTotal = dPTotal.ToString("##,###,##0.#0");
                        strPTotal = dPTotal.ToString("#######0.#0");
                    }
                    HtmlGenericControl dvTotal = (HtmlGenericControl)e.Item.FindControl("dvTotal");
                    HtmlGenericControl spnTotalPrice = (HtmlGenericControl)e.Item.FindControl("spnTotalPrice");
                    spnTotalPrice.InnerText = Convert.ToString(dTotalPrice);
                    hidePrice.Value = Convert.ToString(dTotalPrice);
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        dvTotal.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dTotalPrice), GlobalFunctions.GetCurrencySymbol(), intLanguageId);

                        int dtotalpoint = Convert.ToInt32(GlobalFunctions.RemovePointsSpanIntheText(dvTotal.InnerHtml));

                        HtmlGenericControl divShipping = (HtmlGenericControl)e.Item.FindControl("divShipping");
                        HtmlGenericControl divTax = (HtmlGenericControl)e.Item.FindControl("divTax");

                        divTax.Attributes.Add("style", "display:none");
                        if (dtotalpoint > lstUser.Points)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_InSufficient_Point, AlertType.Warning);
                        }
                    }
                    else
                    {
                        if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                        {
                            dvTotal.InnerHtml = strCurrencySymbol + "<span>" + Convert.ToString(dTotalPrice) + "</span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId());
                        }
                        else
                        {
                            dvTotal.InnerHtml = strCurrencySymbol + "<span>" + dTotalPrice.ToString("#######0.#0")/* + "</span>" + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId())*/;
                        }
                    }

                    Button aProceed = (Button)e.Item.FindControl("aProceed");
                    aProceed.Text = strSC_Place_Order;

                    //Added By Hardik "04/Oct/2016"
                    Button aEmail = (Button)e.Item.FindControl("aEmail");
                    aEmail.Text = strSC_Email_Me_Basket;

                    HtmlGenericControl spnBsktSubTotal = (HtmlGenericControl)e.Item.FindControl("spnBsktSubTotal");
                    spnBsktSubTotal.InnerText = strsc_basket_subtotal;

                    //Button aEmailbtn = (Button)e.Item.FindControl("aEmail");
                    if (GlobalFunctions.IsEmailMeBasketbtnEnabled())
                    {
                        aEmail.Visible = true;
                    }
                    else
                    {
                        aEmail.Visible = false;
                    }
                    if (Session["budget"] != null)
                    {
                        HtmlGenericControl spnbudget = (HtmlGenericControl)e.Item.FindControl("spnbudget");
                        HtmlGenericControl dvbudget = (HtmlGenericControl)e.Item.FindControl("dvbudget");
                        spnbudget.InnerText = strAvailableBudget;// need to add resource by tripti
                        dvbudget.InnerHtml =GlobalFunctions.GetCurrencySymbol() + Convert.ToString(Session["budget"]);
                    }
                    Exceptions.WriteInfoLog("Exiting rptBasketListing_ItemDataBound");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 15-09-15
        /// Scope   : Bind User Addresses
        /// </summary>            
        /// <returns></returns>
        protected void BindUserAddress()
        {
            try
            {
                Exceptions.WriteInfoLog("Inside BindUserAddress()");
                UserBE UserdetailLanguageWise = new UserBE();
                if (Session["User"] != null)
                {
                    Exceptions.WriteInfoLog("Inside Session[User] != null in BindUserAddress()");
                    UserdetailLanguageWise = Session["User"] as UserBE;
                    UserdetailLanguageWise.EmailId = UserdetailLanguageWise.EmailId;
                    Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.EmailId in BindUserAddress()");
                }
                else if (Session["GuestUser"] != null)
                {
                    Exceptions.WriteInfoLog("Inside Session[GuestUser] != null in BindUserAddress()");
                    string GuestEmailId = Convert.ToString(Session["GuestUser"]);
                    UserdetailLanguageWise.EmailId = GuestEmailId;
                    Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.EmailId in BindUserAddress()");
                }
                Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.LanguageId in BindUserAddress()");
                UserdetailLanguageWise.LanguageId = GlobalFunctions.GetLanguageId();
                Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.CurrencyId in BindUserAddress()");
                UserdetailLanguageWise.CurrencyId = GlobalFunctions.GetCurrencyId();
                Exceptions.WriteInfoLog("Before Calling UserBL.GetSingleObjectDetails in BindUserAddress()");
                UserdetailLanguageWise = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, UserdetailLanguageWise);
                if (Session["GuestUser"] == null)
                {
                    Exceptions.WriteInfoLog("Inside Session[GuestUser] == null in BindUserAddress()");
                    #region
                    if (UserdetailLanguageWise.UserPreDefinedColumns.Count > 0)
                    {
                        Exceptions.WriteInfoLog("Assigining UserdetailLanguageWise.UserPreDefinedColumns in BindUserAddress()");
                        UserdetailLanguageWise.UserPreDefinedColumns = UserdetailLanguageWise.UserPreDefinedColumns.FindAll(x => x.IsVisible == true);
                        for (int i = 0; i < lstUser.UserPreDefinedColumns.Count; i++)
                        {
                            if (UserdetailLanguageWise.UserPreDefinedColumns[i].ParentFieldGroup == 0)
                            {
                                ltrColumnName.Text = "<p id=\"pBillingTitle\" class=\"pageText\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</p>";
                            }
                            else
                            {
                                bool bMandatory = UserdetailLanguageWise.UserPreDefinedColumns[i].IsMandatory;
                                string strColumnName = UserdetailLanguageWise.UserPreDefinedColumns[i].SystemColumnName.ToLower();
                                // Removed OR Condition by SHRIGANESH 27 July 2016 Earlier added by Sripal to show last failed order address 
                                // || lstCustomerOrders.CustomerOrderId <= 0                                

                                #region " Added by SHRIGANESH so taht Address Fields are populated from the DataBase 27 July 2016"
                                #region Bind From UserTable
                                switch (strColumnName)
                                {
                                    case "predefinedcolumn1":
                                        {
                                            ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                            txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtContact_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            //txtContact_Name.Value = (!string.IsNullOrEmpty(UserdetailLanguageWise.PredefinedColumn1)) ? UserdetailLanguageWise.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                            txtContact_Name.Value = (!string.IsNullOrEmpty(UserdetailLanguageWise.PredefinedColumn1)) ? HttpUtility.HtmlDecode(UserdetailLanguageWise.PredefinedColumn1) : HttpUtility.HtmlDecode(lstUser.FirstName) + " " + HttpUtility.HtmlDecode(lstUser.LastName);
                                            txtContact_Name.Attributes.Remove("readonly");
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn1: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn2":
                                        {
                                            #region Section Added by SHRIGANESH to diaplay CompanyName in delivery address 27 July 2016
                                            ltrDCompany_Name.Text = "<label id=\"lblDCompany_Name\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                            txtDCompany_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDCompany_Name.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            #endregion

                                            ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtCompany.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            //txtCompany.Value = UserdetailLanguageWise.PredefinedColumn2;
                                            txtCompany.Value = HttpUtility.HtmlDecode(UserdetailLanguageWise.PredefinedColumn2);
                                            txtCompany.Attributes.Remove("readonly");
                                            break;
                                        }
                                    case "predefinedcolumn3":
                                        {
                                            ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtAddress1.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtAddress1.Value = HttpUtility.HtmlDecode(UserdetailLanguageWise.PredefinedColumn3);
                                            txtAddress1.Attributes.Remove("readonly");
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn3: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn4":
                                        {
                                            ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtAddress2.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtAddress2.Value = HttpUtility.HtmlDecode(UserdetailLanguageWise.PredefinedColumn4);
                                            txtAddress2.Attributes.Remove("readonly");
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn4: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn5":
                                        {
                                            ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtTown.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtTown.Value = HttpUtility.HtmlDecode(UserdetailLanguageWise.PredefinedColumn5);
                                            txtTown.Attributes.Remove("readonly");
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn5: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn6":
                                        {
                                            ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtState__County.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtState__County.Value = HttpUtility.HtmlDecode(UserdetailLanguageWise.PredefinedColumn6);
                                            txtState__County.Attributes.Remove("readonly");
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn6: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn7":
                                        {
                                            ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtPostal_Code.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            txtPostal_Code.Value = HttpUtility.HtmlDecode(UserdetailLanguageWise.PredefinedColumn7);
                                            txtPostal_Code.Attributes.Remove("readonly");
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn7: in BindUserAddress()");
                                            break;
                                        }
                                    case "predefinedcolumn8":
                                        {
                                            ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            ddlDCountry.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            ddlCountry.Attributes.Add("data-value", HttpUtility.HtmlDecode(UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ")));

                                          

                                            try
                                            {
                                                if (Session["IndeedSSO_User"] == null || Session["IndeedEmployeeSSO_User"] != null)
                                                {
                                                    ddlCountry.ClearSelection();
                                                    ddlDCountry.ClearSelection();
                                                    ddlCountry.Items.FindByValue(UserdetailLanguageWise.PredefinedColumn8).Selected = true;
                                                    ddlDCountry.Items.FindByValue(UserdetailLanguageWise.PredefinedColumn8).Selected = true;
                                                }
                                            }
                                                       


                                            catch (Exception) { }
                                            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                                            {
                                                ddlCountry.Enabled = true;
                                               
                                            }
                                            else
                                            {
                                                ddlCountry.Enabled = false;
                                            }
                                            break;
                                        }
                                    case "predefinedcolumn9":
                                        {
                                            ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtDPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                            ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                            txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                            txtPhone.Attributes.Add("data-value", UserdetailLanguageWise.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                            //txtPhone.Value = lstUser.PredefinedColumn9;
                                            txtPhone.Value = HttpUtility.HtmlDecode(lstUser.PredefinedColumn9);
                                            txtPhone.Attributes.Remove("readonly");
                                            Exceptions.WriteInfoLog("Inside case predefinedcolumn9: in BindUserAddress()");
                                            break;
                                        }
                                }
                                #endregion
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    Exceptions.WriteInfoLog("Inside Else of Session[GuestUser] == null in BindUserAddress()");
                    UserRegistrationBE objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                    Exceptions.WriteInfoLog("After calling UserRegistrationBL.GetUserRegistrationDetails() in BindUserAddress()");
                    UserRegistrationBE objTempUserRegistrationBE = new UserRegistrationBE();
                    objTempUserRegistrationBE.UserRegistrationLanguagesLst = objUserRegistrationBE.UserRegistrationLanguagesLst.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId <= 10);
                    Exceptions.WriteInfoLog("After assigning objTempUserRegistrationBE.UserRegistrationLanguagesLst in BindUserAddress()");

                    for (int i = 0; i < objTempUserRegistrationBE.UserRegistrationLanguagesLst.Count; i++)
                    {
                        //short ias= objTempUserRegistrationBE.RegistrationFieldsConfigurationBE[i+1].RegistrationFieldsConfigurationId;

                        UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].RegistrationFieldsConfigurationId);

                        bool bMandatory = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst[i + 1].IsMandatory;
                        //bool bMandatory = false;//objTempUserRegistrationBE.RegistrationFieldsConfigurationBE[i]..IsMandatory;
                        string strColumnName = objRegistrationFieldsConfigurationBE.SystemColumnName.ToLower();

                        #region "Invoice Address"
                        #region Bind Address
                        switch (strColumnName)
                        {
                            case "predefinedcolumn1":
                                {
                                    if (Session["AccessToken"] != null)
                                    {
                                        ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                        txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                        txtDContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                        ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                        txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                        txtContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                        Exceptions.WriteInfoLog("Inside case predefinedcolumn1: for Guest User in BindUserAddress()");
                                        txtContact_Name.Value = Session["FullName"].ToString();
                                        txtContact_Name.Attributes.Remove("readonly");
                                        break;

                                    }
                                    else
                                    {
                                        ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                        txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                        txtDContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                        ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                        txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                        txtContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                        txtContact_Name.Attributes.Remove("readonly");
                                        Exceptions.WriteInfoLog("Inside case predefinedcolumn1: for Guest User in BindUserAddress()");
                                        //txtContact_Name.Value = (!string.IsNullOrEmpty(lstUser.PredefinedColumn1)) ? lstUser.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                        break;
                                    }
                                }
                            case "predefinedcolumn2":
                                {
                                    #region Section Added by SHRIGANESH to diaplay CompanyName in delivery address 27 July 2016
                                    ltrDCompany_Name.Text = "<label id=\"lblDCompany_Name\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                    txtDCompany_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDCompany_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtDCompany_Name.Attributes.Remove("readonly");
                                    #endregion

                                    ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtCompany.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtCompany.Attributes.Remove("readonly");
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn2: for Guest User in BindUserAddress()");
                                    //txtCompany.Value = lstUser.PredefinedColumn2;
                                    break;
                                }
                            case "predefinedcolumn3":
                                {
                                    ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtAddress1.Attributes.Remove("readonly");
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn3: for Guest User in BindUserAddress()");
                                    //txtAddress1.Value = lstUser.PredefinedColumn3;
                                    break;
                                }
                            case "predefinedcolumn4":
                                {
                                    ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtDAddress2.Attributes.Remove("readonly");

                                    ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtAddress2.Attributes.Remove("readonly");
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn4: for Guest User in BindUserAddress()");
                                    //txtAddress2.Value = lstUser.PredefinedColumn4;
                                    break;
                                }
                            case "predefinedcolumn5":
                                {
                                    ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtDTown.Attributes.Remove("readonly");
                                    ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtTown.Attributes.Remove("readonly");
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn5: for Guest User in BindUserAddress()");
                                    //txtTown.Value = lstUser.PredefinedColumn5;
                                    break;
                                }
                            case "predefinedcolumn6":
                                {
                                    ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtDState__County.Attributes.Remove("readonly");

                                    ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtState__County.Attributes.Remove("readonly");
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn6: for Guest User in BindUserAddress()");
                                    //txtState__County.Value = lstUser.PredefinedColumn6;
                                    break;
                                }
                            case "predefinedcolumn7":
                                {
                                    ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtDPostal_Code.Attributes.Remove("readonly");

                                    ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtPostal_Code.Attributes.Remove("readonly");

                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn7: for Guest User in BindUserAddress()");
                                    //txtPostal_Code.Value = lstUser.PredefinedColumn7;
                                    break;
                                }
                            case "predefinedcolumn8":
                                {
                                    ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlDCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //ddlCountry.ClearSelection();
                                    //ddlDCountry.ClearSelection();
                                    //try
                                    //{
                                    //    ddlCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //    ddlDCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //}
                                    //catch (Exception) { }

                                   
               
                                    ddlCountry.SelectedValue = "12";
                                    ddlDCountry.SelectedValue = "12";

                                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                                    {
                                        ddlCountry.Attributes.Remove("disabled");
                                    }
                                    else
                                    {
                                        string strcountryid =Convert.ToString( objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[1].FeatureDefaultValue);
                                        ddlCountry.SelectedValue = strcountryid;
                                        ddlDCountry.SelectedValue = strcountryid;
                                        ddlCountry.Attributes.Add("disabled", "disabled");
                                    }
                                    break;
                                }
                            case "predefinedcolumn9":
                                {
                                    ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtDPhone.Attributes.Remove("readonly");

                                    ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    txtPhone.Attributes.Remove("readonly");
                                    Exceptions.WriteInfoLog("Inside case predefinedcolumn1: for Guest User in BindUserAddress()");
                                    //txtPhone.Value = lstUser.PredefinedColumn9;
                                    break;
                                }
                        }
                        #endregion
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author: Tripti
        /// date: 27-03-2017
        /// </summary>
        private void budgetwebservices()
        {
            List<BudgetNode> lstBudgetNode = new List<BudgetNode>();

            if (Session["GuestUser"] == null)
            {
                UserBE objUserBE = (UserBE)HttpContext.Current.Session["User"];
                List<IndeedSalesBE.BA_Budget> lstBudget = new List<IndeedSalesBE.BA_Budget>();

                lstBudget = IndeedSalesBL.GetBudgetPerUser(objUserBE.UserId, GlobalFunctions.GetCurrencyId());

                if (lstBudget != null)
                {
                    Budgets objBudget = new Budgets();
                    objBudget = Budget_OASIS.GetBudgetDetails(lstBudget[0].Budget_Code, lstBudget[0].Division_ID, lstBudget[0].Currency_Symbol);

                    //  objBudget = Budget_OASIS.GetBudgetDetails("4210697769", 265, "USD");
                    if (objBudget.BudgetNodes != null)
                    {
                        lstBudgetNode = objBudget.BudgetNodes.ToList<BudgetNode>();
                        Session["Budget"] = lstBudgetNode[0].WebCurrOutstandingBudget;
                    }
                    else
                    {
                        Session["Budget"] = "0.00";
                    }
                }

                else
                {
                    Budgets objBudget = new Budgets();
                    objBudget = Budget_OASIS.GetBudgetDetails(Budgetvalue, DivisionID, BudgetCurrencySymbol);

                    if (objBudget.BudgetNodes != null)
                    {
                        lstBudgetNode = objBudget.BudgetNodes.ToList<BudgetNode>();
                        Session["Budget"] = lstBudgetNode[0].WebCurrOutstandingBudget;
                    }
                    else
                    {
                        Session["Budget"] = "0.00";
                    }
                }
            }
            else
            {
                Session["Budget"] = "0.00";
            }

        }

        /// <summary>
        /// Author: Tripti
        /// date: 27-03-2017
        /// </summary>
        private void paymentselection()
        {
            string strPaymentMethod = ddlPaymentTypes.SelectedValue;
            if (strPaymentMethod != "0" && strPaymentMethod != "")
            {
                string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                if (strSplitPaymentMethod.Length > 1)
                {
                    strPaymentMethod = strSplitPaymentMethod[0];
                }
                List<object> lstPaymentTypes = new List<object>();
                List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();

                lstPaymentTypes = ShoppingCartBL.getpoints(Convert.ToInt16(strPaymentMethod));
                lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                if (GlobalFunctions.IsPointsEnbled())
                {
                    if (lstPaymentTypesBE[0].IsPoint)
                    {
                        Session["ispoint"] = 1;
                    }
                    else
                    {
                        Session["ispoint"] = 0;
                    }
                }
                if (lstPaymentTypesBE[0].IsBudget)
                {
                    // Session["isBudget"] = 1;
                    budgetwebservices();
                }
                else
                {
                    //Session["isBudget"] = 0;
                }
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 15-09-15
        /// Scope   : Bind User Addresses
        /// </summary>            
        /// <returns></returns>
        protected void BindUserAddress_bkp()
        {
            try
            {
                if (Session["GuestUser"] == null)
                {
                    #region
                    if (lstUser.UserPreDefinedColumns.Count > 0)
                    {
                        lstUser.UserPreDefinedColumns = lstUser.UserPreDefinedColumns.FindAll(x => x.IsVisible == true);
                        for (int i = 0; i < lstUser.UserPreDefinedColumns.Count; i++)
                        {
                            if (lstUser.UserPreDefinedColumns[i].ParentFieldGroup == 0)
                            {
                                ltrColumnName.Text = "<p id=\"pBillingTitle\" class=\"pageText\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</p>";
                            }
                            else
                            {
                                bool bMandatory = lstUser.UserPreDefinedColumns[i].IsMandatory;
                                string strColumnName = lstUser.UserPreDefinedColumns[i].SystemColumnName.ToLower();
                                if (lstCustomerOrders == null || lstCustomerOrders.CustomerOrderId <= 0) //Added by Sripal Or condition
                                {
                                    #region Bind From UserTable
                                    switch (strColumnName)
                                    {
                                        case "predefinedcolumn1":
                                            {
                                                ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                                txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDContact_Name.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtContact_Name.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtContact_Name.Value = (!string.IsNullOrEmpty(lstUser.PredefinedColumn1)) ? lstUser.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                                break;
                                            }
                                        case "predefinedcolumn2":
                                            {
                                                ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtCompany.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtCompany.Value = lstUser.PredefinedColumn2;
                                                break;
                                            }
                                        case "predefinedcolumn3":
                                            {
                                                ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress1.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress1.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtAddress1.Value = lstUser.PredefinedColumn3;
                                                break;
                                            }
                                        case "predefinedcolumn4":
                                            {
                                                ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress2.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress2.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtAddress2.Value = lstUser.PredefinedColumn4;
                                                break;
                                            }
                                        case "predefinedcolumn5":
                                            {
                                                ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDTown.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtTown.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtTown.Value = lstUser.PredefinedColumn5;
                                                break;
                                            }
                                        case "predefinedcolumn6":
                                            {
                                                ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDState__County.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtState__County.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtState__County.Value = lstUser.PredefinedColumn6;
                                                break;
                                            }
                                        case "predefinedcolumn7":
                                            {
                                                ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPostal_Code.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPostal_Code.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtPostal_Code.Value = lstUser.PredefinedColumn7;
                                                break;
                                            }
                                        case "predefinedcolumn8":
                                            {
                                                ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlDCountry.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlCountry.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlCountry.ClearSelection();
                                                ddlDCountry.ClearSelection();
                                                try
                                                {
                                                    ddlCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                                    ddlDCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                                }
                                                catch (Exception) { }
                                                break;
                                            }
                                        case "predefinedcolumn9":
                                            {
                                                ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPhone.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPhone.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtPhone.Value = lstUser.PredefinedColumn9;
                                                break;
                                            }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Bind From CustomerOrder Table
                                    switch (strColumnName)
                                    {
                                        case "predefinedcolumn1":
                                            {

                                                ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDContact_Name.Value = lstCustomerOrders.DeliveryContactPerson;
                                                txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDContact_Name.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtContact_Name.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtContact_Name.Value = lstCustomerOrders.InvoiceContactPerson;
                                                break;
                                            }
                                        case "predefinedcolumn2":
                                            {
                                                ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtCompany.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                txtCompany.Value = lstCustomerOrders.InvoiceCompany;
                                                break;
                                            }
                                        case "predefinedcolumn3":
                                            {
                                                ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress1.Value = lstCustomerOrders.DeliveryAddress1;
                                                txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress1.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress1.Value = lstCustomerOrders.InvoiceAddress1;
                                                txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress1.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn4":
                                            {
                                                ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDAddress2.Value = lstCustomerOrders.DeliveryAddress2;
                                                txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDAddress2.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtAddress2.Value = lstCustomerOrders.InvoiceAddress2;
                                                txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtAddress2.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn5":
                                            {
                                                ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDTown.Value = lstCustomerOrders.DeliveryCity;
                                                txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDTown.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtTown.Value = lstCustomerOrders.InvoiceCity;
                                                txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtTown.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn6":
                                            {
                                                ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDState__County.Value = lstCustomerOrders.DeliveryCounty;
                                                txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDState__County.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtState__County.Value = lstCustomerOrders.InvoiceCounty;
                                                txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtState__County.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn7":
                                            {
                                                ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPostal_Code.Value = lstCustomerOrders.DeliveryPostalCode;
                                                txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPostal_Code.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPostal_Code.Value = lstCustomerOrders.InvoicePostalCode;
                                                txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPostal_Code.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                        case "predefinedcolumn8":
                                            {
                                                ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlDCountry.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlDCountry.ClearSelection();

                                                ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                ddlCountry.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                ddlCountry.ClearSelection();
                                                try
                                                {
                                                    ddlDCountry.Items.FindByValue(lstCustomerOrders.DeliveryCountry).Selected = true;
                                                    ddlCountry.Items.FindByValue(lstCustomerOrders.InvoiceCountry).Selected = true;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Exceptions.WriteExceptionLog(ex);
                                                }
                                                break;
                                            }
                                        case "predefinedcolumn9":
                                            {
                                                ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtDPhone.Value = lstCustomerOrders.DeliveryPhone;
                                                txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtDPhone.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));

                                                ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                                txtPhone.Value = lstCustomerOrders.InvoicePhone;
                                                txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                                txtPhone.Attributes.Add("data-value", lstUser.UserPreDefinedColumns[i].ColumnName.Replace("__", "/").Replace("_", " "));
                                                break;
                                            }
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    UserRegistrationBE objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                    UserRegistrationBE objTempUserRegistrationBE = new UserRegistrationBE();
                    objTempUserRegistrationBE.UserRegistrationLanguagesLst = objUserRegistrationBE.UserRegistrationLanguagesLst.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId <= 10);

                    for (int i = 0; i < objTempUserRegistrationBE.UserRegistrationLanguagesLst.Count; i++)
                    {
                        UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].RegistrationFieldsConfigurationId);

                        bool bMandatory = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst[i + 1].IsMandatory;
                        //bool bMandatory = false;//objTempUserRegistrationBE.RegistrationFieldsConfigurationBE[i]..IsMandatory;
                        string strColumnName = objRegistrationFieldsConfigurationBE.SystemColumnName.ToLower();

                        #region "Invoice Address"
                        #region Bind Address
                        switch (strColumnName)
                        {
                            case "predefinedcolumn1":
                                {
                                    ltrDContact_Name.Text = "<label id=\"lblDContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span class='text-danger'>*</span>" : "");
                                    txtDContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrContact_Name.Text = "<label id=\"lblContactName\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtContact_Name.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtContact_Name.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtContact_Name.Value = (!string.IsNullOrEmpty(lstUser.PredefinedColumn1)) ? lstUser.PredefinedColumn1 : lstUser.FirstName + " " + lstUser.LastName;
                                    break;
                                }
                            case "predefinedcolumn2":
                                {
                                    ltrCompany.Text = "<label id=\"lblCompany\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtCompany.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtCompany.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtCompany.Value = lstUser.PredefinedColumn2;
                                    break;
                                }
                            case "predefinedcolumn3":
                                {
                                    ltrDAddress1.Text = "<label id=\"lblDAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress1.Text = "<label id=\"lblAddress1\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress1.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress1.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtAddress1.Value = lstUser.PredefinedColumn3;
                                    break;
                                }
                            case "predefinedcolumn4":
                                {
                                    ltrDAddress2.Text = "<label id=\"lblDAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrAddress2.Text = "<label id=\"lblAddress2\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtAddress2.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtAddress2.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtAddress2.Value = lstUser.PredefinedColumn4;
                                    break;
                                }
                            case "predefinedcolumn5":
                                {
                                    ltrDTown.Text = "<label id=\"lblDTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrTown.Text = "<label id=\"lblTown\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtTown.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtTown.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtTown.Value = lstUser.PredefinedColumn5;
                                    break;
                                }
                            case "predefinedcolumn6":
                                {
                                    ltrdState__County.Text = "<label id=\"lblDState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrState__County.Text = "<label id=\"lblState__County\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtState__County.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtState__County.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtState__County.Value = lstUser.PredefinedColumn6;
                                    break;
                                }
                            case "predefinedcolumn7":
                                {
                                    ltrDPostalCode.Text = "<label id=\"lblDPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrPostalCode.Text = "<label id=\"lblPostalCode\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPostal_Code.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPostal_Code.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtPostal_Code.Value = lstUser.PredefinedColumn7;
                                    break;
                                }
                            case "predefinedcolumn8":
                                {
                                    ltrDCountry.Text = "<label id=\"lblDCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlDCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlDCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrCountry.Text = "<label id=\"lblCountry\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    ddlCountry.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    ddlCountry.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //ddlCountry.ClearSelection();
                                    //ddlDCountry.ClearSelection();
                                    //try
                                    //{
                                    //    ddlCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //    ddlDCountry.Items.FindByValue(lstUser.PredefinedColumn8).Selected = true;
                                    //}
                                    //catch (Exception) { }
                                    ddlCountry.SelectedValue = "12";
                                    ddlDCountry.SelectedValue = "12";
                                    break;
                                }
                            case "predefinedcolumn9":
                                {
                                    ltrDPhone.Text = "<label id=\"lblDPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtDPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtDPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));

                                    ltrPhone.Text = "<label id=\"lblPhone\" class=\"customLabel\">" + objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " ") + "</label>" + (bMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    txtPhone.Attributes.Add("rel", (bMandatory == true ? "m" : ""));
                                    txtPhone.Attributes.Add("data-value", objTempUserRegistrationBE.UserRegistrationLanguagesLst[i].LabelTitle.Replace("__", "/").Replace("_", " "));
                                    //txtPhone.Value = lstUser.PredefinedColumn9;
                                    break;
                                }
                        }
                        #endregion
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind User delivery Address
        /// </summary>            
        /// <returns></returns>
        protected void BindPaymentTypes()
        {
            try
            {
                Session["Budget"] = null;
                Exceptions.WriteInfoLog("Inside BindPaymentTypes()");
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails()");
                List<FeatureBE> objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                Exceptions.WriteInfoLog("After assingning objFeatures in BindPaymentTypes()");
                if (objFeatures != null)
                {
                    List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;
                    Exceptions.WriteInfoLog("After assingning objLstFeatureValueBE in BindPaymentTypes()");
                    if (objLstFeatureValueBE != null)
                    {
                        FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                        Exceptions.WriteInfoLog("After assingning objFeatureValueBE in BindPaymentTypes()");
                        if (objFeatureValueBE != null)
                        {

                            List<object> lstPaymentTypes = new List<object>();
                            List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                            Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetAllPaymentTypesByLanguageID() in BindPaymentTypes()");
                            //lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypes();


                            lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypesByLanguageID(GlobalFunctions.GetLanguageId(), iUserTypeID);/*User Type*/
                            Exceptions.WriteInfoLog("After calling ShoppingCartBL.GetAllPaymentTypesByLanguageID() in BindPaymentTypes()");

                            #region CODE ADDED BY SHRIGANESH TO REMOVE CREDIT CARD OPTIONS FOR SPECIFIC CURRENCIES AS PER DAVID'S EMAIL 06 DECEMBER 2016
                            //Code Only for Nissan Dealers
                            lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                            List<StoreBE.StoreCurrencyBE> objStoresCur = StoreBL.GetCurrency();
                            if (lstPaymentTypesBE[0].IsBudget)
                            {
                                budgetwebservices();
                            }
                            if (lstPaymentTypes.Count > 0)
                            {
                                ddlPaymentTypes.DataTextField = "PaymentTypeRef";
                                ddlPaymentTypes.DataValueField = "PaymentMethod";

                                var objCurrency = objStoresCur.FindAll(x => x.ValidForAdflex == false);
                                foreach (StoreBE.StoreCurrencyBE oCurrecny in objCurrency)
                                {
                                    if (oCurrecny.CurrencyId == GlobalFunctions.GetCurrencyId())
                                    {
                                        var pay = lstPaymentTypesBE.FindAll(x => x.PaymentMethod.Contains('C'));
                                        lstPaymentTypesBE.RemoveAll(Items => pay.Contains(Items));
                                    }
                                }



                            #endregion

                                ddlPaymentTypes.DataSource = lstPaymentTypesBE;
                                Exceptions.WriteInfoLog("After DataBinding ddlPaymentTypes in BindPaymentTypes()");
                                ddlPaymentTypes.DataBind();
                                ListItem litem = new ListItem();
                                if (lstPaymentTypesBE.Count >= 1)
                                {
                                    #region Vikram for Legal Entity
                                    string DefaultPaymentMethod = ""; // vikram for Legal entity 

                                    if (lstPaymentTypesBE.Count > 1)
                                    {
                                        PaymentTypesBE objPayBE = lstPaymentTypesBE.FirstOrDefault(x => x.IsDefault == "Y");
                                        if (objPayBE != null)
                                        {
                                            if (objPayBE.PaymentMethod != null)
                                            {

                                                litem.Text = strSC_Payment_Types;// "Please select";
                                                litem.Value = "0";
                                                ddlPaymentTypes.Items.Insert(0, litem);

                                            }
                                        }

                                        else
                                        {
                                            litem.Text = strSC_Payment_Types;// "Please select";
                                            litem.Value = "0";
                                            ddlPaymentTypes.Items.Insert(0, litem);
                                        }
                                    }

                                    if (ddlPaymentTypes.SelectedValue.Contains("|"))
                                    {
                                        string[] items = ddlPaymentTypes.SelectedValue.Split('|');
                                        Int16 iPaymentTypeId = Convert.ToInt16(items[0]);
                                        lstPaymentTypes = ShoppingCartBL.GetPaymentdetailsById(iPaymentTypeId, GlobalFunctions.GetLanguageId());
                                        lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                                        if (!lstPaymentTypesBE[0].InvoiceAccountId)
                                        {
                                            hdisLegalEntity.Value = "false";
                                        }
                                        else
                                        {
                                            hdisLegalEntity.Value = "True";
                                        }
                                    }
                                    else
                                    {
                                        hdisLegalEntity.Value = "false";
                                    }
                                    #endregion
                                }


                            }
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Before calling RemovePaymentDropdown() in BindPaymentTypes()");
                            RemovePaymentDropdown();
                            Exceptions.WriteInfoLog("After calling RemovePaymentDropdown() in BindPaymentTypes()");
                        }
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Before calling RemovePaymentDropdown() in BindPaymentTypes()");
                        RemovePaymentDropdown();
                        Exceptions.WriteInfoLog("After calling RemovePaymentDropdown() in BindPaymentTypes()");
                    }
                }
                else
                {
                    Exceptions.WriteInfoLog("Before calling RemovePaymentDropdown() in BindPaymentTypes()");
                    RemovePaymentDropdown();
                    Exceptions.WriteInfoLog("After calling RemovePaymentDropdown() in BindPaymentTypes()");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void RemovePaymentDropdown()
        {
            try
            {
                Exceptions.WriteInfoLog("Inside RemovePaymentDropdown()");
                ddlPaymentTypes.Visible = false;
                ddlPaymentTypes.Attributes.Remove("onchange");
                Exceptions.WriteInfoLog("after executing RemovePaymentDropdown()");
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #region Snehal - 05 01 2017 Customer Ref UDF

        /// <summary>
        /// Author  : Snehal Jadhav
        /// Date    : 05 01 2017
        /// Scope   : Bind Customer Ref UDF Fields
        /// </summary>            
        /// <returns></returns>
        protected void BindCustomerRefUDFFields()
        {
            try
            {
                if (!Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                {
                    tblCustRefUDFS.Controls.Clear();
                    tblCustRefUDFS.Visible = true;
                    TableRow trUDFS = null;
                    TableCell tcUDFS = null;
                    TextBox txtUDFS = null;
                    Label lblUDFS = null;

                    RequiredFieldValidator rfvUDF = null;
                    string str = Convert.ToString(ddlPaymentTypes.SelectedValue);
                    string[] strArr = str.Split('|');

                    List<UDFCustRefBE> lstCustRefUDF = PaymentTypesBL.GetCustRefUDFSDetails<UDFCustRefBE>(Convert.ToInt16(GlobalFunctions.GetLanguageId()), Convert.ToInt32(strArr[0]));
                    for (int j = 0; j < lstCustRefUDF.Count; j++)
                    {
                        if (lstCustRefUDF[j].IsVisible)
                        {
                            trUDFS = new TableRow();
                            tcUDFS = new TableCell();
                            lblUDFS = new Label();

                            lblUDFS.ID = "lbl" + lstCustRefUDF[j].CustomerRefID;
                            lblUDFS.Text = lstCustRefUDF[j].DisplayName + " :" + (lstCustRefUDF[j].IsMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                            lblUDFS.CssClass = "LabelText customLabel";
                            tcUDFS.HorizontalAlign = HorizontalAlign.Left;
                            tcUDFS.Width = Unit.Percentage(32);
                            tcUDFS.Controls.Add(lblUDFS);
                            trUDFS.Cells.Add(tcUDFS);
                            // trUDFS.Attributes.Add("class", "paymentid");

                            tcUDFS = new TableCell();
                            txtUDFS = new TextBox();
                            txtUDFS.ID = "txtCustomRef" + lstCustRefUDF[j].CustomerRefID;
                            if (Convert.ToString(lstCustRefUDF[j].MaximumLength) != "-1")
                            {
                                txtUDFS.MaxLength = lstCustRefUDF[j].MaximumLength;
                            }
                            //txtUDFS.TextMode = TextBoxMode.MultiLine;
                            if (lstCustRefUDF[j].IsMandatory)
                            {
                                rfvUDF = new RequiredFieldValidator();
                                rfvUDF.ID = "rfv1" + txtUDFS.ID.ToString();
                                rfvUDF.ControlToValidate = txtUDFS.ID.ToString();
                                rfvUDF.ErrorMessage = strRequired;// "Required";
                                rfvUDF.Display = ValidatorDisplay.Dynamic;
                                rfvUDF.CssClass = "errortext";
                                tcUDFS.Controls.Add(rfvUDF);
                            }
                            txtUDFS.CssClass = "InputField form-control input-sm customInput";
                            tcUDFS.Controls.Add(txtUDFS);
                            tcUDFS.Width = Unit.Percentage(62);
                            trUDFS.Cells.Add(tcUDFS);

                            tblCustRefUDFS.Rows.Add(trUDFS);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #endregion


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 7-10-15
        /// Scope   : Bind UDF Fields
        /// </summary>            
        /// <returns></returns>
        protected void BindUDFFields(bool bSetValues)
        {
            try
            {
                Exceptions.WriteInfoLog("Inside BindUDFFields()");
                #region BindUDF Fields
                List<object> lstPaymentTypes = new List<object>();
                List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetAllPaymentTypesByLanguageID() in BindUDFFields()");

                lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypesByLanguageID(GlobalFunctions.GetLanguageId(), iUserTypeID);/*User Type*/
                Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetAllPaymentTypesByLanguageID() in BindUDFFields()");
                if (lstPaymentTypes.Count > 0)
                {
                    Exceptions.WriteInfoLog("Inside lstPaymentTypes.Count > 0 in BindUDFFields()");
                    lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();
                    List<object> lstObj = new List<object>();
                    lstObj = ShoppingCartBL.GetUDFS(0);

                    if (lstObj != null && lstObj.Count > 0)
                    {

                        Exceptions.WriteInfoLog("Inside lstObj != null && lstObj.Count > 0 in BindUDFFields()");
                        List<UDFBE> lstUDF = new List<UDFBE>();
                        List<UDFBE> lstUDFFilterBE = new List<UDFBE>();
                        lstUDF = lstObj.Cast<UDFBE>().ToList();

                        tblUDFS.Controls.Clear();
                        tblUDFS.Visible = true;
                        TableRow trUDFS = null;
                        TableCell tcUDFS = null;
                        TextBox txtUDFS = null;
                        Label lblUDFS = null;
                        RegularExpressionValidator revNumeric = null;
                        RegularExpressionValidator revCase = null;
                        RequiredFieldValidator rfvUDF = null;
                        RequiredFieldValidator rfvCase = null;
                        RangeValidator rvRange = null;
                        DropDownList ddlPickListItems = null;
                        CustomerOrderBE.UDFValuesBE objUDFValues;

                        for (int i = 0; i < lstPaymentTypesBE.Count; i++)
                        {
                            lstUDFFilterBE = new List<UDFBE>();
                            lstUDFFilterBE = lstUDF.FindAll(x => x.PaymentTypeID == lstPaymentTypesBE[i].PaymentTypeID && x.LanguageId == GlobalFunctions.GetLanguageId());/*User Type*/
                            for (int j = 0; j < lstUDFFilterBE.Count; j++)
                            {
                                if (lstUDFFilterBE[j].IsVisible || lstUDFFilterBE[j].IsPoint || lstUDFFilterBE[j].IsBudget)
                                {
                                    string strUDFValue = string.Empty;
                                    if (lstCustomerOrders != null && lstCustomerOrders.UDFValues != null && lstCustomerOrders.UDFValues.Count > 0 && bSetValues)
                                    {
                                        objUDFValues = new CustomerOrderBE.UDFValuesBE();
                                        objUDFValues = lstCustomerOrders.UDFValues.FirstOrDefault(x => x.UDFOasisID == lstUDFFilterBE[j].UDFOasisID);
                                        if (objUDFValues != null)
                                            strUDFValue = objUDFValues.UDFValue;
                                    }
                                    trUDFS = new TableRow();
                                    tcUDFS = new TableCell();
                                    lblUDFS = new Label();

                                    lblUDFS.ID = "lbl" + lstUDFFilterBE[j].UDFOasisID;
                                    lblUDFS.Text = lstUDFFilterBE[j].Caption + " :" + (lstUDFFilterBE[j].IsMandatory == true ? "<span  class='text-danger'>*</span>" : "");
                                    lblUDFS.CssClass = "LabelText customLabel";
                                    tcUDFS.HorizontalAlign = HorizontalAlign.Left;
                                    tcUDFS.Width = Unit.Percentage(32);
                                    tcUDFS.Controls.Add(lblUDFS);
                                    trUDFS.Cells.Add(tcUDFS);

                                    if (lstUDFFilterBE[j].IsBudget || lstUDFFilterBE[j].IsPoint)
                                    {
                                        trUDFS.Attributes.Add("style", "display:none;");
                                    }
                                    else
                                    {
                                        if (Session["Approver"] != null)
                                        {
                                            if (lstUDFFilterBE[j].SequenceNo.Equals(2))
                                            {
                                                trUDFS.Attributes.Add("style", "display:none;");
                                            }
                                            if (lstUDFFilterBE[j].SequenceNo.Equals(5))
                                            {
                                                trUDFS.ID = "EventId";
                                                trUDFS.Attributes.Add("style", "display:none;");
                                            }
                                            if (lstUDFFilterBE[j].SequenceNo.Equals(6))
                                            {
                                                trUDFS.Attributes.Add("style", "display:none;");
                                            }
                                            if (lstUDFFilterBE[j].SequenceNo.Equals(7))
                                            {
                                                trUDFS.Attributes.Add("style", "display:none;");
                                            }
                                            if (lstUDFFilterBE[j].SequenceNo.Equals(8))
                                            {
                                                trUDFS.Attributes.Add("style", "display:none;");
                                            }
                                        }
                                        else
                                        {
                                            if (Session["budget"] != null)
                                            {
                                                if (lstUDFFilterBE[j].SequenceNo.Equals(2))
                                                {
                                                    trUDFS.Attributes.Add("style", "display:none;");
                                                }
                                                if (lstUDFFilterBE[j].SequenceNo.Equals(5))
                                                {
                                                   
                                                    trUDFS.Attributes.Add("style", "display:none;");
                                                }
                                            }
                                            else
                                            {
                                                trUDFS.Attributes.Add("class", "paymentid paymentid" + lstUDFFilterBE[j].PaymentTypeID);
                                            }
                                        }

                                    }
                                    if (lstUDFFilterBE[j].RuleType == "A")
                                    {
                                        #region RULE TYPE A
                                        Exceptions.WriteInfoLog("Inside lstUDFFilterBE[j].RuleType == A in BindUDFFields()");
                                        tcUDFS = new TableCell();
                                        txtUDFS = new TextBox();
                                        txtUDFS.ID = "txt" + lstUDFFilterBE[j].UDFOasisID;
                                        if (Convert.ToString(lstUDFFilterBE[j].MaximumLength) != "-1")
                                        {
                                            txtUDFS.MaxLength = lstUDFFilterBE[j].MaximumLength;
                                        }

                                        #region Added By Hardik on "2/March/2017"
                                        if (objStoreBE.StoreId == 471) //417 for Danskebank 382 for local
                                        {
                                            if (txtUDFS.ID == "txt2" && lstUDFFilterBE[j].UDFOasisID == 2)
                                            {
                                                txtUDFS.Text = "";
                                                txtUDFS.Attributes.Add("runat", "server");
                                                txtUDFS.AutoPostBack = true;
                                                txtUDFS.TextChanged += new System.EventHandler(this.txtUDFS_TextChanged);
                                            }
                                            if (txtUDFS.ID == "txt4" && lstUDFFilterBE[j].UDFOasisID == 4)
                                            {
                                                txtUDFS.Text = "";
                                                txtUDFS.Enabled = false;
                                            }
                                        }
                                        #endregion

                                        if (lstUDFFilterBE[j].CaseRule == "Y")
                                        {
                                            revCase = new RegularExpressionValidator();
                                            revCase.ID = "rev1" + txtUDFS.ID.ToString();
                                            revCase.ControlToValidate = txtUDFS.ID.ToString();
                                            revCase.ValidationExpression = "^[A-Z]*$";
                                            revCase.ErrorMessage = strUppercase;// "Text can only be in Uppercase";
                                            revCase.Display = ValidatorDisplay.Dynamic;
                                            revCase.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                            revCase.CssClass = "errortext";
                                            tcUDFS.Controls.Add(revCase);
                                        }
                                        if (lstUDFFilterBE[j].IsMandatory)
                                        {
                                            rfvUDF = new RequiredFieldValidator();
                                            rfvUDF.ID = "rfv1" + txtUDFS.ID.ToString();
                                            rfvUDF.ControlToValidate = txtUDFS.ID.ToString();
                                            rfvUDF.ErrorMessage = strRequired;// "Required";
                                            rfvUDF.Display = ValidatorDisplay.Dynamic;
                                            rfvUDF.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                            rfvUDF.CssClass = "errortext";
                                            tcUDFS.Controls.Add(rfvUDF);
                                        }
                                        txtUDFS.CssClass = "InputField form-control input-sm customInput";
                                        tcUDFS.Controls.Add(txtUDFS);
                                        tcUDFS.Width = Unit.Percentage(62);
                                        trUDFS.Cells.Add(tcUDFS);
                                        if (bSetValues)
                                            txtUDFS.Text = strUDFValue;

                                        if (lstUDFFilterBE[j].IsPoint)
                                        {
                                            if (Session["dShpCartPrice"] != null)
                                            {
                                                var total = Convert.ToString(Session["dShpCartPrice"]);
                                                txtUDFS.Text = total /*+ GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId())*/;
                                                txtUDFS.Enabled = false;
                                            }
                                        }

                                        if (lstUDFFilterBE[j].IsBudget)
                                        {
                                            txtUDFS.Text = Budgetvalue;
                                            Session["Approver"] = true;
                                        }
                                        if (Session["budget"] != null)
                                        {

                                            if (lstUDFFilterBE[j].SequenceNo.Equals(2))
                                            {
                                                txtUDFS.Text = team;
                                            }
                                            if (lstUDFFilterBE[j].SequenceNo.Equals(5))
                                            {
                                                txtUDFS.Text = "NA";
                                            }
                                        }
                                        if (Session["Approver"] != null)
                                        {
                                           
                                            if (lstUDFFilterBE[j].SequenceNo.Equals(6))
                                            {
                                                txtUDFS.Text = Approver_name;
                                            }
                                            if (lstUDFFilterBE[j].SequenceNo.Equals(7))
                                            {
                                                txtUDFS.Text = Approver_email;
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (lstUDFFilterBE[j].RuleType == "B")
                                    {
                                        #region RULE TYPE B
                                        Exceptions.WriteInfoLog("Inside lstUDFFilterBE[j].RuleType == B in BindUDFFields()");
                                        tcUDFS = new TableCell();
                                        txtUDFS = new TextBox();
                                        rvRange = new RangeValidator();
                                        revNumeric = new RegularExpressionValidator();
                                        txtUDFS.ID = "txt" + lstUDFFilterBE[j].UDFOasisID;
                                        rvRange.ControlToValidate = txtUDFS.ID.ToString();
                                        if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1" || Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                        {
                                            if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1" && Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                            {
                                                rvRange.MinimumValue = Convert.ToString(lstUDFFilterBE[j].MinimumValue);
                                                rvRange.MaximumValue = Convert.ToString(lstUDFFilterBE[j].MaximumValue);
                                                strSC_UDF_Minimum_Maximum_Range = strSC_UDF_Minimum_Maximum_Range.Replace("@min", Convert.ToString(lstUDFFilterBE[j].MinimumValue));
                                                strSC_UDF_Minimum_Maximum_Range = strSC_UDF_Minimum_Maximum_Range.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumValue));
                                                rvRange.ErrorMessage = strSC_UDF_Minimum_Maximum_Range;// "Enter value between " + lstUDFFilterBE[j].MinimumValue + " and " + lstUDFFilterBE[j].MaximumValue;
                                            }
                                            else if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1")
                                            {
                                                rvRange.MinimumValue = Convert.ToString(lstUDFFilterBE[j].MinimumValue);
                                                strSC_UDF_Range2 = strSC_UDF_Range2.Replace("@min", Convert.ToString(lstUDFFilterBE[j].MinimumValue));
                                                rvRange.ErrorMessage = strSC_UDF_Range2;// "Enter value more or equal to " + lstUDFFilterBE[j].MinimumValue;
                                            }
                                            else if (Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                            {
                                                rvRange.MaximumValue = Convert.ToString(lstUDFFilterBE[j].MaximumValue);
                                                strSC_UDF_Range3 = strSC_UDF_Range3.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumValue));
                                                rvRange.ErrorMessage = strSC_UDF_Range3;// "Enter value less or equal to " + lstUDFFilterBE[j].MaximumValue;
                                            }
                                        }
                                        revNumeric.ControlToValidate = txtUDFS.ID;
                                        if (Convert.ToString(lstUDFFilterBE[j].DecimalPlaces) == "-1")
                                        {
                                            revNumeric.ValidationExpression = @"\d+";
                                            revNumeric.Text = strSC_Numeric;// "Enter only numbers";
                                        }
                                        else
                                        {
                                            revNumeric.ValidationExpression = @"^\d+(?:\.\d{" + lstUDFFilterBE[j].DecimalPlaces + "})?$";
                                            strSC_NumberMinDecimal = strSC_NumberMinDecimal.Replace("@num", Convert.ToString(lstUDFFilterBE[j].DecimalPlaces));
                                            revNumeric.Text = strSC_NumberMinDecimal;// "Enter only numbers with " + lstUDFFilterBE[j].DecimalPlaces + " decimal places";
                                        }
                                        revNumeric.SetFocusOnError = true;
                                        revNumeric.Display = ValidatorDisplay.None;
                                        rvRange.Display = ValidatorDisplay.None;
                                        revNumeric.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                        rvRange.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                        txtUDFS.CssClass = "InputField form-control input-sm customInput";
                                        revNumeric.CssClass = "errortext";
                                        rvRange.CssClass = "errortext";
                                        tcUDFS.Controls.Add(txtUDFS);
                                        tcUDFS.Controls.Add(revNumeric);
                                        tcUDFS.Controls.Add(rvRange);
                                        tcUDFS.Width = Unit.Pixel(475);
                                        trUDFS.Cells.Add(tcUDFS);
                                        if (bSetValues)
                                            txtUDFS.Text = strUDFValue;
                                        #endregion
                                    }
                                    else if (lstUDFFilterBE[j].RuleType == "E")
                                    {
                                        #region RULE TYPE E
                                        Exceptions.WriteInfoLog("Inside lstUDFFilterBE[j].RuleType == E in BindUDFFields()");
                                        List<object> lstObjPicklist = new List<object>();
                                        List<PickListItemsBE> lstPicklist = new List<PickListItemsBE>();
                                        lstObjPicklist = ShoppingCartBL.GetUDFSValues(lstUDFFilterBE[j].PaymentTypeID, lstUDFFilterBE[j].SequenceNo);
                                        if (lstObjPicklist != null && lstObjPicklist.Count > 0)
                                        {
                                            #region WORKDAY specific code for UDFS selection
                                            if (objStoreBE.StoreId == 443)
                                            {
                                                lstPicklist = lstObjPicklist.Cast<PickListItemsBE>().ToList();
                                                tcUDFS = new TableCell();
                                                ddlPickListItems = new DropDownList();
                                                ddlPickListItems.ID = "ddl" + lstUDFFilterBE[j].UDFOasisID;
                                                ddlPickListItems.DataSource = lstPicklist;
                                                ddlPickListItems.DataTextField = "PayValue";
                                                ddlPickListItems.DataValueField = "ID";
                                                ddlPickListItems.CssClass = "InputField form-control input-sm customInput";
                                                ddlPickListItems.DataBind();

                                                ddlPickListItems.Items.Insert(0, new ListItem("--" + Generic_Select_Text + "--", "0"));
                                                lstPicklist = lstPicklist.FindAll(x => x.IsDefault == "Y");
                                                if (lstPicklist.Count > 0)
                                                {
                                                    ddlPickListItems.SelectedValue = Convert.ToString(lstPicklist[0].ID);
                                                }
                                                else
                                                {
                                                    // Below LOC is commented by shriganesh as it is not required for workday store, 23 May'17
                                                    //ddlPickListItems.SelectedValue = "0";
                                                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_EnableCouponCode").FeatureValues[0].IsEnabled)
                                                    {
                                                        if (string.IsNullOrEmpty(txtCouponCode.Value))
                                                        {
                                                            ddlPickListItems.SelectedIndex = 1;
                                                            ddlPickListItems.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            ddlPickListItems.SelectedIndex = 4;
                                                            ddlPickListItems.Enabled = false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ddlPickListItems.SelectedIndex = 3;
                                                        ddlPickListItems.Enabled = false;
                                                    }
                                                }
                                                if (lstUDFFilterBE[j].IsMandatory)
                                                {
                                                    rfvUDF = new RequiredFieldValidator();
                                                    rfvUDF.ID = "rfv1" + ddlPickListItems.ID;
                                                    rfvUDF.ControlToValidate = ddlPickListItems.ID.ToString();
                                                    rfvUDF.ErrorMessage = strRequired;// "Required";
                                                    rfvUDF.Display = ValidatorDisplay.Dynamic;
                                                    rfvUDF.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                                    rfvUDF.CssClass = "text-danger";
                                                    rfvUDF.InitialValue = "0";
                                                    tcUDFS.Controls.Add(rfvUDF);
                                                }

                                                ddlPickListItems.CssClass = "InputField form-control input-sm customInput ";
                                                tcUDFS.Controls.Add(ddlPickListItems);
                                                tcUDFS.Width = Unit.Pixel(475);
                                                trUDFS.Cells.Add(tcUDFS);
                                                try
                                                {
                                                    if (bSetValues && !string.IsNullOrEmpty(strUDFValue))
                                                        ddlPickListItems.Items.FindByText(strUDFValue).Selected = true;
                                                }
                                                catch (Exception) { }
                                            }
                                            #endregion

                                            #region GENERAL code for remaining stores
                                            else
                                            {
                                                lstPicklist = lstObjPicklist.Cast<PickListItemsBE>().ToList();
                                                tcUDFS = new TableCell();
                                                ddlPickListItems = new DropDownList();
                                                ddlPickListItems.ID = "ddl" + lstUDFFilterBE[j].UDFOasisID;
                                                ddlPickListItems.DataSource = lstPicklist;
                                                ddlPickListItems.DataTextField = "PayValue";
                                                ddlPickListItems.DataValueField = "ID";
                                                ddlPickListItems.CssClass = "InputField form-control input-sm customInput";
                                                ddlPickListItems.DataBind();

                                                ddlPickListItems.Items.Insert(0, new ListItem("--" + Generic_Select_Text + "--", "0"));
                                                lstPicklist = lstPicklist.FindAll(x => x.IsDefault == "Y");
                                                if (lstPicklist.Count > 0)
                                                {
                                                    ddlPickListItems.SelectedValue = Convert.ToString(lstPicklist[0].ID);
                                                }
                                                else
                                                {
                                                    ddlPickListItems.SelectedValue = "0";
                                                }
                                                if (lstUDFFilterBE[j].IsMandatory)
                                                {
                                                    rfvUDF = new RequiredFieldValidator();
                                                    rfvUDF.ID = "rfv1" + ddlPickListItems.ID;
                                                    rfvUDF.ControlToValidate = ddlPickListItems.ID.ToString();
                                                    rfvUDF.ErrorMessage = strRequired;// "Required";
                                                    rfvUDF.Display = ValidatorDisplay.Dynamic;
                                                    rfvUDF.ValidationGroup = Convert.ToString(lstUDFFilterBE[j].PaymentTypeID);
                                                    rfvUDF.CssClass = "text-danger";
                                                    rfvUDF.InitialValue = "0";
                                                    tcUDFS.Controls.Add(rfvUDF);
                                                }

                                                ddlPickListItems.CssClass = "InputField form-control input-sm customInput ";
                                                tcUDFS.Controls.Add(ddlPickListItems);
                                                tcUDFS.Width = Unit.Pixel(475);
                                                trUDFS.Cells.Add(tcUDFS);
                                                try
                                                {
                                                    if (bSetValues && !string.IsNullOrEmpty(strUDFValue))
                                                        ddlPickListItems.Items.FindByText(strUDFValue).Selected = true;
                                                }
                                                catch (Exception) { }
                                            }
                                            #endregion                                       
                                        }
                                        #endregion
                                    }
                                    tblUDFS.Rows.Add(trUDFS);
                                }
                            }
                        }
                    }
                }
                #endregion
                Exceptions.WriteInfoLog("Exiting BindUDFFields()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind User delivery Address
        /// </summary>            
        /// <returns></returns>
        protected void BindDeliveryAddress()
        {
            try
            {
                Exceptions.WriteInfoLog("Inside BindDeliveryAddress()");
                if (Session["GuestUser"] == null)
                {
                    Exceptions.WriteInfoLog("Inside Session[GuestUser] == null");
                    if (lstUser.UserDeliveryAddress.Count > 0)
                    {
                        ddlDAddressTitle.DataTextField = "AddressTitle";
                        ddlDAddressTitle.DataValueField = "DeliveryAddressId";
                        Exceptions.WriteInfoLog("Before assigning datasource to ddlDAddressTitle");
                        if (Session["IndeedSSO_User"] != null || Session["IndeedEmployeeSSO_User"] != null)
                        {
                            if (Session["Ship_To_Location"] != null || Session["IndeedEmployeeCountry"] != null)
                            {
                                ddlDAddressTitle.DataSource = lstUser.UserDeliveryAddress.FindAll(x => x.PreDefinedColumn8 == Convert.ToString(iUserDeliveryCountryID));
                            }
                        }
                        else
                        {
                            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                            {
                               ddlDAddressTitle.DataSource = lstUser.UserDeliveryAddress;
                            }
                            else
                            {
                                if (Session["user"] != null)
                                {
                                    List<CountryBE> lstCountry = new List<CountryBE>();
                                    lstCountry = CountryBL.GetAllCountries();
                                    string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(lstUser.PredefinedColumn8)).RegionCode;
                                    lstCountry = lstCountry.FindAll(x => x.RegionCode == strRegionCode);
                                    ddlDAddressTitle.DataSource = lstUser.UserDeliveryAddress.Where(x => lstCountry.Any(y => x.PreDefinedColumn8 == Convert.ToString(y.CountryId))).ToList();

                                }
                                else { ddlDAddressTitle.DataSource = lstUser.UserDeliveryAddress; }
                               
                            }


                           
                        }
                        Exceptions.WriteInfoLog("After assigning datasource to ddlDAddressTitle");
                        ddlDAddressTitle.DataBind();
                        Exceptions.WriteInfoLog("After Calling dataBind event for ddlDAddressTitle");
                    }
                    ListItem litem = new ListItem();
                    litem.Text = strNew_Delivery_Address;// "Add New";
                    litem.Value = "0";
                    ddlDAddressTitle.Items.Insert(0, litem);
                }
                else
                {
                    Exceptions.WriteInfoLog("Inside else of Session[GuestUser] == null");
                    ddlDAddressTitle.DataTextField = "AddressTitle";
                    ddlDAddressTitle.DataValueField = "DeliveryAddressId";

                    ListItem litem = new ListItem();
                    litem.Text = strNew_Delivery_Address;// "Add New";
                    litem.Value = "0";
                    ddlDAddressTitle.Items.Insert(0, litem);

                }
                Exceptions.WriteInfoLog("Exiting BindDeliveryAddress()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 15-09-15
        /// Scope   : aCheckOut_ServerClick of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void aProceed_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (Session["Budget"] != null)
                {
                    HtmlGenericControl spnTotalPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTotalPrice");
                    decimal baskettotal = Convert.ToDecimal(spnTotalPrice.InnerText);

                    if (baskettotal > Convert.ToDecimal(Session["Budget"]))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strBudgetErrorMsg, AlertType.Failure);
                        return;
                    }
                }

                #region Added By Snehal - 06 01 2017
                string strCust = Convert.ToString(ddlPaymentTypes.SelectedValue);
                string[] strArr = strCust.Split('|');
                if (strArr[0] != "58")
                {
                    if (!Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                    {
                        string strErrormessage = string.Empty;
                        List<UDFCustRefBE> lstCustRefUDF = PaymentTypesBL.GetCustRefUDFSDetails<UDFCustRefBE>(Convert.ToInt16(GlobalFunctions.GetLanguageId()), Convert.ToInt32(strArr[0]));
                        if (lstCustRefUDF[0].IsVisible)
                        {
                            tblCustRefUDFS.Visible = true;

                            TextBox txtUDFS = (TextBox)tblCustRefUDFS.FindControl("txtCustomRef" + lstCustRefUDF[0].CustomerRefID);
                            if (Convert.ToString(lstCustRefUDF[0].MaximumLength) != "-1")
                            {
                                if (txtUDFS.Text.Trim().Length > lstCustRefUDF[0].MaximumLength)
                                {
                                    strSC_UDF_Max_Character = strSC_UDF_Max_Character.Replace("@max", Convert.ToString(lstCustRefUDF[0].MaximumLength));
                                    strErrormessage += lstCustRefUDF[0].Caption + " " + strSC_UDF_Max_Character + "<br>";
                                }
                            }


                            if (lstCustRefUDF[0].IsMandatory)
                            {
                                if (GlobalFunctions.IsEmpty(txtUDFS.Text.Trim()))
                                    strErrormessage += lstCustRefUDF[0].Caption + ": " + strRequired + "<br>";
                            }


                            if (!string.IsNullOrEmpty(strErrormessage))
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strErrormessage, AlertType.Failure);
                                return;
                            }
                            else
                            {
                                Session["CustomerRef"] = txtUDFS.Text.Trim();
                                if (txtUDFS.Text != null && txtUDFS.Text != "")
                                {
                                    Session["UDFUSer"] = lstCustRefUDF[0].Caption + "-" + txtUDFS.Text.Trim() + ",";
                                }
                            }
                        }
                        else
                        {
                            Session["CustomerRef"] = "";
                        }
                    }
                }
                else
                {
                    Session["CustomerRef"] = "";
                }
                #endregion


                Exceptions.WriteInfoLog("Inside Place Order Button");
                /*Added by sripal for non payment*/
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After Calling StoreBL.GetStoreDetails() in Place Order Button");
                List<FeatureBE> objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                Exceptions.WriteInfoLog("After assingning objFeatures in Place Order Button");
                string strShipEmail = "";
                if (rbService1.Checked == true)
                {
                    strShipEmail = spnService1Text.InnerText;
                }
                else if (rbService2.Checked == true)
                {
                    strShipEmail = spnService2Text.InnerText;
                }
                else if (rbService3.Checked == true)
                {
                    strShipEmail = spnService3Text.InnerText;
                }

                Session["strShipEmail"] = strShipEmail;

                if (objFeatures != null)
                {
                    Exceptions.WriteInfoLog("Inside objFeatures != null in Place Order Button");
                    List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;
                    Exceptions.WriteInfoLog("After assingning objLstFeatureValueBE in Place Order Button");
                    if (objLstFeatureValueBE != null)
                    {
                        FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);

                        if (objFeatureValueBE == null)
                        {
                            if (!string.IsNullOrEmpty(txtCouponCode.Value) || !string.IsNullOrEmpty(hidCouponCode.Value))
                            {
                                Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(txtCouponCode.Value) || !string.IsNullOrEmpty(hidCouponCode.Value) in Place Order Button");
                                #region
                                string str = hidCouponCode.Value;
                                string strShippmentType = "";
                                //string strShippmentType = (rbService1.Checked) ? "1" : "2";
                                if (rbService1.Checked == true)
                                {
                                    strShippmentType = "1";
                                }
                                else if (rbService2.Checked == true)
                                {
                                    strShippmentType = "2";
                                }
                                else if (rbService3.Checked == true)
                                {
                                    strShippmentType = "3";
                                }
                                Exceptions.WriteInfoLog("Before Calling CalculateCoupon() in Place Order Button");
                                string strCoupon = CalculateCoupon(hidCouponCode.Value, strShippmentType, Convert.ToInt16(ddlCountry.SelectedValue));
                                Exceptions.WriteInfoLog("After Calling CalculateCoupon() in Place Order Button");
                                string strShipmentType = strCoupon.Split(',')[7].Split(':')[1].ToString();
                                strShipmentType = strShipmentType.Replace("}]", "").Trim('\"');

                                if (!string.IsNullOrEmpty(strCoupon))
                                {
                                    string strStatus = strCoupon.Split(',')[0].Split(':')[1].ToString();
                                    strStatus = strStatus.Trim('\"');

                                    string strGoodsTotal = strCoupon.Split(',')[1].Split(':')[1].ToString();
                                    strGoodsTotal = strGoodsTotal.Trim('\"');

                                    if (!string.Equals(strStatus.ToLower(), "success"))
                                    {
                                        //GlobalFunctions.ShowModalAlertMessages(this, strStatus, AlertType.Failure);
                                        return;
                                    }
                                    else
                                    {
                                        #region
                                        string SZPrice = "";
                                        string EZPrice = "";
                                        string PSPrice = "";
                                        string SZtax = "";
                                        string EZtax = "";
                                        string PStax = "";
                                        if (strShipmentType == "0")
                                        {
                                            SZPrice = strCoupon.Split(',')[2].Split(':')[1].ToString();
                                            SZPrice = SZPrice.Trim('\"');

                                            EZPrice = strCoupon.Split(',')[3].Split(':')[1].ToString();
                                            EZPrice = EZPrice.Trim('\"');

                                            SZtax = strCoupon.Split(',')[4].Split(':')[1].ToString();
                                            SZtax = SZtax.Trim('\"');

                                            EZtax = strCoupon.Split(',')[5].Split(':')[1].ToString();
                                            EZtax = EZtax.Trim('\"');

                                        }
                                        else if (strShipmentType == "1")
                                        {
                                            SZPrice = strCoupon.Split(',')[2].Split(':')[1].ToString();
                                            SZPrice = SZPrice.Trim('\"');

                                            SZtax = strCoupon.Split(',')[4].Split(':')[1].ToString();
                                            SZtax = SZtax.Trim('\"');

                                        }
                                        else if (strShipmentType == "2")
                                        {
                                            EZPrice = strCoupon.Split(',')[3].Split(':')[1].ToString();
                                            EZPrice = EZPrice.Trim('\"');

                                            EZtax = strCoupon.Split(',')[5].Split(':')[1].ToString();
                                            EZtax = EZtax.Trim('\"');

                                        }
                                        double dTax = 0, dFreightPrice = 0;
                                        if (rbService1.Checked)
                                        {
                                            dTax = Convert.ToDouble(SZtax);
                                            dFreightPrice = Convert.ToDouble(SZPrice);
                                        }
                                        else if (rbService2.Checked)
                                        {
                                            dTax = Convert.ToDouble(EZtax);
                                            dFreightPrice = Convert.ToDouble(EZPrice);
                                        }
                                        else if (rbService3.Checked)
                                        {
                                            dTax = Convert.ToDouble(PStax);
                                            dFreightPrice = Convert.ToDouble(PSPrice);
                                        }
                                        double dGoodsTotal = Convert.ToDouble(strGoodsTotal);
                                        double dFullTotal = 0;
                                        dFullTotal = dGoodsTotal + dTax + dFreightPrice;
                                        if (dFullTotal > 0)
                                        {
                                            GlobalFunctions.ShowModalAlertMessages(this, strsc_insufficient_couponcode_balance, AlertType.Failure);
                                            return;
                                        }
                                        else { }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                GlobalFunctions.ShowModalAlertMessages(this, strSC_Require_Coupon, AlertType.Failure);
                                return;
                            }
                        }
                    }
                }

                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                {
                    if (ddlInvoiceCountry.SelectedIndex == 0)
                    {

                    }
                    if (txtTaxNo.Value != null)
                    {
                        Session["TaxNumberValue"] = txtTaxNo.Value;
                    }
                }

                Exceptions.WriteInfoLog("Before Calling ValidateDetails() in Place Order Button");
                if (!ValidateDetails())
                {
                    return;
                }
                else
                {
                    #region
                    //add/update delivery address
                    Exceptions.WriteInfoLog("Before calling SaveAddressDetails() in Place Order button");
                    if (SaveAddressDetails())
                    {
                        Exceptions.WriteInfoLog("After calling SaveAddressDetails() in Place Order button");
                        #region
                        //add/update order data
                        if (UpdateCustomerOrderData())
                        {
                            #region
                            Exceptions.WriteInfoLog("After calling UpdateCustomerOrderData() in Place Order button");
                            objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                            if (objFeatures != null)
                            {
                                List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;

                                if (objLstFeatureValueBE != null)
                                {
                                    FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                                    if (objFeatureValueBE == null)
                                    {
                                        if (!string.IsNullOrEmpty(hidCouponCode.Value))
                                        {
                                            SaveUDFValues(Convert.ToInt32(1));
                                            //For other payment methods 
                                            Session["transaction"] = "true";
                                            Exceptions.WriteInfoLog("Before Redirecting to OrderConfirmation from Place Order button");
                                            Response.RedirectToRoute("OrderConfirmation");
                                        }
                                        else
                                        {
                                            string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                                            string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                                            if (strSplitPaymentMethod.Length > 1)
                                            {
                                                #region
                                                //If payment method is credit card
                                                if (strSplitPaymentMethod[1] == "C")
                                                {
                                                    SaveUDFValues(Convert.ToInt32(strSplitPaymentMethod[0]));
                                                    Exceptions.WriteInfoLog("Before Redirecting to checkoutPagePurchase from Place Order button");
                                                    Response.RedirectToRoute("checkoutPagePurchase");
                                                }
                                                else if (strSplitPaymentMethod[0] == "58")
                                                {
                                                    Exceptions.WriteInfoLog("Before Redirecting to Telecash from Place Order button");
                                                    Response.RedirectToRoute("checkoutPagePayment");
                                                }


                                                //For testing purpose                                                
                                                else
                                                {
                                                    SaveUDFValues(Convert.ToInt32(strSplitPaymentMethod[0]));
                                                    //For other payment methods 
                                                    Session["transaction"] = "true";
                                                    Exceptions.WriteInfoLog("Before Redirecting to OrderConfirmation from Place Order button");
                                                    Response.RedirectToRoute("OrderConfirmation");
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Error_Processing_Payment, AlertType.Failure);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                                        string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                                        if (strSplitPaymentMethod.Length > 1)
                                        {
                                            #region
                                            //If payment method is credit card
                                            if (strSplitPaymentMethod[1] == "C")
                                            {
                                                SaveUDFValues(Convert.ToInt32(strSplitPaymentMethod[0]));
                                                Exceptions.WriteInfoLog("Before Redirecting to checkoutPagePurchase from Place Order button");
                                                Response.RedirectToRoute("checkoutPagePurchase");
                                            }
                                            else if (strSplitPaymentMethod[0] == "58")
                                            {
                                                Exceptions.WriteInfoLog("Before Redirecting to Telecash from Place Order button");
                                                Response.RedirectToRoute("checkoutPagePayment");
                                            }
                                            //For testing purpose                                            
                                            else
                                            {
                                                if (!Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                                                {
                                                    // udfs will not be saved in case GC value is 0
                                                    SaveUDFValues(Convert.ToInt32(strSplitPaymentMethod[0]));
                                                }
                                                //For other payment methods 
                                                Session["transaction"] = "true";
                                                Exceptions.WriteInfoLog("Before Redirecting to OrderConfirmation from Place Order button");
                                                Response.RedirectToRoute("OrderConfirmation");
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Error_Processing_Payment, AlertType.Failure);
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Updating_Order_Details, AlertType.Failure);
                            //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is some error while updating order details.", AlertType.Failure);
                        }
                        #endregion
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strSC_Error_Update_Delivery_Address, AlertType.Failure);
                        //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is some error while updating delivery Address details.", AlertType.Failure);
                    }
                    #endregion
                }
                Exceptions.WriteInfoLog("Exiting Place Order Button CLick");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Hardik Gohil
        /// Date    : 05-10-16
        /// Scope   : aEmail_ServerClick of the ShoppingCart_Payment page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void aEmail_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (rptBasketListing.Items.Count > 0)
                {
                    eCounter = 0;
                    eCounter = ShoppingCartBL.UpdateEmailCounter();
                    SetFreightValues();
                    BindPriceDetails();
                    List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                    EmailManagementBE objEmail = new EmailManagementBE();
                    objEmail.EmailTemplateName = "Email Me a Copy of My Basket";
                    objEmail.LanguageId = Convert.ToInt16(intLanguageId);
                    objEmail.EmailTemplateId = 0;

                    //string strSiteLogoDir = string.Empty;
                    DirectoryInfo dirSiteLogo = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Images/SiteLogo"));
                    FileInfo[] logoFileInfo = dirSiteLogo.GetFiles();

                    HtmlGenericControl spnShippingPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnShippingPrice");
                    HtmlGenericControl dvTotal = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("dvTotal");
                    HtmlGenericControl spnTaxPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTaxPrice");

                    lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");

                    #region New email content
                    if (lstEmail != null && lstEmail.Count > 0)
                    {
                        //Get logo
                        foreach (FileInfo logoFile in logoFileInfo)
                        {
                            if (logoFile.Name.ToLower().Contains("sitelogo"))
                            {
                                strSiteLogoDir = "<a href='" + host + "index'><img height='60px' src='" + host + "Images/SiteLogo/" + logoFile.Name + "'></a>";
                            }
                        }

                        string strBillingAddress = txtContact_Name.Value + "<br />" + txtAddress1.Value + "<br />" + txtTown.Value + "<br />" + txtState__County.Value + "<br />" + txtPostal_Code.Value + "<br />" + ddlCountry.SelectedItem.Text;

                        string str, strTotEmail;
                        str = dvTotal.InnerText;
                        str = str.Replace("<span>", "");
                        str = str.Replace("</span>", "");
                        //lblItemPrice.Text = hidePrice.Value;
                        //lblPostagePacking.Text = spnShippingPrice.InnerText;
                        //lblTotalBeforeVAT.Text = Convert.ToString(Convert.ToDecimal(hidePrice.Value) + Convert.ToDecimal(spnShippingPrice.InnerText));
                        //lblVAT.Text = spnTaxPrice.InnerText;
                        strTotEmail = Convert.ToString(Convert.ToDecimal(hidePrice.Value) + Convert.ToDecimal(spnShippingPrice.InnerText) + Convert.ToDecimal(spnTaxPrice.InnerText));

                        strProductPriceEmail = "<tr><td>" + strsc_basket_subtotal + "</td> <td>" + dvTotal.InnerHtml + "</td></tr>";
                        StoreBE objStoreBE = StoreBL.GetStoreDetails();
                        if (objStoreBE.StoreId != 435)
                        {
                            strProductPriceEmail += "<tr><td>" + strShipping + "</td> <td>" + strCurrencySymbol + spnShippingPrice.InnerText + "</td></tr>";
                            strProductPriceEmail += "<tr><td>" + strTax + "</td> <td>" + strCurrencySymbol + spnTaxPrice.InnerText + "</td></tr>";
                        }
                        strProductPriceEmail += "<tr><td>" + strTotal_Text + "</td> <td>" + strCurrencySymbol + strTotEmail + "</td></tr><br />";

                        if (addressCheckbox.Checked == true)
                        {
                            strDeliveryAddress = txtContact_Name.Value + "<br />" + txtAddress1.Value + "<br />" + txtTown.Value + "<br />" + txtState__County.Value + "<br />" + txtPostal_Code.Value + "<br />" + ddlCountry.SelectedItem.Text;
                        }
                        else
                        {
                            strDeliveryAddress = txtDContact_Name.Value + "<br />" + txtDAddress1.Value + "<br />" + txtDTown.Value + "<br />" + txtDState__County.Value + "<br />" + txtDPostal_Code.Value + "<br />" + ddlDCountry.SelectedItem.Text;
                        }

                        if (rbService1.Checked == true)
                        {
                            strShippingMethodEmail = spnService1Text.InnerText;
                        }
                        if (rbService2.Checked == true)
                        {
                            strShippingMethodEmail = spnService2Text.InnerText;
                        }
                        if (rbService3.Checked == true)
                        {
                            strShippingMethodEmail = spnService3Text.InnerText;
                        }

                        string strFromEmailId = lstEmail[0].FromEmailId;
                        string strCCEmailId = lstEmail[0].CCEmailId;
                        string strBCCEmailId = lstEmail[0].BCCEmailId;
                        bool IsHtml = lstEmail[0].IsHtml;
                        string strSubject = "";
                        if (objStoreBE.StoreId != 435)
                        {
                            strSubject = HttpUtility.HtmlDecode(lstEmail[0].Subject) + " - " + Convert.ToString(eCounter);
                        }
                        else
                        {
                            strSubject = HttpUtility.HtmlDecode(lstEmail[0].Subject);
                        }
                        string strMailBody = lstEmail[0].Body;
                        //string strProductDetails = Session["strProductBasketdetails"] as string;
                        string strProductDetails = ViewState["strProductBasketdetails"].ToString();
                        //string strProductPriceEmail = ViewState["strProductPriceEmail"].ToString();
                        strMailBody = strMailBody.Replace("@ProductDetails", Convert.ToString(strProductDetails));
                        strMailBody = strMailBody.Replace("@sitelogo", strSiteLogoDir);
                        strMailBody = strMailBody.Replace("@SiteName", objStoreBE.StoreName);
                        strMailBody = strMailBody.Replace("@quoteCounter", Convert.ToString(eCounter));
                        strMailBody = strMailBody.Replace("@spnProdPrice", Convert.ToString(strProductPriceEmail));
                        strMailBody = strMailBody.Replace("@BillingAddress", Convert.ToString(strBillingAddress));
                        strMailBody = strMailBody.Replace("@DeliveryAddress", Convert.ToString(strDeliveryAddress));
                        if (ddlPaymentTypes != null)
                        {
                            strMailBody = strMailBody.Replace("@PaymentMethod", "");
                        }
                        else
                        {
                            strMailBody = strMailBody.Replace("@PaymentMethod", ddlPaymentTypes.SelectedItem.Text);
                        }
                        strMailBody = strMailBody.Replace("@DeliveryService", Convert.ToString(strShippingMethodEmail));

                        bool res;
                        if (Session["GuestUser"] != null && Session["User"] == null)
                        {
                            res = GlobalFunctions.SendEmail(Convert.ToString(Session["GuestUser"]), strFromEmailId, "", strCCEmailId, strBCCEmailId, strSubject, strMailBody, "", "", IsHtml);
                        }
                        else
                        {
                            res = GlobalFunctions.SendEmail(lstUser.EmailId, strFromEmailId, "", strCCEmailId, strBCCEmailId, strSubject, strMailBody, "", "", IsHtml);
                        }
                        if (res)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strEmailMeaCopyOfMyBasket, AlertType.Success);
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void PlaceOrder()
        {
            try
            {
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 06-10-15
        /// Scope   : Save UDF values into DB
        /// </summary>            
        /// <returns>True/False</returns>
        protected bool SaveUDFValues(Int32 intPaymentTypeId)
        {
            bool bStatus = false;
            try
            {
                Exceptions.WriteInfoLog("Inside SaveUDFValues()");
                string strErrormessage = string.Empty;
                List<object> lstObj = new List<object>();
                lstObj = ShoppingCartBL.GetUDFS(0);

                List<UDFBE> lstUDF = new List<UDFBE>();
                List<UDFBE> lstUDFFilterBE = new List<UDFBE>();
                lstUDF = lstObj.Cast<UDFBE>().ToList();

                lstUDFFilterBE = new List<UDFBE>();
                lstUDFFilterBE = lstUDF.FindAll(x => x.PaymentTypeID == intPaymentTypeId);

                StringBuilder sbUDFValues = new StringBuilder();
                for (int j = 0; j < lstUDFFilterBE.Count; j++)
                {
                    if (lstUDFFilterBE[j].IsVisible)
                    {

                        if (lstUDFFilterBE[j].RuleType == "A")
                        {
                            Exceptions.WriteInfoLog("Inside lstUDFFilterBE[j].RuleType == A in SaveUDFValues()");
                            TextBox txtUDFS = (TextBox)tblUDFS.FindControl("txt" + lstUDFFilterBE[j].UDFOasisID);
                            if (Convert.ToString(lstUDFFilterBE[j].MaximumLength) != "-1")
                            {
                                if (txtUDFS.Text.Trim().Length > lstUDFFilterBE[j].MaximumLength)
                                {
                                    strSC_UDF_Max_Character = strSC_UDF_Max_Character.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumLength));
                                    strErrormessage += lstUDFFilterBE[j].Caption + strSC_UDF_Max_Character + "<br>";//: can not be more than " + lstUDFFilterBE[j].MaximumLength + " characters.<br>";
                                }
                            }
                            if (lstUDFFilterBE[j].CaseRule == "Y")
                            {
                                Regex rg = new Regex("^[A-Z]*$");
                                if (rg.IsMatch(txtUDFS.Text.Trim()) == false)
                                    strErrormessage += lstUDFFilterBE[j].Caption + ": " + strUppercase + " <br>";//Text can only be in Uppercase.<br>";
                            }
                            if (lstUDFFilterBE[j].IsMandatory)
                            {
                                if (GlobalFunctions.IsEmpty(txtUDFS.Text.Trim()))
                                    strErrormessage += lstUDFFilterBE[j].Caption + ": " + strRequired + "<br>";
                            }

                            if (txtUDFS.Text != "NA")
                            {
                                sbUDFValues.Append(lstUDFFilterBE[j].UDFOasisID + "|" + Sanitizer.GetSafeHtmlFragment(txtUDFS.Text.Trim()) + ";");
                            }
                            if (txtUDFS.Text != null && txtUDFS.Text != "")
                            {
                                Session["UDFUSer"] = Session["UDFUSer"] + lstUDFFilterBE[j].Caption + "-" + Sanitizer.GetSafeHtmlFragment(txtUDFS.Text.Trim()) + ",";
                            }
                        }
                        else if (lstUDFFilterBE[j].RuleType == "B")
                        {
                            Exceptions.WriteInfoLog("Inside lstUDFFilterBE[j].RuleType == B in SaveUDFValues()");
                            TextBox txtUDFS = (TextBox)tblUDFS.FindControl("txt" + lstUDFFilterBE[j].UDFOasisID);
                            if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1" || Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                            {
                                if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1" && Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                {
                                    if (Convert.ToInt32(txtUDFS.Text.Trim()) < Convert.ToInt32(lstUDFFilterBE[j].MinimumValue) &&
                                        Convert.ToInt32(txtUDFS.Text.Trim()) > Convert.ToInt32(lstUDFFilterBE[j].MaximumValue))
                                    {
                                        strSC_UDF_Minimum_Maximum_Range = strSC_UDF_Minimum_Maximum_Range.Replace("@min", Convert.ToString(lstUDFFilterBE[j].MinimumValue));
                                        strSC_UDF_Minimum_Maximum_Range = strSC_UDF_Minimum_Maximum_Range.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumValue));
                                        strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_UDF_Minimum_Maximum_Range + "<br>";
                                        //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter value between " + lstUDFFilterBE[j].MinimumValue + " and " + lstUDFFilterBE[j].MaximumValue + "<br>";
                                    }
                                }
                                else if (Convert.ToString(lstUDFFilterBE[j].MinimumValue) != "-1")
                                {
                                    if (Convert.ToInt32(txtUDFS.Text.Trim()) < Convert.ToInt32(lstUDFFilterBE[j].MinimumValue))
                                    {
                                        strSC_UDF_Range2 = strSC_UDF_Range2.Replace("@min", Convert.ToString(lstUDFFilterBE[j].MinimumValue));
                                        strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_UDF_Range2 + "<br>";
                                        //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter value more or equal to " + lstUDFFilterBE[j].MinimumValue + "<br>";
                                    }
                                }
                                else if (Convert.ToString(lstUDFFilterBE[j].MaximumValue) != "-1")
                                {
                                    if (Convert.ToInt32(txtUDFS.Text.Trim()) > Convert.ToInt32(lstUDFFilterBE[j].MaximumValue))
                                    {
                                        strSC_UDF_Range3 = strSC_UDF_Range3.Replace("@max", Convert.ToString(lstUDFFilterBE[j].MaximumValue));
                                        strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_UDF_Range3 + "<br>";
                                        //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter value less or equal to " + lstUDFFilterBE[j].MaximumValue + "<br>";
                                    }
                                }
                            }
                            if (Convert.ToString(lstUDFFilterBE[j].DecimalPlaces) == "-1")
                            {
                                Regex rg = new Regex("^\\d+$");
                                if (rg.IsMatch(txtUDFS.Text.Trim()) == false)
                                {
                                    strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_Numeric + "<br>";
                                    //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter only numbers.<br>";
                                }
                            }
                            else
                            {
                                Regex rg = new Regex(@"^\d+(?:\.\d{" + lstUDFFilterBE[j].DecimalPlaces + "})?$");
                                if (rg.IsMatch(txtUDFS.Text.Trim()) == false)
                                {
                                    strSC_NumberMinDecimal = strSC_NumberMinDecimal.Replace("@num", Convert.ToString(lstUDFFilterBE[j].DecimalPlaces));
                                    strErrormessage += lstUDFFilterBE[j].Caption + ": " + strSC_NumberMinDecimal + "<br>";
                                    //strErrormessage += lstUDFFilterBE[j].Caption + ": Enter only numbers with " + lstUDFFilterBE[j].DecimalPlaces + " decimal places<br>";
                                }
                            }
                            sbUDFValues.Append(lstUDFFilterBE[j].UDFOasisID + "|" + Sanitizer.GetSafeHtmlFragment(txtUDFS.Text.Trim()) + ";");
                        }
                        else if (lstUDFFilterBE[j].RuleType == "E")
                        {
                            Exceptions.WriteInfoLog("Inside lstUDFFilterBE[j].RuleType == E in SaveUDFValues()");
                            DropDownList ddlPickListItems = (DropDownList)tblUDFS.FindControl("ddl" + lstUDFFilterBE[j].UDFOasisID);
                            if (Session["Approver"] != null)
                            {
                                if (lstUDFFilterBE[j].SequenceNo.Equals(8))
                                {
                                    if (Session["budget"] != null)
                                    {
                                        HtmlGenericControl spnbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnbudget");
                                        HtmlGenericControl dvbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("dvbudget");
                                        HtmlGenericControl spnTotalPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTotalPrice");
                                        decimal baskettotal = Convert.ToDecimal(spnTotalPrice.InnerText);
                                        decimal budgettotal = Convert.ToDecimal(Session["budget"]);
                                        decimal finalbudgettotal = budgettotal - baskettotal;
                                        if (finalbudgettotal > 0)
                                        {
                                            isapproved = "Y";
                                        }
                                        if (finalbudgettotal < 0) { isapproved = "NA"; }


                                    }
                                    else { isapproved = "NA"; }
                                    if (isapproved != "NA")
                                    {
                                        sbUDFValues.Append(lstUDFFilterBE[j].UDFOasisID + "|" + Sanitizer.GetSafeHtmlFragment(isapproved) + ";");
                                    }

                                }
                                else { sbUDFValues.Append(lstUDFFilterBE[j].UDFOasisID + "|" + Sanitizer.GetSafeHtmlFragment(ddlPickListItems.SelectedItem.Text.Trim()) + ";"); }
                            }
                            else
                            {
                                sbUDFValues.Append(lstUDFFilterBE[j].UDFOasisID + "|" + Sanitizer.GetSafeHtmlFragment(ddlPickListItems.SelectedItem.Text.Trim()) + ";");
                            }
                            if (ddlPickListItems.SelectedItem.Text != null && ddlPickListItems.SelectedItem.Text != "")
                            {
                                Session["UDFUSer"] = Session["UDFUSer"] + Convert.ToString(lstUDFFilterBE[j].Caption + "-" + Sanitizer.GetSafeHtmlFragment(ddlPickListItems.SelectedItem.Text.Trim())) + ",";
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(strErrormessage))
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strErrormessage, AlertType.Failure);
                    bStatus = false;
                }
                else
                {
                    if (!string.IsNullOrEmpty(sbUDFValues.ToString()))
                    {
                        ShoppingCartBL.UpdateUDFValues(sbUDFValues.ToString(), intCustomerOrderId);
                    }
                    bStatus = true;
                }
                Exceptions.WriteInfoLog("Exiting SaveUDFValues()");
                return bStatus;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 30-09-15
        /// Scope   : Validate address and other Details
        /// </summary>            
        /// <returns>True/False</returns>
        protected bool ValidateDetails()
        {
            bool bStatus = false;
            bool validspecialchar = true;
            try
            {
                Exceptions.WriteInfoLog("Inside ValidateDetails()");
                string strErrormessage = string.Empty;
                string strDeliveryErrorMsg = "", strInvoiceErrorMsg = "";
                //Validate billing contact name
                #region "strInvoiceErrorMsg"
                if (txtContact_Name.Attributes["rel"] == "m")
                {
                    Exceptions.WriteInfoLog("Inside txtContact_Name.Attributes[rel] == m in ValidateDetails()");
                    if (GlobalFunctions.IsEmpty(txtContact_Name.Value))
                        strInvoiceErrorMsg += txtContact_Name.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtContact_Name.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtContact_Name.Value, 100))
                            strInvoiceErrorMsg += strValid + " " + txtContact_Name.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtContact_Name.Attributes["data-value"] + "<br>";
                }
                else if (GlobalFunctions.IsEmpty(txtContact_Name.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtContact_Name.Value, 100))
                    {
                        strInvoiceErrorMsg += strValid + " " + txtContact_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtContact_Name.Attributes["data-value"] + "<br>";
                    }
                //Validate billing txtCompany name
                if (txtCompany.Attributes["rel"] == "m")
                {
                    Exceptions.WriteInfoLog("Inside txtCompany.Attributes[rel] == m in ValidateDetails()");
                    if (GlobalFunctions.IsEmpty(txtCompany.Value))
                        strInvoiceErrorMsg += txtCompany.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtCompany.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtCompany.Value, 100))
                            strInvoiceErrorMsg += strValid + " " + txtCompany.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtCompany.Attributes["data-value"] + "<br>";

                }
                else if (GlobalFunctions.IsEmpty(txtCompany.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtCompany.Value, 100))
                        strInvoiceErrorMsg += strValid + " " + txtCompany.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtCompany.Attributes["data-value"] + "<br>";

                //Validate billing Address line 1
                if (txtAddress1.Attributes["rel"] == "m")
                {
                    Exceptions.WriteInfoLog("Inside txtAddress1.Attributes[rel] == m in ValidateDetails()");
                    if (GlobalFunctions.IsEmpty(txtAddress1.Value))
                        strInvoiceErrorMsg += txtAddress1.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtAddress1.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtAddress1.Value, 200))
                            strInvoiceErrorMsg += strValid + " " + txtAddress1.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtAddress1.Attributes["data-value"] + "<br>";

                }
                else if (!GlobalFunctions.IsEmpty(txtAddress1.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtAddress1.Value, 200))
                        strInvoiceErrorMsg += strValid + " " + txtAddress1.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtAddress1.Attributes["data-value"] + "<br>";

                //Validate billing Address line 2
                if (txtAddress2.Attributes["rel"] == "m")
                {
                    Exceptions.WriteInfoLog("Inside txtAddress2.Attributes[rel] == m in ValidateDetails()");
                    if (GlobalFunctions.IsEmpty(txtAddress2.Value))
                        strInvoiceErrorMsg += txtAddress2.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtAddress2.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtAddress2.Value, 200))
                            strInvoiceErrorMsg += strValid + " " + txtAddress2.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtAddress2.Attributes["data-value"] + "<br>";

                }
                else if (!GlobalFunctions.IsEmpty(txtAddress2.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtAddress2.Value, 200))
                        strInvoiceErrorMsg += strValid + " " + txtAddress2.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtAddress2.Attributes["data-value"] + "<br>";

                //Validate billing Town
                if (txtTown.Attributes["rel"] == "m")
                {
                    Exceptions.WriteInfoLog("Inside txtTown.Attributes[rel] == m in ValidateDetails()");
                    if (GlobalFunctions.IsEmpty(txtTown.Value))
                        strInvoiceErrorMsg += txtTown.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtTown.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtTown.Value, 100))
                            strInvoiceErrorMsg += strValid + " " + txtTown.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtTown.Attributes["data-value"] + "<br>";

                }
                else if (!GlobalFunctions.IsEmpty(txtTown.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtTown.Value, 100))
                        strInvoiceErrorMsg += strValid + " " + txtTown.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtTown.Attributes["data-value"] + "<br>";

                //Validate billing State/County
                if (txtState__County.Attributes["rel"] == "m")
                {
                    Exceptions.WriteInfoLog("Inside txtState__County.Attributes[rel] == m in ValidateDetails()");
                    if (GlobalFunctions.IsEmpty(txtState__County.Value))
                        strInvoiceErrorMsg += txtState__County.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtState__County.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsAngularBrackets(txtState__County.Value, 100))
                            strInvoiceErrorMsg += strValid + " " + txtState__County.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtState__County.Attributes["data-value"] + "<br>";

                }
                else if (!GlobalFunctions.IsEmpty(txtState__County.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtState__County.Value, 100))
                        strInvoiceErrorMsg += strValid + " " + txtState__County.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtState__County.Attributes["data-value"] + "<br>";

                //Validate billing Postal Code
                if (txtPostal_Code.Attributes["rel"] == "m")
                {
                    Exceptions.WriteInfoLog("Inside txtPostal_Code.Attributes[rel] == m in ValidateDetails()");
                    if (GlobalFunctions.IsEmpty(txtPostal_Code.Value))
                        strInvoiceErrorMsg += txtPostal_Code.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtPostal_Code.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsValidPostCode(txtPostal_Code.Value, 20))
                            strInvoiceErrorMsg += strValid + " " + txtPostal_Code.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtPostal_Code.Attributes["data-value"] + "<br>";

                }
                else if (!GlobalFunctions.IsEmpty(txtPostal_Code.Value))
                    if (!GlobalFunctions.IsValidPostCode(txtPostal_Code.Value, 20))
                        strInvoiceErrorMsg += strValid + " " + txtPostal_Code.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtPostal_Code.Attributes["data-value"] + "<br>";

                //Validate billing Country
                if (ddlDCountry.Attributes["rel"] == "m")
                    if (ddlDCountry.SelectedValue == "0")
                        strInvoiceErrorMsg += ddlDCountry.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please choose " + ddlDCountry.Attributes["data-value"] + "<br>";

                //Validate billing Phone
                if (txtPhone.Attributes["rel"] == "m")
                {
                    Exceptions.WriteInfoLog("Inside txtPhone.Attributes[rel] == m in ValidateDetails()");
                    if (GlobalFunctions.IsEmpty(txtPhone.Value))
                        strInvoiceErrorMsg += txtPhone.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter " + txtPhone.Attributes["data-value"] + "<br>";
                    else
                        if (!GlobalFunctions.IsValidPhone(txtPhone.Value, 20))
                            strInvoiceErrorMsg += strValid + " " + txtPhone.Attributes["data-value"] + "<br>";
                    //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtPhone.Attributes["data-value"] + "<br>";

                }
                else if (!GlobalFunctions.IsEmpty(txtPhone.Value))
                    if (!GlobalFunctions.IsValidPhone(txtPhone.Value, 20))
                        strInvoiceErrorMsg += strValid + " " + txtPhone.Attributes["data-value"] + "<br>";
                //strErrormessage += strBillingtitle + ": " + "Please enter valid " + txtPhone.Attributes["data-value"] + "<br>";

                #endregion

                if (!string.IsNullOrEmpty(strInvoiceErrorMsg))
                {
                    strErrormessage = strRegister_RegisteredAddress_Text + ": <br>" + strInvoiceErrorMsg + "<br>";
                }

                //validate delivery address if it is not same as billing address
                #region Delivery
                if (addressCheckbox.Checked == false)
                {
                    //Validate delivery adress title
                    if (ddlDAddressTitle.SelectedValue == "0")
                    {
                        if (GlobalFunctions.IsEmpty(txtAddressTitle.Value))
                            strDeliveryErrorMsg += strBasket_Title_Text + " <br>";
                        //strErrormessage += "Please enter delivery address title <br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtAddressTitle.Value, 50))
                                strDeliveryErrorMsg += strValid + " " + strBasket_Title_Text + " <br>";
                        //strErrormessage += "Please enter valid delivery address title <br>";
                    }

                    //Validate delivery contact name
                    if (txtDContact_Name.Attributes["rel"] == "m")
                    {
                        Exceptions.WriteInfoLog("Inside txtDContact_Name.Attributes[rel] == m in ValidateDetails()");
                        if (GlobalFunctions.IsEmpty(txtDContact_Name.Value))
                            strDeliveryErrorMsg += txtDContact_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDContact_Name.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDContact_Name.Value, 100))
                                strDeliveryErrorMsg += strValid + " " + txtDContact_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDContact_Name.Attributes["data-value"] + "<br>";
                    }
                    else if (GlobalFunctions.IsEmpty(txtDContact_Name.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDContact_Name.Value, 100))
                            strDeliveryErrorMsg += strValid + " " + txtDContact_Name.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDContact_Name.Attributes["data-value"] + "<br>";

                    #region Code Added by SHRIGANESH to Validate Company Name in Delivery Address section 27 July 2016

                    //Validate delivery contact name
                    if (txtDCompany_Name.Attributes["rel"] == "m")
                    {
                        Exceptions.WriteInfoLog("Inside txtDCompany_Name.Attributes[rel] == m in ValidateDetails()");
                        if (GlobalFunctions.IsEmpty(txtDCompany_Name.Value))
                            strDeliveryErrorMsg += txtDCompany_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDCompany_Name.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDCompany_Name.Value, 100))
                                strDeliveryErrorMsg += strValid + " " + txtDCompany_Name.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDCompany_Name.Attributes["data-value"] + "<br>";
                    }
                    else if (GlobalFunctions.IsEmpty(txtDCompany_Name.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDCompany_Name.Value, 100))
                            strDeliveryErrorMsg += strValid + " " + txtDCompany_Name.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDCompany_Name.Attributes["data-value"] + "<br>";

                    #endregion

                    //Validate delivery Address line 1
                    if (txtDAddress1.Attributes["rel"] == "m")
                    {
                        Exceptions.WriteInfoLog("Inside txtDAddress1.Attributes[rel] == m in ValidateDetails()");
                        if (GlobalFunctions.IsEmpty(txtDAddress1.Value))
                            strDeliveryErrorMsg += txtDAddress1.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDAddress1.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDAddress1.Value, 200))
                                strDeliveryErrorMsg += strValid + " " + txtDAddress1.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDAddress1.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDAddress1.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDAddress1.Value, 200))
                            strDeliveryErrorMsg += strValid + " " + txtDAddress1.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDAddress1.Attributes["data-value"] + "<br>";

                    //Validate delivery Address line 2
                    if (txtDAddress2.Attributes["rel"] == "m")
                    {
                        Exceptions.WriteInfoLog("Inside txtDAddress2.Attributes[rel] == m in ValidateDetails()");
                        if (GlobalFunctions.IsEmpty(txtDAddress2.Value))
                            strDeliveryErrorMsg += txtDAddress2.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDAddress2.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDAddress2.Value, 200))
                                strDeliveryErrorMsg += strValid + " " + txtDAddress2.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDAddress2.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDAddress2.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDAddress2.Value, 200))
                            strDeliveryErrorMsg += strValid + " " + txtDAddress2.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDAddress2.Attributes["data-value"] + "<br>";

                    //Validate delivery Town
                    if (txtDTown.Attributes["rel"] == "m")
                    {
                        Exceptions.WriteInfoLog("Inside txtDTown.Attributes[rel] == m in ValidateDetails()");
                        if (GlobalFunctions.IsEmpty(txtDTown.Value))
                            strDeliveryErrorMsg += txtDTown.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDTown.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDTown.Value, 100))
                                strDeliveryErrorMsg += strValid + " " + txtDTown.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDTown.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDTown.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDTown.Value, 100))
                            strDeliveryErrorMsg += strValid + " " + txtDTown.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDTown.Attributes["data-value"] + "<br>";

                    //Validate delivery State/County
                    if (txtDState__County.Attributes["rel"] == "m")
                    {
                        Exceptions.WriteInfoLog("Inside txtDState__County.Attributes[rel] == m in ValidateDetails()");
                        if (GlobalFunctions.IsEmpty(txtDState__County.Value))
                            strDeliveryErrorMsg += txtDState__County.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDState__County.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsAngularBrackets(txtDState__County.Value, 100))
                                strDeliveryErrorMsg += strValid + " " + txtDState__County.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDState__County.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDState__County.Value))
                        if (!GlobalFunctions.IsAngularBrackets(txtDState__County.Value, 100))
                            strDeliveryErrorMsg += strValid + " " + txtDState__County.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDState__County.Attributes["data-value"] + "<br>";

                    //Validate delivery Postal Code
                    if (txtDPostal_Code.Attributes["rel"] == "m")
                    {
                        Exceptions.WriteInfoLog("Inside txtDPostal_Code.Attributes[rel] == m in ValidateDetails()");
                        if (GlobalFunctions.IsEmpty(txtDPostal_Code.Value))
                            strDeliveryErrorMsg += txtDPostal_Code.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDPostal_Code.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsValidPostCode(txtDPostal_Code.Value, 20))
                                strDeliveryErrorMsg += strValid + " " + txtDPostal_Code.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDPostal_Code.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDPostal_Code.Value))
                        if (!GlobalFunctions.IsValidPostCode(txtDPostal_Code.Value, 20))
                            strDeliveryErrorMsg += strValid + " " + txtDPostal_Code.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDPostal_Code.Attributes["data-value"] + "<br>";

                    //Validate delivery Country
                    if (ddlDCountry.Attributes["rel"] == "m")
                        if (ddlDCountry.SelectedValue == "0")
                            strDeliveryErrorMsg += ddlDCountry.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please choose " + ddlDCountry.Attributes["data-value"] + "<br>";

                    //Validate delivery Phone
                    if (txtDPhone.Attributes["rel"] == "m")
                    {
                        Exceptions.WriteInfoLog("Inside txtDPhone.Attributes[rel] == m in ValidateDetails()");
                        if (GlobalFunctions.IsEmpty(txtDPhone.Value))
                            strDeliveryErrorMsg += txtDPhone.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter " + txtDPhone.Attributes["data-value"] + "<br>";
                        else
                            if (!GlobalFunctions.IsValidPhone(txtDPhone.Value, 15))
                                strDeliveryErrorMsg += strValid + " " + txtDPhone.Attributes["data-value"] + "<br>";
                        //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDPhone.Attributes["data-value"] + "<br>";
                    }
                    else if (!GlobalFunctions.IsEmpty(txtDPhone.Value))
                        if (!GlobalFunctions.IsValidPhone(txtDPhone.Value, 15))
                            strDeliveryErrorMsg += strValid + " " + txtDPhone.Attributes["data-value"] + "<br>";
                    //strErrormessage += strDeliveryTitle + ": " + "Please enter valid " + txtDPhone.Attributes["data-value"] + "<br>";
                }
                #endregion
                if (!string.IsNullOrEmpty(strDeliveryErrorMsg))
                {
                    strErrormessage += strRegister_DeliveryAddress_Text + ": <br>" + strDeliveryErrorMsg + "<br>";
                }

                //Check for payment method
                if (ddlPaymentTypes.SelectedValue == "0")
                    strDeliveryErrorMsg += strSC_Payment_Types;
                //strErrormessage += "Please select payment method.<br>";

                //check for the special instructions
                if (!GlobalFunctions.IsEmpty(txtInstruction.Value))
                    if (!GlobalFunctions.IsAngularBrackets(txtInstruction.Value, 200))
                        strErrormessage += strValid + " " + strSC_Special_Instruction + "<br>";
                //strErrormessage += "Please enter valid special instructions. <br>";

                //Check for the valid shipping method
                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                Exceptions.WriteInfoLog("Before calling CalculateFreight() in ValidateDetails()");


                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    lstFreightBE = CalculateFreight(Convert.ToInt16(ddlDCountry.SelectedValue));
                }
                else
                {
                    lstFreightBE = CalculateFreight(Convert.ToInt16(ddlCountry.SelectedValue));
                }

               
                Exceptions.WriteInfoLog("After calling CalculateFreight() in ValidateDetails()");
                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    if (rbService1.Checked && lstFreightBE[0].SZDisallowOrder)
                    {
                        strErrormessage += strFreightMOVMessage;
                    }
                    else if (rbService2.Checked && lstFreightBE[0].EZDisallowOrder)
                    {
                        strErrormessage += strFreightMOVMessage;
                    }
                    else if (rbService3.Checked && lstFreightBE[0].PZDisallowOrder)
                    {
                        strErrormessage += strFreightMOVMessage;
                    }
                }
                //Check for the inventory of the products
                if (bBackOrderAllowed == false)
                {
                    Exceptions.WriteInfoLog("Inside bBackOrderAllowed == false in ValidateDetails()");
                    #region
                    for (var i = 0; i < rptBasketListing.Items.Count; i++)
                    {
                        //HtmlInputText txtQuantity = (HtmlInputText)rptBasketListing.Items[i].FindControl("txtQuantity");//Commented by Sripal
                        HtmlGenericControl txtQuantity = (HtmlGenericControl)rptBasketListing.Items[i].FindControl("txtQuantity");//Added by Sripal
                        HtmlInputHidden hdnInventory = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnInventory");
                        HtmlInputHidden hdnStockStatus = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnStockStatus");
                        HtmlGenericControl dvProductName = (HtmlGenericControl)rptBasketListing.Items[i].FindControl("dvProductName");

                        if (bBASysStore == false)
                        {
                            if (Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtQuantity.InnerText)) > Convert.ToInt32(hdnInventory.Value))
                            {
                                strErrormessage += dvProductName.InnerHtml + ": " + strBasket_Quantity_Not_More_Than_Stock_Message + hdnInventory.Value + "<br>";
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtQuantity.InnerText)) > Convert.ToInt32(hdnStockStatus.Value))
                                strErrormessage += dvProductName.InnerHtml + ": " + strBasket_Quantity_Not_More_Than_Stock_Message + hdnStockStatus.Value + "<br>";
                        }
                    }
                    #endregion
                }
                if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                {
                    Exceptions.WriteInfoLog("Inside IsPointsEnbled in ValidateDetails()");
                    #region
                    List<object> lstObjShoppingBE = new List<object>();
                    lstObjShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId);
                    List<ShoppingCartBE> lstShoppingBE = new List<ShoppingCartBE>();
                    lstShoppingBE = lstObjShoppingBE.Cast<ShoppingCartBE>().ToList();

                    Int32 intPoints = 0;
                    for (int i = 0; i < rptBasketListing.Items.Count; i++)
                    {
                        HtmlInputHidden hdnShoppingCartProductId = (HtmlInputHidden)rptBasketListing.Items[i].FindControl("hdnShoppingCartProductId");


                        HtmlGenericControl txtQuantity = (HtmlGenericControl)rptBasketListing.Items[i].FindControl("txtQuantity");//Added by Sripal

                        if (GlobalFunctions.isNumeric(txtQuantity.InnerText.Trim(), System.Globalization.NumberStyles.Integer))
                        {
                            decimal dPriceS = lstShoppingBE.FirstOrDefault(x => x.ShoppingCartProductId == Convert.ToInt32(hdnShoppingCartProductId.Value)).Price;
                            int iQTY = Convert.ToInt32(txtQuantity.InnerText.Trim());
                            string stqty = GlobalFunctions.RemovePointsSpanIntheText(GlobalFunctions.DisplayPriceOrPoints((dPriceS * iQTY).ToString(), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()));
                            var ppoint = Convert.ToDecimal(stqty);
                            intPoints += Convert.ToInt32(ppoint);
                        }
                    }
                    if (intPoints > lstUser.Points)
                    {
                        strErrormessage += strSC_InSufficient_Point;// "You do not have sufficient points for this order.";
                    }
                    #endregion
                }

                if (!string.IsNullOrEmpty(strErrormessage))
                {
                    string strMs = "<font color:red>" + strRequired + "</font>" + strErrormessage;
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strMs, AlertType.Failure);
                    bStatus = false;
                }
                else
                {
                    bStatus = true;
                }
                bool astatus = true;//CheckSpecialCharactersAllfields();
                if (astatus && bStatus)
                {
                    bStatus = true;
                }
                else
                {
                    bStatus = false;
                }
                Exceptions.WriteInfoLog("Exiting ValidateDetails()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
            return bStatus;
        }

        protected bool CheckSpecialCharactersAllfields()
        {
            bool isvalidated = true;
            #region vikram to check specialcharacters
            //////////////////////// invoice address//////////////////////////		
            if (!GlobalFunctions.IsSpecialCharacters(txtContact_Name.Value))
            {
                txtContact_Name.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            //         return bStatus;
            if (!GlobalFunctions.IsSpecialCharacters(txtCompany.Value))
            {
                txtCompany.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtAddress1.Value))
            {
                txtAddress1.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }

            if (!GlobalFunctions.IsSpecialCharacters(txtAddress2.Value))
            {
                txtAddress2.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtTown.Value))
            {
                txtTown.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtState__County.Value))
            {
                txtState__County.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtPostal_Code.Value))
            {
                txtPostal_Code.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtPhone.Value))
            {
                txtPhone.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }

            //////////////////////// delivery address//////////////////////////		
            if (!GlobalFunctions.IsSpecialCharacters(txtDAddress1.Value))
            {
                txtContact_Name.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtDAddress2.Value))
            {
                txtCompany.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtDContact_Name.Value))
            {
                txtAddress1.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }

            if (!GlobalFunctions.IsSpecialCharacters(txtDPostal_Code.Value))
            {
                txtAddress2.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtDTown.Value))
            {
                txtTown.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtDState__County.Value))
            {
                txtState__County.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            if (!GlobalFunctions.IsSpecialCharacters(txtDPostal_Code.Value))
            {
                txtPostal_Code.Focus();
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                isvalidated = false;
                return false;
            }
            return isvalidated;
            #endregion
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind UpdateCustomerOrderData
        /// </summary>            
        /// <returns>True</returns>
        protected bool UpdateCustomerOrderData()
        {
            bool bStatus = false;
            try
            {
                Exceptions.WriteInfoLog("Inside UpdateCustomerOrderData()");
                TxRefGUID = Funcs.CreateTxRefGUID();
                CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
                objCustomerOrderBE.OrderedBy = lstUser.UserId;
                HtmlGenericControl spnadditionalcharge = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnadditionalcharge");
                #region Code Added by SHRIGANESH 28 July 2016 to insert the Latest Order details into DataBase.
                objCustomerOrderBE.CustomerOrderId = 0;
                objCustomerOrderBE.action = DBAction.Insert;
                #endregion

                objCustomerOrderBE.ApprovedBy = 0;
                objCustomerOrderBE.InvoiceCompany = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                objCustomerOrderBE.InvoiceContactPerson = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                objCustomerOrderBE.InvoiceAddress1 = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                objCustomerOrderBE.InvoiceAddress2 = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                objCustomerOrderBE.InvoiceCity = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                objCustomerOrderBE.InvoicePostalCode = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                objCustomerOrderBE.InvoiceCountry = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                {
                    #region vikram for legal entity

                    List<object> lstPaymentTypes = new List<object>();
                    List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                    string strPaymentMethod = Convert.ToString(ddlPaymentTypes.SelectedValue);
                    if (strPaymentMethod.Contains("|"))
                    {
                        string[] items = strPaymentMethod.Split('|');
                        Int16 iPaymentTypeId = Convert.ToInt16(items[0]);
                        lstPaymentTypes = ShoppingCartBL.GetPaymentdetailsById(iPaymentTypeId, GlobalFunctions.GetLanguageId());
                        lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                        if (Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                        {
                            objCustomerOrderBE.InvoiceCountry = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                        }
                        else
                        {
                            if (!lstPaymentTypesBE[0].InvoiceAccountId)
                            {
                                objCustomerOrderBE.InvoiceCountry = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                            }
                            else
                            {
                                objCustomerOrderBE.InvoiceCountry = Sanitizer.GetSafeHtmlFragment(ddlInvoiceCountry.SelectedValue.Trim());
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    objCustomerOrderBE.InvoiceCountry = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                }
                objCustomerOrderBE.InvoiceCounty = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                objCustomerOrderBE.InvoicePhone = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                objCustomerOrderBE.InvoiceFax = "";
                objCustomerOrderBE.InvoiceEmail = lstUser.EmailId;

                /*Added by sripal for non payment*/
                List<FeatureBE> objFeatures = objStoreBE.StoreFeatures.FindAll(s => s.FeatureName.ToLower() == "sc_paymentgateway");
                if (objFeatures != null)
                {
                    List<FeatureBE.FeatureValueBE> objLstFeatureValueBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "sc_paymentgateway").FeatureValues;

                    if (objLstFeatureValueBE != null)
                    {
                        FeatureBE.FeatureValueBE objFeatureValueBE = objLstFeatureValueBE.FirstOrDefault(x => x.IsEnabled == true && x.IsActive == true);
                        if (objFeatureValueBE == null)
                        {
                            objCustomerOrderBE.PaymentMethod = "1";
                        }
                        else
                        {
                            string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                            string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                            if (strSplitPaymentMethod.Length > 1)
                            {
                                objCustomerOrderBE.PaymentMethod = Sanitizer.GetSafeHtmlFragment(strSplitPaymentMethod[0]);
                            }
                        }
                    }
                    else
                    {
                        string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                        string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                        if (strSplitPaymentMethod.Length > 1)
                        {
                            objCustomerOrderBE.PaymentMethod = Sanitizer.GetSafeHtmlFragment(strSplitPaymentMethod[0]);
                        }
                    }
                }
                else
                {
                    string strPaymentMethod = ddlPaymentTypes.SelectedValue;
                    string[] strSplitPaymentMethod = strPaymentMethod.Split('|');
                    if (strSplitPaymentMethod.Length > 1)
                    {
                        objCustomerOrderBE.PaymentMethod = Sanitizer.GetSafeHtmlFragment(strSplitPaymentMethod[0]);
                    }
                }

                objCustomerOrderBE.OrderRefNo = Convert.ToString(System.DateTime.Now);
                objCustomerOrderBE.AuthorizationId_OASIS = "";
                objCustomerOrderBE.OrderId_OASIS = 0;
                objCustomerOrderBE.TxnRefGUID = TxRefGUID;
                objCustomerOrderBE.IsOrderCompleted = false;
                objCustomerOrderBE.DeliveryCompany = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                if (addressCheckbox.Checked == true)
                    objCustomerOrderBE.DeliveryAddressId = 0;
                else if (hidDeliveryAddressId.Value == "")
                    objCustomerOrderBE.DeliveryAddressId = Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(ddlDAddressTitle.SelectedValue));
                else
                    objCustomerOrderBE.DeliveryAddressId = Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(hidDeliveryAddressId.Value.ToString()));
                objCustomerOrderBE.DeliveryContactPerson = Sanitizer.GetSafeHtmlFragment(txtDContact_Name.Value.Trim());
                objCustomerOrderBE.DeliveryCompany = Sanitizer.GetSafeHtmlFragment(txtDCompany_Name.Value.Trim()); // Added By SHRIGANESH 27 July 2016 for Delivery Company
                objCustomerOrderBE.DeliveryAddress1 = Sanitizer.GetSafeHtmlFragment(txtDAddress1.Value.Trim());
                objCustomerOrderBE.DeliveryAddress2 = Sanitizer.GetSafeHtmlFragment(txtDAddress2.Value.Trim());
                objCustomerOrderBE.DeliveryCity = Sanitizer.GetSafeHtmlFragment(txtDTown.Value.Trim());
                objCustomerOrderBE.DeliveryPostalCode = Sanitizer.GetSafeHtmlFragment(txtDPostal_Code.Value.Trim());
                objCustomerOrderBE.DeliveryCountry = Sanitizer.GetSafeHtmlFragment(ddlDCountry.SelectedValue.Trim());
                objCustomerOrderBE.DeliveryCounty = Sanitizer.GetSafeHtmlFragment(txtDState__County.Value.Trim());
                objCustomerOrderBE.DeliveryPhone = Sanitizer.GetSafeHtmlFragment(txtDPhone.Value.Trim());
                objCustomerOrderBE.DeliveryFax = "";
                objCustomerOrderBE.stCardScheme = "";
                objCustomerOrderBE.SpecialInstructions = Sanitizer.GetSafeHtmlFragment(txtInstruction.Value.Trim());
                objCustomerOrderBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                objCustomerOrderBE.LanguageId = GlobalFunctions.GetLanguageId();

                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                Exceptions.WriteInfoLog("Before calling CalculateFreight() in UpdateCustomerOrderData()");
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    lstFreightBE = CalculateFreight(Convert.ToInt16(ddlDCountry.SelectedValue));
                }
                else
                {
                    lstFreightBE = CalculateFreight(Convert.ToInt16(ddlCountry.SelectedValue));
                }
                
                Exceptions.WriteInfoLog("After calling CalculateFreight() in UpdateCustomerOrderData()");
                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    string sfSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                    string sfEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                    string pfPZPrice = !string.IsNullOrEmpty(lstFreightBE[0].PSPrice) ? lstFreightBE[0].PSPrice : "0";
                    if (rbService1.Checked)
                    {
                        //objCustomerOrderBE.StandardCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].SZPrice)));
                        additionalcharge = Convert.ToDecimal(spnadditionalcharge.InnerHtml);
                        objCustomerOrderBE.StandardCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(sfSZPrice) + additionalcharge));
                        objCustomerOrderBE.ExpressCharges = 0;
                        objCustomerOrderBE.PalletCharges = 0;
                        objCustomerOrderBE.OrderCarrierId = lstFreightBE[0].SZCarrierServiceId;
                        objCustomerOrderBE.OrderCarrierName = lstFreightBE[0].SZCarrierServiceText;
                    }
                    else if (rbService2.Checked)
                    {
                        additionalcharge = Convert.ToDecimal(spnadditionalcharge.InnerHtml);
                        objCustomerOrderBE.StandardCharges = 0;
                        //objCustomerOrderBE.ExpressCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].EZPrice)));
                        objCustomerOrderBE.ExpressCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(sfEZPrice) + additionalcharge));
                        objCustomerOrderBE.PalletCharges = 0;
                        objCustomerOrderBE.OrderCarrierId = lstFreightBE[0].EZCarrierServiceId;
                        objCustomerOrderBE.OrderCarrierName = lstFreightBE[0].EZCarrierServiceText;
                    }
                    else if (rbService3.Checked)
                    {
                        additionalcharge = Convert.ToDecimal(spnadditionalcharge.InnerHtml);
                        objCustomerOrderBE.StandardCharges = 0;
                        objCustomerOrderBE.ExpressCharges = 0;
                        //objCustomerOrderBE.ExpressCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].EZPrice)));
                        objCustomerOrderBE.PalletCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(pfPZPrice) + additionalcharge));
                        objCustomerOrderBE.OrderCarrierId = lstFreightBE[0].PZCarrierServiceId;
                        objCustomerOrderBE.OrderCarrierName = lstFreightBE[0].PZCarrierServiceText;
                    }
                    else
                    {
                        objCustomerOrderBE.OrderCarrierId = string.IsNullOrEmpty(lstFreightBE[0].SZCarrierServiceId) ? lstFreightBE[0].EZCarrierServiceId : lstFreightBE[0].SZCarrierServiceId;
                        objCustomerOrderBE.OrderCarrierName = string.IsNullOrEmpty(lstFreightBE[0].SZCarrierServiceText) ? lstFreightBE[0].EZCarrierServiceText : lstFreightBE[0].SZCarrierServiceText;
                    }
                    objCustomerOrderBE.handlingfee = Convert.ToDouble(additionalcharge);
                    double discountedPercentage = 0.0;
                    if (Session["discountPercentage"] != null)
                        discountedPercentage = Convert.ToDouble(Session["discountPercentage"]);

                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";

                    string strSZPrice = "0", strEZPrice = "0", strPZPrice = "0"; ;
                    decimal price = 0;
                    if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                    {
                        price = Convert.ToDecimal(lstFreightBE[0].SZPrice) + additionalcharge;
                        strSZPrice = Convert.ToString(float.Parse(Convert.ToString(price), NumberStyles.Any, ci), CultureInfo.InvariantCulture);
                    }
                    if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                    {
                        price = Convert.ToDecimal(lstFreightBE[0].EZPrice) + additionalcharge;
                        strEZPrice = Convert.ToString(float.Parse(Convert.ToString(price), NumberStyles.Any, ci), CultureInfo.InvariantCulture);
                    }
                    if (!string.IsNullOrEmpty(lstFreightBE[0].PSPrice))
                    {
                        price = Convert.ToDecimal(lstFreightBE[0].PSPrice) + additionalcharge;
                        strPZPrice = Convert.ToString(float.Parse(Convert.ToString(price), NumberStyles.Any, ci), CultureInfo.InvariantCulture);
                    }
                    Exceptions.WriteInfoLog("Before calling CalculateTax() in UpdateCustomerOrderData()");
                    string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), strSZPrice, strEZPrice, strPZPrice, discountedPercentage);
                    Exceptions.WriteInfoLog("strTotalTax" + strTotalTax);
                    //string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), lstFreightBE[0].SZPrice, lstFreightBE[0].EZPrice, discountedPercentage);
                    Exceptions.WriteInfoLog("After calling CalculateTax() in UpdateCustomerOrderData()");
                    if (!string.IsNullOrEmpty(strTotalTax))
                    {
                        string[] strTax = strTotalTax.Split('|');
                        if (strTax.Length > 0)
                        {
                            Exceptions.WriteInfoLog("strTax 0 " + strTax[0]);
                            Exceptions.WriteInfoLog("strTax 1 " + strTax[1]);
                            Exceptions.WriteInfoLog("strTax 2" + strTax[2]);
                            if (rbService1.Checked)
                            {
                                objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[0]))); //Total tax for Service 1
                            }
                            else if (rbService2.Checked)
                            {
                                objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[1]))); //Total tax for Service 2
                            }
                            else if (rbService3.Checked)
                            {
                                objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[2]))); // Total tax for Service 3
                            }
                        }
                    }

                    objCustomerOrderBE.CouponCode = string.Empty; ;

                    if (!string.IsNullOrEmpty(hidCouponCode.Value.Trim()))
                    {
                        #region Freight&TaxForCouponCode
                        double OrderItemTotal = 0;
                        string StandardFreightValueAfterDiscount = "0";
                        string ExpressFreightValueAfterDiscount = "0";
                        string PalletFreightValueAfterDiscount = "0";
                        string StandardTaxValueAfterDiscount = "0";
                        //double OrderValueAfterDiscount = 0;
                        string ExpressTaxValueAfterDiscount = "0";
                        string PalletTaxValueAfterDiscount = "0";
                        string strBehaviourType = string.Empty;
                        List<object> lstShoppingBE = new List<object>();
                        Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData() in UpdateCustomerOrderData()");
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                        Exceptions.WriteInfoLog("After calling ShoppingCartBL.GetBasketProductsData() in UpdateCustomerOrderData()");
                        if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                        {
                            List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                            lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                            for (int i = 0; i < lstShoppingBE.Count; i++)
                            {
                                OrderItemTotal += Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                            }
                        }

                        CouponBE objCouponBE = new CouponBE();
                        List<CouponBE> lstCouponBE = new List<CouponBE>();
                        List<CountryBE> lstCountry = new List<CountryBE>();
                        objCouponBE.OrderValue = float.Parse(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(OrderItemTotal)));
                        objCouponBE.CouponCode = hidCouponCode.Value.Trim();
                        lstCountry = CountryBL.GetAllCountries();
                        if (lstCountry != null && lstCountry.Count > 0)
                        {
                            string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(ddlDCountry.SelectedValue)).RegionCode;
                            // objCouponBE.Region = strRegionCode == "ROW" ? "Other" : strRegionCode; SHRIGANESH 14 Nov 2016
                            if (strRegionCode == "EU")
                            {
                                objCouponBE.Region = "EU Region";
                            }
                            else if (strRegionCode == "UK")
                            {
                                objCouponBE.Region = "UK Region";
                            }
                            else
                            {
                                objCouponBE.Region = strRegionCode;
                            }
                        }
                        //objCouponBE.ShipmentType = rbService1.Checked == true ? "1" : "2"; // 1 for Standard, 2 for express shipping method
                        if (rbService1.Checked)
                        {
                            objCouponBE.ShipmentType = "1";
                        }
                        else if (rbService2.Checked)
                        {
                            objCouponBE.ShipmentType = "2";
                        }
                        else if (rbService3.Checked)
                        {
                            objCouponBE.ShipmentType = "3";
                        }
                        Exceptions.WriteInfoLog("Before calling CouponBL.ValidateCouponCode() in UpdateCustomerOrderData()");
                        lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, lstUser, GlobalFunctions.GetCurrencyId());
                        Exceptions.WriteInfoLog("After calling CouponBL.ValidateCouponCode() in UpdateCustomerOrderData()");
                        if (lstCouponBE[0].IsActive && lstCouponBE[0].IsExpired == false && lstCouponBE[0].IsValidOrderValue && lstCouponBE[0].IsValidRegion && lstCouponBE[0].IsValidShipmentType)
                        {
                            string strFSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                            string strFEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                            string strFPSPrice = !string.IsNullOrEmpty(lstFreightBE[0].PSPrice) ? lstFreightBE[0].PSPrice : "0";

                            strBehaviourType = lstCouponBE[0].BehaviourType;
                            if (lstCouponBE[0].BehaviourType.ToLower() != "product")
                            {
                                double dDiscountPercentage = 0;
                                //Apply to goods only, excluding Freight Value 
                                if (lstCouponBE[0].BehaviourType.ToLower() == "basket")
                                {
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 1
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 2
                                    PalletFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFPSPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 3
                                    dDiscountPercentage = Convert.ToDouble(lstCouponBE[0].DiscountPercentage);
                                }
                                //Free shipping excluding Freight Value
                                else if (lstCouponBE[0].BehaviourType.ToLower() == "freeshipping")
                                {
                                    StandardFreightValueAfterDiscount = "0"; //Total tax for Service 1
                                    ExpressFreightValueAfterDiscount = "0"; //Total tax for Service 2
                                    PalletFreightValueAfterDiscount = "0"; //Total Tax for Service 3
                                    dDiscountPercentage = 0;
                                }
                                //Discounted Shipping
                                else if (lstCouponBE[0].BehaviourType.ToLower() == "shipping")
                                {
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 1
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 2
                                    PalletFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(strFPSPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 3
                                    dDiscountPercentage = 0;
                                }
                                Exceptions.WriteInfoLog("Before calling CalculateTax() in UpdateCustomerOrderData()");
                                string strTotalTaxForCoupon = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), StandardFreightValueAfterDiscount.Replace(",", "."), ExpressFreightValueAfterDiscount.Replace(",", "."), PalletFreightValueAfterDiscount.Replace(",", "."), dDiscountPercentage);
                                Exceptions.WriteInfoLog("After calling CalculateTax() in UpdateCustomerOrderData()");
                                if (!string.IsNullOrEmpty(strTotalTaxForCoupon))
                                {
                                    string[] strTax = strTotalTaxForCoupon.Split('|');
                                    if (strTax.Length > 0)
                                    {
                                        StandardTaxValueAfterDiscount = strTax[0]; //Total Tax for Service 1
                                        ExpressTaxValueAfterDiscount = strTax[1]; //Total Tax for Service 2
                                        PalletTaxValueAfterDiscount = strTax[2]; //Total Tax for Service 3
                                    }
                                }

                                if (rbService1.Checked)
                                {
                                    objCustomerOrderBE.StandardCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(StandardFreightValueAfterDiscount)));
                                    objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(StandardTaxValueAfterDiscount))); //Total tax for Service 1
                                    objCustomerOrderBE.ExpressCharges = 0;
                                    objCustomerOrderBE.PalletCharges = 0;
                                }
                                else if (rbService2.Checked)
                                {
                                    objCustomerOrderBE.StandardCharges = 0;
                                    objCustomerOrderBE.PalletCharges = 0;
                                    objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ExpressTaxValueAfterDiscount))); //Total tax for Service 2
                                    objCustomerOrderBE.ExpressCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ExpressFreightValueAfterDiscount)));
                                }
                                else if (rbService3.Checked)
                                {
                                    objCustomerOrderBE.StandardCharges = 0;
                                    objCustomerOrderBE.ExpressCharges = 0;
                                    objCustomerOrderBE.TotalTax = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(PalletTaxValueAfterDiscount))); //Total tax for Service 3
                                    objCustomerOrderBE.PalletCharges = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(PalletFreightValueAfterDiscount)));
                                }
                            }
                            //Add coupon code in session and use it on the creditcard payment and Order xml.
                            objCustomerOrderBE.CouponCode = hidCouponCode.Value.Trim();
                        }
                        #endregion
                    }
                }

                CustomerOrderBE lstShoppingCart = new CustomerOrderBE();

                lstShoppingCart = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                Exceptions.WriteInfoLog("After calling ShoppingCartBL.GetBasketProductsData() in UpdateCustomerOrderData()");
                if (lstShoppingCart != null && lstShoppingCart.CustomerOrderId != 0)
                {
                    intCustomerOrderId = lstShoppingCart.CustomerOrderId;
                    bStatus = true;
                }
                Exceptions.WriteInfoLog("Exiting UpdateCustomerOrderData()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return bStatus;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind SaveAddressDetails
        /// </summary>            
        /// <returns>True</returns>
        protected bool SaveAddressDetails()
        {
            bool bStatus = false;
            bool bPunchout = false;
            int Basys = 0;
            try
            {
                Exceptions.WriteInfoLog("Inside SaveAddressDetails()");
                if (Session["GuestUser"] != null)
                {

                    Exceptions.WriteInfoLog("Inside Session[GuestUser] != null in SaveAddressDetails()");
                    Session["IsLogin"] = true;
                    #region "Insert into database and BASYS"
                    #region
                    int i = 0;

                    try
                    {
                        UserBE objBE = new UserBE();
                        UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                        //objBE.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["GuestUser"]).Trim());
                        // in database we will store encoded emailid but for display and xml it will be decoded
                        objBE.EmailId = Convert.ToString(Session["GuestUser"]).Trim(); // vikram for apostrophy
                        objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);

                        #region Added by snehal - Punchout user checkout 20 12 2016

                        StoreBE objStoreBE = StoreBL.GetStoreDetails();
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_IsPunchoutRegistration").FeatureValues[0].IsEnabled)
                        {
                            int IsPunchout = UserBL.IsPunchoutUser(Constants.USP_IsPunchoutUser, true, objBE);
                            if (IsPunchout == 1)
                            {
                                bPunchout = true;
                            }
                        }

                        #endregion


                        Exceptions.WriteInfoLog("textBox = " + txtContact_Name.Value);
                        #region "Region for the First name and last Name"
                        List<string> objLs = SplitName(txtContact_Name.Value);
                        if (objLs.Count == 2)
                        {
                            objBE.FirstName = objLs[0];
                            objBE.LastName = objLs[1];
                        }

                        #endregion
                        objBE.PredefinedColumn1 = txtContact_Name.Value;
                        objBE.PredefinedColumn2 = txtCompany.Value;
                        objBE.PredefinedColumn3 = txtAddress1.Value;
                        objBE.PredefinedColumn4 = txtAddress2.Value;
                        objBE.PredefinedColumn5 = txtTown.Value;
                        objBE.PredefinedColumn6 = txtState__County.Value;
                        objBE.PredefinedColumn7 = txtPostal_Code.Value;
                        objBE.PredefinedColumn8 = ddlCountry.SelectedValue;
                        objBE.PredefinedColumn9 = txtPhone.Value;

                        if (addressCheckbox.Checked)
                        {
                            Exceptions.WriteInfoLog("Inside addressCheckbox.Checked in SaveAddressDetails()");
                            obj.AddressTitle = "Default";
                            obj.PreDefinedColumn1 = txtContact_Name.Value;
                            obj.PreDefinedColumn2 = txtCompany.Value;
                            obj.PreDefinedColumn3 = txtAddress1.Value;
                            obj.PreDefinedColumn4 = txtAddress2.Value;
                            obj.PreDefinedColumn5 = txtTown.Value;
                            obj.PreDefinedColumn6 = txtState__County.Value;
                            obj.PreDefinedColumn7 = txtPostal_Code.Value;
                            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                            {
                                #region vikram for legal entity
                                if (Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                                {
                                    obj.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                                }
                                else
                                {
                                    List<object> lstPaymentTypes = new List<object>();
                                    List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                                    string strPaymentMethod = Convert.ToString(ddlPaymentTypes.SelectedValue);
                                    if (strPaymentMethod.Contains("|"))
                                    {
                                        string[] items = strPaymentMethod.Split('|');
                                        Int16 iPaymentTypeId = Convert.ToInt16(items[0]);
                                        lstPaymentTypes = ShoppingCartBL.GetPaymentdetailsById(iPaymentTypeId, GlobalFunctions.GetLanguageId());
                                        lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                                        if (!lstPaymentTypesBE[0].InvoiceAccountId)
                                        {
                                            obj.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                                        }
                                        else
                                        {
                                            obj.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlInvoiceCountry.SelectedValue.Trim());
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                obj.PreDefinedColumn8 = ddlCountry.SelectedValue;
                            }
                            obj.PreDefinedColumn9 = txtPhone.Value;
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Inside Else addressCheckbox.Checked in SaveAddressDetails()");
                            obj.AddressTitle = !string.IsNullOrEmpty(txtAddressTitle.Value) ? txtAddressTitle.Value : "Default";
                            obj.PreDefinedColumn1 = txtDContact_Name.Value;
                            obj.PreDefinedColumn2 = txtDCompany_Name.Value; // Added by SHRIGANESH for Delivery Comapny Name 27 July 2016
                            obj.PreDefinedColumn3 = txtDAddress1.Value;
                            obj.PreDefinedColumn4 = txtDAddress2.Value;
                            obj.PreDefinedColumn5 = txtDTown.Value;
                            obj.PreDefinedColumn6 = txtDState__County.Value;
                            obj.PreDefinedColumn7 = txtDPostal_Code.Value;
                            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                            {
                                #region vikram for legal entity
                                if (Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                                {
                                    obj.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                                }
                                else
                                {
                                    List<object> lstPaymentTypes = new List<object>();
                                    List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                                    string strPaymentMethod = Convert.ToString(ddlPaymentTypes.SelectedValue);
                                    if (strPaymentMethod.Contains("|"))
                                    {
                                        string[] items = strPaymentMethod.Split('|');
                                        Int16 iPaymentTypeId = Convert.ToInt16(items[0]);
                                        lstPaymentTypes = ShoppingCartBL.GetPaymentdetailsById(iPaymentTypeId, GlobalFunctions.GetLanguageId());
                                        lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                                        if (!lstPaymentTypesBE[0].InvoiceAccountId)
                                        {
                                            obj.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                                        }
                                        else
                                        {
                                            obj.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlInvoiceCountry.SelectedValue.Trim());
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                obj.PreDefinedColumn8 = ddlCountry.SelectedValue;
                            }
                            obj.PreDefinedColumn9 = txtDPhone.Value;
                        }

                        objBE.UserDeliveryAddress.Add(obj);
                        objBE.IPAddress = GlobalFunctions.GetIpAddress();
                        objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objBE.IsGuestUser = true;
                        objBE.LanguageId = Convert.ToInt16(intLanguageId);
                        objBE.IsMarketing = false;
                        objBE.UserTypeID = iUserTypeID;// Added by SHRIGANESH 22 Sept 2016
                        objBE.Points = -1; // Added by ShriGanesh 26 April 2017
                        #region vikram for apostrophy
                        // in database we will store encoded emailid but for display and xml it will be decoded
                        objBE.EmailId = GlobalFunctions.ReplaceEmailIdspecialCharactersbyDecoding(objBE.EmailId);
                        objBE.EmailId = HttpUtility.HtmlEncode(objBE.EmailId);
                        #endregion

                        Exceptions.WriteInfoLog("Before calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                        if (bPunchout == true)
                        {
                            i = UserBL.ExecuteRegisterDetails(Constants.USP_UpdatePunchoutUserRegistrationDetails, true, objBE, "StoreCustomer");
                            objBE.UserId = Convert.ToInt16(i);
                            Basys = UserBL.IsPunchoutUser(Constants.USP_IsPunchoutBASYS, true, objBE);
                        }
                        else
                        {
                            i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                            objBE.UserId = Convert.ToInt16(i);
                        }
                        Exceptions.WriteInfoLog("After calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                        if (i > 0 || i == -2)
                        {
                            if (i > 0)
                            {

                                Exceptions.WriteInfoLog("Before calling StoreBL.GetStoreDetails() in SaveAddressDetails()");
                                //   StoreBE objStoreBE = StoreBL.GetStoreDetails();
                                Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails() in SaveAddressDetails()");
                                if (bPunchout == true)
                                {
                                    if (Basys > 0)
                                    {
                                        if (objStoreBE.IsBASYS)
                                        {
                                            for (int x = 0; x < objStoreBE.StoreCurrencies.Count(); x++)
                                            {
                                                if (objStoreBE.StoreCurrencies[x].IsActive)
                                                {
                                                    InsertBASYSId(objStoreBE.StoreCurrencies[x].CurrencyId, Convert.ToString(Session["GuestUser"]).Trim(), objBE.UserId);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (objStoreBE.IsBASYS)
                                    {
                                        for (int x = 0; x < objStoreBE.StoreCurrencies.Count(); x++)
                                        {
                                            if (objStoreBE.StoreCurrencies[x].IsActive)
                                            {
                                                InsertBASYSId(objStoreBE.StoreCurrencies[x].CurrencyId, Convert.ToString(Session["GuestUser"]).Trim(), objBE.UserId);
                                            }
                                        }
                                    }
                                }
                            }

                            //Customer already registered as a guest user
                            UserBE objBEs = new UserBE();
                            UserBE objUser = new UserBE();
                            objBEs.EmailId = Convert.ToString(Session["GuestUser"]).Trim();
                            objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                            objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                            objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                            //Below if condition is added according to harley guestcheckout tax calculation July 28,2016
                            if (objUser != null || objUser.EmailId != null)
                            {
                                HttpContext.Current.Session["User"] = objUser;
                                Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                Session["GuestUser"] = null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                    #endregion
                    #endregion
                }



                UserBE objUserBE = new UserBE();
                UserBE.UserDeliveryAddressBE objUserDeliveryAddress = new UserBE.UserDeliveryAddressBE();
                if (Session["User"] != null)
                {

                    lstUser = Session["User"] as UserBE;
                    objUserBE.UserId = lstUser.UserId;
                    if (string.IsNullOrEmpty(lstUser.BASYS_CustomerContactId))
                    {
                        if (Session["IndeedEmployeeSSO_User"] == null && Session["IndeedSSO_User"] == null)
                        {
                            InsertBASYSId(GlobalFunctions.GetCurrencyId(), lstUser.EmailId, lstUser.UserId);
                        }
                        UserBE objBE = new UserBE();
                        //objBE.EmailId = Sanitizer.GetSafeHtmlFragment(lstUser.EmailId);
                        objBE.EmailId = HttpUtility.HtmlEncode(lstUser.EmailId); // vikram for aphostrphy
                        objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());

                        UserBE objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBE);
                        Session["User"] = objUser;
                    }
                }

                if (addressCheckbox.Checked)
                {
                    txtDContact_Name.Value = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                    txtDCompany_Name.Value = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim()); // Added by SHRIGANESH 27 July 2016 for Delivery Company Name
                    txtDAddress1.Value = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                    txtDAddress2.Value = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                    txtDTown.Value = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                    txtDState__County.Value = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                    txtDPostal_Code.Value = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                    ddlDCountry.SelectedValue = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                    {
                        #region vikram for legal entity
                        if (Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                        {
                            ddlDCountry.SelectedValue = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                        }
                        else
                        {
                            List<object> lstPaymentTypes = new List<object>();
                            List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                            string strPaymentMethod = Convert.ToString(ddlPaymentTypes.SelectedValue);
                            if (strPaymentMethod.Contains("|"))
                            {
                                string[] items = strPaymentMethod.Split('|');
                                Int16 iPaymentTypeId = Convert.ToInt16(items[0]);
                                lstPaymentTypes = ShoppingCartBL.GetPaymentdetailsById(iPaymentTypeId, GlobalFunctions.GetLanguageId());
                                lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                                if (!lstPaymentTypesBE[0].InvoiceAccountId)
                                {
                                    ddlDCountry.SelectedValue = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                                }
                                else
                                {
                                    ddlDCountry.SelectedValue = Sanitizer.GetSafeHtmlFragment(ddlInvoiceCountry.SelectedValue.Trim());
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        ddlDCountry.SelectedValue = Sanitizer.GetSafeHtmlFragment(ddlCountry.SelectedValue.Trim());
                    }
                    txtDPhone.Value = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                    bStatus = true;
                }
                else
                {
                    #region "UserDeliveryAddress"
                    objUserDeliveryAddress.PreDefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtDContact_Name.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtDCompany_Name.Value.Trim()); // Added by SHRIGANESH 27 July 2016 for Delivery Company Name
                    objUserDeliveryAddress.PreDefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtDAddress1.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtDAddress2.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtDTown.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtDState__County.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtDPostal_Code.Value.Trim());
                    objUserDeliveryAddress.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlDCountry.SelectedValue.Trim());
                    objUserDeliveryAddress.PreDefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtDPhone.Value.Trim());
                    objUserDeliveryAddress.IsDefault = chkIsDefault.Checked;
                    objUserDeliveryAddress.AddressTitle = Sanitizer.GetSafeHtmlFragment(txtAddressTitle.Value);
                    //objUserBE.UserDeliveryAddress.Add(objUserDeliveryAddress);
                    DBAction type;

                    if (ddlDAddressTitle.SelectedIndex == 0)
                    {
                        type = DBAction.Insert;
                    }
                    else
                    {
                        type = DBAction.Update;
                        objUserDeliveryAddress.DeliveryAddressId = Convert.ToInt16(Sanitizer.GetSafeHtmlFragment(ddlDAddressTitle.SelectedValue));
                    }
                    objUserBE.UserDeliveryAddress.Add(objUserDeliveryAddress);
                    Exceptions.WriteInfoLog("Before calling UserBL.ExecuteLoginDetails in SaveAddressDetails()");
                    Int32 intDeliveryId = UserBL.ExecuteLoginDetails(objUserBE, type);
                    Exceptions.WriteInfoLog("After calling UserBL.ExecuteLoginDetails in SaveAddressDetails()");
                    if (intDeliveryId > 0)
                    {
                        UserBE objUser = new UserBE();
                        UserBE objBEs = new UserBE();
                        objBEs.EmailId = lstUser.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                        }
                        hidDeliveryAddressId.Value = Convert.ToString(intDeliveryId);
                        bStatus = true;
                    }
                    #endregion
                }

                #region Update billing address in case of Guest user checkout
                lstUser = new UserBE();
                lstUser = Session["User"] as UserBE;
                if (lstUser.IsGuestUser == true)
                {
                    try
                    {
                        //Update billing address
                        UserBE objBE = new UserBE();
                        objBE.UserId = Convert.ToInt16(lstUser.UserId);
                        objBE.FirstName = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                        objBE.LastName = lstUser.LastName;
                        objBE.Password = lstUser.Password;
                        objBE.PredefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                        objBE.PredefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                        objBE.PredefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                        objBE.PredefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                        objBE.PredefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                        objBE.PredefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                        objBE.PredefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                        objBE.PredefinedColumn8 = ddlCountry.SelectedValue;
                        objBE.PredefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                        objBE.IPAddress = GlobalFunctions.GetIpAddress();
                        objBE.IsGuestUser = lstUser.IsGuestUser;
                        //int i = UserBL.ExecuteProfileDetails(Constants.USP_InsertUserProfileDetails, true, objBE);

                        UserBE objUser = new UserBE();
                        UserBE objBEs = new UserBE();
                        objBEs.EmailId = lstUser.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                        }
                        //Update billing address ends here                      
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                }
                #endregion

                #region "For SSO"
                if (Session["IsSSO"] != null)
                {
                    Exceptions.WriteInfoLog("Inside Session[IsSSO] != null in SaveAddressDetails()");
                    int iRes = UpdateRegistration();
                    UserBE objUser = new UserBE();
                    UserBE objBEs = new UserBE();
                    objBEs.EmailId = lstUser.EmailId;
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                    if (string.IsNullOrEmpty(objUser.BASYS_CustomerContactId))
                    {
                        //Need to Added condition
                        //StoreBE objStoreBE = StoreBL.GetStoreDetails();Commented by Sripal OnPageLoad()
                        if (objStoreBE.IsBASYS)
                        {
                            for (int x = 0; x < objStoreBE.StoreCurrencies.Count(); x++)
                            {
                                if (objStoreBE.StoreCurrencies[x].IsActive)
                                {
                                    InsertBASYSId(objStoreBE.StoreCurrencies[x].CurrencyId, lstUser.EmailId.Trim(), lstUser.UserId);
                                }
                            }
                        }

                        objBEs.EmailId = lstUser.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                        }
                    }
                }
                #endregion
                Exceptions.WriteInfoLog("Exiting SaveAddressDetails()");
                return bStatus;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return bStatus;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 25-09-15
        /// Scope   : Calculate freight charges for standard and express zone
        /// </summary>            
        /// <returns></returns>
        protected static List<FreightManagementBE> CalculateFreight(Int16 intCountryId)
        {
            try
            {
                Exceptions.WriteInfoLog("Inside CalculateFreight() CalculateFreight()");
                FreightManagementBE objFreight = new FreightManagementBE();
                objFreight.Weight = objFreight.WeightUnits = float.Parse(Convert.ToString(HttpContext.Current.Session["ProductWeight"]));
                objFreight.WeightPerBox = float.Parse(ConfigurationManager.AppSettings["WeightPerBox"]);
                List<CountryBE> lstCountry = new List<CountryBE>();
                Exceptions.WriteInfoLog("Before Calling CountryBL.GetAllCountries() CalculateFreight()");
                lstCountry = CountryBL.GetAllCountries();
                Exceptions.WriteInfoLog("After Calling CountryBL.GetAllCountries() CalculateFreight()");
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    objFreight.CountryCode = lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).CountryCode;
                    string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).RegionCode;

                    if (strRegionCode == "EU")
                    { objFreight.FreightRegionName = "EU Region"; }
                    else if (strRegionCode == "UK")
                    { objFreight.FreightRegionName = "UK Region"; }
                    else
                    { objFreight.FreightRegionName = strRegionCode; }
                    //objFreight.FreightRegionName = "ROW";
                }
                objFreight.LanguageId = GlobalFunctions.GetLanguageId();
                Exceptions.WriteInfoLog("Before Calling StoreBL.GetStoreDetails() CalculateFreight()");
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After Calling StoreBL.GetStoreDetails() CalculateFreight()");
                if (objStoreBE != null)
                {
                    Exceptions.WriteInfoLog("Inside objStoreBE != null in CalculateFreight()");
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";
                    objFreight.FreightMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_FreightMultiplier").FeatureValues[0].FeatureDefaultValue), NumberStyles.Any, ci);// setting for express configuration
                    objFreight.StandardFreightMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_FreightMultiplier").FeatureValues[0].FeatureDefaultValue), NumberStyles.Any, ci);//setiing for standard configuration
                    objFreight.CurrencyMultiplier = float.Parse(Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(s => s.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyConversionFactor), NumberStyles.Any, ci);
                }

                #region NEW CODE UNDER TESTING 21 OCTOBER 2016

                List<FreightManagementBE.FreightMultiplerCurrency> lstAllFreightMultiplerCurrency;
                List<FreightManagementBE.FreightConfiguration> lstAllFreightConfiguration;
                List<FreightManagementBE.FreightConfiguration> lstfilterFreightConfiguration;
                Exceptions.WriteInfoLog("Before Calling FreightManagementBL.GetAllFreightMultiplerCurrency() CalculateFreight()");
                lstAllFreightMultiplerCurrency = FreightManagementBL.GetAllFreightMultiplerCurrency<FreightManagementBE.FreightMultiplerCurrency>(0, "CM");

                lstAllFreightConfiguration = FreightManagementBL.GetAllFreightConfiguration<FreightManagementBE.FreightConfiguration>(objFreight.CountryCode, objFreight.FreightRegionName, Convert.ToInt16(GlobalFunctions.GetLanguageId())); // Modified Last parameter FreightModeID to Language ID 11 nov 2016 SHRIGANESH
                int standardfreightconfigurationid = 0;
                int expressfreightconfigurationid = 0;
                int palletshipmentConfigurationid = 0;

                if (lstAllFreightConfiguration != null)
                {
                    lstfilterFreightConfiguration = lstAllFreightConfiguration.Where(x => x.FreightModeId == 1).ToList();
                    if (lstfilterFreightConfiguration.Count > 0)
                    {
                        standardfreightconfigurationid = lstfilterFreightConfiguration.FirstOrDefault().FreightConfigurationId;
                    }
                    lstfilterFreightConfiguration = lstAllFreightConfiguration.Where(x => x.FreightModeId == 2).ToList();
                    if (lstfilterFreightConfiguration.Count > 0)
                    {
                        expressfreightconfigurationid = lstfilterFreightConfiguration.FirstOrDefault().FreightConfigurationId;
                    }
                    lstfilterFreightConfiguration = lstAllFreightConfiguration.Where(x => x.FreightModeId == 3).ToList();
                    if (lstfilterFreightConfiguration.Count > 0)
                    {
                        palletshipmentConfigurationid = lstfilterFreightConfiguration.FirstOrDefault().FreightConfigurationId;
                    }
                }

                Exceptions.WriteInfoLog("Before Calling FreightManagementBL.GetAllFreightMultiplerCurrency() CalculateFreight()");
                if (lstAllFreightMultiplerCurrency != null)
                {
                    if (lstAllFreightMultiplerCurrency.Count > 0)
                    {
                        decimal fFreightMultiplier = 0.00M;
                        decimal sFreightMultiplier = 0.00M;
                        decimal pFreightMultiplier = 0.00M; // Freight Multiplier for Pallet Shippment
                        Exceptions.WriteInfoLog("Inside lstAllFreightMultiplerCurrency.Count > 0 in CalculateFreight()");
                        lstAllFreightMultiplerCurrency = lstAllFreightMultiplerCurrency.Where(x => x.CurrencyID == GlobalFunctions.GetCurrencyId()).ToList();

                        // 0 0 1
                        if (standardfreightconfigurationid == 0 && expressfreightconfigurationid == 0 && palletshipmentConfigurationid != 0)
                        {
                            fFreightMultiplier = 0.00M;
                            sFreightMultiplier = 0.00M;
                            pFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == palletshipmentConfigurationid).CValue;
                        }
                        // 0 1 0
                        if (standardfreightconfigurationid == 0 && expressfreightconfigurationid != 0 && palletshipmentConfigurationid == 0)
                        {
                            fFreightMultiplier = 0.00M;
                            sFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == expressfreightconfigurationid).CValue;
                            pFreightMultiplier = 0.00M;
                        }
                        // 0 1 1 
                        if (standardfreightconfigurationid == 0 && expressfreightconfigurationid != 0 && palletshipmentConfigurationid != 0)
                        {
                            fFreightMultiplier = 0.00M;
                            sFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == expressfreightconfigurationid).CValue;
                            pFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == palletshipmentConfigurationid).CValue;
                        }
                        // 1 0 0
                        if (standardfreightconfigurationid != 0 && expressfreightconfigurationid == 0 && palletshipmentConfigurationid == 0)
                        {
                            fFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == standardfreightconfigurationid).CValue;
                            sFreightMultiplier = 0.00M;
                            pFreightMultiplier = 0.00M;
                        }
                        //1 0 1
                        if (standardfreightconfigurationid != 0 && expressfreightconfigurationid == 0 && palletshipmentConfigurationid != 0)
                        {
                            fFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == standardfreightconfigurationid).CValue;
                            sFreightMultiplier = 0.00M;
                            pFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == palletshipmentConfigurationid).CValue;
                        }
                        // 1 1 0
                        if (standardfreightconfigurationid != 0 && expressfreightconfigurationid != 0 && palletshipmentConfigurationid == 0)
                        {
                            fFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == standardfreightconfigurationid).CValue;
                            sFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == expressfreightconfigurationid).CValue;
                            pFreightMultiplier = 0.00M;
                        }
                        // 1 1 1
                        if (standardfreightconfigurationid != 0 && expressfreightconfigurationid != 0 && palletshipmentConfigurationid != 0)
                        {
                            fFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == standardfreightconfigurationid).CValue;
                            sFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == expressfreightconfigurationid).CValue;
                            pFreightMultiplier = lstAllFreightMultiplerCurrency.FirstOrDefault(x => x.FreightConfigurationId == palletshipmentConfigurationid).CValue;
                        }
                        objFreight.FreightMultiplier = float.Parse(Convert.ToString(fFreightMultiplier));
                        objFreight.StandardFreightMultiplier = float.Parse(Convert.ToString(sFreightMultiplier));
                        objFreight.PalletShipmentFreightMultiplier = float.Parse(Convert.ToString(pFreightMultiplier));
                        Exceptions.WriteInfoLog("Exiting lstAllFreightMultiplerCurrency.Count > 0 in CalculateFreight()");
                    }
                }

                #endregion

                #region Commented Region on 11 Nov 2016 SHRIGANESH

                #endregion

                List<object> lstShoppingBE = new List<object>();
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                double OrderItemTotal = 0;
                if (lstUser != null && lstUser.UserId != 0)
                {
                    Exceptions.WriteInfoLog("Inside lstUser != null && lstUser.UserId != 0 in CalculateFreight()");
                    Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData in CalculateFreight()");
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                    Exceptions.WriteInfoLog("Exiting calling ShoppingCartBL.GetBasketProductsData in CalculateFreight()");
                }
                else
                {
                    Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData in ELSE in CalculateFreight()");
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                    Exceptions.WriteInfoLog("After calling ShoppingCartBL.GetBasketProductsData in ELSE in CalculateFreight()");
                }
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                    lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    for (int i = 0; i < lstShoppingBE.Count; i++)
                    {
                        double OrderItemAddTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                        //if (DiscountPercentage == 0)
                        OrderItemTotal += Convert.ToDouble(Convert.ToDecimal(OrderItemAddTotal));
                    }
                }

                objFreight.OrderValue = OrderItemTotal;
                List<FreightManagementBE> lstFreight = new List<FreightManagementBE>();
                Exceptions.WriteInfoLog("Before calling FreightManagementBL.GetFreightDetails in CalculateFreight()");
                lstFreight = FreightManagementBL.GetFreightDetails(objFreight);
                Exceptions.WriteInfoLog("After calling FreightManagementBL.GetFreightDetails in CalculateFreight()");
                FreightManagementBE CurrFreightConfig = new FreightManagementBE();
                Exceptions.WriteInfoLog("Exiting objStoreBE != null in CalculateFreight()");
                return lstFreight;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Snehal
        /// Date    : 13-09-2016
        /// Scope   : Calculate freight charges for standard and express zone
        /// </summary>            
        /// <returns></returns>
        protected void SetFreightValues_Invoice()
        {
            try
            {
                Exceptions.WriteInfoLog("Inside SetFreightValues_Invoice()");
                HtmlGenericControl spnShippingPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnShippingPrice");
                spnShippingPrice.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                HtmlGenericControl spnTaxPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTaxPrice");

                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                Exceptions.WriteInfoLog("Before Calling CalculateFreight() in SetFreightValues_Invoice()");
                lstFreightBE = CalculateFreight(Convert.ToInt16(12)); // Hardcoded SHRIGANESH SINGH 14 Sept 16
                Exceptions.WriteInfoLog("After Calling CalculateFreight() in SetFreightValues_Invoice()");
                List<CountryBE> lstCountry = new List<CountryBE>();
                Exceptions.WriteInfoLog("Before Calling CountryBL.GetAllCountries() in SetFreightValues_Invoice()");
                lstCountry = CountryBL.GetAllCountries();
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    if (!string.Equals(ddlInvoiceCountry.SelectedValue, "0"))
                    {
                        Exceptions.WriteInfoLog("Inside !string.Equals(ddlDCountry.SelectedValue, 0) in SetFreightValues_Invoice()");
                        if (lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(ddlInvoiceCountry.SelectedValue)).DisplayDutyMessage)
                        { divDutyMessage.Attributes.Add("style", "display:''"); }
                    }
                }


                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    Exceptions.WriteInfoLog("Inside lstFreightBE != null && lstFreightBE.Count > 0 in SetFreightValues_Invoice()");

                    hidService1.Value = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                    rbService2.Value = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                    spnService1Text.InnerHtml = lstFreightBE[0].StandardMethodName + " (" + lstFreightBE[0].SZTransitTime + ")";
                    spnService2Text.InnerHtml = lstFreightBE[0].ExpressMethodName + " (" + lstFreightBE[0].EZTransitTime + ")";
                    if (lstFreightBE[0].IsStandard)
                    {
                        Exceptions.WriteInfoLog("Inside lstFreightBE[0].IsStandard in SetFreightValues_Invoice()");
                        divService1Cont.Attributes.Add("style", "display:''");
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Inside else lstFreightBE[0].IsStandard in SetFreightValues_Invoice()");
                        divService1Cont.Attributes.Add("style", "display:none");
                    }
                    if (lstFreightBE[0].IsExpress)
                    {
                        Exceptions.WriteInfoLog("Inside lstFreightBE[0].IsExpress in SetFreightValues_Invoice()");
                        divService2Cont.Attributes.Add("style", "display:''");
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Inside else lstFreightBE[0].IsExpress in SetFreightValues_Invoice()");
                        divService2Cont.Attributes.Add("style", "display:none");
                    }

                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        spnService1.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstFreightBE[0].SZPrice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                        spnService2.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstFreightBE[0].EZPrice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                        {
                            Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) in SetFreightValues_Invoice()");
                            spnService1.InnerHtml = Convert.ToDecimal(lstFreightBE[0].SZPrice).ToString("##,###,##0.#0");
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Inside Else !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) in SetFreightValues_Invoice()");
                            spnService1.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                        }
                        if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                        {
                            Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) in SetFreightValues_Invoice()");
                            spnService2.InnerHtml = Convert.ToDecimal(lstFreightBE[0].EZPrice).ToString("##,###,##0.#0");
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Inside Else !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) in SetFreightValues_Invoice()");
                            spnService2.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                        }
                    }

                    if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                    {
                        if (Convert.ToDouble(lstFreightBE[0].SZPrice) >= 0)
                        {
                            Exceptions.WriteInfoLog("Inside Convert.ToDouble(lstFreightBE[0].SZPrice) >= 0 in SetFreightValues_Invoice()");
                            rbService1.Disabled = false;
                            rbService1.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].SZPrice));
                        }
                    }

                    if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                    {
                        if (Convert.ToDouble(lstFreightBE[0].EZPrice) >= 0)
                        {
                            Exceptions.WriteInfoLog("Inside Convert.ToDouble(lstFreightBE[0].EZPrice) >= 0 in SetFreightValues()");
                            rbService2.Disabled = false;
                            if (!rbService1.Checked)
                            {
                                rbService2.Checked = true;
                                spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].EZPrice));
                            }
                        }
                    }

                    if (!(lstCustomerOrders == null || (lstCustomerOrders.StandardCharges == 0 && lstCustomerOrders.ExpressCharges == 0)))
                    {
                        if (lstCustomerOrders.StandardCharges != 0)
                        {
                            Exceptions.WriteInfoLog("Inside lstCustomerOrders.StandardCharges != 0 in SetFreightValues_Invoice()");
                            rbService1.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.StandardCharges));
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Inside ELSE lstCustomerOrders.StandardCharges != 0 in SetFreightValues_Invoice()");
                            rbService2.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.ExpressCharges));
                        }
                        spnTaxPrice.InnerHtml = Convert.ToString(lstCustomerOrders.TotalTax);
                    }
                    spnTaxPrice.InnerHtml = "0";
                    #region corrected by vikram to get proper tax start

                    decimal dSZPrice = 0, dEZPrice = 0;
                    if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                    {
                        Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) in SetFreightValues_Invoice()");
                        dSZPrice = Convert.ToDecimal(lstFreightBE[0].SZPrice);
                    }
                    if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                    {
                        Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) in SetFreightValues_Invoice()");
                        dEZPrice = Convert.ToDecimal(lstFreightBE[0].EZPrice);
                    }
                    Exceptions.WriteInfoLog("Before calling CalculateTax in SetFreightValues_Invoice()");
                    string strTotalTax = CalculateTax(Convert.ToInt16(ddlInvoiceCountry.SelectedValue), dSZPrice.ToString(CultureInfo.InvariantCulture), dEZPrice.ToString(CultureInfo.InvariantCulture), 0);// Hardcoded SHRIGANESH SINGH 14 Sept 16
                    Exceptions.WriteInfoLog("After calling CalculateTax in SetFreightValues_Invoice()");

                    #endregion end
                    if (!string.IsNullOrEmpty(strTotalTax))
                    {
                        string[] strTax = strTotalTax.Split('|');
                        if (strTax.Length > 0)
                        {

                            if (GlobalFunctions.IsPointsEnbled())
                            {
                                hidService1Tax.Value = GlobalFunctions.DisplayPriceOrPoints(strTax[0], GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                                hidService2Tax.Value = GlobalFunctions.DisplayPriceOrPoints(strTax[1], GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                            }
                            else
                            {
                                hidService1Tax.Value = strTax[0];
                                hidService2Tax.Value = strTax[1];
                            }
                            if (rbService1.Checked)
                            {
                                spnTaxPrice.InnerHtml = Convert.ToDecimal(strTax[0]).ToString("##,###,##0.#0");
                            }
                            else
                            {
                                spnTaxPrice.InnerHtml = Convert.ToDecimal(strTax[1]).ToString("##,###,##0.#0");
                            }
                        }
                    }
                    if (!(lstCustomerOrders == null || lstCustomerOrders.TotalTax == 0))
                    {
                        spnTaxPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.TotalTax));
                    }

                    #region Show when the error msg appears.
                    divService1Cont.Visible = true;
                    divService2Cont.Visible = true;
                    #endregion
                    if (rbService1.Checked && lstFreightBE[0].SZDisallowOrder)
                    {
                        divMOVMessage.Attributes.Add("style", "display:''");
                        #region Hide when the error msg appears.
                        divService1Cont.Visible = false;
                        divService2Cont.Visible = false;
                        #endregion
                        GlobalFunctions.ShowModalAlertMessages(this.Page, divMOVMessage.InnerHtml, AlertType.Warning);
                    }
                    else if (lstFreightBE[0].EZDisallowOrder)
                    {
                        divMOVMessage.Attributes.Add("style", "display:''");
                        #region Hide when the error msg appears.
                        divService1Cont.Visible = false;
                        divService2Cont.Visible = false;
                        #endregion
                        GlobalFunctions.ShowModalAlertMessages(this.Page, divMOVMessage.InnerHtml, AlertType.Warning);
                    }
                }
                else
                {
                    divService2Cont.Attributes.Add("style", "display:none");
                    divService1Cont.Attributes.Add("style", "display:none");
                }
                Exceptions.WriteInfoLog("Exiting SetFreightvalues()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        //Added By Ravi Gohil On 24-04-2017//
        [System.Web.Services.WebMethod]
        public static string PopulateAddess()
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
            {
                DeliveryAddress();
            }
            string JeasonData = string.Empty;
            try
            {
                Exceptions.WriteInfoLog("Inside PopulateAddress()");
                UserBE lstUser = new UserBE();
                List<UserBE.UserDeliveryAddressBE> lstNew = new List<UserBE.UserDeliveryAddressBE>();
                List<UserBE.UserDeliveryAddressBE> lstUpdatedNewAdd = new List<UserBE.UserDeliveryAddressBE>();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                if (lstUser != null && lstUser.UserDeliveryAddress.Count > 0)
                {
                    lstNew = lstUser.UserDeliveryAddress.FindAll(x => x.IsDefault);
                    if (lstNew.Count > 0)
                    {
                        Exceptions.WriteInfoLog("Before passing JSON in IF from PopulateAddess()");
                        System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        JeasonData = jSearializer.Serialize(lstNew);
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Before passing JSON in ELSE from PopulateAddess()");
                        List<UserBE.UserDeliveryAddressBE> lstBlank = new List<UserBE.UserDeliveryAddressBE>();
                        System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        JeasonData = null;
                    }
                }
                else
                {
                    JeasonData = null;
                }
                Exceptions.WriteInfoLog("Exiting PopulateAddess()");
                return JeasonData;
            }


            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 24-09-15
        /// Scope   : Calculate freight charges for standard and express zone
        /// </summary>            
        /// <returns></returns>
        protected void SetFreightValues()
        {
            try
            {

              
                     

                List<ShoppingCartAdditionalBE> lstShoppingBE = new List<ShoppingCartAdditionalBE>();
                UserBE lstUser = new UserBE();

                lstUser = HttpContext.Current.Session["User"] as UserBE;

                if (lstUser != null && lstUser.UserId != 0)
                {  
                    lstShoppingBE = ShoppingCartBL.GetAdditionalData(GlobalFunctions.GetCurrencyId(), lstUser.UserId, "");

                }
                else
                {

                    lstShoppingBE = ShoppingCartBL.GetAdditionalData(GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID);

                }

                additionalcharge = Convert.ToDecimal(lstShoppingBE[0].Additionalcharges);
                HtmlGenericControl spnadditionalcharge = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnadditionalcharge");
                spnadditionalcharge.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                HtmlGenericControl divaddtionalcharges = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("divaddtionalcharges");

                HtmlGenericControl spntotalnet = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spntotalnet");
                spntotalnet.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                HtmlGenericControl divtotalnet = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("divtotalnet");


                if (additionalcharge != 0)
                {
                    Exceptions.WriteInfoLog("Inside lstFreightBE[0].IsStandard in SetFreightValues()");
                    divaddtionalcharges.Visible = true;
                    divtotalnet.Visible = true;
                    spnadditionalcharge.InnerHtml = Convert.ToString(additionalcharge.ToString("##,###,##0.#0"));
                    // divaddtionalcharges.InnerHtml = strAddtionalcharges +" "+ GlobalFunctions.GetCurrencySymbol() + " " + additionalcharge;
                    //divaddtionalcharges.Attributes.Add("style", "display:''");
                }
                else
                {
                    Exceptions.WriteInfoLog("Inside else lstFreightBE[0].IsStandard in SetFreightValues()");
                    divaddtionalcharges.Visible = false;
                    if (objStoreBE.StoreId == 383)
                    {
                        divtotalnet.Visible = true;
                    }
                    else
                    {
                        divtotalnet.Visible = false;
                    }
                    spnadditionalcharge.InnerHtml = "0";
                    //divaddtionalcharges.Attributes.Add("style", "display:none");
                }


                Exceptions.WriteInfoLog("Inside SetFreightValues()");
                HtmlGenericControl spnShippingPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnShippingPrice");
                spnShippingPrice.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                HtmlGenericControl spnTaxPrice = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnTaxPrice");

                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                Exceptions.WriteInfoLog("Before Calling CalculateFreight() in SetFreightValues()");
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    lstFreightBE = CalculateFreight(Convert.ToInt16(ddlDCountry.SelectedValue));
                }
                else
                {
                    IsMultifreight = true;
                    lstFreightBE = CalculateFreight(Convert.ToInt16(ddlCountry.SelectedValue));
                }
               
                Exceptions.WriteInfoLog("After Calling CalculateFreight() in SetFreightValues()");
                List<CountryBE> lstCountry = new List<CountryBE>();
                Exceptions.WriteInfoLog("Before Calling CountryBL.GetAllCountries() in SetFreightValues()");
                lstCountry = CountryBL.GetAllCountries();
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    if (!string.Equals(ddlDCountry.SelectedValue, "0"))
                    {
                        Exceptions.WriteInfoLog("Inside !string.Equals(ddlDCountry.SelectedValue, 0) in SetFreightValues()");
                        if (lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(ddlDCountry.SelectedValue)).DisplayDutyMessage)
                        { divDutyMessage.Attributes.Add("style", "display:''"); }
                    }
                }

                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    Exceptions.WriteInfoLog("Inside lstFreightBE != null && lstFreightBE.Count > 0 in SetFreightValues()");

                    hidService1.Value = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                    hidService2.Value = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                    hidService3.Value = !string.IsNullOrEmpty(lstFreightBE[0].PSPrice) ? lstFreightBE[0].PSPrice : "0";

                    spnService1Text.InnerHtml = lstFreightBE[0].StandardMethodName + " (" + lstFreightBE[0].SZTransitTime + ")";
                    spnService2Text.InnerHtml = lstFreightBE[0].ExpressMethodName + " (" + lstFreightBE[0].EZTransitTime + ")";
                    spnService3Text.InnerHtml = lstFreightBE[0].palletMethodName + " (" + lstFreightBE[0].PZTransitTime + ")";

                    hdncustom1.Value = lstFreightBE[0].SZCustomText;
                    hdncustom2.Value = lstFreightBE[0].EZCustomText;
                    hdncustom3.Value = lstFreightBE[0].PZCustomText;

                    if (lstFreightBE[0].IsStandard)
                    {
                        Exceptions.WriteInfoLog("Inside lstFreightBE[0].IsStandard in SetFreightValues()");
                        divService1Cont.Attributes.Add("style", "display:''");
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Inside else lstFreightBE[0].IsStandard in SetFreightValues()");
                        divService1Cont.Attributes.Add("style", "display:none");
                    }
                    if (lstFreightBE[0].IsExpress)
                    {
                        Exceptions.WriteInfoLog("Inside lstFreightBE[0].IsExpress in SetFreightValues()");
                        divService2Cont.Attributes.Add("style", "display:''");
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Inside else lstFreightBE[0].IsExpress in SetFreightValues()");
                        divService2Cont.Attributes.Add("style", "display:none");
                    }

                    if (lstFreightBE[0].IsPalletShipment)
                    {
                        Exceptions.WriteInfoLog("Inside lstFreightBE[0].IsExpress in SetFreightValues()");
                        divService3Cont.Attributes.Add("style", "display:''");
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Inside else lstFreightBE[0].IsExpress in SetFreightValues()");
                        divService3Cont.Attributes.Add("style", "display:none");
                    }


                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        spnService1.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstFreightBE[0].SZPrice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                        divService1Cont.Attributes.Add("style", "");
                        spnService2.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstFreightBE[0].EZPrice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                        spnService3.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstFreightBE[0].PSPrice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

                        divService2Cont.Attributes.Add("style", "display:none");
                        divService3Cont.Attributes.Add("style", "display:none");
                    }
                    else
                    {

                        if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                        {
                            Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) in SetFreightValues()");
                            spnService1.InnerHtml = Convert.ToDecimal((lstFreightBE[0].SZPrice)).ToString("##,###,##0.#0");
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Inside Else !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) in SetFreightValues()");
                            spnService1.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                        }
                        if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                        {
                            Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) in SetFreightValues()");
                            spnService2.InnerHtml = Convert.ToDecimal((lstFreightBE[0].EZPrice)).ToString("##,###,##0.#0");
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Inside Else !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) in SetFreightValues()");
                            spnService2.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                        }

                        if (!string.IsNullOrEmpty(lstFreightBE[0].PSPrice))
                        {
                            Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) in SetFreightValues()");
                            spnService3.InnerHtml = Convert.ToDecimal((lstFreightBE[0].PSPrice)).ToString("##,###,##0.#0");
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Inside Else !string.IsNullOrEmpty(lstFreightBE[0].PsPrice) in SetFreightValues()");
                            spnService3.InnerHtml = Convert.ToDouble(0).ToString("##,###,##0.#0");
                        }
                    }

                    if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                    {
                        if (Convert.ToDouble(lstFreightBE[0].SZPrice) >= 0 && lstFreightBE[0].IsStandard)
                        {
                            Exceptions.WriteInfoLog("Inside Convert.ToDouble(lstFreightBE[0].SZPrice) >= 0 in SetFreightValues()");
                            rbService2.Disabled = false;
                            rbService3.Disabled = false;
                            rbService1.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].SZPrice));

                        }
                    }

                    if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                    {
                        if (Convert.ToDouble(lstFreightBE[0].EZPrice) >= 0 && lstFreightBE[0].IsExpress)
                        {
                            Exceptions.WriteInfoLog("Inside Convert.ToDouble(lstFreightBE[0].EZPrice) >= 0 in SetFreightValues()");
                            rbService2.Disabled = false;
                            rbService3.Disabled = false;
                            if (!rbService1.Checked)
                            {
                                rbService2.Checked = true;
                                spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].EZPrice));
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(lstFreightBE[0].PSPrice))
                    {
                        if (Convert.ToDouble(lstFreightBE[0].PSPrice) >= 0 && lstFreightBE[0].IsExpress)
                        {
                            Exceptions.WriteInfoLog("Inside Convert.ToDouble(lstFreightBE[0].EZPrice) >= 0 in SetFreightValues()");
                            rbService2.Disabled = false;
                            rbService3.Disabled = false;
                            if (!rbService1.Checked && !rbService2.Checked)
                            {
                                rbService3.Checked = true;
                                spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstFreightBE[0].PSPrice));
                            }
                        }
                    }


                    if (!(lstCustomerOrders == null || (lstCustomerOrders.StandardCharges == 0 && lstCustomerOrders.ExpressCharges == 0 && lstCustomerOrders.PalletCharges == 0)))
                    {
                        if (lstCustomerOrders.StandardCharges != 0)
                        {
                            Exceptions.WriteInfoLog("Inside lstCustomerOrders.StandardCharges != 0 in SetFreightValues()");
                            rbService1.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.StandardCharges));
                        }
                        else if (lstCustomerOrders.ExpressCharges != 0)
                        {
                            Exceptions.WriteInfoLog("Inside ELSE lstCustomerOrders.StandardCharges != 0 in SetFreightValues()");
                            rbService2.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.ExpressCharges));
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Inside ELSE lstCustomerOrders.StandardCharges != 0 in SetFreightValues()");
                            rbService3.Checked = true;
                            spnShippingPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.PalletCharges));
                        }
                        spnTaxPrice.InnerHtml = Convert.ToString(lstCustomerOrders.TotalTax);
                    }
                    spnTaxPrice.InnerHtml = "0";
                    #region corrected by vikram to get proper tax start
                    // string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue),lstFreightBE[0].SZPrice, lstFreightBE[0].EZPrice, 0);
                    decimal dSZPrice = 0, dEZPrice = 0, dPZPrice = 0;
                    if (!string.IsNullOrEmpty(lstFreightBE[0].SZPrice))
                    {
                        Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) in SetFreightValues()");
                        dSZPrice = Convert.ToDecimal(lstFreightBE[0].SZPrice);
                    }
                    if (!string.IsNullOrEmpty(lstFreightBE[0].EZPrice))
                    {
                        Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) in SetFreightValues()");
                        dEZPrice = Convert.ToDecimal(lstFreightBE[0].EZPrice);
                    }
                    if (!string.IsNullOrEmpty(lstFreightBE[0].PSPrice))
                    {
                        Exceptions.WriteInfoLog("Inside !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) in SetFreightValues()");
                        dPZPrice = Convert.ToDecimal(lstFreightBE[0].PSPrice);
                    }

                    if (additionalcharge != 0)
                    {
                        dSZPrice = dSZPrice + additionalcharge;
                        dEZPrice = dEZPrice + additionalcharge;
                        dPZPrice = dPZPrice + additionalcharge;
                    }

                    Exceptions.WriteInfoLog("Before calling CalculateTax in SetFreightValues()");
                    
                    
                    string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), dSZPrice.ToString(CultureInfo.InvariantCulture), dEZPrice.ToString(CultureInfo.InvariantCulture), dPZPrice.ToString(CultureInfo.InvariantCulture), 0);
                    Exceptions.WriteInfoLog("After calling CalculateTax in SetFreightValues()");
                    //string strTotalTax = CalculateTax(Convert.ToInt16(ddlDCountry.SelectedValue), Convert.ToDecimal(lstFreightBE[0].SZPrice).ToString(CultureInfo.InvariantCulture), Convert.ToDecimal(lstFreightBE[0].EZPrice).ToString(CultureInfo.InvariantCulture), 0);
                    #endregion end
                    if (!string.IsNullOrEmpty(strTotalTax))
                    {
                        string[] strTax = strTotalTax.Split('|');
                        if (strTax.Length > 0)
                        {
                            if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                            {
                                //    hidService1Tax.Value = GlobalFunctions.DisplayPriceOrPoints(strTax[0], GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                                //    hidService2Tax.Value = GlobalFunctions.DisplayPriceOrPoints(strTax[1], GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                                //    hidService3Tax.Value = GlobalFunctions.DisplayPriceOrPoints(strTax[2], GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                                hidService1Tax.Value = strTax[0];
                                hidService2Tax.Value = strTax[1];
                                hidService3Tax.Value = strTax[2];
                            }
                            else
                            {
                                hidService1Tax.Value = strTax[0];
                                hidService2Tax.Value = strTax[1];
                                hidService3Tax.Value = strTax[2];
                            }
                            if (rbService1.Checked)
                            {
                                //spnTaxPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[0]));
                                spnTaxPrice.InnerHtml = Convert.ToDecimal(strTax[0]).ToString("##,###,##0.#0");
                            }
                            else if (rbService2.Checked)
                            {
                                //spnTaxPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(strTax[1]));
                                spnTaxPrice.InnerHtml = Convert.ToDecimal(strTax[1]).ToString("##,###,##0.#0");
                            }
                            else
                            {
                                spnTaxPrice.InnerHtml = Convert.ToDecimal(strTax[2]).ToString("##,###,##0.#0");

                            }
                        }
                    }
                    if (!(lstCustomerOrders == null || lstCustomerOrders.TotalTax == 0))
                    {
                        spnTaxPrice.InnerHtml = GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(lstCustomerOrders.TotalTax));
                    }

                    #region Show when the error msg appears.
                    divService1Cont.Visible = true;
                    divService2Cont.Visible = true;
                    divService3Cont.Visible = true;
                    #endregion
                    if (rbService1.Checked && lstFreightBE[0].SZDisallowOrder)
                    {
                        divMOVMessage.Attributes.Add("style", "display:''");
                        #region Hide when the error msg appears.
                        divService1Cont.Visible = true;
                        divService2Cont.Visible = false;
                        divService3Cont.Visible = false;
                        #endregion
                        GlobalFunctions.ShowModalAlertMessages(this.Page, divMOVMessage.InnerHtml, AlertType.Warning);
                    }
                    else if (lstFreightBE[0].EZDisallowOrder)
                    {
                        divMOVMessage.Attributes.Add("style", "display:''");
                        #region Hide when the error msg appears.
                        divService1Cont.Visible = false;
                        divService2Cont.Visible = true;
                        divService3Cont.Visible = false;
                        #endregion
                        GlobalFunctions.ShowModalAlertMessages(this.Page, divMOVMessage.InnerHtml, AlertType.Warning);
                    }
                    else if (lstFreightBE[0].PZDisallowOrder)
                    {
                        divMOVMessage.Attributes.Add("style", "display:''");
                        #region Hide when the error msg appears.
                        divService1Cont.Visible = false;
                        divService2Cont.Visible = false;
                        divService3Cont.Visible = true;
                        #endregion
                        GlobalFunctions.ShowModalAlertMessages(this.Page, divMOVMessage.InnerHtml, AlertType.Warning);
                    }
                }
                else
                {
                    divService2Cont.Attributes.Add("style", "display:none");
                    divService1Cont.Attributes.Add("style", "display:none");
                    divService3Cont.Attributes.Add("style", "display:none");
                }
                Exceptions.WriteInfoLog("Exiting SetFreightvalues()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 25-09-15
        /// Scope   : CalculateTax from BASyS
        /// </summary>            
        /// <returns></returns>
        protected static string CalculateTax(Int16 intDeliveryCountryId, string strStandardFreightCharges, string strExpressFreightCharges, double DiscountPercentage)
        {
            double dStandardTotalTax = 0;
            double dExpressTotalTax = 0;
            try
            {
                Exceptions.WriteInfoLog("Inside CalculateTax()");
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails() in CalculateTax()");
                UserBE lstUser = new UserBE();
                string strCustomerContactId = string.Empty;
                string strTaxNo = string.Empty;
                if (objStoreBE != null && objStoreBE.IsBASYS)
                {
                    Exceptions.WriteInfoLog("Inside objStoreBE != null && objStoreBE.IsBASYS In CalculateTax");
                    if (HttpContext.Current.Session["User"] != null)
                    {
                        lstUser = HttpContext.Current.Session["User"] as UserBE;
                        strCustomerContactId = lstUser.BASYS_CustomerContactId;
                    }
                    else
                    {
                        if (HttpContext.Current.Session["GuestUser"] == null)
                            return "";
                        else
                        {
                            StoreBE.StoreCurrencyBE objCurrency = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId());
                            strCustomerContactId = Convert.ToString(objCurrency.Default_CUST_CONTACT_ID);
                        }
                    }
                    if (string.IsNullOrEmpty(strCustomerContactId))
                    {
                        StoreBE.StoreCurrencyBE objCurrency = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId());
                        strCustomerContactId = Convert.ToString(objCurrency.Default_CUST_CONTACT_ID);
                    }

                    string strCountryCode = string.Empty;
                    string strCountryName = string.Empty;
                    Int32 intCustomerOrderId = Convert.ToInt32(HttpContext.Current.Session["CustomerOrderId"]);
                    List<CountryBE> lstCountry = new List<CountryBE>();
                    Exceptions.WriteInfoLog("Before calling CountryBL.GetAllCountries() In CalculateTax");
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        strCountryCode = lstCountry.FirstOrDefault(x => x.CountryId == intDeliveryCountryId).CountryCode;
                        strCountryName = lstCountry.FirstOrDefault(x => x.CountryId == intDeliveryCountryId).CountryName;
                    }

                    Webservice_OASIS wsTax = new Webservice_OASIS();
                    DataTable dtTax = new DataTable();
                    ArrayList arrProducts = new ArrayList();

                    List<object> lstShoppingBE = new List<object>();
                    if (HttpContext.Current.Session["GuestUser"] == null)
                    {
                        Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData for Reg User In CalculateTax");
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData for Guest User In CalculateTax");
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                    }
                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                        lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();

                        double OrderItemTotal = 0.0;
                        string strBASYSProductId = string.Empty;
                        for (int i = 0; i < lstShoppingBE.Count; i++)
                        {
                            strBASYSProductId = Convert.ToString(lstNewShopping[i].BASYSProductId);
                            arrProducts.Add(strBASYSProductId);
                            double price = DiscountPercentage > 0 ?
                                            Convert.ToDouble(lstNewShopping[i].Price) - (Convert.ToDouble(lstNewShopping[i].Price) * DiscountPercentage * 0.01) : Convert.ToDouble(lstNewShopping[i].Price);
                            OrderItemTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * (price);

                            arrProducts.Add(OrderItemTotal.ToString(CultureInfo.InvariantCulture));

                            FreightSourceCountryMasterBE objFreightSourceCountryMaster = FreightManagementBL.GetFreightSourceCountryMaster(strCountryCode);
                            string fscountryName = objFreightSourceCountryMaster.FreightSourceCountryName;
                            string fsCountryCode = objFreightSourceCountryMaster.FreightSourceCountryCode;

                            arrProducts.Add(objFreightSourceCountryMaster.FreightSourceCountryName);
                            arrProducts.Add(objFreightSourceCountryMaster.FreightSourceCountryCode);
                        }

                        Exceptions.WriteInfoLog("Before calling wsTax.getTaxDetails for Standard In CalculateTax");
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                        {
                            List<InvoiceBE> lstSelectedEntityDetails = new List<InvoiceBE>();
                            lstSelectedEntityDetails = HttpContext.Current.Session["SelectedEntityDetails"] as List<InvoiceBE>;
                            if (lstSelectedEntityDetails == null)
                            {
                                strTaxNo = "";
                            }
                            else
                            {
                                strCustomerContactId = Convert.ToString(lstSelectedEntityDetails[0].Contact_Id);
                                strTaxNo = Convert.ToString(lstSelectedEntityDetails[0].Tax_Number);
                            }
                        }

                        dtTax = wsTax.getTaxDetails(strCustomerContactId, strTaxNo, HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strStandardFreightCharges, CultureInfo.InvariantCulture).ToString("#######0.#0").Replace(',', '.'), arrProducts, intCustomerOrderId);
                        Exceptions.WriteInfoLog("After calling wsTax.getTaxDetails for Standard In CalculateTax");
                        if (dtTax != null)
                        {
                            if (dtTax.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtTax.Rows.Count; j++)
                                {
                                    dStandardTotalTax += Convert.ToDouble(dtTax.Rows[j]["TotalSalesTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                                }
                                dStandardTotalTax += Convert.ToDouble(dtTax.Rows[0]["FreightTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                            }
                        }

                        dtTax = null;
                        Exceptions.WriteInfoLog("Before calling wsTax.getTaxDetails for Express In CalculateTax");
                        dtTax = wsTax.getTaxDetails(strCustomerContactId, strTaxNo, HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strExpressFreightCharges, CultureInfo.InvariantCulture).ToString("#######0.#0").Replace(',', '.'), arrProducts, intCustomerOrderId);
                        Exceptions.WriteInfoLog("After calling wsTax.getTaxDetails for Express In CalculateTax");
                        if (dtTax != null)
                        {
                            if (dtTax.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtTax.Rows.Count; j++)
                                {
                                    dExpressTotalTax += Convert.ToDouble(dtTax.Rows[j]["TotalSalesTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                                }
                                dExpressTotalTax += Convert.ToDouble(dtTax.Rows[0]["FreightTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                            }
                        }
                        HttpContext.Current.Session["TaxDetails"] = dtTax;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
            return dStandardTotalTax.ToString("##,###,##0.#0") + "|" + dExpressTotalTax.ToString("##,###,##0.#0");
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 25-09-15
        /// Scope   : CalculateTax from BASyS
        /// </summary>            
        /// <returns></returns>
        protected static string CalculateTax(Int16 intDeliveryCountryId, string strStandardFreightCharges, string strExpressFreightCharges, string strpalletFreightCharges, double DiscountPercentage)
        {
            double dStandardTotalTax = 0;
            double dExpressTotalTax = 0;
            double dPalletTotalTax = 0;
            try
            {
                Exceptions.WriteInfoLog("Inside CalculateTax()");
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails() in CalculateTax()");
                UserBE lstUser = new UserBE();
                string strCustomerContactId = string.Empty;
                string strTaxNo = string.Empty;
                if (objStoreBE != null && objStoreBE.IsBASYS)
                {
                    Exceptions.WriteInfoLog("Inside objStoreBE != null && objStoreBE.IsBASYS In CalculateTax");
                    if (HttpContext.Current.Session["User"] != null)
                    {
                        lstUser = HttpContext.Current.Session["User"] as UserBE;
                        strCustomerContactId = lstUser.BASYS_CustomerContactId;
                    }
                    else
                    {
                        if (HttpContext.Current.Session["GuestUser"] == null)
                            return "";
                        else
                        {
                            StoreBE.StoreCurrencyBE objCurrency = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId());
                            strCustomerContactId = Convert.ToString(objCurrency.Default_CUST_CONTACT_ID);
                        }
                    }
                    if (string.IsNullOrEmpty(strCustomerContactId))
                    {
                        StoreBE.StoreCurrencyBE objCurrency = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId());
                        strCustomerContactId = Convert.ToString(objCurrency.Default_CUST_CONTACT_ID);
                    }

                    string strCountryCode = string.Empty;
                    string strCountryName = string.Empty;
                    Int32 intCustomerOrderId = Convert.ToInt32(HttpContext.Current.Session["CustomerOrderId"]);
                    List<CountryBE> lstCountry = new List<CountryBE>();
                    Exceptions.WriteInfoLog("Before calling CountryBL.GetAllCountries() In CalculateTax");
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        strCountryCode = lstCountry.FirstOrDefault(x => x.CountryId == intDeliveryCountryId).CountryCode;
                        strCountryName = lstCountry.FirstOrDefault(x => x.CountryId == intDeliveryCountryId).CountryName;
                    }

                    Webservice_OASIS wsTax = new Webservice_OASIS();
                    DataTable dtTax = new DataTable();
                    ArrayList arrProducts = new ArrayList();

                    List<object> lstShoppingBE = new List<object>();
                    if (HttpContext.Current.Session["GuestUser"] == null)
                    {
                        Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData for Reg User In CalculateTax");
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Before calling ShoppingCartBL.GetBasketProductsData for Guest User In CalculateTax");
                        lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                    }
                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                        lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();

                        double OrderItemTotal = 0.0;
                        string strBASYSProductId = string.Empty;
                        for (int i = 0; i < lstShoppingBE.Count; i++)
                        {
                            strBASYSProductId = Convert.ToString(lstNewShopping[i].BASYSProductId);
                            arrProducts.Add(strBASYSProductId);
                            double price = DiscountPercentage > 0 ?
                                            Convert.ToDouble(lstNewShopping[i].Price) - (Convert.ToDouble(lstNewShopping[i].Price) * DiscountPercentage * 0.01) : Convert.ToDouble(lstNewShopping[i].Price);
                            OrderItemTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * (price);

                            arrProducts.Add(OrderItemTotal.ToString(CultureInfo.InvariantCulture));

                            FreightSourceCountryMasterBE objFreightSourceCountryMaster = FreightManagementBL.GetFreightSourceCountryMaster(strCountryCode);
                            string fscountryName = objFreightSourceCountryMaster.FreightSourceCountryName;
                            string fsCountryCode = objFreightSourceCountryMaster.FreightSourceCountryCode;

                            arrProducts.Add(objFreightSourceCountryMaster.FreightSourceCountryName);
                            arrProducts.Add(objFreightSourceCountryMaster.FreightSourceCountryCode);
                        }

                        Exceptions.WriteInfoLog("Before calling wsTax.getTaxDetails for Standard In CalculateTax");
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                        {
                            List<InvoiceBE> lstSelectedEntityDetails = new List<InvoiceBE>();
                            lstSelectedEntityDetails = HttpContext.Current.Session["SelectedEntityDetails"] as List<InvoiceBE>;
                            if (lstSelectedEntityDetails == null)
                            {
                                strTaxNo = "";
                            }
                            else
                            {
                                strCustomerContactId = Convert.ToString(lstSelectedEntityDetails[0].Contact_Id);
                                strTaxNo = Convert.ToString(lstSelectedEntityDetails[0].Tax_Number);
                            }
                        }

                        dtTax = wsTax.getTaxDetails(strCustomerContactId, strTaxNo, HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strStandardFreightCharges, CultureInfo.InvariantCulture).ToString("#######0.#0").Replace(',', '.'), arrProducts, intCustomerOrderId);
                        Exceptions.WriteInfoLog("After calling wsTax.getTaxDetails for Standard In CalculateTax");
                        if (dtTax != null)
                        {
                            if (dtTax.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtTax.Rows.Count; j++)
                                {
                                    dStandardTotalTax += Convert.ToDouble(dtTax.Rows[j]["TotalSalesTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                                }
                                dStandardTotalTax += Convert.ToDouble(dtTax.Rows[0]["FreightTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                            }
                        }

                        dtTax = null;
                        Exceptions.WriteInfoLog("Before calling wsTax.getTaxDetails for Express In CalculateTax");
                        dtTax = wsTax.getTaxDetails(strCustomerContactId, strTaxNo, HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strExpressFreightCharges, CultureInfo.InvariantCulture).ToString("#######0.#0").Replace(',', '.'), arrProducts, intCustomerOrderId);
                        Exceptions.WriteInfoLog("After calling wsTax.getTaxDetails for Express In CalculateTax");
                        if (dtTax != null)
                        {
                            if (dtTax.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtTax.Rows.Count; j++)
                                {
                                    dExpressTotalTax += Convert.ToDouble(dtTax.Rows[j]["TotalSalesTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                                }
                                dExpressTotalTax += Convert.ToDouble(dtTax.Rows[0]["FreightTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                            }
                        }

                        dtTax = null;
                        Exceptions.WriteInfoLog("Before calling wsTax.getTaxDetails for Express In CalculateTax");
                        dtTax = wsTax.getTaxDetails(strCustomerContactId, strTaxNo, HttpUtility.HtmlEncode(strCountryName), strCountryCode, Convert.ToDouble(strpalletFreightCharges, CultureInfo.InvariantCulture).ToString("#######0.#0").Replace(',', '.'), arrProducts, intCustomerOrderId);
                        Exceptions.WriteInfoLog("After calling wsTax.getTaxDetails for Express In CalculateTax");
                        if (dtTax != null)
                        {
                            if (dtTax.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtTax.Rows.Count; j++)
                                {
                                    dPalletTotalTax += Convert.ToDouble(dtTax.Rows[j]["TotalSalesTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                                }
                                dPalletTotalTax += Convert.ToDouble(dtTax.Rows[0]["FreightTax"], CultureInfo.InvariantCulture);//added by Sripal CultureInfo
                            }
                        }

                        HttpContext.Current.Session["TaxDetails"] = dtTax;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
            return dStandardTotalTax.ToString("##,###,##0.#0") + "|" + dExpressTotalTax.ToString("##,###,##0.#0") + "|" + dPalletTotalTax.ToString("##,###,##0.#0");
        }



        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 14-10-15
        /// Scope   : ApplyCoupon of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="CouponCode"></param>        
        /// <param name="ShipmentType"></param>        
        /// <param name="CountryId"></param> 
        /// <returns>json data</returns>
        [System.Web.Services.WebMethod]
        public static string ApplyCoupon_bkp(string CouponCode, string ShipmentType, Int16 CountryId)
        {
            try
            {
                string strMessage = string.Empty;
                double OrderItemTotal = 0;
                double OrderValueAfterDiscount = 0;
                string StandardFreightValueAfterDiscount = "0";
                string ExpressFreightValueAfterDiscount = "0";
                string StandardTaxValueAfterDiscount = "0";
                string ExpressTaxValueAfterDiscount = "0";
                string strShipmentType = "0";
                string strBehaviourType = string.Empty;
                List<object> lstShoppingBE = new List<object>();
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                if (lstUser != null && lstUser.UserId != 0)
                {
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                        lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                        for (int i = 0; i < lstShoppingBE.Count; i++)
                        {
                            double ProductTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                            ProductTotal = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ProductTotal)));
                            OrderItemTotal = OrderItemTotal + ProductTotal;
                        }
                    }

                    CouponBE objCouponBE = new CouponBE();
                    List<CouponBE> lstCouponBE = new List<CouponBE>();
                    objCouponBE.OrderValue = float.Parse(Convert.ToString(OrderItemTotal));
                    objCouponBE.CouponCode = CouponCode.Trim();

                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == CountryId).RegionCode;
                        objCouponBE.Region = strRegionCode == "ROW" ? "Other" : strRegionCode;
                    }
                    objCouponBE.ShipmentType = ShipmentType; // 1 for Standard, 2 for express shipping method
                    lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, lstUser, GlobalFunctions.GetCurrencyId());
                    if (lstCouponBE != null && lstCouponBE.Count > 0)
                    {
                        if (lstCouponBE[0].IsActive == false)
                        {
                            strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                            //return strMessage;
                        }
                        else // vikram
                            if (lstCouponBE[0].IsExpired == true && string.IsNullOrEmpty(strMessage))
                            {
                                //strMessage = "Coupon code has been expired.";
                                strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                                //return strMessage;
                            }
                            else // vikram
                                if (lstCouponBE[0].IsValidOrderValue == false && string.IsNullOrEmpty(strMessage))
                                {
                                    if (lstCouponBE[0].MinimumOrderValue == 0)
                                    {
                                        strSC_Coupon_Code_Maximum_Order_Value = strSC_Coupon_Code_Maximum_Order_Value.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                                        strMessage = strSC_Coupon_Code_Maximum_Order_Value;
                                        //strMessage = "Coupon code is valid only for maximum order value " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue;
                                    }
                                    else if (lstCouponBE[0].MaximumOrderValue == 0)
                                    {
                                        strSC_Coupon_Code_Minimum_Order_Value = strSC_Coupon_Code_Minimum_Order_Value.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                                        strMessage = strSC_Coupon_Code_Minimum_Order_Value;
                                        //strMessage = "Coupon code is valid only for minimum order value " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue;
                                    }
                                    else
                                    {
                                        strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                                        strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                                        strMessage = strSC_Coupon_Code_Minimum_Maximum_Range;// "Coupon code is valid only for order value between " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue + " and " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue;
                                        //return strMessage;
                                    }
                                }
                                else // vikram
                                    if (lstCouponBE[0].IsValidRegion == false && string.IsNullOrEmpty(strMessage))
                                    {
                                        strMessage = strSC_Invalid_Coupon_Delivery_Country;// "Coupon code is not valid for the selected delivery country.";
                                        //return strMessage;
                                    }
                                    else // vikram
                                        if (lstCouponBE[0].IsValidShipmentType == false && string.IsNullOrEmpty(strMessage))
                                        {
                                            strMessage = strSC_Invalid_Coupon_Shipping_Method;// "Coupon code is not valid for the selected shipping method.";
                                            //return strMessage;
                                        }

                        if (string.IsNullOrEmpty(strMessage))
                        {
                            strBehaviourType = lstCouponBE[0].BehaviourType;
                            //Apply to goods only, excluding Freight Value 
                            if (lstCouponBE[0].BehaviourType.ToLower() == "product")
                            {
                                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                                lstFreightBE = CalculateFreight(CountryId);

                                if (lstFreightBE != null && lstFreightBE.Count > 0)
                                {
                                    StandardFreightValueAfterDiscount = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0"; //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0"; //Total tax for express zone

                                    string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));

                                    if (!string.IsNullOrEmpty(strTotalTax))
                                    {
                                        string[] strTax = strTotalTax.Split('|');
                                        if (strTax.Length > 0)
                                        {
                                            StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                            ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                        }
                                    }
                                }
                                OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                                strMessage = "Success";
                                //return strMessage;
                            }
                            //Apply to goods only, excluding Freight Value 
                            else if (lstCouponBE[0].BehaviourType.ToLower() == "basket")
                            {
                                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                                lstFreightBE = CalculateFreight(CountryId);

                                if (lstFreightBE != null && lstFreightBE.Count > 0)
                                {
                                    string saSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                    string saEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(saSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(saEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone

                                    string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));

                                    if (!string.IsNullOrEmpty(strTotalTax))
                                    {
                                        string[] strTax = strTotalTax.Split('|');
                                        if (strTax.Length > 0)
                                        {
                                            StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                            ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                        }
                                    }
                                }
                                OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                                strMessage = "Success";
                                //return strMessage;
                            }
                            //Free shipping excluding Freight Value
                            else if (lstCouponBE[0].BehaviourType.ToLower() == "freeshipping")
                            {
                                StandardFreightValueAfterDiscount = "0"; //Total tax for standard zone
                                ExpressFreightValueAfterDiscount = "0"; //Total tax for express zone
                                string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, 0);
                                if (!string.IsNullOrEmpty(strTotalTax))
                                {
                                    string[] strTax = strTotalTax.Split('|');
                                    if (strTax.Length > 0)
                                    {
                                        StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                        ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                    }
                                }

                                OrderValueAfterDiscount = OrderItemTotal;
                                strMessage = "Success";
                            }
                            //Discounted Shipping
                            else if (lstCouponBE[0].BehaviourType.ToLower() == "shipping")
                            {
                                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                                lstFreightBE = CalculateFreight(CountryId);

                                if (lstFreightBE != null && lstFreightBE.Count > 0)
                                {
                                    string sbSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                    string sbEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";

                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sbSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sbEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone

                                    string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, 0);

                                    if (!string.IsNullOrEmpty(strTotalTax))
                                    {
                                        string[] strTax = strTotalTax.Split('|');
                                        if (strTax.Length > 0)
                                        {
                                            StandardTaxValueAfterDiscount = strTax[0]; //Total tax for standard zone
                                            ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for express zone
                                        }
                                    }
                                }
                                OrderValueAfterDiscount = OrderItemTotal;
                                strMessage = strSuccess;// "Success";
                            }
                        }
                        strShipmentType = lstCouponBE[0].ShipmentType;
                    }
                    else
                    {
                        strMessage = strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
                    }
                }
                else
                {
                    strMessage = strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
                }
                return "[{\"Message\":\"" + strMessage + "\",\"OrderValueAfterDiscount\":\"" + OrderValueAfterDiscount + "\",\"StandardFreightValueAfterDiscount\":\"" +
                    StandardFreightValueAfterDiscount + "\",\"ExpressFreightValueAfterDiscount\":\"" + ExpressFreightValueAfterDiscount +
                    "\",\"StandardTaxValueAfterDiscount\":\"" + StandardTaxValueAfterDiscount + "\",\"ExpressTaxValueAfterDiscount\":\"" +
                    ExpressTaxValueAfterDiscount + "\",\"BehaviourType\":\"" + strBehaviourType.ToLower() + "\",\"ShipmentType\":\"" + strShipmentType.ToLower() + "\"}]";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 14-10-15
        /// Scope   : ApplyCoupon of the ShoppingCart_Basket page
        /// </summary>
        /// <param name="CouponCode"></param>        
        /// <param name="ShipmentType"></param>        
        /// <param name="CountryId"></param> 
        /// <returns>json data</returns>
        [System.Web.Services.WebMethod]
        public static string ApplyCoupon(string CouponCode, string ShipmentType, Int16 CountryId)
        {
            try
            {
                Exceptions.WriteInfoLog("Before Calling CalculateCoupon()");
                return CalculateCoupon(CouponCode, ShipmentType, CountryId);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
            }
        }

        private static string CalculateCoupon(string CouponCode, string ShipmentType, Int16 CountryId)
        {
            Exceptions.WriteInfoLog("Inside CalculateCoupon()");
            string strMessage = string.Empty;
            double OrderItemTotal = 0;
            double OrderValueAfterDiscount = 0;
            string strShipmentType = "0";
            string StandardFreightValueAfterDiscount = "0";
            string ExpressFreightValueAfterDiscount = "0";
            string PalletShipmentFreightValueAfterDiscount = "0"; // SHRIGANESH 11 Nov 2016
            string StandardTaxValueAfterDiscount = "0";
            string ExpressTaxValueAfterDiscount = "0";
            string PalletShipentTaxValueAfterDiscount = "0"; // SHRIGANESH 11 Nov 2016
            string strBehaviourType = string.Empty;
            List<object> lstShoppingBE = new List<object>();
            UserBE lstUser = new UserBE();

            try
            {
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                if (lstUser != null && lstUser.UserId != 0)
                {
                    Exceptions.WriteInfoLog("Inside lstUser != null && lstUser.UserId != 0 in CalculateCoupon()");
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), lstUser.UserId, "", true);
                }
                else
                {
                    Exceptions.WriteInfoLog("Inside ELSE of lstUser != null && lstUser.UserId != 0 in CalculateCoupon()");
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID, true);
                }
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    Exceptions.WriteInfoLog("Inside lstUser != null && lstUser.UserId != 0 in CalculateCoupon()");
                    List<ShoppingCartBE> lstNewShopping = new List<ShoppingCartBE>();
                    lstNewShopping = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    for (int i = 0; i < lstShoppingBE.Count; i++)
                    {
                        double ProductTotal = Convert.ToDouble(lstNewShopping[i].Quantity) * Convert.ToDouble(lstNewShopping[i].Price);
                        ProductTotal = Convert.ToDouble(GlobalFunctions.ConvertToDecimalPrecision(Convert.ToDecimal(ProductTotal)));
                        OrderItemTotal = OrderItemTotal + ProductTotal;
                    }
                }
                CouponBE objCouponBE = new CouponBE();
                List<CouponBE> lstCouponBE = new List<CouponBE>();
                objCouponBE.OrderValue = float.Parse(Convert.ToString(OrderItemTotal));
                objCouponBE.CouponCode = CouponCode.Trim();

                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    Exceptions.WriteInfoLog("Inside lstCountry != null && lstCountry.Count > 0 in CalculateCoupon()");
                    string strRegionCode = lstCountry.FirstOrDefault(x => x.CountryId == CountryId).RegionCode;
                    objCouponBE.Region = strRegionCode == "ROW" ? "Other" : strRegionCode;
                    if (strRegionCode == "EU")
                    {
                        objCouponBE.Region = "EU Region";
                    }
                    else if (strRegionCode == "UK")
                    {
                        objCouponBE.Region = "UK Region";
                    }
                    else
                    {
                        objCouponBE.Region = strRegionCode;
                    }
                }
                objCouponBE.ShipmentType = ShipmentType; // 1 for Standard, 2 for express shipping method

                if (lstUser != null && lstUser.UserId != 0)
                {
                    Exceptions.WriteInfoLog("Before Calling CouponBL.ValidateCouponCode in CalculateCoupon()");
                    lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, lstUser, GlobalFunctions.GetCurrencyId());
                }
                else
                {
                    Exceptions.WriteInfoLog("Before Calling CouponBL.ValidateCouponCode for Guest User in CalculateCoupon()");
                    UserBE objUserBE = new UserBE();
                    objUserBE.UserId = 0;
                    objUserBE.EmailId = Convert.ToString(HttpContext.Current.Session["GuestUser"]);
                    lstCouponBE = CouponBL.ValidateCouponCode(objCouponBE, objUserBE, GlobalFunctions.GetCurrencyId());
                }
                if (lstCouponBE != null && lstCouponBE.Count > 0)
                {
                    #region validate coupon
                    if (lstCouponBE[0].IsActive == false)
                    {
                        strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                    }
                    else if (lstCouponBE[0].IsExpired == true && string.IsNullOrEmpty(strMessage))
                    {
                        strMessage = strSC_Invalid_Coupon_Code;// "Coupon code is not valid.";
                    }
                    else if (lstCouponBE[0].IsValidOrderValue == false && string.IsNullOrEmpty(strMessage))
                    {
                        if (lstCouponBE[0].MaximumOrderValue == 0)
                        {
                            strSC_Coupon_Code_Maximum_Order_Value = strSC_Coupon_Code_Maximum_Order_Value.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                            strMessage = strSC_Coupon_Code_Maximum_Order_Value;
                        }
                        else if (lstCouponBE[0].MinimumOrderValue == 0)
                        {
                            strSC_Coupon_Code_Minimum_Order_Value = strSC_Coupon_Code_Minimum_Order_Value.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                            strMessage = strSC_Coupon_Code_Minimum_Order_Value;
                        }
                        else
                        {
                            strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@min", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue);
                            strSC_Coupon_Code_Minimum_Maximum_Range = strSC_Coupon_Code_Minimum_Maximum_Range.Replace("@max", GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue);
                            strMessage = strSC_Coupon_Code_Minimum_Maximum_Range;// "Coupon code is valid only for order value between " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MinimumOrderValue + " and " + GlobalFunctions.GetCurrencySymbol() + lstCouponBE[0].MaximumOrderValue;
                        }
                    }
                    else if (lstCouponBE[0].IsValidRegion == false && string.IsNullOrEmpty(strMessage))
                    {
                        strMessage = strSC_Invalid_Coupon_Delivery_Country;// "Coupon code is not valid for the selected delivery country.";
                    }
                    else if (lstCouponBE[0].IsValidShipmentType == false && string.IsNullOrEmpty(strMessage))
                    {
                        strMessage = strSC_Invalid_Coupon_Shipping_Method;// "Coupon code is not valid for the selected shipping method.";
                    }
                    #endregion

                    if (string.IsNullOrEmpty(strMessage))
                    {
                        strBehaviourType = lstCouponBE[0].BehaviourType;
                        strShipmentType = lstCouponBE[0].ShipmentType;

                        double discountPercentage = Convert.ToDouble(lstCouponBE[0].DiscountPercentage);
                        bool isvalid = false;
                        #region      //Apply to goods only, excluding Freight Value
                        if (lstCouponBE[0].BehaviourType.ToLower() == "product")
                        {
                            Exceptions.WriteInfoLog("Inside lstCouponBE[0].BehaviourType.ToLower() == product in CalculateCoupon()");
                            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                            lstFreightBE = CalculateFreight(CountryId);
                            Exceptions.WriteInfoLog("After calling CalculateFreight() in CalculateCoupon()");
                            isvalid = true;
                            if (lstFreightBE != null && lstFreightBE.Count > 0)
                            {
                                Exceptions.WriteInfoLog("Inside lstFreightBE != null && lstFreightBE.Count > 0 in CalculateCoupon()");
                                string scSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                string scEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                                string scPSPrice = !string.IsNullOrEmpty(lstFreightBE[0].PSPrice) ? lstFreightBE[0].PSPrice : "0"; // SHRIGANESH 11 Nov 2016
                                StandardFreightValueAfterDiscount = Convert.ToString(scSZPrice); //Total tax for standard zone
                                ExpressFreightValueAfterDiscount = Convert.ToString(scEZPrice); //Total tax for express zone
                                PalletShipmentFreightValueAfterDiscount = Convert.ToString(scPSPrice); // Total tax for Pallet zone SHRIGANESH 11 Nov 2016
                            }
                            OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                            strMessage = "Success";
                        }
                        #endregion
                        #region   "Apply to goods only, excluding Freight Value"
                        else if (lstCouponBE[0].BehaviourType.ToLower() == "basket")
                        {
                            Exceptions.WriteInfoLog("Inside lstCouponBE[0].BehaviourType.ToLower() == basket in CalculateCoupon()");
                            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                            lstFreightBE = CalculateFreight(CountryId);
                            isvalid = true;
                            if (lstFreightBE != null && lstFreightBE.Count > 0)
                            {
                                Exceptions.WriteInfoLog("Inside lstFreightBE != null && lstFreightBE.Count > 0 in CalculateCoupon()");
                                string sdSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                string sdEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                                string sdPSPrice = !string.IsNullOrEmpty(lstFreightBE[0].PSPrice) ? lstFreightBE[0].PSPrice : "0"; // SHRIGANESH 11 Nov 2016

                                if (strShipmentType.Equals("1"))
                                {
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 1
                                }
                                if (strShipmentType.Equals("2"))
                                {
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 2
                                }
                                if (strShipmentType.Equals("3"))
                                {
                                    PalletShipmentFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdPSPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 3
                                }
                                if (strShipmentType.Equals("0"))
                                {
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 1
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 2
                                    PalletShipmentFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sdPSPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 3
                                }
                            }
                            OrderValueAfterDiscount = GlobalFunctions.DiscountedAmount(OrderItemTotal, Convert.ToDouble(lstCouponBE[0].DiscountPercentage));
                            strMessage = "Success";
                        }
                        #endregion
                        #region //Free shipping excluding Freight Value
                        else if (lstCouponBE[0].BehaviourType.ToLower() == "freeshipping")
                        {
                            Exceptions.WriteInfoLog("Inside lstCouponBE[0].BehaviourType.ToLower() == freeshipping in CalculateCoupon()");
                            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                            lstFreightBE = CalculateFreight(CountryId);
                            Exceptions.WriteInfoLog("After calling CalculateFreight() in CalculateCoupon()");
                            isvalid = true;
                            string seSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                            string seEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                            string sePSPrice = !string.IsNullOrEmpty(lstFreightBE[0].PSPrice) ? lstFreightBE[0].PSPrice : "0";

                            if (strShipmentType.Equals("1"))
                            {
                                //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 1
                                StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                            }
                            if (strShipmentType.Equals("2"))
                            {
                                //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 2
                                ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                            }
                            if (strShipmentType.Equals("3"))
                            {
                                PalletShipmentFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 3
                            }
                            if (strShipmentType.Equals("0"))
                            {
                                StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 1
                                ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(seEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 2
                                PalletShipmentFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sePSPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 3
                            }
                            OrderValueAfterDiscount = OrderItemTotal;
                            discountPercentage = 0.0;
                            strMessage = "Success";
                        }
                        #endregion
                        #region  //Discounted Shipping
                        else if (lstCouponBE[0].BehaviourType.ToLower() == "shipping")
                        {
                            Exceptions.WriteInfoLog("Inside lstCouponBE[0].BehaviourType.ToLower() == shipping in CalculateCoupon()");
                            isvalid = true;
                            List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                            lstFreightBE = CalculateFreight(CountryId);
                            Exceptions.WriteInfoLog("After calling CalculateFreight() in CalculateCoupon()");
                            if (lstFreightBE != null && lstFreightBE.Count > 0)
                            {
                                string sfSZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                                string sfEZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                                string sfPSPRice = !string.IsNullOrEmpty(lstFreightBE[0].PSPrice) ? lstFreightBE[0].PSPrice : "0";

                                if (strShipmentType.Equals("1"))
                                {
                                    //StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].SZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for standard zone
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 1
                                }
                                if (strShipmentType.Equals("2"))
                                {
                                    //ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstFreightBE[0].EZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for express zone
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 2
                                }
                                if (strShipmentType.Equals("3"))
                                {
                                    PalletShipmentFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfPSPRice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 3
                                }
                                if (strShipmentType.Equals("0"))
                                {
                                    StandardFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfSZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 1
                                    ExpressFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfEZPrice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 2
                                    PalletShipmentFreightValueAfterDiscount = Convert.ToString(GlobalFunctions.DiscountedAmount(Convert.ToDouble(sfPSPRice), Convert.ToDouble(lstCouponBE[0].DiscountPercentage))); //Total tax for Service 3
                                }
                                discountPercentage = 0.0;

                            }
                            OrderValueAfterDiscount = OrderItemTotal;
                            strMessage = strSuccess;// "Success";
                        }
                        #endregion
                        #region Tax Calculation after discount
                        if (isvalid)
                        {
                            Exceptions.WriteInfoLog("Inside region Tax Calculation after discount in CalculateCoupon()");
                            string strTotalTax = CalculateTax(CountryId, StandardFreightValueAfterDiscount, ExpressFreightValueAfterDiscount, PalletShipmentFreightValueAfterDiscount, discountPercentage);
                            Exceptions.WriteInfoLog("After calling CalculateTax() in CalculateCoupon()");
                            HttpContext.Current.Session["discountPercentage"] = discountPercentage;
                            if (!string.IsNullOrEmpty(strTotalTax))
                            {
                                string[] strTax = strTotalTax.Split('|');
                                if (strTax.Length > 0)
                                {
                                    StandardTaxValueAfterDiscount = strTax[0]; //Total tax for Service 1
                                    ExpressTaxValueAfterDiscount = strTax[1]; //Total tax for Service 2
                                    PalletShipentTaxValueAfterDiscount = strTax[2]; // Total Tax for Service 3
                                }
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    strMessage = strSC_Invalid_Coupon_Code;// "Invalid coupon code.";
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }

            return "[{\"Message\":\"" + strMessage
                + "\",\"OrderValueAfterDiscount\":\"" + OrderValueAfterDiscount
                + "\",\"StandardFreightValueAfterDiscount\":\"" + StandardFreightValueAfterDiscount
                + "\",\"ExpressFreightValueAfterDiscount\":\"" + ExpressFreightValueAfterDiscount
                + "\",\"PalletShipmentFreightValueAfterDiscount\":\"" + PalletShipmentFreightValueAfterDiscount
                + "\",\"StandardTaxValueAfterDiscount\":\"" + StandardTaxValueAfterDiscount
                + "\",\"ExpressTaxValueAfterDiscount\":\"" + ExpressTaxValueAfterDiscount
                + "\",\"PalletShipentTaxValueAfterDiscount\":\"" + PalletShipentTaxValueAfterDiscount
                + "\",\"BehaviourType\":\"" + strBehaviourType.ToLower()
                + "\",\"ShipmentType\":\"" + strShipmentType.ToLower() + "\"}]";

        }
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 11-09-15
        /// Scope   : changeaddress webmethod of the ShoppingCart_Payment page
        /// </summary>
        /// <param name="DeliveryAddressId"></param>                
        /// <returns>Delivery Address</returns>
        [System.Web.Services.WebMethod]
        public static string changeaddress(Int32 DeliveryAddressId)
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
            {
                DeliveryAddress();
            }

            string jsondata = string.Empty;
            try
            {
                Exceptions.WriteInfoLog("Inside changeaddress()");
                UserBE lstUser = new UserBE();
                List<UserBE.UserDeliveryAddressBE> lstNew = new List<UserBE.UserDeliveryAddressBE>();
                List<UserBE.UserDeliveryAddressBE> lstUpdatedNewAdd = new List<UserBE.UserDeliveryAddressBE>();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                if (lstUser != null && lstUser.UserDeliveryAddress.Count > 0)
                {
                    lstNew = lstUser.UserDeliveryAddress.FindAll(x => x.DeliveryAddressId == DeliveryAddressId);
                    lstNew = lstNew.GroupBy(x => x.DeliveryAddressId).Select(
                                     g => new UserBE.UserDeliveryAddressBE
                                     {
                                         //Key = g.Key,
                                         AddressTitle = HttpUtility.HtmlDecode(g.First().AddressTitle),
                                         PreDefinedColumn1 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn1),
                                         PreDefinedColumn2 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn2),
                                         PreDefinedColumn3 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn3),
                                         PreDefinedColumn4 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn4),
                                         PreDefinedColumn5 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn5),
                                         PreDefinedColumn6 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn6),
                                         PreDefinedColumn7 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn7),
                                         PreDefinedColumn8 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn8),
                                         PreDefinedColumn9 = HttpUtility.HtmlDecode(g.First().PreDefinedColumn9),

                                     }).ToList();
                    if (lstNew.Count > 0)
                    {
                        Exceptions.WriteInfoLog("Before passing JSON in IF from changeaddress()");
                        System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        jsondata = jSearializer.Serialize(lstNew);
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Before passing JSON in ELSE from changeaddress()");
                        List<UserBE.UserDeliveryAddressBE> lstBlank = new List<UserBE.UserDeliveryAddressBE>();
                        System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        jsondata = jSearializer.Serialize(lstBlank);
                    }
                }
                Exceptions.WriteInfoLog("Exiting changeaddress()");
                return jsondata;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
        }


        public static void DeliveryAddress()
        {

        }
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 24-09-15
        /// Scope   : CalculateFreightCharges webmethod of the ShoppingCart_Payment page
        /// </summary>
        /// <param name="intCountryId"></param>                
        /// <returns>Freight charges</returns>
        [System.Web.Services.WebMethod]
        public static string CalculateFreightCharges(Int16 intCountryId)
        {
            string jsondata = string.Empty;
            try
            {
                
                List<ShoppingCartAdditionalBE> lstShoppingBE = new List<ShoppingCartAdditionalBE>();
                UserBE lstUser = new UserBE();
                decimal additional = 0;
                lstUser = HttpContext.Current.Session["User"] as UserBE;

                if (lstUser != null && lstUser.UserId != 0)
                {
                    lstShoppingBE = ShoppingCartBL.GetAdditionalData(GlobalFunctions.GetCurrencyId(), lstUser.UserId, "");

                }
                else
                {

                    lstShoppingBE = ShoppingCartBL.GetAdditionalData(GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID);

                }

                additional = Convert.ToDecimal(lstShoppingBE[0].Additionalcharges);
                string standard = "";
                string express = "";
                string pallet = "";

                Exceptions.WriteInfoLog("Inside CalculateFreightCharges()");
                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                Exceptions.WriteInfoLog("Before calling CalculateFreight()");
                lstFreightBE = CalculateFreight(intCountryId);
                if (lstFreightBE != null && lstFreightBE.Count > 0)
                {
                    Exceptions.WriteInfoLog("Inside lstFreightBE != null && lstFreightBE.Count > 0 in CalculateFreightCharges()");
                    lstFreightBE[0].SZPrice = !string.IsNullOrEmpty(lstFreightBE[0].SZPrice) ? lstFreightBE[0].SZPrice : "0";
                    lstFreightBE[0].EZPrice = !string.IsNullOrEmpty(lstFreightBE[0].EZPrice) ? lstFreightBE[0].EZPrice : "0";
                    lstFreightBE[0].PSPrice = !string.IsNullOrEmpty(lstFreightBE[0].PSPrice) ? lstFreightBE[0].PSPrice : "0";
                    if (additional != 0)
                    {
                        standard = Convert.ToString(Convert.ToDecimal(lstFreightBE[0].SZPrice) + additional);
                        express = Convert.ToString(Convert.ToDecimal(lstFreightBE[0].EZPrice) + additional);
                        pallet = Convert.ToString(Convert.ToDecimal(lstFreightBE[0].PSPrice) + additional);
                    }


                    Exceptions.WriteInfoLog("Before calling CalculateTax() in CalculateFreight()");
                    string strTotalTax = "";
                    if (additional != 0)
                    {
                        strTotalTax = CalculateTax(intCountryId, standard, express, pallet, 0);
                    }
                    else
                    {
                        strTotalTax = CalculateTax(intCountryId, lstFreightBE[0].SZPrice, lstFreightBE[0].EZPrice, lstFreightBE[0].PSPrice, 0);
                    }
                    Exceptions.WriteInfoLog("After calling CalculateTax() in CalculateFreight()");
                    if (!string.IsNullOrEmpty(strTotalTax))
                    {
                        string[] strTax = strTotalTax.Split('|');
                        if (strTax.Length > 0)
                        {
                            lstFreightBE[0].StandardZone = strTax[0]; //Total tax for standard zone
                            lstFreightBE[0].ExpressZone = strTax[1];//Total tax for express zone
                            lstFreightBE[0].PalletShipmentZone = strTax[2];//TEMP logic SHRIGANESH 11 Nov 2016
                        }
                    }
                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();
                    if (lstCountry != null && lstCountry.Count > 0)
                    {
                        if (lstCountry.FirstOrDefault(x => x.CountryId == intCountryId).DisplayDutyMessage)
                            lstFreightBE[0].DisplayDutyMessage = true;
                    }
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsondata = jSearializer.Serialize(lstFreightBE);
                }
                Exceptions.WriteInfoLog("Exiting CalculateFreightCharges()");
                return jsondata;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
        }

        //Added by Sripal For SSO the data enter in the BSYS table
        private void InsertBASYSId(Int16 CurrencyIdx, string strEmailId, Int16 iUserID)
        {
            try
            {
                #region vikram for apostrophy
                // in database we will store encoded emailid but for display and xml it will be decoded
                strEmailId = GlobalFunctions.ReplaceEmailIdspecialCharactersbyDecoding(strEmailId);
                #endregion
                Exceptions.WriteInfoLog("Inside InsertBASYSId() in Payment.aspx.cs");
                StoreBE objStore = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails()");
                int intCustomerID_OASIS;
                UserBE objUser = new UserBE();

                #region "Assign the data to object from the inputs"
                //objUser.EmailId = Sanitizer.GetSafeHtmlFragment(strEmailId.Trim());
                objUser.EmailId = HttpUtility.HtmlDecode(strEmailId.Trim());// vikram for apostrophy
                objUser.UserId = iUserID;
                List<string> objLs = SplitName(txtContact_Name.Value);
                if (objLs.Count == 2)
                {
                    objUser.FirstName = objLs[0];
                    objUser.LastName = objLs[1];
                }

                objUser.PredefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                objUser.PredefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                objUser.PredefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                objUser.PredefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                objUser.PredefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                objUser.PredefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                objUser.PredefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                objUser.PredefinedColumn8 = ddlCountry.SelectedValue;
                objUser.CountryName = ddlCountry.SelectedItem.Text;
                objUser.PredefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                objUser.IPAddress = GlobalFunctions.GetIpAddress();
                #endregion
                objUser.DefaultCustId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId; //1281850; // objStore.StoreCurrencies[i].DefaultCompanyId;
                objUser.SourceCodeId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).SourceCodeId; //5281; //objStore.StoreCurrencies[i].SourceCodeId;
                Exceptions.WriteInfoLog("Before calling RegisterCustomer_OASIS.BuildCustContact_OASIS()");
                intCustomerID_OASIS = RegisterCustomer_OASIS.BuildCustContact_OASIS(objUser);
                if (intCustomerID_OASIS > 0)
                {
                    Exceptions.WriteInfoLog("Before calling UserBL.ExecuteBASYSDetails()");
                    UserBL.ExecuteBASYSDetails(Constants.USP_InsertUserBASYSDetails, true, objUser.UserId, CurrencyIdx, intCustomerID_OASIS);
                    Exceptions.WriteInfoLog("After calling UserBL.ExecuteBASYSDetails()");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        private int UpdateRegistration()
        {
            int i = 0;
            try
            {
                Exceptions.WriteInfoLog("Inside UpdateRegistration() In Payment Page");
                UserBE objRegBE = new UserBE();
                string SaltPass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["FirstName"]) + " " + Convert.ToString(Session["LastName"])), "SHA512", null);
                objRegBE.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["UserName"]));
                objRegBE.Password = SaltPass;
                objRegBE.FirstName = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["FirstName"]));
                objRegBE.LastName = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["LastName"]));

                objRegBE.PredefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtContact_Name.Value.Trim());
                objRegBE.PredefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtCompany.Value.Trim());
                objRegBE.PredefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtAddress1.Value.Trim());
                objRegBE.PredefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtAddress2.Value.Trim());
                objRegBE.PredefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtTown.Value.Trim());
                objRegBE.PredefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtState__County.Value.Trim());
                objRegBE.PredefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtPostal_Code.Value.Trim());
                objRegBE.PredefinedColumn8 = ddlCountry.SelectedValue;
                objRegBE.PredefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtPhone.Value.Trim());
                UserBE.UserDeliveryAddressBE objReg = new UserBE.UserDeliveryAddressBE();
                objReg.PreDefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtDContact_Name.Value.Trim());
                objReg.PreDefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtDCompany_Name.Value.Trim()); // Added by SHRIGANESH 27 July 2016 for Delivery Company Name
                objReg.PreDefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtDAddress1.Value.Trim());
                objReg.PreDefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtDAddress2.Value.Trim());
                objReg.PreDefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtDTown.Value.Trim());
                objReg.PreDefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtDState__County.Value.Trim());
                objReg.PreDefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtDPostal_Code.Value.Trim());
                objRegBE.IsGuestUser = false;
                objReg.PreDefinedColumn8 = ddlDCountry.SelectedValue;
                objReg.PreDefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtDPhone.Value.Trim());
                objRegBE.UserDeliveryAddress.Add(objReg);
                objRegBE.IPAddress = GlobalFunctions.GetIpAddress();
                objRegBE.CurrencyId = Convert.ToInt16(intCurrencyId);
                objRegBE.Points = -1; // Added by ShriGanesh 26 April 2017
                Exceptions.WriteInfoLog("Before calling UserBL.ExecuteRegisterDetailsSSO()");
                i = UserBL.ExecuteRegisterDetailsSSO(Constants.USP_UpdateTbl_UsersSSO, true, objRegBE, "StoreCustomer");
                Exceptions.WriteInfoLog("After calling UserBL.ExecuteRegisterDetailsSSO()");
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return i;
        }

        protected void btnApplyGiftCertificate_Click(object sender, EventArgs e)
        {
            #region vikram for legal entity 03032017
            ApplyGiftCertificate();
            BindUserAddressafterGC();
            #endregion
        }
        protected void btnCancelGiftCertificate_OnClick(object sender, EventArgs e)
        {
            try
            {
                Exceptions.WriteInfoLog("Inside btnCancelGiftCertificate_OnClick()");

                Session["Amount"] = null;
                Control FooterTemplate = rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0];
                HtmlGenericControl divGiftCertificate = FooterTemplate.FindControl("divGiftCertificate") as HtmlGenericControl;
                divGiftCertificate.Visible = false;

                txtGiftCertificateCode.Text = "";
                Exceptions.WriteInfoLog("Before calling BindPaymentTypes() in btnCancelGiftCertificate_OnClick()");
                BindPaymentTypes();
                Exceptions.WriteInfoLog("Before calling BindPriceDetails()");
                BindPriceDetails();
                Exceptions.WriteInfoLog("After calling BindPriceDetails()");

                ListItem lItem;
                if (ddlPaymentTypes.Items.Count > 1)
                {
                    lItem = ddlPaymentTypes.Items.FindByValue("0");
                    ddlPaymentTypes.SelectedValue = "0";
                }
                else
                {
                    lItem = ddlPaymentTypes.Items.FindByValue("1|I");
                    ddlPaymentTypes.SelectedValue = "1|I";
                }
                #region vikram for legal entity
                Session["IsTotalvalueZeroafterGC"] = false;
                BindUserAddressafterGC();
                #endregion

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private int GenerateGiftCertificateXML()
        {
            int intOrderID = 0;
            try
            {
                Exceptions.WriteInfoLog("Inside GenerateGiftCertificateXML()");
                StoreBE lstStoreDetail = new StoreBE();
                lstStoreDetail = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails()");
                //txtGiftCertificateCode.Text = "0324-D49C-2615-88A2";
                #region CheckBalance
                XElement xCheckBalanceElement;

             
              //  if (Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode).ToLower() != "gbp")
              //  {
                    Exceptions.WriteInfoLog("Before assigning xCheckBalanceElement in IF");
                    xCheckBalanceElement = new XElement("CheckBalance", new XElement("CertificateReference", txtGiftCertificateCode.Text.Trim())
                                                              , new XElement("Currency", Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode))
                                                              , new XElement("DivisionID", Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId)));
                //}
                //else
                //{
                //    Exceptions.WriteInfoLog("Before assigning xCheckBalanceElement in ELSE");
                //    xCheckBalanceElement = new XElement("CheckBalance", new XElement("CertificateReference", txtGiftCertificateCode.Text.Trim())
                //                                             , new XElement("Currency")
                //                                             , new XElement("DivisionID", Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId)));
                //}

                #endregion
                XDocument xOrderXML = new XDocument(xCheckBalanceElement);
                XmlNode OrderXMLNode = new XmlDocument().ReadNode(xOrderXML.Root.CreateReader());

                string str = "";
                if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
                {
                    Exceptions.WriteInfoLog("Before Calling giftcertificateservice.CheckBalance for LIVE");
                    PWGlobalEcomm.GiftCertificateServiceLive.GiftCertificateService giftcertificateservice = new PWGlobalEcomm.GiftCertificateServiceLive.GiftCertificateService();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                       ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    giftcertificateservice.Credentials = objNetworkCredentials;
                    GlobalFunctions.WriteGiftLog(Convert.ToString("GC REQUEST from payment page: " + xCheckBalanceElement));
                    str = giftcertificateservice.CheckBalance(OrderXMLNode);
                    GlobalFunctions.WriteGiftLog(Convert.ToString("GC Response from payment page: " + str));
                }
                else
                {
                    Exceptions.WriteInfoLog("Before Calling giftcertificateservice.CheckBalance for BETA");
                    PWGlobalEcomm.GiftCertificateService.GiftCertificateService giftcertificateservice = new PWGlobalEcomm.GiftCertificateService.GiftCertificateService();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                        ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    giftcertificateservice.Credentials = objNetworkCredentials;
                    GlobalFunctions.WriteGiftLog(Convert.ToString("GC REQUEST from payment page: " + xCheckBalanceElement));
                    str = giftcertificateservice.CheckBalance(OrderXMLNode);
                    GlobalFunctions.WriteGiftLog(Convert.ToString("GC Response from payment page: " + str));
                }

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(str);

                XmlNodeList elemList = xd.GetElementsByTagName("GiftCertificateCheckResult");
                double dRGCAmount = 0;
                for (int i = 0; i < elemList.Count; i++)
                {
                    string attrVal = elemList[i].Attributes["Valid"].Value;
                    if (attrVal.ToLower() == "true")
                    {
                        XmlNodeList elemList1 = xd.GetElementsByTagName("Amount");
                        if (elemList1.Count > 0)
                        {
                            string strAmount = elemList1[0].InnerText;
                            //Session["Amount"] = strAmount;
                            dRGCAmount += Convert.ToDouble(strAmount, CultureInfo.InvariantCulture);
                            StringBuilder sbGift = new StringBuilder();
                            sbGift.Append(txtGiftCertificateCode.Text.Trim() + "|*|" + strAmount);
                            Session["sbGift"] = sbGift;
                        }
                    }
                }
                if (dRGCAmount == 0) { Session["Amount"] = null; }
                else { Session["Amount"] = dRGCAmount; }
                //Session["Amount"] = 600;
                Exceptions.WriteInfoLog("Exiting GenerateGiftCertificateXML()");
            }
            catch (Exception ex)
            {
                UserBE lstUserBE;
                lstUserBE = HttpContext.Current.Session["User"] as UserBE;
                GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In GenerateGiftCertificateXML", ex + "USER ID- " + lstUserBE.UserId + "USER EMAIL- " + lstUserBE.EmailId, "", "", false);
                Exceptions.WriteExceptionLog(ex);
            }
            return intOrderID;
        }

        protected void BindResourceData()
        {
            try
            {
                Exceptions.WriteInfoLog("Inside BindResourceData()");
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        Exceptions.WriteInfoLog("Inside lstStaticPageBE != null && lstStaticPageBE.Count > 0 in BindResourceData()");
                        strPayment_UDF_InvalidRegNum = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "payment_udf_invalidregnum").ResourceValue;
                        strFreightMOVMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_freight_mov_message").ResourceValue;
                        strDutyMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_duty_message").ResourceValue;
                        strSC_InSufficient_Point = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_insufficient_point").ResourceValue;
                        strSC_Payment_Types = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_payment_types").ResourceValue);
                        strSC_UDF_Minimum_Maximum_Range = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_udf_minimum_maximum_range").ResourceValue;
                        strSC_UDF_Range2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_udf_range2").ResourceValue;
                        strSC_UDF_Range3 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_udf_range3").ResourceValue;
                        strSC_Numeric = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_numeric").ResourceValue;
                        strSC_NumberMinDecimal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_numbermindecimal").ResourceValue;
                        strNew_Delivery_Address = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "new_delivery_address").ResourceValue;
                        strSC_Error_Processing_Payment = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_error_processing_payment").ResourceValue;
                        strSC_Updating_Order_Details = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_updating_order_details").ResourceValue;
                        strSC_Error_Update_Delivery_Address = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_error_update_delivery_address").ResourceValue;
                        strSC_Invalid_Coupon_Code = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_invalid_coupon_code").ResourceValue);
                        strSC_Coupon_Code_Maximum_Order_Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_coupon_code_maximum_order_value").ResourceValue;
                        strSC_Coupon_Code_Minimum_Order_Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_coupon_code_minimum_order_value").ResourceValue;
                        strSC_Coupon_Code_Minimum_Maximum_Range = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_coupon_code_minimum_maximum_range").ResourceValue;
                        strSC_Invalid_Coupon_Delivery_Country = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_invalid_coupon_delivery_country").ResourceValue;
                        strSC_Invalid_Coupon_Shipping_Method = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_invalid_coupon_shipping_method").ResourceValue;
                        strSuccess = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Success").ResourceValue;
                        strSC_UDF_Max_Character = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_udf_max_character").ResourceValue;
                        strUppercase = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "uppercase").ResourceValue;
                        strRequired = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "required").ResourceValue);
                        strFile_Extension = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "file_extension").ResourceValue;
                        strRegister_RegisteredAddress_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_registeredaddress_text").ResourceValue;
                        strRegister_DeliveryAddress_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_deliveryaddress_text").ResourceValue;
                        strValid = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "valid").ResourceValue;
                        stValid = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "valid").ResourceValue);
                        strBasket_Title_Text = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "basket_title_text").ResourceValue);
                        strSC_Special_Instruction = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_special_instruction").ResourceValue);
                        strBasket_Quantity_Not_More_Than_Stock_Message = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "basket_quantity_not_more_than_stock_message").ResourceValue);
                        strSC_Reapply_Coupon = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_reapply_coupon").ResourceValue);

                        strEmailMeaCopyOfMyBasket = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "emailthisproduct_emailalert").ResourceValue);

                        strSC_Require_Coupon = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_require_coupon").ResourceValue);
                        strSC_Place_Order = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_place_order").ResourceValue);
                        strSC_Email_Me_Basket = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_email_me_basket").ResourceValue);
                        //Button aProceed = (Button)rptBasketListing.FindControl("aProceed");
                        //aProceed.Text = strSC_Place_Order;

                        foreach (RepeaterItem item in rptBasketListing.Items)
                        {
                            if (item.ItemType == ListItemType.Footer)
                            {
                                Button aProceed = (Button)item.FindControl("aProceed");
                                aProceed.Text = strSC_Place_Order;

                                //Added By Hardik "04/Oct/2016"
                                Button aEmail = (Button)item.FindControl("aEmail");
                                aEmail.Text = strSC_Email_Me_Basket;

                                HtmlGenericControl spnBsktSubTotal = (HtmlGenericControl)item.FindControl("spnBsktSubTotal");
                                spnBsktSubTotal.InnerText = strsc_basket_subtotal;
                            }
                        }

                        strSC_Enter_Card_Details = GlobalFunctions.ReplaceSingleQuote(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_enter_card_details").ResourceValue);
                        spnRegisterAddress.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_section_2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnDelivery_Address.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_deliveryaddress_text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnDelivery_AddressTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_delivery_address_title" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        //New resource for Indeed Beta for undefined//
                        spanBillingTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Billing_Address" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnPageTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_payment_page_title" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnShipping_Method.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_shipping_method" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblSetDefault.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_set_as_default" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnPaymentMethod.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_payment_method" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblGC.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_code" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblChkAddress.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_same_as_delivery_address" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblInstruction.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_instructions" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblUploadFile.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_upload_file" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblCouponCode.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_couponcode" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblSucCoupon.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_applied_coupon_success" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnApplyCoupon.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "apply" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnRemoveCoupon.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "cancel" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnReviewNConfirm.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_review_and_confirm" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        strsc_basket_subtotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_basket_subtotal" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strTotal_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_total_text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strShipping = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "shipping" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGiftCertificateTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sitelinks_giftcertificatetitle" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strTax = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "tax" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strsc_insufficient_couponcode_balance = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_insufficient_couponcode_balance" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        ltrInvoiceAddress.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Invoice_Address_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        ltrInvoiceCountry.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Invoice_Country" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        ltrLegalEntity.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Legal_Entity" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        ltrTaxNo.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Tax_Number" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Generic_Select_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Select_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Invoice_others = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Invoice_others" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_txtSplChar_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_txtSplChar_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_txtSplChar_Message = GlobalFunctions.ReplaceSingleQuote(strRegister_txtSplChar_Message);

                        /* Added By Hardik "04/Oct/2016" */
                        str_SC_Email_Me_Copy = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "sc_email_me_basket" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Status_Title = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Status_Title").ResourceValue;
                        strUnitCost = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Unit_Cost_Text").ResourceValue;
                        strProductTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Title").ResourceValue;
                        strStockTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Stock_Title_Text").ResourceValue;

                        strAddtionalcharges = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Handlingfee" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strtotalnet = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_totalnet" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGenericPointsText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Points_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        // strGenericPointsText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Points_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strAvailableBudget = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "ProductPaymentAvailable_Budget" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        strBudgetErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Budget_ErrorMessage" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    }
                }
                Exceptions.WriteInfoLog("Exiting BindResourceData()");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private List<string> SplitName(string strName)
        {
            Exceptions.WriteInfoLog("Inside SplitName()");
            List<string> objLstStr = new List<string>();
            try
            {
                if (strName.Trim().IndexOf(' ') > 0)
                {
                    Exceptions.WriteInfoLog("Inside IF of SplitName()");
                    objLstStr.Add(strName.Trim().Substring(0, txtContact_Name.Value.Trim().IndexOf(' ')));
                    objLstStr.Add(strName.Trim().Substring(strName.Trim().IndexOf(' ') + 1, (strName.Trim().Length - (strName.Trim().IndexOf(' ') + 1))));
                }
                else
                {
                    Exceptions.WriteInfoLog("Inside ELSE of SplitName()");
                    objLstStr.Add(strName.Trim());
                    objLstStr.Add(".");
                }
            }
            catch (Exception)
            {
                objLstStr.Add(strName.Trim());
                objLstStr.Add(".");
            }
            return objLstStr;
        }

        #region Added By Hardik on "02/MARCH/2017"
        protected void BindlstRegData_Danskebank()
        {
            Exceptions.WriteInfoLog("Inside BindResourceData()");
            lstRegData_Danskebank = StaticPageManagementBL.GetAllCustomUDFRegDataDanske();
        }
        protected void ddlPaymentTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region vikram for legal entity
            List<object> lstPaymentTypes = new List<object>();
            List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();

            string strPaymentMethod = Convert.ToString(ddlPaymentTypes.SelectedValue);
            string[] items = strPaymentMethod.Split('|');
            Int16 iPaymentTypeId = Convert.ToInt16(items[0]);
            if (iPaymentTypeId > 0)
            {
                lstPaymentTypes = ShoppingCartBL.GetPaymentdetailsById(iPaymentTypeId, GlobalFunctions.GetLanguageId());
                lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                if (!lstPaymentTypesBE[0].InvoiceAccountId)
                {
                    divCountry.Visible = true;
                    hdisLegalEntity.Value = "false";
                    divHeader_Invoice.Visible = false;
                    IsInvoiceAccountEnabled = false;
                   // BindUserAddress();

                }
                else
                {
                    divCountry.Visible = false;
                    divHeader_Invoice.Visible = true;
                    IsInvoiceAccountEnabled = true;
                    hdisLegalEntity.Value = "true";
                    ddlInvoiceCountry.SelectedValue = "0";
                    ddlLegalEntity.SelectedValue = "0";
                    BindInvoiceUserAddress(null);
                }
            }
            Session["SelectedEntityDetails"] = null;
            #endregion
            string str = Convert.ToString(ddlPaymentTypes.SelectedValue);
            string[] strArr = str.Split('|');
            if (strArr[0] != "58")
            {
                if (!Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                {
                    BindCustomerRefUDFFields();
                }
            }
            if (lstPaymentTypesBE[0].IsBudget)
            {
                if (Session["Budget"] != null)
                {
                    HtmlGenericControl spnbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnbudget");
                    HtmlGenericControl dvbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("dvbudget");

                    spnbudget.InnerText = strAvailableBudget;// need to add resource by tripti
                    dvbudget.InnerHtml =GlobalFunctions.GetCurrencySymbol() + Convert.ToString(Session["budget"]);
                }
            }
            else
            {
                HtmlGenericControl spnbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("spnbudget");
                HtmlGenericControl dvbudget = (HtmlGenericControl)rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0].FindControl("dvbudget");

                spnbudget.InnerText = "";// need to add resource by tripti
                dvbudget.InnerHtml = "";
                Session["Budget"] = null;
            }
        }
        #endregion

        protected void txtUDFS_TextChanged(object sender, EventArgs e)
        {
            TextBox txtUDFS = (TextBox)tblUDFS.FindControl("txt2");
            TextBox txtUDFS_EAN = (TextBox)tblUDFS.FindControl("txt4");
            if (txtUDFS.Text != "")
            {
                CustomUDFRegDataDanske RegData_DanskebankBE = lstRegData_Danskebank.FirstOrDefault(x => x.Reg_Number == txtUDFS.Text);
                if (RegData_DanskebankBE != null)
                {
                    if (RegData_DanskebankBE.Reg_Number == txtUDFS.Text)
                    {
                        Session["RegData_DanskebankBE"] = RegData_DanskebankBE;
                        txtUDFS_EAN.Enabled = false;
                        txtUDFS_EAN.Text = Convert.ToString(RegData_DanskebankBE.EAN_Number);
                        BindCustomerRefUDFFields();
                        //GlobalFunctions.ShowModalAlertMessages(this.Page, "valid", AlertType.Success);
                    }
                    else
                    {
                        BindCustomerRefUDFFields();
                        txtUDFS.Text = "";
                        txtUDFS_EAN.Text = "";
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPayment_UDF_InvalidRegNum, AlertType.Failure);
                        return;
                    }
                }
                else
                {
                    BindCustomerRefUDFFields();
                    txtUDFS.Text = "";
                    txtUDFS_EAN.Text = "";
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPayment_UDF_InvalidRegNum, AlertType.Failure);
                    return;
                }
            }
        }

        protected void ApplyGiftCertificate()
        {
            try
            {
                Exceptions.WriteInfoLog("Inside btnApplyGiftCertificate_Click()");
                BindPriceDetails();
                Exceptions.WriteInfoLog("After calling BindPriceDetails()");
                GenerateGiftCertificateXML();
                Exceptions.WriteInfoLog("After calling GenerateGiftCertificateXML()");
                Control FooterTemplate = rptBasketListing.Controls[rptBasketListing.Controls.Count - 1].Controls[0];
                HtmlGenericControl divGiftCertificate = FooterTemplate.FindControl("divGiftCertificate") as HtmlGenericControl;

                HtmlGenericControl spnGiftCertificateAmount = FooterTemplate.FindControl("spnGiftCertificateAmount") as HtmlGenericControl;
                HtmlGenericControl dvTotal = (HtmlGenericControl)FooterTemplate.FindControl("dvTotal");
                HtmlGenericControl spnShippingPrice = (HtmlGenericControl)FooterTemplate.FindControl("spnShippingPrice");
                HtmlGenericControl spnTaxPrice = (HtmlGenericControl)FooterTemplate.FindControl("spnTaxPrice");
                HtmlGenericControl spnTotalPrice = (HtmlGenericControl)FooterTemplate.FindControl("spnTotalPrice");
                string str;
                str = dvTotal.InnerText.Replace(strCurrencySymbol, "");
                str = str.Replace("<span>", "");
                str = str.Replace("</span>", "");

                divGiftCertificate.Visible = true;
                //Session["Amount"] = "10";
                if (Session["Amount"] != null)
                {
                    str_SessionGCAmount = Convert.ToString(Session["Amount"]);
                    double dDiff, dShpCartPrice = 0;
                    double dGCAmt = Convert.ToDouble(Session["Amount"]);
                    if (rbService1.Checked)
                    {
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(hidService1Tax.Value) + Convert.ToDouble(spnService1.InnerText);// Convert.ToInt64(spnTotalPrice.InnerText);
                    }
                    else if (rbService2.Checked)
                    {
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(hidService2Tax.Value) + Convert.ToDouble(spnService2.InnerText);
                    }
                    else if (rbService3.Checked)
                    {
                        dShpCartPrice = Convert.ToDouble(str) + Convert.ToDouble(hidService3Tax.Value) + Convert.ToDouble(spnService3.InnerText);
                    }

                    if (dGCAmt < dShpCartPrice)
                    {
                        dDiff = dShpCartPrice - dGCAmt;
                        spnTotalPrice.InnerText = Convert.ToString(dDiff.ToString("f"));
                        spnGiftCertificateAmount.InnerText = "-" + Convert.ToString(dGCAmt.ToString("f"));
                    }
                    else if (dGCAmt > dShpCartPrice)
                    {
                        dDiff = dGCAmt - dShpCartPrice;
                        //spnTotalPrice.InnerText = "0.00"; This is not place to update final total
                        //IsTotalvalueZeroafterGC = true;//vikram for legal entity on 17032017 not working
                        //Session["IsTotalvalueZeroafterGC"] = IsTotalvalueZeroafterGC;//vikram for legal entity on 17032017 not working
                        spnGiftCertificateAmount.InnerText = "-" + Convert.ToString(dShpCartPrice.ToString("f"));
                    }
                    else if (dGCAmt == dShpCartPrice)
                    {
                        dDiff = 0;
                        //spnTotalPrice.InnerText = "0.00"; This is not place to update final total
                        //IsTotalvalueZeroafterGC = true;//vikram for legal entity on 17032017 not working
                        //Session["IsTotalvalueZeroafterGC"] = IsTotalvalueZeroafterGC;//vikram for legal entity on 17032017 not working
                        spnGiftCertificateAmount.InnerText = "-" + Convert.ToString(dShpCartPrice.ToString("f"));
                    }
                    Exceptions.WriteInfoLog("Before calling BindPriceDetails()");
                    BindPriceDetails();
                    Exceptions.WriteInfoLog("After calling BindPriceDetails()");
                    //spnGiftCertificateAmount.InnerText = Convert.ToString(Session["Amount"]);


                }
                else
                {
                    spnGiftCertificateAmount.InnerText = "0.00"; ;
                }
                //BindPriceDetails();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void BindUserAddressafterGC()
        {
            #region vikram for legal entity

            //if (Session["Amount"] != null)
            //{
            List<object> lstPaymentTypes = new List<object>();
            List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();

            string strPaymentMethod = Convert.ToString(ddlPaymentTypes.SelectedValue);
            if (Session["Amount"] == null && Convert.ToInt16(ddlPaymentTypes.Items.Count) > 1)
            {
                strPaymentMethod = "0";
                ddlPaymentTypes.SelectedValue = "0";
            }
            if (strPaymentMethod.Contains('|'))
            {
                string[] items = strPaymentMethod.Split('|');
                Int16 iPaymentTypeId = Convert.ToInt16(items[0]);
                if (iPaymentTypeId > 0)
                {
                    lstPaymentTypes = ShoppingCartBL.GetPaymentdetailsById(iPaymentTypeId, GlobalFunctions.GetLanguageId());
                    lstPaymentTypesBE = lstPaymentTypes.Cast<PaymentTypesBE>().ToList();

                    if (Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                    {
                        divCountry.Visible = true;
                        divHeader_Invoice.Visible = false;
                        Session["SelectedEntityDetails"] = null;
                        BindUserAddress();
                    }
                    else
                    {
                        if (!lstPaymentTypesBE[0].InvoiceAccountId)
                        {
                            divCountry.Visible = true;
                            divHeader_Invoice.Visible = false;
                            Session["SelectedEntityDetails"] = null;
                            BindUserAddress();
                        }
                        else
                        {
                            divCountry.Visible = false;
                            divHeader_Invoice.Visible = true;
                            if (Session["SelectedEntityDetails"] != null)
                            {
                                List<InvoiceBE> lstInvoice = Session["SelectedEntityDetails"] as List<InvoiceBE>;
                                BindInvoiceUserAddress(lstInvoice);
                            }
                            else
                            {
                                BindInvoiceUserAddress(null);
                            }
                        }
                    }
                }
            }
            else
            {
                if (Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))
                {
                    divCountry.Visible = true;
                    divHeader_Invoice.Visible = false;
                    Session["SelectedEntityDetails"] = null;
                    BindUserAddress();
                }
            }

            #endregion
        }

    }
}