﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class ShoppingCart_OrderDetailsGiftCertificate : BasePage //System.Web.UI.Page
    {
        private string orderId, storeId;
        protected global::System.Web.UI.WebControls.Label lblAddress;
        protected global::System.Web.UI.WebControls.Literal ltrOrderNo, ltrPaymentMethod;
        protected global::System.Web.UI.WebControls.Repeater rptOrderDetails;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvSubTotal, dvShippingCharges, dvTaxes, dvTotal;
        protected global::System.Web.UI.WebControls.TextBox txtODPassword, txtODConfirmPassword;

        protected global::System.Web.UI.WebControls.RequiredFieldValidator reqtxtPassword, reqtxtConfirmPassword;
        protected global::System.Web.UI.WebControls.CompareValidator cmpValidatorPwd;
        protected global::System.Web.UI.WebControls.RegularExpressionValidator regexPwdLentxtPassword, regexAlphaNumtxtPassword, regexAlphaNumSymtxtPassword;
        protected global::System.Web.UI.WebControls.Label lblPasswordPolicyMessage;
        protected string strMinPassLenErrorMsg, strAlphaNumPassReqErrorMsg, strAlphaNumSymPassReqErrorMsg;

        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        public string OrderId
        {
            get
            {
                return orderId;
            }
            set
            {
                orderId = value;
            }
        }
        public string host = GlobalFunctions.GetVirtualPath();
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();

        double dAmount = 0;
        string minPasswordLength = "0";

        public string strConfirmation, strOrderPlaceNote, strOrderEmailConfirmationNote, strOrderNumberText, strAmountText, strQuantityText, strRecipientEmailText, strPersonalMessageText, strRegistrationSuccessful, strGeneric_RegistrationFailed_Message;
        public string Generic_Order_Confirmation, OrderDetails_CheckOut_Faster, Generic_View_Order_History, Generic_WriteReviews, Order_Details_Set_Password_Reset_Text, Register_Password_Text, Register_ConfirmPassword_Text, OrderDetails_SaveDetails_FutureUse_Note;
        public string strSubTotalText, strShippingText, strTaxText, strTotalText, strBillingAddressText, strPaymentMethodText, strPrintText,strOrderNumberGenerationErrorText,strCreditCardText;
        public Int16 intLanguageID = 0, intCurrencyID = 0;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divGuestCheckout;
        //Dictionary<string, string> dictGiftOrder = new Dictionary<string, string>();
        //List<Dictionary<string, string>> dictGiftOrderDetailsLst = new List<Dictionary<string, string>>();

        protected global::System.Web.UI.WebControls.Button btnCreateAccount;
        StoreBE lstStoreDetail = new StoreBE();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["User"] == null)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strOrderNumberGenerationErrorText, GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                    return;
                }
                UserBE lstUserBE = new UserBE();
                lstUserBE = Session["User"] as UserBE;
                if (lstUserBE.IsGuestUser == false)
                    divGuestCheckout.Visible = false;
                else
                    divGuestCheckout.Visible = true;
                BindResourceData();
                GetQueryString();

                //dictGiftOrder = (Dictionary<string, string>)Session["dictGiftOrder"];
                //dictGiftOrderDetailsLst = (List<Dictionary<string, string>>)Session["dictGiftOrderDetails"];
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("GiftOrderID", Convert.ToString(OrderId));
                GiftCertificateOrderBE objGiftCertificateOrderBE = GiftCertificateBL.getItem(Constants.USP_GetGiftCertificateDetails, param, true);

                ltrOrderNo.Text = objGiftCertificateOrderBE.GiftOASISID.ToString() ;
                lblAddress.Text = objGiftCertificateOrderBE.PreDefinedColumn1 + " " + objGiftCertificateOrderBE.PreDefinedColumn2 + " " + objGiftCertificateOrderBE.PreDefinedColumn3 + " " + objGiftCertificateOrderBE.PreDefinedColumn4 + " " + objGiftCertificateOrderBE.PreDefinedColumn5 + " " + objGiftCertificateOrderBE.PreDefinedColumn6 + " " + objGiftCertificateOrderBE.PreDefinedColumn7 + " " + objGiftCertificateOrderBE.PreDefinedColumn8 + " " + objGiftCertificateOrderBE.PreDefinedColumn9;
                rptOrderDetails.DataSource = objGiftCertificateOrderBE.lstGiftOrderDetails;
                rptOrderDetails.DataBind();

                ltrPaymentMethod.Text = strCreditCardText;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void GetQueryString()
        {
            try
            {
                // Check for encrypted query string
                orderId = Request.QueryString["orderid"];
                string encryptedQueryString = Request.QueryString["request"];
                string decryptedQueryString = string.Empty;
                string cryptoKey = string.Empty;
                if (!string.IsNullOrEmpty(encryptedQueryString))
                {
                    // Decrypt query strings
                    cryptoKey = System.Web.Configuration.WebConfigurationManager.AppSettings["CryptoKey"];
                    decryptedQueryString = GlobalFunctions.DecryptQueryStrings(encryptedQueryString, cryptoKey);
                }
                string[] strArr = decryptedQueryString.Split('&');
                string[] KeyValue = null;
                for (int iArr = 0; iArr < strArr.Length; iArr++)
                {
                    KeyValue = strArr[iArr].Split('=');
                    if (strArr[iArr].IndexOf('=') > 0)
                    {
                        if (Convert.ToString(KeyValue[0]).ToUpper() == "ORDERID")
                            orderId = Convert.ToString(KeyValue[1]);

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptOrderDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    dAmount += Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Amount")) * Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                }
                else if(e.Item.ItemType==ListItemType.Footer)
                {
                    dvSubTotal = (HtmlGenericControl)e.Item.FindControl("dvSubTotal");
                    dvSubTotal.InnerHtml = GlobalFunctions.GetCurrencySymbol() + Convert.ToString(dAmount);

                    dvShippingCharges = (HtmlGenericControl)e.Item.FindControl("dvShippingCharges");
                    dvShippingCharges.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "0";

                    dvTaxes = (HtmlGenericControl)e.Item.FindControl("dvTaxes");
                    dvTaxes.InnerHtml = GlobalFunctions.GetCurrencySymbol() + "0";

                    dvTotal = (HtmlGenericControl)e.Item.FindControl("dvTotal");
                    dvTotal.InnerHtml = GlobalFunctions.GetCurrencySymbol() + Convert.ToString(dAmount);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void BindResourceData()
        {
            try
            {
                intLanguageID = GlobalFunctions.GetLanguageId();
                intCurrencyID = GlobalFunctions.GetCurrencyId();
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();

                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x=> x.LanguageId == intLanguageID);
                    if(lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strOrderPlaceNote = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Thank_You_Note").ResourceValue;
                        strOrderEmailConfirmationNote = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Confirmation_Email_Note").ResourceValue;
                        strOrderNumberText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Number_Text").ResourceValue;
                        strAmountText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Amount").ResourceValue;
                        strQuantityText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        strRecipientEmailText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Recipient_Email_Text").ResourceValue;
                        strPersonalMessageText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Message_Title").ResourceValue;
                        strSubTotalText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_SubTotal").ResourceValue;
                        strShippingText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Shipping_Text").ResourceValue;
                        strTaxText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Tax_Text").ResourceValue;
                        strTotalText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Total_Text").ResourceValue;
                        strBillingAddressText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Billing_Address").ResourceValue;
                        strPaymentMethodText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Payment_Method_Text").ResourceValue;
                        strPrintText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Print_Text").ResourceValue;
                        strOrderNumberGenerationErrorText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Number_Generation_Error_Message").ResourceValue;
                        strCreditCardText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Credit_Card_Text").ResourceValue;
                        strConfirmation = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Confirmation_Title_Text").ResourceValue;
                        Generic_Order_Confirmation = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Confirmation").ResourceValue;
                        OrderDetails_CheckOut_Faster = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "OrderDetails_CheckOut_Faster").ResourceValue;
                        Generic_View_Order_History = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_View_Order_History").ResourceValue;
                        Generic_WriteReviews = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Write_Reviews").ResourceValue;
                        Order_Details_Set_Password_Reset_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_Details_Set_Password_Reset_Text").ResourceValue;
                        Register_Password_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Password_Text").ResourceValue;
                        Register_ConfirmPassword_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_ConfirmPassword_Text").ResourceValue;
                        OrderDetails_SaveDetails_FutureUse_Note = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "OrderDetails_SaveDetails_FutureUse_Note").ResourceValue;
                        strRegistrationSuccessful = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Registration_Successfully_Msg").ResourceValue;
                        strGeneric_RegistrationFailed_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegistrationDeclined_Message").ResourceValue;
                        btnCreateAccount.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Create_Account_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        #region Code Added by SHRIGANESH SINGH 16 May 2016 for Password Policy

                        #region Minimum Password Length
                        string minPass = "0";
                        lstStoreDetail = StoreBL.GetStoreDetails();
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue != null)
                        {
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 2a");
                            minPass = lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue;
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 3a");
                            Exceptions.WriteInfoLog("Registration:PS_MinimumPasswordLength -" + minPass);
                        }
                        else if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue != null)
                        {
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 2b");
                            minPass = lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue;
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 3b");
                            Exceptions.WriteInfoLog("Registration:PS_MinimumPasswordLength -" + minPass);
                        }
                        #endregion

                        #region Password Policy

                        lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();

                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;

                            regexAlphaNumtxtPassword.Enabled = false;
                            regexAlphaNumSymtxtPassword.Enabled = false;

                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";

                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;

                        }

                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;
                            regexAlphaNumtxtPassword.Enabled = true;
                            regexAlphaNumSymtxtPassword.Enabled = false;

                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;

                            regexAlphaNumtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]{0,50}";
                            regexAlphaNumtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        }

                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;
                            regexAlphaNumtxtPassword.Enabled = false;
                            regexAlphaNumSymtxtPassword.Enabled = true;

                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;

                            regexAlphaNumSymtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                            regexAlphaNumSymtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        }
                        #endregion

                        strMinPassLenErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strMinPassLenErrorMsg = strMinPassLenErrorMsg.Replace("@Num@", minPass);

                        strAlphaNumPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strAlphaNumSymPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnCreateAccount.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Create_Account_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        reqtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_Password_ConfirmPassword_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtConfirmPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_Password_ConfirmPassword_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        cmpValidatorPwd.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_PasswordMisMatch_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);             
            }
        }

        protected void btnCreateAccount_OnClick(object sender, EventArgs e)
        {
            try
            {

                UserBE lstUserBE = new UserBE();
                if (Session["User"] != null)
                {
                    lstUserBE = Session["User"] as UserBE;

                    intLanguageId = GlobalFunctions.GetLanguageId();
                    intCurrencyId = GlobalFunctions.GetCurrencyId();
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    
                    
                    #region code added by SHRIGANESH SINGH for Password Policy 13 May 2016

                    if (objStoreBE != null)
                    {
                        if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue != null)
                        {
                            minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                        }
                        else
                        {
                            minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].DefaultValue;
                        }

                        if (objStoreBE != null)
                        {
                            lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();

                            if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                            {
                                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                                {
                                    string password = txtODPassword.Text;
                                    minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                    if (password.Length < Convert.ToInt16(minPasswordLength))
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        return;
                                    }
                                }

                                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                                {
                                    string password = txtODPassword.Text;
                                    minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                    if (password.Length >= Convert.ToInt16(minPasswordLength))
                                    {
                                        string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]{0,50}";
                                        bool check = true;
                                        check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                        if (!check)
                                        {
                                            string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        return;
                                    }
                                }

                                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                                {
                                    string password = txtODPassword.Text;
                                    minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                    if (password.Length >= Convert.ToInt16(minPasswordLength))
                                    {
                                        string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                                        bool check = true;
                                        check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                        if (!check)
                                        {
                                            string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        return;
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    UserBE objBE = new UserBE();
                    string SaltPass = SaltHash.ComputeHash((txtODPassword.Text.Trim()), "SHA512", null);

                    objBE.UserId = lstUserBE.UserId;
                    objBE.FirstName = lstUserBE.FirstName;
                    objBE.LastName = lstUserBE.LastName;
                    objBE.Password = SaltPass;
                    objBE.PredefinedColumn1 = lstUserBE.PredefinedColumn1;
                    objBE.PredefinedColumn2 = lstUserBE.PredefinedColumn2;
                    objBE.PredefinedColumn3 = lstUserBE.PredefinedColumn3;
                    objBE.PredefinedColumn4 = lstUserBE.PredefinedColumn4;
                    objBE.PredefinedColumn5 = lstUserBE.PredefinedColumn5;
                    objBE.PredefinedColumn6 = lstUserBE.PredefinedColumn6;
                    objBE.PredefinedColumn7 = lstUserBE.PredefinedColumn7;
                    objBE.PredefinedColumn8 = lstUserBE.PredefinedColumn8;
                    objBE.PredefinedColumn9 = lstUserBE.PredefinedColumn9;
                    objBE.IPAddress = GlobalFunctions.GetIpAddress();
                    objBE.IsGuestUser = false;
                    int i = UserBL.ExecuteProfileDetails(Constants.USP_InsertUserProfileDetails, true, objBE);
                    if (i > 0)
                    {
                        UserBE objUser = new UserBE();
                        UserBE objBEs = new UserBE();
                        objBEs.EmailId = lstUserBE.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                        }

                        if (GlobalFunctions.SendRegistrationMail(lstUserBE.EmailId, lstUserBE.FirstName, lstUserBE.LastName))
                        {
                            //Session.Abandon();
                            Session["IsAnonymous"] = null;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strRegistrationSuccessful, host + "index", AlertType.Success);
                        }
                        else
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strGeneric_RegistrationFailed_Message, AlertType.Failure);
                    }
                    else
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strGeneric_RegistrationFailed_Message, AlertType.Failure);
                }
                else
                {
                    Response.RedirectToRoute("login");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}