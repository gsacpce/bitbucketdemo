﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.BusinessEntity;
using System.Text;
using PWGlobalEcomm.GlobalUtilities;
using System.Configuration;
namespace Presentation
{
    public partial class ShoppingCart_NameBadge : BasePage
    {
        protected global::System.Web.UI.WebControls.Repeater rptText;
        protected global::System.Web.UI.WebControls.TextBox txtNumOfNames, txtinput_badges_name, txtinput_badges_department, txtinput_badges_number, txtinput_badges_email, txtDeliveryName, txtDeliveryAddress,txtDate;
        protected global::System.Web.UI.WebControls.Button btnSubmit, Submit12;
        protected global::System.Web.UI.WebControls.LinkButton lnkButton;
        UserBE.NameBadgeBE objNameBadgeBE = new UserBE.NameBadgeBE();
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime d = DateTime.Now;
            string dateString = d.ToString("dd-MM-yyyy");
            txtDate.Text = dateString;
            

        }


        protected void lnkButton_Click(object sender, EventArgs e)
        {
            if (txtNumOfNames.Text=="")
            {
                GlobalFunctions.ShowModalAlertMessages(this, "Please Enter Number in Below Textbox", AlertType.Warning);
            }

            try
            {
                Session["NameBadges"] = null;

                int numberOfTextboxes;

                numberOfTextboxes = Convert.ToInt32(txtNumOfNames.Text);
                List<int> dataSource = new List<int>();
                for (int i = 0; i < numberOfTextboxes; i++)
                {
                    dataSource.Add(i);
                }
                this.rptText.DataSource = dataSource;
                this.rptText.DataBind();
                foreach(RepeaterItem rptItem in rptText.Items)
                {
                    Label lbl = (Label)rptItem.FindControl("Name");
                    lbl.Text =lbl.Text+Convert.ToString(rptItem.ItemIndex + 1);
                }
                if (Convert.ToInt16(txtNumOfNames.Text) <= 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Minimum Order Quantity Should Be 1", AlertType.Warning);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "ShowModal", "<script>javascript:ShowModal();</script>");
                }

            }
            catch (Exception ex)
            {
                GlobalFunctions.ShowModalAlertMessages(this, ex.Message, AlertType.Warning);
            }

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            StringBuilder strbldr = new StringBuilder();
            foreach (RepeaterItem rptItem in rptText.Items)
            {
                
                Label lbl = (Label)rptItem.FindControl("Name");
                

                TextBox txt = (TextBox)rptItem.FindControl("txt1");
                if (txt.Text != "")
                {

                    strbldr.Append("<html>");
                    strbldr.Append("<head>");
                    strbldr.Append("</head>");
                    strbldr.Append("<body>");
                    strbldr.Append("<table>");
                    strbldr.Append("<tr>");
                    strbldr.Append("<td>");
                    strbldr.Append((lbl.Text +":-" +txt.Text) + "");
                    strbldr.Append("</td>");
                    strbldr.Append("</tr>");
                    strbldr.Append("</table>");
                    strbldr.Append("</body>");
                    strbldr.Append("</html>");
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Please Fill All The Textbox", AlertType.Warning);
                }
            }
            Session["NameBadges"] = strbldr.ToString();
        }
        protected void Submit12_Click(object sender, EventArgs e)
        {
            if (txtNumOfNames.Text == "" || txtinput_badges_name.Text == "" || txtinput_badges_department.Text == "" || txtinput_badges_number.Text == "" || txtinput_badges_email.Text == "" || txtDeliveryName.Text == "" || txtDeliveryAddress.Text == "")
            {
                GlobalFunctions.ShowModalAlertMessages(this, "Please Fill Form Properly", AlertType.Warning);

            }
           
            bool success;
            try
            {
                if (Session["NameBadges"] != null)
                {
                    

                    objNameBadgeBE.Name = txtinput_badges_name.Text;
                    objNameBadgeBE.DepartmentName = txtinput_badges_department.Text;
                    objNameBadgeBE.PhoneNumber = txtinput_badges_number.Text;
                    objNameBadgeBE.Email_id = txtinput_badges_email.Text;
                    objNameBadgeBE.Delivery_Name = txtDeliveryName.Text;
                    objNameBadgeBE.Delivery_Address = txtDeliveryAddress.Text;
                    objNameBadgeBE.NumberOfNames = Convert.ToInt16(txtNumOfNames.Text);
                    success = UserBL.InsertNameBadges(objNameBadgeBE);
                    if (success == true)
                    {
                        string strEmailTo = "";
                        string strEmailCC = "";
                        string strEmailBCC = "";
                        
                        StringBuilder strbldrEmailMessage = new StringBuilder();
                      
                        strbldrEmailMessage.Append("<html>");
                        strbldrEmailMessage.Append("<head>");
                        strbldrEmailMessage.Append("</head>");
                        strbldrEmailMessage.Append("<body>");

                        strbldrEmailMessage.Append("<table>");
                        strbldrEmailMessage.Append("<CAPTION>");
                        strbldrEmailMessage.Append("Order Confirmation mail: <br>");
                        strbldrEmailMessage.Append("</CAPTION>");
                        strbldrEmailMessage.Append("<tr>");
                        strbldrEmailMessage.Append("<td>");
                        strbldrEmailMessage.Append(" ==================================================  <br/>");
                        strbldrEmailMessage.Append(" Date: " + txtDate.Text + "<br/>");
                        strbldrEmailMessage.Append("Name: " + txtinput_badges_name.Text + "<br/>");
                        strbldrEmailMessage.Append("Department Name: " + txtinput_badges_department.Text + "<br/>");
                        strbldrEmailMessage.Append("Telephone: " + txtinput_badges_number.Text + "<br/><br/>");
                        strbldrEmailMessage.Append("Email: " + txtinput_badges_email.Text + "<br/>");
                        strbldrEmailMessage.Append("</td>");
                        strbldrEmailMessage.Append("</tr>");
                        strbldrEmailMessage.Append("<tr>");
                        strbldrEmailMessage.Append("<td>");
                        strbldrEmailMessage.Append(" ==================================================  <br/>");
                        strbldrEmailMessage.Append("Delivery Address: <br />" + txtDeliveryAddress.Text + "<br/>");
                        strbldrEmailMessage.Append("Name: " + txtDeliveryName.Text + "<br/>");
                        strbldrEmailMessage.Append("</td>");
                        strbldrEmailMessage.Append("</tr>");
                        strbldrEmailMessage.Append("<tr>");
                        strbldrEmailMessage.Append("<td>");
                        strbldrEmailMessage.Append(" ==================================================  <br/>");
                        strbldrEmailMessage.Append(" <b>Name Badge</b><br/>");
                        strbldrEmailMessage.Append("</td>");
                        strbldrEmailMessage.Append("</tr>");
                        strbldrEmailMessage.Append("<tr>");
                        strbldrEmailMessage.Append("<td>");
                        strbldrEmailMessage.Append(" ==================================================  <br/>");

                        strbldrEmailMessage.Append(Session["NameBadges"]);
                        strbldrEmailMessage.Append("<tr><td>Name Badge Qty : " + txtNumOfNames.Text + "</td></tr>");
                        strbldrEmailMessage.Append("<tr>");
                        strbldrEmailMessage.Append("<td>");
                        strbldrEmailMessage.Append(" ==================================================  <br/>");
                        strbldrEmailMessage.Append("</td>");
                        strbldrEmailMessage.Append("</tr>");
                        
                        strbldrEmailMessage.Append("</table>");
                        strbldrEmailMessage.Append("</body>");
                        strbldrEmailMessage.Append("</html>");
                        txtinput_badges_name.Text = "";
                        txtinput_badges_department.Text = "";
                        txtinput_badges_number.Text = "";
                        txtinput_badges_email.Text = "";
                        txtDeliveryName.Text = "";
                        txtDeliveryAddress.Text = "";
                        txtNumOfNames.Text = "";
                        GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("NameBadgeToEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Name Badges", strbldrEmailMessage.ToString(), "", "", true);
                        Session["NameBadges"] = null;
                        GlobalFunctions.ShowModalAlertMessages(this, "Order Places Successfully", AlertType.Success);
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this, "Can't Place Order", AlertType.Warning);
                    }
                }

                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Please Enter Names In All Textbox", AlertType.Warning);
                }
               

            }
            catch (Exception ex)
            {
                GlobalFunctions.ShowModalAlertMessages(this, ex.Message, AlertType.Warning);
            }

        }

       

    }
}