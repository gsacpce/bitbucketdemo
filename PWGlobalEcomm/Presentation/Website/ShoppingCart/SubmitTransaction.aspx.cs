﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHPP.ASP.Net.Lib;
using System.Configuration;

namespace Presentation
{
    public class ShoppingCart_SubmitTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Message transaction;
                if (!IsPostBack)
                {
                    // Get stored transaction data
                    transaction = (Message)Session["transaction"];
                    Adflex.ErrorManager.ErrorMsg err;
                    err = transaction.PostMessageToAdflex(ConfigurationManager.AppSettings["TransactionSubmitURL"], ConfigurationManager.AppSettings["AdflexSecurityKey"], true);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}