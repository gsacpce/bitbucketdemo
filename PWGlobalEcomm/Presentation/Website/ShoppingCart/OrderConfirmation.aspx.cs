﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Collections;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using AHPP.ASP.Net.Lib;
using PWGlobalEcomm.BusinessLogic;
using System.Threading;
using System.Xml.Linq;
using System.Net;
//using PWGlobalEcomm.Order;
using System.Xml;
using System.Globalization;

namespace Presentation
{
    public partial class ShoppingCart_OrderConfirmation : BasePage //System.Web.UI.Page
    {
        #region Variables
        private string orderId, storeId, txRefGUID;

        public string OrderId
        {
            get
            {
                return orderId;
            }
            set
            {
                orderId = value;
            }
        }

        public string StoreId
        {
            get
            {
                return storeId;
            }
            set
            {
                storeId = value;
            }
        }

        public string TxRefGUID
        {
            get
            {
                return txRefGUID;
            }
            set
            {
                txRefGUID = value;
            }
        }
        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        protected global::System.Web.UI.HtmlControls.HtmlInputRadioButton rbService1, rbService2, rbService3;
        //protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnStandardText, spnExpressText;
        public string strShippingMethodEmail;
        UserBE lst = new UserBE();

        Dictionary<string, string> dictGiftOrder = new Dictionary<string, string>();
        List<Dictionary<string, string>> dictGiftOrderDetailsLst = new List<Dictionary<string, string>>();
        AdFlexTransactionsBE objAdflexTrans = new AdFlexTransactionsBE();
        TeleCashTransactionsBE objTeleCashtrans = new TeleCashTransactionsBE();
        public string strGiftCertificate, strSubTotal, strShipping, strTax, strTotal;
        public Int16 intLanguageId = 0;
        public string strAddtionalcharges = string.Empty;

        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-09-15
        /// Scope   : Page_Load of the ShoppingCart_OrderConfirmation page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bOrderPlaced = false;
            if (!Page.IsPostBack && Session["transaction"] != null)
            {
                Exceptions.WriteInfoLog("Inside Session[transaction] ");
                intLanguageId = GlobalFunctions.GetLanguageId();
                BindResourceData();
                if (Session["User"] == null)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                    return;
                }
                GetQueryString();
                try
                {
                    lst = Session["User"] as UserBE;
                    if (Session["dictGiftOrder"] == null && Session["dictGiftOrder"] == null)
                    {
                        Exceptions.WriteInfoLog("Inside Session[transaction] ");
                        #region "Product Purchase"
                        CustomerOrderBE lstCustomerOrders = new CustomerOrderBE();
                        CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
                        objCustomerOrderBE.OrderedBy = lst.UserId;
                        objCustomerOrderBE.LanguageId = GlobalFunctions.GetLanguageId();
                        objCustomerOrderBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                        lstCustomerOrders = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                        Exceptions.WriteInfoLog("Payment Method : " + lstCustomerOrders.PaymentMethod);
                        if (lstCustomerOrders == null)
                        {
                            Response.RedirectToRoute("index");
                            return;
                        }
                        //For testing purpose
                        //SendOrderEmail(lstCustomerOrders, lst.EmailId, 12345);
                        //return;
                        Exceptions.WriteInfoLog(lstCustomerOrders.PaymentMethodType.ToLower());
                        Exceptions.WriteInfoLog(lstCustomerOrders.PaymentMethodType.ToLower());
                        if (Convert.ToBoolean(Session["IsTotalvalueZeroafterGC"]))// vikram for legal entity
                        {
                            // vikram for legal entity
                            #region Non-Credit card Payments
                            //For other payment methods 
                            double intGoodsTotlal = 0;
                            double unitprice = 0;

                            for (int intCart = 0; intCart < lstCustomerOrders.CustomerOrderProducts.Count; intCart++)
                            {
                                if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode) && (lstCustomerOrders.BehaviourType.ToLower() == "product" || lstCustomerOrders.BehaviourType.ToLower() == "basket"))
                                    unitprice = GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstCustomerOrders.CustomerOrderProducts[intCart].Price), lstCustomerOrders.DiscountPercentage);
                                else
                                    unitprice = double.Parse(Convert.ToString(lstCustomerOrders.CustomerOrderProducts[intCart].Price));
                                intGoodsTotlal = intGoodsTotlal + (Convert.ToInt32(lstCustomerOrders.CustomerOrderProducts[intCart].Quantity) * unitprice);
                            }


                            Int32 intAuthorizeCreditCard = 1;
                            StoreBE objStoreBE = StoreBL.GetStoreDetails();
                            /*
                             *   Commented as per Gary advise to discard authorise payment webservice for all scenerios
                            if (objStoreBE != null && objStoreBE.IsBASYS)
                                intAuthorizeCreditCard = ValidateWebServices_OASIS.BuildCreditCardDetails(intGoodsTotlal.ToString("#####0.###0", CultureInfo.InvariantCulture), lst.BASYS_CustomerContactId, ConfigurationManager.AppSettings["MerchantNumber"], "", "", "", "", "", "", "", "", "", "", "", "", lstCustomerOrders.PaymentMethod);
                            else
                                intAuthorizeCreditCard = lstCustomerOrders.CustomerOrderId;
                             * */

                            if (intAuthorizeCreditCard > 0)
                            {
                                //Calling the Order Webservice to place the order in BA system//
                                Int32 intOASISOrderId = 0;
                                if (objStoreBE != null && objStoreBE.IsBASYS)
                                    // bOrderPlaced = Order_Webservice(ref intOASISOrderId, lst, lstCustomerOrders, "", "", false, Convert.ToString(intAuthorizeCreditCard),"","");
                                    bOrderPlaced = Order_Webservice(ref intOASISOrderId, lst, lstCustomerOrders, "", "", false, Convert.ToString(intAuthorizeCreditCard), "", "", Convert.ToString(Session["CustomerRef"]));

                                else
                                {
                                    bOrderPlaced = true;
                                    intOASISOrderId = lstCustomerOrders.CustomerOrderId;
                                }
                                if (bOrderPlaced)
                                {
                                    UpdateOrderStatus(lstCustomerOrders, lst.EmailId, intOASISOrderId, Convert.ToString(intAuthorizeCreditCard), "");
                                    Session["sbGift"] = null;
                                }
                                else
                                {
                                    Session["transaction"] = null;
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                }
                            }
                            else
                            {
                                Session["transaction"] = null;
                                GlobalFunctions.ShowModalAlertMessages(this.Page, "There was an error processing your request please contact the helpdesk.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                            }
                            #endregion // vikram for legal entity
                        }
                        else 
                        {
                            if (lstCustomerOrders.PaymentMethodType.ToLower() == "c")
                            {
                                Exceptions.WriteInfoLog("Inside Credit Card ");
                                #region CreditCard Payment
                                if (!string.IsNullOrEmpty(StoreId) && !string.IsNullOrEmpty(orderId) && Convert.ToInt32(OrderId) > 0)
                                {
                                    //AdFlexTransactionsBE objAdflexTrans = new AdFlexTransactionsBE();
                                    objAdflexTrans = ShoppingCartBL.GetAdFlexTransactionDetails(Convert.ToInt16(storeId), Convert.ToInt16(orderId), TxRefGUID);

                                    if (objAdflexTrans != null && objAdflexTrans.inTransId != 0)
                                    {
                                        if (objAdflexTrans.inStatusCode == 690 && (TrnStatus)objAdflexTrans.inStatusCode == TrnStatus.AuthorisedOnly)
                                        {
                                            //Calling the Order Webservice to place the order in BA system//
                                            Int32 intOASISOrderId = 0;
                                            StoreBE objStoreBE = StoreBL.GetStoreDetails();
                                            if (objStoreBE != null && objStoreBE.IsBASYS)
                                                bOrderPlaced = Order_Webservice(ref intOASISOrderId, lst, lstCustomerOrders, objAdflexTrans.stTxRefGUID, objAdflexTrans.stCardNumber, true, objAdflexTrans.stAuthCode, objAdflexTrans.ReqCardToken, objAdflexTrans.MID, Session["CustomerRef"].ToString());
                                            // bOrderPlaced = Order_Webservice(ref intOASISOrderId, lst, lstCustomerOrders, objAdflexTrans.stTxRefGUID, objAdflexTrans.stCardNumber, true, objAdflexTrans.stAuthCode); vikram 07122016 adflex
                                            else
                                            {
                                                intOASISOrderId = lstCustomerOrders.CustomerOrderId;
                                                bOrderPlaced = true;
                                            }

                                            if (bOrderPlaced)
                                            {
                                                UpdateOrderStatus(lstCustomerOrders, lst.EmailId, intOASISOrderId, objAdflexTrans.stAuthCode, objAdflexTrans.stCardScheme);
                                            }
                                            else
                                            {
                                                Session["transaction"] = null;
                                                GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                            }
                                        }
                                        else if (objAdflexTrans.inStatusCode == 683 && (TrnStatus)objAdflexTrans.inStatusCode == TrnStatus.Cancelled)
                                        {
                                            Session["transaction"] = null;
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, "You have cancelled this transaction.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                        }
                                        else
                                        {
                                            Session["transaction"] = null;
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                        }
                                    }
                                    else
                                    {
                                        //return;
                                        Session["transaction"] = null;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                    }
                                }
                                else
                                {
                                    Session["transaction"] = null;
                                    Response.RedirectToRoute("checkoutPage");
                                }
                                #endregion
                            }
                            #region commented  as we need to build xml in Invoice pattern


                            else if (lstCustomerOrders.PaymentMethod.ToLower() == "58")
                            {
                                Exceptions.WriteInfoLog("Inside TeleCash");
                                #region Payment Telecash
                                if (!string.IsNullOrEmpty(StoreId) && !string.IsNullOrEmpty(orderId) && Convert.ToInt32(OrderId) > 0)
                                {
                                    //AdFlexTransactionsBE objAdflexTrans = new AdFlexTransactionsBE();
                                    objTeleCashtrans = ShoppingCartBL.GetTeleCashTransactionDetails(Convert.ToInt16(orderId));

                                    if (objTeleCashtrans != null)
                                    {
                                        string[] approval = objTeleCashtrans.approval_code.Split(':');
                                        string bname = Convert.ToString(objTeleCashtrans.bname);
                                        string successcode = approval[0];
                                        Exceptions.WriteInfoLog("successcode:" + successcode);
                                        if (successcode == "Y" || bname.ToLower().Contains("powerweave"))
                                        {
                                            //Calling the Order Webservice to place the order in BA system//
                                            Int32 intOASISOrderId = 0;
                                            StoreBE objStoreBE = StoreBL.GetStoreDetails();
                                            double intGoodsTotlal = 0;
                                            double unitprice = 0;

                                            for (int intCart = 0; intCart < lstCustomerOrders.CustomerOrderProducts.Count; intCart++)
                                            {
                                                if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode) && (lstCustomerOrders.BehaviourType.ToLower() == "product" || lstCustomerOrders.BehaviourType.ToLower() == "basket"))
                                                    unitprice = GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstCustomerOrders.CustomerOrderProducts[intCart].Price), lstCustomerOrders.DiscountPercentage);
                                                else
                                                    unitprice = double.Parse(Convert.ToString(lstCustomerOrders.CustomerOrderProducts[intCart].Price));
                                                intGoodsTotlal = intGoodsTotlal + (Convert.ToInt32(lstCustomerOrders.CustomerOrderProducts[intCart].Quantity) * unitprice);
                                            }


                                            Int32 intAuthorizeCreditCard = 1;

                                            /* 
                                             * Commented as per Gary advise to discard authorise payment webservice for all scenerios
                                            if (objStoreBE != null && objStoreBE.IsBASYS)
                                                intAuthorizeCreditCard = ValidateWebServices_OASIS.BuildCreditCardDetails(intGoodsTotlal.ToString("#####0.###0", CultureInfo.InvariantCulture), lst.BASYS_CustomerContactId, ConfigurationManager.AppSettings["MerchantNumber"], "", "", "", "", "", "", "", "", "", "", "", "", lstCustomerOrders.PaymentMethod);
                                            else
                                                intAuthorizeCreditCard = lstCustomerOrders.CustomerOrderId;
                                             **/

                                            if (intAuthorizeCreditCard > 0)
                                            {
                                                //Calling the Order Webservice to place the order in BA system//
                                                //  Int32 intOASISOrderId = 0;
                                                if (objStoreBE != null && objStoreBE.IsBASYS)
                                                    bOrderPlaced = Order_Webservice(ref intOASISOrderId, lst, lstCustomerOrders, "", "", false, Convert.ToString(intAuthorizeCreditCard), "", "", Convert.ToString(Session["CustomerRef"]));
                                                else
                                                {
                                                    bOrderPlaced = true;
                                                    intOASISOrderId = lstCustomerOrders.CustomerOrderId;
                                                }
                                                if (bOrderPlaced)
                                                {
                                                    UpdateOrderStatus(lstCustomerOrders, lst.EmailId, intOASISOrderId, Convert.ToString(intAuthorizeCreditCard), "");
                                                    Session["sbGift"] = null;
                                                }
                                                else
                                                {
                                                    Session["transaction"] = null;
                                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                                }
                                            }
                                            else
                                            {
                                                Session["transaction"] = null;
                                                GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                            }
                                        }
                                        else if (successcode == "N")
                                        {
                                            Session["transaction"] = null;
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);

                                        }
                                        else
                                        {
                                            Session["transaction"] = null;
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                        }


                                    }
                                    else
                                    {
                                        //return;
                                        Session["transaction"] = null;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                    }
                                }
                                else
                                {
                                    Session["transaction"] = null;
                                    Response.RedirectToRoute("checkoutPage");
                                }
                                #endregion
                            }


                            #endregion
                            else
                            {
                                Exceptions.WriteInfoLog("Inside InVoice");
                                #region Non-Credit card Payments
                                //For other payment methods 
                                double intGoodsTotlal = 0;
                                double unitprice = 0;

                                for (int intCart = 0; intCart < lstCustomerOrders.CustomerOrderProducts.Count; intCart++)
                                {
                                    if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode) && (lstCustomerOrders.BehaviourType.ToLower() == "product" || lstCustomerOrders.BehaviourType.ToLower() == "basket"))
                                        unitprice = GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstCustomerOrders.CustomerOrderProducts[intCart].Price), lstCustomerOrders.DiscountPercentage);
                                    else
                                        unitprice = double.Parse(Convert.ToString(lstCustomerOrders.CustomerOrderProducts[intCart].Price));
                                    intGoodsTotlal = intGoodsTotlal + (Convert.ToInt32(lstCustomerOrders.CustomerOrderProducts[intCart].Quantity) * unitprice);
                                }


                                Int32 intAuthorizeCreditCard = 1;
                                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                                /*
                                 *   Commented as per Gary advise to discard authorise payment webservice for all scenerios
                                if (objStoreBE != null && objStoreBE.IsBASYS)
                                    intAuthorizeCreditCard = ValidateWebServices_OASIS.BuildCreditCardDetails(intGoodsTotlal.ToString("#####0.###0", CultureInfo.InvariantCulture), lst.BASYS_CustomerContactId, ConfigurationManager.AppSettings["MerchantNumber"], "", "", "", "", "", "", "", "", "", "", "", "", lstCustomerOrders.PaymentMethod);
                                else
                                    intAuthorizeCreditCard = lstCustomerOrders.CustomerOrderId;
                                 * */

                                if (intAuthorizeCreditCard > 0)
                                {
                                    //Calling the Order Webservice to place the order in BA system//
                                    Int32 intOASISOrderId = 0;
                                    if (objStoreBE != null && objStoreBE.IsBASYS)
                                        // bOrderPlaced = Order_Webservice(ref intOASISOrderId, lst, lstCustomerOrders, "", "", false, Convert.ToString(intAuthorizeCreditCard),"","");
                                        bOrderPlaced = Order_Webservice(ref intOASISOrderId, lst, lstCustomerOrders, "", "", false, Convert.ToString(intAuthorizeCreditCard), "", "", Convert.ToString(Session["CustomerRef"]));

                                    else
                                    {
                                        bOrderPlaced = true;
                                        intOASISOrderId = lstCustomerOrders.CustomerOrderId;
                                    }
                                    if (bOrderPlaced)
                                    {
                                        UpdateOrderStatus(lstCustomerOrders, lst.EmailId, intOASISOrderId, Convert.ToString(intAuthorizeCreditCard), "");
                                        Session["sbGift"] = null;
                                    }
                                    else
                                    {
                                        Session["transaction"] = null;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                    }
                                }
                                else
                                {
                                    Session["transaction"] = null;
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There was an error processing your request please contact the helpdesk.", GlobalFunctions.GetVirtualPath() + "checkout", AlertType.Failure);
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Gift Certificate
                        if (!string.IsNullOrEmpty(StoreId) && !string.IsNullOrEmpty(orderId) && Convert.ToInt32(OrderId) > 0)
                        {
                            objAdflexTrans = ShoppingCartBL.GetAdFlexTransactionDetails(Convert.ToInt16(storeId), Convert.ToInt16(orderId), TxRefGUID);

                            if (objAdflexTrans != null && objAdflexTrans.inTransId != 0)
                            {
                                if (objAdflexTrans.inStatusCode == 690 && (TrnStatus)objAdflexTrans.inStatusCode == TrnStatus.AuthorisedOnly || lst.EmailId.Contains("powerweave"))
                                {
                                    dictGiftOrder = (Dictionary<string, string>)Session["dictGiftOrder"];
                                    dictGiftOrderDetailsLst = (List<Dictionary<string, string>>)Session["dictGiftOrderDetails"];

                                    Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                                    DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());
                                    DataAccessBase.Insert(Constants.USP_InsertGiftCertificate, dictGiftOrder, ref DictionaryOutParameterInstance, true);
                                    int iGiftID = 0;
                                    foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                                    {
                                        if (item.Key == "ReturnId")
                                            iGiftID = Convert.ToInt16(item.Value);
                                    }

                                    foreach (Dictionary<string, string> strDetails in dictGiftOrderDetailsLst)
                                    {
                                        strDetails["GiftOrderID"] = Convert.ToString(iGiftID);
                                        DataAccessBase.Insert("USP_InsertGiftOrderDetails", strDetails, true);
                                    }
                                    int iOasis = GenerateGiftCertificateXML();

                                    GiftCertificateOrderBE objGiftCertificateOrderBE = new GiftCertificateOrderBE();
                                    objGiftCertificateOrderBE.GiftOASISID = iOasis;
                                    objGiftCertificateOrderBE.GiftOrderID = iGiftID;
                                    GiftCertificateBL.Update(objGiftCertificateOrderBE);

                                    Hashtable hstParameters = new Hashtable();
                                    hstParameters.Add("orderId", iGiftID);
                                    string url = GlobalFunctions.EncryptURL(hstParameters, host + "shoppingcart/OrderDetailsGiftCertificate.aspx?");
                                    objAdflexTrans.inOrderId = iGiftID;
                                    if (iGiftID > 0 && iOasis > 0)
                                    {
                                        ShoppingCartBL.UpdateAdFlexTransactionDetails(objAdflexTrans);

                                        SendOrderEmailGiftCertificate(iOasis);

                                        Session["dictGiftOrder"] = null;
                                        Session["dictGiftOrderDetails"] = null;
                                        Session["dTotlaGoods"] = null;
                                        Response.Redirect(url);
                                    }
                                    else
                                    {
                                        Session["transaction"] = null;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                                    }
                                }
                                else
                                {
                                    Session["transaction"] = null;
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                                }
                            }
                            else
                            {
                                //Session["transaction"] = null;
                                //GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                            }
                        }
                        else
                        {
                            Session["transaction"] = null;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, "There is an error in processing your payment. Please contact site admin.", GlobalFunctions.GetVirtualPath() + "index", AlertType.Failure);
                        }
                        #endregion
                    }
                }
                catch (ThreadAbortException) { }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            else
            {
                Response.RedirectToRoute("index");
                return;
            }
        }

        ///<Summary>
        ///Author   :   SHRIGANESH SINGH
        ///Date     :   29 March 2016
        ///Scope    :   BindResourceData of OrderConfirmation Page 
        ///</Summary>
        ///<results></results>
        protected void BindResourceData()
        {
            try
            {
                //Cache["AllResourceData"] = null
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strGiftCertificate = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_GiftCertificateTitle").ResourceValue;
                        strSubTotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_SubTotal").ResourceValue;
                        strShipping = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Shipping").ResourceValue;
                        strTax = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Tax").ResourceValue;
                        strTotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Total_Text").ResourceValue;
                        strAddtionalcharges = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Handlingfee" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 07-10-15
        /// Scope   : Update customer order information
        /// </summary>
        protected void UpdateOrderStatus(CustomerOrderBE lstCustomerOrders, string strUserEmailId, Int32 intOASISOrderId, string strAuthCode, string strCardScheme)
        {
            try
            {
                #region UpdateOrderstatus
                CustomerOrderBE objUpdateCustomerOrder = new CustomerOrderBE();
                objUpdateCustomerOrder.OrderedBy = lstCustomerOrders.OrderedBy;
                objUpdateCustomerOrder.action = DBAction.Update;
                objUpdateCustomerOrder.CustomerOrderId = lstCustomerOrders.CustomerOrderId;
                objUpdateCustomerOrder.ApprovedBy = lstCustomerOrders.ApprovedBy;
                objUpdateCustomerOrder.InvoiceCompany = lstCustomerOrders.InvoiceCompany;
                objUpdateCustomerOrder.InvoiceContactPerson = lstCustomerOrders.InvoiceContactPerson;
                objUpdateCustomerOrder.InvoiceAddress1 = lstCustomerOrders.InvoiceAddress1;
                objUpdateCustomerOrder.InvoiceAddress2 = lstCustomerOrders.InvoiceAddress2;
                objUpdateCustomerOrder.InvoiceCity = lstCustomerOrders.InvoiceCity;
                objUpdateCustomerOrder.InvoicePostalCode = lstCustomerOrders.InvoicePostalCode;
                objUpdateCustomerOrder.InvoiceCountry = lstCustomerOrders.InvoiceCountry;
                objUpdateCustomerOrder.InvoiceCounty = lstCustomerOrders.InvoiceCounty;
                objUpdateCustomerOrder.InvoicePhone = lstCustomerOrders.InvoicePhone;
                objUpdateCustomerOrder.InvoiceFax = lstCustomerOrders.InvoiceFax;
                objUpdateCustomerOrder.InvoiceEmail = lstCustomerOrders.InvoiceEmail;
                objUpdateCustomerOrder.PaymentMethod = lstCustomerOrders.PaymentMethod;
                objUpdateCustomerOrder.OrderRefNo = lstCustomerOrders.OrderRefNo;
                objUpdateCustomerOrder.AuthorizationId_OASIS = strAuthCode;
                objUpdateCustomerOrder.OrderId_OASIS = intOASISOrderId;
                objUpdateCustomerOrder.TxnRefGUID = lstCustomerOrders.TxnRefGUID;
                objUpdateCustomerOrder.IsOrderCompleted = true;
                objUpdateCustomerOrder.DeliveryCompany = lstCustomerOrders.DeliveryCompany;
                objUpdateCustomerOrder.DeliveryAddressId = lstCustomerOrders.DeliveryAddressId;
                objUpdateCustomerOrder.DeliveryContactPerson = lstCustomerOrders.DeliveryContactPerson;
                objUpdateCustomerOrder.DeliveryAddress1 = lstCustomerOrders.DeliveryAddress1;
                objUpdateCustomerOrder.DeliveryAddress2 = lstCustomerOrders.DeliveryAddress2;
                objUpdateCustomerOrder.DeliveryCity = lstCustomerOrders.DeliveryCity;
                objUpdateCustomerOrder.DeliveryPostalCode = lstCustomerOrders.DeliveryPostalCode;
                objUpdateCustomerOrder.DeliveryCountry = lstCustomerOrders.DeliveryCountry;
                objUpdateCustomerOrder.DeliveryCounty = lstCustomerOrders.DeliveryCounty;
                objUpdateCustomerOrder.DeliveryPhone = lstCustomerOrders.DeliveryPhone;
                objUpdateCustomerOrder.DeliveryFax = lstCustomerOrders.DeliveryFax;
                objUpdateCustomerOrder.stCardScheme = strCardScheme;
                objUpdateCustomerOrder.SpecialInstructions = lstCustomerOrders.SpecialInstructions;
                objUpdateCustomerOrder.CurrencyId = lstCustomerOrders.CurrencyId;
                objUpdateCustomerOrder.LanguageId = lstCustomerOrders.LanguageId;
                objUpdateCustomerOrder.StandardCharges = lstCustomerOrders.StandardCharges;
                objUpdateCustomerOrder.ExpressCharges = lstCustomerOrders.ExpressCharges;
                objUpdateCustomerOrder.PalletCharges = lstCustomerOrders.PalletCharges;//commented for Adient changes
                objUpdateCustomerOrder.TotalTax = lstCustomerOrders.TotalTax;
                objUpdateCustomerOrder.OrderCarrierId = lstCustomerOrders.OrderCarrierId;
                objUpdateCustomerOrder.OrderCarrierName = lstCustomerOrders.OrderCarrierName;
                objUpdateCustomerOrder.CouponCode = lstCustomerOrders.CouponCode;
                objUpdateCustomerOrder.handlingfee = lstCustomerOrders.handlingfee;
                ShoppingCartBL.CustomerOrder_SAE(objUpdateCustomerOrder);

                //Update coupon code status if user has applied coupon code                
                ShoppingCartBL.UpdateUsedCouponCode(objUpdateCustomerOrder);

                //Update user points if store is points enabled
                if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                {
                    #region UpdateUserPoints
                    List<UserPointBE> Users = new List<UserPointBE>();
                    UserBE lstUser = new UserBE();
                    lstUser = Session["User"] as UserBE;

                    double unitprice = 0;
                    double intGoodsTotlal = 0;
                     double intPoints = 0;		
	                    string test;
                    for (int intCart = 0; intCart < lstCustomerOrders.CustomerOrderProducts.Count; intCart++)
                    {
                        if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode) && (lstCustomerOrders.BehaviourType.ToLower() == "product" || lstCustomerOrders.BehaviourType.ToLower() == "basket"))
                            unitprice = GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstCustomerOrders.CustomerOrderProducts[intCart].Price), lstCustomerOrders.DiscountPercentage);
                        else
                            unitprice = double.Parse(Convert.ToString(lstCustomerOrders.CustomerOrderProducts[intCart].Price));
                        intGoodsTotlal += Convert.ToDouble(Convert.ToString(Convert.ToInt32(lstCustomerOrders.CustomerOrderProducts[intCart].Quantity) * unitprice));
                     
                       // intGoodsTotlal = Convert.ToDouble(GlobalFunctions.RemovePointsSpanIntheText(GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(intGoodsTotlal), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId())));

                        //intGoodsTotlal += Convert.ToDouble(GlobalFunctions.RemovePointsSpanIntheText(GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(Convert.ToInt32(lstCustomerOrders.CustomerOrderProducts[intCart].Quantity) * unitprice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId())));

                        //test = GlobalFunctions.RemovePointsSpanIntheText(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(Convert.ToInt32(lstCustomerOrders.CustomerOrderProducts[intCart].Quantity) * unitprice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()));
                        //var classa = Convert.ToDecimal(test);
                        //intPoints += Convert.ToInt32(test);
                    }
                    intGoodsTotlal = Convert.ToDouble(GlobalFunctions.RemovePointsSpanIntheText(GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(intGoodsTotlal), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId())));
                    #region CODE ADDED BY SHRIGANESH 19 Sept 2016
                    //if (lstCustomerOrders.StandardCharges != 0)
                   // {
                        /*intGoodsTotlal = intGoodsTotlal;+ Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstCustomerOrders.StandardCharges), "", GlobalFunctions.GetLanguageId()));*/
                      //  intGoodsTotlal = Convert.ToDouble(GlobalFunctions.RemovePointsSpanIntheText(GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(intGoodsTotlal), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId())));
                    //}
                   // else
                    //{
                        if (lstCustomerOrders.ExpressCharges != 0)
                        {
                            intGoodsTotlal = intGoodsTotlal + Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstCustomerOrders.ExpressCharges), "", GlobalFunctions.GetLanguageId()));
                        }
                        /* Commented for Adient changes
                    else if (lstCustomerOrders.PalletCharges != 0)
                    {
                        intGoodsTotlal = intGoodsTotlal + Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstCustomerOrders.PalletCharges), "", GlobalFunctions.GetLanguageId()));
                    }*/
                    //}
                    #endregion
                    if (intGoodsTotlal > lstUser.Points)
                        intGoodsTotlal = 0;
                    else
                        intGoodsTotlal = lstUser.Points - intGoodsTotlal;

                    Users.Add(new UserPointBE() { UserId = lstUser.UserId, PreviousPoints = lstUser.Points, Points = Convert.ToInt16(intGoodsTotlal) });
                    DataTable dtUpdatePoint = GetDataIntoTable(Users);
                    bool IsPointUpdated = UserBL.UpdatePoints(dtUpdatePoint);
                    if (IsPointUpdated)
                    {
                        UserBE objUser = new UserBE();
                        UserBE objBEs = new UserBE();
                        objBEs.EmailId = lstUser.EmailId;
                        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                        }
                    }
                    #endregion
                }
                string strGift = string.Empty, strGCAmount = string.Empty;
                if (Session["Amount"] != null)
                {
                    strGCAmount = Convert.ToString(Session["Amount"]);
                    string strRGift = Convert.ToString(Session["sbGift"]);
                    string[] strArr = strRGift.Split('|');
                    //strGift = strArr[0];
                    for (int i = 0; i < strArr.Length; i = (i + 3))
                    {
                        if (i == 0)
                        {
                            strGift = strArr[i];
                        }
                        else
                        {
                            strGift = strGift + "|" + strArr[i];
                        }
                    }
                }
                if (!string.IsNullOrEmpty(strGift))
                {
                    bool bRes = ShoppingCartBL.UpdateUsedGiftCertificateCode(strGift, objUpdateCustomerOrder.CustomerOrderId, strGCAmount);
                }

                //sending order confirmation email to user//
                SendOrderEmail(lstCustomerOrders, strUserEmailId, intOASISOrderId);

                Hashtable hstParameters = new Hashtable();
                hstParameters.Add("orderId", lstCustomerOrders.CustomerOrderId);
                string url = GlobalFunctions.EncryptURL(hstParameters, host + "shoppingcart/OrderDetails.aspx?");
                Response.Redirect(url);
                #endregion
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 24-11-15
        /// Scope   : convert list to datatable
        /// </summary>
        protected DataTable GetDataIntoTable(List<UserPointBE> returnList)
        {
            DataTable dt = GlobalFunctions.To_DataTable(returnList);
            dt.Columns["UserId"].ColumnName = "UserId";
            dt.Columns["PreviousPoints"].ColumnName = "PreviousPoints";
            dt.Columns["Points"].ColumnName = "Points";
            return dt;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 01-10-15
        /// Scope   : Send order confirmation mail to the customer
        /// </summary>
        protected void SendOrderEmail(CustomerOrderBE lstCustomerOrder, string strToEmailId, Int32 intOrderId_OASIS)
        {
            try
            {
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = "Order Confirmation";
                objEmail.LanguageId = lstCustomerOrder.LanguageId;
                objEmail.EmailTemplateId = 0;

                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    string strFromEmailId = lstEmail[0].FromEmailId;
                    string strCCEmailId = lstEmail[0].CCEmailId;
                    string strBCCEmailId = lstEmail[0].BCCEmailId;
                    bool IsHtml = lstEmail[0].IsHtml;
                    string strSubject = HttpUtility.HtmlDecode(lstEmail[0].Subject) + " - " + intOrderId_OASIS;
                    string strMailBody = GenerateMailBody(lstCustomerOrder, lstEmail[0].Body, intOrderId_OASIS);

                    #region Gift Certificate
                    string strGift = string.Empty, strGCAmount = string.Empty;
                    if (Session["Amount"] != null)
                    {
                        strGCAmount = Convert.ToString(Session["Amount"]);
                        string strRGift = Convert.ToString(Session["sbGift"]);
                        string[] strArr = strRGift.Split('|');
                        //strGift = strArr[0];
                        for (int i = 0; i < strArr.Length; i = (i + 3))
                        {
                            if (i == 0)
                            {
                                strGift = strArr[i];
                            }
                            else
                            {
                                strGift = strGift + "," + strArr[i];
                            }
                        }

                        strMailBody = strMailBody + "<tr><td style='padding: 15px; ' valign='top' width='100%'><table align='left' border='0' cellpadding='0' cellspacing='0' class='margin010' style='font-family: sans-serif; font-size: 14px;'>"
                        + "<tbody><tr style='color: #007cbf;'><th align='left' class='fontsize' valign='top'>" + strGiftCertificate + "</th></tr>"
                           + "<tr><td align='left' class='fontsize' style=' line-height: 19px; font-family: sans-serif; font-size: 14px; font-weight: 300;' valign='top'>"
                       + strGift + "</td><td align='left' class='fontsize' style=' line-height: 19px; font-family: sans-serif; font-size: 14px; font-weight: 300;' valign='top'>"
                                   + Convert.ToString(Session["Amount"])
                                + "</td></tr></tbody></table></td></tr>";
                    }
                    #endregion

                   
                    //Response.Clear();
                    //Response.Write(strMailBody);
                    //Response.End();
                    GlobalFunctions.SendEmail(strToEmailId, strFromEmailId, "", strCCEmailId, strBCCEmailId, strSubject, strMailBody, "", "", IsHtml);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-09-15
        /// Scope   : setting query string values
        /// </summary>
        private string GenerateMailBody(CustomerOrderBE lstCustomerOrders, string strMailBody, Int32 intOrderId_OASIS)
        {
            try
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                string strBillingAddress = lstCustomerOrders.InvoiceContactPerson + "<br>" + lstCustomerOrders.InvoiceCompany + "<br>" +
                      lstCustomerOrders.InvoiceAddress1 + "<br>" + lstCustomerOrders.InvoiceAddress2 + "<br>" + lstCustomerOrders.InvoiceCity + "<br>" +
                      lstCustomerOrders.InvoiceCounty + "<br>" + lstCustomerOrders.InvoicePostalCode + "<br>" + lstCustomerOrders.InvoiceCountryName
                      + "<br>" + lstCustomerOrders.InvoicePhone;

                string strDeliveryAddress = lstCustomerOrders.DeliveryContactPerson + "<br>" + lstCustomerOrders.DeliveryCompany + "<br>" +
                     lstCustomerOrders.DeliveryAddress1 + "<br>" + lstCustomerOrders.DeliveryAddress2 + "<br>" + lstCustomerOrders.DeliveryCity + "<br>" +
                     lstCustomerOrders.DeliveryCounty + "<br>" + lstCustomerOrders.DeliveryPostalCode + "<br>" + lstCustomerOrders.DeliveryCountryName
                     + "<br>" + lstCustomerOrders.DeliveryPhone;

                strMailBody = strMailBody.Replace("@OrderNumber", Convert.ToString(intOrderId_OASIS));
                strMailBody = strMailBody.Replace("@BillingAddress", strBillingAddress);
                strMailBody = strMailBody.Replace("@ShippingAddress", strDeliveryAddress);
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "oc_udfemail").FeatureValues[0].IsEnabled)
                {
                    if (Session["UDFUSer"] != null)
                    {
                        string strFinalUDF = string.Empty;
                        string UDFSstring = Convert.ToString(Session["UDFUSer"]);
                        string[] splitUDFSstring = UDFSstring.Split(',');
                        if (splitUDFSstring.Length > 0)
                        {
                            for (int i = 0; i <= splitUDFSstring.Length - 1; i++)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(splitUDFSstring[i])))
                                {
                                    strFinalUDF = strFinalUDF + Convert.ToString(splitUDFSstring[i]) + "<br>";
                                }
                            }
                        }
                        strMailBody = strMailBody.Replace("@PaymentMethod", lstCustomerOrders.PaymentTypeRef + "<br>" + strFinalUDF);
                    }
                    else
                    {
                        strMailBody = strMailBody.Replace("@PaymentMethod", lstCustomerOrders.PaymentTypeRef);
                    }
                    Session["UDFUSer"] = null;
                }
                else
                {
                    strMailBody = strMailBody.Replace("@PaymentMethod", lstCustomerOrders.PaymentTypeRef);
                }
                strMailBody = strMailBody.Replace("@PaymentMethod", lstCustomerOrders.PaymentTypeRef);
                strMailBody = strMailBody.Replace("@DeliveryService", Convert.ToString(Session["strShipEmail"]));

                //if (rbStandard.Checked == true)
                //{
                //    strShippingMethodEmail = Convert.ToString(lstCustomerOrders.StandardCharges);
                //}
                //if (rbExpress.Checked == true)
                //{
                //    strShippingMethodEmail = Convert.ToString(lstCustomerOrders.ExpressCharges);
                //}

                string strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                {
                    strCurrencySymbol = string.Empty;
                }

                decimal dSubTotalPrice = 0;
                StringBuilder sbProductDetails = new StringBuilder();
                sbProductDetails.Append("</td></tr><tr>");
                for (int i = 0; i < lstCustomerOrders.CustomerOrderProducts.Count; i++)
                {
                    sbProductDetails.Append("<tr><td valign=\"top\" align=\"left\" width=\"100\" class=\"fontsize\">");
                    string strImageSrc = string.Empty;
                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + lstCustomerOrders.CustomerOrderProducts[i].DefaultImageName))
                        strImageSrc = strProductImagePath + lstCustomerOrders.CustomerOrderProducts[i].DefaultImageName;
                    else
                        strImageSrc = host + "Images/Products/default.jpg";

                    sbProductDetails.Append("<img src=\"" + strImageSrc + "\" width=\"60\" height=\"60\" style=\"display:block;\" border=\"0\" /></td>");
                    sbProductDetails.Append("<td valign=\"top\" align=\"left\" class=\"fontsize\">" + lstCustomerOrders.CustomerOrderProducts[i].SKU +
                        "<br>" + lstCustomerOrders.CustomerOrderProducts[i].ProductName + "</td>");

                    decimal dPrice = 0;
                    if (!string.IsNullOrEmpty(lstCustomerOrders.CouponCode) && (lstCustomerOrders.BehaviourType.ToLower() == "product" || lstCustomerOrders.BehaviourType.ToLower() == "basket"))
                    {
                        dPrice = Convert.ToDecimal(GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstCustomerOrders.CustomerOrderProducts[i].Price), lstCustomerOrders.DiscountPercentage));
                    }
                    else
                    {
                        dPrice = lstCustomerOrders.CustomerOrderProducts[i].Price;
                    }
                    //dPrice = Convert.ToDecimal(GlobalFunctions.ConvertToDecimalPrecision(dPrice));
                    dPrice = Convert.ToDecimal(dPrice.ToString("#######0.#0"));
                    sbProductDetails.Append("<td valign=\"top\" align=\"center\" width=\"100\" class=\"fontsize\">" + lstCustomerOrders.CustomerOrderProducts[i].Quantity + "</td>");

                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {

                        //  sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dPrice), "", GlobalFunctions.GetLanguageId()) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) + "</td>");
                        // sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dPrice * lstCustomerOrders.CustomerOrderProducts[i].Quantity), "", GlobalFunctions.GetLanguageId()) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) + "</td></tr>");
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dPrice), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()) /*+ GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId())*/ + "</td>");
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dPrice * lstCustomerOrders.CustomerOrderProducts[i].Quantity), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()) /*+ GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) */+ "</td></tr>");

                    }
                    else
                    {

                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dPrice), "", GlobalFunctions.GetLanguageId()) + "</td>");
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dPrice * lstCustomerOrders.CustomerOrderProducts[i].Quantity), "", GlobalFunctions.GetLanguageId()) + "</td></tr>");

                    }
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        dSubTotalPrice += Convert.ToDecimal(GlobalFunctions.RemovePointsSpanIntheText(GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dPrice * lstCustomerOrders.CustomerOrderProducts[i].Quantity), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId())));
                    }
                    else
                    {
                        dSubTotalPrice += Convert.ToDecimal(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(dPrice * lstCustomerOrders.CustomerOrderProducts[i].Quantity), "", GlobalFunctions.GetLanguageId()));
                    }
                }
                decimal dShippingCharges = Convert.ToDecimal(lstCustomerOrders.StandardCharges == 0 ? lstCustomerOrders.ExpressCharges : lstCustomerOrders.StandardCharges);
                
                decimal dTotalTax = Convert.ToDecimal(lstCustomerOrders.TotalTax);

                //dShippingCharges = Convert.ToDecimal(GlobalFunctions.ConvertToDecimalPrecision(dShippingCharges));
                //dTotalTax = Convert.ToDecimal(GlobalFunctions.ConvertToDecimalPrecision(dTotalTax));

                dShippingCharges = Convert.ToDecimal(dShippingCharges.ToString("#######0.#0"));
                dTotalTax = Convert.ToDecimal(dTotalTax.ToString("#######0.#0"));
                decimal handlingfee = Convert.ToDecimal(lstCustomerOrders.handlingfee.ToString("#######0.#0"));
                sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strSubTotal + "</td>");
                //sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + dSubTotalPrice + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) + "</td></tr>");
                if (!GlobalFunctions.IsPointsEnbled())
                {
                    sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + dSubTotalPrice + "</td></tr>");
                    sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strShipping + "</td>");
                    sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + dShippingCharges + "</td></tr>");
                    if (lstCustomerOrders.handlingfee != null && lstCustomerOrders.handlingfee != 0)
                    {
                        sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strAddtionalcharges + "</td>");
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + handlingfee + "</td></tr>");

                    }
                    sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strTax + "</td>");
                    sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + dTotalTax + "</td></tr>");
                }
                else
                {
                    //dShippingCharges = 0;
                    //dTotalTax = 0;
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + dSubTotalPrice + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) + "</td></tr>");
                        dShippingCharges = 0;
                        dTotalTax = 0;
                        sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strShipping + "</td>");
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dShippingCharges), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()) + "</td></tr>");
                        sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strTax + "</td>");
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dTotalTax), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()) + "</td></tr>");
                    }
                    else
                    {
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + dSubTotalPrice + "</td></tr>");
                        sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strShipping + "</td>");
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + dShippingCharges + "</td></tr>");
                        if (lstCustomerOrders.handlingfee != null && lstCustomerOrders.handlingfee != 0)
                        {
                            sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strAddtionalcharges + "</td>");
                            sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + handlingfee + "</td></tr>");

                        }
                        sbProductDetails.Append("<tr><td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strTax + "</td>");
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + dTotalTax + "</td></tr>");
                    }
                }
                sbProductDetails.Append("<tr><td height=\"10\"></td></tr><tr>");
                sbProductDetails.Append("<td valign=\"top\" align=\"right\" colspan=\"4\" width=\"100\" class=\"fontsize\">" + strTotal + "</td>");
                if (!GlobalFunctions.IsPointsEnbled())
                {
                    sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + (dSubTotalPrice + dShippingCharges + dTotalTax));
                }
                else
                {
                    //dSubTotalPrice + dShippingCharges + dTotalTax;
                    if (GlobalFunctions.IsPointsEnbled() && Convert.ToString(Session["ispoint"]) == "1")
                    {
                        string dSh = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dShippingCharges), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                        string ddTotalTax = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(dTotalTax), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId()).Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
                        ddTotalTax = GlobalFunctions.RemovePointsSpanIntheText(ddTotalTax);
                        dSh = GlobalFunctions.RemovePointsSpanIntheText(dSh);
                        sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + (dSubTotalPrice + Convert.ToDecimal(dSh) + Convert.ToDecimal(ddTotalTax)) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()));
                    }
                    else { sbProductDetails.Append("<td valign=\"top\" align=\"right\" width=\"100\" class=\"fontsize\">" + strCurrencySymbol + (dSubTotalPrice + dShippingCharges + dTotalTax)); }
                }
                strMailBody = strMailBody.Replace("@ProductDetails", sbProductDetails.ToString());

                //Get logo
                string strSiteLogoDir = string.Empty;
                DirectoryInfo dirSiteLogo = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Images/SiteLogo"));
                FileInfo[] logoFileInfo = dirSiteLogo.GetFiles();

                foreach (FileInfo logoFile in logoFileInfo)
                {
                    if (logoFile.Name.ToLower().Contains("sitelogo"))
                    {
                        strSiteLogoDir = "<a href='" + host + "index'><img height='80' src='" + host + "Images/SiteLogo/" + logoFile.Name + "'></a>";
                    }
                }
                strMailBody = strMailBody.Replace("@sitelogo", strSiteLogoDir);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return strMailBody;
            }
            return strMailBody;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-09-15
        /// Scope   : setting query string values
        /// </summary>
        private void GetQueryString()
        {
            try
            {
                // Check for encrypted query string
                orderId = Request.QueryString["orderid"];
                storeId = Request.QueryString["storeid"];
                txRefGUID = Request.QueryString["TxRefGUID"];
                string encryptedQueryString = Request.QueryString["request"];
                if (string.IsNullOrEmpty(encryptedQueryString))
                    encryptedQueryString = Convert.ToString(Page.RouteData.Values["request"]);
                string decryptedQueryString = string.Empty;
                string cryptoKey = string.Empty;
                if (!string.IsNullOrEmpty(encryptedQueryString))
                {
                    // Decrypt query strings
                    cryptoKey = System.Web.Configuration.WebConfigurationManager.AppSettings["CryptoKey"];
                    decryptedQueryString = GlobalFunctions.DecryptQueryStrings(encryptedQueryString, cryptoKey);
                }
                string[] strArr = decryptedQueryString.Split('&');
                string[] KeyValue = null;
                for (int iArr = 0; iArr < strArr.Length; iArr++)
                {
                    KeyValue = strArr[iArr].Split('=');
                    if (strArr[iArr].IndexOf('=') > 0)
                    {
                        if (Convert.ToString(KeyValue[0]).ToUpper() == "ORDERID")
                            orderId = Convert.ToString(KeyValue[1]);

                        if (Convert.ToString(KeyValue[0]).ToUpper() == "STOREID")
                            storeId = Convert.ToString(KeyValue[1]);

                        if (Convert.ToString(KeyValue[0]).ToUpper() == "TXREFGUID")
                            txRefGUID = Convert.ToString(KeyValue[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public bool Order_Webservice(ref int Oasis_orderid, UserBE lstUser, CustomerOrderBE lstCustomerOrderBE, string strTxRefGUID, string strCardNumber,
            bool lbIsAdflexOrder, string AuthorisationCode, string CardToken, string MerchantId, string CustomerRef)
        {
            bool IsOrderPlaced = false;
            try
            {
                //Product Details 
                #region AddProductDetailsToArray
                ArrayList arrProducts = new ArrayList();
                double intGoodsTotlal = 0;
                DataTable dtTax = null;
                double unitprice = 0;
                string strCatalogueId = string.Empty;
                string TaxNumber = string.Empty;
                string VendorTaxCode = string.Empty;
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    strCatalogueId = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CatalogueId);
                }

                for (int intCart = 0; intCart < lstCustomerOrderBE.CustomerOrderProducts.Count; intCart++)
                {
                    #region Commented by Sripal
                    //if (Session["Amount"] != null)
                    //{ arrProducts.Add("990"); }
                    //else
                    //{ arrProducts.Add(strCatalogueId); } 
                    #endregion
                    arrProducts.Add(strCatalogueId);
                    if (lstCustomerOrderBE.CustomerOrderProducts[intCart].SKU != "")
                        arrProducts.Add(lstCustomerOrderBE.CustomerOrderProducts[intCart].SKU);
                    else
                        arrProducts.Add(lstCustomerOrderBE.CustomerOrderProducts[intCart].CatalogueAlias);


                    if (!string.IsNullOrEmpty(lstCustomerOrderBE.CouponCode) && (lstCustomerOrderBE.BehaviourType.ToLower() == "product" || lstCustomerOrderBE.BehaviourType.ToLower() == "basket"))
                        unitprice = GlobalFunctions.DiscountedAmount(Convert.ToDouble(lstCustomerOrderBE.CustomerOrderProducts[intCart].Price), lstCustomerOrderBE.DiscountPercentage);
                    else
                        unitprice = double.Parse(Convert.ToString(lstCustomerOrderBE.CustomerOrderProducts[intCart].Price));

                    arrProducts.Add(unitprice.ToString("#####0.###0", CultureInfo.InvariantCulture));
                    arrProducts.Add(lstCustomerOrderBE.CustomerOrderProducts[intCart].Quantity);
                    arrProducts.Add(lstCustomerOrderBE.CustomerOrderProducts[intCart].BASYSProductId);
                    intGoodsTotlal = intGoodsTotlal + Math.Round((Convert.ToInt32(lstCustomerOrderBE.CustomerOrderProducts[intCart].Quantity) * unitprice), 2, MidpointRounding.AwayFromZero);
                    arrProducts.Add(lstCustomerOrderBE.CustomerOrderProducts[intCart].BaseColorId);
                    arrProducts.Add(lstCustomerOrderBE.CustomerOrderProducts[intCart].TrimColorId);
                    arrProducts.Add(lstCustomerOrderBE.CustomerOrderProducts[intCart].SupplierId);
                    dtTax = Session["TaxDetails"] as DataTable;
                    if (dtTax != null)
                    {
                      // TaxNumber = Convert.ToString(dtTax.Rows[0]["TaxNumber"]);
                        VendorTaxCode = Convert.ToString(dtTax.Rows[0]["VendorTaxCode"]);

                        DataRow[] tempRow = dtTax.Select("ProductID='" + lstCustomerOrderBE.CustomerOrderProducts[intCart].BASYSProductId + "'");
                        if (tempRow.Length > 0)
                            arrProducts.Add(tempRow[0]["VatInvoiceText"]);
                        else
                            arrProducts.Add(string.Empty);
                    }
                    else
                    {
                        arrProducts.Add(string.Empty);
                    }
                    arrProducts.Add(lstCustomerOrderBE.CustomerOrderProducts[intCart].UserInputText);		
	
                }
                #endregion

                #region freightdetails Commented
                //if (loHashtable["FreightDetails"] != null)
                //    dttblFreightDetails = (DataTable)loHashtable["FreightDetails"];

                //string strServiceID = string.Empty;
                //string fstrDeliveryMethod = "";
                //DataRow[] drValues = null;

                //if (dttblFreightDetails != null && dttblFreightDetails.Rows.Count > 0)
                //{
                //    if (loHashtable["DeliveryMethod"] != null)
                //    {
                //        fstrDeliveryMethod = Convert.ToString(loHashtable["DeliveryMethod"]).Remove(Convert.ToString(loHashtable["DeliveryMethod"]).IndexOf("("));

                //        drValues = dttblFreightDetails.Select("carrier_service_text='" + fstrDeliveryMethod + "'", "");

                //        foreach (DataRow dr in drValues)
                //            strServiceID = Convert.ToString(dr["carrier_service_id"]);

                //        DataColumn dtCarrierID = new DataColumn("carrier_service_id");
                //        DataColumn dtCarrierText = new DataColumn("carrier_service_text");
                //        dttblCustomerOrderSplitDetails.Columns.Add(dtCarrierID);
                //        dttblCustomerOrderSplitDetails.Columns.Add(dtCarrierText);
                //        for (int intShip = 0; intShip < dttblCustomerOrderSplitDetails.Rows.Count; intShip++)
                //        {
                //            dttblCustomerOrderSplitDetails.Rows[intShip]["carrier_service_id"] = strServiceID;
                //            dttblCustomerOrderSplitDetails.Rows[intShip]["carrier_service_text"] = Convert.ToString(loHashtable["DeliveryMethod"]);
                //        }
                //    }
                //}
                #endregion

                ValidateWebServices_OASIS validWebservice = new ValidateWebServices_OASIS();
                string strGift = "";
                if (Session["Amount"] != null)
                {
                    strGift = Convert.ToString(Session["sbGift"]);
                }

                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                {

                    List<InvoiceBE> lstSelectedEntityDetails = new List<InvoiceBE>();
                    lstSelectedEntityDetails = HttpContext.Current.Session["SelectedEntityDetails"] as List<InvoiceBE>;

                    if (lstSelectedEntityDetails != null)
                    {
                        if (lstSelectedEntityDetails[0].Tax_Number != null)
                        {
                            TaxNumber = lstSelectedEntityDetails[0].Tax_Number;
                        }
                        else
                        {
                            TaxNumber = string.Empty;
                        }
                    }
                    else
                    {
                        if (Session["TaxNumberValue"] != null)
                        {
                            TaxNumber = Convert.ToString(Session["TaxNumberValue"]) + "User Input";
                        }
                        else
                        {
                            TaxNumber = string.Empty;
                        }
                    }
                }
                else
                {
                    TaxNumber = string.Empty;
                }

                IsOrderPlaced = validWebservice.ValidateOrderDetails(intGoodsTotlal.ToString("#######0.#0", CultureInfo.InvariantCulture),
                    arrProducts, ref Oasis_orderid, TaxNumber, VendorTaxCode, lstCustomerOrderBE, lstUser, strTxRefGUID, strCardNumber,
                    lbIsAdflexOrder, AuthorisationCode, strGift, CardToken, MerchantId, CustomerRef);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsOrderPlaced;
        }

        private int GenerateGiftCertificateXML()
        {
            int intOrderID = 0;
            try
            {
                StoreBE lstStoreDetail = new StoreBE();
                #region Gift Entity
                GiftCertificateOrderXMLEntity objGiftCertificateOrderXMLEntity = new GiftCertificateOrderXMLEntity();
                lstStoreDetail = StoreBL.GetStoreDetails();
                //dictGiftOrderDetailsLst.Sum()
                //dictGiftOrderDetailsLst.ToDictionary(k => k.Key, v => v.Value.Sum())
                double dAmount = 0;
                foreach (Dictionary<string, string> str in dictGiftOrderDetailsLst)
                {
                    dAmount += Convert.ToInt32(str["Quantity"]) * Convert.ToDouble(str["Amount"]);
                }
                objGiftCertificateOrderXMLEntity.CustomerContactId = lst.BASYS_CustomerContactId;// "960963";// "973830";// lst.BASYS_CustomerContactId;
                //List<StoreBE.StoreCurrencyBE> Storecurrency = lstStoreDetail.StoreCurrencies.FindAll(x => x.IsActive == true && x.CurrencyId == GlobalFunctions.GetCurrencyId());
                objGiftCertificateOrderXMLEntity.DivisionId = Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId); //248;
                objGiftCertificateOrderXMLEntity.SkipPipeline = "Y"; //objGiftCertificateOrderXMLEntity.SkipPipeline = "N";
                objGiftCertificateOrderXMLEntity.OrderCurrency = Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode);
                objGiftCertificateOrderXMLEntity.PaymentTypeId = 4;
                objGiftCertificateOrderXMLEntity.TransactionRef = objAdflexTrans.stTxRefGUID;// "c1ba8129-5826-40f5-84e8-d96d1473bba1";// Guid.NewGuid().ToString();
                objGiftCertificateOrderXMLEntity.Last4Digits = objAdflexTrans.stCardNumber;// "4463";
                objGiftCertificateOrderXMLEntity.CollectedAmount = Convert.ToDecimal(dAmount);
                objGiftCertificateOrderXMLEntity.CollectedDate = DateTime.Now;
                objGiftCertificateOrderXMLEntity.CollectedFlag = "Y";
                objGiftCertificateOrderXMLEntity.GoodsTotal = Convert.ToDecimal(dAmount);
                objGiftCertificateOrderXMLEntity.CustomerRef = "";
                objGiftCertificateOrderXMLEntity.OrderNotes = "";
                objGiftCertificateOrderXMLEntity.SourceCode = Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeName); //"GGLMRKEURWEB";// "GGLMRKGBPWEB";// "GGLRDMPWEB";// "GGLRETWEB";//GGLRDMPWEB "WEBZ";// Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeName);
                objGiftCertificateOrderXMLEntity.SourceCodeId = Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeId); //"7778";// "7779";// "7977";// "7772";// "5281";// Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeId);
                objGiftCertificateOrderXMLEntity.cardtoken = Convert.ToString(objAdflexTrans.ReqCardToken);
                objGiftCertificateOrderXMLEntity.merchantID = Convert.ToString(objAdflexTrans.MID);


                #endregion
                #region Generate XML
                #region OrderHeader

                XElement xOrderHeaderElement = null;

                #region Offline
                //switch (objGiftCertificateOrderXMLEntity.cardType)
                //{
                //    case CardType.PurchaseOrder:
                //        xOrderHeaderElement = new XElement("OrderHeader", new XElement("DivisionID", objOrderXMLEntity.DivisionId)
                //                                                               , new XElement("KeyGroupID", objOrderXMLEntity.KeyGroupId)
                //                                                               , new XElement("CustomerID", objOrderXMLEntity.CustomerId)
                //                                                               , new XElement("CustomerContactID", objOrderXMLEntity.CustomerContactId)
                //                                                               , new XElement("PaymentTypeID", objOrderXMLEntity.PaymentTypeId)
                //                                                               , new XElement("AuthorisationCode", objOrderXMLEntity.AuthorisationCode)
                //                                                               , new XElement("SourceCode", objOrderXMLEntity.SourceCodeName)
                //                                                               , new XElement("SourceCodeID", objOrderXMLEntity.SourceCodeId)
                //                                                               , new XElement("OrderCurrency", objOrderXMLEntity.OrderCurrency)
                //                                                               , new XElement("GoodsTotal", objOrderXMLEntity.GoodsTotal)
                //                                                               , new XElement("Freight", objOrderXMLEntity.Fright)
                //                                                               , new XElement("Tax", objOrderXMLEntity.Tax)
                //                                                               , new XElement("CustomerRef", objOrderXMLEntity.CustomerRef)
                //                                                               , new XElement("OrderNotes", objOrderXMLEntity.OrderNotes)
                //                                                               , new XElement("TaxNumber", objOrderXMLEntity.dtTax.Rows[0]["TaxNumber"])
                //                                                               , new XElement("VendorTaxCode", objOrderXMLEntity.dtTax.Rows[0]["VendorTaxCode"]), new XAttribute("powerWeaveID", string.Empty)
                //                                                                                                                                                       , new XAttribute("orderDate", string.Empty));
                //        break;

                //case CardType.Card: 
                #endregion
                xOrderHeaderElement = new XElement("OrderHeader", new XElement("CustomerContactID", objGiftCertificateOrderXMLEntity.CustomerContactId)
                                                                        , new XElement("DivisionID", objGiftCertificateOrderXMLEntity.DivisionId)
                                                                       , new XElement("SkipPipeline", objGiftCertificateOrderXMLEntity.SkipPipeline)
                                                                       , new XElement("OrderCurrency", objGiftCertificateOrderXMLEntity.OrderCurrency)
                                                                       , new XElement("PaymentTypeID", objGiftCertificateOrderXMLEntity.PaymentTypeId)
                                                                       , new XElement("TransactionRef", objGiftCertificateOrderXMLEntity.TransactionRef)
                                                                       , new XElement("Last4Digits", objGiftCertificateOrderXMLEntity.Last4Digits)
                                                                       , new XElement("CollectedDate", DateTime.Now.ToString("yyyy-MM-dd"))
                                                                       , new XElement("CollectedAmount", objGiftCertificateOrderXMLEntity.CollectedAmount)
                                                                       , new XElement("CollectedFlag", objGiftCertificateOrderXMLEntity.CollectedFlag)
                                                                       , new XElement("CardToken", objGiftCertificateOrderXMLEntity.cardtoken)
                                                                       , new XElement("MerchantID", objGiftCertificateOrderXMLEntity.merchantID)
                                                                       , new XElement("GoodsTotal", objGiftCertificateOrderXMLEntity.GoodsTotal)
                                                                       , new XElement("CustomerRef", objGiftCertificateOrderXMLEntity.CustomerRef)
                                                                       , new XElement("OrderNotes", objGiftCertificateOrderXMLEntity.OrderNotes)
                                                                       , new XElement("SourceCode", objGiftCertificateOrderXMLEntity.SourceCode)
                                                                       , new XElement("SourceCodeID", objGiftCertificateOrderXMLEntity.SourceCodeId));
                #endregion
                #region ShipTo
                UserRegistrationBE objUserRegistrationBE = new UserRegistrationBE();
                objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                string strCountryCode = objUserRegistrationBE.CountryLst.FirstOrDefault(x => x.CountryId == Convert.ToInt16(dictGiftOrder["PreDefinedColumn8"])).CountryCode;
                string strCountryName = objUserRegistrationBE.CountryLst.FirstOrDefault(x => x.CountryId == Convert.ToInt16(dictGiftOrder["PreDefinedColumn8"])).CountryName;
                XElement xBillToElement = new XElement("BillTo", new XElement("ContactName", dictGiftOrder["PreDefinedColumn1"])
                                                          , new XElement("Address1", dictGiftOrder["PreDefinedColumn3"])
                                                          , new XElement("Address2", dictGiftOrder["PreDefinedColumn4"])
                                                          , new XElement("Address3", string.Empty)
                                                          , new XElement("City", dictGiftOrder["PreDefinedColumn5"])
                                                          , new XElement("PostCode", dictGiftOrder["PreDefinedColumn7"])
                                                          , new XElement("County", dictGiftOrder["PreDefinedColumn6"])
                                                          , new XElement("Country", strCountryName, new XAttribute("CountryCode", strCountryCode))
                                                          , new XElement("Telephone", dictGiftOrder["PreDefinedColumn9"]));
                #endregion
                #region OrderLines
                XElement xOrderLineElement = new XElement("OrderLines");
                //List<GiftCertificateOrderXMLEntity.OrderLineGift> OrderLineGiftLst = new List<GiftCertificateOrderXMLEntity.OrderLineGift>();
                int i = 1;
                //foreach (RepeaterItem item in rptCertificate.Items)
                //foreach (KeyValuePair<string, string> str in dictGiftOrderDetailsLst)
                foreach (Dictionary<string, string> str in dictGiftOrderDetailsLst)
                {
                    //GiftCertificateOrderXMLEntity.OrderLineGift objOrderLine = new GiftCertificateOrderXMLEntity.OrderLineGift();
                    //TextBox txtrptAmount = (TextBox)item.FindControl("txtrptAmount");
                    //TextBox txtrptQuantity = (TextBox)item.FindControl("txtrptQuantity");
                    //TextBox txtrptRecipientEmail = (TextBox)item.FindControl("txtrptRecipientEmail");
                    //TextBox txtrptPersonalMessage = (TextBox)item.FindControl("txtrptPersonalMessage");

                    //objOrderLine.LineQty = Convert.ToInt32(txtrptQuantity.Text);
                    //objOrderLine.LineValue = Convert.ToDecimal(txtrptAmount.Text);
                    //objOrderLine.LineMessage = txtrptPersonalMessage.Text;
                    //objOrderLine.RecipientEmail = txtrptRecipientEmail.Text;

                    //objOrderLineGiftLst.Add(objOrderLine);
                    XElement productElement = new XElement("OrderLine", new XAttribute("LineNumber", Convert.ToString(i))
                                                                         , new XElement("LineQty", Convert.ToInt32(str["Quantity"]))//txtrptQuantity.Text))
                                                                        , new XElement("LineValue", Convert.ToDecimal(str["Amount"]))
                                                                        , new XElement("LineMessage", str["PersonalMessage"])//txtrptPersonalMessage.Text)
                                                                        , new XElement("RecipientEmail", str["RecipientEmail"]));// txtrptRecipientEmail.Text));
                    xOrderLineElement.Add(productElement);
                    i++;
                }
                #endregion
                #region OrderXML Node
                xOrderHeaderElement.Add(xBillToElement);
                XElement xOrderElement;

                xOrderElement = new XElement("GiftOrder", xOrderHeaderElement, xOrderLineElement);
                XDocument xOrderXML = new XDocument(xOrderElement);
                XmlNode OrderXMLNode = new XmlDocument().ReadNode(xOrderXML.Root.CreateReader());
                #endregion
                #region Make Order

                try
                {
                    getXmlFileGiftCertificate(Convert.ToString(OrderXMLNode.OuterXml), intOrderID.ToString());

                    //if (!lst.EmailId.ToLower().Contains("@powerweave.com"))
                    //{
                    if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
                    {
                        PWGlobalEcomm.OrderLive.Order order = new PWGlobalEcomm.OrderLive.Order();
                        NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                        order.Credentials = objNetworkCredentials;
                        intOrderID = order.CreateGiftOrder(OrderXMLNode);
                    }
                    else
                    {
                        PWGlobalEcomm.Order.Order order = new PWGlobalEcomm.Order.Order();
                        NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                        order.Credentials = objNetworkCredentials;
                        intOrderID = order.CreateGiftOrder(OrderXMLNode);
                    }

                    //NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    //order.Credentials = objNetworkCredentials;
                    //intOrderID = order.CreateGiftOrder(OrderXMLNode);
                    //}
                    //else
                    //{
                    //    intOrderID = 1;
                    //}
                }
                catch (Exception ex)
                {
                    UserBE lstUserBE;
                    lstUserBE = HttpContext.Current.Session["User"] as UserBE;
                    GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In getXmlFileGiftCertificate", ex + "USER ID- " + lstUserBE.UserId + "USER EMAIL- " + lstUserBE.EmailId, "", "", false);
                    Exceptions.WriteExceptionLog(ex);
                }
                #endregion
                #endregion
            }
            catch (Exception ex) 
            {
                UserBE lstUserBE;
                lstUserBE = HttpContext.Current.Session["User"] as UserBE;
                GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In GenerateGiftCertificateXML", ex + "USER ID- " + lstUserBE.UserId + "USER EMAIL- " + lstUserBE.EmailId, "", "", false);
                Exceptions.WriteExceptionLog(ex);
            }
            return intOrderID;
        }

        public void getXmlFileGiftCertificate(string strxml, string lstrOrderId)
        {
            try
            {
                if (!Directory.Exists(GlobalFunctions.GetPhysicalFolderPath() + "XML\\GiftCertificateXMLs"))
                {
                    Directory.CreateDirectory(GlobalFunctions.GetPhysicalFolderPath() + "XML\\GiftCertificateXMLs");
                }

                TextWriter txtwr = new StreamWriter(GlobalFunctions.GetPhysicalFolderPath() + "XML\\GiftCertificateXMLs\\Orders.txt", true);
                txtwr.WriteLine("\r\n\r\n");
                txtwr.WriteLine("****************************Order Information - GiftOASISID Id -- " + lstrOrderId + " **********Start*************** " + DateTime.Now.ToString() + "************\r\n\r\n");
                txtwr.WriteLine(strxml);
                txtwr.WriteLine("****************************Order Information**********End***************************\r\n\r\n");
                txtwr.Close();
            }

            catch (Exception ex)
            {
            }
        }
        protected void SendOrderEmailGiftCertificate(Int32 intOrderId_OASIS)
        {
            try
            {
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = "Order Confirmation";
                objEmail.LanguageId = GlobalFunctions.GetLanguageId();
                objEmail.EmailTemplateId = 0;

                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    string strFromEmailId = lstEmail[0].FromEmailId;
                    string strCCEmailId = lstEmail[0].CCEmailId;
                    string strBCCEmailId = lstEmail[0].BCCEmailId;
                    bool IsHtml = lstEmail[0].IsHtml;
                    string strSubject = lstEmail[0].Subject;
                    string strMailBody = "Your OrderID : " + intOrderId_OASIS;// GenerateMailBody(lstCustomerOrder, lstEmail[0].Body, intOrderId_OASIS);

                    //Response.Clear();
                    //Response.Write(strMailBody);
                    //Response.End();
                    GlobalFunctions.SendEmail(lst.EmailId, strFromEmailId, "", strCCEmailId, strBCCEmailId, strSubject, strMailBody, "", "", IsHtml);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}