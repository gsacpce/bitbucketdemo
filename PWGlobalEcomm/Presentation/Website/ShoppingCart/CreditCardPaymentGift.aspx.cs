﻿using AHPP.ASP.Net.Lib;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;

namespace Presentation
{
    public partial class ShoppingCart_CreditCardPaymentGift : System.Web.UI.Page
    {
        protected global::System.Web.UI.HtmlControls.HtmlIframe TransFrame;

        Int16 intCurrencyId = 0, intLanguageId = 0, intUserId = 0;
        public string host = GlobalFunctions.GetVirtualPath();

        Dictionary<string, string> dictGiftOrder = new Dictionary<string, string>();
        List<Dictionary<string, string>> dictGiftOrderDetailsLst = new List<Dictionary<string, string>>();
        UserBE lst;
        string strGUID = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["User"] != null)
                {
                    lst = new UserBE();
                    lst = Session["User"] as UserBE;
                    intUserId = lst.UserId;
                }
                else
                {
                    lst = new UserBE();
                    lst.EmailId = Convert.ToString(Session["GuestUser"]);
                    intUserId =0;
                    //Response.RedirectToRoute("login-page");
                    //return;
                }

                if (Session["dictGiftOrder"] == null && Session["dictGiftOrderDetails"] == null)
                {
                    Response.RedirectToRoute("index");
                    return;
                }
                else
                {
                    dictGiftOrder = (Dictionary<string, string>)Session["dictGiftOrder"];
                    dictGiftOrderDetailsLst = (List<Dictionary<string, string>>)Session["dictGiftOrderDetails"];
                }
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                intLanguageId = GlobalFunctions.GetLanguageId();

                Message transaction = new Message("3.3.2");
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                ///CustomerOrderBE lstCustomerOrders = new CustomerOrderBE();
                //CustomerOrderBE objCustomerOrderBE = new CustomerOrderBE();
                //objCustomerOrderBE.OrderedBy = lst.UserId;
                //objCustomerOrderBE.LanguageId = intLanguageId;
                ////lstCustomerOrders = ShoppingCartBL.CustomerOrder_SAE(objCustomerOrderBE);
                //if (lstCustomerOrders == null)
                //{
                //    Response.RedirectToRoute("index");
                //    return;
                //}

                strGUID = Funcs.CreateTxRefGUID();
                //transaction.TransactionType = 4;
                transaction.TransactionType = 1;
                transaction.Language = objStoreBE.StoreLanguages.FirstOrDefault(x => x.LanguageId == GlobalFunctions.GetLanguageId()).CultureCode;
                transaction.CurrencyCode = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).AdflexCurrencyCode;
                //transaction.Amount = Convert.ToString(CalculateTotalAmount(lstCustomerOrders.StandardCharges == 0 ? lstCustomerOrders.ExpressCharges : lstCustomerOrders.StandardCharges, lstCustomerOrders.TotalTax, lstCustomerOrders));
                transaction.Amount = Convert.ToString(Session["dTotlaGoods"]);
                transaction.TransactionRef = "P" + Convert.ToString(Guid.NewGuid()).Replace("-", "").Substring(0, 11);
                transaction.Version = Convert.ToString(ConfigurationManager.AppSettings["Version"]);
                transaction.AuthenticationToken = Convert.ToString(ConfigurationManager.AppSettings["PSPToken"]);
                transaction.MessageType = (int)MsgType.MerchantSubmitTransaction;
                transaction.TransactionFlags = 0;
                transaction.TransactionDescription = "Goods Sold";
                transaction.NotificationURLID = 4;
                transaction.SupportedCardsURLID = 0;
                transaction.CardHolderName = lst.FirstName.Trim() + " " + lst.LastName.Trim();
                transaction.TxRefGUID = strGUID;
                transaction.CardHolderAddress1 = dictGiftOrder["PreDefinedColumn3"];
                transaction.CardHolderAddress2 = dictGiftOrder["PreDefinedColumn4"];
                transaction.CardHolderCity = dictGiftOrder["PreDefinedColumn5"];
                transaction.CardHolderStateCounty = dictGiftOrder["PreDefinedColumn6"];
                transaction.CardHolderPostcode = dictGiftOrder["PreDefinedColumn7"];
                transaction.CustomerPhone = dictGiftOrder["PreDefinedColumn9"];

                string lstrOrderURL = host + "ShoppingCart/OrderConfirmation.aspx?storeid=" + objStoreBE.StoreId + "&orderid=" + 1 + "&TxRefGUID=" + strGUID;
                transaction.UserData1 = lstrOrderURL.Replace("/", ",").Replace("?", "@").Replace("&", "_").Replace("=", "(");
                transaction.CustomerEmail = lst.EmailId.Trim();
                Session["transaction"] = transaction;
                TransFrame.Visible = true;

                this.Page.Title = "Payment";
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

      
    }
}