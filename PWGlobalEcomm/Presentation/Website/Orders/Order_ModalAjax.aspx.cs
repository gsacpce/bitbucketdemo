﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Orders_Order_ModalAjax : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Label lblMDespatchNumber, lblShippingDate, lblShippingMethod, lblTrackingNumber, lblMName, lblMCompany, lblMAddress, lblMCity, lblMPostCode, lblMCountry;
        protected global::System.Web.UI.WebControls.Repeater rptModalItem;
        CustomerSalesOrder obj;
        Dictionary<string, Int32> dictDispatches = new Dictionary<string, int>();

        public string Line, Item_Code, Item_Description, Qty_Ordered, Generic_Quantity_Despatch, Backorder, Despatch_Number, Shipping_Date, Tracking_Details, Shipping_Method, Tracking_Numbers, Delivery_Address, Generic_Item_in_Despatch;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string trackingId = Convert.ToString(Request.Form["TrackingId"]);
                    string strDelveryAddressId = "";
                    obj = new CustomerSalesOrder();
                    obj = (CustomerSalesOrder)Session["ObjOrderItem"];
                    if (obj.orderheaders != null)
                    {
                        var dispatch = obj.dispatches.despatch.FirstOrDefault(x => x.ConsignmentNumbers == trackingId);
                        if (dispatch != null)
                        {
                            lblMDespatchNumber.Text = dispatch.DespatchID;
                            lblShippingDate.Text = dispatch.DespatchDate.ToString("dd MMMM yyy");
                            lblShippingMethod.Text = dispatch.ShippingMethod;
                            lblTrackingNumber.Text = "<a href='" + dispatch.TrackingURL + "?brand=DHL&AWB=" + dispatch.ConsignmentNumbers + " ' target='_blank'>" + dispatch.ConsignmentNumbers + "</a>";
                            strDelveryAddressId = dispatch.DelAddressID;
                        }
                        var dAddress = obj.deliveryaddresses.deliveryaddress.FirstOrDefault(x => x.DelAddressID == strDelveryAddressId);
                        if (dAddress != null)
                        {
                            lblMName.Text = dAddress.ContactName;
                            lblMCompany.Text = dAddress.CompanyName;
                            lblMAddress.Text = dAddress.Address1;
                            lblMCity.Text = dAddress.City;
                            lblMPostCode.Text = dAddress.PostCode;
                            lblMCountry.Text = dAddress.Country;
                        }
                        DespatchLine[] d = dispatch.despatchlines.despatchline;
                        List<OrderLine> oLines = new List<OrderLine>();
                        foreach (var item in d)
                        {
                            oLines.Add(obj.orderlines.orderline.FirstOrDefault(x => x.OrderLineID == item.OrderLineID));
                        }



                        foreach (DespatchLine item in d)
                        {
                            int DeliveredQty = item.DeliveredQuantity;
                            string OrderLine = item.OrderLineID;
                            dictDispatches.Add(OrderLine, DeliveredQty);
                        }

                        rptModalItem.DataSource = oLines;
                        rptModalItem.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            BindResourceData();
        }
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId().To_Int16());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        Tracking_Details = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Tracking_Details").ResourceValue;
                        Despatch_Number = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Despatch_Number").ResourceValue;
                        Shipping_Date = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Shipping_Date").ResourceValue;
                        Shipping_Method = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SC_Shipping_Method").ResourceValue;
                        Tracking_Numbers = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Tracking_Number").ResourceValue;
                        Delivery_Address = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DeliveryAddress").ResourceValue;
                        Generic_Item_in_Despatch = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Item_in_Despatch").ResourceValue;
                        Line = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Line").ResourceValue;
                        Item_Code = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Item_Code").ResourceValue;
                        Item_Description = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Item_Description").ResourceValue;
                        Backorder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_Item_Backorder_Text").ResourceValue;
                        Qty_Ordered = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Ordered").ResourceValue;
                        Generic_Quantity_Despatch = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Despatch").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void rptModalItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblShipped = e.Item.FindControl("lblShipped") as Label;
                    Label lblQuantity = e.Item.FindControl("lblQuantity") as Label;
                    Label lblBackOrder = e.Item.FindControl("lblBackOrder") as Label;
                    OrderLine ol = (OrderLine)e.Item.DataItem;
                    //Despatch despatch = obj.dispatches.despatch.FirstOrDefault(x => x.despatchlines.despatchline.FirstOrDefault(y => y.OrderLineID == ol.OrderLineID) != null);
                    if (obj.dispatches.despatch != null)
                    {
                        string OrderLineID = ol.OrderLineID;
                        //DespatchLine despatchLine = obj.dispatches.despatch.Where(x => x.despatchlines.despatchline != null).SelectMany(x => x.despatchlines.despatchline).ToList().FirstOrDefault(y => y.OrderLineID == ol.OrderLineID);
                        //lblShipped.Text = despatchLine != null ? Convert.ToString(despatchLine.DeliveredQuantity) : "-";
                        lblShipped.Text = Convert.ToString(dictDispatches[OrderLineID]);
                    }
                    else
                        lblShipped.Text = "-";
                    //lblShipped.Text = Convert.ToString(Convert.ToInt32(lblQuantity.Text) - Convert.ToInt32(lblBackOrder.Text));
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }
}