﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Orders_OrderItem : BasePage
    {
        string SalesOrderID = "";
        static int CatalogueId;
        protected global::System.Web.UI.WebControls.Button btnNext, btnPrevious;
        protected global::System.Web.UI.WebControls.Literal ltrDelAddress, ltrHeader;
        protected global::System.Web.UI.WebControls.Label lblPaymentMethod, lblCustomerRef, lblRAddress, Label1, Label2, Label3, Label4, lblCurrentId, lblRCountry, lblDName, lblRPostCode, lblDCompany, lblDAddress, lblDCity, lblRCity, lblDPostCode, lblDCountry, lblRName, lblRCompany;
        protected global::System.Web.UI.WebControls.Repeater rptProductItem, rptTrackingNumber;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvTax, spnBackToMyAcc, divMyAccount;
        public string Line, Item_Code, Item_Description, Quantity, Shipped, Backorder, Unit_Price, Payment_Method, Line_total, Order_details, Action, Delivery_Address, Order_Number, Invoice_Address, Tracking_Numbers, Reference, Subtotal, Carriage, Tax, Total, strbtnBuyIt , strMultipleDeliveryAddress;
        CustomerSalesOrder obj;
        public string strCurrencySymbol = string.Empty;
        public string strCurrencySymbolPoints = string.Empty;
        Dictionary<string, Int32> dictDispatches = new Dictionary<string, int>();
        Int16 NoofDeliveryAddress = 0;
        Int16 intLanguageId = 0;
        bool isPointEnabled = false;
        public string host = GlobalFunctions.GetVirtualPath();
        public string BudgetUrl = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.UrlReferrer != null)
                {
                    //string previousPageUrl = Request.UrlReferrer.AbsoluteUri;
                    Exceptions.WriteInfoLog("IF - Request.UrlReferrer" + Request.UrlReferrer);

                    BudgetUrl = System.IO.Path.GetFileName(Request.UrlReferrer.AbsolutePath).ToString();

                    Exceptions.WriteInfoLog("BudgetUrl - " + BudgetUrl);
                }
                else
                {
                    Exceptions.WriteInfoLog("ELSE - Request.UrlReferrer - " + Request.UrlReferrer);
                }
                BindResourceData();
                intLanguageId = GlobalFunctions.GetLanguageId();

                if (BudgetUrl.Trim() == "BudgetTransaction.aspx")
                {
                    divMyAccount.Visible = true;
                    strCurrencySymbol = Convert.ToString(Session["IndeedCurrSymbol"]);

                    Exceptions.WriteInfoLog("strCurrencySymbol - " + strCurrencySymbol);
                }
                else
                {
                    divMyAccount.Visible = false;

                    #region New Changes For Points
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        strCurrencySymbol = string.Empty;
                        strCurrencySymbolPoints = GlobalFunctions.GetCurrencySymbol();

                    }
                    else
                    {
                        strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                    }
                    Exceptions.WriteInfoLog("ELSE - strCurrencySymbol - " + strCurrencySymbol);

                    #endregion

                }

                if (!IsPostBack)
                {
                    if (Session["AccessToken"] != null)
                    {
                        // Response.RedirectToRoute("login-page");
                    }
                    else
                    {
                        if (Session["User"] == null)
                        {
                            Response.RedirectToRoute("login-page");
                        }
                        else
                        {
                            UserBE user = Session["User"] as UserBE;
                            if (user.IsGuestUser == true)
                                Response.RedirectToRoute("login-page");
                        }
                    }
                    if (Convert.ToString(Session["CurrOrderItemId"]) != "")
                    {
                        int OrderID = Convert.ToInt32(Session["CurrOrderItemId"]);
                        BindOrderItem(OrderID);//2298843
                        if (SalesOrderID == Convert.ToString(Session["NextOrderItemId"]))
                        { btnNext.Enabled = false; }
                        if (SalesOrderID == Convert.ToString(Session["PrevOrderItemId"]))
                        { btnPrevious.Enabled = false; }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindOrderItem(int OrderId)
        {
            try
            {
                obj = new CustomerSalesOrder();
                obj = CustomerOrder_OASIS.GetUserOrder(OrderId);

                Despatches custOrderDespatches = obj.dispatches;
                #region Reading Despatch Details and Quantity
                try
                {
                    if (custOrderDespatches.despatch != null)
                    {
                        string DeliveryAddresss = "";

                        foreach (Despatch ordDespatch in custOrderDespatches.despatch)
                        {
                            int DeliveredQty;
                            string DespacthId = "";
                            for (int i = 0; i < ordDespatch.despatchlines.despatchline.Length; i++)
                            {
                                string DeliveryAddressId = ordDespatch.DelAddressID;
                                if (!DeliveryAddresss.Contains(DeliveryAddressId))
                                {
                                    DeliveryAddresss = DeliveryAddresss + ordDespatch.DelAddressID;
                                    DeliveryAddresss = DeliveryAddresss + ",";
                                }
                                DespacthId = ordDespatch.despatchlines.despatchline[i].OrderLineID;
                                DeliveredQty = ordDespatch.despatchlines.despatchline[i].DeliveredQuantity;
                                if (dictDispatches.ContainsKey(DespacthId))
                                {
                                    int iExistingValue = dictDispatches.FirstOrDefault(x => x.Key == DespacthId).Value;
                                    iExistingValue += DeliveredQty;
                                    dictDispatches[DespacthId] = iExistingValue;
                                }
                                else
                                {
                                    dictDispatches.Add(DespacthId, DeliveredQty);
                                }
                            }
                        }

                    }
                    foreach (DeliveryAddress objDeliveryAddress in obj.deliveryaddresses.deliveryaddress)
                    {
                        if (Convert.ToString(objDeliveryAddress.DelAddressID) != null)
                        {
                            NoofDeliveryAddress++;
                        }
                    }
                }
               
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
                #endregion
                #region Read Addresses
                if (obj.orderheaders != null)
                {
                    if (NoofDeliveryAddress > 1)
                    {
                        lblDName.Text = strMultipleDeliveryAddress;
                        //lblDName.Text = "";
                        lblDCompany.Text = "";
                        lblDAddress.Text = "";
                        lblDCity.Text = "";
                        lblDPostCode.Text = "";
                        lblDCountry.Text = "";
                    }
                    else
                    {
                       
                        lblDName.Text = obj.deliveryaddresses.deliveryaddress[0].ContactName;
                        lblDCompany.Text = obj.deliveryaddresses.deliveryaddress[0].CompanyName;
                        lblDAddress.Text = obj.deliveryaddresses.deliveryaddress[0].Address1;
                        lblDCity.Text = obj.deliveryaddresses.deliveryaddress[0].City;
                        lblDPostCode.Text = obj.deliveryaddresses.deliveryaddress[0].PostCode;
                        lblDCountry.Text = obj.deliveryaddresses.deliveryaddress[0].Country;
                    }
                    lblPaymentMethod.Text = obj.orderheaders.PaymentType;
                    #region for checking the payment method for Points
                    if (lblPaymentMethod.Text != null)
                    {
                        if (lblPaymentMethod.Text == "Points")
                        {

                            isPointEnabled = true;
                        }
                        else
                        {
                            isPointEnabled = false;
                        }


                    }
                    #endregion
                    lblCustomerRef.Text = obj.orderheaders.CustomerRef;

                    lblRName.Text = obj.billingaddress.ContactName;
                    lblRCompany.Text = obj.billingaddress.CompanyName;
                    lblRAddress.Text = obj.billingaddress.Address1;
                    lblRCity.Text = obj.billingaddress.City;
                    lblRPostCode.Text = obj.billingaddress.PostCode;
                    lblRCountry.Text = obj.billingaddress.Country;
                   
                    rptProductItem.DataSource = obj.orderlines.orderline;
                    rptProductItem.DataBind();
                    rptTrackingNumber.DataSource = obj.dispatches.despatch;
                    rptTrackingNumber.DataBind();
                    Session["ObjOrderItem"] = obj;
                    lblCurrentId.Text = Convert.ToString(obj.orderheaders.SalesOrderID);
                    SalesOrderID = Convert.ToString(obj.orderheaders.SalesOrderID);
                    //Label1.Text = obj.orderheaders.GoodsValue;
                    //Label2.Text = obj.orderheaders.FreightValue;
                    //Label3.Text = obj.orderheaders.TaxValue;
                    //Label4.Text = obj.orderheaders.OrderValue.ToString("##,###,##0.#0");

                    if (GlobalFunctions.IsPointsEnbled() && isPointEnabled == true)
                    {
                        dvTax.Visible = false;
                        obj.orderheaders.FreightValue = "0";
                        Label1.Text = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(obj.orderheaders.GoodsValue, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbolPoints, intLanguageId) /*+ GlobalFunctions.PointsText(intLanguageId)*/; ;
                        Label2.Text = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(obj.orderheaders.FreightValue, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbolPoints, intLanguageId) /*+ GlobalFunctions.PointsText(intLanguageId)*/;
                        
                        // Below LOC is commented as Tax is not applicable to Points Stores SHRIGANESH SINGH 26 August 2016
                        //Label3.Text = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(obj.orderheaders.TaxValue, CultureInfo.InvariantCulture.NumberFormat), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId);

                        Double OrderValue = Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(obj.orderheaders.GoodsValue, CultureInfo.InvariantCulture.NumberFormat), "", intLanguageId));
                           /* + Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(obj.orderheaders.FreightValue, CultureInfo.InvariantCulture.NumberFormat), "", intLanguageId));*/

                        //Label4.Text = Convert.ToString(OrderValue) + GlobalFunctions.PointsText(intLanguageId);
                        Label4.Text = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(OrderValue, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbolPoints, intLanguageId) /*+ GlobalFunctions.PointsText(intLanguageId)*/;
                    }
                    else
                    {
                        /* Changes By vivek
                        Label1.Text = GlobalFunctions.GetCurrencySymbol() + obj.orderheaders.GoodsValue;
                        Label2.Text = GlobalFunctions.GetCurrencySymbol() + obj.orderheaders.FreightValue;
                        Label3.Text = GlobalFunctions.GetCurrencySymbol() + obj.orderheaders.TaxValue;
                        Label4.Text = GlobalFunctions.GetCurrencySymbol() + obj.orderheaders.OrderValue.ToString("##,###,##0.#0");*/
                        Label1.Text = strCurrencySymbolPoints+ obj.orderheaders.GoodsValue;
                        Label2.Text = strCurrencySymbolPoints + obj.orderheaders.FreightValue;
                        Label3.Text = strCurrencySymbolPoints + obj.orderheaders.TaxValue;
                        Label4.Text = strCurrencySymbolPoints + obj.orderheaders.OrderValue.ToString("##,###,##0.#0");
                        
                    }   

                }
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptProductItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblShipped = e.Item.FindControl("lblShipped") as Label;
                    Label lblQuantity = e.Item.FindControl("lblQuantity") as Label;
                    Label lblBackOrder = e.Item.FindControl("lblBackOrder") as Label;
                    Button btnBuyIt = e.Item.FindControl("btnBuyIt") as Button;
                    Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                    Label lblLineTotal = e.Item.FindControl("lblLineTotal") as Label;
                    string strPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "UnitPrice")).ToString("#######0.#0");
                    string strLineTotal = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "LineValue")).ToString("#######0.#0");
                    if (GlobalFunctions.IsPointsEnbled() && isPointEnabled == true)
                    {
                        lblPrice.Text = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(strPrice, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbolPoints, intLanguageId) /*+ GlobalFunctions.PointsText(intLanguageId)*/;
                        lblLineTotal.Text = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(strLineTotal, CultureInfo.InvariantCulture.NumberFormat), strCurrencySymbolPoints, intLanguageId)/* + GlobalFunctions.PointsText(intLanguageId)*/;
                    
                    }
                    else
                    {
                        /*commented by vivek 
                        lblPrice.Text = GlobalFunctions.GetCurrencySymbol() + strPrice;
                        lblLineTotal.Text = GlobalFunctions.GetCurrencySymbol() + strLineTotal;
                         */
                        lblPrice.Text = strCurrencySymbolPoints + strPrice;
                        lblLineTotal.Text = strCurrencySymbolPoints + strLineTotal;
                    }
                    OrderLine ol = (OrderLine)e.Item.DataItem;
                    btnBuyIt.Text = strbtnBuyIt;
                    //Despatch despatch = obj.dispatches.despatch.FirstOrDefault(x => x.despatchlines.despatchline.FirstOrDefault(y => y.OrderLineID == ol.OrderLineID) != null);
                    if (obj.dispatches.despatch != null)
                    {
                        string DispatchId = ol.OrderLineID;
                        DespatchLine despatchLine = obj.dispatches.despatch.Where(x => x.despatchlines.despatchline != null).SelectMany(x => x.despatchlines.despatchline).ToList().FirstOrDefault(y => y.OrderLineID == ol.OrderLineID);
                        // lblShipped.Text = despatchLine != null ? Convert.ToString(despatchLine.DeliveredQuantity) : "-";
                        lblShipped.Text = Convert.ToString(dictDispatches[DispatchId]);
                    }
                    else
                        lblShipped.Text = "-";
                    //lblShipped.Text = Convert.ToString(Convert.ToInt32(lblQuantity.Text) - Convert.ToInt32(lblBackOrder.Text));
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnNavigate_Click(object sender, EventArgs e)
        {
            try
            {
                //Button btn = sender as Button;
                //if (btn.ID == "btnPrevious")
                //{ BindOrderItem(Convert.ToInt32(Session["PrevOrderItemId"])); }
                //else if (btn.ID == "btnNext")
                //{ BindOrderItem(Convert.ToInt32(Session["NextOrderItemId"])); }
                //SalesOrders objSalesOrders = (SalesOrders)Session["ObjOrderHistory"];
                //int currIndex = Array.FindIndex(objSalesOrders.orderHeaders, x => x.SalesOrderID == SalesOrderID);
                //var nxt = "";
                //var prev = "";
                //try { nxt = objSalesOrders.orderHeaders.ElementAtOrDefault(currIndex + 1).SalesOrderID; }
                //catch { nxt = objSalesOrders.orderHeaders.ElementAtOrDefault(currIndex).SalesOrderID; }
                //try { prev = objSalesOrders.orderHeaders.ElementAtOrDefault(currIndex - 1).SalesOrderID; }
                //catch { prev = objSalesOrders.orderHeaders.ElementAtOrDefault(currIndex).SalesOrderID; }
                //var curr = objSalesOrders.orderHeaders[currIndex].SalesOrderID;
                //Session["NextOrderItemId"] = nxt;
                //Session["PrevOrderItemId"] = prev;
                //Session["CurrOrderItemId"] = curr;
                //if (SalesOrderID == Convert.ToString(Session["NextOrderItemId"]))
                //{ btnNext.Enabled = false; }
                //else { btnNext.Enabled = true; }
                //if (SalesOrderID == Convert.ToString(Session["PrevOrderItemId"]))
                //{ btnPrevious.Enabled = false; }
                //else { btnPrevious.Enabled = true; }



                Button btn = sender as Button;
                if (btn.ID == "btnPrevious")
                { BindOrderItem(Convert.ToInt32(Session["PrevOrderItemId"])); }
                else if (btn.ID == "btnNext")
                { BindOrderItem(Convert.ToInt32(Session["NextOrderItemId"])); }
                List<OrderHeaders> lstOrderHeader = (List<OrderHeaders>)Session["ObjOrderHistory"];
                int currIndex = lstOrderHeader.FindIndex(x => x.SalesOrderID == Convert.ToString(Session["CurrOrderItemId"]));
                var next = "";
                var prev = "";
                try { prev = lstOrderHeader.ElementAtOrDefault(currIndex + 1).SalesOrderID; }
                catch { prev = lstOrderHeader.ElementAtOrDefault(currIndex).SalesOrderID; }
                try { next = lstOrderHeader.ElementAtOrDefault(currIndex - 1).SalesOrderID; }
                catch { next = lstOrderHeader.ElementAtOrDefault(currIndex).SalesOrderID; }
                var curr = lstOrderHeader[currIndex].SalesOrderID;
                Session["NextOrderItemId"] = next;
                Session["PrevOrderItemId"] = prev;
                Session["CurrOrderItemId"] = curr;
                if (SalesOrderID == Convert.ToString(Session["NextOrderItemId"]))
                { btnNext.Enabled = false; }
                else { btnNext.Enabled = true; }
                if (SalesOrderID == Convert.ToString(Session["PrevOrderItemId"]))
                { btnPrevious.Enabled = false; }
                else { btnPrevious.Enabled = true; }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                List<OrderHeaders> lstOrderHeader = (List<OrderHeaders>)Session["ObjOrderHistory"];
                int currIndex = lstOrderHeader.FindIndex(x => x.SalesOrderID == Convert.ToString(Session["CurrOrderItemId"]));
                var next = "";
                var prev = "";
                try { prev = lstOrderHeader.ElementAtOrDefault(currIndex + 1).SalesOrderID; }
                catch { prev = lstOrderHeader.ElementAtOrDefault(currIndex).SalesOrderID; }
                try { next = lstOrderHeader.ElementAtOrDefault(currIndex - 1).SalesOrderID; }
                catch { next = lstOrderHeader.ElementAtOrDefault(currIndex).SalesOrderID; }

                Session["NextOrderItemId"] = next;
                Session["PrevOrderItemId"] = prev;
                Session["CurrOrderItemId"] = prev;
                if (SalesOrderID == Convert.ToString(Session["NextOrderItemId"]))
                { btnNext.Enabled = false; }
                else { btnNext.Enabled = true; }
                if (SalesOrderID == Convert.ToString(Session["PrevOrderItemId"]))
                { btnPrevious.Enabled = false; }
                else { btnPrevious.Enabled = true; }
                BindOrderItem(Convert.ToInt32(Session["CurrOrderItemId"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                List<OrderHeaders> lstOrderHeader = (List<OrderHeaders>)Session["ObjOrderHistory"];
                int currIndex = lstOrderHeader.FindIndex(x => x.SalesOrderID == Convert.ToString(Session["CurrOrderItemId"]));
                var next = "";
                var prev = "";
                try { prev = lstOrderHeader.ElementAtOrDefault(currIndex + 1).SalesOrderID; }
                catch { prev = lstOrderHeader.ElementAtOrDefault(currIndex).SalesOrderID; }
                try { next = lstOrderHeader.ElementAtOrDefault(currIndex - 1).SalesOrderID; }
                catch { next = lstOrderHeader.ElementAtOrDefault(currIndex).SalesOrderID; }
               
                Session["NextOrderItemId"] = next;
                Session["PrevOrderItemId"] = prev;
                Session["CurrOrderItemId"] = next;
                if (SalesOrderID == Convert.ToString(Session["NextOrderItemId"]))
                { btnNext.Enabled = false; }
                else { btnNext.Enabled = true; }
                if (SalesOrderID == Convert.ToString(Session["PrevOrderItemId"]))
                { btnPrevious.Enabled = false; }
                else { btnPrevious.Enabled = true; }
                BindOrderItem(Convert.ToInt32(Session["CurrOrderItemId"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptProductItem_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "AddBasket")
                {
                    string productCode = Convert.ToString(e.CommandArgument);
                    int i = OrderTrackingBL.GetProductSKUId(Constants.USP_GetSingleProductSKUId, true, productCode);
                    if (i != 0)
                    {
                        int intUserId = 0;
                        string UserSessionId = HttpContext.Current.Session.SessionID;
                        if (Session["User"] != null)
                        {
                            UserBE objUserBE = (UserBE)Session["User"];
                            intUserId = objUserBE.UserId;
                        }
                        Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                        Dictionary<string, string> dictionary = new Dictionary<string, string>();
                        dictionary.Add("UserId", Convert.ToString(intUserId));
                        dictionary.Add("ProductSKUId", i.ToString());
                        dictionary.Add("Quantity", lblQuantity.Text);
                        dictionary.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                        dictionary.Add("UserSessionId", UserSessionId);
                        dictionary.Add("UserInput", "");                        
                        dictionary.Add("ProductId", "");
                        //dictionary.Add("ProductId", Convert.ToString(objCurrentProductBE.ProductId));
                        int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, "Sorry, this product is no longer available.", AlertType.Warning); return; }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId().To_Int16());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        Item_Code = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Item_Code").ResourceValue;
                        Quantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        Line = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Line").ResourceValue;
                        Item_Description = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Item_Description").ResourceValue;
                        //Quantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Delivered_to_Text").ResourceValue;
                        Shipped = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Shipped_Text").ResourceValue;
                        Action = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Action_Text").ResourceValue;
                        Backorder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_Item_Backorder_Text").ResourceValue;
                        Payment_Method = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SC_Payment_Method").ResourceValue;
                        Order_details = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Details").ResourceValue;
                        Line_total = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Line_Total_Text").ResourceValue;
                        Delivery_Address = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DeliveryAddress").ResourceValue;
                        Invoice_Address = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Invoice_Address_Text").ResourceValue;
                        Reference = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Reference_Text").ResourceValue;
                        Tracking_Numbers = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Tracking_Number").ResourceValue;
                        Subtotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SC_Basket_Subtotal").ResourceValue;
                        Tax = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Tax").ResourceValue;
                        Carriage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Carriage_Text").ResourceValue;
                        Total = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Total_Text").ResourceValue;
                        strbtnBuyIt = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Buy_it_again").ResourceValue;
                        Unit_Price = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Unit_Price").ResourceValue;
                        Order_Number = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Number_Text").ResourceValue;
                        strMultipleDeliveryAddress = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Multiple_Delivery_Address").ResourceValue;
                        spnBackToMyAcc.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BackToMyAccount").ResourceValue;

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}
