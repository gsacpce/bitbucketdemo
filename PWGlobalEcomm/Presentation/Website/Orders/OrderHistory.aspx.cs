﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Globalization;
using System.Web.UI.HtmlControls;

namespace Presentation
{
    public class Orders_OrderHistory : BasePage //System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptPager, rptOrders;
        protected global::System.Web.UI.WebControls.DropDownList ddlSearchOption;
        protected global::System.Web.UI.WebControls.TextBox txtSearch;
        protected global::System.Web.UI.WebControls.Label lblNoRecord;
        protected global::System.Web.UI.WebControls.Button btnSearch;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spOrder_History_Title, dvOrderHistorySearch, dvNoRecord, divOrderValueText;
        public string Order_Number, Order_Date, Delivered_to, Order_value, Order_status, Action, Search_Placeholder, strBtnView;
        private int PageSize = 10;
        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        private string contactlist = "";
        protected global::System.Web.UI.WebControls.HiddenField hdnStoreId;
        List<OrderHeaders> lstSortedOrderHeaders;
        Int16 StoreId = 0;
        FeatureBE OrderHistoryMonthstoReturn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindResourceData();
                intLanguageId = GlobalFunctions.GetLanguageId();

                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    StoreId = objStoreBE.StoreId;
                    List<FeatureBE> GetAllFeatures = FeatureBL.GetAllFeatures(StoreId, true);
                    OrderHistoryMonthstoReturn = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "orderhistorymonthstoreturn");
                }

                if (!IsPostBack)
                {
                    if (Session["AccessToken"] != null)
                    {
                        // Response.RedirectToRoute("login-page");
                    }
                    else
                    {
                        if (Session["User"] == null)
                        {
                            Response.RedirectToRoute("login-page");
                        }
                        else
                        {
                            UserBE user = Session["User"] as UserBE;
                            if (user.IsGuestUser == true)
                                Response.RedirectToRoute("login-page");
                        }
                    }
                    GetCustomerOrders(1);
                    foreach (RepeaterItem x in rptPager.Items)
                    {
                        System.Web.UI.HtmlControls.HtmlGenericControl liPage = (System.Web.UI.HtmlControls.HtmlGenericControl)x.FindControl("liPage");
                        LinkButton lnkPage = (LinkButton)x.FindControl("lnkPage");
                        if (x.ItemIndex == 0)
                        {
                            liPage.Attributes.Add("class", "active");
                            lnkPage.CssClass = "customPagination";
                            lnkPage.OnClientClick = "return false;";
                        }
                        else
                        {
                            liPage.Attributes.Add("class", " ");
                            lnkPage.CssClass = " ";
                            lnkPage.OnClientClick = "return true;";
                        }
                    }
                    //added by Sripal
                    ReadMetaTagsData();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public void GetCustomerOrders(int PageIndex)
        {
            try
            {
                int OasisContactId = 0;
                DateTime StartDate = DateTime.Now;

                //Added By Hardik for "calculate the StartDate parameter as today’s date minus x months"
                int month = Convert.ToInt32(OrderHistoryMonthstoReturn.FeatureValues[0].FeatureDefaultValue);                                
                StartDate = StartDate.AddMonths(-month);
                
                if (Session["User"] != null)
                {

                    UserBE objUserBE = (UserBE)Session["User"];
                    objUserBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    objUserBE = UserBL.Getorderhistory(Constants.USP_GetOrdershistory, true, objUserBE);
                    contactlist = Convert.ToString(objUserBE.BASYS_CustomerContactId);
                }
                else
                {
                    if (Session["AccessToken"] != null)
                    {
                        //Response.RedirectToRoute("login-page");
                    }
                    else
                    {
                        Response.RedirectToRoute("login-page");
                    }
                }
                SalesOrders objSalesOrders = new SalesOrders();
                SalesOrders objSalesOrderStartDate = new SalesOrders();

                //objSalesOrders = UserOrder_OASIS.GetAllUserOrders(OasisContactId); //924186   //926393
                //Added By Hardik 
                #region
                //if (contactlist != null)
                //{
                //    string[] contact = contactlist.Split(',');
                //    for (int i = 0; i < contact.Length; i++)
                //    {
                //        OasisContactId = Convert.ToInt32(contact[i]);
                //        objSalesOrderStartDate = UserOrder_OASIS.GetAllUserOrderStartDate(OasisContactId, StartDate.Date);
                //        if (objSalesOrderStartDate.orderHeaders != null)
                //        {
                //            lstSortedOrderHeaders = objSalesOrderStartDate.orderHeaders.OrderByDescending(x => x.SalesOrderID).ToList<OrderHeaders>();
                //        }

                //    }
                //}
                #endregion
                if (contactlist != null)
                {
                    string[] contact = contactlist.Split(',');
                    for (int i = 0; i < contact.Length; i++)
                    {
                        OasisContactId = Convert.ToInt32(contact[i]);
                        objSalesOrderStartDate = UserOrder_OASIS.GetAllUserOrderStartDate(OasisContactId, StartDate.Date);
                        if (objSalesOrderStartDate.orderHeaders != null)
                        {
                            List<OrderHeaders> lstSortedOrderHeadersper;
                            lstSortedOrderHeadersper = objSalesOrderStartDate.orderHeaders.OrderByDescending(x => x.SalesOrderID).ToList<OrderHeaders>();
                            if (lstSortedOrderHeaders != null)
                            {
                                foreach (OrderHeaders orderitem in lstSortedOrderHeadersper)
                                {
                                    lstSortedOrderHeaders.Add(orderitem);
                                }
                            }
                            else
                            {
                                lstSortedOrderHeaders = lstSortedOrderHeadersper;
                            }
                        }

                    }
                }

                //var abc = objSalesOrders.orderHeaders.ToList<OrderHeaders>().OrderByDescending(x => x.SalesOrderID);



                if (lstSortedOrderHeaders != null)
                {
                    Session["ObjOrderHistory"] = lstSortedOrderHeaders;
                    PopulatePager(lstSortedOrderHeaders.Count, PageIndex);
                    dvOrderHistorySearch.Visible = true;
                    dvNoRecord.Visible = false;
                }
                else
                {
                    rptOrders.DataSource = null;
                    rptOrders.DataBind();

                    dvOrderHistorySearch.Visible = false;
                    dvNoRecord.Visible = true;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptOrders_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ViewItem")
                {
                    List<OrderHeaders> lstOrderHeader = (List<OrderHeaders>)Session["ObjOrderHistory"];

                    var next = "";
                    var prev = "";

                    if (e.Item.ItemIndex == 0)
                    {
                        next = lstOrderHeader[e.Item.ItemIndex].SalesOrderID;
                        if (lstOrderHeader.Count > 1)
                            prev = lstOrderHeader[e.Item.ItemIndex + 1].SalesOrderID;
                        else
                            prev = lstOrderHeader[e.Item.ItemIndex].SalesOrderID;
                    }
                    else if (lstOrderHeader.Count - 1 == e.Item.ItemIndex)
                    {
                        prev = lstOrderHeader[e.Item.ItemIndex].SalesOrderID;
                        next = lstOrderHeader[e.Item.ItemIndex - 1].SalesOrderID;
                    }
                    else
                    {
                        prev = lstOrderHeader[e.Item.ItemIndex + 1].SalesOrderID;
                        next = lstOrderHeader[e.Item.ItemIndex - 1].SalesOrderID;
                    }

                    Session["NextOrderItemId"] = next;
                    Session["PrevOrderItemId"] = prev;
                    Session["CurrOrderItemId"] = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect(GlobalFunctions.GetVirtualPath() + "Orders/OrderItem.aspx");

                    //SalesOrders objSalesOrders = (SalesOrders)Session["ObjOrderHistory"];
                    ////var nxtIndex = objSalesOrders.orderHeaders.GetLowerBound(e.Item.ItemIndex);
                    ////var prevIndex = objSalesOrders.orderHeaders.GetUpperBound(e.Item.ItemIndex);
                    ////var nxt = objSalesOrders.orderHeaders[nxtIndex].SalesOrderID;
                    ////var prev = objSalesOrders.orderHeaders[prevIndex].SalesOrderID;
                    //var nxt = "";
                    //var prev = "";

                    //if (e.Item.ItemIndex == 0)
                    //{
                    //    prev = objSalesOrders.orderHeaders[e.Item.ItemIndex].SalesOrderID;
                    //    if (objSalesOrders.orderHeaders.Length > 1)
                    //        nxt = objSalesOrders.orderHeaders[e.Item.ItemIndex + 1].SalesOrderID;
                    //    else
                    //        nxt = objSalesOrders.orderHeaders[e.Item.ItemIndex].SalesOrderID;
                    //}
                    //else if (objSalesOrders.orderHeaders.Length - 1 == e.Item.ItemIndex)
                    //{
                    //    nxt = objSalesOrders.orderHeaders[e.Item.ItemIndex].SalesOrderID;
                    //    prev = objSalesOrders.orderHeaders[e.Item.ItemIndex - 1].SalesOrderID;
                    //}
                    //else
                    //{
                    //    nxt = objSalesOrders.orderHeaders[e.Item.ItemIndex + 1].SalesOrderID;
                    //    prev = objSalesOrders.orderHeaders[e.Item.ItemIndex - 1].SalesOrderID;
                    //}

                    //Session["NextOrderItemId"] = nxt;
                    //Session["PrevOrderItemId"] = prev;
                    //Session["CurrOrderItemId"] = Convert.ToInt32(e.CommandArgument);
                    //Response.Redirect(GlobalFunctions.GetVirtualPath() + "Orders/OrderItem.aspx");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void Page_Changed(object sender, EventArgs e)
        {
            try
            {
                int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                GetOrderPageWise(pageIndex);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GetOrderPageWise(int pageIndex)
        {
            try
            {
                //SalesOrders objSalesOrders = new SalesOrders();
                List<OrderHeaders> lstSortedOrderHeaders = new List<OrderHeaders>();
                lstSortedOrderHeaders = (List<OrderHeaders>)Session["ObjOrderHistory"];
                //  objSalesOrders = (SalesOrders)Session["ObjOrderHistory"];
                if (pageIndex == 1)
                {
                    int numberOfPages = (lstSortedOrderHeaders.Count() / PageSize) + (lstSortedOrderHeaders.Count() % PageSize == 0 ? 0 : 1);
                    int currentPage = 0;
                    rptOrders.DataSource = lstSortedOrderHeaders.Skip(currentPage * PageSize).Take(PageSize);
                    rptOrders.DataBind();
                    //rptOrders.DataSource = lstSortedOrderHeaders;
                    //rptOrders.DataBind();
                }
                else
                {
                    rptOrders.DataSource = lstSortedOrderHeaders.Skip(PageSize * pageIndex - 1).Take(PageSize);
                    rptOrders.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header)
                {
                    HtmlGenericControl divOrderValueHeader = e.Item.FindControl("divOrderValueHeader") as HtmlGenericControl;//Added by Nilesh 19/09/2016		
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        divOrderValueHeader.Attributes.Add("style", "visibility:hidden");
                    }
                    else
                    {
                        divOrderValueHeader.Attributes.Add("style", "visibility:visible");
                    }
                }

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblOrderNumber = (Label)e.Item.FindControl("lblOrderNumber");
                    Label lblDeliveredTo = (Label)e.Item.FindControl("lblDeliveredTo");
                    Label lblOrderValue = (Label)e.Item.FindControl("lblOrderValue");
                    HtmlGenericControl divOrderValueText = e.Item.FindControl("divOrderValueText") as HtmlGenericControl;//Added by Nilesh 19/09/2016
                    UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                    Button btnView = (Button)e.Item.FindControl("btnView");
                    btnView.Text = strBtnView;
                   // string strPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "OrderValue"),CultureInfo.InvariantCulture.NumberFormat).ToString();

                    //Changes done by Snehal - 06 04 2017
                    string strPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "OrderValue"), CultureInfo.InvariantCulture).ToString("#######0.#0");

                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        lblOrderValue.Text = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(strPrice, CultureInfo.InvariantCulture.NumberFormat), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId);
                        divOrderValueText.Attributes.Add("style", "visibility:hidden");
                    }
                    else
                    {
                      //  lblOrderValue.Text = GlobalFunctions.GetCurrencySymbol() + strPrice;
                        lblOrderValue.Text = GlobalFunctions.GetCurrencySymbol() + strPrice;
                        divOrderValueText.Attributes.Add("style", "visibility:visible");
                    }
                    obj = UserBL.GetOrderDeliveryAddress(Constants.Usp_GetOrderDeliveryAddress, true, lblOrderNumber.Text, GlobalFunctions.GetCurrencyId().ToString());
                    if (obj != null)
                    {
                        if (obj.PreDefinedColumn1 != "")
                        {
                            string s = HttpUtility.HtmlDecode(obj.PreDefinedColumn2) + "," + HttpUtility.HtmlDecode(obj.PreDefinedColumn1) + "," + HttpUtility.HtmlDecode(obj.PreDefinedColumn3) + "<br/>";
                            s += HttpUtility.HtmlDecode(obj.PreDefinedColumn4) + "," + HttpUtility.HtmlDecode(obj.PreDefinedColumn5) + " " + HttpUtility.HtmlDecode(obj.PreDefinedColumn6) + " " + obj.CountryName + " " + HttpUtility.HtmlDecode(obj.PreDefinedColumn9);
                            lblDeliveredTo.Text = s;
                        }
                        else
                        {
                            string s = "Offline order <br/> Click View Details for more information";
                            lblDeliveredTo.Text = s;
                        }
                    }
                    else
                    {
                        string s = "Offline order <br/> Click View Details for more information";
                        lblDeliveredTo.Text = s;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void PopulatePager(int recordCount, int currentPage)
        {
            try
            {
                double dblPageCount = (double)((decimal)recordCount / Convert.ToDecimal(PageSize));
                int pageCount = (int)Math.Ceiling(dblPageCount);
                List<ListItem> pages = new List<ListItem>();
                if (pageCount > 0)
                {
                    for (int i = 1; i <= pageCount; i++)
                    { pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPage)); }
                }
                rptPager.DataSource = pages;
                rptPager.DataBind();
                GetOrderPageWise(currentPage);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //SalesOrders objSalesOrders = new SalesOrders();
                //objSalesOrders = (SalesOrders)Session["ObjOrderHistory"];
                //if (objSalesOrders.orderHeaders.Count() > 0)
                //{
                //    List<OrderHeaders> lst = new List<OrderHeaders>();
                //    string _type = ddlSearchOption.SelectedValue.ToLower();
                //    string _val = txtSearch.Text.Trim();
                //    for (int i = 0; i < objSalesOrders.orderHeaders.Count(); i++)
                //    {
                //        if (_type == "date")
                //        {
                //            rptPager.Visible = false;
                //            DateTime d1 = objSalesOrders.orderHeaders[i].OrderDate.Date;
                //            DateTime d2 = Convert.ToDateTime(_val).Date;
                //            if (d1 == d2)
                //            { lst.Add(objSalesOrders.orderHeaders[i]); }
                //        }
                //        else if (_type == "number")
                //        {
                //            rptPager.Visible = false;
                //            if (objSalesOrders.orderHeaders[i].SalesOrderID == Convert.ToString(_val))
                //            { lst.Add(objSalesOrders.orderHeaders[i]); }
                //        }
                //        else if (_type == "value")
                //        {
                //            rptPager.Visible = false;
                //            if (objSalesOrders.orderHeaders[i].OrderValue == Convert.ToDouble(_val))
                //            { lst.Add(objSalesOrders.orderHeaders[i]); }
                //        }
                //        else if (_type == "status")
                //        {
                //            rptPager.Visible = false;
                //            if (objSalesOrders.orderHeaders[i].OrderStatus == Convert.ToString(_val))
                //            { lst.Add(objSalesOrders.orderHeaders[i]); }
                //        }
                //        else if (_type == "all")
                //        {
                //            rptPager.Visible = true;
                //            GetCustomerOrders(1);
                //        }
                //    }
                //    rptOrders.DataSource = lst;
                //    rptOrders.DataBind();
                //}

                List<OrderHeaders> lstOrderHeader = (List<OrderHeaders>)Session["ObjOrderHistory"];
                lstOrderHeader = (List<OrderHeaders>)Session["ObjOrderHistory"];
                if (lstOrderHeader.Count() > 0)
                {
                    List<OrderHeaders> lst = new List<OrderHeaders>();
                    string _type = ddlSearchOption.SelectedValue.ToLower();
                    string _val = txtSearch.Text.Trim();
                    for (int i = 0; i < lstOrderHeader.Count(); i++)
                    {
                        if (_type == "date")
                        {
                            rptPager.Visible = false;
                            DateTime d1 = lstOrderHeader[i].OrderDate.Date;
                            DateTime d2 = Convert.ToDateTime(_val).Date;
                            if (d1 == d2)
                            { lst.Add(lstOrderHeader[i]); }
                        }
                        else if (_type == "number")
                        {
                            rptPager.Visible = false;
                            if (lstOrderHeader[i].SalesOrderID == Convert.ToString(_val))
                            { lst.Add(lstOrderHeader[i]); }
                        }
                        else if (_type == "value")
                        {
                            rptPager.Visible = false;
                            if (lstOrderHeader[i].OrderValue == Convert.ToString(_val))
                            { lst.Add(lstOrderHeader[i]); }
                        }
                        else if (_type == "status")
                        {
                            rptPager.Visible = false;
                            if (lstOrderHeader[i].OrderStatus == Convert.ToString(_val))
                            { lst.Add(lstOrderHeader[i]); }
                        }
                        else if (_type == "all")
                        {
                            rptPager.Visible = true;
                            Response.RedirectToRoute("MyOrders");
                            //GetCustomerOrders(1);
                        }
                    }
                    rptOrders.DataSource = lst;
                    rptOrders.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptPager_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "clicked")
                {
                    foreach (RepeaterItem x in rptPager.Items)
                    {
                        System.Web.UI.HtmlControls.HtmlGenericControl liPage = (System.Web.UI.HtmlControls.HtmlGenericControl)x.FindControl("liPage");
                        LinkButton lnkPage = (LinkButton)x.FindControl("lnkPage");
                        if (e.Item.ItemIndex == x.ItemIndex)
                        {
                            liPage.Attributes.Add("class", "active");
                            lnkPage.CssClass = "customPagination";
                            lnkPage.OnClientClick = "return false;";
                        }
                        else
                        {
                            liPage.Attributes.Add("class", " ");
                            lnkPage.CssClass = " ";
                            lnkPage.OnClientClick = "return true;";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId().To_Int16());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        spOrder_History_Title.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_History_Title").ResourceValue;
                        lblNoRecord.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_History_No_Order").ResourceValue;
                        Order_Date = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Date_Text").ResourceValue;
                        Order_Number = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Number_Text").ResourceValue;
                        Delivered_to = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Delivered_to_Text").ResourceValue;
                        Order_value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Value_Text").ResourceValue;
                        Action = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Action_Text").ResourceValue;
                        Search_Placeholder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_History_Search_Placeholder").ResourceValue;
                        txtSearch.Attributes.Add("placeholder", Search_Placeholder);
                        btnSearch.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Order_History_Search_Btn").ResourceValue;
                        strBtnView = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_ViewDetails_Text").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "Order History");
                    if (lstMetaTags.Count > 0)
                    {
                        Page.Title = lstMetaTags[0].MetaContentTitle;
                        Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                        Page.MetaDescription = lstMetaTags[0].MetaDescription;
                    }
                    else
                    {
                        Page.Title = "";
                        ////
                        Page.MetaKeywords = "";
                        Page.MetaDescription = "";
                    }
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}