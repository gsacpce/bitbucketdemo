﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Linq;
using System.Web;
using Microsoft.Security.Application;

namespace Presentation
{
    public class StaticPage_StaticPage : BasePage
    {
        string pageTitle = "Brand Addition";
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnHeading;
        protected global::System.Web.UI.WebControls.Literal ltrContent;
        protected global::System.Web.UI.WebControls.Image imgBannerImage;

        public StaticPage_StaticPage()
        {
            this.Init += new EventHandler(Page_Init);
        }
        public string Preivew
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["preview"])) ? "false" : Convert.ToString(Page.RouteData.Values["preview"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { BindStaticPageDetails(); }

            /*Sachin Chauhan : 22 09 2015 : Redirect user to Product details page with language change in place from drop down*/
            if (Session["PrevLanguageId"] == null)
            {
                Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
            }


            if (!Session["PrevLanguageId"].ToString().To_Int32().Equals(GlobalFunctions.GetLanguageId().To_Int32()))
            {
                //intLanguageId = GlobalFunctions.GetLanguageId();
                List<StaticPageManagementBE> LstStaticPageAllLanguage;

                LstStaticPageAllLanguage = StaticPageManagementBL.GetStaticPageAllLanguage(Session["CurrStaticPageId"].To_Int16());

                if (Request.Url.Equals(Request.UrlReferrer))
                {
                    string[] urlParts = Request.Url.ToString().Split('/');
                    Int16 urlOffset = 0;
                    for (Int16 i = 0; i < urlParts.Length; i++)
                    {
                        if (urlParts[i].ToLower().Contains("info"))
                        {
                            urlOffset = (Int16)(i + 1);
                            break;
                        }
                    }

                  

                    string redirectUrl = "";

                    for (Int16 i = 0; i < urlOffset-1; i++)
                    {
                        redirectUrl += urlParts[i] + "/";
                    }
                    StaticPageManagementBE currStaticPage = LstStaticPageAllLanguage.FirstOrDefault(x => x.LanguageId == GlobalFunctions.GetLanguageId()) ;
                    redirectUrl += currStaticPage.PageURL.Substring(1, currStaticPage.PageURL.Length-1 ) + "/";

                 

                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();

                    Response.Redirect(redirectUrl);
                }

            }

            /*Sachin Chauhan : 22 09 2015 */

        }
        public void BindStaticPageDetails()
        {
            try
            {
                List<StaticPageManagementBE> lst = new List<StaticPageManagementBE>();
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBE.PageName = "/info/" + Convert.ToString(Page.RouteData.Values["pageurl"]);
                lst = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "PageUrl");
                if (lst != null && lst.Count > 0)
                {
                    /*Sachin Chauhan : 23 09 2015 : Added below StaticPageId variable to Session 
                     *                              so that it can be used when language change from drop down is occured
                    */
                    Session["CurrStaticPageId"] = lst[0].StaticPageId;
                    /*Sachin Chauhan : 23 09 2015*/
                    if (lst[0].IsActive) //|| Convert.ToBoolean(Preivew)    //check whether static page is active or not
                    {
                        if (lst[0].IsLoginBased)
                        {
                            if (!Convert.ToBoolean(Preivew))
                            {
                                if (Session["User"] == null)//check for logged in user session
                                { Session["ReturnUrl"] = lst[0].PageURL.Remove(0, 1); Response.Redirect("~/Signin", true); }
                            }
                        }
                        //Bind Page BasicDetails
                        if (lst[0].PageTitle.ToString() == "" && lst[0].PageName.ToString() == "")
                        { Page.Title = pageTitle; }
                        else
                        {
                            if (lst[0].PageTitle.ToString() != "")
                            { Page.Title = lst[0].PageTitle.ToString(); } // Page Title parameter removed by SHRIGANESH SINGH 19 May 2016
                            else
                            { Page.Title = lst[0].PageName.ToString(); } // Page Title parameter removed by SHRIGANESH SINGH 19 May 2016
                        }
                        if (lst[0].MetaKeyword != "")
                        { Page.MetaKeywords = HttpUtility.HtmlDecode(Convert.ToString(lst[0].MetaKeyword)); }
                        else
                        { Page.MetaKeywords = pageTitle; }
                        if (lst[0].MetaDescription != "")
                        { Page.MetaDescription = HttpUtility.HtmlDecode(Convert.ToString(lst[0].MetaDescription)); }
                        else
                        { Page.MetaDescription = pageTitle; }
                        spnHeading.InnerText = HttpUtility.HtmlDecode(Convert.ToString(lst[0].PageName));
                        
                        /*Sachin Chauhan Start : 05 01 2015*/
                        string strContent = Convert.ToString(lst.FirstOrDefault(x => x.LanguageId.Equals(GlobalFunctions.GetLanguageId())).WebsiteContent).Replace("~/", GlobalFunctions.GetVirtualPath());
                        /*Sachin Chauhan End : 05 01 2015*/

                        List<StaticTextKeyBE> lstKey = new List<StaticTextKeyBE>();
                        StaticTextKeyBE objBEs = new StaticTextKeyBE(); 
                        objBEs.LanguageId = Convert.ToInt16(lst[0].LanguageId);
                        objBEs.StaticTextId = Convert.ToInt16(lst[0].StaticPageId);
                        lstKey = StaticTextKeyBL.GetAllKeys(Constants.USP_GetStaticTextDetails, objBEs, "A");
                        if (lstKey != null)
                        {
                            if (lstKey.Count > 0)
                            {
                                for (int i = 0; i < lstKey.Count; i++)
                                {
                                    strContent = strContent.Replace(Convert.ToString(lstKey[i].StaticTextKey), Convert.ToString(lstKey[i].StaticTextValue));
                                }
                            }
                        }
                        /*Sachin Chauhan Start : 25-11-15 : Replaced square brackets which were place holders for site keys*/
                        ltrContent.Text = HttpUtility.HtmlDecode((strContent.Replace("[", "").Replace("]", "")));
                        /*Sachin Chauhan end : 25-11-15*/
                    }
                    else
                    { imgBannerImage.Visible = false; Response.Redirect(GlobalFunctions.GetVirtualPath() + "info/page-not-found", true); }
                }
                else
                { imgBannerImage.Visible = false; Response.Redirect(GlobalFunctions.GetVirtualPath() + "info/page-not-found", true); }
            }
            catch { }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableViewState = false;
        }
    }
}