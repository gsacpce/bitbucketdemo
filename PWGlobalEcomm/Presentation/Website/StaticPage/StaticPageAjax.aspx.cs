﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Presentation
{
    public class StaticPage_StaticPageAjax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string ValidateMethod(string Param1)
        {
            try
            {
                string str = "";
                List<StaticPageManagementBE> lst = new List<StaticPageManagementBE>();
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBE.PageName = Param1;
                lst = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "PageUrl");
                if (lst != null && lst.Count > 0)
                {
                    str = lst[0].PageTitle;
                    str = str + "^" + lst[0].WebsiteContent;
                }
                return str;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}