﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml;
using System.Net;
using System.Web.Services;


namespace Presentation
{
    public class MasterPage : System.Web.UI.MasterPage
    {
        public string host = GlobalFunctions.GetVirtualPath();
        public string strProductSearch = string.Empty;
        public string strSearchTitle = string.Empty;
        public string strSearchButtonText = string.Empty;
        public string strSearchAlertMessage = string.Empty;
        public string strUserNamenmsg = "";
        public bool isEmailValid = false;
        public string Basket_HaveAnAccount, Basket_Email, Basket_Password, Basket_BeforeYouCheckout, Basket_SignIntoYourAccount,
                      Basket_ForgotPassword, Basket_NewToOurSite, Basket_OrderConfirmationMessage, Basket_CheckoutAsGuest,
                      Basket_InvalidEmailPassword, MsgEmailBlank, MsgPasswordBlank, MsgInvalidEmail, MsgInvalidUsenamePass, strInvalidEmail, strAlreadySubscribed, strNewSubscribed, strEnterDetails;

        public string ContactusTitle, PhoneTitle, EmailTitle, PhoneValue, EmailValue, ContactPersonTitle, ContactPersonValue, HelpDeskBodyTitle, HelpDeskTextValue, ModalConfirmTitle, Close_Btn, strSuccess, strWarning, strError, btn_OK, GCBalanceTitle, strInvalidGCCode, strRemainingCGBalance;

        protected global::System.Web.UI.WebControls.Literal ltlBreadCrumb, litTypekit, ltMsgCookie, ltCloseCookie, ltAcceptCookie;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor EmailAnchor;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divContactPhone, divContactHelpDeskBody, divContactPerson1, divContactEmail
            , spnHeadingCookie;
        protected global::System.Web.UI.WebControls.TextBox txtGiftCertificateCode;
        protected global::System.Web.UI.WebControls.Label lblGCBalanceNote, lblGCCode;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ModalGCBalance;
        protected global::System.Web.UI.WebControls.Button btnGetGCBalance;

        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        /*Sachin Chauhan End : 22 02 2016 */

        /* START - Added By Snehal For Google Referrel URL - 05 12 2016*/
        public string AbsolutePath = string.Empty;
        public string URLReturnPath = GlobalFunctions.GetSetting("GoogleReferrelURL");
        public string UserType = GlobalFunctions.GetSetting("GoogleUserType");
        public static StoreReferral objreferral;
        public static List<StoreReferral.referralurlBE> lstreferral;
        protected string UFName, ULname, ULanguage, UUserType, UCurrency, URegistrationCountry, UPoints,UEmailId,CommunigatorGId;
        /* END */
        string INDEEDSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\IndeedBrandStore_SSOLogs";

        //protected override void Render(HtmlTextWriter writer)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    StringWriter sw = new StringWriter(sb);
        //    HtmlTextWriter hWriter = new HtmlTextWriter(sw);
        //    base.Render(hWriter);
        //    string html = sb.ToString();
        //    html = Regex.Replace(html, "<input[^>]*id=\"(__PREVIOUSPAGE)\"[^>]*>", string.Empty, RegexOptions.IgnoreCase);
        //    html = Regex.Replace(html, "<input[^>]*id=\"(__VIEWSTATE)\"[^>]*>", string.Empty, RegexOptions.IgnoreCase);
        //    writer.Write(html);
        //}

        public string Host
        {
            get
            {
                return host;
            }
        }
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 06-08-15
        /// Scope   : Page_PreInit event of the page
        /// Modified by : Sachin Chauhan
        /// Modified Date : 22 02 2016
        /// Modify Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.Url.Host.ToLower().Contains("localhost"))
            {
                /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
                #region "CSRF prevention code"
                try
                {
                    //First, check for the existence of the Anti-XSS cookie
                    var requestCookie = Request.Cookies[AntiXsrfTokenKey];
                    Guid requestCookieGuidValue;

                    //If the CSRF cookie is found, parse the token from the cookie.
                    //Then, set the global page variable and view state user
                    //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
                    //method.
                    if (requestCookie != null
                    && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
                    {
                        //Set the global token variable so the cookie value can be
                        //validated against the value in the view state form field in
                        //the Page.PreLoad method.
                        _antiXsrfTokenValue = requestCookie.Value;

                        //Set the view state user key, which will be validated by the
                        //framework during each request
                        Page.ViewStateUserKey = _antiXsrfTokenValue;
                    }
                    //If the CSRF cookie is not found, then this is a new session.
                    else
                    {
                        //Generate a new Anti-XSRF token
                        _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                        //Set the view state user key, which will be validated by the
                        //framework during each request
                        Page.ViewStateUserKey = _antiXsrfTokenValue;

                        //Create the non-persistent CSRF cookie
                        var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                        {
                            //Set the HttpOnly property to prevent the cookie from
                            //being accessed by client side script
                            HttpOnly = true,

                            //Add the Anti-XSRF token to the cookie value
                            Value = _antiXsrfTokenValue
                        };

                        //If we are using SSL, the cookie should be set to secure to
                        //prevent it from being sent over HTTP connections
                        if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                            responseCookie.Secure = true;

                        //Add the CSRF cookie to the response
                        Response.Cookies.Set(responseCookie);
                    }

                    Page.PreLoad += master_Page_PreLoad;
                }
                catch (Exception ex)
                {
                    Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
                    Exceptions.WriteExceptionLog(ex);
                    //throw;
                }
                #endregion
                /*Sachin Chauhan End : 22 02 2016*/
            }

            try
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();

                if (GlobalFunctions.GetCurrencyId() == 0 || GlobalFunctions.GetCurrencyId() == null)
                {
                    GlobalFunctions.SetCurrencyId(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyId);
                    GlobalFunctions.SetCurrencySymbol(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencySymbol);
                    GlobalFunctions.SetLanguageId(objStoreBE.StoreLanguages.FirstOrDefault(x => x.IsDefault == true).LanguageId);
                }

                if (GlobalFunctions.GetLanguageId() == 0 || GlobalFunctions.GetLanguageId() == null)
                    GlobalFunctions.SetLanguageId(objStoreBE.StoreLanguages.FirstOrDefault(x => x.IsDefault.Equals(true)).LanguageId);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date : 22 02 2016
        /// Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.Url.Host.ToLower().Contains("localhost"))
            {

                #region
                //During the initial page load, add the Anti-XSRF token and user
                //name to the ViewState
                if (!IsPostBack)
                {
                    //Set Anti-XSRF token
                    ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

                    //If a user name is assigned, set the user name
                    //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                    if (Session["ContextUserGUID"] == null)
                        Session["ContextuserGUID"] = Guid.NewGuid().ToString();

                    ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
                }
                //During all subsequent post backs to the page, the token value from
                //the cookie should be validated against the token in the view state
                //form field. Additionally user name should be compared to the
                //authenticated users name
                else
                {
                    //Validate the Anti-XSRF token
                    if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                        try
                        {
                            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())
                                Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                            Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed due to unhandled exception.");
                            //throw;
                        }
                }
                #endregion
            }
        }

        protected void BindBreadCrumb()
        {
            try
            {
                ltlBreadCrumb.Visible = false;
                string VirutalPath = "";
                if (Request.RequestContext.RouteData.Route != null)
                {
                    VirutalPath = ((System.Web.Routing.PageRouteHandler)(((System.Web.Routing.Route)(Request.RequestContext.RouteData.Route)).RouteHandler)).VirtualPath;
                }

                List<BreadCrumbManagementBE> lstBreadCrumbs = BreadCrumbManagementBL.GetBreadCrumb();
                int LanguageId = GlobalFunctions.GetLanguageId() == 0 ? 1 : GlobalFunctions.GetLanguageId();

                lstBreadCrumbs = lstBreadCrumbs.Where(b => b.LanguageId == LanguageId).ToList();
                string[] Uri = Request.Url.Segments;
                //string Home = "";
                ltlBreadCrumb.Text = "<section id=\"HEADER_BREADCRUMBS\" ><div class=\"hidden-xs container\"><div class=\"row\"><div class=\"col-xs-12\"><ul class=\"breadcrumb customBreadcrumbBg\">";
                BreadCrumbManagementBE ParentPage = lstBreadCrumbs.Where(b => b.ParentBreadCrumbId == 0).FirstOrDefault();
                string targetPage = "";
                foreach (string Uri1 in Uri)
                {
                    if (Uri1.Contains("aspx"))
                    {
                        targetPage = Uri1;
                        targetPage = targetPage.Replace(".aspx", "");
                        if (targetPage.Contains("/"))
                        {
                            targetPage = targetPage.Replace("/", "");
                        }
                    }
                }

                if (!VirutalPath.ToLower().Contains("homepage.aspx"))
                {
                    if (VirutalPath.ToLower().Contains("staticpage.aspx"))
                    {
                        #region "staticpage"
                        if (lstBreadCrumbs != null)
                        {
                            if (lstBreadCrumbs.Count() > 0)
                            {

                                ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href=" + host + ParentPage.Link + ">" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[2].ToString())) + "</li></ul>";
                                ltlBreadCrumb.Visible = true;
                            }
                        }
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("categorylisting.aspx"))
                    {
                        #region "categorylisting"
                        int i = 3;
                        if (Request.Url.ToString().Contains("bav2.cwwws") || Request.Url.ToString().Contains("brand-estore")) { i = 4; }

                        if (Uri.Length == i)
                        {

                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                            ltlBreadCrumb.Visible = true;
                        }
                        else if (Uri.Length == (i + 1))
                        {
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='javascript:void(0);'/>" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                            ltlBreadCrumb.Visible = true;
                        }
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("productlisting.aspx"))
                    {
                        #region "productlisting"
                        int i = 3;
                        if (Request.Url.ToString().Contains("bav2.cwwws") || Request.Url.ToString().Contains("brand-estore")) { i = 4; }

                        if (Uri.Length == i)
                        {
                            if (Uri[1].ToString().ToLower().Contains("search"))
                            {
                                ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                            }
                            else
                            {
                                ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                            }
                        }
                        else if (Uri.Length == (i + 1))
                        {
                            /*Sachin Chauhan Start : 08 03 2016 : Changing logic of breadcrum getting built for category and or subcategory scenarios*/
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";

                            /*Sachin Chauhan Start : 03 03 2016 :*/
                            //if (Request.Url.AbsoluteUri.ToLower().Contains("subcategory"))
                            //    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + Request.Url + "'></a></li><li><a class=\"customBreadcrumb\" href='" + host + "Categories" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString()) + "</a></li><li class=\"active customBreadcrumbActive\">" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()) + "</li></ul>";

                            /*Sachin Chauhan End : 03 03 2016*/

                            /*Sachin Chauhan End : 08 03 2016*/
                        }
                        else if (Uri.Length == (i + 2))
                        {
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString())) + "'>" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString())) + "</a><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                        }

                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("productdetails.aspx"))
                    {
                        #region "productdetails"
                        int i = 3;
                        if (Request.Url.ToString().Contains("bav2.cwwws") || Request.Url.ToString().Contains("brand-estore")) { i = 4; }
                        if (Uri.Length == i)
                        {
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[3].Replace("/", "").ToString())) + "</li></ul>";
                        }
                        else if (Uri.Length == (i + 1))
                        {
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "Category" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString()) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                        }
                        else if (Uri.Length == (i + 2))
                        {
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString()) + "</a><li><a class=\"customBreadcrumb\" href='" + host + "SubCategory" + "/" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3])) + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").Split('_').Last().ToString())) + "</li></ul>";
                        }
                        else if (Uri.Length == (i + 3))
                        {
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li>"
                           + "<li><a class=\"customBreadcrumb\"  href='" + host + "SubCategories" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 4].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 4].Replace("/", "").ToString()) + "</a></li>"
                           + "<li><a class=\"customBreadcrumb\"  href='" + host + "SubCategories" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString()) + "</a></li>"
                           + "<li><a class=\"customBreadcrumb\"  href='" + host + "SubCategory" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3]) + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString()) + "</a></li>"
                           + "<li class=\"active customBreadcrumbActive\">'" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()) + "'</li></ul>";

                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("basket.aspx"))
                    {
                        #region "basket"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "Basket");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("payment.aspx"))
                    {
                        #region "payment"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "CheckOut");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);

                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("CreditCardPayment.aspx"))
                    {
                        #region "CreditCardPayment"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "CreditCardPayment");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }

                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("OrderConfirmation.aspx"))
                    {
                        #region "OrderConfirmation"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "OrderConfirmation");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href=" + host + a1.Link + "/>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + ">" + a1.BreadCrumbName + "'</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("OrderDetails.aspx"))
                    {
                        #region "OrderDetails"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "OrderDetails");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + ">" + c.BreadCrumbName + "'</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "/>" + a1.BreadCrumbName + "'</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + ">" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("pagenotfound.aspx"))
                    {
                        #region "pagenotfound"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "pagenotfound");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + System.Web.HttpUtility.UrlDecode(a.BreadCrumbName) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + System.Web.HttpUtility.UrlDecode(a.BreadCrumbName) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else
                    {
                        #region "Else"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == targetPage);
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                    ltlBreadCrumb.Visible = true;
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                    ltlBreadCrumb.Visible = true;
                                }
                            }
                        }
                        #endregion
                    }
                    ltlBreadCrumb.Text += "</div></div></div></section>";
                }
                else
                {
                    ltlBreadCrumb.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-07-15
        /// Scope   : Page_Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store");
                Response.AddHeader("Pragma", "no-cache");


                // BindBreadCrumb();
                Literal litCSSnJS = (Literal)FindControl("litCSSnJS");
                // HiddenField hdnstorename = (HiddenField)FindControl("hdnstorename");
                litCSSnJS.Text = "<!-- core CSS --> " +
                     "<link href='" + host + "css/bootstrap.min.css' rel='stylesheet'>" +
                     "<link href='" + host + "css/font-awesome.min.css' rel='stylesheet'>" +
                    // "<link href='" + host + "css/responsive.css' rel='stylesheet'>" +
                    // "<!-- SmartMenus jQuery Bootstrap Addon CSS -->" +
                    //  "<link href='" + host + "css/jquery.smartmenus.bootstrap.css' rel='stylesheet'>" +
                     "<link href='" + host + "css/bootstrap-slider.css' rel='stylesheet'>" +
                     "<!--[if lt IE 9]>" +
                     "<script src='" + host + "js/html5shiv.js'></script>" +
                     "<script src='" + host + "js/respond.min.js'></script>" +
                     "<![endif]-->" +
                    //"<link href='" + host + "css/font.css' rel='stylesheet'> " +
                     "<link href='" + host + "css/bootstrap-theme.css' rel='stylesheet'>" +
                    //"<link href='" + host + "css/style.css' rel='stylesheet'>" +
                     "<link href='" + host + "css/modified.css' rel='stylesheet'>" +
                     "<link href='" + host + "css/media-query.css' rel='stylesheet'>" +
                     "<link href='" + host + "css/animate.css' rel='stylesheet'> " +
                     "<link href='" + host + "css/font-awesome.min.css' rel='stylesheet'> " +
                     "<link href='" + host + "CSS/jquery.flipcountdown.css' rel='stylesheet'> "
                     ;
                /*Sachin Chauhan : 28 08 2015 : Commented below slide show related CSS and will be applied from Home Page only
                    /*"<link href='" + host + "css/owl.carousel.css' rel='stylesheet'> " +
                     "<link href='" + host + "css/owl.theme.css' rel='stylesheet'> " +
                     "<link href='" + host + "css/owl.transitions.css' rel='stylesheet'> " +
                     "<link href='" + host + "css/nerveslider.css' rel='stylesheet'> " +
                     "<link href='" + host + "css/font-awesome.min.css' rel='stylesheet'> " +
                      */
                /*Sachin Chauhan : 28 08 2015*/

                //Commented by Anoop as it is creating problem in displaying price filter on product listing page
                //"<script src='" + host + "js/jquery-ui-1.10.2.min.js'></script>";
                #region code removed for google

                //Literal litJS = (Literal)FindControl("litJS");
                //litJS.Text =

                //    "<script type='text/javascript' src='" + host + "js/respond.js'></script> " +
                //    "<script src='" + host + "js/jquery.js'></script>" +
                //    "<script src='" + host + "js/bootstrap.min.js'></script>" +
                //    "<script type='text/javascript' src='" + host + "js/typeahead.bundle.js'></script>" +
                //   "<script src='" + host + "js/custom.js'></script>" +
                //    // "<!-- SmartMenus jQuery plugin -->" +
                //    //"<script type='text/javascript' src='" + host + "js/jquery.smartmenus.js'></script>" +
                //    // "<!-- SmartMenus jQuery Bootstrap Addon -->" +
                //    // "<script type='text/javascript' src='" + host + "js/jquery.smartmenus.bootstrap.js'></script>" +

                //    "<script type='text/javascript' src='" + host + "js/ie10-viewport-bug-workaround.js'></script> " +
                //    "<script type='text/javascript' src='" + host + "js/bootstrap-slider.js'></script>" +
                //     "<script type='text/javascript' src='" + host + "js/wow.min.js'></script>" +
                //      "<script type='text/javascript' src='" + host + "JS/jquery.flipcountdown.js'></script>" +
                //      "<script type='text/javascript' src='" + host + "js/StoreMaster.js'></script>";//Store Master JS should be load at the last


                #endregion

                /*Sachin Chauhan : 28 08 2015 : Commented below slide show related CSS and will be applied from Home Page only /*
                    "<script type='text/javascript' src='" + host + "js/jquery.nerveSlider.min.js'></script> " +
                    "<script type='text/javascript' src='" + host + "js/owl.carousel.js'></script> " +
                    */
                /*Sachin Chauhan : 28 08 2015*/

                //Code Added by Vinit Falgunia for TypeKit
                try
                {
                    if (File.Exists(GlobalFunctions.GetPhysicalFontsPath() + "Typekit.txt"))
                    {
                        string strTypeKit = System.IO.File.ReadAllText(GlobalFunctions.GetPhysicalFontsPath() + "Typekit.txt");
                        if (!string.IsNullOrEmpty(strTypeKit))
                        {
                            litTypekit.Text = "<script src=" + strTypeKit + "></script> " +
                                            "<script>try { Typekit.load(); } catch (e) { }</script>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
                //PRAJWAL *10/14/2015 - Google Analytics Code
                StoreBE objStoreBE = StoreBL.GetStoreDetails();

                #region "Added by Sripal"
                if (Session["User"] == null)
                {
                    string strStoreAccess = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CR_StoreAccess").FeatureValues.FirstOrDefault(y => y.IsEnabled == true).FeatureValue;
                    if (strStoreAccess.ToLower() == "mandatory login")
                    {
                        if (!Request.Url.AbsolutePath.ToLower().Contains("login") && !Request.Url.AbsolutePath.ToLower().Contains("signin") && !Request.Url.AbsolutePath.ToLower().Contains("userregistration") && !Request.Url.AbsolutePath.ToLower().Contains("forgotpassword"))
                        {
                            //GlobalFunctions.ShowModalAlertMessages(Request.Url.AbsolutePath.ToLower())
                            if (objStoreBE.StoreId == 532 || objStoreBE.StoreId == 519)
                            {
                                Response.Redirect("https://www.indeedstore.com");
                                return;
                            }
                            else
                            {
                                Response.RedirectToRoute("login-page");
                                return;
                            }
                        }
                    }
                    else
                    {
                        #region Commented by Snehal for Indeed - 17 03 2017
                        /*Added by Sripal for disabling Guest points*/
                        /*
                        if (GlobalFunctions.IsPointsEnbled())
                        {
                            if (!Request.Url.AbsolutePath.ToLower().Contains("login") && !Request.Url.AbsolutePath.ToLower().Contains("signin") && !Request.Url.AbsolutePath.ToLower().Contains("userregistration") && !Request.Url.AbsolutePath.ToLower().Contains("forgotpassword"))
                            {
                                Response.RedirectToRoute("login-page");
                                return;
                            }
                        }*/
                        /*End by Sripal for disabling Guest points*/
                        #endregion
                        if (Session["IsAnonymous"] == null)
                        {
                            Response.RedirectToRoute("login-page");
                            return;
                        }
                        //else
                    }
                }
                #endregion
                if (!Request.Url.ToString().ToLower().Contains("localhost"))
                    ApplyGoogleAnalyticsCode(objStoreBE);
                #region commented by vikram For GA
                //Int16 curryId = GlobalFunctions.GetCurrencyId();
                //string codeGA = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == curryId).GACode;
                //Literal litGA = (Literal)FindControl("litGA");
                //litGA.Text = "<script type='text/javascript'>" +
                //                "var _gaq = _gaq || [];" +
                //                "_gaq.push(['_setAccount', '" + codeGA + "']);" +
                //                "_gaq.push(['_trackPageview']);" +
                //                "(function () {" +
                //                    "var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;" +
                //                    "ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';" +
                //                    "var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);" +
                //                "})();" +
                //             "</script>";

                //PRAJWAL *10/14/2015 - Google Analytics Code
                #endregion
                BindResourceData();

                #region "added by Sripal"
                StoreBE lstStoreDetail = new StoreBE();
                lstStoreDetail = StoreBL.GetStoreDetails();
                FeatureBE PL_DisplaytypeFeature = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_enablecookie");

                if (PL_DisplaytypeFeature != null)
                {
                    if (PL_DisplaytypeFeature.FeatureValues[0].IsEnabled)
                    {

                        HttpCookie myCookieExist;// = new HttpCookie("CookieEnabled");
                        string storename = lstStoreDetail.StoreName;
                        myCookieExist = Request.Cookies[storename];
                        if (myCookieExist != null)
                        {
                            if (myCookieExist.Name != storename)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:ShowCookieDiv();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:hideCookieDiv();", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:ShowCookieDiv();", true);
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:hideCookieDiv();", true);
                    }
                }
                #endregion

                #region Added By Snehal 05 12 2016 - For Google Referrel URL
                string source = "";
                if (Request.QueryString["Source"] != null)
                {
                    Exceptions.WriteInfoLog("Source : " + Convert.ToString(Request.QueryString["Source"]));
                    source = Convert.ToString(Request.QueryString["Source"]);
                }

                if (HttpContext.Current.Request.UrlReferrer != null || source != null)
                {
                    lstreferral = StoreBL.Getreferralurl();
                    if (HttpContext.Current.Request.UrlReferrer != null)
                    {
                        AbsolutePath = HttpContext.Current.Request.UrlReferrer.AbsoluteUri.ToLower();
                    }
                    else if (source == "Internal")
                    {
                        AbsolutePath = "https://multilang.yourmerchandise.com/googleinternal";
                    }
                    else if (source == "Marketing")
                    {
                        AbsolutePath = "https://multilang.yourmerchandise.com/GoogleMarketing";
                    }

                    Exceptions.WriteInfoLog("url" + AbsolutePath);
                    if (lstreferral != null)
                    {
                        if (lstreferral.Count > 0)
                        {
                            StoreReferral.referralurlBE objref = lstreferral.FirstOrDefault(x => x.ReferralURL.ToLower() == AbsolutePath.Trim().ToLower());
                            if (objref != null && Session["ReferrelURL"] == null)
                            {
                                Session["ReferrelURL"] = true;
                                Session["referralUsertype"] = objref.UsertypeID;
                                Response.RedirectToRoute("login-page");

                            }
                        }

                    }


                }
                #endregion

                if (Session["IndeedSSO_User"] != null)
                {
                    Exceptions.WriteSSOLog("IndeedSSO_User NOT NULL Master Page", INDEEDSSOLogFolderPath);
                }
                UserBE lstUser = new UserBE();
                lstUser = Session["User"] as UserBE;
                if (Session["User"] != null)
                {
                    Exceptions.WriteSSOLog("User Session NOT NULL " + lstUser.EmailId, INDEEDSSOLogFolderPath);
                }
                List<CurrencyBE> LstObjCurrBE = new List<CurrencyBE>();
                LstObjCurrBE = CurrencyBL.GetAllCurrencyDetails();
                UCurrency = Convert.ToString(LstObjCurrBE.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode);
                ULanguage = Convert.ToString(GlobalFunctions.GetLanguageId());
                UUserType = "1";
                FeatureBE CommunigatorGroupId = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "communigatorgroupid");
                if (CommunigatorGroupId != null)
                {
                    CommunigatorGId = CommunigatorGroupId.FeatureValues[0].FeatureDefaultValue;
                }
                if (Session["User"] != null)
                {
                    string StrUCurrenc = string.Empty;
                    UserBE ObjUser = Session["User"] as UserBE;
                    if (ObjUser != null)
                    {
                        List<CurrencyBE> ObjCurrBE = new List<CurrencyBE>();
                        ObjCurrBE = CurrencyBL.GetAllCurrencyDetails();
                        List<CountryBE> ObjCountry = new List<CountryBE>();
                        ObjCountry = CountryBL.GetAllCountries();
                        UEmailId = ObjUser.EmailId;
                        UFName = ObjUser.FirstName;
                        ULname = ObjUser.LastName;
                        ULanguage = Convert.ToString(GlobalFunctions.GetLanguageId());
                        UUserType = Convert.ToString(ObjUser.UserTypeID);
                        UCurrency = Convert.ToString(ObjCurrBE.FirstOrDefault(x => x.CurrencyId == ObjUser.CurrencyId).CurrencyCode);
                        URegistrationCountry = ObjCountry.Find(x => x.CountryId == Convert.ToInt16((ObjUser.PredefinedColumn8))).CountryName;
                        UPoints = Convert.ToString(ObjUser.Points);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 4-09-15
        /// Scope   : BindResourceData of the Master page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strInvalidEmail = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "IsMarketing_InvalidEmail").ResourceValue;
                        strAlreadySubscribed = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "IsMarketing_AlreadySubscribed").ResourceValue;
                        strNewSubscribed = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "IsMarketing_NewSubscribed").ResourceValue;
                        strEnterDetails = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "IsMarketing_EnterDetails").ResourceValue;

                        strProductSearch = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_Watermark_Text").ResourceValue;
                        strSearchTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_Popup_Title").ResourceValue;
                        strSearchButtonText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_Button_Text").ResourceValue;
                        strSearchAlertMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_Blank_Text_Alert_Message").ResourceValue;

                        ContactusTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_ContactusTitle").ResourceValue;
                        EmailTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Master_ContactUs_Email").ResourceValue;
                        PhoneTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Master_ContactUs_Phone").ResourceValue;
                        ContactPersonTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Master_ContactUs_ContactPerson").ResourceValue;
                        HelpDeskBodyTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Master_ContactUs_HelpDeskText").ResourceValue;
                        ModalConfirmTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Master_ModalConfirmTitle").ResourceValue;
                        Close_Btn = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Close_Btn").ResourceValue;
                        strSuccess = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Success").ResourceValue;
                        strWarning = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Warning_Text").ResourceValue;
                        btn_OK = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_OK_Text").ResourceValue;
                        strError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Error_Text").ResourceValue;

                        GCBalanceTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_GCBalance_Text").ResourceValue;
                        lblGCCode.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_GCCodeEnter_Text").ResourceValue;
                        btnGetGCBalance.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_CheckGC_Balance_Text").ResourceValue;
                        strInvalidGCCode = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_InvalidGCCode_Msg").ResourceValue;
                        strRemainingCGBalance = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RemainingGCBalance_Msg").ResourceValue;

                        string HelloMsg = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_hello_text").ResourceValue);
                        string PointBalanceText = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_your_point_balance").ResourceValue);

                        if (Session["User"] != null)
                        {
                            UserBE objBEs = Session["User"] as UserBE;
                            if (GlobalFunctions.IsPointsEnbled())
                            {
                                strUserNamenmsg = HelloMsg + " " + objBEs.FirstName;
                                strUserNamenmsg = strUserNamenmsg + " " + PointBalanceText + " " + Convert.ToString(objBEs.Points);
                            }
                        }

                        Basket_HaveAnAccount = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_HaveAnAccount").ResourceValue;
                        Basket_Email = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Email").ResourceValue;
                        Basket_Password = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Password").ResourceValue;
                        Basket_BeforeYouCheckout = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_BeforeYouCheckout").ResourceValue;
                        Basket_SignIntoYourAccount = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_SignIntoYourAccount").ResourceValue;
                        Basket_ForgotPassword = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_ForgotPassword").ResourceValue;
                        Basket_NewToOurSite = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_NewToOurSite").ResourceValue;
                        Basket_OrderConfirmationMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_OrderConfirmationMessage").ResourceValue;
                        Basket_CheckoutAsGuest = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_CheckoutAsGuest").ResourceValue;

                        MsgPasswordBlank = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password_Req_Message").ResourceValue;
                        MsgEmailBlank = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Valid_Email_Address_Message").ResourceValue;
                        MsgInvalidEmail = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ValidEmail_Message").ResourceValue;
                        MsgInvalidUsenamePass = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_InvalidUsernamePassword_Message").ResourceValue;
                        ReadContactUsData();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 1-02-15
        /// Scope   : BindResourceData of the Master page
        /// </summary>
        /// <returns></returns>
        protected void ReadContactUsData()
        {
            try
            {
                StoreBE.ContactBE objContactBE = new StoreBE.ContactBE();
                objContactBE.Action = Convert.ToInt16(DBAction.Select);
                objContactBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                StoreBE.ContactBE ContactBE = StoreBL.GetContactInfo(objContactBE);
                EmailValue = Convert.ToString(ContactBE.HelpDeskEmail);
                PhoneValue = Convert.ToString(ContactBE.HelpDeskPhone);
                ContactPersonValue = Convert.ToString(ContactBE.HelpDeskContactPerson);
                HelpDeskTextValue = Convert.ToString(ContactBE.HelpDeskText);
                string mailTo = "mailto:" + Convert.ToString(EmailValue);
                EmailAnchor.Attributes.Add("href", mailTo);
                if (ContactPersonValue == "")
                { ContactPersonTitle = ""; divContactPerson1.Attributes.Add("style", "display:none"); }
                else
                { divContactPerson1.Attributes.Add("style", "display:block"); }
                if (HelpDeskTextValue == "")
                { HelpDeskBodyTitle = ""; divContactHelpDeskBody.Attributes.Add("style", "display:none"); }
                else
                { divContactHelpDeskBody.Attributes.Add("style", "display:block"); }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 15-01-15
        /// Scope   : BindResourceData of the Master page
        /// </summary>
        /// <returns></returns>
        protected void ApplyGoogleAnalyticsCode(StoreBE objStoreBE)
        {
            try
            {
                //Int16 curryId = GlobalFunctions.GetCurrencyId();
                //string codeGA = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == curryId).GACode;
                //string codeGA = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).GACode;
                if (!string.IsNullOrEmpty(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).GACode))
                {
                    Literal litGA = (Literal)FindControl("litGA");

                    //StringBuilder strBuilder = new StringBuilder();
                    string data = "";
                    data = data + " <script>";
                    data = data + " (function(i, s, o, g, r, a, m) {";
                    data = data + "  i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function() {";
                    data = data + "    (i[r].q = i[r].q || []).push(arguments)";
                    data = data + " }, i[r].l = 1 * new Date(); a = s.createElement(o),";
                    data = data + "m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)";
                    data = data + " })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');";
                    data = data + "   ga('create', '" + objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).GACode + "', 'auto');";
                    data = data + " ga('send', 'pageview');";
                    data = data + "</script>";
                    litGA.Text = data;
                    //strBuilder.Append(data);
                    //litGA.Text = Convert.ToString(strBuilder);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author      : ShriGAnesh Singh
        /// Date        : 04 May 2016
        /// Description : To validate and return the Balance of the entered Gift Certificate code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGetGCBalance_OnClick(object sender, EventArgs e)
        {
            try
            {
                GenerateGiftCertificateXML();
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:ShowGCBalance();", true);
                ModalGCBalance.Style.Add("display", "block");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void GenerateGiftCertificateXML()
        {
            try
            {
                StoreBE lstStoreDetail = new StoreBE();
                lstStoreDetail = StoreBL.GetStoreDetails();

                //txtGiftCertificateCode.Text = "0324-D49C-2615-88A2";
                #region CheckBalance
                XElement xCheckBalanceElement;



                //if (Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode).ToLower() != "gbp")
                //{
                xCheckBalanceElement = new XElement("CheckBalance", new XElement("CertificateReference", txtGiftCertificateCode.Text.Trim())
                                                              , new XElement("Currency", Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode))
                                                              , new XElement("DivisionID", Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId)));
                //}
                //else
                //{
                //    xCheckBalanceElement = new XElement("CheckBalance", new XElement("CertificateReference", txtGiftCertificateCode.Text.Trim())
                //                                             , new XElement("Currency")
                //        //, new XElement("DivisionID", Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId)));
                //                                             , new XElement("DivisionID", Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId)));
                //} 


                #endregion
                //XElement xOrderElement;
                //xOrderElement = new XElement("GiftOrder", xOrderHeaderElement, xOrderLineElement);
                XDocument xOrderXML = new XDocument(xCheckBalanceElement);
                XmlNode OrderXMLNode = new XmlDocument().ReadNode(xOrderXML.Root.CreateReader());

                string str = "";
                if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
                {
                    PWGlobalEcomm.GiftCertificateServiceLive.GiftCertificateService giftcertificateservice = new PWGlobalEcomm.GiftCertificateServiceLive.GiftCertificateService();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                       ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    giftcertificateservice.Credentials = objNetworkCredentials;
                    GlobalFunctions.WriteGiftLog(Convert.ToString("GC REQUEST : " + xCheckBalanceElement));
                    str = giftcertificateservice.CheckBalance(OrderXMLNode);
                    GlobalFunctions.WriteGiftLog(Convert.ToString("GC Response : " + str));
                }
                else
                {
                    PWGlobalEcomm.GiftCertificateService.GiftCertificateService giftcertificateservice = new PWGlobalEcomm.GiftCertificateService.GiftCertificateService();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                        ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    giftcertificateservice.Credentials = objNetworkCredentials;
                    GlobalFunctions.WriteGiftLog(Convert.ToString("GC REQUEST : " + xCheckBalanceElement));
                    str = giftcertificateservice.CheckBalance(OrderXMLNode);
                    GlobalFunctions.WriteGiftLog(Convert.ToString("GC Response : " + str));
                }

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(str);

                XmlNodeList elemList = xd.GetElementsByTagName("GiftCertificateCheckResult");

                for (int i = 0; i < elemList.Count; i++)
                {
                    string attrVal = elemList[i].Attributes["Valid"].Value;
                    if (attrVal.ToLower() == "true")
                    {
                        XmlNodeList elemList1 = xd.GetElementsByTagName("Amount");
                        if (elemList1.Count > 0)
                        {
                            string strAmount = elemList1[0].InnerText;
                            lblGCBalanceNote.Text = strRemainingCGBalance + strAmount;
                        }
                    }
                    else
                    {
                        lblGCBalanceNote.Text = strInvalidGCCode;
                    }
                }
            }
            catch (Exception ex)
            {
                UserBE lstUserBE;
                lstUserBE = HttpContext.Current.Session["User"] as UserBE;
                GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In GenerateGiftCertificateXML", ex + "USER ID- " + lstUserBE.UserId + "USER EMAIL- " + lstUserBE.EmailId, "", "", false);
                Exceptions.WriteExceptionLog(ex);
            }
        }




    }
}