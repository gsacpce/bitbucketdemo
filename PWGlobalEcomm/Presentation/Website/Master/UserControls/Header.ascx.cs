﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using PWGlobalEcomm.BusinessLogic;
using System.Text;
using System.Web.UI.HtmlControls;
using System.IO;
using PWGlobalEcomm.DataAccess;
using System.Text.RegularExpressions;
using System.Configuration;



namespace Presentation
{
    public class Master_UserControl_Header : UserControl
    {
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divHeader, categories, divmobilesearch, HEADER_CATEGORIES;
        //protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ulCategoriesMenu, basket_counter, spnHeadingCookie;
        protected global::System.Web.UI.WebControls.Label lblMDBasketCounter;
        protected global::System.Web.UI.WebControls.Image imgMDBasket;
        protected global::System.Web.UI.WebControls.Image aSearch;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aMDBasket;

        protected global::System.Web.UI.WebControls.Literal ltMsgCookie, ltCloseCookie, ltAcceptCookie;
        public string host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.HtmlControls.HtmlButton product_hamburger_button, hamburger_button;
        protected global::System.Web.UI.WebControls.Literal ltlBreadCrumb;
        //int LanguageId = 1;
        string strSearchTitle = string.Empty;
        public string UFName, ULname, ULanguage, UUserType, UCurrency, URegistrationCountry,UPoints;
        public string strProductSearch = string.Empty;
        public string strSearchButtonText = string.Empty;
        public string strUpper = string.Empty;
        bool searchtype = false;

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 11-09-15
        /// Scope   : Page Load event of HEADER USER control
        /// </summary> 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                /*Sachin Chauhan : 01 09 2015 : Commented Old Prajwal's code*/
                //if (!IsPostBack)
                //{

                /*Added by Sripal*/
                aMDBasket.HRef = host + "checkout/ShoppingCart";
                bool bHeader = false;
                HiddenField hdnstorename = (HiddenField)FindControl("hdnstorename");

                StoreBE objStoreBE = (StoreBE)HttpRuntime.Cache["StoreDetails"];
                hdnstorename.Value = objStoreBE.StoreName;
                if (Session["User"] == null)
                {
                    string strStoreAccess = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CR_StoreAccess").FeatureValues.FirstOrDefault(y => y.IsEnabled == true).FeatureValue;
                    if (strStoreAccess.ToLower() == "mandatory login")
                    {
                        bHeader = true;
                    }
                    if (Session["ReferrelURL"] != null)
                    {
                        bHeader = true;
                    }

                }
                else
                {
                    string StrUCurrenc = string.Empty;
                    UserBE ObjUser = Session["User"] as UserBE;
                    List<CurrencyBE> ObjCurrBE = new List<CurrencyBE>();
                    ObjCurrBE = CurrencyBL.GetAllCurrencyDetails();
                    List<CountryBE> ObjCountry = new List<CountryBE>();
                    ObjCountry = CountryBL.GetAllCountries();
                    UFName = ObjUser.FirstName;
                    ULname = ObjUser.LastName;
                    ULanguage = Convert.ToString(GlobalFunctions.GetLanguageId());
                    UUserType = Convert.ToString(ObjUser.UserTypeID);
                    UCurrency = Convert.ToString(ObjCurrBE.FirstOrDefault(x => x.CurrencyId == ObjUser.CurrencyId).CurrencyName);
                    URegistrationCountry = ObjCountry.Find(x => x.CountryId == Convert.ToInt16(ObjUser.UserDeliveryAddress.Find(y => y.UserId == ObjUser.UserId).PreDefinedColumn8)).CountryName;
                    UPoints = Convert.ToString(ObjUser.Points);
                }
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_SearchSetting").FeatureValues[0].IsEnabled)
                {
                    searchtype = true;
                }
                else
                {
                    searchtype = false;
                }
                //if (!bHeader)
                //{
                BindResourceData();
                if (divHeader != null)
                {
                    List<object> lstAllHeaderFooterElements = HeaderFooterManagementBL.GetAllHeaderFooterElements(true, "header");
                    if (lstAllHeaderFooterElements != null && lstAllHeaderFooterElements.Count > 0)
                    {
                        foreach (HeaderFooterManagementBE headerItem in lstAllHeaderFooterElements)
                        {
                            string SectionName = "";
                            SectionName = headerItem.ConfigSectionName.Contains("menu") || headerItem.ConfigSectionName.Contains("Menu") ? headerItem.ConfigSectionName.Replace("gmcontent", "").Replace("gmContent", "") : headerItem.ConfigSectionName;

                            HtmlGenericControl gmContent = this.FindControl(SectionName) as HtmlGenericControl;
                            try
                            {
                                #region "Load Header Elements"
                                if (gmContent != null)
                                {
                                    switch (headerItem.ConfigElementName.ToLower())
                                    {
                                        #region "Site Logo"
                                        case "sitelogo":
                                            {

                                                string strSiteLogoDir;
                                                DirectoryInfo dirSiteLogo;
                                                FileInfo[] logoFileInfo;
                                                strSiteLogoDir = HttpContext.Current.Server.MapPath("~/Images/SiteLogo");

                                                gmContent.InnerHtml = "";
                                                dirSiteLogo = new DirectoryInfo(strSiteLogoDir);
                                                logoFileInfo = dirSiteLogo.GetFiles();
                                                gmContent.InnerHtml += "<a href='" + host + "index'>";
                                                foreach (FileInfo logoFile in logoFileInfo)
                                                {
                                                    if (logoFile.Name.ToLower().Contains("sitelogo"))
                                                    {
                                                        gmContent.InnerHtml += "<img class='logo' src='" + host + "Images/SiteLogo/" + logoFile.Name + "'>";

                                                        /*Sachin Chauhan : 12 02 2015 : Check if object is not null*/
                                                        //if (imgMDBasket != null)
                                                        // imgMDBasket.ImageUrl = host + "Images/UI/icon_basket.png";
                                                        /*Sachin Chauhan : 12 02 2015 : Check if object is not null*/
                                                        if (lblMDBasketCounter != null)
                                                            lblMDBasketCounter.Text = BindNotification();
                                                    }
                                                }


                                                strSiteLogoDir = HttpContext.Current.Server.MapPath("~/Images/SiteLogo/Mobile");
                                                dirSiteLogo = new DirectoryInfo(strSiteLogoDir);
                                                logoFileInfo = dirSiteLogo.GetFiles();
                                                if (logoFileInfo.Length > 0)
                                                {
                                                    foreach (FileInfo logoFile in logoFileInfo)
                                                    {
                                                        gmContent.InnerHtml += "<img class='logo-mobile' src='" + host + "Images/SiteLogo/Mobile/" + logoFile.Name + "'>";
                                                    }
                                                }


                                                gmContent.InnerHtml += "</a>";
                                                break;
                                            }
                                        #endregion

                                        #region "Site Links"
                                        case "sitelinks":
                                            {
                                                gmContent.InnerHtml = "";


                                                Master_UserControls_SiteLanguages siteLanguages = (Master_UserControls_SiteLanguages)Page.LoadControl("~/Master/UserControls/SiteLanguages.ascx");
                                                if (objStoreBE.StoreLanguages.Count > 1)
                                                {
                                                    //ddlLanguage.ID = "ddlLanguages";
                                                    PopulateLanguageDropDownList(siteLanguages, objStoreBE.StoreLanguages);
                                                    //ddlLanguage.AutoPostBack = true;
                                                    //ddlLanguage.Attributes.Add("style", "margin: 12px 0 12px 7px;");
                                                    //ddlLanguage.SelectedIndexChanged += ddlLanguage_SelectedIndexChanged;
                                                    //gmContent.Controls.Add(ddlLanguage);
                                                }

                                                Master_UserControls_SiteLinks ctrlSiteLinks = (Master_UserControls_SiteLinks)Page.LoadControl("~/Master/UserControls/SiteLinks.ascx");
                                                ctrlSiteLinks.LinkDisplayLocation = "header";
                                                ctrlSiteLinks.LinkDisplayStyle = "horizontal";
                                                if (objStoreBE.StoreLanguages.Count > 1)
                                                {
                                                    HtmlGenericControl ulHeaderLinks = ctrlSiteLinks.FindControl("ulHeaderLinks") as HtmlGenericControl;
                                                    HtmlGenericControl liLanguage = new HtmlGenericControl("li");
                                                    liLanguage.Controls.Add(siteLanguages);
                                                    ulHeaderLinks.Controls.AddAt(ulHeaderLinks.Controls.Count, liLanguage);
                                                }
                                                gmContent.Controls.Add(ctrlSiteLinks);

                                                if (!bHeader)
                                                {
                                                    #region
                                                    //DropDownList ddlLanguage = new DropDownList();
                                                    /*Sachin Chauhan Start : 09 02 2016 : Added below lines of code to access langauage & currency count for the store*/



                                                    /*Sachin Chauhan End : 09 02 2016 */



                                                    /*Sachin Chauhan Start : 09 02 2016 : Site Currency User control load in case of multi currency site */
                                                    /*Don't show currency dropdown for non relevent pages*/

                                                    /*Sachin Chauhan Start : 26 02 2016 : Finding actual page serving the url rewrite path*/
                                                    string vPath = String.Empty;
                                                    if (Request.RequestContext.RouteData.Route != null)
                                                    {
                                                        vPath = ((System.Web.Routing.PageRouteHandler)(((System.Web.Routing.Route)(Request.RequestContext.RouteData.Route)).RouteHandler)).VirtualPath;
                                                    }
                                                    /*Sachin Chauhan End : 26 02 2016*/

                                                    if (!vPath.ToLower().EndsWith("payment.aspx") && !vPath.ToLower().EndsWith("orderconfirmation.aspx"))// && !vPath.ToLower().Contains("staticpage.aspx"))
                                                    {
                                                        UserBE objUSerBE = new UserBE();
                                                        UserTypesBE objlstUserTypesBE = new UserTypesBE();
                                                        List<StoreBE.StoreCurrencyBE> objCur = new List<StoreBE.StoreCurrencyBE>();
                                                        List<StoreBE.StoreCurrencyBE> lstCurrencies = objStoreBE.StoreCurrencies.Where(x => x.IsActive.Equals(true)).ToList();
                                                        Master_UserControls_SiteCurrencies ctrlSiteCurrencies = (Master_UserControls_SiteCurrencies)Page.LoadControl("~/Master/UserControls/SiteCurrencies.ascx");
                                                        if (Session["User"] != null)
                                                        {
                                                            objUSerBE = HttpContext.Current.Session["User"] as UserBE;
                                                            List<StoreBE.StoreCurrencyBE> lstAllCurrencies = new List<StoreBE.StoreCurrencyBE>();
                                                            List<StoreBE.StoreCurrencyBE> lstDefaultCurrencies = new List<StoreBE.StoreCurrencyBE>();

                                                            if (objUSerBE.UserTypeID > 0)
                                                            {
                                                                objlstUserTypesBE = UserTypesBL.getCollectionItem(Constants.USP_ManageUserType_SAED, null, true);
                                                                if (objlstUserTypesBE.lstUserTypeCatalogue.Count > 0)
                                                                {
                                                                    List<UserTypesBE.UserTypeCatalogue> objUserTypeCatalogue = objlstUserTypesBE.lstUserTypeCatalogue.FindAll(x => x.IsActive == true && x.UserTypeID == objUSerBE.UserTypeID);
                                                                    objCur = (from a in lstCurrencies.AsEnumerable()
                                                                              where objUserTypeCatalogue.Any(xc => xc.CatalogueID.Equals(a.CatalogueId))
                                                                              select a).ToList();
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            List<StoreBE.StoreCurrencyBE> lstAllCurrencies = new List<StoreBE.StoreCurrencyBE>();
                                                            List<StoreBE.StoreCurrencyBE> lstDefaultCurrencies = new List<StoreBE.StoreCurrencyBE>();
                                                            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_usertype").FeatureValues[0].IsEnabled == true)
                                                            {
                                                                //filter out 
                                                                lstAllCurrencies = lstCurrencies;
                                                                objlstUserTypesBE = UserTypesBL.getCollectionItem(Constants.USP_ManageUserType_SAED, null, true);

                                                                List<UserTypesBE.UserTypeCatalogue> objUserTypeCatalogues = objlstUserTypesBE.lstUserTypeCatalogue.FindAll(x => x.UserTypeID == 1);
                                                                objCur = (from a in lstAllCurrencies.AsEnumerable()
                                                                          where objUserTypeCatalogues.Any(xc => xc.UserTypeID == 1 && xc.CatalogueID.Equals(a.CatalogueId))
                                                                          select a).ToList();
                                                            }
                                                        }
                                                        if (objCur != null && objCur.Count > 1)
                                                        {
                                                            //PopulateCurrencyDropDownList(lstCurrencies, ctrlSiteCurrencies);
                                                            HtmlGenericControl ulHeaderLinks = ctrlSiteLinks.FindControl("ulHeaderLinks") as HtmlGenericControl;
                                                            HtmlGenericControl liCurrency = new HtmlGenericControl("li");
                                                            string LoginType = Convert.ToString(ConfigurationManager.AppSettings["LoginType"]);
                                                            if (LoginType != "Country")
                                                            {
                                                                /* Condition added by Hardik on "23/Apr/2017" to hide currency dropdown for punchout based on setting [Enabled / Disabled] */
                                                                if (Convert.ToBoolean(Session["S_ISPUNCHOUT"]) == true && objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "hidecurrdropdown").FeatureValues[0].IsEnabled)
                                                                {

                                                                }
                                                                else
                                                                {
                                                                    liCurrency.Controls.Add(ctrlSiteCurrencies);
                                                                    ulHeaderLinks.Controls.AddAt(ulHeaderLinks.Controls.Count, liCurrency);
                                                                }
                                                            }
                                                        }
                                                    }

                                                    /*Sachin Chauhan End : 09 02 2016*/

                                                    #endregion
                                                }
                                                break;
                                            }
                                        #endregion
                                        case "categoriesmenu":
                                            {
                                                gmContent.InnerHtml = "";

                                                if (!bHeader)
                                                {
                                                    Master_UserControls_Menu ctrlCategoriesMenu = (Master_UserControls_Menu)Page.LoadControl("~/Master/UserControls/menu.ascx");
                                                    ulCategoriesMenu.InnerHtml = "";
                                                    ulCategoriesMenu.Controls.Add(ctrlCategoriesMenu);
                                                }//aSearch

                                                product_hamburger_button.Visible = false;
                                                categories.Visible = true;
                                                hamburger_button.Visible = false;
                                                basket_counter.Visible = false;
                                                if (!bHeader)
                                                {
                                                    product_hamburger_button.Visible = true;
                                                    categories.Visible = true;
                                                    hamburger_button.Visible = true;
                                                    basket_counter.Visible = true;
                                                }
                                                break;
                                            }

                                        case "sitesearch":
                                            {
                                                gmContent.InnerHtml = "";
                                                if (!bHeader)
                                                {
                                                    if (searchtype)
                                                    {
                                                        gmContent.InnerHtml = "<a id='aSearch' class='menuLink' href='javascript:void(0);'><i class='fa fa-search' aria-hidden='true'></i></a>";
                                                        gmContent.Attributes["class"] = gmContent.Attributes["class"] + " searchlink";
                                                        divmobilesearch.InnerHtml = " <a id='aSearch' class='headerLink' href='javascript:void(0);'><i class='fa fa-search' aria-hidden='true'></i></a><a data-toggle='modal' href='#ModalSearch'></a>";
                                                    }
                                                    else
                                                    {
                                                        gmContent.InnerHtml = "<div id='asitesearch' class='modal-body clearfix'><input id='txtKeyword'  type='text' onkeydown='SetFocusOnButton(event,btnSearch)' class='form-control typeahead customInput tt-query inputWidth'></div>";
                                                        gmContent.InnerHtml += "<button id='btnSearch' class='btn btn-primary customActionBtn' type='button'>" + strSearchButtonText + "</button>";
                                                        divmobilesearch.InnerHtml = "<div id='asitesearch_mobile' class='modal-body clearfix' >";
                                                        divmobilesearch.InnerHtml += "<input id='txtKeyword1'  type='text' onkeydown='SetFocusOnButton(event,btnSearch)' class='form-control typeahead customInput tt-query inputWidth'></div>";
                                                        divmobilesearch.InnerHtml += "<button id='btnSearch' class='btn btn-primary customActionBtn' type='button'>GO</button>";
                                                    }
                                                }
                                                break;
                                            }
                                        case "socialmedia":
                                            {
                                                gmContent.InnerHtml = "";
                                                if (!bHeader)
                                                {
                                                    Master_UserControls_SocialMedia ctrlSocialMedia = (Master_UserControls_SocialMedia)Page.LoadControl("~/Master/UserControls/SocialMedia.ascx");
                                                    gmContent.Controls.Add(ctrlSocialMedia);
                                                }
                                                break;
                                            }
                                        default:
                                            break;
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                            }
                            /*Sachin Chauhan : 08 09 2015 : Handeling Categories Menu Outer Markupcase */
                            /*Sachin Chauhan : 08 09 2015 */
                        }
                    }
                    BindBreadCrumb();
                    //if (!objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_CategoryDisplay").FeatureValues[1].IsEnabled)
                    //{
                    //    HEADER_CATEGORIES.Visible = true;
                    //}
                    //else
                    //{
                    //    HEADER_CATEGORIES.Visible = false;
                    //}
                    //}
                }
                ///}
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 02-11-15
        /// Scope   : Bind Product Count In Basket Icon
        /// </summary>
        /// <returns></returns>
        protected string BindNotification()
        {
            int intUserId = 0;
            int intCurrencyId = GlobalFunctions.GetCurrencyId();
            string UserSessionId = HttpContext.Current.Session.SessionID;
            if (Session["User"] != null)
            {
                UserBE objUserBE = (UserBE)Session["User"];
                intUserId = objUserBE.UserId;
            }
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("UserId", Convert.ToString(intUserId));
            dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
            dictionary.Add("UserSessionId", UserSessionId);
            int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_GetShoppingCartProducts, dictionary, true);
            return Convert.ToString(shopId);
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 4-09-15
        /// Scope   : BindResourceData of the Master page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strSearchTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_Popup_Title").ResourceValue;
                        spnHeadingCookie.InnerText = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "cookie_heading").ResourceValue);
                        ltMsgCookie.Text = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "cookie_text").ResourceValue);
                        ltCloseCookie.Text = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "cookie_close").ResourceValue);
                        ltAcceptCookie.Text = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "cookie_accept").ResourceValue);

                        strProductSearch = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_Watermark_Text").ResourceValue;
                        strSearchButtonText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_Button_Text").ResourceValue;


                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-08-15
        /// Scope   : it populates language dropdown
        /// Modified: Sachin Chauhan
        /// Date    : 21-11-15
        /// Scope   : Dropdown replaced with UL LI
        /// </summary>
        //private void PopulateLanguageDropDownList(DropDownList ddlLanguage,List<StoreBE.StoreLanguageBE> languages )
        private void PopulateLanguageDropDownList(Master_UserControls_SiteLanguages ulSiteLanguages, List<StoreBE.StoreLanguageBE> languages)
        {
            if (languages != null)
            {
                if (languages.Count() > 0)
                {
                    Repeater rptSiteLanguages = ulSiteLanguages.FindControl("rptSiteLanguage") as Repeater;
                    rptSiteLanguages.DataSource = languages;
                    rptSiteLanguages.DataBind();
                    /*Sachin Chauhan Start: 21 11 15*/
                    /*ddlLanguage.DataSource = languages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                    ddlLanguage.ClearSelection();
                    ddlLanguage.Items.FindByValue(Convert.ToString(GlobalFunctions.GetLanguageId())).Selected = true;
                    */
                    /*Sachin Chauhan End: 21 11 15*/
                }
            }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 09-02-16
        /// Scope   : Populate Multicurrency drop down list in top menu
        /// </summary>
        //private void PopulateCurrencyDropDownList(List<StoreBE.StoreCurrencyBE> lstCurrencies, Master_UserControls_SiteCurrencies ctrlSiteCurrencies)
        //{
        //    Repeater rptSiteCurrencies = ctrlSiteCurrencies.FindControl("rptSiteCurrencies") as Repeater;
        //    rptSiteCurrencies.DataSource = lstCurrencies;
        //    rptSiteCurrencies.DataBind();
        //}

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-08-15
        /// Scope   : ddlLanguage_SelectedIndexChanged
        /// </summary>
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlLanguage = sender as DropDownList;
            try
            {
                GlobalFunctions.SetLanguageId(Convert.ToInt16(ddlLanguage.SelectedValue));
                //Response.Redirect(host + "home/home.aspx");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindBreadCrumb()
        {
            try
            {

                ltlBreadCrumb.Visible = false;
                string VirutalPath = "";
                if (Request.RequestContext.RouteData.Route != null)
                {

                    VirutalPath = ((System.Web.Routing.PageRouteHandler)(((System.Web.Routing.Route)(Request.RequestContext.RouteData.Route)).RouteHandler)).VirtualPath;
                }

                List<BreadCrumbManagementBE> lstBreadCrumbs = BreadCrumbManagementBL.GetBreadCrumb();
                int LanguageId = GlobalFunctions.GetLanguageId() == 0 ? 1 : GlobalFunctions.GetLanguageId();

                lstBreadCrumbs = lstBreadCrumbs.Where(b => b.LanguageId == LanguageId).ToList();
                string[] Uri = Request.Url.Segments;
                //string Home = "";
                ltlBreadCrumb.Text = "<section id=\"HEADER_BREADCRUMBS\" ><div class=\"hidden-xs container\"><div class=\"row\"><div class=\"col-xs-12\"><ul class=\"breadcrumb customBreadcrumbBg\">";
                BreadCrumbManagementBE ParentPage = lstBreadCrumbs.Where(b => b.ParentBreadCrumbId == 0).FirstOrDefault();
                string targetPage = "";
                foreach (string Uri1 in Uri)
                {
                    if (Uri1.Contains("aspx"))
                    {
                        targetPage = Uri1;
                        targetPage = targetPage.Replace(".aspx", "");
                        if (targetPage.Contains("/"))
                        {
                            targetPage = targetPage.Replace("/", "");
                        }
                    }
                }

                if (!VirutalPath.ToLower().Contains("homepage.aspx"))
                {

                    if (VirutalPath.ToLower().Contains("staticpage.aspx"))
                    {
                        #region "staticpage"
                        if (lstBreadCrumbs != null)
                        {
                            if (lstBreadCrumbs.Count() > 0)
                            {

                                ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href=" + host + ParentPage.Link + ">" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[2].ToString())) + "</li></ul>";
                                ltlBreadCrumb.Visible = true;
                            }
                        }
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("categorylisting.aspx"))
                    {
                        #region "categorylisting"
                        int i = 3;
                        if (Request.Url.ToString().Contains("bav2.cwwws") || Request.Url.ToString().Contains("brand-estore")) { i = 4; }

                        if (Uri.Length == i)
                        {

                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                            ltlBreadCrumb.Visible = true;
                        }
                        else if (Uri.Length == (i + 1))
                        {
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='javascript:void(0);'/>" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                            ltlBreadCrumb.Visible = true;
                        }
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("productlisting.aspx"))
                    {
                        #region "productlisting"
                        int i = 3;
                        Exceptions.WriteInfoLog("productlisting.aspx - Uri.Length = " + Uri.Length);
                        //indeedstore Hardcoded in the below condition as Instructed by Reena Ma'am on "06/Apr/2017"
                        if (Request.Url.ToString().Contains("bav2.cwwws") || Request.Url.ToString().Contains("brand-estore") || Request.Url.ToString().Contains("indeedstore"))
                        {
                            i = 4;
                        }

                        if (Uri.Length == i)
                        {
                            string strUpper = GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()));
                            strUpper = char.ToUpper(strUpper[0]) + strUpper.Substring(1);
                            if (Uri[1].ToString().ToLower().Contains("search"))
                            {
                                //ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                                ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.EncodeUrlBredCrumb(strUpper) + "</li></ul>";
                            }
                            else
                            {
                                //ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                                ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.EncodeUrlBredCrumb(strUpper) + "</li></ul>";
                            }
                        }
                        else if (Uri.Length == (i + 1))
                        {
                            string strUpper = GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()));
                            strUpper = char.ToUpper(strUpper[0]) + strUpper.Substring(1);
                            /*Sachin Chauhan Start : 08 03 2016 : Changing logic of breadcrum getting built for category and or subcategory scenarios*/
                            //ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.EncodeUrlBredCrumb(strUpper) + "</li></ul>";

                            /*Sachin Chauhan Start : 03 03 2016 :*/
                            //if (Request.Url.AbsoluteUri.ToLower().Contains("subcategory"))
                            //    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + Request.Url + "'></a></li><li><a class=\"customBreadcrumb\" href='" + host + "Categories" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString()) + "</a></li><li class=\"active customBreadcrumbActive\">" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()) + "</li></ul>";

                            /*Sachin Chauhan End : 03 03 2016*/

                            /*Sachin Chauhan End : 08 03 2016*/
                        }
                        else if (Uri.Length == (i + 2))
                        {
                            string strUpper = GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()));
                            strUpper = char.ToUpper(strUpper[0]) + strUpper.Substring(1);
                            //ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString())) + "</a><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString())) + "</li></ul>";
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString())) + "</a><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.EncodeUrlBredCrumb(strUpper) + "</li></ul>";
                        }

                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("productdetails.aspx"))
                    {

                        ProductBE CurruntProdBE=Session["objCurrentProductBE"] as ProductBE;
                        string ProdName = CurruntProdBE.ProductName;
                        #region "productdetails"
                        int i = 3;

                        Exceptions.WriteInfoLog("productdetails.aspx - Uri.Length = " + Uri.Length);
                        //indeedstore Hardcoded in the below condition as Instructed by Reena Ma'am on "06/Apr/2017"
                        if (Request.Url.ToString().Contains("bav2.cwwws") || Request.Url.ToString().Contains("brand-estore") || Request.Url.ToString().Contains("indeedstore"))
                        {
                            i = 4;
                        }
                        if (Uri.Length == i)
                        {
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(Uri[2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + ProdName + "</li></ul>";
                        }
                        else if (Uri.Length == (i + 1))
                        {
                            /* Edited By Hardik to remove ProductCode from BreadCrumb [split by '_' ] */
                            string[] ProductName = new string[2];
                            if (Uri[3].Contains('_')) /// for local
                            {
                                ProductName = Uri[3].Split('_');
                                /* Edited By Hardik to remove ProductCode from BreadCrumb [split by '_' ] */

                                strUpper = GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(ProductName[1].Replace("/", "").ToString()));
                                strUpper = char.ToUpper(strUpper[0]) + strUpper.Substring(1);

                                //ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "Category" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(strUpper.Replace("/", "").ToString())) + "</li></ul>";
                                ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "Category" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + ProdName + "</li></ul>";
                            }
                            else /// for server
                            {
                                if (Uri[4].Contains('_'))
                                {
                                    ProductName = Uri[4].Split('_');
                                    strUpper = GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(ProductName[1].Replace("/", "").ToString()));
                                    strUpper = char.ToUpper(strUpper[0]) + strUpper.Substring(1);
                                    //ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "Category" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(ProductName[1].Substring(1).ToUpper().Replace("/", "").ToString())) + "</li></ul>";
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "Category" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + ProdName + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "Category" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + ProdName + "</li></ul>";
                                }



                            }
                        }
                        else if (Uri.Length == (i + 2))
                        {
                            Exceptions.WriteInfoLog("value of i + 2 = " + i);
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + "SubCategories" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString())) + "</a><li><a class=\"customBreadcrumb\" href='" + host + "SubCategory" + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", ""))) + "/" + GlobalFunctions.DecodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "'>" + GlobalFunctions.EncodeUrlBredCrumb(System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString())) + "</a></li><li class=\"active customBreadcrumbActive\">" + ProdName + "</li></ul>";
                        }
                        else if (Uri.Length == (i + 3))
                        {
                            Exceptions.WriteInfoLog("value of i + 3 = " + i);
                            ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + ParentPage.Link + "'>" + ParentPage.BreadCrumbName + "</a></li>"
                           + "<li><a class=\"customBreadcrumb\"  href='" + host + "SubCategories" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 4].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 4].Replace("/", "").ToString()) + "</a></li>"
                           + "<li><a class=\"customBreadcrumb\"  href='" + host + "SubCategories" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3].Replace("/", "").ToString()) + "</a></li>"
                           + "<li><a class=\"customBreadcrumb\"  href='" + host + "SubCategory" + "/" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 3]) + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()) + "'>" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 2].Replace("/", "").ToString()) + "</a></li>"
                           + "<li class=\"active customBreadcrumbActive\">'" + System.Web.HttpUtility.UrlDecode(Uri[Uri.Length - 1].Replace("/", "").ToString()) + "'</li></ul>";

                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("basket.aspx"))
                    {
                        #region "basket"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "Basket");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("payment.aspx"))
                    {
                        #region "payment"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "CheckOut");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);

                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("CreditCardPayment.aspx"))
                    {
                        #region "CreditCardPayment"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "CreditCardPayment");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }

                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("OrderConfirmation.aspx"))
                    {
                        #region "OrderConfirmation"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "OrderConfirmation");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href=" + host + a1.Link + "/>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + ">" + a1.BreadCrumbName + "'</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("OrderDetails.aspx"))
                    {
                        #region "OrderDetails"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "OrderDetails");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + ">" + c.BreadCrumbName + "'</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "/>" + a1.BreadCrumbName + "'</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + ">" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else if (VirutalPath.ToLower().Contains("pagenotfound.aspx"))
                    {
                        #region "pagenotfound"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == "pagenotfound");
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + System.Web.HttpUtility.UrlDecode(a.BreadCrumbName) + "</li></ul>";
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + System.Web.HttpUtility.UrlDecode(a.BreadCrumbName) + "</li></ul>";
                                }
                            }
                        }
                        ltlBreadCrumb.Visible = true;
                        #endregion
                    }
                    else
                    {
                        #region "Else"
                        BreadCrumbManagementBE a = lstBreadCrumbs.FirstOrDefault(b => b.Key == targetPage);
                        if (a != null)
                        {
                            BreadCrumbManagementBE a1 = lstBreadCrumbs.FirstOrDefault(b => b.BreadCrumbId == a.ParentBreadCrumbId);
                            if (a1 != null)
                            {
                                var c = lstBreadCrumbs.Where(b => b.BreadCrumbId == a1.ParentBreadCrumbId).FirstOrDefault();
                                if (c != null)
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + c.Link + "'>" + c.BreadCrumbName + "</a></li><li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                    ltlBreadCrumb.Visible = true;
                                }
                                else
                                {
                                    ltlBreadCrumb.Text = ltlBreadCrumb.Text + "<li><a class=\"customBreadcrumb\" href='" + host + a1.Link + "'>" + a1.BreadCrumbName + "</a></li><li class=\"active customBreadcrumbActive\">" + GlobalFunctions.DecodeCategoryURL(System.Web.HttpUtility.UrlDecode(a.BreadCrumbName)) + "</li></ul>";
                                    ltlBreadCrumb.Visible = true;
                                }
                            }
                        }
                        #endregion
                    }
                    ltlBreadCrumb.Text += "</div></div></div></section>";
                }
                else
                {
                    ltlBreadCrumb.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}