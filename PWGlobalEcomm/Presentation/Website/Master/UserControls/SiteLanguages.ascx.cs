﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Text;
using System.Web.UI.HtmlControls;
using System.IO;


namespace Presentation
{
    public partial class Master_UserControls_SiteLanguages : System.Web.UI.UserControl
    {

        protected global::System.Web.UI.WebControls.Repeater rptSiteLanguage;


        public string host = GlobalFunctions.GetVirtualPath();
        public string strLanguageTitle;



        protected void Page_Load(object sender, EventArgs e)
        {
            strLanguageTitle = BindResourceData();
        }

        protected void rptSiteLanguage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string strLanguageId = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "LanguageId"));
                LinkButton lnkSiteLanguage = e.Item.FindControl("lnkSiteLanguage") as LinkButton;
                lnkSiteLanguage.ID += strLanguageId;
                if (strLanguageId.Equals(GlobalFunctions.GetLanguageId().ToString()))
                {
                    lnkSiteLanguage.BackColor = System.Drawing.Color.FromArgb( 245,245,245);
                }
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 4-09-15
        /// Scope   : BindResourceData of the Master page
        /// </summary>
        /// <returns></returns>
        protected string BindResourceData()
        {
            string strLangaugeTitle = string.Empty;
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strLangaugeTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_LanguageTitle").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return strLangaugeTitle;
        }


    }
}
