﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;


namespace Presentation
{
    public partial class Master_UserControls_SocialMedia : System.Web.UI.UserControl
    {
        protected global::System.Web.UI.WebControls.Repeater rptSocialMedia;

        protected void Page_Load(object sender, EventArgs e)
        {
               List<SocialMediaManagementBE> lstSocialMediaLinks =  SocialMediaManagementBL.ListSocialMediaManagement();

               lstSocialMediaLinks = lstSocialMediaLinks.Where(x => x.IsActive.Equals(true)).ToList() ;

               rptSocialMedia.DataSource = lstSocialMediaLinks;
               rptSocialMedia.DataBind(); 

        }
        
        protected void rptSocialMedia_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ImageButton imgSocialMedia = e.Item.FindControl("imgBtnSocialMedia") as ImageButton;
                SocialMediaManagementBE socialMedia = e.Item.DataItem as SocialMediaManagementBE;
                imgSocialMedia.ImageUrl = GlobalFunctions.GetVirtualPath() + "Images/SocialMedia/" + socialMedia.SocialMediaId + "" + socialMedia.IconExtension;
                imgSocialMedia.PostBackUrl = socialMedia.SocialMediaUrl;
                
            }
        }
    }

}

