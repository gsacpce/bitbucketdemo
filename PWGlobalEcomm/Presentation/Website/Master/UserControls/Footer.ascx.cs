﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using System.IO;

namespace Presentation
{
    public class Master_UserControls_Footer : UserControl
    {
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divFooter;
        //protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        public string host = GlobalFunctions.GetVirtualPath();
        //int LanguageId = 1;
        private string Footer_Sitemap;
        private string SiteLinks_ContactusTitle;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                #region
                /*Sachin Chauhan : 01 09 2015 : Commented Old Prajwal's code*/
                //if (!IsPostBack)
                //{
                BindResourceData();

                if (divFooter != null)
                {
                    List<object> lstAllHeaderFooterElements = HeaderFooterManagementBL.GetAllHeaderFooterElements(true, "footer");

                    List<HomePageManagementBE> lstFooterElements = HomePageManagementBL.GetAllHomePageContentDetails().Cast<HomePageManagementBE>()
                                                                                                    .Where((x => x.ContentType.ToLower().Equals("footertext") ||
                                                                                                            x.ContentType.ToLower().Equals("footercopyright"))
                                                                                                            ).ToList();

                    lstFooterElements = lstFooterElements.Where(x => x.LanguageId.Equals(GlobalFunctions.GetLanguageId())).Cast<HomePageManagementBE>().ToList();
                    /*Sachin Chauhan Start : 03 02 2015 : Below code will decide */
                    /**/

                    if (lstAllHeaderFooterElements != null && lstAllHeaderFooterElements.Count > 0)
                    {
                        foreach (HeaderFooterManagementBE headerItem in lstAllHeaderFooterElements)
                        {
                            HtmlGenericControl gmContent = this.FindControl(headerItem.ConfigSectionName) as HtmlGenericControl;
                            if (gmContent != null)
                            {
                                gmContent.InnerHtml = "";

                                switch (headerItem.ConfigElementName.ToLower())
                                {
                                    case "footerlogo":
                                        {

                                            string strSiteLogoDir = HttpContext.Current.Server.MapPath("~/Images/FooterLogo");

                                            DirectoryInfo dirSiteLogo = new DirectoryInfo(strSiteLogoDir);
                                            FileInfo[] logoFileInfo = dirSiteLogo.GetFiles();

                                            foreach (FileInfo logoFile in logoFileInfo)
                                            {
                                                if (logoFile.Name.ToLower().Contains("footerlogo"))
                                                {
                                                    //string[] fileNameParts = logoFile.Name.Split('_');
                                                    //string width = fileNameParts[1];
                                                    //string wunit = fileNameParts[2];
                                                    //string height = fileNameParts[3];
                                                    //string hunit = fileNameParts[4];
                                                    gmContent.InnerHtml = "<a class=\"footerLink footerLogo\" href='" + host + "Home/Home.aspx'><img width='100%' height='auto' src='" + GlobalFunctions.GetVirtualPath() + "Images/FooterLogo/" + logoFile.Name + "'></a>";
                                                }
                                            }
                                            break;
                                        }
                                    case "footerlinks":
                                        {
                                            StoreBE objStoreBE = StoreBL.GetStoreDetails();
                                            bool bHeader = false;

                                            if (Session["User"] == null)
                                            {
                                                string strStoreAccess = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CR_StoreAccess").FeatureValues.FirstOrDefault(y => y.IsEnabled == true).FeatureValue;
                                                if (strStoreAccess.ToLower() == "mandatory login")
                                                {
                                                    bHeader = true;
                                                }
                                            }
                                            if (!bHeader)
                                            {
                                                Master_UserControls_SiteLinks ctrlSiteLinks = (Master_UserControls_SiteLinks)Page.LoadControl("~/Master/UserControls/SiteLinks.ascx");
                                                ctrlSiteLinks.LinkDisplayLocation = "footer";
                                                ctrlSiteLinks.LinkDisplayStyle = "horizontal";
                                                gmContent.Controls.Add(ctrlSiteLinks);
                                            }
                                            break;
                                        }
                                    case "footertext":
                                        {

                                            Literal ltrFooterText = new Literal();
                                            HomePageManagementBE footerText = lstFooterElements.SingleOrDefault(x => x.ContentType.ToLower().Equals("footertext"));
                                            if (footerText != null)
                                            {
                                                ltrFooterText.Text = "<div class=\"footerText\"><span class=\"footerLink\">" + GlobalFunctions.RemoveSanitisedPrefixes(Server.HtmlDecode(footerText.Content)) + "</span></div>";
                                                gmContent.Controls.Add(ltrFooterText);
                                            }
                                            break;
                                        }
                                    case "footercopyright":
                                        {
                                            Literal ltrFooterCopyright = new Literal();
                                            HomePageManagementBE footerCopyright = lstFooterElements.SingleOrDefault(x => x.ContentType.ToLower().Equals("footercopyright"));
                                            if (footerCopyright != null)
                                            {
                                                ltrFooterCopyright.Text = "© " + DateTime.Now.Year.ToString() + " " + footerCopyright.Content;
                                                gmContent.Controls.Add(ltrFooterCopyright);
                                            }
                                            break;
                                        }
                                    case "socialmedia":
                                        {
                                            Master_UserControls_SocialMedia ctrlSocialMedia = (Master_UserControls_SocialMedia)Page.LoadControl("~/Master/UserControls/SocialMedia.ascx");
                                            gmContent.Controls.Add(ctrlSocialMedia);
                                            break;
                                        }
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
                //}  
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// Author  : Sachin Chauhan
        /// Date    : 23-11-15
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        Footer_Sitemap = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Footer_Sitemap").ResourceValue;
                        SiteLinks_ContactusTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_ContactusTitle").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}