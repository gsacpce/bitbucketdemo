﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace Presentation
{
    public class Master_UserControls_Menu : System.Web.UI.UserControl
    {
        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        string host = GlobalFunctions.GetVirtualPath();
        string StoreCategoryType = Convert.ToString(ConfigurationManager.AppSettings["StoreCategoryType"]);
        StoreBE objStoreBE = new StoreBE();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                objStoreBE = StoreBL.GetStoreDetails();
                
                if (!objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_CategoryDisplay").FeatureValues[1].IsEnabled)
                {
                    MenuDetails();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 17-07-15
        /// Scope   : Binding the Menu listing on the page
        /// </summary>
        /// /// <param></param>
        /// <returns></returns>
        private void MenuDetails()
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            List<CategoryBE> objCategoryBE;
            Dictionary<int, List<CategoryBE>> objDictAllCategoriesAllLanguages;
            try
            {
                /*Sachin Chauhan : 18 09 2015 : Added Dictionalry for all categories languagewise*/
                #region "/*User Type*/"
                Int16 iUserTypeID = 1;
                if (Session["User"] != null)
                { iUserTypeID = ((UserBE)Session["User"]).UserTypeID; }
                objCategoryBE = CategoryBL.GetAllCategories(intLanguageId, intCurrencyId, iUserTypeID);
                #endregion
                //objCategoryBE = CategoryBL.GetAllCategories(intLanguageId, intCurrencyId);//commented /*User Type*/
                objDictAllCategoriesAllLanguages = CategoryBL.GetAllCategoriesAllLanguages();

                /*Sachin Chauhan : 18 09 2015*/

                string strParentCategoryName = string.Empty;
                HtmlGenericControl parentUL = new HtmlGenericControl("ul");
                //parentUL.Attributes.Add("class", "dropdown-menu");
                parentUL.Attributes.Add("class", "dropdown-menu");

                #region Categories
                if (objCategoryBE != null)
                {
                    List<CategoryBE> objParentCategory = objCategoryBE.FindAll(x => x.ParentCategoryId == 0);
                    HtmlGenericControl parentLI1 = new HtmlGenericControl("li");
                    HtmlAnchor parentAnchor1 = new HtmlAnchor();
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_HomeLink").FeatureValues[0].IsEnabled)
                    {

                        parentAnchor1.InnerText = "Home";
                        parentAnchor1.Attributes.Add("class", "categoryText");
                        parentAnchor1.HRef = host + "index";
                        parentLI1.Controls.Add(parentAnchor1);
                        this.Controls.Add(parentLI1);

                    }
                    foreach (CategoryBE parentCategory in objParentCategory)
                    {
                        string Mouseovermenuurl = "";
                        HtmlGenericControl parentLI = new HtmlGenericControl("li");
                        HtmlAnchor parentAnchor = new HtmlAnchor();
                        List<CategoryBE> objChildCategory = objCategoryBE.FindAll(x => x.ParentCategoryId == parentCategory.CategoryId);
                        strParentCategoryName = parentCategory.CategoryName;
                        if (StoreCategoryType == "Image")
                        {
                            //Mouseovermenuurl = "<img class='' alt='" + parentCategory.CategoryName + "'  src='" + host + "Admin/Images/Category/MouseOver/" + parentCategory.CategoryId + "_MouseOver_" + intLanguageId + parentCategory.MenuMouseoverExtension + "'>";
                            if (parentCategory.MenuMouseoverExtension == "" || parentCategory.MenuMouseoverExtension == null)
                            {
                                parentAnchor.InnerText = strParentCategoryName;
                                parentAnchor.Attributes.Add("class", "categoryText");
                            }
                            else
                            {
                                string strMouseOverImgname = host + "Admin/Images/Category/MouseOver/" + parentCategory.CategoryId + "_MouseOver_" + intLanguageId + parentCategory.MenuMouseoverExtension;
                                parentAnchor.Attributes.Add("data-src", strMouseOverImgname);
                            }
                            parentAnchor.Attributes.Add("class", "menuLink");

                        }
                        if (objChildCategory.Count > 0)
                        {
                            parentLI.Attributes.Add("class", "dropdown");
                            parentAnchor.Attributes.Add("class", "dropdown-toggle");
                            parentAnchor.Attributes.Add("data-toggle", "dropdown");
                            //  parentAnchor.ID = "htmAnchor1";
                            parentAnchor.HRef = "~/#";
                        }
                        else
                        {

                            //if (parentCategory.CategoryName == "Home")
                            //{
                            //    parentAnchor.HRef = host + "index";
                            //}
                            //else
                            //{
                            parentAnchor.HRef = host + "Category/" + GlobalFunctions.EncodeCategoryURL(strParentCategoryName);
                            //}
                        }
                        #region commented and added by vikram to show category as image of as link

                        if (StoreCategoryType == "Image")
                        {
                            if (parentCategory.MenuImageExtension == "" || parentCategory.MenuImageExtension == null)
                            {
                                parentAnchor.InnerText = strParentCategoryName;
                                parentAnchor.Attributes.Add("class", "categoryText");
                            }
                            else
                            {
                                parentAnchor.InnerHtml = "<img alt='" + parentCategory.CategoryName + "' src='" + host + "Admin/Images/Category/categoryMenu/" + parentCategory.CategoryId + "_" + intLanguageId + parentCategory.MenuImageExtension + "'>";
                            }
                        }
                        else
                        { parentAnchor.InnerText = strParentCategoryName; }
                        #endregion

                        //if (parentCategory.CategoryName == "Home")
                        //{
                        //    parentLI.Controls.Add(parentAnchor);
                        //}

                        if (objChildCategory.Count == 0 && parentCategory.ProductCount == 0)
                        {

                        }
                        else
                        {
                            parentLI.Controls.Add(parentAnchor);
                        }

                        if (objChildCategory.Count > 0)
                        {
                            HtmlGenericControl span = new HtmlGenericControl("span");
                            if (StoreCategoryType != "Image")// only for link
                            {
                                span.Attributes.Add("class", "caret");
                            }
                            parentAnchor.Controls.Add(span);
                            HtmlGenericControl childUL = new HtmlGenericControl("ul");
                            childUL.Attributes.Add("class", "dropdown-menu");

                            foreach (CategoryBE childCategory in objChildCategory)
                            {
                                HtmlGenericControl childLI = new HtmlGenericControl("li");
                                HtmlAnchor childAnchor = new HtmlAnchor();
                                List<CategoryBE> objSubSubCategory = objCategoryBE.FindAll(x => x.ParentCategoryId == childCategory.CategoryId);
                                //List<CategoryBE> objSubSubCategory = objCategoryBE.FindAll(x => x.SubSubCategoryId == childCategory.SubSubCategoryId);/*User Type*/
                                if (objSubSubCategory.Count > 0)
                                {
                                    childAnchor.HRef = "";
                                    childLI.Attributes.Add("class", "dropdown");
                                    childAnchor.Attributes.Add("class", "dropdown-toggle");
                                    childAnchor.Attributes.Add("data-toggle", "dropdown");
                                    childAnchor.HRef = "~/#";
                                }
                                else
                                {
                                    childAnchor.HRef = host + "SubCategory/" + GlobalFunctions.EncodeCategoryURL(strParentCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(childCategory.CategoryName);
                                }
                                childAnchor.InnerText = childCategory.CategoryName;
                                if (objSubSubCategory.Count == 0 && childCategory.ProductCount == 0)
                                {

                                }
                                else
                                {
                                    if (childCategory.UserTypeStatus)
                                        childLI.Controls.Add(childAnchor);
                                }
                                SubMenuDetails(objCategoryBE, childCategory, childLI, childAnchor, strParentCategoryName);
                                childUL.Controls.Add(childLI);
                            }
                            parentLI.Controls.Add(childUL);
                        }

                        //if (parentCategory.CategoryName == "Home")
                        //{
                        //    this.Controls.Add(parentLI);
                        //}



                        if (objChildCategory.Count == 0 && parentCategory.ProductCount == 0)
                        {
                        }
                        else
                            this.Controls.Add(parentLI);

                    }
                }
                #endregion


                #region Gift Certificate/eVouchers
                /*Sachin Chauhan Start : 30 12 15 : Check if gift certificate feature exists then append a link to categories menu*/
                //StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    bool IsGiftCetificateEnabledStore = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_GiftCertificate").FeatureValues[0].IsEnabled;
                    if (IsGiftCetificateEnabledStore)
                    {
                        List<StaticPageManagementBE> lstStaticLinksFullList = new List<StaticPageManagementBE>();
                        List<StaticPageManagementBE> lstGCLinksList = new List<StaticPageManagementBE>();// ShriGanesh 07 June 2016
                        StaticPageManagementBE objBE = new StaticPageManagementBE();
                        objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBE.PageName = "";
                        lstStaticLinksFullList = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "All");
                        if (lstStaticLinksFullList != null)
                        {
                            /* SHRIGANESH SINGH 09 June 2016
                             * In the below code, we have used PageURL as GiftCertificateLink so ato differentiate the PageURLs of Parent link and 
                             * its child link. Please do not change this PageURL for the Parent GiftCertificate link. 
                             */

                            StaticPageManagementBE gcLink = lstStaticLinksFullList.FirstOrDefault(x => x.PageURL.Contains("GiftCertificateLink"));
                            lstGCLinksList = lstStaticLinksFullList.FindAll(x => x.ParentStaticPageId == gcLink.StaticPageId);// ShriGanesh 07 June 2016

                            HtmlGenericControl parentLI = new HtmlGenericControl("li");
                            HtmlAnchor parentAnchor = new HtmlAnchor();

                            #region Original
                            //parentAnchor.HRef = host + gcLink.PageURL;
                            //parentAnchor.InnerText = gcLink.PageName;
                            //parentLI.Controls.Add(parentAnchor); 
                            #endregion

                            parentLI.Attributes.Add("class", "dropdown");
                            parentAnchor.Attributes.Add("class", "dropdown-toggle categoryText");
                            parentAnchor.Attributes.Add("data-toggle", "dropdown");
                            parentAnchor.HRef = "~/#";
                            parentAnchor.InnerText = gcLink.PageName;
                            parentLI.Controls.Add(parentAnchor);

                            // ShriGanesh 07 June 2016 Start
                            if (lstGCLinksList.Count > 0)
                            {
                                HtmlGenericControl span = new HtmlGenericControl("span");
                                span.Attributes.Add("class", "caret");
                                parentAnchor.Controls.Add(span);
                                HtmlGenericControl GC_ChildUL = new HtmlGenericControl("ul");// ShriGanesh 07 June 2016
                                GC_ChildUL.Attributes.Add("class", "dropdown-menu");// ShriGanesh 07 June 2016
                                foreach (StaticPageManagementBE GCLink in lstGCLinksList)
                                {
                                    HtmlGenericControl GC_ChildLI = new HtmlGenericControl("li");// ShriGanesh 07 June 2016
                                    GC_ChildUL.Controls.Add(GC_ChildLI);
                                    HtmlAnchor gcchildanchor = new HtmlAnchor();// ShriGanesh 07 June 2016

                                    //gcchildanchor.HRef = host + GCLink.PageURL;// commented by vikram

                                    if (GCLink.PageURL.ToLower().Contains("#modalgcbalance"))
                                    {
                                        gcchildanchor.HRef = host + GCLink.PageURL;
                                        gcchildanchor.Attributes.Add("data-toggle", "modal");
                                        gcchildanchor.InnerText = GCLink.PageName;
                                        GC_ChildLI.Controls.Add(gcchildanchor);
                                    }
                                    if (GCLink.PageURL.Contains("GiftCertificate"))
                                    {
                                        if (Session["User"] == null)
                                        {
                                            gcchildanchor.HRef = host + "#divGuestLoginGc";
                                            gcchildanchor.Attributes.Add("data-toggle", "modal");
                                            gcchildanchor.InnerText = GCLink.PageName;
                                            GC_ChildLI.Controls.Add(gcchildanchor);
                                        }
                                        else
                                        {
                                            gcchildanchor.HRef = host + GCLink.PageURL;
                                            gcchildanchor.InnerText = GCLink.PageName;
                                            GC_ChildLI.Controls.Add(gcchildanchor);
                                        }
                                    }


                                    if (Session["User"] == null)
                                    {

                                    }
                                    //GC_ChildUL.Controls.Add(GC_ChildLI);

                                }
                                parentLI.Controls.Add(GC_ChildUL);// ShriGanesh 07 June 2016
                            }
                            // ShriGanesh 07 June 2016 End

                            this.Controls.Add(parentLI);// Original




                            //this.Controls.Add(parentLI);
                        }

                    }
                }
                /*Sachin Chauhan End : 30 12 15*/

                #endregion

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 17-07-15
        /// Scope   : Decode the special characters with the predefined text for each special character
        /// </summary>
        /// <param name="objCategoryBE"></param>
        /// <param name="parentCategory"></param>
        /// <param name="parentLI"></param>
        /// <param name="parentAnchor"></param>
        /// <param name="strParentCategoryName"></param>
        /// <returns></returns>
        private void SubMenuDetails(List<CategoryBE> objCategoryBE, CategoryBE parentCategory, HtmlGenericControl parentLI, HtmlAnchor parentAnchor, string strParentCategoryName)
        {
            try
            {
                List<CategoryBE> objChildCategory = objCategoryBE.FindAll(x => x.ParentCategoryId == parentCategory.CategoryId);
                if (objChildCategory.Count > 0)
                {
                    parentLI.Style.Add("z-index", "10");
                    parentAnchor.Attributes.Add("class", "abc");
                    HtmlGenericControl span = new HtmlGenericControl("span");
                    span.Attributes.Add("class", "caret");
                    parentAnchor.InnerText += " ";
                    parentAnchor.Controls.Add(span);
                    HtmlGenericControl childUL = new HtmlGenericControl("ul");
                    childUL.Attributes.Add("class", "dropdown-menu");

                    foreach (CategoryBE childCategory in objChildCategory)
                    {
                        if (childCategory.ProductCount != 0)
                        {
                            HtmlGenericControl childLI = new HtmlGenericControl("li");
                            HtmlAnchor childAnchor = new HtmlAnchor();
                            childAnchor.HRef = host + "SubCategory/" + GlobalFunctions.EncodeCategoryURL(strParentCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(parentCategory.CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(childCategory.CategoryName);
                            childAnchor.InnerText = childCategory.CategoryName;
                            childAnchor.Attributes.Add("class", "menuLink");
                            childLI.Controls.Add(childAnchor);
                            SubMenuDetails(objCategoryBE, childCategory, childLI, childAnchor, parentCategory.CategoryName);
                            childUL.Controls.Add(childLI);
                        }
                    }
                    parentLI.Controls.Add(childUL);
                }
                else
                    parentAnchor.Attributes.Add("class", "");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }
}

