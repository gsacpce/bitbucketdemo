﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Text;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Configuration;


namespace Presentation
{
	public partial class Master_UserControls_SiteLinks : System.Web.UI.UserControl
	{

		protected global::System.Web.UI.WebControls.Repeater rptHeaderLinks, rptFooterLinks;
		protected global::System.Web.UI.WebControls.Repeater rptChildSiteLinks;
		protected global::System.Web.UI.WebControls.Repeater rptViewBasket, rptViewBasket_new;
		protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnCounter, spnItemCount, quickbasket, dv1, dv2, quickbasket_new, dv1_new, dv2_new;
		protected global::System.Web.UI.HtmlControls.HtmlImage imgBasket;
		protected global::System.Web.UI.WebControls.Label lblProductTitle, lblProductDescription, lblProductQuantity, lblProductPrice, lblTotalProductCost, lblTotalCheckoutAmount,
															lblProductQuantity_new, lblTotalProductCost_new, lblTotalCheckoutAmount_new,lblspnor, lblsPrice;
		protected global::System.Web.UI.WebControls.Literal ltrCounter;
		protected global::System.Web.UI.HtmlControls.HtmlAnchor aButton2, aButton1, aButton1_new;
		public string stror;
		double totalCheckoutAmount = 0;
		double totalCheckoutAmount_new = 0;
		protected string linkDisplayLocation;
		protected string linkDisplayStyle;
		public string host = GlobalFunctions.GetVirtualPath();

		public string Basket_MiniBasketHead1, Basket_MiniBasketHead2, Basket_MiniBasketView, Basket_MiniBasketCheckout,
					  Basket_MiniBasketTotalBeforeDelivery, Basket_MiniBasketFillMeUp;

		string MaerskSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_OIL_SSOLogs";
		string StoreCategoryType = Convert.ToString(ConfigurationManager.AppSettings["StoreCategoryType"]);

		public string LinkDisplayLocation
		{
			get { return linkDisplayLocation; }
			set { linkDisplayLocation = value; }
		}

		public string LinkDisplayStyle
		{
			get { return linkDisplayStyle; }
			set { linkDisplayStyle = value; }
		}
		
		Int16 intLanguageId = 0;
		Int16 intCurrencyId = 0;

		string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();

		List<StaticPageManagementBE> lstStaticLinksFullList = new List<StaticPageManagementBE>();
		List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
		StoreBE objStoreBE = new StoreBE();
		public string basketclass = GlobalFunctions.GetSetting("BasketIcon");
		public string usericon = GlobalFunctions.GetSetting("AccountIcon");

		protected void Page_Load(object sender, EventArgs e)
		{
			StaticPageManagementBE objBE = new StaticPageManagementBE();
			objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
			objBE.PageName = "";
			objStoreBE = StoreBL.GetStoreDetails();
			bool bHeader = false;
			BindResourceData();
			string host = GlobalFunctions.GetVirtualPath();
			intLanguageId = GlobalFunctions.GetLanguageId();
			intCurrencyId = GlobalFunctions.GetCurrencyId();

			lstStaticLinksFullList = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "All");
			if (lstStaticLinksFullList != null)
			{
				Session["lstItemz"] = lstStaticLinksFullList;
				lstStaticLinksFullList = lstStaticLinksFullList.Where(x => x.IsActive.Equals(true) && x.DisplayLocation.ToLower().Equals(LinkDisplayLocation)).Cast<StaticPageManagementBE>().OrderBy(x => x.DisplayOrder).ToList();
				if (lstStaticLinksFullList.Count > 0)
				{
					List<StaticPageManagementBE> lstParentStaticLinks;
					if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_HomeLink").FeatureValues[1].IsEnabled)
					{
						lstParentStaticLinks = lstStaticLinksFullList.
												Where(y => y.ParentStaticPageId.Equals(0) && y.StaticPageId != 26).
												Cast<StaticPageManagementBE>().ToList();
					}
					else
					{
						lstParentStaticLinks = lstStaticLinksFullList.
																		 Where(y => y.ParentStaticPageId.Equals(0) && y.StaticPageId != 26 && y.StaticPageId != 12).
																		 Cast<StaticPageManagementBE>().ToList();
					}


					HtmlGenericControl ulSiteLinks = this.FindControl("ulSiteLinks") as HtmlGenericControl;
					if (LinkDisplayStyle.ToLower().Equals("vertical"))
						ulSiteLinks.Attributes.Remove("class");

					if (LinkDisplayLocation.ToLower().Equals("header"))
					{
						string strStoreAccess = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CR_StoreAccess").FeatureValues.FirstOrDefault(y => y.IsEnabled == true).FeatureValue;
						if (Session["User"] == null)
						{
							lstParentStaticLinks.RemoveAll(x => x.PageName.ToLower().Contains("dashboard"));
							if (strStoreAccess.ToLower() == "mandatory login")
							{
								bHeader = true;
							}
							if (Session["ReferrelURL"] != null)
							{
								bHeader = true;
							}
						}
						else
						{
							if (objStoreBE.StoreId == 532)
							{
								UserBE user = Session["User"] as UserBE;

								if (user.UserTypeID != 6)
								{
									lstParentStaticLinks.RemoveAll(x => x.PageName.ToLower().Contains("dashboard"));
								}
							}
							else
							{
								lstParentStaticLinks.RemoveAll(x => x.PageName.ToLower().Contains("dashboard"));
							}
						}
						if (!bHeader)
						{
							if (objStoreBE.StoreId == 462)
							{
								if (Session["S_ISPUNCHOUT"] != null)
								{
									foreach (var lstParentStaticLink in lstParentStaticLinks)
									{
										if (lstParentStaticLink.StaticPageId == 142)
										{
											lstParentStaticLink.PageURL = Convert.ToString(Session["S_ReturnToFusionURL"]);
										}
									}
								}
								else
								{
									lstParentStaticLinks = lstParentStaticLinks.Where(x => x.StaticPageId != 142).Cast<StaticPageManagementBE>().ToList();
								}
							}
							
							rptHeaderLinks.DataSource = lstParentStaticLinks;
							rptHeaderLinks.DataBind();
							if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_CategoryDisplay").FeatureValues[1].IsEnabled)
							{
								MenuDetails();
							}
						}
					}
					if (LinkDisplayLocation.ToLower().Equals("footer"))
					{
						rptFooterLinks.DataSource = lstParentStaticLinks;
						rptFooterLinks.DataBind();
					}
				}
			}
		}

		protected void rptHeaderLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			StaticPageManagementBE currSiteLink = e.Item.DataItem as StaticPageManagementBE;
			HtmlAnchor lnkSiteLink = e.Item.FindControl("lnkSiteLink") as HtmlAnchor;
			HtmlGenericControl liSiteLink = e.Item.FindControl("liSiteLink") as HtmlGenericControl;
			HtmlGenericControl spnItemCount = e.Item.FindControl("spnItemCount") as HtmlGenericControl;
			lnkSiteLink.InnerText = currSiteLink.PageName;
			lnkSiteLink.InnerText = WebUtility.HtmlDecode(currSiteLink.PageName);
			string currSiteLinkUrl = "";
			#region "Basket Link"
			if (currSiteLink.PageURL.ToLower().Contains("basket") || currSiteLink.PageURL.ToLower().Contains("shoppingcart"))
			{
				liSiteLink.Attributes.Add("class", "BasketLink hidden-xs");
				if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_CategoryDisplay").FeatureValues[1].IsEnabled)
				{
					lnkSiteLink.InnerHtml = "<img  src=\'" + host + "Images/Basket.png' width=\'27\' />";
				}
				else
				{
					lnkSiteLink.InnerHtml = "<i class='" + basketclass + "' aria-hidden='true'></i>&nbsp; <span class=\"basket_qty_lg menuBg1\">0</span>" + currSiteLink.PageName + "";
				}

				int intUserId = 0;
				Int16 intCurrencyId, intLanguageId;
				quickbasket = liSiteLink.FindControl("quickbasket") as HtmlGenericControl;                
				dv1 = quickbasket.FindControl("dv1") as HtmlGenericControl;
				dv2 = quickbasket.FindControl("dv2") as HtmlGenericControl;
				aButton1 = quickbasket.FindControl("aButton1") as HtmlAnchor;
				aButton2 = quickbasket.FindControl("aButton2") as HtmlAnchor;
				quickbasket_new = liSiteLink.FindControl("quickbasket_new") as HtmlGenericControl;
				dv1_new = quickbasket_new.FindControl("dv1_new") as HtmlGenericControl;
				dv2_new = quickbasket_new.FindControl("dv2_new") as HtmlGenericControl;
				aButton1_new = quickbasket_new.FindControl("aButton1_new") as HtmlAnchor;
				if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_BasketPopup").FeatureValues[1].IsEnabled)
				{
					quickbasket.Visible = false;
					quickbasket_new.Visible = true;
				}
				else
				{
					quickbasket.Visible = true;
					quickbasket_new.Visible = false;
				}
				string strUserSessionId = "";
				try
				{
					if (Session["User"] != null)
					{
						UserBE lst = new UserBE(); lst = Session["User"] as UserBE; intUserId = lst.UserId;
						if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_CategoryDisplay").FeatureValues[1].IsEnabled)
						{
							aButton1_new.Visible = true;
						}
						else
						{
							aButton1.Visible = true;
						}
					}
					else
					{
						strUserSessionId = HttpContext.Current.Session.SessionID;
						if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_CategoryDisplay").FeatureValues[1].IsEnabled)
						{
							aButton1_new.Visible = false;
						}
						else
						{
							aButton1.Visible = false;
						}
					}
					#region Added by Sripal for Punchout"
					if (Session["S_ISPUNCHOUT"] != null)
					{
						if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_CategoryDisplay").FeatureValues[1].IsEnabled)
						{
							aButton1_new.Visible = false;
						}
						else
						{
							aButton1.Visible = false;
						}
					}
					#endregion

					intCurrencyId = GlobalFunctions.GetCurrencyId();
					intLanguageId = GlobalFunctions.GetLanguageId();
					List<object> lstShoppingBE = new List<object>();
					lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId);
					if (lstShoppingBE != null && lstShoppingBE.Count > 0)
					{
						if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_CategoryDisplay").FeatureValues[1].IsEnabled)
						{
							lnkSiteLink.InnerHtml = lstShoppingBE.Count + "  " + "<img src=\'" + host + "Images/Basket.png' height=\'23\' width=\'27\' />";
						}
						else
						{
							lnkSiteLink.InnerHtml = "<i class='" + basketclass + "' aria-hidden='true'></i>&nbsp; <span class=\"basket_qty_lg menuBg1\">" + lstShoppingBE.Count + "</span>" + currSiteLink.PageName + "";
						}
						spnItemCount.InnerHtml = Convert.ToString(lstShoppingBE.Count);
						if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_BasketPopup").FeatureValues[1].IsEnabled)
						{
							Repeater rptViewBasket_new = e.Item.FindControl("rptViewBasket_new") as Repeater;
							rptViewBasket_new.DataSource = lstShoppingBE;
							rptViewBasket_new.DataBind();
							Label lblTotalCheckoutAmount_new = e.Item.FindControl("lblTotalCheckoutAmount_new") as Label;
							if (GlobalFunctions.IsPointsEnbled())
							{
								lblTotalCheckoutAmount_new.Text = Convert.ToString(totalCheckoutAmount_new) + GlobalFunctions.PointsText(intLanguageId);
							}
							else
							{
								lblTotalCheckoutAmount_new.Text = GlobalFunctions.GetCurrencySymbol() + totalCheckoutAmount_new.ToString("f");
							}
							aButton1_new.HRef = GlobalFunctions.GetVirtualPath() + "Checkout";
							dv1_new.Visible = true;
							dv2_new.Visible = false;
						}
						else
						{
							Repeater rptViewBasket = e.Item.FindControl("rptViewBasket") as Repeater;
							rptViewBasket.DataSource = lstShoppingBE;
							rptViewBasket.DataBind();
							Label lblTotalCheckoutAmount = e.Item.FindControl("lblTotalCheckoutAmount") as Label;
							Label lblspnor = (Label)e.Item.FindControl("lblspnor");
							Label lblsPrice = (Label)e.Item.FindControl("lblsPrice");
							if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled)
							{
								lblspnor.Text = stror;
								lblsPrice.Text = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(totalCheckoutAmount), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
							}
							if (GlobalFunctions.IsPointsEnbled())
								lblTotalCheckoutAmount.Text = GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(totalCheckoutAmount), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

							else
								lblTotalCheckoutAmount.Text = GlobalFunctions.GetCurrencySymbol() + totalCheckoutAmount.ToString("f");
							aButton2.HRef = GlobalFunctions.GetVirtualPath() + "Checkout/ShoppingCart";

							aButton1.HRef = GlobalFunctions.GetVirtualPath() + "Checkout";
							dv1.Visible = true;
							dv2.Visible = false;
						}                        
					}
					else
					{
						spnItemCount.InnerHtml = "0";
						if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "CR_BasketPopup").FeatureValues[1].IsEnabled)
						{
							dv1_new.Visible = false;
							dv2_new.Visible = true;
						}
						else
						{
							dv1.Visible = false;
							dv2.Visible = true;
						}
					}
				}
				catch (Exception ex)
				{ Exceptions.WriteExceptionLog(ex); }
			}
			#endregion

			#region to decide whether to display account icon or not
			if (currSiteLink.PageAliasName.ToLower().Contains("my account"))
			{
				if (usericon != "")
				{
					lnkSiteLink.InnerHtml = "<i class='" + usericon + "' aria-hidden='true'></i>";
				}
			}
			#endregion
						
			if (currSiteLink.PageURL.StartsWith("/"))
				currSiteLinkUrl = currSiteLink.PageURL.Substring(1, currSiteLink.PageURL.Length - 1);
			else
				currSiteLinkUrl = currSiteLink.PageURL;

			lnkSiteLink.Attributes.Add("class", "dropdown-toggle headerLink");            		

			if (currSiteLink.PageType.Equals(2))
			{
				if (currSiteLinkUrl.StartsWith("https://"))
					lnkSiteLink.HRef = currSiteLinkUrl;
				else if (currSiteLinkUrl.StartsWith("http://"))
					lnkSiteLink.HRef = currSiteLinkUrl;
				else
					lnkSiteLink.HRef = "http://" + currSiteLinkUrl;

				if (currSiteLink.NewWindow == 1)
				{
					lnkSiteLink.Attributes.Add("target", "_blank");
				}
			}
			else
			{
				if (objStoreBE.StoreId == 462)
				{
					if (Session["S_ISPUNCHOUT"] != null)
					{
						if (currSiteLink.StaticPageId == 142)
						{
							lnkSiteLink.HRef = currSiteLinkUrl;
						}
					}
				}
				else 
				{
					lnkSiteLink.HRef = currSiteLink.IsParent.Equals(true) ? "#" : host + currSiteLinkUrl;
				}                
			}			
			if (currSiteLink.NewWindow.Equals(1))
				lnkSiteLink.Attributes.Add("target", "_blank");

			if (currSiteLink.PageURL.ToLower().Contains("#modalcontactus"))
				lnkSiteLink.Attributes.Add("data-toggle", "modal");

			if (currSiteLink.IsSystemLink && currSiteLink.IsParent == false)
				lnkSiteLink.HRef = "~/" + currSiteLinkUrl;

			if (currSiteLink.IsParent.Equals(true))
			{
				lnkSiteLink.HRef = "return javascript:void(0);";

				liSiteLink.Attributes.Add("class", "dropdown sitelink ");//added by Sripal sitelink 
				
				lnkSiteLink.InnerHtml += "<span class=\"caret\"></span>";
				lnkSiteLink.Attributes.Add("class", "dropdown-toggle headerLink");
				lnkSiteLink.Attributes.Add("data-toggle", "dropdown");

				HtmlGenericControl ulChildSiteLinks = e.Item.FindControl("ulChildSiteLinks") as HtmlGenericControl;
				ulChildSiteLinks.Visible = true;
				ulChildSiteLinks.Attributes.Add("class", "dropdown-menu");
				Repeater rptChildSiteLinks = ulChildSiteLinks.FindControl("rptChildSiteLinks") as Repeater;
				List<StaticPageManagementBE> lstChildLinks = (List<StaticPageManagementBE>)Session["lstItemz"];
				lstChildLinks = lstChildLinks.Where(x => x.ParentStaticPageId.Equals(currSiteLink.StaticPageId) && x.IsActive.Equals(true)).Cast<StaticPageManagementBE>().ToList();

				#region MY ACCOUNT
				if (currSiteLink.PageAliasName.ToLower().Contains("my account"))
				{
					if (Session["User"] != null)
					{
						UserBE user = Session["User"] as UserBE;                        						
						#region Vikram to Hide Wish list if disabled
						if (objStoreBE != null)
						{
							if (!(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableWishlist").FeatureValues[0].IsEnabled))
							{
								lstChildLinks.RemoveAll(x => x.PageName.ToLower().Contains("my wishlist"));
							}
							if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "ss_indeededitprofile").FeatureValues[1].IsEnabled)
							{
								lstChildLinks.RemoveAll(x => x.PageName.ToLower().Contains("edit profile"));
							}
							else
							{
								lstChildLinks.RemoveAll(x => x.PageName.ToLower().Contains("my profile"));
							}
						}
						#endregion
						StaticPageManagementBE loginLink = lstChildLinks.FirstOrDefault(x => x.PageURL.ToLower().Contains("signin"));
						if (loginLink != null && user.IsGuestUser.Equals(false))
						{
							lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signin");
						}

						if (user.IsGuestUser == true)
						{
							if (Session["AccessToken"] != null)
							{
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signin");
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "userregistration");
								if (Session["IsLogin"] != null)
								{ lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("myaccount")); }
							}
							else if (Session["Maersk_SSO_MGIS_User"] != null)
							{
								Exceptions.WriteSSOLog("inside Maersk_SSO_MGIS_User != null PAGE LOAD ", MaerskSSOLogFolderPath);
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signin");
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "userregistration");
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("orderhistory"));
								if (Session["IsLogin"] != null)
								{
									lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("myaccount"));
								}
							}
							else if (Session["Maersk_SSO_OIL_User"] != null)
							{
								Exceptions.WriteSSOLog("inside Maersk_SSO_OIL_User != null PAGE LOAD ", MaerskSSOLogFolderPath);
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signin");
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "userregistration");
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("orderhistory"));
								if (Session["IsLogin"] != null)
								{
									lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("myaccount"));
								}
							}
							else
							{
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signout");
								lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("orderhistory"));
								if (Session["IsLogin"] != null)
								{ lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("myaccount")); }
							}
						}
						else
						{
							lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signin");
							lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "userregistration");
						}
					}
					else
					{
						lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signout");
						lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("myaccount"));
						lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("orderhistory"));
						lstChildLinks.RemoveAll(x => x.PageName.ToLower().Contains("my profile"));
					}
				}
				#endregion			

				rptChildSiteLinks.DataSource = lstChildLinks;
				rptChildSiteLinks.DataBind();
			}
		}

		protected void rptFooterLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			StaticPageManagementBE currSiteLink = e.Item.DataItem as StaticPageManagementBE;
			HtmlAnchor lnkSiteLink = e.Item.FindControl("lnkSiteLink") as HtmlAnchor;

			if (LinkDisplayLocation.ToLower().Equals("header"))
				lnkSiteLink.Attributes.Add("class", "dropdown-toggle");

			HtmlGenericControl liSiteLink = e.Item.FindControl("liSiteLink") as HtmlGenericControl;

			if (LinkDisplayLocation.ToLower().Equals("header"))
				liSiteLink.Attributes.Add("class", "dropdown sitelink");//added by Sripal sitelink 

			lnkSiteLink.InnerText = currSiteLink.PageName;
			string currSiteLinkUrl = "";

			if (currSiteLink.PageURL.StartsWith("/"))
				currSiteLinkUrl = currSiteLink.PageURL.Substring(1, currSiteLink.PageURL.Length - 1);
			else
				currSiteLinkUrl = currSiteLink.PageURL;

			lnkSiteLink.HRef = currSiteLink.IsParent.Equals(true) ? "#" : host + currSiteLinkUrl;
			StaticPageManagementBE objCurrSiteLink = e.Item.DataItem as StaticPageManagementBE;
			if (objCurrSiteLink.PageType.Equals(2))
			{
				lnkSiteLink.HRef = currSiteLinkUrl.StartsWith("http://") ? currSiteLinkUrl : "http://" + currSiteLinkUrl;
				lnkSiteLink.Attributes.Add("target", "_blank");
			}

			if (objCurrSiteLink.NewWindow.Equals(1))
				lnkSiteLink.Attributes.Add("target", "_blank");

			if (objCurrSiteLink.PageURL.ToLower().Contains("#modalcontactus"))
				lnkSiteLink.Attributes.Add("data-toggle", "modal");


			if (currSiteLink.IsSystemLink)
				lnkSiteLink.HRef = "~/" + currSiteLinkUrl;


			if (currSiteLink.IsParent.Equals(true))
			{
				lnkSiteLink.HRef = "return javascript:void(0);";
			}
		}

		protected void rptChildSiteLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			StaticPageManagementBE currChildSiteLink = e.Item.DataItem as StaticPageManagementBE;
			HtmlAnchor lnkChildSiteLink = e.Item.FindControl("lnkChildSiteLink") as HtmlAnchor;

			if (LinkDisplayLocation.ToLower().Equals("header"))
				lnkChildSiteLink.Attributes.Add("class", "dropdown-toggle");
			HtmlGenericControl liChildSiteLink = e.Item.FindControl("liChildSiteLink") as HtmlGenericControl;

			if (LinkDisplayLocation.ToLower().Equals("header"))
				liChildSiteLink.Attributes.Add("class", "dropdown sitelink");//added by Sripal sitelink 

			lnkChildSiteLink.InnerText = currChildSiteLink.PageName;

			string currChildSiteLinkUrl = "";
			if (currChildSiteLink.PageURL.StartsWith("/"))
			{ currChildSiteLinkUrl = currChildSiteLink.PageURL.Substring(1, currChildSiteLink.PageURL.Length - 1); }
			else
			{ currChildSiteLinkUrl = currChildSiteLink.PageURL; }

			if (currChildSiteLinkUrl.ToLower().EndsWith("signin"))
				lnkChildSiteLink.Attributes.Add("class", "dropdown-toggle headerLink loginlink");

			if (currChildSiteLinkUrl.ToLower().Contains("#modalgcbalance"))
			{
				lnkChildSiteLink.Attributes.Add("data-toggle", "modal");
			}

			  lnkChildSiteLink.HRef = currChildSiteLink.IsParent.Equals(true) ? "#" : host + currChildSiteLinkUrl;              
			  if (Session["Maersk_SSO_MGIS_User"] != null)
			  {
				  Exceptions.WriteSSOLog("inside Maersk_SSO_MGIS_User != null LINK URL = " + lnkChildSiteLink.HRef, MaerskSSOLogFolderPath);
			  }
			  if (Session["Maersk_SSO_OIL_User"] != null)
			  {
				  Exceptions.WriteSSOLog("inside Maersk_SSO_OIL_User != null LINK URL = " + lnkChildSiteLink.HRef, MaerskSSOLogFolderPath);
			  }

			if (currChildSiteLink.PageURL == "#")
			{
				lnkChildSiteLink.HRef = "return javascript:void(0);";
			}
			if (currChildSiteLink.PageType.Equals(2))
			{
				if (!currChildSiteLinkUrl.StartsWith("https://"))
				{
					lnkChildSiteLink.HRef = currChildSiteLinkUrl.StartsWith("http://") ? currChildSiteLinkUrl : "http://" + currChildSiteLinkUrl;
				}
				else
				{
					lnkChildSiteLink.HRef = currChildSiteLinkUrl;
				}
				if (currChildSiteLink.NewWindow == 1)
				{
					lnkChildSiteLink.Attributes.Add("target", "_blank");
				}
			}

			if (currChildSiteLink.IsParent.Equals(true))
			{
				HtmlGenericControl ulGrandChildSiteLinks = e.Item.FindControl("ulGrandChildSiteLinks") as HtmlGenericControl;

				if (LinkDisplayLocation.ToLower().Equals("header"))
					ulGrandChildSiteLinks.Attributes.Add("class", "dropdown-menu");

				Repeater rptGrandChildSiteLinks = ulGrandChildSiteLinks.FindControl("rptChildSiteLinks") as Repeater;
				List<StaticPageManagementBE> lstGrandChildLinks = (List<StaticPageManagementBE>)Session["lstItemz"];
				lstGrandChildLinks = lstGrandChildLinks.Where(x => x.ParentStaticPageId.Equals(currChildSiteLink.StaticPageId)).Cast<StaticPageManagementBE>().ToList();
				rptGrandChildSiteLinks.DataSource = lstGrandChildLinks;
				rptGrandChildSiteLinks.DataBind();
			}
		}

		protected void rptGrandChildSiteLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			StaticPageManagementBE currGrandChildSiteLink = e.Item.DataItem as StaticPageManagementBE;
			HtmlAnchor lnkChildSiteLink = e.Item.FindControl("lnkGrandChildSiteLink") as HtmlAnchor;

			lnkChildSiteLink.InnerText = currGrandChildSiteLink.PageName;

			string currGrandChildSiteLinkUrl = "";
			if (currGrandChildSiteLink.PageURL.StartsWith("/"))
				currGrandChildSiteLinkUrl = currGrandChildSiteLink.PageURL.Substring(1, currGrandChildSiteLink.PageURL.Length - 1);

			lnkChildSiteLink.HRef = host + currGrandChildSiteLinkUrl;
		}

		protected string BindNotification()
		{
			int intUserId = 0;
			int intCurrencyId = GlobalFunctions.GetCurrencyId();
			string UserSessionId = HttpContext.Current.Session.SessionID;
			if (Session["User"] != null)
			{
				UserBE objUserBE = (UserBE)Session["User"];
				intUserId = objUserBE.UserId;
			}
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary.Add("UserId", Convert.ToString(intUserId));
			dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
			dictionary.Add("UserSessionId", UserSessionId);
			int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_GetShoppingCartProducts, dictionary, true);
			return Convert.ToString(shopId);
		}

		protected void rptViewBasket_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				ShoppingCartBE CurrProduct = e.Item.DataItem as ShoppingCartBE;
				Label lblTotalProductCost = e.Item.FindControl("lblTotalProductCost") as Label;
				Label lblProductQuantity = e.Item.FindControl("lblProductQuantity") as Label;
				Label lblProductPrice = e.Item.FindControl("lblProductPrice") as Label;
				HtmlImage img1 = e.Item.FindControl("img1") as HtmlImage;

				double dPrice = Convert.ToDouble(CurrProduct.Quantity * CurrProduct.Price);//.ToString("##,###,##0.#0");
				string strPrice = dPrice.ToString("#######0.#0");

				double dUnitPrice = Convert.ToDouble(CurrProduct.Price);
				lblTotalProductCost.Text = " =  " + GlobalFunctions.DisplayPriceAndPoints(strPrice, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
				totalCheckoutAmount = totalCheckoutAmount + Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(strPrice, "", GlobalFunctions.GetLanguageId()));
			  lblProductPrice.Text = "@ " + GlobalFunctions.DisplayPriceAndPoints(dUnitPrice.ToString("#######0.#0"), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

				if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
					img1.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
				else
					img1.Src = host + "Images/Products/default.jpg";
			}
		}

		protected void rptViewBasket_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "DeleteItem")
			{
				try
				{
					string strUserSessionId = string.Empty;
					Int32 intUserId = 0;
					if (HttpContext.Current.Session["User"] != null)
					{
						UserBE lst = new UserBE();
						lst = HttpContext.Current.Session["User"] as UserBE;
						intUserId = lst.UserId;
					}
					else
					{ strUserSessionId = HttpContext.Current.Session.SessionID; }
					List<object> lstShoppingBE = new List<object>();
					lstShoppingBE = ShoppingCartBL.RemoveProductsFromBasket(e.CommandArgument.To_Int32(), intUserId, strUserSessionId);
					if (lstShoppingBE != null && lstShoppingBE.Count > 0)
					{
						Response.Redirect(Request.RawUrl);
					}
					else { }
				}
				catch (Exception ex)
				{ Exceptions.WriteExceptionLog(ex); }
			}
		}

		protected void BindResourceData()
		{
			try
			{
				intLanguageId = GlobalFunctions.GetLanguageId();
				intCurrencyId = GlobalFunctions.GetCurrencyId();
				lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
				if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
				{
					lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
					if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
					{
						Basket_MiniBasketHead1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketHead1" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
						Basket_MiniBasketHead2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketHead2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
						Basket_MiniBasketView = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketView" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
						Basket_MiniBasketCheckout = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketCheckout" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
						Basket_MiniBasketTotalBeforeDelivery = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketTotalBeforeDelivery" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
						Basket_MiniBasketFillMeUp = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketFillMeUp" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
						stror = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "RecieveCommunication_or_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;	
					}
				}
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		protected void lnkLLLogout_Click(object sender, EventArgs e)
		{
			Session["AccessToken"] = null;
			Response.Redirect("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:7482/index");
		}

		#region Added By Hardik for Maersk Phase 2 changes
		protected void rptViewBasket_new_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				ShoppingCartBE CurrProduct_new = e.Item.DataItem as ShoppingCartBE;
				Label lblTotalProductCost_new = e.Item.FindControl("lblTotalProductCost_new") as Label;
				Label lblProductQuantity_new = e.Item.FindControl("lblProductQuantity_new") as Label;
				double dPrice_new = Convert.ToDouble(CurrProduct_new.Quantity * CurrProduct_new.Price);
				string strPrice_new = dPrice_new.ToString("#######0.#0");

				double dUnitPrice_new = Convert.ToDouble(CurrProduct_new.Price);
				lblTotalProductCost_new.Text = GlobalFunctions.DisplayPriceOrPoints(strPrice_new, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
				totalCheckoutAmount_new = totalCheckoutAmount_new + Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(strPrice_new, "", GlobalFunctions.GetLanguageId()));
			}
		}

		/// <summary>
		/// Author  : Anoop Gupta
		/// Date    : 17-07-15
		/// Scope   : Binding the Menu listing on the page
		/// </summary>
		/// /// <param></param>
		/// <returns></returns>
		private void MenuDetails()
		{
			StoreBE objStoreBE = StoreBL.GetStoreDetails();
			List<CategoryBE> objCategoryBE;
			Dictionary<int, List<CategoryBE>> objDictAllCategoriesAllLanguages;
			try
			{
				#region "/*User Type*/"
				Int16 iUserTypeID = 1;
				if (Session["User"] != null)
				{ iUserTypeID = ((UserBE)Session["User"]).UserTypeID; }
				objCategoryBE = CategoryBL.GetAllCategories(intLanguageId, intCurrencyId, iUserTypeID);
				#endregion
				objDictAllCategoriesAllLanguages = CategoryBL.GetAllCategoriesAllLanguages();
				string strParentCategoryName = string.Empty;
				HtmlGenericControl parentUL = new HtmlGenericControl("ul");
				parentUL.Attributes.Add("id", "myCategory_new");

				#region Categories
				if (objCategoryBE != null)
				{
					List<CategoryBE> objParentCategory = objCategoryBE.FindAll(x => x.ParentCategoryId == 0);
					HtmlGenericControl parentLI1 = new HtmlGenericControl("li");
					HtmlAnchor parentAnchor1 = new HtmlAnchor();
					if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_HomeLink").FeatureValues[0].IsEnabled)
					{
						parentAnchor1.InnerText = "Home";
						parentAnchor1.Attributes.Add("class", "categoryText");
						parentAnchor1.HRef = host + "index";
						parentLI1.Controls.Add(parentAnchor1);
						parentUL.Controls.Add(parentLI1);
					}
					foreach (CategoryBE parentCategory in objParentCategory)
					{
						string Mouseovermenuurl = "";
						HtmlGenericControl parentLI = new HtmlGenericControl("li");
						HtmlAnchor parentAnchor = new HtmlAnchor();
						List<CategoryBE> objChildCategory = objCategoryBE.FindAll(x => x.ParentCategoryId == parentCategory.CategoryId);
						strParentCategoryName = parentCategory.CategoryName;
						if (StoreCategoryType == "Image")
						{
							if (parentCategory.MenuMouseoverExtension == "" || parentCategory.MenuMouseoverExtension == null)
							{
								parentAnchor.InnerText = strParentCategoryName;
								parentAnchor.Attributes.Add("class", "categoryText");
							}
							else
							{
								string strMouseOverImgname = host + "Admin/Images/Category/MouseOver/" + parentCategory.CategoryId + "_MouseOver_" + intLanguageId + parentCategory.MenuMouseoverExtension;
								parentAnchor.Attributes.Add("data-src", strMouseOverImgname);
							}
							parentAnchor.Attributes.Add("class", "menuLink");

						}
						if (objChildCategory.Count > 0)
						{
							parentLI.Attributes.Add("class", "");
							parentAnchor.Attributes.Add("class", "dropdown-toggle");
							parentAnchor.Attributes.Add("data-toggle", "dropdown");
							parentAnchor.HRef = "~/#";
						}
						else
						{
							parentAnchor.HRef = host + "Category/" + GlobalFunctions.EncodeCategoryURL(strParentCategoryName);
						}
						#region added by vikram to show category as image of as link

						if (StoreCategoryType == "Image")
						{
							if (parentCategory.MenuImageExtension == "" || parentCategory.MenuImageExtension == null)
							{
								parentAnchor.InnerText = strParentCategoryName;
								parentAnchor.Attributes.Add("class", "categoryText");
							}
							else
							{
								parentAnchor.InnerHtml = "<img alt='" + parentCategory.CategoryName + "' src='" + host + "Admin/Images/Category/categoryMenu/" + parentCategory.CategoryId + "_" + intLanguageId + parentCategory.MenuImageExtension + "'>";
							}
						}
						else
						{ parentAnchor.InnerText = strParentCategoryName; }
						#endregion                        					

						if (objChildCategory.Count == 0 && parentCategory.ProductCount == 0)
						{

						}
						else
						{
							parentLI.Controls.Add(parentAnchor);
						}

						if (objChildCategory.Count > 0)
						{
							HtmlGenericControl span = new HtmlGenericControl("span");
							if (StoreCategoryType != "Image")// only for link
							{
								span.Attributes.Add("class", "caret");
							}
							parentAnchor.Controls.Add(span);
							HtmlGenericControl childUL = new HtmlGenericControl("ul");
							childUL.Attributes.Add("class", "dropdown-menu");

							foreach (CategoryBE childCategory in objChildCategory)
							{
								HtmlGenericControl childLI = new HtmlGenericControl("li");
								HtmlAnchor childAnchor = new HtmlAnchor();
								List<CategoryBE> objSubSubCategory = objCategoryBE.FindAll(x => x.ParentCategoryId == childCategory.CategoryId);
								if (objSubSubCategory.Count > 0)
								{
									childAnchor.HRef = "";
									childLI.Attributes.Add("class", "dropdown");
									childAnchor.Attributes.Add("class", "dropdown-toggle");
									childAnchor.Attributes.Add("data-toggle", "dropdown");
									childAnchor.HRef = "~/#";
								}
								else
								{
									childAnchor.HRef = host + "SubCategory/" + GlobalFunctions.EncodeCategoryURL(strParentCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(childCategory.CategoryName);
								}
								childAnchor.InnerText = childCategory.CategoryName;
								if (objSubSubCategory.Count == 0 && childCategory.ProductCount == 0)
								{
								}
								else
								{
									if (childCategory.UserTypeStatus)
										childLI.Controls.Add(childAnchor);
								}
								SubMenuDetails(objCategoryBE, childCategory, childLI, childAnchor, strParentCategoryName);
								childUL.Controls.Add(childLI);
							}
							parentLI.Controls.Add(childUL);
						}

						if (objChildCategory.Count == 0 && parentCategory.ProductCount == 0)
						{
						}
						else
							parentUL.Controls.Add(parentLI);

						this.Controls.Add(parentUL);
					}
				}
				#endregion
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		}

		/// <summary>
		/// Author  : Anoop Gupta
		/// Date    : 17-07-15
		/// Scope   : Decode the special characters with the predefined text for each special character
		/// </summary>
		/// <param name="objCategoryBE"></param>
		/// <param name="parentCategory"></param>
		/// <param name="parentLI"></param>
		/// <param name="parentAnchor"></param>
		/// <param name="strParentCategoryName"></param>
		/// <returns></returns>
		private void SubMenuDetails(List<CategoryBE> objCategoryBE, CategoryBE parentCategory, HtmlGenericControl parentLI, HtmlAnchor parentAnchor, string strParentCategoryName)
		{
			try
			{
				List<CategoryBE> objChildCategory = objCategoryBE.FindAll(x => x.ParentCategoryId == parentCategory.CategoryId);
				if (objChildCategory.Count > 0)
				{
					parentLI.Style.Add("z-index", "10");
					parentAnchor.Attributes.Add("class", "abc");
					HtmlGenericControl span = new HtmlGenericControl("span");
					span.Attributes.Add("class", "caret");
					parentAnchor.InnerText += " ";
					parentAnchor.Controls.Add(span);
					HtmlGenericControl childUL = new HtmlGenericControl("ul");
					childUL.Attributes.Add("class", "dropdown-menu");

					foreach (CategoryBE childCategory in objChildCategory)
					{
						if (childCategory.ProductCount != 0)
						{
							HtmlGenericControl childLI = new HtmlGenericControl("li");
							HtmlAnchor childAnchor = new HtmlAnchor();
							childAnchor.HRef = host + "SubCategory/" + GlobalFunctions.EncodeCategoryURL(strParentCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(parentCategory.CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(childCategory.CategoryName);
							childAnchor.InnerText = childCategory.CategoryName;
							childAnchor.Attributes.Add("class", "menuLink");
							childLI.Controls.Add(childAnchor);
							SubMenuDetails(objCategoryBE, childCategory, childLI, childAnchor, parentCategory.CategoryName);
							childUL.Controls.Add(childLI);
						}
					}
					parentLI.Controls.Add(childUL);
				}
				else
					parentAnchor.Attributes.Add("class", "");
			}
			catch (Exception ex)
			{
				Exceptions.WriteExceptionLog(ex);
			}
		} 
		#endregion

	}
}
