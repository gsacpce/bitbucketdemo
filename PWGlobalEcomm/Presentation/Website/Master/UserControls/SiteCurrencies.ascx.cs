﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Master_UserControls_SiteCurrencies : System.Web.UI.UserControl
    {
        protected global::System.Web.UI.WebControls.Repeater rptSiteCurrencies;

        public string host = GlobalFunctions.GetVirtualPath();
        public string strCurrencyTitle = string.Empty;
        public string strCurrencyChangeMessage = string.Empty;
        string strUserSessionId = HttpContext.Current.Session.SessionID;
        Boolean IsAnonymousBasketEmpty = true;
        public Int16 PCount = 0;
        UserBE objUSerBE = new UserBE();
        UserTypesBE objlstUserTypesBE = new UserTypesBE();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindResourceData();

                /*Sachin Chauhan Start : 09 02 2016 : Added below lines of code to access langauage & currency count for the store*/
                int intUserId = 0;

                if (Session["User"] != null)
                {
                    intUserId = ((UserBE)Session["User"]).UserId;
                }
                List<object> lstShoppingBE = new List<object>();
                lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(),
                                                                    GlobalFunctions.GetCurrencyId(),
                                                                    intUserId, strUserSessionId);
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    IsAnonymousBasketEmpty = false;
                    PCount = lstShoppingBE.Count.To_Int16();
                }

                StoreBE getStores;
                if (HttpRuntime.Cache["StoreDetails"] == null)
                {
                    getStores = StoreDA.getItem(Constants.USP_GetAllStoreDetails, null, true);
                    HttpRuntime.Cache.Insert("StoreDetails", getStores);
                }
                else
                {
                    getStores = (StoreBE)HttpRuntime.Cache["StoreDetails"];
                }

                List<StoreBE.StoreCurrencyBE> lstCurrencies = new List<StoreBE.StoreCurrencyBE>();
                lstCurrencies = getStores.StoreCurrencies.Where(x => x.IsActive.Equals(true)).ToList();

                #region "UserType"
                if (Session["User"] != null)
                {
                    objUSerBE = HttpContext.Current.Session["User"] as UserBE;
                    // Added by SHRIGANESH SINGH 01 July 2016
                    List<StoreBE.StoreCurrencyBE> lstAllCurrencies = new List<StoreBE.StoreCurrencyBE>();
                    List<StoreBE.StoreCurrencyBE> lstDefaultCurrencies = new List<StoreBE.StoreCurrencyBE>();

                    if (objUSerBE.UserTypeID > 0)
                    {
                        objlstUserTypesBE = UserTypesBL.getCollectionItem(Constants.USP_ManageUserType_SAED, null, true);
                        if (objlstUserTypesBE.lstUserTypeCatalogue.Count > 0)
                        {
                            List<UserTypesBE.UserTypeCatalogue> objUserTypeCatalogue = objlstUserTypesBE.lstUserTypeCatalogue.FindAll(x => x.IsActive == true && x.UserTypeID == objUSerBE.UserTypeID);
                            List<StoreBE.StoreCurrencyBE> objCur = new List<StoreBE.StoreCurrencyBE>();
                            objCur = (from a in lstCurrencies.AsEnumerable()
                                      where objUserTypeCatalogue.Any(xc => xc.CatalogueID.Equals(a.CatalogueId))
                                      select a).ToList();

                            if (Session["IndeedSSO_User"] != null || Session["IndeedEmployeeSSO_User"] != null)
                            {
                                if (Session["UserCatalogueID"] != null)
                                {
                                    Int16 UserCatalogueID = Convert.ToInt16(Session["UserCatalogueID"].ToString());
                                    objCur = objCur.FindAll(x => x.CatalogueId == UserCatalogueID);
                                }
                                if (Session["EmployeeUserCatalogueID"] != null)
                                {
                                    Int16 UserCatalogueID = Convert.ToInt16(Session["EmployeeUserCatalogueID"].ToString());
                                    objCur = objCur.FindAll(x => x.CatalogueId == UserCatalogueID);
                                }
                            }

                            var aCurrency = (from y in objCur
                                             select new { y.CurrencySymbol, y.CurrencyName, y.CurrencyId, y.CurrencyCode });        

                            //if (objCur.Count() == 1)
                            //{
                            //    Int16 CurrentCurrencyId = 0;
                            //    var newcurrency = objCur.FirstOrDefault();
                            //    CurrentCurrencyId = newcurrency.CurrencyId;
                            //    GlobalFunctions.SetCurrencyId(CurrentCurrencyId);
                            //}

                            rptSiteCurrencies.DataSource = aCurrency;
                            rptSiteCurrencies.DataBind();
                        }
                    }
                    else
                    {
                        if (getStores.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_usertype").FeatureValues[0].IsEnabled == true)
                        {
                            lstAllCurrencies = lstCurrencies;
                            objlstUserTypesBE = UserTypesBL.getCollectionItem(Constants.USP_ManageUserType_SAED, null, true);
                            List<UserTypesBE.UserTypeCatalogue> objUserTypeCatalogues = objlstUserTypesBE.lstUserTypeCatalogue.FindAll(x => x.UserTypeID == 1);

                            List<StoreBE.StoreCurrencyBE> objCur = new List<StoreBE.StoreCurrencyBE>();
                            objCur = (from a in lstAllCurrencies.AsEnumerable()
                                      where objUserTypeCatalogues.Any(xc => xc.UserTypeID != 1 && xc.CatalogueID.Equals(a.CatalogueId))
                                      select a).ToList();

                            rptSiteCurrencies.DataSource = objCur;// lstDefaultCurrencies;
                            rptSiteCurrencies.DataBind();
                        }
                        else
                        {
                            rptSiteCurrencies.DataSource = lstCurrencies;
                            rptSiteCurrencies.DataBind();
                        }
                    }
                }
                else
                {
                    // Added by SHRIGANESH SINGH 01 July 2016
                    List<StoreBE.StoreCurrencyBE> lstAllCurrencies = new List<StoreBE.StoreCurrencyBE>();
                    List<StoreBE.StoreCurrencyBE> lstDefaultCurrencies = new List<StoreBE.StoreCurrencyBE>();
                    if (getStores.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_usertype").FeatureValues[0].IsEnabled == true)
                    {
                        //filter out 
                        lstAllCurrencies = lstCurrencies;
                        objlstUserTypesBE = UserTypesBL.getCollectionItem(Constants.USP_ManageUserType_SAED, null, true);

                        List<UserTypesBE.UserTypeCatalogue> objUserTypeCatalogues = objlstUserTypesBE.lstUserTypeCatalogue.FindAll(x => x.UserTypeID == 1);

                        List<StoreBE.StoreCurrencyBE> objCur = new List<StoreBE.StoreCurrencyBE>();
                        objCur = (from a in lstAllCurrencies.AsEnumerable()
                                  where objUserTypeCatalogues.Any(xc => xc.UserTypeID == 1 && xc.CatalogueID.Equals(a.CatalogueId))
                                  select a).ToList();

                        rptSiteCurrencies.DataSource = objCur;//lstDefaultCurrencies
                        rptSiteCurrencies.DataBind();
                    }
                    else
                    {
                        rptSiteCurrencies.DataSource = lstCurrencies;
                        rptSiteCurrencies.DataBind();
                    }
                }
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #region Site currency item command commented
        protected void rptSiteCurrencies_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                /*Empty previous anonymous basket in case of currency change*/
                if (Session["PrevCurrencyId"] != null)
                {
                    if (Session["PrevCurrencyId"].To_Int16() != 0)
                    {
                        if (Session["User"] == null && !Session["PrevCurrencyId"].To_Int16().Equals(GlobalFunctions.GetCurrencyId()))
                        {
                            ShoppingCartBL.RemoveAllBasketProducts(strUserSessionId);
                            IsAnonymousBasketEmpty = true;
                            Response.RedirectToRoute("Index");
                        }
                    }
                }
                if (Session["User"] != null && !Session["PrevCurrencyId"].To_Int16().Equals(GlobalFunctions.GetCurrencyId()))
                {
                    /*Sachin Chauhan Start : 10 02 2016 : Below code ensures that on changing currency 
                     *                                    the user's basysid should also get updated.
                     *                                    When placing an Order correct basys id of user should pass to order xml
                    */
                    UserBE objUserBE = Session["User"] as UserBE;

                    /*If User is GUEST user empty his basket*/
                    if (objUserBE.IsGuestUser)
                    {
                        ShoppingCartBL.RemoveAllBasketProducts(strUserSessionId);
                        objUserBE.EmailId = objUserBE.EmailId;
                        objUserBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objUserBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    }
                    else if (objUserBE.IsGuestUser.Equals(false))
                    {
                        objUserBE.EmailId = objUserBE.EmailId;
                        objUserBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objUserBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        Session["User"] = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objUserBE);

                    }
                    /*Sachin Chauhan End : 10 02 2016*/
                    Response.RedirectToRoute("Index");
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #endregion
        protected void rptSiteCurrencies_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                //HtmlAnchor lnkCurrency = sender as HtmlAnchor;
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string strCurrencyId = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CurrencyId"));
                    string strCurrencyCode = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CurrencyCode"));
                    string strCurrencySymbol = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CurrencySymbol"));
                    string strLanguageId = GlobalFunctions.GetLanguageId().ToString();
                    HyperLink lnkSiteCurrency = e.Item.FindControl("lnkSiteCurrency") as HyperLink;

                    lnkSiteCurrency.ID += strCurrencyId + "|" + strCurrencyCode;
                    if (strCurrencyId.Equals(GlobalFunctions.GetCurrencyId().ToString()))
                    {
                        //lnkSiteCurrency.BackColor = System.Drawing.Color.FromArgb(0 0 0);
                        lnkSiteCurrency.Attributes.Add("class", "sitelink active");
                    }
                    if (Session["User"] == null)
                    {
                        lnkSiteCurrency.Attributes.Add("onclick", "javascript:return false;");

                        if (IsAnonymousBasketEmpty.Equals(false) && Session["user"] == null)
                            lnkSiteCurrency.Attributes.Add("onclick", "funCurrencyChange('" + strCurrencyId + "','" + strCurrencySymbol + "','" + strLanguageId + "');");
                        if (IsAnonymousBasketEmpty)
                            lnkSiteCurrency.Attributes.Add("onclick", "funCurrencyChange('" + strCurrencyId + "','" + strCurrencySymbol + "','" + strLanguageId + "');");
                        if (Session["user"] != null)
                            lnkSiteCurrency.Attributes.Add("onclick", "funCurrencyChange('" + strCurrencyId + "','" + strCurrencySymbol + "','" + strLanguageId + "');");
                        if (strCurrencyId.Equals(GlobalFunctions.GetCurrencyId().ToString()))
                            lnkSiteCurrency.Attributes.Add("onclick", "javascript:return false;");
                    }
                    else
                    {
                        lnkSiteCurrency.Attributes.Add("onclick", "funCurrencyChange('" + strCurrencyId + "','" + strCurrencySymbol + "','" + strLanguageId + "');");
                    }


                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 09-02-2015
        /// Scope   : BindResourceData of the Master page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strCurrencyTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_CurrencyTitle").ResourceValue;
                        strCurrencyChangeMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_ChangeCurrencyMessage").ResourceValue;

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

        }
    }
}
