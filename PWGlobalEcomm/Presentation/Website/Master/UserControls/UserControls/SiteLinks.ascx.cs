﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Text;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;


namespace Presentation
{
    public partial class Master_UserControls_SiteLinks : System.Web.UI.UserControl
    {

        protected global::System.Web.UI.WebControls.Repeater rptHeaderLinks, rptFooterLinks;
        protected global::System.Web.UI.WebControls.Repeater rptChildSiteLinks;
        // Added by PJ*** 09/25/15 for basket counter
        protected global::System.Web.UI.WebControls.Repeater rptViewBasket;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnCounter, spnItemCount, quickbasket, dv1, dv2;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgBasket;
        protected global::System.Web.UI.WebControls.Label lblProductTitle, lblProductDescription, lblProductQuantity, lblProductPrice, lblTotalProductCost, lblTotalCheckoutAmount;
        protected global::System.Web.UI.WebControls.Literal ltrCounter;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aButton2, aButton1;

        double totalCheckoutAmount = 0;
        // End by PJ*** 09/25/15 for basket counter
        protected string linkDisplayLocation;
        protected string linkDisplayStyle;
        public string host = GlobalFunctions.GetVirtualPath();

        public string Basket_MiniBasketHead1, Basket_MiniBasketHead2, Basket_MiniBasketView, Basket_MiniBasketCheckout,
                      Basket_MiniBasketTotalBeforeDelivery, Basket_MiniBasketFillMeUp;

        public string LinkDisplayLocation
        {
            get { return linkDisplayLocation; }
            set { linkDisplayLocation = value; }
        }

        public string LinkDisplayStyle
        {
            get { return linkDisplayStyle; }
            set { linkDisplayStyle = value; }
        }

        static int intLanguageId;
        static int intCurrencyId;

        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();

        List<StaticPageManagementBE> lstStaticLinksFullList = new List<StaticPageManagementBE>();
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
        StoreBE objStoreBE = new StoreBE();


        protected void Page_Load(object sender, EventArgs e)
        {
            StaticPageManagementBE objBE = new StaticPageManagementBE();
            objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
            objBE.PageName = "";
            objStoreBE = StoreBL.GetStoreDetails();
            bool bHeader = false;
            lstStaticLinksFullList = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "All");
            if (lstStaticLinksFullList != null)
            {
                Session["lstItemz"] = lstStaticLinksFullList;
                lstStaticLinksFullList = lstStaticLinksFullList.Where(x => x.IsActive.Equals(true) && x.DisplayLocation.ToLower().Equals(LinkDisplayLocation)).Cast<StaticPageManagementBE>().OrderBy(x => x.DisplayOrder).ToList();
                if (lstStaticLinksFullList.Count > 0)
                {
                    List<StaticPageManagementBE> lstParentStaticLinks;
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_HomeLink").FeatureValues[1].IsEnabled)
                    {
                        lstParentStaticLinks = lstStaticLinksFullList.
                                                Where(y => y.ParentStaticPageId.Equals(0) && y.StaticPageId != 26).
                                                Cast<StaticPageManagementBE>().ToList();
                    }
                    else
                    {
                        lstParentStaticLinks = lstStaticLinksFullList.
                                                                         Where(y => y.ParentStaticPageId.Equals(0) && y.StaticPageId != 26 && y.StaticPageId != 12).
                                                                         Cast<StaticPageManagementBE>().ToList();
                    }


                    HtmlGenericControl ulSiteLinks = this.FindControl("ulSiteLinks") as HtmlGenericControl;
                    if (LinkDisplayStyle.ToLower().Equals("vertical"))
                        ulSiteLinks.Attributes.Remove("class");

                    if (LinkDisplayLocation.ToLower().Equals("header"))
                    {
                        string strStoreAccess = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CR_StoreAccess").FeatureValues.FirstOrDefault(y => y.IsEnabled == true).FeatureValue;
                        if (Session["User"] == null)
                        {
                            if (strStoreAccess.ToLower() == "mandatory login")
                            {
                                bHeader = true;
                            }
                        }
                        if (!bHeader)
                        {
                            rptHeaderLinks.DataSource = lstParentStaticLinks;
                            rptHeaderLinks.DataBind();
                        }
                    }

                    if (LinkDisplayLocation.ToLower().Equals("footer"))
                    {
                        rptFooterLinks.DataSource = lstParentStaticLinks;
                        rptFooterLinks.DataBind();
                    }

                }
            }
            BindResourceData();
        }

        protected void rptHeaderLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            StaticPageManagementBE currSiteLink = e.Item.DataItem as StaticPageManagementBE;
            /*Sachin Chauhan Start:19 01 2015: Changed link button server control to html anchor tag */
            //LinkButton lnkSiteLink = e.Item.FindControl("lnkSiteLink") as LinkButton;
            HtmlAnchor lnkSiteLink = e.Item.FindControl("lnkSiteLink") as HtmlAnchor;

            //lnkSiteLink.Attributes.Add("data-toggle", "dropdown");
            HtmlGenericControl liSiteLink = e.Item.FindControl("liSiteLink") as HtmlGenericControl;
            HtmlGenericControl spnItemCount = e.Item.FindControl("spnItemCount") as HtmlGenericControl;

            lnkSiteLink.InnerText = currSiteLink.PageName;
            lnkSiteLink.InnerText = WebUtility.HtmlDecode(currSiteLink.PageName);
            string currSiteLinkUrl = "";

            // START by PJ*** 09/25/15 for basket counter
            #region "Basket Link"
            if (currSiteLink.PageURL.ToLower().Contains("basket") || currSiteLink.PageURL.ToLower().Contains("shoppingcart"))
            {
                liSiteLink.Attributes.Add("class", "BasketLink hidden-xs");

                //HtmlImage imgBasket = new HtmlImage();
                //imgBasket.Src = host +"Images/UI/icon_basket.png";
                //imgBasket.Height = 50;
                //lnkSiteLink.Controls.Add(imgBasket);

                //lnkSiteLink.Text = "<img src=\""+ host + "Images/UI/icon_basket.png\" height=\"50\" id=\"imgBasket\" />&nbsp;" + currSiteLink.PageName + " ( " + BindNotification() + " )";
                //lnkSiteLink.Text = "<img src=\"" + host + "Images/UI/icon_basket.png\" height=\"50\" id=\"imgBasket\" />&nbsp; <span class=\"basket_qty_lg menuBg\">" + BindNotification() + "</span>" + currSiteLink.PageName + "";
                // lnkSiteLink.InnerHtml = "<img src=\"" + host + "Images/UI/icon_basket.png\" height=\"50\" id=\"imgBasket\" />&nbsp; <span class=\"basket_qty_lg menuBg1\">0</span>" + currSiteLink.PageName + "";
                lnkSiteLink.InnerHtml = "<i class='fa fa-shopping-basket' aria-hidden='true'></i>&nbsp; <span class=\"basket_qty_lg menuBg1\">0</span>" + currSiteLink.PageName + "";

                int intUserId = 0;
                Int16 intCurrencyId, intLanguageId;
                quickbasket = liSiteLink.FindControl("quickbasket") as HtmlGenericControl;
                quickbasket.Visible = true;
                dv1 = quickbasket.FindControl("dv1") as HtmlGenericControl;
                dv2 = quickbasket.FindControl("dv2") as HtmlGenericControl;
                aButton1 = quickbasket.FindControl("aButton1") as HtmlAnchor;
                aButton2 = quickbasket.FindControl("aButton2") as HtmlAnchor;
                string strUserSessionId = "";
                try
                {

                    if (Session["User"] != null)
                    {
                        UserBE lst = new UserBE(); lst = Session["User"] as UserBE; intUserId = lst.UserId;
                        aButton1.Visible = true;
                    }
                    else
                    {
                        strUserSessionId = HttpContext.Current.Session.SessionID;
                        aButton1.Visible = false;
                    }
                    #region Added by Sripal for Punchout"
                    if (Session["S_ISPUNCHOUT"] != null)
                    {
                        aButton1.Visible = false;
                    }
                    #endregion
                    intCurrencyId = GlobalFunctions.GetCurrencyId();
                    intLanguageId = GlobalFunctions.GetLanguageId();
                    List<object> lstShoppingBE = new List<object>();
                    lstShoppingBE = ShoppingCartBL.GetBasketProductsData(intLanguageId, intCurrencyId, intUserId, strUserSessionId);
                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        // lnkSiteLink.InnerHtml = "<img src=\"" + host + "Images/UI/icon_basket.png\" height=\"50\" id=\"imgBasket\" />&nbsp; <span class=\"basket_qty_lg menuBg1\">" + lstShoppingBE.Count + "</span>" + currSiteLink.PageName + "";
                        lnkSiteLink.InnerHtml = "<i class='fa fa-shopping-basket' aria-hidden='true'></i>&nbsp; <span class=\"basket_qty_lg menuBg1\">" + lstShoppingBE.Count + "</span>" + currSiteLink.PageName + "";
                        spnItemCount.InnerHtml = Convert.ToString(lstShoppingBE.Count);
                        Repeater rptViewBasket = e.Item.FindControl("rptViewBasket") as Repeater;
                        rptViewBasket.DataSource = lstShoppingBE;
                        rptViewBasket.DataBind();
                        Label lblTotalCheckoutAmount = e.Item.FindControl("lblTotalCheckoutAmount") as Label;
                        if (GlobalFunctions.IsPointsEnbled())
                            lblTotalCheckoutAmount.Text = Convert.ToString(totalCheckoutAmount) + GlobalFunctions.PointsText(intLanguageId);
                        else
                            lblTotalCheckoutAmount.Text = GlobalFunctions.GetCurrencySymbol() + totalCheckoutAmount.ToString("f");
                        aButton2.HRef = GlobalFunctions.GetVirtualPath() + "Checkout/ShoppingCart";

                        aButton1.HRef = GlobalFunctions.GetVirtualPath() + "Checkout";
                        dv1.Visible = true;
                        dv2.Visible = false;
                    }
                    else
                    {
                        spnItemCount.InnerHtml = "0";
                        dv1.Visible = false;
                        dv2.Visible = true;
                    }
                }
                catch (Exception ex)
                { Exceptions.WriteExceptionLog(ex); }
            }
            //END by PJ*** 09/25/15 for basket counter
            #endregion
            if (currSiteLink.PageURL.StartsWith("/"))
                currSiteLinkUrl = currSiteLink.PageURL.Substring(1, currSiteLink.PageURL.Length - 1);
            else
                currSiteLinkUrl = currSiteLink.PageURL;

            lnkSiteLink.Attributes.Add("class", "dropdown-toggle headerLink");


            //lnkSiteLink.PostBackUrl = currSiteLink.IsParent.Equals(true) ? "#" : host + currSiteLinkUrl;
            #region commented by vikram to show the link specified by the client for page type 2 (another website or url link)
            //lnkSiteLink.HRef = currSiteLink.IsParent.Equals(true) ? "#" : host + currSiteLinkUrl;
            //if (currSiteLink.PageType.Equals(2))
            //{
            //    lnkSiteLink.HRef = currSiteLinkUrl.StartsWith("http://") ? currSiteLinkUrl : "http://" + currSiteLinkUrl;
            //    lnkSiteLink.Attributes.Add("target", "_blank");
            //}

            if (currSiteLink.PageType.Equals(2))
            {
                if (currSiteLinkUrl.StartsWith("https://"))
                    lnkSiteLink.HRef = currSiteLinkUrl;
                else if (currSiteLinkUrl.StartsWith("http://"))
                    lnkSiteLink.HRef = currSiteLinkUrl;
                else
                    lnkSiteLink.HRef = "http://" + currSiteLinkUrl;

                // lnkSiteLink.HRef = currSiteLinkUrl.StartsWith("https://") ? currSiteLinkUrl : "http://" + currSiteLinkUrl;
                lnkSiteLink.Attributes.Add("target", "_blank");
            }
            else
            {
                lnkSiteLink.HRef = currSiteLink.IsParent.Equals(true) ? "#" : host + currSiteLinkUrl;
            }
            #endregion
            if (currSiteLink.NewWindow.Equals(1))
                lnkSiteLink.Attributes.Add("target", "_blank");

            if (currSiteLink.PageURL.ToLower().Contains("#modalcontactus"))
                lnkSiteLink.Attributes.Add("data-toggle", "modal");

            if (currSiteLink.IsSystemLink && currSiteLink.IsParent == false)
                lnkSiteLink.HRef = "~/" + currSiteLinkUrl;


            if (currSiteLink.IsParent.Equals(true))
            {

                lnkSiteLink.HRef = "return javascript:void(0);";

                //liSiteLink.Attributes.Add("class", "dropdown headerLink");Commented by Sripal
                liSiteLink.Attributes.Add("class", "dropdown sitelink ");//added by Sripal sitelink 


                //lnkSiteLink.Text += "<span class=\"caret\"></span>";
                lnkSiteLink.InnerHtml += "<span class=\"caret\"></span>";
                lnkSiteLink.Attributes.Add("class", "dropdown-toggle headerLink");
                lnkSiteLink.Attributes.Add("data-toggle", "dropdown");

                HtmlGenericControl ulChildSiteLinks = e.Item.FindControl("ulChildSiteLinks") as HtmlGenericControl;
                ulChildSiteLinks.Visible = true;
                ulChildSiteLinks.Attributes.Add("class", "dropdown-menu");
                Repeater rptChildSiteLinks = ulChildSiteLinks.FindControl("rptChildSiteLinks") as Repeater;
                List<StaticPageManagementBE> lstChildLinks = (List<StaticPageManagementBE>)Session["lstItemz"];
                lstChildLinks = lstChildLinks.Where(x => x.ParentStaticPageId.Equals(currSiteLink.StaticPageId) && x.IsActive.Equals(true)).Cast<StaticPageManagementBE>().ToList();

                #region REGION MY ACCOUNT
                if (currSiteLink.PageAliasName.ToLower().Contains("my account"))
                {
                    if (Session["User"] != null)
                    {
                        UserBE user = Session["User"] as UserBE;

                        #region CODE COMMENTED BY SHRIGANESH 25 May 2016 as Check GC Balance is moved under GIFT CERTIFICATE link
                        //#region MyRegion CODE ADDED BY SHRIGANESH SINGH TO REMOVE GC BALANCE CHECK IF GC IS NOT ENABLED 05 May 2016
                        //StoreBE objStoreBE = new StoreBE();
                        //objStoreBE = StoreBL.GetStoreDetails();
                        //if (objStoreBE != null)
                        //{
                        //    if (!(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_GiftCertificate").FeatureValues[0].IsEnabled))
                        //    {
                        //        lstChildLinks.RemoveAll(x => x.PageName.ToLower().Contains("check gc"));
                        //    }
                        //}
                        //#endregion 
                        #endregion
                        #region Vikram to Hide Wish list if disabled
                        //StoreBE objStoreBE = new StoreBE();
                        //objStoreBE = StoreBL.GetStoreDetails();
                        if (objStoreBE != null)
                        {
                            if (!(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableWishlist").FeatureValues[0].IsEnabled))
                            {
                                lstChildLinks.RemoveAll(x => x.PageName.ToLower().Contains("my wishlist"));
                            }
                        }
                        #endregion
                        //Changed position to Top of True GuestUser
                        /*Sachin Chauhan Start: 09 11 2015 : Change login link to logout*/
                        StaticPageManagementBE loginLink = lstChildLinks.FirstOrDefault(x => x.PageURL.ToLower().Contains("signin"));

                        if (loginLink != null && user.IsGuestUser.Equals(false))
                        {
                            //loginLink.PageURL = "Logout";
                            //loginLink.PageName = "Logout";
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signin");

                        }
                        /*Sachin Chauhan End: 09 11 2015 */

                        if (user.IsGuestUser == true)
                        {
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signout");
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("orderhistory"));
                            if (Session["IsLogin"] != null)
                            { lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("myaccount")); }
                        }
                        else
                        {
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signin");
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "userregistration");
                        }
                    }
                    else
                    {
                        if (Session["AccessToken"]!=null)
                        {
                            //loginLink.PageURL = "Logout";
                            //loginLink.PageName = "Logout";
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signin");
                            lstChildLinks.RemoveAll(x => x.PageName == "Edit Profile");
                            lstChildLinks.RemoveAll(x => x.PageName.Contains("My WishList"));
                            lstChildLinks.RemoveAll(x => x.PageName.Contains("Register"));
                           // lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("orderhistory"));
                         //  lstChildLinks.FirstOrDefault(x => x.PageURL.ToLower() == "signout").PageURL = @"https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=" + host + "login/login.aspx";
                        }
                        else
                        {
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower() == "signout");
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("myaccount"));
                            lstChildLinks.RemoveAll(x => x.PageURL.ToLower().Contains("orderhistory"));
                            // Commented by SHRIGANESH SINGH as this link is no longer under MY ACCOUNT 25 May 2016
                            //lstChildLinks.RemoveAll(x => x.PageName.ToLower().Contains("check gc")); // Added by SHRIGANESH SINGH TO REMOVE GB BALANCE LINK
                        }
                    }
                }
                #endregion

                #region REGION GIFT CERTIFICATE ADDED BY SHRIGANESH SINGH
                //if (currSiteLink.PageAliasName.ToLower().Contains("gift certificate"))
                //{
                //    if (Session["User"] == null)
                //    {
                //        StaticPageManagementBE loginLink = lstChildLinks.FirstOrDefault(x => x.PageURL.ToLower().Contains("#modalgcbalance"));
                //        loginLink.PageURL = "SignIn";
                //    }
                //}
                #endregion

                rptChildSiteLinks.DataSource = lstChildLinks;
                rptChildSiteLinks.DataBind();
            }
        }

        protected void rptFooterLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            StaticPageManagementBE currSiteLink = e.Item.DataItem as StaticPageManagementBE;
            //LinkButton lnkSiteLink = e.Item.FindControl("lnkSiteLink") as LinkButton;
            HtmlAnchor lnkSiteLink = e.Item.FindControl("lnkSiteLink") as HtmlAnchor;


            if (LinkDisplayLocation.ToLower().Equals("header"))
                lnkSiteLink.Attributes.Add("class", "dropdown-toggle");

            HtmlGenericControl liSiteLink = e.Item.FindControl("liSiteLink") as HtmlGenericControl;

            if (LinkDisplayLocation.ToLower().Equals("header"))
                liSiteLink.Attributes.Add("class", "dropdown sitelink");//added by Sripal sitelink 

            lnkSiteLink.InnerText = currSiteLink.PageName;
            string currSiteLinkUrl = "";



            if (currSiteLink.PageURL.StartsWith("/"))
                currSiteLinkUrl = currSiteLink.PageURL.Substring(1, currSiteLink.PageURL.Length - 1);
            else
                currSiteLinkUrl = currSiteLink.PageURL;

            //lnkSiteLink.PostBackUrl = currSiteLink.IsParent.Equals(true) ? "#" : host + currSiteLinkUrl;
            lnkSiteLink.HRef = currSiteLink.IsParent.Equals(true) ? "#" : host + currSiteLinkUrl;
            StaticPageManagementBE objCurrSiteLink = e.Item.DataItem as StaticPageManagementBE;
            if (objCurrSiteLink.PageType.Equals(2))
            {
                lnkSiteLink.HRef = currSiteLinkUrl.StartsWith("http://") ? currSiteLinkUrl : "http://" + currSiteLinkUrl;
                lnkSiteLink.Attributes.Add("target", "_blank");
            }

            if (objCurrSiteLink.NewWindow.Equals(1))
                lnkSiteLink.Attributes.Add("target", "_blank");

            if (objCurrSiteLink.PageURL.ToLower().Contains("#modalcontactus"))
                lnkSiteLink.Attributes.Add("data-toggle", "modal");


            if (currSiteLink.IsSystemLink)
                lnkSiteLink.HRef = "~/" + currSiteLinkUrl;


            if (currSiteLink.IsParent.Equals(true))
            {
                //lnkSiteLink.OnClientClick = "return false;"; 
                lnkSiteLink.HRef = "return javascript:void(0);";
            }

            /*if (currSiteLink.IsParent.Equals(true))
            {
                HtmlGenericControl ulChildSiteLinks = e.Item.FindControl("ulChildSiteLinks") as HtmlGenericControl;
                
                if (LinkDisplayLocation.ToLower().Equals("header"))
                    ulChildSiteLinks.Attributes.Add("class", "dropdown-menu");
                
                Repeater rptChildSiteLinks = ulChildSiteLinks.FindControl("rptChildSiteLinks") as Repeater;
                List<StaticPageManagementBE> lstChildLinks = (List<StaticPageManagementBE>)Session["lstItemz"];
                lstChildLinks = lstChildLinks.Where(x => x.ParentStaticPageId.Equals(currSiteLink.StaticPageId)).Cast<StaticPageManagementBE>().ToList();

                rptChildSiteLinks.DataSource = lstChildLinks;
                rptChildSiteLinks.DataBind();
            }*/
        }

        protected void rptChildSiteLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            StaticPageManagementBE currChildSiteLink = e.Item.DataItem as StaticPageManagementBE;
            //LinkButton lnkChildSiteLink = e.Item.FindControl("lnkChildSiteLink") as LinkButton;
            HtmlAnchor lnkChildSiteLink = e.Item.FindControl("lnkChildSiteLink") as HtmlAnchor;

            if (LinkDisplayLocation.ToLower().Equals("header"))
                lnkChildSiteLink.Attributes.Add("class", "dropdown-toggle");
            HtmlGenericControl liChildSiteLink = e.Item.FindControl("liChildSiteLink") as HtmlGenericControl;

            if (LinkDisplayLocation.ToLower().Equals("header"))
                liChildSiteLink.Attributes.Add("class", "dropdown sitelink");//added by Sripal sitelink 

            //lnkChildSiteLink.Text = currChildSiteLink.PageName;
            lnkChildSiteLink.InnerText = currChildSiteLink.PageName;

            string currChildSiteLinkUrl = "";
            if (currChildSiteLink.PageURL.StartsWith("/"))
            { currChildSiteLinkUrl = currChildSiteLink.PageURL.Substring(1, currChildSiteLink.PageURL.Length - 1); }
            else
            { currChildSiteLinkUrl = currChildSiteLink.PageURL; }

            if (currChildSiteLinkUrl.ToLower().EndsWith("signin"))
                lnkChildSiteLink.Attributes.Add("class", "dropdown-toggle headerLink loginlink");

            if (currChildSiteLinkUrl.ToLower().Contains("#modalgcbalance"))
            {
                lnkChildSiteLink.Attributes.Add("data-toggle", "modal");
            }

            //lnkChildSiteLink.PostBackUrl = currChildSiteLink.IsParent.Equals(true) ? "#" : host + currChildSiteLinkUrl;
            lnkChildSiteLink.HRef = currChildSiteLink.IsParent.Equals(true) ? "#" : host + currChildSiteLinkUrl;


            if (currChildSiteLink.PageURL == "#")
            {
                //lnkChildSiteLink.OnClientClick = "return false;";
                lnkChildSiteLink.HRef = "return javascript:void(0);";
            }
            if (currChildSiteLink.PageType.Equals(2))
            {
                if (!currChildSiteLinkUrl.StartsWith("https://"))
                {
                    lnkChildSiteLink.HRef = currChildSiteLinkUrl.StartsWith("http://") ? currChildSiteLinkUrl : "http://" + currChildSiteLinkUrl;
                }
                else
                {
                    lnkChildSiteLink.HRef = currChildSiteLinkUrl;
                }
                if (currChildSiteLink.NewWindow == 1)
                {
                    lnkChildSiteLink.Attributes.Add("target", "_blank");
                }
            }


            if (currChildSiteLink.IsParent.Equals(true))
            {
                HtmlGenericControl ulGrandChildSiteLinks = e.Item.FindControl("ulGrandChildSiteLinks") as HtmlGenericControl;

                if (LinkDisplayLocation.ToLower().Equals("header"))
                    ulGrandChildSiteLinks.Attributes.Add("class", "dropdown-menu");

                Repeater rptGrandChildSiteLinks = ulGrandChildSiteLinks.FindControl("rptChildSiteLinks") as Repeater;
                List<StaticPageManagementBE> lstGrandChildLinks = (List<StaticPageManagementBE>)Session["lstItemz"];
                lstGrandChildLinks = lstGrandChildLinks.Where(x => x.ParentStaticPageId.Equals(currChildSiteLink.StaticPageId)).Cast<StaticPageManagementBE>().ToList();
                rptGrandChildSiteLinks.DataSource = lstGrandChildLinks;
                rptGrandChildSiteLinks.DataBind();
            }
        }

        protected void rptGrandChildSiteLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            StaticPageManagementBE currGrandChildSiteLink = e.Item.DataItem as StaticPageManagementBE;

            //LinkButton lnkChildSiteLink = e.Item.FindControl("lnkGrandChildSiteLink") as LinkButton;
            HtmlAnchor lnkChildSiteLink = e.Item.FindControl("lnkGrandChildSiteLink") as HtmlAnchor;

            //lnkChildSiteLink.Text = currGrandChildSiteLink.PageName;
            lnkChildSiteLink.InnerText = currGrandChildSiteLink.PageName;

            string currGrandChildSiteLinkUrl = "";
            if (currGrandChildSiteLink.PageURL.StartsWith("/"))
                currGrandChildSiteLinkUrl = currGrandChildSiteLink.PageURL.Substring(1, currGrandChildSiteLink.PageURL.Length - 1);

            //lnkChildSiteLink.PostBackUrl = host + currGrandChildSiteLinkUrl;
            lnkChildSiteLink.HRef = host + currGrandChildSiteLinkUrl;

        }

        protected string BindNotification()
        {
            int intUserId = 0;
            int intCurrencyId = GlobalFunctions.GetCurrencyId();
            string UserSessionId = HttpContext.Current.Session.SessionID;
            if (Session["User"] != null)
            {
                UserBE objUserBE = (UserBE)Session["User"];
                intUserId = objUserBE.UserId;
            }
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("UserId", Convert.ToString(intUserId));
            dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
            dictionary.Add("UserSessionId", UserSessionId);
            int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_GetShoppingCartProducts, dictionary, true);
            return Convert.ToString(shopId);
        }

        protected void rptViewBasket_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                ShoppingCartBE CurrProduct = e.Item.DataItem as ShoppingCartBE;
                Label lblTotalProductCost = e.Item.FindControl("lblTotalProductCost") as Label;
                Label lblProductQuantity = e.Item.FindControl("lblProductQuantity") as Label;
                Label lblProductPrice = e.Item.FindControl("lblProductPrice") as Label;
                HtmlImage img1 = e.Item.FindControl("img1") as HtmlImage;
                //lblTotalProductCost.Text = " =  " + GlobalFunctions.GetCurrencySymbol() + "" + Convert.ToString(CurrProduct.Quantity * CurrProduct.Price);

                double dPrice = Convert.ToDouble(CurrProduct.Quantity * CurrProduct.Price);//.ToString("##,###,##0.#0");
                //string strPrice = dPrice.ToString("##,###,##0.#0");
                //GlobalFunctions.ConvertToDecimalPrecision Removed due to multi Language currency (example: 0,6300.00)
                string strPrice = dPrice.ToString("#######0.#0");

                double dUnitPrice = Convert.ToDouble(CurrProduct.Price);
                lblTotalProductCost.Text = " =  " + GlobalFunctions.DisplayPriceOrPoints(strPrice, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                totalCheckoutAmount = totalCheckoutAmount + Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(strPrice, "", GlobalFunctions.GetLanguageId()));

                //lblTotalProductCost.Text = " =  " + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(CurrProduct.Quantity * CurrProduct.Price), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                //totalCheckoutAmount = totalCheckoutAmount + Convert.ToDouble(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(CurrProduct.Quantity * CurrProduct.Price), "", GlobalFunctions.GetLanguageId()));

                //lblProductPrice.Text = "@ " + GlobalFunctions.DisplayPriceOrPoints(dUnitPrice.ToString("##,###,##0.#0"), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                //GlobalFunctions.ConvertToDecimalPrecision Removed due to multi Language currency (example: 0,6300.00)
                lblProductPrice.Text = "@ " + GlobalFunctions.DisplayPriceOrPoints(dUnitPrice.ToString("#######0.#0"), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

                //lblProductPrice.Text = "@ " + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(CurrProduct.Price), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());


                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                    img1.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                else
                    img1.Src = host + "Images/Products/default.jpg";
            }
        }

        protected void rptViewBasket_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "DeleteItem")
            {
                try
                {
                    string strUserSessionId = string.Empty;
                    Int32 intUserId = 0;
                    if (HttpContext.Current.Session["User"] != null)
                    {
                        UserBE lst = new UserBE();
                        lst = HttpContext.Current.Session["User"] as UserBE;
                        intUserId = lst.UserId;
                    }
                    else
                    { strUserSessionId = HttpContext.Current.Session.SessionID; }
                    List<object> lstShoppingBE = new List<object>();
                    lstShoppingBE = ShoppingCartBL.RemoveProductsFromBasket(e.CommandArgument.To_Int32(), intUserId, strUserSessionId);
                    if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                    {
                        Response.Redirect(Request.RawUrl);
                    }
                    else { }
                }
                catch (Exception ex)
                { Exceptions.WriteExceptionLog(ex); }
            }
        }

        protected void BindResourceData()
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {

                        /*Sachin Chauhan Start : 28 03 2016 : Variables to save resource languages titles*/
                        Basket_MiniBasketHead1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketHead1" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Basket_MiniBasketHead2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketHead2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        Basket_MiniBasketView = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketView" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Basket_MiniBasketCheckout = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketCheckout" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Basket_MiniBasketTotalBeforeDelivery = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketTotalBeforeDelivery" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Basket_MiniBasketFillMeUp = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_MiniBasketFillMeUp" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        /*Sachin Chauhan End */

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lnkLLLogout_Click(object sender, EventArgs e)
        {
            Session["AccessToken"] = null;
            Response.Redirect("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:7482/index");
        }
    }
}
