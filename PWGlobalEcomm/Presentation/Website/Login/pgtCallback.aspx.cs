﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Login_pgtCallback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string MichelinPLESSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MichelinPLE_SSOLogs";
                string strpgtIou;
                string strpgtId;
                Exceptions.WriteSSOLog("INSIDE PGTCALLBACK PAGE", MichelinPLESSOLogFolderPath);
                strpgtIou = Convert.ToString(Request.QueryString["pgtIou"]).Trim();
                strpgtId = Convert.ToString(Request.QueryString["pgtId"]).Trim();
                Exceptions.WriteSSOLog("strpgtIou =" + strpgtIou, MichelinPLESSOLogFolderPath);
                Exceptions.WriteSSOLog("strpgtId =" + strpgtId, MichelinPLESSOLogFolderPath);
                if (!string.IsNullOrEmpty(strpgtIou.Trim()) && !string.IsNullOrEmpty(strpgtId.Trim()))
                {
                    Session["PGTIOU"] = strpgtIou;
                    Session["PGTId"] = strpgtId;
                }
                Exceptions.WriteSSOLog("Pg call back Session Id " + Session.SessionID + "Keys Count= " + Session.Keys.Count, MichelinPLESSOLogFolderPath);
                MichelinSmileSSOBL objMichelinSmileSSOBL = new MichelinSmileSSOBL();
                objMichelinSmileSSOBL.AddPGTValue(Constants.USP_INSERTPGTVALUE, strpgtIou, strpgtId);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
         
        }
    }
}