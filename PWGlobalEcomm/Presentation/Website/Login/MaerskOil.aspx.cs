﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using OneLogin.Saml;
using System.IO;
using PWGlobalEcomm.BusinessLogic;
using Microsoft.Security.Application;
using PWGlobalEcomm.DataAccess;
using System.Xml;
using PWGlobalEcomm.GlobalUtilities;

namespace Presentation
{
    public partial class Login_MaerskOil : BasePage
    {
        AccountSettings accountSettings = new AccountSettings();
        string MaerskSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_OIL_SSOLogs";
        Int16 iUserTypeID = 1;
        string  displayname, identityprovider, givenname, surname, emailaddress, strInvalidUserErrorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Exceptions.WriteSSOLog("---INSIDE PAGE LOAD OF MAERSK OIL SSO LOGIN--- ", MaerskSSOLogFolderPath);
            SetupRequest();
        }

        public void SetupRequest()
        {
            try
            {
                Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST  MAERSK OIL SSO LOGIN PAGE--- ", MaerskSSOLogFolderPath);
                string Domain = Request.QueryString["Domain"];
                string cXMLSetupRequest = "";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;

                if (!String.IsNullOrEmpty(Domain))
                {
                    Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST  POWERWEAVE USER--- ", MaerskSSOLogFolderPath);
                    StreamReader fper = default(StreamReader);
                    fper = File.OpenText(Server.MapPath("~/SSO/SAML Maersk OIL.xml"));
                    cXMLSetupRequest = fper.ReadToEnd();

                    xmlDoc.LoadXml(cXMLSetupRequest);
                }
                else
                {
                    Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST LIVE USER--- ", MaerskSSOLogFolderPath);
                    cXMLSetupRequest = ReturnStream();
                    Exceptions.WriteSSOLog("---OUTPUT OF RETURN STREAM LIVE USER--- " + cXMLSetupRequest, MaerskSSOLogFolderPath);
                    //cXMLSetupRequest = "SAMLResponse=PHNhbWxwOlJlc3BvbnNlIHhtbG5zOnNhbWxwPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIERlc3RpbmF0aW9uPSJodHRwczovL21hZXJza2JyYW5kc3RvcmUuY29tL21hZXJza1NTTyIgSUQ9Il84YWVlZWRmZC1lZTYzLTQ2YTgtYTYzYy0wN2FiMzQxM2QxYzgiIElzc3VlSW5zdGFudD0iMjAxNy0wMS0xN1QxMTowMzo1OC43NjI0OTU0WiIgVmVyc2lvbj0iMi4wIj48SXNzdWVyIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIj51cm46Y2FtLm1hZXJza29pbC5jb20vQ2xvdWRBY2Nlc3NNYW5hZ2VyL1JQU1RTPC9Jc3N1ZXI%2BPFNpZ25hdHVyZSB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI%2BPFNpZ25lZEluZm8%2BPENhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiIC8%2BPFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNyc2Etc2hhMSIgLz48UmVmZXJlbmNlIFVSST0iI184YWVlZWRmZC1lZTYzLTQ2YTgtYTYzYy0wN2FiMzQxM2QxYzgiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIgLz48VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIj48SW5jbHVzaXZlTmFtZXNwYWNlcyBQcmVmaXhMaXN0PSIjZGVmYXVsdCBzYW1scCBzYW1sIGRzIHhzIHhzaSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIgLz48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM%2BPERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIiAvPjxEaWdlc3RWYWx1ZT4rOWhiYnpEMHBaUUYxMjBHeElKZ2svak5nM1U9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8%2BPFNpZ25hdHVyZVZhbHVlPkVzeVNzS1U5bnlacWRDd1RrY1Y0TmsrWTRNZXhRLzl3cVZCSm9lbFMrdEE5QkNXc2xGSk92RzhNZkx5Z1p3T3lPd2t4ZmJKY0VCYS9yRENoNXVORlhsNW5aQzZERExhQnVaM3BpbFdxdVRsYjBQVDZETzkxSDVHaEtFWkJEY0h6Ym1hdHlWVVNFMFZ5S3EwWENYSjZkYWZYanVRZTZDM1ViZkRiUW1jWVRiRnFtc2crbjFyTDJzTHg0VWdwSHNtZlhsbXdXeWJMYk40SDZYRGVDYVUyWTIwWEZRZC9LRnRpbmN0ekI1dFN4cTRpYW5iU1ZRV2JCMTF6UmdsMXFuZGZ1bG5tNzl0WTI4Ukt4THpxYTljVUF3R2c0S0pnZi85bGQ0UWdkTm5lRFMxaEhLWE5vVEpsK0xiVzhnUjhBWHBIWTFHcmhnNlJScEJ2VEQxL0lwUnp6UT09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE%2BPFg1MDlDZXJ0aWZpY2F0ZT5NSUlEQURDQ0FlaWdBd0lCQWdJUUFLZ05TQ0FWMktFbExZSXd0dmhFR3pBTkJna3Foa2lHOXcwQkFRc0ZBREE3TVRrd053WURWUVFERERCRVpXeHNJRU5zYjNWa0lFRmpZMlZ6Y3lCTllXNWhaMlZ5SUVGd2NDQk5ZV1Z5YzJzZ1luSmhibVFnYzNSdmNtVXdJQmNOTVRjd01URXhNREF3TURBd1doZ1BNakExTnpBeE1USXdNREF3TURCYU1Ec3hPVEEzQmdOVkJBTU1NRVJsYkd3Z1EyeHZkV1FnUVdOalpYTnpJRTFoYm1GblpYSWdRWEJ3SUUxaFpYSnpheUJpY21GdVpDQnpkRzl5WlRDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBSW4xdEM2bzB1L1NjdkN6NlQ0Ry9vbTI2QUtPNVpEVEpxSW54ODFkTDdadTVVYW5nMnp3bkJIcWQvV0hIK1JiK3hXWmVGMmJ2eTBaVDVmT0s5c0VqTlYwK2E5eVlWUFh2YnRXeXMyTzhSaGZKRHk4M2l2VlBnT013d1ozYXNST3RtN2FyU1Z3Ylp4TXljbU1RUVpYMkdWdzlkalA5TVpicnkvSjhsOXlEd0llbFBOZDZ6aE9abGdRNkNObXd2dEpqNFhqQ1FmNHl5VDYxc2pybCs0dHdmakkwbFh1TzRWYjE1V1Ivbnd0TUtaelo4THVvZmlSUzB2RjV6ZDhOZWlHdHhNNGwveDZBNjBtbUV0MzZvOFZub2V6NFhYa2VGRG83UDdpTnZpRUt5WG03N0hhM2Q0ODBBU1FGNVZ1ZE9zeENXc1hCbUhrSDhsNDNoU2tKREFpUHlrQ0F3RUFBVEFOQmdrcWhraUc5dzBCQVFzRkFBT0NBUUVBRFZTT1FDOW1SUk5OUTV0ekRGZGVUbjF2U3p4d0QzbDNhUlAwSXBOY1JVT0ptUURkMldEdHlvVTVSUFhabnZRekI1UEx3MDdXVWEvaXdEYWtrMUNCdzBra2xQRU9wSEdNaC9QM0hpSEVvOXNmMS8xNkh6dmtjendrRnlIVVdubjZmRlUzR2JSZEwwTVBobkE3d0Nxakc4cHYzbzN1S0lSU0M4a2p2Rmg5VHpEOHQvQTFoejNNN1V6Qk9ockFXeU1LL0NFWURCb29FNk1SMFR3alZrUlJsaGlvSVhNdGVua3NwNEZZQjkxRmZoNHRTNmphbnArdEIyak9VUlBmZmRtU2psRnNNbW0vTFFrWGlESk90a2hTbEJQM2w4dStsQzV6NWhLVGZLdHNmdnIzalAwVTRtVjIzS2gwbFFDbHZKZEthbFZLOVFia09LbHdJdzNuMVltZnFnPT08L1g1MDlDZXJ0aWZpY2F0ZT48L1g1MDlEYXRhPjwvS2V5SW5mbz48L1NpZ25hdHVyZT48c2FtbHA6U3RhdHVzPjxzYW1scDpTdGF0dXNDb2RlIFZhbHVlPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6c3RhdHVzOlN1Y2Nlc3MiPjwvc2FtbHA6U3RhdHVzQ29kZT48L3NhbWxwOlN0YXR1cz48QXNzZXJ0aW9uIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIiBJRD0iX2I1NzgwMzkyLTZiYWUtNDM2Ny04MmRhLThlNWZiMzk2N2ZmOSIgSXNzdWVJbnN0YW50PSIyMDE3LTAxLTE3VDExOjAzOjU4Ljc2MloiIFZlcnNpb249IjIuMCI%2BPElzc3Vlcj51cm46Y2FtLm1hZXJza29pbC5jb20vQ2xvdWRBY2Nlc3NNYW5hZ2VyL1JQU1RTPC9Jc3N1ZXI%2BPFN1YmplY3Q%2BPE5hbWVJRCBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OmVtYWlsQWRkcmVzcyI%2BSGVucmlrLlNvbGJlcmdAbWFlcnNrb2lsLmNvbTwvTmFtZUlEPjxTdWJqZWN0Q29uZmlybWF0aW9uIE1ldGhvZD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmNtOmJlYXJlciI%2BPFN1YmplY3RDb25maXJtYXRpb25EYXRhIE5vdE9uT3JBZnRlcj0iMjAxNy0wMS0xN1QxMTozMzo1OC43NjJaIiBSZWNpcGllbnQ9Imh0dHBzOi8vbWFlcnNrYnJhbmRzdG9yZS5jb20vbWFlcnNrU1NPIj48L1N1YmplY3RDb25maXJtYXRpb25EYXRhPjwvU3ViamVjdENvbmZpcm1hdGlvbj48L1N1YmplY3Q%2BPENvbmRpdGlvbnMgTm90QmVmb3JlPSIyMDE3LTAxLTE3VDExOjAzOjU4Ljc2MloiIE5vdE9uT3JBZnRlcj0iMjAxNy0wMS0xN1QxMTozMzo1OC43NjJaIj48QXVkaWVuY2VSZXN0cmljdGlvbj48QXVkaWVuY2U%2BbWFlcnNrYnJhbmRzdG9yZS5jb208L0F1ZGllbmNlPjwvQXVkaWVuY2VSZXN0cmljdGlvbj48L0NvbmRpdGlvbnM%2BPEF1dGhuU3RhdGVtZW50IEF1dGhuSW5zdGFudD0iMjAxNy0wMS0xN1QxMTowMzo1OC43NjJaIiBTZXNzaW9uSW5kZXg9ImIyMWY1NjgzLTZhYTEtNGM3OC04M2Q0LTM2NDRiNWEyODVkYyI%2BPEF1dGhuQ29udGV4dD48QXV0aG5Db250ZXh0Q2xhc3NSZWY%2BdXJuOmZlZGVyYXRpb246YXV0aGVudGljYXRpb246Y2FtOm11bHRpZmFjdG9yOndpbmRvd3M6cmFkaXVzPC9BdXRobkNvbnRleHRDbGFzc1JlZj48L0F1dGhuQ29udGV4dD48L0F1dGhuU3RhdGVtZW50PjxBdHRyaWJ1dGVTdGF0ZW1lbnQ%2BPEF0dHJpYnV0ZSBOYW1lPSJlbWFpbEFkZHJlc3MiIE5hbWVGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6dXJpIj48QXR0cmlidXRlVmFsdWU%2BSGVucmlrLlNvbGJlcmdAbWFlcnNrb2lsLmNvbTwvQXR0cmlidXRlVmFsdWU%2BPC9BdHRyaWJ1dGU%2BPEF0dHJpYnV0ZSBOYW1lPSJnaXZlbm5hbWUiIE5hbWVGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6dXJpIj48QXR0cmlidXRlVmFsdWU%2BSGVucmlrPC9BdHRyaWJ1dGVWYWx1ZT48L0F0dHJpYnV0ZT48QXR0cmlidXRlIE5hbWU9InN1cm5hbWUiIE5hbWVGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6dXJpIj48QXR0cmlidXRlVmFsdWU%2BU29sYmVyZzwvQXR0cmlidXRlVmFsdWU%2BPC9BdHRyaWJ1dGU%2BPEF0dHJpYnV0ZSBOYW1lPSJkaXNwbGF5bmFtZSIgTmFtZUZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmF0dHJuYW1lLWZvcm1hdDp1cmkiPjxBdHRyaWJ1dGVWYWx1ZT5Tb2xiZXJnLCBIZW5yaWs8L0F0dHJpYnV0ZVZhbHVlPjwvQXR0cmlidXRlPjxBdHRyaWJ1dGUgTmFtZT0iaWRlbnRpdHlwcm92aWRlciIgTmFtZUZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmF0dHJuYW1lLWZvcm1hdDp1cmkiPjxBdHRyaWJ1dGVWYWx1ZT5odHRwczovL2NhbS5tYWVyc2tvaWwuY29tL0Nsb3VkQWNjZXNzTWFuYWdlci9SUFNUUy9TYW1sMi9EZWZhdWx0LmFzcHg8L0F0dHJpYnV0ZVZhbHVlPjwvQXR0cmlidXRlPjwvQXR0cmlidXRlU3RhdGVtZW50PjwvQXNzZXJ0aW9uPjwvc2FtbHA6UmVzcG9uc2U%2B&RelayState=undefined";
                    int startIndex = cXMLSetupRequest.IndexOf("SAMLResponse=") + "SAMLResponse".Length;
                    Exceptions.WriteSSOLog("startIndex of SAMLResponse = " + startIndex, MaerskSSOLogFolderPath);

                    int endIndex = cXMLSetupRequest.IndexOf("&RelayState");
                    Exceptions.WriteSSOLog("endIndex of SAMLResponse= " + endIndex, MaerskSSOLogFolderPath);

                    cXMLSetupRequest = cXMLSetupRequest.Substring(startIndex, endIndex - startIndex);
                    cXMLSetupRequest = cXMLSetupRequest.TrimStart('=');
                    Exceptions.WriteSSOLog("Actual SAMLResponse required : " + cXMLSetupRequest, MaerskSSOLogFolderPath);

                    if (cXMLSetupRequest.Contains("%"))
                    {
                        cXMLSetupRequest = HttpUtility.UrlDecode(cXMLSetupRequest);
                        cXMLSetupRequest = cXMLSetupRequest.Replace("%0", "");
                    }

                    OneLogin.Saml.Response samlResponse = new Response(accountSettings);
                    samlResponse.LoadXmlFromBase64(cXMLSetupRequest);

                    System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();

                    xmlDoc.LoadXml(enc.GetString(Convert.FromBase64String(cXMLSetupRequest)));

                    Exceptions.WriteSSOLog("SAML XML : " + Convert.ToString(xmlDoc.InnerXml), MaerskSSOLogFolderPath);
                    xmlDoc.LoadXml(enc.GetString(Convert.FromBase64String(cXMLSetupRequest)));
                }

                XmlElement Nodes = xmlDoc["samlp:Response"]["Assertion"]["AttributeStatement"]["Attribute"];

                try
                {
                    if (Nodes.HasAttributes)
                    {
                        emailaddress = Nodes.InnerText;
                        Exceptions.WriteSSOLog("emailaddress : " + emailaddress, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.HasChildNodes)
                    {
                        givenname = Nodes.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("givenname : " + givenname, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.HasChildNodes)
                    {
                        surname = Nodes.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("surname : " + surname, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        displayname = Nodes.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("displayname : " + displayname, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        identityprovider = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("identityprovider : " + identityprovider, MaerskSSOLogFolderPath);
                    }                   
                    
                }
                catch (Exception ex)
                {
                    Exceptions.WriteSSOLog("Error while fetching SSO user details", MaerskSSOLogFolderPath);
                    Exceptions.WriteExceptionLog(ex);
                }

                try
                {
                    #region Code Added by SHRIGANESH SINGH for Filetring SSO User Emailid as per Store setting 06 Jan 2017

                    bool bDomainWhitelist = false, bDomainBlackList = false, bEmailWhiteList = false, bEmailBlackList = false;
                    UserRegistrationBE objUserRegistrationBE = new UserRegistrationBE();
                    List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();

                    StoreBE getStores;
                    if (HttpRuntime.Cache["StoreDetails"] == null)
                    {
                        getStores = StoreDA.getItem(Constants.USP_GetAllStoreDetails, null, true);
                        HttpRuntime.Cache.Insert("StoreDetails", getStores);
                    }
                    else
                    {
                        getStores = (StoreBE)HttpRuntime.Cache["StoreDetails"];
                    }

                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();

                    string Register_GuestCheckout_Error1_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_GuestCheckout_Error1_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    string Register_GuestCheckout_Error2_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_GuestCheckout_Error2_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    strInvalidUserErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SSO_InvalidUser_Error_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                    Exceptions.WriteInfoLog("Registration:Filtering starts");
                    if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "whitelist").IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Registration:Filtering bDomainWhitelist set true");
                        bDomainWhitelist = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "blacklist").IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Registration:Filtering bDomainBlackList set true");
                        bDomainBlackList = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "whitelist").IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Registration:Filtering bEmailWhiteList set true");
                        bEmailWhiteList = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "blacklist").IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Registration:Filtering bEmailBlackList set true");
                        bEmailBlackList = true;
                    }
                    Exceptions.WriteInfoLog("Registration:before Registeration details");
                    objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                    Exceptions.WriteInfoLog("Registration:after Registeration details");
                    string strUEmail = Sanitizer.GetSafeHtmlFragment(emailaddress);
                    string strDomain = strUEmail.Substring(strUEmail.LastIndexOf("@") + 1);

                    List<UserRegistrationBE.RegistrationFilterBE> objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                    List<UserRegistrationBE.RegistrationFilterBE> objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                    UserRegistrationBE.RegistrationFilterBE objRegistrationWhiteListDomainFilterBE = null;
                    UserRegistrationBE.RegistrationFilterBE objRegistrationBlackListDomainFilterBE = null;
                    UserRegistrationBE.RegistrationFilterBE objRegistrationWhiteListEmailFilterBE = null;
                    UserRegistrationBE.RegistrationFilterBE objRegistrationBlackListEmailFilterBE = null;

                    if (bDomainWhitelist)
                    {
                        objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                        objRegistrationWhiteListDomainFilterBE = objRegistrationWhiteListFilterBELst.FirstOrDefault(x => x.Domain.ToLower() == strDomain.ToLower());
                    }
                    if (bDomainBlackList)
                    {
                        objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                        objRegistrationBlackListDomainFilterBE = objRegistrationBlackListFilterBELst.FirstOrDefault(x => x.Domain.ToLower() == strDomain.ToLower());
                    }

                    if (bEmailWhiteList)
                    {
                        objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                        objRegistrationWhiteListEmailFilterBE = objRegistrationWhiteListFilterBELst.FirstOrDefault(x => x.EmailId.ToLower() == strUEmail.ToLower());
                    }
                    if (bEmailBlackList)
                    {
                        objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                        objRegistrationBlackListEmailFilterBE = objRegistrationBlackListFilterBELst.FirstOrDefault(x => x.EmailId.ToLower() == strUEmail.ToLower());
                    }

                    if (bDomainWhitelist && bEmailWhiteList)
                    {
                        if (objRegistrationWhiteListDomainFilterBE == null && objRegistrationWhiteListEmailFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainWhitelist && !bEmailWhiteList && !bEmailBlackList)
                    {
                        if (objRegistrationWhiteListDomainFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainBlackList && !bEmailWhiteList && !bEmailBlackList)
                    {
                        if (objRegistrationBlackListDomainFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bEmailWhiteList && !bDomainBlackList && !bDomainWhitelist)
                    {
                        if (objRegistrationWhiteListEmailFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bEmailBlackList && !bDomainBlackList && !bDomainWhitelist)
                    {
                        if (objRegistrationBlackListEmailFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainWhitelist && bEmailBlackList)
                    {
                        if (objRegistrationBlackListEmailFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                        else if (objRegistrationWhiteListDomainFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainBlackList && bEmailBlackList)
                    {
                        if (objRegistrationBlackListDomainFilterBE != null && objRegistrationBlackListEmailFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                        else if (objRegistrationBlackListDomainFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }

                        else if (objRegistrationBlackListEmailFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainBlackList && bEmailWhiteList)
                    {
                        if (objRegistrationBlackListDomainFilterBE != null && objRegistrationWhiteListEmailFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    Exceptions.WriteInfoLog("Registration:Filtering end");
                    #endregion

                    Session["Maersk_SSO_OIL_User"] = true;

                    string strMessage = string.Empty;
                    int i = 0;
                    strMessage = Register_GuestCheckout_Error1_Message; //"There is some problem in guest user checkout. Please try again.";

                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();

                    i = UserBL.ChkUserExists(Constants.USP_CheckUserExists, true, Sanitizer.GetSafeHtmlFragment(emailaddress.Trim()));
                    if (i == 0)
                    {
                        UserBE objBE = new UserBE();
                        UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                        objBE.EmailId = emailaddress;
                        objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);
                        objBE.FirstName = givenname;
                        objBE.LastName = surname;
                        objBE.PredefinedColumn1 = displayname;
                        objBE.PredefinedColumn2 = ".";
                        objBE.PredefinedColumn3 = ".";
                        objBE.PredefinedColumn4 = ".";
                        objBE.PredefinedColumn5 = ".";
                        objBE.PredefinedColumn6 = ".";
                        objBE.PredefinedColumn7 = ".";
                        objBE.PredefinedColumn8 = "12"; // As country is not passed in the SAML, we are assingning UK as default

                        objBE.PredefinedColumn9 = "";
                        objBE.UserDeliveryAddress.Add(obj);
                        objBE.IPAddress = GlobalFunctions.GetIpAddress();
                        objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objBE.IsGuestUser = true;
                        objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBE.IsMarketing = false;
                        objBE.UserTypeID = iUserTypeID;// Added by SHRIGANESH 22 Sept 2016         
                        objBE.Points = -1; // Added by ShriGanesh 26 April 2017
                        Exceptions.WriteSSOLog("Before calling UserBL.ExecuteRegisterDetails in SetUpRequest", MaerskSSOLogFolderPath);
                        i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                        Exceptions.WriteSSOLog("After calling UserBL.ExecuteRegisterDetails in SetUpRequest", MaerskSSOLogFolderPath);
                        objBE.UserId = Convert.ToInt16(i);

                        UserBE objBEs = new UserBE();
                        UserBE objUser = new UserBE();
                        objBEs.EmailId = emailaddress.Trim();
                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();

                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                            GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                            GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                            //Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", MaerskSSOLogFolderPath);
                            //Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                            //Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", MaerskSSOLogFolderPath);
                            Session["GuestUser"] = null;
                            Exceptions.WriteSSOLog("Before redirecting to INDEX for IF from SetUpRequest", MaerskSSOLogFolderPath);
                            Response.RedirectToRoute("index");
                        }
                    }
                    else if (i == -2 || i == -1)
                    {
                        UserBE objBEs = new UserBE();
                        UserBE objUser = new UserBE();
                        objBEs.EmailId = emailaddress.Trim();
                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objBEs.FirstName = givenname;
                        objBEs.LastName = surname;
                        objBEs.PredefinedColumn1 = displayname;
                        objBEs.PredefinedColumn2 = ".";
                        objBEs.PredefinedColumn3 = ".";
                        objBEs.PredefinedColumn4 = ".";
                        objBEs.PredefinedColumn5 = ".";
                        objBEs.PredefinedColumn6 = ".";
                        objBEs.PredefinedColumn7 = ".";
                        objBEs.PredefinedColumn8 = "12";

                        objBEs.PredefinedColumn9 = "";

                        int j = UserBL.ExecuteSSOProfileDetails(Constants.USP_UpdateSSOUserProfileDetails, true, objBEs);
                        if (j == 1)
                        {
                            objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                            if (objUser != null || objUser.EmailId != null)
                            {
                                Exceptions.WriteSSOLog("User Email ID : " + objUser.EmailId, MaerskSSOLogFolderPath);
                                Exceptions.WriteSSOLog("IsGuestUser : " + Convert.ToString(objUser.IsGuestUser), MaerskSSOLogFolderPath);
                                Exceptions.WriteSSOLog("First Name : " + objUser.FirstName, MaerskSSOLogFolderPath);
                                //Exceptions.WriteSSOLog("" + objUser., MaerskSSOLogFolderPath);
                                Session["User"] = objUser;
                                GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                                GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                                //Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", MaerskSSOLogFolderPath);
                                //Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                //Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", MaerskSSOLogFolderPath);
                                Session["GuestUser"] = null;
                                Exceptions.WriteSSOLog("Before redirecting to INDEX for ELSE from SetUpRequest", MaerskSSOLogFolderPath);
                                Response.RedirectToRoute("index");
                            }
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
                        }
                    }
                    else
                    {
                        //General error
                        strMessage = Register_GuestCheckout_Error1_Message; //"There is some problem in guest user checkout. Please try again.";
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
                    }
                }
                catch (Exception Ex)
                {
                    Exceptions.WriteExceptionLog(Ex);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
            }
        }

        public string ReturnStream()
        {
            try
            {
                Exceptions.WriteSSOLog("ReturnStream Method call : ", MaerskSSOLogFolderPath);
                Stream str = default(Stream);
                string strmContents = null;
                //int counter = 0;
                int strLen = 0;
                int strRead = 0;

                // Create a Stream object. 
                str = Request.InputStream;
                //Exceptions.WriteSSOLog("Str : " + Convert.ToString(str), MaerskSSOLogFolderPath);
                // Find number of bytes in stream. 
                strLen = (int)str.Length;
                // Create a byte array. 
                byte[] strArr = new byte[strLen + 1];
                // Read stream into byte array. 
                strRead = str.Read(strArr, 0, strLen);

                strmContents = System.Text.Encoding.UTF8.GetString(strArr);
                //Exceptions.WriteSSOLog("strmContents " + strmContents, MaerskSSOLogFolderPath);
                return strmContents;
                //return Server.UrlDecode(strmContents.Replace("\0", ""));

                /*
                Stream str = default(Stream);
                //StreamReader strrdr = new StreamReader();
                string strmContents = null;

                //int counter = 0;
                int strLen = 0;
                int strRead = 0;

                // Create a Stream object. 
                str = Request.InputStream;

                using (StreamReader responseReader = new StreamReader(str))
                {
                    strmContents = responseReader.ReadToEnd();
                    Exceptions.WriteSSOLog("Output of Input Stream = " + strmContents, MaerskSSOLogFolderPath);
                }
                 */

                #region COMMENTED CODE BY SHRIGANESH 12 JAN 2017
                //// Find number of bytes in stream. 
                //strLen = (int)str.Length;
                //Exceptions.WriteSSOLog("length of Input Stream = " + Convert.ToString(strLen), MaerskSSOLogFolderPath);

                //// Create a byte array. 
                //byte[] strArr = new byte[strLen + 1];
                //// Read stream into byte array. 
                //strRead = str.Read(strArr, 0, strLen);

                //strmContents = System.Text.Encoding.UTF8.GetString(strArr);
                //Exceptions.WriteInfoLog((strmContents)); 
                #endregion

                //return strmContents;
            }
            catch (Exception ex)
            {
                Exceptions.WriteSSOLog("Error in ReturnStream : ", MaerskSSOLogFolderPath);
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
            }
            return "blank";
        }
    }
}