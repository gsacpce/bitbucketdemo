﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Presentation
{
    public class Login_UserLogout : System.Web.UI.Page
    {
        public string host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.HtmlControls.HtmlInputText googletoken;
        protected void Page_Load(object sender, EventArgs e)
        {

            //Session.Abandon();
            //Response.Redirect( GlobalFunctions.GetVirtualPath() + "Login/Login.aspx");
            Session["User"] = null;
            Session["GiftOrderDetailslst"] = null;
            Session["dictGiftOrder"] = null;
            Session["dictGiftOrderDetails"] = null;
            Session["dTotlaGoods"] = null;
            Session["UserName"] = null;
            Session["FirstName"] = null;
            Session["LastName"] = null;
            Session["IsSSO"] = null;
            Session["CaptchaText"] = null;
            Session["activeCurrencyId"] = null;
            Session["PrevLanguageId"] = null;
            Session["IsUserHierarchyExist"] = null;
            Session["HeirarchyUsers"] = null;
            Session["RegisterPg"] = null;
            Session["userReg"] = null;
            Session["UserLst"] = null;
            Session["CurrencySymbol"] = null;
            Session["lstItemz"] = null;
            Session["S_ISPUNCHOUT"] = null;
            Session["ObjOrderItem"] = null;
            Session["ObjOrderHistory"] = null;
            Session["NextOrderItemId"] = null;
            Session["PrevOrderItemId"] = null;
            Session["CurrOrderItemId"] = null;
            Session["CurrProductId"] = null;
            Session["objCurrentProductBE"] = null;
            Session["Add"] = null;
            Session["objVariantProductBE"] = null;
            Session["IsCallForOrder"] = null;
            Session["cXML_ALLOWPUNCHOUT"] = null;
            Session["PunchoutUserEmail"] = null;
            Session["CustomerOrderId"] = null;
            Session["S_PUNCHOUTREQUEST"] = null;
            Session["PunchoutSubmitUrl"] = null;
            Session["cXML_Form"] = null;
            Session["cXML_PostURL"] = null;
            Session["transaction"] = null;
            Session["Amount"] = null;
            Session["sbGift"] = null;
            Session["TaxDetails"] = null;
            Session["ProductWeight"] = null;
            Session["IsSSO"] = null;
            Session["ProductWeight"] = null;
            Session["CurrStaticPageId"] = null;
            Session["ReturnUrl"] = null;
            Session["IsGoogleAPILogout"] = "true";// vikram
            Session["ReferrelURL"] = null;
            Session["User"] = null;
            Session["GuestUser"] = null;

            /*Sachin Chauhan Start : 25 02 2016 : Reseting logged out user as anonymous user & setting default currency id & symbol */
            Session["IsAnonymous"] = "0";
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            GlobalFunctions.SetCurrencyId(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencyId);
            GlobalFunctions.SetCurrencySymbol(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.IsDefault == true).CurrencySymbol);
            string MaerskMGISSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_SSOLogs";
            string MaerskOILSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_OIL_SSOLogs";
            string MaerskDRILLSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_DRILL_SSOLogs";
            string MichelinPLESSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MichelinPLE_SSOLogs"; 
            /**/
            if (Session["AccessToken"] != null)
            {
                // var request = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/revoke?token=" + Convert.ToString(googletoken.Value));
                //var response = (HttpWebResponse)request.GetResponse();
                //var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                Session["AccessToken"] = null;
                Session["CheckExist"] = null;
                Session["User"] = null;
                Session["GuestUser"] = null;
                //ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:signOut();</script>");
                // Response.RedirectToRoute("login-page");                
                // Response.Redirect("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=" + host + "Signin", true); 
            }
            else if (Session["Maersk_SSO_MGIS_User"] != null)
            {
                Exceptions.WriteSSOLog("inside LOGOUT PAGE", MaerskMGISSSOLogFolderPath);
                Exceptions.WriteSSOLog("inside Maersk_SSO_MGIS_User != null LOGOUT PAGE", MaerskMGISSSOLogFolderPath);
                Session["Maersk_SSO_MGIS_User"] = null;

                #region Commented
                //WebRequest req = null;
                //WebResponse rsp = null;
                //string cXMLSetupRequest = "";
                //StreamReader fper = default(StreamReader);
                //fper = File.OpenText(Server.MapPath("~/SSO/SAMLLOGOUT.xml"));
                //cXMLSetupRequest = fper.ReadToEnd();
                ////string fileName = "D:\\SAMLLOGOUT.xml";
                //string uri = "https://login.windows.net/05d75c05-fa1a-42e7-9cf1-eb416c396f2d/saml2";
                //req = WebRequest.Create(uri);
                //req.Method = "POST";        // Post method
                //req.ContentType = "text/xml";     // content type
                //StreamWriter writer = new StreamWriter(req.GetRequestStream());
                //writer.WriteLine(cXMLSetupRequest);
                //writer.Close();
                //rsp = req.GetResponse();

                //Response.Redirect(GlobalFunctions.GetSetting("MSILSSSOLogoutLink")); 
                #endregion
            }
            else if (Session["Maersk_SSO_OIL_User"] != null)
            {
                Exceptions.WriteSSOLog("inside LOGOUT PAGE", MaerskOILSSOLogFolderPath);
                Exceptions.WriteSSOLog("inside Maersk_SSO_OIL_User != null LOGOUT PAGE", MaerskOILSSOLogFolderPath);
                Session["Maersk_SSO_OIL_User"] = null;
                Response.Redirect(GlobalFunctions.GetSetting("MOLSSOLogoutLink"));
            }
            else if (Session["Maersk_SSO_DRILL_User"] != null)
            {
                Exceptions.WriteSSOLog("inside LOGOUT PAGE", MaerskDRILLSSOLogFolderPath);
                Exceptions.WriteSSOLog("inside Maersk_SSO_DRILL_User != null LOGOUT PAGE", MaerskDRILLSSOLogFolderPath);
                Session["Maersk_SSO_OIL_User"] = null;
                Response.Redirect(GlobalFunctions.GetSetting("DRILLSSOLogoutLink"));
            }
            else if (Session["IndeedSSO_User"] != null || Session["IndeedEmployeeSSO_User"] != null)
            {
                Session["IndeedEmployeeSSO_User"] = null;
                Session["IndeedSSO_User"] = null;
                Session["Ship_To_Location"] = null;
                Session["IndeedEmployeeCountry"] = null;
                Session["UserCatalogueID"] = null;
                Session["EmployeeUserCatalogueID"] = null;
                Response.Redirect(GlobalFunctions.GetSetting("IndeedLogoutURL"));
            }
            else if (Session["MichelinSSO"] != null)
            {
                Exceptions.WriteSSOLog("inside LOGOUT PAGE", MichelinPLESSOLogFolderPath);
                Exceptions.WriteSSOLog("inside MichelinSSO != null LOGOUT PAGE", MichelinPLESSOLogFolderPath);
                Session["MichelinSSO"] = null;
                Response.Redirect(GlobalFunctions.GetSetting("MichelinSSOLogoutURL"));
            }
            else
            {
                if (Session["Maersk_SSO_MGIS_User"] != null)
                {
                    Exceptions.WriteSSOLog("Before redirecting to Login from LOGOUT PAGE", MaerskMGISSSOLogFolderPath);
                }
                if (Session["Maersk_SSO_OIL_User"] != null)
                {
                    Exceptions.WriteSSOLog("Before redirecting to Login from LOGOUT PAGE", MaerskOILSSOLogFolderPath);
                }
                if (Session["Maersk_SSO_DRILL_User"] != null)
                {
                    Exceptions.WriteSSOLog("Before redirecting to Login from LOGOUT PAGE", MaerskDRILLSSOLogFolderPath);
                }
                Response.RedirectToRoute("login-page");
            }
        }

        private string GetTextFromXMLFile(string file)
        {
            StreamReader reader = new StreamReader(file);
            string ret = reader.ReadToEnd();
            reader.Close();
            return ret;
        }
    }
}