﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Login_SelectCurrency : System.Web.UI.Page
    {
        #region Controls
        //protected global::System.Web.UI.WebControls.Label  lblCurrency;
        protected global::System.Web.UI.WebControls.DropDownList ddlCurrency;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDropDownCurrency, divCurrencyArea, ucHeader1_gmcontent2;
        protected global::System.Web.UI.WebControls.Literal litCSSnJS;
        protected global::System.Web.UI.WebControls.Image imgLanding;

        protected global::System.Web.UI.WebControls.Repeater repaterCurrency;
        //protected global ::System.Web.UI.WebControls.use
        #endregion
        #region Variables
        string strCurrencySymbol = "";
        int intLanguageId;
        public string host = GlobalFunctions.GetVirtualPath();
        string currencyid = "";
        int i = 0;
        int LoginWaitTime = Convert.ToInt16(ConfigurationManager.AppSettings["LoginWaitTime"]);
        int LoginWrongAttempts = Convert.ToInt16(ConfigurationManager.AppSettings["LoginWrongAttempts"]);
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Creating new brannch 10 July 2017
                // TEST CHANGES
                //Ravi Changes
                //Ravi Changes
                if (!IsPostBack)
                {
                    if ((Session["UserName"] != null && Session["FirstName"] != null && Session["LastName"] != null) || (Session["RegisterPg"] != null && Session["userReg"] != null))
                    {
                        //ucHeader1_gmcontent2 = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("ucHeader1_gmcontent2");
                        //ucHeader1_gmcontent2.Visible = false;
                        //Literal litCSSnJS = (Literal)Page.FindControl("litCSSnJS");
                        litCSSnJS.Text = "<!-- core CSS --> " +
                                   "<link href='" + host + "css/bootstrap.min.css' rel='stylesheet'>" +
                                   "<link href='" + host + "css/font-awesome.min.css' rel='stylesheet'>" +
                                   "<link href='" + host + "css/responsive.css' rel='stylesheet'>" +
                                   "<!-- SmartMenus jQuery Bootstrap Addon CSS -->" +
                                   "<link href='" + host + "css/jquery.smartmenus.bootstrap.css' rel='stylesheet'>" +
                                   "<link href='" + host + "css/bootstrap-slider.css' rel='stylesheet'>" +
                                   "<!--[if lt IE 9]>" +
                                   "<script src='" + host + "js/html5shiv.js'></script>" +
                                   "<script src='" + host + "js/respond.min.js'></script>" +
                                   "<![endif]-->" +
                                   "<link href='" + host + "css/bootstrap-theme.css' rel='stylesheet'>" +
                                   "<link href='" + host + "css/style.css' rel='stylesheet'>" +
                                   "<link href='" + host + "css/modified.css' rel='stylesheet'>" +
                                   "<link href='" + host + "css/media-query.css' rel='stylesheet'>" +
                                   "<link href='" + host + "css/animate.css' rel='stylesheet'> " +
                                   "<link href='" + host + "css/font-awesome.min.css' rel='stylesheet'> "
                                   ;
                        BindResourceData();
                        #region "LandingImage"
                        string[] filePaths = Directory.GetFiles(Server.MapPath("~/Images/LandingPage"));
                        foreach (string strfile in filePaths)
                        {
                            if (Path.GetFileName(strfile) == ("LandingPage" + Convert.ToString(Session["LanguageId"]) + Path.GetExtension(strfile)))
                            {
                                imgLanding.ImageUrl = host + "Images/LandingPage/LandingPage" + Convert.ToString(Session["LanguageId"]) + Path.GetExtension(strfile) + "?" + Guid.NewGuid();
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        Response.RedirectToRoute("Index");
                    }
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void BindResourceData()
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                StoreBE lstStoreDetail = StoreBL.GetStoreDetails();
                List<StoreBE.StoreCurrencyBE> Storecurrency = lstStoreDetail.StoreCurrencies.FindAll(x => x.IsActive == true);
                ShowLoginModalSetting(Storecurrency);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }

        }
        protected void ShowLoginModalSetting(List<StoreBE.StoreCurrencyBE> Storecurrency)
        {
            try
            {
                if (Storecurrency.Count > 3)
                {
                    divDropDownCurrency.Visible = true;
                    divCurrencyArea.Visible = false;
                    BindCurrencyDropDown(Storecurrency);
                }
                else
                {
                    repaterCurrency.DataSource = Storecurrency;
                    repaterCurrency.DataBind();
                    divCurrencyArea.Visible = true;
                    divDropDownCurrency.Visible = false;
                }

                #region "Added by Sripal"
                if (Storecurrency.Count == 1)
                {
                    currencyid = Convert.ToString(Storecurrency[0].CurrencyId);
                    string currencysymbol = Storecurrency[0].CurrencySymbol.Trim();
                    Session["CurrencySymbol"] = strCurrencySymbol;
                    Session["activeCurrencyId"] = currencyid;
                    Session["CurrencyId"] = currencyid;
                    if (Session["RegisterPg"] != null)
                    {
                        GetDetails();
                    }
                    else
                    {
                        UserRegisteration();
                    }
                }
                else if (Session["activeCurrencyId"] != null)
                {
                    if (Session["RegisterPg"] != null)
                    {
                        GetDetails();
                    }
                    else
                    {
                        UserRegisteration();
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void BindCurrencyDropDown(List<StoreBE.StoreCurrencyBE> Storecurrency)
        {
            try
            {
                ddlCurrency.DataSource = Storecurrency;
                ddlCurrency.DataValueField = "CurrencyId";
                ddlCurrency.DataTextField = "CurrencyName";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("Select Currency", "0"));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void lnkShowLoginModal_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                string[] commandArgs = lnk.CommandArgument.ToString().Split(new char[] { ',' });
                currencyid = commandArgs[0];
                strCurrencySymbol = commandArgs[1];
                Session["CurrencySymbol"] = strCurrencySymbol;
                Session["activeCurrencyId"] = currencyid;
                Session["CurrencyId"] = currencyid;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                if (Session["RegisterPg"] != null && Session["userReg"] != null)
                {
                    UserBE objBEs = new UserBE();
                    objBEs = (UserBE)Session["userReg"];
                    objBEs.CurrencyId = Convert.ToInt16(currencyid);

                    UserBE objUser = new UserBE();
                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                    if (objUser != null || objUser.EmailId != null)
                    {
                        UpdateBasketSessionProducts(objUser, 'Y');
                        Session["User"] = objUser;
                        Session["CaptchaText"] = null;
                        GlobalFunctions.SetCurrencyId(Convert.ToInt16(Session["activeCurrencyId"]));
                        GlobalFunctions.SetCurrencySymbol(strCurrencySymbol);
                        Session["activeCurrencyId"] = null;
                        Response.RedirectToRoute("index");
                        //Response.Redirect(host + "MyAccount/Profile.aspx", true);
                    }
                }
                else
                {
                    UserRegisteration();
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                List<StoreBE.StoreCurrencyBE> Storecurrency = Cache["CacheStorecurrency"] as List<StoreBE.StoreCurrencyBE>;
                currencyid = Convert.ToString(ddlCurrency.SelectedValue);
                strCurrencySymbol = Convert.ToString(Storecurrency.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(currencyid)).CurrencySymbol);
                Session["CurrencySymbol"] = strCurrencySymbol;
                //lblCurrency.Text = strCurrencySymbol.Trim().ToString();
                //ShowLoginModalSetting(Storecurrency);
                ///ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                Session["activeCurrencyId"] = currencyid;
                Session["CurrencyId"] = currencyid;
                UserRegisteration();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        private void UserRegisteration()
        {
            try
            {
                UserBE objBE = new UserBE();
                string strPassword = Convert.ToString(Session["FirstName"]) + " " + Convert.ToString(Session["LastName"]);
                string SaltPass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(strPassword), "SHA512", null);
                objBE.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["UserName"]));
                objBE.Password = SaltPass;
                objBE.FirstName = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["FirstName"]));
                objBE.LastName = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["LastName"]));
                objBE.PredefinedColumn8 = "12";
                objBE.IPAddress = GlobalFunctions.GetIpAddress();
                objBE.Points = -1;
                UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                objBE.UserDeliveryAddress.Add(obj);

                objBE.IsGuestUser = false;
                objBE.CurrencyId = Convert.ToInt16(currencyid);
                i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");

                int waitTime = LoginWaitTime;
                int validateUser = UserBL.ExecuteLoginDetails(Constants.USP_ValidateLoginDetails, true, objBE, "StoreCustomer", LoginWrongAttempts, waitTime);
                if (validateUser == 1) // -- Success
                {
                    UserBE objBEs = new UserBE();
                    objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["UserName"]));
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    UserBE objUser = new UserBE();
                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                    if (objUser != null || objUser.EmailId != null)
                    {
                        UpdateBasketSessionProducts(objUser, 'Y');
                        Session["User"] = objUser;
                        Session["CaptchaText"] = null;
                        GlobalFunctions.SetCurrencyId(Convert.ToInt16(Session["activeCurrencyId"]));
                        GlobalFunctions.SetCurrencySymbol(strCurrencySymbol);
                        Session["activeCurrencyId"] = null;
                        Response.RedirectToRoute("index");
                        //Response.Redirect(host + "MyAccount/Profile.aspx", true);
                    }
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        public static void UpdateBasketSessionProducts(UserBE objUserBE, char Action)
        {
            try
            {
                List<object> lstShoppingBE = new List<object>();
                lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID);
                if (Action != 'Y')
                {
                    ShoppingCartBL.UpdateBasketSessionProducts(objUserBE.UserId, "");
                }
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShoppingCart = new List<ShoppingCartBE>();
                    lstNewShoppingCart = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    for (int i = 0; i < lstNewShoppingCart.Count; i++)
                    {
                        Dictionary<string, string> dictionary = new Dictionary<string, string>();
                        dictionary.Add("UserId", Convert.ToString(objUserBE.UserId));
                        dictionary.Add("ProductSKUId", Convert.ToString(lstNewShoppingCart[i].ProductSKUId));
                        dictionary.Add("Quantity", Convert.ToString(lstNewShoppingCart[i].Quantity));
                        dictionary.Add("CurrencyId", Convert.ToString(objUserBE.CurrencyId));
                        dictionary.Add("UserSessionId", "");
                        dictionary.Add("UserInput", "");
                        dictionary.Add("ProductId", "");
                        int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                    }
                    ShoppingCartBL.UpdateBasketSessionProducts(0, HttpContext.Current.Session.SessionID);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        private void GetDetails()
        {
            try
            {
                UserBE objBEs = new UserBE();
                objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["UserName"]));
                objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                UserBE objUser = new UserBE();
                objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                if (objUser != null || objUser.EmailId != null)
                {
                    UpdateBasketSessionProducts(objUser, 'Y');
                    Session["User"] = objUser;
                    Session["CaptchaText"] = null;
                    GlobalFunctions.SetCurrencyId(Convert.ToInt16(Session["activeCurrencyId"]));
                    GlobalFunctions.SetCurrencySymbol(strCurrencySymbol);
                    Session["activeCurrencyId"] = null;
                    Session["RegisterPg"] = null;
                    Session["userReg"] = null;
                    Response.RedirectToRoute("index");
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        //protected void repaterCurrency_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //Literal ltrcurrency = (Literal)e.Item.FindControl("ltrcurrency");
        ////HtmlGenericControl divDynamicCurrency = (HtmlGenericControl)e.Item.FindControl("divDynamicCurrency");
        ////divDynamicCurrency.Attributes.Add("class", "col-xs-12 col-md-4");
        //lblCurrency.Text.Trim();
        //}

    }
}