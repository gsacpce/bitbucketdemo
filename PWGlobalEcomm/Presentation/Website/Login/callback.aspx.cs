﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.Script.Serialization;

namespace Presentation
{
    public partial class Login_callback : System.Web.UI.Page
    {
        private string accessToken;
        AddJsonSSOBE objAddJsonSSOBE = new AddJsonSSOBE();
        bool blnresultpageload = false;

        public class GoogleEmail
        {
            public string email { get; set; }
            public string given_name { get; set; }
            public string family_name { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //GANESH
                objAddJsonSSOBE.jsonemail = "step 1: Callback Page_Load";
                objAddJsonSSOBE.jsonuserinfo = "Callback Page_Load";
                blnresultpageload = AddJsonSSOBL.InsertAddJsonSSO(objAddJsonSSOBE);

                accessToken = Request["accesstoken"].ToString();

                if (string.IsNullOrEmpty(accessToken))
                {
                    objAddJsonSSOBE.jsonemail = "step 2: NullOrEmpty response from accessToken";
                    objAddJsonSSOBE.jsonuserinfo = "NullOrEmpty response from accessToken";
                    blnresultpageload = AddJsonSSOBL.InsertAddJsonSSO(objAddJsonSSOBE);
                    return;
                }

                //let's send an http-request to Google+ API using the token          
                string json = GetGoogleUserJSON(accessToken);
                string jsoninfo = GetGoogleUserinfoJSON(accessToken);

                //To add jsoninfo to data
                objAddJsonSSOBE.jsonemail = json;
                objAddJsonSSOBE.jsonuserinfo = jsoninfo;
                blnresultpageload = AddJsonSSOBL.InsertAddJsonSSO(objAddJsonSSOBE);

                //and Deserialize the JSON response
                JavaScriptSerializer js = new JavaScriptSerializer();
                GoogleEmail oUser = js.Deserialize<GoogleEmail>(jsoninfo);

                Session["UserName"] = oUser.email;
                Session["FirstName"] = oUser.given_name;
                Session["LastName"] = oUser.family_name;
                Session["IsSSO"] = true;
                Response.Redirect("~/Login/SelectCurrency.aspx");
                //Response.RedirectToRoute("Index");
            }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }


        private string GetGoogleUserJSON(string access_token)
        {
            string retirnedJson = "";
            try
            {
                string url = "https://www.googleapis.com/userinfo/email?alt=json";

                WebClient wc = new WebClient();
                wc.Headers.Add("Authorization", "OAuth " + accessToken);
                Stream data = wc.OpenRead(url);
                StreamReader reader = new StreamReader(data);
                retirnedJson = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return retirnedJson;
        }

        private string GetGoogleUserinfoJSON(string access_token)
        {
            string retirnedJson = "";
            try
            {
                string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json";

                WebClient wc = new WebClient();
                wc.Headers.Add("Authorization", "OAuth " + accessToken);
                Stream data = wc.OpenRead(url);
                StreamReader reader = new StreamReader(data);
                retirnedJson = reader.ReadToEnd();
                data.Close();
                reader.Close();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return retirnedJson;
        }
    }
}