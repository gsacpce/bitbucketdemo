﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Presentation
{
    public partial class Login_MichelinSSO : BasePage
    {
        CookieContainer cookieContainer = new CookieContainer();
        string MichelinPLESSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MichelinPLE_SSOLogs";
        string PreferredLanguageCode, PreferredCurrencyCode;
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
        static int intLanguageId;
        static int intCurrencyId;
        StoreBE lstStoreDetail = new StoreBE();
        public string strInvalidUserErrorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            Exceptions.WriteSSOLog("Login Page Michelin Smile SSO Log :- " + DateTime.Now, MichelinPLESSOLogFolderPath);
            string strQueryString;
            string strUser = "";
            string strErrorCode = "";
            string strErrorMsg = "";
            string strpgtIou = "";
            string strpgtId = "";
            string strTemppgtIou = "";
            string strProxyTicket = "";
            string UserEmail = "";
            UserRepoData objUserRepoData = null;
            CountryDetails objCountryDetails = null;            
            CompanyDetails objCompanyDetails = null;
            company objCompany = null;
            Int16 iUserTypeID = 1;            
            try
            {
                BindResourceData();
                #region ADDED BY SHRIGANESH 21 Feb 2017 - FOR MICHELIN SSO
                if (!Request.Url.ToString().Contains("localhost") && Session["MichelinSSORedirection"] == null)
                {
                    Session["MichelinSSORedirection"] = true;                    
                    Exceptions.WriteSSOLog("Inside MichelinSSORedirection == null", MichelinPLESSOLogFolderPath);
                    Response.Redirect("https://michelin-auth.clients.smile.fr/portal/cas/login?service=https://www.brand-estore.com/MDClone/Login/MichelinSSO.aspx");
                }
                #endregion
                Exceptions.WriteSSOLog("Michelin Smile SSO Inside Page Load", MichelinPLESSOLogFolderPath);
                strQueryString = Convert.ToString(Request.QueryString["ticket"]).Trim();
                Exceptions.WriteSSOLog("strQueryString = " + strQueryString, MichelinPLESSOLogFolderPath);
                if (!string.IsNullOrEmpty(strQueryString))
                {
                    #region AUTHENTICATION
                    string strCAS_URL = GlobalFunctions.GetSetting("CAS_URL");
                    string strquerystring_TicketValidate = "service=https://www.brand-estore.com/MDClone/Login/MichelinSSO.aspx&ticket=" + strQueryString + "&pgtUrl=https://www.brand-estore.com/MDClone/Login/pgtCallback.aspx";

                    Exceptions.WriteSSOLog("***** Get XDOC *****", MichelinPLESSOLogFolderPath);
                    Exceptions.WriteSSOLog("strCAS_URL = " + strCAS_URL, MichelinPLESSOLogFolderPath);
                    Exceptions.WriteSSOLog("strquerystring_TicketValidate = " + strquerystring_TicketValidate, MichelinPLESSOLogFolderPath);
                    XDocument xdoc = CallHttpGet(strCAS_URL, strquerystring_TicketValidate);
                    Exceptions.WriteSSOLog("XDOC Value = " + Convert.ToString(xdoc), MichelinPLESSOLogFolderPath);

                    XNamespace cas = GlobalFunctions.GetSetting("cas");
                    if (xdoc != null)
                    {
                        if (xdoc.Root.Element(cas + "authenticationSuccess") != null)
                        {
                            strUser = xdoc.Root.Element(cas + "authenticationSuccess").Element(cas + "user").Value.ToString();
                            Exceptions.WriteSSOLog("SSO User Email " + strUser, MichelinPLESSOLogFolderPath);
                            Session["SSO_UserEmail"] = strUser;
                            if (!string.IsNullOrEmpty(xdoc.Root.Element(cas + "authenticationSuccess").Element(cas + "proxyGrantingTicket").Value))
                            {
                                strpgtIou = xdoc.Root.Element(cas + "authenticationSuccess").Element(cas + "proxyGrantingTicket").Value.ToString();
                                Exceptions.WriteSSOLog("Login Page STRPGTIOU = " + strpgtIou, MichelinPLESSOLogFolderPath);
                            }
                        }
                        if (xdoc.Root.Element(cas + "authenticationFailure") != null)
                        {
                            strErrorCode = xdoc.Root.Element(cas + "authenticationFailure").Attribute("code").Value;
                            strErrorMsg = xdoc.Root.Element(cas + "authenticationFailure").Value.ToString();
                            Exceptions.WriteSSOLog("strErrorCode = " + strErrorCode, MichelinPLESSOLogFolderPath);
                            Exceptions.WriteSSOLog("strErrorMsg = " + strErrorMsg, MichelinPLESSOLogFolderPath);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                        }
                    }
                    #endregion

                    #region REGION PGT
                    MichelinSmileSSOBL objMichelinSmileSSOBL = new MichelinSmileSSOBL();
                    DataTable dtPGTValue = objMichelinSmileSSOBL.GetPGTValue(Constants.USP_GETPGTVALUE, strpgtIou);

                    if (dtPGTValue != null)
                    {
                        if (dtPGTValue.Rows.Count > 0)
                        {
                            strpgtId = Convert.ToString(dtPGTValue.Rows[0]["PGTId"]);
                            strTemppgtIou = Convert.ToString(dtPGTValue.Rows[0]["PGTIOU"]);
                        }
                    }

                    string strCAS = GlobalFunctions.GetSetting("CASURL");//https://michelin-auth.clients.smile.fr/portal/cas
                    string strUserRepo_URL = GlobalFunctions.GetSetting("UserRepo_URL");//https://michelin-userrepo.clients.smile.fr/api/v2


                    if (!string.IsNullOrEmpty(strpgtId) && !string.IsNullOrEmpty(strTemppgtIou))
                    {
                        if (strpgtIou.Equals(strTemppgtIou))
                        {
                            Exceptions.WriteSSOLog("PGTIOU are same", MichelinPLESSOLogFolderPath);
                            string strQueryString_ProxyTicket = "targetService=" + strUserRepo_URL + "/auth/multilang&pgt=" + strpgtId;
                            XDocument xdocProxyTicket = CallHttpGet(strCAS + "/proxy", strQueryString_ProxyTicket);
                            if (xdocProxyTicket.Root.Element(cas + "proxySuccess") != null)
                            {
                                strProxyTicket = xdocProxyTicket.Root.Element(cas + "proxySuccess").Element(cas + "proxyTicket").Value.ToString();
                            }
                            if (xdoc.Root.Element(cas + "proxyFailure") != null)
                            {
                                strErrorCode = xdoc.Root.Element(cas + "proxyFailure").Attribute("code").Value;
                                strErrorMsg = xdoc.Root.Element(cas + "proxyFailure").Value.ToString();
                                //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('Error Message '" + strErrorMsg + "'\n Error Code '" + strErrorCode + ");window.location.href = 'default.aspx'", true);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                            }
                            Exceptions.WriteSSOLog("Proxy Ticket = " + strProxyTicket, MichelinPLESSOLogFolderPath);
                        }
                        else
                        {
                            Exceptions.WriteSSOLog("Session User " + Convert.ToString(Session["SSO_UserEmail"]) + " New and Old PGIOU are not same", MichelinPLESSOLogFolderPath);
                            //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('SSO Authentication Failed, You will be redirect to login page.');window.location.href = 'default.aspx'", true);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                        }
                    }
                    else
                    {
                        Exceptions.WriteSSOLog("Session User " + Convert.ToString(Session["SSO_UserEmail"]) + " pgtID and pgtIou Empty", MichelinPLESSOLogFolderPath);
                        //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('SSO Authentication Failed, You will be redirect to login page.');window.location.href = 'default.aspx'", true);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                    }
                    #endregion

                    #region TICKET AND USER REPO REGION
                    if (!string.IsNullOrEmpty(strProxyTicket))
                    {
                        string QueryStringProxyTicket = "ticket=" + strProxyTicket;
                        Exceptions.WriteSSOLog("***** Get strUserRepo *****", MichelinPLESSOLogFolderPath);
                        string strUserRepo = CallHttpGetJson(strUserRepo_URL + "/auth/multilang", QueryStringProxyTicket);
                        JavaScriptSerializer objJavaScriptSerializer = new JavaScriptSerializer();
                        if (!string.IsNullOrEmpty(strUserRepo))
                        {
                            UserRepoToken objUserRepoToken = objJavaScriptSerializer.Deserialize<UserRepoToken>(strUserRepo);
                            var perpareTokenUserRepo = new Dictionary<string, string>
                                {
                                    {"application",objUserRepoToken.application},
                                    {"username",objUserRepoToken.username},
                                    {"token",objUserRepoToken.token}
                                };
                            string strperpareTokenUserRepo = objJavaScriptSerializer.Serialize(perpareTokenUserRepo);
                            var tempperpareTokenUserRepo = System.Text.Encoding.UTF8.GetBytes(strperpareTokenUserRepo);
                            string urtoken = Convert.ToBase64String(tempperpareTokenUserRepo);

                            Exceptions.WriteSSOLog("***** Get UserRepoData *****", MichelinPLESSOLogFolderPath);
                            Exceptions.WriteSSOLog("strUserRepo_URL = " + strUserRepo_URL + "/me", MichelinPLESSOLogFolderPath);
                            Exceptions.WriteSSOLog("urtoken = " + urtoken, MichelinPLESSOLogFolderPath);

                            string UserRepoData = CallHttpGetJson(strUserRepo_URL + "/me", null, urtoken);

                            if (!string.IsNullOrEmpty(UserRepoData))
                            {
                                Exceptions.WriteSSOLog("Before Deserializing UserRepoData ", MichelinPLESSOLogFolderPath);
                                objUserRepoData = objJavaScriptSerializer.Deserialize<UserRepoData>(UserRepoData);

                                string UserCountryURL = objUserRepoData._links.country.href;
                                string UserCompanyURL = objUserRepoData._links.company.href; // Added by SHRIGANESH SINGH 20 May 2016

                                Exceptions.WriteSSOLog("UserCountryURL before Concatanation = " + UserCountryURL, MichelinPLESSOLogFolderPath);
                                Exceptions.WriteSSOLog("UserCompanyURL before Concatanation = " + UserCompanyURL, MichelinPLESSOLogFolderPath);

                                UserEmail = objUserRepoData.email;

                                #region COUNTRY DETAILS
                                if (!string.IsNullOrEmpty(UserCountryURL))
                                {
                                    Exceptions.WriteSSOLog("strUserRepo_URL before Concatenation = " + strUserRepo_URL, MichelinPLESSOLogFolderPath);
                                    strUserRepo_URL = strUserRepo_URL.Replace("/api/v2", "");
                                    UserCountryURL = strUserRepo_URL + UserCountryURL;
                                    Exceptions.WriteSSOLog("UserCountryURL = " + UserCountryURL, MichelinPLESSOLogFolderPath);
                                    string UserRepoCountry = CallHttpGetJson(UserCountryURL, null, urtoken);
                                    objCountryDetails = objJavaScriptSerializer.Deserialize<CountryDetails>(UserRepoCountry);
                                    Exceptions.WriteSSOLog("", MichelinPLESSOLogFolderPath);
                                    Exceptions.WriteSSOLog("objCountryDetails = " + Convert.ToString(objCountryDetails), MichelinPLESSOLogFolderPath);
                                }
                                else
                                {
                                    Exceptions.WriteSSOLog("Session User " + Convert.ToString(Session["SSO_UserEmail"]) + " User Country Data is Null", MichelinPLESSOLogFolderPath);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                                    //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('SSO Authentication Failed, You will be redirect to login page.');window.location.href = 'default.aspx'", true);
                                }

                                #endregion

                                #region COMPANY DETAILS
                                if (!string.IsNullOrEmpty(UserCompanyURL))
                                {
                                    Exceptions.WriteSSOLog("strUserRepo_URL before Concatenation = " + strUserRepo_URL, MichelinPLESSOLogFolderPath);
                                    UserCompanyURL = strUserRepo_URL + UserCompanyURL;
                                    UserCompanyURL = UserCompanyURL.Replace("/api/v2/me", "");
                                    Exceptions.WriteSSOLog("UserCompanyURL = " + UserCompanyURL, MichelinPLESSOLogFolderPath);
                                    string UserRepoCompany = CallHttpGetJson(UserCompanyURL, null, urtoken);
                                    objCompanyDetails = objJavaScriptSerializer.Deserialize<CompanyDetails>(UserRepoCompany);
                                    Exceptions.WriteSSOLog("", MichelinPLESSOLogFolderPath);
                                    Exceptions.WriteSSOLog("objCompanyDetails = " + Convert.ToString(objCompanyDetails), MichelinPLESSOLogFolderPath);
                                }
                                else
                                {
                                    Exceptions.WriteSSOLog("Session User " + Convert.ToString(Session["SSO_UserEmail"]) + " User Country Data is Null", MichelinPLESSOLogFolderPath);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                                    //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('SSO Authentication Failed, You will be redirect to login page.');window.location.href = 'default.aspx'", true);
                                }
                                #endregion

                                #region USER REPO DATA
                                if (objUserRepoData.id != null)
                                {
                                    Exceptions.WriteSSOLog("strUserRepo_URL before Concatenation = " + strUserRepo_URL, MichelinPLESSOLogFolderPath);
                                    string CustomerStructures = "/api/v2/customerstructures/" + objUserRepoData.id;
                                    Exceptions.WriteSSOLog("CustomerStructures = " + CustomerStructures, MichelinPLESSOLogFolderPath);
                                    string CustomerStructuresURL = strUserRepo_URL + CustomerStructures;
                                    Exceptions.WriteSSOLog("CustomerStructuresURL = " + CustomerStructuresURL, MichelinPLESSOLogFolderPath);
                                    string UserRepositoryCustomerStructures = CallHttpGetJson(CustomerStructuresURL, null, urtoken);
                                    objCompany = objJavaScriptSerializer.Deserialize<company>(UserRepositoryCustomerStructures);
                                    Exceptions.WriteSSOLog("objCompany = " + Convert.ToString(objCompany), MichelinPLESSOLogFolderPath);
                                }
                                else
                                {
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                                    //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('SSO Authentication Failed ID is NULL, You will be redirect to login page.');window.location.href = 'default.aspx'", true);
                                }
                                #endregion

                            }
                            else
                            {
                                Exceptions.WriteSSOLog("Session User " + Convert.ToString(Session["SSO_UserEmail"]) + " User Repo Data Null", MichelinPLESSOLogFolderPath);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                                //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('SSO Authentication Failed, You will be redirect to login page.');window.location.href = 'default.aspx'", true);
                            }
                        }
                        else
                        {
                            Exceptions.WriteSSOLog("Session User " + Convert.ToString(Session["SSO_UserEmail"]) + " UserRepo Token Data Null", MichelinPLESSOLogFolderPath);
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                            //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('SSO Authentication Failed, You will be redirect to login page.');window.location.href = 'default.aspx'", true);
                        }
                    }
                    else
                    {
                        Exceptions.WriteSSOLog("Session User " + Convert.ToString(Session["SSO_UserEmail"]) + " Proxy Ticket is Empty", MichelinPLESSOLogFolderPath);
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                        //ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('SSO Authentication Failed, You will be redirect to login page.');window.location.href = 'default.aspx'", true);
                    }
                    #endregion
                }
                else
                {
                      GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
                }

                Exceptions.WriteSSOLog("UserRepoData check", MichelinPLESSOLogFolderPath);
                if (objUserRepoData != null)
                {
                    Exceptions.WriteSSOLog("UserRepoData is not null", MichelinPLESSOLogFolderPath);
                    Exceptions.WriteSSOLog("IsEmailExists check", MichelinPLESSOLogFolderPath);

                    List<CurrencyBE> lstCurrencies = new List<CurrencyBE>();
                    lstCurrencies = CurrencyBL.GetAllCurrencyDetails();

                    int i = 0;
                    int IsExist;
                    UserBE objBE = new UserBE();
                    objBE.EmailId = objUserRepoData.email;
                    UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                    IsExist = UserBL.IsExistUser(Constants.USP_IsExistUser_MichelinSSO, true, objBE);
                    if (IsExist == 0)
                    {
                        objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);
                        objBE.FirstName = objUserRepoData.first_name;
                        objBE.LastName = objUserRepoData.last_name;
                        objBE.PredefinedColumn1 = objUserRepoData.first_name + " " + objUserRepoData.last_name;
                        objBE.PredefinedColumn2 = objCompanyDetails.name;
                        objBE.PredefinedColumn3 = objCompanyDetails.street;
                        objBE.PredefinedColumn4 = "";
                        objBE.PredefinedColumn5 = objCompanyDetails.city;
                        objBE.PredefinedColumn6 = "";
                        objBE.PredefinedColumn7 = objUserRepoData._embedded.company.zipcode.id;
                        //objBE.PredefinedColumn8 = "12";

                        List<CountryBE> lstCountry = new List<CountryBE>();
                        lstCountry = CountryBL.GetAllCountries();

                        if (lstCountry != null && lstCountry.Count > 0)
                        {
                            objBE.PredefinedColumn8 = Convert.ToString(lstCountry.FirstOrDefault(x => x.CountryName == objCountryDetails.name).CountryId);
                        }

                        objBE.PredefinedColumn9 = objUserRepoData.phone;
                        objBE.IPAddress = GlobalFunctions.GetIpAddress();

                        objBE.IsGuestUser = false;
                        objBE.IsMarketing = false;

                        #region USER LOCATION DETAILS
                        if (!string.IsNullOrEmpty(objUserRepoData.locale))
                        {
                            string[] values = null;
                            values = objUserRepoData.locale.Split('_');
                            PreferredLanguageCode = values[0];
                            PreferredCurrencyCode = values[1];
                        }

                        List<SSOUserTypeCatalogueDetailsBE> lstSSOUserTypeCatalogueDetailsBE = new List<SSOUserTypeCatalogueDetailsBE>();
                        lstSSOUserTypeCatalogueDetailsBE = UserTypesBL.GetSSOUserTypeCatalogueDetails(Constants.USP_GetSSOUserTypeCatalogueDetails, null, true);

                        if (lstSSOUserTypeCatalogueDetailsBE != null)
                        {
                            switch (PreferredCurrencyCode)
                            {
                                case "GB":
                                    iUserTypeID = lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeName == "MyAccount UK").UserTypeID;
                                    GlobalFunctions.SetCurrencyId(lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeID == iUserTypeID).CurrencyId);
                                    break;

                                case "CH":
                                    iUserTypeID = lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeName == "MyAccount Switzerland").UserTypeID;
                                    GlobalFunctions.SetCurrencyId(lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeID == iUserTypeID).CurrencyId);
                                    break;
                                case "NO":
                                    iUserTypeID = lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeName == "MyAccount Norway").UserTypeID;
                                    GlobalFunctions.SetCurrencyId(lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeID == iUserTypeID).CurrencyId);
                                    break;
                                case "DK":
                                    iUserTypeID = lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeName == "MyAccount Denmark").UserTypeID;
                                    GlobalFunctions.SetCurrencyId(lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeID == iUserTypeID).CurrencyId);
                                    break;
                                case "SE":
                                    iUserTypeID = lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeName == "MyAccount Sweden").UserTypeID;
                                    GlobalFunctions.SetCurrencyId(lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeID == iUserTypeID).CurrencyId);
                                    break;
                                default:
                                    iUserTypeID = lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeName == "MyAccount EUR").UserTypeID;
                                    GlobalFunctions.SetCurrencyId(lstSSOUserTypeCatalogueDetailsBE.FirstOrDefault(x => x.UserTypeID == iUserTypeID).CurrencyId);
                                    break;
                            }
                        }

                        List<LanguageCodeMaster> lstLanguageCodeMaster = new List<LanguageCodeMaster>();
                        lstLanguageCodeMaster = LanguageBL.GetAlllstLanguageCodeMaster(PreferredLanguageCode);

                        if (lstLanguageCodeMaster != null)
                        {
                            GlobalFunctions.SetLanguageId(lstLanguageCodeMaster.FirstOrDefault(x => x.LanguageCode == PreferredLanguageCode).LanguageId);
                        }
                        else
                        {
                            GlobalFunctions.SetLanguageId(1);// Hard coded as this LanguageID (English) is default set for rest of the users
                        }

                        #endregion

                        objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBE.UserDeliveryAddress.Add(obj);

                        objBE.CustomColumn1 = "";
                        objBE.CustomColumn2 = "";
                        objBE.CustomColumn3 = "";
                        objBE.CustomColumn4 = "";
                        objBE.CustomColumn5 = "";
                        objBE.CustomColumn6 = "";
                        objBE.CustomColumn7 = "";
                        objBE.CustomColumn8 = "";
                        objBE.CustomColumn9 = "";
                        objBE.CustomColumn10 = "";
                        objBE.CustomColumn11 = "";
                        objBE.CustomColumn12 = "";
                        objBE.CustomColumn13 = "";
                        objBE.CustomColumn14 = "";
                        objBE.CustomColumn15 = "";
                        objBE.CustomColumn16 = "";
                        objBE.CustomColumn17 = "";
                        objBE.CustomColumn18 = "";
                        objBE.CustomColumn19 = "";
                        objBE.CustomColumn20 = "";
                        objBE.CustomColumn21 = "";
                        objBE.CustomColumn22 = "";
                        objBE.CustomColumn23 = "";
                        objBE.CustomColumn24 = "";
                        objBE.CustomColumn25 = "";
                        objBE.CustomColumn26 = "";
                        objBE.CustomColumn27 = "";
                        objBE.CustomColumn28 = "";
                        objBE.CustomColumn29 = "";
                        objBE.CustomColumn30 = "";

                        objBE.UserDeliveryAddress[0].AddressTitle = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn1 = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn2 = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn3 = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn4 = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn5 = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn6 = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn7 = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn8 = "";
                        objBE.UserDeliveryAddress[0].PreDefinedColumn9 = "";
                        objBE.UserTypeID = iUserTypeID;// Added by SHRIGANESH 08 Feb 2017
                        objBE.Points = -1; // Added by ShriGanesh 26 April 2017
                        objBE.UserTitle = "";
                        objBE.IsPunchoutUser = false;

                        Exceptions.WriteInfoLog("Before calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                        i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                        Exceptions.WriteInfoLog("After calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                        objBE.UserId = Convert.ToInt16(i);

                        UserPreferredBE objUserPreferredBE = new UserPreferredBE();
                        objUserPreferredBE.UserId = objBE.UserId;
                        objUserPreferredBE.PreferredLanguageId = objBE.LanguageId;
                        objUserPreferredBE.PreferredCurrencyId = objBE.CurrencyId;
                        UserPreferredBL.InsertUpdateUserPreference(Constants.usp_InsertUpdateUserPreferred, true, objUserPreferredBE);

                        InsertBASYSId(GlobalFunctions.GetCurrencyId(), UserEmail, objBE.UserId, objUserRepoData, objCountryDetails, objCompanyDetails);

                        UserBE objBEs = new UserBE();
                        UserBE objUser = new UserBE();
                        objBEs.EmailId = UserEmail;
                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["MichelinSSO"] = true;
                            HttpContext.Current.Session["User"] = objUser;
                            Session["MichelinSSORedirection"] = null; 
                            GlobalFunctions.SetCurrencyId(objUser.UserPreferredCurrencyID);
                            GlobalFunctions.SetCurrencySymbol(lstCurrencies.First(x=> x.CurrencyId == objUser.UserPreferredCurrencyID).CurrencySymbol);                            
                            Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                            Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                            Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                            Session["GuestUser"] = null;
                            Response.RedirectToRoute("index");
                        }
                    }
                    else
                    {
                        UserBE objBEs = new UserBE();
                        UserBE objUser = new UserBE();
                        objBEs.EmailId = UserEmail;
                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["MichelinSSO"] = true;
                            HttpContext.Current.Session["User"] = objUser;
                            Session["MichelinSSORedirection"] = null; 
                            GlobalFunctions.SetCurrencyId(objUser.UserPreferredCurrencyID);
                            GlobalFunctions.SetCurrencySymbol(Convert.ToString(lstCurrencies.First(x=> x.CurrencyId == objUser.UserPreferredCurrencyID).CurrencySymbol));
                            GlobalFunctions.SetLanguageId(objUser.UserPreferredLanguageID);
                            Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                            Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                            Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                            Session["GuestUser"] = null;
                            Response.RedirectToRoute("index");
                        }
                    }

                    #region COMMENTED CODE AS IT IS NO LONGER NEEDED 06 FEB 2017
                    //if (!IsEmailExists(objUserRepoData.email))
                    //{
                    //    Exceptions.WriteSSOLog("Email doesn't exists", MichelinPLESSOLogFolderPath);
                    //    int intCustomerID_OASIS = BuildCustomerContactID(objUserRepoData, objCountryDetails, objCompanyDetails, objCompany);
                    //    Exceptions.WriteSSOLog("CustomerID_OASIS -> " + Convert.ToString(intCustomerID_OASIS), MichelinPLESSOLogFolderPath);
                    //    if (intCustomerID_OASIS > 0)
                    //    {
                    //        UserRegisteration(objUserRepoData, objCountryDetails, intCustomerID_OASIS, objCompanyDetails, objCompany);
                    //    }
                    //}
                    //else
                    //{
                    //    Exceptions.WriteSSOLog("Email exists", MichelinPLESSOLogFolderPath);
                    //    //long userID = new UsersBL().GetUserId(objUserRepoData.email.Trim(), 1130);
                    //    //SetLoginSessions(objUserRepoData, userID);
                    //} 
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteSSOLog("Inside Page_Load Exception - Target Site " + ex.TargetSite + " Inner Exception " + ex.InnerException + " Message " + ex.Message, MichelinPLESSOLogFolderPath);
                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, GlobalFunctions.GetSetting("MichelinSSOLogoutURL"), AlertType.Failure);
            }
        }

        protected XDocument CallHttpGet(string url, string @params)
        {
            Exceptions.WriteSSOLog("INSIDE CallHttpGet", MichelinPLESSOLogFolderPath);
            Exceptions.WriteSSOLog("URL = " + url, MichelinPLESSOLogFolderPath);
            Exceptions.WriteSSOLog("@params = " + @params, MichelinPLESSOLogFolderPath);
            HttpWebRequest loHttp = default(HttpWebRequest);
            if (string.IsNullOrEmpty(@params))
            {
                loHttp = (HttpWebRequest)WebRequest.Create(url);                
            }
            else
            {
                loHttp = (HttpWebRequest)WebRequest.Create(url + "?" + @params);
            }

            loHttp.Method = "GET";
            loHttp.CookieContainer = cookieContainer;
            loHttp.ContentType = "application/x-www-form-urlencoded";
            loHttp.Headers.Set("Pragma", "no-cache");
            loHttp.AllowAutoRedirect = true;
            loHttp.KeepAlive = true;
            loHttp.Timeout = 30 * 1000;

            XDocument doc = new XDocument();
            try
            {
                using (HttpWebResponse loWebResponse = (HttpWebResponse)loHttp.GetResponse())
                {
                    using (Stream stream = loWebResponse.GetResponseStream())
                    {
                        doc = XDocument.Load(stream);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteSSOLog("Inside CallHttpGet Exception - Target Site " + ex.TargetSite + " Inner Exception " + ex.InnerException + " Message " + ex.Message, MichelinPLESSOLogFolderPath);
            }
            Exceptions.WriteSSOLog(doc.ToString(), MichelinPLESSOLogFolderPath);

            return doc;
        }

        protected string CallHttpGetJson(string url, string @params, string urtoken = null)
        {
            HttpWebRequest loHttp = default(HttpWebRequest);
            if (string.IsNullOrEmpty(@params))
            {
                loHttp = (HttpWebRequest)WebRequest.Create(url);
            }
            else
            {
                loHttp = (HttpWebRequest)WebRequest.Create(url + "?" + @params);
            }

            loHttp.Method = "GET";
            loHttp.CookieContainer = cookieContainer;
            if (!string.IsNullOrEmpty(urtoken))
            {
                loHttp.Headers.Add("X-UserRepositoryToken", urtoken);
            }
            loHttp.ContentType = "application/json; charset=utf-8";
            loHttp.Headers.Set("Pragma", "no-cache");
            loHttp.AllowAutoRedirect = true;
            loHttp.KeepAlive = true;
            loHttp.Timeout = 30 * 1000;

            string text = "";
            try
            {
                using (HttpWebResponse loWebResponse = (HttpWebResponse)loHttp.GetResponse())
                {
                    using (var sr = new StreamReader(loWebResponse.GetResponseStream()))
                    {
                        text = sr.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteSSOLog("Inside CallHttpGetJson Exception - Target Site " + ex.TargetSite + " Inner Exception " + ex.InnerException + " Message " + ex.Message, MichelinPLESSOLogFolderPath);
            }
            Exceptions.WriteSSOLog("Json Resp " + text.ToString(), MichelinPLESSOLogFolderPath);

            return text;
        }

        #region CODE COMMENTED AS THIS IS NO LONGER REQUIRED 06 FEB 2017
        //public static void WriteLog(string strLog)
        //{
        //    TextWriter txtwriter = new StreamWriter(@"E:\XML\MichelinSmileSSOLog.txt", true);
        //    try
        //    {
        //        //txtwriter.WriteLine("Michelin Smile SSO Log :- " + DateTime.Now);
        //        txtwriter.WriteLine(strLog);
        //        //txtwriter.WriteLine("*********-----************-----******-----");
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        txtwriter.Close();
        //        txtwriter.Dispose();
        //    }
        //}

        //public bool IsEmailExists(string strEmail)
        //{
        //    bool blnExistData = false;
        //    UsersBL objUsersBL = new UsersBL();
        //    blnExistData = objUsersBL.IsStoreUserExists(strEmail, 1130);
        //    return blnExistData;
        //}

        ///// <summary>
        ///// Author      :   SHRIGANESH SINGH
        ///// Date        :   20 May 2016
        ///// Description :   To create the Basys Contact Id for the user
        ///// </summary>
        ///// <param name="objUserRepoData"></param>
        ///// <param name="objCountryDetails"></param>
        ///// <param name="objCompanyDetails"></param>
        ///// <param name="objCompany"></param>
        ///// <returns></returns>
        //public int BuildCustomerContactID(UserRepoData objUserRepoData, CountryDetails objCountryDetails, CompanyDetails objCompanyDetails, company objCompany)
        //{
        //    int intCustomerID_OASIS = 0;
        //    DataTable dttblStore = null;
        //    //StoresBL objStore = new StoresBL();
        //    try
        //    {
        //        //long LoggedInStoreId = Convert.ToInt64(clsConstantsManager.GetConstant("CS_STORECREATED_STOREID"));
        //        //dttblStore = objStore.Getwsstoredetails(LoggedInStoreId, 1130);
        //        string strCustomerId = dttblStore.Rows[0]["defaultCustomerId"].ToString();
        //        string strSourceCodeId = dttblStore.Rows[0]["SourceCodeId"].ToString();
        //        StoreBE objStore = StoreBL.GetStoreDetails();
        //        string DefaultCustId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId; //1281850; // objStore.StoreCurrencies[i].DefaultCompanyId;
        //        string SourceCodeId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).SourceCodeId; //5281; //objStore.StoreCurrencies[i].SourceCodeId;

        //        string FirstName = objUserRepoData.first_name;
        //        string LastName = objUserRepoData.last_name;
        //        string CompanyName = objCompanyDetails.name;
        //        string CompanyStreet = objCompanyDetails.street;
        //        string Town = objCompanyDetails.city;
        //        string Zipcode = objUserRepoData._embedded.company.zipcode.id;
        //        string CountryCode = objCountryDetails.two_letters_code;
        //        string Country = objCountryDetails.name;
        //        string Phone = objUserRepoData.phone;
        //        string email = objUserRepoData.email;

        //        Exceptions.WriteSSOLog("FirstName = " + FirstName, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("LastName = " + LastName, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("CompanyName = " + CompanyName, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("CompanyStreet = " + CompanyStreet, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("Town = " + Town, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("Zipcode = " + Zipcode, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("CountryCode = " + CountryCode, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("Country = " + Country, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("Phone = " + Phone, MichelinPLESSOLogFolderPath);
        //        Exceptions.WriteSSOLog("email = " + email, MichelinPLESSOLogFolderPath);

        //        intCustomerID_OASIS = ValidateWebServices_OASIS.BuildCustContact_OASIS(objUserRepoData.first_name.Trim(),
        //            objUserRepoData.last_name.Trim(),
        //            objCompanyDetails.name.Trim(),
        //            objCompanyDetails.street,
        //            ".",
        //            string.Empty,
        //            objCompanyDetails.city,
        //            objUserRepoData._embedded.company.zipcode.id,
        //            ".",
        //            objCountryDetails.two_letters_code,
        //            objCountryDetails.name,
        //            objUserRepoData.phone,
        //            string.Empty,
        //            "",
        //            objUserRepoData.email.Trim(),
        //            Convert.ToInt32(strCustomerId),
        //            string.Empty, strSourceCodeId);
        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //    return intCustomerID_OASIS;
        //}

        ///// <summary>
        ///// Author      :   SHRIGANESH SINGH
        ///// Date        :   20 May 2016
        ///// Description :   To register the new user
        ///// </summary>
        ///// <param name="objUserRepoData"></param>
        ///// <param name="objCountryDetails"></param>
        ///// <param name="intCustomerID_OASIS"></param>
        ///// <param name="objCompanyDetails"></param>
        ///// <param name="objcompany"></param>
        //public void UserRegisteration(UserRepoData objUserRepoData, CountryDetails objCountryDetails, int intCustomerID_OASIS, CompanyDetails objCompanyDetails, company objcompany)
        //{
        //    try
        //    {
        //        UsersBL objUsersBL = new UsersBL();
        //        Hashtable hsttblUserRegister = new Hashtable();

        //        string[] arrEmail = objUserRepoData.email.Split('@');
        //        string strPassword = Convert.ToString(arrEmail[0]);

        //        hsttblUserRegister.Add("email", objUserRepoData.email);
        //        hsttblUserRegister.Add("passwords", strPassword);

        //        hsttblUserRegister.Add("firstName", objUserRepoData.first_name);
        //        hsttblUserRegister.Add("lastName", objUserRepoData.last_name);

        //        hsttblUserRegister.Add("oasisCustomerContactId", intCustomerID_OASIS);
        //        hsttblUserRegister.Add("predefinedColumn20", objUserRepoData.first_name + " " + objUserRepoData.last_name);
        //        hsttblUserRegister.Add("predefinedColumn1", objCompanyDetails.name);
        //        hsttblUserRegister.Add("predefinedColumn2", objCompanyDetails.street);
        //        hsttblUserRegister.Add("predefinedColumn3", "");
        //        hsttblUserRegister.Add("predefinedColumn4", objCompanyDetails.city);
        //        hsttblUserRegister.Add("predefinedColumn5", "");
        //        hsttblUserRegister.Add("predefinedColumn6", objCountryDetails.name);
        //        hsttblUserRegister.Add("predefinedColumn7", objUserRepoData._embedded.company.zipcode.id);
        //        hsttblUserRegister.Add("predefinedColumn8", objUserRepoData.phone == null ? "" : objUserRepoData.phone);
        //        hsttblUserRegister.Add("predefinedColumn9", objUserRepoData.fax == null ? "" : objUserRepoData.fax);
        //        hsttblUserRegister.Add("predefinedColumn10", objUserRepoData.first_name + " " + objUserRepoData.last_name);
        //        hsttblUserRegister.Add("predefinedColumn11", objCompanyDetails.name);
        //        hsttblUserRegister.Add("predefinedColumn12", objCompanyDetails.street);
        //        hsttblUserRegister.Add("predefinedColumn13", "");
        //        hsttblUserRegister.Add("predefinedColumn14", objCompanyDetails.city);
        //        hsttblUserRegister.Add("predefinedColumn15", "");
        //        hsttblUserRegister.Add("predefinedColumn16", objCountryDetails.name);
        //        hsttblUserRegister.Add("predefinedColumn17", objUserRepoData._embedded.company.zipcode.id);
        //        hsttblUserRegister.Add("predefinedColumn18", objUserRepoData.phone == null ? "" : objUserRepoData.phone);
        //        hsttblUserRegister.Add("predefinedColumn19", objUserRepoData.fax == null ? "" : objUserRepoData.fax);

        //        hsttblUserRegister.Add("registrationApprovalAuthority", 1);
        //        hsttblUserRegister.Add("orderApprovalAuthority", 1);

        //        hsttblUserRegister.Add("activeStatus", 1);
        //        hsttblUserRegister.Add("accessStatus", 1);

        //        hsttblUserRegister.Add("createdBy", 0);
        //        hsttblUserRegister.Add("modifiedBy", 0);
        //        hsttblUserRegister.Add("createdByParentUserStoreId", Convert.ToString(clsConstantsManager.GetConstant("CS_STORECREATED_STOREID")));
        //        hsttblUserRegister.Add("modifiedByParentUserStoreId", "0");

        //        hsttblUserRegister.Add("catalogueId", 1130);
        //        long lngUserId = objUsersBL.AddUser(hsttblUserRegister);
        //        UserRolesBL objUserRolesBL = new UserRolesBL();
        //        objUserRolesBL.AddUserRole(lngUserId, 165);

        //        SetLoginSessions(objUserRepoData, lngUserId);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        //private void SetLoginSessions(UserRepoData objUserRepoData, long lngUserId)
        //{
        //    try
        //    {
        //        WriteLog("SetLoginSessions method start");
        //        string strRedirectURL = "";
        //        int lngStoreId = Convert.ToInt32(clsConstantsManager.GetConstant("CS_STORESMANAGEMENT_TEMPLATESTOREID"));
        //        Session.Add("S_USERID", lngUserId);
        //        Session.Add("S_USERCatalogueId", 1130);
        //        Session.Add("S_USERCurrencySymbol", "€");
        //        Session.Add("S_USEREMAIL", objUserRepoData.email.Trim());
        //        Session.Add("S_USERSTOREID", lngStoreId);
        //        Session.Add("S_LOGGEDINSTOREID", lngStoreId);

        //        if (Session["S_LOGINREQUESTEDPAGE"] != null)
        //        {
        //            ShoppingCartBL objShopping = new ShoppingCartBL();
        //            bool isRecordInsert = objShopping.AddGuestRecord(Convert.ToString(HttpContext.Current.Session.SessionID), Convert.ToInt32(lngUserId), 1130);
        //            if (isRecordInsert == true)
        //            {
        //                if (Convert.ToString(Session["S_LOGINREQUESTEDPAGE"]).Trim() != "")
        //                    strRedirectURL = Convert.ToString(Session["S_LOGINREQUESTEDPAGE"]).Trim();
        //            }
        //            else
        //            {
        //                strRedirectURL = "Default.aspx";
        //            }
        //        }
        //        else
        //            strRedirectURL = "Default.aspx";

        //        WriteLog("Redirect URL -> " + strRedirectURL);
        //        WriteLog("SetLoginSessions method end");
        //        Response.Redirect(strRedirectURL);
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLog("Error Message -> " + ex.Message + ", Error stacktrace -> " + ex.StackTrace);
        //    }
        //} 
        #endregion

        //Added by SHRIGANESH to Create BASYS Contact ID for SSO USER
        private void InsertBASYSId(Int16 CurrencyIdx, string strEmailId, Int16 iUserID, UserRepoData objUserRepoData, CountryDetails objCountryDetails, CompanyDetails objCompanyDetails)
        {
            try
            {
                Exceptions.WriteInfoLog("Inside InsertBASYSId() in Payment.aspx.cs");
                StoreBE objStore = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails()");

                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();

                int intCustomerID_OASIS;
                UserBE objUser = new UserBE();

                objUser.EmailId = Sanitizer.GetSafeHtmlFragment(strEmailId.Trim());
                objUser.UserId = iUserID;
                objUser.FirstName = objUserRepoData.first_name;
                objUser.LastName = objUserRepoData.last_name;
                objUser.PredefinedColumn1 = Sanitizer.GetSafeHtmlFragment(objUserRepoData.first_name + objUserRepoData.last_name);
                objUser.PredefinedColumn2 = Sanitizer.GetSafeHtmlFragment(objCompanyDetails.name);
                objUser.PredefinedColumn3 = Sanitizer.GetSafeHtmlFragment(objCompanyDetails.street);
                objUser.PredefinedColumn4 = Sanitizer.GetSafeHtmlFragment(".");
                objUser.PredefinedColumn5 = Sanitizer.GetSafeHtmlFragment(objCompanyDetails.city);
                objUser.PredefinedColumn6 = Sanitizer.GetSafeHtmlFragment(".");
                objUser.PredefinedColumn7 = Sanitizer.GetSafeHtmlFragment(objUserRepoData._embedded.company.zipcode.id);
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    objUser.PredefinedColumn8 = Convert.ToString(lstCountry.FirstOrDefault(x => x.CountryName == objCountryDetails.name).CountryId);
                }
                objUser.CountryName = objCountryDetails.name;
                objUser.PredefinedColumn9 = Sanitizer.GetSafeHtmlFragment(objUserRepoData.phone);
                objUser.IPAddress = GlobalFunctions.GetIpAddress();

                objUser.DefaultCustId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId; //1281850; // objStore.StoreCurrencies[i].DefaultCompanyId;
                objUser.SourceCodeId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).SourceCodeId; //5281; //objStore.StoreCurrencies[i].SourceCodeId;
                Exceptions.WriteInfoLog("Before calling RegisterCustomer_OASIS.BuildCustContact_OASIS()");
                intCustomerID_OASIS = RegisterCustomer_OASIS.BuildCustContact_OASIS(objUser);
                if (intCustomerID_OASIS > 0)
                {
                    Exceptions.WriteInfoLog("Before calling UserBL.ExecuteBASYSDetails()");
                    UserBL.ExecuteBASYSDetails(Constants.USP_InsertUserBASYSDetails, true, objUser.UserId, CurrencyIdx, intCustomerID_OASIS);
                    Exceptions.WriteInfoLog("After calling UserBL.ExecuteBASYSDetails()");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindResourceData()
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                lstStoreDetail = StoreBL.GetStoreDetails();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strInvalidUserErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SSO_InvalidUser_Error_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }

    public class UserRepoData
    {
        public string username { get; set; }
        public string id { get; set; }
        public string locale { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string status { get; set; }
        public string vat { get; set; }
        public string company_name { get; set; }
        public string street { get; set; }
        public string town { get; set; }
        public string mobile { get; set; }
        public string fax { get; set; }
        public zipcode zipcode { get; set; }
        public _links _links { get; set; }
        public _embedded _embedded { get; set; }
        public Int16 gender { get; set; }

    }

    public class CountryDetails
    {
        public string two_letters_code { get; set; }
        public string three_letters_code { get; set; }
        public string name { get; set; }
    }

    public class CompanyDetails
    {
        public string id { get; set; }
        public string name { get; set; }
        public string city { get; set; }
        public string street { get; set; }
    }

    public class UserRepoToken
    {
        public string application;
        public string username;
        public string token;
    }

    public class zipcode
    {
        public string id { get; set; }
    }

    public class _links
    {
        public country country { get; set; }
        public company company { get; set; }
    }

    public class _embedded
    {
        public company company { get; set; }

    }

    public class country
    {
        public string href { get; set; }
    }

    public class company
    {
        public string href { get; set; }
        public string name { get; set; }
        public zipcode zipcode { get; set; }
    }
}