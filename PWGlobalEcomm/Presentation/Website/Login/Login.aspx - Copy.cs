﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessEntity;
using System.Web.UI.HtmlControls;
using System.IO;

namespace Presentation
{
    public class Login_UserLogin : BasePage
    {
        #region Controls
        protected global::System.Web.UI.WebControls.TextBox txtUserName, txtPassword, txtCaptcha, txtEmailSubscription;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCaptcha, dvCaptcha1, divCurrencyArea, divDropDownCurrency, dvCurrencySel, collapseCountry, collapseCurrency, divCurrencyLandingImg, divCountryLandingImg, dvCountry, Login_Currency_heading, Login_Country_heading;
        protected global::System.Web.UI.WebControls.Label lblMsg, lblCurrency;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden Hidden1;
        protected global::System.Web.UI.WebControls.Literal ltrwelcomehead1, ltrwelcomehead2, ltrwelcomehead3, ltrSelectCurrency, ltrSubscibemsg1, ltrSubscibemsg2;
        protected global::System.Web.UI.WebControls.DropDownList ddlCurrency;
        protected global::System.Web.UI.WebControls.Repeater repaterCurrency;
        protected global::System.Web.UI.WebControls.LinkButton lnkChangeCurrency, lnkChangeDestion;
        protected global::System.Web.UI.WebControls.Image imgLandingCurrency, imgLandingCountry;
        protected global::System.Web.UI.WebControls.DropDownList ddlCountry;
        protected bool IsShowCurrency, IsShowCountry;
        protected global::System.Web.UI.WebControls.Button btnSubmit, btnSaveEmailSubscription; 

        #endregion

        #region variables
        int intLanguageId;
        String strCurrencySymbol = "";
        int LoginWrongAttempts = Convert.ToInt16(ConfigurationManager.AppSettings["LoginWrongAttempts"]);
        int LoginWaitTime = Convert.ToInt16(ConfigurationManager.AppSettings["LoginWaitTime"]);
        public string host = GlobalFunctions.GetVirtualPath();
        public int curryInt = 0;

        /*Sachin Chauhan Start : 08 02 2016 : Added more language specific label titles*/
        public string SignIn_Selected, SignIn_ChangeCurrency, SignIn_ChangeCurrencyBtn, SignIn_ReceiveCommunication, SignIn_ForgotPassword;
        public string SignIn_Register, SignIn_Password, SignIn_Email, SignIn_Login, SignIn_Title, SignIn_VerificationCode, SignIn_YourEmailAddress, SignIn_SelectCurrencyTitle,
            Heading1, Heading2, Heading3;

        public string Change_Destination;
        /*Sachin Chauhan End : 08 02 2016 */

        #endregion
        StoreBE lstStoreDetail = StoreBL.GetStoreDetails();
        string LoginType = Convert.ToString(ConfigurationManager.AppSettings["LoginType"]);
        string SplashPageUrl = Convert.ToString(ConfigurationManager.AppSettings["SplashPageUrl"]);

        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 28-07-16
        /// Scope   : Page Load Function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindResourceData();
                if (LoginType == "Country")
                    IsShowCountry = true;
                else
                    IsShowCurrency = true;

                #region "Added by Sripal User from Registeration Page"
                if (Session["RegisterPg"] != null && Session["userReg"] != null & Session["UserName"] != null)
                {
                    UserBE objUserBE = new UserBE();
                    objUserBE = Session["User"] as UserBE;

                    UserBE objBEs = new UserBE();
                    UserBE objUser = new UserBE();
                    objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["UserName"]));
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                    if (objUser != null || objUser.EmailId != null)
                    {
                        UpdateBasketSessionProducts(objUser, 'Y');
                        Session["User"] = objUser;
                        Session["CaptchaText"] = null;
                        /*Sachin Chauhan Start : 08 02 2016 : Added below condition if Currency Id & Symbol is null then only set */
                        if (GlobalFunctions.GetCurrencyId().Equals(0) && GlobalFunctions.GetCurrencySymbol().Equals(String.Empty))
                        {
                            GlobalFunctions.SetCurrencyId(Convert.ToInt16(Session["activeCurrencyId"]));
                            GlobalFunctions.SetCurrencySymbol(lblCurrency.Text);
                        }
                        Session["activeCurrencyId"] = null;
                        Session["IsAnonymous"] = "0";
                        Response.RedirectToRoute("index");
                        return;
                    }
                }
                #endregion

                #region "Comments"
                /*Sachin Chauhan Start : 02 01 2016 : Changing behavior for Anonymous user*/
                /*if (Session["User"] != null)
                {
                    UserBE user = Session["User"] as UserBE;
                    if (user.IsGuestUser == false)
                        Response.RedirectToRoute("index");
                }*/

                //if (Session["IsAnonymous"] == null)
                //{
                //    /*Sachin Chauhan : 26 02 2016 : Removed below condition check for current & previous currency change which automatically displays */

                //    if ( Session["PrevCurrencyId"] != null )
                //    {
                //        if ( !Session["IsAnonymous"].ToString().Equals("0") && Session["User"] == null )  //&& !Session["PrevCurrencyId"].To_Int16().Equals(GlobalFunctions.GetCurrencyId()) )
                //        {
                //            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                //        }
                //    }
                //    //if ((!Session["IsAnonymous"].ToString().Equals("1") && ) ) //&& !Session["PrevCurrencyId"].To_Int16().Equals(GlobalFunctions.GetCurrencyId()) )
                //    //{
                //    //    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                //    //}
                //}
                /*Sachin Chauhan End : 02 01 2016*/

                /*Sachin Chauhan : 27 02 2016 : Added below condition if user clicks on login link for multi currency anonymous site very first time w/o changing currency*/
                /*Sachin Chauhan End : 02 01 2016*/

                #endregion

                #region "LandingImage"
                string[] filePaths = Directory.GetFiles(Server.MapPath("~/Images/LandingPage"));
                foreach (string strfile in filePaths)
                {
                    
                    if (strfile.Length > 0)
                    {
                        if (Path.GetExtension(strfile) == ".jpg" || Path.GetExtension(strfile) == ".jpeg" || Path.GetExtension(strfile) == ".png" || Path.GetExtension(strfile) == ".gif")
                        {
                            //if (Path.GetFileName(strfile) == ("LandingPage" + Convert.ToString(Session["LanguageId"]) + Path.GetExtension(strfile)))
                                if (IsShowCountry)
                                {   // imgLandingCountry.ImageUrl = host + "Images/LandingPage/LandingPage" + Convert.ToString(Session["LanguageId"]) + Path.GetExtension(strfile) + "?" + Guid.NewGuid();
                                    imgLandingCountry.ImageUrl = host + "Images/LandingPage/" + Path.GetFileNameWithoutExtension(strfile) + Path.GetExtension(strfile) + "?" + Guid.NewGuid();
                                }
                                else
                                {
                                    // imgLandingCurrency.ImageUrl = host + "Images/LandingPage/LandingPage" + Convert.ToString(Session["LanguageId"]) + Path.GetExtension(strfile) + "?" + Guid.NewGuid();
                                    imgLandingCurrency.ImageUrl =  host +  "Images/LandingPage/" + Path.GetFileNameWithoutExtension(strfile)+Path.GetExtension(strfile) + "?" + Guid.NewGuid();
                                }
                        }
                    }
                }
                #endregion


                if (!IsPostBack)
                {
                    ReadMetaTagsData();
                    ShowCurrencyorCountry();
                    //  ShowLoginModalSetting(lstStoreDetail.StoreCurrencies.FindAll(x => x.IsActive == true));
                    
                    if (Session["IsAnonymous"] != null)
                    {
                        if (Session["IsAnonymous"].ToString().Equals("1"))
                        {
                        }
                        else
                        {
                            #region 2016_03_15
                            //if (!Session["IsAnonymous"].ToString().Equals("0") && Session["User"] != null)
                            //{
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                            //}
                            //else
                            //{
                            //    Response.RedirectToRoute("Index");
                            //} 
                            if (Session["IsAnonymous"].ToString().Equals("0"))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                            }
                            #endregion
                        }
                    }
                    else
                    {

                    }

                }
                else
                {

                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                }
                

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }



        /// <summary>
        /// Author  : Prajwal Hegde
        /// Date    : 28-07-16
        /// Scope   : Submit Button Function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                curryInt = 1;
                if (dvCaptcha.Visible)
                {
                    string theCode = Session["CaptchaText"].ToString();
                    if (Sanitizer.GetSafeHtmlFragment(txtCaptcha.Text.ToLower()) != theCode.ToLower())
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("Login/Captcha"), AlertType.Warning);
                        txtCaptcha.Text = "";
                        return;
                    }
                }
                UserBE objUser = new UserBE();
               // string pass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(txtPassword.Text.Trim()), "SHA512", null);
                string pass = SaltHash.ComputeHash((txtPassword.Text.Trim()), "SHA512", null);
                UserBE objBE = new UserBE();
                objBE.EmailId = Sanitizer.GetSafeHtmlFragment(txtUserName.Text.Trim());
                objBE.Password = pass;
                objBE.IPAddress = GlobalFunctions.GetIpAddress();
                objBE.CurrencyId = GlobalFunctions.GetCurrencyId(); //Hardcode get currencyId selected from splash page

                int waitTime = LoginWaitTime;
                //StoreBE objStore = StoreBL.GetStoreDetails();
                //if (objStore != null)
                //{ waitTime = objStore.StoreFeatures.Where(x => x.FeatureName.ToLower() == "cr_resetpasswordblockaccountduration").To_Int16(); }

                int validateUser = UserBL.ExecuteLoginDetails(Constants.USP_ValidateLoginDetails, true, objBE, "StoreCustomer", LoginWrongAttempts, waitTime);
                if (validateUser == 0 || validateUser == 2) // -- User does not exist || -- Incorrect user password
                {
                    dvCaptcha.Visible = true;
                    dvCaptcha1.Visible = true;
                    txtCaptcha.Text = "";
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("Login/UserCheck"), AlertType.Warning);
                    return;
                }
                /*User Type*/
                else if (validateUser == 4)
                {
                    #region /*User Type*/
                    intLanguageId = GlobalFunctions.GetLanguageId();
                    List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                        if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_invalid_catalogue_access").ResourceValue, AlertType.Warning);
                            return;
                        }
                    } 
                    #endregion
                }
                else if (validateUser == 3) // -- User blocked 
                {
                    dvCaptcha.Visible = true;
                    dvCaptcha1.Visible = true;
                    txtCaptcha.Text = "";
                    string _exp = Exceptions.GetException("Login/UserBlock").Replace("~block~", Convert.ToString(LoginWaitTime));
                    GlobalFunctions.ShowModalAlertMessages(this.Page, _exp, AlertType.Warning);
                    return;
                }
                else if (validateUser == 1) // -- Success
                {
                    UserBE objBEs = new UserBE();
                    objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(txtUserName.Text.Trim());
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    if (IsShowCountry)
                    {
                        if (Convert.ToInt16(ddlCountry.SelectedValue) > 0)
                            objBEs.CountryId = Convert.ToInt16(ddlCountry.SelectedValue);
                    }
                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                    //objUser = UserBL.GetSingleObjectDetails("USP_GetLoginDetails1", true, objBEs);
                    if (objUser != null || objUser.EmailId != null)
                    {
                        UpdateBasketSessionProducts(objUser, 'Y');
                        Session["User"] = objUser;
                        Session["CaptchaText"] = null;
                        /*Sachin Chauhan Start : 08 02 2016 : Added below condition if Currency Id & Symbol is null then only set */
                        if (GlobalFunctions.GetCurrencyId().Equals(0) && GlobalFunctions.GetCurrencySymbol().Equals(String.Empty))
                        {
                            GlobalFunctions.SetCurrencyId(Convert.ToInt16(Session["activeCurrencyId"]));
                            GlobalFunctions.SetCurrencySymbol(lblCurrency.Text);
                        }
                        else if(IsShowCountry)
                        {
                            SetCulture(objUser);
                        }
                        /*Sachin Chauhan End : 08 02 2016*/
                        Session["activeCurrencyId"] = null;
                        Session["IsAnonymous"] = "0";
                        Response.RedirectToRoute("index");
                        // SetCulture();
                        //Session["IsCountry"] = "1";
                        //Response.Redirect(host + "MyAccount/Profile.aspx", true);
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void SetCulture(UserBE objUserBE)
        {
            GlobalFunctions.SetLanguageId(objUserBE.LanguageId);
            GlobalFunctions.SetCurrencyId(objUserBE.CurrencyId);
            GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUserBE.CurrencySymbol));
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 20-10-15
        /// Scope   : Update basket session data
        /// </summary>
        /// <returns></returns>
        public static void UpdateBasketSessionProducts(UserBE objUserBE, char Action)
        {
            try
            {
                List<object> lstShoppingBE = new List<object>();
                lstShoppingBE = ShoppingCartBL.GetBasketProductsData(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), 0, HttpContext.Current.Session.SessionID);
                if (Action != 'Y')
                {
                    ShoppingCartBL.UpdateBasketSessionProducts(objUserBE.UserId, "");
                }
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    List<ShoppingCartBE> lstNewShoppingCart = new List<ShoppingCartBE>();
                    lstNewShoppingCart = lstShoppingBE.Cast<ShoppingCartBE>().ToList();
                    for (int i = 0; i < lstNewShoppingCart.Count; i++)
                    {
                        Dictionary<string, string> dictionary = new Dictionary<string, string>();
                        dictionary.Add("UserId", Convert.ToString(objUserBE.UserId));
                        dictionary.Add("ProductSKUId", Convert.ToString(lstNewShoppingCart[i].ProductSKUId));
                        dictionary.Add("Quantity", Convert.ToString(lstNewShoppingCart[i].Quantity));
                        dictionary.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                        dictionary.Add("UserSessionId", "");
                        int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                    }
                    ShoppingCartBL.UpdateBasketSessionProducts(0, HttpContext.Current.Session.SessionID);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-15
        /// Scope   : Bind BindCountryDropDown
        /// </summary>            
        /// <returns></returns>
        protected void BindCountryDropDown()
        {
            try
            {
                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                lstCountry = lstCountry.FindAll(x => x.IsShow == true);
                ddlCountry.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlCountry.AppendDataBoundItems = true;
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    // lstCountry = lstCountry.FindAll(x => x.RegionCode != "UA");
                    ddlCountry.DataTextField = "CountryName";
                    ddlCountry.DataValueField = "CountryId";
                    ddlCountry.DataSource = lstCountry;
                    ddlCountry.DataBind();
                }
                ddlCountry.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 20-10-15
        /// Scope   : SignIn of user
        /// </summary>
        /// <param name="strEmail"></param>
        /// <param name="strPassword"></param>
        /// <returns name="string"></returns>
        [System.Web.Services.WebMethod]
        public static string CheckLogin(string strEmail, string strPassword)
        {
            string strMessage = "fail";
            try
            {
                UserBE objUser = new UserBE();
                string pass = SaltHash.ComputeHash(Sanitizer.GetSafeHtmlFragment(strPassword.Trim()), "SHA512", null);
                UserBE objBE = new UserBE();
                objBE.EmailId = Sanitizer.GetSafeHtmlFragment(strEmail.Trim());
                objBE.Password = pass;
                objBE.IPAddress = GlobalFunctions.GetIpAddress();
                objBE.CurrencyId = GlobalFunctions.GetCurrencyId();

                int validateUser = UserBL.ExecuteLoginDetails(Constants.USP_ValidateLoginDetails, true, objBE, "StoreCustomer", 0, 0);
                if (validateUser == 1) // -- Success
                {
                    UserBE objBEs = new UserBE();
                    objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(strEmail.Trim());
                    objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                    if (objUser != null || objUser.EmailId != null)
                    {
                        UpdateBasketSessionProducts(objUser, 'Y');
                        HttpContext.Current.Session["User"] = objUser;
                        strMessage = "success";
                    }
                }
                return strMessage;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "fail";
            }
        }

        protected void BindCurrencyDropDown(List<StoreBE.StoreCurrencyBE> Storecurrency)
        {
            ddlCurrency.DataSource = Storecurrency;
            ddlCurrency.DataValueField = "CurrencyId";
            ddlCurrency.DataTextField = "CurrencyName";
            ddlCurrency.DataBind();
            ddlCurrency.Items.Insert(0, SignIn_SelectCurrencyTitle);
            /*if (GlobalFunctions.GetCurrencyId() != 0 || GlobalFunctions.GetCurrencyId() != null)
                ddlCurrency.SelectedValue = GlobalFunctions.GetCurrencyId().ToString(); 2016_03_15*/

        }

        protected void ShowLoginModalSetting(List<StoreBE.StoreCurrencyBE> Storecurrency)
        {
            try
            {
                if (Storecurrency.Count > 3)/*&& !Page.IsPostBack2016_03_15*/
                {
                    divDropDownCurrency.Visible = true;
                    //divDropDownCurrency.Attributes.Add("style", "display:block");
                    divCurrencyArea.Visible = false;
                    BindCurrencyDropDown(Storecurrency);
                }
                else
                {
                    //collapseCurrency.Attributes.Add("style", "diplay:block");
                   // collapseCountry.Visible = true;
                    repaterCurrency.DataSource = Storecurrency;
                    repaterCurrency.DataBind();
                    divCurrencyArea.Visible = true;
                    //divDropDownCurrency.Attributes.Add("style", "display:none");
                    divDropDownCurrency.Visible = false;
                    //divDropDownCurrency.Visible = false;

                }

                #region "Added by Sripal"
                if (Storecurrency.Count == 1)
                {
                    string currencyid = Convert.ToString(Storecurrency[0].CurrencyId);
                    string currencysymbol = Storecurrency[0].CurrencySymbol.Trim();
                    //lblCurrency.Text = currencysymbol.Trim().ToString();

                    /*Sachin Chauhan Start : 08 02 2016 : Set below session variable only if currency id & symbol is not present in session*/
                    if (GlobalFunctions.GetCurrencyId() == null && GlobalFunctions.GetCurrencySymbol() == null)
                        Session["activeCurrencyId"] = currencyid;
                    /*Sachin Chauhan End : 08 02 2016*/

                    lnkChangeCurrency.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                    dvCurrencySel.Visible = false;
                }
                else
                {
                    lnkChangeCurrency.Visible = true;
                }
                #endregion

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindResourceData()
        {
            intLanguageId = GlobalFunctions.GetLanguageId();
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        //ltrwelcomehead1.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_WelComehead1").ResourceValue;
                        //ltrwelcomehead2.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_WelComehead2").ResourceValue;
                        //ltrwelcomehead3.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_WelComehead3").ResourceValue;
                        Heading1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_WelComehead1").ResourceValue;
                        Heading2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_WelComehead2").ResourceValue;
                        Heading3 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_WelComehead3").ResourceValue;

                        ltrSelectCurrency.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_SelectCurrency").ResourceValue;
                        ltrSubscibemsg1.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_subscribemsg1").ResourceValue;
                        ltrSubscibemsg2.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_subscribemsg2").ResourceValue;

                        /*Sachin Chauhan Start : 08 02 2016 : Added more language specific label titles*/
                        SignIn_Title = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Title").ResourceValue;
                        SignIn_Login = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Login").ResourceValue;
                        SignIn_Email = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Email").ResourceValue;
                        SignIn_Password = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Password").ResourceValue;
                        SignIn_Register = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SingIn_Register").ResourceValue;
                        SignIn_ForgotPassword = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_ForgotPassword").ResourceValue;
                        SignIn_ReceiveCommunication = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_ReceiveCommunication").ResourceValue;
                        if (LoginType == "Country")
                            SignIn_ChangeCurrency = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Selected").ResourceValue.Replace("X", lstStoreDetail.StoreName);
                        else
                            SignIn_ChangeCurrency = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Selected").ResourceValue.Replace("X", GlobalFunctions.GetCurrencySymbol());
                        SignIn_ChangeCurrencyBtn = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_ChangeCurrencyBtn").ResourceValue.Replace("X", GlobalFunctions.GetCurrencySymbol());

                        SignIn_Login = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Login").ResourceValue;
                        SignIn_VerificationCode = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_VerificationCode").ResourceValue;
                        SignIn_YourEmailAddress = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_YourEmailAddress").ResourceValue;

                        /*Sachin Chauhan Start : 27 02 2016 : Added Currecy drop down select currency language specific title*/
                        SignIn_SelectCurrencyTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_SelectCurrencyTitle").ResourceValue;
                        /*Sachin Chauhn End : 27 02 2016*/

                        txtUserName.Attributes.Add("placeholder", SignIn_Email);
                        txtPassword.Attributes.Add("placeholder", SignIn_Password);
                        txtEmailSubscription.Attributes.Add("placeholder", SignIn_YourEmailAddress);
                        btnSubmit.Text = SignIn_Login;
                        btnSaveEmailSubscription.Text = SignIn_ReceiveCommunication;


                        Change_Destination = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Change_Destination").ResourceValue.Replace("X", GlobalFunctions.GetCurrencySymbol());
                        /*Sachin Chauhan End : 08 02 2016*/
                    }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lnkShowLoginModal_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                string[] commandArgs = lnk.CommandArgument.ToString().Split(new char[] { ',' });
                string currencyid = commandArgs[0];
                string currencysymbol = commandArgs[1];
                //lblCurrency.Text = currencysymbol.Trim().ToString();
                Session["activeCurrencyId"] = currencyid;

                /*Sachin Chauhan Start : 08 02 2016 : Setting selected currency id & symbol in required session variables*/
                // Save currency id as previous variable for some future reference 
                Session["PrevCurrencyId"] = GlobalFunctions.GetCurrencyId();

                GlobalFunctions.SetCurrencyId(currencyid.To_Int16());
                GlobalFunctions.SetCurrencySymbol(currencysymbol);

                /*Sachin Chauhan Emd : 08 02 2016*/

                /*Sachin Chauhan Start : 02 01 16 : Chnaged behavior for not showing login panel. Anonymous user can browse currency post currency selection*/
                StoreBE lstStoreDetail = (StoreBE)HttpRuntime.Cache["StoreDetails"];

                //List<StoreBE.StoreCurrencyBE> Storecurrency = lstStoreDetail.StoreCurrencies.FindAll(x => x.IsActive == true);
                string strStoreAccess = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CR_StoreAccess").FeatureValues.FirstOrDefault(y => y.IsEnabled == true).FeatureValue;
                if (lstStoreDetail.StoreCurrencies.Count == 1 && strStoreAccess.ToLower().Equals("mandatory login"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                }
                else
                {
                    Session["IsAnonymous"] = 0;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                    //Response.RedirectToRoute("Index");
                }
                BindResourceData();

                //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                /*Sachin Chauhan End : 02 01 16*/
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void lnkChangeCurrency_Click(object sender, EventArgs e)
        {
            try
            {
                //List<StoreBE.StoreCurrencyBE> Storecurrency = Cache["CacheStorecurrency"] as List<StoreBE.StoreCurrencyBE>;

                StoreBE CurrentStore = (StoreBE)HttpRuntime.Cache["StoreDetails"];
                ShowLoginModalSetting(CurrentStore.StoreCurrencies.Where(x => x.IsActive.Equals(true)).ToList<StoreBE.StoreCurrencyBE>());
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_currency();", true);

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //List<StoreBE.StoreCurrencyBE> Storecurrency = Cache["CacheStorecurrency"] as List<StoreBE.StoreCurrencyBE>;
                int Currencyid = Convert.ToInt16(ddlCurrency.SelectedValue);

                /*Sachin Chauhan Start : 26 02 2016 : Changed below logic of creating overhead objects */
                StoreBE CurrentStore = (StoreBE)HttpRuntime.Cache["StoreDetails"];
                strCurrencySymbol = Convert.ToString(CurrentStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId.ToString().Equals(ddlCurrency.SelectedValue)).CurrencySymbol);
                /*Sachin Chauhan End : 26 02 2016*/
                //lblCurrency.Text = strCurrencySymbol.Trim().ToString();
                ShowLoginModalSetting(CurrentStore.StoreCurrencies);
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_login();", true);
                Session["activeCurrencyId"] = Currencyid;
                /*Sachin Chauhan Start : 08 02 2016 : Set currency id & symbol when selected from drop down*/

                /*Sachin Chauhan Start : 27 02 2016 */
                Session["PrevCurrencyId"] = GlobalFunctions.GetCurrencyId();
                /*Sachin Chauhan End : 27 02 2016*/

                GlobalFunctions.SetCurrencyId(Currencyid.To_Int16());
                GlobalFunctions.SetCurrencySymbol(strCurrencySymbol.Trim());
                Session["IsAnonymous"] = 0;
                //Response.RedirectToRoute("Index");2016_03_15
                BindResourceData();
                /*Sachin Chauhan End : 08 02 2016 */
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnSaveEmailSubscription_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    UserBE objBE = new UserBE();
                    objBE.EmailId = txtEmailSubscription.Text.ToString();
                    bool res = UserBL.AddSubscriptionDetail("USP_InserSubscriberData", objBE);
                    if (res)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/SubscriptionAdded"), AlertType.Success);
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("FailureMessage/UserExists"), AlertType.Failure);
                    }
                    List<StoreBE.StoreCurrencyBE> Storecurrency = Cache["CacheStorecurrency"] as List<StoreBE.StoreCurrencyBE>;
                    ShowLoginModalSetting(Storecurrency);
                }
                catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            }

        }


        protected void ShowCurrencyorCountry()
        {
            if (IsShowCountry)
            {
                dvCountry.Visible = true;
                lnkChangeDestion.Attributes.Add("style", "display:inline-block");
                //Login_Country_heading.Visible = true;
                //Login_Currency_heading.Visible = false;
                ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:show_login();</script>");
                BindCountryDropDown();
            }
            else
            {
                dvCountry.Visible = false;
                StoreBE CurrentStore = (StoreBE)HttpRuntime.Cache["StoreDetails"];
                ShowLoginModalSetting(CurrentStore.StoreCurrencies.Where(x => x.IsActive.Equals(true)).ToList<StoreBE.StoreCurrencyBE>());
                //Login_Country_heading.Visible = false;
                //Login_Currency_heading.Visible = true;
                divCurrencyLandingImg.Visible = true;
                lnkChangeCurrency.Attributes.Add("style", "display:inline-block");
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript: show_currency();", true);
            }
        }

        protected void lnkChangeDestion_Click(object sender, EventArgs e)
        {
            Response.Redirect("" + SplashPageUrl + "");
        }

        //protected void repaterCurrency_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    Literal ltrcurrency = (Literal)e.Item.FindControl("ltrcurrency");
        //    //HtmlGenericControl divDynamicCurrency = (HtmlGenericControl)e.Item.FindControl("divDynamicCurrency");
        //    //divDynamicCurrency.Attributes.Add("class", "col-xs-12 col-md-4");
        //    //lblCurrency.Text.Trim();
        //}

        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "SignIn");
                    if (lstMetaTags.Count > 0)
                    {
                        Page.Title = lstMetaTags[0].MetaContentTitle;
                        Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                        Page.MetaDescription = lstMetaTags[0].MetaDescription;
                    }
                    else
                    {
                        Page.Title = "";
                        Page.MetaKeywords = "";
                        Page.MetaDescription = "";
                    }
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}