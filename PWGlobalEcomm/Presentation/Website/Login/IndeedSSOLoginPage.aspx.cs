﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System.Net;
using System.Text;
using System.IO;

namespace Presentation
{
    public partial class Login_IndeedSSOLoginPage : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.TextBox txtEmailAddress;
        protected global::System.Web.UI.WebControls.DropDownList ddlCountry;
        protected global::System.Web.UI.WebControls.Button btnLogin;
        protected global::System.Web.UI.WebControls.Label lblEmail, lblCountry;
        public string SignIn_Email, SignIn_Login, strVaildEmailErrorMsg, Email_Empty;
        public Int16 intLanguageId;
        StoreBE ObjStoreBE = new StoreBE();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDropdownList();
                BindResourceData();
                ObjStoreBE = StoreBL.GetStoreDetails();
                if (ObjStoreBE.StoreId==519)
                {
                    ddlCountry.Visible = false;
                }
            }
        }
        protected void BindDropdownList()
        {
            try
            {
                ddlCountry.DataSource = CountryBL.GetAllCountryCurrencyMappings();
                ddlCountry.DataValueField = "Country_ISO_Code";
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataBind();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName;
                String sURL;
                UserBE.BA_USERFILE ObjUserBE = new UserBE.BA_USERFILE();
                ObjUserBE = UserBL.GetUserDetailsByEmailID(txtEmailAddress.Text);
                if (ObjUserBE == null)
                {
                    GlobalFunctions.ShowModalAlertMessages(this, "Unsuccessful Login", AlertType.Warning);
                }
                else
                {
                    List<CountryBE.CountryCurrencyMapping> ObjCountryBE = new List<CountryBE.CountryCurrencyMapping>();
                    ObjCountryBE = CountryBL.GetAllCountryCurrencyMappings();
                    Session["SSOXMLTransfer"] = 1;
                    ObjStoreBE = StoreBL.GetStoreDetails();
                    if (ObjStoreBE.StoreId==519)
                    {
                        fileName = Server.MapPath("~/SSO/IndeedEmployeeStoreSSO.xml");
                        sURL = GlobalFunctions.GetVirtualPath() + "login/IndeedEmployeeStoreSSO.aspx";
                        String xmlData = GetTextFromXMLFile(fileName);
                        string cXML_Form = "";
                        cXML_Form = CreateCxmlForm(sURL, xmlData);
                        cXML_Form = cXML_Form.Replace("@eid", ObjUserBE.Employee_ID);
                        cXML_Form = cXML_Form.Replace("@Country_ISO_Code", ddlCountry.SelectedValue);
                        Response.Clear();
                        Response.Write(cXML_Form);
                    }
                    else if(ObjStoreBE.StoreId==532)
                    {
                        fileName = Server.MapPath("~/SSO/INDEED_SSO.xml");
                        sURL = GlobalFunctions.GetVirtualPath() + "login/IndeedSSO.aspx";
                        String xmlData = GetTextFromXMLFile(fileName);
                        string cXML_Form = "";
                        cXML_Form = CreateCxmlForm(sURL, xmlData);
                        cXML_Form = cXML_Form.Replace("@eid", ObjUserBE.Employee_ID);
                        cXML_Form = cXML_Form.Replace("@Country_ISO_Code", ddlCountry.SelectedValue);
                        Response.Clear();
                        Response.Write(cXML_Form);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        public static string CreateCxmlForm(String sURL, String sValue)
        {
            StringBuilder cXML_Form = new StringBuilder();
            try
            {
                string strReturn = Microsoft.VisualBasic.Constants.vbCrLf;
                cXML_Form.Append("<!DOCTYPE html>" + strReturn);
                cXML_Form.Append("<html lang='en'>" + strReturn);
                cXML_Form.Append("<HEAD>" + strReturn);
                cXML_Form.Append("<script type='text/javascript' language='javascript'>" + strReturn);
                cXML_Form.Append(" function SubmitForm(){" + strReturn);
                cXML_Form.Append(" document.FormCXML.submit();" + strReturn);
                cXML_Form.Append(" }" + strReturn + strReturn);
                cXML_Form.Append(" </script>" + strReturn);
                cXML_Form.Append("</HEAD>" + strReturn + strReturn + strReturn);
                cXML_Form.Append("<BODY onload='SubmitForm();'> " + strReturn);
                cXML_Form.Append("<FORM method='post'" + " name='FormCXML'" + " action ='" + sURL + "'>");
                cXML_Form.Append("<textarea name='cXML-urlencoded' style='display:none'>" + sValue + "</textarea>");
                cXML_Form.Append("</FORM>" + strReturn + strReturn + strReturn);
                cXML_Form.Append("</BODY>" + strReturn);
                cXML_Form.Append("</HTML>" + strReturn);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return cXML_Form.ToString();
        }
            private string GetTextFromXMLFile(string file)
        {
            try
            {
                StreamReader reader = new StreamReader(file);
                string ret = reader.ReadToEnd();
                reader.Close();
                return ret;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return "";
            }
        }

        protected void BindResourceData()
        {
            intLanguageId = GlobalFunctions.GetLanguageId();
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        lblEmail.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Email").ResourceValue;
                        SignIn_Login = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Login").ResourceValue;
                        strVaildEmailErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ValidEmail_Message").ResourceValue;
                        Email_Empty = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Email_Empty").ResourceValue;
                        btnLogin.Text = SignIn_Login;
                        lblCountry.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Country_Text").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}