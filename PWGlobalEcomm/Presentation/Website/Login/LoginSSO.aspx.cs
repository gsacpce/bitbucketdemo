﻿using System;
using System.Configuration;
using System.Text;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Login_LoginSSO : System.Web.UI.Page
    {
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl plcHolder;

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder lstSB = new StringBuilder();
            string strClientID =Convert.ToString(ConfigurationManager.AppSettings["SSOClientID"]);//. "763601821182-ptceqkedrnjbta1flo9tuv8bmsht7ejs.apps.googleusercontent.com";

            lstSB.Append("<button class='g-signin' ");
            lstSB.Append(" data-scope='https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile' ");
            lstSB.Append(" data-clientid='" + strClientID + "' ");
            lstSB.Append(" data-accesstype='offline'  ");
            lstSB.Append(" data-callback='onSignInCallback' ");
            lstSB.Append(" data-theme='dark' ");
            lstSB.Append(" data-cookiepolicy='single_host_origin' ");
            lstSB.Append(" d='google.com'> ");
            lstSB.Append(" </button>");
            plcHolder.InnerHtml = Convert.ToString(lstSB);
            //     <button class="g-signin"
            //    data-scope="https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"
            //    data-clientid="763601821182-ptceqkedrnjbta1flo9tuv8bmsht7ejs.apps.googleusercontent.com"
            //    data-accesstype="offline"
            //    data-callback="onSignInCallback"
            //    data-theme="dark"
            //    data-cookiepolicy="single_host_origin"
            //    hd="google.com">
            //</button>
        }
    }
}