﻿using Presentation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using OneLogin.Saml;
using System.Xml;
using PWGlobalEcomm.BusinessLogic;
using Microsoft.Security.Application;
using PWGlobalEcomm.DataAccess;
using PWGlobalEcomm.GlobalUtilities;
using System.Net;
using OneLogin.Saml;

namespace Presentation
{
    public class Login_MaerskSSOLogin : BasePage
    {
        AccountSettings accountSettings = new AccountSettings();
        string MaerskSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_SSOLogs";
        Int16 iUserTypeID = 1;
        string tenantid, objectidentifier, displayname, identityprovider, givenname, surname, emailaddress, name, locality, countrycode, jobtitle, postalcode, streetaddress, employeeid, businessunitname, CostCentre, contractingcompany, strInvalidUserErrorMsg = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            Exceptions.WriteSSOLog("---INSIDE PAGE LOAD OF MAERSK SSO LOGIN--- ", MaerskSSOLogFolderPath);
            SetupRequest();
        }

        public void SetupRequest()
        {
            try
            {
                Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST  MAERSK SSO LOGIN PAGE--- ", MaerskSSOLogFolderPath);
                string Domain = Request.QueryString["Domain"];
                string cXMLSetupRequest = "";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;

                if (!String.IsNullOrEmpty(Domain))
                {
                    Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST  POWERWEAVE USER--- ", MaerskSSOLogFolderPath);
                    StreamReader fper = default(StreamReader);
                    fper = File.OpenText(Server.MapPath("~/SSO/ExampleMaerskBrandSSO1.xml"));
                    cXMLSetupRequest = fper.ReadToEnd();
                    
                    xmlDoc.LoadXml(cXMLSetupRequest);
                }
                else
                {
                    Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST LIVE USER--- ", MaerskSSOLogFolderPath);
                    cXMLSetupRequest = ReturnStream();
                    Exceptions.WriteSSOLog("---OUTPUT OF RETURN STREAM LIVE USER--- " + cXMLSetupRequest, MaerskSSOLogFolderPath);

                    int startIndex = cXMLSetupRequest.IndexOf("SAMLResponse=") + "SAMLResponse".Length;
                    Exceptions.WriteSSOLog("startIndex of SAMLResponse = " + startIndex, MaerskSSOLogFolderPath);
                                        
                    int endIndex = cXMLSetupRequest.Length;
                    Exceptions.WriteSSOLog("endIndex of SAMLResponse= " + endIndex, MaerskSSOLogFolderPath);

                    cXMLSetupRequest = cXMLSetupRequest.Substring(startIndex, endIndex - startIndex);
                    cXMLSetupRequest = cXMLSetupRequest.TrimStart('=');
                    Exceptions.WriteSSOLog("Actual SAMLResponse required : " + cXMLSetupRequest, MaerskSSOLogFolderPath);

                    if (cXMLSetupRequest.Contains("%"))
                    {
                        cXMLSetupRequest = HttpUtility.UrlDecode(cXMLSetupRequest);
                        cXMLSetupRequest = cXMLSetupRequest.Replace("%0", "");
                    }

                    OneLogin.Saml.Response samlResponse = new Response(accountSettings);
                    samlResponse.LoadXmlFromBase64(cXMLSetupRequest);
                                        
                    System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();

                    xmlDoc.LoadXml(enc.GetString(Convert.FromBase64String(cXMLSetupRequest)));

                    Exceptions.WriteSSOLog("SAML XML : " + Convert.ToString(xmlDoc.InnerXml), MaerskSSOLogFolderPath);                 
                    xmlDoc.LoadXml(enc.GetString(Convert.FromBase64String(cXMLSetupRequest)));
                }
                               
                XmlElement Nodes = xmlDoc["samlp:Response"]["Assertion"]["AttributeStatement"]["Attribute"];

                try
                {
                    if (Nodes.HasAttributes)
                    {
                        tenantid = Nodes.InnerText;
                        Exceptions.WriteSSOLog("tenantid : " + tenantid, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.HasChildNodes)
                    {
                        objectidentifier = Nodes.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("objectidentifier : " + objectidentifier, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.HasChildNodes)
                    {
                        displayname = Nodes.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("displayname : " + displayname, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        identityprovider = Nodes.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("identityprovider : " + identityprovider, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        givenname = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("givenname : " + givenname, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        surname = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("surname : " + surname, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        emailaddress = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("emailaddress : " + emailaddress, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        name = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("name : " + name, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        locality = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("locality : " + locality, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        countrycode = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("countrycode : " + countrycode, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        jobtitle = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("jobtitle : " + jobtitle, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        postalcode = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("postalcode : " + postalcode, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        streetaddress = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("streetaddress : " + streetaddress, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        employeeid = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("employeeid : " + employeeid, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        businessunitname = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("businessunitname : " + businessunitname, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        CostCentre = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("CostCentre : " + CostCentre, MaerskSSOLogFolderPath);
                    }
                    if (Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.HasChildNodes)
                    {
                        contractingcompany = Nodes.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.InnerText;
                        Exceptions.WriteSSOLog("contractingcompany : " + contractingcompany, MaerskSSOLogFolderPath);
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteSSOLog("Error while fetching SSO user details", MaerskSSOLogFolderPath);
                    Exceptions.WriteExceptionLog(ex);
                }
                
                try
                {
                    #region Code Added by SHRIGANESH SINGH for Filetring SSO User Emailid as per Store setting 06 Jan 2017

                    bool bDomainWhitelist = false, bDomainBlackList = false, bEmailWhiteList = false, bEmailBlackList = false;
                    UserRegistrationBE objUserRegistrationBE = new UserRegistrationBE();
                    List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();

                    StoreBE getStores;
                    if (HttpRuntime.Cache["StoreDetails"] == null)
                    {
                        getStores = StoreDA.getItem(Constants.USP_GetAllStoreDetails, null, true);
                        HttpRuntime.Cache.Insert("StoreDetails", getStores);
                    }
                    else
                    {
                        getStores = (StoreBE)HttpRuntime.Cache["StoreDetails"];
                    }

                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();

                    string Register_GuestCheckout_Error1_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_GuestCheckout_Error1_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    string Register_GuestCheckout_Error2_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_GuestCheckout_Error2_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    strInvalidUserErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SSO_InvalidUser_Error_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                    Exceptions.WriteInfoLog("Registration:Filtering starts");
                    if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "whitelist").IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Registration:Filtering bDomainWhitelist set true");
                        bDomainWhitelist = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "blacklist").IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Registration:Filtering bDomainBlackList set true");
                        bDomainBlackList = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "whitelist").IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Registration:Filtering bEmailWhiteList set true");
                        bEmailWhiteList = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "blacklist").IsEnabled)
                    {
                        Exceptions.WriteInfoLog("Registration:Filtering bEmailBlackList set true");
                        bEmailBlackList = true;
                    }
                    Exceptions.WriteInfoLog("Registration:before Registeration details");
                    objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                    Exceptions.WriteInfoLog("Registration:after Registeration details");
                    string strUEmail = Sanitizer.GetSafeHtmlFragment(emailaddress);
                    string strDomain = strUEmail.Substring(strUEmail.LastIndexOf("@") + 1);

                    List<UserRegistrationBE.RegistrationFilterBE> objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                    List<UserRegistrationBE.RegistrationFilterBE> objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                    UserRegistrationBE.RegistrationFilterBE objRegistrationWhiteListDomainFilterBE = null;
                    UserRegistrationBE.RegistrationFilterBE objRegistrationBlackListDomainFilterBE = null;
                    UserRegistrationBE.RegistrationFilterBE objRegistrationWhiteListEmailFilterBE = null;
                    UserRegistrationBE.RegistrationFilterBE objRegistrationBlackListEmailFilterBE = null;

                    if (bDomainWhitelist)
                    {
                        objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                        objRegistrationWhiteListDomainFilterBE = objRegistrationWhiteListFilterBELst.FirstOrDefault(x => x.Domain.ToLower() == strDomain.ToLower());
                    }
                    if (bDomainBlackList)
                    {
                        objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                        objRegistrationBlackListDomainFilterBE = objRegistrationBlackListFilterBELst.FirstOrDefault(x => x.Domain.ToLower() == strDomain.ToLower());
                    }

                    if (bEmailWhiteList)
                    {
                        objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                        objRegistrationWhiteListEmailFilterBE = objRegistrationWhiteListFilterBELst.FirstOrDefault(x => x.EmailId.ToLower() == strUEmail.ToLower());
                    }
                    if (bEmailBlackList)
                    {
                        objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                        objRegistrationBlackListEmailFilterBE = objRegistrationBlackListFilterBELst.FirstOrDefault(x => x.EmailId.ToLower() == strUEmail.ToLower());
                    }

                    if (bDomainWhitelist && bEmailWhiteList)
                    {
                        if (objRegistrationWhiteListDomainFilterBE == null && objRegistrationWhiteListEmailFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainWhitelist && !bEmailWhiteList && !bEmailBlackList)
                    {
                        if (objRegistrationWhiteListDomainFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainBlackList && !bEmailWhiteList && !bEmailBlackList)
                    {
                        if (objRegistrationBlackListDomainFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bEmailWhiteList && !bDomainBlackList && !bDomainWhitelist)
                    {
                        if (objRegistrationWhiteListEmailFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bEmailBlackList && !bDomainBlackList && !bDomainWhitelist)
                    {
                        if (objRegistrationBlackListEmailFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainWhitelist && bEmailBlackList)
                    {
                        if (objRegistrationBlackListEmailFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                        else if (objRegistrationWhiteListDomainFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainBlackList && bEmailBlackList)
                    {
                        if (objRegistrationBlackListDomainFilterBE != null && objRegistrationBlackListEmailFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                        else if (objRegistrationBlackListDomainFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }

                        else if (objRegistrationBlackListEmailFilterBE != null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    else if (bDomainBlackList && bEmailWhiteList)
                    {
                        if (objRegistrationBlackListDomainFilterBE != null && objRegistrationWhiteListEmailFilterBE == null)
                        {
                            string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        }
                    }
                    Exceptions.WriteInfoLog("Registration:Filtering end");
                    #endregion

                    //emailaddress = "shriganesh.singh1@powerweave.com";
                    //Session["SSOUser_FirstName"] = givenname;
                    //Session["SSOUser_LastName"] = surname;
                    //Session["SSOUser_EmailID"] = emailaddress;
                    Session["Maersk_SSO_MGIS_User"] = true;

                    string strMessage = string.Empty;
                    int i = 0;
                    strMessage = Register_GuestCheckout_Error1_Message; //"There is some problem in guest user checkout. Please try again.";

                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();

                    i = UserBL.ChkUserExists(Constants.USP_CheckUserExists, true, Sanitizer.GetSafeHtmlFragment(emailaddress.Trim()));
                    if (i == 0)
                    {
                        UserBE objBE = new UserBE();
                        UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                        objBE.EmailId = emailaddress;
                        objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);
                        objBE.FirstName = givenname;
                        objBE.LastName = surname;
                        objBE.PredefinedColumn1 = displayname;
                        objBE.PredefinedColumn2 = contractingcompany;
                        objBE.PredefinedColumn3 = streetaddress;
                        objBE.PredefinedColumn4 = "";
                        objBE.PredefinedColumn5 = locality;
                        objBE.PredefinedColumn6 = "";
                        objBE.PredefinedColumn7 = postalcode;

                        if (lstCountry != null && lstCountry.Count > 0)
                        {
                            objBE.PredefinedColumn8 = Convert.ToString(lstCountry.FirstOrDefault(x => x.CountryCode == countrycode).CountryId);
                        }

                        objBE.PredefinedColumn9 = "";
                        objBE.UserDeliveryAddress.Add(obj);
                        objBE.IPAddress = GlobalFunctions.GetIpAddress();
                        objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objBE.IsGuestUser = true;
                        objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objBE.IsMarketing = false;
                        objBE.UserTypeID = iUserTypeID;// Added by SHRIGANESH 22 Sept 2016   
                        objBE.Points = -1; // Added by ShriGanesh 26 April 2017    
                        Exceptions.WriteSSOLog("Before calling UserBL.ExecuteRegisterDetails in SetUpRequest", MaerskSSOLogFolderPath);
                        i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                        Exceptions.WriteSSOLog("After calling UserBL.ExecuteRegisterDetails in SetUpRequest", MaerskSSOLogFolderPath);
                        objBE.UserId = Convert.ToInt16(i);

                        UserBE objBEs = new UserBE();
                        UserBE objUser = new UserBE();
                        objBEs.EmailId = emailaddress.Trim();
                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        if (objUser != null || objUser.EmailId != null)
                        {
                            Session["User"] = objUser;
                            GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                            GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                            Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", MaerskSSOLogFolderPath);
                            Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                            Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", MaerskSSOLogFolderPath);
                            Session["GuestUser"] = null;
                            Exceptions.WriteSSOLog("Before redirecting to INDEX for IF from SetUpRequest", MaerskSSOLogFolderPath);
                            Response.RedirectToRoute("index");
                        }
                    }
                    else if (i == -2 || i == -1)
                    {
                        UserBE objBEs = new UserBE();
                        UserBE objUser = new UserBE();
                        objBEs.EmailId = emailaddress.Trim();
                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objBEs.FirstName = givenname;
                        objBEs.LastName = surname;
                        objBEs.PredefinedColumn1 = displayname;
                        objBEs.PredefinedColumn2 = contractingcompany;
                        objBEs.PredefinedColumn3 = streetaddress;
                        objBEs.PredefinedColumn4 = "";
                        objBEs.PredefinedColumn5 = locality;
                        objBEs.PredefinedColumn6 = "";
                        objBEs.PredefinedColumn7 = postalcode;

                        if (lstCountry != null && lstCountry.Count > 0)
                        {
                            objBEs.PredefinedColumn8 = Convert.ToString(lstCountry.FirstOrDefault(x => x.CountryCode == countrycode).CountryId);
                        }

                        objBEs.PredefinedColumn9 = "";

                        int j = UserBL.ExecuteSSOProfileDetails(Constants.USP_UpdateSSOUserProfileDetails, true, objBEs);
                        if (j == 1)
                        {
                            objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                            if (objUser != null || objUser.EmailId != null)
                            {
                                Session["User"] = objUser;
                                GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                                GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                                Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", MaerskSSOLogFolderPath);
                                Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", MaerskSSOLogFolderPath);
                                Session["GuestUser"] = null;
                                Exceptions.WriteSSOLog("Before redirecting to INDEX for ELSE from SetUpRequest", MaerskSSOLogFolderPath);
                                Response.RedirectToRoute("index");
                            }
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
                        }
                    }
                    else
                    {
                        //General error
                        strMessage = Register_GuestCheckout_Error1_Message; //"There is some problem in guest user checkout. Please try again.";
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
                    }
                }
                catch (Exception Ex)
                {
                    Exceptions.WriteExceptionLog(Ex);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
            }
        }

        public string ReturnStream()
        {
            try
            {
                Exceptions.WriteSSOLog("ReturnStream Method call : ", MaerskSSOLogFolderPath);
                Stream str = default(Stream);
                //StreamReader strrdr = new StreamReader();
                string strmContents = null;

                //int counter = 0;
                int strLen = 0;
                int strRead = 0;

                // Create a Stream object. 
                str = Request.InputStream;

                using (StreamReader responseReader = new StreamReader(str))
                {
                    strmContents = responseReader.ReadToEnd();
                    Exceptions.WriteSSOLog("Output of Input Stream = " + strmContents, MaerskSSOLogFolderPath);
                }

                #region COMMENTED CODE BY SHRIGANESH 12 JAN 2017
                //// Find number of bytes in stream. 
                //strLen = (int)str.Length;
                //Exceptions.WriteSSOLog("length of Input Stream = " + Convert.ToString(strLen), MaerskSSOLogFolderPath);

                //// Create a byte array. 
                //byte[] strArr = new byte[strLen + 1];
                //// Read stream into byte array. 
                //strRead = str.Read(strArr, 0, strLen);

                //strmContents = System.Text.Encoding.UTF8.GetString(strArr);
                //Exceptions.WriteInfoLog((strmContents)); 
                #endregion

                return strmContents;
            }
            catch (Exception ex)
            {
                Exceptions.WriteSSOLog("Error in ReturnStream : ", MaerskSSOLogFolderPath);
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);                
            }
            return "blank";
        }
    }
}