﻿using Microsoft.Security.Application;
using OneLogin.Saml;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.DataAccess;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Presentation
{
    public partial class Login_IndeedSSO : BasePage
    {
        string INDEEDSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\IndeedBrandStore_SSOLogs";
        string Employee_ID, EmailId, Ship_To_Location, strInvalidUserErrorMsg, strSAMLPostMessage = "";
        string User_View;
        AccountSettings accountSettings = new AccountSettings();
        UserBE.BA_USERFILE objUserFile = new UserBE.BA_USERFILE();
        protected void Page_Load(object sender, EventArgs e)
        {
            Exceptions.WriteSSOLog("---INSIDE PAGE LOAD OF INDEED_SSOLogs LOGIN--- ", INDEEDSSOLogFolderPath);
            SetupRequest();
        }

        public void SetupRequest()
        {
            try
            {
                Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST INDEED_SSOLogs LOGIN PAGE--- ", INDEEDSSOLogFolderPath);
                string Domain = Request.QueryString["Domain"];
                string cXMLSetupRequest = "";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                List<CountryBE.CountryCurrencyMapping> lstCountryCurrencyMapping = new List<CountryBE.CountryCurrencyMapping>();

                string Register_GuestCheckout_Error1_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_GuestCheckout_Error1_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                string Register_GuestCheckout_Error2_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_GuestCheckout_Error2_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                strInvalidUserErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SSO_InvalidUser_Error_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                Exceptions.WriteSSOLog("AFTER BINDING RESOURCES", INDEEDSSOLogFolderPath);

                if (!String.IsNullOrEmpty(Domain))
                {
                    Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST  POWERWEAVE USER--- ", INDEEDSSOLogFolderPath);
                    StreamReader fper = default(StreamReader);
                    fper = File.OpenText(Server.MapPath("~/SSO/INDEED_SSO.xml"));
                    cXMLSetupRequest = fper.ReadToEnd();
                    xmlDoc.LoadXml(cXMLSetupRequest);
                    strSAMLPostMessage = cXMLSetupRequest;
                }
                else if (Session["SSOXMLTransfer"] != null)
                {
                    Stream str = default(Stream);
                    string strmContents = null;
                    int strLen = 0;
                    int strRead = 0;
                    str = Request.InputStream;
                    strLen = (int)str.Length;
                    byte[] strArr = new byte[strLen + 1];
                    strRead = str.Read(strArr, 0, strLen);
                    strmContents = System.Text.Encoding.UTF8.GetString(strArr);
                    strmContents = strmContents.Remove(0, 16);
                    strSAMLPostMessage = Server.UrlDecode(strmContents);
                    xmlDoc.LoadXml(strSAMLPostMessage);
                }
                else
                {
                    Exceptions.WriteSSOLog("---INSIDE SETUPREQUEST LIVE USER--- ", INDEEDSSOLogFolderPath);
                    cXMLSetupRequest = ReturnStream();
                    Exceptions.WriteSSOLog("---OUTPUT OF RETURN STREAM LIVE USER--- " + cXMLSetupRequest, INDEEDSSOLogFolderPath);
                    int startIndex = cXMLSetupRequest.IndexOf("SAMLResponse=") + "SAMLResponse".Length;
                    Exceptions.WriteSSOLog("startIndex of SAMLResponse = " + startIndex, INDEEDSSOLogFolderPath);
                    int endIndex = cXMLSetupRequest.IndexOf("&RelayState");
                    Exceptions.WriteSSOLog("endIndex of SAMLResponse= " + endIndex, INDEEDSSOLogFolderPath);
                    cXMLSetupRequest = cXMLSetupRequest.Substring(startIndex, endIndex - startIndex);
                    cXMLSetupRequest = cXMLSetupRequest.TrimStart('=');
                    Exceptions.WriteSSOLog("Actual SAMLResponse required : " + cXMLSetupRequest, INDEEDSSOLogFolderPath);
                    if (cXMLSetupRequest.Contains("%"))
                    {
                        Exceptions.WriteSSOLog("Before HttpUtility.UrlDecode " + endIndex, INDEEDSSOLogFolderPath);
                        cXMLSetupRequest = HttpUtility.UrlDecode(cXMLSetupRequest);
                        cXMLSetupRequest = cXMLSetupRequest.Replace("%0", "");
                        Exceptions.WriteSSOLog("cXMLSetupRequest after Decode " + cXMLSetupRequest, INDEEDSSOLogFolderPath);
                    }
                    Exceptions.WriteSSOLog("Before samlResponse " + cXMLSetupRequest, INDEEDSSOLogFolderPath);
                    OneLogin.Saml.Response samlResponse = new Response(accountSettings);
                    Exceptions.WriteSSOLog("Before LoadXmlFromBase64 " + cXMLSetupRequest, INDEEDSSOLogFolderPath);
                    samlResponse.LoadXmlFromBase64(cXMLSetupRequest);
                    Exceptions.WriteSSOLog("Before enc " + cXMLSetupRequest, INDEEDSSOLogFolderPath);
                    System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
                    Exceptions.WriteSSOLog("Before LoadXml " + cXMLSetupRequest, INDEEDSSOLogFolderPath);
                    xmlDoc.LoadXml(enc.GetString(Convert.FromBase64String(cXMLSetupRequest)));
                    Exceptions.WriteSSOLog("SAML XML : " + Convert.ToString(xmlDoc.InnerXml), INDEEDSSOLogFolderPath);
                    xmlDoc.LoadXml(enc.GetString(Convert.FromBase64String(cXMLSetupRequest)));
                    Exceptions.WriteSSOLog("AFTER LoadXml " + cXMLSetupRequest, INDEEDSSOLogFolderPath);
                    strSAMLPostMessage = cXMLSetupRequest;
                }

                XmlElement Nodes = xmlDoc["samlp:Response"]["Assertion"]["AttributeStatement"]["Attribute"];
                try
                {
                    if (Nodes.HasAttributes)
                    {
                        Employee_ID = Nodes.InnerText;
                        Exceptions.WriteSSOLog("Employee_ID : " + Employee_ID, INDEEDSSOLogFolderPath);
                        bool bresult = UserBL.InsertUpdateEmployeeSAML(Employee_ID, strSAMLPostMessage);
                        if (bresult)
                        {
                            Exceptions.WriteSSOLog("InsertUpdateEmployeeSAML Successfully", INDEEDSSOLogFolderPath);
                        }
                        else
                        {
                            Exceptions.WriteSSOLog("Error while InsertUpdateEmployeeSAML", INDEEDSSOLogFolderPath);
                        }
                    }
                    if (Nodes.NextSibling.HasChildNodes)
                    {
                        Ship_To_Location = Nodes.NextSibling.InnerText;
                        lstCountryCurrencyMapping = CountryBL.GetAllCountryCurrencyMapping(Ship_To_Location);
                        Session["IndeedInvoiceAccointID"] = lstCountryCurrencyMapping.FirstOrDefault(x => x.IsActive = true).InvoiceAccountID;
                        Session["Ship_To_Location"] = lstCountryCurrencyMapping.FirstOrDefault(x => x.IsActive = true).Country_ISO_Code;
                        Exceptions.WriteSSOLog("Ship_To_Location : " + Convert.ToString(Session["Ship_To_Location"]), INDEEDSSOLogFolderPath);
                        Exceptions.WriteSSOLog("IndeedInvoiceAccointID : " + Convert.ToString(Session["IndeedInvoiceAccointID"]), INDEEDSSOLogFolderPath);
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteSSOLog("Error while fetching SSO user details", INDEEDSSOLogFolderPath);
                    Exceptions.WriteExceptionLog(ex);
                }
                try
                {
                    Exceptions.WriteSSOLog("Inside TRY", INDEEDSSOLogFolderPath);
                    HttpContext.Current.Session["IndeedSSO_User"] = true;
                    Exceptions.WriteSSOLog("IndeedSSO_User : " + Convert.ToString(Session["IndeedSSO_User"]), INDEEDSSOLogFolderPath);
                    string strMessage = string.Empty;
                    int UserExistInBASYS, UserExistInStore;
                    strMessage = Register_GuestCheckout_Error1_Message; //"There is some problem in guest user checkout. Please try again.";

                    List<CountryBE> lstCountry = new List<CountryBE>();
                    lstCountry = CountryBL.GetAllCountries();

                    List<CurrencyBE> lstCurrencies = new List<CurrencyBE>();
                    lstCurrencies = CurrencyBL.GetAllCurrencyDetails();
                    Exceptions.WriteSSOLog("BEFORE CALLING UserBL.ChkEmployeeExists", INDEEDSSOLogFolderPath);
                    // Here We will check whether coming Emp ID is BA_USERFILE  or not
                    UserExistInBASYS = UserBL.ChkEmployeeExists(Constants.USP_CheckEmployeeExists, true, Sanitizer.GetSafeHtmlFragment(Employee_ID.Trim()));
                    Exceptions.WriteSSOLog("AFTER CALLING UserBL.ChkEmployeeExists", INDEEDSSOLogFolderPath);
                    // IF EXIST IN BA_USERFILE
                    if (UserExistInBASYS == 0)
                    {
                        Exceptions.WriteSSOLog("INSIDE IF UserExistInBASYS == 0", INDEEDSSOLogFolderPath);

                        UserBE objBE = new UserBE();
                        UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();

                        Exceptions.WriteSSOLog("BEFORE CALLING UserBL.GetIndeedSSOUserDetails", INDEEDSSOLogFolderPath);
                        // For Below Method, the Stored Procedure is not present in the database
                        objUserFile = UserBL.GetIndeedSSOUserDetails(Employee_ID);
                        Exceptions.WriteSSOLog("AFTER CALLING UserBL.GetIndeedSSOUserDetails", INDEEDSSOLogFolderPath);
                        // Here We will check whether coming Emp ID is Tbl_User  or not in CustomColumn2
                        // Once we get all the User Related data in the objUserFile from above, we can use the email id to pass in the below method
                        Exceptions.WriteSSOLog("BEFORE CALLING UserBL.ChkUserExists", INDEEDSSOLogFolderPath);
                        UserExistInStore = UserBL.ChkUserExists(Constants.USP_CheckUserExistInCustonColumn, true, Sanitizer.GetSafeHtmlFragment(objUserFile.Work_Email.Trim()));
                        Exceptions.WriteSSOLog("AFTER CALLING UserBL.ChkUserExists", INDEEDSSOLogFolderPath);

                        Exceptions.WriteSSOLog("AFTER CALLING GetAllCountryCurrencyMapping", INDEEDSSOLogFolderPath);
                        Session["UserCatalogueID"] = lstCountryCurrencyMapping.FirstOrDefault(x => x.IsActive = true).CatalogueID;

                        var objCur = lstCountryCurrencyMapping.FirstOrDefault(y => y.CatalogueID == Convert.ToInt16(Session["UserCatalogueID"].ToString()));
                        GlobalFunctions.SetCurrencyId(objCur.CurrencyID);
                        GlobalFunctions.SetCurrencySymbol(Convert.ToString(objCur.CurrencySymbol));

                        Exceptions.WriteSSOLog("AFTER CREATING UserCatalogueID Session", INDEEDSSOLogFolderPath);
                        if (UserExistInStore == 0)
                        {
                            #region USER NOT REGISTERED IN OUR SYSTEM
                            Exceptions.WriteSSOLog("INSIDE IF UserExistInStore == 0", INDEEDSSOLogFolderPath);
                            if (objUserFile != null)
                            {
                                Exceptions.WriteSSOLog("INSIDE objUserFile != null", INDEEDSSOLogFolderPath);
                                objBE.EmailId = objUserFile.Work_Email;
                                objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);
                                objBE.FirstName = objUserFile.First_Name;
                                objBE.LastName = objUserFile.Last_Name;
                                objBE.PredefinedColumn1 = objUserFile.First_Name + objUserFile.Last_Name;
                                objBE.PredefinedColumn2 = "Indeed";
                                objBE.PredefinedColumn3 = objUserFile.Department;
                                objBE.PredefinedColumn4 = objUserFile.Address_Line_1;
                                objBE.CustomColumn30 = objUserFile.Address_Line_3;
                                objBE.PredefinedColumn5 = objUserFile.Address_City;
                                objBE.PredefinedColumn6 = objUserFile.Address_State;
                                objBE.PredefinedColumn7 = objUserFile.Address_Zip;
                            }

                            if (lstCountry != null && lstCountry.Count > 0)
                            {
                                objBE.PredefinedColumn8 = Convert.ToString(lstCountry.FirstOrDefault(x => x.CountryCode == Session["Ship_To_Location"].ToString()).CountryId);
                            }
                            objBE.PredefinedColumn9 = objUserFile.Work_PhoneNumber;

                            obj.AddressTitle = "Default";
                            obj.PreDefinedColumn1 = objBE.PredefinedColumn1;
                            obj.PreDefinedColumn2 = objBE.PredefinedColumn2;
                            obj.PreDefinedColumn3 = objBE.PredefinedColumn3;
                            obj.PreDefinedColumn4 = objBE.PredefinedColumn4;
                            obj.PreDefinedColumn5 = objBE.PredefinedColumn5;
                            obj.PreDefinedColumn6 = objBE.PredefinedColumn6;
                            obj.PreDefinedColumn7 = objBE.PredefinedColumn7;
                            obj.PreDefinedColumn8 = objBE.PredefinedColumn8;
                            obj.PreDefinedColumn9 = objBE.PredefinedColumn9;
                            obj.IsDefault = true;

                            objBE.UserDeliveryAddress.Add(obj);
                            objBE.IPAddress = GlobalFunctions.GetIpAddress();
                            objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                            objBE.IsGuestUser = false;
                            objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                            objBE.IsMarketing = false;

                            User_View = objUserFile.User_View;
                            switch (User_View)
                            {
                                case "1":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "2":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "3":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "4":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "5":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "6":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                default:
                                    objBE.UserTypeID = 1;
                                    break;
                            }

                            objBE.CustomColumn2 = objUserFile.Employee_ID; // This field is again Hardcoded as Custom column 2 will always be set up as Employee_ID for This Store
                            Exceptions.WriteSSOLog("Before calling UserBL.ExecuteRegisterDetails in SetUpRequest", INDEEDSSOLogFolderPath);
                            int i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                            Exceptions.WriteSSOLog("After calling UserBL.ExecuteRegisterDetails in SetUpRequest", INDEEDSSOLogFolderPath);
                            objBE.UserId = Convert.ToInt16(i);

                            if (i > 0)
                            {
                                Session["CaptchaText"] = null;
                                Exceptions.WriteInfoLog("Registration:before BASYS insert.");
                                InsertBASYSId(Convert.ToInt16(GlobalFunctions.GetCurrencyId()), objUserFile.Work_Email.Trim(), Convert.ToString(Session["IndeedInvoiceAccointID"]));
                                Exceptions.WriteInfoLog("Registration:after BASYS insert.");
                            }

                            UserBE objBEs = new UserBE();
                            UserBE objUser = new UserBE();
                            objBEs.EmailId = objUserFile.Work_Email.Trim();
                            objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                         
                            objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();

                            objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                            if (objUser != null || objUser.EmailId != null)
                            {
                                Session["User"] = objUser;                                
                                Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", INDEEDSSOLogFolderPath);
                                Exceptions.WriteSSOLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SetUpRequest", INDEEDSSOLogFolderPath);
                                Session["GuestUser"] = null;
                                Exceptions.WriteSSOLog("Before redirecting to INDEX for IF from SetUpRequest", INDEEDSSOLogFolderPath);
                                Response.RedirectToRoute("index");
                            }
                            #endregion
                        }
                        else if (UserExistInStore == 1)
                        {
                            #region USER ALREADY REGISTERED ON OUR SYSTEM
                            Exceptions.WriteSSOLog("INSIDE IF UserExistInStore == 1", INDEEDSSOLogFolderPath);
                            User_View = objUserFile.User_View;
                            switch (User_View)
                            {
                                case "1":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "2":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "3":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "4":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "5":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                case "6":
                                    objBE.UserTypeID = lstCountryCurrencyMapping.FirstOrDefault(x => x.UserTypeID == Convert.ToInt16(User_View)).UserTypeID;
                                    break;

                                default:
                                    objBE.UserTypeID = 1;
                                    break;
                            }
                            objBE.EmailId = objUserFile.Work_Email;
                            Exceptions.WriteSSOLog("BEFORE UserBL.UpdateReferrelUserTypeId", INDEEDSSOLogFolderPath);
                            int i = UserBL.UpdateReferrelUserTypeId(Constants.USP_UpdateUserTypeForReferrelURL, true, objBE);
                            Exceptions.WriteSSOLog("AFTER UserBL.UpdateReferrelUserTypeId", INDEEDSSOLogFolderPath);

                            UserBE objBEs = new UserBE();
                            UserBE objUser = new UserBE();
                            objBEs.EmailId = objUserFile.Work_Email;
                            objBEs.LanguageId = GlobalFunctions.GetLanguageId();                                                      

                            objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                            Exceptions.WriteSSOLog("BEFORE UserBL.GetSingleObjectDetails", INDEEDSSOLogFolderPath);
                            objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                            objUser.PredefinedColumn8 = Convert.ToString(lstCountry.FirstOrDefault(x => x.CountryCode == Session["Ship_To_Location"].ToString()).CountryId);
                                                       
                            Exceptions.WriteSSOLog("AFTER UserBL.GetSingleObjectDetails", INDEEDSSOLogFolderPath);
                            if (objUser != null || objUser.EmailId != null)
                            {
                                Exceptions.WriteInfoLog("Before ");
                                if (string.IsNullOrEmpty(objUser.BASYS_CustomerContactId))
                                {
                                    InsertBASYSId(GlobalFunctions.GetCurrencyId(), objUser.EmailId, Convert.ToString(Session["IndeedInvoiceAccointID"]));
                                }
                                objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                                Session["GuestUser"] = null;
                                Session["User"] = objUser;   
                                Exceptions.WriteSSOLog("BEFORE calling Login_UserLogin.UpdateBasketSessionProducts()", INDEEDSSOLogFolderPath);
                                Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                Exceptions.WriteSSOLog("AFTER calling Login_UserLogin.UpdateBasketSessionProducts()", INDEEDSSOLogFolderPath);                                
                                UserBE lstUser = new UserBE();
                                lstUser = Session["User"] as UserBE;
                                Exceptions.WriteSSOLog("User Email = " + lstUser.EmailId, INDEEDSSOLogFolderPath);                                
                                Response.RedirectToRoute("index");
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        Exceptions.WriteSSOLog("INSIDE ELSE UserExistInBASYS == 0", INDEEDSSOLogFolderPath);
                        strMessage = Register_GuestCheckout_Error1_Message; //"There is some problem in guest user checkout. Please try again.";
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
                    }
                }
                catch (Exception Ex)
                {
                    Exceptions.WriteExceptionLog(Ex);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
            }
        }

        public string ReturnStream()
        {
            try
            {
                Exceptions.WriteSSOLog("ReturnStream Method call : ", INDEEDSSOLogFolderPath);
                Stream str = default(Stream);
                string strmContents = null;
                int strLen = 0;
                int strRead = 0;

                str = Request.InputStream;
                strLen = (int)str.Length;
                byte[] strArr = new byte[strLen + 1];
                strRead = str.Read(strArr, 0, strLen);

                strmContents = System.Text.Encoding.UTF8.GetString(strArr);
                return strmContents;
            }
            catch (Exception ex)
            {
                Exceptions.WriteSSOLog("Error in ReturnStream : ", INDEEDSSOLogFolderPath);
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidUserErrorMsg, AlertType.Warning);
            }
            return "blank";
        }

        private static void InsertBASYSId(Int16 CurrencyIdx, string strEmailId, string InvoiceAccountID)
        {
            try
            {
                StoreBE objStore = StoreBL.GetStoreDetails();
                int intCustomerID_OASIS;
                UserBE objUser = new UserBE();
                UserBE objBE = new UserBE();
                List<CurrencyBE> lstCurrencies = new List<CurrencyBE>();
                lstCurrencies = CurrencyBL.GetAllCurrencyDetails();
                objBE.EmailId = Sanitizer.GetSafeHtmlFragment(strEmailId.Trim());
                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBE.CurrencyId = Convert.ToInt16(CurrencyIdx);
                objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBE);
                int id = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId;
                objUser.DefaultCustId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId; //1248609;// 1281850; // objStore.StoreCurrencies[i].DefaultCompanyId;//Updated by Sripal for Gift Certificate
                objUser.SourceCodeId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).SourceCodeId; //7778;// 5281; //objStore.StoreCurrencies[i].SourceCodeId;//Updated by Sripal for Gift Certificate
                objUser.CurrencySymbol = lstCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).CurrencySymbol;
                objUser.CurrencyCode = lstCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).CurrencyCode;
                if (objUser.CurrencyCode.ToLower() == "gbp")
                {
                    objUser.CurrencyCode = "";
                }
                objUser.DivisionID = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DivisionId;
                intCustomerID_OASIS = RegisterCustomer_OASIS.BuildCreateCustomer_OASIS(objUser, InvoiceAccountID, "11");
                if (intCustomerID_OASIS > 0)
                {
                    int x = UserBL.ExecuteBASYSDetails(Constants.USP_InsertUserBASYSDetails, true, objUser.UserId, CurrencyIdx, intCustomerID_OASIS);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}