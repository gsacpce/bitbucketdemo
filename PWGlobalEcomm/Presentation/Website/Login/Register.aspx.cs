﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using Microsoft.Security.Application;
using System.Collections;
using System.Reflection;
using System.Net.Mail;

namespace Presentation
{
    public class Login_RegistrationNew : BasePage
    {
        #region "Controls"
        protected global::System.Web.UI.WebControls.Button btnConfirm;
        protected global::System.Web.UI.WebControls.Panel pnlUserType;/*User Type*/
        protected global::System.Web.UI.WebControls.CheckBox chkRegSameAsDelv;
        protected global::System.Web.UI.WebControls.CompareValidator cmpValidatorPwd;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl hMarketing_Preferences, headingUserType, divUserType/*User Type*/, divSocialLoginRegister, divRegister; //Added By Snehal 04 10 2016
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aLnkUserType, alnkMkt;/*User Type*/
        protected global::System.Web.UI.WebControls.LinkButton btnC1, btnC2, LinkButton1, btnC3, btnR1, lnkUserTypeCancel;/*User Type*/
        protected global::System.Web.UI.WebControls.HiddenField hdnPasswordPolicyType, hdnMinPasswordLength;
        protected global::System.Web.UI.WebControls.DropDownList ddlUserType, drpdwnPreferredLanguage, drpdwnPreferredCurrency;/*User Type*/
        protected global::System.Web.UI.WebControls.RadioButton rdProfile_MarketingOption1, rdProfile_MarketingOption2;
        protected global::System.Web.UI.WebControls.TextBox txtCaptcha, txtPassword, txtEmail, txtFirstName, txtLastName, txtConfirmPassword, txtTitles;
        protected global::System.Web.UI.WebControls.Label lblUserType, lblPwdLength, lblPasswordPolicyMessage, lblUserTypes, lblPreferredLanguage, lblPreferredCurrency;/*User Type*/
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl lblFirstName, lblLastName, lblEmail, lblPassword, lblConfirmPassword, lblGender, lblTitle, Register_PersonalDetails, subSection1, section2, section3, subSection3, loginInfo, sub1, sub2, lblVerification, divddlUserType;/*User Type*/
        protected global::System.Web.UI.WebControls.Repeater rptCustomFields, rptInvoiceAddress, rptDeliveryAddress, rptCustomFieldsAll, rptUserTypeCustomFields, rptCustomMapping; /*User Type*/
        protected global::System.Web.UI.WebControls.RequiredFieldValidator reqtxtFirstName, reqtxtLastName, reqtxtEmail, reqtxtPassword, reqtxtConfirmPassword, reqtxtCaptcha, reqddlUserType, reqtxtTitles; /*User Type*/
        protected global::System.Web.UI.WebControls.RegularExpressionValidator regtxtEmail, regtxtPassword, regtxtConfirmPassword, reg1txtPassword, reg1txtConfirmPassword,
            regexPwdLentxtPassword, regexAlphaNumtxtPassword, regexAlphaNumSymtxtPassword;
       
        #endregion
        #region "Variables"
        public string hostV = GlobalFunctions.GetVirtualPath();
        int intCurrencyId = 0, iCount = 0, i = 0;
        Int16 intFeatureID = 0, intLanguageId = 0, iUserTypeID = 1;/*User Type*/
        bool bDomainWhitelist, bDomainBlackList, bEmailWhiteList, bEmailBlackList = false, b = false;
        protected static string strRegister_txtSplChar_Message;
        string strFeatureValue = "", strRegCountryErrorMsg = "", strDelCountryErrorMsg = "", minPasswordLength = "0", host = "", CustomFieldValidationError = "";        
        StoreBE lstStoreDetail = new StoreBE();
        FeatureBE objFeatureBE = new FeatureBE();
        UserRegistrationBE objUserRegistrationBE = new UserRegistrationBE();
        UserTypeMappingBE objUserTypeMappingBE = new UserTypeMappingBE();
        UserTypeMappingBE objTempUserTypeMappingBE = new UserTypeMappingBE();
        UserRegistrationBE objTempUserRegistrationBE = new UserRegistrationBE();
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
        UserBE objTitles = new UserBE();
        List<UserRegistrationBE.RegistrationFieldsConfigurationBE> objRegistrationFieldsConfigurationBEL = new List<UserRegistrationBE.RegistrationFieldsConfigurationBE>();
        protected string strMinPassLenErrorMsg, Register_Confirm_Registration, Register_Reset_Button, Register_Cancel_Button, Register_Next_Button, Register_Country_Placeholder_2,
          Register_Country_Placeholder_1, JS_ConfirmMsg, Generic_Enter_Text, Generic_Select_Text, strGeneric_Existing_LoginFieldValue_MessagestrMinPassLenErrorMsg, strAlphaNumPassReqErrorMsg, strAlphaNumSymPassReqErrorMsg, strCustomFieldValidationError, strRegistrationDeclinedMessage;/*User Type*/
        #endregion
        public string strGeneric_Registration_Successfully_Msg, strGeneric_Registration_Failure_Msg, strGeneric_InvalidCaptchaText_Message, strGeneric_Email_Sending_Failed_Msg, strOrderDetails_Error_Processing_Registration_Error_Msg, strEmailPattern, strVaildEmailErrorMsg, strRegister_PreferredCurrency_Message, strRegister_PreferredLanguages_Message, strTitleReqMessage, strGeneric_Existing_LoginFieldValue_Message;
        protected global::System.Web.UI.WebControls.TextBox txt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {                
                HttpContext.Current.Session["GuestUser"] = null;//Guest User
                BindStoreFeatures();
                BindResourceData();

                #region Added by Snehal 04 10 2016 - To Check Either Social or store Login to make visible
                StoreBE objStoreBE = StoreBL.GetStoreDetails();

                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_RegisterLoginPanel").FeatureValues[0].IsEnabled && objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_RegisterLoginPanel").FeatureValues[1].IsEnabled) // To Check Social Media Login 
                {
                    divSocialLoginRegister.Visible = true;
                    divRegister.Visible = true;
                }

                else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_RegisterLoginPanel").FeatureValues[0].IsEnabled) // To Check Social Media Login 
                {
                    divSocialLoginRegister.Visible = true;
                    divRegister.Visible = false;
                }
                else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_RegisterLoginPanel").FeatureValues[1].IsEnabled) // TO Check Store Registration
                {
                    divSocialLoginRegister.Visible = false;
                    divRegister.Visible = true;
                }
                #endregion

                Session["User"] = null;
                if (Session["PrevLanguageId"] == null)
                {
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                }
                if (!Session["PrevLanguageId"].ToString().To_Int32().Equals(GlobalFunctions.GetLanguageId().To_Int32()))
                {
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();                    
                    PopulateForm();

                    if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_usertype").FeatureValues[0].IsEnabled == true)
                    {
                        BindUserTypeMappping();
                        BindCustomFields();
                    }
                }
                else
                {
                    if (!IsPostBack)
                    {
                        txtEmail.Attributes.Add("onblur", "javascript: ValidateEmail()");
                        PopulateForm();
                        ReadMetaTagsData();
                        BindPreferredLanguages();
                        BindPreferredCurrency(iUserTypeID);
                        #region  Added by SHRIGANESH SINGH to set Password Policy features 11 May 2016
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue != null)
                        {
                            minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                            hdnMinPasswordLength.Value = minPasswordLength;
                        }

                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                        {
                            hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].FeatureValue;
                        }
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                        {
                            hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].FeatureValue;
                        }
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                        {
                            hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].FeatureValue;
                        }
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_usertype").FeatureValues[0].IsEnabled == true)
                        {
                            BindUserTypeMappping();
                            BindUserSelection();                                                      
                            BindCustomMapping();                            
                        }
                        #endregion
                    }
                }
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_usertype").FeatureValues[0].IsEnabled == true)
                {
                    BindUserTypeMappping();
                    BindCustomFields();
                }                
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void BindStoreFeatures()
        {
            try
            {
                lstStoreDetail = StoreBL.GetStoreDetails();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void BindResourceData()
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        #region 1st Panel
                        strRegister_txtSplChar_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_txtSplChar_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_txtSplChar_Message = GlobalFunctions.ReplaceSingleQuote(strRegister_txtSplChar_Message);
                        subSection1.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Sub_Section_1" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblFirstName.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_FirstName_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblLastName.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_LastName_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblEmail.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblPassword.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Password_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblConfirmPassword.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_ConfirmPassword_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        loginInfo.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Information_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #region "Added By Hardik for Reg message on [11/11/2016] "
                        strGeneric_Registration_Successfully_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Registration_Successfully_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Registration_Failure_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Registration_Failure_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_InvalidCaptchaText_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_InvalidCaptchaText_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Email_Sending_Failed_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Email_Sending_Failed_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strOrderDetails_Error_Processing_Registration_Error_Msg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "OrderDetails_Error_Processing_Registration_Error_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        #region Added by Sripal "For Minimum password length"
                        string minPass = "0";
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue != null)
                        {
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 2a");
                            minPass = lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue;
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 3a");
                            Exceptions.WriteInfoLog("Registration:PS_MinimumPasswordLength -" + minPass);
                        }
                        else if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue != null)
                        {
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 2b");
                            minPass = lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue;
                            Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 3b");
                            Exceptions.WriteInfoLog("Registration:PS_MinimumPasswordLength -" + minPass);
                        }
                        #endregion                        
                        #region Code Added by SHRIGANESH SINGH 13 May 2016 for Password Policy

                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;

                            regexAlphaNumtxtPassword.Enabled = false;
                            regexAlphaNumSymtxtPassword.Enabled = false;

                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";

                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
                        }
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;
                            regexAlphaNumtxtPassword.Enabled = true;
                            regexAlphaNumSymtxtPassword.Enabled = false;
                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
                            regexAlphaNumtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                            regexAlphaNumtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        }
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                        {
                            regexPwdLentxtPassword.Enabled = true;
                            regexAlphaNumtxtPassword.Enabled = false;
                            regexAlphaNumSymtxtPassword.Enabled = true;
                            regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                            string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                            regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
                            regexAlphaNumSymtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                            regexAlphaNumSymtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        }
                        strMinPassLenErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strMinPassLenErrorMsg = strMinPassLenErrorMsg.Replace("@Num@", minPass);
                        strAlphaNumPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strAlphaNumSymPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        string strPwdLen = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Pwd_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblPwdLength.Text = strPwdLen.Replace("@NUM@", minPass);
                        #region "RequiredFieldValidators"
                        reqtxtFirstName.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_FirstName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtLastName.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_LastName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strVaildEmailErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ValidEmail_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_EnterPwd_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtConfirmPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_EnterConfirmPwd_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        cmpValidatorPwd.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_MisMatchPwd_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        #endregion
                        #region "2nd Panel"
                        section2.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Section_2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        sub1.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_RegisteredAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        sub2.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_DeliveryAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        chkRegSameAsDelv.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_SameAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegCountryErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegCountry_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strDelCountryErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelCountry_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        #region "3rd panel"
                        section3.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Section_3" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        subSection3.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Sub_Section_3" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblVerification.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_VerificationCode_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        reqtxtCaptcha.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_CaptchaCode_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        hMarketing_Preferences.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Marketing_Preferences" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        rdProfile_MarketingOption1.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_MarketingOption1" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        rdProfile_MarketingOption2.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_MarketingOption2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_Confirm_Registration = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Confirm_Registration" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_Reset_Button = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Reset_Button" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_Cancel_Button = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Cancel_Button" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_Next_Button = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Next_Button" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_Country_Placeholder_2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Country_Placeholder_2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_Country_Placeholder_1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Country_Placeholder_1" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Generic_Enter_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Generic_Select_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Select_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        JS_ConfirmMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "JS_ConfirmMsg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnC1.Attributes.Add("onclick", "return confirm('" + JS_ConfirmMsg + "')");
                        btnC1.Text = Register_Cancel_Button;
                        btnC2.Attributes.Add("onclick", "return confirm('" + JS_ConfirmMsg + "')");
                        btnC2.Text = Register_Cancel_Button;
                        btnC3.Text = Register_Cancel_Button;
                        btnC3.Attributes.Add("onclick", "return confirm('" + JS_ConfirmMsg + "')");
                        LinkButton1.Attributes.Add("click", "return confirm('" + JS_ConfirmMsg + "')");
                        LinkButton1.Text = Register_Cancel_Button;
                        btnR1.Text = Register_Reset_Button;
                        btnR1.Attributes.Add("onclick", "return confirm('" + JS_ConfirmMsg + "')");
                        lnkUserTypeCancel.Attributes.Add("click", "return confirm('" + JS_ConfirmMsg + "')");/*User Type*/
                        lnkUserTypeCancel.Text = Register_Cancel_Button;
                        btnConfirm.Text = Register_Confirm_Registration;
                        Register_PersonalDetails.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PersonalDetails" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #region "User Type /*User Type*/"
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_usertype").FeatureValues[0].IsEnabled == true)
                        {
                            lblUserTypes.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_usertype" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            headingUserType.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_usertype_heading" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            reqddlUserType.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_usertype_selection_error" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strCustomFieldValidationError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_invalidcustomfield_error_message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            strRegistrationDeclinedMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_registrationdeclined_message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        }
                        strGeneric_Existing_LoginFieldValue_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_existing_loginfieldvalue_message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        lblTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Reg_Titles" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblPreferredCurrency.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredCurrency_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblPreferredLanguage.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredLanguages_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_PreferredCurrency_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredCurrency_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_PreferredLanguages_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredLanguages_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strTitleReqMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ddlTitle_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtTitles.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ddlTitle_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;                        
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void PopulateForm()
        {
            try
            {
                objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();                
                #region "Invoice Address"
                objTempUserRegistrationBE.UserRegistrationLanguagesLst = objUserRegistrationBE.UserRegistrationLanguagesLst.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId <= 10);
                foreach (UserRegistrationBE.RegistrationLanguagesBE obj in objTempUserRegistrationBE.UserRegistrationLanguagesLst)
                {
                    UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                    objRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE);
                }
                rptInvoiceAddress.DataSource = objRegistrationFieldsConfigurationBEL;//.Skip(1);
                rptInvoiceAddress.DataBind();
                rptDeliveryAddress.DataSource = objRegistrationFieldsConfigurationBEL;//.Skip(1);
                rptDeliveryAddress.DataBind();
                #endregion
                #region "rptCustomFields"
                objRegistrationFieldsConfigurationBEL = new List<UserRegistrationBE.RegistrationFieldsConfigurationBE>();
                objTempUserRegistrationBE.UserRegistrationLanguagesLst = objUserRegistrationBE.UserRegistrationLanguagesLst.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId > 10);
                rptCustomFields.DataSource = objRegistrationFieldsConfigurationBEL;
                rptCustomFields.DataBind();
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptAddress_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Int16 iRegId = ((UserRegistrationBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                    UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);
                    UserRegistrationBE.RegistrationLanguagesBE objLang = objTempUserRegistrationBE.UserRegistrationLanguagesLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);
                    HiddenField hdfColumnName = (HiddenField)e.Item.FindControl("hdfColumnName");
                    Label lblRegistrationFieldName = (Label)e.Item.FindControl("lblRegistrationFieldName");
                    TextBox txtRegistrationFieldName = (TextBox)e.Item.FindControl("txtRegistrationFieldName");
                    DropDownList ddlRegisterCountry = (DropDownList)e.Item.FindControl("ddlRegisterCountry");
                    RequiredFieldValidator reqtxtRegistrationFieldName = (RequiredFieldValidator)e.Item.FindControl("reqtxtRegistrationFieldName");
                    RequiredFieldValidator reqddlRegisterCountry = (RequiredFieldValidator)e.Item.FindControl("reqddlRegisterCountry");
                    Label lblValidates = (Label)e.Item.FindControl("lblValidates");
                    if (txtRegistrationFieldName != null)
                    {
                        try
                        {
                            txtRegistrationFieldName.MaxLength = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId).MaxLength;
                        }
                        catch (Exception) { txtRegistrationFieldName.MaxLength = 300; }
                    }

                    if (hdfColumnName != null)
                    {
                        hdfColumnName.Value = objRegistrationFieldsConfigurationBE.SystemColumnName;
                    }
                    if (objRegistrationFieldsConfigurationBE.IsMandatory)
                    {
                        lblRegistrationFieldName.Text = "*" + objLang.LabelTitle;
                    }
                    else
                    {
                        lblRegistrationFieldName.Text = objLang.LabelTitle;
                    }
                    Repeater rpt = (Repeater)sender;
                    if (objRegistrationFieldsConfigurationBE.FieldType == 2)
                    {
                        ddlRegisterCountry.Visible = true;
                        ddlRegisterCountry.DataSource = objUserRegistrationBE.CountryLst.OrderBy(x => x.CountryName);
                        ddlRegisterCountry.DataTextField = "CountryName";
                        ddlRegisterCountry.DataValueField = "CountryId";
                        ddlRegisterCountry.DataBind();
                        if (rpt != null)
                        {
                            if (rpt.ID == "rptInvoiceAddress")
                            {
                                ddlRegisterCountry.Items.Insert(0, new ListItem(strRegCountryErrorMsg, "0"));
                            }
                            else
                            {
                                ddlRegisterCountry.Items.Insert(0, new ListItem(strDelCountryErrorMsg, "0"));
                            }
                        }
                        txtRegistrationFieldName.Visible = false;

                        if (reqtxtRegistrationFieldName != null)
                        {
                            reqtxtRegistrationFieldName.Visible = false;
                            reqtxtRegistrationFieldName.Enabled = false;
                            lblValidates.Enabled = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqddlRegisterCountry != null)
                            {
                                reqddlRegisterCountry.Visible = true;
                                reqddlRegisterCountry.Enabled = true;
                                if (rpt != null)
                                {
                                    if (rpt.ID == "rptInvoiceAddress")
                                    {
                                        reqddlRegisterCountry.ErrorMessage = strRegCountryErrorMsg;
                                    }
                                    else
                                    {
                                        reqddlRegisterCountry.ErrorMessage = strDelCountryErrorMsg;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ddlRegisterCountry.Visible = false;
                        txtRegistrationFieldName.Visible = true;
                        if (reqddlRegisterCountry != null)
                        {
                            reqddlRegisterCountry.Visible = false;
                            reqddlRegisterCountry.Enabled = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqtxtRegistrationFieldName != null)
                            {
                                reqtxtRegistrationFieldName.Visible = true;
                                reqtxtRegistrationFieldName.Enabled = true;
                                reqtxtRegistrationFieldName.ErrorMessage = Generic_Enter_Text + " " + objLang.LabelTitle;
                                lblValidates.Text = Generic_Enter_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqtxtRegistrationFieldName.Visible = false;
                            reqtxtRegistrationFieldName.Enabled = false;
                            lblValidates.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptCustomFields_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Int16 iRegId = ((UserRegistrationBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                    UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);
                    UserRegistrationBE.RegistrationLanguagesBE objLang = objTempUserRegistrationBE.UserRegistrationLanguagesLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId & x.LanguageId == GlobalFunctions.GetLanguageId());
                    HiddenField hdfColumnName = (HiddenField)e.Item.FindControl("hdfColumnName");
                    HiddenField hdfControl = (HiddenField)e.Item.FindControl("hdfControl");
                    Label lblCustomFieldName = (Label)e.Item.FindControl("lblCustomFieldName");
                    TextBox txtCustomFieldName = (TextBox)e.Item.FindControl("txtCustomFieldName");
                    DropDownList ddlCustomValue = (DropDownList)e.Item.FindControl("ddlCustomValue");
                    CheckBoxList chkCustomValue = (CheckBoxList)e.Item.FindControl("chkCustomValue");
                    RequiredFieldValidator reqtxtCustomFieldName = (RequiredFieldValidator)e.Item.FindControl("reqtxtCustomFieldName");
                    RequiredFieldValidator reqddlCustomValue = (RequiredFieldValidator)e.Item.FindControl("reqddlCustomValue");
                    if (hdfColumnName != null)
                    {
                        hdfColumnName.Value = objRegistrationFieldsConfigurationBE.SystemColumnName;
                    }
                    if (objRegistrationFieldsConfigurationBE.IsMandatory)
                    {
                        lblCustomFieldName.Text = "*" + objLang.LabelTitle;
                    }
                    else
                    {
                        lblCustomFieldName.Text = objLang.LabelTitle;
                    }
                    if (objRegistrationFieldsConfigurationBE.FieldType == 2)
                    {
                        #region "DropDownList"
                        hdfControl.Value = "dropdown";
                        ddlCustomValue.Visible = true;
                        Int16 oRegistrationFieldsConfigurationId = ((UserRegistrationBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                        Int16 oRegistrationFieldsLanguageId = ((UserRegistrationBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).LanguageId;
                        ddlCustomValue.DataSource = objUserRegistrationBE.UserRegistrationFieldDataMasterLst.FindAll(x => x.RegistrationFieldsConfigurationId == oRegistrationFieldsConfigurationId && x.LanguageID == GlobalFunctions.GetLanguageId());
                        ddlCustomValue.DataTextField = "RegistrationFieldDataValue";
                        ddlCustomValue.DataValueField = "RegistrationFieldDataMasterId";
                        ddlCustomValue.DataBind();
                        GlobalFunctions.AddDropdownItem(ref ddlCustomValue);
                        txtCustomFieldName.Visible = false;
                        if (reqtxtCustomFieldName != null)
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqddlCustomValue != null)
                            {
                                reqddlCustomValue.Visible = true;
                                reqddlCustomValue.Enabled = true;
                                reqddlCustomValue.ErrorMessage = Generic_Select_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 1)
                    {
                        #region "TextBox"
                        hdfControl.Value = "textbox";
                        ddlCustomValue.Visible = false;
                        txtCustomFieldName.Visible = true;
                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = false;
                        }
                        if (!objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqtxtCustomFieldName != null)
                            {
                                reqtxtCustomFieldName.Visible = true;
                                reqtxtCustomFieldName.Enabled = true;
                                reqtxtCustomFieldName.ErrorMessage = Generic_Enter_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 3)
                    {
                        #region "CheckBox
                        hdfControl.Value = "checkbox";
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = true;
                            Int16 oRegistrationFieldsConfigurationId = ((UserRegistrationBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                            chkCustomValue.DataSource = objUserRegistrationBE.UserRegistrationFieldDataMasterLst.FindAll(x => x.RegistrationFieldsConfigurationId == oRegistrationFieldsConfigurationId && x.LanguageID == GlobalFunctions.GetLanguageId());
                            chkCustomValue.DataTextField = "RegistrationFieldDataValue";
                            chkCustomValue.DataValueField = "RegistrationFieldDataMasterId";
                            chkCustomValue.DataBind();
                        }
                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (txtCustomFieldName != null)
                        {
                            txtCustomFieldName.Visible = false;
                        }
                        if (ddlCustomValue != null)
                        {
                            ddlCustomValue.Visible = false;
                        }

                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (reqtxtCustomFieldName != null)
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            bool bPunchout = false;
            int Basys = 0;
            try
            {
                try
                {
                    string strEmailId = HttpUtility.HtmlEncode(txtEmail.Text.Trim());
                    string[] strdata = strEmailId.Split('@');
                    MailAddress address = new MailAddress(txtEmail.Text.Trim());
                    host = strdata[1];
                }
                catch (Exception)
                {
                    return;
                }
                BindUserTypeMappping();
                if (String.IsNullOrEmpty(txtTitles.Text))
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strTitleReqMessage, AlertType.Warning);
                    return;
                }
                if (drpdwnPreferredLanguage.SelectedIndex == 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_PreferredLanguages_Message, AlertType.Warning);
                    return;
                }
                if (drpdwnPreferredCurrency.SelectedIndex == 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_PreferredCurrency_Message, AlertType.Warning);
                    return;
                }
                Exceptions.WriteInfoLog("Registration:btnConfirm_Click Start");
                string theCode = Session["CaptchaText"].ToString();
                if (Sanitizer.GetSafeHtmlFragment(txtCaptcha.Text.ToLower()) != theCode.ToLower())
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strGeneric_InvalidCaptchaText_Message, AlertType.Warning); //Added By Hardik "11/11/2016"
                    txtCaptcha.Text = "";
                    return;
                }
                intLanguageId = GlobalFunctions.GetLanguageId();
                Exceptions.WriteInfoLog("Registration:GetLanguageId - " + Convert.ToString(intLanguageId));
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                Exceptions.WriteInfoLog("Registration:GetCurrencyId - " + Convert.ToString(intCurrencyId));
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("Registration: After Store Details Fetch");
                string minPass = "5";
                bool flag = false;
                if (objStoreBE != null && objStoreBE.StoreName != null)
                {
                    Exceptions.WriteInfoLog("Registration:objStoreBE has value");
                    Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures before lambda Step 1");
                    #region Added by Sripal "For Minimum password length"
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue != null)
                    {
                        Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 2a");
                        minPass = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue;
                        Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 3a");
                        Exceptions.WriteInfoLog("Registration:PS_MinimumPasswordLength -" + minPass);
                    }
                    else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue != null)
                    {
                        Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 2b");
                        minPass = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue;
                        Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 3b");
                        Exceptions.WriteInfoLog("Registration:PS_MinimumPasswordLength -" + minPass);
                    }
                    #endregion
                    Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures before lambda Step 4");
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_PasswordExpression").FeatureValues[0].IsEnabled == true)
                    {
                        Exceptions.WriteInfoLog("Registration:objStoreBE StoreFeatures after lambda Step 5");
                        flag = true;
                    }
                }
                lstStoreDetail = StoreBL.GetStoreDetails();
                List<CurrencyBE> lstCurrencies = new List<CurrencyBE>();
                lstCurrencies = CurrencyBL.GetAllStoreCurrencyDetails();                
                #region Added by SHRIGANESH SINGH for Validating against password policy 12 May 2016
                if (objStoreBE != null)
                {
                    lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                        {
                            string password = txtPassword.Text;
                            minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                            if (password.Length < Convert.ToInt16(minPasswordLength))
                            {
                                string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                strPwdError = strPwdError.Replace("@Num@", minPass);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                txtCaptcha.Text = "";
                                return;
                            }
                        }
                        if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                        {
                            string password = txtPassword.Text;
                            minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                            if (password.Length >= Convert.ToInt16(minPasswordLength))
                            {
                                string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                                bool check = true;
                                check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                if (!check)
                                {
                                    string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                    txtCaptcha.Text = "";
                                    return;
                                }
                            }
                            else
                            {
                                string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                strPwdError = strPwdError.Replace("@Num@", minPass);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                txtCaptcha.Text = "";
                                return;
                            }
                        }
                        if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                        {
                            string password = txtPassword.Text;
                            minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                            if (password.Length >= Convert.ToInt16(minPasswordLength))
                            {
                                string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                                bool check = true;
                                check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                if (!check)
                                {
                                    string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                    txtCaptcha.Text = "";
                                    return;
                                }
                            }
                            else
                            {
                                string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                strPwdError = strPwdError.Replace("@Num@", minPass);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                txtCaptcha.Text = "";
                                return;
                            }
                        }
                    }
                }
                #endregion
                #region "Filtering"
                Exceptions.WriteInfoLog("Registration:Filtering starts");
                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "whitelist").IsEnabled)
                {
                    Exceptions.WriteInfoLog("Registration:Filtering bDomainWhitelist set true");
                    bDomainWhitelist = true;
                }
                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "blacklist").IsEnabled)
                {
                    Exceptions.WriteInfoLog("Registration:Filtering bDomainBlackList set true");
                    bDomainBlackList = true;
                }
                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "whitelist").IsEnabled)
                {
                    Exceptions.WriteInfoLog("Registration:Filtering bEmailWhiteList set true");
                    bEmailWhiteList = true;
                }
                if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "blacklist").IsEnabled)
                {
                    Exceptions.WriteInfoLog("Registration:Filtering bEmailBlackList set true");
                    bEmailBlackList = true;
                }
                Exceptions.WriteInfoLog("Registration:before Registeration details");
                objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                Exceptions.WriteInfoLog("Registration:after Registeration details");
                string strUEmail = Sanitizer.GetSafeHtmlFragment(txtEmail.Text.Trim());
                string strDomain = strUEmail.Substring(strUEmail.LastIndexOf("@") + 1);                               
                List<UserRegistrationBE.RegistrationFilterBE> objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                List<UserRegistrationBE.RegistrationFilterBE> objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                UserRegistrationBE.RegistrationFilterBE objRegistrationWhiteListDomainFilterBE = null;
                UserRegistrationBE.RegistrationFilterBE objRegistrationBlackListDomainFilterBE = null;
                UserRegistrationBE.RegistrationFilterBE objRegistrationWhiteListEmailFilterBE = null;
                UserRegistrationBE.RegistrationFilterBE objRegistrationBlackListEmailFilterBE = null;
                if (bDomainWhitelist)
                {
                    objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                    objRegistrationWhiteListDomainFilterBE = objRegistrationWhiteListFilterBELst.FirstOrDefault(x => x.Domain.ToLower() == strDomain.ToLower());
                }
                if (bDomainBlackList)
                {
                    objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                    objRegistrationBlackListDomainFilterBE = objRegistrationBlackListFilterBELst.FirstOrDefault(x => x.Domain.ToLower() == strDomain.ToLower());
                }
                if (bEmailWhiteList)
                {
                    objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                    objRegistrationWhiteListEmailFilterBE = objRegistrationWhiteListFilterBELst.FirstOrDefault(x => x.EmailId.ToLower() == strUEmail.ToLower());
                }
                if (bEmailBlackList)
                {
                    objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                    objRegistrationBlackListEmailFilterBE = objRegistrationBlackListFilterBELst.FirstOrDefault(x => x.EmailId.ToLower() == strUEmail.ToLower());
                }
                if (bDomainWhitelist && bEmailWhiteList)
                {
                    if (objRegistrationWhiteListDomainFilterBE == null && objRegistrationWhiteListEmailFilterBE == null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                }
                else if (bDomainWhitelist && !bEmailWhiteList && !bEmailBlackList)
                {
                    if (objRegistrationWhiteListDomainFilterBE == null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                }
                else if (bDomainBlackList && !bEmailWhiteList && !bEmailBlackList)
                {
                    if (objRegistrationBlackListDomainFilterBE != null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                }
                else if (bEmailWhiteList && !bDomainBlackList && !bDomainWhitelist)
                {
                    if (objRegistrationWhiteListEmailFilterBE == null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                }
                else if (bEmailBlackList && !bDomainBlackList && !bDomainWhitelist)
                {
                    if (objRegistrationBlackListEmailFilterBE != null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                }
                else if (bDomainWhitelist && bEmailBlackList)
                {
                    if (objRegistrationBlackListEmailFilterBE != null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                    else if (objRegistrationWhiteListDomainFilterBE == null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                }
                else if (bDomainBlackList && bEmailBlackList)
                {
                    if (objRegistrationBlackListDomainFilterBE != null && objRegistrationBlackListEmailFilterBE != null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                    else if (objRegistrationBlackListDomainFilterBE != null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                    else if (objRegistrationBlackListEmailFilterBE != null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                }
                else if (bDomainBlackList && bEmailWhiteList)
                {
                    if (objRegistrationBlackListDomainFilterBE != null && objRegistrationWhiteListEmailFilterBE == null)
                    {
                        List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErr, AlertType.Warning);
                        return;
                    }
                }
                Exceptions.WriteInfoLog("Registration:Filtering end");
                #region to validate usertype
                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                #endregion
                #endregion
                UserBE objUser = new UserBE();
                string SaltPass = SaltHash.ComputeHash((txtPassword.Text.Trim()), "SHA512", null);
                #region apostrophy changes vikram
                objUser.EmailId = HttpUtility.HtmlEncode(txtEmail.Text.Trim());
                #endregion
                objUser.Password = SaltPass;
                objUser.UserTitle = Sanitizer.GetSafeHtmlFragment(txtTitles.Text.Trim());
                #region Added by snehal - Punchout user checkout 20 12 2016
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_IsPunchoutRegistration").FeatureValues[0].IsEnabled)
                {
                    int IsPunchout = UserBL.IsPunchoutUser(Constants.USP_IsPunchoutUser, true, objUser);
                    if (IsPunchout == 1)
                    {
                        bPunchout = true;
                    }
                }
                #endregion
                objUser.FirstName = Sanitizer.GetSafeHtmlFragment(txtFirstName.Text.Trim());
                objUser.LastName = Sanitizer.GetSafeHtmlFragment(txtLastName.Text.Trim());                               
                objUser.IsGuestUser = false;
                objUser.IPAddress = GlobalFunctions.GetIpAddress();
                objUser.CurrencyId = Convert.ToInt16(intCurrencyId);
                objUser.LanguageId = Convert.ToInt16(intLanguageId);
                objUser.IsMarketing = rdProfile_MarketingOption1.Checked ? true : false;
                objUser.Points = -1; // Added by ShriGanesh 26 April 2017
                #region "Invoice Address"
                Exceptions.WriteInfoLog("Registration:User Invoice Address start.");
                try
                {
                    foreach (RepeaterItem item in rptInvoiceAddress.Items)
                    {
                        TextBox txtRegistrationFieldName = (TextBox)item.FindControl("txtRegistrationFieldName");
                        DropDownList ddlRegisterCountry = (DropDownList)item.FindControl("ddlRegisterCountry");
                        HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                        PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        object value = null;
                        if (fi != null)
                        {
                            try
                            {
                                int q = string.Compare("predefinedcolumn8", hdfColumnName.Value, true);
                                if (hdfColumnName.Value.ToLower() != "predefinedcolumn8")
                                {
                                    Exceptions.WriteInfoLog("Registration:rptInvoiceAddress before set value in InvoiceAddress fields");
                                    if (txtRegistrationFieldName != null)
                                    {
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtRegistrationFieldName.Text.Trim()), fi.PropertyType);
                                    }
                                    Exceptions.WriteInfoLog("Registration:rptInvoiceAddress after set value in InvoiceAddress fields");
                                }
                                else
                                {
                                    Exceptions.WriteInfoLog("Registration:rptInvoiceAddress column name predefinedcolumn8.");
                                    Exceptions.WriteInfoLog("Registration:rptInvoiceAddress before set value in InvoiceAddress fields");
                                    if (ddlRegisterCountry != null)
                                    {
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlRegisterCountry.SelectedValue), fi.PropertyType);
                                    }
                                    Exceptions.WriteInfoLog("Registration:rptInvoiceAddress after set value in InvoiceAddress fields");
                                }
                            }
                            catch (InvalidCastException) { }
                            fi.SetValue(objUser, value, null);
                        }
                    }
                }
                catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                Exceptions.WriteInfoLog("Registration:User Invoice Address end.");
                #endregion
                #region "Delivery Address"
                UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                Exceptions.WriteInfoLog("Registration:User Delivery Address start.");
                if (chkRegSameAsDelv != null)
                {
                    Exceptions.WriteInfoLog("chkRegSameAsDelv not null.");
                    if (!chkRegSameAsDelv.Checked) { Exceptions.WriteInfoLog("Registration: chkRegSameAsDelv checked."); }
                    else
                    { Exceptions.WriteInfoLog("Registration: chkRegSameAsDelv Unchecked."); }
                }
                else
                {
                    Exceptions.WriteInfoLog("chkRegSameAsDelv null.");
                }
                if (!chkRegSameAsDelv.Checked)
                {
                    Exceptions.WriteInfoLog("Registration: chkRegSameAsDelv Unchecked if.");
                    #region "Different Address"
                    try
                    {
                        if (rptDeliveryAddress != null)
                        {
                            Exceptions.WriteInfoLog("Registration: rptDeliveryAddress is not null.");
                            int iItem = 0;
                            foreach (RepeaterItem item in rptDeliveryAddress.Items)
                            {
                                Exceptions.WriteInfoLog("Registration: rptDeliveryAddress item " + iItem.ToString());
                                iItem++;
                            }
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Registration: rptDeliveryAddress is null.");
                        }
                        foreach (RepeaterItem item in rptDeliveryAddress.Items)
                        {
                            TextBox txtRegistrationFieldName = (TextBox)item.FindControl("txtRegistrationFieldName");
                            DropDownList ddlRegisterCountry = (DropDownList)item.FindControl("ddlRegisterCountry");
                            HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");

                            PropertyInfo fi = typeof(UserBE.UserDeliveryAddressBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            object value = null;
                            if (fi != null)
                            {
                                try
                                {
                                    if (hdfColumnName.Value.ToLower() != "predefinedcolumn8")
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress before set value in DeliveryAddress fields");
                                        if (txtRegistrationFieldName != null)
                                        {
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtRegistrationFieldName.Text.Trim()), fi.PropertyType);
                                        }
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress after set value in DeliveryAddress fields");
                                    }
                                    else
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress column name predefinedcolumn8.");
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress before set value in DeliveryAddress fields");
                                        if (ddlRegisterCountry != null)
                                        {
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlRegisterCountry.SelectedValue), fi.PropertyType);
                                        }
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress after set value in DeliveryAddress fields");
                                    }
                                }
                                catch (InvalidCastException) { }
                                fi.SetValue(obj, value, null);
                            }
                        }
                    }
                    catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                    #endregion
                }
                else
                {
                    Exceptions.WriteInfoLog("Registration: chkRegSameAsDelv checked else.");
                    #region "Same Address"
                    try
                    {
                        if (rptInvoiceAddress != null)
                        {
                            Exceptions.WriteInfoLog("Registration: rptInvoiceAddress is not null.");
                            int iItem = 0;
                            foreach (RepeaterItem item in rptInvoiceAddress.Items)
                            {
                                Exceptions.WriteInfoLog("Registration: rptInvoiceAddress item " + iItem.ToString());
                                iItem++;
                            }
                        }
                        else
                        {
                            Exceptions.WriteInfoLog("Registration: rptInvoiceAddress is null.");
                        }

                        foreach (RepeaterItem item in rptInvoiceAddress.Items)
                        {
                            TextBox txtRegistrationFieldName = (TextBox)item.FindControl("txtRegistrationFieldName");
                            DropDownList ddlRegisterCountry = (DropDownList)item.FindControl("ddlRegisterCountry");
                            HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");

                            PropertyInfo fi = typeof(UserBE.UserDeliveryAddressBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            object value = null;
                            if (fi != null)
                            {
                                try
                                {
                                    if (hdfColumnName.Value.ToLower() != "predefinedcolumn8")
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress before set value in DeliveryAddress fields");
                                        if (txtRegistrationFieldName != null)
                                        {
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtRegistrationFieldName.Text.Trim()), fi.PropertyType);
                                        }
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress after set value in DeliveryAddress fields");
                                    }
                                    else
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress column name predefinedcolumn8.");
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress before set value in DeliveryAddress fields");
                                        if (ddlRegisterCountry != null)
                                        {
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlRegisterCountry.SelectedValue), fi.PropertyType);
                                        }
                                        Exceptions.WriteInfoLog("Registration:rptDeliveryAddress after set value in DeliveryAddress fields");
                                    }
                                }
                                catch (InvalidCastException) { }
                                fi.SetValue(obj, value, null);
                            }
                        }
                    }
                    catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                    #endregion
                }
                Exceptions.WriteInfoLog("Registration:User Delivery Address end.");
                #endregion
                objUser.UserDeliveryAddress.Add(obj);
                #region "CustomFields"
                Exceptions.WriteInfoLog("Registration:rptCustomFields start.");
                try
                {
                    foreach (RepeaterItem item in rptCustomFields.Items)
                    {
                        TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                        DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                        HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                        HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                        CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                        PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        object value = null;
                        if (fi != null)
                        {
                            try
                            {
                                #region "customColumns"
                                if (hdfControl.Value.ToLower() == "textbox")
                                {
                                    Exceptions.WriteInfoLog("Registration:rptCustomFields textbox.");
                                    Exceptions.WriteInfoLog("Registration:rptCustomFields before set value in customColumns");
                                    if (txtCustomFieldName != null)
                                    {
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);
                                    }
                                    Exceptions.WriteInfoLog("Registration:rptCustomFields after set value in customColumns");
                                }
                                else if (hdfControl.Value.ToLower() == "dropdown")
                                {
                                    Exceptions.WriteInfoLog("Registration:rptCustomFields dropdown.");
                                    Exceptions.WriteInfoLog("Registration:rptCustomFields before set value in customColumns");
                                    if (ddlCustomValue != null)
                                    {
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                    }
                                    Exceptions.WriteInfoLog("Registration:rptCustomFields after set value in customColumns");
                                }
                                else if (hdfControl.Value.ToLower() == "checkbox")
                                {
                                    Exceptions.WriteInfoLog("Registration:rptCustomFields checkbox.");
                                    if (chkCustomValue != null)
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptCustomFields before set value in customColumns");
                                        #region
                                        foreach (ListItem chkItem in chkCustomValue.Items)
                                        {
                                            if (chkItem.Selected)
                                            {
                                                if (iCount == 0)
                                                {
                                                    value = chkItem.Value;
                                                    iCount++;
                                                }
                                                else
                                                {
                                                    value = value + "|" + chkItem.Value;
                                                    iCount++;
                                                }
                                            }
                                        }
                                        #endregion
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                        Exceptions.WriteInfoLog("Registration:rptCustomFields after set value in customColumns");
                                    }
                                }
                                #endregion
                            }
                            catch (InvalidCastException) { }
                            fi.SetValue(objUser, value, null);
                        }
                    }
                }
                catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                Exceptions.WriteInfoLog("Registration:rptCustomFields end.");
                #endregion                
                #region UserType Filtering"
                objUser.UserTypeID = 1; // Modified to 1 from 0 by SHRIGANESH SINGH 14 July 2016
                #region "Validation according Option Selection"
                if (objUserTypeMasterOptions != null)
                {
                    #region "UserSelection"
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 1)
                    {
                        if (ddlUserType.SelectedValue != "")
                        {
                            objUser.UserTypeID = Convert.ToInt16(ddlUserType.SelectedValue);
                            // Below LOC is added by SHRIGANESH SINGH to display Validation Alert 14 July 2016
                            CustomFieldValidationError = SetCustomFields(objUser);
                            if (CustomFieldValidationError != "")
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                                return;
                            }
                        }
                        else
                        {
                            CustomFieldValidationError = SetCustomFields(objUser);
                            if (CustomFieldValidationError != "")
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                                return;
                            }
                        }
                    }
                    #endregion
                    #region "Email"
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                    {
                        UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidation = objUserTypeMappingBE.lstUserTypeEmailValidation.FirstOrDefault(x => x.EmailId.ToLower() == txtEmail.Text.ToLower().Trim() && x.IsWhiteList == true);
                        UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidationDomain = objUserTypeMappingBE.lstUserTypeEmailValidation.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.Domain.ToLower() == host.ToLower() && x.IsWhiteList == true);
                        if (objRegUserTypeEmailValidation != null)
                        {
                            objUser.UserTypeID = objRegUserTypeEmailValidation.UserTypeId;
                        }
                        else if (objRegUserTypeEmailValidationDomain != null)
                        {
                            objUser.UserTypeID = objRegUserTypeEmailValidationDomain.UserTypeId;
                        }
                        else
                        {
                            UserTypeMappingBE.UserTypeDetails objRegUserTypeDetails = objUserTypeMappingBE.lstUserTypeDetails.OrderBy(x => x.UserTypeID).FirstOrDefault(x => x.IsWhiteList == false);
                            if (objRegUserTypeDetails != null)
                            {
                                objUser.UserTypeID = objRegUserTypeDetails.UserTypeID;
                            }
                            else
                            {
                                objUser.UserTypeID = 1;
                            }
                        }
                        // Below LOC is added by SHRIGANESH SINGH to display Validation Alert 14 July 2016
                        CustomFieldValidationError = SetCustomFields(objUser);
                        if (CustomFieldValidationError != "")
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                            return;
                        }
                    }
                    #endregion
                    #region "Country"
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                    {
                        UserTypeMappingBE.Country objRegCountry = objUserTypeMappingBE.lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(objUser.PredefinedColumn8));
                        if (objRegCountry != null)
                        {
                            if (objRegCountry.UserTypeID > 0)
                            {
                                objUser.UserTypeID = objRegCountry.UserTypeID;
                            }
                            else
                            {
                                string str = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Country_Register" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                GlobalFunctions.ShowModalAlertMessages(this, str, AlertType.Failure);
                                return;
                            }
                        }
                        else
                        {
                            string str = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Country_Register" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            GlobalFunctions.ShowModalAlertMessages(this, str, AlertType.Failure);
                            return;
                        }
                        // Below LOC is added by SHRIGANESH SINGH to display Validation Alert 14 July 2016
                        CustomFieldValidationError = SetCustomFields(objUser);
                        if (CustomFieldValidationError != "")
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                            return;
                        }
                    }
                    #endregion
                    #region "Custom Mapping"
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                    {
                        #region "CustomFields For UserType"
                        Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields start.");
                        try
                        {
                            #region rptUserTypeCustomFields
                            foreach (RepeaterItem item in rptCustomMapping.Items)
                            {
                                TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                                DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                                HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                                HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                                CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                                PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                object value = null;
                                if (fi != null)
                                {
                                    try
                                    {
                                        #region "customColumns"
                                        if (hdfControl.Value.ToLower() == "textbox")
                                        {
                                            Exceptions.WriteInfoLog("Registration:rptCustomMapping textbox.");
                                            Exceptions.WriteInfoLog("Registration:rptCustomMapping before set value in customColumns");
                                            if (txtCustomFieldName != null)
                                            {
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);
                                            }
                                            Exceptions.WriteInfoLog("Registration:rptCustomMapping after set value in customColumns");
                                        }
                                        else if (hdfControl.Value.ToLower() == "dropdown")
                                        {
                                            Exceptions.WriteInfoLog("Registration:rptCustomMapping dropdown.");
                                            Exceptions.WriteInfoLog("Registration:rptCustomMapping before set value in customColumns");
                                            if (ddlCustomValue != null)
                                            {
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                            }
                                            Exceptions.WriteInfoLog("Registration:rptCustomMapping after set value in customColumns");
                                        }
                                        else if (hdfControl.Value.ToLower() == "checkbox")
                                        {
                                            Exceptions.WriteInfoLog("Registration:rptCustomMapping checkbox.");
                                            if (chkCustomValue != null)
                                            {
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping before set value in customColumns");
                                                #region
                                                foreach (ListItem chkItem in chkCustomValue.Items)
                                                {
                                                    if (chkItem.Selected)
                                                    {
                                                        if (iCount == 0)
                                                        {
                                                            value = chkItem.Value;
                                                            iCount++;
                                                        }
                                                        else
                                                        {
                                                            value = value + "|" + chkItem.Value;
                                                            iCount++;
                                                        }
                                                    }
                                                }
                                                #endregion
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                                Exceptions.WriteInfoLog("Registration:rptCustomMapping after set value in customColumns");
                                            }
                                        }
                                        #endregion
                                    }
                                    catch (InvalidCastException) { }
                                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                                    {
                                        b = false;
                                        if (objUserTypeMappingBE != null)
                                        {
                                            Int16 iRegistrationFieldsConfigurationId = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.SystemColumnName == hdfColumnName.Value).RegistrationFieldsConfigurationId;
                                            UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objValRegistrationFieldDataMasterValidationBE = objUserTypeMappingBE.lstRegistrationFieldDataMasterValidationBE.FirstOrDefault(x => x.RegistrationFieldDataValue.ToLower() == Convert.ToString(value).ToLower());
                                            if (objValRegistrationFieldDataMasterValidationBE != null)
                                            {                                              
                                                b = true;
                                                objUser.UserTypeID = Convert.ToInt16(objValRegistrationFieldDataMasterValidationBE.UserTypeID);                                              
                                            }
                                            if (b == false)
                                            {
                                                if (objUser.UserTypeID == 1)
                                                {
                                                    objUser.UserTypeID = 1;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        b = true;
                                    }
                                    fi.SetValue(objUser, value, null);
                                }
                            }
                            #endregion
                            CustomFieldValidationError = SetCustomFields(objUser);
                            if (CustomFieldValidationError != "")
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                                return;
                            }
                        }
                        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                        Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields end.");
                        #endregion
                    }
                    #endregion
                }
                #endregion

                #region "CustomFields For Non UserType"
                Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll start.");
                try
                {
                    #region rptCustomFieldsAll
                    string strError = "";
                    bool custom = true;
                    foreach (RepeaterItem item in rptCustomFieldsAll.Items)
                    {
                        TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                        DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                        HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                        HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                        CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                        HiddenField hdnRegistrationFieldsConfigurationId = (HiddenField)item.FindControl("hdnRegistrationFieldsConfigurationId");
                        Label lblCustomFieldName = (Label)item.FindControl("lblCustomFieldName");
                        PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        object value = null;
                        if (fi != null)
                        {
                            try
                            {
                                #region "customColumns"
                                if (hdfControl.Value.ToLower() == "textbox")
                                {
                                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll textbox.");
                                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll before set value in customColumns");
                                    if (txtCustomFieldName != null)
                                    {
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);
                                        int bResult = RegistrationCustomFieldBL.ValidateRegistrationCustomFields(value, hdnRegistrationFieldsConfigurationId.Value);
                                        if (bResult == 0)
                                        {
                                            strError = strCustomFieldValidationError + " " + lblCustomFieldName.Text.Replace("*", "");
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strError, AlertType.Warning);
                                            return;
                                        }
                                        else if (bResult == 3)
                                        {
                                            strError = strError = strGeneric_Existing_LoginFieldValue_Message.Replace("@fieldName", lblCustomFieldName.Text.Replace("*", "")); 
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, strError, AlertType.Warning);
                                            return;
                                        }
                                    }
                                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll after set value in customColumns");
                                }
                                else if (hdfControl.Value.ToLower() == "dropdown")
                                {
                                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll dropdown.");
                                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll before set value in customColumns");
                                    if (ddlCustomValue != null)
                                    {
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                    }
                                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll after set value in customColumns");
                                }
                                else if (hdfControl.Value.ToLower() == "checkbox")
                                {
                                    Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll checkbox.");
                                    if (chkCustomValue != null)
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll before set value in customColumns");
                                        #region
                                        foreach (ListItem chkItem in chkCustomValue.Items)
                                        {
                                            if (chkItem.Selected)
                                            {
                                                if (iCount == 0)
                                                {
                                                    value = chkItem.Value;
                                                    iCount++;
                                                }
                                                else
                                                {
                                                    value = value + "|" + chkItem.Value;
                                                    iCount++;
                                                }
                                            }
                                        }
                                        #endregion
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                        Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll after set value in customColumns");
                                    }
                                }
                                #endregion
                            }
                            catch (InvalidCastException) { }

                            b = false;
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                            {
                                if (objUserTypeMappingBE != null)
                                {
                                    Int16 iRegistrationFieldsConfigurationId = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.SystemColumnName == hdfColumnName.Value).RegistrationFieldsConfigurationId;
                                    List<UserTypeMappingBE.RegistrationFieldDataMasterValidationBE> objValRegistrationFieldDataMasterValidationBELst = objUserTypeMappingBE.lstRegistrationFieldDataMasterValidationBE.FindAll(x => x.RegistrationFieldsConfigurationId == iRegistrationFieldsConfigurationId);
                                    if (objValRegistrationFieldDataMasterValidationBELst != null)
                                    {
                                        UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objUserTypeMappingBERegistrationFieldDataMasterValidationBE = objValRegistrationFieldDataMasterValidationBELst.FirstOrDefault(x => x.RegistrationFieldDataValue.ToLower().Equals(Convert.ToString(value)));
                                        if (objUserTypeMappingBERegistrationFieldDataMasterValidationBE != null)
                                        {
                                            b = true;
                                            objUser.UserTypeID = Convert.ToInt16(objUserTypeMappingBERegistrationFieldDataMasterValidationBE.UserTypeID);
                                        }
                                    }
                                    if (b == false)
                                    {
                                        if (objUser.UserTypeID == 1)
                                        {
                                            objUser.UserTypeID = 1;
                                        }
                                    }
                                }
                            }
                            else
                            {                               
                                    b = true;                              
                            }
                            fi.SetValue(objUser, value, null);                           
                        }
                    }
                    #endregion
                }
                catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                Exceptions.WriteInfoLog("Registration:rptCustomFieldsAll end.");
                #endregion
                #endregion
                int i = 0;
                Exceptions.WriteInfoLog("Registration:before Insert in Database.");
                if (bPunchout == true)
                {
                    i = UserBL.ExecuteRegisterDetails(Constants.USP_UpdatePunchoutUserRegistrationDetails, true, objUser, "StoreCustomer");
                    Basys = UserBL.IsPunchoutUser(Constants.USP_IsPunchoutBASYS, true, objUser);
                }
                else
                {
                    i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objUser,"StoreCustomer");
                }

                #region to add users preferredlanguage
                if (i > 0)
                {
                    if (objUserTypeMasterOptions != null)
                    {
                        UserPreferredBE objUserPreferredBE = new UserPreferredBE();
                        objUserPreferredBE.UserId = i;
                        if (Convert.ToInt32(drpdwnPreferredCurrency.SelectedValue) > 0 && Convert.ToInt32(drpdwnPreferredLanguage.SelectedValue) > 0)
                        {
                            objUserPreferredBE.PreferredCurrencyId = Convert.ToInt32(drpdwnPreferredCurrency.SelectedValue);
                            objUserPreferredBE.PreferredLanguageId = Convert.ToInt32(drpdwnPreferredLanguage.SelectedValue);
                            UserPreferredBL.InsertUpdateUserPreference(Constants.usp_InsertUpdateUserPreferred, true, objUserPreferredBE);
                            objUser.CurrencyId = Convert.ToInt16(drpdwnPreferredCurrency.SelectedValue);
                            objUser.LanguageId = Convert.ToInt16(drpdwnPreferredLanguage.SelectedValue);
                            GlobalFunctions.SetLanguageId(objUser.CurrencyId);
                            GlobalFunctions.SetLanguageId(objUser.LanguageId);
                            GlobalFunctions.SetCurrencySymbol(lstCurrencies.First(x => x.CurrencyId == objUser.CurrencyId).CurrencySymbol);
                        }
                    }
                }
                #endregion
                if (i > 0)
                {
                    if (bPunchout == true)
                    {
                        if (Basys > 0)
                        {
                            Exceptions.WriteInfoLog("Registration:Successful Insert in database.");
                            for (int x = 0; x < objStoreBE.StoreCurrencies.Count(); x++)
                            {
                                if (objStoreBE.StoreCurrencies[x].IsActive)
                                {
                                    Session["CaptchaText"] = null;
                                    Exceptions.WriteInfoLog("Registration:before BASYS insert.");
                                    InsertBASYSId(Convert.ToInt16(objStoreBE.StoreCurrencies[x].CurrencyId), txtEmail.Text.Trim());
                                    Exceptions.WriteInfoLog("Registration:after BASYS insert.");
                                }
                            }
                        }
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Registration:Successful Insert in database.");
                        for (int x = 0; x < objStoreBE.StoreCurrencies.Count(); x++)
                        {
                            if (objStoreBE.StoreCurrencies[x].IsActive)
                            {
                                Session["CaptchaText"] = null;
                                Exceptions.WriteInfoLog("Registration:before BASYS insert.");
                                InsertBASYSId(Convert.ToInt16(objStoreBE.StoreCurrencies[x].CurrencyId), txtEmail.Text.Trim());
                                Exceptions.WriteInfoLog("Registration:after BASYS insert.");
                            }
                        }
                    }
                }
                else if (i == -1)
                {
                    Exceptions.WriteInfoLog("Registration: User Already Exists");
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strGeneric_Registration_Failure_Msg, AlertType.Warning); //Added By Hardik "11/11/2016"
                    txtCaptcha.Text = "";
                    Session["CaptchaText"] = null;
                    return;
                }
                else if (i == -3)
                {
                    Exceptions.WriteInfoLog("Registration: User Update");
                    i = 1;
                    txtCaptcha.Text = "";
                    Session["CaptchaText"] = null;
                }
                else
                {
                    Exceptions.WriteInfoLog("Registration: Invalid Captcha");
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strOrderDetails_Error_Processing_Registration_Error_Msg, AlertType.Warning); //Added By Hardik "11/11/2016"
                    txtCaptcha.Text = "";
                    Session["CaptchaText"] = null;
                    return;
                }
                if (i > 0)
                {
                    Exceptions.WriteInfoLog("Registration:before Email sent.");
                    if (GlobalFunctions.SendRegistrationMail(txtEmail.Text.Trim(), Sanitizer.GetSafeHtmlFragment(txtFirstName.Text.Trim()), Sanitizer.GetSafeHtmlFragment(txtLastName.Text.Trim())))
                    {
                        objUser.UserId = Convert.ToInt16(i);
                        Session["UserName"] = objUser.EmailId;
                        Session["RegisterPg"] = "RegisterPg";
                        Session["userReg"] = objUser;
                        Session["UserName"] = objUser.EmailId;
                        Session["RegisterPg"] = "RegisterPg";
                        Session["userReg"] = objUser;
                        Session["User"] = objUser;
                        Session["ISAnonymous"] = 0;
                        Exceptions.WriteInfoLog("Registration:after Email sent.");
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strGeneric_Registration_Successfully_Msg , GlobalFunctions.GetVirtualPath() + "Login/login.aspx", AlertType.Success);
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("Registration: Email sending fail.");
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strGeneric_Email_Sending_Failed_Msg, AlertType.Failure); //Added By Hardik "11/11/2016"
                        return;
                    }
                }
                else
                {
                    Exceptions.WriteInfoLog("Hierarchy user registration - " + objUser.EmailId + " ");
                    Response.RedirectToRoute("login-page");
                }              
                Exceptions.WriteInfoLog("Registration:btnConfirm_Click end");
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("Registration: Exception - " + ex.Message + "--" + ex.StackTrace);
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private static void InsertBASYSId(Int16 CurrencyIdx, string strEmailId)
        {
            try
            {
                StoreBE objStore = StoreBL.GetStoreDetails();
                int intCustomerID_OASIS;
                string strIsMarketing;
                UserBE objUser = new UserBE();
                UserBE objBE = new UserBE();
                #region added by vikram for apostrophy start
                objBE.EmailId = HttpUtility.HtmlEncode(strEmailId.Trim());
                #endregion
                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBE.CurrencyId = Convert.ToInt16(CurrencyIdx);
                objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBE);                
                int id = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId;
                objUser.DefaultCustId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId; //1248609;// 1281850; // objStore.StoreCurrencies[i].DefaultCompanyId;//Updated by Sripal for Gift Certificate
                objUser.SourceCodeId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).SourceCodeId; //7778;// 5281; //objStore.StoreCurrencies[i].SourceCodeId;//Updated by Sripal for Gift Certificate
                #region added by vikram for apostrophy start
                objUser.EmailId = Sanitizer.GetSafeHtmlFragment(strEmailId.Trim());
                #endregion
                intCustomerID_OASIS = RegisterCustomer_OASIS.BuildCustContact_OASIS(objUser);
                if (intCustomerID_OASIS > 0)
                {
                    int x = UserBL.ExecuteBASYSDetails(Constants.USP_InsertUserBASYSDetails, true, objUser.UserId, CurrencyIdx, intCustomerID_OASIS);
                }
                #region Added By Hardik on "1/FEB/2017" for Communigator
                strIsMarketing = RegisterCustomer_OASIS.IsMarketingInsertContact(objUser);
                Exceptions.WriteInfoLog("Communigator API called successfull.");
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }        

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-10-15
        /// Scope   : SignUp of user
        /// Modified : 25 03 2016
        /// Modify Author : Sachin Chauhan
        /// Scope : Include resource language for error message
        /// </summary>
        /// <param name="strEmail"></param>        
        /// <returns name="string"></returns>
        [System.Web.Services.WebMethod]
        public static string GuestRegistration(string strEmail)
        {
            #region Code Added by SHRIGANESH SINGH for Filtering Emailid as per Store setting 17 May 2016
            bool bDomainWhitelist = false, bDomainBlackList = false, bEmailWhiteList = false, bEmailBlackList = false;
            UserRegistrationBE objUserRegistrationBE = new UserRegistrationBE();
            List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
            #region "Filtering"
            Exceptions.WriteInfoLog("Registration:Filtering starts");
            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "whitelist").IsEnabled)
            {
                Exceptions.WriteInfoLog("Registration:Filtering bDomainWhitelist set true");
                bDomainWhitelist = true;
            }
            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_domainvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "blacklist").IsEnabled)
            {
                Exceptions.WriteInfoLog("Registration:Filtering bDomainBlackList set true");
                bDomainBlackList = true;
            }
            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "whitelist").IsEnabled)
            {
                Exceptions.WriteInfoLog("Registration:Filtering bEmailWhiteList set true");
                bEmailWhiteList = true;
            }
            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_emailvalidation").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "blacklist").IsEnabled)
            {
                Exceptions.WriteInfoLog("Registration:Filtering bEmailBlackList set true");
                bEmailBlackList = true;
            }
            Exceptions.WriteInfoLog("Registration:before Registeration details");
            objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
            Exceptions.WriteInfoLog("Registration:after Registeration details");
            string strUEmail = Sanitizer.GetSafeHtmlFragment(strEmail);
            string strDomain = strUEmail.Substring(strUEmail.LastIndexOf("@") + 1);                       

            List<UserRegistrationBE.RegistrationFilterBE> objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
            List<UserRegistrationBE.RegistrationFilterBE> objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
            UserRegistrationBE.RegistrationFilterBE objRegistrationWhiteListDomainFilterBE = null;
            UserRegistrationBE.RegistrationFilterBE objRegistrationBlackListDomainFilterBE = null;
            UserRegistrationBE.RegistrationFilterBE objRegistrationWhiteListEmailFilterBE = null;
            UserRegistrationBE.RegistrationFilterBE objRegistrationBlackListEmailFilterBE = null;
            if (bDomainWhitelist)
            {
                objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                objRegistrationWhiteListDomainFilterBE = objRegistrationWhiteListFilterBELst.FirstOrDefault(x => x.Domain.ToLower() == strDomain.ToLower());
            }
            if (bDomainBlackList)
            {
                objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                objRegistrationBlackListDomainFilterBE = objRegistrationBlackListFilterBELst.FirstOrDefault(x => x.Domain.ToLower() == strDomain.ToLower());
            }
            if (bEmailWhiteList)
            {
                objRegistrationWhiteListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "white");
                objRegistrationWhiteListEmailFilterBE = objRegistrationWhiteListFilterBELst.FirstOrDefault(x => x.EmailId.ToLower() == strUEmail.ToLower());
            }
            if (bEmailBlackList)
            {
                objRegistrationBlackListFilterBELst = objUserRegistrationBE.UserRegistrationFilterLst.FindAll(x => x.FilterType.ToLower() == "black");
                objRegistrationBlackListEmailFilterBE = objRegistrationBlackListFilterBELst.FirstOrDefault(x => x.EmailId.ToLower() == strUEmail.ToLower());
            }
            if (bDomainWhitelist && bEmailWhiteList)
            {
                if (objRegistrationWhiteListDomainFilterBE == null && objRegistrationWhiteListEmailFilterBE == null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
            }
            else if (bDomainWhitelist && !bEmailWhiteList && !bEmailBlackList)
            {
                if (objRegistrationWhiteListDomainFilterBE == null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
            }
            else if (bDomainBlackList && !bEmailWhiteList && !bEmailBlackList)
            {
                if (objRegistrationBlackListDomainFilterBE != null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
            }
            else if (bEmailWhiteList && !bDomainBlackList && !bDomainWhitelist)
            {
                if (objRegistrationWhiteListEmailFilterBE == null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
            }
            else if (bEmailBlackList && !bDomainBlackList && !bDomainWhitelist)
            {
                if (objRegistrationBlackListEmailFilterBE != null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
            }
            else if (bDomainWhitelist && bEmailBlackList)
            {
                if (objRegistrationBlackListEmailFilterBE != null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
                else if (objRegistrationWhiteListDomainFilterBE == null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
            }
            else if (bDomainBlackList && bEmailBlackList)
            {
                if (objRegistrationBlackListDomainFilterBE != null && objRegistrationBlackListEmailFilterBE != null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
                else if (objRegistrationBlackListDomainFilterBE != null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
                else if (objRegistrationBlackListEmailFilterBE != null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
            }
            else if (bDomainBlackList && bEmailWhiteList)
            {
                if (objRegistrationBlackListDomainFilterBE != null && objRegistrationWhiteListEmailFilterBE == null)
                {
                    string strErr = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Domain" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    return strErr;
                }
            }
            Exceptions.WriteInfoLog("Registration:Filtering end");
            #endregion
            #endregion
            #region Maintain Checkout users sessions post email filtering
            int intLanguageId = GlobalFunctions.GetLanguageId();
            int intCurrencyId = GlobalFunctions.GetCurrencyId();
            string Register_GuestCheckout_Error1_Message = string.Empty, Register_GuestCheckout_Error2_Message = string.Empty;
            lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
            if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
            {
                lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    Register_GuestCheckout_Error1_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_GuestCheckout_Error1_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    Register_GuestCheckout_Error2_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_GuestCheckout_Error2_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                }
            }
            string strMessage = string.Empty;
            int i = 0;
            #region Guest Checkout changes
            #region
            strMessage = Register_GuestCheckout_Error1_Message; //"There is some problem in guest user checkout. Please try again.";
            #region apostrophy changes by vikram start
            i = UserBL.CheckGuestUserExists(Constants.USP_CheckGuestUserExists, HttpUtility.HtmlEncode(strEmail.Trim()), HttpContext.Current.Session.SessionID);
            #endregion
            if (i == 0)
            {
                HttpContext.Current.Session["IsLogin"] = true;
                strMessage = "success";
                #region apostrophy changes by vikram start
                strEmail = GlobalFunctions.ReplaceEmailIdspecialCharactersbyDecoding(strEmail.Trim());
                HttpContext.Current.Session["GuestUser"] = HttpUtility.HtmlEncode(strEmail.Trim());
                #endregion apostrophy changes by vikram end
            }
            else if (i == -1)
            {
                //Customer already registered
                strMessage = Register_GuestCheckout_Error2_Message;//"You already have an active account. Please login or click on forgot password link to generate new password.";
            }
            else if (i == -2)
            {
                HttpContext.Current.Session["IsLogin"] = true;
                strMessage = "GuestUser";
                //Customer already registered as a guest user
                UserBE objBEs = new UserBE();
                UserBE objUser = new UserBE();
                #region apostrophy changes vikram
                strEmail = GlobalFunctions.ReplaceEmailIdspecialCharactersbyDecoding(strEmail.Trim());
                objBEs.EmailId = HttpUtility.HtmlEncode(strEmail.Trim());
                #endregion
                objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                if (objUser.IsGuestUser == true && objUser.IsPunchoutUser == true && string.IsNullOrEmpty(objUser.PredefinedColumn1) && string.IsNullOrEmpty(objUser.PredefinedColumn2))
                {
                    HttpContext.Current.Session["GuestUser"] = HttpUtility.HtmlEncode(strEmail.Trim());
                }
                else if(objUser != null || objUser.EmailId != null)
                {
                    HttpContext.Current.Session["User"] = objUser;
                    //if (strMessage == "success")
                        Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                }
            }
            else
            {
                strMessage = Register_GuestCheckout_Error1_Message; //"There is some problem in guest user checkout. Please try again.";
            }
            return strMessage;
            #endregion
            #endregion            
            #endregion
        }

        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "Registration");
                    if (lstMetaTags.Count > 0)
                    {
                        Page.Title = lstMetaTags[0].MetaContentTitle;
                        Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                        Page.MetaDescription = lstMetaTags[0].MetaDescription;
                    }
                    else
                    {
                        Page.Title = "";
                        Page.MetaKeywords = "";
                        Page.MetaDescription = "";
                    }
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region "UserType"
        private string SetCustomFields(UserBE obj)
        {
            string strError = "";
            try
            {
                #region rptUserTypeCustomFields
                foreach (RepeaterItem item in rptUserTypeCustomFields.Items)
                {
                    TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                    DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                    HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                    HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                    CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                    Label lblCustomFieldName = (Label)item.FindControl("lblCustomFieldName");

                    // Below LOC is added by SHRIGANESH SINGH 13 July 2016 to get the value each RegistrationFieldsConfigurationId for Validation
                    HiddenField hdnRegistrationFieldsConfigurationId = (HiddenField)item.FindControl("hdnRegistrationFieldsConfigurationId");

                    PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    object value = null;
                    if (fi != null)
                    {
                        try
                        {
                            #region "customColumns"
                            if (hdfControl.Value.ToLower() == "textbox")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields textbox.");
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                if (txtCustomFieldName != null)
                                {
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);

                                    // Below LOC is added by SHRIGANESH SINGH 13 July 2016 for Validating Custom Fields
                                    int bResult = RegistrationCustomFieldBL.ValidateRegistrationCustomFields(value, hdnRegistrationFieldsConfigurationId.Value);

                                    if (bResult == 0)
                                    {
                                        strError = strCustomFieldValidationError + " " + lblCustomFieldName.Text.Replace("*", "");
                                    }
                                    else if (bResult == 3)
                                    {
                                        strError = strGeneric_Existing_LoginFieldValue_Message.Replace("@fieldName",lblCustomFieldName.Text.Replace("*", ""));
                                    }
                                }
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                            }
                            else if (hdfControl.Value.ToLower() == "dropdown")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields dropdown.");
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                if (ddlCustomValue != null)
                                {
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                }
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                            }
                            else if (hdfControl.Value.ToLower() == "checkbox")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields checkbox.");
                                if (chkCustomValue != null)
                                {
                                    Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                    #region
                                    foreach (ListItem chkItem in chkCustomValue.Items)
                                    {
                                        if (chkItem.Selected)
                                        {
                                            if (iCount == 0)
                                            {
                                                value = chkItem.Value;
                                                iCount++;
                                            }
                                            else
                                            {
                                                value = value + "|" + chkItem.Value;
                                                iCount++;
                                            }
                                        }
                                    }
                                    #endregion
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                    Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                                }
                            }
                            #endregion
                            fi.SetValue(obj, value, null);
                        }
                        catch (InvalidCastException) { }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return strError;
        }

        protected void BindUserTypeMappping()
        {
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                objUserTypeMappingBE = UserTypesBL.getCollectionItemUserTypeMappingbyCId(Constants.USP_UserTypeMappingDetailsByCID, DictionaryInstance, true);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindUserSelection()
        {
            if (objUserTypeMappingBE != null)
            {
                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                if (objUserTypeMasterOptions != null)
                {
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 1)
                    {
                        List<UserTypeMappingBE.CustomUserTypes> objCustomUserTypes = objUserTypeMappingBE.lstCustomUserTypes.FindAll(x => x.LanguageID == GlobalFunctions.GetLanguageId());
                        BindPreferredCurrency(objCustomUserTypes[0].UserTypeID);
                        List<UserTypeMappingBE.CustomUserTypes> objCS = objCustomUserTypes.FindAll(x => x.UserTypeID != 1);
                        if (objCS.Count > 0)
                        {
                            ddlUserType.DataSource = objCS;// objCustomUserTypes.FindAll(x => x.UserTypeID != 1);
                            ddlUserType.DataTextField = "UserTypeName";
                            ddlUserType.DataValueField = "UserTypeID";
                            ddlUserType.DataBind();
                            GlobalFunctions.AddDropdownItem(ref ddlUserType);
                        }
                    }
                }

                #region "Default Custom Fields"
                List<UserTypeMappingBE.RegistrationFieldsConfigurationBE> objReggobjRegistrationFieldsConfigurationBEL = new List<UserTypeMappingBE.RegistrationFieldsConfigurationBE>();
                objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping = objUserTypeMappingBE.lstUserTypeCustomFieldMapping.FindAll(x => x.UserTypeID == 1);
                objTempUserTypeMappingBE.lstRegistrationLanguagesBE = objUserTypeMappingBE.lstRegistrationLanguagesBE.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId > 10);

                foreach (UserTypeMappingBE.UserTypeCustomFieldMapping obj in objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping)
                {
                    UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE1 = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == obj.RegistrationFieldsConfigurationId && x.IsVisible == true && x.IsStoreDefault == false);
                    if (objRegistrationFieldsConfigurationBE1 != null)
                    {
                        UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserTypeMappingBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                        objReggobjRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE);
                    }
                }
                if (objReggobjRegistrationFieldsConfigurationBEL.Count > 0)
                {
                    divUserType.Visible = true;
                    aLnkUserType.Visible = true;
                    alnkMkt.Visible = false;
                }
                rptCustomFieldsAll.DataSource = objReggobjRegistrationFieldsConfigurationBEL;
                rptCustomFieldsAll.DataBind();
                #endregion
            }
        }

        private void BindCustomFields()
        {
            try
            {
                if (objUserTypeMappingBE != null)
                {
                    List<UserTypeMappingBE.CustomUserTypes> objCustomUserTypes = objUserTypeMappingBE.lstCustomUserTypes.FindAll(x => x.LanguageID == GlobalFunctions.GetLanguageId());
                    List<UserTypeMappingBE.CustomUserTypes> objCS = objCustomUserTypes.FindAll(x => x.UserTypeID != 1);
                    if (objCS.Count > 0)
                    {                       
                        UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                        #region
                        if (objUserTypeMasterOptions != null)
                        {
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2 || objUserTypeMasterOptions.UserTypeMappingTypeID == 3 || objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                            {
                                divddlUserType.Visible = false;
                                reqddlUserType.Enabled = false;
                            }
                            else
                            {
                                divddlUserType.Visible = true;
                                reqddlUserType.Enabled = true;
                                divUserType.Visible = true;
                                aLnkUserType.Visible = true;
                                alnkMkt.Visible = false;
                            }
                            
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                            {
                                txtEmail.Attributes.Add("onchange", "javascript: ShowLoader();");
                                txtEmail.Attributes.Remove("onblur");
                                txtEmail.AutoPostBack = true;
                                txtEmail.TextChanged += new EventHandler(txtEmail_TextChanged);
                            }

                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                            {
                                foreach (RepeaterItem itm in rptInvoiceAddress.Items)
                                {
                                    DropDownList ddlRegisterCountry = (DropDownList)itm.FindControl("ddlRegisterCountry");
                                    if (ddlRegisterCountry != null)
                                    {
                                        ddlRegisterCountry.Attributes.Add("onchange", "javascript: ShowLoader();");
                                        txtEmail.AutoPostBack = false;
                                        ddlRegisterCountry.AutoPostBack = true;
                                    }
                                }
                            }

                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                            {
                                foreach(RepeaterItem itm in rptCustomMapping.Items)
                                {
                                }
                            }

                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 1)
                            {
                                ddlUserType.Attributes.Add("onchange", "javascript: ShowLoader();");
                            }

                            if (objUserTypeMasterOptions.UserTypeMappingTypeID != 2)
                            {
                                txtEmail.AutoPostBack = false;
                            }

                            if (objUserTypeMasterOptions.UserTypeMappingTypeID != 3)
                            {
                                foreach (RepeaterItem itm in rptInvoiceAddress.Items)
                                {
                                    DropDownList ddlRegisterCountry = (DropDownList)itm.FindControl("ddlRegisterCountry");
                                    if (ddlRegisterCountry != null)
                                    {
                                        ddlRegisterCountry.AutoPostBack = false;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region "rptUserTypeCustomFields"
                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                        {
                            BindCustomMapping();
                        }
                        #endregion        
                    }
                    else
                    {
                        ddlUserType.Visible = false;
                        lblUserTypes.Visible = false;
                        reqddlUserType.Enabled = false;
                    }                   
                }
                else
                {
                    GlobalFunctions.AddDropdownItem(ref ddlUserType);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindVisibleUserType(Convert.ToInt16(ddlUserType.SelectedValue));
                BindPreferredCurrency(Convert.ToInt16(ddlUserType.SelectedValue));
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptUserTypeCustomFields_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    #region
                    Int16 iRegId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                    UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserTypeMappingBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);
                    UserTypeMappingBE.RegistrationLanguagesBE objLang = objUserTypeMappingBE.lstRegistrationLanguagesBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId & x.LanguageId == GlobalFunctions.GetLanguageId());

                    HiddenField hdfColumnName = (HiddenField)e.Item.FindControl("hdfColumnName");
                    HiddenField hdfControl = (HiddenField)e.Item.FindControl("hdfControl");
                    Label lblCustomFieldName = (Label)e.Item.FindControl("lblCustomFieldName");
                    TextBox txtCustomFieldName = (TextBox)e.Item.FindControl("txtCustomFieldName");
                    DropDownList ddlCustomValue = (DropDownList)e.Item.FindControl("ddlCustomValue");
                    CheckBoxList chkCustomValue = (CheckBoxList)e.Item.FindControl("chkCustomValue");

                    // Below LOC added by SHRIGANESH SINGH 13 July 2016 to set the RegistrationFieldsConfigurationId for Validation
                    HiddenField hdnRegistrationFieldsConfigurationId = (HiddenField)e.Item.FindControl("hdnRegistrationFieldsConfigurationId");
                    hdnRegistrationFieldsConfigurationId.Value = Convert.ToString(iRegId);
                    RequiredFieldValidator reqtxtCustomFieldName = (RequiredFieldValidator)e.Item.FindControl("reqtxtCustomFieldName");
                    RequiredFieldValidator reqddlCustomValue = (RequiredFieldValidator)e.Item.FindControl("reqddlCustomValue");

                    if (hdfColumnName != null)
                    {
                        hdfColumnName.Value = objRegistrationFieldsConfigurationBE.SystemColumnName;
                    }
                    if (objRegistrationFieldsConfigurationBE.IsMandatory)
                    {
                        lblCustomFieldName.Text = "*" + objLang.LabelTitle;
                    }
                    else
                    {
                        lblCustomFieldName.Text = objLang.LabelTitle;
                    }
                    if (objRegistrationFieldsConfigurationBE.FieldType == 2)
                    {
                        #region "DropDownList"
                        hdfControl.Value = "dropdown";
                        ddlCustomValue.Visible = true;
                        Int16 oRegistrationFieldsConfigurationId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                        ddlCustomValue.DataSource = objUserTypeMappingBE.lstRegistrationFieldDataMasterBE.FindAll(x => x.RegistrationFieldsConfigurationId == oRegistrationFieldsConfigurationId);// && x.LanguageID == GlobalFunctions.GetLanguageId());
                        ddlCustomValue.DataTextField = "RegistrationFieldDataValue";
                        ddlCustomValue.DataValueField = "RegistrationFieldDataMasterId";
                        ddlCustomValue.DataBind();
                        txtCustomFieldName.Visible = false;
                        if (reqtxtCustomFieldName != null)
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqddlCustomValue != null)
                            {
                                reqddlCustomValue.Visible = true;
                                reqddlCustomValue.Enabled = true;
                                reqddlCustomValue.ErrorMessage = Generic_Select_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 1)
                    {
                        #region "TextBox"
                        hdfControl.Value = "textbox";
                        ddlCustomValue.Visible = false;
                        txtCustomFieldName.Visible = true;

                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)//! remove
                        {
                            if (reqtxtCustomFieldName != null)
                            {
                                reqtxtCustomFieldName.Visible = true;
                                reqtxtCustomFieldName.Enabled = true;
                                reqtxtCustomFieldName.ErrorMessage = Generic_Enter_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 3)
                    {
                        #region "CheckBox
                        hdfControl.Value = "checkbox";
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = true;
                            Int16 oRegistrationFieldsConfigurationId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                            chkCustomValue.DataSource = objUserTypeMappingBE.lstRegistrationFieldDataMasterBE.FindAll(x => x.RegistrationFieldsConfigurationId == oRegistrationFieldsConfigurationId);// && x.LanguageID == GlobalFunctions.GetLanguageId());
                            chkCustomValue.DataTextField = "RegistrationFieldDataValue";
                            chkCustomValue.DataValueField = "RegistrationFieldDataMasterId";
                            chkCustomValue.DataBind();
                        }
                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (txtCustomFieldName != null)
                        {
                            txtCustomFieldName.Visible = false;
                        }
                        if (ddlCustomValue != null)
                        {
                            ddlCustomValue.Visible = false;
                        }

                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (reqtxtCustomFieldName != null)
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 4)//Pending Custom Mapping
                    { }
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>        
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEmail_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtPassword.Attributes.Add("value", txtPassword.Text);
                txtConfirmPassword.Attributes.Add("value", txtConfirmPassword.Text);
                if (txtEmail.Text!="")
                {
                    if (!GlobalFunctions.IsValidEamilId(txtEmail.Text))
                    {
                        GlobalFunctions.ShowModalAlertMessages(this, strVaildEmailErrorMsg, AlertType.Warning);
                        return;
                    }
                }
                try
                {
                    string strEmailId = HttpUtility.HtmlEncode(txtEmail.Text.Trim());
                    string[] strdata = strEmailId.Split('@');
                    host = strdata[1];
                }
                catch (Exception)
                {
                    return;
                }
                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                if (objUserTypeMasterOptions != null)
                {
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                    {
                        #region "Email"
                        if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                        {
                            UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidation = objUserTypeMappingBE.lstUserTypeEmailValidation.FirstOrDefault(x => x.EmailId.ToLower() == txtEmail.Text.ToLower().Trim());
                            UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidationDomain = objUserTypeMappingBE.lstUserTypeEmailValidation.FirstOrDefault(x => x.Domain.ToLower() == host.ToLower());

                            if (objRegUserTypeEmailValidation != null)
                            {
                                divddlUserType.Visible = false;
                                reqddlUserType.Enabled = false;
                                BindVisibleUserType(objRegUserTypeEmailValidation.UserTypeId);
                                BindPreferredCurrency(objRegUserTypeEmailValidation.UserTypeId);
                            }
                            else if (objRegUserTypeEmailValidationDomain != null)
                            {
                                divddlUserType.Visible = false;
                                reqddlUserType.Enabled = false;
                                BindVisibleUserType(objRegUserTypeEmailValidationDomain.UserTypeId);
                                BindPreferredCurrency(objRegUserTypeEmailValidationDomain.UserTypeId);
                            }
                            else
                            {
                                UserTypeMappingBE.UserTypeDetails objRegUserTypeDetails = objUserTypeMappingBE.lstUserTypeDetails.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.IsWhiteList == false);
                                if (objRegUserTypeDetails != null)
                                {
                                    divddlUserType.Visible = false;
                                    reqddlUserType.Enabled = false;
                                    BindVisibleUserType(objRegUserTypeDetails.UserTypeID);
                                    BindPreferredCurrency(objRegUserTypeDetails.UserTypeID);
                                }
                                else
                                {
                                    divddlUserType.Visible = false;
                                    reqddlUserType.Enabled = false;
                                    rptUserTypeCustomFields.Visible = false;
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindVisibleUserType(Int16 iSUertype)
        {
            try
            {
                BindUserTypeMappping();
                #region "rptUserTypeCustomFields"
                List<UserTypeMappingBE.RegistrationFieldsConfigurationBE> objReggobjRegistrationFieldsConfigurationBEL = new List<UserTypeMappingBE.RegistrationFieldsConfigurationBE>();
                objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping = objUserTypeMappingBE.lstUserTypeCustomFieldMapping.FindAll(x => x.UserTypeID != 1 && x.UserTypeID == iSUertype);
                objTempUserTypeMappingBE.lstRegistrationLanguagesBE = objUserTypeMappingBE.lstRegistrationLanguagesBE.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId > 10);
                foreach (UserTypeMappingBE.UserTypeCustomFieldMapping obj in objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping)
                {
                    UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE1 = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == obj.RegistrationFieldsConfigurationId && x.IsVisible == true);
                    if (objRegistrationFieldsConfigurationBE1 != null)
                    {
                        UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserTypeMappingBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                        objReggobjRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE);
                    }
                }
                if (objReggobjRegistrationFieldsConfigurationBEL.Count > 0)
                {
                    divUserType.Visible = true;
                    aLnkUserType.Visible = true;
                    alnkMkt.Visible = false;
                    rptUserTypeCustomFields.Visible = true;
                }
                rptUserTypeCustomFields.DataSource = objReggobjRegistrationFieldsConfigurationBEL;
                rptUserTypeCustomFields.DataBind();
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptInvoiceAddress_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void ddlRegisterCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList drp = (DropDownList)sender;
                RepeaterItem itm = (RepeaterItem)drp.Parent;

                HiddenField hdfColumnName = (HiddenField)itm.FindControl("hdfColumnName");
                if (hdfColumnName != null)
                {
                    if (hdfColumnName.Value.ToLower() == "predefinedcolumn8")
                    {
                        UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                        if (objUserTypeMasterOptions != null)
                        {
                            if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                            {
                                #region "Country"
                                if (objUserTypeMasterOptions.UserTypeMappingTypeID == 3)
                                {
                                    UserTypeMappingBE.Country objRegCountry = objUserTypeMappingBE.lstCountry.FirstOrDefault(x => x.CountryId == Convert.ToInt16(drp.SelectedValue));
                                    if (objRegCountry != null)
                                    {
                                        if (objRegCountry.UserTypeID > 0)
                                        {
                                            BindVisibleUserType(objRegCountry.UserTypeID);
                                            BindPreferredCurrency(objRegCountry.UserTypeID);
                                        }
                                        else
                                        {
                                            divddlUserType.Visible = false;
                                            reqddlUserType.Enabled = false;                                           
                                            rptUserTypeCustomFields.Visible = false;                                            
                                        }
                                    }
                                    else
                                    {
                                        divddlUserType.Visible = false;
                                        reqddlUserType.Enabled = false;                                        
                                        rptUserTypeCustomFields.Visible = false;                                        
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowPnl2();", true);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindCustomMapping()
        {
            try
            {
                List<UserTypeMappingBE.RegistrationFieldsConfigurationBE> objReg = new List<UserTypeMappingBE.RegistrationFieldsConfigurationBE>();
                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                if (objUserTypeMasterOptions != null)
                {
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                    {
                        objReg = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FindAll(x => x.RegistrationFieldsConfigurationId == 11);

                        rptCustomMapping.DataSource = objReg;// objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FindAll(x => x.IsStoreDefault == true);
                        rptCustomMapping.DataBind();

                        foreach (RepeaterItem item in rptCustomMapping.Items)
                        {
                            TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                            txtCustomFieldName.AutoPostBack = true;
                            txtCustomFieldName.TextChanged += txtCustomFieldName_TextChanged;
                            txtCustomFieldName.Attributes.Add("onchange", "javascript: ShowLoader();");
                        }
                    }
                }
                if (objReg.Count > 0)
                {
                    divUserType.Visible = true;
                    aLnkUserType.Visible = true;
                    alnkMkt.Visible = false;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void txtCustomFieldName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                if (objUserTypeMasterOptions != null)
                {
                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                    {
                        #region "CustomFields For UserType"
                        Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields start.");
                        try
                        {
                            #region rptCustomMapping
                            foreach (RepeaterItem item in rptCustomMapping.Items)
                            {
                                TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                                DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                                HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                                HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                                CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                                Label lblCustomFieldName = (Label)item.FindControl("lblCustomFieldName");
                                PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                object value = null;
                                if (fi != null)
                                {
                                    try
                                    {
                                        #region "customColumns"
                                        if (hdfControl.Value.ToLower() == "textbox")
                                        {
                                            if (txtCustomFieldName != null)
                                            {
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);
                                            }
                                        }
                                        else if (hdfControl.Value.ToLower() == "dropdown")
                                        {
                                            if (ddlCustomValue != null)
                                            {
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);
                                            }
                                        }
                                        else if (hdfControl.Value.ToLower() == "checkbox")
                                        {
                                            if (chkCustomValue != null)
                                            {
                                                #region
                                                foreach (ListItem chkItem in chkCustomValue.Items)
                                                {
                                                    if (chkItem.Selected)
                                                    {
                                                        if (iCount == 0)
                                                        {
                                                            value = chkItem.Value;
                                                            iCount++;
                                                        }
                                                        else
                                                        {
                                                            value = value + "|" + chkItem.Value;
                                                            iCount++;
                                                        }
                                                    }
                                                }
                                                #endregion
                                                value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                            }
                                        }
                                        #endregion
                                    }
                                    catch (InvalidCastException) { }
                                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                                    {
                                        if (objUserTypeMappingBE != null)
                                        {
                                            Int16 iRegistrationFieldsConfigurationId = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.SystemColumnName == hdfColumnName.Value).RegistrationFieldsConfigurationId;
                                            UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objValRegistrationFieldDataMasterValidationBE = objUserTypeMappingBE.lstRegistrationFieldDataMasterValidationBE.FirstOrDefault(x => x.RegistrationFieldDataValue.ToLower() == Convert.ToString(value).ToLower());
                                            if (objValRegistrationFieldDataMasterValidationBE != null)
                                            {                                              
                                                iUserTypeID = Convert.ToInt16(objValRegistrationFieldDataMasterValidationBE.UserTypeID);                                             
                                            }
                                            else
                                            {
                                                UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objCustomFieldValidation = UserTypesBL.GetAllCustomFieldValidationRule(iRegistrationFieldsConfigurationId);
                                                if (objCustomFieldValidation.RegisterAsDefault)
                                                {
                                                    iUserTypeID = 1;
                                                }
                                                else if (objCustomFieldValidation.DeclineRegister)
                                                {
                                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strRegistrationDeclinedMessage + " " + lblCustomFieldName.Text, AlertType.Warning);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            BindVisibleUserType(iUserTypeID);
                            BindPreferredCurrency(iUserTypeID);
                            #endregion
                        }
                        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                        #endregion
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindPreferredLanguages()
        {
            try
            {
                List<StoreBE.StoreLanguageBE> GetStoreLanguages = StoreLanguageBL.GetActiveStoreLanguageDetails();
                if (GetStoreLanguages != null)
                {
                    if (GetStoreLanguages.Count > 0)
                    {
                        lblPreferredLanguage.Visible = true;
                        drpdwnPreferredLanguage.Visible = true;
                        drpdwnPreferredLanguage.DataSource = GetStoreLanguages;
                        drpdwnPreferredLanguage.DataValueField = "LanguageId";
                        drpdwnPreferredLanguage.DataTextField = "LanguageName";
                        drpdwnPreferredLanguage.DataBind();
                        GlobalFunctions.AddDropdownItem(ref drpdwnPreferredLanguage);
                        if (GetStoreLanguages.Count == 1)
                        {
                            lblPreferredLanguage.Visible = false;
                            drpdwnPreferredLanguage.Visible = false;
                            drpdwnPreferredLanguage.SelectedValue = Convert.ToString(GetStoreLanguages[0].LanguageId);
                        }
                    }
                    else
                    {
                        lblPreferredLanguage.Visible = false;
                        drpdwnPreferredLanguage.Visible = false;
                    }
                }
                else
                {
                    lblPreferredLanguage.Visible = false;
                    drpdwnPreferredLanguage.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindPreferredCurrency(int UserTypeId)
        {
            try
            {
                List<UserPreferredBE.UserTypeCurrency> GetCurrencyUserTypeWise = UserPreferredBL.GetCurrencyUserTypeWise(UserTypeId);
                if (GetCurrencyUserTypeWise != null)
                {
                    if (GetCurrencyUserTypeWise.Count > 0)
                    {
                        lblPreferredCurrency.Visible = true;
                        drpdwnPreferredCurrency.Visible = true;
                        drpdwnPreferredCurrency.DataSource = GetCurrencyUserTypeWise;
                        drpdwnPreferredCurrency.DataValueField = "CurrencyId";
                        drpdwnPreferredCurrency.DataTextField = "CurrencyName";
                        drpdwnPreferredCurrency.DataBind();
                        GlobalFunctions.AddDropdownItem(ref drpdwnPreferredCurrency);
                        if (GetCurrencyUserTypeWise.Count == 1)
                        {
                            lblPreferredCurrency.Visible = false;
                            drpdwnPreferredCurrency.Visible = false;
                            drpdwnPreferredCurrency.SelectedValue = Convert.ToString(GetCurrencyUserTypeWise[0].CurrencyId);
                        }
                    }
                    else
                    {
                        lblPreferredCurrency.Visible = false;
                        drpdwnPreferredCurrency.Visible = false;
                    }
                }
                else
                {
                    lblPreferredCurrency.Visible = false;
                    drpdwnPreferredCurrency.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion
    }
}