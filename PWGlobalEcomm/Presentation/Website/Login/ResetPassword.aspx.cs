﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Presentation
{
    public class Login_UserResetPassword : BasePage
    {
        protected global::System.Web.UI.WebControls.TextBox txtConfirmPassword, txtPassword, txtForgotEmail, txtCaptcha;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvForgot, dvReset, dvError, dvMsg;
        protected global::System.Web.UI.WebControls.Label lblMsg;
        protected global::System.Web.UI.WebControls.Button btnSubmitForgot, btnCancel, btnUpdate;

        #region  CODE ADDED BY SHRIGANESH SINGH For Password Policy 13 May 2016
        protected global::System.Web.UI.WebControls.RegularExpressionValidator regexPwdLentxtPassword, regexAlphaNumtxtPassword, regexAlphaNumSymtxtPassword;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator reqtxtPassword, reqtxtConfirmPassword;
        protected global::System.Web.UI.WebControls.CompareValidator cmpValidatorPwd;
        protected global::System.Web.UI.WebControls.Label lblPasswordPolicyMessage;
        protected global::System.Web.UI.WebControls.HiddenField hdnPasswordPolicyType, hdnMinPasswordLength; 
        #endregion


        Int16 UserId;
        string UserEmail = "";
        Int16 CurrencyId;

        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();

        /*Sachin Chauhan Start : 24 03 2016 : Private local variables to carry resource language data*/
        int intLanguageId;

        public string Forgot_Password_Text1;
        public string Forgot_Password_Text2;
        public string Forgot_Password_PanelHead;
        public string Forgot_Password_Email_Lbl;
        public string Forgot_Password_Email_Placeholder;
        public string Forgot_Password_Captcha_Text;
        public string Forgot_Password_Captcha_Placeholder;
        public string Forgot_Password_Submit_Btn,strInvalidEmailAddress;
        public string Forgot_Password_Cancel_Btn;
        public string strInvalidCaptchMsg, strEmailSendingFailedMsg, strRegistrationPasswordCheck,strSuccessfulEmailMessage,strRequestFailureMEssage,strResetPassword;
        public string strDesirePasswordText, strPassword, strConfirmPassword, strUpdate, strEmptyEmailAddressMessage, strEmptyCaptchaMessage, strPasswordUpdateMsg, strEmptyPasswordAndConfirmPasswordMessage;

        StoreBE lstStoreDetail = new StoreBE();
        string minPasswordLength = "0";
        protected string strMinPassLenErrorMsg, strAlphaNumPassReqErrorMsg, strAlphaNumSymPassReqErrorMsg;

        /*Sachin Chauhan End : 24 03 2016*/

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                
                #region  Added by SHRIGANESH SINGH to set Password Policy features 11 May 2016

                lstStoreDetail = StoreBL.GetStoreDetails();
                
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue != null)
                {
                    minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                    hdnMinPasswordLength.Value = minPasswordLength;
                }
                else
                {
                    minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].DefaultValue;
                    hdnMinPasswordLength.Value = minPasswordLength;
                }

                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].FeatureValue;
                }
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].FeatureValue;
                }
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].FeatureValue;
                }

                #endregion

                CurrencyId = GlobalFunctions.GetCurrencyId();
                if (!IsPostBack)
                {

                    if (Convert.ToString(Request.QueryString["Request"]) != "" || Convert.ToString(Request.QueryString["Request"]) != null)
                    {
                        dvForgot.Visible = false;
                        dvReset.Visible = true;
                        string str = GlobalFunctions.Decrypt(Convert.ToString(Request.QueryString["Request"]));
                        if (str != "")
                        {
                            string[] val = str.Split('+');
                            UserId = Convert.ToInt16(val[0]);
                            UserEmail = val[1].ToString();
                            VerifyRequest(UserEmail);
                        }
                        else
                        {
                            dvForgot.Visible = true;
                            dvReset.Visible = false;
                            //dvError.Visible = true;
                            dvMsg.Visible = false;
                        }
                    }
                    else
                    {
                        dvForgot.Visible = true;
                        dvReset.Visible = false;
                    }
                }
                /*Sachin Chauhan Start : 24 03 2016 : Bindling Language Resource Data*/
                BindResourceData();
                /*Sachin Chauhan End : 24 03 2016 */
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void VerifyRequest(string UserEmail)
        {
            try
            {
                UserBE lst = new UserBE();
                UserBE objBEs = new UserBE();
                objBEs.EmailId = UserEmail;
                objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                lst = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                if (lst != null || lst.EmailId != null)
                {
                    Session["UserLst"] = lst;
                    DateTime currentDateTime = lst.CurrentTimeStamp;
                    DateTime reqDateTime = lst.ResetTime;
                    int PasswordResetWaitTime = Convert.ToInt16(ConfigurationManager.AppSettings["PasswordResetWaitTime"]);
                    reqDateTime = reqDateTime.AddMinutes(PasswordResetWaitTime);
                    if (currentDateTime <= reqDateTime)
                    {
                        if (lst.IsResetActive)
                        {
                            dvReset.Visible = true;
                            dvForgot.Visible = false;
                            dvMsg.Visible = false;
                            dvError.Visible = false;
                            txtPassword.Text = "";
                            txtConfirmPassword.Text = "";
                        }
                        else
                        {
                            dvReset.Visible = false;
                            dvForgot.Visible = true;
                            dvMsg.Visible = false;
                            dvError.Visible = true;
                            txtForgotEmail.Text = "";
                            txtCaptcha.Text = "";
                            UserBE objBE = new UserBE();
                            objBE.UserId = lst.UserId;
                            int i = UserBL.ExecuteResetDetails(Constants.USP_InsertResetLoginDetails, true, objBE, "E");
                        }
                    }
                    else
                    {
                        dvReset.Visible = false;
                        dvForgot.Visible = true;
                        dvMsg.Visible = false;
                        dvError.Visible = true;
                        txtForgotEmail.Text = "";
                        txtCaptcha.Text = "";
                        UserBE objBE = new UserBE();
                        objBE.UserId = lst.UserId;
                        int i = UserBL.ExecuteResetDetails(Constants.USP_InsertResetLoginDetails, true, objBE, "E");
                    }
                }
                else
                {
                    dvReset.Visible = false;
                    dvForgot.Visible = true;
                    dvMsg.Visible = false;
                    dvError.Visible = true;
                    txtForgotEmail.Text = "";
                    txtCaptcha.Text = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSubmitForgot_Click(object sender, EventArgs e)
        {
            try
            {
                string theCode = Session["CaptchaText"].ToString();
                if (txtCaptcha.Text.ToLower() != theCode.ToLower())
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strInvalidCaptchMsg, AlertType.Warning);
                    txtCaptcha.Text = "";
                    return;
                }
                string EncryptStr = "";
                UserBE lst = new UserBE();
                UserBE objBEs = new UserBE();
                #region vikram for apostrophy
                //objBEs.EmailId = HttpUtility.HtmlEncode(Sanitizer.GetSafeHtmlFragment(txtForgotEmail.Text.Trim()));
                objBEs.EmailId = HttpUtility.HtmlEncode(txtForgotEmail.Text.Trim());
                #endregion
                objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                lst = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);

                if (lst != null && lst.EmailId != null && lst.IsGuestUser == false && lst.IsActive == true)
                {

                    #region vikram for apostrophy
                    string emailid;
                    emailid = GlobalFunctions.ReplaceEmailIdspecialCharactersbyDecoding(lst.EmailId);
                    EncryptStr = lst.UserId + "+" + emailid;
                    #endregion
                    //EncryptStr = lst.UserId + "+" + lst.EmailId;
                    EncryptStr = GlobalFunctions.Encrypt(EncryptStr);
                    string Subject = "";
                    StringBuilder MailBody = new StringBuilder();
                    string ToAddress = lst.EmailId;
                    string FromAddress = "";
                    string CC = "";
                    string BCC = "";
                    bool IsBodyHTML = true;
                    string url = GlobalFunctions.GetVirtualPath() + "Login/ResetPassword.aspx?Request=" + EncryptStr;
                    List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                    EmailManagementBE objEmail = new EmailManagementBE();
                    objEmail.EmailTemplateName = "Forgot password";
                    objEmail.LanguageId = GlobalFunctions.GetLanguageId();
                    lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                    if (lstEmail != null && lstEmail.Count > 0)
                    {
                        Subject = lstEmail[0].Subject;
                        FromAddress = lstEmail[0].FromEmailId;
                        CC = lstEmail[0].CCEmailId;
                        BCC = lstEmail[0].BCCEmailId;
                        IsBodyHTML = lstEmail[0].IsHtml;
                        MailBody.Append(lstEmail[0].Body.Replace("@UserName", lst.FirstName).Replace("@URL", url));
                        GlobalFunctions.SendEmail(ToAddress, FromAddress, "", CC, BCC, Subject, HttpUtility.HtmlDecode(MailBody.ToString()), "", "", IsBodyHTML);
                        UserBE objBE = new UserBE();
                        objBE.UserId = lst.UserId;
                        int i = UserBL.ExecuteResetDetails(Constants.USP_InsertResetLoginDetails, true, objBE, "I");
                        txtCaptcha.Text = "";
                        txtForgotEmail.Text = "";
                        //dvMsg.Visible = true;
                        lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string Forgot_pwd_Success = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_pwd_Success" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this, Forgot_pwd_Success, AlertType.Success);
                    }
                    else
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, strEmailSendingFailedMsg, AlertType.Failure); return; }
                }
                else
                {
                    lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                    string Forgot_pwd_Success = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_pwd_Success" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    txtCaptcha.Text = "";
                    txtForgotEmail.Text = "";
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Forgot_pwd_Success, AlertType.Success);
                    //return;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.RedirectToRoute("login-page");
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                string minPass = "5";
                bool flag = false;

                #region  Code Commented by SHRIGANESH SINGH 13 May 2016
                //if (objStoreBE != null && objStoreBE.StoreName != null)
                //{
                //    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue != null)
                //    {
                //        minPass = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue;
                //    }

                //    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_PasswordExpression").FeatureValues[0].FeatureValue.ToLower() == "yes")
                //    {
                //        flag = true;
                //    }
                //}
                //if (flag)
                //{
                //    string regExp = @"^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[\W_])\S{" + minPass + ",20}$";
                //    bool check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(txtPassword.Text.Trim()));
                //    if (!check)
                //    {
                //        GlobalFunctions.ShowModalAlertMessages(this.Page, strRegistrationPasswordCheck, AlertType.Warning);
                //        return;
                //    }
                //} 
                #endregion

                #region code added by SHRIGANESH SINGH for Password Policy 13 May 2016

                if (lstStoreDetail != null)
                {
                    if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue != null)
                    {
                        minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                    }
                    else
                    {
                        minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].DefaultValue;
                    }

                    if (objStoreBE != null)
                    {
                        lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();

                        if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                        {
                            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                            {
                                string password = txtPassword.Text;
                                minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                if (password.Length < Convert.ToInt16(minPasswordLength))
                                {
                                    string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    strPwdError = strPwdError.Replace("@Num@", minPass);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                    txtCaptcha.Text = "";
                                    return;
                                }
                            }

                            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                            {
                                string password = txtPassword.Text;
                                minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                if (password.Length >= Convert.ToInt16(minPasswordLength))
                                {
                                    string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]{0,50}";
                                    bool check = true;
                                    check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                    if (!check)
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        txtCaptcha.Text = "";
                                        return;
                                    }
                                }
                                else
                                {
                                    string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    strPwdError = strPwdError.Replace("@Num@", minPass);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                    txtCaptcha.Text = "";
                                    return;
                                }
                            }

                            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                            {
                                string password = txtPassword.Text;
                                minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                if (password.Length >= Convert.ToInt16(minPasswordLength))
                                {
                                    string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                                    bool check = true;
                                    check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                    if (!check)
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        txtCaptcha.Text = "";
                                        return;
                                    }
                                }
                                else
                                {
                                    string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    strPwdError = strPwdError.Replace("@Num@", minPass);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                    txtCaptcha.Text = "";
                                    return;
                                }
                            }
                        }
                    }
                }

                #endregion

                UserBE lst = (UserBE)Session["UserLst"];
                string SaltPass = SaltHash.ComputeHash((txtPassword.Text.Trim()), "SHA512", null);
                UserBE objBE = new UserBE();
                objBE.UserId = lst.UserId;
                objBE.Password = SaltPass;
                int i = UserBL.ExecuteResetDetails(Constants.USP_InsertResetLoginDetails, true, objBE, "U");
                string Subject = "";
                StringBuilder MailBody = new StringBuilder();
                string ToAddress = lst.EmailId;
                string FromAddress = "";
                string CC = "";
                string BCC = "";
                bool IsBodyHTML = true;
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = "Change Passwords";
                objEmail.LanguageId = GlobalFunctions.GetLanguageId();
                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    Subject = lstEmail[0].Subject;
                    FromAddress = lstEmail[0].FromEmailId;
                    CC = lstEmail[0].CCEmailId;
                    BCC = lstEmail[0].BCCEmailId;
                    IsBodyHTML = lstEmail[0].IsHtml;
                    MailBody.Append(lstEmail[0].Body.Replace("@UserName", lst.FirstName + " " + lst.LastName).Replace("@Email", lst.EmailId));
                    GlobalFunctions.SendEmail(ToAddress, FromAddress, "", CC, BCC, Subject,HttpUtility.HtmlDecode(MailBody.ToString()), "", "", IsBodyHTML);
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPasswordUpdateMsg, "../signin", AlertType.Success);
                }
               // Response.RedirectToRoute("login-page");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindResourceData()
        {
            intLanguageId = GlobalFunctions.GetLanguageId();

            try
            {

                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        Forgot_Password_Text1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_Text1").ResourceValue;
                        Forgot_Password_Text2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_Text2").ResourceValue;
                        Forgot_Password_PanelHead = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_PanelHead").ResourceValue;
                        Forgot_Password_Email_Lbl = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_Email_Lbl").ResourceValue;
                        Forgot_Password_Email_Placeholder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_Email_Placeholder").ResourceValue;
                        
                        txtForgotEmail.Attributes.Add("placeholder",Forgot_Password_Email_Placeholder);

                        Forgot_Password_Captcha_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_Captcha_Text").ResourceValue;
                        Forgot_Password_Captcha_Placeholder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_Captcha_Placeholder").ResourceValue;
                        
                        txtCaptcha.Attributes.Add("placeholder",Forgot_Password_Captcha_Placeholder);

                        Forgot_Password_Submit_Btn = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_Submit_Btn").ResourceValue;
                        btnSubmitForgot.Text = Forgot_Password_Submit_Btn;

                        
                        Forgot_Password_Cancel_Btn = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_Password_Cancel_Btn").ResourceValue;
                        btnCancel.Text = Forgot_Password_Cancel_Btn;
                        strInvalidCaptchMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_Captcha_Message").ResourceValue;
                        strEmailSendingFailedMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Email_Sending_Failed_Msg").ResourceValue;
                        strRegistrationPasswordCheck = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_Password_Check").ResourceValue;
                        Forgot_Password_Submit_Btn = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_ForgotPassword").ResourceValue;
                        strInvalidEmailAddress = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Invalid").ResourceValue + " " + lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SignIn_Email").ResourceValue; 

                        #region MyRegion CODE ADDED BY SHRIGANESH SINGH 06 April 2016 

                        btnSubmitForgot.ToolTip = Forgot_Password_Submit_Btn;
                        btnCancel.ToolTip = Forgot_Password_Cancel_Btn;
                        strSuccessfulEmailMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Forgot_pwd_Success").ResourceValue;
                        strRequestFailureMEssage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Request_Failure_Message").ResourceValue;
                        strResetPassword = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Reset_Password_Text").ResourceValue;
                        strDesirePasswordText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Desire_Password_Text").ResourceValue;
                        strPassword = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password_Text").ResourceValue;
                        string PasswordPlaceHolder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password_Text").ResourceValue;
                        txtPassword.Attributes.Add("placeholder", PasswordPlaceHolder);
                        txtPassword.ToolTip = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password_Text").ResourceValue;
                        string ConfirmPasswordPlaceHolder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Confirm_Password_Text").ResourceValue;
                        txtConfirmPassword.Attributes.Add("placeholder", ConfirmPasswordPlaceHolder);
                        txtConfirmPassword.ToolTip = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Confirm_Password_Text").ResourceValue;
                        strConfirmPassword = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Confirm_Password_Text").ResourceValue;
                        btnUpdate.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Submit_Text").ResourceValue;
                        btnUpdate.ToolTip = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Submit_Text").ResourceValue;
                        strEmptyEmailAddressMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_EmailID_Message").ResourceValue;
                        strEmptyCaptchaMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_Captcha_Error_Message").ResourceValue;
                        strEmptyPasswordAndConfirmPasswordMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_Password_ConfirmPassword_Message").ResourceValue;
                        strPasswordUpdateMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Update_Password_success").ResourceValue; 
                       
                        #endregion
                    }
                }

                #region Code Added by SHRIGANESH SINGH 13 May 2016 for Password Policy

                if(lstStoreDetail != null)
                {
                    if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                    {
                        regexPwdLentxtPassword.Enabled = true;

                        regexAlphaNumtxtPassword.Enabled = false;
                        regexAlphaNumSymtxtPassword.Enabled = false;

                        regexPwdLentxtPassword.ValidationExpression = ".{" + minPasswordLength + ",50}";

                        string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPasswordLength);
                        regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;

                    }

                    if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                    {
                        regexPwdLentxtPassword.Enabled = true;
                        regexAlphaNumtxtPassword.Enabled = true;
                        regexAlphaNumSymtxtPassword.Enabled = false;

                        regexPwdLentxtPassword.ValidationExpression = ".{" + minPasswordLength + ",50}";
                        string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPasswordLength);
                        regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;

                        regexAlphaNumtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]{0,50}";
                        regexAlphaNumtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    }

                    if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                    {
                        regexPwdLentxtPassword.Enabled = true;
                        regexAlphaNumtxtPassword.Enabled = false;
                        regexAlphaNumSymtxtPassword.Enabled = true;

                        regexPwdLentxtPassword.ValidationExpression = ".{" + minPasswordLength + ",50}";
                        string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPasswordLength);
                        regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;

                        regexAlphaNumSymtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                        regexAlphaNumSymtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    }
                    reqtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_EnterPwd_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    reqtxtConfirmPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_EnterConfirmPwd_Msg" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    cmpValidatorPwd.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_MisMatchPwd_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                    strMinPassLenErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    strMinPassLenErrorMsg = strMinPassLenErrorMsg.Replace("@Num@", minPasswordLength);

                    strAlphaNumPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    strAlphaNumSymPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                }

                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }
}