﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;

namespace Presentation
{
    public class Errors_PageNotFound : System.Web.UI.Page
    {
        public string host = GlobalFunctions.GetVirtualPath();
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
        public string Errorpage_page_Title, Errorpage_pageSubTitle, Errorpage_pageText1, Errorpage_pageText2, Errorpage_pageText3, Errorpage_pageText4,
                      Errorpage_pageText5, Errorpage_pageText6, Errorpage_pageText7, Errorpage_pageText8, Errorpage_pageText9, Errorpage_pageSubTitle2, Errorpage_pageText10, Errorpage_pageSubTitle5
                      , Errorpage_pageText11, Errorpage_pageText12, Errorpage_pageText13, ContactUs_Contact_Us_Title;
        protected void Page_Load(object sender, EventArgs e)
        {
            BindResourceData();
        }
        public void BindResourceData()
        {
            Int16 intLanguageId = GlobalFunctions.GetLanguageId();
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        Errorpage_page_Title = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_page_Title").ResourceValue;
                        Errorpage_pageSubTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageSubTitle").ResourceValue;
                        Errorpage_pageText1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText1").ResourceValue;
                        Errorpage_pageText2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText2").ResourceValue;
                        Errorpage_pageText3 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText3").ResourceValue;
                        Errorpage_pageText4 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText4").ResourceValue;
                        Errorpage_pageText6 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText6").ResourceValue;
                        Errorpage_pageText7 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText7").ResourceValue;
                        Errorpage_pageText8 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText8").ResourceValue;
                        Errorpage_pageText9 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText9").ResourceValue;
                        Errorpage_pageSubTitle5 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageSubTitle5").ResourceValue;
                        Errorpage_pageSubTitle2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageSubTitle2").ResourceValue;
                        Errorpage_pageText10 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText10").ResourceValue;
                        Errorpage_pageText11 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText11").ResourceValue;
                        Errorpage_pageText12 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText12").ResourceValue;
                        Errorpage_pageText13 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Errorpage_pageText13").ResourceValue;
                        ContactUs_Contact_Us_Title = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "ContactUs_Contact_Us_Title").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}