﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Presentation
{
    public partial class MyFiles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpRuntime.Cache.Remove("StoreDetails");
            HttpRuntime.Cache.Remove("AllResourceData");
            HttpRuntime.Cache.Remove("FreightConfigurations");
            HttpRuntime.Cache.Remove("BreadCrumbs");
            Response.Cookies["CookieEnabled"].Expires = DateTime.Now.AddDays(-1);

            Session.Abandon();



            //string statusCode = string.Empty;                       // The variable which we will be storing the status code of the server response
            //string statusText = string.Empty;                       // The variable which we will be storing the status text of the server response
            //string shortUrl = string.Empty;                         // The variable which we will be storing the shortened url
            //string longUrl = string.Empty;                          // The variable which we will be storing the long url

            //string urlToShorten = "http://www.fluxbytes.com/";      // The url we want to shorten
            //XmlDocument xmlDoc = new XmlDocument();                 // The xml document which we will use to parse the response from the server

            //WebRequest request = WebRequest.Create("http://api.bitly.com/v3/shorten");
            //byte[] data = Encoding.UTF8.GetBytes(string.Format("login={0}&apiKey={1}&longUrl={2}&format={3}",
            // "bateamgroup@gmail.com",                             // Your username
            // "c7bb47c0c2132b5a3cb3a01a4ba68de7f40b5a5f",                              // Your API key
            // HttpUtility.UrlEncode(urlToShorten),         // Encode the url we want to shorten
            // "xml"));                                     // The format of the response we want the server to reply with

            //request.Method = "POST";
            //request.ContentType = "application/x-www-form-urlencoded";
            //request.ContentLength = data.Length;
            //using (Stream ds = request.GetRequestStream())
            //{
            //    ds.Write(data, 0, data.Length);
            //}
            //using (WebResponse response = request.GetResponse())
            //{
            //    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
            //    {
            //        xmlDoc.LoadXml(sr.ReadToEnd());
            //    }
            //}

            //statusCode = xmlDoc.GetElementsByTagName("status_code")[0].InnerText;
            //statusText = xmlDoc.GetElementsByTagName("status_txt")[0].InnerText;
            //shortUrl = xmlDoc.GetElementsByTagName("url")[0].InnerText;
            //longUrl = xmlDoc.GetElementsByTagName("long_url")[0].InnerText;

            //Console.WriteLine(statusCode);      // Outputs "200"
            //Console.WriteLine(statusText);      // Outputs "OK"
            //Console.WriteLine(shortUrl);        // Outputs "http://bit.ly/WVk1qN"
            //Console.WriteLine(longUrl);         // Outputs "http://www.fluxbytes.com/"
        }
    }
}