﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class MyAccount_StoreSelection : BasePage
    {
        string INDEEDSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\IndeedBrandStore_SSOLogs";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Exceptions.WriteSSOLog("---INSIDE PAGE LOAD OF MyAccount_StoreSelection--- ", INDEEDSSOLogFolderPath);
                if (Session["User"] != null)
                {
                    try
                    {
                        Exceptions.WriteSSOLog("---INSIDE IF--- ", INDEEDSSOLogFolderPath);
                        UserBE objUser = new UserBE();
                        List<UserBE.BA_USERFILE> objUserSAML = new List<UserBE.BA_USERFILE>();
                        objUser = Session["User"] as UserBE;
                        Exceptions.WriteSSOLog("---BEFORE GetSAMLbyEmployeeID--- ", INDEEDSSOLogFolderPath);
                        objUserSAML = UserBL.GetSAMLbyEmployeeID(objUser.CustomColumn2);
                        Exceptions.WriteSSOLog("---AFTER GetSAMLbyEmployeeID--- ", INDEEDSSOLogFolderPath);
                        string UserSAML = objUserSAML.FirstOrDefault(x => x.SAMLPostMessage != null).SAMLPostMessage;
                        Exceptions.WriteSSOLog("---UserSAML--- " + UserSAML, INDEEDSSOLogFolderPath);
                        string URL = GlobalFunctions.GetSetting("IndeedStoreSelectionSAMLPostURL");

                        HttpResponse response = HttpContext.Current.Response;
                        response.Clear();
                        StringBuilder s = new StringBuilder();
                        s.Append("<html>");
                        s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                        s.AppendFormat("<form name='form' action='{0}' method='post'>", URL);
                        s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", "SAMLResponse", UserSAML);
                        s.Append("</form></body></html>");
                        Exceptions.WriteSSOLog("---SAML REQUEST POST--- " + s.ToString(), INDEEDSSOLogFolderPath);
                        response.Write(s.ToString());
                        response.End();                     
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                    
                }  
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
                  
        }
    } 
}
