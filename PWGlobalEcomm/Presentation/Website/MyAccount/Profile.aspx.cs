﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using System.Threading;
using System.Text;
using System.Reflection;

namespace Presentation
{
    public class MyAccount_Profile : BasePage
    {
        static int UserId;
        static string EmailId;
        static int intLanguageId;
        static int intCurrencyId;
        static int UsertypeID;
        static int UserTitleID;
        static string UserTitle;
        int iCount = 0;
        protected static string errorD1, errorD2, errorD3, errorD4, errorD5, error1, error2, error3, error4, error5, error6, errorR1, errorR2, errorR3,
        errorR4, errorR5, errorR6, strCustomFieldValidationError;
        protected global::System.Web.UI.HtmlControls.HtmlSelect ddlRegCountry, ddlDelCountry;
        protected global::System.Web.UI.WebControls.TextBox txtFirstName, txtLastName, txtRegContactName, txtRegCompany, txtRegAddressLine1, txtRegAddressLine2,
        txtRegTown, txtRegCounty, txtRegPostCode, txtRegPhone, txtDelContactName, txtDelCompany, txtDelAddressLine1, txtDelAddressLine2, txtDelTown, txtDelCounty,
        txtDelPostCode, txtDelPhone, txtCaptcha, txtOldPassword, txtNewPassword, txtConfirmPassword, txtAddressTitle, txtTitles;
        protected global::System.Web.UI.WebControls.Label lblEmail, lblPwdLength, lblPasswordPolicyMessage, lblPreferredLanguage, lblPreferredCurrency;
        protected global::System.Web.UI.WebControls.CheckBox chkChangePassword, chkIsDefault, chkEditIsDefault;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnId;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl header, subHeader, Register_PersonalDetails, lblFName, lblLName, lblEmailText, section2,
        lblCPass, lblOPass, lblNPass, lblCNPass, section3, lblRName, lblRAddress1, lblRCompany, lblRAddress2, lblRTown, lblRCounty, lblRPostCode, lblRCountry,
        lblRPhone, section4, lblVerification, sectionModal1, lblDAddressName, lblDIsDefault, lblDName, lblDCompany, lblDAddress1, lblDAddress2, lblDTown, lblDCounty,
        lblDPostCode, lblDCountry, lblDPhone, lblTitle;
        static string invalidCaptcha, profileSuccess, currentPassCheck, passCheck, defaultAddressDelete;
        protected global::System.Web.UI.WebControls.TextBox txtEditAddressTitle, txtEditDeliveryCompany, txtEditDeliveryAdd1, txtEditDeliveryAdd2,
        txtEditDeliveryCity, txtEditDeliveryCounty,
        txtEditDeliveryPostal, txtEditPhone, txtEditContactName;
        public string Profile_ChangeEmail_Message, SiteLinks_ContactusTitle, Register_RegisteredAddress_Text,
                      Register_DeliveryAddress_Text, Register_Validate_Captcha, Profile_ChangePassword, Profile_PreferredDeliveryAddress,
                      Profile_SelectAddress, strRegister_txtSplChar_Message,strErrormsg_specialChar;

        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvDdlTitle;
        protected global::System.Web.UI.WebControls.DropDownList ddlPreferredDelAddress, ddlEditCountry, ddlAddDelCountry, drpdwnPreferredLanguage, drpdwnPreferredCurrency, ddlTitles;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl lblEditDeliveryContact, lblEditDeliveryAddressTitle, lblEditIsDefault,
        lblEditDeliveryAdd2, lblEditDeliveryAdd1, lblEditDeliveryCounty, lblEditDeliveryCity, lblEditDeliveryComapny, lblEditDeliveryPhone, lblEditDeliveryCountry,
        lblEditDeliveryPostCode;
        protected global::System.Web.UI.WebControls.RadioButton rdProfile_MarketingOption1, rdProfile_MarketingOption2;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvtxtOldPassword, rfvtxtNewPassword, rfvtxtConfirmPassword, reqtxtTitles, reqTxtFirstName, reqTxtLastName;
        protected global::System.Web.UI.WebControls.RegularExpressionValidator regtxtNewPassword, regtxtConfirmPassword, regexPwdLentxtPassword,
        regexAlphaNumtxtPassword, regexAlphaNumSymtxtPassword;
        protected global::System.Web.UI.WebControls.CompareValidator cmpValidatorPwd;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl hMarketing_Preferences;
        protected global::System.Web.UI.WebControls.Button btnChangePassword, btnConfirm, btnAddmodal;
        protected global::System.Web.UI.WebControls.HiddenField hdnPasswordPolicyType, hdnMinPasswordLength;

        protected global::System.Web.UI.WebControls.Repeater rptUserTypeCustomFields;//Added By Nilesh
        string CustomFieldValidationError = "";

        List<UserBE.UserDeliveryAddressBE> lstUserDelAddBE;
        UserBE.UserDeliveryAddressBE objUserDelAddBE = new UserBE.UserDeliveryAddressBE();
        StoreBE lstStoreDetail = new StoreBE();
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();

        UserTypeMappingBE objUserTypeMappingBE = new UserTypeMappingBE();
        UserTypeMappingBE objTempUserTypeMappingBE = new UserTypeMappingBE();

        protected string strMinPassLenErrorMsg, strAlphaNumPassReqErrorMsg, strAlphaNumSymPassReqErrorMsg, minPasswordLength, Generic_DelContactName_Req_Message,
        Generic_AddressTitle_Req_Message, Generic_DelAddressLine1_Req_Message, Generic_DelTown_Req_Message, Generic_DelPostalCode_Req_Message, NewAddTitleReqMsg,
        NewAddNameReqMsg, NewAddAddReqMsg, NewAddTownReqMsg, NewAddPostalReqMsg, RegFirstNameReqMsg, RegLastNameReqMsg, RegContactNameReqMsg, RegCompanyNameReqMsg,
        RegAddressLine1ReqMsg, RegTownReqMsg, RegPostalCodeReqMsg, Generic_Enter_Text, Generic_Select_Text, strGeneric_Existing_LoginFieldValue_Message, strRegister_PreferredCurrency_Message, strRegister_PreferredLanguages_Message, RegTitleMsgstrTitleReqMessage, strTitleReqMessage, strPreferredLanguageReqMessage,
                          strPreferredCurrencyReqMessage;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string host = GlobalFunctions.GetVirtualPath();
                string MaerskSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_OIL_SSOLogs";               

                if (Session["User"] == null)
                {
                    Exceptions.WriteSSOLog("Before redirecting to Login from PROFILE PAGE 1", MaerskSSOLogFolderPath);
                    Response.RedirectToRoute("login-page");
                    return;
                }
                BindResourceData();
                if (!IsPostBack)
                {
                    if (Session["User"] == null)
                    {
                        Exceptions.WriteSSOLog("Before redirecting to Login from PROFILE PAGE 2", MaerskSSOLogFolderPath);
                        Response.RedirectToRoute("login-page");
                        return;
                    }
                    else
                    {
                        UserBE user = Session["User"] as UserBE;
                        if (user.IsGuestUser == true)
                        {
                            Exceptions.WriteSSOLog("inside user.IsGuestUser == true PFOFILE PAGE LOAD", MaerskSSOLogFolderPath);
                            if (Session["AccessToken"] != null)
                            {
                                BindCountry();
                                BindUserDetails();
                                BindDropDownDeliveryAddress();
                                ReadMetaTagsData();
                                btnChangePassword.Visible = false;
                                lblCPass.Visible = false;
                            }
                            else if (Session["Maersk_SSO_MGIS_User"] != null)
                            {
                                Exceptions.WriteSSOLog("inside Maersk_SSO_MGIS_User GUEST USER TRUE ", MaerskSSOLogFolderPath);
                                BindCountry();
                                BindUserDetails();
                                BindDropDownDeliveryAddress();
                                ReadMetaTagsData();
                                btnChangePassword.Visible = false;
                                lblCPass.Visible = false;
                            }
                            else if (Session["Maersk_SSO_OIL_User"] != null)
                            {
                                Exceptions.WriteSSOLog("inside Maersk_SSO_OIL_User GUEST USER TRUE ", MaerskSSOLogFolderPath);
                                BindCountry();
                                BindUserDetails();
                                BindDropDownDeliveryAddress();
                                ReadMetaTagsData();
                                btnChangePassword.Visible = false;
                                lblCPass.Visible = false;
                            }
                            else
                            {
                                Exceptions.WriteSSOLog("Before redirecting to Login from ELSE ", MaerskSSOLogFolderPath);
                                Response.RedirectToRoute("login-page");
                                return;
                            }
                        }
                        else
                        {
                            if (Session["Maersk_SSO_MGIS_User"] != null)
                            {
                                btnChangePassword.Visible = false;
                                lblCPass.Visible = false;
                            }
                            if (Session["Maersk_SSO_MGIS_User"] != null)
                            {
                                btnChangePassword.Visible = false;
                                lblCPass.Visible = false;
                            }
                            if (Session["MichelinSSO"] != null)
                            {
                                btnChangePassword.Visible = false;
                                lblCPass.Visible = false;
                            }
                            BindCountry();
                            BindUserDetails();
                            BindDropDownDeliveryAddress();
                            ReadMetaTagsData();
                        }
                    }
                }
                BindPasswordPolicy(); // Added by SHRIGANESH SINGH for Password Policy 17 May 2016
                txtOldPassword.Attributes.Add("value", txtOldPassword.Text);
                txtNewPassword.Attributes.Add("value", txtNewPassword.Text);
                txtConfirmPassword.Attributes.Add("value", txtConfirmPassword.Text);

                #region  Added by SHRIGANESH SINGH to set Password Policy features 17 May 2016

                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue != null)
                {
                    minPasswordLength = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                    hdnMinPasswordLength.Value = minPasswordLength;
                }

                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].FeatureValue;
                }
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].FeatureValue;
                }
                if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                {
                    hdnPasswordPolicyType.Value = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].FeatureValue;
                }
                #endregion
                if (!IsPostBack)
                {
                    #region preferred currency & languages
                    if (Session["User"] != null)
                    {
                        UserBE lstUser = (UserBE)Session["User"];
                        BindPreferredLanguages(lstUser.UserPreferredLanguageID);
                        BindPreferredCurrency(lstUser.UserTypeID, lstUser.UserPreferredCurrencyID);
                    }
                    #endregion
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }        

        /// <summary>
        /// Author      : SHRIGANESH SINGH
        /// Date        : 17 May 2016
        /// Description : To set up Regular Expression Validator as per store setting
        /// </summary>
        private void BindPasswordPolicy()
        {
            lstStoreDetail = StoreBL.GetStoreDetails();

            #region Added by SHRIGANESH SINGH For Minimum password length
            string minPass = "0";
            if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue != null)
            {
                minPass = lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue;
            }
            else if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue != null)
            {
                minPass = lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue;
            }
            #endregion

            #region Code Added by SHRIGANESH SINGH 17 May 2016 for Password Policy

            if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
            {
                regexPwdLentxtPassword.Enabled = true;

                regexAlphaNumtxtPassword.Enabled = false;
                regexAlphaNumSymtxtPassword.Enabled = false;

                regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";

                string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
            }

            if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
            {
                regexPwdLentxtPassword.Enabled = true;
                regexAlphaNumtxtPassword.Enabled = true;
                regexAlphaNumSymtxtPassword.Enabled = false;
                regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
                regexAlphaNumtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                regexAlphaNumtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
            }

            if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
            {
                regexPwdLentxtPassword.Enabled = true;
                regexAlphaNumtxtPassword.Enabled = false;
                regexAlphaNumSymtxtPassword.Enabled = true;
                regexPwdLentxtPassword.ValidationExpression = ".{" + minPass + ",50}";
                string strGeneric_Minimum_Password_Length_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                strGeneric_Minimum_Password_Length_Message = strGeneric_Minimum_Password_Length_Message.Replace("@Num@", minPass);
                regexPwdLentxtPassword.ErrorMessage = strGeneric_Minimum_Password_Length_Message;
                regexAlphaNumSymtxtPassword.ValidationExpression = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                regexAlphaNumSymtxtPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
            }

            strMinPassLenErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
            strMinPassLenErrorMsg = strMinPassLenErrorMsg.Replace("@Num@", minPass);
            strAlphaNumPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
            strAlphaNumSymPassReqErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
            #endregion
        }

        private void BindCountry()
        {
            try
            {
                List<CountryBE> lstCountry = GlobalFunctions.GetCountries();
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    var source = new Dictionary<int, string>();
                    for (int i = 0; i < lstCountry.Count; i++)
                    { source.Add(Convert.ToInt32(lstCountry[i].CountryId), Convert.ToString(lstCountry[i].CountryName)); }
                    ddlRegCountry.DataSource = source;
                    ddlRegCountry.DataTextField = "value";
                    ddlRegCountry.DataValueField = "key";
                    ddlRegCountry.DataBind();
                    ddlRegCountry.Items.Insert(0, "-- Select --");
                    ddlDelCountry.DataSource = source;
                    ddlDelCountry.DataTextField = "value";
                    ddlDelCountry.DataValueField = "key";
                    ddlDelCountry.DataBind();
                    ddlDelCountry.Items.Insert(0, "-- Select --");
                    ddlEditCountry.DataSource = source;
                    ddlEditCountry.DataTextField = "value";
                    ddlEditCountry.DataValueField = "key";
                    ddlEditCountry.DataBind();
                    ddlEditCountry.Items.Insert(0, "-- Select --");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindUserDetails()
        {
            try
            {
                if (Session["User"] != null)
                {
                    UserBE lstUser = (UserBE)Session["User"];
                    UserId = lstUser.UserId;
                    EmailId = HttpUtility.HtmlDecode(lstUser.EmailId);
                    lblEmail.Text = HttpUtility.HtmlDecode(lstUser.EmailId);
                    txtFirstName.Text = HttpUtility.HtmlDecode(lstUser.FirstName);
                    txtLastName.Text = HttpUtility.HtmlDecode(lstUser.LastName);
                    txtRegContactName.Text = HttpUtility.HtmlDecode(lstUser.PredefinedColumn1);
                    txtRegCompany.Text = HttpUtility.HtmlDecode(lstUser.PredefinedColumn2);
                    txtRegAddressLine1.Text = HttpUtility.HtmlDecode(lstUser.PredefinedColumn3);
                    txtRegAddressLine2.Text = HttpUtility.HtmlDecode(lstUser.PredefinedColumn4);
                    txtRegTown.Text = HttpUtility.HtmlDecode(lstUser.PredefinedColumn5);
                    txtRegCounty.Text = HttpUtility.HtmlDecode(lstUser.PredefinedColumn6);
                    txtRegPostCode.Text = HttpUtility.HtmlDecode(lstUser.PredefinedColumn7);
                    ddlRegCountry.Value = HttpUtility.HtmlDecode(lstUser.PredefinedColumn8);
                    ddlDelCountry.Value = HttpUtility.HtmlDecode(lstUser.PredefinedColumn8);
                    txtRegPhone.Text = HttpUtility.HtmlDecode(lstUser.PredefinedColumn9);
                    UsertypeID = lstUser.UserTypeID;
                    txtTitles.Text = lstUser.UserTitle;
                    if (lstUser.IsMarketing)
                    {
                        rdProfile_MarketingOption1.Checked = true;
                        rdProfile_MarketingOption2.Checked = false;
                    }
                    else
                    {
                        rdProfile_MarketingOption1.Checked = false;
                        rdProfile_MarketingOption2.Checked = true;
                    }                    
                    BindVisibleUserType(Convert.ToInt16(UsertypeID));
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindResourceData()
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                lstStoreDetail = StoreBL.GetStoreDetails();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        lblTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Reg_Titles" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        header.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_Header" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        subHeader.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_SubHeader" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_PersonalDetails.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PersonalDetails" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblFName.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_FirstName_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblLName.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_LastName_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblEmailText.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Email_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblCPass.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_ChangePassword" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblOPass.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_OldPassword" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblNPass.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_NewPassword" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblCNPass.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_ConfirmNewPass" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblRName.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_ContactName_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblRCompany.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Company_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblRAddress1.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Address1_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblRAddress2.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Address2_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblRTown.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Town_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblRCounty.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_County_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblRPostCode.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PostCode_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblRCountry.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Country_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblRPhone.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Phone_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        section4.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_DeliveryAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblVerification.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_VerificationCode_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        sectionModal1.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_DeliveryAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblDAddressName.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_AddressTitle" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblDIsDefault.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_IsDefault" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblDName.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_ContactName_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblDCompany.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Company_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblDAddress1.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Address1_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblDAddress2.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Address2_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblDTown.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Town_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblDCounty.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_County_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblDPostCode.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PostCode_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblDCountry.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Country_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblDPhone.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Phone_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        error1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_FirstName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        error2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_LastName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        error3 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        error4 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password/ConfirmPassword_Mismatch_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorR1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegContactName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorR2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegCompanyName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorR3 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegAddressLine1_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorR4 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegTown_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorR5 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegPostalCode_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorR6 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegCountry_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorD1 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelContactName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorD2 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelAddressLine1_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorD3 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelTown_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorD4 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelPostalCode_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        errorD5 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelCountry_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        error6 = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_CaptchaCode_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        invalidCaptcha = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Login_Captcha_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        profileSuccess = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_Success" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        currentPassCheck = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_Incorrect_Password" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        passCheck = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_Password_Check" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        defaultAddressDelete = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_Default_Delivery_Address_Delete" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblEditDeliveryContact.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_ContactName_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblEditDeliveryAddressTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_AddressTitle" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblEditIsDefault.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_IsDefault" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblEditDeliveryAdd1.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Address1_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblEditDeliveryAdd2.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Address2_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblEditDeliveryCounty.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_County_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblEditDeliveryCity.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Town_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblEditDeliveryComapny.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Company_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblEditDeliveryPhone.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Phone_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblEditDeliveryPostCode.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PostCode_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue + "*";
                        lblEditDeliveryCountry.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Country_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        rdProfile_MarketingOption1.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_MarketingOption1" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        rdProfile_MarketingOption2.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_MarketingOption2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        rfvtxtOldPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_EnterPwd_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        rfvtxtNewPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password/ConfirmPassword_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        rfvtxtConfirmPassword.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Password/ConfirmPassword_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        cmpValidatorPwd.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_MisMatchPwd_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        hMarketing_Preferences.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Marketing_Preferences" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        /*Sachin Chauhan Start : 25 03 2016 : Variables to save resource languages titles*/
                        Profile_ChangeEmail_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_ChangeEmail_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        SiteLinks_ContactusTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_ContactusTitle" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnChangePassword.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_ChangePassword" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnConfirm.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_SaveAllChanges" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_RegisteredAddress_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_RegisteredAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_DeliveryAddress_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_DeliveryAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Register_Validate_Captcha = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Validate_Captcha" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Profile_ChangePassword = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_ChangePassword" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        btnAddmodal.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_AddNewAddress" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Profile_PreferredDeliveryAddress = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_PreferredDeliveryAddress" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Profile_SelectAddress = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_SelectAddress" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        /*Sachin Chauhan End */
                        strRegister_txtSplChar_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_txtSplChar_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_txtSplChar_Message = GlobalFunctions.ReplaceSingleQuote(strRegister_txtSplChar_Message);
                        #region Resources Added by SHRIGANESH SINGH 02 June 2016

                        #region Registered Details
                        RegFirstNameReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_FirstName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        RegLastNameReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_LastName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        RegContactNameReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegContactName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        RegCompanyNameReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegCompanyName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        RegAddressLine1ReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegAddressLine1_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        RegTownReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegTown_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        RegPostalCodeReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegPostalCode_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_txtSplChar_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_txtSplChar_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_txtSplChar_Message = GlobalFunctions.ReplaceSingleQuote(strRegister_txtSplChar_Message);
                        #endregion
                        #region Update Address
                        Generic_DelContactName_Req_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelContactName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Generic_AddressTitle_Req_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AddressTitle_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Generic_DelAddressLine1_Req_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelAddressLine1_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Generic_DelTown_Req_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelTown_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        Generic_DelPostalCode_Req_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelPostalCode_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        #region Add Address
                        NewAddTitleReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AddressTitle_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        NewAddNameReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelContactName_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        NewAddAddReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelAddressLine1_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        NewAddTownReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelTown_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        NewAddPostalReqMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DelPostalCode_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        #region "User Type /*User Type*/"
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cr_usertype").FeatureValues[0].IsEnabled == true)
                        {
                            strCustomFieldValidationError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_invalidcustomfield_error_message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        }
                        #endregion
                        strGeneric_Existing_LoginFieldValue_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "generic_existing_loginfieldvalue_message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        lblPreferredCurrency.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredCurrency_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblPreferredLanguage.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredLanguages_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_PreferredCurrency_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredCurrency_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegister_PreferredLanguages_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_PreferredLanguages_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqtxtTitles.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ddlTitle_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strTitleReqMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ddlTitle_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        reqTxtFirstName.ErrorMessage = error1;
                        reqTxtLastName.ErrorMessage = error2;
                        strErrormsg_specialChar = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Editprofile_SpecialChar_Error" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            bool validspecialchar = true;
            try
            {
                BindUserTypeMappping();
                StringBuilder strErrorList = new StringBuilder();
                UserBE lstUser;
                string strIsMarketing;
                if (Session["User"] != null)
                {
                    #region Code Added by SHRIGANESH 03 June 2016 for All Fields Validation
                                        
                    if (txtFirstName.Text == null || txtFirstName.Text == "")
                    {
                        strErrorList.AppendLine(RegFirstNameReqMsg);
                        strErrorList.Append("<br/>");
                    }

                    if (txtLastName.Text == null || txtLastName.Text == "")
                    {
                        strErrorList.AppendLine(RegLastNameReqMsg);
                        strErrorList.Append("<br/>");
                    }

                    // Registration Details
                    if (txtRegContactName.Text == null || txtRegContactName.Text == "")
                    {
                        strErrorList.AppendLine(RegContactNameReqMsg);
                        strErrorList.Append("<br/>");
                    }

                    if (txtRegCompany.Text == null || txtRegCompany.Text == "")
                    {
                        strErrorList.AppendLine(RegCompanyNameReqMsg);
                        strErrorList.Append("<br/>");
                    }

                    if (txtRegAddressLine1.Text == null || txtRegAddressLine1.Text == "")
                    {
                        strErrorList.AppendLine(RegAddressLine1ReqMsg);
                        strErrorList.Append("<br/>");
                    }

                    if (txtRegTown.Text == null || txtRegTown.Text == "")
                    {
                        strErrorList.AppendLine(RegTownReqMsg);
                        strErrorList.Append("<br/>");
                    }

                    if (txtRegPostCode.Text == null || txtRegPostCode.Text == "")
                    {
                        strErrorList.AppendLine(RegPostalCodeReqMsg);
                        strErrorList.Append("<br/>");
                    }
                    if(ddlRegCountry.SelectedIndex==0)
                    {
                        strErrorList.AppendLine(errorR6);
                        strErrorList.Append("<br/>");
                    }

                    // Delivery Details
                    if (ddlPreferredDelAddress.SelectedIndex != 0)
                    {
                        if (txtEditAddressTitle.Text == null || txtAddressTitle.Text == "")
                        {
                            strErrorList.AppendLine(Generic_AddressTitle_Req_Message);
                            strErrorList.Append("<br/>");
                        }
                        if (txtEditContactName.Text == null || txtDelContactName.Text == "")
                        {
                            strErrorList.AppendLine(Generic_DelContactName_Req_Message);
                            strErrorList.Append("<br/>");
                        }
                        if (txtEditDeliveryAdd1.Text == null || txtDelAddressLine1.Text == "")
                        {
                            strErrorList.AppendLine(Generic_DelAddressLine1_Req_Message);
                            strErrorList.Append("<br/>");
                        }
                        if (txtEditDeliveryCity.Text == null || txtDelTown.Text == "")
                        {
                            strErrorList.AppendLine(Generic_DelTown_Req_Message);
                            strErrorList.Append("<br/>");
                        }
                        if (txtEditDeliveryPostal.Text == null || txtDelPostCode.Text == "")
                        {
                            strErrorList.AppendLine(Generic_DelPostalCode_Req_Message);
                            strErrorList.Append("<br/>");
                        }
                    }
                    if (String.IsNullOrEmpty(txtTitles.Text))
                    {
                        strErrorList.AppendLine(strTitleReqMessage);
                        strErrorList.Append("<br/>");
                    }
                    if (drpdwnPreferredLanguage.SelectedIndex == 0)
                    {
                        strErrorList.AppendLine(strRegister_PreferredLanguages_Message);
                        strErrorList.Append("<br/>");
                    }
                    if (drpdwnPreferredCurrency.SelectedIndex == 0)
                    {
                        strErrorList.AppendLine(strRegister_PreferredCurrency_Message);
                        strErrorList.Append("<br/>");
                    }
                    if (rdProfile_MarketingOption1.Checked == false && rdProfile_MarketingOption2.Checked == false)
                    {
                        strErrorList.AppendLine("Please select at least one marketing preferences");
                        strErrorList.Append("<br/>");
                    }

                    if (strErrorList.Length != 0)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Convert.ToString(strErrorList), AlertType.Warning);
                        return;
                    }                    
                    #endregion

                    lstUser = (UserBE)Session["User"];                                       
                    UserBE objBE = new UserBE();
                    objBE.UserId = Convert.ToInt16(lstUser.UserId);
                    objBE.FirstName = Sanitizer.GetSafeHtmlFragment(txtFirstName.Text.Trim());
                    objBE.LastName = Sanitizer.GetSafeHtmlFragment(txtLastName.Text.Trim());                                      
                    objBE.Password = lstUser.Password;
                    objBE.PredefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtRegContactName.Text.Trim());
                    objBE.PredefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtRegCompany.Text.Trim());
                    objBE.PredefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtRegAddressLine1.Text.Trim());
                    objBE.PredefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtRegAddressLine2.Text.Trim());
                    objBE.PredefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtRegTown.Text.Trim());
                    objBE.PredefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtRegCounty.Text.Trim());
                    objBE.PredefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtRegPostCode.Text.Trim());
                    objBE.PredefinedColumn8 = ddlRegCountry.Value;
                    objBE.PredefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtRegPhone.Text.Trim());
                    objBE.IPAddress = GlobalFunctions.GetIpAddress();
                    objBE.IsGuestUser = lstUser.IsGuestUser;
                    objBE.IsMarketing = rdProfile_MarketingOption1.Checked ? true : false;
                    objBE.UserTypeID = lstUser.UserTypeID;
                    objBE.UserTitle = Sanitizer.GetSafeHtmlFragment(txtTitles.Text.Trim());
                    CustomFieldValidationError = SetCustomFields(objBE);
                    if (CustomFieldValidationError != "")
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, CustomFieldValidationError, AlertType.Warning);
                        return;
                    }
                    int i = UserBL.ExecuteEditProfileDetails(Constants.USP_InsertUserEDITProfileDetails, true, objBE);
                    if (i > 0)
                    {
                        UserPreferredBE objUserPreferredBE = new UserPreferredBE();
                        objUserPreferredBE.UserId = i;
                        if (Convert.ToInt32(drpdwnPreferredCurrency.SelectedValue) > 0 && Convert.ToInt32(drpdwnPreferredLanguage.SelectedValue) > 0)
                        {
                            objUserPreferredBE.PreferredCurrencyId = Convert.ToInt32(drpdwnPreferredCurrency.SelectedValue);
                            objUserPreferredBE.PreferredLanguageId = Convert.ToInt32(drpdwnPreferredLanguage.SelectedValue);
                            UserPreferredBL.InsertUpdateUserPreference(Constants.usp_InsertUpdateUserPreferred, true, objUserPreferredBE);
                            List<UserPreferredBE> objUsersPreferredBE = UserPreferredBL.GetUserPreferred(i);
                            GlobalFunctions.SetLanguageId(Convert.ToInt16(objUsersPreferredBE[0].PreferredLanguageId));
                            GlobalFunctions.SetCurrencyId(Convert.ToInt16(objUsersPreferredBE[0].PreferredCurrencyId));
                            GlobalFunctions.SetCurrencySymbol(objUsersPreferredBE[0].CurrencySymbol);

                            UserBE objBEs = new UserBE();
                            UserBE objUser = new UserBE();
                            objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                            objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                            objBEs.EmailId = lstUser.EmailId;
                            objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                            if (objUser != null || objUser.EmailId != null)
                            {
                                Session["User"] = objUser;
                            }
                        }
                        GetUserDeliveryDetails();
                        GlobalFunctions.ShowModalAlertMessages(this.Page, profileSuccess, AlertType.Success);
                        #region Added By Hardik on "8/FEB/2017" for Communigator
                        Exceptions.WriteInfoLog("Inside btnConfirm_Click() - IsMarketing Radio checked - call Communigator");
                        lstUser = (UserBE)Session["User"];
                        strIsMarketing = RegisterCustomer_OASIS.IsMarketingUpdateContact(lstUser);
                        #endregion
                        return;
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }
        }

        protected void CheckSpecialCharactersAllFields()
        {
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtFirstName.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtFirstName.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtLastName.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtLastName.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtRegContactName.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtRegContactName.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtRegCompany.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtRegCompany.Focus();
                return;
            }
            #endregion

            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtRegAddressLine1.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtRegAddressLine1.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtRegTown.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtRegTown.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtRegPostCode.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtRegPostCode.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtEditAddressTitle.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtEditAddressTitle.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtEditContactName.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtEditContactName.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtEditDeliveryAdd1.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtEditDeliveryAdd1.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtEditDeliveryCity.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtEditDeliveryCity.Focus();
                return;
            }
            #endregion
            #region vikram for specialcharacters
            if (!GlobalFunctions.IsSpecialCharacters(txtEditDeliveryPostal.Text))
            {
                GlobalFunctions.ShowModalAlertMessages(this.Page, strRegister_txtSplChar_Message, AlertType.Warning);
                txtEditDeliveryPostal.Focus();
                return;
            }
            #endregion
        }

        private bool ValidatePassword(string regExp)
        {
            Regex reg = new Regex(regExp); //(regExp, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(Sanitizer.GetSafeHtmlFragment(txtNewPassword.Text.Trim()));
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            StringBuilder strErrorList = new StringBuilder();
            try
            {
                #region Code Added by SHRIGANESH 02 June 2016 for New Address Validation
                if (txtAddressTitle.Text == null || txtAddressTitle.Text == "")
                {
                    strErrorList.AppendLine(NewAddTitleReqMsg);
                    strErrorList.Append("<br/>");
                }
                if (txtDelContactName.Text == null || txtDelContactName.Text == "")
                {
                    strErrorList.AppendLine(NewAddNameReqMsg);
                    strErrorList.Append("<br/>");
                }
                if (txtDelAddressLine1.Text == null || txtDelAddressLine1.Text == "")
                {
                    strErrorList.AppendLine(NewAddAddReqMsg);
                    strErrorList.Append("<br/>");
                }
                if (txtDelTown.Text == null || txtDelTown.Text == "")
                {
                    strErrorList.AppendLine(NewAddTownReqMsg);
                    strErrorList.Append("<br/>");
                }
                if (txtDelPostCode.Text == null || txtDelPostCode.Text == "")
                {
                    strErrorList.AppendLine(NewAddPostalReqMsg);
                    strErrorList.Append("<br/>");
                }

                if (strErrorList.Length != 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Convert.ToString(strErrorList), AlertType.Warning);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:AddFunc();", true);
                    return;
                }

                #endregion

                UserBE objBEx = new UserBE();
                UserBE.UserDeliveryAddressBE objBE = new UserBE.UserDeliveryAddressBE();
                objBE.PreDefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtDelContactName.Text.Trim());
                objBE.PreDefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtDelCompany.Text.Trim());
                objBE.PreDefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtDelAddressLine1.Text.Trim());
                objBE.PreDefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtDelAddressLine2.Text.Trim());
                objBE.PreDefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtDelTown.Text.Trim());
                objBE.PreDefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtDelCounty.Text.Trim());
                objBE.PreDefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtDelPostCode.Text.Trim());
                objBE.PreDefinedColumn8 = ddlDelCountry.Value;
                objBE.PreDefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtDelPhone.Text.Trim());
                objBE.IsDefault = chkIsDefault.Checked;
                objBE.AddressTitle = Sanitizer.GetSafeHtmlFragment(txtAddressTitle.Text.Trim());
                objBE.UserId = Convert.ToInt16(UserId);
                objBE.DeliveryAddressId = Convert.ToInt16(hdnId.Value);
                objBEx.UserDeliveryAddress.Add(objBE);
                int i = UserBL.ExecuteProfileDeliveryAddressDetails(Constants.USP_InsertUpdateDeleteUserDeliveryAddress, true, objBEx, "Insert");
                if (i > 0)
                {
                    GetUserDeliveryDetails();
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/InsertedMessage"), AlertType.Success);
                    BindDropDownDeliveryAddress();
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Warning);
                    return;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            // }
        }

        protected void GetUserDeliveryDetails()
        {
            try
            {
                UserBE lst = new UserBE();
                UserBE objBEs = new UserBE();
                objBEs.EmailId = EmailId;
                objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                lst = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                if (lst != null && lst.EmailId != null)
                {
                    Session["User"] = lst;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void EditDeliveryAddress()
        {
            if (Session["User"] != null)
            {
                UserBE lstUser = (UserBE)Session["User"];
                objUserDelAddBE.UserId = lstUser.UserId;
                objUserDelAddBE.DeliveryAddressId = Convert.ToInt16(ddlPreferredDelAddress.SelectedValue);
            }
            hdnId.Value = ddlPreferredDelAddress.SelectedValue;
            lstUserDelAddBE = UserBL.GetUserDeliveryAddres(objUserDelAddBE);
            txtEditAddressTitle.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].AddressTitle);
            txtEditDeliveryAdd1.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn3);
            txtEditDeliveryAdd2.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn4);
            txtEditDeliveryCity.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn5);
            txtEditDeliveryCompany.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn2);
            txtEditDeliveryPostal.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn7);
            ddlEditCountry.SelectedValue = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn8);
            txtEditDeliveryCounty.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn6);
            txtEditContactName.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn1);
            if (lstUserDelAddBE[0].IsDefault)
            {
                chkEditIsDefault.Checked = true;
            }
            else
            {
                chkEditIsDefault.Checked = false;
            }
            txtEditPhone.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn9);
            /*PATCH DONE FOR UPDATE PAGE WITH DELIVERY ADDRESS*/
            txtAddressTitle.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].AddressTitle);
            txtDelContactName.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn1);
            txtDelAddressLine1.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].AddressTitle);
            txtDelTown.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn5);
            txtDelPostCode.Text = HttpUtility.HtmlDecode(lstUserDelAddBE[0].PreDefinedColumn7);
        }

        protected void BindDropDownDeliveryAddress()
        {

            if (Session["User"] != null)
            {
                UserBE lstUser = (UserBE)Session["User"];
                objUserDelAddBE.UserId = lstUser.UserId;
                objUserDelAddBE.DeliveryAddressId = 0;
            }
            lstUserDelAddBE = UserBL.GetUserDeliveryAddres(objUserDelAddBE);
            ddlPreferredDelAddress.DataSource = lstUserDelAddBE;
            ddlPreferredDelAddress.DataValueField = "DeliveryAddressId";
            ddlPreferredDelAddress.DataTextField = "AddressTitle";
            ddlPreferredDelAddress.DataBind();
            ddlPreferredDelAddress.Items.Insert(0, new ListItem(Profile_SelectAddress, "0"));

            #region "Added by Sripal"
            if (lstUserDelAddBE != null)
            {
                UserBE.UserDeliveryAddressBE objUserDeliveryAddressBE = lstUserDelAddBE.FirstOrDefault(x => x.IsDefault == true);
                if (objUserDeliveryAddressBE != null)
                {
                    ddlPreferredDelAddress.SelectedValue = objUserDeliveryAddressBE.DeliveryAddressId.ToString();
                    ddlPreferredDelAddress_SelectedIndexChanged(ddlPreferredDelAddress, null);
                }
                else
                {
                    if (lstUserDelAddBE.Count == 1)
                    {
                        ddlPreferredDelAddress.SelectedValue = lstUserDelAddBE[0].DeliveryAddressId.ToString();
                        ddlPreferredDelAddress_SelectedIndexChanged(ddlPreferredDelAddress, null);
                    }
                }
            }
            #endregion
        }

        protected void btnUpdateAddress_Click(object sender, EventArgs e)
        {
            StringBuilder strErrorList = new StringBuilder();
            try
            {
                #region Code Added by SHRIGANESH 02 June 2016 for Update Address Validation

                if (txtEditAddressTitle.Text == null || txtAddressTitle.Text == "")
                {
                    strErrorList.AppendLine(Generic_AddressTitle_Req_Message);
                    strErrorList.Append("<br/>");
                }
                if (txtEditContactName.Text == null || txtDelContactName.Text == "")
                {
                    strErrorList.AppendLine(Generic_DelContactName_Req_Message);
                    strErrorList.Append("<br/>");
                }
                if (txtEditDeliveryAdd1.Text == null || txtDelAddressLine1.Text == "")
                {
                    strErrorList.AppendLine(Generic_DelAddressLine1_Req_Message);
                    strErrorList.Append("<br/>");
                }
                if (txtEditDeliveryCity.Text == null || txtDelTown.Text == "")
                {
                    strErrorList.AppendLine(Generic_DelTown_Req_Message);
                    strErrorList.Append("<br/>");
                }
                if (txtEditDeliveryPostal.Text == null || txtDelPostCode.Text == "")
                {
                    strErrorList.AppendLine(Generic_DelPostalCode_Req_Message);
                    strErrorList.Append("<br/>");
                }

                if (strErrorList.Length != 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Convert.ToString(strErrorList), AlertType.Warning);
                    return;
                }

                #endregion

                UserBE objBEx = new UserBE();
                UserBE.UserDeliveryAddressBE objBE = new UserBE.UserDeliveryAddressBE();
                objBE.PreDefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtEditContactName.Text.Trim());
                objBE.PreDefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtEditDeliveryCompany.Text.Trim());
                objBE.PreDefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtEditDeliveryAdd1.Text.Trim());
                objBE.PreDefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtEditDeliveryAdd2.Text.Trim());
                objBE.PreDefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtEditDeliveryCity.Text.Trim());
                objBE.PreDefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtEditDeliveryCounty.Text.Trim());
                objBE.PreDefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtEditDeliveryPostal.Text.Trim());
                objBE.PreDefinedColumn8 = Sanitizer.GetSafeHtmlFragment(ddlEditCountry.Text);
                objBE.PreDefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtEditPhone.Text.Trim());

                if (chkEditIsDefault.Checked)
                    objBE.IsDefault = true;
                else
                    objBE.IsDefault = false;
                objBE.AddressTitle = Sanitizer.GetSafeHtmlFragment(txtEditAddressTitle.Text.Trim());
                objBE.UserId = Convert.ToInt16(UserId);
                objBE.DeliveryAddressId = Convert.ToInt16(hdnId.Value);
                objBEx.UserDeliveryAddress.Add(objBE);
                int i = UserBL.ExecuteProfileDeliveryAddressDetails(Constants.USP_InsertUpdateDeleteUserDeliveryAddress, true, objBEx, "Update");
                if (i > 0)
                {
                    GetUserDeliveryDetails();
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/UpdatedMessage"), AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                    return;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnRemoveAddress_Click(object sender, EventArgs e)
        {
            try
            {
                UserBE objBEx = new UserBE();
                UserBE.UserDeliveryAddressBE objBE = new UserBE.UserDeliveryAddressBE();
                objBE.PreDefinedColumn1 = Sanitizer.GetSafeHtmlFragment(txtDelContactName.Text.Trim());
                objBE.PreDefinedColumn2 = Sanitizer.GetSafeHtmlFragment(txtDelCompany.Text.Trim());
                objBE.PreDefinedColumn3 = Sanitizer.GetSafeHtmlFragment(txtDelAddressLine1.Text.Trim());
                objBE.PreDefinedColumn4 = Sanitizer.GetSafeHtmlFragment(txtDelAddressLine2.Text.Trim());
                objBE.PreDefinedColumn5 = Sanitizer.GetSafeHtmlFragment(txtDelTown.Text.Trim());
                objBE.PreDefinedColumn6 = Sanitizer.GetSafeHtmlFragment(txtDelCounty.Text.Trim());
                objBE.PreDefinedColumn7 = Sanitizer.GetSafeHtmlFragment(txtDelPostCode.Text.Trim());
                objBE.PreDefinedColumn8 = "0";
                objBE.PreDefinedColumn9 = Sanitizer.GetSafeHtmlFragment(txtDelPhone.Text.Trim());
                objBE.IsDefault = chkIsDefault.Checked;
                objBE.AddressTitle = Sanitizer.GetSafeHtmlFragment(txtAddressTitle.Text.Trim());
                objBE.UserId = Convert.ToInt16(UserId);
                objBE.DeliveryAddressId = Convert.ToInt16(hdnId.Value);
                objBEx.UserDeliveryAddress.Add(objBE);
                int i = UserBL.ExecuteProfileDeliveryAddressDetails(Constants.USP_InsertUpdateDeleteUserDeliveryAddress, true, objBEx, "Delete");
                if (i > 0)
                {
                    GetUserDeliveryDetails();
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SuccessMessage/DeletedMessage"), AlertType.Success);
                    BindDropDownDeliveryAddress();
                }
                else if (i == -1)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, defaultAddressDelete, AlertType.Failure);
                    return;
                }
                else if (i == -2)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, defaultAddressDelete, AlertType.Failure);
                    GetUserDeliveryDetails();
                    return;
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, Exceptions.GetException("SystemError/GeneralMessage"), AlertType.Failure);
                    return;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void ddlPreferredDelAddress_SelectedIndexChanged(object sender, EventArgs e)
        {
            EditDeliveryAddress();
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:deliveryAddressChanged();", true);
        }

        protected void btnAddmodal_Click(object sender, EventArgs e)
        {
            ddlDelCountry.Value = ddlRegCountry.Value;
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:AddFunc();", true);
        }

        protected void btnPwdSave_Click(object sender, EventArgs e)
        {
            try
            {
                UserBE lstUser;
                if (Session["User"] != null)
                {
                    lstUser = (UserBE)Session["User"];

                    #region Code Added by SHRIGANESH SINGH 26 May 2016 to fetch updated password everytime
                    UserBE lst = new UserBE();
                    UserBE objBE = new UserBE();
                    objBE.UserId = Convert.ToInt16(lstUser.UserId);
                    objBE.EmailId = EmailId;
                    objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    objBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    lst = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBE);
                    lstUser.Password = lst.Password;
                    #endregion
                    intLanguageId = GlobalFunctions.GetLanguageId();
                    intCurrencyId = GlobalFunctions.GetCurrencyId();
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    string minPass = "5";
                    string minPasswordLength = "5";
                    bool flag = false;

                    #region Added by Sripal "For Minimum password length"
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue != null)
                    {
                        minPass = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].FeatureDefaultValue;
                    }
                    else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue != null)
                    {
                        minPass = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_MinimumPasswordLength").FeatureValues[0].DefaultValue;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_PasswordExpression").FeatureValues[0].IsEnabled == true)
                    {
                        flag = true;
                    }
                    if (flag)
                    {
                        bool check = false;
                        if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_PasswordExpression").FeatureValues[0].IsEnabled == true)
                        {
                            string regExp = @"^[a-zA-Z]{" + minPass + ",50}$";
                            check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(txtNewPassword.Text.Trim()));
                            if (!check)
                            {
                                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                                string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Pwd_Alpha" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                txtCaptcha.Text = "";
                                return;
                            }
                        }
                        else if (lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PS_PasswordExpression").FeatureValues[1].IsEnabled == true)
                        {
                            string regExp = @"^[a-zA-Z0-9]{" + minPass + ",50}$";
                            check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(txtNewPassword.Text.Trim()));
                            if (!check)
                            {
                                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                                string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Pwd_AlphaNumeric" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                txtCaptcha.Text = "";
                                return;
                            }
                        }
                        else
                        {
                            string regExp = @"([a-zA-Z0-9\W]){" + minPass + ",50}";// @"^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[\W_])\S{" + minPass + ",50}$";
                            check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(txtNewPassword.Text.Trim()));
                            if (!check)
                            {
                                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                                string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Pwd_AlphaNumericSymbol" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                txtCaptcha.Text = "";
                                return;
                            }
                        }
                    }

                    #endregion

                    #region Code Added by SHRIGANESH SINGH for Password Policy 17 May 2016

                    if (objStoreBE != null)
                    {
                        lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();

                        if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                        {
                            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[0].IsEnabled == true)
                            {
                                string password = txtNewPassword.Text;
                                minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                if (password.Length < Convert.ToInt16(minPasswordLength))
                                {
                                    string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                    txtCaptcha.Text = "";
                                    return;
                                }
                            }

                            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[1].IsEnabled == true)
                            {
                                string password = txtNewPassword.Text;
                                minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                if (password.Length >= Convert.ToInt16(minPasswordLength))
                                {
                                    string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                                    bool check = true;
                                    check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                    if (!check)
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        txtCaptcha.Text = "";
                                        return;
                                    }
                                }
                                else
                                {
                                    string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                    txtCaptcha.Text = "";
                                    return;
                                }
                            }

                            if (objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_passwordpolicy").FeatureValues[2].IsEnabled == true)
                            {
                                string password = txtNewPassword.Text;
                                minPasswordLength = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "ps_minimumpasswordlength").FeatureValues[0].FeatureDefaultValue;
                                if (password.Length >= Convert.ToInt16(minPasswordLength))
                                {
                                    string regExp = @"^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]{0,50}";
                                    bool check = true;
                                    check = GlobalFunctions.ValidatePassword(regExp, Sanitizer.GetSafeHtmlFragment(password));
                                    if (!check)
                                    {
                                        string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_AlphaNumSymPwd_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                        txtCaptcha.Text = "";
                                        return;
                                    }
                                }
                                else
                                {
                                    string strPwdError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Minimum_Password_Length_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                    strPwdError = strPwdError.Replace("@Num@", minPasswordLength);
                                    GlobalFunctions.ShowModalAlertMessages(this.Page, strPwdError, AlertType.Warning);
                                    txtCaptcha.Text = "";
                                    return;
                                }
                            }
                        }
                    }

                    #endregion

                    if (SaltHash.ComputeHash((txtOldPassword.Text.Trim()), "SHA512", null) != lstUser.Password)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, currentPassCheck, AlertType.Warning);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "NewPwd", "showPwd();", true);
                        txtCaptcha.Text = "";
                        return;
                    }

                    string SaltPass = SaltHash.ComputeHash((txtNewPassword.Text.Trim()), "SHA512", null);
                    objBE.Password = SaltPass;
                    bool res = UserBL.UpdatePassword(objBE);
                    if (res)
                    {
                        lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strmsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Update_Password_success" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this, strmsg, AlertType.Success);
                    }
                    else
                    {
                        lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        string strmsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Update_Password_error" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        GlobalFunctions.ShowModalAlertMessages(this, strmsg, AlertType.Success);
                        ddlPreferredDelAddress_SelectedIndexChanged(sender, null);
                    }
                }
                else
                {
                    Response.RedirectToRoute("login-page");
                    return;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "Edit Profile");
                    if (lstMetaTags.Count > 0)
                    {
                        Page.Title = lstMetaTags[0].MetaContentTitle;
                        Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                        Page.MetaDescription = lstMetaTags[0].MetaDescription;
                    }
                    else
                    {
                        Page.Title = "";
                        Page.MetaKeywords = "";
                        Page.MetaDescription = "";
                    }
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region Start Added By Nilesh

        private void BindVisibleUserType(Int16 iSUertype)
        {
            try
            {
                BindUserTypeMappping();
                #region "rptUserTypeCustomFields"
                List<UserTypeMappingBE.RegistrationFieldsConfigurationBE> objReggobjRegistrationFieldsConfigurationBEL = new List<UserTypeMappingBE.RegistrationFieldsConfigurationBE>();
                UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                if (objUserTypeMasterOptions.UserTypeMappingTypeID == 4)
                {
                    objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping = objUserTypeMappingBE.lstUserTypeCustomFieldMapping.FindAll(x => x.UserTypeID > 0 && x.UserTypeID == iSUertype && x.RegistrationFieldsConfigurationId > 10);
                    objTempUserTypeMappingBE.lstRegistrationLanguagesBE = objUserTypeMappingBE.lstRegistrationLanguagesBE.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId > 10);
                }
                else
                {
                    objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping = objUserTypeMappingBE.lstUserTypeCustomFieldMapping.FindAll(x => x.UserTypeID > 0 && (x.UserTypeID == iSUertype || x.UserTypeID == 1) && x.RegistrationFieldsConfigurationId > 11);
                    objTempUserTypeMappingBE.lstRegistrationLanguagesBE = objUserTypeMappingBE.lstRegistrationLanguagesBE.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId > 11);
                }

                foreach (UserTypeMappingBE.UserTypeCustomFieldMapping obj in objTempUserTypeMappingBE.lstUserTypeCustomFieldMapping)
                {
                    UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE1 = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == obj.RegistrationFieldsConfigurationId && x.IsVisible == true);
                    if (objRegistrationFieldsConfigurationBE1 != null)
                    {
                        UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserTypeMappingBE.RegistrationFieldsConfigurationBE();
                        objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                        objReggobjRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE1);
                    }
                }
                if (objReggobjRegistrationFieldsConfigurationBEL.Count > 0)
                {
                    rptUserTypeCustomFields.Visible = true;
                }
                rptUserTypeCustomFields.DataSource = objReggobjRegistrationFieldsConfigurationBEL;
                rptUserTypeCustomFields.DataBind();
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void BindUserTypeMappping()
        {
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                objUserTypeMappingBE = UserTypesBL.getCollectionItemUserTypeMappingbyCId(Constants.USP_UserTypeMappingDetailsByCID, DictionaryInstance, true);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptUserTypeCustomFields_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    #region
                    Int16 iRegId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                    UserTypeMappingBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserTypeMappingBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE = objUserTypeMappingBE.lstRegistrationFieldsConfigurationBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);
                    UserTypeMappingBE.RegistrationLanguagesBE objLang = objUserTypeMappingBE.lstRegistrationLanguagesBE.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);
                    HiddenField hdfColumnName = (HiddenField)e.Item.FindControl("hdfColumnName");
                    HiddenField hdfControl = (HiddenField)e.Item.FindControl("hdfControl");
                    Label lblCustomFieldName = (Label)e.Item.FindControl("lblCustomFieldName");
                    TextBox txtCustomFieldName = (TextBox)e.Item.FindControl("txtCustomFieldName");
                    DropDownList ddlCustomValue = (DropDownList)e.Item.FindControl("ddlCustomValue");
                    CheckBoxList chkCustomValue = (CheckBoxList)e.Item.FindControl("chkCustomValue");
                    HiddenField hdnRegistrationFieldsConfigurationId = (HiddenField)e.Item.FindControl("hdnRegistrationFieldsConfigurationId");
                    hdnRegistrationFieldsConfigurationId.Value = Convert.ToString(iRegId);
                    RequiredFieldValidator reqtxtCustomFieldName = (RequiredFieldValidator)e.Item.FindControl("reqtxtCustomFieldName");
                    RequiredFieldValidator reqddlCustomValue = (RequiredFieldValidator)e.Item.FindControl("reqddlCustomValue");
                    if (hdfColumnName != null)
                    {
                        hdfColumnName.Value = objRegistrationFieldsConfigurationBE.SystemColumnName;
                    }
                    if (objRegistrationFieldsConfigurationBE.IsMandatory)
                    {
                        lblCustomFieldName.Text = "*" + objLang.LabelTitle;
                    }
                    else
                    {
                        lblCustomFieldName.Text = objLang.LabelTitle;
                    }

                    if (objRegistrationFieldsConfigurationBE.FieldType == 2)
                    {
                        #region "DropDownList"
                        hdfControl.Value = "dropdown";
                        ddlCustomValue.Visible = true;
                        Int16 oRegistrationFieldsConfigurationId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                        ddlCustomValue.DataSource = objUserTypeMappingBE.lstRegistrationFieldDataMasterBE.FindAll(x => x.RegistrationFieldsConfigurationId == oRegistrationFieldsConfigurationId);// && x.LanguageID == GlobalFunctions.GetLanguageId());
                        ddlCustomValue.DataTextField = "RegistrationFieldDataValue";
                        ddlCustomValue.DataValueField = "RegistrationFieldDataMasterId";
                        ddlCustomValue.DataBind();
                        txtCustomFieldName.Visible = false;
                        if (reqtxtCustomFieldName != null)
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqddlCustomValue != null)
                            {
                                reqddlCustomValue.Visible = true;
                                reqddlCustomValue.Enabled = true;
                                reqddlCustomValue.ErrorMessage = Generic_Select_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                        DictionaryInstance.Add("ColumnName", Convert.ToString(hdfColumnName.Value));
                        DictionaryInstance.Add("UserId", Convert.ToString(UserId));
                        string strCustomValue = "";
                        strCustomValue = UserBL.GetCustomColumnValue(Constants.USP_GetCustomColumnValue, DictionaryInstance, true);
                        ddlCustomValue.SelectedValue = strCustomValue;
                        if (objRegistrationFieldsConfigurationBE.IsUserEditable)
                        {
                            ddlCustomValue.Enabled = true;
                        }
                        else
                        {
                            ddlCustomValue.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 1)
                    {
                        #region "TextBox"
                        hdfControl.Value = "textbox";
                        ddlCustomValue.Visible = false;
                        txtCustomFieldName.Visible = true;

                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)//! remove
                        {
                            if (reqtxtCustomFieldName != null)
                            {
                                reqtxtCustomFieldName.Visible = true;
                                reqtxtCustomFieldName.Enabled = true;
                                reqtxtCustomFieldName.ErrorMessage = Generic_Enter_Text + " " + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                        DictionaryInstance.Add("ColumnName", Convert.ToString(hdfColumnName.Value));
                        DictionaryInstance.Add("UserId", Convert.ToString(UserId));
                        string strCustomValue = "";
                        strCustomValue = UserBL.GetCustomColumnValue(Constants.USP_GetCustomColumnValue, DictionaryInstance, true);
                        txtCustomFieldName.Text = strCustomValue;
                        if (objRegistrationFieldsConfigurationBE.IsUserEditable)
                        {
                            txtCustomFieldName.Enabled = true;
                        }
                        else
                        {
                            txtCustomFieldName.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 3)
                    {
                        #region "CheckBox
                        hdfControl.Value = "checkbox";
                        if (chkCustomValue != null)
                        {
                            chkCustomValue.Visible = true;
                            Int16 oRegistrationFieldsConfigurationId = ((UserTypeMappingBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                            chkCustomValue.DataSource = objUserTypeMappingBE.lstRegistrationFieldDataMasterBE.FindAll(x => x.RegistrationFieldsConfigurationId == oRegistrationFieldsConfigurationId);// && x.LanguageID == GlobalFunctions.GetLanguageId());
                            chkCustomValue.DataTextField = "RegistrationFieldDataValue";
                            chkCustomValue.DataValueField = "RegistrationFieldDataMasterId";
                            chkCustomValue.DataBind();
                        }
                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (txtCustomFieldName != null)
                        {
                            txtCustomFieldName.Visible = false;
                        }
                        if (ddlCustomValue != null)
                        {
                            ddlCustomValue.Visible = false;
                        }

                        if (reqddlCustomValue != null)
                        {
                            reqddlCustomValue.Visible = false;
                            reqddlCustomValue.Enabled = false;
                        }
                        if (reqtxtCustomFieldName != null)
                        {
                            reqtxtCustomFieldName.Visible = false;
                            reqtxtCustomFieldName.Enabled = false;
                        }
                        Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                        DictionaryInstance.Add("ColumnName", Convert.ToString(hdfColumnName.Value));
                        DictionaryInstance.Add("UserId", Convert.ToString(UserId));
                        string strCustomValue = "";
                        string[] strseperator = { "|" };
                        strCustomValue = UserBL.GetCustomColumnValue(Constants.USP_GetCustomColumnValue, DictionaryInstance, true);

                        string[] strValue = strCustomValue.Split(strseperator, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < strValue.Length; i++)
                        {
                            if (chkCustomValue.Items[i].Value == strValue[i].ToString())
                            {
                                chkCustomValue.Items[i].Selected = true;
                            }
                        }

                        if (objRegistrationFieldsConfigurationBE.IsUserEditable)
                        {
                            chkCustomValue.Enabled = true;
                        }
                        else
                        {
                            chkCustomValue.Enabled = false;
                        }
                        #endregion
                    }
                    else if (objRegistrationFieldsConfigurationBE.FieldType == 4)//Pending Custom Mapping
                    { }
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private string SetCustomFields(UserBE obj)
        {
            string strError = "";
            try
            {
                #region rptUserTypeCustomFields
                foreach (RepeaterItem item in rptUserTypeCustomFields.Items)
                {
                    TextBox txtCustomFieldName = (TextBox)item.FindControl("txtCustomFieldName");
                    DropDownList ddlCustomValue = (DropDownList)item.FindControl("ddlCustomValue");
                    HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");
                    HiddenField hdfControl = (HiddenField)item.FindControl("hdfControl");
                    CheckBoxList chkCustomValue = (CheckBoxList)item.FindControl("chkCustomValue");
                    Label lblCustomFieldName = (Label)item.FindControl("lblCustomFieldName");

                    // Below LOC is added by SHRIGANESH SINGH 13 July 2016 to get the value each RegistrationFieldsConfigurationId for Validation
                    HiddenField hdnRegistrationFieldsConfigurationId = (HiddenField)item.FindControl("hdnRegistrationFieldsConfigurationId");

                    PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    object value = null;
                    if (fi != null)
                    {
                        try
                        {
                            #region "customColumns"
                            if (hdfControl.Value.ToLower() == "textbox")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields textbox.");
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                if (txtCustomFieldName != null)
                                {
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtCustomFieldName.Text.Trim()), fi.PropertyType);

                                    // added by Snehal Jadhav 14 10 2016 for Validating Custom Fields for User
                                    int bResult = RegistrationCustomFieldBL.ValidateRegistrationCustomFieldsEditProfile(value, hdnRegistrationFieldsConfigurationId.Value, obj.UserId);

                                    if (bResult == 0)
                                    {
                                        //GlobalFunctions.ShowModalAlertMessages(this.Page, "Please enter a Valid Custom Field value against " + Convert.ToString(value), AlertType.Warning);
                                        strError = strCustomFieldValidationError + " " + lblCustomFieldName.Text.Replace("*", "");
                                        //return strError;
                                    }
                                    else if (bResult == 3)
                                    {
                                        strError = strError = strGeneric_Existing_LoginFieldValue_Message.Replace("@fieldName", lblCustomFieldName.Text.Replace("*", ""));
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strError, AlertType.Warning);
                                        //return;
                                    }
                                    else
                                    {
                                        Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                                        dictionaryInstance.Add("ColumnName", Convert.ToString(hdfColumnName.Value));
                                        dictionaryInstance.Add("ColumnNameValue", Convert.ToString(txtCustomFieldName.Text.Trim()));
                                        dictionaryInstance.Add("UserId", Convert.ToString(obj.UserId));
                                        bool Result = UserBL.UpdateCustomColumnValue(dictionaryInstance);
                                    }

                                }
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                            }
                            else if (hdfControl.Value.ToLower() == "dropdown")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields dropdown.");
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                if (ddlCustomValue != null)
                                {
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlCustomValue.SelectedValue), fi.PropertyType);

                                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                                    dictionaryInstance.Add("ColumnName", Convert.ToString(hdfColumnName.Value));
                                    dictionaryInstance.Add("ColumnNameValue", Convert.ToString(value));
                                    dictionaryInstance.Add("UserId", Convert.ToString(obj.UserId));
                                    bool Result = UserBL.UpdateCustomColumnValue(dictionaryInstance);
                                }
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                            }
                            else if (hdfControl.Value.ToLower() == "checkbox")
                            {
                                Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields checkbox.");
                                if (chkCustomValue != null)
                                {
                                    Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields before set value in customColumns");
                                    #region
                                    foreach (ListItem chkItem in chkCustomValue.Items)
                                    {
                                        if (chkItem.Selected)
                                        {
                                            if (iCount == 0)
                                            {
                                                value = chkItem.Value;
                                                iCount++;
                                            }
                                            else
                                            {
                                                value = value + "|" + chkItem.Value;
                                                iCount++;
                                            }
                                        }
                                    }
                                    #endregion
                                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(Convert.ToString(value)), fi.PropertyType);
                                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                                    dictionaryInstance.Add("ColumnName", Convert.ToString(hdfColumnName.Value));
                                    dictionaryInstance.Add("ColumnNameValue", Convert.ToString(value));
                                    dictionaryInstance.Add("UserId", Convert.ToString(obj.UserId));
                                    bool Result = UserBL.UpdateCustomColumnValue(dictionaryInstance);
                                    Exceptions.WriteInfoLog("Registration:rptUserTypeCustomFields after set value in customColumns");
                                }
                            }
                            #endregion
                            fi.SetValue(obj, value, null);
                            //return strError;
                        }
                        catch (InvalidCastException) { }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return strError;
        }

        private void BindPreferredLanguages(int PreferredLanguageId)
        {
            try
            {
                List<StoreBE.StoreLanguageBE> GetStoreLanguages = StoreLanguageBL.GetActiveStoreLanguageDetails();

                if (GetStoreLanguages != null)
                {
                    if (GetStoreLanguages.Count > 0)
                    {
                        lblPreferredLanguage.Visible = true;
                        drpdwnPreferredLanguage.Visible = true;
                        drpdwnPreferredLanguage.DataSource = GetStoreLanguages;
                        drpdwnPreferredLanguage.DataValueField = "LanguageId";
                        drpdwnPreferredLanguage.DataTextField = "LanguageName";
                        drpdwnPreferredLanguage.DataBind();
                        GlobalFunctions.AddDropdownItem(ref drpdwnPreferredLanguage);
                        if (GetStoreLanguages.Count == 1)
                        {
                            lblPreferredLanguage.Visible = false;
                            drpdwnPreferredLanguage.Visible = false;
                            drpdwnPreferredLanguage.SelectedValue = Convert.ToString(GetStoreLanguages[0].LanguageId);
                        }
                        else
                        {
                            drpdwnPreferredLanguage.SelectedValue = Convert.ToString(PreferredLanguageId);
                        }
                    }
                    else
                    {
                        lblPreferredLanguage.Visible = false;
                        drpdwnPreferredLanguage.Visible = false;
                    }
                }
                else
                {
                    lblPreferredLanguage.Visible = true;
                    drpdwnPreferredLanguage.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindPreferredCurrency(int UserTypeId, int UserPreferredCurrencyID)
        {
            try
            {
                List<UserPreferredBE.UserTypeCurrency> GetCurrencyUserTypeWise = UserPreferredBL.GetCurrencyUserTypeWise(UserTypeId);
                if (GetCurrencyUserTypeWise != null)
                {
                    if (GetCurrencyUserTypeWise.Count > 0)
                    {
                        lblPreferredCurrency.Visible = true;
                        drpdwnPreferredCurrency.Visible = true;
                        drpdwnPreferredCurrency.DataSource = GetCurrencyUserTypeWise;
                        drpdwnPreferredCurrency.DataValueField = "CurrencyId";
                        drpdwnPreferredCurrency.DataTextField = "CurrencyName";
                        drpdwnPreferredCurrency.DataBind();
                        GlobalFunctions.AddDropdownItem(ref drpdwnPreferredCurrency);
                        if (GetCurrencyUserTypeWise.Count == 1)
                        {
                            lblPreferredCurrency.Visible = false;
                            drpdwnPreferredCurrency.Visible = false;
                            drpdwnPreferredCurrency.SelectedValue = Convert.ToString(GetCurrencyUserTypeWise[0].CurrencyId);
                        }
                        else
                        {
                            drpdwnPreferredCurrency.SelectedValue = Convert.ToString(UserPreferredCurrencyID);
                        }
                    }
                    else
                    {
                        lblPreferredCurrency.Visible = false;
                        drpdwnPreferredCurrency.Visible = false;
                    }
                }
                else
                {
                    lblPreferredCurrency.Visible = false;
                    drpdwnPreferredCurrency.Visible = false;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion
    }
}