﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class MyAccount_MyWishList : System.Web.UI.Page
{
    #region Variables
    protected global::System.Web.UI.WebControls.Repeater rptWishListing;
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl hPageHeading;
    protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnBasket_InvalidEmailPassword, hdnBasket_AddPreviousProducts, hdnBasket_ErrorUpdatingBasket, hdnBasket_PreviousProductAdded;
    public Int16 intLanguageId = 0;
    public Int16 intCurrencyId = 0;
    Int32 intUserId = 0;
    public string host = GlobalFunctions.GetVirtualPath();
    string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
    public decimal dTotalPrice = 0;
    string strUserSessionId = string.Empty;
    public bool bBackOrderAllowed = false;
    public bool bBASysStore = false;
    string strCurrencySymbol = string.Empty;
    public string strProductText, strTitleText, strQuantityText, strUnitCost, strLineTotal, strRemoveText, strTotalText, strContinueShoppingText, strNoteLable,
        strCheckoutText, strQuantityNotZero, strMinMaxQuantity, strQuantityNotMoreThanStock,
        strWishListProductRemovemsg, strBasketErrorWhileRemovingProduct, strInvalidQuantityMessage, strPageTitle, strPageMetaKeywords,
        strPageMetaDescription, strCommonErrorMessageWhileUpdatingBasket = string.Empty;
    public bool bIsPointsEnabled = false;
    Int32 intUserPoints = 0;

    ProductBE.ProductWishList objWishList = new ProductBE.ProductWishList();

    List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();

    /*Sachin Chauhan Start : 27 03 2016 : varaible declarations for resource languages*/
    public string Basket_HaveAnAccount, Basket_Email, Basket_Password, Basket_BeforeYouCheckout, Basket_SignIntoYourAccount,
                  Basket_ForgotPassword, Basket_NewToOurSite, Basket_OrderConfirmationMessage, Basket_CheckoutAsGuest,
                  Basket_InvalidEmailPassword, Basket_AddPreviousProducts, Basket_ErrorUpdatingBasket, Basket_PreviousProductAdded,
                  strBasket_Quantity_Not_Zero_Message, strBasket_Invalid_Quantity_Message, strLimitedProductMessage, strButtonAddCartText;
    /*Sachin Chauhan Emd : 27 03 2016*/

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {

            Session["Amount"] = null;



            if (Session["User"] != null)
            {
                UserBE lst = new UserBE();
                lst = Session["User"] as UserBE;
                intUserId = lst.UserId;
                intUserPoints = lst.Points;
            }
            // bIsPointsEnabled = GlobalFunctions.IsPointsEnbled();
            intCurrencyId = GlobalFunctions.GetCurrencyId();
            intLanguageId = GlobalFunctions.GetLanguageId();
            strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();

            if (GlobalFunctions.IsPointsEnbled())
                strCurrencySymbol = "";

            BindResourceData();
            if (!IsPostBack)
            {


                List<ProductBE.ProductWishList> lstProductWishList = new List<ProductBE.ProductWishList>();
                objWishList.Action = Convert.ToInt16(DBAction.Select);
                objWishList.LanguageId = intLanguageId;
                lstProductWishList = ProductBL.GetWishList<ProductBE.ProductWishList>(objWishList, intUserId);

                if (lstProductWishList != null && lstProductWishList.Count > 0)
                {
                    rptWishListing.DataSource = lstProductWishList;
                    rptWishListing.DataBind();
                }
                else
                {
                    rptWishListing.DataSource = null;
                    rptWishListing.DataBind();
                }

            }
            ReadMetaTagsData();
            //this.Page.Title = strPageTitle;
            //this.MetaDescription = strPageMetaDescription;
            //this.MetaKeywords = strPageMetaKeywords;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }


    protected void BindResourceData()
    {
        try
        {
            lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
            if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
            {
                lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    hPageHeading.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "WishList_Page_Title").ResourceValue;
                    strProductText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Product_Text").ResourceValue;
                    strTitleText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Title_Text").ResourceValue;
                    strQuantityText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Text").ResourceValue;
                    strUnitCost = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Unit_Cost_Text").ResourceValue;
                    strLineTotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Line_Total_Text").ResourceValue;
                    strRemoveText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Remove_Text").ResourceValue;
                    strTotalText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Total_Text").ResourceValue;
                    strContinueShoppingText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Continue_Shopping_Text").ResourceValue;
                    strCheckoutText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Checkout_Text").ResourceValue;
                    strQuantityNotZero = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Not_Zero_Message").ResourceValue;
                    strMinMaxQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Min_Max_Quantity_Message").ResourceValue;
                    strQuantityNotMoreThanStock = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Not_More_Than_Stock_Message").ResourceValue;
                    strWishListProductRemovemsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "WishList_Product_Remove_msg").ResourceValue;
                    strBasketErrorWhileRemovingProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Error_While_Removing_Product_Message").ResourceValue;
                    strInvalidQuantityMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Invalid_Quantity_Message").ResourceValue;
                    strNoteLable = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Note_Label").ResourceValue;
                    strButtonAddCartText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Add_To_Basket_Text").ResourceValue;
                    strBasket_Quantity_Not_Zero_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Not_Zero_Message").ResourceValue;
                    strBasket_Invalid_Quantity_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Invalid_Quantity_Message").ResourceValue;
                    hdnBasket_ErrorUpdatingBasket.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_ErrorUpdatingBasket").ResourceValue;
                    hdnBasket_PreviousProductAdded.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_PreviousProductAdded").ResourceValue;
                    strLimitedProductMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Limited_Products_Message").ResourceValue;
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }


    protected void rptWishListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                HtmlGenericControl divProductText = (HtmlGenericControl)e.Item.FindControl("divProductText");
                HtmlGenericControl divTitleText = (HtmlGenericControl)e.Item.FindControl("divTitleText");
                HtmlGenericControl divQuantityText = (HtmlGenericControl)e.Item.FindControl("divQuantityText");
                //HtmlGenericControl divUnitCostText = (HtmlGenericControl)e.Item.FindControl("divUnitCostText");
                HtmlGenericControl divRemoveText = (HtmlGenericControl)e.Item.FindControl("divRemoveText");
                //HtmlGenericControl divNotelabel = (HtmlGenericControl)e.Item.FindControl("divNotelabel");
                divProductText.InnerHtml = strProductText;
                divTitleText.InnerHtml = strTitleText;
                divQuantityText.InnerHtml = strQuantityText;
                //divUnitCostText.InnerHtml = strUnitCost;
                divRemoveText.InnerHtml = strRemoveText;
                //  divNotelabel.InnerHtml = strNoteLable;
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                HtmlGenericControl dvUnitPrice = (HtmlGenericControl)e.Item.FindControl("dvUnitPrice");
                HtmlGenericControl dvBasketRowContainer = (HtmlGenericControl)e.Item.FindControl("dvBasketRowContainer");
                //HtmlGenericControl dvTotalPrice = (HtmlGenericControl)e.Item.FindControl("dvTotalPrice");
                HtmlInputHidden hdnWishListId = (HtmlInputHidden)e.Item.FindControl("hdnWishListId");
                Literal ltrSKUName = (Literal)e.Item.FindControl("ltrSKUName");
                Literal ltrProductName = (Literal)e.Item.FindControl("ltrProductName");
                HtmlInputText txtQuantity = (HtmlInputText)e.Item.FindControl("txtQuantity");
                HtmlInputHidden hdnIsBackOrderAllowed = (HtmlInputHidden)e.Item.FindControl("hdnIsBackOrderAllowed");
                HtmlInputHidden hdnMinimumOrderQuantity = (HtmlInputHidden)e.Item.FindControl("hdnMinimumOrderQuantity");
                HtmlInputHidden hdnMaximumOrderQuantity = (HtmlInputHidden)e.Item.FindControl("hdnMaximumOrderQuantity");
                HtmlInputHidden hdnSKUId = (HtmlInputHidden)e.Item.FindControl("hdnSKUId");

                LinkButton lnkDeleteProduct = (LinkButton)e.Item.FindControl("lnkDeleteProduct");
                lnkDeleteProduct.Attributes.Add("onclick", lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Remove_Items_From_Cart_Message").ResourceValue);

                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ImageName"))))
                    imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ImageName"));
                else
                    imgProduct.Src = host + "Images/Products/default.jpg";

                // dvUnitPrice.InnerHtml = strCurrencySymbol + "<span>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Price")), "", intLanguageId) + "</span>" + GlobalFunctions.PointsText(intLanguageId);
                dvBasketRowContainer.ID = "dvBasketRowContainer" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "WishListId"));
                hdnWishListId.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "WishListId"));
                ltrSKUName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCode"));
                ltrProductName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductName"));
                txtQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                hdnMinimumOrderQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MinimumOrderQuantity"));
                hdnMaximumOrderQuantity.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MaximumOrderQuantity"));
                hdnSKUId.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductSKUId"));
                try
                {

                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Button imgbtnAddtoCart = (Button)e.Item.FindControl("imgbtnAddtoCart");
                imgbtnAddtoCart.Text = strButtonAddCartText;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void imgbtnAddtoCart_Click(object sender, EventArgs e)
    {
        try
        {
            int intUserId = 0;
            string UserSessionId = HttpContext.Current.Session.SessionID;
            if (Session["User"] != null)
            {
                UserBE objUserBE = (UserBE)Session["User"];
                intUserId = objUserBE.UserId;
            }

            if (bBackOrderAllowed == false)
            {
                //#region "Validation"
                //string strErrormessage = "";
                //for (var i = 0; i < rptWishListing.Items.Count; i++)
                //{
                //    HtmlInputText txtQuantity = (HtmlInputText)rptWishListing.Items[i].FindControl("txtQuantity");
                //    Label lblStock = (Label)rptWishListing.Items[i].FindControl("lblStock");
                //    HtmlInputHidden hdnStockStatus = (HtmlInputHidden)rptWishListing.Items[i].FindControl("hdnStockStatus");
                //    Label lblProductName = (Label)rptWishListing.Items[i].FindControl("lblProductName");
                //    if (txtQuantity.Value != "")
                //    {
                //        try
                //        {
                //            if (Convert.ToInt32(txtQuantity.Value) <= 0)
                //            {
                //                strErrormessage = strBasket_Quantity_Not_Zero_Message;
                //            }
                //            if (!GlobalFunctions.isNumeric(txtQuantity.Value.Trim(), System.Globalization.NumberStyles.Integer))
                //            { strErrormessage += "<br>" + strBasket_Invalid_Quantity_Message; }
                //        }
                //        catch (Exception) { strErrormessage = strBasket_Invalid_Quantity_Message; }
                //        if (!string.IsNullOrEmpty(strErrormessage))
                //        {
                //            if (bBASysStore == false)
                //            {
                //                #region
                //                if (Convert.ToInt32(txtQuantity.Value) > Convert.ToInt32(lblStock.Text))
                //                {
                //                    txtQuantity.Value = "";
                //                    txtQuantity.Style.Add("borderColor", "red");
                //                    strErrormessage += lblProductName.Text + ": " + strLimitedProductMessage + lblStock.Text;
                //                }
                //                else
                //                { txtQuantity.Style.Add("borderColor", "#ccc"); }
                //                #endregion
                //            }
                //            else
                //            {
                //                #region
                //                if (Convert.ToInt32(txtQuantity.Value) > Convert.ToInt32(hdnStockStatus.Value))
                //                {
                //                    txtQuantity.Value = "";
                //                    txtQuantity.Style.Add("borderColor", "red");
                //                    strErrormessage += lblProductName.Text + ": " + strLimitedProductMessage + hdnStockStatus.Value;
                //                }
                //                else
                //                { txtQuantity.Style.Add("borderColor", "#ccc"); }
                //                #endregion
                //            }
                //        }
                //    }
                //}
                //if (strErrormessage != "")
                //{ GlobalFunctions.ShowModalAlertMessages(this.Page, strErrormessage, AlertType.Warning); return; }
                //#endregion
            }
            #region "Insert"
            foreach (RepeaterItem item in rptWishListing.Items)
            {
                HtmlInputText txtQuantity = (HtmlInputText)item.FindControl("txtQuantity");
                HtmlInputHidden hdnSKUId = (HtmlInputHidden)item.FindControl("hdnSKUId");
                HtmlInputHidden hdnWishListId = (HtmlInputHidden)item.FindControl("hdnWishListId");
                HtmlInputHidden hdnMinimumOrderQuantity = (HtmlInputHidden)item.FindControl("hdnMinimumOrderQuantity");
                HtmlInputHidden hdnMaximumOrderQuantity = (HtmlInputHidden)item.FindControl("hdnMaximumOrderQuantity");
                if (txtQuantity.Value != "")
                {
                    try
                    {
                        if (Convert.ToInt32(txtQuantity.Value) > 0)
                        {
                            if (Convert.ToInt32(txtQuantity.Value) < Convert.ToInt16(hdnMinimumOrderQuantity.Value))
                            {
                                strMinMaxQuantity = strMinMaxQuantity.Replace("{min}", hdnMinimumOrderQuantity.Value);
                                strMinMaxQuantity = strMinMaxQuantity.Replace("{max}", hdnMaximumOrderQuantity.Value);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strMinMaxQuantity, AlertType.Warning);
                            }
                            else if (Convert.ToInt32(txtQuantity.Value) > Convert.ToInt16(hdnMaximumOrderQuantity.Value))
                            {
                                strMinMaxQuantity = strMinMaxQuantity.Replace("{min}", hdnMinimumOrderQuantity.Value);
                                strMinMaxQuantity = strMinMaxQuantity.Replace("{max}", hdnMaximumOrderQuantity.Value);
                                GlobalFunctions.ShowModalAlertMessages(this.Page, strMinMaxQuantity, AlertType.Warning);
                            }
                            else
                            {
                                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                                dictionary.Add("UserId", Convert.ToString(intUserId));
                                dictionary.Add("ProductSKUId", hdnSKUId.Value);
                                dictionary.Add("Quantity", txtQuantity.Value.Trim());
                                dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
                                dictionary.Add("UserSessionId", "");//UserSessionId
                                dictionary.Add("UserInput", "");
                                dictionary.Add("ProductId", "");
                                int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                                Session["Add"] = true;
                                DeleteFromWishList(Convert.ToInt16(hdnWishListId.Value));
                                Response.Redirect(Request.RawUrl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowBasket();", true);
                            }
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strBasket_Quantity_Not_Zero_Message, AlertType.Failure);
                        }

                    }
                    catch (Exception) { }
                }
            }

            #endregion
            //Response.Redirect(Request.RawUrl);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected int DeleteFromWishList(int WishListId)
    {
        try
        {
            string strUserSessionId = string.Empty;
            Int32 intUserId = 0;
            if (HttpContext.Current.Session["User"] != null)
            {
                UserBE lst = new UserBE();
                lst = HttpContext.Current.Session["User"] as UserBE;
                intUserId = lst.UserId;
            }
            ProductBE.ProductWishList objWishListBE = new ProductBE.ProductWishList();
            objWishListBE.Action = Convert.ToInt16(DBAction.Delete);
            objWishListBE.WishListId = WishListId;
            int res = ProductBL.ManageWishListItem(objWishListBE, intUserId);
            return res;

        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); return 0; }

    }

    protected void lnkDeleteProduct_Click(object sender, EventArgs e)
    {
        LinkButton lnkDeleteProduct = (LinkButton)sender;
        Int32 WishListId = Convert.ToInt32(lnkDeleteProduct.CommandArgument);
        int res = DeleteFromWishList(WishListId);
        if (res > 0)
        {
            GlobalFunctions.ShowModalAlertMessages(this.Page, strWishListProductRemovemsg, AlertType.Success);
        }
        else
        {
            string strErrorWhileSave = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Save_Fail_Msg").ResourceValue;
            GlobalFunctions.ShowModalAlertMessages(this.Page, strErrorWhileSave, AlertType.Failure);
        }
        Response.Redirect(Request.RawUrl);
    }

    private void ReadMetaTagsData()
    {
        try
        {
            StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
            List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
            MetaTags.Action = Convert.ToInt16(DBAction.Select);
            lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

            if (lstMetaTags != null)
            {
                lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "My WishList");
                if (lstMetaTags.Count > 0)
                {
                    Page.Title = lstMetaTags[0].MetaContentTitle;
                    Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                    Page.MetaDescription = lstMetaTags[0].MetaDescription;
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            else
            {
                Page.Title = "";
                Page.MetaKeywords = "";
                Page.MetaDescription = "";
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
}