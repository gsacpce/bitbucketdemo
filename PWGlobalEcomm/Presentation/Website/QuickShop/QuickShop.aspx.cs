﻿using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.Stock;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class QuickShop : BasePage
    {
        public Int16 LanguageId = 0;
        public Int16 CurrencyId = 0;
        protected global::System.Web.UI.WebControls.Repeater rptParentCategory, rptCategory, rptSubCategory_header;
        protected global::System.Web.UI.WebControls.Literal ltrlNoData;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidPageNo;
        protected global::System.Web.UI.HtmlControls.HtmlContainerControl divProductInCategory, divLazy, divNoDataMsg, quickshop_sticky;
        protected global::System.Web.UI.WebControls.LinkButton lbtnAddBasket; // Added by SHRIGANESH SINGH 27 March 2016
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox noStockCheckbox; // Added by SHRIGANESH SINGH 27 March 2016
        protected global::System.Web.UI.WebControls.Label lblCheckboxText;

        protected bool IsCategoryHeaderEnabled;
        public string host = GlobalFunctions.GetVirtualPath();
        public bool bBackOrderAllowed = false;
        public bool bBASysStore = false;

        //Added By Snehal 23 09 2016
        public int strMaxAllowQtyPerItem;
        public bool IsMaxQtyEnabled = false;

        public string QuickShop_NoItemsWarning;
        public string QuickShop_MinQtyWarning;
        public string QuickShop_MaxQtyWarning;
        public string QuickShop_NoData;
        public string QuickShop_PgTtl, QuickShop_HeaderImage, QuickShop_HeaderProduct, QuickShop_HeaderStock, QuickShop_HeaderPrice, QuickShop_HeaderQuantity, strAlertMessage, strLimitedProductMessage,
                      strOutOfStotckTitle, strStockDueText, strInStockTitle;

        List<CategoryBE> Categories;

        /*Sachin Chauhan Start : 24-11-15 : For Stcok qty fetching*/
        CatalogueProductDetail[] objStockCatalogueProductDetail;
        /*Sachin Chauhan End*/
        Int16 iUsertypeID = 1;/*User Type*/
        List<CategoryBE> objSubCat;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                #region
                LanguageId = GlobalFunctions.GetLanguageId();
                CurrencyId = GlobalFunctions.GetCurrencyId();
                BindResourceData();

                /*User Type*/
                if (Session["User"] != null)
                {
                    UserBE objUserBE = Session["User"] as UserBE;
                    iUsertypeID = objUserBE.UserTypeID;
                }

                /*if (Session["User"] == null)
                {
                    Response.RedirectToRoute("Login-Page");
                }*/
                divProductInCategory.Attributes["style"] = "display:block";
                divLazy.Attributes["style"] = "display:none";

                if (!Page.IsPostBack)
                {
                    #region
                    /*Sachin Chauhan Start : 24-11-15*/
                    StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    if (objStoreBE != null)
                    {
                        bBackOrderAllowed = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "OD_AllowBackOrder").FeatureValues[0].IsEnabled;
                        bBASysStore = objStoreBE.IsBASYS;

                        #region To check max min Qty - Added by Snehal 23 09 2016

                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_MaxAllowQty").FeatureValues[0].IsEnabled)
                        {
                            IsMaxQtyEnabled = true;
                            strMaxAllowQtyPerItem = Convert.ToInt32(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_MaxAllowQty").FeatureValues[0].FeatureDefaultValue);
                        }
                        else
                        {
                            IsMaxQtyEnabled = false;
                            strMaxAllowQtyPerItem = 0;
                        }

                        #endregion
                    }

                    //if (bBackOrderAllowed == false)
                    //{
                    if (bBASysStore)
                    {
                        #region
                        try
                        {
                            Stock objStock = new Stock();
                            System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                            objStock.Credentials = objNetworkCredentials;
                            StockDetails objStockDetails = new StockDetails();
                            //objStockCatalogueProductDetail = objStock.GetCatalogueProducts(963);
                            objStockCatalogueProductDetail = objStock.GetCatalogueProducts(Convert.ToInt32(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CatalogueId));

                            //foreach (PWGlobalEcomm.Stock.CatalogueProductDetail item in objStockCatalogueProductDetail)
                            //{
                            // //   Response.Write( item.CatalogueAlias.Trim() + " - " + item.StockLevel + " - " + item.StockDueDate + " <br />" ) ;
                            //}
                            ////Response.End();
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                        }
                        #endregion
                    }
                    //}
                    /*Sachin Chauhan End : 24-11-15*/
                    BindCategories();
                    ReadMetaTagsData();
                    #endregion
                }
                if (Session["PrevLanguageId"] == null)
                { Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId(); }
                if (!Session["PrevLanguageId"].ToString().To_Int32().Equals(GlobalFunctions.GetLanguageId().To_Int32()))
                {
                    //BindCategories();
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                    Response.RedirectToRoute("QuickShop");
                }
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void BindCategories()
        {
            try
            {
                #region
                //Categories = new List<CategoryBE>();
                /*Sachin Chauhan Start : 24-11-15 : Commented below line of code as only english content is coming*/
                //Categories = CategoryBL.ListCategories();
                List<CategoryBE> objCategoryBE;
                //objCategoryBE = CategoryBL.GetAllCategories(LanguageId, CurrencyId);
                objCategoryBE = CategoryBL.GetAllCategories(LanguageId, CurrencyId, iUsertypeID);/*User Type*/

                //Dictionary<int, List<CategoryBE>> objDictAllCategoriesAllLanguages;
                //objDictAllCategoriesAllLanguages = CategoryBL.GetAllCategoriesAllLanguages();

                /*Check wheather the categories or products data is available or not */
                if (objCategoryBE != null)
                {
                    // objCategoryBE = objCategoryBE.OrderBy(x => x.ParentCategoryId).ToList();

                    /*Sachin Chauhan End : 24-11-15*/
                    if (objCategoryBE.Count() > 0)
                    {

                        Categories = objCategoryBE;
                        //rptParentCategory.DataSource = Categories;
                        rptParentCategory.DataSource = Categories.Where(x => x.ParentCategoryId == 0).ToList();
                        rptParentCategory.DataBind();
                        objSubCat = Categories.Where(x => x.ParentCategoryId > 0).ToList();
                        rptCategory.DataSource = Categories.Where(x => x.ParentCategoryId == 0).ToList();
                        rptCategory.DataBind();
                    }
                }
                else
                {
                    divNoDataMsg.Visible = true;
                    ltrlNoData.Text = "<h3 class='text-warning' style='font-size:26px'>" + QuickShop_NoData + "</h3>";
                    quickshop_sticky.Visible = false;
                }
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptParentCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnRelatedCategoryId = (HiddenField)e.Item.FindControl("hdnRelatedCategoryId");
                    //List<ProductBE> Products = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId);
                    List<ProductBE> Products = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId, iUsertypeID);/*User Type*/
                    Products = Products.Where(p => p.IsShowPriceAndEnquiry.Equals(false) && p.IsCallForPrice.Equals(false)).ToList();
                    Repeater rptProducts = (Repeater)e.Item.FindControl("rptProducts");
                    Literal ltlCategory = (Literal)e.Item.FindControl("ltlCategory");
                    HtmlContainerControl quickshop_headings = (HtmlContainerControl)e.Item.FindControl("quickshop_headings");
                    quickshop_headings.ID = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));

                    string ParentCategory, SubCategory;
                    string CategoryType = "";
                    if (Products != null)
                    {
                        if (Products.Count() > 0)
                        {
                            List<CategoryBE> CategoriesRpt = new List<CategoryBE>();

                            //string CategoryType = "";
                            Int16 CategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId"));
                            Int16 SubCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SubCategoryId"));
                            Int16 SubSubCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SubSubCategoryId"));

                            //string ParentCategory, SubCategory;
                            //Read the store configuration for category requirement or not

                            if (CategoryId == SubCategoryId && CategoryId == SubSubCategoryId && SubCategoryId == SubSubCategoryId)
                            {
                                CategoryType = "C";
                            }
                            //else if (CategoryId == SubCategoryId && CategoryId != SubSubCategoryId && SubCategoryId != SubSubCategoryId)
                            /*Sachin Chauhan Start : 18 02 2016 : Changed below condition to handle sub category*/
                            else if (CategoryId == SubSubCategoryId && CategoryId != SubCategoryId)
                            {
                                CategoryType = "SC";
                            }
                            /*Changed below condition to handle sub sub category*/
                            else if (CategoryId != SubCategoryId && CategoryId != SubSubCategoryId && SubCategoryId != SubSubCategoryId)
                            {
                                CategoryType = "SSC";
                            }
                            /*Sachin Chauhan End : 18 02 2016 */
                            if (CategoryType == "C")
                            {
                                ltlCategory.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            else if (CategoryType == "SC")
                            {
                                CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                CategoryBE pCat = Categories.FirstOrDefault(x => x.CategoryId.Equals(CategoryId));

                                ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;
                                ltlCategory.Text = ParentCategory + " >  " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            else if (CategoryType == "SSC")
                            {
                                SubCategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                SubCategory = Categories.Where(c => c.CategoryId == SubCategoryId).FirstOrDefault().CategoryName;

                                CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;

                                ltlCategory.Text = ParentCategory + " >  " + SubCategory + " > " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            rptProducts.DataSource = Products;
                            rptProducts.DataBind();
                        }
                        else
                        {
                            int iCat = ((CategoryBE)(e.Item.DataItem)).CategoryId;
                            List<CategoryBE> subCatList = Categories.Where(x => x.ParentCategoryId == iCat).ToList();
                            quickshop_headings.Visible = false;

                            Repeater rptSubCategory_header = (Repeater)e.Item.FindControl("rptSubCategory_header");
                            rptSubCategory_header.DataSource = subCatList;
                            rptSubCategory_header.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Literal ltlCategoryWithLink = (Literal)e.Item.FindControl("ltlCategoryWithLink");

                    HiddenField hdnRelatedCategoryId = (HiddenField)e.Item.FindControl("hdnRelatedCategoryId");
                    Int16 CategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId"));
                    Int16 SubCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SubCategoryId"));
                    Int16 SubSubCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SubSubCategoryId"));
                    Int16 RelatedCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "RelatedCategoryId"));
                    HtmlAnchor anchorCategoryLink = (HtmlAnchor)e.Item.FindControl("anchorCategoryLink");

                    //List<ProductBE> Products = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId);
                    List<ProductBE> Products = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId, iUsertypeID);/*User Type*/
                    if (Products != null && Products.Count > 0)
                    {
                        #region
                        string CategoryType = "";
                        string ParentCategory, SubCategory;
                        //ltlCategoryWithLink.Text = DataBinder.Eval(e.Item.DataItem, "CategoryName").ToString();

                        if (CategoryId == SubCategoryId && CategoryId == SubSubCategoryId && SubCategoryId == SubSubCategoryId)
                        {
                            CategoryType = "C";
                        }
                        else if (CategoryId == RelatedCategoryId && RelatedCategoryId == SubSubCategoryId && SubCategoryId != SubSubCategoryId)
                        {
                            CategoryType = "SC";
                        }
                        else if (SubSubCategoryId == RelatedCategoryId && RelatedCategoryId != CategoryId && RelatedCategoryId != SubCategoryId)
                        {
                            CategoryType = "SSC";
                        }
                        if (Products.Count(c => c.CategoryName == Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"))) > 0)
                        {
                            #region
                            if (CategoryType == "C")
                            {
                                ltlCategoryWithLink.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            else if (CategoryType == "SC")
                            {
                                CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;
                                ltlCategoryWithLink.Text = ParentCategory + " >  " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            else if (CategoryType == "SSC")
                            {
                                SubCategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                SubCategory = Categories.Where(c => c.CategoryId == SubCategoryId).FirstOrDefault().CategoryName;

                                CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;

                                ltlCategoryWithLink.Text = ParentCategory + " >  " + SubCategory + " > " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            /*Sachin Chauhan Start : 24-11-15*/
                            anchorCategoryLink.Attributes.Add("onclick", "javascript:quickshop_scrollto('" + DataBinder.Eval(e.Item.DataItem, "CategoryName") + "'," + DataBinder.Eval(e.Item.DataItem, "RelatedCategoryId") + ")");
                            /*Sachin Chauhan end*/

                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        //int iCat = ((CategoryBE)(e.Item.DataItem)).CategoryId;
                        List<CategoryBE> subCatList = objSubCat.Where(x => x.ParentCategoryId == CategoryId).ToList();
                        if (subCatList != null && subCatList.Count() > 0)
                        {
                            Repeater rptSubCategory = (Repeater)e.Item.FindControl("rptSubCategory");
                            rptSubCategory.DataSource = subCatList.Where(x => x.UserTypeId == iUsertypeID).ToList();
                            rptSubCategory.DataBind();
                            //BindSubCategories(subCatList);

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSubCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Literal ltlCategoryWithLink = (Literal)e.Item.FindControl("ltlCategoryWithLink");

                HiddenField hdnRelatedCategoryId = (HiddenField)e.Item.FindControl("hdnRelatedCategoryId");
                Int16 CategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId"));
                Int16 SubCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SubCategoryId"));
                Int16 SubSubCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SubSubCategoryId"));
                Int16 RelatedCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "RelatedCategoryId"));
                HtmlAnchor anchorCategoryLink = (HtmlAnchor)e.Item.FindControl("anchorCategoryLink");

                //List<ProductBE> Products = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId);
                List<ProductBE> Products = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId, iUsertypeID);/*User Type*/
                if (Products != null && Products.Count > 0)
                {
                    #region
                    string CategoryType = "";
                    string ParentCategory, SubCategory;
                    //ltlCategoryWithLink.Text = DataBinder.Eval(e.Item.DataItem, "CategoryName").ToString();

                    if (CategoryId == SubCategoryId && CategoryId == SubSubCategoryId && SubCategoryId == SubSubCategoryId)
                    {
                        CategoryType = "C";
                    }
                    else if (CategoryId == RelatedCategoryId && RelatedCategoryId == SubSubCategoryId && SubCategoryId != SubSubCategoryId)
                    {
                        CategoryType = "SC";
                    }
                    else if (SubSubCategoryId == RelatedCategoryId && RelatedCategoryId != CategoryId && RelatedCategoryId != SubCategoryId)
                    {
                        CategoryType = "SSC";
                    }
                    if (Products.Count(c => c.CategoryName == Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"))) > 0)
                    {
                        #region
                        if (CategoryType == "C")
                        {
                            ltlCategoryWithLink.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                        }
                        else if (CategoryType == "SC")
                        {
                            CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                            ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;
                            ltlCategoryWithLink.Text = ParentCategory + " >  " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                        }
                        else if (CategoryType == "SSC")
                        {
                            SubCategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                            SubCategory = Categories.Where(c => c.CategoryId == SubCategoryId).FirstOrDefault().CategoryName;

                            CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                            ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;

                            ltlCategoryWithLink.Text = ParentCategory + " >  " + SubCategory + " > " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                        }
                        /*Sachin Chauhan Start : 24-11-15*/
                        anchorCategoryLink.Attributes.Add("onclick", "javascript:quickshop_scrollto('" + DataBinder.Eval(e.Item.DataItem, "CategoryName") + "'," + DataBinder.Eval(e.Item.DataItem, "RelatedCategoryId") + ")");
                        /*Sachin Chauhan end*/
                        //CategoryBE objCategoryBE = Categories.FirstOrDefault(x => x.CategoryId == Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryID")));
                        //Categories.Remove(objCategoryBE);
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void BindSubCategories(List<CategoryBE> subCatList)
        {
            foreach (RepeaterItem rpt in rptCategory.Items)
            {

                Literal ltlCategoryWithLink = (Literal)rpt.FindControl("ltlCategoryWithLink");
                HiddenField hdnRelatedCategoryId = (HiddenField)rpt.FindControl("hdnRelatedCategoryId");
                Int16 CategoryId = Convert.ToInt16(DataBinder.Eval(rpt.DataItem, "CategoryId"));
                Int16 SubCategoryId = Convert.ToInt16(DataBinder.Eval(rpt.DataItem, "SubCategoryId"));
                Int16 SubSubCategoryId = Convert.ToInt16(DataBinder.Eval(rpt.DataItem, "SubSubCategoryId"));
                Int16 RelatedCategoryId = Convert.ToInt16(DataBinder.Eval(rpt.DataItem, "RelatedCategoryId"));
                HtmlAnchor anchorCategoryLink = (HtmlAnchor)rpt.FindControl("anchorCategoryLink");
                string CategoryType = "";
                string ParentCategory, SubCategory;
                if (CategoryId == SubCategoryId && CategoryId == SubSubCategoryId && SubCategoryId == SubSubCategoryId)
                {
                    CategoryType = "C";
                }
                else if (CategoryId == RelatedCategoryId && RelatedCategoryId == SubSubCategoryId && SubCategoryId != SubSubCategoryId)
                {
                    CategoryType = "SC";
                }
                else if (SubSubCategoryId == RelatedCategoryId && RelatedCategoryId != CategoryId && RelatedCategoryId != SubCategoryId)
                {
                    CategoryType = "SSC";
                }
                List<ProductBE> ProductSubCategoriesWise = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId, iUsertypeID);/*User Type*/
                if (ProductSubCategoriesWise.Count(c => c.CategoryName == Convert.ToString(DataBinder.Eval(rpt.DataItem, "CategoryName"))) > 0)
                {
                    if (CategoryType == "C")
                    {
                        ltlCategoryWithLink.Text = Convert.ToString(DataBinder.Eval(rpt.DataItem, "CategoryName"));
                    }
                    else if (CategoryType == "SC")
                    {
                        CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                        ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;
                        ltlCategoryWithLink.Text = ParentCategory + " >  " + Convert.ToString(DataBinder.Eval(rpt.DataItem, "CategoryName"));
                    }
                    else if (CategoryType == "SSC")
                    {
                        SubCategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                        SubCategory = Categories.Where(c => c.CategoryId == SubCategoryId).FirstOrDefault().CategoryName;

                        CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                        ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;

                        ltlCategoryWithLink.Text = ParentCategory + " >  " + SubCategory + " > " + Convert.ToString(DataBinder.Eval(rpt.DataItem, "CategoryName"));
                    }
                    /*Sachin Chauhan Start : 24-11-15*/
                    anchorCategoryLink.Attributes.Add("onclick", "javascript:quickshop_scrollto('" + DataBinder.Eval(rpt.DataItem, "CategoryName") + "'," + DataBinder.Eval(rpt.DataItem, "RelatedCategoryId") + ")");

                }
            }
        }

        protected void rptProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string ProductSKU = DataBinder.Eval(e.Item.DataItem, "ProductSkuName").ToString();

                    Literal ltlCost = (Literal)e.Item.FindControl("ltlCost");
                    ltlCost.Text = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCost")), GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = GlobalFunctions.GetThumbnailProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";
                    int StockLevel = 0;
                    /*Sachin Chauhan  Start : 24-11-15*/
                    Label lblStock = e.Item.FindControl("lblStock") as Label;
                    Label lblStockStatus = e.Item.FindControl("lblStockStatus") as Label;
                    PWGlobalEcomm.Stock.CatalogueProductDetail varobjStockCatalogueProductDetail = objStockCatalogueProductDetail.FirstOrDefault<PWGlobalEcomm.Stock.CatalogueProductDetail>(p => p.CatalogueAlias.Trim().ToLower().Equals(ProductSKU.Trim().ToLower()));
                    if (varobjStockCatalogueProductDetail != null)
                    {
                        Literal ltlProductCode = e.Item.FindControl("ltlProductCode") as Literal;
                        ltlProductCode.Text = varobjStockCatalogueProductDetail.CatalogueAlias.Trim();
                        lblStock.Text = Convert.ToString(varobjStockCatalogueProductDetail.StockLevel);
                        #region "Comment"
                        //int iResult = varobjStockCatalogueProductDetail.Count();
                        //if (iResult > 0)
                        //{
                        //Literal ltlProductCode = e.Item.FindControl("ltlProductCode") as Literal;
                        //ltlProductCode.Text = varobjStockCatalogueProductDetail.CatalogueAlias.Trim();
                        //foreach (CatalogueProductDetail objCatalogueProductDetail in varobjStockCatalogueProductDetail)
                        //{
                        //lblStock.Text = Convert.ToString(varobjStockCatalogueProductDetail.StockLevel);
                        //    break;
                        //}
                        //}
                        //else
                        //{
                        //    lblStock.Text = "0";
                        //} 
                        #endregion
                    }
                    else
                    {
                        lblStock.Text = "0";
                    }
                    /*Sachin Chauhan End*/
                    StockLevel = Convert.ToInt16(lblStock.Text);
                    if (StockLevel > 0)
                    {
                        lblStockStatus.Text = strInStockTitle;
                    }
                    else
                    {
                        lblStockStatus.Text = strOutOfStotckTitle;
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<ProductBE> GetAllProducts(short pageNo)
        {
            var lstProducts = new List<ProductBE>();
            lstProducts = ProductBL.GetAllProducts(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), pageNo);
            return lstProducts;
        }

        protected void lbtnAddBasket_Click(object sender, EventArgs e)
        {
            try
            {
                int intCurrencyId = GlobalFunctions.GetCurrencyId();
                int intUserId = 0;
                string UserSessionId = HttpContext.Current.Session.SessionID;
                string strErrormessage = string.Empty;
                if (Session["User"] != null)
                {
                    UserBE objUserBE = (UserBE)Session["User"];
                    intUserId = objUserBE.UserId;
                }
                //Repeater rptParentCategory = this.Page.FindControl("rptParentCategory") as Repeater;
                // Repeater rptProducts = rptParentCategory.Controls[     FindControl("rptProducts") as Repeater;
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    bBackOrderAllowed = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "OD_AllowBackOrder").FeatureValues[0].IsEnabled;
                    bBASysStore = objStoreBE.IsBASYS;
                }
                foreach (RepeaterItem parentItem in rptParentCategory.Items)
                {
                    foreach (RepeaterItem childItem in ((Repeater)(parentItem.FindControl("rptProducts"))).Items)
                    {
                        TextBox txtQuantity = childItem.FindControl("txtQuantity") as TextBox;
                        HiddenField hdnSKUId = childItem.FindControl("hdnProductSKU") as HiddenField;
                        Label lblStock = childItem.FindControl("lblStock") as Label;
                        Literal ltlProductName = (Literal)childItem.FindControl("ltlProductName");
                        if (!bBackOrderAllowed)
                        {
                            if (txtQuantity.Text != "")
                            {
                                if (Convert.ToInt32(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtQuantity.Text)) > Convert.ToInt32(lblStock.Text))
                                {

                                    strErrormessage += ltlProductName.Text + ":" + strLimitedProductMessage + " " + lblStock.Text + "<br/>";
                                }
                                else
                                {
                                    Dictionary<string, string> dictionary = new Dictionary<string, string>();
                                    dictionary.Add("UserId", Convert.ToString(intUserId));
                                    dictionary.Add("ProductSKUId", hdnSKUId.Value);
                                    dictionary.Add("Quantity", txtQuantity.Text.Trim());
                                    dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
                                    dictionary.Add("UserSessionId", UserSessionId);
                                    dictionary.Add("UserInput", "");
                                    dictionary.Add("ProductId", "");
                                    int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                                }
                            }
                        }
                        else
                        {
                            if (txtQuantity.Text != "")
                            {
                                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                                dictionary.Add("UserId", Convert.ToString(intUserId));
                                dictionary.Add("ProductSKUId", hdnSKUId.Value);
                                dictionary.Add("Quantity", txtQuantity.Text.Trim());
                                dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
                                dictionary.Add("UserSessionId", UserSessionId);
                                dictionary.Add("UserInput", "");
                                dictionary.Add("ProductId", "");
                                int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                            }
                        }
                    }
                    foreach (RepeaterItem rptSubCategory_headerItem in ((Repeater)(parentItem.FindControl("rptSubCategory_header"))).Items)
                    {
                        //   foreach (RepeaterItem rptSubCategory_headerItem in rptSubCategory_header.Items)

                        foreach (RepeaterItem childItem in ((Repeater)(rptSubCategory_headerItem.FindControl("rptProducts"))).Items)
                        {
                            TextBox txtQuantity = childItem.FindControl("txtQuantity") as TextBox;
                            HiddenField hdnSKUId = childItem.FindControl("hdnProductSKU") as HiddenField;
                            Label lblStock = childItem.FindControl("lblStock") as Label;
                            Literal ltlProductName = (Literal)childItem.FindControl("ltlProductName");
                            if (!bBackOrderAllowed)
                            {
                                if (txtQuantity.Text != "")
                                {
                                    if (Convert.ToInt32(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtQuantity.Text)) > Convert.ToInt32(lblStock.Text))
                                    {

                                        strErrormessage += ltlProductName.Text + ":" + strLimitedProductMessage + " " + lblStock.Text + "<br/>";
                                    }
                                    else
                                    {
                                        Dictionary<string, string> dictionary = new Dictionary<string, string>();
                                        dictionary.Add("UserId", Convert.ToString(intUserId));
                                        dictionary.Add("ProductSKUId", hdnSKUId.Value);
                                        dictionary.Add("Quantity", txtQuantity.Text.Trim());
                                        dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
                                        dictionary.Add("UserSessionId", UserSessionId);
                                        dictionary.Add("UserInput", "");
                                        dictionary.Add("ProductId", "");
                                        int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                                    }
                                }
                            }
                            else
                            {
                                if (txtQuantity.Text != "")
                                {
                                    Dictionary<string, string> dictionary = new Dictionary<string, string>();
                                    dictionary.Add("UserId", Convert.ToString(intUserId));
                                    dictionary.Add("ProductSKUId", hdnSKUId.Value);
                                    dictionary.Add("Quantity", txtQuantity.Text.Trim());
                                    dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
                                    dictionary.Add("UserSessionId", UserSessionId);
                                    dictionary.Add("UserInput", "");
                                    dictionary.Add("ProductId", "");
                                    int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                                }
                            }
                        }
                    }
                }

                #region "Comments"
                //GlobalFunctions.ShowModalAlertMessages(this, "Item successfully added to basket", AlertType.Success);
                //Master_UserControls_SiteLinks ctrlSiteLinks = (Master_UserControls_SiteLinks)Page.LoadControl("~/Master/UserControls/SiteLinks.ascx");
                //ctrlSiteLinks.LinkDisplayLocation = "header";
                //ctrlSiteLinks.LinkDisplayStyle = "horizontal";
                //if (objStoreBE.StoreLanguages.Count > 1)
                //{
                //    HtmlGenericControl ulHeaderLinks = ctrlSiteLinks.FindControl("ulHeaderLinks") as HtmlGenericControl;
                //    HtmlGenericControl liLanguage = new HtmlGenericControl("li");
                //    liLanguage.Controls.Add(siteLanguages);
                //    ulHeaderLinks.Controls.AddAt(ulHeaderLinks.Controls.Count, liLanguage);
                //}
                //gmContent.Controls.Add(ctrlSiteLinks);
                //GlobalFunctions.ShowModalAlertMessages(this, "Item successfully added to basket", Request.RawUrl, AlertType.Success); Commented by SHRIGANESH SINGH 27 March 2016
                //Response.Redirect(Request.RawUrl); 
                #endregion
                if (strErrormessage != "")
                {
                    strErrormessage += "Product not added in the basket.";
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strErrormessage, Request.RawUrl, AlertType.Warning);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, strAlertMessage, Request.RawUrl, AlertType.Success);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 24-11-15
        /// Scope   : BindResourceData of the basket page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        QuickShop_NoItemsWarning = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "QuickShop_NoItemsWarning").ResourceValue;
                        QuickShop_MinQtyWarning = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "QuickShop_MinQtyWarning").ResourceValue;
                        QuickShop_MaxQtyWarning = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "QuickShop_MaxQtyWarning").ResourceValue;
                        QuickShop_NoData = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Site_NoDataAvailable").ResourceValue;
                        strInStockTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_InStocktitle").ResourceValue;
                        strStockDueText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "StockDue_Title").ResourceValue;
                        strOutOfStotckTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_OutStockTitle").ResourceValue;
                        #region MyRegion Translations added by SHRIGANESH SINGH 27 March 2016
                        lbtnAddBasket.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Add_To_Basket_Text").ResourceValue;
                        lblCheckboxText.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "QuickShop_NoStock_CheckBox_Text").ResourceValue;
                        //noStockCheckbox.Value = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "QuickShop_NoStock_CheckBox_Text").ResourceValue; 
                        QuickShop_PgTtl = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_QuckShop_Text").ResourceValue;
                        QuickShop_HeaderImage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Image_Text").ResourceValue;
                        QuickShop_HeaderProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Product_Text").ResourceValue;
                        QuickShop_HeaderStock = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Stock_Title_Text").ResourceValue;
                        QuickShop_HeaderPrice = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Price_Title_Text").ResourceValue;
                        QuickShop_HeaderQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        strAlertMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "QuickShop_Add_To_Basket_Alert").ResourceValue;
                        strLimitedProductMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Limited_Products_Message").ResourceValue;
                        #endregion
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "QuickShop");
                    if (lstMetaTags.Count > 0)
                    {
                        Page.Title = lstMetaTags[0].MetaContentTitle;
                        Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                        Page.MetaDescription = lstMetaTags[0].MetaDescription;
                    }
                    else
                    {
                        Page.Title = "";
                        Page.MetaKeywords = "";
                        Page.MetaDescription = "";
                    }
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSubCategory_header_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnRelatedCategoryId = (HiddenField)e.Item.FindControl("hdnRelatedCategoryId");
                    //List<ProductBE> Products = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId);
                    List<ProductBE> Products = ProductBL.GetProductDetailsByCategory(Convert.ToInt16(hdnRelatedCategoryId.Value), LanguageId, CurrencyId, iUsertypeID);/*User Type*/
                    Products = Products.Where(p => p.IsShowPriceAndEnquiry.Equals(false) && p.IsCallForPrice.Equals(false)).ToList();
                    Repeater rptProducts = (Repeater)e.Item.FindControl("rptProducts");
                    Literal ltlSubCategory = (Literal)e.Item.FindControl("ltlSubCategory");
                    HtmlContainerControl quickshop_subheadings = (HtmlContainerControl)e.Item.FindControl("quickshop_subheadings");
                    quickshop_subheadings.ID = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));

                    string ParentCategory, SubCategory;
                    string CategoryType = "";
                    if (Products != null)
                    {
                        if (Products.Count() > 0)
                        {
                            List<CategoryBE> CategoriesRpt = new List<CategoryBE>();

                            //string CategoryType = "";
                            Int16 CategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "CategoryId"));
                            Int16 SubCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SubCategoryId"));
                            Int16 SubSubCategoryId = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "SubSubCategoryId"));

                            //string ParentCategory, SubCategory;
                            //Read the store configuration for category requirement or not

                            if (CategoryId == SubCategoryId && CategoryId == SubSubCategoryId && SubCategoryId == SubSubCategoryId)
                            {
                                CategoryType = "C";
                            }
                            //else if (CategoryId == SubCategoryId && CategoryId != SubSubCategoryId && SubCategoryId != SubSubCategoryId)
                            /*Sachin Chauhan Start : 18 02 2016 : Changed below condition to handle sub category*/
                            else if (CategoryId == SubSubCategoryId && CategoryId != SubCategoryId)
                            {
                                CategoryType = "SC";
                            }
                            /*Changed below condition to handle sub sub category*/
                            else if (CategoryId != SubCategoryId && CategoryId != SubSubCategoryId && SubCategoryId != SubSubCategoryId)
                            {
                                CategoryType = "SSC";
                            }
                            /*Sachin Chauhan End : 18 02 2016 */
                            if (CategoryType == "C")
                            {
                                ltlSubCategory.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            else if (CategoryType == "SC")
                            {
                                CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                CategoryBE pCat = Categories.FirstOrDefault(x => x.CategoryId.Equals(CategoryId));

                                ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;
                                ltlSubCategory.Text = ParentCategory + " >  " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            else if (CategoryType == "SSC")
                            {
                                SubCategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                SubCategory = Categories.Where(c => c.CategoryId == SubCategoryId).FirstOrDefault().CategoryName;

                                CategoryId = Categories.Where(p => p.RelatedCategoryId == Convert.ToInt16(hdnRelatedCategoryId.Value)).FirstOrDefault().ParentCategoryId;
                                ParentCategory = Categories.Where(c => c.CategoryId == CategoryId).FirstOrDefault().CategoryName;

                                ltlSubCategory.Text = ParentCategory + " >  " + SubCategory + " > " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                            }
                            rptProducts.DataSource = Products;
                            rptProducts.DataBind();
                        }
                        else
                        {
                            // int iCat = ((CategoryBE)(e.Item.DataItem)).CategoryId;
                            //List<CategoryBE> subCatList = Categories.Where(x => x.ParentCategoryId == iCat).ToList();
                            quickshop_subheadings.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}