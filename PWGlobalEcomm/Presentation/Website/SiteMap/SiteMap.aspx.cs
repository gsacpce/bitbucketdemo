﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Presentation
{
    public partial class SiteMap_SiteMap : BasePage //System.Web.UI.Page
    {
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvSiteMapContent;
        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        string host = GlobalFunctions.GetVirtualPath();
        public string strHelp, strProduct, strHome, SiteMap_Page_Heading;
        UserBE objUser = new UserBE();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                if (Session["User"] != null)
                    objUser = Session["User"] as UserBE;
                BindResourceData();
                BindCategory();
                BindMyAccount();
                BindHelp();
                ReadMetaTagsData();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindHelp()
        {
            List<StaticPageManagementBE> lstStaticPages;
            try
            {
                lstStaticPages = StaticPageManagementBL.GetAllStaticPages<StaticPageManagementBE>(intLanguageId);
                if (lstStaticPages != null)
                {
                    if (lstStaticPages.Count > 0)
                    {
                        lstStaticPages = lstStaticPages.FindAll(x => x.DisplayLocation.ToLower() == "footer");
                        if (lstStaticPages != null)
                        {
                            if (lstStaticPages.Count > 0)
                            {
                                HtmlGenericControl dv = new HtmlGenericControl("div");
                                dv.Attributes.Add("class", "col-xs-12 col-sm-4");
                                HtmlGenericControl h3Title = new HtmlGenericControl("h3");
                                h3Title.Attributes.Add("class", "pageSubTitle");
                                h3Title.InnerText = strHelp;
                                dv.Controls.Add(h3Title);
                                HtmlGenericControl parentUL = new HtmlGenericControl("ul");
                                #region Home Link
                                HtmlGenericControl liHome = new HtmlGenericControl("li");
                                HtmlAnchor aCategoryHome = new HtmlAnchor();
                                aCategoryHome.Attributes.Add("class", "smlHyperLink");
                                aCategoryHome.HRef = host + "Index";
                                aCategoryHome.InnerText = strHome;
                                liHome.Controls.Add(aCategoryHome);
                                parentUL.Controls.Add(liHome);
                                #endregion
                                foreach (var StaticPage in lstStaticPages)
                                {
                                    HtmlGenericControl li = new HtmlGenericControl("li");
                                    HtmlAnchor aCategory = new HtmlAnchor();
                                    aCategory.Attributes.Add("class", "smlHyperLink");
                                    aCategory.HRef = host + StaticPage.PageURL;
                                    aCategory.InnerText = StaticPage.PageName;
                                    #region ShowModal
                                    if (StaticPage.PageName == "Contact Us")
                                    {
                                        aCategory.Attributes.Add("data-toggle", "modal");
                                    }
                                    #endregion
                                    li.Controls.Add(aCategory);
                                    parentUL.Controls.Add(li);
                                }
                                dv.Controls.Add(parentUL);
                                dvSiteMapContent.Controls.Add(dv);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindMyAccount()
        {
            List<StaticPageManagementBE> lstStaticPages;
            try
            {
                lstStaticPages = StaticPageManagementBL.GetAllStaticPages<StaticPageManagementBE>(intLanguageId);
                if (lstStaticPages != null)
                {
                    if (lstStaticPages.Count > 0)
                    {
                        StaticPageManagementBE objStaticPage = lstStaticPages.Find(x => x.PageAliasName.ToLower() == "my account");
                        if (objStaticPage != null)
                        {
                            lstStaticPages = lstStaticPages.FindAll(x => x.ParentStaticPageId == objStaticPage.StaticPageId);
                            if (lstStaticPages != null)
                            {
                                if (lstStaticPages.Count > 0)
                                {
                                    HtmlGenericControl dv = new HtmlGenericControl("div");
                                    dv.Attributes.Add("class", "col-xs-12 col-sm-4");
                                    HtmlGenericControl h3Title = new HtmlGenericControl("h3");
                                    h3Title.Attributes.Add("class", "pageSubTitle");
                                    h3Title.InnerText = objStaticPage.PageName;
                                    dv.Controls.Add(h3Title);
                                    HtmlGenericControl parentUL = new HtmlGenericControl("ul");
                                    foreach (var StaticPage in lstStaticPages)
                                    {
                                        HtmlGenericControl li = new HtmlGenericControl("li");
                                        HtmlAnchor aCategory = new HtmlAnchor();
                                        aCategory.Attributes.Add("class", "smlHyperLink");
                                        aCategory.HRef = host + StaticPage.PageURL;
                                        aCategory.InnerText = StaticPage.PageName;

                                        if (StaticPage.IsLoginBased)
                                        {
                                            if (objUser.UserId > 0)
                                            {
                                                if (StaticPage.PageName == "Log in")
                                                {
                                                    aCategory.InnerText = "Log Out";
                                                    aCategory.HRef = host + "Logout";
                                                }
                                                li.Controls.Add(aCategory);
                                                parentUL.Controls.Add(li);
                                            }
                                        }
                                        else
                                        {
                                            li.Controls.Add(aCategory);
                                            parentUL.Controls.Add(li);
                                        }
                                    }
                                    dv.Controls.Add(parentUL);
                                    dvSiteMapContent.Controls.Add(dv);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindCategory()
        {
            List<CategoryBE> lstCategoryBE;
            Int16 iUsertypeID = 1;/*User Type*/
            try
            {
                /*User Type*/
                if (Session["User"] != null)
                {
                    UserBE objUserBE = Session["User"] as UserBE;
                    iUsertypeID = objUserBE.UserTypeID;
                }

                //lstCategoryBE = CategoryBL.GetAllCategories(intLanguageId, intCurrencyId);
                lstCategoryBE = CategoryBL.GetAllCategories(intLanguageId, intCurrencyId, iUsertypeID);/*User Type*/
                bool boolshowcategory = false;
                if (lstCategoryBE != null)
                {
                    if (lstCategoryBE.Count > 0)
                    {
                        List<CategoryBE> lstParentCategory = lstCategoryBE.FindAll(x => x.ParentCategoryId == 0);
                        if (lstParentCategory != null)
                        {
                            if (lstParentCategory.Count > 0)
                            {
                                HtmlGenericControl dv = new HtmlGenericControl("div");
                                dv.Attributes.Add("class", "col-xs-12 col-sm-4");
                                HtmlGenericControl h3Title = new HtmlGenericControl("h3");
                                h3Title.Attributes.Add("class", "pageSubTitle");
                                h3Title.InnerText = strProduct;
                                dv.Controls.Add(h3Title);
                                HtmlGenericControl parentUL = new HtmlGenericControl("ul");
                                foreach (var parentcategory in lstParentCategory)
                                {
                                    boolshowcategory = false;
                                    List<CategoryBE> objChildCategory = lstCategoryBE.FindAll(x => x.ParentCategoryId == parentcategory.CategoryId);
                                    List<CategoryBE> objChildChildCategory = new List<CategoryBE>();
                                    foreach (var childcategory in objChildCategory)
                                    {
                                        objChildChildCategory = lstCategoryBE.FindAll(x => x.ParentCategoryId == childcategory.CategoryId);
                                        objChildChildCategory = objChildChildCategory.FindAll(x => x.ProductCount > 0);
                                        if (objChildChildCategory.Count > 0)
                                        {
                                            boolshowcategory = true;
                                            break;
                                        }
                                    }
                                    if (!boolshowcategory)
                                    {
                                        objChildCategory = objChildCategory.FindAll(x => x.ProductCount > 0);
                                        if (objChildCategory.Count > 0)
                                        {
                                            boolshowcategory = true;
                                        }
                                    }
                                    HtmlGenericControl li = new HtmlGenericControl("li");
                                    HtmlAnchor aCategory = new HtmlAnchor();
                                    aCategory.Attributes.Add("class", "smlHyperLink");
                                    if (objChildCategory.Count > 0)
                                    {
                                        aCategory.HRef = host + "SubCategories/" + GlobalFunctions.EncodeCategoryURL(parentcategory.CategoryName);
                                    }
                                    else
                                    {
                                        aCategory.HRef = host + "Category/" + GlobalFunctions.EncodeCategoryURL(parentcategory.CategoryName);
                                    }
                                    aCategory.InnerText = parentcategory.CategoryName;
                                    li.Controls.Add(aCategory);

                                    if (boolshowcategory)
                                    {
                                        parentUL.Controls.Add(li);
                                    }
                                    else if (parentcategory.ProductCount > 0)
                                    {
                                        parentUL.Controls.Add(li);
                                    }
                                    else { }
                                }
                                dv.Controls.Add(parentUL);
                                dvSiteMapContent.Controls.Add(dv);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 04-11-15
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        //ltrTotalItems.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Pagination_TotalItems_Text").ResourceValue;
                        strHelp = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteMap_Help").ResourceValue;
                        strProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteMap_Product").ResourceValue;
                        strHome = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteMap_Home").ResourceValue;
                        SiteMap_Page_Heading = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteMap_Page_Heading").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "Sitemap");
                    if (lstMetaTags.Count > 0)
                    {
                        Page.Title = lstMetaTags[0].MetaContentTitle;
                        Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                        Page.MetaDescription = lstMetaTags[0].MetaDescription;
                    }
                    else
                    {
                        Page.Title = "";
                        Page.MetaKeywords = "";
                        Page.MetaDescription = "";
                    }
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}