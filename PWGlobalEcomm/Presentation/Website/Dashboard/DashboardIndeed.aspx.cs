﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.BusinessEntity;
using System.Configuration;
using System.Data;

namespace Presentation
{
    public class Dashboard_DashboardIndeed : BasePage
    {
        protected global::System.Web.UI.WebControls.GridView gvQTRRegion, gvQTRTeam, gvQTRNewProduct, gvQTRTopProduct;
        protected global::System.Web.UI.WebControls.DropDownList ddlCurrency;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnHeading, spnTotalOrder, spnTotalSales, spnTotal, spnProjected, spnOrderByRegion, spnOrderByTeam, 
        spnOrderUsage, spnNewProducts, spnTopProducts,spnFromLastQtr,spnProjectedSale;
        protected global::System.Web.UI.WebControls.Label lblCurrency;

        public string host = GlobalFunctions.GetVirtualPath();
        public string ProjectedSales = Convert.ToString(ConfigurationManager.AppSettings["ProjectedSales"]);

        public int LanguageId ;
        public int CurrencyId;


        //public int LanguageId = 1;
        //public int CurrencyId = 1;

        public string strRegion, strTotalOrders, strTotalSales, strTeam, strProduct, strProductName, strUnits, strSales,strNoOrderUsageMsg;

        protected void Page_Load(object sender, EventArgs e)
        {
            LanguageId = GlobalFunctions.GetLanguageId();
            CurrencyId = GlobalFunctions.GetCurrencyId();
       
           BindResourceData();

            if (!IsPostBack)
            {
                BindCurrency();
                BindColumnToGridviewRegion();
                BindColumnToGridviewTeam();
                BindColumnToGridviewNewProduct();
                BindColumnToGridviewTopProduct();
            }
        }

        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == LanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        spnHeading.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_Heading").ResourceValue;
                        spnTotalOrder.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_TotalOrders").ResourceValue;
                        spnTotalSales.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_TotalSales").ResourceValue;
                        spnTotal.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Total_Text").ResourceValue;
                        spnProjected.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_Projected").ResourceValue;
                        spnOrderByRegion.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_SalesOrderByRegion").ResourceValue;
                        spnOrderByTeam.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_SalesOrderByTeam").ResourceValue;
                        spnOrderUsage.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_OrderUsage").ResourceValue;
                        spnNewProducts.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_NewProducts").ResourceValue;
                        spnTopProducts.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_TopProducts").ResourceValue;
                        spnFromLastQtr.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_FromLastQuarter").ResourceValue;
                        spnProjectedSale.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_ProjectedSales").ResourceValue;
                        spnProjectedSale.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_ProjectedSales").ResourceValue;
                        strRegion = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_Region").ResourceValue;
                        strTotalOrders = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_TotalOrders").ResourceValue;
                        strTotalSales = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_TotalSales").ResourceValue;
                        strTeam = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_Team").ResourceValue;
                        strProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Product_Text").ResourceValue;
                        strProductName = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_ProductName").ResourceValue;
                        strUnits = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_Units").ResourceValue;
                        strSales = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_Sales").ResourceValue;
                        lblCurrency.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "SiteLinks_CurrencyTitle").ResourceValue;
                        strNoOrderUsageMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_NoOrderUsageMsg").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindColumnToGridviewRegion()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Groupvalue");
            dt.Columns.Add("Totalorders");
            dt.Columns.Add("TotalSales");
         
            dt.Rows.Add();
            gvQTRRegion.DataSource = dt;
            gvQTRRegion.DataBind();
          //  gvQTRRegion.Rows[0].Visible = false;
            //gvQTRRegion.UseAccessibleHeader = true;
            //gvQTRRegion.HeaderRow.TableSection = TableRowSection.TableHeader; 
        }

        private void BindColumnToGridviewTeam()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Team");
            dt.Columns.Add("Total Orders");
            dt.Columns.Add("Total Sales");
            dt.Rows.Add();
            gvQTRTeam.DataSource = dt;
            gvQTRTeam.DataBind();
            //  gvQTRRegion.Rows[0].Visible = false;
            //gvQTRTeam.UseAccessibleHeader = true;
            //gvQTRTeam.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        private void BindColumnToGridviewNewProduct()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Product");
            //dt.Columns.Add("Totalorders");
            //dt.Columns.Add("TotalSales");
            dt.Rows.Add();
            gvQTRNewProduct.DataSource = dt;
            gvQTRNewProduct.DataBind();
            //  gvQTRRegion.Rows[0].Visible = false;
            gvQTRNewProduct.UseAccessibleHeader = true;
            gvQTRNewProduct.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        private void BindColumnToGridviewTopProduct()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Product");
            dt.Columns.Add("Units");
            dt.Columns.Add("Sales");
            dt.Rows.Add();
            gvQTRTopProduct.DataSource = dt;
            gvQTRTopProduct.DataBind();
            //  gvQTRRegion.Rows[0].Visible = false;
            gvQTRTopProduct.UseAccessibleHeader = true;
            gvQTRTopProduct.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        [WebMethod]
        public static string GetTotalOrderData()
        {
            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_SALESGRANDTOTAL> lstSales = new List<IndeedSalesBE.BA_SALESGRANDTOTAL>();
            lstSales = IndeedSalesBL.GetAllTotalOrderDetails();
            if (lstSales != null)
            {
                if (lstSales.Count > 0)
                {
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsondata = jSearializer.Serialize(lstSales);
                }
            }
            return jsondata;
        }

        [WebMethod]
        public static string GetCurQtrSale(int CurrId)
        {
            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_SALESGRANDTOTAL> lstSales = new List<IndeedSalesBE.BA_SALESGRANDTOTAL>();
            lstSales = IndeedSalesBL.GetAllTotalSaleDetails(CurrId);
            if (lstSales != null)
            {
                if (lstSales.Count > 0)
                {
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsondata = jSearializer.Serialize(lstSales);
                }
            }
            return jsondata;
        }

        private void BindCurrency()
        {
            List<CurrencyBE> objCurr = new List<CurrencyBE>();
            objCurr = CurrencyBL.Indeed_GetAllCurrencyDetails();
            ddlCurrency.DataTextField = "CurrencyName";
            ddlCurrency.DataValueField = "CurrencyId";
            ddlCurrency.DataSource = objCurr;
            ddlCurrency.DataBind();
        }

        [WebMethod]
        public static IndeedSalesBE.BA_SALESBYGROUP[] GetSalesOrderByRegion(string ValueRange, int CurrId)
        {
            DataTable dt = new DataTable();
           // dt = IndeedSalesBL.GetAllTotalSaleOrderByRegion(ValueRange, CurrId);

            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_SALESBYGROUP> lstSales = new List<IndeedSalesBE.BA_SALESBYGROUP>();
            dt = IndeedSalesBL.GetAllTotalSaleOrderByRegion("R",ValueRange,CurrId);
            foreach (DataRow dtrow in dt.Rows)
            {
                IndeedSalesBE.BA_SALESBYGROUP user = new IndeedSalesBE.BA_SALESBYGROUP();
                user.Groupvalue = dtrow["Groupvalue"].ToString();
                user.Totalorders =Convert.ToInt32(dtrow["Totalorders"].ToString());
                user.TotalSales = dtrow["TotalSales"].ToString();
                lstSales.Add(user);
            }
            return lstSales.ToArray();
           // return lstSales.OrderByDescending(e => e.Totalorders).ToArray();

        }

        [WebMethod]
        public static IndeedSalesBE.BA_SALESBYGROUP[] GetSalesOrderByTeam(string ValueRange, int CurrId)
        {
            DataTable dt = new DataTable();

            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_SALESBYGROUP> lstSales = new List<IndeedSalesBE.BA_SALESBYGROUP>();
            dt = IndeedSalesBL.GetAllTotalSaleOrderByRegion("T",ValueRange, CurrId);
            foreach (DataRow dtrow in dt.Rows)
            {
                IndeedSalesBE.BA_SALESBYGROUP user = new IndeedSalesBE.BA_SALESBYGROUP();
                user.Groupvalue = dtrow["Groupvalue"].ToString();
                user.Totalorders = Convert.ToInt32(dtrow["Totalorders"].ToString());
                user.TotalSales = dtrow["TotalSales"].ToString();
                lstSales.Add(user);
            }
            return lstSales.ToArray();

            //System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //jsondata = jSearializer.Serialize(lstSales);

        }

        [WebMethod]
        public static IndeedSalesBE.BA_SALESBYPRODUCT[] GetNewProducts(string ValueRange, int CurrId, int Filter)
        {
            DataTable dt = new DataTable();

            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_SALESBYPRODUCT> lstSales = new List<IndeedSalesBE.BA_SALESBYPRODUCT>();
            dt = IndeedSalesBL.GetAllNewProduct("N",ValueRange, CurrId, Filter);
            foreach (DataRow dtrow in dt.Rows)
            {
                IndeedSalesBE.BA_SALESBYPRODUCT user = new IndeedSalesBE.BA_SALESBYPRODUCT();
                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + dtrow["DefaultImageName"].ToString()))
                {
                    user.DefaultImageName = dtrow["DefaultImageName"].ToString();
                }
                else
                {
                    user.DefaultImageName = "default.jpg";
                }
                //user.DefaultImageName = dtrow["DefaultImageName"].ToString();
                user.ProductName = dtrow["ProductName"].ToString();
                user.ProductId = dtrow["ProductId"].ToString();
                lstSales.Add(user);
            }
            return lstSales.ToArray();
            //System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //jsondata = jSearializer.Serialize(lstSales.OrderByDescending (e =>e.ProductCode).ToArray());
          //  jsResult.EVENTS = jsResult.EVENTS.OrderBy(e => e.TIME).ToArray();
        }

        [WebMethod]
        public static IndeedSalesBE.BA_SALESBYPRODUCT[] GetTopProducts(string ValueRange, int CurrId, int Filter)
        {
            DataTable dt = new DataTable();

            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_SALESBYPRODUCT> lstSales = new List<IndeedSalesBE.BA_SALESBYPRODUCT>();
            dt = IndeedSalesBL.GetAllNewProduct("T",ValueRange, CurrId, Filter);
            foreach (DataRow dtrow in dt.Rows)
            {
                IndeedSalesBE.BA_SALESBYPRODUCT user = new IndeedSalesBE.BA_SALESBYPRODUCT();
                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + dtrow["DefaultImageName"].ToString()))
                {
                    user.DefaultImageName = dtrow["DefaultImageName"].ToString();
                }
                else
                {
                    user.DefaultImageName = "default.jpg";
                }
               // user.DefaultImageName = dtrow["DefaultImageName"].ToString();
                user.ProductCode = dtrow["ProductCode"].ToString();
                user.Sales = Convert.ToInt32(dtrow["Sales"].ToString());
                user.Units = Convert.ToInt32(dtrow["Units"].ToString());
                user.ProductName = dtrow["ProductName"].ToString();
                user.ProductId = dtrow["ProductId"].ToString();
                lstSales.Add(user);
            }
            return lstSales.ToArray();

            //System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //jsondata = jSearializer.Serialize(lstSales);

        }

        [WebMethod]
        public static List<object> GetOrderUsage(string ValueRange)
        {
            DataTable dt = new DataTable();

            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_SALESBYGROUP> lstSales = new List<IndeedSalesBE.BA_SALESBYGROUP>();
            dt = IndeedSalesBL.GetAllOrderUsage(ValueRange);
            //foreach (DataRow dtrow in dt.Rows)
            //{
            //    IndeedSalesBE.BA_SALESBYGROUP user = new IndeedSalesBE.BA_SALESBYGROUP();
            //    user.Groupvalue = dtrow["Groupvalue"].ToString();
            //    user.Totalorders = Convert.ToInt32(dtrow["Totalorders"].ToString());
            //    lstSales.Add(user);
            //}
            List<object> chartData = new List<object>();
            chartData.Add(new object[]
        {
            "GroupValue", "TotalOrders" 
        });
            foreach (DataRow dtrow in dt.Rows)
            {
                chartData.Add(new object[]
                    {
                        dtrow["GroupValue"].ToString().Replace("\r","<br/>"), dtrow["TotalOrders"] 
                    });

            }

            return chartData;
            //System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //jsondata = jSearializer.Serialize(lstSales);

        }
    }
}