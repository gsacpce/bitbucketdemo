﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Dashboard_BudgetTransaction : BasePage
    {
        public string host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.GridView gvBudgetTransaction;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnHeading, spnBackToMyAcc, spnSpent1, spnPending1, spnAvailable1, spnBudgetTrans;

        public int LanguageId;
        public int CurrencyId;

        public string strDate, strOrderNumber, strTotal;

        protected void Page_Load(object sender, EventArgs e)
        {
            LanguageId = GlobalFunctions.GetLanguageId().To_Int16();
            CurrencyId = GlobalFunctions.GetCurrencyId();

            BindResourceData();

            if (!IsPostBack)
            {
                BindColumnToGridviewBudget();
            }
        }

        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == LanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        spnHeading.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BudgetTransactions").ResourceValue;
                        spnBackToMyAcc.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BackToMyAccount").ResourceValue;
                        spnSpent1.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Spent").ResourceValue;
                        spnPending1.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Pending").ResourceValue;
                        spnAvailable1.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Available").ResourceValue;
                        spnBudgetTrans.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BudgetTransactions").ResourceValue;
                        strDate = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Date").ResourceValue;
                        strOrderNumber = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Order_Number_Text").ResourceValue;
                        strTotal = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Total_Text").ResourceValue;

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindColumnToGridviewBudget()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Date");
            dt.Columns.Add("Order Number");
            dt.Columns.Add("Total");

            dt.Rows.Add();
            gvBudgetTransaction.DataSource = dt;
            gvBudgetTransaction.DataBind();
        }

        [WebMethod]
        public static OrderHeaders[] GetBudgetTransactionDetails()
        {
            List<OrderHeaders> lstMain = new List<OrderHeaders>();
            List<OrderHeaders> lstSortedOrderHeaders = new List<OrderHeaders>();

            string jsondata = string.Empty;


            UserBE objUserBE = (UserBE)HttpContext.Current.Session["User"];
            List<IndeedSalesBE.BA_Budget> lstBudget = new List<IndeedSalesBE.BA_Budget>();

            if (objUserBE.CustomColumn2.Length > 0)
            {
                lstBudget = IndeedSalesBL.GetBudgetTransactionPerUser(objUserBE.UserId); 

                if(lstBudget==null)
                {
                    lstBudget = IndeedSalesBL.GetBudgetTransactionUser(objUserBE.EmailId); 
                }
            }
            else
            {
                lstBudget = IndeedSalesBL.GetBudgetTransactionUser(objUserBE.EmailId); 
            }

            //lstBudget = IndeedSalesBL.GetBudgetTransactionPerUser(objUserBE.UserId);
            Exceptions.WriteInfoLog("GetBudgetTransactionDetails() - UserId : " + objUserBE.UserId);


            SalesOrders objBudget = new SalesOrders();

            if (lstBudget != null)
            {
                for (int i = 0; i < lstBudget.Count; i++)
                {
                    Exceptions.WriteInfoLog("BudgetRefCode" + i + " : " + lstBudget[i].Budget_Code);
                    Exceptions.WriteInfoLog("Division_ID" + i + " : " + lstBudget[i].Division_ID);
                    Exceptions.WriteInfoLog("Currency_Symbol" + i + " : " + lstBudget[i].Currency_Symbol);
                    Exceptions.WriteInfoLog("SequenceNo" + i + " : " + lstBudget[i].SequenceNo);

                    objBudget = Budget_OASIS.GetBudgetTransactionDetails(lstBudget[i].Division_ID, lstBudget[i].SequenceNo, lstBudget[i].Budget_Code);

                    // objBudget = Budget_OASIS.GetBudgetTransactionDetails(265, 1, "4210697769");
                    if (objBudget.orderHeaders != null)
                    {
                        lstSortedOrderHeaders = objBudget.orderHeaders.ToList<OrderHeaders>();
                    }
                    if (lstSortedOrderHeaders.Count > 0)
                    {
                        lstMain.AddRange(lstSortedOrderHeaders.Where(x => x.OrderStatus.ToLower() != "cancelled").OrderByDescending(x => x.OrderDate));
                    }
                }
            }
            else
            {
                Exceptions.WriteInfoLog("User does not Exists in BA_Budgets");
            }

            if (lstMain != null)
            {
                Exceptions.WriteInfoLog("Budget Transaction Webservice returns data");
            }
            else
            {
                Exceptions.WriteInfoLog("Budget Transaction Webservice does not return data");
            }

            HttpContext.Current.Session["ObjOrderHistory"] = lstMain;
            return lstMain.ToArray();
        }

        [WebMethod]
        public static string RedirectOrderHistory(int salesorderid, string CurrSymbol)
        {
            string msg = string.Empty;

            try
            {
                List<OrderHeaders> lstOrderHeader = (List<OrderHeaders>)HttpContext.Current.Session["ObjOrderHistory"];

                var next = "";
                var prev = "";

                HttpContext.Current.Session["NextOrderItemId"] = next;
                HttpContext.Current.Session["PrevOrderItemId"] = prev;
                HttpContext.Current.Session["CurrOrderItemId"] = salesorderid;
                HttpContext.Current.Session["IndeedCurrSymbol"] = CurrSymbol;
                msg = "success";
                return msg;

            }
            catch (Exception ex)
            {
                return msg;
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}