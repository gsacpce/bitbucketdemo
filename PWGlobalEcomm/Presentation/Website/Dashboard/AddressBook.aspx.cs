﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.BusinessEntity;
using System.Configuration;
using System.Web.Services;
using System.Web.UI.HtmlControls;

namespace Presentation
{
    public partial class Dashboard_AddressBook : BasePage
    {
        protected global::System.Web.UI.WebControls.GridView GridView1;
        protected global::System.Web.UI.WebControls.DropDownList ddlCountry;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnHeading, spnBackToMyAcc, spnAddressTitle, spnAddress, spnCompany, spnAddressLine1,
        spnAddressLine2, spnTown, spnCounty, spnPostcode, spnCountry;
        protected global::System.Web.UI.HtmlControls.HtmlButton btn1;

        public int LanguageId;
        public int CurrencyId;
        public string host = GlobalFunctions.GetVirtualPath();
        public string option = string.Empty;


        public string strMandatory, strBillingAddrMsg, strAddressUpdate, strDeleterAlert,strAddressTitle,strCancel,strSaveAddress;

        protected void Page_Load(object sender, EventArgs e)
        {
            LanguageId = GlobalFunctions.GetLanguageId();
            CurrencyId = GlobalFunctions.GetCurrencyId();
            option = Request.QueryString["mode"];


            BindResourceData();

            if(!IsPostBack)
            {
                BindGrid();
                BindCountryDropDown();
            }
        }

        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == LanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        spnHeading.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_AddressBook").ResourceValue;
                        spnBackToMyAcc.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BackToMyAccount").ResourceValue;
                        spnAddressTitle.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_AddressBook").ResourceValue;
                        spnAddress.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Address").ResourceValue;
                        spnCompany.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Company_Text").ResourceValue;
                        spnAddressLine1.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Address1_Text").ResourceValue;
                        spnAddressLine2.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Address2_Text").ResourceValue;
                        spnTown.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Town_Text").ResourceValue;
                        spnCounty.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_County_Text").ResourceValue;
                        spnPostcode.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Postcode").ResourceValue;
                        spnCountry.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Country_Text").ResourceValue;
                        strCancel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Cancel").ResourceValue;
                        strSaveAddress= lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_SaveAddress").ResourceValue;
                        strMandatory = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_MandatoryDetailMsg").ResourceValue;
                        strBillingAddrMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BillingAddressMsg").ResourceValue;
                        strAddressUpdate = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_AddressUpdatedMsg").ResourceValue;
                        strDeleterAlert = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_DeleteAlertMsg").ResourceValue;
                        strAddressTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_AddressBook_AddressTitle").ResourceValue;
                    }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindGrid()
        {
            try
            {
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                List<IndeedSalesBE.BA_UserAddress> userAddrs = new List<IndeedSalesBE.BA_UserAddress>();

                userAddrs = IndeedSalesBL.GetAllCustomerAddress(lstUser.UserId, option);
                if (userAddrs.Count > 0)
                {
                    GridView1.DataSource = userAddrs;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            
        }

        protected void BindCountryDropDown()
        {
            try
            {
                Exceptions.WriteInfoLog(" Inside BindCountryDropDown()");
                List<CountryBE> lstCountry = new List<CountryBE>();
                lstCountry = CountryBL.GetAllCountries();
                ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
                ddlCountry.AppendDataBoundItems = true;
                if (lstCountry != null && lstCountry.Count > 0)
                {
                    lstCountry = lstCountry.FindAll(x => x.RegionCode != "UA");
                    ddlCountry.DataTextField = "CountryName";
                    ddlCountry.DataValueField = "CountryId";
                    Exceptions.WriteInfoLog("Before assingning DataSource to ddlDCountry in BindCountryDropDown()");
                    ddlCountry.DataSource = lstCountry;
                    ddlCountry.DataBind();
                    Exceptions.WriteInfoLog("Before DataBind event of ddlDCountry in BindCountryDropDown()");

                  
                }
                ddlCountry.SelectedIndex = 0;
                Exceptions.WriteInfoLog("Exiting BindCountryDropDown()");
            }
            //}
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        [WebMethod]
        public static string GetAddressData(int id, string option)
        {
            string jsondata = string.Empty;

            try
            {
                List<UserBE.UserDeliveryAddressBE> lstUserAddress = new List<UserBE.UserDeliveryAddressBE>();
                lstUserAddress = IndeedSalesBL.GetUserDeliveryAddresDetails(id, option);
                if (lstUserAddress.Count > 0)
                {
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsondata = jSearializer.Serialize(lstUserAddress);
                }
                return jsondata;

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return jsondata;
            }
        }

        [WebMethod]
        public static string UpdateUserAddressData(int id, string option, string company, string add1, string add2, string town, string county, string postcode, string country)
        {
            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_UserAddress> lstUserAddress = new List<IndeedSalesBE.BA_UserAddress>();
            UserBE.UserDeliveryAddressBE objAddress = new UserBE.UserDeliveryAddressBE();
            objAddress.PreDefinedColumn2 = company;
            objAddress.PreDefinedColumn3 = add1;
            objAddress.PreDefinedColumn4 = add2;
            objAddress.PreDefinedColumn5 = town;
            objAddress.PreDefinedColumn6 = county;
            objAddress.PreDefinedColumn7 = postcode;
            objAddress.PreDefinedColumn8 = country;
            lstUserAddress = IndeedSalesBL.UpdateUserDeliveryAddresDetails(id, option, objAddress);
            if (lstUserAddress.Count > 0)
            {
                System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                jsondata = jSearializer.Serialize(lstUserAddress);
            }
            return jsondata;
        }

            [WebMethod]
        public static string DeleteUserAddressData(int id)
        {
            string jsondata = string.Empty;
            bool isDeleted = IndeedSalesBL.DeleteUserDeliveryAddress(id);
                if(isDeleted==true)
                {
                    jsondata = "Success";
                }
                else
                {
                    jsondata = "Fail";
                }
                return jsondata;
        }

    }
}