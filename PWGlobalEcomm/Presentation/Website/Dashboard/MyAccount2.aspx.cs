﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.BusinessEntity;
using System.Configuration;
using System.Data;
using System.Web.Services;
using PWGlobalEcomm.GlobalUtilities;


namespace Presentation
{
    public partial class Dashboard_MyAccount2 : BasePage
    {
        public string host = GlobalFunctions.GetVirtualPath();
        public string emailid = string.Empty;

        protected global::System.Web.UI.WebControls.TextBox txtOldPass, txtNewPass, txtConfirmPass;
        protected global::System.Web.UI.WebControls.RadioButton rdProfile_MarketingOption1, rdProfile_MarketingOption2;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnHeading, spnContactInformation, spnBudget, spnExpirationText, spnBudgetTrans, spnPoints,
        spnPointsText, spnBdayAward, spnAnvAward, spnPointsTrans, spnOrderHistory, spnOrder1, spnOrder2, spnViewOrder, spnAddressBook, spnAddrs1, spnAddrs2, spnBillingAddress,
        spnViewAddress, spnMarketPre, spnUpdate, spnChangePassword, divChangePass, spnNoBudget;
        protected global::System.Web.UI.WebControls.Label lblOldPassword, lblNewPassword, lblConfPassword;
        protected global::System.Web.UI.WebControls.Button btnPassUpdate;

        public int LanguageId;
        public int CurrencyId;
        public bool IsBudgetActive = false;
        public string strSpent, strPending, strAvailable, strIncorrectPass, strPassUpdate, strErrorPass, strNoBudgetMsg, strNoContactInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] == null)
            {
                Response.RedirectToRoute("login-page");
                return;
            }
            if (Session["IndeedSSO_User"] != null || Session["IndeedEmployeeSSO_User"] != null)
            {
                divChangePass.Visible = false;
            }
            else
            {
                divChangePass.Visible = true;
            }


            LanguageId = GlobalFunctions.GetLanguageId().To_Int16();
            CurrencyId = GlobalFunctions.GetCurrencyId();

            UserBE lstUser = (UserBE)Session["User"];
            IsBudgetActive = UserTypesBL.IsBudgetActive(GlobalFunctions.GetCurrencyId(),lstUser.UserTypeID);

            BindResourceData();

            if (!IsPostBack)
            {
                //GetBudgetDetails();
                if (lstUser.IsMarketing)
                {
                    rdProfile_MarketingOption1.Checked = true;
                    rdProfile_MarketingOption2.Checked = false;
                }
                else
                {
                    rdProfile_MarketingOption1.Checked = false;
                    rdProfile_MarketingOption2.Checked = true;
                }
            }
            txtOldPass.Attributes["value"] = txtOldPass.Text;
            txtNewPass.Attributes["value"] = txtNewPass.Text;
            txtConfirmPass.Attributes["value"] = txtConfirmPass.Text;


        }

        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == LanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        spnHeading.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_MyProfile_Heading").ResourceValue;
                        spnContactInformation.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_ContactInformation").ResourceValue;
                        spnBudget.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Budget").ResourceValue;
                        spnExpirationText.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Expiration").ResourceValue;
                        spnBudgetTrans.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BudgetTransactions").ResourceValue;
                        spnPoints.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Points_Text").ResourceValue;
                        spnPointsText.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_YouCurrentlyHave").ResourceValue;
                        spnBdayAward.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BirthdayAward").ResourceValue;
                        spnAnvAward.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_AnniversaryAward").ResourceValue;
                        spnPointsTrans.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_PointTransaction").ResourceValue;
                        spnOrderHistory.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_OrderHistory").ResourceValue;
                        spnOrder1.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_YouCurrentlyHave").ResourceValue;
                        spnOrder2.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_PreviousOrders").ResourceValue;
                        spnViewOrder.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_ViewOrders").ResourceValue;
                        spnAddressBook.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_AddressBook").ResourceValue;
                        spnAddrs1.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_YouCurrentlyHave").ResourceValue;
                        spnAddrs2.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_AddressesinYourBook").ResourceValue;
                        spnViewAddress.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_ViewEdit").ResourceValue;
                        spnBillingAddress.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_ViewEdit").ResourceValue;
                        spnMarketPre.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Marketing_Preferences").ResourceValue;
                        rdProfile_MarketingOption1.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_MarketingOption1").ResourceValue;
                        rdProfile_MarketingOption2.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_MarketingOption2").ResourceValue;
                        spnUpdate.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Update").ResourceValue;
                        btnPassUpdate.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Update").ResourceValue;
                        lblOldPassword.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_OldPassword").ResourceValue + "*";
                        lblNewPassword.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_NewPassword").ResourceValue + "*";
                        lblConfPassword.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_ConfirmNewPass").ResourceValue + "*";
                        spnChangePassword.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_ChangePassword").ResourceValue;
                        strSpent = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Spent").ResourceValue;
                        strPending = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Pending").ResourceValue;
                        strAvailable = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Available").ResourceValue;
                        strIncorrectPass = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Profile_Incorrect_Password").ResourceValue;
                        strPassUpdate = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Update_Password_success").ResourceValue;
                        strErrorPass = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Update_Password_error").ResourceValue;
                        strNoBudgetMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Budget_NoBudgetMsg").ResourceValue;
                        strNoContactInfo = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_Dashboard_NoContactInformationMsg").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        [WebMethod]
        public static string GetContactInformation()
        {
            UserBE lstUser = new UserBE();
            lstUser = HttpContext.Current.Session["User"] as UserBE;

            string jsondata = string.Empty;
            List<UserBE.BA_USERFILE> user = new List<UserBE.BA_USERFILE>();

            if (lstUser.CustomColumn2.Length > 0)
            {
                user = IndeedSalesBL.GetAllContactInformationDetails(lstUser.CustomColumn2);

                if(user==null)
                {
                    user = IndeedSalesBL.GetContactInformation(lstUser.UserId);
                }
            }
            else
            {
                user = IndeedSalesBL.GetContactInformation(lstUser.UserId);
            }
            if (user != null)
            {
                System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                jsondata = jSearializer.Serialize(user);
            }
            return jsondata;
        }


        [WebMethod]
        public static int GetCustomerOrdersCount()
        {
            try
            {
                FeatureBE OrderHistoryMonthstoReturn = new FeatureBE();
                List<OrderHeaders> lstSortedOrderHeaders = new List<OrderHeaders>();
                string contactlist = "";
                int OrderCount = 0;
                Int16 StoreId = 0;
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    StoreId = objStoreBE.StoreId;
                    List<FeatureBE> GetAllFeatures = FeatureBL.GetAllFeatures(StoreId, true);
                    OrderHistoryMonthstoReturn = GetAllFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "orderhistorymonthstoreturn");
                }

                int OasisContactId = 0;
                DateTime StartDate = DateTime.Now;

                //Added By Hardik for "calculate the StartDate parameter as today’s date minus x months"
                int month = Convert.ToInt32(OrderHistoryMonthstoReturn.FeatureValues[0].FeatureDefaultValue);
                StartDate = StartDate.AddMonths(-month);

                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                if (lstUser != null)
                {

                    UserBE objUserBE = (UserBE)HttpContext.Current.Session["User"];

                    /* Changes done by Snehal - Instructed By Reena ma'am */
                    Exceptions.WriteInfoLog("HttpContext.Current.Session['IndeedSSO_User']['IndeedEmployeeSSO_User']");
                    if (HttpContext.Current.Session["IndeedSSO_User"] != null || HttpContext.Current.Session["IndeedEmployeeSSO_User"] != null)
                    {
                        Exceptions.WriteInfoLog("MyAccount2.aspx - Session IndeedSSO_User != Null Or IndeedEmployeeSSO_User != Null");
                    }
                    else
                    {
                        Exceptions.WriteInfoLog("MyAccount2.aspx - Session IndeedSSO_User = Null Or IndeedEmployeeSSO_User = Null");
                    }

                    Exceptions.WriteInfoLog("GlobalFunctions.GetCurrencyId() = " + Convert.ToString(GlobalFunctions.GetCurrencyId()));
                    Exceptions.WriteInfoLog("objUserBE.CurrencyID = " + Convert.ToString(objUserBE.CurrencyId));

                    if (HttpContext.Current.Session["IndeedSSO_User"] != null || HttpContext.Current.Session["IndeedEmployeeSSO_User"] != null)
                    {
                        objUserBE = UserBL.Getorderhistory(Constants.USP_GetOrdershistory, true, objUserBE);
                    }
                    else
                    {
                        objUserBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        objUserBE = UserBL.Getorderhistory(Constants.USP_GetOrdershistory, true, objUserBE);
                    }

                    /* END */

                    contactlist = Convert.ToString(objUserBE.BASYS_CustomerContactId);
                }

                SalesOrders objSalesOrders = new SalesOrders();
                SalesOrders objSalesOrderStartDate = new SalesOrders();

                if (contactlist != null)
                {
                    string[] contact = contactlist.Split(',');
                    for (int i = 0; i < contact.Length; i++)
                    {
                        OasisContactId = Convert.ToInt32(contact[i]);
                        //objSalesOrderStartDate = UserOrder_OASIS.GetAllUserOrderStartDate(1116266, StartDate);
                        objSalesOrderStartDate = UserOrder_OASIS.GetAllUserOrderStartDate(OasisContactId, StartDate);
                        if (objSalesOrderStartDate.orderHeaders != null)
                        {
                            lstSortedOrderHeaders = objSalesOrderStartDate.orderHeaders.OrderByDescending(x => x.SalesOrderID).ToList<OrderHeaders>();
                        }
                    }
                }

                OrderCount = lstSortedOrderHeaders.Count;
                return OrderCount;

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }

        }

        [WebMethod]
        public static string GetBudgetDetails()
        {
            List<BudgetNode> lstMain = new List<BudgetNode>();
            List<BudgetNode> lstBudgetNode = new List<BudgetNode>();
            string jsondata = string.Empty;


            UserBE objUserBE = (UserBE)HttpContext.Current.Session["User"];
            List<IndeedSalesBE.BA_Budget> lstBudget = new List<IndeedSalesBE.BA_Budget>();

            if (objUserBE.CustomColumn2.Length > 0)
            {
                lstBudget = IndeedSalesBL.GetBudgetPerUser(objUserBE.UserId, 0); // As instructed by Reena ma'am 27 03 2017

                if(lstBudget==null)
                {
                    lstBudget = IndeedSalesBL.GetBudgetUser(objUserBE.EmailId, GlobalFunctions.GetCurrencyId()); 
                }
            }
            else
            {
                lstBudget = IndeedSalesBL.GetBudgetUser(objUserBE.EmailId, GlobalFunctions.GetCurrencyId()); 
            }
            Exceptions.WriteInfoLog("GetBudgetDetails() - UserId : " + objUserBE.UserId);
   
            Budgets objBudget = new Budgets();

            if (lstBudget != null)
            {
                Exceptions.WriteInfoLog("User Exists in BA_Budgets");

                for (int i = 0; i < lstBudget.Count; i++)
                {
                    Exceptions.WriteInfoLog("Budget Ref Code : " + lstBudget[i].Budget_Code);
                    Exceptions.WriteInfoLog("Division_ID : " + lstBudget[i].Division_ID);
                    Exceptions.WriteInfoLog("Currency_Symbol : " + lstBudget[i].Currency_Symbol);

                    objBudget = Budget_OASIS.GetBudgetDetails(lstBudget[i].Budget_Code, lstBudget[i].Division_ID, lstBudget[i].Currency_Symbol);

                    //objBudget = Budget_OASIS.GetBudgetDetails("BAUK000004_1_2017", 382, "USD");

                    if (objBudget.BudgetNodes != null)
                    {

                        lstBudgetNode = objBudget.BudgetNodes.ToList<BudgetNode>();
                        if (lstBudgetNode.Count > 0)
                        {
                            if (Convert.ToDecimal(lstBudgetNode[i].WebCurrTotalBudget) > 0 || Convert.ToDecimal(lstBudgetNode[i].WebCurrOrdersNotInvoiced) > 0 || Convert.ToDecimal(lstBudgetNode[i].WebCurrOutstandingBudget) > 0 || Convert.ToDecimal(lstBudgetNode[i].WebCurrOrdersInvoiced) > 0)
                            {
                                lstMain.AddRange(lstBudgetNode);
                            }
                        }
                    }

                }
               

                System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                jsondata = jSearializer.Serialize(lstMain);

            }
            else
            {
                Exceptions.WriteInfoLog("User does not Exists in BA_Budgets");
            }
            // return lstMain.ToArray();

            return jsondata;
        }

        [WebMethod]
        public static OrderHeaders[] GetBudgetTransactionDetails()
        {
            List<OrderHeaders> lstMain = new List<OrderHeaders>();
            List<OrderHeaders> lstSortedOrderHeaders = new List<OrderHeaders>();

            string jsondata = string.Empty;


            UserBE objUserBE = (UserBE)HttpContext.Current.Session["User"];
            List<IndeedSalesBE.BA_Budget> lstBudget = new List<IndeedSalesBE.BA_Budget>();

            if (objUserBE.CustomColumn2.Length > 0)
            {
                lstBudget = IndeedSalesBL.GetBudgetTransactionPerUser(objUserBE.UserId);              

                if(lstBudget==null)
                {
                    lstBudget = IndeedSalesBL.GetBudgetTransactionUser(objUserBE.EmailId); 
                }
            }
            else
            {
                lstBudget = IndeedSalesBL.GetBudgetTransactionUser(objUserBE.EmailId); 
            }

         //   lstBudget = IndeedSalesBL.GetBudgetPerUser(objUserBE.UserId, GlobalFunctions.GetCurrencyId());
            Exceptions.WriteInfoLog("GetBudgetTransactionDetails() - UserId : " + objUserBE.UserId);

            SalesOrders objBudget = new SalesOrders();

            #region Commented By Snehal - 27 03 2017

            // objBudget = Budget_OASIS.GetBudgetDetails(lstBudget[0].Budget_Code, lstBudget[0].Division_ID,lstBudget[0].Currency_Symbol);
            //objBudget = Budget_OASIS.GetBudgetTransactionDetails(265, 4, "4210697769");
            //if (objBudget.orderHeaders != null)
            //{
            //    lstSortedOrderHeaders = objBudget.orderHeaders.ToList<OrderHeaders>();
            //}
            //if (lstSortedOrderHeaders.Count > 0)
            //{
            //    lstMain.AddRange(lstSortedOrderHeaders);
            //    //lstSortedOrderHeaders.
            //    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //    jsondata = jSearializer.Serialize(lstSortedOrderHeaders);
            //}

            #endregion

            if (lstBudget != null)
            {
                for (int i = 0; i < lstBudget.Count; i++)
                {
                    Exceptions.WriteInfoLog("BudgetRefCode" + i + " : " + lstBudget[i].Budget_Code);
                    Exceptions.WriteInfoLog("Division_ID" + i + " : " + lstBudget[i].Division_ID);
                    Exceptions.WriteInfoLog("Currency_Symbol" + i + " : " + lstBudget[i].Currency_Symbol);
                    Exceptions.WriteInfoLog("SequenceNo" + i + " : " + lstBudget[i].SequenceNo);

                    objBudget = Budget_OASIS.GetBudgetTransactionDetails(lstBudget[i].Division_ID, lstBudget[i].SequenceNo, lstBudget[i].Budget_Code);

                    //objBudget = Budget_OASIS.GetBudgetTransactionDetails(265, 1, "4210697769");

                    if (objBudget.orderHeaders != null)
                    {
                        lstSortedOrderHeaders = objBudget.orderHeaders.ToList<OrderHeaders>();
                    }
                    if (lstSortedOrderHeaders.Count > 0)
                    {
                        lstMain.AddRange(lstSortedOrderHeaders);
                    } 
                }
            }
            else
            {
                Exceptions.WriteInfoLog("User does not Exists in BA_Budgets");

            }
            if (lstMain != null)
            {
                Exceptions.WriteInfoLog("Budget Transaction Webservice returns data");
            }
            else
            {
                Exceptions.WriteInfoLog("Budget Transaction Webservice does not return data");
            }

            HttpContext.Current.Session["ObjOrderHistory"] = lstMain;
            return lstMain.ToArray();
        }

        [WebMethod]
        public static int UpdatePassword(string oldpass, string newpass)
        {
            UserBE objUserBE = (UserBE)HttpContext.Current.Session["User"];
            if (SaltHash.ComputeHash((oldpass.Trim()), "SHA512", null) != objUserBE.Password)
            {
                return 0;
            }
            else
            {
                string SaltPass = SaltHash.ComputeHash((oldpass), "SHA512", null);
                int Id = IndeedSalesBL.ChangePassword(objUserBE.UserId, SaltHash.ComputeHash((oldpass), "SHA512", null), SaltHash.ComputeHash((newpass), "SHA512", null));
                if (Id == 1)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }

            }

        }

        [WebMethod]
        public static int GetCustomerAddressCount()
        {
            int Count = 0;
            try
            {
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                List<IndeedSalesBE.BA_UserAddress> userAddrs = new List<IndeedSalesBE.BA_UserAddress>();

                userAddrs = IndeedSalesBL.GetAllCustomerAddress(lstUser.UserId, "S");
                Count = userAddrs.Count;
                return Count;
            }
            catch (Exception ex)
            {
                return Count;
            }
        }

        [WebMethod]
        public static string GetUserPointsData()
        {
            string jsondata = string.Empty;
            try
            {
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                List<IndeedSalesBE.BA_Points> userPoints = new List<IndeedSalesBE.BA_Points>();
                userPoints = IndeedSalesBL.GetUsersPoint(lstUser.UserId);
                if (userPoints != null)
                {
                    if (userPoints.Count > 0)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        jsondata = jSearializer.Serialize(userPoints);
                    }
                }
                return jsondata;


            }
            catch (Exception ex)
            {
                return jsondata;
            }
        }

        [WebMethod]
        public static string UpdateMarketPreference(bool option)
        {
            string strIsMarketing = string.Empty;
            try
            {
                UserBE lstUser = new UserBE();
                lstUser = HttpContext.Current.Session["User"] as UserBE;
                lstUser.IsMarketing = option;
                IndeedSalesBL.UpdateMarketPreference(lstUser);
                strIsMarketing = RegisterCustomer_OASIS.IsMarketingUpdateContact(lstUser);
                return strIsMarketing;
            }
            catch (Exception ex)
            {
                return strIsMarketing;
            }
        }
    }
}