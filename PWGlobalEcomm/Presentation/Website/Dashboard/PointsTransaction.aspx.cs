﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Dashboard_PointsTransaction : BasePage
    {
        public string host = GlobalFunctions.GetVirtualPath();
        protected global::System.Web.UI.WebControls.GridView gvPointsTransaction;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnHeading, spnBackToMyAcc, spnTotalBalance, spnBirthdayAward, spnAnnvAward, spnPonitsTrans;

        public string strDate, strEvent, strPoints;

        public int LanguageId;
        public int CurrencyId;

        protected void Page_Load(object sender, EventArgs e)
        {
            LanguageId = GlobalFunctions.GetLanguageId();
            CurrencyId = GlobalFunctions.GetCurrencyId();

            BindResourceData();


            if (!IsPostBack)
            {
                BindColumnToGridviewPoints();
            }
        }

        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == LanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        spnHeading.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_PointTransaction").ResourceValue;
                        spnBackToMyAcc.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BackToMyAccount").ResourceValue;
                        spnTotalBalance.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_TotalBalance").ResourceValue;
                        spnBirthdayAward.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_BirthdayAward").ResourceValue;
                        spnAnnvAward.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_AnniversaryAward").ResourceValue;
                        spnPonitsTrans.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_PointTransaction").ResourceValue;
                        strDate = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_MyProfile_Date").ResourceValue;
                        strEvent = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Event").ResourceValue;
                        strPoints = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Points_Text").ResourceValue;

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindColumnToGridviewPoints()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("dateProcessed");
            dt.Columns.Add("Reason");
            dt.Columns.Add("Points");

            dt.Rows.Add();
            gvPointsTransaction.DataSource = dt;
            gvPointsTransaction.DataBind();
        }

        [WebMethod]
        public static IndeedSalesBE.BA_Points[] GetPointsTransactionDetails()
        {
            UserBE lstUser = new UserBE();
            lstUser = HttpContext.Current.Session["User"] as UserBE;
            DataTable dt = new DataTable();

            string jsondata = string.Empty;
            List<IndeedSalesBE.BA_Points> lstSales = new List<IndeedSalesBE.BA_Points>();
            dt = IndeedSalesBL.GetPointsTransactionDetails(lstUser.UserId);
            foreach (DataRow dtrow in dt.Rows)
            {
                IndeedSalesBE.BA_Points user = new IndeedSalesBE.BA_Points();
                user.dateProcessed = dtrow["dateProcessed"].ToString();
                user.Reason = dtrow["Reason"].ToString();
                user.points = Convert.ToInt32(dtrow["points"]);
                lstSales.Add(user);
            }
            return lstSales.ToArray();
            // return lstSales.OrderByDescending(e => e.Totalorders).ToArray();

        }


    }
}