﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;


namespace Presentation
{
    public partial class Dashboard_ProductView : BasePage
    {

        protected global::System.Web.UI.WebControls.Literal ltrProductTitle, ltrProductCode, ltrDescription;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aViewDetails;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgProduct;

        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        Int32 intProductId = 0;
        Int16 iUserTypeID = 1;/*User Type*/

        string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetMediumProductImagePath();

        public string strViewProduct;

        ProductBE objProductBE;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                intLanguageId = Convert.ToInt16(Request.Form["languageid"]);
                intCurrencyId = Convert.ToInt16(Request.Form["currencyid"]);
                intProductId = Convert.ToInt32(Request.Form["productid"]);

                BindResourceData();

                BindProductData();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strViewProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Indeed_ViewProduct").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindProductData()
        {
            try
            {
                /*User Type*/
                if (Session["User"] != null)
                {
                    UserBE lstUser = new UserBE();
                    lstUser = Session["User"] as UserBE;
                    iUserTypeID = lstUser.UserTypeID;
                }
                //objProductBE = ProductBL.GetProductDetails(intProductId, intLanguageId, intCurrencyId);
                objProductBE = ProductBL.GetProductDetails(intProductId, intLanguageId, intCurrencyId, iUserTypeID);/*User Type*/

                if (objProductBE != null)
                {

                    ltrDescription.Text = Convert.ToString(objProductBE.ProductDescription).Trim();
                    ltrProductTitle.Text = objProductBE.ProductName;
                    ltrProductCode.Text = objProductBE.ProductCode;

                    if (!string.IsNullOrEmpty(objProductBE.CategoryName) && !string.IsNullOrEmpty(objProductBE.SubCategoryName) && !string.IsNullOrEmpty(objProductBE.SubSubCategoryName))
                    {
                        if (objProductBE.CategoryName == objProductBE.SubCategoryName && objProductBE.CategoryName == objProductBE.SubSubCategoryName)
                        {
                            aViewDetails.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(objProductBE.CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductCode.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductName);
                        }
                        else if (objProductBE.CategoryName == objProductBE.SubCategoryName && objProductBE.SubCategoryName != objProductBE.SubSubCategoryName)
                        {
                            aViewDetails.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(objProductBE.CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.SubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductCode.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductName);
                        }
                        else
                        {
                            aViewDetails.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(objProductBE.CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.SubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.SubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductCode.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductName);
                        }
                    }
                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + objProductBE.DefaultImageName))
                        imgProduct.Src = strProductImagePath + objProductBE.DefaultImageName;
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";


                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}