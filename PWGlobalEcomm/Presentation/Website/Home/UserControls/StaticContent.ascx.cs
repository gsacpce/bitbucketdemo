﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;


namespace Presentation
{

    public partial class Home_UserControls_StaticContent : System.Web.UI.UserControl
    {
        protected global::System.Web.UI.WebControls.Literal ltrStaticContent;
        private string host = GlobalFunctions.GetVirtualPath();

        public HomePageManagementBE StaticContent { get; set; }

        public string Host
        {
            get
            {
                return host;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (StaticContent != null)
            {
                if (StaticContent.ContentType.ToLower().Equals("statictext"))
                {
                    StaticContent.Content = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(StaticContent.Content.Replace("x_","")), @"\s+", " ");
                    ltrStaticContent.Text = StaticContent.Content;
                }
                

                if(StaticContent.ContentType.ToLower().Equals("staticimage"))
                {
                    string imgHref = "" ;
                    

                    if ( StaticContent.URL.Trim().Length > 0 )
                    {
                        imgHref = StaticContent.URL ;
                        ltrStaticContent.Text = "<a href='"+ imgHref +"'><img src = '" + Host + "Images/StaticImage/" + StaticContent.HomePageStaticContentId + "" + StaticContent.ImageExtension + "' width='100%' ></a>";
                    }
                        
                    else
                        ltrStaticContent.Text = "<img src = '" + Host +"Images/StaticImage/"+ StaticContent.HomePageStaticContentId + "" + StaticContent.ImageExtension + "' width='100%' >";
                }

                if (StaticContent.ContentType.ToLower().Equals("video"))
                { 
                    string vdoUrl = "" ;
                    if (StaticContent.URL.Trim().Length > 0)
                    {
                        vdoUrl = StaticContent.URL;
                        string width = "", wunit = "", height = "" , hunit = "";
                        if (StaticContent.Width != 0 && StaticContent.WidthUnit != null)
                        {
                            width = StaticContent.Width.ToString();
                            wunit = StaticContent.WidthUnit;
                        }
                        if (StaticContent.Height != 0 && StaticContent.HeightUnit != null)
                        {
                            height = StaticContent.Height.ToString();
                            hunit = StaticContent.HeightUnit;
                        }
                        ltrStaticContent.Text = "<iframe src='" + vdoUrl + "?rel=0 'width='100%' height='" + height + " " + hunit + "' frameborder='0' allowfullscreen></iframe>";
                    }
                }

            }
        }
    }
}