﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using RandomStringGenerator;

namespace Presentation
{
    public partial class Home_UserControls_SlideShow : System.Web.UI.UserControl
    {
        protected global::System.Web.UI.WebControls.Repeater rptBootStrapImages;
        protected global::System.Web.UI.WebControls.Repeater rptOwlImages, rptOwlIndicators;
        protected global::System.Web.UI.WebControls.Repeater rptNerveImages;
        protected global::System.Web.UI.WebControls.Literal ltrlSliderScript;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl BOOTSTRAP_CAROUSEL;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl OWL_CAROUSEL;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl NERVESLIDER_CAROUSEL;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divImages, divOwlImages, divOverlayText;

        //public ImageSlideShowBE thisSlideShow = new ImageSlideShowBE();
        public ImageSlideShowBE thisSlideShow { get; set; }
        private string slideShowName = "";
        private byte bootStrapFirstImage = 1;
        private string host = GlobalFunctions.GetVirtualPath();
        RandomStringGenerator.RandomStringGenerator rndStrGen;
        public string Host
        {
            get
            {
                return host;
            }
        }
        /// <summary> 
        /// Author  : Sachin Chauhan 
        /// Date    : 18 08 2015
        /// Scope   : Default Constructor Required for Inheriting 
        /// </summary>
        public Home_UserControls_SlideShow()
        {
            rndStrGen = new RandomStringGenerator.RandomStringGenerator(true, true, true, false);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //List<object> lstAllImageSliders = ImageSlideShowBL.GetAllImageSliders(false);

            if (thisSlideShow != null)
            {
                /*
                Order images with displayorder set for them 
                */
                try
                {
                    /*Sachin Chauhan Start : 23-11-15*/
                    //List<ImageSlideShowBE.SlideShowImage> lstSlideShowImages = thisSlideShow.LstSlideShowImages.Cast<ImageSlideShowBE.SlideShowImage>().OrderBy(x => x.DisplayOrder).ToList<ImageSlideShowBE.SlideShowImage>();
                    List<ImageSlideShowBE.SlideShowImage> lstSlideShowImages = thisSlideShow.LstSlideShowImages.Cast<ImageSlideShowBE.SlideShowImage>().Where(x => x.IsActive.Equals(true)).OrderBy(x => x.DisplayOrder).ToList<ImageSlideShowBE.SlideShowImage>();
                    /*Sachin Chauhan End:*/

                    Repeater rptSliderImages = new Repeater();

                    HtmlGenericControl divBootstrap = this.FindControl("BOOTSTRAP_CAROUSEL") as HtmlGenericControl;
                    //HtmlGenericControl divOwl = this.FindControl("OWL_CAROUSEL") as HtmlGenericControl;
                    //HtmlGenericControl divOwl = this.FindControl("Owl") as HtmlGenericControl;
                    HtmlGenericControl divNerve = this.FindControl("NERVESLIDER_CAROUSEL") as HtmlGenericControl;

                    divBootstrap.Visible = false;
                    OWL_CAROUSEL.Visible = false;
                    divNerve.Visible = false;

                    switch (thisSlideShow.SlideShowMasterName.ToLower())
                    {
                        case "nerve":
                            {
                                divImages = this.FindControl("divNerveImages") as HtmlGenericControl;
                                divImages.ID = "divNerveImages_" + rndStrGen.Generate(5);
                                divNerve.Visible = true;
                                divImages.Visible = true;
                                divImages.Style.Add("height", Convert.ToString(thisSlideShow.Height + "px"));
                                rptNerveImages.DataSource = lstSlideShowImages;
                                rptNerveImages.DataBind();
                                //ltrlSliderScript.Text = @"<script> " +
                                //                       "$(document).ready(function() " +
                                //                        "{" +
                                //                        "$(\"#" + divImages.ID + "\").nerveSlider({ " +
                                //                        "slideTransitionSpeed:" + thisSlideShow.SlideEffectTime + "" +
                                //                        ",slideTransitionDelay:" + thisSlideShow.SlideDelayTime + "" +
                                //                        ", slideTransitionEasing:\"easeOutExpo\",showTimer:false,preDelay:0,sliderWidth:\"100%\",sliderHeight:\"" + thisSlideShow.Height + "px\",sliderResizable:true,sliderHeightAdaptable:true,sliderKeepAspectRatio:false});" +
                                //                        "}" +
                                //                        ");" +
                                //                        "</script>";

                                ltrlSliderScript.Text = @"<script> " +
                                                    "$(window).load(function() " +
                                                     "{" +
                                                     "$('.slider_inner').nerveSlider({ " +
                                                     "slideTransitionSpeed:" + thisSlideShow.SlideEffectTime + "" +
                                                     ",slideTransitionDelay:" + thisSlideShow.SlideDelayTime + "" +
                                                     ", slideTransitionEasing:\"easeOutExpo\",showTimer:false,preDelay:0,sliderWidth:\"100%\",sliderHeight:\"" + thisSlideShow.Height + "px\",sliderResizable:true,sliderHeightAdaptable:true,sliderKeepAspectRatio:false});" +
                                                     "}" +
                                                     ");" +
                                                     "</script>";

                                break;
                            }
                        case "owl carousel":
                            {
                                //divImages = this.FindControl("divOwlImages") as HtmlGenericControl ;
                                divOwlImages.Visible = true;
                                OWL_CAROUSEL.Visible = true;
                                //divOwl.ID = "divOwl_" + thisSlideShow.SlideShowId;
                                rptOwlImages.DataSource = lstSlideShowImages;
                                rptOwlImages.DataBind();


                                Random rnd = new Random();

                                divOwlImages.ID = "divImages_" + rndStrGen.Generate(5);
                                ltrlSliderScript.Text = @"<script> " +
                                                         "$(document).ready(function(){ " +
                                    //"   var owl = $(\"#divOwlImages\"); " +
                                                         "   var owl = $(\"#" + divOwlImages.ID + "\"); " +
                                                         "   owl.owlCarousel({ " +
                                                         "       autoPlay: " + thisSlideShow.SlideDelayTime + ", " +
                                                         "    paginationSpeed: " + thisSlideShow.SlideEffectTime + ", " +
                                                         "       navigation: true, " +
                                                         "       singleItem: true, " +
                                                         "       navigation: false, " +
                                                         "       responsiveRefreshRate: 30, " +
                                                         "       slideSpeed: 1000, " +
                                    /*"       transitionStyle: \"fade\" " +*/
                                                         "       }); " +
                                                         "   }); " +
                                                         "</script> ";
                                break;
                            }
                        case "bootstrap":
                            {
                                divImages = this.FindControl("divBootstrapImages") as HtmlGenericControl;
                                divImages.Visible = true;
                                divBootstrap.Visible = true;
                                rptBootStrapImages.DataSource = lstSlideShowImages;
                                rptBootStrapImages.DataBind();

                                rptOwlIndicators.DataSource = lstSlideShowImages;
                                rptOwlIndicators.DataBind();

                                Double SlideEffectTime = thisSlideShow.SlideEffectTime / 1000;
                                ltrlSliderScript.Text = @"<script> " +
                                                        "$('.carousel').carousel({" +
                                                        "   interval:" + thisSlideShow.SlideDelayTime + "" +
                                                        "  });" +
                                                        "</script>" +
                                                        "<!-- FOR SLIDE SPEED -->" +
                                                        "<style>" +
                                                        "   .carousel-inner > .item {    " +
                                                        "    -webkit-transition: " + SlideEffectTime + "s ease-in-out left;" +
                                                        "    -moz-transition: " + SlideEffectTime + "s ease-in-out left;" +
                                                        "    -o-transition: " + SlideEffectTime + "s ease-in-out left;" +
                                                        "    transition: " + SlideEffectTime + "s ease-in-out left; " +
                                                        "} " +
                                                        "</style> ";

                                break;
                            }
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                    //throw;
                }

            }

        }

        protected void rptBootStrapImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ImageSlideShowBE.SlideShowImage currImage = e.Item.DataItem as ImageSlideShowBE.SlideShowImage;

            HtmlGenericControl divBootImg = e.Item.FindControl("divBootImg") as HtmlGenericControl;
            HyperLink hyplImageNavigateUrl = e.Item.FindControl("hyplImageNavigateUrl") as HyperLink;

            Image imgSlider = e.Item.FindControl("imgSlider") as Image;

            if (bootStrapFirstImage == 1)
            {
                divBootImg.Attributes.Clear();
                divBootImg.Attributes.Add("class", "item active imageanchor");
                bootStrapFirstImage += 1;
            }
            Label txtOverlay = e.Item.FindControl("txtOverlay") as Label;

            imgSlider.ImageUrl = Host + "Images/SlideShow/" + currImage.SlideShowImageId.ToString() + currImage.ImageExtension;

            if (currImage.OverLayText != null && currImage.OverLayText.Trim() != String.Empty)
                txtOverlay.Text = currImage.OverLayText;

            if (currImage.NavigateUrl.StartsWith("http"))
                hyplImageNavigateUrl.NavigateUrl = currImage.NavigateUrl;

            if (currImage.IsNewWindow.Equals(true))
                hyplImageNavigateUrl.Attributes.Add("target", "_blank");
        }

        protected void rptOwlImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ImageSlideShowBE.SlideShowImage currImage = e.Item.DataItem as ImageSlideShowBE.SlideShowImage;
            HyperLink hyplImageNavigateUrl = e.Item.FindControl("hyplImageNavigateUrl") as HyperLink;
            HtmlGenericControl divOverlayText = e.Item.FindControl("divOverlayText") as HtmlGenericControl; ;

            Image imgSlider = e.Item.FindControl("imgSlider") as Image;

            imgSlider.ImageUrl = Host + "Images/SlideShow/" + currImage.SlideShowImageId.ToString() + currImage.ImageExtension;

            Label txtOverlay = e.Item.FindControl("txtOverlay") as Label;

            #region MyRegion CODE ADDED BY SHRIGANESH 29 JAN 2016 TO DISPLAY OVERLAY AREA
            if (currImage.OverLayText != null && currImage.OverLayText.Trim() != String.Empty)
            {
                txtOverlay.Text = currImage.OverLayText;
                txtOverlay.Font.Size = currImage.OverlayFontSize;
                txtOverlay.ForeColor = System.Drawing.ColorTranslator.FromHtml(currImage.OverlayForeColor);
            }

            if (currImage.OverlayPosition.ToLower() == "top")
            {
                divOverlayText.Style["position"] = "absolute";
                divOverlayText.Style["top"] = "0px";
                divOverlayText.Style["left"] = "0px";
                divOverlayText.Style["width"] = "100%";
                divOverlayText.Style["background-color"] = currImage.OverlayBGColor;
                divOverlayText.Style["padding"] = currImage.OverlayHeight;
                divOverlayText.Style["opacity"] = "0.3";
            }
            else if (currImage.OverlayPosition.ToLower() == "bottom")
            {
                divOverlayText.Style["position"] = "absolute";
                divOverlayText.Style["bottom"] = "0px";
                divOverlayText.Style["left"] = "0px";
                divOverlayText.Style["width"] = "100%";
                divOverlayText.Style["background-color"] = currImage.OverlayBGColor;
                divOverlayText.Style["padding"] = currImage.OverlayHeight;
                divOverlayText.Style["opacity"] = "0.3";
            }
            #endregion

            if (currImage.NavigateUrl.StartsWith("http"))
                hyplImageNavigateUrl.NavigateUrl = currImage.NavigateUrl;

            if (currImage.IsNewWindow.Equals(true))
                hyplImageNavigateUrl.Attributes.Add("target", "_blank");
        }

        protected void rptOwlIndicators_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HtmlGenericControl liIndicator = e.Item.FindControl("liIndicator") as HtmlGenericControl;
            liIndicator.Attributes.Add("data-slide-to", e.Item.ItemIndex.ToString());

            if (e.Item.ItemIndex == 0)
                liIndicator.Attributes.Add("class", "active");
        }

        protected void rptNerveImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ImageSlideShowBE.SlideShowImage currImage = e.Item.DataItem as ImageSlideShowBE.SlideShowImage;
            HyperLink hyplImageNavigateUrl = e.Item.FindControl("hyplImageNavigateUrl") as HyperLink;

            Image imgSlider = e.Item.FindControl("imgSlider") as Image;

            imgSlider.ImageUrl = Host + "Images/SlideShow/" + currImage.SlideShowImageId.ToString() + currImage.ImageExtension;

            if (currImage.NavigateUrl.StartsWith("http"))
                hyplImageNavigateUrl.NavigateUrl = currImage.NavigateUrl;

            if (currImage.IsNewWindow.Equals(true))
                hyplImageNavigateUrl.Attributes.Add("target", "_blank");

        }

    }

}
