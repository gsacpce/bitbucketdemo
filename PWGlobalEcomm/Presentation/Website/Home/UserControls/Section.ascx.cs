﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Home_UserControls_Section : System.Web.UI.UserControl
{
    #region UI Controls
    public global::System.Web.UI.WebControls.Repeater rptProducts;
    public global::System.Web.UI.WebControls.Label lblSectionName;
    #endregion

    public static string host = GlobalFunctions.GetVirtualPath();
    string strProductImagePath = GlobalFunctions.GetMediumProductImagePath();
    StoreBE objStoreBE;
    Int16 intLanguageId = 0;
    string strCategoryName = string.Empty;
    string strSubCategoryName = string.Empty;
    string strSubSubCategoryName = string.Empty;
    string strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
    string stror;

    protected void Page_Load(object sender, EventArgs e)
    {
      
       
    }

    /// <summary>
    /// Author  : Vikram Singh
    /// Date    : 27-10-15
    /// Scope   : rptProducts_ItemDataBound of the repeater
    /// </summary>
    /// <param name="sender"></param>        
    /// <param name="e"></param>        
    /// <returns></returns>
    protected void rptProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Int32 ProductId = 0;
                string ProductName = string.Empty;

                HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
                //HtmlAnchor aProductDetail = (HtmlAnchor)e.Item.FindControl("aProductDetail");
                HtmlAnchor aProductImage = (HtmlAnchor)e.Item.FindControl("aProductImage");
                Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
                HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
                HtmlGenericControl spnstrike = (HtmlGenericControl)e.Item.FindControl("spnstrike");
                // HtmlGenericControl dvDescription = (HtmlGenericControl)e.Item.FindControl("dvDescription");
                HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
                HtmlGenericControl liProducts = (HtmlGenericControl)e.Item.FindControl("liProducts");
                HtmlGenericControl spnPrice1 = (HtmlGenericControl)e.Item.FindControl("spnPrice1");
                HtmlGenericControl spnor = (HtmlGenericControl)e.Item.FindControl("spnor");

                ProductId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "productId"));
                ProductName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductName"));
                intLanguageId = GlobalFunctions.GetLanguageId();
                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                    imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                else
                    imgProduct.Src = host + "Images/Products/default.jpg";

                if (ProductName.Length > 60)
                    aProductName.InnerHtml = ProductName.Substring(0, 60);
                else
                    aProductName.InnerHtml = ProductName;

                ltrProductCode.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCode"));


                string AsLowAsPrice = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice"));
                string AsLowAsstrikePrice = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsstrikePrice"));
                //if (!string.IsNullOrEmpty(AsLowAsPrice) && AsLowAsPrice != "0")
                //{
                //    //spnPrice.InnerHtml = strAsLowAs + " : <strong>" + strCurrencySymbol + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")) + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DiscountCode")) + "</strong>";
                //    //spnPrice.InnerHtml = strAsLowAs + " : <strong>" + strCurrencySymbol + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")) + "</strong>";
                //    spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")), strCurrencySymbol, GlobalFunctions.GetLanguageId());
                //}
                //else
                //{
                //    spnPrice.Visible = false;
                //}
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {

                        stror = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "RecieveCommunication_or_Text").ResourceValue;
                    }
                }
                #region code added to display strike price
                
                
                if (!string.IsNullOrEmpty(AsLowAsstrikePrice) && AsLowAsstrikePrice != "0")
                {
                    spnstrike.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsstrikePrice")), strCurrencySymbol, GlobalFunctions.GetLanguageId()); 
                    if (!string.IsNullOrEmpty(AsLowAsPrice) && AsLowAsPrice != "0")
                    {
                        spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")), strCurrencySymbol, GlobalFunctions.GetLanguageId());
                    }
                }
                else if (!string.IsNullOrEmpty(AsLowAsPrice) && AsLowAsPrice != "0")
                {
                    //spnPrice.InnerHtml = strAsLowAs + " : <strong>" + strCurrencySymbol + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")) + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DiscountCode")) + "</strong>";
                    //spnPrice.InnerHtml = strAsLowAs + " : <strong>" + strCurrencySymbol + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")) + "</strong>";
                    //spnPrice.Attributes.Remove("class", "col-sm-6");
                    //if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled)
                    if (GlobalFunctions.IsPointsEnbled())
                    {

                        spnPrice.Attributes.Remove("class");
                        spnPrice.Attributes.Add("class", "price col-sm-12");
                        spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")), strCurrencySymbol, GlobalFunctions.GetLanguageId());
                        spnor.Attributes.Add("class", "price col-sm-12");
                        spnor.InnerHtml = stror;
                        spnPrice1.Attributes.Add("class", "price col-sm-12");
                        spnPrice1.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")), strCurrencySymbol, GlobalFunctions.GetLanguageId());
                        //spnPrice.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")), strCurrencySymbol, GlobalFunctions.GetLanguageId()); 		


                        spnstrike.Visible = false;
                    }
                    else 
                    {
                        spnPrice.Attributes.Remove("class");
                        spnPrice.Attributes.Add("class", "price col-sm-12");
                        spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")), strCurrencySymbol, GlobalFunctions.GetLanguageId()); ;
                    
                    }
                }
                else { spnstrike.Visible = false; }
                #endregion
                string strDescription = GlobalFunctions.RemoveHtmlTags(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductDescription")));
                //if (strDescription.Length > 300)
                //{
                //    dvDescription.InnerHtml = strDescription.Substring(0, 300) + "...";
                //}
                //else
                //{
                //    dvDescription.InnerHtml = GlobalFunctions.RemoveHtmlTags(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductDescription")));
                //}
                strCategoryName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                strSubCategoryName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SubCategoryName"));
                strSubSubCategoryName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SubSubCategoryName"));



                if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
                {
                    aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(ProductName);
                }
                else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
                {
                    aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(ProductName);
                }
                else
                {
                    aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(ProductName);
                }
                //aProductDetail.InnerHtml = strViewDetails;

                //aProductDetail.Attributes.Add("class", "productdetail" + ProductId);
                if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
                {
                    string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
                    string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    string strSectionImageData = string.Empty;
                    string strSectionNames = string.Empty;
                    for (int i = 0; i < strSections.Length; i++)
                    {
                        string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                        {
                            strSectionImageData += "<img title='" + strSectionIcon[1].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_list_icon'>";
                        }
                        if (strSectionIcon.Length > 1)
                            strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                    }
                    dvSectionIcons.InnerHtml = strSectionImageData;

                }


            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

   
}