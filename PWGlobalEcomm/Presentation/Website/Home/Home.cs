﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PWGlobalEcomm.BusinessLogic;
using HtmlAgilityPack;
using System.Web.UI;
using System.Web.Services;
using System.Web;
using PWGlobalEcomm.DataAccess;
using System.Threading;
using Microsoft.Security.Application;

namespace Presentation
{
    public class Home : BasePage
    {
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divHomeContainer;
        protected global::System.Web.UI.WebControls.Literal litSliderCSS;
        protected global::System.Web.UI.WebControls.Literal litSliderJS;

        private string host = GlobalFunctions.GetVirtualPath();

        public string Host
        {
            get
            {
                return host;
            }
        }

        StoreBE lstStoreDetail;
        Int16 iUserTypeID = 1;

        string MaerskMGISSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_SSOLogs";
        string MaerskOILSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_OIL_SSOLogs";
        string MaerskDRILLSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\MAERSK_DRILL_SSOLogs";
        string INDEEDSSOLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\IndeedBrandStore_SSOLogs";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UserBE lstUser = new UserBE();
                lstUser = Session["User"] as UserBE;
                if (Session["User"] != null)
                {
                    Exceptions.WriteSSOLog("User Session NOT NULL " + lstUser.EmailId, INDEEDSSOLogFolderPath);
                }
                if (Session["IndeedSSO_User"] != null)
                {
                    Exceptions.WriteSSOLog("IndeedSSO_User NOT NULL " + lstUser.EmailId, INDEEDSSOLogFolderPath);
                    Exceptions.WriteSSOLog("User Email in Index page load = " + lstUser.EmailId, INDEEDSSOLogFolderPath);
                }

                // Added By Snehal 28 09 2016
                if (Session["AccessToken"] != null)
                {
                    try
                    {   
                        int IsExist;
                        UserBE objUser = new UserBE();
                        UserBE objBE = new UserBE();
                        objBE.EmailId = Sanitizer.GetSafeHtmlFragment(Session["APIEmailId"].ToString());
                        IsExist = UserBL.IsExistUser(Constants.USP_IsExistUserAPI, true, objBE);

                        if(IsExist == 1)
                        { 
                            UserBE objBEs = new UserBE();
                            objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(Session["APIEmailId"].ToString());
                            objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                            objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                            objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                            if (objUser != null || objUser.EmailId != null)
                            {
                                Login_UserLogin.UpdateBasketSessionProducts(objUser, 'Y');
                                HttpContext.Current.Session["User"] = objUser;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                }               
                
                if (Session["User"] != null)
                {                                   
                    UserBE objUserBE = new UserBE();
                    objUserBE = Session["User"] as UserBE;
                    iUserTypeID = objUserBE.UserTypeID;
                }
                if (Session["PrevLanguageId"] == null)
                {
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                }
               
                if (Session["GuestUser"] == null)
                {                                      
                }                
                ReadMetaTagsData();
                LoadHomePageContents();          
            }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }  
 


        private void LoadHomePageContents()
        {
            try
            {
                List<HomePageManagementBE> lstHomePageConfigurations = HomePageManagementBL.GetAllHomePageElements(false).Cast<HomePageManagementBE>().ToList();

                if (lstHomePageConfigurations != null && lstHomePageConfigurations.Count > 0)
                {
                    List<HomePageManagementBE> homePageConfigurations = lstHomePageConfigurations.Cast<HomePageManagementBE>().ToList().OrderBy(x => x.ConfigSectionName).ToList<HomePageManagementBE>();

                    foreach (HomePageManagementBE section in homePageConfigurations)
                    {
                        switch (section.ConfigElementName.ToLower())
                        {
                            case "slideshow":
                                {
                                    LoadHomePageSlider((Int16)section.ConfigElementID, section.ConfigSectionName);
                                    break;
                                }
                            case "staticimage":
                            case "statictext":
                            case "video":
                                {
                                    LoadHomePageStaticElements((Int16)section.ConfigElementID, section.ConfigSectionName, section.ConfigElementName);
                                    break;
                                }
                            case "sections":
                                {
                                    LoadHomePageSection((Int16)section.ConfigElementID, section.ConfigSectionName, section.ConfigElementValue);
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    Response.Write("Home page configuration details are missing.");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void LoadHomePageStaticElements(short elementId, string sectionName, string elementName)
        {
            try
            {
                List<HomePageManagementBE> homePageContents = HomePageManagementBL.GetAllHomePageContentDetails();
                Int16 currLanguageId = GlobalFunctions.GetLanguageId();
                homePageContents = homePageContents.Where(x => x.LanguageId.Equals(currLanguageId)).ToList<HomePageManagementBE>();
                HtmlGenericControl gmContent = divHomeContainer.FindControl(sectionName) as HtmlGenericControl;
                if ( gmContent != null)
                {
                    gmContent.InnerHtml = "";
                    gmContent.InnerText = "";
                    Home_UserControls_StaticContent ctrl = (Home_UserControls_StaticContent)Page.LoadControl("UserControls/StaticContent.ascx");
                    ctrl.StaticContent = homePageContents.Cast<HomePageManagementBE>().ToList<HomePageManagementBE>().SingleOrDefault(x => x.HomePageStaticContentId.Equals(elementId));
                    gmContent.Controls.Add(ctrl);
                }
                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void LoadHomePageSlider(Int16 sliderId, string sectionName)
        {
            List<ImageSlideShowBE> lstSlideShows = ImageSlideShowBL.GetAllImageSliders();

            try
            {
                HtmlGenericControl gmContent = divHomeContainer.FindControl(sectionName) as HtmlGenericControl;
                gmContent.InnerHtml = "";
                gmContent.InnerText = "";
                Home_UserControls_SlideShow ctrl = (Home_UserControls_SlideShow)Page.LoadControl("UserControls/SlideShow.ascx");


                ctrl.thisSlideShow = lstSlideShows.Cast<ImageSlideShowBE>().ToList<ImageSlideShowBE>().SingleOrDefault(x => x.SlideShowId.Equals(sliderId));

                /*if (ctrl.thisSlideShow.Width != null || ctrl.thisSlideShow.Width > 0)
                {
                    gmContent.Attributes.Add("width", ctrl.thisSlideShow.Width.ToString() + " " + ctrl.thisSlideShow.WidthUnit);
                }*/
                litSliderCSS.Text = "<link href='" + host + "css/nerveslider.css' rel='stylesheet'> ";
                litSliderJS.Text = "<script src='" + host + "js/jquery-ui-1.10.2.min.js'></script>" +
                                   "<script type='text/javascript' src='" + host + "js/jquery.nerveSlider.min.js'></script> ";

                litSliderCSS.Text += "<link href='" + host + "css/owl.carousel.css' rel='stylesheet'> " +
                                               "<link href='" + host + "css/owl.theme.css' rel='stylesheet'> " +
                                               "<link href='" + host + "css/owl.transitions.css' rel='stylesheet'> ";
                litSliderJS.Text += "<script type='text/javascript' src='" + host + "js/owl.carousel.js'></script> ";

                if (!ctrl.thisSlideShow.ShowPagination)
                {
                    switch (ctrl.thisSlideShow.SlideShowMasterName.ToLower())
                    {
                        case "owl carousel":
                            {
                                litSliderCSS.Text += "<style>.owl-controls{ display:none !important;} </style>";
                                break;
                            }
                        case "nerve":
                            {
                                litSliderCSS.Text += "<style>.ns_dots{ display:none !important;} </style>";
                                break;
                            }
                        case "bootstrap":
                            {
                                litSliderCSS.Text += "<style>.carousel-indicators{ display:none !important;} </style>";
                                break;
                            }
                        default:
                            break;
                    }
                }
                gmContent.Controls.Add(ctrl);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void LoadHomePageSection(Int16 sectionId, string sectionName, string elementName)
        {
            try
            {
                SectionBE.CustomSection CurrHomePageSection = new SectionBE.CustomSection();
                CurrHomePageSection.SectionIds = sectionId.ToString();

                CurrHomePageSection.LanguageId = GlobalFunctions.GetLanguageId();
                CurrHomePageSection.CurrencyId = GlobalFunctions.GetCurrencyId();
                CurrHomePageSection.toprow = 4;


                List<ProductBE> lstSectionProducts = SectionBL.GetHomePageProductSectionData(CurrHomePageSection, iUserTypeID);/*User Type*/
                Home_UserControls_Section CtrlCurrSection = (Home_UserControls_Section)Page.LoadControl("UserControls/Section.ascx");
                CtrlCurrSection.lblSectionName.Text = elementName;
                CtrlCurrSection.rptProducts.DataSource = lstSectionProducts;
                CtrlCurrSection.rptProducts.DataBind();

                HtmlGenericControl gmContent = divHomeContainer.FindControl(sectionName) as HtmlGenericControl;
                gmContent.InnerHtml = "";
                gmContent.InnerText = "";

                gmContent.Controls.Add(CtrlCurrSection);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "Home");

                    Page.Title = lstMetaTags[0].MetaContentTitle;
                    Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                    Page.MetaDescription = lstMetaTags[0].MetaDescription;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        [WebMethod]
        public static void ChangeDropdownCurrency(string CId, string CSymb, string LId)
        {
            try
            {
                #region
                string strUserSessionId = HttpContext.Current.Session.SessionID;
                Boolean IsAnonymousBasketEmpty = true;

                StoreBE getStores = (StoreBE)HttpRuntime.Cache["StoreDetails"];
                
                
                int intUserId = 0;
                if (HttpContext.Current.Session["User"] != null)
                    intUserId = ((UserBE)HttpContext.Current.Session["User"]).UserId;

                List<object> lstShoppingBE = new List<object>();
                //Int16 currencyId = Session["PrevCurrencyId"].To_Int16() != 0 ? Session["PrevCurrencyId"].To_Int16() : GlobalFunctions.GetCurrencyId();
                lstShoppingBE = ShoppingCartBL.GetBasketProductsData(LId.To_Int16(),
                                                                     CId.To_Int16(),
                                                                     intUserId, strUserSessionId);
                if (lstShoppingBE != null && lstShoppingBE.Count > 0)
                {
                    IsAnonymousBasketEmpty = false;
                }


               
                if (HttpContext.Current.Session["User"] == null && !HttpContext.Current.Session["PrevCurrencyId"].To_Int16().Equals(GlobalFunctions.GetCurrencyId()))
                {
                    ShoppingCartBL.RemoveAllBasketProducts(strUserSessionId);
                    IsAnonymousBasketEmpty = true;
                    //HttpContext.Current.Response.Redirect("~/Home.aspx");
                }
               

                /*Sachin Chauhan : 18 09 2015 : Adding current languge id to one more session variable */
                //if (HttpContext.Current.Session["PrevLanguageId"] == null || HttpContext.Current.Session["PrevLanguageId"].To_Int16() == 0)
                //HttpContext.Current.Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();

                //if (HttpContext.Current.Session["PrevCurrencyId"] == null || HttpContext.Current.Session["PrevCurrencyId"].To_Int16() == 0)
                //
                /*Sachin Chauhan : 18 09 2015*/

                //string strCurrencySymbol = getStores.StoreCurrencies.FirstOrDefault(x => x.CurrencyCode.Equals(CCode)).CurrencySymbol;
                //Int16 currencyId = getStores.StoreCurrencies.FirstOrDefault(x => x.CurrencyCode.Equals(CId)).CurrencyId;

                HttpContext.Current.Session["PrevCurrencyId"] = GlobalFunctions.GetCurrencyId();
                GlobalFunctions.SetCurrencyId(CId.To_Int16());
                GlobalFunctions.SetCurrencySymbol(CSymb);

               

            
                if (HttpContext.Current.Session["User"] != null && !HttpContext.Current.Session["PrevCurrencyId"].To_Int16().Equals(GlobalFunctions.GetCurrencyId()))
                {
                    /*Sachin Chauhan Start : 10 02 2016 : Below code ensures that on changing currency 
                     *                                    the user's basysid should also get updated.
                     *                                    When placing an Order correct basys id of user should pass to order xml
                    */
                    UserBE objUserBE = HttpContext.Current.Session["User"] as UserBE;

                    /*If User is GUEST user empty his basket*/
                    if (objUserBE.IsGuestUser)
                    {
                        ShoppingCartBL.RemoveAllBasketProducts(strUserSessionId);
                        objUserBE.EmailId = objUserBE.EmailId;
                        objUserBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objUserBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                    }
                    else if (objUserBE.IsGuestUser.Equals(false))
                    {
                        objUserBE.EmailId = objUserBE.EmailId;
                        objUserBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                        objUserBE.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
                        HttpContext.Current.Session["User"] = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objUserBE);

                    }
                 
                    /*Sachin Chauhan End : 10 02 2016*/
                    //HttpContext.Current.Response.Redirect("~/Home.aspx");
                }

                HttpContext.Current.Session["IsAnonymous"] = 0;

                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

       [WebMethod]
       public static void LoginLinkClicked()
       {
           HttpContext.Current.Session["IsAnonymous"] = 0;
       }

    }
}
