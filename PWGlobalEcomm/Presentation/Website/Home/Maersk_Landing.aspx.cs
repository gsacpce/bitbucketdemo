﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Maersk_Landing : System.Web.UI.Page
{
    #region Controls
    protected global::System.Web.UI.WebControls.DropDownList ddlBusinessUnit;
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strVal_1 = GlobalFunctions.GetSetting("LINKGROUP");
        string strVal_2 = "https://cam.maerskoil.com/CloudAccessManager/RPSTS/Saml2/Default.aspx?relayState=undefined&ssoSiteId=16";
        string strVal_3 = GlobalFunctions.GetSetting("LINKDRILL");
        string strVal_4 = GlobalFunctions.GetSetting("LINKTRAINING");


        if (ddlBusinessUnit.SelectedValue == "1" || ddlBusinessUnit.SelectedValue == "6" || ddlBusinessUnit.SelectedValue == "10" || ddlBusinessUnit.SelectedValue == "12"|| ddlBusinessUnit.SelectedValue == "13"|| ddlBusinessUnit.SelectedValue == "14"|| ddlBusinessUnit.SelectedValue == "15"|| ddlBusinessUnit.SelectedValue == "16"|| ddlBusinessUnit.SelectedValue == "17"|| ddlBusinessUnit.SelectedValue == "18"|| ddlBusinessUnit.SelectedValue == "19")
        {
            Response.Redirect(strVal_1);
        }
        else if (ddlBusinessUnit.SelectedValue == "4"|| ddlBusinessUnit.SelectedValue == "8")
        {
            Response.Redirect(strVal_2);
        }
        else if (ddlBusinessUnit.SelectedValue == "3"|| ddlBusinessUnit.SelectedValue == "9")
        {
            Response.Redirect(strVal_3);
        }
        else if(ddlBusinessUnit.SelectedValue == "2" || ddlBusinessUnit.SelectedValue == "5" || ddlBusinessUnit.SelectedValue == "7" || ddlBusinessUnit.SelectedValue == "11" || ddlBusinessUnit.SelectedValue == "20")
        {
            Response.Redirect(strVal_4);
        }      
    }
}