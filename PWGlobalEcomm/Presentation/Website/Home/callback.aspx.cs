﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Web.Configuration;
using System.Data;
using System.Text;
using System.Configuration;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.BusinessEntity;
using System.Net.Mail;
using Microsoft.Security.Application;
using PWGlobalEcomm.GlobalUtilities;

namespace Presentation
{
    public partial class Home_callback : BasePage
    {
        private string accessToken;
        private string CASE;
        public string host = GlobalFunctions.GetVirtualPath();
        public static bool SessionAccessToken = false;
        Int16 iUserTypeID = 1;
        GoogleEmailBE oUser = new GoogleEmailBE();
        bool validation = true;
        string message = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                //GetQueryString();

                //if (string.IsNullOrEmpty(Request.QueryString["access_token"])) return;
                UserTypeMappingBE objUserTypeMappingBE = new UserTypeMappingBE();
                BindResourceData();
                StoreBE objStoreBE = StoreBL.GetStoreDetails();

                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_LoginPanel").FeatureValues[0].IsEnabled || Session["ReferrelURL"] != null)
                {

                    try
                    {

                        oUser.email = Request["email"].ToString();
                        oUser.given_name = Request["firstname"].ToString();
                        oUser.family_name = Request["lastname"].ToString();
                        oUser.name = Request["name"].ToString();
                        Session["APIEmailId"] = oUser.email;
                        Session["FirstName"] = oUser.given_name;
                        Session["LastName"] = oUser.family_name;
                        Session["FullName"] = oUser.name;
                        CASE = Request["Case"].ToString();
                        Session["API"] = true;
                        Session["AccessToken"] = true;


                    }
                    catch (Exception ex) { }



                    if (oUser.email != null)
                    {

                        if (Session["ReferrelURL"] != null)
                        {

                            MailAddress address = new MailAddress(oUser.email.Trim());
                            host = address.Host;
                            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                            DictionaryInstance.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                            objUserTypeMappingBE = UserTypesBL.getCollectionItemUserTypeMappingbyCId(Constants.USP_UserTypeMappingDetailsByCID, DictionaryInstance, true);
                            UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                            if (objUserTypeMasterOptions != null)
                            {
                                if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                                {
                                    #region "Email"
                                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                                    {

                                        UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidation = objUserTypeMappingBE.lstUserTypeEmailValidation.FirstOrDefault(x => x.EmailId.ToLower() == oUser.email.Trim() && x.UserTypeId == Convert.ToInt16(Session["referralUsertype"].ToString()));
                                        UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidationDomain = objUserTypeMappingBE.lstUserTypeEmailValidation.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.Domain.ToLower() == host.ToLower() && x.UserTypeId == Convert.ToInt16(Session["referralUsertype"].ToString()));

                                        if (objRegUserTypeEmailValidation != null)
                                        {
                                            iUserTypeID = objRegUserTypeEmailValidation.UserTypeId;
                                        }
                                        else if (objRegUserTypeEmailValidationDomain != null)
                                        {
                                            iUserTypeID = objRegUserTypeEmailValidationDomain.UserTypeId;
                                        }
                                        else
                                        {
                                            UserTypeMappingBE.UserTypeDetails objRegUserTypeDetails = objUserTypeMappingBE.lstUserTypeDetails.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.IsWhiteList == false);
                                            if (objRegUserTypeDetails != null)
                                            {
                                                iUserTypeID = objRegUserTypeDetails.UserTypeID;
                                            }

                                        }
                                        if (objRegUserTypeEmailValidation != null && objRegUserTypeEmailValidationDomain != null)
                                        {
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Invalid Domain/Email ID ", AlertType.Warning);
                                            return;
                                        }
                                    }
                                    #endregion
                                }
                            }

                        }
                        else
                        {
                            #region ADDED BY SHRIGANESH FOR USER TYPE ID MAPPING


                            MailAddress address = new MailAddress(oUser.email.Trim());
                            host = address.Host;
                            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                            DictionaryInstance.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                            objUserTypeMappingBE = UserTypesBL.getCollectionItemUserTypeMappingbyCId(Constants.USP_UserTypeMappingDetailsByCID, DictionaryInstance, true);
                            UserTypeMappingBE.UserTypeMasterOptions objUserTypeMasterOptions = objUserTypeMappingBE.lstUserTypeMasterOptions.FirstOrDefault(x => x.IsActive == true);
                            if (objUserTypeMasterOptions != null)
                            {
                                if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                                {
                                    #region "Email"
                                    if (objUserTypeMasterOptions.UserTypeMappingTypeID == 2)
                                    {

                                        UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidation = objUserTypeMappingBE.lstUserTypeEmailValidation.FirstOrDefault(x => x.EmailId.ToLower() == oUser.email.Trim());
                                        UserTypeMappingBE.UserTypeEmailValidation objRegUserTypeEmailValidationDomain = objUserTypeMappingBE.lstUserTypeEmailValidation.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.Domain.ToLower() == host.ToLower());
                                        UserTypeMappingBE.UserTypeDetails objRegUserTypeDetails = objUserTypeMappingBE.lstUserTypeDetails.OrderBy(x => x.UserTypeSequence).FirstOrDefault(x => x.IsWhiteList == false);
                                        if (objRegUserTypeEmailValidation != null)
                                        {
                                            validation = false;
                                            if (objRegUserTypeDetails != null)
                                            {
                                                iUserTypeID = objRegUserTypeDetails.UserTypeID;
                                            }

                                        }
                                        else if (objRegUserTypeEmailValidationDomain != null)
                                        {
                                            validation = false;
                                            if (objRegUserTypeDetails != null)
                                            {
                                                iUserTypeID = objRegUserTypeDetails.UserTypeID;
                                            }

                                        }

                                        if (objRegUserTypeDetails != null)
                                        {
                                            iUserTypeID = objRegUserTypeDetails.UserTypeID;
                                        }

                                        if (objRegUserTypeEmailValidation != null && objRegUserTypeEmailValidationDomain != null)
                                        {
                                            GlobalFunctions.ShowModalAlertMessages(this.Page, "Invalid Domain/Email ID ", AlertType.Warning);
                                            return;
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }


                        #region Insert Google SignIn User Into DataBase

                        #region
                        int i = 0;
                        int IsExist;
                        try
                        {
                            UserBE objBE = new UserBE();
                            UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                            objBE.EmailId = oUser.email;
                            IsExist = UserBL.IsExistUser(Constants.USP_IsExistUserAPI, true, objBE);
                            if (IsExist == 0)
                            {
                                objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);
                                objBE.FirstName = oUser.given_name;
                                objBE.LastName = oUser.family_name;
                                objBE.PredefinedColumn1 = oUser.name;
                                objBE.PredefinedColumn2 = "";
                                objBE.PredefinedColumn3 = "";
                                objBE.PredefinedColumn4 = "";
                                objBE.PredefinedColumn5 = "";
                                objBE.PredefinedColumn6 = "";
                                objBE.PredefinedColumn7 = "";
                                objBE.PredefinedColumn8 = "12";
                                objBE.PredefinedColumn9 = "";
                                objBE.UserDeliveryAddress.Add(obj);
                                objBE.IPAddress = GlobalFunctions.GetIpAddress();
                                objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                                objBE.IsGuestUser = true;
                                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                                objBE.IsMarketing = false;
                                objBE.UserTypeID = iUserTypeID;// Added by SHRIGANESH 22 Sept 2016      
                                objBE.Points = -1; // Added by ShriGanesh 26 April 2017

                                Exceptions.WriteInfoLog("Before calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                                i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                                Exceptions.WriteInfoLog("After calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                                objBE.UserId = Convert.ToInt16(i);

                                UserBE objBEs = new UserBE();
                                UserBE objUser = new UserBE();
                                objBEs.EmailId = oUser.email.Trim();
                                objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                                objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                                objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                                //Below if condition is added according to harley guestcheckout tax calculation July 28,2016
                                if (objUser != null || objUser.EmailId != null)
                                {
                                    HttpContext.Current.Session["User"] = objUser;
                                    GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                                    GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                                    Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                    Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                    Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                    Session["GuestUser"] = null;
                                }
                            }
                            else
                            {
                                if (Session["ReferrelURL"] != null)
                                {
                                    //iUserTypeID = Convert.ToInt16(GlobalFunctions.GetSetting("GoogleUserType"));
                                    objBE.UserTypeID = iUserTypeID;

                                    i = UserBL.UpdateReferrelUserTypeId(Constants.USP_UpdateUserTypeForReferrelURL, true, objBE);

                                    objBE.UserId = Convert.ToInt16(i);

                                    UserBE objBEs = new UserBE();
                                    UserBE objUser = new UserBE();
                                    objBEs.EmailId = oUser.email.Trim();
                                    objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                                    objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                                    if (objUser != null || objUser.EmailId != null)
                                    {
                                        HttpContext.Current.Session["User"] = objUser;
                                        GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                                        GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                                        Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                        Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                        Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                        Session["GuestUser"] = null;
                                    }
                                }
                                else
                                {

                                    objBE.UserTypeID = iUserTypeID;
                                    i = UserBL.UpdateReferrelUserTypeId(Constants.USP_UpdateUserTypeForReferrelURL, true, objBE);

                                    objBE.UserId = Convert.ToInt16(i);

                                    UserBE objBEs = new UserBE();
                                    UserBE objUser = new UserBE();
                                    objBEs.EmailId = oUser.email.Trim();
                                    objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                                    objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                                    objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                                    if (objUser != null || objUser.EmailId != null)
                                    {
                                        HttpContext.Current.Session["User"] = objUser;
                                        GlobalFunctions.SetCurrencyId(objUser.CurrencyId);
                                        GlobalFunctions.SetCurrencySymbol(Convert.ToString(objUser.CurrencySymbol));
                                        Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                        Login_UserLogin.UpdateBasketSessionProducts(objUser, 'N');
                                        Exceptions.WriteInfoLog("Before calling Login_UserLogin.UpdateBasketSessionProducts() in SaveAddressDetails()");
                                        Session["GuestUser"] = null;
                                    }
                                }


                            }

                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                        }
                        #endregion


                        #endregion



                        //BasePage.AccessGoogle = true;
                        if (CASE == "Login" || CASE == "Register")
                        {
                            //Response.Redirect("~/Home/Home.aspx", false);
                            if (!validation)
                            {
                                GlobalFunctions.ShowModalAlertMessages(this.Page, message, "/index", AlertType.Warning);
                            }
                            else
                            {
                                Response.RedirectToRoute("index");
                            }//Added code for the url rewrite
                        }
                        //else if (CASE == "Basket")
                        //{
                        //    CheckUserExist(Session["APIEmailId"].ToString());
                        //}
                    }
                }


            }
            catch (Exception ex)
            {


            }

        }

       
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == GlobalFunctions.GetLanguageId());
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "ut_custom_mapping").ResourceValue;
                       
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public string GetGoogleUserJSON(string access_token)
        {
            string url = "https://www.googleapis.com/userinfo/email?alt=json";

            WebClient wc = new WebClient();
            wc.Headers.Add("Authorization", "OAuth " + accessToken);
            Stream data = wc.OpenRead(url);
            StreamReader reader = new StreamReader(data);
            string retirnedJson = reader.ReadToEnd();
            data.Close();
            reader.Close();
            return retirnedJson;
        }

        public string GetGoogleUserinfoJSON(string access_token)
        {
            string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json";

            WebClient wc = new WebClient();
            wc.Headers.Add("Authorization", "OAuth " + accessToken);
            Stream data = wc.OpenRead(url);
            StreamReader reader = new StreamReader(data);
            string retirnedJson = reader.ReadToEnd();
            data.Close();
            reader.Close();

            return retirnedJson;
        }

        #region Commented by Snehal 06 10 2016
        // private void CheckUserExist(string emailid)
        //{
        //    int IsExist;
        //    UserBE objUser = new UserBE();
        //    UserBE objBE = new UserBE();
        //    objBE.EmailId = emailid;
        //    IsExist = UserBL.IsExistUser(Constants.USP_IsExistUserAPI, true, objBE);

        //    if (IsExist == 1)
        //    {
        //        UserBE objBEs = new UserBE();
        //        objBEs.EmailId = emailid;
        //        objBEs.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
        //        objBEs.CurrencyId = Convert.ToInt16(GlobalFunctions.GetCurrencyId());
        //        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
        //        if (objUser != null || objUser.EmailId != null)
        //        {
        //            Login_UserLogin.UpdateBasketSessionProducts(objUser, 'Y');
        //            HttpContext.Current.Session["User"] = objUser;
        //          //  Response.Redirect(host + "checkout");//Added code for the url rewrite
        //            ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:Checkout();</script>");
        //        }
        //    }
        //    else
        //    {
        //        Session["Basket"] = true;
        //        ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:Test();</script>");

        //       // ClientScript.RegisterStartupScript(this.GetType(), "ShowLogin", "<script>javascript:LoginAPI();</script>");
        //    }
        //}
        #endregion
    }
}