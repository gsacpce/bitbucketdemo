﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Configuration;
using AHPP.ASP.Net.Lib;
using System.Data;
using System.Collections;
using System.Net;
using PWGlobalEcomm.Stock;
using Microsoft.Security.Application;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using PWGlobalEcomm.GlobalUtilities;
using Microsoft.VisualBasic;
using System.Xml;
using PWGlobalEcomm.Order;
using System.Reflection;
using System.Xml.Linq;
using System.Threading;

namespace Presentation
{
    public partial class GiftCertificate_GiftCertificate : BasePage //System.Web.UI.Page
    {
        #region variables
        //protected global::System.Web.UI.WebControls.Repeater rptBasketListing;
        //protected global::System.Web.UI.HtmlControls.HtmlInputText txtDContact_Name, txtDPhone, txtDAddress1,
        //    txtDAddress2, txtDTown, txtDState__County, txtDPostal_Code, txtAddressTitle, txtContact_Name, txtPhone, txtAddress1,
        //    txtAddress2, txtTown, txtState__County, txtPostal_Code, txtCompany, txtCouponCode;

        //protected global::System.Web.UI.WebControls.DropDownList ddlDCountry, ddlDAddressTitle, ddlPaymentTypes, ddlCountry;
        //protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidDeliveryAddressId, hidStandard, hidExpress, hidStandardTax, hidExpressTax, hidCouponCode;
        //protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox addressCheckbox, chkIsDefault;
        //protected global::System.Web.UI.HtmlControls.HtmlInputRadioButton rbStandard, rbExpress;
        //protected global::System.Web.UI.WebControls.Literal ltrDContact_Name, ltrDPhone, ltrDAddress1, ltrDAddress2, ltrDTown, ltrdState__County, ltrDPostalCode,
        //    ltrDCountry, ltrContact_Name, ltrPhone, ltrAddress1, ltrAddress2, ltrTown, ltrState__County, ltrPostalCode, ltrCountry, ltrCompany, ltrColumnName;
        //protected global::System.Web.UI.HtmlControls.HtmlTextArea txtInstruction;
        //protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnstandard, spnexpress, spnStandardText, spnExpressText, divStandardCont, divExpressCont,
        //    divMOVMessage, divDutyMessage, divStatictext, header2, dvApplyCode;
        //protected global::System.Web.UI.WebControls.Table tblUDFS;
        //protected global::System.Web.UI.WebControls.FileUpload fuDoc;

        //public Int16 intLanguageId = 0;
        //public Int16 intCurrencyId = 0;
        //Int32 intUserId = 0;
        public string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        //public decimal dTotalPrice = 0;
        //public bool bDeliveryAddress = false;
        //string strUserSessionId = string.Empty;
        ////UserBE lstUser;
        //CustomerOrderBE lstCustomerOrders;
        //Int32 intCustomerOrderId = 0;
        //string TxRefGUID = string.Empty;
        //decimal dTotalWeight = 0;
        //public bool bBackOrderAllowed = false;
        //public bool bBASysStore = false;
        //public string strFreightMOVMessage = "You can not proceed because your order value is not fullfilling the criteria.";
        //public string strDutyMessage = "International duty may apply in your country.";
        //public string strCurrencySymbol = string.Empty;
        //public string strPointsText = string.Empty;
        //public bool IsPointsEnbled = false;
        #endregion

        #region New
        protected global::System.Web.UI.WebControls.TextBox txtAmount, txtQuantity, txtPersonalMessage, txtRecipientEmail, txtFriendlyName;
        protected global::System.Web.UI.WebControls.Repeater rptInvoiceAddress, rptCertificate, rptListBasket;
        //protected global::System.Web.UI.WebControls.GridView gvCertificate;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvEditCurrency, divStatictext, header2_space, spnTotalPrice;
        protected global::System.Web.UI.WebControls.Label lblTotal;
        protected global::System.Web.UI.WebControls.DropDownList ddlPaymentTypes;
        protected global::System.Web.UI.WebControls.RegularExpressionValidator regtxtAmount, regtxtQuantity, regtxtRecipientEmail;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvtxtAmount, rfvtxtQuantity, reqtxtFriendlyName, rfvtxtRecipientEmail;
        protected global::System.Web.UI.WebControls.Button btnAddGift;

      
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnRegisterAddress, spnReviewNConfirm, spnPageTitle, spnGC_Friend_Name_Title
           , spnGCDetail, spnAddMore, spnBSubTotal, spnShipping, spnTax, lblAmount, lblQuantity, lblEmail, lblMessage_Title;
        protected global::System.Web.UI.WebControls.Button aProceed;

        public string strRegisteredAddressColumn, strNote, strrptAmount, strrptrfvtxtrptAmount, strrptregtxtrptAmount, strrptQuantity, strrptrfvtxtrptQuantity, strrptregtxtrptQuantity;
        public string strlblrptRecipientEmail, strrfvtxtrptRecipientEmail, strregtxtrptRecipientEmail, strlblrptPersonalMsg,strPleaseEnterText,strAddGiftCertificate;

        StoreBE lstStoreDetail = new StoreBE();
        UserRegistrationBE objUserRegistrationBE = new UserRegistrationBE();
        UserRegistrationBE objTempUserRegistrationBE = new UserRegistrationBE();
        List<UserRegistrationBE.RegistrationFieldsConfigurationBE> objRegistrationFieldsConfigurationBEL = new List<UserRegistrationBE.RegistrationFieldsConfigurationBE>();
        List<StaticPageManagementBE> lstStaticPageBE = new List<StaticPageManagementBE>();
        UserBE lstUser;

        Int16 intFeatureID = 0;
        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        Int32 intUserId = 0;
        double dGoodsTotal = 0;
        public string strCurrencySymbol = string.Empty, strRegCountryErrorMsg = "";
        public bool IsPointsEnbled = false;
        //public string strPointsText = string.Empty;
        public string strFreightMOVMessage = "You can not proceed because your order value is not fullfilling the criteria.";
        public string strDutyMessage = "International duty may apply in your country.";
        List<GiftOrderDetailsBE> GiftOrderDetailslst = new List<GiftOrderDetailsBE>();
        #endregion

       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {
                    if (Session["User"] == null)
                    {
                       //UserBE lstUser = new UserBE();
                       // lstUser.EmailId ="hardik.gohil@powerweave.com";
                       // Session["GuestUser"] = lstUser;
                        lstUser = new UserBE();
                        lstUser = Session["GuestUser"] as UserBE;
                        //Response.RedirectToRoute("login-page");
                        //return;
                    }
                    else
                    {
                        lstUser = new UserBE();
                        lstUser = Session["User"] as UserBE;
                        intUserId = lstUser.UserId;
                    }
                    //Dictionary<string, string> param = new Dictionary<string, string>();
                    //param.Add("GiftOrderID", Convert.ToString(1));
                    //GiftCertificateOrderBE objGiftCertificateOrderBE = GiftCertificateBL.getItem(Constants.USP_GetGiftCertificateDetails, param, true);

                    BindStoreFeatures();
                    //added by Sripal
                    if (!lstStoreDetail.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_GiftCertificate").FeatureValues[0].IsEnabled)
                    {
                        Response.Redirect("~/UnAuthorized.html");
                        return;
                    }
                    BindResourceData();
                    PopulateForm();
                    BindStaticText();
                    Session["GiftOrderDetailslst"] = null;
                    Session["dictGiftOrder"] = null;
                    Session["dictGiftOrderDetails"] = null;
                    Session["dTotlaGoods"] = null;
                    rptCertificate.DataSource = null;
                    rptCertificate.DataBind();
                    lblTotal.Text = "0";
                    spnTotalPrice.InnerText = "0";
                }
                strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void BindStoreFeatures()
        {
            try
            {
                lstStoreDetail = StoreBL.GetStoreDetails();

                
                #region "Comment"
                //FeatureBE PL_DisplaytypeFeature = lstStoreDetail.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "cl_cookielifetime");

                //if (PL_DisplaytypeFeature != null)
                //{
                //    if (PL_DisplaytypeFeature.FeatureValues[0].IsEnabled)
                //    {

                //        HttpCookie myCookieExist = new HttpCookie("CookieEnabled");
                //        myCookieExist = Request.Cookies["CookieEnabled"];

                //        if (myCookieExist == null)
                //        {
                //            HttpCookie myCookie = new HttpCookie("CookieEnabled");
                //            myCookie["name"] = "sripal";
                //            Response.Cookies.Add(myCookie);

                //            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowCookieDiv();", true);

                //        }
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "hideCookieDiv();", true);
                //    }
                //}
                //    lblPLDisplayTypeHeading.Text = PL_DisplaytypeFeature.FeatureAlias;
                //    rptPLDisplayType.DataSource = PL_DisplaytypeFeature.FeatureValues;
                //    rptPLDisplayType.DataBind();
                //} 
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void PopulateForm()
        {
            try
            {
                objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                dvEditCurrency.InnerHtml = GlobalFunctions.GetCurrencySymbol();
                #region "Invoice Address"
                objTempUserRegistrationBE.UserRegistrationLanguagesLst = objUserRegistrationBE.UserRegistrationLanguagesLst.FindAll(x => x.IsActive == true && x.LanguageId == GlobalFunctions.GetLanguageId() && x.RegistrationFieldsConfigurationId <= 10);

                foreach (UserRegistrationBE.RegistrationLanguagesBE obj in objTempUserRegistrationBE.UserRegistrationLanguagesLst)
                {
                    UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE.RegistrationFieldsConfigurationId = obj.RegistrationFieldsConfigurationId;
                    objRegistrationFieldsConfigurationBEL.Add(objRegistrationFieldsConfigurationBE);
                }
                rptInvoiceAddress.DataSource = objRegistrationFieldsConfigurationBEL;//.Skip(1);
                rptInvoiceAddress.DataBind();
                BindAddressDetails();
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void BindResourceData()
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        #region "2nd Panel"
                        //section2.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Section_2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        //sub1.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_RegisteredAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        //sub2.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_DeliveryAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        //chkRegSameAsDelv.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_SameAddress_Text" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        strRegCountryErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_RegCountry_Req_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        //strDelCountryErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Register_Error16_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        regtxtAmount.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Valid_Amount" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        rfvtxtAmount.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Required_Amount" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        rfvtxtQuantity.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Quantity_Required" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        regtxtQuantity.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Quantity_Required" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        #endregion
                        spnRegisterAddress.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "register_section_2" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnReviewNConfirm.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_review_and_confirm" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        aProceed.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_place_order").ResourceValue;
                        spnPageTitle.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_payment_page_title" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnGC_Friend_Name_Title.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_friend_name_title" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnGCDetail.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_detail" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnAddMore.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "add_more" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnBSubTotal.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "sc_basket_subtotal" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnShipping.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "shipping" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        spnTax.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "tax" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        reqtxtFriendlyName.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_required_friendly_name" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        lblAmount.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_amount" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblQuantity.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_quantity" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblEmail.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_recipient_email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        lblMessage_Title.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_message_title" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;


                        rfvtxtRecipientEmail.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_required_recipient_email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        regtxtRecipientEmail.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_valid_recipient_email" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        btnAddGift.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "add" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strNote = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_note" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                        strRegisteredAddressColumn = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_registered_address" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;

                        strrptAmount = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "gc_amount").ResourceValue;
                        strrptrfvtxtrptAmount = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Required_Amount").ResourceValue;
                        strrptregtxtrptAmount = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Valid_Amount").ResourceValue;

                        strrptQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        strrptrfvtxtrptQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Quantity_Required").ResourceValue;
                        strrptregtxtrptQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Invalid_Quantity_Message").ResourceValue;
                        strlblrptRecipientEmail = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Recipient_Email_Text").ResourceValue;
                        strrfvtxtrptRecipientEmail = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Required_Recipient_Email").ResourceValue;
                        strregtxtrptRecipientEmail = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Valid_Recipient_Email").ResourceValue;
                        strlblrptPersonalMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "GC_Message_Title").ResourceValue;
                        strPleaseEnterText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue;
                        strAddGiftCertificate = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Gift_Certificate_Require_Message").ResourceValue;
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void rptAddress_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header)
                {
                    Literal ltrColumnName = (Literal)e.Item.FindControl("ltrColumnName");
                    Literal ltrNote = (Literal)e.Item.FindControl("ltrNote");
                    if (lstUser != null)
                    {
                        if (lstUser.UserPreDefinedColumns[0].ParentFieldGroup == 0)
                        {
                           // ltrColumnName.Text = "<p id=\"pBillingTitle\" class=\"pageText\">" + lstUser.UserPreDefinedColumns[0].ColumnName.Replace("__", "/").Replace("_", " ") + "</p>";
                            ltrColumnName.Text = "<p id=\"pBillingTitle\" class=\"pageText\">"+strRegisteredAddressColumn +"</p>";
                            ltrNote.Text = strNote;
                        }
                    }
                }
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Int16 iRegId = ((UserRegistrationBE.RegistrationFieldsConfigurationBE)(e.Item.DataItem)).RegistrationFieldsConfigurationId;
                    UserRegistrationBE.RegistrationFieldsConfigurationBE objRegistrationFieldsConfigurationBE = new UserRegistrationBE.RegistrationFieldsConfigurationBE();
                    objRegistrationFieldsConfigurationBE = objUserRegistrationBE.UserRegistrationFieldsConfigurationLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);
                    UserRegistrationBE.RegistrationLanguagesBE objLang = objTempUserRegistrationBE.UserRegistrationLanguagesLst.FirstOrDefault(x => x.RegistrationFieldsConfigurationId == iRegId);

                    HiddenField hdfColumnName = (HiddenField)e.Item.FindControl("hdfColumnName");
                    Label lblRegistrationFieldName = (Label)e.Item.FindControl("lblRegistrationFieldName");
                    TextBox txtRegistrationFieldName = (TextBox)e.Item.FindControl("txtRegistrationFieldName");
                    DropDownList ddlRegisterCountry = (DropDownList)e.Item.FindControl("ddlRegisterCountry");
                    RequiredFieldValidator reqtxtRegistrationFieldName = (RequiredFieldValidator)e.Item.FindControl("reqtxtRegistrationFieldName");
                    RequiredFieldValidator reqddlRegisterCountry = (RequiredFieldValidator)e.Item.FindControl("reqddlRegisterCountry");

                    if (hdfColumnName != null)
                    {
                        hdfColumnName.Value = objRegistrationFieldsConfigurationBE.SystemColumnName;
                    }
                    if (objRegistrationFieldsConfigurationBE.IsMandatory)
                    {
                        lblRegistrationFieldName.Text = "*" + HttpUtility.HtmlDecode(objLang.LabelTitle);
                    }
                    else
                    {
                        lblRegistrationFieldName.Text = HttpUtility.HtmlDecode(objLang.LabelTitle);
                    }

                    Repeater rpt = (Repeater)sender;
                    if (objRegistrationFieldsConfigurationBE.FieldType == 2)
                    {
                        ddlRegisterCountry.Visible = true;
                        ddlRegisterCountry.DataSource = objUserRegistrationBE.CountryLst.OrderBy(x => x.CountryName);
                        ddlRegisterCountry.DataTextField = "CountryName";
                        ddlRegisterCountry.DataValueField = "CountryId";
                        ddlRegisterCountry.DataBind();

                        if (rpt != null)
                        {
                            if (rpt.ID == "rptInvoiceAddress")
                            {
                                ddlRegisterCountry.Items.Insert(0, new ListItem(strRegCountryErrorMsg, "0"));
                            }
                        }
                        txtRegistrationFieldName.Visible = false;

                        if (reqtxtRegistrationFieldName != null)
                        {
                            reqtxtRegistrationFieldName.Visible = false;
                            reqtxtRegistrationFieldName.Enabled = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqddlRegisterCountry != null)
                            {
                                reqddlRegisterCountry.Visible = true;
                                reqddlRegisterCountry.Enabled = true;


                                if (rpt != null)
                                {
                                    if (rpt.ID == "rptInvoiceAddress")
                                    {
                                        reqddlRegisterCountry.ErrorMessage = strRegCountryErrorMsg;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ddlRegisterCountry.Visible = false;
                        txtRegistrationFieldName.Visible = true;
                        if (reqddlRegisterCountry != null)
                        {
                            reqddlRegisterCountry.Visible = false;
                            reqddlRegisterCountry.Enabled = false;
                        }
                        if (objRegistrationFieldsConfigurationBE.IsMandatory)
                        {
                            if (reqtxtRegistrationFieldName != null)
                            {
                                reqtxtRegistrationFieldName.Visible = true;
                                reqtxtRegistrationFieldName.Enabled = true;
                                reqtxtRegistrationFieldName.ErrorMessage = strPleaseEnterText + objLang.LabelTitle;
                            }
                        }
                        else
                        {
                            reqtxtRegistrationFieldName.Visible = false;
                            reqtxtRegistrationFieldName.Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void BindStaticText()
        {
            try
            {
                List<StaticPageManagementBE> lst = new List<StaticPageManagementBE>();
                StaticPageManagementBE objBE = new StaticPageManagementBE();
                objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                objBE.PageName = "/info/checkoutcontent";
                lst = StaticPageManagementBL.GetAllDetails(Constants.usp_CheckStaticPageExists, objBE, "PageUrl");
                if (lst != null && lst.Count > 0)
                {
                    string strContent = Convert.ToString(lst[0].WebsiteContent).Replace("~/", GlobalFunctions.GetVirtualPath()).Replace("$host$", GlobalFunctions.GetVirtualPath());

                    List<StaticTextKeyBE> lstKey = new List<StaticTextKeyBE>();
                    StaticTextKeyBE objBEs = new StaticTextKeyBE();
                    objBE.LanguageId = Convert.ToInt16(GlobalFunctions.GetLanguageId());
                    lstKey = StaticTextKeyBL.GetAllKeys(Constants.USP_GetStaticTextDetails, objBEs, "A");
                    if (lstKey != null)
                    {
                        if (lstKey.Count > 0)
                        {
                            for (int i = 0; i < lstKey.Count; i++)
                            {
                                strContent = strContent.Replace("[" + Convert.ToString(lstKey[i].StaticTextKey) + "]", Convert.ToString(lstKey[i].StaticTextValue));
                            }
                        }
                    }
                    if (divStatictext != null)
                        divStatictext.InnerHtml = strContent;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        protected void btnAddGift_OnClick(object sender, EventArgs e)
        {
            try
            {
                GiftOrderDetailsBE objGiftOrderDetailsBE = new GiftOrderDetailsBE();
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                intLanguageId = GlobalFunctions.GetLanguageId();
                int i = 0;
                foreach (RepeaterItem item in rptCertificate.Items)
                {
                    objGiftOrderDetailsBE = new GiftOrderDetailsBE();
                    Dictionary<string, string> dictGiftOrderDetails = new Dictionary<string, string>();
                    GiftCertificateOrderXMLEntity.OrderLineGift objOrderLine = new GiftCertificateOrderXMLEntity.OrderLineGift();
                    TextBox txtrptAmount = (TextBox)item.FindControl("txtrptAmount");
                    TextBox txtrptQuantity = (TextBox)item.FindControl("txtrptQuantity");
                    TextBox txtrptRecipientEmail = (TextBox)item.FindControl("txtrptRecipientEmail");
                    TextBox txtrptPersonalMessage = (TextBox)item.FindControl("txtrptPersonalMessage");

                    bool b = IsValidEmail(txtrptRecipientEmail.Text);
                    if (b)
                    {
                        objGiftOrderDetailsBE.GiftOrderDetailsID = i + 1;
                        objGiftOrderDetailsBE.Amount = Convert.ToDecimal(txtrptAmount.Text);
                        objGiftOrderDetailsBE.PersonalMessage = Sanitizer.GetSafeHtmlFragment(txtrptPersonalMessage.Text.Trim());
                        objGiftOrderDetailsBE.Quantity = Convert.ToInt16(txtrptQuantity.Text);
                        objGiftOrderDetailsBE.RecipientEmail = txtrptRecipientEmail.Text;
                        GiftOrderDetailslst.Add(objGiftOrderDetailsBE);
                        i++;
                    }
                    else
                    {
                        if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                        {
                            lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                            if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                            {
                                #region "2nd Panel"
                                string strError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ValidEmail_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                GlobalFunctions.ShowModalAlertMessages(this, strError, AlertType.Warning);
                                return;
                                #endregion
                            }
                        }
                    }
                }
                Session["GiftOrderDetailslst"] = GiftOrderDetailslst;
                if (Session["GiftOrderDetailslst"] != null)
                {
                    GiftOrderDetailslst = (List<GiftOrderDetailsBE>)Session["GiftOrderDetailslst"];
                    if (GiftOrderDetailslst.Count > 0)
                    {
                        GiftOrderDetailsBE objBE = GiftOrderDetailslst.Last();
                        i = objBE.GiftOrderDetailsID;
                    }
                }

                objGiftOrderDetailsBE = new GiftOrderDetailsBE();
                objGiftOrderDetailsBE.GiftOrderDetailsID = i + 1;
                objGiftOrderDetailsBE.Amount = Convert.ToDecimal(txtAmount.Text);
                objGiftOrderDetailsBE.PersonalMessage = Sanitizer.GetSafeHtmlFragment(txtPersonalMessage.Text.Trim());
                objGiftOrderDetailsBE.Quantity = Convert.ToInt16(txtQuantity.Text);
                objGiftOrderDetailsBE.RecipientEmail = txtRecipientEmail.Text;

                GiftOrderDetailslst.Add(objGiftOrderDetailsBE);

                Session["GiftOrderDetailslst"] = GiftOrderDetailslst;
                rptCertificate.DataSource = GiftOrderDetailslst;
                rptCertificate.DataBind();
                rptListBasket.DataSource = GiftOrderDetailslst;
                rptListBasket.DataBind();
                lblTotal.Text = Convert.ToString(GiftOrderDetailslst.Sum(x => x.Amount * x.Quantity));

                txtAmount.Text = "";
                txtPersonalMessage.Text = "";
                txtQuantity.Text = "1";
                txtRecipientEmail.Text = "";
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void aProceed_ServerClick(object sender, EventArgs e)
        {
            try
            {
                UserBE objUser = new UserBE();
                if (Session["User"] != null)
                {
                    objUser = (UserBE)Session["User"];
                }
                else
                {
                    objUser.EmailId = Convert.ToString(Session["GuestUser"]);
                    //Response.RedirectToRoute("login-page");
                    //return;
                }
                objUserRegistrationBE = UserRegistrationBL.GetUserRegistrationDetails();
                int iGiftID = 0;
                lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                intLanguageId = GlobalFunctions.GetLanguageId();
                foreach (RepeaterItem item in rptCertificate.Items)
                {
                    TextBox txtrptRecipientEmail = (TextBox)item.FindControl("txtrptRecipientEmail");

                    bool b = IsValidEmail(txtrptRecipientEmail.Text);
                    if (!b)
                    {
                        if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                        {
                            lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                            if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                            {
                                #region "2nd Panel"
                                string strError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ValidEmail_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                GlobalFunctions.ShowModalAlertMessages(this, strError, AlertType.Warning);
                                return;
                                #endregion
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(txtAmount.Text) && !string.IsNullOrEmpty(txtQuantity.Text) && !string.IsNullOrEmpty(txtRecipientEmail.Text))
                {
                    bool b = IsValidEmail(txtRecipientEmail.Text);
                    if (!b)
                    {
                        if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                        {
                            lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                            if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                            {
                                #region "2nd Panel"
                                string strError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_ValidEmail_Message" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                                GlobalFunctions.ShowModalAlertMessages(this, strError, AlertType.Warning);
                                txtRecipientEmail.Focus();
                                return;
                                #endregion
                            }
                        }
                    }
                }
                #region GiftOrderDetails Dictionary
                double dTotlaGoods = 0;
                List<Dictionary<string, string>> dictGiftOrderDetailsLst = new List<Dictionary<string, string>>();
                foreach (RepeaterItem item in rptCertificate.Items)
                {
                    Dictionary<string, string> dictGiftOrderDetails = new Dictionary<string, string>();
                    GiftCertificateOrderXMLEntity.OrderLineGift objOrderLine = new GiftCertificateOrderXMLEntity.OrderLineGift();
                    TextBox txtrptAmount = (TextBox)item.FindControl("txtrptAmount");
                    TextBox txtrptQuantity = (TextBox)item.FindControl("txtrptQuantity");
                    TextBox txtrptRecipientEmail = (TextBox)item.FindControl("txtrptRecipientEmail");
                    TextBox txtrptPersonalMessage = (TextBox)item.FindControl("txtrptPersonalMessage");

                    dictGiftOrderDetails.Add("GiftOrderID", Convert.ToString(iGiftID));
                    dictGiftOrderDetails.Add("Amount", Convert.ToString(txtrptAmount.Text));
                    dictGiftOrderDetails.Add("Quantity", Convert.ToString(txtrptQuantity.Text));
                    dictGiftOrderDetails.Add("PersonalMessage", txtrptPersonalMessage.Text);
                    dictGiftOrderDetails.Add("RecipientEmail", txtrptRecipientEmail.Text);
                    if (Session["User"] != null)
                      dictGiftOrderDetails.Add("CreatedBy", Convert.ToString(objUser.UserId));
                    else
                        dictGiftOrderDetails.Add("CreatedBy", Convert.ToString(0));
                    dictGiftOrderDetails.Add("CreatedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    dictGiftOrderDetails.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    dictGiftOrderDetailsLst.Add(dictGiftOrderDetails);

                    dTotlaGoods += Convert.ToDouble(txtrptAmount.Text) * Convert.ToInt32(txtrptQuantity.Text);
                    //DataAccessBase.Insert("USP_InsertGiftOrderDetails", dictGiftOrderDetails, true);
                }
                #region "for Outside div"
                if (!string.IsNullOrEmpty(txtAmount.Text) && !string.IsNullOrEmpty(txtQuantity.Text) && !string.IsNullOrEmpty(txtRecipientEmail.Text))
                {
                    Dictionary<string, string> dictGiftOrderDetails = new Dictionary<string, string>();
                    dictGiftOrderDetails.Add("GiftOrderID", Convert.ToString(iGiftID));
                    dictGiftOrderDetails.Add("Amount", Convert.ToString(txtAmount.Text));
                    dictGiftOrderDetails.Add("Quantity", Convert.ToString(txtQuantity.Text));
                    dictGiftOrderDetails.Add("PersonalMessage", txtPersonalMessage.Text);
                    dictGiftOrderDetails.Add("RecipientEmail", txtRecipientEmail.Text);
                    if (Session["User"] != null)
                        dictGiftOrderDetails.Add("CreatedBy", Convert.ToString(objUser.UserId));
                    else
                        dictGiftOrderDetails.Add("CreatedBy", Convert.ToString(0));
                    dictGiftOrderDetails.Add("CreatedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    dictGiftOrderDetails.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    dictGiftOrderDetailsLst.Add(dictGiftOrderDetails);

                    dTotlaGoods += Convert.ToDouble(txtAmount.Text) * Convert.ToInt32(txtQuantity.Text);
                }
                #endregion
                #endregion

                if (dTotlaGoods > 0)
                {
                    //UserBE objUser = (UserBE)Session["User"];
                    if (Session["User"] == null)
                    {
                        objUser.EmailId = Convert.ToString(Session["GuestUser"]);
                    }
                    else
                    {
                        objUser = (UserBE)Session["User"];
                    }
                    #region "Save to database"
                    GiftCertificateOrderBE objGiftCertificateOrderBE = new GiftCertificateOrderBE();
                    Dictionary<string, string> dictGiftOrder = new Dictionary<string, string>();
                    foreach (RepeaterItem item in rptInvoiceAddress.Items)
                    {
                        TextBox txtRegistrationFieldName = (TextBox)item.FindControl("txtRegistrationFieldName");
                        DropDownList ddlRegisterCountry = (DropDownList)item.FindControl("ddlRegisterCountry");
                        HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");

                        PropertyInfo fi = typeof(GiftCertificateOrderBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        object value = null;
                        if (fi != null)
                        {
                            try
                            {
                                if (hdfColumnName.Value.ToLower() != "predefinedcolumn8")
                                {
                                    if (txtRegistrationFieldName != null)
                                    {
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtRegistrationFieldName.Text.Trim()), fi.PropertyType);
                                    }
                                    //Exceptions.WriteInfoLog("Registration:rptInvoiceAddress after set value in InvoiceAddress fields");
                                }
                                else
                                {
                                    if (ddlRegisterCountry != null)
                                    {
                                        value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlRegisterCountry.SelectedValue), fi.PropertyType);
                                    }
                                }
                            }
                            catch (InvalidCastException) { }
                            fi.SetValue(objGiftCertificateOrderBE, value, null);
                        }
                    }

                                                                                                                                                                                                                                                                                                                                                                                                                         objGiftCertificateOrderBE.FriendlyName = txtFriendlyName.Text;
                    objGiftCertificateOrderBE.IsActive = true;
                    if (Session["GuestUser"] != null)
                    {
                        objGiftCertificateOrderBE.CreatedBy = 0;
                    }
                    else
                    {
                        objGiftCertificateOrderBE.CreatedBy = objUser.UserId;
                    }
                    objGiftCertificateOrderBE.CreatedDate = DateTime.Now;
                    objGiftCertificateOrderBE.IPAddress = GlobalFunctions.GetIpAddress();
                    objGiftCertificateOrderBE.ModifiedDate = DateTime.Now;

                    foreach (PropertyInfo obj in typeof(GiftCertificateOrderBE).GetProperties())
                    {
                        if (obj.Name.ToLower() != "lstgiftorderdetails")
                        {
                            if (obj.Name.ToLower().Contains("date"))
                            {
                                dictGiftOrder.Add(Convert.ToString(obj.Name), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            }
                            else
                            { 
                            dictGiftOrder.Add(Convert.ToString(obj.Name), Convert.ToString(obj.GetValue(objGiftCertificateOrderBE)));
                            }
                        }
                    }
                    
                    Session["dictGiftOrder"] = dictGiftOrder;
                    Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                    DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());
                    //DataAccessBase.Insert(Constants.USP_InsertGiftCertificate, dictGiftOrder, ref DictionaryOutParameterInstance, true);
                    iGiftID = 0;
                    #region "Comments"
                    //foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                    //{
                    //    if (item.Key == "ReturnId")
                    //        iGiftID = Convert.ToInt16(item.Value);
                    //} 
                    #endregion
                    GiftOrderDetailsBE objGiftOrderDetailsBE = new GiftOrderDetailsBE();

                    #region GiftOrderDetails Dictionary
                    //double dTotlaGoods = 0;
                    //List<Dictionary<string, string>> dictGiftOrderDetailsLst = new List<Dictionary<string, string>>();
                    //foreach (RepeaterItem item in rptCertificate.Items)
                    //{
                    //    Dictionary<string, string> dictGiftOrderDetails = new Dictionary<string, string>();
                    //    GiftCertificateOrderXMLEntity.OrderLineGift objOrderLine = new GiftCertificateOrderXMLEntity.OrderLineGift();
                    //    TextBox txtrptAmount = (TextBox)item.FindControl("txtrptAmount");
                    //    TextBox txtrptQuantity = (TextBox)item.FindControl("txtrptQuantity");
                    //    TextBox txtrptRecipientEmail = (TextBox)item.FindControl("txtrptRecipientEmail");
                    //    TextBox txtrptPersonalMessage = (TextBox)item.FindControl("txtrptPersonalMessage");

                    //    dictGiftOrderDetails.Add("GiftOrderID", Convert.ToString(iGiftID));
                    //    dictGiftOrderDetails.Add("Amount", Convert.ToString(txtrptAmount.Text));
                    //    dictGiftOrderDetails.Add("Quantity", Convert.ToString(txtrptQuantity.Text));
                    //    dictGiftOrderDetails.Add("PersonalMessage", txtrptPersonalMessage.Text);
                    //    dictGiftOrderDetails.Add("RecipientEmail", txtrptRecipientEmail.Text);
                    //    dictGiftOrderDetails.Add("CreatedBy", Convert.ToString(objUser.UserId));
                    //    dictGiftOrderDetails.Add("CreatedDate", DateTime.Now.ToString());
                    //    dictGiftOrderDetails.Add("ModifiedDate", DateTime.Now.ToString());

                    //    dictGiftOrderDetailsLst.Add(dictGiftOrderDetails);

                    //    dTotlaGoods += Convert.ToDouble(txtrptAmount.Text) * Convert.ToInt32(txtrptQuantity.Text);
                    //    //DataAccessBase.Insert("USP_InsertGiftOrderDetails", dictGiftOrderDetails, true);
                    //}
                    //#region "for Outside div"
                    //if (!string.IsNullOrEmpty(txtAmount.Text) && !string.IsNullOrEmpty(txtQuantity.Text) && !string.IsNullOrEmpty(txtRecipientEmail.Text))
                    //{
                    //    Dictionary<string, string> dictGiftOrderDetails = new Dictionary<string, string>();
                    //    dictGiftOrderDetails.Add("GiftOrderID", Convert.ToString(iGiftID));
                    //    dictGiftOrderDetails.Add("Amount", Convert.ToString(txtAmount.Text));
                    //    dictGiftOrderDetails.Add("Quantity", Convert.ToString(txtQuantity.Text));
                    //    dictGiftOrderDetails.Add("PersonalMessage", txtPersonalMessage.Text);
                    //    dictGiftOrderDetails.Add("RecipientEmail", txtRecipientEmail.Text);
                    //    dictGiftOrderDetails.Add("CreatedBy", Convert.ToString(objUser.UserId));
                    //    dictGiftOrderDetails.Add("CreatedDate", DateTime.Now.ToString());
                    //    dictGiftOrderDetails.Add("ModifiedDate", DateTime.Now.ToString());

                    //    dictGiftOrderDetailsLst.Add(dictGiftOrderDetails);

                    //    dTotlaGoods += Convert.ToDouble(txtAmount.Text) * Convert.ToInt32(txtQuantity.Text);
                    //}
                    //#endregion
                    #endregion
                    Session["dTotlaGoods"] = dTotlaGoods;
                    Session["dictGiftOrderDetails"] = dictGiftOrderDetailsLst;
                    SaveGuestUserRegistration();
                    Response.Redirect("~/ShoppingCart/CreditCardPaymentGift.aspx");
                    return;

                    #region "Comments"
                    //Dictionary<string, string> dictGiftOrder = new Dictionary<string, string>();
                    //foreach (PropertyInfo obj in typeof(GiftCertificateOrderXMLEntity).GetProperties())
                    //{
                    //    dictGiftOrder.Add(Convert.ToString(obj.Name), Convert.ToString(obj.GetValue(obj.Name)));
                    //}

                    //foreach(PropertyInfo objAddress in typeof(GiftCertificateOrderXMLEntity.BillTo).GetProperties())
                    //{
                    //    dictGiftOrder.Add(Convert.ToString(objAddress.Name), Convert.ToString(objAddress.GetValue(objAddress.Name)));
                    //} 
                    #endregion
                    #endregion
                    #region "Comments"
                    //#region Gift Entity
                    //GiftCertificateOrderXMLEntity objGiftCertificateOrderXMLEntity = new GiftCertificateOrderXMLEntity();
                    //lstStoreDetail = StoreBL.GetStoreDetails();

                    //objGiftCertificateOrderXMLEntity.CustomerContactId = objUser.BASYS_CustomerContactId;
                    ////List<StoreBE.StoreCurrencyBE> Storecurrency = lstStoreDetail.StoreCurrencies.FindAll(x => x.IsActive == true && x.CurrencyId == GlobalFunctions.GetCurrencyId());
                    //objGiftCertificateOrderXMLEntity.DivisionId = Convert.ToInt32(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId);
                    //objGiftCertificateOrderXMLEntity.SkipPipeline = "Y";
                    //objGiftCertificateOrderXMLEntity.OrderCurrency = Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode); ;
                    //objGiftCertificateOrderXMLEntity.PaymentTypeId = 52;
                    //objGiftCertificateOrderXMLEntity.TransactionRef = "c1ba8129-5826-40f5-84e8-d96d1473bba1";// Guid.NewGuid().ToString();
                    //objGiftCertificateOrderXMLEntity.Last4Digits = "4463";
                    //objGiftCertificateOrderXMLEntity.CollectedAmount = Convert.ToDecimal(lblTotal.Text);
                    //objGiftCertificateOrderXMLEntity.CollectedDate = DateTime.Now;
                    //objGiftCertificateOrderXMLEntity.CollectedFlag = "Y";
                    //objGiftCertificateOrderXMLEntity.GoodsTotal = Convert.ToDecimal(lblTotal.Text);
                    //objGiftCertificateOrderXMLEntity.CustomerRef = "";
                    //objGiftCertificateOrderXMLEntity.OrderNotes = "";
                    //objGiftCertificateOrderXMLEntity.SourceCode = "WEBZ";// Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeName);
                    //objGiftCertificateOrderXMLEntity.SourceCodeId = "5281";// Convert.ToString(lstStoreDetail.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeId);

                    //#endregion
                    //#region Bill TO
                    //#region "Invoice Address"
                    //UserBE objBE = new UserBE();
                    //foreach (RepeaterItem item in rptInvoiceAddress.Items)
                    //{
                    //    TextBox txtRegistrationFieldName = (TextBox)item.FindControl("txtRegistrationFieldName");
                    //    DropDownList ddlRegisterCountry = (DropDownList)item.FindControl("ddlRegisterCountry");
                    //    HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");

                    //    PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    //    object value = null;
                    //    if (fi != null)
                    //    {
                    //        try
                    //        {
                    //            if (hdfColumnName.Value.ToLower() != "predefinedcolumn8")
                    //            {
                    //                if (txtRegistrationFieldName != null)
                    //                {
                    //                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtRegistrationFieldName.Text.Trim()), fi.PropertyType);
                    //                }
                    //                //Exceptions.WriteInfoLog("Registration:rptInvoiceAddress after set value in InvoiceAddress fields");
                    //            }
                    //            else
                    //            {
                    //                if (ddlRegisterCountry != null)
                    //                {
                    //                    value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlRegisterCountry.SelectedValue), fi.PropertyType);
                    //                }
                    //            }
                    //        }
                    //        catch (InvalidCastException) { }
                    //        fi.SetValue(objBE, value, null);
                    //    }
                    //}
                    //#endregion
                    //GiftCertificateOrderXMLEntity.BillTo objBillTo = new GiftCertificateOrderXMLEntity.BillTo();
                    //objBillTo.ContactName = objBE.PredefinedColumn1;
                    //objBillTo.Address1 = objBE.PredefinedColumn3;
                    //objBillTo.Address2 = objBE.PredefinedColumn4;
                    //objBillTo.City = objBE.PredefinedColumn5;
                    //objBillTo.County = objBE.PredefinedColumn6;
                    //objBillTo.PostalCode = objBE.PredefinedColumn7;
                    //objBillTo.Country = objBE.PredefinedColumn8;
                    //objBillTo.Telephone = objBE.PredefinedColumn9;

                    ////objGiftCertificateOrderXMLEntity.BillToInstance = objBillTo;
                    //#endregion
                    //#region OrderLineGift
                    //List<GiftCertificateOrderXMLEntity.OrderLineGift> objOrderLineGiftLst = new List<GiftCertificateOrderXMLEntity.OrderLineGift>();
                    //foreach (RepeaterItem item in rptCertificate.Items)
                    //{
                    //    GiftCertificateOrderXMLEntity.OrderLineGift objOrderLine = new GiftCertificateOrderXMLEntity.OrderLineGift();
                    //    TextBox txtrptAmount = (TextBox)item.FindControl("txtrptAmount");
                    //    TextBox txtrptQuantity = (TextBox)item.FindControl("txtrptQuantity");
                    //    TextBox txtrptRecipientEmail = (TextBox)item.FindControl("txtrptRecipientEmail");
                    //    TextBox txtrptPersonalMessage = (TextBox)item.FindControl("txtrptPersonalMessage");

                    //    objOrderLine.LineValue = Convert.ToDecimal(txtrptAmount.Text);
                    //    objOrderLine.LineQty = Convert.ToInt32(txtrptQuantity.Text);
                    //    objOrderLine.LineMessage = txtrptPersonalMessage.Text;
                    //    objOrderLine.RecipientEmail = txtrptRecipientEmail.Text;

                    //    objOrderLineGiftLst.Add(objOrderLine);
                    //}
                    //#endregion




                    //#region Generate XML
                    //#region OrderHeader

                    //XElement xOrderHeaderElement = null;

                    ////switch (objGiftCertificateOrderXMLEntity.cardType)
                    ////{
                    ////    case CardType.PurchaseOrder:
                    ////        xOrderHeaderElement = new XElement("OrderHeader", new XElement("DivisionID", objOrderXMLEntity.DivisionId)
                    ////                                                               , new XElement("KeyGroupID", objOrderXMLEntity.KeyGroupId)
                    ////                                                               , new XElement("CustomerID", objOrderXMLEntity.CustomerId)
                    ////                                                               , new XElement("CustomerContactID", objOrderXMLEntity.CustomerContactId)
                    ////                                                               , new XElement("PaymentTypeID", objOrderXMLEntity.PaymentTypeId)
                    ////                                                               , new XElement("AuthorisationCode", objOrderXMLEntity.AuthorisationCode)
                    ////                                                               , new XElement("SourceCode", objOrderXMLEntity.SourceCodeName)
                    ////                                                               , new XElement("SourceCodeID", objOrderXMLEntity.SourceCodeId)
                    ////                                                               , new XElement("OrderCurrency", objOrderXMLEntity.OrderCurrency)
                    ////                                                               , new XElement("GoodsTotal", objOrderXMLEntity.GoodsTotal)
                    ////                                                               , new XElement("Freight", objOrderXMLEntity.Fright)
                    ////                                                               , new XElement("Tax", objOrderXMLEntity.Tax)
                    ////                                                               , new XElement("CustomerRef", objOrderXMLEntity.CustomerRef)
                    ////                                                               , new XElement("OrderNotes", objOrderXMLEntity.OrderNotes)
                    ////                                                               , new XElement("TaxNumber", objOrderXMLEntity.dtTax.Rows[0]["TaxNumber"])
                    ////                                                               , new XElement("VendorTaxCode", objOrderXMLEntity.dtTax.Rows[0]["VendorTaxCode"]), new XAttribute("powerWeaveID", string.Empty)
                    ////                                                                                                                                                       , new XAttribute("orderDate", string.Empty));
                    ////        break;

                    ////case CardType.Card:
                    //xOrderHeaderElement = new XElement("OrderHeader", new XElement("CustomerContactID", objGiftCertificateOrderXMLEntity.CustomerContactId)
                    //    , new XElement("DivisionID", objGiftCertificateOrderXMLEntity.DivisionId)
                    //                                                       , new XElement("SkipPipeline", objGiftCertificateOrderXMLEntity.SkipPipeline)
                    //                                                       , new XElement("OrderCurrency", objGiftCertificateOrderXMLEntity.OrderCurrency)
                    //                                                       , new XElement("PaymentTypeID", objGiftCertificateOrderXMLEntity.PaymentTypeId)
                    //                                                       , new XElement("TransactionRef", objGiftCertificateOrderXMLEntity.TransactionRef)
                    //                                                       , new XElement("Last4Digits", objGiftCertificateOrderXMLEntity.Last4Digits)
                    //                                                       , new XElement("CollectedDate", DateTime.Now.ToString("yyyy-MM-dd"))
                    //                                                       , new XElement("CollectedAmount", objGiftCertificateOrderXMLEntity.CollectedAmount)
                    //                                                       , new XElement("CollectedFlag", objGiftCertificateOrderXMLEntity.CollectedFlag)
                    //                                                       , new XElement("GoodsTotal", objGiftCertificateOrderXMLEntity.GoodsTotal)
                    //                                                       , new XElement("CustomerRef", objGiftCertificateOrderXMLEntity.CustomerRef)
                    //                                                       , new XElement("OrderNotes", objGiftCertificateOrderXMLEntity.OrderNotes)
                    //                                                       , new XElement("SourceCode", objGiftCertificateOrderXMLEntity.SourceCode)
                    //                                                       , new XElement("SourceCodeId", objGiftCertificateOrderXMLEntity.SourceCodeId));
                    ////        break;
                    ////}

                    ////Logger.LogMsg(PageNames.WebService_OASIS, LogLevel.Info, GlobalUtilities.LoggedInUserId, MethodBase.GetCurrentMethod().Name, "OrderHeader node created.");

                    //#endregion
                    //#region ShipTo
                    //string strCountryCode = objUserRegistrationBE.CountryLst.FirstOrDefault(x => x.CountryId == Convert.ToInt16(objBE.PredefinedColumn8)).CountryCode;
                    //string strCountryName = objUserRegistrationBE.CountryLst.FirstOrDefault(x => x.CountryId == Convert.ToInt16(objBE.PredefinedColumn8)).CountryName;
                    //XElement xBillToElement = new XElement("BillTo", new XElement("ContactName", objBillTo.ContactName)
                    //                                          , new XElement("Address1", objBillTo.Address1)
                    //                                          , new XElement("Address2", objBillTo.Address2)
                    //                                          , new XElement("Address3", string.Empty)
                    //                                          , new XElement("City", objBillTo.City)
                    //                                          , new XElement("PostCode", objBillTo.PostalCode)
                    //                                          , new XElement("County", objBillTo.County)
                    //                                          , new XElement("Country", strCountryName, new XAttribute("CountryCode", strCountryCode))
                    //                                          , new XElement("Telephone", objBillTo.Telephone));
                    //#endregion
                    //#region OrderLines
                    //XElement xOrderLineElement = new XElement("OrderLines");
                    ////List<GiftCertificateOrderXMLEntity.OrderLineGift> OrderLineGiftLst = new List<GiftCertificateOrderXMLEntity.OrderLineGift>();
                    //int i = 1;
                    //foreach (RepeaterItem item in rptCertificate.Items)
                    //{
                    //    //GiftCertificateOrderXMLEntity.OrderLineGift objOrderLine = new GiftCertificateOrderXMLEntity.OrderLineGift();
                    //    TextBox txtrptAmount = (TextBox)item.FindControl("txtrptAmount");
                    //    TextBox txtrptQuantity = (TextBox)item.FindControl("txtrptQuantity");
                    //    TextBox txtrptRecipientEmail = (TextBox)item.FindControl("txtrptRecipientEmail");
                    //    TextBox txtrptPersonalMessage = (TextBox)item.FindControl("txtrptPersonalMessage");

                    //    //objOrderLine.LineQty = Convert.ToInt32(txtrptQuantity.Text);
                    //    //objOrderLine.LineValue = Convert.ToDecimal(txtrptAmount.Text);
                    //    //objOrderLine.LineMessage = txtrptPersonalMessage.Text;
                    //    //objOrderLine.RecipientEmail = txtrptRecipientEmail.Text;

                    //    //objOrderLineGiftLst.Add(objOrderLine);
                    //    XElement productElement = new XElement("OrderLine", new XAttribute("LineNumber", Convert.ToString(i))
                    //                                                         , new XElement("LineQty", Convert.ToInt32(txtrptQuantity.Text))
                    //                                                        , new XElement("LineValue", Convert.ToDecimal(txtrptAmount.Text))
                    //                                                        , new XElement("LineMessage", txtrptPersonalMessage.Text)
                    //                                                        , new XElement("RecipientEmail", txtrptRecipientEmail.Text));
                    //    xOrderLineElement.Add(productElement);
                    //    i++;
                    //}
                    //#endregion
                    //#region OrderXML Node
                    //xOrderHeaderElement.Add(xBillToElement);
                    //XElement xOrderElement;

                    //xOrderElement = new XElement("GiftOrder", xOrderHeaderElement, xOrderLineElement);
                    //XDocument xOrderXML = new XDocument(xOrderElement);
                    //XmlNode OrderXMLNode = new XmlDocument().ReadNode(xOrderXML.Root.CreateReader());
                    //#endregion
                    //#region Make Order
                    //int intOrderID = 0;
                    //try
                    //{
                    //    if (!objUser.EmailId.ToLower().Contains("@powerweave.com"))
                    //    {
                    //        Order order = new Order();
                    //        NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    //        order.Credentials = objNetworkCredentials;
                    //        intOrderID = order.CreateGiftOrder(OrderXMLNode);
                    //    }
                    //    else
                    //        intOrderID = 1;
                    //}
                    //catch (Exception ex)
                    //{
                    //    Exceptions.WriteExceptionLog(ex);
                    //}

                    //#endregion
                    //#endregion

                    //#region "Comments"
                    ////objOrderLine.LineQty
                    ////int intPaymentTypeId = Convert.ToInt16(ddlPaymentType.SelectedValue);
                    ////int intPaymentTypeId = 4;
                    ////List<object> obj = ShoppingCartBL.GetUDFS(intPaymentTypeId);
                    ////List<ShoppingCartBE> getUDFSDetails = (List<ShoppingCartBE>)obj;

                    ////if (getUDFSDetails != null)
                    ////    Logger.LogMsg(PageNames.ShoppingCartPage, LogLevel.Info, GlobalUtilities.LoggedInUserId, MethodBase.GetCurrentMethod().Name, getUDFSDetails.Count > 0 ? "UDFS Details fetched from DB." : "UDFS Details object is null.");

                    ////if (getUDFSDetails.Count > 0)
                    ////{
                    ////    foreach (ShoppingCartBE item in getUDFSDetails)
                    ////    {
                    ////        TextBox txtPONo = (TextBox)tblUDFS.FindControl("txt" + Convert.ToString(item.ID));

                    ////        if (txtPONo != null)
                    ////        {
                    ////            item.Caption = txtPONo.Text.Trim();
                    ////            item.SequenceNo = item.SequenceNo;
                    ////        }
                    ////    }
                    ////} 
                    //#endregion 
                    #endregion
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this, strAddGiftCertificate, AlertType.Failure);
                }

            }
            catch (ThreadAbortException) { }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void BindPaymentTypes()
        {
            try
            {
                List<object> lstPaymentTypes = new List<object>();
                List<PaymentTypesBE> lstPaymentTypesBE = new List<PaymentTypesBE>();
                lstPaymentTypes = ShoppingCartBL.GetAllPaymentTypes();

                if (lstPaymentTypes.Count > 0)
                {
                    ddlPaymentTypes.DataTextField = "PaymentTypeRef";
                    ddlPaymentTypes.DataValueField = "PaymentMethod";
                    ddlPaymentTypes.DataSource = lstPaymentTypes;
                    ddlPaymentTypes.DataBind();

                    ListItem litem = new ListItem();
                    litem.Text = "Please select";
                    litem.Value = "0";
                    ddlPaymentTypes.Items.Insert(0, litem);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        protected void rptListBasket_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl spnQty = (HtmlGenericControl)e.Item.FindControl("spnQty");
                    HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
                    HtmlGenericControl spnUnitAmt = (HtmlGenericControl)e.Item.FindControl("spnUnitAmt");

                    HtmlGenericControl spnStGC = (HtmlGenericControl)e.Item.FindControl("spnStGC");

                    if (spnPrice != null)
                    {
                        double dPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "Amount"));
                        int iQty = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                        spnUnitAmt.InnerHtml = Convert.ToString(dPrice);
                        spnUnitAmt.Attributes.Add("class", "uamt" + (e.Item.ItemIndex + 1));

                        spnQty.InnerHtml = Convert.ToString(iQty);
                        spnQty.Attributes.Add("class", "qty" + (e.Item.ItemIndex + 1));

                        double dGiftPrice = dPrice * iQty;
                        spnPrice.InnerHtml = Convert.ToString(dGiftPrice);
                        spnPrice.Attributes.Add("class", "prc" + (e.Item.ItemIndex + 1));

                        dGoodsTotal += dGiftPrice;

                        lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                        if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                        {
                            lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                            if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                            {
                                spnStGC.InnerText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "gc_store" && x.LanguageId == GlobalFunctions.GetLanguageId()).ResourceValue;
                            }
                        }
                    }
                }
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    lblTotal.Text = Convert.ToString(dGoodsTotal);
                    spnTotalPrice.InnerText = Convert.ToString(dGoodsTotal);
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void lnkrptDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                int iCmd = Convert.ToInt32(lnk.CommandArgument);
                if (Session["GiftOrderDetailslst"] != null)
                {
                    GiftOrderDetailslst = (List<GiftOrderDetailsBE>)Session["GiftOrderDetailslst"];
                    GiftOrderDetailsBE objBE = GiftOrderDetailslst.FirstOrDefault(x => x.GiftOrderDetailsID == iCmd);
                    GiftOrderDetailslst.Remove(objBE);
                }
                rptCertificate.DataSource = GiftOrderDetailslst;
                rptCertificate.DataBind();
                rptListBasket.DataSource = GiftOrderDetailslst;
                rptListBasket.DataBind();

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void rptCertificate_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblrptAmount = (Label)e.Item.FindControl("lblrptAmount");
                    Label lblrptQuantity = (Label)e.Item.FindControl("lblrptQuantity");
                    Label lblrptRecipientEmail = (Label)e.Item.FindControl("lblrptRecipientEmail");
                    Label lblrptPersonalMsg = (Label)e.Item.FindControl("lblrptPersonalMsg");

                    RequiredFieldValidator rfvtxtrptAmount = (RequiredFieldValidator)e.Item.FindControl("rfvtxtrptAmount");
                    RequiredFieldValidator rfvtxtrptQuantity = (RequiredFieldValidator)e.Item.FindControl("rfvtxtrptQuantity");
                    RequiredFieldValidator rfvtxtrptRecipientEmail = (RequiredFieldValidator)e.Item.FindControl("rfvtxtrptRecipientEmail");

                    RegularExpressionValidator regtxtrptAmount = (RegularExpressionValidator)e.Item.FindControl("regtxtrptAmount");
                    RegularExpressionValidator regtxtrptQuantity = (RegularExpressionValidator)e.Item.FindControl("regtxtrptQuantity");
                    RegularExpressionValidator regtxtrptRecipientEmail = (RegularExpressionValidator)e.Item.FindControl("regtxtrptRecipientEmail");


                    LinkButton lnkrptDelete = (LinkButton)e.Item.FindControl("lnkrptDelete");

                    lblrptAmount.Text = strrptAmount;
                    lblrptQuantity.Text = strrptQuantity;
                    lblrptRecipientEmail.Text = strlblrptRecipientEmail;
                    lblrptPersonalMsg.Text = strlblrptPersonalMsg;

                    rfvtxtrptAmount.ErrorMessage = strrptrfvtxtrptAmount;
                    rfvtxtrptQuantity.ErrorMessage = strrptrfvtxtrptQuantity;
                    rfvtxtrptRecipientEmail.ErrorMessage = strrfvtxtrptRecipientEmail;

                    regtxtrptAmount.ErrorMessage = strrptregtxtrptAmount;
                    regtxtrptQuantity.ErrorMessage = strrptregtxtrptQuantity;
                    regtxtRecipientEmail.ErrorMessage = strregtxtrptRecipientEmail;


                    if (Convert.ToInt32(e.Item.ItemIndex + 1) == 1)
                    {
                        lnkrptDelete.Visible = false;
                    }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        void BindAddressDetails()
        {
            try
            {
                #region
                if (lstUser != null)
                {
                    foreach (RepeaterItem item in rptInvoiceAddress.Items)
                    {
                        TextBox txtRegistrationFieldName = (TextBox)item.FindControl("txtRegistrationFieldName");
                        DropDownList ddlRegisterCountry = (DropDownList)item.FindControl("ddlRegisterCountry");
                        HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");

                        try
                        {
                            if (hdfColumnName.Value.ToLower() != "predefinedcolumn8")
                            {
                                if (txtRegistrationFieldName != null)
                                {
                                    txtRegistrationFieldName.Text = HttpUtility.HtmlDecode(Convert.ToString(typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).GetValue(lstUser)));// lstUser.PredefinedColumn8;
                                }
                            }
                            else
                            {
                                if (ddlRegisterCountry != null)
                                {
                                    ddlRegisterCountry.SelectedValue = Convert.ToString(typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).GetValue(lstUser));// lstUser.PredefinedColumn8;
                                }
                            }
                        }
                        catch (InvalidCastException) { }
                    }
                }
                #endregion
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void SaveGuestUserRegistration()
        {
            if (Session["GuestUser"] != null)
            {
                Exceptions.WriteInfoLog("Inside Session[GuestUser] != null in SaveAddressDetails()");
                Session["IsLogin"] = true;
                #region "Insert into database and BASYS"
                #region
                int i = 0;
                try
                {
                    UserBE objBE = new UserBE();
                    UserBE.UserDeliveryAddressBE obj = new UserBE.UserDeliveryAddressBE();
                    objBE.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["GuestUser"]).Trim());
                    objBE.Password = SaltHash.ComputeHash("randompassword", "SHA512", null);
                    objBE.FirstName = txtFriendlyName.Text.Trim();
                    objBE.UserDeliveryAddress.Add(obj);
                    objBE.IPAddress = GlobalFunctions.GetIpAddress();
                    objBE.CurrencyId = GlobalFunctions.GetCurrencyId();
                    objBE.IsGuestUser = true;
                    objBE.LanguageId = Convert.ToInt16(intLanguageId);
                    objBE.IsMarketing = false;
                    objBE.UserTypeID = 1;// Added by SHRIGANESH 22 Sept 2016
                    objBE.Points = -1; // Added by ShriGanesh 26 April 2017

                    #region "Invoice Address"
                    Exceptions.WriteInfoLog("Registration:User Invoice Address start.");
                    try
                    {
                        foreach (RepeaterItem item in rptInvoiceAddress.Items)
                        {
                            TextBox txtRegistrationFieldName = (TextBox)item.FindControl("txtRegistrationFieldName");
                            DropDownList ddlRegisterCountry = (DropDownList)item.FindControl("ddlRegisterCountry");
                            HiddenField hdfColumnName = (HiddenField)item.FindControl("hdfColumnName");

                            PropertyInfo fi = typeof(UserBE).GetProperty(hdfColumnName.Value, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            object value = null;
                            if (fi != null)
                            {
                                try
                                {
                                    int q = string.Compare("predefinedcolumn8", hdfColumnName.Value, true);

                                    if (hdfColumnName.Value.ToLower() != "predefinedcolumn8")
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptInvoiceAddress before set value in InvoiceAddress fields");
                                        if (txtRegistrationFieldName != null)
                                        {
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(txtRegistrationFieldName.Text.Trim()), fi.PropertyType);
                                        }
                                        Exceptions.WriteInfoLog("Registration:rptInvoiceAddress after set value in InvoiceAddress fields");
                                    }
                                    else
                                    {
                                        Exceptions.WriteInfoLog("Registration:rptInvoiceAddress column name predefinedcolumn8.");
                                        Exceptions.WriteInfoLog("Registration:rptInvoiceAddress before set value in InvoiceAddress fields");
                                        if (ddlRegisterCountry != null)
                                        {
                                            value = Convert.ChangeType(Sanitizer.GetSafeHtmlFragment(ddlRegisterCountry.SelectedValue), fi.PropertyType);
                                        }
                                        Exceptions.WriteInfoLog("Registration:rptInvoiceAddress after set value in InvoiceAddress fields");
                                    }
                                }
                                catch (InvalidCastException) { }
                                fi.SetValue(objBE, value, null);
                            }
                        }
                    }
                    catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
                    Exceptions.WriteInfoLog("Registration:User Invoice Address end.");
                    #endregion

                    Exceptions.WriteInfoLog("Before calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                    i = UserBL.ExecuteRegisterDetails(Constants.USP_InsertUserRegistrationDetails, true, objBE, "StoreCustomer");
                    Exceptions.WriteInfoLog("After calling UserBL.ExecuteRegisterDetails in SaveAddressDetails()");
                    objBE.UserId = Convert.ToInt16(i);
                    if (i > 0 || i == -2)
                    {
                        if (i > 0)
                        {
                            Exceptions.WriteInfoLog("Before calling StoreBL.GetStoreDetails() in SaveAddressDetails()");
                            StoreBE objStoreBE = StoreBL.GetStoreDetails();
                            Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails() in SaveAddressDetails()");
                            if (objStoreBE.IsBASYS)
                            {
                                for (int x = 0; x < objStoreBE.StoreCurrencies.Count(); x++)
                                {
                                    if (objStoreBE.StoreCurrencies[x].IsActive)
                                    {
                                        InsertBASYSId(objStoreBE.StoreCurrencies[x].CurrencyId, Convert.ToString(Session["GuestUser"]).Trim(), objBE.UserId, objBE);
                                    }
                                }
                            }
                        }

                        //Customer already registered as a guest user
                        UserBE objBEs = new UserBE();
                        UserBE objUser = new UserBE();
                        objBEs.EmailId = Sanitizer.GetSafeHtmlFragment(Convert.ToString(Session["GuestUser"]).Trim());
                        objBEs.LanguageId = GlobalFunctions.GetLanguageId();
                        objBEs.CurrencyId = GlobalFunctions.GetCurrencyId();
                        objUser = UserBL.GetSingleObjectDetails(Constants.USP_GetLoginDetails, true, objBEs);
                        //Below if condition is added according to harley guestcheckout tax calculation July 28,2016
                        if (objUser != null || objUser.EmailId != null)
                        {
                           Session["User"] = objUser;
                            Session["GuestUser"] = null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
                #endregion
                #endregion
            }
        }

        private void InsertBASYSId(Int16 CurrencyIdx, string strEmailId, Int16 iUserID,UserBE objUserBE)
        {
            try
            {
                Exceptions.WriteInfoLog("Inside InsertBASYSId() in Payment.aspx.cs");
                StoreBE objStore = StoreBL.GetStoreDetails();
                Exceptions.WriteInfoLog("After calling StoreBL.GetStoreDetails()");
                int intCustomerID_OASIS;
                

                #region "Assign the data to object from the inputs"
                objUserBE.EmailId = Sanitizer.GetSafeHtmlFragment(strEmailId.Trim());
                objUserBE.UserId = iUserID;

                objUserBE.IPAddress = GlobalFunctions.GetIpAddress();
                #endregion
                objUserBE.DefaultCustId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).DefaultCompanyId; //1281850; // objStore.StoreCurrencies[i].DefaultCompanyId;
                objUserBE.SourceCodeId = objStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == Convert.ToInt16(CurrencyIdx)).SourceCodeId; //5281; //objStore.StoreCurrencies[i].SourceCodeId;
                    Exceptions.WriteInfoLog("Before calling RegisterCustomer_OASIS.BuildCustContact_OASIS()");
                    intCustomerID_OASIS = RegisterCustomer_OASIS.BuildCustContact_OASIS(objUserBE);
                    if (intCustomerID_OASIS > 0)
                    {
                        Exceptions.WriteInfoLog("Before calling UserBL.ExecuteBASYSDetails()");
                        UserBL.ExecuteBASYSDetails(Constants.USP_InsertUserBASYSDetails, true, objUserBE.UserId, CurrencyIdx, intCustomerID_OASIS);
                        Exceptions.WriteInfoLog("After calling UserBL.ExecuteBASYSDetails()");
                    }
                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}


//<asp:GridView ID="gvCertificate" runat="server" ShowFooter="false" AutoGenerateColumns="false">
//                                <Columns>
//                                    <asp:TemplateField>
//                                        <HeaderTemplate>
//                                            <div id="header2_space" style="margin-top: 15px;">
//                                        </HeaderTemplate>
//                                        <ItemTemplate>
//                                            <div class="col-xs-6">
//                                                <div class="form-group">
//                                                    <label>Amount</label>
//                                                    <div class="input-group  input-group-sm">
//                                                        <div class="input-group-addon">£</div>
//                                                        <asp:TextBox ID="txtgvAmount" runat="server" Text='<%# Eval("Amount") %>' CssClass="form-control"></asp:TextBox>
//                                                        <input type="text" placeholder="5.00" id="exampleInputAmount" class="form-control">
//                                                    </div>
//                                                </div>
//                                            </div>
//                                            <div class="col-xs-6">
//                                                <div class="form-group">
//                                                    <label>Quantity</label>
//                                                    <div class="input-group input-group-sm ">
//                                                        <span class="input-group-btn"><a role="button" class="btn btn-default active" href="">&ndash;</a></span>
//                                                        <asp:TextBox ID="txtgvQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="btn btn-default active"></asp:TextBox>
//                                                        <span class="input-group-btn"><a role="button" class="btn btn-default active" href="">+</a></span>
//                                                    </div>
//                                                </div>
//                                            </div>
//                                            <div class="col-xs-12">
//                                                <div class="form-group">
//                                                    <label>Recipient E-mail</label>
//                                                    <asp:TextBox ID="txtRecipientEmail" runat="server" Text='<%# Eval("RecipientEmail") %>' CssClass="form-control input-sm"></asp:TextBox>
//                                                </div>
//                                            </div>
//                                            <div class="col-xs-12">
//                                                <div class="form-group">
//                                                    <label>Personal message</label>
//                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("PersonalMessage") %>' CssClass="form-control input-sm" TextMode="MultiLine" Rows="3"></asp:TextBox>
//                                                </div>
//                                                <div class="checkout_giftcert_bottom"><a href="#"><span aria-hidden="true" class="glyphicon glyphicon-remove"></span></a></div>
//                                            </div>
//                                        </ItemTemplate>
//                                        <FooterTemplate>
//                                            </div>
//                                        </FooterTemplate>
//                                    </asp:TemplateField>
//                                </Columns>
//                            </asp:GridView>