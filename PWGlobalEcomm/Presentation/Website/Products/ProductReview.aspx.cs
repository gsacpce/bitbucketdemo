﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Products_ProductReview : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptProductReview;
        protected global::System.Web.UI.WebControls.HiddenField hdnTotalReview, hdnPageStartRowNumber, hdnPageEndRowNumber, hdnAvgRating;
        List<ProductBE.ReviewRating> lstReviewRating;
        int ProductId;
        Int16 PageIndex;
        Int16 PageSize;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ProductId = Convert.ToInt32(Request.Form["ProductId"]);
                PageIndex = Convert.ToInt16(Request.Form["PageIndex"]);
                PageSize = Convert.ToInt16(Request.Form["PageSize"]);
                if (ProductId != 0)
                {
                    lstReviewRating = ProductBL.GetProductReviewByProductId<ProductBE.ReviewRating>(ProductId, PageIndex, PageSize);

                }
                if (lstReviewRating != null)
                {
                    if (lstReviewRating.Count > 0)
                    {
                        rptProductReview.DataSource = lstReviewRating;
                        rptProductReview.DataBind();
                        hdnTotalReview.Value = Convert.ToString(lstReviewRating[0].TotalReviewCount);
                        hdnPageStartRowNumber.Value = Convert.ToString(lstReviewRating[0].RowNumber);
                        hdnPageEndRowNumber.Value = Convert.ToString(lstReviewRating[lstReviewRating.Count - 1].RowNumber);
                        decimal decAvgRating = Math.Round(lstReviewRating[0].AvgRating);
                        hdnAvgRating.Value = Convert.ToString(decAvgRating);
                        

                    }
                    else
                    {
                        hdnTotalReview.Value = Convert.ToString(0);
                    }
                }
                else
                {
                    hdnTotalReview.Value = Convert.ToString(0);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptProductReview_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl spnRating1 = (HtmlGenericControl)e.Item.FindControl("spnRating1");
                    HtmlGenericControl spnRating2 = (HtmlGenericControl)e.Item.FindControl("spnRating2");
                    HtmlGenericControl spnRating3 = (HtmlGenericControl)e.Item.FindControl("spnRating3");
                    HtmlGenericControl spnRating4 = (HtmlGenericControl)e.Item.FindControl("spnRating4");
                    HtmlGenericControl spnRating5 = (HtmlGenericControl)e.Item.FindControl("spnRating5");
                    ProductBE.ReviewRating objReviewRating = (ProductBE.ReviewRating)e.Item.DataItem;
                    switch (Convert.ToInt32(objReviewRating.Rating))
                    {
                        case 5:
                            spnRating5.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            goto case 4;
                        case 4:
                            spnRating4.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            goto case 3;
                        case 3:
                            spnRating3.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            goto case 2;
                        case 2:
                            spnRating2.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            goto case 1;
                        case 1:
                            spnRating1.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}