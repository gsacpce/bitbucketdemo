﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.BusinessLogic;
using System.Web.Configuration;


namespace Presentation
{
    public partial class Products_SMSharedUrl : System.Web.UI.Page
    {
        bool isReferrerSM;
        Int16 languageId;
        Int16 currencyId;
        string currencySymbol;
        string productUrl;
        public static string host = GlobalFunctions.GetVirtualPath(); 

        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                //test string encryptedQueryString = "LgKBs/HOY/6FkqJTc0/84FmNB8K8pBU0Dx4ImJw9eDHPUt tgKYP8ASyVNuMQkieaqb tiWLqi WFYUVFPDLciuXB89xcV4lkKEfd3euQqFfEYzjpTgdg7h1TsZgICN1nIpityAScH/zLtRSKSp0pdsgldUtoqcqChDUm3h/ToSAk6PC7DRjOBWAZ65G0mv2hh0plGVaWPaNdAdLWSr0Ww==";//Request.QueryString["SMPurl"];
                string encryptedQueryString = Request.QueryString["SMPurl"].Replace("request=","");
                
                Exceptions.WriteInfoLog(" SMPUrl = " + encryptedQueryString);
                string decryptedQueryString = string.Empty;
                string cryptoKey = string.Empty;
                if (!string.IsNullOrEmpty(encryptedQueryString))
                {
                    // Decrypt query strings
                    decryptedQueryString = GlobalFunctions.DecryptQueryStrings(encryptedQueryString, GlobalFunctions.GetCryptoKey() );
                }
                string[] strArr = decryptedQueryString.Split('&');
                string[] KeyValue = null;

                Int16 SMLId = 0, SMCId = 0;
                Int32 SMPId = 0;
                String SMShareUrl = string.Empty , SMCSymbol = String.Empty ;

                for (int iArr = 0; iArr < strArr.Length; iArr++)
                {
                    KeyValue = strArr[iArr].Split('=');
                    if (strArr[iArr].IndexOf('=') > 0)
                    {

                        if (Convert.ToString(KeyValue[0]).ToUpper() == "SMLID")
                        {
                            SMLId = Convert.ToInt16(KeyValue[1]);
                        }
                        if (Convert.ToString(KeyValue[0]).ToUpper() == "SMCId")
                        {
                            SMCId = Convert.ToInt16(KeyValue[1]);
                        }
                        if (Convert.ToString(KeyValue[0]).ToUpper() == "SMCSYMBOL")
                        {
                            SMCSymbol = Convert.ToString(KeyValue[1]);
                        }
                        if (Convert.ToString(KeyValue[0]).ToUpper() == "SMPID")
                        {
                            SMPId = Convert.ToInt32(KeyValue[1]);
                        }
                        if (Convert.ToString(KeyValue[0]).ToUpper() == "SMSHAREURL")
                        {
                            SMShareUrl = Convert.ToString(KeyValue[1]);
                        }
                       
                    }
                }


                GlobalFunctions.SetCurrencyId(SMCId);
                GlobalFunctions.SetCurrencySymbol(SMCSymbol);
                GlobalFunctions.SetLanguageId(SMLId);

                //List<ProductBE> lstProduct = ProductBL.GetProductNameAllLanguages(ProductId);
                //Int16 LanguageId = Convert.ToInt16(Request.QueryString["SMLId"]) ;
                //string ProductName = lstProduct.FirstOrDefault(x => x.ProductId.Equals(LanguageId)).ProductName;

                Response.Redirect(SMShareUrl);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }    
            
        }
    }
}