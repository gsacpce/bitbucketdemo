﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using System.Text;

namespace Presentation
{
    public class Products_CompareProduct_ajax : System.Web.UI.Page
    {
        #region Variables

        protected global::System.Web.UI.WebControls.Repeater rptCategories;
        protected global::System.Web.UI.WebControls.Repeater rptColor;
        protected global::System.Web.UI.WebControls.Repeater rptSection;


        public string strHTML = string.Empty;
        StringBuilder sbHTML = new StringBuilder();
        public static string host = GlobalFunctions.GetVirtualPath();
        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        string strCurrencySymbol, strProductIds = string.Empty;
        public string strPrintText, strEmailText = string.Empty;
        string strPricing, strQuantity, strDescription, strAddToWishlist, strPrice = string.Empty;
        string strProductDetailURL, strSectionIcons = string.Empty;

        #endregion

        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        /*Sachin Chauhan End : 22 02 2016 */

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date : 22 02 2016
        /// Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            //During the initial page load, add the Anti-XSRF token and user
            //name to the ViewState
            if (!IsPostBack)
            {
                //Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

                //If a user name is assigned, set the user name
                //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                if (Session["ContextUserGUID"] == null)
                    Session["ContextuserGUID"] = Guid.NewGuid().ToString();

                ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
            }
            //During all subsequent post backs to the page, the token value from
            //the cookie should be validated against the token in the view state
            //form field. Additionally user name should be compared to the
            //authenticated users name
            else
            {
                //Validate the Anti-XSRF token
                //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                try
                {
                    if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())
                        Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                    Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed due to unhandled exception.");
                    //throw;
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
            #region "CSRF prevention code"
            try
            {
                //First, check for the existence of the Anti-XSS cookie
                var requestCookie = Request.Cookies[AntiXsrfTokenKey];
                Guid requestCookieGuidValue;

                //If the CSRF cookie is found, parse the token from the cookie.
                //Then, set the global page variable and view state user
                //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
                //method.
                if (requestCookie != null
                && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
                {
                    //Set the global token variable so the cookie value can be
                    //validated against the value in the view state form field in
                    //the Page.PreLoad method.
                    _antiXsrfTokenValue = requestCookie.Value;

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;
                }
                //If the CSRF cookie is not found, then this is a new session.
                else
                {
                    //Generate a new Anti-XSRF token
                    _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;

                    //Create the non-persistent CSRF cookie
                    var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                    {
                        //Set the HttpOnly property to prevent the cookie from
                        //being accessed by client side script
                        HttpOnly = true,

                        //Add the Anti-XSRF token to the cookie value
                        Value = _antiXsrfTokenValue
                    };

                    //If we are using SSL, the cookie should be set to secure to
                    //prevent it from being sent over HTTP connections
                    if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                        responseCookie.Secure = true;

                    //Add the CSRF cookie to the response
                    Response.Cookies.Set(responseCookie);
                }

                Page.PreLoad += Page_PreLoad;
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            #endregion
            /*Sachin Chauhan End : 22 02 2016*/
        }



        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 14-08-15
        /// Scope   : Page_Load of the CompareProduct_ajax page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            strProductIds = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["ProductIds"]));
            intLanguageId = Convert.ToInt16(Request.Form["languageid"]);
            intCurrencyId = Convert.ToInt16(Request.Form["currencyid"]);
            strCurrencySymbol = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["currencysymbol"]));

            BindResourceData();

            try
            {
                strProductIds = strProductIds.Replace("'", "");
                List<ProductBE> lstProductBE = new List<ProductBE>();
                ProductBL objProductBL = new ProductBL();
                lstProductBE = ProductBL.GetCompareProductsDetail(strProductIds, intLanguageId, intCurrencyId);

                if (lstProductBE != null && lstProductBE.Count > 0)
                {
                    // Bind basic product information
                    for (int i = 0; i < lstProductBE.Count; i++)
                    {
                        if (i == 0)
                            sbHTML.Append("<tr><td></td>");
                        if (lstProductBE.Count > 2)
                            sbHTML.Append("<td><div class='removeCompareBtn text-right noprint'><a id='aRemoveProduct' rel='" + lstProductBE[i].ProductId + "' class='compareClose glyphicon glyphicon-remove customClose'></a></div>");
                        else
                            sbHTML.Append("<td><div class='removeCompareBtn'></div>");

                        if (!string.IsNullOrEmpty(lstProductBE[i].CategoryName) && !string.IsNullOrEmpty(lstProductBE[i].SubCategoryName) && !string.IsNullOrEmpty(lstProductBE[i].SubSubCategoryName))
                        {
                            if (lstProductBE[i].CategoryName == lstProductBE[i].SubCategoryName && lstProductBE[i].CategoryName == lstProductBE[i].SubSubCategoryName)
                            {
                                strProductDetailURL = host + "details/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].ProductCode) + "_" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].ProductName);
                            }
                            else if (lstProductBE[i].CategoryName == lstProductBE[i].SubCategoryName && lstProductBE[i].SubCategoryName != lstProductBE[i].SubSubCategoryName)
                            {
                                strProductDetailURL = host + "details/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].SubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].ProductCode) + "_" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].ProductName);
                            }
                            else
                            {
                                strProductDetailURL = host + "details/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].SubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].SubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].ProductCode) + "_" + GlobalFunctions.EncodeCategoryURL(lstProductBE[i].ProductName);
                            }
                        }


                        sbHTML.Append("<div class='productData text-center'><a target='_blank' class='compareProductImage prod_list_image_outer' href='" + strProductDetailURL + "'>");

                        if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + lstProductBE[i].DefaultImageName))
                            sbHTML.Append("<img class='prod_list_image_inner' src='" + GlobalFunctions.GetThumbnailProductImagePath() + lstProductBE[i].DefaultImageName + "'");
                        else
                            sbHTML.Append("<img class='prod_list_image_inner' src='" + host + "Images/Products/default.jpg" + "' ");

                        sbHTML.Append("alt='" + lstProductBE[i].ProductName + "'></a>");

                        if (!string.IsNullOrEmpty(lstProductBE[i].SectionIds))
                        {
                            string strSectionIds = lstProductBE[i].SectionIds;
                            string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            string strSectionImageData = string.Empty;
                            string strSectionNames = string.Empty;
                            for (int l = 0; l < strSections.Length; l++)
                            {
                                string[] strSectionIcon = strSections[l].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                                {
                                    strSectionImageData += "<span><img title='" + strSectionIcon[1].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_swatch'></span>";
                                }
                                if (strSectionIcon.Length > 1)
                                    strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                            }
                            strSectionIcons = "<p>" + strSectionImageData + "</p>";
                            sbHTML.Append("<div class='sectionimagecontainer'>" + strSectionIcons + "</div></div>");
                        }

                        sbHTML.Append("<div class='productCode text-center'>#" + lstProductBE[i].ProductCode + "</div>");
                        sbHTML.Append("<div class='productName product_Name_Blk text-center'>" + lstProductBE[i].ProductName + "</div></td>");




                        if (i == lstProductBE.Count - 1)
                            sbHTML.Append("</tr>");
                    }



                    // Bind Pricing details
                    for (int i = 0; i < lstProductBE.Count; i++)
                    {
                        if (i == 0)
                            sbHTML.Append("<tr><td class='customTableHead'><div>" + strPricing + "</div></td>");

                        List<ProductBE.PriceBreaks> lstPriceBreaks = lstProductBE[0].PropGetAllPriceBreak.FindAll(x => x.ProductId == lstProductBE[i].ProductId).GroupBy(x => x.PriceBreakId).Select(x => x.First()).ToList();

                        if (lstPriceBreaks != null && lstPriceBreaks.Count > 0)
                        {
                            for (int j = 0; j < lstPriceBreaks.Count; j++)
                            {
                                if (j == 0)
                                { sbHTML.Append("<td><div class='table-responsive'><table class='table customTable'>"); }

                                List<ProductBE.PriceBreakDetails> lstPriceBreakDetails = lstProductBE[0].PropGetAllPriceBreakDetails.FindAll(x => x.PriceBreakId == lstPriceBreaks[j].PriceBreakId);
                                if (lstPriceBreakDetails.Count > 1)
                                {

                                    sbHTML.Append("<tr  class='hidden'><td class='pricebreakname pageText' colspan='2'>" + lstPriceBreaks[j].PriceBreakName + "</td></tr>");
                                    if (!string.IsNullOrEmpty(strQuantity) && !string.IsNullOrEmpty(strPrice))
                                    {
                                        sbHTML.Append("<tr><th class='customTableHead'>" + strQuantity + "</th><th class='customTableHead'>" + strPrice + "</th></tr>");
                                    }

                                    for (int k = 0; k < lstPriceBreakDetails.Count; k++)
                                    {
                                        sbHTML.Append("<tr><td class='customTableText'>" + lstPriceBreakDetails[k].BreakFrom);


                                        if (lstPriceBreaks[j].EnableStrikePrice == true && lstPriceBreaks[j].ExpirationDate >= DateTime.Now.Date)
                                        {
                                            sbHTML.Append("</td></td><td class='customTableText'><div class='grid prod_list_text_price productPrice'><div class='price_was'> " + strCurrencySymbol + lstPriceBreakDetails[k].Price + "</div>");
                                            sbHTML.Append("<div class='price_now'> " + strCurrencySymbol + " " + lstPriceBreakDetails[k].StrikePrice+"</div></div>");
                                        }
                                        else
                                        {
                                            sbHTML.Append("</td></td><td class='customTableText'>" + strCurrencySymbol + lstPriceBreakDetails[k].Price);
                                        }
                                        sbHTML.Append("</td></tr>");

                                    }
                                }
                                else
                                {
                                    sbHTML.Append("<tr class='hidden'><td class='pricebreakname pageText' colspan='2'>" + lstPriceBreaks[j].PriceBreakName + "</td></tr>");
                                  

                                    if (lstPriceBreaks[j].EnableStrikePrice == true && lstPriceBreaks[j].ExpirationDate >= DateTime.Now.Date)
                                    {
                                        sbHTML.Append("</td></td><td class=''><p><strike>" + strCurrencySymbol + lstPriceBreakDetails[0].Price + "</strike></p>");
                                        sbHTML.Append(  strCurrencySymbol + " " + lstPriceBreakDetails[0].StrikePrice );
                                    }
                                    else
                                    {

                                        sbHTML.Append("</td></td><td class=''>" + strCurrencySymbol + lstPriceBreakDetails[0].Price);
                                    }

                                    sbHTML.Append("</td></tr>");
                                }
                                if (j == lstPriceBreaks.Count - 1)
                                    sbHTML.Append("</table></div></td>");
                            }
                        }
                        sbHTML.Append("");


                        if (i == lstProductBE.Count - 1)
                            sbHTML.Append("</tr>");
                    }

                    // Bind basic product information
                    for (int i = 0; i < lstProductBE.Count; i++)
                    {
                        if (i == 0)
                            sbHTML.Append("<tr><td class='customTableHead'>" + strDescription + "</td>");
                        sbHTML.Append("<td class='customTableText'>" + GlobalFunctions.RemoveHtmlTags(lstProductBE[i].ProductDescription) + "</td>");

                        if (i == lstProductBE.Count - 1)
                            sbHTML.Append("</tr>");
                    }

                    //Bind Product details
                    List<ProductBE.ProductDetailGroupMappings> lstProductDetailGroupMappings = lstProductBE[0].PropGetAllProductDetailGroupMappings.GroupBy(x => x.ProductDetailGroupMappingId).Select(x => x.First()).ToList();

                    for (int j = 0; j < lstProductDetailGroupMappings.Count; j++)
                    {
                        for (int i = 0; i < lstProductBE.Count; i++)
                        {
                            if (i == 0)
                                sbHTML.Append("<tr><td class='customTableHead'>" + lstProductDetailGroupMappings[j].DetailTitle + "</td>");
                            sbHTML.Append("<td class='customTableText'>" + lstProductBE[0].PropGetAllProductDetails.FirstOrDefault(x => x.ProductDetailGroupMappingId == lstProductDetailGroupMappings[j].ProductDetailGroupMappingId && x.ProductId == lstProductBE[i].ProductId).DetailDescription.Replace("|", ", ") + "</td>");

                            if (i == lstProductBE.Count - 1)
                                sbHTML.Append("</tr>");
                        }
                    }

                    //Add addToWishlit buttons
                    //for (int i = 0; i < lstProductBE.Count; i++)
                    //{
                    //    if (i == 0)
                    //        sbHTML.Append("<tr class='noprint'><td></td>");
                    //    sbHTML.Append("<td><input type='button' rel='" + lstProductBE[i].ProductId + "' value='" + strAddToWishlist + "' class='btn btn-sm btn-primary list_button customActionBtn'></td>");

                    //    if (i == lstProductBE.Count - 1)
                    //        sbHTML.Append("</tr>");
                    //}
                    strHTML = sbHTML.ToString();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }



        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 27-08-15
        /// Scope   : BindResourceData of the ProductComparision page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strPrintText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Print").ResourceValue;
                        strEmailText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Email").ResourceValue;
                        strPricing = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Price_Label").ResourceValue;
                        strQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Quantity_Label").ResourceValue;
                        strDescription = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Description_Label").ResourceValue;
                        strAddToWishlist = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_AddToWishList").ResourceValue;
                        strPrice = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Price_Title_Text").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}