﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using System.IO;

namespace Presentation
{
    public partial class Products_Products_CustomRequest : BasePage
    {
        #region UI Controls
        protected global::System.Web.UI.WebControls.Label lblContactName, lblEmail, lblPhoneNumber, lblEvent, lblTargetAud, lblIdeas, lblQty, lblBudget, lblColour, lblBranding, lblDelAdd, lblDelDate, lblAddCommnets;
        protected global::System.Web.UI.WebControls.TextBox txtContactName, txtEmail, txtPhoneNumber, txtEvent, txtTargetAud, txtIdeas, txtQty, txtBudget, txtColour, txtBranding, txtDelDate;
        protected global::System.Web.UI.HtmlControls.HtmlTextArea txtareaDelAdd, txtareaAddCommnets;
        protected global::System.Web.UI.WebControls.Literal ltrTitle, ltrDetailPara1, ltrDetailPara2, ltrDetailPara3, ltrDetailPara4;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvContactName, rfvEmail, rfvPhoneNumber, rfvEvent, rfvTargetAud, rfvIdeas, rfvQty, rfvBudget, rfvColour, rfvBranding, rfvDelDate; // Added by SHARIGANESH SINGH 27 March 2016
        protected global::System.Web.UI.WebControls.Button btnSubmit;
        #endregion

        public static string host = GlobalFunctions.GetVirtualPath();
        ProductBE objProductBE;
        StoreBE objStoreBE;
        UserBE objUserBE;
        Int16 intLanguageId;
        string strSuccessMsg, strFailureMsg;
        protected void Page_Load(object sender, EventArgs e)
        {
            intLanguageId = GlobalFunctions.GetLanguageId();
            objStoreBE = StoreBL.GetStoreDetails();
            //if (Session["StoreUser"] != null)//Commented by Sripal
            if (Session["User"] != null)//Added User frontEnd session
            {
                objUserBE = new UserBE();
                objUserBE = Session["User"] as UserBE;
                //txtEmail.Text = objUserBE.EmailId;
                txtEmail.Text = HttpUtility.HtmlDecode(objUserBE.EmailId); // vikram for apostrophy
            }
            BindResourceData();
            ReadMetaTagsData();
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 05/11/2015
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        lblContactName.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_ContactName").ResourceValue;
                        lblEmail.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Email").ResourceValue;
                        lblPhoneNumber.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_PhoneNumber").ResourceValue;
                        lblEvent.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Event").ResourceValue;
                        lblTargetAud.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_TargetAudience").ResourceValue;
                        lblIdeas.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Ideas").ResourceValue;
                        lblQty.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        lblBudget.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Budget").ResourceValue; ;
                        lblColour.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Colour").ResourceValue; ;
                        lblBranding.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Branding").ResourceValue;
                        lblDelAdd.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_DeliveryAddress").ResourceValue;
                        lblDelDate.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_DeliveryDate").ResourceValue;
                        lblAddCommnets.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_AdditionalComments").ResourceValue;
                        strSuccessMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_SuccessMsg").ResourceValue;
                        strFailureMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_FailureMsg").ResourceValue;
                        ltrTitle.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Title").ResourceValue;
                        ltrDetailPara1.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_DetailPara1").ResourceValue;
                        ltrDetailPara2.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_DetailPara2").ResourceValue;
                        ltrDetailPara3.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_DetailPara3").ResourceValue;
                        ltrDetailPara4.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_DetailPara4").ResourceValue;

                        #region Required Field Validators added by SHRIGANESH SINGH 27 March 2016

                        rfvContactName.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblContactName.Text;
                        rfvEmail.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblEmail.Text;
                        rfvPhoneNumber.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblPhoneNumber.Text;
                        rfvEvent.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblEvent.Text;
                        rfvTargetAud.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblTargetAud.Text;
                        rfvIdeas.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblIdeas.Text;
                        rfvQty.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblQty.Text;
                        rfvBudget.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblBudget.Text;
                        rfvColour.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblColour.Text;
                        rfvBranding.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblBranding.Text;
                        rfvDelDate.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lblDelDate.Text;
                        #endregion
                        btnSubmit.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Submit_Text").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (Page.IsValid)
            {
                try
                {
                    objProductBE = new ProductBE();
                    objProductBE.PropCustomRequest.ContactName = Sanitizer.GetSafeHtmlFragment(txtContactName.Text);
                    objProductBE.PropCustomRequest.EmailId = HttpUtility.HtmlEncode(txtEmail.Text);
                    objProductBE.PropCustomRequest.PhoneNumber = Sanitizer.GetSafeHtmlFragment(txtPhoneNumber.Text);
                    objProductBE.PropCustomRequest.Event = Sanitizer.GetSafeHtmlFragment(txtEvent.Text);
                    objProductBE.PropCustomRequest.TargetAudience = Sanitizer.GetSafeHtmlFragment(txtTargetAud.Text);
                    objProductBE.PropCustomRequest.Ideas = Sanitizer.GetSafeHtmlFragment(txtIdeas.Text);
                    objProductBE.PropCustomRequest.Quantity = Sanitizer.GetSafeHtmlFragment(txtQty.Text);
                    objProductBE.PropCustomRequest.Budget = Sanitizer.GetSafeHtmlFragment(txtBudget.Text);
                    objProductBE.PropCustomRequest.Colour = Sanitizer.GetSafeHtmlFragment(txtColour.Text);
                    objProductBE.PropCustomRequest.Branding = Sanitizer.GetSafeHtmlFragment(txtBranding.Text);
                    objProductBE.PropCustomRequest.DeliveryAddress = Sanitizer.GetSafeHtmlFragment(txtareaDelAdd.InnerText);
                    string dt = Sanitizer.GetSafeHtmlFragment(txtDelDate.Text);
                    dt = string.IsNullOrEmpty(txtDelDate.Text) ? null : Convert.ToDateTime(dt).ToString("yyyy-MM-dd");
                    objProductBE.PropCustomRequest.DeliveryDate = Convert.ToDateTime(dt);
                    objProductBE.PropCustomRequest.AdditionalComments = Sanitizer.GetSafeHtmlFragment(txtareaAddCommnets.InnerText);
                    result = ProductBL.InsertProductCustomRequest(objProductBE);
                    objProductBE.PropCustomRequest.EmailId = HttpUtility.HtmlDecode(txtEmail.Text); // vikram for apostrophy
                    SendProductEnquiryEmail(objProductBE);
                    if (result)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strSuccessMsg, AlertType.Success);
                        ResetControls();
                    }
                    else
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strFailureMsg, AlertType.Failure);
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
                finally
                {
                    objProductBE = null;
                }
            }
        }

        private void ResetControls()
        {
            try
            {
                txtContactName.Text = "";
                txtEmail.Text = "";
                txtPhoneNumber.Text = "";
                txtEvent.Text = "";
                txtTargetAud.Text = "";
                txtIdeas.Text = "";
                txtQty.Text = "";
                txtBudget.Text = "";
                txtColour.Text = "";
                txtBranding.Text = "";
                txtareaDelAdd.InnerText = "";
                txtDelDate.Text = "";
                txtareaAddCommnets.InnerText = "";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void SendProductEnquiryEmail(ProductBE objProductBE)
        {
            try
            {
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = "Product Custom Request";
                objEmail.LanguageId = intLanguageId;
                objEmail.EmailTemplateId = 0;

                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    string strFromEmailId = lstEmail[0].FromEmailId;
                    string strCCEmailId = lstEmail[0].CCEmailId;
                    string strBCCEmailId = lstEmail[0].BCCEmailId;
                    bool IsHtml = lstEmail[0].IsHtml;
                    string strSubject = lstEmail[0].Subject;
                    string strMailBody = GenerateMailBody(objProductBE, lstEmail[0].Body);
                    string strAttachmentPath = null;

                    //Response.Clear();
                    //Response.Write(strMailBody);
                    //Response.End();
                    //objProductBE.PropProductEnquiry.EmailAddress
                    GlobalFunctions.SendEmail(objProductBE.PropCustomRequest.EmailId, strFromEmailId, "", strCCEmailId, strBCCEmailId, strSubject, strMailBody, strAttachmentPath, "", IsHtml);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private string GenerateMailBody(ProductBE objProductBE, string strMailBody)
        {
            try
            {

                strMailBody = strMailBody.Replace("@ContactName", objProductBE.PropCustomRequest.ContactName);
                strMailBody = strMailBody.Replace("@EmailId", objProductBE.PropCustomRequest.EmailId);
                strMailBody = strMailBody.Replace("@PhoneNumber", objProductBE.PropCustomRequest.PhoneNumber);
                strMailBody = strMailBody.Replace("@Event", objProductBE.PropCustomRequest.Event);
                strMailBody = strMailBody.Replace("@TargetAudience", objProductBE.PropCustomRequest.TargetAudience);
                strMailBody = strMailBody.Replace("@Ideas", objProductBE.PropCustomRequest.Ideas);
                strMailBody = strMailBody.Replace("@Quantity", objProductBE.PropCustomRequest.Quantity);
                strMailBody = strMailBody.Replace("@Budget", objProductBE.PropCustomRequest.Budget);
                strMailBody = strMailBody.Replace("@Colour", objProductBE.PropCustomRequest.Colour);
                strMailBody = strMailBody.Replace("@Branding", objProductBE.PropCustomRequest.Branding);
                strMailBody = strMailBody.Replace("@DeliveryAddress", objProductBE.PropCustomRequest.DeliveryAddress);
                strMailBody = strMailBody.Replace("@DeliveryDate", objProductBE.PropCustomRequest.DeliveryDate.ToString());
                strMailBody = strMailBody.Replace("@AdditionalComments", objProductBE.PropCustomRequest.AdditionalComments);
                //Get logo
                string strSiteLogoDir = string.Empty;
                DirectoryInfo dirSiteLogo = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Images/SiteLogo"));
                FileInfo[] logoFileInfo = dirSiteLogo.GetFiles();

                foreach (FileInfo logoFile in logoFileInfo)
                {
                    if (logoFile.Name.ToLower().Contains("sitelogo"))
                    {
                        strSiteLogoDir = "<a href='" + host + "index'><img width='100%;' height='auto' src='" + host + "Images/SiteLogo/" + logoFile.Name + "'></a>";
                    }
                }
                strMailBody = strMailBody.Replace("@sitelogo", strSiteLogoDir);
                strMailBody = strMailBody.Replace("@SiteName", objStoreBE.StoreName);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return strMailBody;
            }
            return strMailBody;
        }


        private void ReadMetaTagsData()
        {
            try
            {
                StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
                List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
                MetaTags.Action = Convert.ToInt16(DBAction.Select);
                lstMetaTags = StoreBL.GetListMetaTagContents(MetaTags);

                if (lstMetaTags != null)
                {
                    lstMetaTags = lstMetaTags.FindAll(x => x.PageName == "Custom Request");
                    if (lstMetaTags.Count > 0)
                    {
                        Page.Title = lstMetaTags[0].MetaContentTitle;
                        Page.MetaKeywords = lstMetaTags[0].MetaKeyword;
                        Page.MetaDescription = lstMetaTags[0].MetaDescription;
                    }
                    else
                    {
                        Page.Title = "";
                        Page.MetaKeywords = "";
                        Page.MetaDescription = "";
                    }
                }
                else
                {
                    Page.Title = "";
                    Page.MetaKeywords = "";
                    Page.MetaDescription = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}