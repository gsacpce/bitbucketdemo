﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Products_Products_YouMayAlsoLike : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptProducts1, rptProducts2, rptProducts3;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liRelatedProduct, liRelatedPrice, liRelatedCategory, tab1, tab2, tab3;
        protected global::System.Web.UI.WebControls.Literal ltrRelatedProduct, ltrRelatedPrice, ltrRelatedCategory;
        public static string host = GlobalFunctions.GetVirtualPath();
        public static ProductBE objProductBE;
        public static int ProductId;
        public static StoreBE objStoreBE;
        public bool blnShowProduct, blnShowPrice, blnShowCategory = false, blnShowSectionsIcons = false;
        Int16 intLanguageId;
        Int16 intCurrencyId;
        string strCurrencySymbol, stror;
        int intPricePercentage = 1;
        Int16 iUserTypeID = 1;/*User Type*/
        public static YouMayAlsoLikeMappingBE objYouMayAlsoLikeMappingBE;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                BindResourceData();
                ProductId = Convert.ToInt32(Request.Form["ProductId"]);
                objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "pd_enableyoumayalsolike").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "related by products").IsEnabled)
                        blnShowProduct = true;
                    #region by price
                    FeatureBE PL_DisplaytypeFeature = objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName.ToLower() == "pd_enableyoumayalsolike");

                    if (PL_DisplaytypeFeature != null)
                    {
                        FeatureBE.FeatureValueBE FValue = PL_DisplaytypeFeature.FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "related by price");

                        if (FValue.IsEnabled)
                        {
                            blnShowPrice = true;
                            if (FValue.FeatureDefaultValue != "")
                                intPricePercentage = Convert.ToInt16(FValue.FeatureDefaultValue);
                        }
                    }
                    //if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "pd_enableyoumayalsolike").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "related by price").IsEnabled)
                    //{
                    //    blnShowPrice = true;
                    //    intPricePercentage = Convert.ToInt16(objStoreBE.StoreFeatures[0].FeatureValues[0].FeatureDefaultValue);
                    //}
                    #endregion

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "pd_enableyoumayalsolike").FeatureValues.FirstOrDefault(s => s.FeatureValue.ToLower() == "related by category").IsEnabled)
                        blnShowCategory = true;
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableSectionIcons").FeatureValues[0].IsEnabled)
                    { blnShowSectionsIcons = true; }
                }

                if (!IsPostBack)
                {



                    if (blnShowProduct)
                    {
                        liRelatedProduct.Visible = true;
                        //BindYouMayAlsoLikeProducts("product");
                    }
                    if (blnShowPrice)
                    {
                        liRelatedPrice.Visible = true;
                        //BindYouMayAlsoLikeProducts("price");
                    }
                    if (blnShowCategory)
                    {
                        liRelatedCategory.Visible = true;
                        //BindYouMayAlsoLikeProducts("category");
                    }
                    BindYoumaylike();
                }

                ShowDefaultActiveTab();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void BindYoumaylike()
        {
            string strSearchProductIds = "";
            try
            {
                if (Session["User"] != null)
                {
                    UserBE lstUser = new UserBE();
                    lstUser = Session["User"] as UserBE;
                    iUserTypeID = lstUser.UserTypeID;
                }
                objYouMayAlsoLikeMappingBE = ProductBL.Getproductlike(Constants.USP_Getyoumaylikeproductdetail, ProductId, intLanguageId, intCurrencyId, iUserTypeID, 10,intPricePercentage, true);

                if (objYouMayAlsoLikeMappingBE != null)
                {

                    #region PRODUCT
                    if (objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct != null)
                    {
                        if (objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct.Count > 0)
                        {
                            foreach (YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct objPBE in objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct)
                            {
                                strSearchProductIds += objPBE.ProductId.ToString() + ",";
                            }
                            strSearchProductIds = strSearchProductIds.TrimEnd(',');
                            List<ProductBE.ProductColorVariantMappping> objProductColorVariantMapppingLst = new List<ProductBE.ProductColorVariantMappping>();
                            objProductColorVariantMapppingLst = ProductBL.getProductIDwithVariant<ProductBE.ProductColorVariantMappping>(strSearchProductIds);
                            if (objProductColorVariantMapppingLst != null)
                            {
                                foreach (ProductBE.ProductColorVariantMappping objBE in objProductColorVariantMapppingLst)
                                {
                                    if (objBE.ProductId != objBE.MappedProductId)
                                    {
                                        objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct.FirstOrDefault(x => x.ProductId == objBE.MappedProductId);
                                        objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct.Remove(objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct.FirstOrDefault(x => x.ProductId == objBE.ProductId));
                                    }
                                }
                            }
                            rptProducts1.DataSource = objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct.Distinct().ToList().Count() > 4 ? objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct.Distinct().Take(4).ToList() : objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct.Distinct().ToList();
                            rptProducts1.DataBind();
                        }
                        else
                        { liRelatedProduct.Visible = false; }
                    }
                    else { liRelatedProduct.Visible = false; }

                    #endregion

                    #region "Price"
                    if (objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice != null)
                    {
                        if (objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice.Count > 0)
                        {
                            foreach (YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice objPBE in objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice)
                            {
                                strSearchProductIds += objPBE.ProductId.ToString() + ",";
                            }
                            strSearchProductIds = strSearchProductIds.TrimEnd(',');
                            List<ProductBE.ProductColorVariantMappping> objProductColorVariantMapppingLst = new List<ProductBE.ProductColorVariantMappping>();
                            objProductColorVariantMapppingLst = ProductBL.getProductIDwithVariant<ProductBE.ProductColorVariantMappping>(strSearchProductIds);
                            if (objProductColorVariantMapppingLst != null)
                            {
                                foreach (ProductBE.ProductColorVariantMappping objBE in objProductColorVariantMapppingLst)
                                {
                                    if (objBE.ProductId != objBE.MappedProductId)
                                    {
                                        objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice.FirstOrDefault(x => x.ProductId == objBE.MappedProductId);
                                        objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice.Remove(objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice.FirstOrDefault(x => x.ProductId == objBE.ProductId));
                                    }
                                }
                            }

                            rptProducts2.DataSource = objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice.Distinct().ToList().Count() > 4 ? objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice.Distinct().Take(4).ToList() : objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice.Distinct().ToList();
                            rptProducts2.DataBind();
                        }
                        else
                        { liRelatedPrice.Visible = false; }
                    }
                    else { liRelatedPrice.Visible = false; }

                    #endregion

                    #region "Category"
                    if (objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory != null)
                    {
                        if (objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory.Count > 0)
                        {
                            foreach (YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory objPBE in objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory)
                            {
                                strSearchProductIds += objPBE.ProductId.ToString() + ",";
                            }
                            strSearchProductIds = strSearchProductIds.TrimEnd(',');
                            List<ProductBE.ProductColorVariantMappping> objProductColorVariantMapppingLst = new List<ProductBE.ProductColorVariantMappping>();
                            objProductColorVariantMapppingLst = ProductBL.getProductIDwithVariant<ProductBE.ProductColorVariantMappping>(strSearchProductIds);
                            if (objProductColorVariantMapppingLst != null)
                            {
                                foreach (ProductBE.ProductColorVariantMappping objBE in objProductColorVariantMapppingLst)
                                {
                                    if (objBE.ProductId != objBE.MappedProductId)
                                    {
                                        objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory.FirstOrDefault(x => x.ProductId == objBE.MappedProductId);
                                        objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory.Remove(objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory.FirstOrDefault(x => x.ProductId == objBE.ProductId));
                                    }
                                }
                            }

                            rptProducts3.DataSource = objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory.Distinct().ToList().Count() > 4 ? objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory.Distinct().Take(4).ToList() : objYouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory.Distinct().ToList();
                            rptProducts3.DataBind();
                        }
                        else
                        { liRelatedCategory.Visible = false; }
                    }
                    else { liRelatedCategory.Visible = false; }

                    #endregion

                }



            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private void ShowDefaultActiveTab()
        {
            try
            {
                if (liRelatedProduct.Visible)
                {
                    liRelatedProduct.Attributes.Add("class", "active");
                    liRelatedPrice.Attributes.Remove("class");
                    liRelatedCategory.Attributes.Remove("class");
                    tab1.Attributes.Add("class", "tab-pane in active");
                    tab2.Attributes.Add("class", "tab-pane fade");
                    tab3.Attributes.Add("class", "tab-pane fade");
                }
                else if (liRelatedPrice.Visible)
                {
                    liRelatedPrice.Attributes.Add("class", "active");
                    liRelatedProduct.Attributes.Remove("class");
                    liRelatedCategory.Attributes.Remove("class");
                    tab2.Attributes.Add("class", "tab-pane in active");
                    tab1.Attributes.Add("class", "tab-pane fade ");
                    tab3.Attributes.Add("class", "tab-pane fade");
                }
                else if (liRelatedCategory.Visible)
                {
                    liRelatedCategory.Attributes.Add("class", "active");
                    liRelatedProduct.Attributes.Remove("class");
                    liRelatedPrice.Attributes.Remove("class");
                    tab3.Attributes.Add("class", "tab-pane in active");
                    tab1.Attributes.Add("class", "tab-pane fade");
                    tab2.Attributes.Add("class", "tab-pane fade");
                }
                #region "Comment"
                //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "$('.nav-tabs').parent().children(':first').children(':first').next().show();", true);

                //if (liRelatedProduct.Visible == false)
                //{
                //    liRelatedPrice.Attributes.Add("class", "active");

                //    tab2.Attributes.Add("class", "tab-pane fade in active");
                //}
                //else if (liRelatedPrice.Visible == false)
                //{
                //    liRelatedCategory.Attributes.Add("class", "active");
                //    tab3.Attributes.Add("class", "tab-pane fade in active");
                //}
                //else
                //{
                //    liRelatedProduct.Attributes.Add("class", "active");
                //    tab1.Attributes.Add("class", "tab-pane fade in active");
                //} 
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindYouMayAlsoLikeProducts(string strRelatedBy)
        {

            objProductBE = new ProductBE();
            string strSearchProductIds = "";
            try
            {
                if (Session["User"] != null)
                {
                    UserBE lstUser = new UserBE();
                    lstUser = Session["User"] as UserBE;
                    iUserTypeID = lstUser.UserTypeID;
                }

                switch (strRelatedBy.ToLower())
                {
                    case "product":
                        #region "Product"
                        //objProductBE = ProductBL.GetProductDetails(ProductId, intLanguageId, intCurrencyId, 50, strRelatedBy, 0);
                        objProductBE = ProductBL.GetProductDetails(ProductId, intLanguageId, intCurrencyId, iUserTypeID, 50, strRelatedBy, 0);/*User Type*/
                        if (objProductBE != null)
                        {
                            if (objProductBE.PropGetAllYouMayAlsoLikeProducts.Count > 0)
                            {
                                foreach (ProductBE.YouMayAlsoLike objPBE in objProductBE.PropGetAllYouMayAlsoLikeProducts)
                                {
                                    strSearchProductIds += objPBE.ProductId.ToString() + ",";
                                }
                                strSearchProductIds = strSearchProductIds.TrimEnd(',');
                                List<ProductBE.ProductColorVariantMappping> objProductColorVariantMapppingLst = new List<ProductBE.ProductColorVariantMappping>();
                                //List<ProductBE.ProductSKU> objProductSKU = new List<ProductBE.ProductSKU>();
                                //objProductSKU = objProductBE.PropGetAllProductSKU;
                                objProductColorVariantMapppingLst = ProductBL.getProductIDwithVariant<ProductBE.ProductColorVariantMappping>(strSearchProductIds);
                                if (objProductColorVariantMapppingLst != null)
                                {
                                    foreach (ProductBE.ProductColorVariantMappping objBE in objProductColorVariantMapppingLst)
                                    {
                                        if (objBE.ProductId != objBE.MappedProductId)
                                        {
                                            objProductBE.PropGetAllYouMayAlsoLikeProducts.FirstOrDefault(x => x.ProductId == objBE.MappedProductId);
                                            objProductBE.PropGetAllYouMayAlsoLikeProducts.Remove(objProductBE.PropGetAllYouMayAlsoLikeProducts.FirstOrDefault(x => x.ProductId == objBE.ProductId));
                                        }
                                    }
                                }
                                rptProducts1.DataSource = objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().ToList().Count() > 4 ? objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().Take(4).ToList() : objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().ToList();
                                rptProducts1.DataBind();
                            }
                            else
                                liRelatedProduct.Visible = false;
                        }
                        else
                            liRelatedProduct.Visible = false;
                        break;

                        #endregion
                    case "price":
                        #region "Price"
                        //objProductBE = ProductBL.GetProductDetails(ProductId, intLanguageId, intCurrencyId, 50, strRelatedBy, intPricePercentage);
                        objProductBE = ProductBL.GetProductDetails(ProductId, intLanguageId, intCurrencyId, iUserTypeID, 50, strRelatedBy, intPricePercentage);/*User Type*/
                        if (objProductBE != null)
                        {
                            if (objProductBE.PropGetAllYouMayAlsoLikeProducts.Count > 0)
                            {
                                foreach (ProductBE.YouMayAlsoLike objPBE in objProductBE.PropGetAllYouMayAlsoLikeProducts)
                                {
                                    strSearchProductIds += objPBE.ProductId.ToString() + ",";
                                }
                                strSearchProductIds = strSearchProductIds.TrimEnd(',');
                                List<ProductBE.ProductColorVariantMappping> objProductColorVariantMapppingLst = new List<ProductBE.ProductColorVariantMappping>();
                                objProductColorVariantMapppingLst = ProductBL.getProductIDwithVariant<ProductBE.ProductColorVariantMappping>(strSearchProductIds);
                                if (objProductColorVariantMapppingLst != null)
                                {
                                    foreach (ProductBE.ProductColorVariantMappping objBE in objProductColorVariantMapppingLst)
                                    {
                                        if (objBE.ProductId != objBE.MappedProductId)
                                        {
                                            objProductBE.PropGetAllYouMayAlsoLikeProducts.FirstOrDefault(x => x.ProductId == objBE.MappedProductId);
                                            objProductBE.PropGetAllYouMayAlsoLikeProducts.Remove(objProductBE.PropGetAllYouMayAlsoLikeProducts.FirstOrDefault(x => x.ProductId == objBE.ProductId));
                                        }
                                    }
                                }

                                rptProducts2.DataSource = objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().ToList().Count() > 4 ? objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().Take(4).ToList() : objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().ToList();
                                rptProducts2.DataBind();
                            }
                            else
                                liRelatedPrice.Visible = false;
                        }
                        else
                            liRelatedPrice.Visible = false;
                        break;
                        #endregion
                    case "category":
                        #region "Category"
                        //objProductBE = ProductBL.GetProductDetails(ProductId, intLanguageId, intCurrencyId, 50, strRelatedBy, 0);
                        objProductBE = ProductBL.GetProductDetails(ProductId, intLanguageId, intCurrencyId, iUserTypeID, 50, strRelatedBy, 0);/*User Type*/
                        if (objProductBE != null)
                        {
                            if (objProductBE.PropGetAllYouMayAlsoLikeProducts.Count > 0)
                            {
                                foreach (ProductBE.YouMayAlsoLike objPBE in objProductBE.PropGetAllYouMayAlsoLikeProducts)
                                {
                                    strSearchProductIds += objPBE.ProductId.ToString() + ",";
                                }
                                strSearchProductIds = strSearchProductIds.TrimEnd(',');
                                List<ProductBE.ProductColorVariantMappping> objProductColorVariantMapppingLst = new List<ProductBE.ProductColorVariantMappping>();
                                objProductColorVariantMapppingLst = ProductBL.getProductIDwithVariant<ProductBE.ProductColorVariantMappping>(strSearchProductIds);
                                if (objProductColorVariantMapppingLst != null)
                                {
                                    foreach (ProductBE.ProductColorVariantMappping objBE in objProductColorVariantMapppingLst)
                                    {
                                        if (objBE.ProductId != objBE.MappedProductId)
                                        {
                                            objProductBE.PropGetAllYouMayAlsoLikeProducts.FirstOrDefault(x => x.ProductId == objBE.MappedProductId);
                                            objProductBE.PropGetAllYouMayAlsoLikeProducts.Remove(objProductBE.PropGetAllYouMayAlsoLikeProducts.FirstOrDefault(x => x.ProductId == objBE.ProductId));
                                        }
                                    }
                                }

                                //     objProductBE.PropGetAllYouMayAlsoLikeProduct.Select(e => new { e.empLoc, e.empPL, e.empShift })
                                //.Distinct();
                                //var distinctNames = (from d in objProductBE.PropGetAllYouMayAlsoLikeProducts select d).Distinct();
                                //rptProducts3.DataSource =objProductBE.PropGetAllYouMayAlsoLikeProducts.Select(x=> new {x.ProductId,x.ProductName,x.ProductCode).Distinct();//
                                rptProducts3.DataSource = objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().ToList().Count() > 4 ? objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().Take(4).ToList() : objProductBE.PropGetAllYouMayAlsoLikeProducts.Distinct().ToList();
                                rptProducts3.DataBind();
                            }
                            else
                                liRelatedCategory.Visible = false;
                        }
                        else
                            liRelatedCategory.Visible = false;
                        break;
                        #endregion
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 20-10-16
        /// Scope   : rptProducts_ItemDataBound of the repeater 
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlAnchor lnkProductImage = (HtmlAnchor)e.Item.FindControl("lnkProductImage");
                    HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
                    Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
                    HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
                    HtmlGenericControl spnstrike = (HtmlGenericControl)e.Item.FindControl("spnstrike");
                    HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
                    HtmlGenericControl spnPrice1 = (HtmlGenericControl)e.Item.FindControl("spnPrice1");
                    HtmlGenericControl spnor = (HtmlGenericControl)e.Item.FindControl("spnor");
                    string strCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).CategoryName);
                    string strSubCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).SubCategoryName);
                    string strSubSubCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).SubSubCategoryName);
                    string strProductName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).ProductName);
                    if (!string.IsNullOrEmpty(((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).DefaultImageName))
                    {
                        if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).DefaultImageName).Trim()))
                        {
                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + ((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).DefaultImageName.Trim();
                            imgProduct.Alt = ((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).ProductName.Trim();
                            imgProduct.Attributes.Add("title", ((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).ProductName.Trim());
                        }
                        else
                        {
                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                            imgProduct.Alt = "Default";
                            imgProduct.Attributes.Add("title", "Default");
                        }
                    }
                    else
                    {
                        imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                        imgProduct.Alt = "Default";
                        imgProduct.Attributes.Add("title", "Default");
                    }

                    aProductName.InnerHtml = strProductName;
                    //spnPrice.InnerHtml = strCurrencySymbol + (((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice).ToString();



                    #region Code for strike price

                    //string minprice = Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice);
                    //string minstrikeprice =Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinstrikePrice);
                    double dMinPrice = Convert.ToDouble(((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).MinPrice);
                    double dminstrikeprice = Convert.ToDouble(((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)e.Item.DataItem).MinstrikePrice);
                    string minprice = dMinPrice.ToString("##,###,##0.#0");
                    string minstrikeprice = dminstrikeprice.ToString("##,###,##0.#0");

                    if (!string.IsNullOrEmpty(minstrikeprice) && dminstrikeprice > 0)//minstrikeprice != "0" )
                    {
                        //spnstrike.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinstrikePrice), strCurrencySymbol, intLanguageId);
                        spnstrike.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minstrikeprice, strCurrencySymbol, intLanguageId);
                        if (!string.IsNullOrEmpty(minprice) && dMinPrice > 0)//minprice != "0")
                        {
                            //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                        }
                    }
                    else if (!string.IsNullOrEmpty(minprice) && dMinPrice > 0)//minprice != "0")
                    {
                        if (GlobalFunctions.IsPointsEnbled())
                        {
                            spnPrice.Attributes.Remove("class");
                            spnPrice.Attributes.Add("class", "price col-sm-12");
                            //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                            spnor.Attributes.Add("class", "price col-sm-12");
                            spnor.InnerHtml = stror;
                            spnPrice1.Attributes.Add("class", "price col-sm-12");
                            spnPrice1.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(minprice, strCurrencySymbol, intLanguageId);
                        
                        }
                        else
                        { 
                        spnPrice.Attributes.Remove("class");
                        spnPrice.Attributes.Add("class", "price col-sm-12");
                        //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                        spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                        }
                        spnstrike.Visible = false;
                    }
                    else { spnstrike.Visible = false; }
                    #endregion

                    if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
                    {
                        if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                        else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                        else
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                    }
                    if (blnShowSectionsIcons)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
                        {
                            string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
                            string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            string strSectionImageData = string.Empty;
                            string strSectionNames = string.Empty;
                            for (int i = 0; i < strSections.Length; i++)
                            {
                                string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                                {
                                    strSectionImageData += "<img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_list_icon'>";
                                }
                                if (strSectionIcon.Length > 1)
                                    strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                            }
                            dvSectionIcons.InnerHtml = strSectionImageData;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptProducts1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlAnchor lnkProductImage = (HtmlAnchor)e.Item.FindControl("lnkProductImage");
                    HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
                    Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
                    HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
                    HtmlGenericControl spnstrike = (HtmlGenericControl)e.Item.FindControl("spnstrike");
                    HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
                    HtmlGenericControl spnPrice1 = (HtmlGenericControl)e.Item.FindControl("spnPrice1");
                    HtmlGenericControl spnor = (HtmlGenericControl)e.Item.FindControl("spnor");
                    string strCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).CategoryName);
                    string strSubCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).SubCategoryName);
                    string strSubSubCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).SubSubCategoryName);
                    string strProductName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).ProductName);
                    if (!string.IsNullOrEmpty(((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).DefaultImageName))
                    {
                        if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).DefaultImageName).Trim()))
                        {
                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + ((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).DefaultImageName.Trim();
                            imgProduct.Alt = ((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).ProductName.Trim();
                            imgProduct.Attributes.Add("title", ((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).ProductName.Trim());
                        }
                        else
                        {
                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                            imgProduct.Alt = "Default";
                            imgProduct.Attributes.Add("title", "Default");
                        }
                    }
                    else
                    {
                        imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                        imgProduct.Alt = "Default";
                        imgProduct.Attributes.Add("title", "Default");
                    }

                    aProductName.InnerHtml = strProductName;
                    //spnPrice.InnerHtml = strCurrencySymbol + (((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice).ToString();



                    #region Code for strike price

                    //string minprice = Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice);
                    //string minstrikeprice =Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinstrikePrice);
                    double dMinPrice = Convert.ToDouble(((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).MinPrice);
                    double dminstrikeprice = Convert.ToDouble(((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)e.Item.DataItem).MinstrikePrice);
                    string minprice = dMinPrice.ToString("##,###,##0.#0");
                    string minstrikeprice = dminstrikeprice.ToString("##,###,##0.#0");

                    if (!string.IsNullOrEmpty(minstrikeprice) && dminstrikeprice > 0)//minstrikeprice != "0" )
                    {
                        //spnstrike.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinstrikePrice), strCurrencySymbol, intLanguageId);
                        spnstrike.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minstrikeprice, strCurrencySymbol, intLanguageId);
                        if (!string.IsNullOrEmpty(minprice) && dMinPrice > 0)//minprice != "0")
                        {
                            //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                        }
                    }
                    else if (!string.IsNullOrEmpty(minprice) && dMinPrice > 0)//minprice != "0")
                    {
                        if (GlobalFunctions.IsPointsEnbled())
                        {
                            spnPrice.Attributes.Remove("class");
                            spnPrice.Attributes.Add("class", "price col-sm-12");
                            //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                            spnor.Attributes.Add("class", "price col-sm-12");
                            spnor.InnerHtml = stror;
                            spnPrice1.Attributes.Add("class", "price col-sm-12");
                            spnPrice1.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(minprice, strCurrencySymbol, intLanguageId);

                        }
                        else
                        {
                            spnPrice.Attributes.Remove("class");
                            spnPrice.Attributes.Add("class", "price col-sm-12");
                            //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                        }
                    }
                    else { spnstrike.Visible = false; }
                    #endregion

                    if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
                    {
                        if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                        else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                        else
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                    }
                    if (blnShowSectionsIcons)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
                        {
                            string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
                            string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            string strSectionImageData = string.Empty;
                            string strSectionNames = string.Empty;
                            for (int i = 0; i < strSections.Length; i++)
                            {
                                string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                                {
                                    strSectionImageData += "<img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_list_icon'>";
                                }
                                if (strSectionIcon.Length > 1)
                                    strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                            }
                            dvSectionIcons.InnerHtml = strSectionImageData;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        protected void rptProducts2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlAnchor lnkProductImage = (HtmlAnchor)e.Item.FindControl("lnkProductImage");
                    HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
                    Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
                    HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
                    HtmlGenericControl spnstrike = (HtmlGenericControl)e.Item.FindControl("spnstrike");
                    HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
                    HtmlGenericControl spnPrice1 = (HtmlGenericControl)e.Item.FindControl("spnPrice1");
                    HtmlGenericControl spnor = (HtmlGenericControl)e.Item.FindControl("spnor");
                    string strCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).CategoryName);
                    string strSubCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).SubCategoryName);
                    string strSubSubCategoryName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).SubSubCategoryName);
                    string strProductName = (((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).ProductName);
                    if (!string.IsNullOrEmpty(((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).DefaultImageName))
                    {
                        if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).DefaultImageName).Trim()))
                        {
                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + ((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).DefaultImageName.Trim();
                            imgProduct.Alt = ((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).ProductName.Trim();
                            imgProduct.Attributes.Add("title", ((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).ProductName.Trim());
                        }
                        else
                        {
                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                            imgProduct.Alt = "Default";
                            imgProduct.Attributes.Add("title", "Default");
                        }
                    }
                    else
                    {
                        imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                        imgProduct.Alt = "Default";
                        imgProduct.Attributes.Add("title", "Default");
                    }

                    aProductName.InnerHtml = strProductName;
                    //spnPrice.InnerHtml = strCurrencySymbol + (((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice).ToString();



                    #region Code for strike price

                    //string minprice = Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice);
                    //string minstrikeprice =Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinstrikePrice);
                    double dMinPrice = Convert.ToDouble(((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).MinPrice);
                    double dminstrikeprice = Convert.ToDouble(((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)e.Item.DataItem).MinstrikePrice);
                    string minprice = dMinPrice.ToString("##,###,##0.#0");
                    string minstrikeprice = dminstrikeprice.ToString("##,###,##0.#0");

                    if (!string.IsNullOrEmpty(minstrikeprice) && dminstrikeprice > 0)//minstrikeprice != "0" )
                    {
                        //spnstrike.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinstrikePrice), strCurrencySymbol, intLanguageId);
                        spnstrike.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minstrikeprice, strCurrencySymbol, intLanguageId);
                        if (!string.IsNullOrEmpty(minprice) && dMinPrice > 0)//minprice != "0")
                        {
                            //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                        }
                    }
                    else if (!string.IsNullOrEmpty(minprice) && dMinPrice > 0)//minprice != "0")
                    {
                        if (GlobalFunctions.IsPointsEnbled())
                        {
                            spnPrice.Attributes.Remove("class");
                            spnPrice.Attributes.Add("class", "price col-sm-12");
                            //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                            spnor.Attributes.Add("class", "price col-sm-12");
                            spnor.InnerHtml = stror;
                            spnPrice1.Attributes.Add("class", "price col-sm-12");
                            spnPrice1.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(minprice, strCurrencySymbol, intLanguageId);

                        }
                        else
                        {
                            spnPrice.Attributes.Remove("class");
                            spnPrice.Attributes.Add("class", "price col-sm-12");
                            //spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(minprice, strCurrencySymbol, intLanguageId);
                        }
                    }
                    else { spnstrike.Visible = false; }
                    #endregion

                    if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
                    {
                        if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                        else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                        else
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                    }
                    if (blnShowSectionsIcons)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
                        {
                            string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
                            string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            string strSectionImageData = string.Empty;
                            string strSectionNames = string.Empty;
                            for (int i = 0; i < strSections.Length; i++)
                            {
                                string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                                {
                                    strSectionImageData += "<img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_list_icon'>";
                                }
                                if (strSectionIcon.Length > 1)
                                    strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                            }
                            dvSectionIcons.InnerHtml = strSectionImageData;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }


        #region
        //protected void rptProducts1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //        {
        //            HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
        //            HtmlAnchor lnkProductImage = (HtmlAnchor)e.Item.FindControl("lnkProductImage");
        //            HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
        //            Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
        //            HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
        //            HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
        //            string strCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).CategoryName);
        //            string strSubCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).SubCategoryName);
        //            string strSubSubCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).SubSubCategoryName);
        //            string strProductName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName);
        //            if (!string.IsNullOrEmpty(((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName))
        //            {
        //                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName).Trim()))
        //                {
        //                    imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + ((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName.Trim();
        //                    imgProduct.Alt = ((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName.Trim();
        //                    imgProduct.Attributes.Add("title", ((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName.Trim());
        //                }
        //                else
        //                {
        //                    imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
        //                    imgProduct.Alt = "Default";
        //                    imgProduct.Attributes.Add("title", "Default");
        //                }
        //            }
        //            else
        //            {
        //                imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
        //                imgProduct.Alt = "Default";
        //                imgProduct.Attributes.Add("title", "Default");

        //            }

        //            aProductName.InnerHtml = strProductName;
        //            //spnPrice.InnerHtml = strCurrencySymbol + (((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice).ToString();
        //            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);

        //            if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
        //            {
        //                if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //                else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //                else
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //            }
        //            if (blnShowSectionsIcons)
        //            {
        //                if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
        //                {
        //                    string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
        //                    string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        //                    string strSectionImageData = string.Empty;
        //                    string strSectionNames = string.Empty;
        //                    for (int i = 0; i < strSections.Length; i++)
        //                    {
        //                        string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
        //                        if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
        //                        {
        //                            strSectionImageData += "<img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_list_icon'>";
        //                        }
        //                        if (strSectionIcon.Length > 1)
        //                            strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
        //                    }
        //                    dvSectionIcons.InnerHtml = strSectionImageData;
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //}

        //protected void rptProducts2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //        {
        //            HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
        //            HtmlAnchor lnkProductImage = (HtmlAnchor)e.Item.FindControl("lnkProductImage");
        //            HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
        //            Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
        //            HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
        //            HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
        //            string strCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).CategoryName);
        //            string strSubCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).SubCategoryName);
        //            string strSubSubCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).SubSubCategoryName);
        //            string strProductName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName);
        //            if (!string.IsNullOrEmpty(((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName))
        //            {
        //                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName).Trim()))
        //                {
        //                    imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + ((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName.Trim();
        //                    imgProduct.Alt = ((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName.Trim();
        //                    imgProduct.Attributes.Add("title", ((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName.Trim());
        //                }
        //                else
        //                {
        //                    imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
        //                    imgProduct.Alt = "Default";
        //                    imgProduct.Attributes.Add("title", "Default");
        //                }
        //            }
        //            else
        //            {
        //                imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
        //                imgProduct.Alt = "Default";
        //                imgProduct.Attributes.Add("title", "Default");

        //            }

        //            aProductName.InnerHtml = strProductName;
        //            //spnPrice.InnerHtml = strCurrencySymbol + (((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice).ToString();
        //            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);

        //            if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
        //            {
        //                if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //                else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //                else
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //            }
        //            if (blnShowSectionsIcons)
        //            {
        //                if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
        //                {
        //                    string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
        //                    string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        //                    string strSectionImageData = string.Empty;
        //                    string strSectionNames = string.Empty;
        //                    for (int i = 0; i < strSections.Length; i++)
        //                    {
        //                        string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
        //                        if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
        //                        {
        //                            strSectionImageData += "<img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_list_icon'>";
        //                        }
        //                        if (strSectionIcon.Length > 1)
        //                            strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
        //                    }
        //                    dvSectionIcons.InnerHtml = strSectionImageData;
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //}

        //protected void rptProducts3_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //        {
        //            HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
        //            HtmlAnchor lnkProductImage = (HtmlAnchor)e.Item.FindControl("lnkProductImage");
        //            HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
        //            Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
        //            HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
        //            HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
        //            string strCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).CategoryName);
        //            string strSubCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).SubCategoryName);
        //            string strSubSubCategoryName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).SubSubCategoryName);
        //            string strProductName = (((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName);
        //            if (!string.IsNullOrEmpty(((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName))
        //            {
        //                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName).Trim()))
        //                {
        //                    imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + ((ProductBE.YouMayAlsoLike)e.Item.DataItem).DefaultImageName.Trim();
        //                    imgProduct.Alt = ((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName.Trim();
        //                    imgProduct.Attributes.Add("title", ((ProductBE.YouMayAlsoLike)e.Item.DataItem).ProductName.Trim());
        //                }
        //                else
        //                {
        //                    imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
        //                    imgProduct.Alt = "Default";
        //                    imgProduct.Attributes.Add("title", "Default");
        //                }
        //            }
        //            else
        //            {
        //                imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
        //                imgProduct.Alt = "Default";
        //                imgProduct.Attributes.Add("title", "Default");

        //            }

        //            aProductName.InnerHtml = strProductName;
        //            //spnPrice.InnerHtml = strCurrencySymbol + (((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice).ToString();
        //            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(((ProductBE.YouMayAlsoLike)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);

        //            if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
        //            {
        //                if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //                else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //                else
        //                {
        //                    lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);

        //                }
        //            }
        //            if (blnShowSectionsIcons)
        //            {
        //                if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
        //                {
        //                    string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
        //                    string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        //                    string strSectionImageData = string.Empty;
        //                    string strSectionNames = string.Empty;
        //                    for (int i = 0; i < strSections.Length; i++)
        //                    {
        //                        string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
        //                        if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
        //                        {
        //                            strSectionImageData += "<img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_list_icon'>";
        //                        }
        //                        if (strSectionIcon.Length > 1)
        //                            strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
        //                    }
        //                    dvSectionIcons.InnerHtml = strSectionImageData;
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //}
        #endregion

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 12-08-15
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        ltrRelatedProduct.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "relatedby_product").ResourceValue;
                        ltrRelatedPrice.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "relatedby_price").ResourceValue;
                        ltrRelatedCategory.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.ToLower() == "relatedby_category").ResourceValue;
                        stror = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "RecieveCommunication_or_Text").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}