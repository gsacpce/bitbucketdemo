﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.IO;
using System.Web.UI.HtmlControls;
using Microsoft.Security.Application;
namespace Presentation
{
    public class Products_CategoryListing : BasePage
    {
        #region Variables
        protected global::System.Web.UI.WebControls.Label lblHeader;
        protected global::System.Web.UI.WebControls.Repeater rptSubCategory;
        protected global::System.Web.UI.WebControls.Literal lblDescription;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgCategoryBanner;

        public string host = GlobalFunctions.GetVirtualPath();
        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-15
        /// Scope   : Set the CaegoryName property
        /// </summary>
        public string strCategoryName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["categoryname"])) ? "" : Sanitizer.GetSafeHtmlFragment(Convert.ToString(Page.RouteData.Values["categoryname"])); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-15
        /// Scope   : Set the SubCaegoryName property
        /// </summary>
        public string strSubCategoryName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["subcategoryname"])) ? "" : Sanitizer.GetSafeHtmlFragment(Convert.ToString(Page.RouteData.Values["subcategoryname"])); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-15
        /// Scope   : Page_Load of the CategoryListing page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            intLanguageId = GlobalFunctions.GetLanguageId();
            intCurrencyId = GlobalFunctions.GetCurrencyId();
            if (!Page.IsPostBack)
            {
                PoplutateSubCategory();
            }

            /*Sachin Chauhan : 22 09 2015 : Redirect user to Product details page with language change in place from drop down*/
            if (Session["PrevLanguageId"] == null)
            {
                Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
            }

            if (!Session["PrevLanguageId"].ToString().To_Int32().Equals(GlobalFunctions.GetLanguageId().To_Int32()))
            {
                intLanguageId = GlobalFunctions.GetLanguageId();
                Dictionary<int, List<CategoryBE>> objDictAllCategoriesAllLanguages;
                objDictAllCategoriesAllLanguages = CategoryBL.GetAllCategoriesAllLanguages();

                if (Request.Url.Equals(Request.UrlReferrer))
                {
                    string[] urlParts = Request.Url.ToString().Split('/');
                    Int16 urlOffset = 0;
                    for (Int16 i = 0; i < urlParts.Length; i++)
                    {
                        if (urlParts[i].ToLower().Contains("subcategories"))
                        {
                            urlOffset = (Int16)(i + 1);
                            break;
                        }
                    }

                    List<Int32> lstCatNameIndexes = new List<Int32>();
                    for (Int16 i = urlOffset; i < urlParts.Length; i++)
                    {
                        List<CategoryBE> lstCategoryNamesPrevLang = objDictAllCategoriesAllLanguages[Session["PrevLanguageId"].To_Int32()];
                        
                        Int32 catNameIndex = lstCategoryNamesPrevLang.FindIndex(x => x.CategoryName.Equals(urlParts[i]));
                        if (!catNameIndex.Equals(-1))
                            lstCatNameIndexes.Add(catNameIndex);
                    }

                    List<CategoryBE> lstCategoryNamesCurrLang = objDictAllCategoriesAllLanguages[GlobalFunctions.GetLanguageId().To_Int32()];
                    List<string> lstCatNamesCurrLang = new List<string>();
                    foreach (Int32 catIndex in lstCatNameIndexes)
                    {
                        lstCatNamesCurrLang.Add(lstCategoryNamesCurrLang[catIndex].CategoryName);
                    }

                    string redirectUrl = "";

                    for (Int16 i = 0; i < urlOffset ; i++)
                    {
                        redirectUrl += urlParts[i] + "/";
                    }

                    for (int i = 0; i < lstCatNamesCurrLang.Count; i++)
                    {
                        redirectUrl += GlobalFunctions.EncodeCategoryURL(lstCatNamesCurrLang[i]) + "/";
                    }

                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();

                    Response.Redirect(redirectUrl);
                }
                
            }

            /*Sachin Chauhan : 22 09 2015 */
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-15
        /// Scope   : Bind all the subcategories of a particular category with the image
        /// </summary>
        /// <param name=""></param>        
        /// <returns></returns>
        private void PoplutateSubCategory()
        {
            try
            {
                List<CategoryBE> objCategoryListingBE;
                List<ProductBE> objproductlistingBE;
                objCategoryListingBE = CategoryBL.GetSubCategories(intLanguageId, intCurrencyId, GlobalFunctions.DecodeCategoryURL(strCategoryName), GlobalFunctions.DecodeCategoryURL(strSubCategoryName));
                
                if (objCategoryListingBE != null && objCategoryListingBE.Count > 0)
                {
                    lblHeader.Text = GlobalFunctions.EncodeUrlBredCrumb(Convert.ToString(objCategoryListingBE[0].ParentCategoryName));
                    lblDescription.Text = objCategoryListingBE[0].DescriptionText;
                    if (string.IsNullOrEmpty(objCategoryListingBE[0].ParentCategoryBannerImgExt))
                    {
                        imgCategoryBanner.Visible = false;
                    }
                    else
                    {
                        if (File.Exists(Server.MapPath("~/Admin/Images/Category/Banner/" + objCategoryListingBE[0].ParentCategoryBannerImageName)))
                        {
                            imgCategoryBanner.Src = host + "Admin/Images/Category/Banner/" + objCategoryListingBE[0].ParentCategoryBannerImageName;
                            imgCategoryBanner.Attributes.Add("alt", strSubCategoryName == "" ? strCategoryName : strSubCategoryName);
                        }
                        else
                        {
                            imgCategoryBanner.Visible = false;
                        }
                    }
                    if (objCategoryListingBE[0].ChildCategoriesCount == 0)
                    {
                        objCategoryListingBE = objCategoryListingBE.FindAll(x => x.ProductCount > 0);
                    }
                    if (objCategoryListingBE.Count > 0)
                    {
                        rptSubCategory.DataSource = objCategoryListingBE;
                        rptSubCategory.DataBind();

                        //Set Page Title, Meta description, Keyword & Meta Title
                        this.MetaDescription = objCategoryListingBE[0].MetaDescription;
                        this.MetaKeywords = objCategoryListingBE[0].MetaKeywords;

                        Image objImage;
                        HtmlAnchor objAnch;
                        for (int i = 0; i <= rptSubCategory.Items.Count - 1; i++)
                        {
                            objAnch = new HtmlAnchor();
                            objAnch = (HtmlAnchor)rptSubCategory.Items[i].FindControl("hrfSubCategory");

                            objImage = new Image();
                            objImage = (Image)rptSubCategory.Items[i].FindControl("imgSubCategory");
                            objImage.ToolTip = GlobalFunctions.EncodeUrlBredCrumb(objCategoryListingBE[i].CategoryName);

                            if (string.IsNullOrEmpty(strSubCategoryName))
                            {
                                if (objCategoryListingBE[i].ChildCategoriesCount == 0)
                                {
                                    objAnch.HRef = host + "SubCategory/" + GlobalFunctions.DecodeUrlBredCrumb(objCategoryListingBE[i].ParentCategoryName) + "/" + GlobalFunctions.DecodeUrlBredCrumb(objCategoryListingBE[i].CategoryName);
                                }
                                else
                                {
                                    objAnch.HRef = host + "SubCategories/" + GlobalFunctions.DecodeUrlBredCrumb(objCategoryListingBE[i].ParentCategoryName) + "/" + GlobalFunctions.DecodeUrlBredCrumb(objCategoryListingBE[i].CategoryName);
                                }
                                this.Page.Title = objCategoryListingBE[0].MetaTitle == "" ? GlobalFunctions.DecodeCategoryURL(strCategoryName) : objCategoryListingBE[0].MetaKeywords;
                            }
                            else
                            {
                                objAnch.HRef = host + "SubCategory/" + GlobalFunctions.EncodeCategoryURL(objCategoryListingBE[i].ParentCategoryName) + "/" + strSubCategoryName + "/" + GlobalFunctions.EncodeCategoryURL(objCategoryListingBE[i].CategoryName);
                                this.Page.Title = objCategoryListingBE[0].MetaTitle == "" ? GlobalFunctions.DecodeCategoryURL(strSubCategoryName) : objCategoryListingBE[0].MetaKeywords;
                            }
                            if (File.Exists(Server.MapPath("~/Admin/Images/Category/" + objCategoryListingBE[i].SubSubCategoryId + objCategoryListingBE[i].ImageExtension)))
                            {
                                objImage.ImageUrl = host + "Admin/Images/Category/" + objCategoryListingBE[i].SubSubCategoryId + objCategoryListingBE[i].ImageExtension;
                            }
                            else
                            {

                                objproductlistingBE =ProductBL.GetSubCatImage(objCategoryListingBE[i].CategoryId, objCategoryListingBE[i].SubCategoryId, objCategoryListingBE[i].SubSubCategoryId, intLanguageId);
                                //objproductlistingBE = objproductlistingBE.Take(1).ToList();
                                if (objproductlistingBE!=null && objCategoryListingBE.Count>0)
                                {
                                    objImage.ImageUrl = host + "/Images/Products/Thumbnail/" + objproductlistingBE[0].DefaultImageName;
                                }
                               
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect(host + "page-not-found");
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}