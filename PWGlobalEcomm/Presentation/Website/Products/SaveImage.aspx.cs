﻿using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Products_SaveImage : System.Web.UI.Page
    {
        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 18/08/2015
        /// Scope   : Set the ImageName property
        /// </summary>
        public string strImageNameName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["imagename"])) ? "" : Sanitizer.GetSafeHtmlFragment(Convert.ToString(Page.RouteData.Values["imagename"])); }
        }

        protected global::System.Web.UI.HtmlControls.HtmlImage imgSaveImage;

        #region Declaration
        public static string host = GlobalFunctions.GetVirtualPath();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(strImageNameName))
                if (File.Exists(GlobalFunctions.GetPhysicalLargeProductImagePath() + GlobalFunctions.DecodeCategoryURL(strImageNameName.Trim())))
                {
                    imgSaveImage.Src = GlobalFunctions.GetLargeProductImagePath() + GlobalFunctions.DecodeCategoryURL(strImageNameName.Trim());
                    imgSaveImage.Attributes.Add("title", GlobalFunctions.DecodeCategoryURL(strImageNameName));
                }
                else
                    imgSaveImage.Src = GlobalFunctions.GetLargeProductImagePath() + "Default.jpg";
            else
                imgSaveImage.Src = GlobalFunctions.GetLargeProductImagePath() + "Default.jpg";
        }
    }
}