﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.IO;
using Microsoft.Security.Application;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Presentation
{
    /*Sachin Chauhan : 18 09 2015 : Changed Inheritance to BasPage for culture settings*/
    //public class Products_ProductListing : System.Web.UI.Page

    public class Products_ProductListing : BasePage
    {
        #region Variable
        public static string host = GlobalFunctions.GetVirtualPath();
        public Int16 intLanguageId = 0;
        public Int16 intCurrencyId = 0;
        public string strCurrencySymbol = string.Empty;

        public Int16 intPageSize = 9;
        public bool blnDefaultListView = true;
        public bool blnShowRatingsReview = false;
        public string strSortName = "productname asc";
        public string strCPMinimumProductMsg, strCPMaximumProductMsg, strProductComparison, strViewGrid, strViewList,
            strNoResultFoundMessage, strNoResultFoundMessage1, strResultsFoundMessage = string.Empty;
        public bool blnIsInfiniteScroll = false;
        public bool blnIsGoogleMixIt = false;
        bool blnShowGridView = false;
        bool blnShowListView = false;
        public bool blnShowFilters = false;
        public string strSearchProductIds, strSearchKeyword = string.Empty;
        string strProductSku = string.Empty;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgCategoryBanner;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor hrefBannerLink;
        protected global::System.Web.UI.WebControls.Literal ltrCategoryDescription, ltrJS, ltrTotalItems, ltrPages, ltrOf, ltrViewAll;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvViewAsContainer, dvFilterContainer, dvSortingContainer, dvProductContainer,
            dvSectionIconContainer, pfilterTitle, pfilterDesc, compareBtn, dvSearchMessage, hCategoryTitle, dvProductData;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor alstView, agrdView, resetFilters;
        protected global::System.Web.UI.HtmlControls.HtmlSelect ddlSorting;
        protected global::System.Web.UI.HtmlControls.HtmlInputButton btnCompare;
        protected global::System.Web.UI.WebControls.Repeater rptRandomProducts;
        protected global::System.Web.UI.WebControls.Label lblMobileCategoryTitle;

        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        private static Dictionary<string, int> featurevalue = new Dictionary<string, int>();
        Int16 iUserTypeID = 1; /*User Type*/
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-15
        /// Scope   : Set the CaegoryName property
        /// </summary>
        public string strCategoryName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["categoryname"])) ? "" : Convert.ToString(Page.RouteData.Values["categoryname"]); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-15
        /// Scope   : Set the SubCaegoryName property
        /// </summary>
        public string strSubCategoryName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["subcategoryname"])) ? "" : Convert.ToString(Page.RouteData.Values["subcategoryname"]); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-15
        /// Scope   : Set the SubCaegoryName property
        /// </summary>
        public string strSubSubCategoryName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["subsubcategoryname"])) ? "" : Convert.ToString(Page.RouteData.Values["subsubcategoryname"]); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 4-08-15
        /// Scope   : Set the SectionName property
        /// </summary>
        public string strSectionName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["sectionname"])) ? "" : Convert.ToString(Page.RouteData.Values["sectionname"]); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 3-09-15
        /// Scope   : Set the keyword property
        /// </summary>
        public string strKeyword
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["keyword"])) ? "" : Sanitizer.GetSafeHtmlFragment(Convert.ToString(Page.RouteData.Values["keyword"])); }//2016-05-26
            //get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["keyword"])) ? "" : Convert.ToString(Page.RouteData.Values["keyword"]); }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-15
        /// Scope   : Page_Load of the ProductListing page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { 
                if (Session["User"] != null)
                { iUserTypeID = ((UserBE)Session["User"]).UserTypeID; }

                //hCategoryTitle.InnerHtml = !string.IsNullOrEmpty(strSubSubCategoryName) ? strSubCategoryName : strCategoryName;
                dvSearchMessage.Visible = false;
                /*Sachin Chauhan : 18 09 2015 : Redirect user to Product listing page with language change in place from drop down*/
                //List<CategoryBE> objCategoryBE;
                //objCategoryBE = CategoryBL.GetAllCategories(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId());

                //objCategoryBE = HttpContext.Current.Cache["CategoriesCurrLang"] as List<CategoryBE>;
                //objDictAllCategoriesAllLanguages = HttpContext.Current.Cache["CategoriesAllLang"] as Dictionary<int, List<CategoryBE>>;


                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();


                if (Session["PrevLanguageId"] == null)
                {
                    Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();
                }

                if (!Session["PrevLanguageId"].ToString().To_Int32().Equals(GlobalFunctions.GetLanguageId().To_Int32()))
                {
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    if (!path.Contains("search"))
                    {

                        if (Request.Url.Equals(Request.UrlReferrer))
                        {
                            string[] urlParts = Request.Url.ToString().Split('/');
                            Int16 urlOffset = 0;
                            for (Int16 i = 0; i < urlParts.Length; i++)
                            {
                                if (urlParts[i].ToLower().Contains("category"))
                                {
                                    urlOffset = (Int16)(i + 1);
                                    break;
                                }
                            }
                            Dictionary<int, List<CategoryBE>> objDictAllCategoriesAllLanguages;
                            objDictAllCategoriesAllLanguages = CategoryBL.GetAllCategoriesAllLanguages();

                            CategoryBE prevLangCategory = new CategoryBE();
                            CategoryBE currLangCategoryName = new CategoryBE();
                            List<CategoryBE> lstCatNamesPrevLang = new List<CategoryBE>();
                            List<CategoryBE> lstCategoryNamesPrevLang = new List<CategoryBE>();
                            for (Int16 i = urlOffset; i < urlParts.Length; i++)
                            {
                                lstCategoryNamesPrevLang = objDictAllCategoriesAllLanguages[Session["PrevLanguageId"].To_Int32()];
                                /*Sachin Chauhan Start : 06 03 2016 : Changed logic of finding category name of previous language*/
                                /* changes by the vivek for the category page*/
                               /* prevLangCategory = lstCategoryNamesPrevLang.FirstOrDefault(x => x.CategoryName.Equals(GlobalFunctions.EncodeCategoryURL(urlParts[i])));*/
                                prevLangCategory = lstCategoryNamesPrevLang.FirstOrDefault(x => GlobalFunctions.EncodeCategoryURLLanguage(x.CategoryName) == urlParts[i]);
                                if (prevLangCategory != null)
                                {
                                    lstCatNamesPrevLang.Add(prevLangCategory);
                                }
                                /*Sachin Chauhan End : 06 03 2016*/
                            }

                            List<CategoryBE> lstCategoryNamesCurrLang = objDictAllCategoriesAllLanguages[GlobalFunctions.GetLanguageId().To_Int32()];


                            /*Sachin Chauhan Start : 06 03 2016*/
                            List<string> lstCatNamesCurrLang = new List<string>();
                            foreach (CategoryBE prevLangCat in lstCatNamesPrevLang)
                            {
                                lstCatNamesCurrLang.Add(lstCategoryNamesCurrLang.FirstOrDefault(x => x.CategoryId.Equals(prevLangCat.CategoryId)).CategoryName);
                            }
                            /*Sachin Chauhan End : 06 03 2016*/

                            string redirectUrl = "";

                            for (Int16 i = 0; i < urlOffset; i++)
                            {
                                redirectUrl += urlParts[i] + "/";
                            }

                            for (int i = 0; i < lstCatNamesCurrLang.Count; i++)
                            {
                                redirectUrl += GlobalFunctions.EncodeCategoryURL(lstCatNamesCurrLang[i]) + "/";
                            }

                            redirectUrl += GlobalFunctions.EncodeCategoryURL(currLangCategoryName.CategoryName);

                            Session["PrevLanguageId"] = GlobalFunctions.GetLanguageId();

                            Response.Redirect(redirectUrl);
                        }
                    }
                }
                /*Sachin Chauhan : 18 09 2015*/

                if (!string.IsNullOrEmpty(Request.QueryString["q"]))
                {
                    string jsondata = string.Empty;

                    // perform Lucene search
                    IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();
                    searchResults = ProductSearchBL.Search(Sanitizer.GetSafeHtmlFragment(Request.QueryString["q"].Trim()));

                    if (searchResults != null && searchResults.Count() > 0)
                    {
                        searchResults = searchResults.Where(x => x.LanguageId == GlobalFunctions.GetLanguageId() && x.CurrencyId == GlobalFunctions.GetCurrencyId());

                        if (searchResults.Count() > 0)
                        {
                            searchResults = searchResults.Take(15);
                            foreach (var item in searchResults)
                            {
                                jsondata += "{\"ProductName\" : \"" + item.ProductCode + " - " + item.ProductName.Replace("\"", "") + "\"},"; ;

                            }
                            jsondata = "[" + jsondata.Substring(0, jsondata.Length - 1) + "]";
                        }
                    }
                    strSearchKeyword = !string.IsNullOrEmpty(strKeyword) ? Server.HtmlEncode(GlobalFunctions.DecodeCategoryURL(strKeyword)) : "";
                    Response.Clear();
                    Response.Write(jsondata);
                    Response.End();
                }

                strSearchKeyword = Server.HtmlEncode(GlobalFunctions.DecodeCategoryURL(strKeyword));


                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {


                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "gridview").IsEnabled &&
                        objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "listview").IsEnabled)
                    {
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.IsDefault == true).FeatureValue.ToLower() == "gridview")
                        {
                            dvProductData.Attributes.Remove("class");
                            dvProductData.Attributes.Add("class", "row grid");
                            agrdView.Attributes.Add("class", "btn btn-sm btn-primary grid_button customActionBtn hidden");
                            alstView.Attributes.Add("class", "btn btn-sm btn-primary list_button customActionBtn visible");
                        }
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.IsDefault == true).FeatureValue.ToLower() == "listview")
                        {
                            dvProductData.Attributes.Remove("class");
                            dvProductData.Attributes.Add("class", "row list");
                            alstView.Attributes.Add("class", "btn btn-sm btn-primary list_button customActionBtn hidden");
                            agrdView.Attributes.Add("class", "btn btn-sm btn-primary grid_button customActionBtn visible");
                        }
                    }
                    else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "gridview").IsEnabled)
                    {
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.IsDefault == true).FeatureValue.ToLower() == "gridview")
                        {
                            dvProductData.Attributes.Remove("class");
                            dvProductData.Attributes.Add("class", "row grid");
                            agrdView.Attributes.Add("style","visibility:hidden");
                            alstView.Attributes.Add("style", "visibility:hidden");
                        }
                    }
                    else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "listview").IsEnabled)
                    {
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.IsDefault == true).FeatureValue.ToLower() == "listview")
                        {
                            dvProductData.Attributes.Remove("class");
                            dvProductData.Attributes.Add("class", "row list");
                            agrdView.Attributes.Add("style", "visibility:hidden");
                            alstView.Attributes.Add("style", "visibility:hidden");
                        }
                    }


                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_CompareProducts").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "product comparison").IsEnabled)
                    {
                        compareBtn.Visible = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableReviewRating").FeatureValues[0].IsEnabled)
                    { blnShowRatingsReview = true; }
                    #region Added by Sripal
                    FeatureBE objFeatureBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Filter");
                    List<FeatureBE.FeatureValueBE> lstobjFeatureValueBE = objFeatureBE.FeatureValues;
                    FeatureBE.FeatureValueBE objFeatureValueBE = lstobjFeatureValueBE.FirstOrDefault(v => v.FeatureValue.ToLower() == "enable filter");

                    if (objFeatureValueBE != null)
                    {
                        if (objFeatureValueBE.IsEnabled)
                        {
                            blnShowFilters = true;
                            dvFilterContainer.Visible = true;
                        }
                        else
                        {
                            dvFilterContainer.Visible = false;
                        }
                    }
                    #endregion
                    //objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Filter").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "enable filter").IsEnabled


                    #region Commented by Sripal
                    //if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Filter").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "enable filter").IsEnabled)
                    //{
                    //    blnShowFilters = true;
                    //    dvFilterContainer.Visible = true;
                    //}
                    //else
                    //{
                    //    dvFilterContainer.Visible = false;
                    //} 
                    #endregion

                    List<FeatureBE.FeatureValueBE> objFeatureValeBE;
                    objFeatureValeBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Sorting").FeatureValues.FindAll(v => v.IsEnabled == true);


                    if (objFeatureValeBE != null && objFeatureValeBE.Count > 0)
                    {
                        strSortName = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Sorting").FeatureValues.FirstOrDefault(v => v.IsDefault == true).DefaultValue;
                        blnShowFilters = true;
                        featurevalue.Clear();
                        foreach (FeatureBE.FeatureValueBE obj in objFeatureValeBE)
                        {
                            featurevalue.Add(obj.DefaultValue, Convert.ToInt32(obj.IsEnabled));
                        }

                        //featurevalue.Add(feat);
                        //ddlSorting.DataTextField = "FeatureValue";
                        //ddlSorting.DataValueField = "DefaultValue";

                        //ddlSorting.DataSource = objFeatureValeBE;
                        //ddlSorting.DataBind();

                        //ddlSorting.Items.FindByValue(strSortName).Selected = true;
                        dvSortingContainer.Visible = true;
                    }
                    else
                    {
                        dvSortingContainer.Visible = false;
                    }

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Pagination").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureValue.ToLower() == "lazy load")
                    {
                        blnIsInfiniteScroll = true;
                    }
                    else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Pagination").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureValue.ToLower() == "pagination")
                    {
                        intPageSize = Convert.ToInt16(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Pagination").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureDefaultValue);
                    }
                    else if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Pagination").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureValue.ToLower() == "mix it up")
                    {
                        blnIsGoogleMixIt = true;
                        blnShowFilters = false;
                        dvSortingContainer.Visible = false;
                        dvFilterContainer.Visible = false;
                        dvSectionIconContainer.Visible = true;
                        strSortName = "AsLowAsPrice asc";
                        ltrJS.Text = "<script src='" + host + "js/mixitup.js'></script><style>#dvProductData .mix-target {display: none;}</style>";
                    }

                    if (!blnShowFilters)
                        dvProductContainer.Attributes.Add("class", "col-md-12");

                    BindResourceData();

                    if (!string.IsNullOrEmpty(strKeyword))
                    {
                        dvSearchMessage.Visible = true;
                        imgCategoryBanner.Visible = false;
                        hrefBannerLink.Visible = false;
                        this.Page.Title = "Search - " + strKeyword;
                        strSearchProductIds = "0";
                        //set product ids
                        try
                        {
                            // perform Lucene search
                            IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();
                            searchResults = ProductSearchBL.Search(Sanitizer.GetSafeHtmlFragment(strKeyword.Trim()));//2016-05-26
                            //searchResults = ProductSearchBL.Search(strKeyword.Trim());

                            #region "Added by Sripal for filtering the search"
                            //string[] args = strKeyword.Split(' ');
                            //string newInput = string.Empty;
                            //if (args.Length > 1)
                            //{
                            //    newInput = Convert.ToString(args[0]);
                            //    searchResults = searchResults.Where(x => x.ProductCode.Contains(newInput));
                            //}

                            #endregion
                            if (searchResults != null && searchResults.Count() > 0)
                            {
                                searchResults = searchResults.Where(x => x.LanguageId == GlobalFunctions.GetLanguageId() && x.CurrencyId == GlobalFunctions.GetCurrencyId());
                                if (searchResults.Count() > 0)
                                {
                                    #region "Added by Sripal for filtering"
                                    if (strKeyword.Contains("- "))
                                    {
                                        string[] args = strSearchKeyword.Split(' ');

                                        string newInput = string.Empty;

                                        for (int i = 1; i < args.Length; i++)
                                        {
                                            if (Convert.ToString(args[i]) != "-")//added by Sripal if Condition
                                            {
                                                newInput += Convert.ToString(args[i]) + ' ';
                                            }
                                        }
                                        if (args.Length >= 2)
                                        {
                                            //searchResults = searchResults.Where(x => x.ProductCode.Trim().ToLower() + " - " + x.ProductName.Trim().ToLower() == Sanitizer.GetSafeHtmlFragment(strKeyword.Trim().ToLower()));commented by sripal
                                            //searchResults = searchResults.Where(x => x.ProductCode.Trim().ToLower().Replace("*", "") + " - " + x.ProductName.Trim().ToLower() == Sanitizer.GetSafeHtmlFragment(strKeyword.Trim().ToLower()));//2016-05-26
                                            //searchResults = searchResults.Where(x => x.ProductCode.Trim().ToLower().Replace("*", "") + " - " + x.ProductName.Trim().ToLower() == strKeyword.Trim().ToLower());
                                               if (args[0].Contains("-")) //code contains hypons
                                            {
                                                //string temp=args[0].Replace("-","");
                                                //string temp1 = args[i+1].Replace("-", "");
                                                   string temp=args[0];
                                                 
                                                 
                                                string[] strPdtCode = strKeyword.Split('-');
                                                if (strPdtCode != null)
                                                {
                                                    short iLanguage = GlobalFunctions.GetLanguageId();
                                                    searchResults = searchResults.Where(x => x.LanguageId == iLanguage && x.ProductCode.Trim().ToLower().Replace("*", "") == Sanitizer.GetSafeHtmlFragment(temp.Trim().ToLower()));//2016-05-26
                                                }
                                           
                                            }
                                            //if (args[0].Contains("-")) //code contains hypons
                                            //{
                                            //    searchResults = searchResults.Where(x => x.ProductCode.Trim().ToLower().Replace(":", "") + " - " + x.ProductName.Trim().ToLower() == strKeyword.Trim().ToLower());
                                            //}
                                            else
                                            {
                                                string[] strPdtCode = strKeyword.Split('-');
                                                if (strPdtCode != null)
                                                {
                                                    short iLanguage = GlobalFunctions.GetLanguageId();
                                                    searchResults = searchResults.Where(x => x.LanguageId == iLanguage && x.ProductCode.Trim().ToLower().Replace("*", "") == Sanitizer.GetSafeHtmlFragment(strPdtCode[0].Trim().ToLower()));//2016-05-26
                                                }
                                                else
                                                {
                                                    searchResults = searchResults.Where(x => x.ProductCode.Trim().ToLower().Replace("*", "") + " - " + x.ProductName.Trim().ToLower() == Sanitizer.GetSafeHtmlFragment(strKeyword.Trim().ToLower()));//2016-05-26
                                                }
                                            }
                                       }
                                    }
                                    #endregion
                                    strSearchProductIds = string.Empty;
                                    foreach (var item in searchResults)
                                    {
                                        strSearchProductIds += item.ProductId + ",";
                                    }
                                    if (strSearchProductIds.Length > 1)
                                        strSearchProductIds = strSearchProductIds.Substring(0, strSearchProductIds.Length - 1);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                        }

                    }
                    else if (string.IsNullOrEmpty(strSectionName))
                    {
                        List<CategoryBE> objCategoryListingBE;
                        objCategoryListingBE = CategoryBL.GetCategoryBannerImageExtension(intLanguageId, GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(strCategoryName)), GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(strSubCategoryName)), GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(strSubSubCategoryName)));
                        if (objCategoryListingBE != null && objCategoryListingBE.Count > 0)
                        {
                            ltrCategoryDescription.Text = objCategoryListingBE[0].DescriptionText;
                            //Set Page Title, Meta description, Keyword & Meta Title
                            this.MetaDescription = objCategoryListingBE[0].MetaDescription;
                            this.MetaKeywords = objCategoryListingBE[0].MetaKeywords;
                            this.Page.Title = objCategoryListingBE[0].MetaTitle == "" ? strCategoryName + " " + strSubCategoryName + " " + strSubSubCategoryName : objCategoryListingBE[0].MetaTitle;

                            if (string.IsNullOrEmpty(objCategoryListingBE[0].BannerImageExtension))
                            {
                                imgCategoryBanner.Visible = false;
                                hrefBannerLink.Visible = false;
                            }
                            else
                            {
                                if (File.Exists(Server.MapPath("~/Admin/Images/Category/Banner/" + objCategoryListingBE[0].CategoryId + objCategoryListingBE[0].BannerImageExtension)))
                                {
                                    imgCategoryBanner.Src = host + "Admin/Images/Category/Banner/" + objCategoryListingBE[0].CategoryId + objCategoryListingBE[0].BannerImageExtension;
                                    imgCategoryBanner.Attributes.Add("alt", strSubCategoryName == "" ? strCategoryName : strSubCategoryName);
                                    hrefBannerLink.HRef = objCategoryListingBE[0].BannerLink;
                                }
                                else
                                {
                                    imgCategoryBanner.Visible = false;
                                    hrefBannerLink.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect(host + "page-not-found");
                        }
                    }
                    else
                    {
                        List<SectionBE> objSectionBE;
                        objSectionBE = SectionBL.GetSectionBannerImageExtension(intLanguageId, GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(strSectionName)));
                        if (objSectionBE != null)
                        {
                            if (objSectionBE.Count > 0)
                            {
                                ltrCategoryDescription.Text = objSectionBE[0].SectionDescription;
                                //Set Page Title, Meta description, Keyword & Meta Title
                                this.MetaDescription = objSectionBE[0].MetaDescription;
                                this.MetaKeywords = objSectionBE[0].MetaKeyword;
                                this.Page.Title = objSectionBE[0].PageTitle == "" ? strSectionName : objSectionBE[0].PageTitle;

                                if (string.IsNullOrEmpty(objSectionBE[0].BannerImageExtension))
                                {
                                    imgCategoryBanner.Visible = false;
                                    hrefBannerLink.Visible = false;
                                }
                                else
                                {
                                    if (File.Exists(Server.MapPath("~/Admin/Images/Section/Banner/" + objSectionBE[0].SectionId + objSectionBE[0].BannerImageExtension)))
                                    {
                                        imgCategoryBanner.Src = host + "Admin/Images/Section/Banner/" + objSectionBE[0].SectionId + objSectionBE[0].BannerImageExtension;
                                        imgCategoryBanner.Attributes.Add("alt", strSectionName);
                                    }
                                    else
                                    {
                                        imgCategoryBanner.Visible = false;
                                        hrefBannerLink.Visible = false;
                                    }
                                }
                            }
                        }
                    }

                }

                #region "Random Products"
                List<ProductBE> lstSectionProducts = ProductBL.GetRandom(GlobalFunctions.GetLanguageId(), GlobalFunctions.GetCurrencyId(), iUserTypeID);/*User Type*/
                rptRandomProducts.DataSource = lstSectionProducts;
                rptRandomProducts.DataBind();
                #endregion

                /*Sachin Chauhan Start: 23 03 2016 :*/
                if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("category") || Request.Url.AbsoluteUri.ToString().ToLower().Contains("subcategory"))
                {
                    string[] urlParts = Request.Url.ToString().Split('/');
                    Int16 urlOffset = 0;

                    #region
                    string strProductDetailsUrl = urlParts[urlParts.Length - 1].ToString();
                    int iQt = strProductDetailsUrl.IndexOf('?');
                    if (iQt > 0)
                    {
                        string strURL = strProductDetailsUrl.Substring(0, iQt);
                        lblMobileCategoryTitle.Text = strProductDetailsUrl.Substring(0, iQt);
                    }
                    else
                    {
                        lblMobileCategoryTitle.Text = urlParts[urlParts.Length - 1].ToString();
                    }
                    #endregion
                    //lblMobileCategoryTitle.Text = urlParts[urlParts.Length - 1].ToString();Commented 

                }

                /*Sachin Chauhan End : 23 03 2016*/
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                //Cache["AllResourceData"] = null;
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        ltrTotalItems.Text = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Pagination_TotalItems_Text").ResourceValue);
                        //ltrSortBy.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_SortBy_Text").ResourceValue;
                        ltrPages.Text = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Pages_Text").ResourceValue);
                        ltrOf.Text = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_PagesOf_Text").ResourceValue);
                        ltrViewAll.Text = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_ViewAll_Text").ResourceValue);
                        //ltrViewAs.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_ViewAs_Text").ResourceValue;
                        pfilterTitle.InnerHtml = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Filter_Title").ResourceValue);
                        pfilterDesc.InnerHtml = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Filter_Description").ResourceValue);
                        resetFilters.InnerHtml = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Reset_Filter_Text").ResourceValue);
                        //strQuickViewTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_QuickView_Text").ResourceValue;
                        btnCompare.Value = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_CompareProduct_Button_Text").ResourceValue);
                        strCPMinimumProductMsg = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_CompareProduct_MinimumProducts_Message").ResourceValue);
                        strCPMaximumProductMsg = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_CompareProduct_MaximumProducts_Message").ResourceValue);
                        strProductComparison = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Product_Comparison").ResourceValue);
                        strNoResultFoundMessage = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_NoRecords_Message").ResourceValue);
                        strResultsFoundMessage = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_Results_Message").ResourceValue);
                        string strRemoveSanitisedPrefixes = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_NoRecords_Message1").ResourceValue);
                        strRemoveSanitisedPrefixes = strRemoveSanitisedPrefixes.Replace("\"", "");
                        strRemoveSanitisedPrefixes = strRemoveSanitisedPrefixes.Replace(System.Environment.NewLine, " ");
                        strNoResultFoundMessage1 = strRemoveSanitisedPrefixes;// lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Search_NoRecords_Message1").ResourceValue;
                        strViewGrid = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Grid_Text").ResourceValue);
                        strViewList = GlobalFunctions.RemoveSanitisedPrefixes(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_List_Text").ResourceValue);
                        if (blnShowFilters)
                        {
                            ddlSorting.DataTextField = "ResourceKey";
                            ddlSorting.DataValueField = "ResourceValue";

                            string ProductNameAsc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "productname asc").ResourceValue);
                            string ProductNameDesc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.Replace('\n', ' ').Replace('\r', ' ').Replace('\t', ' ').Trim() == "productname desc").ResourceValue);
                            string ProductPriceAsc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "AsLowAsPrice asc").ResourceValue);
                            string ProductPriceDesc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.Replace('\n', ' ').Replace('\r', ' ').Replace('\t', ' ').Trim() == "AsLowAsPrice desc").ResourceValue);
                            string ProductNewFirst = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Sequence asc").ResourceValue);
                            string ProductOldFirst = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Sequence desc").ResourceValue);
                            string RatingAsc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Rating_asc_text").ResourceValue);
                            string RatingDesc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Rating_desc_text").ResourceValue);
                            string Stockasc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Stock_asc_text").ResourceValue);
                            string Stockdesc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Stock_desc_text").ResourceValue);

                            #region "Said by Nilesh"
                            //string ProductNameAsc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Product_Name_Ascending").ResourceValue);
                            //string ProductNameDesc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.Replace('\n', ' ').Replace('\r', ' ').Replace('\t', ' ').Trim() == "Generic_Product_Name_Descending").ResourceValue);
                            //string ProductPriceAsc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Product_Price_Ascending").ResourceValue);
                            //string ProductPriceDesc = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey.Replace('\n', ' ').Replace('\r', ' ').Replace('\t', ' ').Trim() == "Generic_Product_Price_Descending").ResourceValue);
                            //string ProductNewFirst = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Newest_First").ResourceValue);
                            //string ProductOldFirst = (lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Oldest_First").ResourceValue); 
                            #endregion


                            ddlSorting.Items.Clear();
                            foreach (KeyValuePair<string, int> pair in featurevalue)
                            {
                                if (pair.Key == "productname asc" && pair.Value == 1)
                                {
                                    ddlSorting.Items.Add(new ListItem(ProductNameAsc, "productname asc"));
                                }
                                if (pair.Key == "productname desc" && pair.Value == 1)
                                {
                                    ddlSorting.Items.Add(new ListItem(ProductNameDesc, "productname desc"));
                                }
                                if (pair.Key == "AsLowAsPrice asc" && pair.Value == 1)
                                {
                                    ddlSorting.Items.Add(new ListItem(ProductPriceAsc, "AsLowAsPrice asc"));
                                }
                                if (pair.Key == "AsLowAsPrice desc" && pair.Value == 1)
                                {
                                    ddlSorting.Items.Add(new ListItem(ProductPriceDesc, "AsLowAsPrice desc"));
                                }
                                if (pair.Key == "Sequence asc" && pair.Value == 1)
                                {
                                    ddlSorting.Items.Add(new ListItem(ProductNewFirst, "Sequence asc"));
                                }
                                if (pair.Key == "Sequence desc" && pair.Value == 1)
                                {
                                    ddlSorting.Items.Add(new ListItem(ProductOldFirst, "Sequence desc"));
                                }
                                if (blnShowRatingsReview)
                                {
                                    if (pair.Key == "Rating asc" && pair.Value == 1)
                                    {
                                        ddlSorting.Items.Add(new ListItem(RatingAsc, "Rating asc"));
                                    }
                                    if (pair.Key == "Rating desc" && pair.Value == 1)
                                    {
                                        ddlSorting.Items.Add(new ListItem(RatingDesc, "Rating desc"));
                                    }
                                }
                                if (pair.Key == "Stock asc" && pair.Value == 1)
                                {
                                    ddlSorting.Items.Add(new ListItem(Stockasc, "Stock asc"));
                                }
                                if (pair.Key == "Stock desc" && pair.Value == 1)
                                {
                                    ddlSorting.Items.Add(new ListItem(Stockdesc, "Stock desc"));
                                }
                            }

                            ddlSorting.Items.FindByValue(strSortName).Selected = true;
                            dvSortingContainer.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        [System.Web.Services.WebMethod]
        public static string GetProductData(string Keyword)
        {
            string jsondata = string.Empty;
            try
            {
                // perform Lucene search
                IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();
                searchResults = ProductSearchBL.Search(Sanitizer.GetSafeHtmlFragment(Keyword.Trim()));

                if (searchResults != null && searchResults.Count() > 0)
                {
                    searchResults = searchResults.Where(x => x.LanguageId == GlobalFunctions.GetLanguageId() && x.CurrencyId == GlobalFunctions.GetCurrencyId());
                    if (searchResults.Count() > 0)
                    {
                        searchResults = searchResults.Take(10);
                        System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        jsondata = jSearializer.Serialize(searchResults);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return jsondata;
        }


        protected void rptRandomProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Int32 ProductId = 0;
                    string ProductName = string.Empty;

                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
                    HtmlAnchor aProductImage = (HtmlAnchor)e.Item.FindControl("aProductImage");
                    Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
                    HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
                    HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
                    HtmlGenericControl liProducts = (HtmlGenericControl)e.Item.FindControl("liProducts");

                    ProductId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "productId"));
                    ProductName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductName"));

                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";

                    if (ProductName.Length > 60)
                        aProductName.InnerHtml = ProductName.Substring(0, 60);
                    else
                        aProductName.InnerHtml = ProductName;

                    ltrProductCode.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCode"));


                    string AsLowAsPrice = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice"));
                    if (!string.IsNullOrEmpty(AsLowAsPrice) && AsLowAsPrice != "0")
                    {
                        spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")), strCurrencySymbol, GlobalFunctions.GetLanguageId());
                    }
                    //else
                    //{
                    //    spnPrice.Visible = false;
                    //}
                    string strDescription = GlobalFunctions.RemoveHtmlTags(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductDescription")));

                    string strCategoryName1 = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName")).Replace("amp;", "");
                    string strSubCategoryName1 = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SubCategoryName")).Replace("amp;", "");
                    string strSubSubCategoryName1 = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SubSubCategoryName")).Replace("amp;", "");


                    string ProductNamewithoutSpecialchar = Regex.Replace(ProductName, "[^a-zA-Z0-9]+", " ");

                    if (strCategoryName == strSubCategoryName1 && strCategoryName == strSubSubCategoryName1)
                    {
                        aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName1) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(ProductNamewithoutSpecialchar);
                    }
                    else if (strCategoryName == strSubCategoryName1 && strSubCategoryName1 != strSubSubCategoryName1)
                    {
                        aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName1) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName1) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(ProductNamewithoutSpecialchar);
                    }
                    else
                    {
                        aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName1) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(ProductNamewithoutSpecialchar);
                    }


                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}