﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Products_filter_ajax : System.Web.UI.Page
    {
        #region Variables

        protected global::System.Web.UI.WebControls.Repeater rptCategories;
        protected global::System.Web.UI.WebControls.Repeater rptColor;
        protected global::System.Web.UI.WebControls.Repeater rptSection;

        protected global::System.Web.UI.WebControls.Repeater rptCustomFilter1;
        protected global::System.Web.UI.WebControls.Repeater rptCustomFilter2;
        protected global::System.Web.UI.WebControls.Repeater rptCustomFilter3;
        protected global::System.Web.UI.WebControls.Repeater rptCustomFilter4;
        protected global::System.Web.UI.WebControls.Repeater rptCustomFilter5;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl hCustomFilter1;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl hCustomFilter2;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl hCustomFilter3;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl hCustomFilter4;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl hCustomFilter5;

        protected global::System.Web.UI.HtmlControls.HtmlAnchor aParentCategory;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvSubCategoryFilter, hPrice, hSubCategory, hColor, hSection;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvColorFilter;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvPriceFilter;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvSectionFilter;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCustomFilter1;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCustomFilter2;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCustomFilter3;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCustomFilter4;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvCustomFilter5;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl pPriceRange;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl bMinPrice;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl bMaxPrice;
        protected global::System.Web.UI.HtmlControls.HtmlInputText priceRange;

        public static string host = GlobalFunctions.GetVirtualPath();
        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        string strCurrencySymbol = string.Empty;
        string strCategoryName = string.Empty;
        string strSubCategoryName = string.Empty;
        string strSubSubCategoryName = string.Empty;
        string strSectionName = string.Empty;
        string strColorIds,Culturedata, strSectionIds = string.Empty;
        decimal decMinPrice = 0;
        decimal decMaxPrice = 0;

        bool blnShowSubCategoryFilter = false;
        bool blnShowPriceFilter = false;
        bool blnShowColorFilter = false;
        bool blnShowSectionFilter = false;
        bool IsGoogleMixIt = false;

        string strCustomFilter1 = string.Empty;
        string strCustomFilter2 = string.Empty;
        string strCustomFilter3 = string.Empty;
        string strCustomFilter4 = string.Empty;
        string strCustomFilter5 = string.Empty;
        string strPriceRange, strPriceLowToHigh, strPriceHighToLow = string.Empty;
        string strSearchProductIds = string.Empty;
        Int16 iUserTypeID = 1;/*User Type*/
        #endregion

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 28-07-15
        /// Scope   : Page_Load of the Filter_ajax page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            #region "/*User Type*/"
            if (Session["User"] != null)
            {
                UserBE objUserBE = new UserBE();
                objUserBE = Session["User"] as UserBE;
                iUserTypeID = objUserBE.UserTypeID;
            } 
            #endregion
            intLanguageId = Convert.ToInt16(Request.Form["languageid"]);
            intCurrencyId = Convert.ToInt16(Request.Form["currencyid"]);
            strCurrencySymbol = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["currencysymbol"]));

            BindResourceData();
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Filter").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "enable category filter").IsEnabled)
                {
                    blnShowSubCategoryFilter = true;
                }
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Filter").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "enable price filter").IsEnabled)
                {
                    blnShowPriceFilter = true;
                }
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Filter").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "enable color filter").IsEnabled)
                {
                    blnShowColorFilter = true;
                }
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Filter").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "enable section filter").IsEnabled)
                {
                    blnShowSectionFilter = true;
                }
            }

            strCategoryName = GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(Request.Form["categoryname"]));
            strSubCategoryName = GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(Request.Form["subcategoryname"]));
            strSubSubCategoryName = GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(Request.Form["subsubcategoryname"]));
            strColorIds = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["colorids"]));
            strSectionIds = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["sectionids"]));
            strSectionName = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["sectionname"]));

            decMinPrice = Convert.ToDecimal(Request.Form["minprice"]);
            decMaxPrice = Convert.ToDecimal(Request.Form["maxprice"]);
            IsGoogleMixIt = Convert.ToBoolean(Request.Form["isgooglemixit"]);

            strCustomFilter1 = Convert.ToString(Request.Form["customfilter1"]);
            strCustomFilter2 = Convert.ToString(Request.Form["customfilter2"]);
            strCustomFilter3 = Convert.ToString(Request.Form["customfilter3"]);
            strCustomFilter4 = Convert.ToString(Request.Form["customfilter4"]);
            strCustomFilter5 = Convert.ToString(Request.Form["customfilter5"]);
            strSearchProductIds = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["searchproductids"]));

            try
            {
                ProductBE objProductBE;
                ProductBE objProdBE = new ProductBE();
                objProdBE.LanguageId = intLanguageId;
                objProdBE.CurrencyId = intCurrencyId;
                objProdBE.CategoryName = strCategoryName;
                objProdBE.SubCategoryName = strSubCategoryName;
                objProdBE.SubSubCategoryName = strSubSubCategoryName;
                objProdBE.MinPrice = decMinPrice;
                objProdBE.MaxPrice = decMaxPrice;
                objProdBE.SectionName = strSectionName;
                //objProdBE.SearchProductIds = strSearchProductIds;

                List<ProductBE.ProductColorVariantMappping> objProductColorVariantMapppingLst = new List<ProductBE.ProductColorVariantMappping>();
                objProductColorVariantMapppingLst = ProductBL.getProductIDwithVariant<ProductBE.ProductColorVariantMappping>(strSearchProductIds);
                if (objProductColorVariantMapppingLst != null)
                {
                    objProdBE.SearchProductIds = Convert.ToString(objProductColorVariantMapppingLst[0].ProductId);
                }

                else
                {
                    objProdBE.SearchProductIds = strSearchProductIds;
                }

                if (string.IsNullOrEmpty(strSectionIds))
                {
                    objProdBE.SectionIds = strSectionIds;
                }
                else
                {
                    objProdBE.SectionIds = strSectionIds.Replace("'", "");
                }

                if (string.IsNullOrEmpty(strColorIds))
                {
                    objProdBE.Colors = strColorIds;
                }
                else
                {
                    objProdBE.Colors = strColorIds.Replace("'", "");
                }

                if (string.IsNullOrEmpty(strCustomFilter1))
                {
                    objProdBE.CustomFilter1 = "";
                }
                else
                {
                    objProdBE.CustomFilter1 = strCustomFilter1.Replace("'", "");
                }
                if (string.IsNullOrEmpty(strCustomFilter2))
                {
                    objProdBE.CustomFilter2 = "";
                }
                else
                {
                    objProdBE.CustomFilter2 = strCustomFilter2.Replace("'", "");
                }
                if (string.IsNullOrEmpty(strCustomFilter3))
                {
                    objProdBE.CustomFilter3 = "";
                }
                else
                {
                    objProdBE.CustomFilter3 = strCustomFilter3.Replace("'", "");
                }
                if (string.IsNullOrEmpty(strCustomFilter4))
                {
                    objProdBE.CustomFilter4 = "";
                }
                else
                {
                    objProdBE.CustomFilter4 = strCustomFilter4.Replace("'", "");
                }
                if (string.IsNullOrEmpty(strCustomFilter5))
                {
                    objProdBE.CustomFilter5 = "";
                }
                else
                {
                    objProdBE.CustomFilter5 = strCustomFilter5.Replace("'", "");
                }

                objProductBE = ProductBL.GetProductFilterData(objProdBE, iUserTypeID);

                if (objProductBE != null)
                {
                    if (IsGoogleMixIt)
                    {
                        Response.Clear();
                        string strGoolgeMixItData = "<div class='dvsections col-md-6'><a class='filter-btn' data-filter='all'><img title='All' alt='all' src='" + host + "admin/Images/Section/Icon/all.png' class='SectionIcon'></span>";
                        if (objProductBE.ProductSectionFilter != null && objProductBE.ProductSectionFilter.Count > 0)
                        {
                            for (int i = 0; i < objProductBE.ProductSectionFilter.Count; i++)
                            {
                                if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + objProductBE.ProductSectionFilter[i].SectionId + objProductBE.ProductSectionFilter[i].IconImageExtension)))
                                {
                                    strGoolgeMixItData += "<a class='filter-btn' data-filter='" + objProductBE.ProductSectionFilter[i].SectionName.Replace("'", "''").Replace(" ", "-") + "'><img title='" + objProductBE.ProductSectionFilter[i].SectionName.Replace("'", "''") + "' alt='" + objProductBE.ProductSectionFilter[i].SectionName.Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + objProductBE.ProductSectionFilter[i].SectionId + objProductBE.ProductSectionFilter[i].IconImageExtension + "' class='SectionIcon'></a>";
                                }
                                else
                                {
                                    strGoolgeMixItData += "<a class='filter-btn' data-filter='" + objProductBE.ProductSectionFilter[i].SectionName.Replace("'", "''").Replace(" ", "-") + "'>" + objProductBE.ProductSectionFilter[i].SectionName + "</a>";
                                }
                            }
                            strGoolgeMixItData += "</div>";
                            strGoolgeMixItData += "<div class='dvpricefilter priceFilterBtn col-md-6'><input data-sort='data-cat' data-order='desc' value='" + strPriceLowToHigh + "' type='button'" +
                            " class='sort active btn btn-sm btn-primary customActionBtn'/><input data-sort='data-cat' data-order='asc'" +
                            " type='button' value='" + strPriceHighToLow + "' class='sort btn btn-sm btn-primary list_button customActionBtn'/></div>";
                        }
                        Response.Write(strGoolgeMixItData);
                        Response.End();
                    }
                    else
                    {
                        BindCategoryFilterData(objProductBE);

                        BindPriceFilterData(objProductBE);

                        BindColorFilterData(objProductBE);

                        BindCustomFilter1Data(objProductBE);

                        BindCustomFilter2Data(objProductBE);

                        BindCustomFilter3Data(objProductBE);

                        BindCustomFilter4Data(objProductBE);

                        BindCustomFilter5Data(objProductBE);

                        BindSectionFilterData(objProductBE);
                    }
                }
                else
                {
                    Response.Clear();
                    Response.Write("norecords");
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        hSubCategory.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Filter_SubCategory_Title").ResourceValue;
                        hPrice.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Price_Title_Text").ResourceValue; // Key updated by SHRIGANESH SINGH 27 March 2016
                        hColor.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Filter_Color_Title").ResourceValue;
                        hSection.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Filter_Section_Title").ResourceValue;
                        strPriceRange = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Price_Range").ResourceValue;
                        strPriceHighToLow = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Price_highest_first").ResourceValue;
                        strPriceLowToHigh = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Price_Lowest_first").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : It binds the data for the Color filter
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindColorFilterData(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductColorFilter != null && objProductBE.ProductColorFilter.Count > 0 && blnShowColorFilter)
                {
                    rptColor.DataSource = objProductBE.ProductColorFilter;
                    rptColor.DataBind();
                }
                else
                {
                    dvColorFilter.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : rptColor_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptColor_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlInputCheckBox chkColorName = (HtmlInputCheckBox)e.Item.FindControl("chkColorName");
                    chkColorName.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ColorId"));
                    if (strColorIds.Contains("'" + chkColorName.Value + "',"))
                    {
                        chkColorName.Checked = true;
                    }
                    else
                    {
                        chkColorName.Checked = false;
                    }
                    Literal ltrColorName = (Literal)e.Item.FindControl("ltrColorName");
                    ltrColorName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ColorName")) + " (" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + ")";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 28-07-15
        /// Scope   : It binds the data for the Price filter
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindPriceFilterData(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductPriceFilter != null && objProductBE.ProductPriceFilter.Count > 0 && blnShowPriceFilter && GlobalFunctions.IsPointsEnbled() == false)
                {
                    if (objProductBE.ProductPriceFilter[0].MaxPrice != 0 && (objProductBE.ProductPriceFilter[0].MinPrice != objProductBE.ProductPriceFilter[0].MaxPrice))
                    {
                        #region
                        StoreBE objStoreBE = new StoreBE();
                        objStoreBE = StoreBL.GetStoreDetails();
                        Culturedata = objStoreBE.StoreLanguages.FirstOrDefault(x => x.LanguageId == GlobalFunctions.GetLanguageId()).CultureCode;
                        CultureInfo ci = new CultureInfo(Culturedata);
                        decimal decMinPrice = objProductBE.ProductPriceFilter[0].MinPrice;
                        decimal decMaxPrice = objProductBE.ProductPriceFilter[0].MaxPrice;
                        decMinPrice = Convert.ToDecimal(decMinPrice, CultureInfo.InvariantCulture);
                        decMaxPrice = Convert.ToDecimal(decMaxPrice, CultureInfo.InvariantCulture);
                        pPriceRange.InnerHtml = strPriceRange + " : " + strCurrencySymbol + decMinPrice + " - " + strCurrencySymbol + decMaxPrice;
                        bMinPrice.InnerHtml = strCurrencySymbol + decMinPrice + "&nbsp;";
                        bMaxPrice.InnerHtml = "&nbsp;" + strCurrencySymbol + decMaxPrice;
                        priceRange.Attributes.Add("data-slider-min", Convert.ToString(decMinPrice));
                        priceRange.Attributes.Add("data-slider-max", Convert.ToString(decMaxPrice));
                        priceRange.Attributes.Add("data-slider-step", "0.05");
                        priceRange.Attributes.Add("data-slider-value", "[" + decMinPrice + "," + decMaxPrice + "]");
                        #endregion
                    }
                    else
                    {
                        dvPriceFilter.Visible = false;
                    }
                }
                else
                {
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        decimal decMinPrice = objProductBE.ProductPriceFilter[0].MinPrice;
                        decimal decMaxPrice = objProductBE.ProductPriceFilter[0].MaxPrice;

                        string strMinPrice = decMinPrice.ToString();
                        string strMinPointsToPrice = "";
                        strMinPointsToPrice = GlobalFunctions.DisplayRoundOfPoints(strMinPrice, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());

                        string strMaxPrice = decMaxPrice.ToString();
                        string strMaxPointsToPrice = "";
                        strMaxPointsToPrice = GlobalFunctions.DisplayRoundOfPoints(strMaxPrice, GlobalFunctions.GetCurrencySymbol(), GlobalFunctions.GetLanguageId());
                        
                        pPriceRange.InnerHtml = strPriceRange + " : " + strMinPointsToPrice + " - " + strMaxPointsToPrice;
                        bMinPrice.InnerHtml = strMinPointsToPrice + "&nbsp;";
                        bMaxPrice.InnerHtml = "&nbsp;" + strMaxPointsToPrice;
                        priceRange.Attributes.Add("data-slider-min", Convert.ToString(strMinPrice));
                        priceRange.Attributes.Add("data-slider-max", Convert.ToString(strMaxPrice));
                        priceRange.Attributes.Add("data-slider-step", "0.05");
                        priceRange.Attributes.Add("data-slider-value", "[" + strMinPrice + "," + strMaxPrice + "]");
                        dvPriceFilter.Visible = true;
                    }
                    else
                    {
                        dvPriceFilter.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 28-07-15
        /// Scope   : It binds the data for the categories
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindCategoryFilterData(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductCategoryFilter != null && objProductBE.ProductCategoryFilter.Count > 0 && blnShowSubCategoryFilter)
                {
                    rptCategories.DataSource = objProductBE.ProductCategoryFilter;
                    rptCategories.DataBind();

                    if (string.IsNullOrEmpty(strSubSubCategoryName))
                    {
                        if (!string.IsNullOrEmpty(strSubCategoryName))
                        {
                            aParentCategory.InnerHtml = strCategoryName;
                            aParentCategory.HRef = host + "SubCategories/" + GlobalFunctions.DecodeUrlBredCrumb(strCategoryName);
                        }
                    }
                    else
                    {
                        aParentCategory.InnerHtml = strSubCategoryName;
                        aParentCategory.HRef = host + "SubCategories/" + GlobalFunctions.DecodeUrlBredCrumb(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName);
                    }
                }
                else
                {
                    dvSubCategoryFilter.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 28-07-15
        /// Scope   : rptCategories_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlAnchor aCategory = (HtmlAnchor)e.Item.FindControl("aCategory");
                    aCategory.InnerHtml = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                    if (string.IsNullOrEmpty(strSubSubCategoryName))
                    {
                        if (!string.IsNullOrEmpty(strSubCategoryName))
                        {
                            if (strSubCategoryName == aCategory.InnerHtml)
                            {
                                aCategory.Attributes.Add("class", "active hyperLinkSml");
                            }
                            aCategory.HRef = host + "SubCategory/" + GlobalFunctions.DecodeUrlBredCrumb(strCategoryName) + "/" + GlobalFunctions.DecodeUrlBredCrumb(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName")));
                        }
                    }
                    else
                    {
                        if (strSubSubCategoryName.ToLower() == aCategory.InnerHtml.ToLower())
                        {
                            aCategory.Attributes.Add("class", "active hyperLinkSml");
                        }
                        aCategory.HRef = host + "SubCategory/" + GlobalFunctions.DecodeUrlBredCrumb(strCategoryName) + "/" + GlobalFunctions.DecodeUrlBredCrumb(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName")));
                    }
                    aCategory.InnerHtml += " (" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + ")";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : It binds the data for the Custom filter1
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindCustomFilter1Data(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductCustomFilter1 != null && objProductBE.ProductCustomFilter1.Count > 0 && !string.IsNullOrEmpty(objProductBE.ProductCustomFilter1[0].FilterData))
                {
                    List<ProductBE.ProductCustomFilterBE> objCustomFilter = new List<ProductBE.ProductCustomFilterBE>();
                    ProductBE.ProductCustomFilterBE objFilter = null;
                    var dict = new Dictionary<string, Int32>();

                    string strFilterData = string.Empty;
                    Int32 intProductCount = 0;
                    for (int i = 0; i < objProductBE.ProductCustomFilter1.Count; i++)
                    {
                        strFilterData = objProductBE.ProductCustomFilter1[i].FilterData;
                        intProductCount = objProductBE.ProductCustomFilter1[i].ProductCount;
                        string[] strFilters = strFilterData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                        for (int j = 0; j < strFilters.Length; j++)
                        {
                            if (dict.ContainsKey(strFilters[j]))
                            {
                                objCustomFilter.RemoveAll(x => x.FilterData == strFilters[j]);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount + dict.First(x => x.Key == strFilters[j]).Value;
                                objCustomFilter.Add(objFilter);

                                dict.Remove(strFilters[j]);
                                dict.Add(strFilters[j], objFilter.ProductCount);

                                objFilter = null;
                            }
                            else
                            {
                                dict.Add(strFilters[j], intProductCount);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount;
                                objCustomFilter.Add(objFilter);
                            }
                        }
                    }

                    hCustomFilter1.InnerHtml = objProductBE.ProductCustomFilter1[0].FilterName;
                    rptCustomFilter1.DataSource = objCustomFilter;
                    rptCustomFilter1.DataBind();
                }
                else
                {
                    dvCustomFilter1.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : rptCustomFilter1_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptCustomFilter1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlInputCheckBox chkCustomFilter1 = (HtmlInputCheckBox)e.Item.FindControl("chkCustomFilter1");
                    Literal ltrCustomFilterName1 = (Literal)e.Item.FindControl("ltrCustomFilterName1");

                    chkCustomFilter1.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData"));
                    ltrCustomFilterName1.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData")) + " (" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + ")";
                    if (strCustomFilter1.Contains("'" + chkCustomFilter1.Value + "'|"))
                    {
                        chkCustomFilter1.Checked = true;
                    }
                    else
                    {
                        chkCustomFilter1.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : It binds the data for the Custom filter2
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindCustomFilter2Data(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductCustomFilter2 != null && objProductBE.ProductCustomFilter2.Count > 0 && !string.IsNullOrEmpty(objProductBE.ProductCustomFilter2[0].FilterData))
                {
                    List<ProductBE.ProductCustomFilterBE> objCustomFilter = new List<ProductBE.ProductCustomFilterBE>();
                    ProductBE.ProductCustomFilterBE objFilter = null;
                    var dict = new Dictionary<string, Int32>();

                    string strFilterData = string.Empty;
                    Int32 intProductCount = 0;
                    for (int i = 0; i < objProductBE.ProductCustomFilter2.Count; i++)
                    {
                        strFilterData = objProductBE.ProductCustomFilter2[i].FilterData;
                        intProductCount = objProductBE.ProductCustomFilter2[i].ProductCount;
                        string[] strFilters = strFilterData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                        for (int j = 0; j < strFilters.Length; j++)
                        {
                            if (dict.ContainsKey(strFilters[j]))
                            {
                                objCustomFilter.RemoveAll(x => x.FilterData == strFilters[j]);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount + dict.First(x => x.Key == strFilters[j]).Value;
                                objCustomFilter.Add(objFilter);

                                dict.Remove(strFilters[j]);
                                dict.Add(strFilters[j], objFilter.ProductCount);

                                objFilter = null;
                            }
                            else
                            {
                                dict.Add(strFilters[j], intProductCount);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount;
                                objCustomFilter.Add(objFilter);
                            }
                        }
                    }


                    hCustomFilter2.InnerHtml = objProductBE.ProductCustomFilter2[0].FilterName;
                    rptCustomFilter2.DataSource = objCustomFilter; // objProductBE.ProductCustomFilter2;
                    rptCustomFilter2.DataBind();
                }
                else
                {
                    dvCustomFilter2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : rptCustomFilter2_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptCustomFilter2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlInputCheckBox chkCustomFilter2 = (HtmlInputCheckBox)e.Item.FindControl("chkCustomFilter2");
                    Literal ltrCustomFilterName2 = (Literal)e.Item.FindControl("ltrCustomFilterName2");

                    chkCustomFilter2.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData"));
                    ltrCustomFilterName2.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData")) + " (" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + ")";

                    if (strCustomFilter2.Contains("'" + chkCustomFilter2.Value + "'|"))
                    {
                        chkCustomFilter2.Checked = true;
                    }
                    else
                    {
                        chkCustomFilter2.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : It binds the data for the Custom filter3
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindCustomFilter3Data(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductCustomFilter3 != null && objProductBE.ProductCustomFilter3.Count > 0 && !string.IsNullOrEmpty(objProductBE.ProductCustomFilter3[0].FilterData))
                {
                    List<ProductBE.ProductCustomFilterBE> objCustomFilter = new List<ProductBE.ProductCustomFilterBE>();
                    ProductBE.ProductCustomFilterBE objFilter = null;
                    var dict = new Dictionary<string, Int32>();

                    string strFilterData = string.Empty;
                    Int32 intProductCount = 0;
                    for (int i = 0; i < objProductBE.ProductCustomFilter3.Count; i++)
                    {
                        strFilterData = objProductBE.ProductCustomFilter3[i].FilterData;
                        intProductCount = objProductBE.ProductCustomFilter3[i].ProductCount;
                        string[] strFilters = strFilterData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                        for (int j = 0; j < strFilters.Length; j++)
                        {
                            if (dict.ContainsKey(strFilters[j]))
                            {
                                objCustomFilter.RemoveAll(x => x.FilterData == strFilters[j]);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount + dict.First(x => x.Key == strFilters[j]).Value;
                                objCustomFilter.Add(objFilter);

                                dict.Remove(strFilters[j]);
                                dict.Add(strFilters[j], objFilter.ProductCount);

                                objFilter = null;
                            }
                            else
                            {
                                dict.Add(strFilters[j], intProductCount);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount;
                                objCustomFilter.Add(objFilter);
                            }
                        }
                    }
                    hCustomFilter3.InnerHtml = objProductBE.ProductCustomFilter3[0].FilterName;
                    rptCustomFilter3.DataSource = objCustomFilter; // objProductBE.ProductCustomFilter3;
                    rptCustomFilter3.DataBind();
                }
                else
                {
                    dvCustomFilter3.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : rptCustomFilter3_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptCustomFilter3_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlInputCheckBox chkCustomFilter3 = (HtmlInputCheckBox)e.Item.FindControl("chkCustomFilter3");
                    Literal ltrCustomFilterName3 = (Literal)e.Item.FindControl("ltrCustomFilterName3");

                    chkCustomFilter3.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData"));
                    ltrCustomFilterName3.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData")) + " (" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + ")";

                    if (strCustomFilter3.Contains("'" + chkCustomFilter3.Value + "'|"))
                    {
                        chkCustomFilter3.Checked = true;
                    }
                    else
                    {
                        chkCustomFilter3.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : It binds the data for the Custom filter4
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindCustomFilter4Data(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductCustomFilter4 != null && objProductBE.ProductCustomFilter4.Count > 0 && !string.IsNullOrEmpty(objProductBE.ProductCustomFilter4[0].FilterData))
                {
                    List<ProductBE.ProductCustomFilterBE> objCustomFilter = new List<ProductBE.ProductCustomFilterBE>();
                    ProductBE.ProductCustomFilterBE objFilter = null;
                    var dict = new Dictionary<string, Int32>();

                    string strFilterData = string.Empty;
                    Int32 intProductCount = 0;
                    for (int i = 0; i < objProductBE.ProductCustomFilter4.Count; i++)
                    {
                        strFilterData = objProductBE.ProductCustomFilter4[i].FilterData;
                        intProductCount = objProductBE.ProductCustomFilter4[i].ProductCount;
                        string[] strFilters = strFilterData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                        for (int j = 0; j < strFilters.Length; j++)
                        {
                            if (dict.ContainsKey(strFilters[j]))
                            {
                                objCustomFilter.RemoveAll(x => x.FilterData == strFilters[j]);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount + dict.First(x => x.Key == strFilters[j]).Value;
                                objCustomFilter.Add(objFilter);

                                dict.Remove(strFilters[j]);
                                dict.Add(strFilters[j], objFilter.ProductCount);

                                objFilter = null;
                            }
                            else
                            {
                                dict.Add(strFilters[j], intProductCount);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount;
                                objCustomFilter.Add(objFilter);
                            }
                        }
                    }

                    hCustomFilter4.InnerHtml = objProductBE.ProductCustomFilter4[0].FilterName;
                    rptCustomFilter4.DataSource = objCustomFilter; // objProductBE.ProductCustomFilter4;
                    rptCustomFilter4.DataBind();
                }
                else
                {
                    dvCustomFilter4.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : rptCustomFilter4_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptCustomFilter4_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlInputCheckBox chkCustomFilter4 = (HtmlInputCheckBox)e.Item.FindControl("chkCustomFilter4");
                    Literal ltrCustomFilterName4 = (Literal)e.Item.FindControl("ltrCustomFilterName4");

                    chkCustomFilter4.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData"));
                    ltrCustomFilterName4.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData")) + " (" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + ")";

                    if (strCustomFilter4.Contains("'" + chkCustomFilter4.Value + "'|"))
                    {
                        chkCustomFilter4.Checked = true;
                    }
                    else
                    {
                        chkCustomFilter4.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : It binds the data for the Custom filter5
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindCustomFilter5Data(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductCustomFilter5 != null && objProductBE.ProductCustomFilter5.Count > 0 && !string.IsNullOrEmpty(objProductBE.ProductCustomFilter5[0].FilterData))
                {
                    List<ProductBE.ProductCustomFilterBE> objCustomFilter = new List<ProductBE.ProductCustomFilterBE>();
                    ProductBE.ProductCustomFilterBE objFilter = null;
                    var dict = new Dictionary<string, Int32>();

                    string strFilterData = string.Empty;
                    Int32 intProductCount = 0;
                    for (int i = 0; i < objProductBE.ProductCustomFilter5.Count; i++)
                    {
                        strFilterData = objProductBE.ProductCustomFilter5[i].FilterData;
                        intProductCount = objProductBE.ProductCustomFilter5[i].ProductCount;
                        string[] strFilters = strFilterData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                        for (int j = 0; j < strFilters.Length; j++)
                        {
                            if (dict.ContainsKey(strFilters[j]))
                            {
                                objCustomFilter.RemoveAll(x => x.FilterData == strFilters[j]);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount + dict.First(x => x.Key == strFilters[j]).Value;
                                objCustomFilter.Add(objFilter);

                                dict.Remove(strFilters[j]);
                                dict.Add(strFilters[j], objFilter.ProductCount);

                                objFilter = null;
                            }
                            else
                            {
                                dict.Add(strFilters[j], intProductCount);

                                objFilter = new ProductBE.ProductCustomFilterBE();
                                objFilter.FilterData = strFilters[j];
                                objFilter.ProductCount = intProductCount;
                                objCustomFilter.Add(objFilter);
                            }
                        }
                    }
                    hCustomFilter5.InnerHtml = objProductBE.ProductCustomFilter5[0].FilterName;
                    rptCustomFilter5.DataSource = objCustomFilter; // objProductBE.ProductCustomFilter5;
                    rptCustomFilter5.DataBind();
                }
                else
                {
                    dvCustomFilter5.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-07-15
        /// Scope   : rptCustomFilter5_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptCustomFilter5_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlInputCheckBox chkCustomFilter5 = (HtmlInputCheckBox)e.Item.FindControl("chkCustomFilter5");
                    Literal ltrCustomFilterName5 = (Literal)e.Item.FindControl("ltrCustomFilterName5");

                    chkCustomFilter5.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData"));
                    ltrCustomFilterName5.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FilterData")) + " (" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + ")";

                    if (strCustomFilter5.Contains("'" + chkCustomFilter5.Value + "'|"))
                    {
                        chkCustomFilter5.Checked = true;
                    }
                    else
                    {
                        chkCustomFilter5.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 31-07-15
        /// Scope   : It binds the data for the section filter
        /// </summary>
        /// <param name="objProductBE"></param>        
        /// <returns></returns>
        protected void BindSectionFilterData(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.ProductSectionFilter != null && objProductBE.ProductSectionFilter.Count > 0 && blnShowSectionFilter)
                {
                    rptSection.DataSource = objProductBE.ProductSectionFilter;
                    rptSection.DataBind();
                }
                else
                {
                    dvSectionFilter.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 31-07-15
        /// Scope   : rptSection_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptSection_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlInputCheckBox chkSectionName = (HtmlInputCheckBox)e.Item.FindControl("chkSectionName");
                    chkSectionName.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionId"));
                    if (strSectionIds.Contains("'" + chkSectionName.Value + "',"))
                    {
                        chkSectionName.Checked = true;
                    }
                    else
                    {
                        chkSectionName.Checked = false;
                    }
                    Literal ltrSectionName = (Literal)e.Item.FindControl("ltrSectionName");
                    ltrSectionName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionName")) + " (" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCount")) + ")";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}