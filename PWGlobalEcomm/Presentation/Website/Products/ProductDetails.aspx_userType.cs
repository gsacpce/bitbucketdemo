﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.Stock;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Products_ProductDetails : BasePage
    {

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 21-07-16
        /// Scope   : Set the CaegoryName property
        /// </summary>
        public string strCategoryName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["categoryname"])) ? "" : GlobalFunctions.DecodeCategoryURL(Convert.ToString(Page.RouteData.Values["categoryname"])); }
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 21-07-16
        /// Scope   : Set the SubCaegoryName property
        /// </summary>
        public string strSubCategoryName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["subcategoryname"])) ? "" : GlobalFunctions.DecodeCategoryURL(Convert.ToString(Page.RouteData.Values["subcategoryname"])); }
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 21-07-16
        /// Scope   : Set the SubCaegoryName property
        /// </summary>
        public string strSubSubCategoryName
        {
            get { return string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["subsubcategoryname"])) ? "" : GlobalFunctions.DecodeCategoryURL(Convert.ToString(Page.RouteData.Values["subsubcategoryname"])); }
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 21-07-16
        /// Scope   : Set the SubCaegoryName property
        /// </summary>
        public string strProductName
        {
            get
            {
                string strskuproductname = string.Empty;
                strskuproductname = string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["ProductSKUId"])) ? "" : GlobalFunctions.DecodeCategoryURL(Convert.ToString(Page.RouteData.Values["ProductSKUId"]));
                strskuproductname = strskuproductname.Replace("*", "") + "_" + (string.IsNullOrEmpty(Convert.ToString(Page.RouteData.Values["productname"])) ? "" : GlobalFunctions.DecodeCategoryURL(Convert.ToString(Page.RouteData.Values["productname"])));
                return strskuproductname;
            }

        }



        #region UI Controls
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl spnProductCode, spnProductName, dvReviewRating, spnProductDescription, dvYouMayAlsoLike, dvTableProductDetail, dvProductPriceTable, dvSectionIcons, dvSocialMedia, spnReviewCount, spnLetterRemaining, spnWriteReview, spnReadReview, spnreviewnext, spnreviewprev, spnProductReviews, dvProductOrder;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvRecentlyViewContainer, lstPrint, lstEmail, lstSaveImage, lstWishlist, lstSavePDF, dvConfigurableFields, dvProductSize, dvProductEnquiry, dvfileupload;
        protected global::System.Web.UI.WebControls.Repeater rptVariantList, rptVariantTypeList, rptRecentlyViewed, rptReportSKU, rptSocialMedia;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgProduct, imgRecentlyViewedProduct, product_colour_selected;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aVideoBox, hrefDownloadImage;
        protected global::System.Web.UI.WebControls.HiddenField hdnImageName, hdnRating, hdnSKUIdAddWishList, hdnSKUCount, hdnimagepath;
        protected global::System.Web.UI.WebControls.Literal ltrPlayVideo, ltrPrint, ltrEmail, ltrPDF, ltrDownloadImage, ltrDescription, ltrEnquireText, ltrEnquirySentMsg, ltrEnquireProductTitle, ltrReviewHeading, ltrRatingText, ltrheaderEmail, ltrSenderdetail, ltrSenderName, ltrSenderEmail, ltrSenderMsg, ltrReciepientdetail, ltrRecipientName, ltrRecipientEmail;
        protected global::System.Web.UI.WebControls.Label lblEmailAddress, lblQtyRequired, lblDeliveryDate, lblBranding, lblLogo, lblAdditionalInfo, lblEmailId, lblReviewTitle, lblReviewComment, lblRating;
        protected global::System.Web.UI.HtmlControls.HtmlTextArea txtareaAdditionalInfo, txtareaCommentfield;
        protected global::System.Web.UI.WebControls.TextBox txtEmailAddress, txtQtyRequired, txtDeliveryDate, txtBranding, txtReviewEmailId, txtReviewTitle, txtSenderName, txtSenderEmail, txtSenderMsg, txtReciepientName, txtReciepientEmail;
        protected global::System.Web.UI.WebControls.FileUpload fuLogo;
        protected global::System.Web.UI.WebControls.Button btnSubmitEnquiry, btnReviewSubmit, btnShopping, imgbtnAddtoCart, btnSend, btnCancel, btnAdd;
        protected global::System.Web.UI.WebControls.LinkButton lnkDownloadImg;
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvEmailAddress, rfvQtyrequired, rfvDeliveryDate, rfvReviewEmailId, rfvEmail, rfvRecipientEmail;
        protected global::System.Web.UI.WebControls.RegularExpressionValidator rxvEmailAddress, rxvReviewEmaiId, revEmail, revRecipientEmail;
        protected global::System.Web.UI.WebControls.DropDownList ddlSKU;
        protected string strRatingErrorMsg = "";
        protected string SocialMediaText, ZoomMoveText, strAddToWishList, ReadReviews, WriteReviews, SizeQuantityText, strProductTitle, strStockTitle, strStockDueText, strStockDueDate, strPriceTitle, strQuantityTitle;
        #endregion

        #region Declaration
        public static string host = GlobalFunctions.GetVirtualPath();
        public static string strVideoBoxTitle = "Product Video";
        /*Sachin Chauhan Start : 04 02 2015 : Variable to store social media short url*/
        string strSocialMediaShortUrl = String.Empty;
        /*Sachin Chauhan End : 04 02 2015*/
        StoreBE objStoreBE;
        UserBE objUserBE;
        public static ProductBE objCurrentProductBE;
        public static ProductBE objVariantProductBE;

        public static int ProductId;
        Int16 intLanguageId;
        Int16 intCurrencyId;
        public Int16 ReviewPageSize = 6;
        /*Sachin Chauhan Start: 20 01 2015 : For checking product variant image count more than one or not*/
        Int16 intProductVariantImgCount = 0;
        /*Sachin Chauhan End : 20 01 2015*/
        string strCurrencySymbol, strQuantityLabel, strPricingLabel, strNote, strProductionTime = "";
        int UserId;
        int intProdStockLevel = 0;
        int SKUCount = 0;
        public bool blnShowPrint = false;
        public bool blnShowEmail = false;
        public bool blnShowDownloadImage = false;
        public bool blnShowSocialMediaIcons = false;
        public bool blnShowRecentlyViewedProducts = false;
        public bool blnShowYouMayAsloLike = false;
        public bool blnShowWishlist = false;
        public bool blnShowRatingsReview = false;
        public bool blnShowSavePDF = false;
        public bool blnShowSectionsIcons = false;
        public bool bBackOrderAllowed = false;
        public bool bBASysStore = false;
        public string strReviewSuccessMsg, strReviewFailureMsg, strReviewValidateEmail, strWishListSaveSuccessMsg, strGenericSaveFailMsg, Wish_Choose_SKU_Item_Text, Close_Btn;
        public string strStarRating1, strStarRating2, strStarRating3, strStarRating4, strStarRating5;
        public string strmailprodcodelabel, strmailprodNamelabel, strmailprodDesclabel, strmailfurtherDesclabel, strEmailThisProduct,
            strBasket_Quantity_Not_Zero_Message, strBasket_Invalid_Quantity_Message = string.Empty, strSizeGuideTitle = string.Empty;
        public string strInStockTitle = string.Empty, strOutOfStotckTitle = string.Empty;

        public string strNoProductsFound, strErrorProcessingDataMsg, strPleaseEnter, strLimitedProductMessage, strLogoFileSize, strInvalidFileType, strPDFCreationError, strProductEnquiry;
        public string MinMaxProductQuantity, AddProductQuantity, BlockUI_Message, Status_Title, strUnitCost;

        string sstrPrice = "";
        double ddPrice = 0;
        public DateTime? stockduedate;
        #endregion

        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        Int16 iUserTypeID = 0;/*User Type*/
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                #region
                if (Session["User"] != null)
                {
                    objUserBE = new UserBE();
                    objUserBE = Session["User"] as UserBE;
                    //commented by vikram //objUserBE = Session["StoreUser"] as UserBE;
                    txtEmailAddress.Text = objUserBE.EmailId;
                    txtReviewEmailId.Text = objUserBE.EmailId;
                    iUserTypeID = objUserBE.UserTypeID;
                    UserId = objUserBE.UserId;
                }
                dvfileupload.Visible = false;
                if (!IsPostBack)
                {
                    //if (Request.UrlReferrer != Request.Url)
                    //{
                    Session["tempProductId"] = null;
                    Session["SessionSwatches"] = null;
                    Session["objVariantProductBE"] = null;
                    //}
                }
                intLanguageId = GlobalFunctions.GetLanguageId().To_Int16();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                // Added by vikram for Store user instead of Admin User //if (Session["StoreUser"] != null) 

                //if (objUserBE != null)
                //{
                //    UserId = objUserBE.UserId;
                //}
                BindResourceData();
                objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    bBackOrderAllowed = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "OD_AllowBackOrder").FeatureValues[0].IsEnabled;
                    bBASysStore = objStoreBE.IsBASYS;
                }

                if (!string.IsNullOrEmpty(strProductName))
                {
                    //ProductId = ProductBL.GetProductIdByName(strProductName.Trim(), intLanguageId);

                    ProductId = ProductBL.GetProductIdByName(strProductName.Trim(), Session["PrevLanguageId"].To_Int16() != null ? Session["PrevLanguageId"].To_Int16() : GlobalFunctions.GetLanguageId());
                    if (ProductId == 0)
                        ProductId = ProductBL.GetProductIdByName(strProductName.Trim(), intLanguageId);
                    Session["CurrProductId"] = ProductId;
                }
                else
                    ProductId = Session["CurrProductId"].To_Int32();
                //ProductId = 0;

                if (Session["PrevLanguageId"] == null)
                { Session["PrevLanguageId"] = intLanguageId; }
                if (!Session["PrevLanguageId"].ToString().To_Int32().Equals(intLanguageId))
                {
                    RedirectToProduct(ProductId);
                }
                /*Sachin Chauhan : 18 09 2015*/
                if (objStoreBE != null)
                {
                    #region
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnablePrint").FeatureValues[0].IsEnabled)
                    { blnShowPrint = true; }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableEmail").FeatureValues[0].IsEnabled)
                    { blnShowEmail = true; }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableDownloadImage").FeatureValues[0].IsEnabled)
                    { blnShowDownloadImage = true; }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableSocialMedia").FeatureValues[0].IsEnabled)
                    {
                        blnShowSocialMediaIcons = true;
                        /*Sachin Chauhan Start : 04 02 2015 : Adding below parameters for social media url share to identify catalogue*/
                        NameValueCollection nvcParams = new NameValueCollection();

                        nvcParams.Add("IsReferrerSM", "true");
                        nvcParams.Add("SMLId", GlobalFunctions.GetLanguageId().ToString());
                        nvcParams.Add("SMCId", GlobalFunctions.GetCurrencyId().ToString());
                        nvcParams.Add("SMCSymbol", GlobalFunctions.GetCurrencySymbol().ToString());
                        nvcParams.Add("SMPid", ProductId.ToString());
                        nvcParams.Add("SMShareUrl", Request.Url.ToString());

                        string strSMUrlToShare = String.Empty;

                        if (Request.Url.ToString().Contains("localhost"))
                            strSMUrlToShare = "http://bav2.cwwws.com/DemoStore/Products/SMSharedUrl.aspx?SMPurl=" + GlobalFunctions.EncryptQueryStrings(nvcParams, GlobalFunctions.GetCryptoKey());
                        else
                            strSMUrlToShare = host + "Products/SMSharedUrl.aspx?SMPurl=" + GlobalFunctions.EncryptQueryStrings(nvcParams, GlobalFunctions.GetCryptoKey());

                        #region "Comment"
                        //if (Request.Url.ToString().Contains("localhost") )
                        //    strSMUrlToShare = "http://bav2.cwwws.com/DemoStore/Products/SMSharedUrl.aspx?SMPurl=" + ProductId +"&SMLId=" + GlobalFunctions.GetLanguageId().ToString() + 
                        //                         "&SMCId=" + GlobalFunctions.GetCurrencyId().ToString() + 
                        //                         "&SMCSymbol="+ GlobalFunctions.GetCurrencySymbol() + 
                        //                         "&IsReferrerSM=true "  ;
                        //else
                        //    strSMUrlToShare = host + "Products/SMSharedUrl.aspx?SMPid=" + ProductId + "&SMLId=" + GlobalFunctions.GetLanguageId().ToString() +
                        //                         "&SMCId=" + GlobalFunctions.GetCurrencyId().ToString() +
                        //                         "&SMCSymbol=" + GlobalFunctions.GetCurrencySymbol() +
                        //                         "&IsReferrerSM=true ";

                        //strSocialMediaShortUrl = GlobalFunctions.GetGoogleURLForSocialMedia(strSMUrlToShare); 
                        #endregion
                        strSocialMediaShortUrl = GlobalFunctions.GetBitlyURLForSocialMedia(strSMUrlToShare);
                        /*Sachin Chauhan End : 04 02 2015*/
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableRecentlyViewedProducts").FeatureValues[0].IsEnabled)
                    { blnShowRecentlyViewedProducts = true; }
                    //if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableYouMayAlsoLike").FeatureValues[0].IsEnabled)
                    ////if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableYouMayAlsoLike").IsActive)
                    //{ blnShowYouMayAsloLike = true; }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableYouMayAlsoLike").FeatureValues.FindAll(v => v.IsEnabled == true).Count > 0)
                    { blnShowYouMayAsloLike = true; }

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableWishlist").FeatureValues[0].IsEnabled)
                    { blnShowWishlist = true; }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableReviewRating").FeatureValues[0].IsEnabled)
                    { blnShowRatingsReview = true; }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableSavePDF").FeatureValues[0].IsEnabled)
                    { blnShowSavePDF = true; }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableSectionIcons").FeatureValues[0].IsEnabled)
                    { blnShowSectionsIcons = true; }
                    #endregion
                }
                ShowHidePageComponents();
                #region "Comment"
                //if (!IsPostBack)
                //{
                //    if (Session["tempProductId"] != null)
                //    {
                //        objCurrentProductBE = ProductBL.GetProductDetails(Convert.ToInt32(Session["tempProductId"]), intLanguageId, intCurrencyId);
                //        Session["objCurrentProductBE"] = objCurrentProductBE;
                //    }
                //    else if (ProductId != 0)
                //    {
                //        objCurrentProductBE = ProductBL.GetProductDetails(ProductId, intLanguageId, intCurrencyId);
                //        Session["objCurrentProductBE"] = objCurrentProductBE;
                //    }
                //    else
                //        Response.RedirectToRoute("PageNotFound");
                //}
                //else
                //{ 
                #endregion
                if (ProductId != 0)
                {
                    objCurrentProductBE = ProductBL.GetProductDetails(ProductId, intLanguageId, intCurrencyId, iUserTypeID);/*User Type*/
                    Session["objCurrentProductBE"] = objCurrentProductBE;
                }
                else
                    Response.RedirectToRoute("PageNotFound");
                //}
                if (!IsPostBack)
                {
                    bool Result;
                    if (Session["UserId"] != null)
                        Result = ProductBL.AddRecentlyViewedProducts(ProductId, 1, HttpContext.Current.Session.SessionID);
                    else
                        Result = ProductBL.AddRecentlyViewedProducts(ProductId, 0, HttpContext.Current.Session.SessionID);
                }
                if (objCurrentProductBE != null)
                {
                    try
                    {
                        #region
                        spnProductCode.InnerHtml = objCurrentProductBE.ProductCode.Trim();
                        /*Sachin Chauhan Start : 08 01 2015 : Changed Displayed Product Name Which Is Language Specific */
                        //spnProductName.InnerHtml = objProductBE.ProductName.Trim();
                        spnProductName.InnerHtml = objCurrentProductBE.FurtherDescription.Trim();
                        Session["VName"] = spnProductName.InnerHtml;

                        /*Sachin Chauhan End : 08 01 2015*/
                        spnProductDescription.InnerHtml = Convert.ToString(objCurrentProductBE.ProductDescription).Trim();
                        //Page.Title = strProductName;
                        Page.Title = objCurrentProductBE.PageTitle.Trim();
                        Page.MetaDescription = objCurrentProductBE.MetaDescription.Trim();
                        Page.MetaKeywords = objCurrentProductBE.MetaKeyword.Trim();

                        if (!string.IsNullOrEmpty(objCurrentProductBE.SectionIds))
                        {
                            string strSectionIds = Convert.ToString(objCurrentProductBE.SectionIds);
                            string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            string strSectionImageData = string.Empty;
                            string strSectionNames = string.Empty;
                            for (int i = 0; i < strSections.Length; i++)
                            {
                                string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                                { strSectionImageData += "<span><img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='SectionImage product_list_icon'></span>"; }
                                if (strSectionIcon.Length > 1)
                                    strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                            }
                            dvSectionIcons.InnerHtml = "<p>" + strSectionImageData + "</p>";
                        }
                        else
                        { dvSectionIcons.Visible = false; }

                        //Bind Product SKU
                        if (!IsPostBack)
                        {
                            #region "IsPostBack"
                            ltrRatingText.Visible = false;
                            //Bind Product Detail Tables
                            BindProductDetailTable(objCurrentProductBE);

                            //Bind Product Variants
                            BindProductVariants(objCurrentProductBE);
                            //Bind Product Attributes
                            BindProductAttributes();
                            //Bind Social Media Icons
                            if (blnShowSocialMediaIcons)
                            { BindSocialMediaIcons(); }
                            rptReportSKU.DataSource = objCurrentProductBE.PropGetAllProductSKU;
                            rptReportSKU.DataBind();

                            #endregion
                        }
                        //Bind Product Price Table
                        BindProductPriceTable(objCurrentProductBE);




                        #endregion
                    }
                    catch (Exception ex)
                    { Exceptions.WriteExceptionLog(ex); }
                }

                if (Session["Add"] != null)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowBasket();", true);
                    Session["Add"] = null;
                }

                #endregion
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void RedirectToProduct(int intProductId)
        {
            try
            {
                #region
                /*Sachin Chauhan : 22 09 2015 : Redirect user to Product details page with language change in place from drop down*/
                List<ProductBE> lstProductNameAllLangauge;
                lstProductNameAllLangauge = ProductBL.GetProductNameAllLanguages(intProductId);
                Dictionary<int, List<CategoryBE>> objDictAllCategoriesAllLanguages;
                objDictAllCategoriesAllLanguages = CategoryBL.GetAllCategoriesAllLanguages();
                if (Request.Url.Equals(Request.UrlReferrer))
                {
                    string[] urlParts = Request.Url.ToString().Split('/');
                    Int16 urlOffset = 0;
                    for (Int16 i = 0; i < urlParts.Length - 1; i++)
                    {
                        if (urlParts[i].ToLower().Contains("details"))
                        {
                            urlOffset = (Int16)(i + 1);
                            break;
                        }
                    }
                    // List<Int32> lstCatNameIndexes = new List<Int32>();

                    CategoryBE prevLangCategory = new CategoryBE();
                    CategoryBE currLangCategoryName = new CategoryBE();
                    List<CategoryBE> lstCatNamesPrevLang = new List<CategoryBE>();
                    List<CategoryBE> lstCategoryNamesPrevLang = new List<CategoryBE>();
                    for (Int16 i = urlOffset; i < urlParts.Length - 1; i++)
                    {
                        lstCategoryNamesPrevLang = objDictAllCategoriesAllLanguages[Session["PrevLanguageId"].To_Int32()];
                        /*Sachin Chauhan Start : 06 03 2016 : Changed logic of finding category name of previous language*/
                        prevLangCategory = lstCategoryNamesPrevLang.FirstOrDefault(x => x.CategoryName.Equals(urlParts[i]));
                        lstCatNamesPrevLang.Add(prevLangCategory);
                        /*Sachin Chauhan End : 06 03 2016*/
                    }

                    List<CategoryBE> lstCategoryNamesCurrLang = objDictAllCategoriesAllLanguages[GlobalFunctions.GetLanguageId().To_Int32()];


                    /*Sachin Chauhan Start : 06 03 2016*/
                    List<string> lstCatNamesCurrLang = new List<string>();
                    foreach (CategoryBE prevLangCat in lstCatNamesPrevLang)
                    {
                        lstCatNamesCurrLang.Add(lstCategoryNamesCurrLang.FirstOrDefault(x => x.CategoryId.Equals(prevLangCat.CategoryId)).CategoryName);
                    }
                    /*Sachin Chauhan End : 06 03 2016*/

                    string redirectUrl = "";
                    for (Int16 i = 0; i < urlOffset - 1; i++)
                    { redirectUrl += urlParts[i] + "/"; }
                    redirectUrl += "details/";
                    for (int i = 0; i < lstCatNamesCurrLang.Count; i++)
                    {
                        redirectUrl += GlobalFunctions.EncodeCategoryURL(lstCatNamesCurrLang[i]) + "/";
                    }
                    redirectUrl += lstProductNameAllLangauge.FirstOrDefault(x => x.LanguageId.Equals(intLanguageId)).ProductCode + "_" + lstProductNameAllLangauge.FirstOrDefault(x => x.LanguageId.Equals(intLanguageId)).ProductName;
                    Session["PrevLanguageId"] = intLanguageId;
                    Response.Redirect(redirectUrl.Trim());
                }
                #endregion
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindProductAttributes()
        {
            List<ProductBE.ProductAttributes> lstProductAttributes;
            List<ProductBE.ProductSize> lstProductSize;
            List<ProductBE.ProductElements> lstProductElements;
            List<ProductBE.ProductUOM> lstProductUOM;
            StringBuilder sbProductSize = new StringBuilder();
            try
            {
                lstProductAttributes = new List<ProductBE.ProductAttributes>();
                lstProductAttributes = ProductBL.GetProductAttributeManagementByProductId<ProductBE.ProductAttributes>(ProductId);
                lstProductSize = new List<ProductBE.ProductSize>();
                lstProductSize = ProductBL.GetAllProductAttributesByType<ProductBE.ProductSize>(ProductAttributeType.Size, intLanguageId);
                lstProductElements = new List<ProductBE.ProductElements>();
                lstProductElements = ProductBL.GetAllProductAttributesByType<ProductBE.ProductElements>(ProductAttributeType.Element, intLanguageId);
                lstProductUOM = new List<ProductBE.ProductUOM>();
                lstProductUOM = ProductBL.GetAllProductAttributesByType<ProductBE.ProductUOM>(ProductAttributeType.UOM, intLanguageId);
                if (lstProductAttributes != null && lstProductSize != null && lstProductElements != null && lstProductUOM != null)
                {
                    if (lstProductAttributes.Count > 0 && lstProductSize.Count > 0 && lstProductElements.Count > 0 && lstProductUOM.Count > 0)
                    {

                        if (lstProductAttributes[0].SizeId != 0)
                        {

                            sbProductSize.Append("<div class='product_sizes_guide customTableHead'>");
                            sbProductSize.Append("" + strSizeGuideTitle + "");
                            sbProductSize.Append("</div>");
                            sbProductSize.Append("<div class='product_size_table'>");
                            sbProductSize.Append("<table class='table table-bordered  customTableText customTable'>");
                            sbProductSize.Append("<tr>");
                            sbProductSize.Append("<td>" + lstProductElements.Find(x => x.ElementId == lstProductAttributes[0].ElementId1).Element + "</td>");
                            for (int i = 0; i < lstProductAttributes.Count; i++)
                            {
                                sbProductSize.Append("<td>" + lstProductSize.Find(x => x.SizeId == lstProductAttributes[i].SizeId).Size + "</td>");
                            }
                            sbProductSize.Append("</tr>");
                            sbProductSize.Append("<tr>");
                            sbProductSize.Append("<td>" + lstProductUOM.Find(x => x.UOMId == lstProductAttributes[0].UOMIdA1).UOM + "</td>");
                            for (int i = 0; i < lstProductAttributes.Count; i++)
                            {
                                sbProductSize.Append("<td>" + lstProductAttributes[i].ValueA1 + "</td>");
                            }
                            sbProductSize.Append("</tr>");
                            if (lstProductAttributes[0].UOMIdA2 != 0)
                            {
                                sbProductSize.Append("<tr>");
                                sbProductSize.Append("<td>" + lstProductUOM.Find(x => x.UOMId == lstProductAttributes[0].UOMIdA2).UOM + "</td>");
                                for (int i = 0; i < lstProductAttributes.Count; i++)
                                {
                                    sbProductSize.Append("<td>" + lstProductAttributes[i].ValueA2 + "</td>");
                                }
                                sbProductSize.Append("</tr>");
                            }
                            sbProductSize.Append("</table>");
                            sbProductSize.Append("</div>");

                            if (lstProductAttributes[0].ElementId2 != 0)
                            {
                                #region "ElementId2"
                                sbProductSize.Append("<div class='product_size_table'>");
                                sbProductSize.Append("<table class='table table-bordered  customTableText customTable'>");
                                sbProductSize.Append("<tr>");
                                sbProductSize.Append("<td>" + lstProductElements.Find(x => x.ElementId == lstProductAttributes[0].ElementId2).Element + "</td>");
                                for (int i = 0; i < lstProductAttributes.Count; i++)
                                {
                                    sbProductSize.Append("<td>" + lstProductSize.Find(x => x.SizeId == lstProductAttributes[i].SizeId).Size + "</td>");
                                }
                                sbProductSize.Append("</tr>");
                                sbProductSize.Append("<tr>");
                                sbProductSize.Append("<td>" + lstProductUOM.Find(x => x.UOMId == lstProductAttributes[0].UOMIdB1).UOM + "</td>");
                                for (int i = 0; i < lstProductAttributes.Count; i++)
                                {
                                    sbProductSize.Append("<td>" + lstProductAttributes[i].ValueB1 + "</td>");
                                }
                                sbProductSize.Append("</tr>");
                                if (lstProductAttributes[0].UOMIdB2 != 0)
                                {
                                    sbProductSize.Append("<tr>");
                                    sbProductSize.Append("<td>" + lstProductUOM.Find(x => x.UOMId == lstProductAttributes[0].UOMIdB2).UOM + "</td>");
                                    for (int i = 0; i < lstProductAttributes.Count; i++)
                                    {
                                        sbProductSize.Append("<td>" + lstProductAttributes[i].ValueB2 + "</td>");
                                    }
                                    sbProductSize.Append("</tr>");
                                }
                                sbProductSize.Append("</table>");
                                sbProductSize.Append("</div>");
                                #endregion
                            }

                            dvProductSize.InnerHtml = sbProductSize.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                lstProductAttributes = null;
                lstProductSize = null;
                lstProductElements = null;
                lstProductUOM = null;
            }
        }

        private void ShowHidePageComponents()
        {
            try
            {
                if (blnShowRatingsReview)
                    dvReviewRating.Visible = true;
                if (blnShowSocialMediaIcons)
                    dvSocialMedia.Visible = true;
                if (blnShowRecentlyViewedProducts)
                    dvRecentlyViewContainer.Visible = true;
                if (blnShowYouMayAsloLike)
                    dvYouMayAlsoLike.Visible = true;
                if (blnShowDownloadImage)
                    lstSaveImage.Visible = true;
                if (blnShowEmail)
                    lstEmail.Visible = true;
                if (blnShowPrint)
                    lstPrint.Visible = true;
                if (blnShowWishlist)
                {
                    if (Session["User"] != null)
                    {
                        UserBE objUserBE = (UserBE)Session["User"];
                        int intUserId = objUserBE.UserId;
                        if (intUserId > 0)
                            lstWishlist.Visible = true;
                    }

                }
                if (blnShowSavePDF)
                    lstSavePDF.Visible = true;
                if (blnShowSectionsIcons)
                    dvSectionIcons.Visible = true;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        #region Recently Viewed Products
        //private void BindRecentlyViewedProducts()
        //{
        //    List<ProductBE.RecentlyViewedProducts> lstRecentlyViewedProducts = new List<ProductBE.RecentlyViewedProducts>();
        //    try
        //    {
        //        if (Session["User"] != null)
        //            lstRecentlyViewedProducts = ProductBL.GetRecentlyViewedProducts(UserId, HttpContext.Current.Session.SessionID,ProductId ,intCurrencyId, intLanguageId);
        //        else
        //            lstRecentlyViewedProducts = ProductBL.GetRecentlyViewedProducts(0, HttpContext.Current.Session.SessionID, ProductId,intCurrencyId, intLanguageId);

        //        if(lstRecentlyViewedProducts != null)
        //        {
        //            if(lstRecentlyViewedProducts.Count > 0)
        //            {
        //                rptRecentlyViewed.DataSource = lstRecentlyViewedProducts;
        //                rptRecentlyViewed.DataBind();
        //            }
        //            else
        //            {
        //                dvRecentlyViewContainer.Visible = false;
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //}

        //protected void rptRecentlyViewed_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if(e.Item.ItemType== ListItemType.Item || e.Item.ItemType== ListItemType.AlternatingItem)
        //    {
        //        HtmlImage imgRecentlyViewedProduct = (HtmlImage)e.Item.FindControl("imgRecentlyViewedProduct");
        //        HtmlAnchor lnkProductImage = (HtmlAnchor)e.Item.FindControl("lnkProductImage");
        //        HtmlAnchor lnkProductName = (HtmlAnchor)e.Item.FindControl("lnkProductName");
        //        string strCategoryName = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).CategoryName);
        //        string strSubCategoryName = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).SubCategoryName);
        //        string strSubSubCategoryName = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).SubSubCategoryName);
        //        string strProductName = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).ProductName);
        //        if (!string.IsNullOrEmpty(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).DefaultImageName))
        //        {
        //            if (IsExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).DefaultImageName).Trim()))
        //            {
        //                imgRecentlyViewedProduct.Src = GlobalFunctions.GetMediumProductImagePath() + ((ProductBE.RecentlyViewedProducts)e.Item.DataItem).DefaultImageName.Trim();
        //                imgRecentlyViewedProduct.Alt = ((ProductBE.ProductVariants)e.Item.DataItem).ImageName.Trim();
        //                imgRecentlyViewedProduct.Attributes.Add("title", ((ProductBE.ProductVariants)e.Item.DataItem).ImageName.Trim());
        //            }
        //            else
        //            {
        //                imgRecentlyViewedProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
        //                imgRecentlyViewedProduct.Alt = "Default";
        //                imgRecentlyViewedProduct.Attributes.Add("title", "Default");

        //            }

        //        }
        //        else
        //        {
        //            imgRecentlyViewedProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
        //            imgRecentlyViewedProduct.Alt = "Default";
        //            imgRecentlyViewedProduct.Attributes.Add("title", "Default");

        //        }

        //        if(!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
        //        {
        //            if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
        //            {
        //                lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);
        //                lnkProductName.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);
        //            }
        //            else if(strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
        //            {
        //                lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);
        //                lnkProductName.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);
        //            }
        //            else
        //            {
        //                lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);
        //                lnkProductName.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductName);
        //            }
        //        }


        //    }

        //}
        #endregion

        #region Bind Product Variants
        private void BindProductVariants(ProductBE objProductBE)
        {
            List<ProductBE.ProductColorVariantMappping> lstProductColorVarinatMapping = new List<ProductBE.ProductColorVariantMappping>();
            try
            {
                if (objProductBE != null)
                {
                    #region Commented by Sripal
                    if (!string.IsNullOrEmpty(objProductBE.DefaultImageName))
                    {
                        if (IsExists(GlobalFunctions.GetPhysicalLargeProductImagePath() + Convert.ToString(objProductBE.DefaultImageName.Trim())))
                        {
                            imgProduct.Src = GlobalFunctions.GetLargeProductImagePath() + objProductBE.DefaultImageName.Trim();
                            hdnimagepath.Value = GlobalFunctions.GetLargeProductImagePath() + objProductBE.DefaultImageName.Trim();
                            imgProduct.Alt = objProductBE.DefaultImageName.Trim();
                            imgProduct.Attributes.Add("title", objProductBE.DefaultImageName.Trim());
                            hdnImageName.Value = objProductBE.DefaultImageName.Trim();
                        }
                        else
                        {
                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                            imgProduct.Attributes.Add("title", "Default");
                        }
                    }
                    else
                    {
                        imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                        imgProduct.Attributes.Add("title", "Default");
                    }
                    #endregion
                }


                List<ProductBE.ProductSKU> lstInsertedSKU = new List<ProductBE.ProductSKU>();


                if (objCurrentProductBE.PropGetAllProductSKU.Count > 0)
                {
                    Int16 intInsertAt = 0;

                    ProductBE.ProductSKU prodSKU = objCurrentProductBE.PropGetAllProductSKU.FirstOrDefault(x => x.SKU.Trim().ToLower().Equals(x.ProductCode.Trim().ToLower() + "s"));
                    if (prodSKU != null)
                    {
                        lstInsertedSKU.Insert(intInsertAt, prodSKU);
                        intInsertAt += 1;
                    }

                    prodSKU = objCurrentProductBE.PropGetAllProductSKU.FirstOrDefault(x => x.SKU.Trim().ToLower().Equals(x.ProductCode.Trim().ToLower() + "m"));
                    if (prodSKU != null)
                    {
                        lstInsertedSKU.Insert(intInsertAt, prodSKU);
                        intInsertAt += 1;
                    }

                    prodSKU = objCurrentProductBE.PropGetAllProductSKU.FirstOrDefault(x => x.SKU.Trim().ToLower().Equals(x.ProductCode.Trim().ToLower() + "l"));
                    if (prodSKU != null)
                    {
                        lstInsertedSKU.Insert(intInsertAt, prodSKU);
                        intInsertAt += 1;
                    }

                    prodSKU = objCurrentProductBE.PropGetAllProductSKU.FirstOrDefault(x => x.SKU.Trim().ToLower().Equals(x.ProductCode.Trim().ToLower() + "xl"));
                    if (prodSKU != null)
                    {
                        lstInsertedSKU.Insert(intInsertAt, prodSKU);
                        intInsertAt += 1;
                    }


                    prodSKU = objCurrentProductBE.PropGetAllProductSKU.FirstOrDefault(x => x.SKU.Trim().ToLower().Equals(x.ProductCode.Trim().ToLower() + "xxl"));
                    if (prodSKU != null)
                    {
                        lstInsertedSKU.Insert(intInsertAt, prodSKU);
                        intInsertAt += 1;
                    }

                    prodSKU = objCurrentProductBE.PropGetAllProductSKU.FirstOrDefault(x => x.SKU.Trim().ToLower().Equals(x.ProductCode.Trim().ToLower() + "xxxl"));
                    if (prodSKU != null)
                    {
                        lstInsertedSKU.Insert(intInsertAt, prodSKU);
                        intInsertAt += 1;
                    }

                }

                if (lstInsertedSKU.Count > 0)
                    objCurrentProductBE.PropGetAllProductSKU = lstInsertedSKU;

                if (objProductBE.PropGetAllVariantType.Count > 0 && objProductBE.PropGetAllProductVariants.Count > 0)
                {
                    //Bind Color Image Near Product Odering Table

                    string strImgName = string.Empty;
                    string strName = string.Empty;
                    try
                    {
                        strImgName = objProductBE.PropGetAllProductVariants.FirstOrDefault(x => x.ProductVariantTypeId == 1 && x.ProductId == ProductId).ImageName;
                        strName = objProductBE.PropGetAllProductVariants.FirstOrDefault(x => x.ProductVariantTypeId == 1 && x.ProductId == ProductId).ProductVariantName;
                    }
                    catch (Exception) { }

                    #region Commented by Sripal
                    //if (!string.IsNullOrEmpty(strImgName))
                    //{
                    //    if (IsExists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + strImgName.Trim()))
                    //    {
                    //        product_colour_selected.Src = GlobalFunctions.GetThumbnailProductImagePath() + strImgName.Trim();
                    //        product_colour_selected.Alt = strName;
                    //        product_colour_selected.Attributes.Add("title", strName);
                    //    }
                    //    else
                    //    {
                    //        product_colour_selected.Attributes.Add("style", "display:none");
                    //    }
                    //}
                    //else
                    //{
                    //    product_colour_selected.Attributes.Add("style", "display:none");
                    //} 
                    #endregion

                    List<ProductBE.ProductVariantType> lstDistinctProductVariantType = new List<ProductBE.ProductVariantType>();
                    lstDistinctProductVariantType = objProductBE.PropGetAllVariantType.GroupBy(x => x.ProductVariantTypeName).Select(x => x.First()).ToList();
                    if (lstDistinctProductVariantType.Count > 0)
                    {
                        rptVariantTypeList.DataSource = lstDistinctProductVariantType;
                        rptVariantTypeList.DataBind();

                        #region "Comment"
                        //for(int i=0;i<lstDistinctProductVariantType.Count;i++)
                        //{
                        //    //rptVariantList = new Repeater();
                        //    List<ProductBE.ProductVariants> lstProductVariant = new List<ProductBE.ProductVariants>();
                        //    lstProductVariant = objProductBE.PropGetAllProductVariants.FindAll(x => x.ProductVariantTypeId == lstDistinctProductVariantType[i].ProductVariantTypeId);
                        //    rptVariantList.DataSource = lstProductVariant;
                        //    rptVariantList.DataBind();
                        //} 
                        #endregion

                        #region Added by Sripal

                        List<ProductBE.ProductVariants> lstProductVariant = new List<ProductBE.ProductVariants>();

                        if (Session["SessionSwatches"] != null)
                        {
                            lstProductVariant = (List<ProductBE.ProductVariants>)Session["SessionSwatches"];
                            if (lstProductVariant.Count > 0)
                            {
                                int iPdt = 0;
                                if (Session["tempProductId"] != null)
                                {
                                    iPdt = Convert.ToInt32(Session["tempProductId"]);
                                }
                                else if (ProductId > 0)
                                {
                                    iPdt = Convert.ToInt32(ProductId);
                                }
                                string strImgName1 = lstProductVariant.FirstOrDefault(x => x.ProductId == iPdt).ImageName;
                                string strTil = lstProductVariant.FirstOrDefault(x => x.ProductId == iPdt).ProductVariantName;
                                product_colour_selected.Src = GlobalFunctions.GetThumbnailProductImagePath() + strImgName1.Trim();
                                product_colour_selected.Alt = strImgName1;
                                product_colour_selected.Attributes.Add("title", strTil);

                                //if (IsExists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + Convert.ToString(((ProductBE.ProductVariants)e.Item.DataItem).ImageName).Trim()))
                                //{
                                //    product_colour_selected.Src = GlobalFunctions.GetThumbnailProductImagePath() + ((ProductBE.ProductVariants)e.Item.DataItem).ImageName.Trim();
                                //    product_colour_selected.Alt = ((ProductBE.ProductVariants)e.Item.DataItem).ProductVariantName;
                                //    product_colour_selected.Attributes.Add("title", ((ProductBE.ProductVariants)e.Item.DataItem).ProductVariantName);
                                //}
                                //else
                                //{
                                //    product_colour_selected.Attributes.Add("style", "display:none");
                                //}
                            }
                            else
                            {
                                product_colour_selected.Attributes.Add("style", "display:none");
                            }

                        }
                        else
                        {
                            //liProductVariant.Attributes.Add("style", "display:none");
                            product_colour_selected.Attributes.Add("style", "display:none");
                        }

                        //List<ProductBE.ProductVariants> lstProductVariant = new List<ProductBE.ProductVariants>();
                        //lstProductVariant = (List<ProductBE.ProductVariants>)Session["lstProductVariant"];

                        //if (IsExists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + Convert.ToString(lstProductVariant[0].ImageName).Trim()))
                        //{
                        //    product_colour_selected.Src = GlobalFunctions.GetThumbnailProductImagePath() + strImgName.Trim();
                        //    product_colour_selected.Alt = strName;
                        //    product_colour_selected.Attributes.Add("title", strName);
                        //}
                        //else
                        //{
                        //    product_colour_selected.Attributes.Add("style", "display:none");
                        //}
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptVariantTypeList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater rptVariantList = e.Item.FindControl("rptVariantList") as Repeater;
                    List<ProductBE.ProductVariants> lstProductVariant = new List<ProductBE.ProductVariants>();
                    HtmlGenericControl productVariantTitle = e.Item.FindControl("productVariantTitle") as HtmlGenericControl;

                    lstProductVariant = objCurrentProductBE.PropGetAllProductVariants.FindAll(x => x.ProductVariantTypeId == (e.Item.DataItem as ProductBE.ProductVariantType).ProductVariantTypeId);
                    Session["SessionSwatches"] = lstProductVariant;

                    /*Sachin Chauhan Start : 20 01 2015 : Only bind product variants if available images are more then one 
                     *                                    and also only for product variant type which is 1 i.e. Product Images */
                    ProductBE.ProductVariantType objProductVariantType = e.Item.DataItem as ProductBE.ProductVariantType;

                    if (objProductVariantType.ProductVariantTypeId == 1)
                    {
                        foreach (ProductBE.ProductVariants objCurrProductVariant in lstProductVariant)
                        {
                            string strPdtImgName = spnProductCode.InnerHtml.ToLower();
                            strPdtImgName = strPdtImgName.Replace("*", "").Replace("**", "");

                            #region Commented by Sripal
                            //if (objCurrProductVariant.ImageName.ToLower().Contains(spnProductCode.InnerHtml.ToLower()))
                            //    intProductVariantImgCount += 1; 
                            #endregion

                            //if (objCurrProductVariant.ImageName.ToLower().Contains(strPdtImgName.ToLower()))
                            //    intProductVariantImgCount += 1;
                            //hardcode value for product variant image
                            intProductVariantImgCount = 1;

                        }
                        //if (intProductVariantImgCount > 1 && Session["objVariantProductBE"] == null)//Commented by Sripal
                        if (intProductVariantImgCount > 0 && Session["objVariantProductBE"] == null)
                        {
                            productVariantTitle.Visible = true;
                            rptVariantList.DataSource = lstProductVariant.Where(x => x.ProductId.Equals(ProductId)).ToList();
                            rptVariantList.DataBind();
                        }
                        //else if (intProductVariantImgCount > 1 && Session["objVariantProductBE"] != null)//Commented by Sripal
                        else if (intProductVariantImgCount > 0 && Session["objVariantProductBE"] != null)
                        {
                            productVariantTitle.Visible = true;
                            #region
                            //ProductBE.ProductVariants objVariantProductBE = Session["objVariantProductBE"] as ProductBE.ProductVariants;Commented by Sripal
                            //rptVariantList.DataSource = lstProductVariant.Where(x => x.ProductId.Equals(objVariantProductBE.ProductId)).ToList();Commented by Sripal

                            //ProductBE objVProductBE1 = Session["objVariantProductBE"] as ProductBE;
                            //List<ProductBE.ProductVariants> objProductVariants = objVProductBE1.PropGetAllProductVariants;
                            //List<ProductBE.ProductVariants> iPdt = objProductVariants.FindAll(y => y.ProductId == Convert.ToInt32(Session["tempProductId"]) && y.ProductVariantTypeId == 1).ToList();

                            #endregion
                            List<ProductBE.ProductVariants> obh = lstProductVariant.Where(x => x.ProductId == Convert.ToInt32(Session["tempProductId"]) && x.ProductVariantTypeId == 1).ToList();
                            rptVariantList.DataSource = obh;// lstProductVariant.Where(x => x.ProductId.Equals(objProductVariants.FindAll(y => y.ProductId == Convert.ToInt32(Session["tempProductId"])))).ToList();
                            rptVariantList.DataBind();
                        }
                        Session["lstProductVariant"] = lstProductVariant;
                    }
                    else
                    {
                        productVariantTitle.Visible = true;
                        rptVariantList.DataSource = lstProductVariant;
                        rptVariantList.DataBind();
                    }
                    /*Sachin Chauhan End : 20 01 2015*/
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptVariantList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlAnchor lnkVariant = (HtmlAnchor)e.Item.FindControl("lnkVariant");
                    //LinkButton lnkVariant = (LinkButton)e.Item.FindControl("lnkVariant");
                    HtmlImage imgVariant = (HtmlImage)e.Item.FindControl("imgVariant");
                    //HtmlGenericControl liProductVariant = (HtmlGenericControl)e.Item.FindControl("liProductVariant");
                    ProductBE.ProductVariants tempProductBE = (ProductBE.ProductVariants)e.Item.DataItem;

                    if (!string.IsNullOrEmpty(((ProductBE.ProductVariants)e.Item.DataItem).ImageName))
                    {
                        if (IsExists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + Convert.ToString(((ProductBE.ProductVariants)e.Item.DataItem).ImageName).Trim()))
                        {
                            imgVariant.Src = GlobalFunctions.GetThumbnailProductImagePath() + ((ProductBE.ProductVariants)e.Item.DataItem).ImageName.Trim();
                            imgVariant.Alt = ((ProductBE.ProductVariants)e.Item.DataItem).ProductVariantName;
                            imgVariant.Attributes.Add("title", ((ProductBE.ProductVariants)e.Item.DataItem).ProductVariantName);
                        }
                        else
                        {
                            //liProductVariant.Attributes.Add("style", "display:none");
                            imgVariant.Attributes.Add("style", "display:none");
                        }
                    }
                    else
                    {
                        //liProductVariant.Attributes.Add("style", "display:none");
                        imgVariant.Attributes.Add("style", "display:none");
                    }

                    string strZoomImgPath = "";
                    if (!string.IsNullOrEmpty(((ProductBE.ProductVariants)e.Item.DataItem).ImageName))
                    {
                        if (IsExists(GlobalFunctions.GetPhysicalLargeProductImagePath() + Convert.ToString(((ProductBE.ProductVariants)e.Item.DataItem).ImageName).Trim()))
                        {
                            strZoomImgPath = GlobalFunctions.GetLargeProductImagePath() + ((ProductBE.ProductVariants)e.Item.DataItem).ImageName.Trim();
                        }
                        else
                        {
                            strZoomImgPath = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                        }
                    }
                    else
                    {
                        strZoomImgPath = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                    }

                    //Added by Sripal For zoombox on clientside
                    if (tempProductBE.ProductVariantTypeId == 1)
                    {
                        lnkVariant.Attributes.Add("onclick", "return swap_zoomed('" + strZoomImgPath + "')");
                        lnkVariant.Disabled = true;
                    }

                    if (tempProductBE.ProductVariantTypeId != 1)
                    {
                        //lnkVariant.Attributes.Add("onclick", "return swap_zoomed('" + strZoomImgPath + "')");
                        //liProductVariant.Attributes.Add("class", "product_thumbnails product_swatch");

                        imgVariant.Attributes.Add("class", "thumbnails");
                        if (ProductId > 0 && e.Item.ItemIndex == 0 && Session["objVariantProductBE"] == null)
                            imgVariant.Attributes.Add("class", "thumbnails active");
                        if (Session["objVariantProductBE"] != null)
                        {
                            ProductBE.ProductVariants currVariantProduct = e.Item.DataItem as ProductBE.ProductVariants;
                            //List<ProductBE.ProductVariants> lstProductVariant = Session["lstProductVariant"] as List<ProductBE.ProductVariants> ;
                            if (Session["tempProductId"] != null)
                            {
                                Int16 currClickedVariantProductId = Session["tempProductId"].To_Int16();
                                if (currVariantProduct.ProductId.Equals(currClickedVariantProductId))
                                    imgVariant.Attributes.Add("class", "thumbnails active");
                            }
                        }
                        //liProductVariant.Attributes.Add("class", "product_thumbnails");
                    }
                    else
                    {
                        //liProductVariant.Attributes.Add("class", "product_swatch");
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void lnkVariant_ServerClick(object sender, EventArgs e)
        {
            HtmlAnchor lnkVariant = (HtmlAnchor)sender;
            //ProductBE objVariantProductBE;
            try
            {
                /*Sachin Chauhan Start : 23 01 2015 : Added variant type id to REL attribute of anchor tag to identify which variant type is getting clicked */
                string[] relVal = lnkVariant.Attributes["rel"].ToString().Split('|');

                int tempProductId = Convert.ToInt32(relVal[0]);
                Int16 variantTypeId = Convert.ToInt16(relVal[1]);
                /**/
                lnkVariant.Attributes.Add("class", "thumbnails active");

                if (ProductId != 0)
                {
                    //objVariantProductBE = new ProductBE();
                    objVariantProductBE = ProductBL.GetProductDetails(tempProductId, intLanguageId, intCurrencyId, iUserTypeID);/*User Type*/
                    Session["objVariantProductBE"] = objVariantProductBE;
                    Session["tempProductId"] = tempProductId;
                    if (objVariantProductBE.ProductId.Equals(ProductId))
                    {
                        Session["objVariantProductBE"] = null;
                        Session["tempProductId"] = null;
                    }

                    if (objVariantProductBE != null)
                    {
                        string strImgName = objVariantProductBE.PropGetAllProductVariants.FirstOrDefault(x => x.ProductVariantTypeId == 1 && x.ProductId == tempProductId).ImageName;
                        string strName = objVariantProductBE.PropGetAllProductVariants.FirstOrDefault(x => x.ProductVariantTypeId == 1 && x.ProductId == tempProductId).ProductVariantName;
                        if (!string.IsNullOrEmpty(strImgName))
                        {
                            if (IsExists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + strImgName.Trim()))
                            {
                                product_colour_selected.Src = GlobalFunctions.GetThumbnailProductImagePath() + strImgName.Trim();
                                product_colour_selected.Alt = strName;
                                product_colour_selected.Attributes.Add("title", strName);
                            }
                            else
                            {
                                product_colour_selected.Attributes.Add("style", "display:none");
                            }
                        }
                        else
                        {
                            product_colour_selected.Attributes.Add("style", "display:none");
                        }

                        if (objVariantProductBE.PropGetAllProductSKU.Count > 0)
                        {
                            /*Sachin Chauhan Start : 08 01 2015 : Populating details for the selected variant*/
                            try
                            {
                                spnProductCode.InnerHtml = objVariantProductBE.ProductCode.Trim();
                                /*Sachin Chauhan Start : 08 01 02015 : Changed Displayed Product Name Which Is Language Specific */
                                //spnProductName.InnerHtml = objProductBE.ProductName.Trim();
                                spnProductName.InnerHtml = objVariantProductBE.PageTitle.Trim();
                                Literal ltlBreadCrumb = Master.FindControl("ltlBreadCrumb") as Literal;
                                if (ltlBreadCrumb != null)
                                {
                                    if (Session["VName"] != null)
                                    {
                                        ltlBreadCrumb.Text = ltlBreadCrumb.Text.Replace(Convert.ToString(Session["VName"]), spnProductName.InnerHtml);
                                    }

                                }
                                /*Sachin Chauhan End : 08 01 2015*/
                                spnProductDescription.InnerHtml = Convert.ToString(objVariantProductBE.ProductDescription).Trim();
                                if (!string.IsNullOrEmpty(objVariantProductBE.SectionIds))
                                {
                                    string strSectionIds = Convert.ToString(objVariantProductBE.SectionIds);
                                    string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                                    string strSectionImageData = string.Empty;
                                    string strSectionNames = string.Empty;
                                    for (int i = 0; i < strSections.Length; i++)
                                    {
                                        string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                        if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                                        { strSectionImageData += "<span><img title='" + strSectionIcon[1].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='SectionImage product_list_icon'></span>"; }
                                        if (strSectionIcon.Length > 1)
                                            strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                                    }
                                    dvSectionIcons.InnerHtml = "<p>" + strSectionImageData + "</p>";
                                }
                                else
                                    dvSectionIcons.Visible = false;
                                //Bind Product SKU
                                //if (!IsPostBack)
                                //{
                                ltrRatingText.Visible = false;
                                //Bind Product Detail Tables
                                BindProductDetailTable(objVariantProductBE);

                                //Bind Product Variants
                                BindProductVariants(objVariantProductBE);

                                //Bind Recently Viewed Products
                                //BindRecentlyViewedProducts();
                                //Bind Product Attributes
                                BindProductAttributes();
                                //Bind Social Media Icons
                                if (blnShowSocialMediaIcons)
                                    BindSocialMediaIcons();

                                rptReportSKU.DataSource = objVariantProductBE.PropGetAllProductSKU;
                                rptReportSKU.DataBind();

                                //}
                                //Bind Product Price Table
                                BindProductPriceTable(objVariantProductBE);
                                //Check for Video Link
                                /*Sachin Chauhan Start : 22 01 15 : Changed below behavior to pick correct large image from clicked product variant */
                                if (!(objVariantProductBE.PropGetAllVariantType.Count <= 2 && objVariantProductBE.PropGetAllProductVariants.Count <= 2))
                                {
                                    if (variantTypeId != 2)
                                    {
                                        HtmlAnchor lnkClickedVariant = sender as HtmlAnchor;
                                        HtmlImage imgClickedVariant = lnkClickedVariant.Controls[1] as HtmlImage;

                                        String strDefaultImageName = string.Empty;
                                        if (imgClickedVariant != null)
                                            strDefaultImageName = imgClickedVariant.Src.Replace(host, "~/").Replace("Thumbnail", "Large");
                                        if (!string.IsNullOrEmpty(strDefaultImageName))
                                        {
                                            if (IsExists(Server.MapPath(strDefaultImageName)))
                                            {
                                                imgProduct.Src = strDefaultImageName;
                                                imgProduct.Alt = strDefaultImageName;
                                                imgProduct.Attributes.Add("title", strDefaultImageName);
                                                hdnImageName.Value = strDefaultImageName;
                                            }
                                            else
                                            {
                                                imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                                                imgProduct.Attributes.Add("title", "Default");
                                            }
                                        }
                                        else
                                        {
                                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                                            imgProduct.Attributes.Add("title", "Default");
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(objVariantProductBE.DefaultImageName))
                                        {
                                            if (IsExists(GlobalFunctions.GetPhysicalLargeProductImagePath() + Convert.ToString(objVariantProductBE.DefaultImageName.Trim())))
                                            {
                                                imgProduct.Src = GlobalFunctions.GetLargeProductImagePath() + objVariantProductBE.DefaultImageName.Trim();
                                                imgProduct.Alt = objVariantProductBE.DefaultImageName.Trim();
                                                imgProduct.Attributes.Add("title", objVariantProductBE.DefaultImageName.Trim());
                                                hdnImageName.Value = objVariantProductBE.DefaultImageName.Trim();
                                            }
                                            else
                                            {
                                                imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                                                imgProduct.Attributes.Add("title", "Default");
                                            }
                                        }
                                        else
                                        {
                                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                                            imgProduct.Attributes.Add("title", "Default");
                                        }
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(objVariantProductBE.DefaultImageName))
                                    {
                                        if (IsExists(GlobalFunctions.GetPhysicalLargeProductImagePath() + Convert.ToString(objVariantProductBE.DefaultImageName.Trim())))
                                        {
                                            imgProduct.Src = GlobalFunctions.GetLargeProductImagePath() + objVariantProductBE.DefaultImageName.Trim();
                                            imgProduct.Alt = objVariantProductBE.DefaultImageName.Trim();
                                            imgProduct.Attributes.Add("title", objVariantProductBE.DefaultImageName.Trim());
                                            hdnImageName.Value = objVariantProductBE.DefaultImageName.Trim();
                                        }
                                        else
                                        {
                                            imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                                            imgProduct.Attributes.Add("title", "Default");
                                        }
                                    }
                                    else
                                    {
                                        imgProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                                        imgProduct.Attributes.Add("title", "Default");
                                    }
                                }

                                /*Sachin Chauhan End : 22 01 15*/
                                #region "Commented by Sripal"
                                ///control is commented in aspx earlier so it was throwing error of Object reference
                                //if (string.IsNullOrEmpty(objVariantProductBE.VideoLink))
                                //{ aVideoBox.Visible = false; } 
                                #endregion
                            }
                            catch (Exception ex)
                            { Exceptions.WriteExceptionLog(ex); }
                            /*Sachin Chauhan End : 08 01 2015*/
                        }
                        else
                        {
                            GlobalFunctions.ShowModalAlertMessages(this.Page, strNoProductsFound, AlertType.Failure);
                        }
                    }
                    else
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErrorProcessingDataMsg, AlertType.Failure);
                    }
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strErrorProcessingDataMsg, AlertType.Failure);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objVariantProductBE = null;
            }
        }
        #endregion

        #region Bind Product Price Table
        private void BindProductPriceTable(ProductBE objProductBE)
        {
            try
            {
                if (objProductBE.PropGetAllPriceBreak.Count > 0 && objProductBE.PropGetAllPriceBreakDetails.Count > 0)
                {
                    if (objProductBE.PropGetAllPriceBreak[0].IsCallForPrice)
                    {
                        BindProductEnquiryFieldsConfiguration();
                        Session["IsCallForOrder"] = true;
                        dvProductEnquiry.Visible = true;
                        dvProductOrder.Visible = false;
                        if (objProductBE.PropGetAllPriceBreak.Count == 1 && objProductBE.PropGetAllPriceBreakDetails.Count == 1)
                        { }
                        else
                        {
                            ShowPriceTable(objProductBE);
                        }
                    }
                    else if (objProductBE.PropGetAllPriceBreak[0].IsShowPriceAndEnquiry)
                    {
                        BindProductEnquiryFieldsConfiguration();
                        Session["IsCallForOrder"] = true;
                        if (objProductBE.PropGetAllPriceBreak.Count == 1 && objProductBE.PropGetAllPriceBreakDetails.Count == 1)
                        { }
                        else
                        {
                            ShowPriceTable(objProductBE);
                        }
                        dvProductEnquiry.Visible = true;
                        dvProductOrder.Visible = false;
                    }
                    else
                    {
                        Session["IsCallForOrder"] = false;
                        if (objProductBE.PropGetAllPriceBreak.Count == 1 && objProductBE.PropGetAllPriceBreakDetails.Count == 1)
                        { }
                        else
                        {
                            ShowPriceTable(objProductBE);
                        }
                        dvProductEnquiry.Visible = false;
                        dvProductOrder.Visible = true;
                    }
                }
                else
                    dvProductPriceTable.Visible = false;

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void ShowPriceTable(ProductBE objProductBE)
        {
            StringBuilder sbProductPrice = new StringBuilder();
            string PriceNote = null;
            try
            {
                sbProductPrice.Append("<div class='productTableTitle customTableHead'>" + strPricingLabel + "</div>");
                sbProductPrice.Append("<div class='priceTable table-responsive'>");
                sbProductPrice.Append("<table class='table customTable'>");
                sbProductPrice.Append("<tbody>");
                for (int i = 0; i < objProductBE.PropGetAllPriceBreak.Count; i++)
                {

                    string strDiscountCode = null;
                    if (i == 0)
                    {
                        sbProductPrice.Append("<tr>");
                        sbProductPrice.Append("<td class=pageHighlightText>" + strQuantityLabel + "</td>");
                        for (int j = 0; j < objProductBE.PropGetAllPriceBreakDetails.FindAll(x => x.PriceBreakId == objProductBE.PropGetAllPriceBreak[i].PriceBreakId).Count; j++)
                        { sbProductPrice.Append("<td>" + objProductBE.PropGetAllPriceBreakDetails[j].BreakFrom + "</td>"); }
                        //sbProductPrice.Append("<td> </td>");
                        //sbProductPrice.Append("<td>" + strProductionTime + "</td>");
                        sbProductPrice.Append("</tr>");
                    }
                    sbProductPrice.Append("<tr>");
                    sbProductPrice.Append("<td class='pageHighlightText'>" + objProductBE.PropGetAllPriceBreak[i].PriceBreakName + "</td>");
                    List<ProductBE.PriceBreakDetails> lstPriceBreakDetail = objProductBE.PropGetAllPriceBreakDetails.FindAll(x => x.PriceBreakId == objProductBE.PropGetAllPriceBreak[i].PriceBreakId);
                    for (int j = 0; j < lstPriceBreakDetail.Count; j++)
                    {
                        //sbProductPrice.Append("<td>" + strCurrencySymbol + " " + lstPriceBreakDetail[j].Price);
                        DateTime dt = DateTime.Now;
                        string comparedate = dt.ToShortDateString();
                        DateTime date = (objCurrentProductBE.PropGetAllPriceBreak[i].ExpirationDate);
                        string expirationdate = date.ToShortDateString();
                        if (objProductBE.PropGetAllPriceBreak[i].EnableStrikePrice == true && Convert.ToDateTime(expirationdate) >= Convert.ToDateTime(comparedate))
                        {
                            //sbProductPrice.Append("<p><strike>" + strCurrencySymbol + " " + lstPriceBreakDetail[j].StrikePrice + "</strike></p></td>");

                            sbProductPrice.Append("<td><p><strike>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstPriceBreakDetail[j].Price), strCurrencySymbol, intLanguageId) + "</strike></p>");
                            sbProductPrice.Append(GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(lstPriceBreakDetail[j].StrikePrice), strCurrencySymbol, intLanguageId) + "</td>");
                        }
                        else
                        {
                            sbProductPrice.Append("<td>" + GlobalFunctions.DisplayPriceOrPoints(lstPriceBreakDetail[j].Price.ToString("#######0.#0"), strCurrencySymbol, intLanguageId));
                            sbProductPrice.Append("</td>");
                        }
                        strDiscountCode += lstPriceBreakDetail[j].DiscountCode;
                    }
                    //sbProductPrice.Append("<td>"+GlobalFunctions.FindFormattedDiscountCode(strDiscountCode)+"</td>");
                    //sbProductPrice.Append("<td>" + objProductBE.PropGetAllPriceBreak[i].ProductionTime + "</td>");
                    sbProductPrice.Append("</tr>");
                    if (!string.IsNullOrEmpty(objProductBE.PropGetAllPriceBreak[i].NoteText))
                    {
                        PriceNote = objProductBE.PropGetAllPriceBreak[i].NoteText;
                    }
                }
                sbProductPrice.Append("</tbody>");
                sbProductPrice.Append("</table>");
                sbProductPrice.Append("</div>");
                if (!string.IsNullOrEmpty(PriceNote))
                {
                    sbProductPrice.Append("<p class='productNote'><strong>" + strNote + " :</strong>" + PriceNote + "</p>");
                }
                dvProductPriceTable.InnerHtml = sbProductPrice.ToString();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindProductEnquiryFieldsConfiguration()
        {
            List<ProductBE.ProductEnquiryFieldsConfiguration> lstProductEnquiryFieldsConfig = new List<ProductBE.ProductEnquiryFieldsConfiguration>();
            try
            {
                lstProductEnquiryFieldsConfig = ProductBL.GetProductEnquiryFieldsConfiguration<ProductBE.ProductEnquiryFieldsConfiguration>(intLanguageId);
                if (lstProductEnquiryFieldsConfig != null)
                {
                    if (lstProductEnquiryFieldsConfig.Count > 0)
                    {
                        dvConfigurableFields.Visible = true;
                        Label lblEnquiryField = null;
                        TextBox txtEnquiryField = null;
                        RequiredFieldValidator rfvEnquiryField = null;
                        HtmlGenericControl span = null;
                        HtmlGenericControl div = null;
                        for (int i = 0; i < lstProductEnquiryFieldsConfig.Count; i++)
                        {
                            switch (lstProductEnquiryFieldsConfig[i].FieldTypeName.ToLower())
                            {
                                case "textbox":
                                    div = new HtmlGenericControl("div");
                                    div.Attributes.Add("class", "form-group");
                                    lblEnquiryField = new Label();
                                    lblEnquiryField.ID = "lbl" + lstProductEnquiryFieldsConfig[i].LabelTitle;
                                    lblEnquiryField.Attributes.Add("class", "customLabel");
                                    lblEnquiryField.Text = lstProductEnquiryFieldsConfig[i].LabelTitle + " :";
                                    dvConfigurableFields.Controls.Add(div);
                                    dvConfigurableFields.Controls.Add(lblEnquiryField);
                                    if (lstProductEnquiryFieldsConfig[i].IsMandatory)
                                    {
                                        span = new HtmlGenericControl("span");
                                        span.Attributes.Add("class", "text-danger");
                                        span.InnerText = "*";
                                        dvConfigurableFields.Controls.Add(span);
                                    }
                                    txtEnquiryField = new TextBox();
                                    txtEnquiryField.ID = "txt" + lstProductEnquiryFieldsConfig[i].LabelTitle;
                                    txtEnquiryField.Attributes.Add("class", "form-control input-sm customInput");
                                    dvConfigurableFields.Controls.Add(txtEnquiryField);
                                    if (lstProductEnquiryFieldsConfig[i].IsMandatory)
                                    {
                                        rfvEnquiryField = new RequiredFieldValidator();
                                        rfvEnquiryField.ControlToValidate = txtEnquiryField.ID.ToString();
                                        rfvEnquiryField.Text = strPleaseEnter + lstProductEnquiryFieldsConfig[i].LabelTitle;
                                        rfvEnquiryField.Attributes.Add("class", "text-danger errorMsg");
                                        rfvEnquiryField.ValidationGroup = "ValidateEnquiry";
                                        rfvEnquiryField.Display = ValidatorDisplay.Static;
                                        dvConfigurableFields.Controls.Add(rfvEnquiryField);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion

        #region Bind Product Detail Table
        private void BindProductDetailTable(ProductBE objProductBE)
        {
            StringBuilder sbProductDetail = new StringBuilder();
            try
            {
                if (objProductBE.PropGetAllProductDetails.Count > 0 && objProductBE.PropGetAllProductDetails.Count > 0)
                {
                    List<ProductBE.ProductDetailGroups> lstDistinctGroup = objProductBE.PropGetAllProductDetailGroups.GroupBy(x => x.ProductDetailGroupName).Select(x => x.First()).ToList();
                    if (lstDistinctGroup.Count > 0)
                    {
                        for (int i = 0; i < lstDistinctGroup.Count; i++)
                        {
                            sbProductDetail.Append("<div class='productInfoTable'>");
                            sbProductDetail.Append("<div class='productTableTitle customTableHead'>" + lstDistinctGroup[i].ProductDetailGroupName + "</div>");
                            sbProductDetail.Append(" <table class='table customTable'>");
                            sbProductDetail.Append("<tbody>");
                            List<ProductBE.ProductDetailGroupMappings> lstProductDetailGroupMapping = objProductBE.PropGetAllProductDetailGroupMappings.FindAll(x => x.ProductDetailGroupId == lstDistinctGroup[i].ProductDetailGroupId);
                            for (int j = 0; j < lstProductDetailGroupMapping.Count; j++)
                            {
                                sbProductDetail.Append("<tr>");
                                sbProductDetail.Append("<td class='pageHighlightText'>");
                                sbProductDetail.Append(lstProductDetailGroupMapping[j].DetailTitle);
                                sbProductDetail.Append("</td>");
                                sbProductDetail.Append("<td>");
                                sbProductDetail.Append(objProductBE.PropGetAllProductDetails.FirstOrDefault(x => x.ProductDetailGroupMappingId == lstProductDetailGroupMapping[j].ProductDetailGroupMappingId).DetailDescription);
                                sbProductDetail.Append("</td>");
                                sbProductDetail.Append("</tr>");
                            }
                            sbProductDetail.Append("</tbody>");
                            sbProductDetail.Append("</table>");
                            sbProductDetail.Append("</div>");
                        }
                    }
                    dvTableProductDetail.InnerHtml = sbProductDetail.ToString();
                }
                else
                    dvTableProductDetail.Visible = false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion

        #region Methods
        private bool IsExists(string FilePath)
        {
            try
            {
                return (File.Exists(FilePath));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 12-08-15
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        ltrheaderEmail.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_HeaderEmail").ResourceValue;
                        ltrSenderdetail.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_SenderDetail").ResourceValue;
                        ltrSenderName.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_SenderName").ResourceValue;
                        ltrSenderEmail.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_SenderEmail").ResourceValue;
                        ltrSenderMsg.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_SenderMsg").ResourceValue;
                        ltrReciepientdetail.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_Reciepientdetail").ResourceValue;
                        ltrRecipientName.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_ReciepientName").ResourceValue;
                        ltrRecipientEmail.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_ReciepientEmail").ResourceValue;

                        txtSenderName.Attributes["placeholder"] = ltrSenderdetail.Text;
                        txtSenderMsg.Attributes["placeholder"] = ltrSenderMsg.Text;
                        txtSenderEmail.Attributes["placeholder"] = ltrSenderEmail.Text;
                        txtReciepientName.Attributes["placeholder"] = ltrRecipientName.Text;
                        txtReciepientEmail.Attributes["placeholder"] = ltrReciepientdetail.Text;

                        strmailprodcodelabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_Codelabel").ResourceValue;
                        strmailprodNamelabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_Namelabel").ResourceValue;
                        strmailprodDesclabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_Desclabel").ResourceValue;
                        strmailfurtherDesclabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_FurtherDesclabel").ResourceValue;
                        strEmailThisProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailThisProduct_EmailAlert").ResourceValue;

                        //ltrTotalItems.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Pagination_TotalItems_Text").ResourceValue;
                        ltrDescription.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Description_Label").ResourceValue;
                        ltrDownloadImage.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_DownloadImage").ResourceValue;
                        ltrEmail.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Email").ResourceValue;
                        string PDFLabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_PDF").ResourceValue;
                        ltrPDF.Text = "<p>" + Convert.ToString(PDFLabel) + "</p>";
                        // ltrPlayVideo.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_PlayVideo").ResourceValue;
                        ltrPrint.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Print").ResourceValue;
                        strPricingLabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Price_Label").ResourceValue;
                        strProductionTime = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_ProductionTime_Label").ResourceValue;
                        strQuantityLabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        strNote = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Note_Label").ResourceValue;
                        ltrReviewHeading.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Modal_Title").ResourceValue;
                        lblEmailId.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "EmailId").ResourceValue;
                        lblReviewTitle.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Title").ResourceValue;
                        lblReviewComment.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Commnets").ResourceValue;
                        lblRating.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Rating").ResourceValue;
                        ltrRatingText.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Rating0").ResourceValue;
                        strRatingErrorMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Rating0").ResourceValue;
                        strStarRating1 = HttpUtility.HtmlEncode(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Rating1").ResourceValue);
                        strStarRating2 = HttpUtility.HtmlEncode(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Rating2").ResourceValue);
                        strStarRating3 = HttpUtility.HtmlEncode(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Rating3").ResourceValue);
                        strStarRating4 = HttpUtility.HtmlEncode(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Rating4").ResourceValue);
                        strStarRating5 = HttpUtility.HtmlEncode(lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Rating5").ResourceValue);
                        spnLetterRemaining.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Letters_Remaining").ResourceValue;
                        spnWriteReview.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Write_Review").ResourceValue;
                        spnReadReview.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Read_Review").ResourceValue;
                        spnProductReviews.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Read_Review").ResourceValue;
                        spnreviewnext.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Next").ResourceValue;
                        spnreviewprev.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Previous").ResourceValue;
                        strReviewSuccessMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Success_Alert_Msg").ResourceValue;
                        strReviewFailureMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Failure_Alert_Msg").ResourceValue;
                        strReviewValidateEmail = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Validate_Email").ResourceValue;
                        strBasket_Quantity_Not_Zero_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Quantity_Not_Zero_Message").ResourceValue;
                        strBasket_Invalid_Quantity_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Invalid_Quantity_Message").ResourceValue;
                        strSizeGuideTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_SizeGuideTitle").ResourceValue;
                        strInStockTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_InStocktitle").ResourceValue;
                        strOutOfStotckTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_OutStockTitle").ResourceValue;
                        #region MyRegion Translations Added by SHRIGANESH SINGH 27 March 2016
                        SocialMediaText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_SocialMediaTitle").ResourceValue;
                        btnShopping.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Continue_Shopping_Text").ResourceValue;
                        imgbtnAddtoCart.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Add_To_Basket_Text").ResourceValue;
                        ltrEnquireText.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enquire_Text").ResourceValue;
                        ltrEnquireProductTitle.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Product_Enquiry_Title_Text").ResourceValue;
                        lblEmailAddress.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Details_Email_Address").ResourceValue;
                        rfvEmailAddress.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_EmailID_Message").ResourceValue;
                        lblQtyRequired.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        rxvEmailAddress.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Valid_Email_Address_Message").ResourceValue;
                        rfvQtyrequired.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;

                        rfvDeliveryDate.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue + " " + lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_DeliveryDate").ResourceValue;
                        lblLogo.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Attach_Logo_Message").ResourceValue;

                        strNoProductsFound = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_No_Product_SKU_Found_Message").ResourceValue;
                        strErrorProcessingDataMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Processing_Data_Message").ResourceValue;
                        strPleaseEnter = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Enter_Text").ResourceValue;
                        strLimitedProductMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Limited_Products_Message").ResourceValue;
                        strPDFCreationError = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_PDF_Creation_Error_Message").ResourceValue;
                        strInvalidFileType = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "File_Extension").ResourceValue;
                        strEmailThisProduct = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Email_This_Product_Text").ResourceValue;
                        strProductEnquiry = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Product_Enquiry_Text").ResourceValue;
                        strAddToWishList = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_AddToWishList").ResourceValue;
                        ltrEnquirySentMsg.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Equiry_Sent_Message").ResourceValue;
                        //txtEmailAddress.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Valid_Email_Address_Message").ResourceValue;
                        lblBranding.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_Branding").ResourceValue;
                        lblAdditionalInfo.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Details_Additional_Info_Text").ResourceValue;
                        btnSubmitEnquiry.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Submit_Enquiry_Text").ResourceValue;
                        SizeQuantityText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_SKU_Note").ResourceValue;
                        strProductTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Title").ResourceValue;
                        strStockTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Stock_Title_Text").ResourceValue;
                        strStockDueText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "StockDue_Title").ResourceValue;
                        strPriceTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Price_Title_Text").ResourceValue;
                        strQuantityTitle = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        rfvReviewEmailId.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_EmailID_Message").ResourceValue;
                        rxvReviewEmaiId.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Valid_Email_Address_Message").ResourceValue;
                        btnReviewSubmit.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Submit_Text").ResourceValue;
                        rfvEmail.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_EmailID_Message").ResourceValue;
                        revEmail.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Review_Validate_Email").ResourceValue;
                        rfvRecipientEmail.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Empty_Recipient_Email_Message").ResourceValue;
                        revRecipientEmail.ErrorMessage = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Valid_Recipient_Email_Message").ResourceValue;
                        btnSend.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Send_Text").ResourceValue;
                        btnCancel.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Cancel_Text").ResourceValue;
                        MinMaxProductQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Min_Max_Product_Message").ResourceValue;
                        AddProductQuantity = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Add_Quantity_Message").ResourceValue;
                        BlockUI_Message = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_BlockUI_Message").ResourceValue;
                        string QtyPlaceHolder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        txtQtyRequired.Attributes.Add("placeholder", QtyPlaceHolder);
                        string EmailPlaceHolder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Valid_Email_Address_Message").ResourceValue;
                        txtEmailAddress.Attributes.Add("placeholder", EmailPlaceHolder);
                        string DeliveryDatePlaceHolder = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_DeliveryDate").ResourceValue;
                        txtDeliveryDate.Attributes.Add("placeholder", DeliveryDatePlaceHolder);
                        lblDeliveryDate.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "CustomRequest_DeliveryDate").ResourceValue;
                        strLogoFileSize = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Invalid_Logo_FileSize_Message").ResourceValue;
                        Status_Title = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Status_Title").ResourceValue;
                        strUnitCost = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Basket_Unit_Cost_Text").ResourceValue;

                        #endregion
                        strWishListSaveSuccessMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "WishList_Add_Success_Msg").ResourceValue;
                        strGenericSaveFailMsg = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Save_Fail_Msg").ResourceValue;
                        Wish_Choose_SKU_Item_Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Wish_Choose_SKU_Item_Text").ResourceValue;
                        btnAdd.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Add").ResourceValue;
                        Close_Btn = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Close_Btn").ResourceValue;
                        //strStockDueDate = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_StockDueDate").ResourceValue;

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion

        #region Web Methods
        [WebMethod]
        public static string BindVideoBox(ProductBE objProductBE)
        {
            try
            {
                if (!string.IsNullOrEmpty(objProductBE.VideoLink))
                {
                    return ("<iframe width='100%' height='300' src='" + objProductBE.VideoLink + "' frameborder='0' allowfullscreen></iframe>").ToString();
                    //return "<iframe width='100%' height='300' src='http://www.youtube.com/v/GAwuurmFYKk' frameborder='0' allowfullscreen></iframe>";
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
        #endregion


        protected void rptReportSKU_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //if (bBackOrderAllowed == false && bBASysStore)
                    if (bBASysStore)
                    {
                        Int32 intProductid_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BASYSProductId")),
                            intBaseColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "BaseColorId")),
                            intTrimColorId_OASIS = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "TrimColorId"));

                        #region "Comment"
                        Stock objStock = new Stock();
                        NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                            ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                        objStock.Credentials = objNetworkCredentials;
                        StockDetails objStockDetails = new StockDetails();
                        objStockDetails = objStock.LevelEnquiry(intProductid_OASIS, intBaseColorId_OASIS, intTrimColorId_OASIS);

                        if (objStockDetails != null)
                        {
                            Label lblStock = e.Item.FindControl("lblStock") as Label;
                            Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                            Label lbldate = e.Item.FindControl("lblDate") as Label;
                            /*Sachin Chauhan Start : 04 01 15*/
                            intProdStockLevel = objStockDetails.StockLevel;
                            intProdStockLevel = intProdStockLevel <= 0 ? 0 : intProdStockLevel;
                            /*Sachin Chauhan Start : 18 02 2015 : Stock status language wise update*/
                            lblStatus.Text = intProdStockLevel <= 0 ? strOutOfStotckTitle : strInStockTitle;
                            /*Sachin Chauhan End : 18 02 2015*/
                            //HtmlInputHidden hdnStockStatus = (HtmlInputHidden)e.Item.FindControl("hdnStockStatus");
                            //hdnStockStatus.Value = Convert.ToString(intProdStockLevel);
                            lblStock.Text = intProdStockLevel.ToString();
                            /*Sachin Chauhan End : 04 01 15*/

                            stockduedate = objStockDetails.StockDueDate;
                            // lbldate.Text = stockduedate.ToString("dd/MM/yyyy");

                            if (intProdStockLevel > 0 && !stockduedate.HasValue)
                            {
                                lbldate.Text = strInStockTitle;
                            }
                            else if (intProdStockLevel <= 0 && !stockduedate.HasValue)
                            {

                                lbldate.Text = strOutOfStotckTitle;
                            }
                            else if (intProdStockLevel > 0 && stockduedate.HasValue)
                            {

                                lbldate.Text = (stockduedate != null ? stockduedate.Value.ToString("dd/MM/yyyy") : "n/a");
                            }
                            else if (intProdStockLevel <= 0 && stockduedate.HasValue)
                            {
                                lbldate.Text = (stockduedate != null ? stockduedate.Value.ToString("dd/MM/yyyy") : "n/a");
                            }


                        }
                        #endregion
                        HtmlInputHidden hdnStockStatus = (HtmlInputHidden)e.Item.FindControl("hdnStockStatus");
                        hdnStockStatus.Value = "150";
                    }
                    Label lblSKuCurrencySymbol = (Label)e.Item.FindControl("lblSKuCurrencySymbol");
                    Label lblSKuCurrencySymbolstrike = (Label)e.Item.FindControl("lblSKuCurrencySymbolstrike");
                    Label lblPrice = (Label)e.Item.FindControl("lblPrice");
                    Label lblstrickprice = (Label)e.Item.FindControl("lblstrickprice");
                    #region added by vikram for WishList
                    SKUCount++;
                    Label SKUName = (Label)e.Item.FindControl("lblProductName");
                    Label lblSKUId = (Label)e.Item.FindControl("lblSKUId");
                    System.Web.UI.WebControls.ListItem Data = new System.Web.UI.WebControls.ListItem(Convert.ToString(SKUName.Text), Convert.ToString(lblSKUId.Text));
                    ddlSKU.Items.Add(Data);
                    if (SKUCount > 1)
                    {
                        hdnSKUCount.Value = Convert.ToString(SKUCount);
                    }
                    else
                    {
                        hdnSKUCount.Value = "1";
                        HiddenField hdnSKUId = (HiddenField)e.Item.FindControl("hdnSKUId");
                        hdnSKUIdAddWishList.Value = hdnSKUId.Value;
                    }
                    #endregion
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        lblSKuCurrencySymbol.Visible = false;
                    }
                    else
                    {
                        lblSKuCurrencySymbol.Text = GlobalFunctions.GetCurrencySymbol();
                    }
                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        lblSKuCurrencySymbolstrike.Visible = false;
                    }
                    else
                    {
                        lblSKuCurrencySymbolstrike.Text = "<strike>" + GlobalFunctions.GetCurrencySymbol() + "</strike>";
                    }
                    sstrPrice = "";
                    ddPrice = 0;
                    ddPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "MaxstrikePrice"));
                    sstrPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "MaxstrikePrice")).ToString("##,###,##0.#0");
                    string strickprice = GlobalFunctions.DisplayPriceOrPoints(sstrPrice, "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId);
                    //string strickprice = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MaxstrikePrice")), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId);
                    if (strickprice == "0" || strickprice == "" || ddPrice <= 0)
                    {
                        lblSKuCurrencySymbolstrike.Visible = false;
                        lblstrickprice.Visible = false;
                        sstrPrice = "";
                        sstrPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "MaxPrice")).ToString("##,###,##0.#0");
                        lblPrice.Text = GlobalFunctions.DisplayPriceOrPoints(sstrPrice, "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId);
                        //lblPrice.Text = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MaxPrice")), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId);


                    }
                    else
                    {
                        sstrPrice = "";
                        sstrPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "MaxPrice")).ToString("##,###,##0.#0");
                        lblPrice.Text = GlobalFunctions.DisplayPriceOrPoints(sstrPrice, "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId);

                        sstrPrice = "";
                        sstrPrice = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "MaxstrikePrice")).ToString("##,###,##0.#0");
                        lblstrickprice.Text = "<strike>" + GlobalFunctions.DisplayPriceOrPoints(sstrPrice, "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId) + "</strike>";
                        //lblPrice.Text = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MaxPrice")), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId);
                        //lblstrickprice.Text = "<strike>" + GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MaxstrikePrice")), "", intLanguageId) + GlobalFunctions.PointsText(intLanguageId) + "</strike>";


                    }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void imgbtnAddtoCart_Click(object sender, EventArgs e)
        {
            try
            {
                int intUserId = 0;
                string UserSessionId = HttpContext.Current.Session.SessionID;
                if (Session["User"] != null)
                {
                    UserBE objUserBE = (UserBE)Session["User"];
                    intUserId = objUserBE.UserId;
                }
                string strErrormessage = "";
                if (bBackOrderAllowed == false)
                {
                    #region "Validation"

                    for (var i = 0; i < rptReportSKU.Items.Count; i++)
                    {
                        TextBox txtQuantity = (TextBox)rptReportSKU.Items[i].FindControl("txtQuantity");
                        Label lblStock = (Label)rptReportSKU.Items[i].FindControl("lblStock");
                        HtmlInputHidden hdnStockStatus = (HtmlInputHidden)rptReportSKU.Items[i].FindControl("hdnStockStatus");
                        Label lblProductName = (Label)rptReportSKU.Items[i].FindControl("lblProductName");
                        if (txtQuantity.Text != "")
                        {
                            try
                            {
                                if (Convert.ToInt32(txtQuantity.Text) <= 0)
                                {
                                    strErrormessage = strBasket_Quantity_Not_Zero_Message;
                                }
                                if (!GlobalFunctions.isNumeric(txtQuantity.Text.Trim(), System.Globalization.NumberStyles.Integer))
                                { strErrormessage += "<br>" + strBasket_Invalid_Quantity_Message; }
                            }
                            catch (Exception) { strErrormessage = strBasket_Invalid_Quantity_Message; }
                            if (!string.IsNullOrEmpty(strErrormessage))
                            {
                                if (bBASysStore == false)
                                {
                                    #region
                                    if (Convert.ToInt32(txtQuantity.Text) > Convert.ToInt32(lblStock.Text))
                                    {
                                        txtQuantity.Text = "";
                                        txtQuantity.Style.Add("borderColor", "red");
                                        strErrormessage += lblProductName.Text + ": " + strLimitedProductMessage + lblStock.Text;
                                    }
                                    else
                                    { txtQuantity.Style.Add("borderColor", "#ccc"); }
                                    #endregion
                                }
                                else
                                {
                                    #region
                                    if (Convert.ToInt32(txtQuantity.Text) > Convert.ToInt32(hdnStockStatus.Value))
                                    {
                                        txtQuantity.Text = "";
                                        txtQuantity.Style.Add("borderColor", "red");
                                        strErrormessage += lblProductName.Text + ": " + strLimitedProductMessage + hdnStockStatus.Value;
                                    }
                                    else
                                    { txtQuantity.Style.Add("borderColor", "#ccc"); }
                                    #endregion
                                }
                            }
                        }
                    }
                    if (strErrormessage != "")
                    { GlobalFunctions.ShowModalAlertMessages(this.Page, strErrormessage, AlertType.Warning); return; }
                    #endregion
                }
                #region "Insert"
                foreach (RepeaterItem item in rptReportSKU.Items)
                {
                    TextBox txtQuantity = item.FindControl("txtQuantity") as TextBox;
                    Label lblStock = item.FindControl("lblStock") as Label;
                    Label lblProductId = item.FindControl("lblProductId") as Label;
                    HiddenField hdnSKUId = item.FindControl("hdnSKUId") as HiddenField;
                    Label lblProductName = item.FindControl("lblProductName") as Label;


                    if (txtQuantity.Text != "")
                    {
                        try
                        {
                            if (Convert.ToInt32(txtQuantity.Text) > 0)
                            {

                                if (bBackOrderAllowed == false)
                                {
                                    if (Convert.ToInt32(Sanitizer.GetSafeHtmlFragment(txtQuantity.Text)) > Convert.ToInt32(lblStock.Text))
                                    {
                                        strErrormessage += lblProductName.Text + ": " + strLimitedProductMessage + lblStock.Text;
                                        GlobalFunctions.ShowModalAlertMessages(this.Page, strErrormessage, AlertType.Warning); return;

                                    }
                                    else
                                    {

                                        Dictionary<string, string> dictionary = new Dictionary<string, string>();
                                        dictionary.Add("UserId", Convert.ToString(intUserId));
                                        dictionary.Add("ProductSKUId", hdnSKUId.Value);
                                        dictionary.Add("Quantity", txtQuantity.Text.Trim());
                                        dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
                                        dictionary.Add("UserSessionId", UserSessionId);
                                        int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                                        Session["Add"] = true;
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowBasket();", true);
                                    }
                                }
                                else
                                {

                                    Dictionary<string, string> dictionary = new Dictionary<string, string>();
                                    dictionary.Add("UserId", Convert.ToString(intUserId));
                                    dictionary.Add("ProductSKUId", hdnSKUId.Value);
                                    dictionary.Add("Quantity", txtQuantity.Text.Trim());
                                    dictionary.Add("CurrencyId", Convert.ToString(intCurrencyId));
                                    dictionary.Add("UserSessionId", UserSessionId);
                                    int shopId = ProductBL.InsertShoppingCartDetails(Constants.USP_InsertShoppingCartProducts, dictionary, true);
                                    Session["Add"] = true;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowBasket();", true);
                                }
                            }
                        }
                        catch (Exception) { }
                    }
                }
                #endregion
                Response.Redirect(Request.RawUrl);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void btnSubmitEnquiry_Click(object sender, EventArgs e)
        {
            ProductBE objProductBE = new ProductBE();
            List<ProductBE.ProductEnquiryFieldsConfiguration> lstProductEnquiryFieldsConfig = new List<ProductBE.ProductEnquiryFieldsConfiguration>();
            try
            {
                if (Session["IsCallForOrder"] != null)
                {
                    if (Convert.ToBoolean(Session["IsCallForOrder"]))
                    {
                        lstProductEnquiryFieldsConfig = ProductBL.GetProductEnquiryFieldsConfiguration<ProductBE.ProductEnquiryFieldsConfiguration>(intLanguageId);
                        if (lstProductEnquiryFieldsConfig != null)
                        {
                            if (lstProductEnquiryFieldsConfig.Count > 0)
                            {
                                for (int i = 0; i < lstProductEnquiryFieldsConfig.Count; i++)
                                {
                                    switch (lstProductEnquiryFieldsConfig[i].FieldTypeName.ToLower())
                                    {
                                        case "textbox":
                                            TextBox txtEnquiryField = (TextBox)dvConfigurableFields.FindControl("txt" + lstProductEnquiryFieldsConfig[i].LabelTitle);
                                            Type typ = objProductBE.PropProductEnquiry.GetType();
                                            PropertyInfo pi = typ.GetProperty(lstProductEnquiryFieldsConfig[i].SystemColumnName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                            pi.SetValue(objProductBE.PropProductEnquiry, txtEnquiryField.Text);
                                            break;

                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                objProductBE.PropProductEnquiry.UserId = UserId;
                objProductBE.PropProductEnquiry.ProductId = ProductId;
                objProductBE.PropProductEnquiry.EmailAddress = Sanitizer.GetSafeHtmlFragment(txtEmailAddress.Text);
                objProductBE.PropProductEnquiry.QuantityRequired = Sanitizer.GetSafeHtmlFragment(txtQtyRequired.Text);
                objProductBE.PropProductEnquiry.DeliveryDateRequired = Convert.ToDateTime(Sanitizer.GetSafeHtmlFragment(txtDeliveryDate.Text));
                objProductBE.PropProductEnquiry.Branding = Sanitizer.GetSafeHtmlFragment(txtBranding.Text);
                objProductBE.PropProductEnquiry.AdditionalInformation = Sanitizer.GetSafeHtmlFragment(txtareaAdditionalInfo.InnerText);
                if (fuLogo.HasFiles)
                {
                    if (fuLogo.PostedFile.ContentLength > 0)
                    {
                        if (GlobalFunctions.IsFileSizeValid(fuLogo, Convert.ToInt64(GlobalFunctions.GetSetting("ENQUIRY_LOGOSIZE"))) == false)
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, strLogoFileSize, AlertType.Warning);
                            SetFocus(fuLogo.ClientID);
                            return;
                        }

                        //Chk for File Extension
                        string[] strFileTypes = { ".jpg", ".jpeg", ".gif", ".png", ".ai", ".pdf", ".eps", ".bmp", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pps", ".ppsx" };
                        string[] strMimeTypes = { "image/jpeg", "image/gif", "image/png", "image/bmp", "image/tiff", "image/x-icon", "application/postscript", "application/pdf", "application/msword", "application/excel", "application/vnd.ms-powerpoint" };
                        bool bChkFileType = false;
                        bChkFileType = GlobalFunctions.CheckFileExtension(fuLogo, strFileTypes);
                        bool bChkFileMimeType = false;
                        bChkFileMimeType = GlobalFunctions.CheckFileMIMEType(fuLogo, strMimeTypes);
                        if (!(bChkFileType && bChkFileMimeType))
                        {
                            GlobalFunctions.ShowModalAlertMessages(Page, strInvalidFileType, Request.Url.AbsoluteUri, AlertType.Warning);
                            SetFocus(fuLogo.ClientID);
                            return;
                        }
                        string strFileName = fuLogo.FileName;
                        string strNewFileName = "ProductEnquiry-" + DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Month + "-" + DateTime.Now.Date.Year + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + "-" + strFileName;
                        objProductBE.PropProductEnquiry.LogoFileName = strNewFileName;
                        string strImgPath = GlobalFunctions.GetPhysicalEnquiryImagePath() + strNewFileName;
                        fuLogo.SaveAs(strImgPath);
                    }
                }
                bool result = ProductBL.AddProductEnquiry(objProductBE);
                SendProductEnquiryEmail(objProductBE);
                if (result)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Function", "submit_MadeToOrder();", true);
                    txtEmailAddress.Text = "";
                    txtQtyRequired.Text = "";
                    txtDeliveryDate.Text = "";
                    txtBranding.Text = "";
                    txtareaAdditionalInfo.InnerText = "";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void SendProductEnquiryEmail(ProductBE objProductBE)
        {
            try
            {
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = strProductEnquiry;
                objEmail.LanguageId = intLanguageId;
                objEmail.EmailTemplateId = 0;

                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    string strFromEmailId = lstEmail[0].FromEmailId;
                    string strCCEmailId = lstEmail[0].CCEmailId;
                    string strBCCEmailId = lstEmail[0].BCCEmailId;
                    bool IsHtml = lstEmail[0].IsHtml;
                    string strSubject = lstEmail[0].Subject + " - " + ((ProductBE)(Session["objCurrentProductBE"])).ProductCode + " - " + ((ProductBE)(Session["objCurrentProductBE"])).ProductName;
                    string strMailBody = GenerateMailBody(objProductBE, lstEmail[0].Body);
                    strMailBody = strMailBody.Replace("@ProductCode", ((ProductBE)(Session["objCurrentProductBE"])).ProductCode);
                    strMailBody = strMailBody.Replace("@ProductName", ((ProductBE)(Session["objCurrentProductBE"])).ProductName);
                    string strAttachmentPath;
                    if (File.Exists(GlobalFunctions.GetPhysicalEnquiryImagePath() + objProductBE.PropProductEnquiry.LogoFileName))
                        strAttachmentPath = GlobalFunctions.GetPhysicalEnquiryImagePath() + objProductBE.PropProductEnquiry.LogoFileName;
                    else
                        strAttachmentPath = null;
                    /*Sachin Chauhan : Start : 05 04 2016*/
                    //GlobalFunctions.SendEmail(objProductBE.PropProductEnquiry.EmailAddress, strFromEmailId, "", strCCEmailId, strBCCEmailId, strSubject, strMailBody, strAttachmentPath, "", IsHtml);
                    StoreBE.ContactBE objContactBE = new StoreBE.ContactBE();
                    StoreBE.ContactBE lstContactBE = new StoreBE.ContactBE();
                    lstContactBE = StoreBL.GetContactInfo(objContactBE);
                    GlobalFunctions.SendEmail(lstContactBE.HelpDeskEmail, "", "", "", txtEmailAddress.Text, strSubject, strMailBody, strAttachmentPath, "", IsHtml);
                    /*Sachin Chauhan : End : 05 04 2016*/
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private string GenerateMailBody(ProductBE objProductBE, string strMailBody)
        {
            try
            {
                strMailBody = strMailBody.Replace("@EmailAddress", objProductBE.PropProductEnquiry.EmailAddress);
                strMailBody = strMailBody.Replace("@QtyRequired", objProductBE.PropProductEnquiry.QuantityRequired);
                strMailBody = strMailBody.Replace("@DeliveryDate", objProductBE.PropProductEnquiry.DeliveryDateRequired.ToString("dd/MM/yyyy"));
                strMailBody = strMailBody.Replace("@Branding", objProductBE.PropProductEnquiry.Branding);
                strMailBody = strMailBody.Replace("@AdditonalInfo", objProductBE.PropProductEnquiry.AdditionalInformation);
                //Get logo
                string strSiteLogoDir = string.Empty;
                DirectoryInfo dirSiteLogo = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Images/SiteLogo"));
                FileInfo[] logoFileInfo = dirSiteLogo.GetFiles();

                foreach (FileInfo logoFile in logoFileInfo)
                {
                    if (logoFile.Name.ToLower().Contains("sitelogo"))
                    {
                        strSiteLogoDir = "<a href='" + host + "index'><img height='60px' src='" + host + "Images/SiteLogo/" + logoFile.Name + "'></a>";
                    }
                }
                strMailBody = strMailBody.Replace("@sitelogo", strSiteLogoDir);
                strMailBody = strMailBody.Replace("@SiteName", objStoreBE.StoreName);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return strMailBody;
            }
            return strMailBody;
        }

        protected void btnReviewSubmit_Click(object sender, EventArgs e)
        {
            ProductBE.ReviewRating objReviewRating;
            try
            {

                if (string.IsNullOrEmpty(hdnRating.Value))
                {
                    ltrRatingText.Visible = true;

                    #region To call Alert from code if Enter Review validation failed
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "javascript:ShowReviewA();", true);
                    // call modal from code behind
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "JsFunc", "alert('" + strRatingErrorMsg + "')", true);
                    return;
                    #endregion
                }
                objReviewRating = new ProductBE.ReviewRating();
                string strRatingValue = Sanitizer.GetSafeHtmlFragment(hdnRating.Value);
                objReviewRating.EmailId = Sanitizer.GetSafeHtmlFragment(txtReviewEmailId.Text);
                objReviewRating.ProductId = ProductId;
                objReviewRating.ReviewTitle = Sanitizer.GetSafeHtmlFragment(txtReviewTitle.Text);
                objReviewRating.Review = Sanitizer.GetSafeHtmlFragment(txtareaCommentfield.InnerHtml);
                objReviewRating.Rating = Convert.ToDecimal(strRatingValue);
                objReviewRating.Action = 'P';

                bool result = ProductBL.AddProductReview(objReviewRating);
                if (result)
                {
                    bool IsMailSent = SendRequestReviewApproval();
                    if (IsMailSent)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strReviewSuccessMsg, Request.Url.AbsoluteUri, AlertType.Success);

                    }
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strReviewFailureMsg, AlertType.Failure);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objReviewRating = null;
            }
        }

        private void ResetReviewFields()
        {
            try
            {
                hdnRating.Value = null;
                txtReviewEmailId.Text = "";
                txtReviewTitle.Text = "";
                txtareaCommentfield.InnerHtml = "";
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        private void BindSocialMediaIcons()
        {
            List<SocialMediaManagementBE> lstSocialMedia;
            try
            {
                lstSocialMedia = new List<SocialMediaManagementBE>();
                lstSocialMedia = SocialMediaManagementBL.GetAllSocialMedia<SocialMediaManagementBE>();
                if (lstSocialMedia != null)
                {
                    if (lstSocialMedia.Count > 0)
                    {
                        rptSocialMedia.DataSource = lstSocialMedia;
                        rptSocialMedia.DataBind();
                    }
                    else
                    {
                        dvSocialMedia.Visible = false;
                    }
                }
                else
                {
                    dvSocialMedia.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptSocialMedia_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlAnchor aSocialMedia = (HtmlAnchor)e.Item.FindControl("aSocialMedia");
                    HtmlImage imgSocialMedia = (HtmlImage)e.Item.FindControl("imgSocialMedia");
                    SocialMediaManagementBE objSocialMedia = (SocialMediaManagementBE)e.Item.DataItem;

                    /*Sachin Chauhan Start : Changed URL with shorter generated urls for SM platform */
                    //aSocialMedia.HRef = objSocialMedia.SocialMediaUrl.Replace("$URL$", Request.Url.ToString());
                    aSocialMedia.HRef = objSocialMedia.SocialMediaUrl.Replace("$URL$", strSocialMediaShortUrl);
                    aSocialMedia.Attributes.Add("target", "_blank");
                    /**/
                    imgSocialMedia.Src = GlobalFunctions.GetVirtualPath() + "Images/SocialMedia/" + objSocialMedia.SocialMediaId + objSocialMedia.IconExtension;
                    imgSocialMedia.Alt = objSocialMedia.SocialMediaPlatform;
                    imgSocialMedia.Attributes.Add("title", objSocialMedia.SocialMediaPlatform);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void SendOrderEmail(string ImageName, ProductBE objProductBE)
        {
            try
            {
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = strEmailThisProduct;
                objEmail.LanguageId = Convert.ToInt16(intLanguageId);
                objEmail.EmailTemplateId = 0;

                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    string strFromEmailId = lstEmail[0].FromEmailId;
                    string strCCEmailId = lstEmail[0].CCEmailId;
                    string strBCCEmailId = lstEmail[0].BCCEmailId;
                    bool IsHtml = lstEmail[0].IsHtml;
                    string strSubject = lstEmail[0].Subject;
                    string strMailBody = GenerateContentForMailandPDF(objProductBE, lstEmail[0].Body, ImageName, "Email");
                    bool res = GlobalFunctions.SendEmail(txtReciepientEmail.Text.ToString(), txtSenderEmail.Text.ToString(), "", strCCEmailId, strBCCEmailId, strSubject, strMailBody, "", "", IsHtml);
                    if (res)
                    {
                        GlobalFunctions.ShowModalAlertMessages(this.Page, strEmailThisProduct, AlertType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected bool SendRequestReviewApproval()
        {
            try
            {
                bool res = false;
                List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                EmailManagementBE objEmail = new EmailManagementBE();
                objEmail.EmailTemplateName = "Review Approval";
                objEmail.LanguageId = Convert.ToInt16(intLanguageId);
                objEmail.EmailTemplateId = 0;

                lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                if (lstEmail != null && lstEmail.Count > 0)
                {
                    string strFromEmailId = lstEmail[0].FromEmailId;
                    string strCCEmailId = lstEmail[0].CCEmailId;
                    string strBCCEmailId = lstEmail[0].BCCEmailId;
                    bool IsHtml = lstEmail[0].IsHtml;
                    string strSubject = lstEmail[0].Subject;
                    string strMailBody = lstEmail[0].Body;
                    res = GlobalFunctions.SendEmail(strFromEmailId, strFromEmailId, "", strCCEmailId, strBCCEmailId, strSubject, strMailBody, "", "", IsHtml);
                    return res;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        protected string GenerateContentForMailandPDF(ProductBE objProductBE, string strMailBody, string ImageName, string Mode)
        {
            try
            {
                string strProductMediumImagePath = GlobalFunctions.GetMediumProductImagePath();
                if (Mode == "Email")
                {
                    strMailBody = "<style type='text/css'>td{padding:10px 15px; font-family:Verdana, Geneva, sans-serif; font-size:13px; color::#000;text-align:left;vertical-align:middle;}</style>";
                    //}  
                    List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
                    EmailManagementBE objEmail = new EmailManagementBE();
                    objEmail.EmailTemplateName = "Email This Product";
                    objEmail.LanguageId = Convert.ToInt16(intLanguageId);
                    objEmail.EmailTemplateId = 0;

                    lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
                    if (lstEmail != null && lstEmail.Count > 0)
                    {
                        string strFromEmailId = lstEmail[0].FromEmailId;
                        string strCCEmailId = lstEmail[0].CCEmailId;
                        string strBCCEmailId = lstEmail[0].BCCEmailId;
                        bool IsHtml = lstEmail[0].IsHtml;
                        string strSubject = HttpUtility.HtmlDecode(lstEmail[0].Subject);
                        strMailBody = strMailBody + HttpUtility.HtmlDecode(lstEmail[0].Body);
                    }
                }
                else  // pdf
                {
                    #region to read html of pdf so that it should be configurable mode
                    //HtmlDocument htmlDocument = new HtmlDocument();
                    //htmlDocument.LoadHtml(GlobalFunctions.ReadFile(HttpContext.Current.Server.MapPath("~/PDFContent.html")));
                    //HtmlNode divPdf = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='pdf']");

                    //strMailBody = divPdf.InnerText;
                    #endregion

                    StoreBE.ContactBE objContactBE = new StoreBE.ContactBE();
                    objContactBE.Action = Convert.ToInt16(DBAction.Select);
                    StoreBE.ContactBE ContactBE = StoreBL.GetContactInfo(objContactBE);
                    // EmailValue = Convert.ToString(ContactBE.HelpDeskEmail);
                    string PhoneValue = Convert.ToString(ContactBE.HelpDeskPhone);

                    //string StoreURL = Convert.ToString(ConfigurationManager.AppSettings["StoreURL"]);


                    strMailBody = strMailBody + "<table style='font-family:neue,helvetica,arial,sans-serif;' width='800' border='0' cellspacing='0' cellpadding='0' align='center'>";
                    //strMailBody += "<tr><td><a>" + StoreURL + "</a><td></tr>";
                    //strMailBody += "<tr><td>" + PhoneValue + "<td></tr><br><br><br><br><br>";
                    //strMailBody += "<div><a>" + StoreURL + "</a></div><tr><td style='text-align:right'>" + PhoneValue + "<br><br></td></tr>";
                    strMailBody += "<tr><td>@sitelogo<br><br><br><br><br><br></td></tr><tr><td ><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td  colspan='2'>@ProductDetails<br><br><br><br><br><br>";
                    strMailBody += "</td></tr></table></td></tr><tr><td><table style='margin-bottom:10px;' width='100%' border='0' cellspacing='0' cellpadding='0'>";
                    strMailBody += "<tr><td >@ProductCodeLabel</td><td >@ProductCodevalue</td><td >&nbsp;</td></tr>";
                    strMailBody += "<tr><td >@ProductNamelabel</td><td >@ProductNamevalue</td><td >&nbsp;</td></tr>";
                    strMailBody += "<tr><td >@ProductDescriptionlabel</td><td >@ProductDescriptionvalue</td><td >&nbsp;</td></tr>";
                    strMailBody += "</table></td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td><br><br><br></td></tr><tr><td  style='padding:5px;' class='tdtable'>@griddata</td></tr></table>";
                    strMailBody += "<br />";
                }


                StringBuilder sbProductDetails = new StringBuilder();
                string strImageSrc = string.Empty;
                string username = string.Empty;
                if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + ImageName))
                    strImageSrc = strProductMediumImagePath + ImageName;
                else
                    strImageSrc = host + "Images/Products/default.jpg";

                if (Mode == "Email")
                {
                    username = txtSenderName.Text;
                    strMailBody = strMailBody.Replace("@UserName", username.ToString());
                }

                sbProductDetails.Append("<div id='zoom' class='col-lg-5 col-md-5 col-sm-5 col-xs-12 product_image_panel'><span id='productZoomImage' class='zoom' style='position: relative; overflow: hidden;'>");
                sbProductDetails.Append("<img src=\"" + strImageSrc + "\" style='width: 50%;' class='product_feature' />");
                sbProductDetails.Append("</span></div>");

                StringBuilder strbilderRepeaterdata = new StringBuilder();
                strbilderRepeaterdata.Append("<table width='100%'  cellspacing='0' cellpadding='0' border='1' style='border-collapse: collapse;  border-top: medium none; font-size: 14px; line-height: 21px; padding-top: 40px;'>");
                strbilderRepeaterdata.Append("<tr><td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + strProductTitle + "</td>");
                strbilderRepeaterdata.Append("<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + Status_Title + "</td>");
                strbilderRepeaterdata.Append("<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + strStockTitle + "</td>");
                strbilderRepeaterdata.Append("<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + strUnitCost + "</td></tr>");
                foreach (RepeaterItem items in rptReportSKU.Items)
                {
                    Label lblSKuCurrencySymbol = (Label)items.FindControl("lblSKuCurrencySymbol");
                    Label lblItemCode = (Label)items.FindControl("lblItemCode");
                    Label lblProductName = (Label)items.FindControl("lblProductName");
                    Label lblStock = (Label)items.FindControl("lblStock");
                    Label lblPrice = (Label)items.FindControl("lblPrice");
                    Label lblStatus = (Label)items.FindControl("lblStatus");

                    if (GlobalFunctions.IsPointsEnbled())
                    {
                        lblSKuCurrencySymbol.Visible = false;
                    }
                    else
                    {
                        lblSKuCurrencySymbol.Text = GlobalFunctions.GetCurrencySymbol();
                    }
                    strbilderRepeaterdata.Append("<tr><td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + Convert.ToString(lblItemCode.Text) + " - " + Convert.ToString(lblProductName.Text) + "</td>");
                    strbilderRepeaterdata.Append("<td style='border: 1px solid #ccc; padding:5px;text-align:center;'>" + Convert.ToString(lblStatus.Text) + "</td>");
                    strbilderRepeaterdata.Append("<td style='border: 1px solid #ccc; padding:5px;text-align:center'>" + Convert.ToString(lblStock.Text) + "</td>");
                    strbilderRepeaterdata.Append("<td style='border: 1px solid #ccc; padding:5px;text-align:center'>");
                    if (lblSKuCurrencySymbol.Visible)
                    {
                        strbilderRepeaterdata.Append(Convert.ToString(lblSKuCurrencySymbol.Text));
                    }
                    else
                    {
                        strbilderRepeaterdata.Append(Convert.ToString(lblSKuCurrencySymbol.Text));
                    }
                    strbilderRepeaterdata.Append(Convert.ToString(lblPrice.Text));

                    strbilderRepeaterdata.Append("</td></tr>");
                }
                strbilderRepeaterdata.Append("</table>");
                //Get logo
                string strSiteLogoDir = string.Empty;
                DirectoryInfo dirSiteLogo = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Images/SiteLogo"));
                FileInfo[] logoFileInfo = dirSiteLogo.GetFiles();

                foreach (FileInfo logoFile in logoFileInfo)
                {
                    if (logoFile.Name.ToLower().Contains("sitelogo"))
                    {
                        if (Mode == "PDF")
                        {
                            strSiteLogoDir = "<img height='80' src='" + host + "Images/SiteLogo/" + logoFile.Name + "'>";
                        }
                        else
                        {
                            strSiteLogoDir = "<a href='" + host + "index'><img height='80' src='" + host + "Images/SiteLogo/" + logoFile.Name + "'></a>";
                        }
                    }
                }
                strMailBody = strMailBody.Replace("@ProductDetails", sbProductDetails.ToString());
                strMailBody = strMailBody.Replace("@Reciepient", txtReciepientName.Text);
                strMailBody = strMailBody.Replace("@ProductCodeLabel", strmailprodcodelabel);
                strMailBody = strMailBody.Replace("@ProductCodevalue", objProductBE.ProductCode);
                strMailBody = strMailBody.Replace("@ProductNamelabel", strmailprodNamelabel);
                strMailBody = strMailBody.Replace("@ProductNamevalue", objProductBE.ProductName);
                if (objProductBE.ProductDescription != "")
                {
                    strMailBody = strMailBody.Replace("@ProductDescriptionlabel", strmailprodDesclabel);
                    strMailBody = strMailBody.Replace("@ProductDescriptionvalue", objProductBE.ProductDescription);
                }
                else
                {
                    strMailBody = strMailBody.Replace("@ProductDescriptionlabel", "");
                    strMailBody = strMailBody.Replace("@ProductDescriptionvalue", "");
                }
                //strMailBody = strMailBody.Replace("@FutherDescriptionlabel", strmailfurtherDesclabel);
                //strMailBody = strMailBody.Replace("@FutherDescriptionvalue", objProductBE.FurtherDescription);
                strMailBody = strMailBody.Replace("@griddata", Convert.ToString(strbilderRepeaterdata));

                //Exceptions.WriteInfoLog("strSiteLogoDir" + strSiteLogoDir);
                strMailBody = strMailBody.Replace("@sitelogo", strSiteLogoDir);//Uncommented by Sripal added by Sripal

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return strMailBody;
            }

            return strMailBody;
        }

        protected void CreatePDF(ProductBE objProductBE)
        {
            try
            {
                //Dummy DataTable

                string html = string.Empty;
                string FileName = Path.GetFileName(imgProduct.Src);
                html += GenerateContentForMailandPDF(objProductBE, "", FileName, "PDF");
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        StringReader sr = new StringReader(html);
                        Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);


                        //  html.Replace("@sripal", Environment.NewLine);

                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                        pdfDoc.Open();
                        htmlparser.Parse(sr);
                        pdfDoc.Close();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=HTMLExport.pdf");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDoc);
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                GlobalFunctions.ShowModalAlertMessages(this, strPDFCreationError, AlertType.Failure);
            }
        }

        protected void lnkPDF_Click(object sender, EventArgs e)
        {
            try
            {
                ProductBE objProductBE = new ProductBE();
                if (Session["objCurrentProductBE"] != null)
                    objProductBE = Session["objCurrentProductBE"] as ProductBE;
                if (Session["objVariantProductBE"] != null)
                {
                    ProductBE objVProductBE = Session["objVariantProductBE"] as ProductBE;
                    if (!objVProductBE.ProductId.Equals(objProductBE.ProductId))
                        objProductBE = objVProductBE;
                }
                CreatePDF(objProductBE);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                string FileName = Path.GetFileName(imgProduct.Src);
                ProductBE objProductBE = new ProductBE();
                if (Session["objCurrentProductBE"] != null)
                    objProductBE = Session["objCurrentProductBE"] as ProductBE;
                if (Session["objVariantProductBE"] != null)
                {
                    ProductBE objVProductBE = Session["objVariantProductBE"] as ProductBE;
                    if (!objVProductBE.ProductId.Equals(objProductBE.ProductId))
                        objProductBE = objVProductBE;
                }
                SendOrderEmail(FileName, objProductBE);
                ClearSendEmailBoxes();
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        protected void lnkDownloadImg_Click(object sender, EventArgs e)
        {
            DownloadImage();
        }

        protected void DownloadImage()
        {
            // Product have Medium images while downloading it show the zoom images
            /*WebClient req = new WebClient();
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            string ImagePath = Convert.ToString(imgProduct.Src);
            ImagePath = ImagePath.Replace("/", "\\");
            string[] ZipFilepath = ImagePath.Split('\\');
            response.AddHeader("Content-Disposition", "attachment;filename=\"" + ZipFilepath[ZipFilepath.Length - 1] + "\"");
            byte[] data = req.DownloadData(imgProduct.Src);
            response.BinaryWrite(data);
            response.End();*/

            #region code to open image in new browser as request

            //string path = Convert.ToString(imgProduct.Src);
            string path = Convert.ToString(hdnimagepath.Value);
            WebClient client = new WebClient();
            Byte[] buffer = client.DownloadData(path);
            if (buffer != null)
            {
                Response.ContentType = "image/jpeg";
                Response.AddHeader("content-length", buffer.Length.ToString());
                Response.BinaryWrite(buffer);
                Response.End();
            }

            #endregion

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearSendEmailBoxes();
        }

        protected void ClearSendEmailBoxes()
        {
            txtSenderEmail.Text = "";
            txtSenderMsg.Text = "";
            txtReciepientEmail.Text = "";
            txtReciepientName.Text = "";
            txtSenderName.Text = "";
        }

        protected void btnShopping_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(host + "index", true);
            }
            catch (ThreadAbortException) { }
            catch (Exception) { }
        }

        protected void AddWishList()
        {
            ProductBE.ProductWishList objProductWishList = new ProductBE.ProductWishList();
            try
            {
                int intUserId = 0;
                string UserSessionId = HttpContext.Current.Session.SessionID;
                if (Session["User"] != null)
                {
                    UserBE objUserBE = (UserBE)Session["User"];
                    intUserId = objUserBE.UserId;
                }

                objProductWishList.LanguageId = intLanguageId;
                objProductWishList.Action = Convert.ToInt16(DBAction.Insert);
                objProductWishList.ProductSKUId = Convert.ToInt16(hdnSKUIdAddWishList.Value);

                int result = ProductBL.ManageWishListItem(objProductWishList, intUserId);

                if (result > 0)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strWishListSaveSuccessMsg, Request.Url.AbsoluteUri, AlertType.Success);
                }
                else
                    GlobalFunctions.ShowModalAlertMessages(this.Page, strGenericSaveFailMsg, AlertType.Failure);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                objProductWishList = null;
            }
        }

        protected void lnkAddtoWishList_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt16(hdnSKUCount.Value) > 1)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "ShowModalWishList", "<script>javascript:ShowModalWishList();</script>");
                }
                else
                {
                    AddWishList();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {

            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hdnSKUIdAddWishList.Value = ddlSKU.SelectedValue;
            AddWishList();
        }
    }
}
