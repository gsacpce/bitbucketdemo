﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Products_Products_RecentlyViewedProducts : System.Web.UI.Page
    {
        protected global::System.Web.UI.WebControls.Repeater rptRecentlyViewed;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvRecentlyViewContainer;
        protected global::System.Web.UI.WebControls.Literal ltrRecentlyViewedProducts;
        public static ProductBE objProductBE;
        public static UserBE objUserBE;
        public static StoreBE objStoreBE;
        public static int ProductId;
        public bool blnShowSectionsIcons = false;
        Int16 intLanguageId;
        Int16 intCurrencyId;
        string strCurrencySymbol, stror;
        int UserId;
        Int16 iUserTypeID = 1;/*User Type*/

        public static string host = GlobalFunctions.GetVirtualPath();

        /////*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        ////private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        ////private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        ////private string _antiXsrfTokenValue;
        /////*Sachin Chauhan End : 22 02 2016 */

        /////// <summary>
        /////// Author  : Sachin Chauhan
        /////// Date : 22 02 2016
        /////// Scope : Added CSRF attack prevention code
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        /////// <returns></returns>
        ////protected void Page_PreLoad(object sender, EventArgs e)
        ////{
        ////    //During the initial page load, add the Anti-XSRF token and user
        ////    //name to the ViewState
        ////    if (!IsPostBack)
        ////    {
        ////        //Set Anti-XSRF token
        ////        ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

        ////        //If a user name is assigned, set the user name
        ////        //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        ////        if (Session["ContextUserGUID"] == null)
        ////            Session["ContextuserGUID"] = Guid.NewGuid().ToString();

        ////        ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
        ////    }
        ////    //During all subsequent post backs to the page, the token value from
        ////    //the cookie should be validated against the token in the view state
        ////    //form field. Additionally user name should be compared to the
        ////    //authenticated users name
        ////    else
        ////    {
        ////        //Validate the Anti-XSRF token
        ////        //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
        ////        try
        ////        {
        ////            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())
        ////                Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            Exceptions.WriteExceptionLog(ex);
        ////            Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed due to unhandled exception.");
        ////            //throw;
        ////        }
        ////    }
        ////}

        ////protected void Page_Init(object sender, EventArgs e)
        ////{
        ////    /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        ////    #region "CSRF prevention code"
        ////    try
        ////    {
        ////        //First, check for the existence of the Anti-XSS cookie
        ////        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        ////        Guid requestCookieGuidValue;

        ////        //If the CSRF cookie is found, parse the token from the cookie.
        ////        //Then, set the global page variable and view state user
        ////        //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
        ////        //method.
        ////        if (requestCookie != null
        ////        && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        ////        {
        ////            //Set the global token variable so the cookie value can be
        ////            //validated against the value in the view state form field in
        ////            //the Page.PreLoad method.
        ////            _antiXsrfTokenValue = requestCookie.Value;

        ////            //Set the view state user key, which will be validated by the
        ////            //framework during each request
        ////            Page.ViewStateUserKey = _antiXsrfTokenValue;
        ////        }
        ////        //If the CSRF cookie is not found, then this is a new session.
        ////        else
        ////        {
        ////            //Generate a new Anti-XSRF token
        ////            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

        ////            //Set the view state user key, which will be validated by the
        ////            //framework during each request
        ////            Page.ViewStateUserKey = _antiXsrfTokenValue;

        ////            //Create the non-persistent CSRF cookie
        ////            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
        ////            {
        ////                //Set the HttpOnly property to prevent the cookie from
        ////                //being accessed by client side script
        ////                HttpOnly = true,

        ////                //Add the Anti-XSRF token to the cookie value
        ////                Value = _antiXsrfTokenValue
        ////            };

        ////            //If we are using SSL, the cookie should be set to secure to
        ////            //prevent it from being sent over HTTP connections
        ////            if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
        ////                responseCookie.Secure = true;

        ////            //Add the CSRF cookie to the response
        ////            Response.Cookies.Set(responseCookie);
        ////        }

        ////        Page.PreLoad += Page_PreLoad;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
        ////        Exceptions.WriteExceptionLog(ex);
        ////        //throw;
        ////    }
        ////    #endregion
        ////    /*Sachin Chauhan End : 22 02 2016*/
        ////}


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["User"] != null)
                {
                    objUserBE = new UserBE();
                    objUserBE = Session["User"] as UserBE;
                    iUserTypeID = objUserBE.UserTypeID;/*User Type*/
                }

                intLanguageId = GlobalFunctions.GetLanguageId();
                intCurrencyId = GlobalFunctions.GetCurrencyId();
                strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                BindResourceData();
                ProductId = Convert.ToInt32(Request.Form["ProductId"]);

                if (!IsPostBack)
                {
                    //Bind Recently Viewed Products
                    GetFeaturesDetails();
                    BindRecentlyViewedProducts();
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #region Featrues Added by vikram on 24-01-2015
        protected void GetFeaturesDetails()
        {
            try
            {
                objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableSectionIcons").FeatureValues[0].IsEnabled)
                    { blnShowSectionsIcons = true; }
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }
        #endregion
        private void BindRecentlyViewedProducts()
        {
            if (Session["User"] != null)
            {
                objUserBE = new UserBE();
                objUserBE = Session["User"] as UserBE;
            }
            if (objUserBE != null)
            {
                UserId = objUserBE.UserId;
            }
            List<ProductBE.RecentlyViewedProducts> lstRecentlyViewedProducts = new List<ProductBE.RecentlyViewedProducts>();
            try
            {
                if (UserId != 0)
                {//lstRecentlyViewedProducts = ProductBL.GetRecentlyViewedProducts(UserId, HttpContext.Current.Session.SessionID, ProductId, intCurrencyId, intLanguageId);
                    lstRecentlyViewedProducts = ProductBL.GetRecentlyViewedProducts(UserId, HttpContext.Current.Session.SessionID, ProductId, intCurrencyId, intLanguageId, iUserTypeID);/*User Type*/
                }
                else
                {
                    //lstRecentlyViewedProducts = ProductBL.GetRecentlyViewedProducts(0, HttpContext.Current.Session.SessionID, ProductId, intCurrencyId, intLanguageId);
                    lstRecentlyViewedProducts = ProductBL.GetRecentlyViewedProducts(0, HttpContext.Current.Session.SessionID, ProductId, intCurrencyId, intLanguageId, iUserTypeID);/*User Type*/
                }
                if (lstRecentlyViewedProducts != null)
                {
                    if (lstRecentlyViewedProducts.Count > 0)
                    {
                        rptRecentlyViewed.DataSource = lstRecentlyViewedProducts;
                        rptRecentlyViewed.DataBind();
                    }
                    else
                    {
                        dvRecentlyViewContainer.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void rptRecentlyViewed_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlImage imgRecentlyViewedProduct = (HtmlImage)e.Item.FindControl("imgRecentlyViewedProduct");
                    HtmlAnchor lnkProductImage = (HtmlAnchor)e.Item.FindControl("lnkProductImage");
                    HtmlAnchor lnkProductName = (HtmlAnchor)e.Item.FindControl("lnkProductName");
                    Literal ltrPriceAndDiscountCode = (Literal)e.Item.FindControl("ltrPriceAndDiscountCode");
                    Literal ltrstrikeprice = (Literal)e.Item.FindControl("ltrstrikeprice");
                    string strCategoryName = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).CategoryName);
                    string strSubCategoryName = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).SubCategoryName);
                    HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
                    HtmlGenericControl dvstrike = (HtmlGenericControl)e.Item.FindControl("dvstrike");
                    HtmlGenericControl dvprice = (HtmlGenericControl)e.Item.FindControl("dvprice");
                    string strSubSubCategoryName = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).SubSubCategoryName);
                    string strProductName = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).ProductName);
                    string strProductSku = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).ProductCode);
                    decimal minprice = (((ProductBE.RecentlyViewedProducts)e.Item.DataItem).MinstrikePrice);
                    HtmlGenericControl spnPrice1 = (HtmlGenericControl)e.Item.FindControl("spnPrice1");
                    HtmlGenericControl spnor = (HtmlGenericControl)e.Item.FindControl("spnor");
                    #region New changes for points
                    if (minprice != 0)
                    {
                        if (GlobalFunctions.IsPointsEnbled())
                        {
                            ltrPriceAndDiscountCode.Text = GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                            spnor.InnerHtml = stror;
                            spnPrice1.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                      
                        }
                        else 
                        {
                            ltrPriceAndDiscountCode.Text = GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);
                        
                        }
                        ltrstrikeprice.Text = GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).MinstrikePrice), strCurrencySymbol, intLanguageId);
                    }
                    else
                    {
                        ltrstrikeprice.Visible = false;
                        dvstrike.Visible = false;
                       
                        dvprice.Attributes.Remove("class");
                        dvprice.Attributes.Add("class", "price col-sm-12");
                        if (GlobalFunctions.IsPointsEnbled())
                        {
                         
                            spnor.InnerHtml = stror;
                            spnPrice1.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).MinPrice), strCurrencySymbol, intLanguageId);

                        }
                    }
                    #endregion
                    if (!string.IsNullOrEmpty(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).DefaultImageName))
                    {
                        if (IsExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(((ProductBE.RecentlyViewedProducts)e.Item.DataItem).DefaultImageName).Trim()))
                        {
                            imgRecentlyViewedProduct.Src = GlobalFunctions.GetMediumProductImagePath() + ((ProductBE.RecentlyViewedProducts)e.Item.DataItem).DefaultImageName.Trim();
                            imgRecentlyViewedProduct.Alt = ((ProductBE.RecentlyViewedProducts)e.Item.DataItem).ProductName.Trim();
                            imgRecentlyViewedProduct.Attributes.Add("title", ((ProductBE.RecentlyViewedProducts)e.Item.DataItem).ProductName.Trim());
                        }
                        else
                        {
                            imgRecentlyViewedProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                            imgRecentlyViewedProduct.Alt = "Default";
                            imgRecentlyViewedProduct.Attributes.Add("title", "Default");

                        }
                    }
                    else
                    {
                        imgRecentlyViewedProduct.Src = GlobalFunctions.GetMediumProductImagePath() + "Default.jpg";
                        imgRecentlyViewedProduct.Alt = "Default";
                        imgRecentlyViewedProduct.Attributes.Add("title", "Default");
                    }

                    if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
                    {
                        if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductSku.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                            lnkProductName.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductSku.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                        else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductSku.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                            lnkProductName.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductSku.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                        else
                        {
                            lnkProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductSku.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                            lnkProductName.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strProductSku.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(strProductName);
                        }
                    }
                    #region added by vikram for Section if Feature Allowed on store
                    if (blnShowSectionsIcons)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
                        {
                            string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
                            string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            string strSectionImageData = string.Empty;
                            string strSectionNames = string.Empty;
                            for (int i = 0; i < strSections.Length; i++)
                            {
                                string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                                {
                                    strSectionImageData += "<img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='SectionImage product_list_icon'>";
                                }
                                if (strSectionIcon.Length > 1)
                                    strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                            }
                            dvSectionIcons.InnerHtml = strSectionImageData;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        }

        private bool IsExists(string FilePath)
        {
            return (File.Exists(FilePath));
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 12-08-15
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        ltrRecentlyViewedProducts.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_RecentlyViewedProducts").ResourceValue;
                        stror = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "RecieveCommunication_or_Text").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

    }
}