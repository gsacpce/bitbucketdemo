﻿using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Presentation
{
    public class Products_QuickView_ajax : System.Web.UI.Page
    {
        #region Variables
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvProductVariants, dvProductPriceTable;
        protected global::System.Web.UI.WebControls.Literal ltrNoteTitle, ltrNoteText, ltrDescriptionTitle, ltrDescription;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aViewDetails;
        protected global::System.Web.UI.HtmlControls.HtmlImage imgProduct;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidProductName;


        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        Int32 intProductId = 0;
        string strCurrencySymbol, strQuantityLabel, strPricingLabel, strProductionTime = "";
        string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetMediumProductImagePath();

        ProductBE objProductBE;
        Int16 iUserTypeID = 1;/*User Type*/
        #endregion

        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        /*Sachin Chauhan End : 22 02 2016 */

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date : 22 02 2016
        /// Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            //During the initial page load, add the Anti-XSRF token and user
            //name to the ViewState
            if (!IsPostBack)
            {
                //Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

                //If a user name is assigned, set the user name
                //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                if (Session["ContextUserGUID"] == null)
                    Session["ContextuserGUID"] = Guid.NewGuid().ToString();

                ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
            }
            //During all subsequent post backs to the page, the token value from
            //the cookie should be validated against the token in the view state
            //form field. Additionally user name should be compared to the
            //authenticated users name
            else
            {
                //Validate the Anti-XSRF token
                //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                try
                {
                    if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())
                        Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                    Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed due to unhandled exception.");
                    //throw;
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
            #region "CSRF prevention code"
            try
            {
                //First, check for the existence of the Anti-XSS cookie
                var requestCookie = Request.Cookies[AntiXsrfTokenKey];
                Guid requestCookieGuidValue;

                //If the CSRF cookie is found, parse the token from the cookie.
                //Then, set the global page variable and view state user
                //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
                //method.
                if (requestCookie != null
                && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
                {
                    //Set the global token variable so the cookie value can be
                    //validated against the value in the view state form field in
                    //the Page.PreLoad method.
                    _antiXsrfTokenValue = requestCookie.Value;

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;
                }
                //If the CSRF cookie is not found, then this is a new session.
                else
                {
                    //Generate a new Anti-XSRF token
                    _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;

                    //Create the non-persistent CSRF cookie
                    var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                    {
                        //Set the HttpOnly property to prevent the cookie from
                        //being accessed by client side script
                        HttpOnly = true,

                        //Add the Anti-XSRF token to the cookie value
                        Value = _antiXsrfTokenValue
                    };

                    //If we are using SSL, the cookie should be set to secure to
                    //prevent it from being sent over HTTP connections
                    if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                        responseCookie.Secure = true;

                    //Add the CSRF cookie to the response
                    Response.Cookies.Set(responseCookie);
                }

                Page.PreLoad += Page_PreLoad;
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            #endregion
            /*Sachin Chauhan End : 22 02 2016*/
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 13-08-15
        /// Scope   : Page_Load of the quickview page
        /// </summary>
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                intLanguageId = Convert.ToInt16(Request.Form["languageid"]);
                intCurrencyId = Convert.ToInt16(Request.Form["currencyid"]);
                strCurrencySymbol = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["currencysymbol"]));
                intProductId = Convert.ToInt32(Request.Form["productid"]);

                BindResourceData();

                BindProductData();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

  


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 13-08-15
        /// Scope   : BindResourceData of the quickview page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                //ltrNoteTitle.Visible = false;
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strPricingLabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Price_Label").ResourceValue;
                        //ltrNoteTitle.Text = "<strong>" + lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Note_Label").ResourceValue + ":</strong>";
                        //ltrDescriptionTitle.Text = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_Description_Label").ResourceValue;
                        strQuantityLabel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Quantity_Text").ResourceValue;
                        strProductionTime = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Detail_ProductionTime_Label").ResourceValue;
                        aViewDetails.InnerHtml = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_ViewDetails_Text").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 13-08-15
        /// Scope   : BindProductData of the quickview page
        /// </summary>
        /// <returns></returns>
        protected void BindProductData()
        {
            try
            {
                /*User Type*/
                if (Session["User"] != null)
                {
                    UserBE lstUser = new UserBE();
                    lstUser = Session["User"] as UserBE;
                    iUserTypeID = lstUser.UserTypeID;
                }
                //objProductBE = ProductBL.GetProductDetails(intProductId, intLanguageId, intCurrencyId);
                objProductBE = ProductBL.GetProductDetails(intProductId, intLanguageId, intCurrencyId, iUserTypeID);/*User Type*/

                if (objProductBE != null)
                {
                    //dvProductCode.InnerHtml = "#" + objProductBE.ProductCode.Trim();
                    //hProductName.InnerHtml = objProductBE.ProductName.Trim();
                    //aViewDetails.HRef = "javascript:void(0);";
                    ltrDescription.Text = Convert.ToString(objProductBE.ProductDescription).Trim();
                    hidProductName.Value = objProductBE.ProductName;
                    //BindProductVariants();
                    //Bind Product Price Table
                    //BindProductPriceTable();
                    //Bind Product Variants

                    if (!string.IsNullOrEmpty(objProductBE.CategoryName) && !string.IsNullOrEmpty(objProductBE.SubCategoryName) && !string.IsNullOrEmpty(objProductBE.SubSubCategoryName))
                    {
                        if (objProductBE.CategoryName == objProductBE.SubCategoryName && objProductBE.CategoryName == objProductBE.SubSubCategoryName)
                        {
                            aViewDetails.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(objProductBE.CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductCode.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductName);
                        }
                        else if (objProductBE.CategoryName == objProductBE.SubCategoryName && objProductBE.SubCategoryName != objProductBE.SubSubCategoryName)
                        {
                            aViewDetails.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(objProductBE.CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.SubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductCode.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductName);
                        }
                        else
                        {
                            aViewDetails.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(objProductBE.CategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.SubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.SubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductCode.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(objProductBE.ProductName);
                        }
                    }
                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + objProductBE.DefaultImageName))
                        imgProduct.Src = strProductImagePath + objProductBE.DefaultImageName;
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";


                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 13-08-15
        /// Scope   : BindProductVariants of the quickview page
        /// </summary>
        /// <returns></returns>
        private void BindProductVariants()
        {
            StringBuilder sbProductVariant = new StringBuilder();
            StringBuilder sbProductImages = new StringBuilder();
            try
            {
                if (objProductBE.PropGetAllVariantType.Count > 0 && objProductBE.PropGetAllProductVariants.Count > 0)
                {
                    List<ProductBE.ProductVariants> lstProductVariant = new List<ProductBE.ProductVariants>();
                    List<ProductBE.ProductVariantType> lstDistinctProductVariantType = new List<ProductBE.ProductVariantType>();
                    lstDistinctProductVariantType = objProductBE.PropGetAllVariantType.GroupBy(x => x.ProductVariantTypeName).Select(x => x.First()).ToList();
                    Int16 intSelector = 0;
                    for (int i = 0; i < lstDistinctProductVariantType.Count; i++)
                    {
                        sbProductVariant.Append("<p class='text-center'><strong>" + lstDistinctProductVariantType[i].ProductVariantTypeName + "</strong></p>");
                        sbProductVariant.Append("<!-- thumb navigation carousel items --><ul class='list-inline'>");

                        lstProductVariant = objProductBE.PropGetAllProductVariants.FindAll(x => x.ProductVariantTypeId == lstDistinctProductVariantType[i].ProductVariantTypeId);
                        for (int j = 0; j < lstProductVariant.Count; j++)
                        {
                            if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalThumbnailProductImagePath() + lstProductVariant[j].ImageName) && GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + lstProductVariant[j].ImageName))
                            {
                                if (intSelector == 0)
                                    sbProductVariant.Append("<li><a id='carousel-selector-" + (intSelector) + "' class='selectedCarosuelItem'>");
                                else
                                    sbProductVariant.Append("<li><a id='carousel-selector-" + (intSelector) + "' class=''>");

                                sbProductVariant.Append("<img alt='" + lstProductVariant[j].ProductVariantName + "' src='" + GlobalFunctions.GetThumbnailProductImagePath() + lstProductVariant[j].ImageName + "'");
                                sbProductVariant.Append("width='45' height='45' class='img-responsive'></a></li>");

                                if (intSelector == 0)
                                    sbProductImages.Append("<div class='active item' data-slide-number='" + (intSelector) + "'>");
                                else
                                    sbProductImages.Append("<div class='item' data-slide-number='" + (intSelector) + "'>");
                                sbProductImages.Append("<img alt='" + lstProductVariant[j].ProductVariantName + "' src='" + GlobalFunctions.GetMediumProductImagePath() + lstProductVariant[j].ImageName + "'");
                                sbProductImages.Append(" class='img-responsive'></div>");
                                intSelector++;
                            }
                        }
                        sbProductVariant.Append("</ul>");
                    }

                    dvProductVariants.InnerHtml = sbProductVariant.ToString();
                    //dvProductImage.InnerHtml = sbProductImages.ToString();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 13-08-15
        /// Scope   : BindProductPriceTable of the quickview page
        /// </summary>
        /// <returns></returns>
        private void BindProductPriceTable()
        {
            StringBuilder sbProductPrice = new StringBuilder();

            try
            {
                if (objProductBE.PropGetAllPriceBreak.Count > 0 && objProductBE.PropGetAllPriceBreakDetails.Count > 0)
                {
                    sbProductPrice.Append("<div class='productTableTitle pageSubTitle'>" + strPricingLabel + "</div>");
                    sbProductPrice.Append("<div class='priceTable table-responsive'>");
                    sbProductPrice.Append("<table class='table customTable'>");
                    sbProductPrice.Append("<tbody>");
                    for (int i = 0; i < objProductBE.PropGetAllPriceBreak.Count; i++)
                    {

                        string strDiscountCode = null;
                        if (i == 0)
                        {
                            sbProductPrice.Append("<tr>");
                            sbProductPrice.Append("<td class=pageHighlightText>" + strQuantityLabel + "</td>");
                            for (int j = 0; j < objProductBE.PropGetAllPriceBreakDetails.FindAll(x => x.PriceBreakId == objProductBE.PropGetAllPriceBreak[i].PriceBreakId).Count; j++)
                                sbProductPrice.Append("<td>" + objProductBE.PropGetAllPriceBreakDetails[j].BreakFrom + "</td>");
                            //sbProductPrice.Append("<td> </td>");
                            //sbProductPrice.Append("<td>" + strProductionTime + "</td>");
                            sbProductPrice.Append("</tr>");
                        }
                        sbProductPrice.Append("<tr>");
                        sbProductPrice.Append("<td class='pageHighlightText'>" + objProductBE.PropGetAllPriceBreak[i].PriceBreakName + "</td>");
                        List<ProductBE.PriceBreakDetails> lstPriceBreakDetail = objProductBE.PropGetAllPriceBreakDetails.FindAll(x => x.PriceBreakId == objProductBE.PropGetAllPriceBreak[i].PriceBreakId);
                        for (int j = 0; j < lstPriceBreakDetail.Count; j++)
                        {
                            sbProductPrice.Append("<td>" + strCurrencySymbol + lstPriceBreakDetail[j].Price);
                            if (objProductBE.PropGetAllPriceBreak[i].EnableStrikePrice == true)
                                sbProductPrice.Append("<p><strike>" + strCurrencySymbol + lstPriceBreakDetail[j].StrikePrice + "</strike></p></td>");
                            else
                                sbProductPrice.Append("</td>");
                            strDiscountCode += lstPriceBreakDetail[j].DiscountCode;
                        }
                        //sbProductPrice.Append("<td>" + GlobalFunctions.FindFormattedDiscountCode(strDiscountCode) + "</td>");
                        //sbProductPrice.Append("<td>" + objProductBE.PropGetAllPriceBreak[i].ProductionTime + "</td>");
                        sbProductPrice.Append("</tr>");
                    }
                    ltrNoteText.Text = objProductBE.PropGetAllPriceBreak[0].NoteText;
                    if (!string.IsNullOrEmpty(ltrNoteText.Text))
                    {
                        ltrNoteTitle.Visible = true;
                    }
                    sbProductPrice.Append("</tbody>");
                    sbProductPrice.Append("</table>");
                    sbProductPrice.Append("</div>");
                    dvProductPriceTable.InnerHtml = sbProductPrice.ToString();
                }
                else
                    dvProductPriceTable.Visible = false;

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}