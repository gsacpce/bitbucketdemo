﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PWGlobalEcomm.BusinessLogic;
using System.Web.UI.HtmlControls;
using Microsoft.Security.Application;
using PWGlobalEcomm.Stock;
using System.Net;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;

namespace Presentation
{
    public class Products_Products_ajax : System.Web.UI.Page
    {
        #region Variables

        protected global::System.Web.UI.WebControls.Repeater rptProducts;
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hidTotalRecords;
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox chkCompare;

        Int16 intLanguageId = 0;
        Int16 intCurrencyId = 0;
        string strCurrencySymbol = "";
        public static string host = GlobalFunctions.GetVirtualPath();
        string strProductImagePath = GlobalFunctions.GetThumbnailProductImagePath();
        public bool blnShowRatingsReview = false;
        string strCategoryName = string.Empty;
        string strProductSku = string.Empty;
        string strSubCategoryName = string.Empty;
        string strSubSubCategoryName = string.Empty;
        string strSectionName = string.Empty;
        string strSortName = string.Empty;
        Int16 intPageNo = 0;
        Int16 intPageSize = 12;
        bool blnQuickViewEnabled, IsGoogleMixIt, IsEnableFilter, blnProductComparisonEnabled = false, blnShowSectionsIcons = false; bool blnrating = false; bool blnstock = false;
        string strColorIds, strSectionIds = string.Empty;
        decimal decMinPrice = 0;
        decimal decMaxPrice = 0;

        string strCustomFilter1 = string.Empty;
        string strCustomFilter2 = string.Empty;
        string strCustomFilter3 = string.Empty;
        string strCustomFilter4 = string.Empty;
        string strCustomFilter5 = string.Empty;
        string strSearchProductIds = string.Empty;
        string culture = string.Empty;
        string strQuickView, strCompare, strViewDetails, strstock, stror, strrating, strstocklevel, strAsLowAs, strCompareProductsIds = string.Empty;
        #endregion

        /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        /*Sachin Chauhan End : 22 02 2016 */
        public Int16 iUserTypeID = 1; /*User Type*/
        public bool IsListingType = false;
        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date : 22 02 2016
        /// Scope : Added CSRF attack prevention code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            //During the initial page load, add the Anti-XSRF token and user
            //name to the ViewState
            if (!IsPostBack)
            {
                //Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

                //If a user name is assigned, set the user name
                //ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                if (Session["ContextUserGUID"] == null)
                    Session["ContextuserGUID"] = Guid.NewGuid().ToString();

                ViewState[AntiXsrfUserNameKey] = Session["ContextuserGUID"].ToString() ?? String.Empty;
            }
            //During all subsequent post backs to the page, the token value from
            //the cookie should be validated against the token in the view state
            //form field. Additionally user name should be compared to the
            //authenticated users name
            else
            {
                //Validate the Anti-XSRF token
                //if ( (string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                try
                {
                    if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != Session["ContextuserGUID"].ToString())
                        Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed.");
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                    Exceptions.WriteInfoLog("Validation of Anti-XSRF token failed due to unhandled exception.");
                    //throw;
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            /*Sachin Chauhan Start : 22 02 2016 : Added below lines of code to verify if CSRF attack gets prevented*/
            #region "CSRF prevention code"
            try
            {
                //First, check for the existence of the Anti-XSS cookie
                var requestCookie = Request.Cookies[AntiXsrfTokenKey];
                Guid requestCookieGuidValue;

                //If the CSRF cookie is found, parse the token from the cookie.
                //Then, set the global page variable and view state user
                //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
                //method.
                if (requestCookie != null
                && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
                {
                    //Set the global token variable so the cookie value can be
                    //validated against the value in the view state form field in
                    //the Page.PreLoad method.
                    _antiXsrfTokenValue = requestCookie.Value;

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;
                }
                //If the CSRF cookie is not found, then this is a new session.
                else
                {
                    //Generate a new Anti-XSRF token
                    _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                    //Set the view state user key, which will be validated by the
                    //framework during each request
                    Page.ViewStateUserKey = _antiXsrfTokenValue;

                    //Create the non-persistent CSRF cookie
                    var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                    {
                        //Set the HttpOnly property to prevent the cookie from
                        //being accessed by client side script
                        HttpOnly = true,

                        //Add the Anti-XSRF token to the cookie value
                        Value = _antiXsrfTokenValue
                    };

                    //If we are using SSL, the cookie should be set to secure to
                    //prevent it from being sent over HTTP connections
                    if (Request.Url.AbsoluteUri.ToString().StartsWith("https://") && Request.IsSecureConnection)
                        responseCookie.Secure = true;

                    //Add the CSRF cookie to the response
                    Response.Cookies.Set(responseCookie);
                }

                Page.PreLoad += Page_PreLoad;
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("Error occured while CSRF prevent code got executed");
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            #endregion
            /*Sachin Chauhan End : 22 02 2016*/
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-07-16
        /// Scope   : Page_Load of the Product_ajax page
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                intLanguageId = Convert.ToInt16(Request.Form["languageid"]);
                intCurrencyId = Convert.ToInt16(Request.Form["currencyid"]);

                if (Session["User"] != null)
                { iUserTypeID = ((UserBE)Session["User"]).UserTypeID; }
                //intLanguageId = GlobalFunctions.GetLanguageId();
                //intCurrencyId = GlobalFunctions.GetCurrencyId();
                //strCurrencySymbol = GlobalFunctions.GetCurrencySymbol();
                strCurrencySymbol = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["currencysymbol"]));
                strCompareProductsIds = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["productids"]));
                //strSearchProductIds = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["searchproductids"]));
                strSearchProductIds = Convert.ToString(Request.Form["searchproductids"]);
                BindResourceData();

                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE != null)
                {
                    culture = objStoreBE.StoreLanguages.FirstOrDefault(x => x.LanguageId == intLanguageId).CultureCode;
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_QuickView").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "quickview").IsEnabled)
                    {
                        blnQuickViewEnabled = true;
                    }

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_CompareProducts").FeatureValues.FirstOrDefault(v => v.FeatureValue.ToLower() == "product comparison").IsEnabled)
                    {
                        blnProductComparisonEnabled = true;
                    }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableSectionIcons").FeatureValues[0].IsEnabled)
                    { blnShowSectionsIcons = true; }
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PD_EnableReviewRating").FeatureValues[0].IsEnabled)
                    { blnShowRatingsReview = true; }

                    //added by Nilesh
                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.IsDefault == true).FeatureValue.ToLower() == "gridview")
                    {
                        IsListingType = false;
                    }

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_DisplayType").FeatureValues.FirstOrDefault(v => v.IsDefault == true).FeatureValue.ToLower() == "listview")
                    {
                        IsListingType = true;
                    }

                    //End
                }
                strCategoryName = GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(Request.Form["categoryname"]));
                strSubCategoryName = GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(Request.Form["subcategoryname"]));
                strSubSubCategoryName = GlobalFunctions.DecodeCategoryURL(Sanitizer.GetSafeHtmlFragment(Request.Form["subsubcategoryname"]));
                intPageNo = Convert.ToInt16(Request.Form["pageno"]);
                intPageSize = Convert.ToInt16(Request.Form["pagesize"]);
                strSortName = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["sortname"]));
                strColorIds = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["colorids"]));
                strSectionIds = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["sectionids"]));
                strSectionName = Convert.ToString(Sanitizer.GetSafeHtmlFragment(Request.Form["sectionname"]));
                decMinPrice = Convert.ToDecimal(Request.Form["minprice"]);
                decMaxPrice = Convert.ToDecimal(Request.Form["maxprice"]);
                IsGoogleMixIt = Convert.ToBoolean(Request.Form["isgooglemixit"]);
                IsEnableFilter = Convert.ToBoolean(Request.Form["isenablefilters"]);

                strCustomFilter1 = Convert.ToString(Request.Form["customfilter1"]);
                strCustomFilter2 = Convert.ToString(Request.Form["customfilter2"]);
                strCustomFilter3 = Convert.ToString(Request.Form["customfilter3"]);
                strCustomFilter4 = Convert.ToString(Request.Form["customfilter4"]);
                strCustomFilter5 = Convert.ToString(Request.Form["customfilter5"]);


                List<ProductBE> objProductBE;
                ProductBE objProdBE = new ProductBE();
                objProdBE.LanguageId = intLanguageId;
                objProdBE.CurrencyId = intCurrencyId;
                objProdBE.CategoryName = strCategoryName;
                objProdBE.SubCategoryName = strSubCategoryName;
                objProdBE.SubSubCategoryName = strSubSubCategoryName;
                objProdBE.PageNo = intPageNo;
                objProdBE.PageSize = intPageSize;
                objProdBE.SortName = strSortName;
                objProdBE.MinPrice = decMinPrice;
                objProdBE.MaxPrice = decMaxPrice;
                objProdBE.SectionName = strSectionName;
                objProdBE.UserTypeID = iUserTypeID;

                //objProdBE.SearchProductIds = strSearchProductIds;
                List<FeatureBE.FeatureValueBE> objFeatureValeBE;
                objFeatureValeBE = objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "PL_Sorting").FeatureValues.FindAll(v => v.IsEnabled == true);
                if (objFeatureValeBE != null && objFeatureValeBE.Count > 0)
                {
                    foreach (FeatureBE.FeatureValueBE obj in objFeatureValeBE)
                    {
                        if (obj.DefaultValue == "Rating asc" || obj.DefaultValue == "Rating desc" && obj.IsEnabled)
                        {
                            blnrating = true;

                        }
                        if (obj.DefaultValue == "Stock asc" || obj.DefaultValue == "Stock desc" && obj.IsEnabled)
                        {
                            blnstock = true;
                        }

                    }
                }
                List<ProductBE.ProductColorVariantMappping> objProductColorVariantMapppingLst = new List<ProductBE.ProductColorVariantMappping>();
                objProductColorVariantMapppingLst = ProductBL.getProductIDwithVariant<ProductBE.ProductColorVariantMappping>(strSearchProductIds);

                if (objProductColorVariantMapppingLst != null)
                {
                    //objProdBE.SearchProductIds = Convert.ToString(objProductColorVariantMapppingLst[0].ProductId);
                    foreach (ProductBE.ProductColorVariantMappping objProductBEProductColorVariantMappping in objProductColorVariantMapppingLst)
                    {
                        objProdBE.SearchProductIds += Convert.ToString(objProductBEProductColorVariantMappping.ProductId) + ",";
                    }
                    objProdBE.SearchProductIds = objProdBE.SearchProductIds.TrimEnd(',');
                }

                else
                {
                    objProdBE.SearchProductIds = strSearchProductIds;
                }
                if (string.IsNullOrEmpty(strSectionIds))
                {
                    objProdBE.SectionIds = strSectionIds;
                }
                else
                {
                    objProdBE.SectionIds = strSectionIds.Replace("'", "");
                }

                if (string.IsNullOrEmpty(strColorIds))
                {
                    objProdBE.Colors = strColorIds;
                }
                else
                {
                    objProdBE.Colors = strColorIds.Replace("'", "");
                }

                if (string.IsNullOrEmpty(strCustomFilter1))
                {
                    objProdBE.CustomFilter1 = "";
                }
                else
                {
                    objProdBE.CustomFilter1 = strCustomFilter1.Replace("'", "");
                }
                if (string.IsNullOrEmpty(strCustomFilter2))
                {
                    objProdBE.CustomFilter2 = "";
                }
                else
                {
                    objProdBE.CustomFilter2 = strCustomFilter2.Replace("'", "");
                }
                if (string.IsNullOrEmpty(strCustomFilter3))
                {
                    objProdBE.CustomFilter3 = "";
                }
                else
                {
                    objProdBE.CustomFilter3 = strCustomFilter3.Replace("'", "");
                }
                if (string.IsNullOrEmpty(strCustomFilter4))
                {
                    objProdBE.CustomFilter4 = "";
                }
                else
                {
                    objProdBE.CustomFilter4 = strCustomFilter4.Replace("'", "");
                }
                if (string.IsNullOrEmpty(strCustomFilter5))
                {
                    objProdBE.CustomFilter5 = "";
                }
                else
                {
                    objProdBE.CustomFilter5 = strCustomFilter5.Replace("'", "");
                }

                if (strSortName == "Stock asc")
                {

                    CatalogueProductDetail[] objStockCatalogueProductDetail;
                    Stock objStock = new Stock();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    objStock.Credentials = objNetworkCredentials;
                    StockDetails objStockDetails = new StockDetails();
                    //objStockCatalogueProductDetail = objStock.GetCatalogueProducts(963);
                    objStockCatalogueProductDetail = objStock.GetCatalogueProducts(Convert.ToInt32(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == intCurrencyId).CatalogueId));
                    objProductBE = ProductBL.GetProductDataStockCk(objProdBE);


                    for (int i = 0; i < objProductBE.Count; i++)
                    {
                        PWGlobalEcomm.Stock.CatalogueProductDetail varobjStockCatalogueProductDetail = objStockCatalogueProductDetail.FirstOrDefault<PWGlobalEcomm.Stock.CatalogueProductDetail>(p => p.CatalogueAlias.Trim().ToLower().Equals(objProductBE[i].ProductSKUName.Trim().ToLower()));
                        if (varobjStockCatalogueProductDetail != null)
                        {
                            objProductBE[i].stocklevel = varobjStockCatalogueProductDetail.StockLevel;
                        }
                    }

                    objProductBE = objProductBE.GroupBy(x => x.ProductCode).Select(
                           g => new ProductBE
                           {
                               //Key = g.Key,
                               stocklevel = g.Sum(s => s.stocklevel),
                               CategoryName = g.First().CategoryName,
                               SubCategoryName = g.First().SubCategoryName,
                               SubSubCategoryName = g.First().SubSubCategoryName,
                               ProductId = g.First().ProductId,
                               ProductName = g.First().ProductName,
                               ProductColorGroupName = g.First().ProductColorGroupName,
                               Sequence = g.First().Sequence,
                               ProductDescription = g.First().ProductDescription,
                               AsLowAsPrice = g.First().AsLowAsPrice,
                               AsLowAsstrikePrice = g.First().AsLowAsstrikePrice,
                               rating = g.First().rating,
                               SKUcode = g.First().SKUcode,
                               ProductSKUName = g.First().ProductSKUName,
                               DiscountCode = g.First().DiscountCode,
                               ProductCount = g.First().ProductCount,
                               DefaultImageName = g.First().DefaultImageName,
                               SectionIds = g.First().SectionIds,
                               ProductCode = g.First().ProductCode,
                           }).ToList();

                    if (objProductBE != null && objProductBE.Count > 0)
                    {
                        hidTotalRecords.Value = Convert.ToString(objProductBE.Count);// Convert.ToString(objProductBE[0].ProductCount);
                    }

                    var skip = objProdBE.PageSize * (intPageNo - 1);
                    objProductBE = objProductBE.OrderBy(o => o.stocklevel).Skip(skip).Take(Convert.ToInt32(objProdBE.PageSize)).ToList();

                }
                else if (strSortName == "Stock desc")
                {
                    CatalogueProductDetail[] objStockCatalogueProductDetail;
                    Stock objStock = new Stock();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    objStock.Credentials = objNetworkCredentials;
                    StockDetails objStockDetails = new StockDetails();
                    //objStockCatalogueProductDetail = objStock.GetCatalogueProducts(963);
                    objStockCatalogueProductDetail = objStock.GetCatalogueProducts(Convert.ToInt32(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == intCurrencyId).CatalogueId));
                    objProductBE = ProductBL.GetProductDataStockCk(objProdBE);
                    for (int i = 0; i < objProductBE.Count; i++)
                    {
                        PWGlobalEcomm.Stock.CatalogueProductDetail varobjStockCatalogueProductDetail = objStockCatalogueProductDetail.FirstOrDefault<PWGlobalEcomm.Stock.CatalogueProductDetail>(p => p.CatalogueAlias.Trim().ToLower().Equals(objProductBE[i].ProductSKUName.Trim().ToLower()));
                        if (varobjStockCatalogueProductDetail != null)
                        {
                            objProductBE[i].stocklevel = varobjStockCatalogueProductDetail.StockLevel;
                        }
                    }


                    objProductBE = objProductBE.GroupBy(x => x.ProductCode).Select(
                         g => new ProductBE
                         {
                             //Key = g.Key,
                             stocklevel = g.Sum(s => s.stocklevel),
                             CategoryName = g.First().CategoryName,
                             SubCategoryName = g.First().SubCategoryName,
                             SubSubCategoryName = g.First().SubSubCategoryName,
                             ProductId = g.First().ProductId,
                             ProductName = g.First().ProductName,
                             ProductColorGroupName = g.First().ProductColorGroupName,
                             Sequence = g.First().Sequence,
                             ProductDescription = g.First().ProductDescription,
                             AsLowAsPrice = g.First().AsLowAsPrice,
                             AsLowAsstrikePrice = g.First().AsLowAsstrikePrice,
                             rating = g.First().rating,
                             SKUcode = g.First().SKUcode,
                             ProductSKUName = g.First().ProductSKUName,
                             DiscountCode = g.First().DiscountCode,
                             ProductCount = g.First().ProductCount,
                             DefaultImageName = g.First().DefaultImageName,
                             SectionIds = g.First().SectionIds,
                             ProductCode = g.First().ProductCode,
                         }).ToList();

                    if (objProductBE != null && objProductBE.Count > 0)
                    {
                        hidTotalRecords.Value = Convert.ToString(objProductBE.Count);// Convert.ToString(objProductBE[0].ProductCount);
                    }

                    var skip = objProdBE.PageSize * (intPageNo - 1);
                    objProductBE = objProductBE.OrderByDescending(o => o.stocklevel).Skip(skip).Take(Convert.ToInt32(objProdBE.PageSize)).ToList();

                }
                else
                {
                    if (blnstock)
                    {
                        CatalogueProductDetail[] objStockCatalogueProductDetail;
                        Stock objStock = new Stock();
                        System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                        objStock.Credentials = objNetworkCredentials;
                        StockDetails objStockDetails = new StockDetails();
                        //objStockCatalogueProductDetail = objStock.GetCatalogueProducts(963);
                        objStockCatalogueProductDetail = objStock.GetCatalogueProducts(Convert.ToInt32(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == intCurrencyId).CatalogueId));
                        objProductBE = ProductBL.GetProductDataStock(objProdBE);
                        for (int i = 0; i < objProductBE.Count; i++)
                        {
                            PWGlobalEcomm.Stock.CatalogueProductDetail varobjStockCatalogueProductDetail = objStockCatalogueProductDetail.FirstOrDefault<PWGlobalEcomm.Stock.CatalogueProductDetail>(p => p.CatalogueAlias.Trim().ToLower().Equals(objProductBE[i].ProductSKUName.Trim().ToLower()));
                            if (varobjStockCatalogueProductDetail != null)
                            {
                                objProductBE[i].stocklevel = varobjStockCatalogueProductDetail.StockLevel;
                            }
                        }
                        objProductBE = objProductBE.GroupBy(x => x.ProductCode).Select(
                              g => new ProductBE
                              {
                                  //Key = g.Key,
                                  stocklevel = g.Sum(s => s.stocklevel),
                                  CategoryName = g.First().CategoryName,
                                  SubCategoryName = g.First().SubCategoryName,
                                  SubSubCategoryName = g.First().SubSubCategoryName,
                                  ProductId = g.First().ProductId,
                                  ProductName = g.First().ProductName,
                                  ProductColorGroupName = g.First().ProductColorGroupName,
                                  Sequence = g.First().Sequence,
                                  ProductDescription = g.First().ProductDescription,
                                  AsLowAsPrice = g.First().AsLowAsPrice,
                                  AsLowAsstrikePrice = g.First().AsLowAsstrikePrice,
                                  rating = g.First().rating,
                                  SKUcode = g.First().SKUcode.Trim(),
                                  ProductSKUName = g.First().ProductSKUName,
                                  DiscountCode = g.First().DiscountCode,
                                  ProductCount = g.First().ProductCount,
                                  DefaultImageName = g.First().DefaultImageName,
                                  SectionIds = g.First().SectionIds,
                                  ProductCode = g.First().ProductCode,
                              }).ToList();

                        if (objProductBE != null && objProductBE.Count > 0)
                        {
                            hidTotalRecords.Value = Convert.ToString(objProductBE.Count);// Convert.ToString(objProductBE[0].ProductCount);
                        }
                        var skip = objProdBE.PageSize * (intPageNo - 1);
                        objProductBE = objProductBE.Skip(skip).Take(Convert.ToInt32(objProdBE.PageSize)).ToList();
                    }
                    else
                    {
                        objProductBE = ProductBL.GetProductData(objProdBE);
                        if (objProductBE != null && objProductBE.Count > 0)
                        {
                            hidTotalRecords.Value = Convert.ToString(objProductBE[0].ProductCount);// Convert.ToString(objProductBE[0].ProductCount);
                        }
                    }

                }
                if (objProductBE != null && objProductBE.Count > 0)
                {
                    rptProducts.DataSource = objProductBE;
                    rptProducts.DataBind();
                    //hidTotalRecords.Value = Convert.ToString(objProductBE.Count);// Convert.ToString(objProductBE[0].ProductCount);
                }
                else
                {
                    Response.Clear();
                    Response.Write("norecords");
                    Response.End();
                }
                //ClientScript.RegisterStartupScript(this.GetType(), "setViewMode", "ShowProductListingPageView();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowProductListingPageView();", true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-15
        /// Scope   : BindResourceData of the ProductListing page
        /// </summary>
        /// <returns></returns>
        protected void BindResourceData()
        {
            try
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strQuickView = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_QuickView_Text").ResourceValue;
                        strCompare = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Compare_Text").ResourceValue;
                        strViewDetails = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_ViewDetails_Text").ResourceValue;
                        strAsLowAs = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_AsLowAs_Text").ResourceValue;
                        strstock = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Stock_text").ResourceValue;
                        strstocklevel = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Stock_level_text").ResourceValue;
                        strrating = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Product_Listing_Not_yet_rated_text").ResourceValue;
                        stror = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "RecieveCommunication_or_Text").ResourceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }



        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-07-16
        /// Scope   : rptProducts_ItemDataBound of the repeater
        /// </summary>
        /// <param name="sender"></param>        
        /// <param name="e"></param>        
        /// <returns></returns>
        protected void rptProducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Int32 ProductId = 0;
                    string ProductName, ProductColorGroupName = string.Empty;

                    HtmlImage imgProduct = (HtmlImage)e.Item.FindControl("imgProduct");
                    HtmlGenericControl aProductName = (HtmlGenericControl)e.Item.FindControl("aProductName");
                    //HtmlAnchor aProductDetail = (HtmlAnchor)e.Item.FindControl("aProductDetail");
                    HtmlAnchor aQuickView = (HtmlAnchor)e.Item.FindControl("aQuickView");
                    HtmlAnchor aProductImage = (HtmlAnchor)e.Item.FindControl("aProductImage");
                    Literal ltrProductCode = (Literal)e.Item.FindControl("ltrProductCode");
                    HtmlGenericControl spnPrice = (HtmlGenericControl)e.Item.FindControl("spnPrice");
                    HtmlGenericControl spnstrike = (HtmlGenericControl)e.Item.FindControl("spnstrike");
                    HtmlGenericControl spnstock = (HtmlGenericControl)e.Item.FindControl("spnstock");
                    HtmlGenericControl dvDescription = (HtmlGenericControl)e.Item.FindControl("dvDescription");
                    HtmlGenericControl spnPrice1 = (HtmlGenericControl)e.Item.FindControl("spnPrice1");
                    HtmlGenericControl spnor = (HtmlGenericControl)e.Item.FindControl("spnor");
                    HtmlGenericControl dvSectionIcons = (HtmlGenericControl)e.Item.FindControl("dvSectionIcons");
                    HtmlGenericControl liProducts = (HtmlGenericControl)e.Item.FindControl("liProducts");
                    HtmlGenericControl spnCompareText = (HtmlGenericControl)e.Item.FindControl("spnCompareText");
                    HtmlInputCheckBox chkCompare = (HtmlInputCheckBox)e.Item.FindControl("chkCompare");
                    HtmlGenericControl lblProductComparison = (HtmlGenericControl)e.Item.FindControl("lblProductComparison");
                    /* for rating display*/
                    HtmlGenericControl rating = (HtmlGenericControl)e.Item.FindControl("rating");
                    HtmlGenericControl ratingtext = (HtmlGenericControl)e.Item.FindControl("ratingtext");
                    HtmlGenericControl spnRating1 = (HtmlGenericControl)e.Item.FindControl("spnRating1");
                    HtmlGenericControl spnRating2 = (HtmlGenericControl)e.Item.FindControl("spnRating2");
                    HtmlGenericControl spnRating3 = (HtmlGenericControl)e.Item.FindControl("spnRating3");
                    HtmlGenericControl spnRating4 = (HtmlGenericControl)e.Item.FindControl("spnRating4");
                    HtmlGenericControl spnRating5 = (HtmlGenericControl)e.Item.FindControl("spnRating5");


                    ProductId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "productId"));
                    ProductName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductName"));
                    ProductColorGroupName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductColorGroupName"));
                    if (blnrating && blnShowRatingsReview)
                    {
                        switch (Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "rating")))
                        {
                            case 5:
                                spnRating5.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                                goto case 4;
                            case 4:
                                spnRating4.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                                goto case 3;
                            case 3:
                                spnRating3.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                                goto case 2;
                            case 2:
                                spnRating2.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                                goto case 1;
                            case 1:
                                spnRating1.Attributes.Add("class", "glyphicon glyphicon glyphicon-star");
                                break;
                            case 0:
                                rating.Visible = false;
                                ratingtext.Visible = true;
                                ratingtext.InnerHtml = strrating;
                                break;

                        }
                    }
                    else
                    {
                        rating.Visible = false;
                        ratingtext.Visible = false;
                    }
                    if (GlobalFunctions.IsFileExists(GlobalFunctions.GetPhysicalMediumProductImagePath() + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"))))
                        imgProduct.Src = strProductImagePath + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultImageName"));
                    else
                        imgProduct.Src = host + "Images/Products/default.jpg";

                    //if (ProductName.Length > 60)
                    //    aProductName.InnerHtml = ProductName.Substring(0, 60);
                    //else
                    //    aProductName.InnerHtml = ProductName;

                    if (!string.IsNullOrEmpty(ProductColorGroupName))
                    {
                        if (ProductColorGroupName.Length > 60)
                            aProductName.InnerHtml = ProductColorGroupName.Substring(0, 60);
                        else
                            aProductName.InnerHtml = ProductColorGroupName;
                    }
                    else
                    {
                        if (ProductName.Length > 60)
                            aProductName.InnerHtml = ProductName.Substring(0, 60);
                        else
                            aProductName.InnerHtml = ProductName;
                    }

                    ltrProductCode.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductCode"));
                    if (blnQuickViewEnabled)
                    {
                        aQuickView.Visible = true;
                        aQuickView.InnerHtml = strQuickView;
                        aQuickView.Attributes.Add("ProductId", Convert.ToString(ProductId));
                    }
                    else
                    {
                        aQuickView.Visible = false;
                    }
                    if (blnProductComparisonEnabled)
                        lblProductComparison.Visible = true;

                    spnCompareText.InnerHtml = strCompare;

                    if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
                    {
                        culture = Session["MyUICulture"].ToString();

                    }

                    string AsLowAsPrice = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice"), CultureInfo.GetCultureInfo(culture).NumberFormat);
                    string AsLowAsstrikePrice = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsstrikePrice"), CultureInfo.GetCultureInfo(culture).NumberFormat);
                    string stock = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "stocklevel"));

                    if (blnstock)
                    {

                        if (!string.IsNullOrEmpty(stock) && stock != "0")
                        {
                            spnstock.InnerHtml = strstock + " : " + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "stocklevel"));
                        }
                        else
                        {
                            spnstock.InnerHtml = strstocklevel;

                        }
                    }
                    else
                    {
                        spnstock.Visible = false;
                    }
                    if (!string.IsNullOrEmpty(AsLowAsstrikePrice) && AsLowAsstrikePrice != "0")
                    {
                        spnstrike.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsstrikePrice"), CultureInfo.GetCultureInfo(culture).NumberFormat), strCurrencySymbol, intLanguageId);
                        if (!string.IsNullOrEmpty(AsLowAsPrice) && AsLowAsPrice != "0")
                        {
                            spnPrice.InnerHtml = GlobalFunctions.DisplayPriceOrPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice"), CultureInfo.GetCultureInfo(culture).NumberFormat), strCurrencySymbol, intLanguageId);
                        }
                    }
                    else if (!string.IsNullOrEmpty(AsLowAsPrice) && AsLowAsPrice != "0")
                    {
                        //spnPrice.InnerHtml = strAsLowAs + " : <strong>" + strCurrencySymbol + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")) + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DiscountCode")) + "</strong>";
                        //spnPrice.InnerHtml = strAsLowAs + " : <strong>" + strCurrencySymbol + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")) + "</strong>";
                        //spnPrice.Attributes.Remove("class", "col-sm-6");
                        StoreBE objStoreBE = StoreBL.GetStoreDetails();
                        spnPrice1.Attributes.Remove("class");
                        spnPrice1.Attributes.Add("class", "price col-sm-12");
                        spnPrice1.InnerHtml = GlobalFunctions.DisplayPriceAndPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice"), CultureInfo.GetCultureInfo(culture).NumberFormat), strCurrencySymbol, intLanguageId);
                        if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled)
                        {
                            spnor.Attributes.Remove("class");
                            spnor.Attributes.Add("class", "price col-sm-12");

                            spnor.InnerHtml = stror;
                            spnPrice.Attributes.Remove("class");
                            spnPrice.Attributes.Add("class", "price col-sm-12");
                            spnPrice.InnerHtml = GlobalFunctions.DisplayRoundOfPoints(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice"), CultureInfo.GetCultureInfo(culture).NumberFormat), strCurrencySymbol, intLanguageId);
                        }  
                        
                        spnstrike.Visible = false;
                    }
                    else { spnstrike.Visible = false; }

                    string strDescription = GlobalFunctions.RemoveHtmlTags(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductDescription")));
                    if (strDescription.Length > 300)
                    {
                        dvDescription.InnerHtml = strDescription.Substring(0, 300) + "...";
                    }
                    else
                    {
                        dvDescription.InnerHtml = GlobalFunctions.RemoveHtmlTags(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ProductDescription")));
                    }
                    strCategoryName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CategoryName"));
                    strSubCategoryName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SubCategoryName"));
                    strSubSubCategoryName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SubSubCategoryName"));
                    strProductSku = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SKUcode"));
                    //if (!string.IsNullOrEmpty(strCategoryName) && string.IsNullOrEmpty(strSubCategoryName) && string.IsNullOrEmpty(strSubSubCategoryName))
                    //{
                    //    aProductImage.HRef = aProductDetail.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ProductName);
                    //}
                    //else if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && string.IsNullOrEmpty(strSubSubCategoryName))
                    //{
                    //    aProductImage.HRef = aProductDetail.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ProductName);
                    //}
                    //else if (!string.IsNullOrEmpty(strCategoryName) && !string.IsNullOrEmpty(strSubCategoryName) && !string.IsNullOrEmpty(strSubSubCategoryName))
                    //{
                    //    aProductImage.HRef = aProductDetail.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ProductName);
                    //}
                    string ProductNamewithoutSpecialchar = Regex.Replace(ProductName, "[^a-zA-Z0-9]+", " ");

                    if (strCategoryName == strSubCategoryName && strCategoryName == strSubSubCategoryName)
                    {
                        aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(!string.IsNullOrEmpty(ProductNamewithoutSpecialchar) ? ProductNamewithoutSpecialchar : ProductColorGroupName);
                        //aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(!string.IsNullOrEmpty(ProductName) ? ProductName : ProductColorGroupName);
                    }
                    else if (strCategoryName == strSubCategoryName && strSubCategoryName != strSubSubCategoryName)
                    {
                        //aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(!string.IsNullOrEmpty(ProductName) ? ProductName : ProductColorGroupName);
                        aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(!string.IsNullOrEmpty(ProductName) ? ProductNamewithoutSpecialchar : ProductColorGroupName);
                    }
                    else
                    {
                        aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(!string.IsNullOrEmpty(ProductNamewithoutSpecialchar) ? ProductNamewithoutSpecialchar : ProductColorGroupName);
                        //aProductImage.HRef = host + "details/" + GlobalFunctions.EncodeCategoryURL(strCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(strSubSubCategoryName) + "/" + GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + GlobalFunctions.EncodeCategoryURL(!string.IsNullOrEmpty(ProductName) ? ProductName : ProductColorGroupName);
                    }

                    /*Sachin Chauhan Start: 06 03 2016 : Changed the logic of urll binding for product items*/
                    List<String> lstUrlParts = Request.UrlReferrer.AbsoluteUri.ToString().Split('/').ToList<string>();

                    Int32 intCategoryindex = lstUrlParts.FindIndex(x => x.ToLower().Equals("category"));
                    string strProductDetailsUrl = string.Empty;
                   
                    if (intCategoryindex > 0)
                    {
                        for (int i = intCategoryindex + 1; i < lstUrlParts.ToArray<string>().Length; i++)
                        {
                            strProductDetailsUrl += lstUrlParts.ToArray<string>()[i] + "/";
                        }
                        #region
                        int iQt = strProductDetailsUrl.IndexOf('?');
                        if (iQt > 0)
                        {
                            strProductDetailsUrl = strProductDetailsUrl.Substring(0, iQt) + "/";

                        }
                        else
                        {
                            strProductDetailsUrl = strProductDetailsUrl;
                        }
                        #endregion
                        // strProductDetailsUrl += GlobalFunctions.EncodeCategoryURL(GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + ProductName.Trim());
                        strProductDetailsUrl += GlobalFunctions.EncodeCategoryURL(GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + ProductNamewithoutSpecialchar.Trim());
                        aProductImage.HRef = host + "details/" + strProductDetailsUrl;
                    }

                    Int32 intSubCategoryindex = lstUrlParts.FindIndex(x => x.ToLower().Equals("subcategory"));
                    if (intSubCategoryindex > 0)
                    {
                        for (int i = intSubCategoryindex + 1; i < lstUrlParts.ToArray<string>().Length; i++)
                        {
                            strProductDetailsUrl += lstUrlParts.ToArray<string>()[i] + "/";
                        }
                        #region
                        int iQt = strProductDetailsUrl.IndexOf('?');
                        if (iQt > 0)
                        {
                            strProductDetailsUrl = strProductDetailsUrl.Substring(0, iQt) + "/";

                        }
                        else
                        {
                            strProductDetailsUrl = strProductDetailsUrl;
                        }
                        #endregion
                        //strProductDetailsUrl += GlobalFunctions.EncodeCategoryURL(GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + ProductName.Replace("*", "").Trim());
                        strProductDetailsUrl += GlobalFunctions.EncodeCategoryURL(GlobalFunctions.EncodeCategoryURL(ltrProductCode.Text.Replace("*", "").Trim()) + "_" + ProductNamewithoutSpecialchar.Trim());
                        aProductImage.HRef = host + "details/" + strProductDetailsUrl;
                    }
                    /*Sachin Chauhan End:* 06 03 2016*/
                    //aProductDetail.InnerHtml = strViewDetails;

                    //aProductDetail.Attributes.Add("class", "productdetail" + ProductId);

                    if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"))))
                    {
                        string strSectionNames = string.Empty;
                        if (blnShowSectionsIcons)
                        {
                            string strSectionIds = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SectionIds"));
                            string[] strSections = strSectionIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            string strSectionImageData = string.Empty;

                            for (int i = 0; i < strSections.Length; i++)
                            {
                                string[] strSectionIcon = strSections[i].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                if (System.IO.File.Exists(Server.MapPath("~/admin/Images/Section/Icon/" + strSectionIcon[0])))
                                {
                                    strSectionImageData += "<img title='" + strSectionIcon[2].Replace("'", "''") + "' alt='" + strSectionIcon[1].Replace("'", "''") + "' src='" + host + "admin/Images/Section/Icon/" + strSectionIcon[0] + "' class='product_list_icon'>";
                                }
                                if (strSectionIcon.Length > 1)
                                    strSectionNames += strSectionIcon[1].Replace("'", "''").Replace(" ", "-") + " ";
                            }
                            dvSectionIcons.InnerHtml = strSectionImageData;
                        }

                        if (IsGoogleMixIt)
                        {
                            liProducts.Attributes.Add("class", "col-lg-4 col-md-4 col-sm-6 col-xs-12 mix-target " + strSectionNames);
                            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice"))))
                                liProducts.Attributes.Add("data-cat", Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")));
                        }
                    }

                    else
                    {
                        if (IsGoogleMixIt)
                        {
                            liProducts.Attributes.Add("class", "col-lg-4 col-md-4 col-sm-6 col-xs-12 mix-target ");
                            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice"))))
                                liProducts.Attributes.Add("data-cat", Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AsLowAsPrice")));
                        }
                        else if (IsEnableFilter == false)
                            liProducts.Attributes.Add("class", "col-lg-4 col-md-4 col-sm-6 col-xs-12");
                    }

                    if (IsListingType)
                    {
                        liProducts.Attributes.Add("class", "col-xs-12");//for list
                    }
                    else
                    {
                        liProducts.Attributes.Add("class", "col-lg-4 col-md-4 col-sm-6 col-xs-12");//for grid
                    }




                    if (strCompareProductsIds.Contains("'" + ProductId + "'"))
                        chkCompare.Checked = true;
                    else
                        chkCompare.Checked = false;
                    chkCompare.Attributes.Add("data-value", Convert.ToString(ProductId));

                    chkCompare.Attributes.Add("class", "compareproduct" + ProductId);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}