﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.34209.
// 
#pragma warning disable 1591

namespace PWGlobalEcomm.PaymentLive {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.34209")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="PaymentSoap", Namespace="http://idynamics.com/")]
    public partial class Payment : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback TestAuthorisePaymentOperationCompleted;
        
        private System.Threading.SendOrPostCallback AuthorisePaymentOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public Payment() {
            this.Url = global::PWGlobalEcomm.Properties.Settings.Default.PWGlobalEcomm_PaymentLive_Payment;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event TestAuthorisePaymentCompletedEventHandler TestAuthorisePaymentCompleted;
        
        /// <remarks/>
        public event AuthorisePaymentCompletedEventHandler AuthorisePaymentCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://idynamics.com/TestAuthorisePayment", RequestNamespace="http://idynamics.com/", ResponseNamespace="http://idynamics.com/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string TestAuthorisePayment() {
            object[] results = this.Invoke("TestAuthorisePayment", new object[0]);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void TestAuthorisePaymentAsync() {
            this.TestAuthorisePaymentAsync(null);
        }
        
        /// <remarks/>
        public void TestAuthorisePaymentAsync(object userState) {
            if ((this.TestAuthorisePaymentOperationCompleted == null)) {
                this.TestAuthorisePaymentOperationCompleted = new System.Threading.SendOrPostCallback(this.OnTestAuthorisePaymentOperationCompleted);
            }
            this.InvokeAsync("TestAuthorisePayment", new object[0], this.TestAuthorisePaymentOperationCompleted, userState);
        }
        
        private void OnTestAuthorisePaymentOperationCompleted(object arg) {
            if ((this.TestAuthorisePaymentCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.TestAuthorisePaymentCompleted(this, new TestAuthorisePaymentCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://idynamics.com/AuthorisePayment", RequestNamespace="http://idynamics.com/", ResponseNamespace="http://idynamics.com/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public int AuthorisePayment(System.Xml.XmlNode paymentXml) {
            object[] results = this.Invoke("AuthorisePayment", new object[] {
                        paymentXml});
            return ((int)(results[0]));
        }
        
        /// <remarks/>
        public void AuthorisePaymentAsync(System.Xml.XmlNode paymentXml) {
            this.AuthorisePaymentAsync(paymentXml, null);
        }
        
        /// <remarks/>
        public void AuthorisePaymentAsync(System.Xml.XmlNode paymentXml, object userState) {
            if ((this.AuthorisePaymentOperationCompleted == null)) {
                this.AuthorisePaymentOperationCompleted = new System.Threading.SendOrPostCallback(this.OnAuthorisePaymentOperationCompleted);
            }
            this.InvokeAsync("AuthorisePayment", new object[] {
                        paymentXml}, this.AuthorisePaymentOperationCompleted, userState);
        }
        
        private void OnAuthorisePaymentOperationCompleted(object arg) {
            if ((this.AuthorisePaymentCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.AuthorisePaymentCompleted(this, new AuthorisePaymentCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.34209")]
    public delegate void TestAuthorisePaymentCompletedEventHandler(object sender, TestAuthorisePaymentCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.34209")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class TestAuthorisePaymentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal TestAuthorisePaymentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.34209")]
    public delegate void AuthorisePaymentCompletedEventHandler(object sender, AuthorisePaymentCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.34209")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class AuthorisePaymentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal AuthorisePaymentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public int Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591