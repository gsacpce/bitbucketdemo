﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class EmailManagementDA : DataAccessBase
    {
        public static EmailManagementBE getSingleObjectItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            EmailManagementBE EmailBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            { CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value); }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();
                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                { lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower()); }
                                EmailBEInstance = new EmailManagementBE();
                                while (ReaderInstance.Read())
                                { EmailBEInstance = ((EmailManagementBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_EmailManagementBE)); }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            return EmailBEInstance;
        }

        public static List<EmailManagementBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<EmailManagementBE> EmailBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                //Exceptions.WriteInfoLog("EmailManagementDA --> SpName " + spName);
                //Exceptions.WriteInfoLog("EmailManagementDA --> Connection String " + connectionString ) ;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {  
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                                //Exceptions.WriteInfoLog("EmailManagementDA --> key " + para.Key);
                                //Exceptions.WriteInfoLog("EmailManagementDA --> value " + para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                { lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower()); }
                                EmailBEInstance = new List<EmailManagementBE>();
                                while (ReaderInstance.Read())
                                { EmailBEInstance.Add((EmailManagementBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_EmailManagementBE)); }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return EmailBEInstance;
        }
    }
}
