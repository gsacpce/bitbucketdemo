﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class UserPreferredDA:DataAccessBase
{

    public static void InsertUserPreferred(string spName, Dictionary<string, int> param, bool IsStoreConnectionString)
    {
        try
        {
            string connectionString = string.Empty;
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;
            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, int> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                  Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    public static List<UserPreferredBE.UserTypeCurrency> GetCurrencyUserTypeWise(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
    {
        List<UserPreferredBE.UserTypeCurrency> UserTypeCurrencyBEInstance = null;

        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                    {
                        if (ReaderInstance.HasRows)
                        {
                            DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            List<string> lstColNames = new List<string>();

                            foreach (DataRow drRow in dtSchemaTable.Rows)
                            {
                                lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            }
                            UserTypeCurrencyBEInstance = new List<UserPreferredBE.UserTypeCurrency>();

                            while (ReaderInstance.Read())
                            {
                                UserTypeCurrencyBEInstance.Add((UserPreferredBE.UserTypeCurrency)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserPreferredCurrencyBE));
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return UserTypeCurrencyBEInstance;
    }


    public static List<UserPreferredBE> GetUserPreferred(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
    {
        List<UserPreferredBE> UserTypeCurrencyBEInstance = null;

        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                    {
                        if (ReaderInstance.HasRows)
                        {
                            DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            List<string> lstColNames = new List<string>();

                            foreach (DataRow drRow in dtSchemaTable.Rows)
                            {
                                lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            }
                            UserTypeCurrencyBEInstance = new List<UserPreferredBE>();

                            while (ReaderInstance.Read())
                            {
                                UserTypeCurrencyBEInstance.Add((UserPreferredBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserPreferredBE));
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return UserTypeCurrencyBEInstance;
    }
}

