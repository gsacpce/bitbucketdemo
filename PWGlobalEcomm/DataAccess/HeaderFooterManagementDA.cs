﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class HeaderFooterManagementDA : DataAccessBase
    {
        public static List<Object> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString, string objectType)
        {
            List<object> objectInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                objectInstance = new List<object>();
                                while (ReaderInstance.Read())
                                {
                                    objectInstance.Add((object)fillObject(ReaderInstance, lstColNames, objectType));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return objectInstance;
        }

        public static List<Object> getCollectionItem(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString, string objectType)
        {
            List<object> objectInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                objectInstance = new List<object>();
                                while (ReaderInstance.Read())
                                {
                                    objectInstance.Add((object)fillObject(ReaderInstance, lstColNames, objectType));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return objectInstance;
        }

    }
}
