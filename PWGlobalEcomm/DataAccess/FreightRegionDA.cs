﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using PWGlobalEcomm.BusinessEntity;
using System.Data;

namespace PWGlobalEcomm.DataAccess
{
    class FreightRegionDA : DataAccessBase
    {
        public static FreightRegionManagementBE getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {

            FreightRegionManagementBE FreightRegionBEInstance = new FreightRegionManagementBE();
            List<FreightRegionManagementBE.ShipmentMethodMasterBE> ShipmentMethodMasterBEInstance = new List<FreightRegionManagementBE.ShipmentMethodMasterBE>();
            List<FreightRegionManagementBE.ShipmentMethodLanguagesBE> ShipmentMethodLanguagesBEInstance = new List<FreightRegionManagementBE.ShipmentMethodLanguagesBE>();
            List<FreightRegionManagementBE.RegionServiceShipmentBE> RegionServiceShipmentBEInstance = new List<FreightRegionManagementBE.RegionServiceShipmentBE>();
            List<FreightRegionManagementBE.FreightRegionsBE> RegionsBEInstance = new List<FreightRegionManagementBE.FreightRegionsBE>();

            
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();
                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                { lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower()); }
                                while (ReaderInstance.Read())
                                {
                                    ShipmentMethodMasterBEInstance.Add((FreightRegionManagementBE.ShipmentMethodMasterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ShipmentMethodMasterBE));
                                }
                                FreightRegionBEInstance.FreightShipmentMethodMasterLst = ShipmentMethodMasterBEInstance;
                            }

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {
                                        ShipmentMethodLanguagesBEInstance.Add((FreightRegionManagementBE.ShipmentMethodLanguagesBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ShipmentMethodLanguagesBE));
                                    }
                                    FreightRegionBEInstance.FreightShipmentMethodLanguagesBELst = ShipmentMethodLanguagesBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {
                                        RegionServiceShipmentBEInstance.Add((FreightRegionManagementBE.RegionServiceShipmentBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_RegionServiceShipmentBE));
                                    }
                                    FreightRegionBEInstance.FreightRegionServiceShipmentBELst = RegionServiceShipmentBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {
                                        RegionsBEInstance.Add((FreightRegionManagementBE.FreightRegionsBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FreightRegionsBE));
                                    }
                                    FreightRegionBEInstance.FreightRegionsBELst = RegionsBEInstance;
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return FreightRegionBEInstance;
                                
        }

  
        
    }
}
