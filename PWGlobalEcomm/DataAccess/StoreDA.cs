﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Microsoft.SqlServer.Management.Common;
//using Microsoft.SqlServer.Management.Smo;
using System.Data.Common;
using System.Threading;
using PWGlobalEcomm.BusinessEntity;

namespace PWGlobalEcomm.DataAccess
{
    public class StoreDA : DataAccessBase
    {
        #region Static Methods

        public static List<StoreBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<StoreBE> StoreBEInstance = null;
            List<StoreBE.StoreLanguageBE> StoreLanguageBEInstance = null;
            List<StoreBE.StoreCurrencyBE> StoreCurrencyBEInstance = null;
            List<FeatureBE> FeatureBEInstance = null;
            List<FeatureBE.FeatureValueBE> FeatureValueBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                StoreBEInstance = new List<StoreBE>();
                                while (ReaderInstance.Read())
                                {
                                    StoreBEInstance.Add((StoreBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreBE));
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    StoreLanguageBEInstance = new List<StoreBE.StoreLanguageBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        StoreLanguageBEInstance.Add((StoreBE.StoreLanguageBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreLanguageBE));
                                    }
                                    if (StoreLanguageBEInstance.Count > 0)
                                    {
                                        foreach (var item in StoreBEInstance)
                                        {
                                            item.StoreLanguages = StoreLanguageBEInstance.FindAll(x => x.StoreId == item.StoreId);
                                        }
                                    }
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    StoreCurrencyBEInstance = new List<StoreBE.StoreCurrencyBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        StoreCurrencyBEInstance.Add((StoreBE.StoreCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreCurrencyBE));
                                    }
                                    if (StoreCurrencyBEInstance.Count > 0)
                                    {
                                        foreach (var item in StoreBEInstance)
                                        {
                                            item.StoreCurrencies = StoreCurrencyBEInstance.FindAll(x => x.StoreId == item.StoreId);
                                        }
                                    }
                                }
                            }
                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureBEInstance = new List<FeatureBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                            //        }
                            //    }
                            //}
                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureValueBEInstance = new List<FeatureBE.FeatureValueBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureValueBEInstance.Add((FeatureBE.FeatureValueBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureValueBE));
                            //        }
                            //        if (FeatureValueBEInstance.Count > 0)
                            //        {

                            //            foreach (var store in StoreBEInstance)
                            //            {
                            //                List<FeatureBE.FeatureValueBE> FeatureValues = FeatureValueBEInstance.FindAll(x => x.StoreId == store.StoreId);

                            //                foreach (var item in FeatureBEInstance)
                            //                {
                            //                    item.FeatureValues = FeatureValues.FindAll(x => x.FeatureId == item.FeatureId);
                            //                }

                            //                store.StoreFeatures = FeatureBEInstance;
                            //            }
                            //        }
                            //    }
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreBEInstance;
        }

        public static List<StoreBE> getCollectionItem(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            List<StoreBE> StoreBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                StoreBEInstance = new List<StoreBE>();
                                while (ReaderInstance.Read())
                                {
                                    StoreBEInstance.Add((StoreBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreBE));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreBEInstance;
        }

        public static StoreBE getItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            StoreBE StoreBEInstance = null;
            List<StoreBE.StoreLanguageBE> StoreLanguageBEInstance = null;
            List<StoreBE.StoreCurrencyBE> StoreCurrencyBEInstance = null;
            List<FeatureBE> FeatureBEInstance = null;
            List<FeatureBE.FeatureValueBE> FeatureValueBEInstance = null;
            List<EmailManagementBE.FromEmailBE> EmailManagementBE = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    StoreBEInstance = (StoreBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreBE);
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    StoreLanguageBEInstance = new List<StoreBE.StoreLanguageBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        StoreLanguageBEInstance.Add((StoreBE.StoreLanguageBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreLanguageBE));
                                    }
                                    if (StoreLanguageBEInstance.Count > 0)
                                    {
                                        StoreBEInstance.StoreLanguages = StoreLanguageBEInstance.FindAll(x => x.StoreId == StoreBEInstance.StoreId);
                                    }
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    StoreCurrencyBEInstance = new List<StoreBE.StoreCurrencyBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        StoreCurrencyBEInstance.Add((StoreBE.StoreCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreCurrencyBE));
                                    }
                                    if (StoreCurrencyBEInstance.Count > 0)
                                    {
                                        StoreBEInstance.StoreCurrencies = StoreCurrencyBEInstance.FindAll(x => x.StoreId == StoreBEInstance.StoreId);
                                    }
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    FeatureBEInstance = new List<FeatureBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                                    }
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    FeatureValueBEInstance = new List<FeatureBE.FeatureValueBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        FeatureValueBEInstance.Add((FeatureBE.FeatureValueBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureValueBE));
                                    }
                                    if (FeatureValueBEInstance.Count > 0)
                                    {
                                        List<FeatureBE.FeatureValueBE> FeatureValues = FeatureValueBEInstance.FindAll(x => x.StoreId == StoreBEInstance.StoreId);

                                        foreach (var item in FeatureBEInstance)
                                        {
                                            item.FeatureValues = FeatureValues.FindAll(x => x.FeatureId == item.FeatureId);
                                        }
                                        StoreBEInstance.StoreFeatures = FeatureBEInstance;
                                    }
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    EmailManagementBE = new List<EmailManagementBE.FromEmailBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        EmailManagementBE.Add((EmailManagementBE.FromEmailBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FromEmailBE));
                                    }
                                    StoreBEInstance.FromEmailIdAlias = EmailManagementBE;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreBEInstance;
        }

        //public static bool ExecuteNewDatabaseScript(string Script, string NewStoreConnectionString)
        //{
        //    bool status = false;
        //    try
        //    {
        //        SqlConnection sqlConnection = new SqlConnection(NewStoreConnectionString);
        //        ServerConnection svrConnection = new ServerConnection(sqlConnection);
        //        Server server = new Server(svrConnection);
        //        int result = server.ConnectionContext.ExecuteNonQuery(Script);
        //        if (result > 0)
        //            status = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //    return status;
        //}

        /// <summary>
        /// return bool value 
        /// </summary>
        /// <param name="spName"> name of store procedure to be executed</param>
        /// <param name="param"> collection of paramaters of store procedure </param>
        /// <returns>bool value</returns>
        public static bool CopyStoreDetailsFromMCPToStore(string spName, Dictionary<string, DataTable> param, string NewStoreConnectionString, string NewDBName)
        {
            int intValue = 0;
            bool status = false;
            try
            {
                if (GetSqlContextInfo(NewStoreConnectionString, NewDBName))
                {
                    using (SqlConnection ConnectionInstance = new SqlConnection(NewStoreConnectionString))
                    {
                        using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                        {
                            CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                            CommandInstance.CommandType = CommandType.StoredProcedure;
                            if (param != null)
                            {
                                foreach (KeyValuePair<string, DataTable> para in param)
                                {
                                    CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                                }
                            }

                            ConnectionInstance.Open();
                            intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                            if (intValue > 0)
                                status = true;

                            Exceptions.WriteInfoLog("intValue -> " + Convert.ToString(intValue));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return status;
        }


        /// <summary>
        /// return bool value 
        /// </summary>
        /// <param name="spName"> name of store procedure to be executed</param>
        /// <param name="param"> collection of paramaters of store procedure </param>
        /// <returns>bool value</returns>
        public static bool CloneDatabaseData(string spName, Dictionary<string, string> param, string NewStoreConnectionString, string NewDBName)
        {
            int intValue = 0;
            bool status = false;
            try
            {
                if (GetSqlContextInfo(NewStoreConnectionString, NewDBName))
                {
                    using (SqlConnection ConnectionInstance = new SqlConnection(NewStoreConnectionString))
                    {
                        using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                        {
                            CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                            CommandInstance.CommandType = CommandType.StoredProcedure;
                            if (param != null)
                            {
                                foreach (KeyValuePair<string, string> para in param)
                                {
                                    CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                                }
                            }

                            ConnectionInstance.Open();
                            intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                            if (intValue > 0)
                                status = true;

                            Exceptions.WriteInfoLog("intValue -> " + Convert.ToString(intValue));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return status;
        }

        public static DataSet getMCPStoreDetails(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            int intValue = 0;
            //bool status = false;
            DataSet dsMCPStoreDetails = new DataSet();
            SqlDataAdapter dtAdapter = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        dtAdapter = new SqlDataAdapter(CommandInstance);
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();

                        //intValue = Convert.ToInt32(CommandInstance.ExecuteReader());
                        intValue = dtAdapter.Fill(dsMCPStoreDetails);
                        //if (intValue > 0)
                        //    status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return dsMCPStoreDetails;
        }

        public static bool GetSqlContextInfo(string NewStoreConnectionString, string NewDBName)
        {
            bool sqlContext = false;
            SqlConnection ConnectionInstance = new SqlConnection(NewStoreConnectionString);

            Int16 sqlMaxRetries = Convert.ToInt16(ConfigurationManager.AppSettings["StoreDBMaxRetryCount"]);
            int sqlRetrySleep = 30000; // in miliseconds

            for (int retryCount = 0; retryCount <= sqlMaxRetries; retryCount++)
            {
                try
                {
                    ConnectionInstance.Open();
                    Exceptions.WriteInfoLog("Connection Opened.");
                    // get the SQL Context and validate the connection is still valid
                    // using (SqlCommand cmd = new SqlCommand("SELECT CONVERT(NVARCHAR(36), CONTEXT_INFO())", ConnectionInstance))
                    //using (SqlCommand cmd = new SqlCommand("select is_read_only from sys.databases where name = '" + NewDBName + "'", ConnectionInstance))
                    using (SqlCommand cmd = new SqlCommand("update tbl_stores set CreatedDate = getdate();", ConnectionInstance))
                    {
                        Exceptions.WriteInfoLog("Before ExecuteNonQuery: ");
                        Int16 intResult = Convert.ToInt16(cmd.ExecuteNonQuery());
                        Exceptions.WriteInfoLog("IsReadOnly: " + intResult);
                        if (intResult > 0)
                        {
                            retryCount = sqlMaxRetries + 1;
                            sqlContext = true;
                            break;
                        }
                    }
                }

                catch (SqlException ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                    Exceptions.WriteInfoLog("Connection.Open() exception: " + ex.Message);
                    ConnectionInstance.Close();
                    SqlConnection.ClearPool(ConnectionInstance);

                    if (retryCount < sqlMaxRetries)
                    {
                        System.Threading.Thread.Sleep(sqlRetrySleep);
                    }
                }
                finally
                {
                    Exceptions.WriteInfoLog("Connection.Closed in finally block");
                    if (ConnectionInstance.State == ConnectionState.Open)
                    {
                        ConnectionInstance.Close();
                        SqlConnection.ClearPool(ConnectionInstance);
                    }
                }
            }
            return sqlContext;
        }

        /// <summary>
        /// return string value 
        /// </summary>
        /// <param name="spName"> name of store procedure to be executed</param>
        /// <param name="param"> collection of paramaters of store procedure </param>
        /// <returns>string value</returns>
        public static string GetDatabasePassword(string spName, Dictionary<string, string> param, string NewStoreConnectionString)
        {
            string strValue = string.Empty;
            string connectionString = string.Empty;
            try
            {
                using (SqlConnection ConnectionInstance = new SqlConnection(NewStoreConnectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        strValue = Convert.ToString(CommandInstance.ExecuteScalar());
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return strValue;
        }

        /// <summary>
        /// return bool value 
        /// </summary>
        /// <param name="spName"> name of store procedure to be executed</param>
        /// <param name="param"> collection of paramaters of store procedure </param>
        /// <returns>bool value</returns>
        public static bool UpdateNewlyCreatedStoreDetails(string spName, Dictionary<string, string> param, string NewStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                using (SqlConnection ConnectionInstance = new SqlConnection(NewStoreConnectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return status;
        }

        public static List<StoreBE.ContactBE> getContactInfo(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            List<StoreBE.ContactBE> objContactBE = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                objContactBE = new List<StoreBE.ContactBE>();
                                while (ReaderInstance.Read())
                                {
                                    objContactBE.Add((StoreBE.ContactBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ContactBE));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return objContactBE;
        }

        public static List<StoreBE.StoreCurrencyBE> getStoreCurrencyItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<StoreBE.StoreCurrencyBE> StoreCurrencyBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                StoreCurrencyBEInstance = new List<StoreBE.StoreCurrencyBE>();

                                while (ReaderInstance.Read())
                                {
                                    StoreCurrencyBEInstance.Add((StoreBE.StoreCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreCurrencyBE));
                                }
                            }


                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureBEInstance = new List<FeatureBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                            //        }
                            //    }
                            //}
                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureValueBEInstance = new List<FeatureBE.FeatureValueBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureValueBEInstance.Add((FeatureBE.FeatureValueBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureValueBE));
                            //        }
                            //        if (FeatureValueBEInstance.Count > 0)
                            //        {

                            //            foreach (var store in StoreBEInstance)
                            //            {
                            //                List<FeatureBE.FeatureValueBE> FeatureValues = FeatureValueBEInstance.FindAll(x => x.StoreId == store.StoreId);

                            //                foreach (var item in FeatureBEInstance)
                            //                {
                            //                    item.FeatureValues = FeatureValues.FindAll(x => x.FeatureId == item.FeatureId);
                            //                }

                            //                store.StoreFeatures = FeatureBEInstance;
                            //            }
                            //        }
                            //    }
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreCurrencyBEInstance;
        }




        public static List<StoreBE.StoreLanguageBE> getStoreLanguageItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<StoreBE.StoreLanguageBE> StoreLanguageBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                StoreLanguageBEInstance = new List<StoreBE.StoreLanguageBE>();

                                while (ReaderInstance.Read())
                                {
                                    StoreLanguageBEInstance.Add((StoreBE.StoreLanguageBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreLanguageBE));
                                }
                            }


                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureBEInstance = new List<FeatureBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                            //        }
                            //    }
                            //}
                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureValueBEInstance = new List<FeatureBE.FeatureValueBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureValueBEInstance.Add((FeatureBE.FeatureValueBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureValueBE));
                            //        }
                            //        if (FeatureValueBEInstance.Count > 0)
                            //        {

                            //            foreach (var store in StoreBEInstance)
                            //            {
                            //                List<FeatureBE.FeatureValueBE> FeatureValues = FeatureValueBEInstance.FindAll(x => x.StoreId == store.StoreId);

                            //                foreach (var item in FeatureBEInstance)
                            //                {
                            //                    item.FeatureValues = FeatureValues.FindAll(x => x.FeatureId == item.FeatureId);
                            //                }

                            //                store.StoreFeatures = FeatureBEInstance;
                            //            }
                            //        }
                            //    }
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreLanguageBEInstance;
        }
        #endregion
        public static List<StoreBE.StoreCurrencyBE> getSingleItem(string spName, Dictionary<string, string> param, string IsStoreConnectionString)
        {
            List<StoreBE.StoreCurrencyBE> StoreCurrencyBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                connectionString = IsStoreConnectionString;


                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();
                                StoreCurrencyBEInstance = new List<StoreBE.StoreCurrencyBE>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    //  StoreBEInstance = (StoreBE.StoreCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreBE);
                                    StoreCurrencyBEInstance.Add((StoreBE.StoreCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreCurrencyBE));
                                }
                            }



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreCurrencyBEInstance;
        }
        public static List<StoreBE.StoreCurrencyBE> getSingleItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<StoreBE.StoreCurrencyBE> StoreCurrencyBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();
                                StoreCurrencyBEInstance = new List<StoreBE.StoreCurrencyBE>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                  //  StoreBEInstance = (StoreBE.StoreCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreBE);
                                    StoreCurrencyBEInstance.Add((StoreBE.StoreCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreCurrencyBE));
                                }
                            }



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreCurrencyBEInstance;
        }


        public static List<StoreReferral.referralurlBE> getrefferal(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<StoreReferral.referralurlBE> referralurlBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();
                                referralurlBEInstance = new List<StoreReferral.referralurlBE>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    //  StoreBEInstance = (StoreBE.StoreCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StoreBE);
                                    referralurlBEInstance.Add((StoreReferral.referralurlBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StorereferralurlBE));
                                }
                            }



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return referralurlBEInstance;
        }



    }
}
