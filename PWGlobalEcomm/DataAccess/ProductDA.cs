﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class ProductDA : DataAccessBase
    {
        #region Static Methods

        public static List<ProductBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<ProductBE> ProductBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                ProductBEInstance = new List<ProductBE>();
                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance.Add((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return ProductBEInstance;
        }

        public static List<ProductBE> getCollectionItem(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            List<ProductBE> ProductBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                ProductBEInstance = new List<ProductBE>();
                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance.Add((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return ProductBEInstance;
        }

        public static ProductBE getFilterCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ProductBE ProductBEInstance = new ProductBE();
            List<ProductBE.ProductCategoryFilterBE> ProductCategoryFilterBEInstance = null;
            List<ProductBE.ProductPriceFilterBE> ProductPriceFilterBEInstance = null;
            List<ProductBE.ProductColorFilterBE> ProductColorFilterBEInstance = null;
            List<ProductBE.ProductCustomFilterBE> ProductCustomFilterBEInstance = null;
            List<ProductBE.ProductSectionFilterBE> ProductSectionFilterBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                ProductCategoryFilterBEInstance = new List<ProductBE.ProductCategoryFilterBE>();
                                while (ReaderInstance.Read())
                                {
                                    ProductCategoryFilterBEInstance.Add((ProductBE.ProductCategoryFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductCategoryFilterBE));
                                }

                                ProductBEInstance.ProductCategoryFilter = ProductCategoryFilterBEInstance;
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    ProductPriceFilterBEInstance = new List<ProductBE.ProductPriceFilterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        ProductPriceFilterBEInstance.Add((ProductBE.ProductPriceFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductPriceFilterBE));
                                    }
                                    ProductBEInstance.ProductPriceFilter = ProductPriceFilterBEInstance;
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    ProductColorFilterBEInstance = new List<ProductBE.ProductColorFilterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        ProductColorFilterBEInstance.Add((ProductBE.ProductColorFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductColorFilterBE));
                                    }
                                    ProductBEInstance.ProductColorFilter = ProductColorFilterBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    ProductCustomFilterBEInstance = new List<ProductBE.ProductCustomFilterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        ProductCustomFilterBEInstance.Add((ProductBE.ProductCustomFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductCustomFilterBE));
                                    }
                                    ProductBEInstance.ProductCustomFilter1 = ProductCustomFilterBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                ProductCustomFilterBEInstance = null;
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    ProductCustomFilterBEInstance = new List<ProductBE.ProductCustomFilterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        ProductCustomFilterBEInstance.Add((ProductBE.ProductCustomFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductCustomFilterBE));
                                    }
                                    ProductBEInstance.ProductCustomFilter2 = ProductCustomFilterBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                ProductCustomFilterBEInstance = null;
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    ProductCustomFilterBEInstance = new List<ProductBE.ProductCustomFilterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        ProductCustomFilterBEInstance.Add((ProductBE.ProductCustomFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductCustomFilterBE));
                                    }
                                    ProductBEInstance.ProductCustomFilter3 = ProductCustomFilterBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                ProductCustomFilterBEInstance = null;
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    ProductCustomFilterBEInstance = new List<ProductBE.ProductCustomFilterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        ProductCustomFilterBEInstance.Add((ProductBE.ProductCustomFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductCustomFilterBE));
                                    }
                                    ProductBEInstance.ProductCustomFilter4 = ProductCustomFilterBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                ProductCustomFilterBEInstance = null;
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    ProductCustomFilterBEInstance = new List<ProductBE.ProductCustomFilterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        ProductCustomFilterBEInstance.Add((ProductBE.ProductCustomFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductCustomFilterBE));
                                    }
                                    ProductBEInstance.ProductCustomFilter5 = ProductCustomFilterBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    ProductSectionFilterBEInstance = new List<ProductBE.ProductSectionFilterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        ProductSectionFilterBEInstance.Add((ProductBE.ProductSectionFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductSectionFilterBE));
                                    }
                                    ProductBEInstance.ProductSectionFilter = ProductSectionFilterBEInstance;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }

        public static ProductBE getProductDetailsAdmin(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ProductBE ProductBEInstance = new ProductBE();
            List<ProductBE.PriceBreakDetails> PropGetAllPriceBreakDetails = null;
            List<ProductBE.PriceBreaks> PropGetAllPriceBreak = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }


                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance = ((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                                }
                            }
                            //while (ReaderInstance.NextResult())
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    PropGetAllPriceBreak = new List<ProductBE.PriceBreaks>();
                                    while (ReaderInstance.Read())
                                    {

                                        PropGetAllPriceBreak.Add((ProductBE.PriceBreaks)fillObject(ReaderInstance, lstColNames, Constants.Entity_PriceBreakBE));
                                    }
                                    ProductBEInstance.PropGetAllPriceBreak = PropGetAllPriceBreak;
                                }


                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    PropGetAllPriceBreakDetails = new List<ProductBE.PriceBreakDetails>();
                                    while (ReaderInstance.Read())
                                    {

                                        PropGetAllPriceBreakDetails.Add((ProductBE.PriceBreakDetails)fillObject(ReaderInstance, lstColNames, Constants.Entity_PriceBreakDetailsBE));
                                    }
                                    ProductBEInstance.PropGetAllPriceBreakDetails = PropGetAllPriceBreakDetails;
                                }
                            }



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }

        public static ProductBE getProductVariantType(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ProductBE ProductBEInstance = new ProductBE();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }


                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance.PropGetAllVariantType.Add((ProductBE.ProductVariantType)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductVariantType));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }

        public static ProductBE getProductVariant(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ProductBE ProductBEInstance = new ProductBE();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance = ((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                                }


                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }


                                    while (ReaderInstance.Read())
                                    {
                                        ProductBEInstance.PropGetAllVariantType.Add((ProductBE.ProductVariantType)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductVariantType));
                                    }
                                }
                                if (ReaderInstance.NextResult())
                                {


                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {

                                        ProductBEInstance.PropGetAllProductVariants.Add((ProductBE.ProductVariants)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductVariant));
                                    }
                                }


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }

        public static ProductBE getProductDetails(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ProductBE ProductBEInstance = new ProductBE();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            // Fills Basic Product Details
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance = ((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                                }


                            }
                            //Fills Product SKU's
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }


                                    while (ReaderInstance.Read())
                                    {
                                        ProductBEInstance.PropGetAllProductSKU.Add((ProductBE.ProductSKU)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductSKU));
                                    }
                                }
                                //Fill Product Details
                                if (ReaderInstance.NextResult())
                                {


                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {

                                        ProductBEInstance.PropGetAllProductDetails.Add((ProductBE.ProductDetails)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductDetails));
                                        ProductBEInstance.PropGetAllProductDetailGroups.Add((ProductBE.ProductDetailGroups)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductDetailGroups));
                                        ProductBEInstance.PropGetAllProductDetailGroupMappings.Add((ProductBE.ProductDetailGroupMappings)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductDetailGroupMappings));
                                    }
                                }
                                //Fill Related Products
                                if (ReaderInstance.NextResult())
                                {


                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {

                                        ProductBEInstance.PropGetAllYouMayAlsoLikeProducts.Add((ProductBE.YouMayAlsoLike)fillObject(ReaderInstance, lstColNames, Constants.Entity_YouMayAlsoLike));

                                    }
                                }
                                //Fill Product Price Break
                                if (ReaderInstance.NextResult())
                                {


                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {

                                        ProductBEInstance.PropGetAllPriceBreak.Add((ProductBE.PriceBreaks)fillObject(ReaderInstance, lstColNames, Constants.Entity_PriceBreakBE));

                                    }
                                }
                                //Fill Product Price Break Details
                                if (ReaderInstance.NextResult())
                                {


                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {

                                        ProductBEInstance.PropGetAllPriceBreakDetails.Add((ProductBE.PriceBreakDetails)fillObject(ReaderInstance, lstColNames, Constants.Entity_PriceBreakDetailsBE));

                                    }
                                }
                                //Fill Related Categories
                                if (ReaderInstance.NextResult())
                                {


                                    //DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    //List<string> lstColNames = new List<string>();

                                    //foreach (DataRow drRow in dtSchemaTable.Rows)
                                    //{
                                    //    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    //}
                                    //while (ReaderInstance.Read())
                                    //{

                                    //    ProductBEInstance.PropGetAllPriceBreak.Add((ProductBE.PriceBreaks)fillObject(ReaderInstance, lstColNames, Constants.Entity_PriceBreakBE));

                                    //}
                                }
                                //Fill Category Details
                                if (ReaderInstance.NextResult())
                                {


                                    //DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    //List<string> lstColNames = new List<string>();

                                    //foreach (DataRow drRow in dtSchemaTable.Rows)
                                    //{
                                    //    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    //}
                                    //while (ReaderInstance.Read())
                                    //{

                                    //    ProductBEInstance.PropGetAllPriceBreak.Add((ProductBE.PriceBreaks)fillObject(ReaderInstance, lstColNames, Constants.Entity_PriceBreakBE));

                                    //}
                                }
                                //Fill Variants
                                if (ReaderInstance.NextResult())
                                {


                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {

                                        ProductBEInstance.PropGetAllProductVariants.Add((ProductBE.ProductVariants)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductVariant));
                                        ProductBEInstance.PropGetAllVariantType.Add((ProductBE.ProductVariantType)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductVariantType));

                                    }
                                }


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }

        public static List<ProductBE.RecentlyViewedProducts> getRecentlyViewedProducts(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ProductBE ProductBEInstance = new ProductBE();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            // Fills Basic Product Details
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance.PropGetAllRecentlyViewedProducts.Add((ProductBE.RecentlyViewedProducts)fillObject(ReaderInstance, lstColNames, Constants.Entity_RecentlyViewedProducts));
                                }


                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance.PropGetAllRecentlyViewedProducts;
        }

        internal static List<ProductBE> GetProductByCategoryId(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ProductBE ProductBEInstance = new ProductBE();
            string connectionString = string.Empty;
            List<ProductBE> lstProducts = new List<ProductBE>();
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            // Fills Basic Product Details
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    lstProducts.Add((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                                }


                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProducts;
        }

        internal static List<ProductBE> GetAllProducts(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ProductBE ProductBEInstance = new ProductBE();
            string connectionString = string.Empty;
            List<ProductBE> lstProducts = new List<ProductBE>();
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            // Fills Basic Product Details
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    lstProducts.Add((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                                }


                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProducts;
        }

        public static List<ProductBE> GetCompareProductsDetail(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<ProductBE> lstProducts = new List<ProductBE>();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            // Fills Basic Product Details
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    lstProducts.Add((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                                }
                            }
                            //Fill Product Details
                            if (ReaderInstance.NextResult())
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                while (ReaderInstance.Read())
                                {
                                    lstProducts[0].PropGetAllProductDetails.Add((ProductBE.ProductDetails)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductDetails));
                                    lstProducts[0].PropGetAllProductDetailGroups.Add((ProductBE.ProductDetailGroups)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductDetailGroups));
                                    lstProducts[0].PropGetAllProductDetailGroupMappings.Add((ProductBE.ProductDetailGroupMappings)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductDetailGroupMappings));
                                }
                                 
                                //Fill Product Price Break with details
                                if (ReaderInstance.NextResult())
                                {
                                    DataTable dtSchemaTable1 = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames1 = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable1.Rows)
                                    {
                                        lstColNames1.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    while (ReaderInstance.Read())
                                    {
                                        lstProducts[0].PropGetAllPriceBreak.Add((ProductBE.PriceBreaks)fillObject(ReaderInstance, lstColNames1, Constants.Entity_PriceBreakBE));
                                        lstProducts[0].PropGetAllPriceBreakDetails.Add((ProductBE.PriceBreakDetails)fillObject(ReaderInstance, lstColNames1, Constants.Entity_PriceBreakDetailsBE));
                                    }
                                }
                                //Fill Product Price Break Details
                                //if (ReaderInstance.NextResult())
                                //{
                                //    DataTable dtSchemaTable2 = ReaderInstance.GetSchemaTable();
                                //    List<string> lstColNames2 = new List<string>();

                                //    foreach (DataRow drRow in dtSchemaTable2.Rows)
                                //    {
                                //        lstColNames2.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                //    }
                                //    while (ReaderInstance.Read())
                                //    {

                                //        lstProducts[0].PropGetAllPriceBreakDetails.Add((ProductBE.PriceBreakDetails)fillObject(ReaderInstance, lstColNames2, Constants.Entity_PriceBreakDetailsBE));

                                //    }
                                //}
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProducts;
        }

        public static List<ProductBE.ProductAttributes> getAttributeCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<ProductBE.ProductAttributes> ProductBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                ProductBEInstance = new List<ProductBE.ProductAttributes>();
                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance.Add((ProductBE.ProductAttributes)fillObject(ReaderInstance, lstColNames, Constants.ProductAttributes));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return ProductBEInstance;
        }

        public static List<ProductBE.ProductImportHistory> getImportHistoryCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<ProductBE.ProductImportHistory> ProductBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                ProductBEInstance = new List<ProductBE.ProductImportHistory>();
                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance.Add((ProductBE.ProductImportHistory)fillObject(ReaderInstance, lstColNames, Constants.ProductImportHistory));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return ProductBEInstance;
        }

        public static bool TruncateProductAttribute(string spname, bool IsStoreConnectionString)
        {
            #region not in use
            string connectionString = string.Empty;
             if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spname, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        ConnectionInstance.Open();
                        int i = CommandInstance.ExecuteNonQuery();
                        return true;

                    }
                }
            #endregion
        }

        internal static string selectQuery(DataTable dt)
        {
            if (dt != null && dt.Columns.Count > 0)
            {
                string strQuery = "select top 1 ";
                foreach (DataColumn dc in dt.Columns)
                {
                    strQuery += ",[" + dc.Caption + "]";
                }
                strQuery = strQuery.Replace("select top 1 ,", "select top 1 ");
                strQuery += " from " + dt.TableName;
                return strQuery;
            }
            else
                return "select top 1 * from " + dt.TableName + " with (readpast)";
        }

        public static int SaveUpdateProductAttribute(DataSet dsData, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;
            int res = 1;
            if (dsData == null) return -1;
            using (SqlConnection myConnection = new SqlConnection(connectionString))
            {
                myConnection.Open();
                SqlTransaction transaction = myConnection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                try
                {
                    foreach (DataTable dt in dsData.Tables)
                    {
                       
                        if (dt != null && dt.GetChanges() != null && dt.GetChanges().Rows.Count > 0)
                        {
                            SqlDataAdapter da = new SqlDataAdapter();
                            string strQuery = selectQuery(dt);
                            SqlCommand cmd = new SqlCommand(strQuery, myConnection, transaction);
                            da.SelectCommand = cmd;
                            //da.UpdateCommand = new SqlCommandBuilder(da).GetUpdateCommand();
                            SqlCommandBuilder cmb = new SqlCommandBuilder();
                            cmb.DataAdapter = da;
                            da.UpdateBatchSize = dt.Rows.Count;
                            da.Update(dt);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                    res = -1;
                    transaction.Rollback();
                }
                finally
                {
                    if (myConnection != null && myConnection.State == ConnectionState.Open)
                        myConnection.Close();
                }
            }
            return res;
        }
       
        public static  bool BulkInsertProductAttribute(DataTable dt, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                DataTable table = dt;
                DataRow[] rowArray = table.Select();


                using (SqlBulkCopy bulkcopy = new SqlBulkCopy(con, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null))
                {
                    bulkcopy.DestinationTableName = "dbo.Tbl_ProductAttributeManagement";
                    //bulkcopy.DestinationTableName = "dbo.test";
                    try
                    {
                        bulkcopy.WriteToServer(rowArray);
                        SqlBulkCopyOptions.FireTriggers.ToString();
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                    }
                }
                return true;
            }
        }

        public static DataTable GettableRowProductAttribute(string spname,bool IsStoreConnectionString)
        {
            DataTable dtAttribute = new DataTable();
            try
            {
                string connectionString = string.Empty;
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString; 
               

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spname, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                        da.Fill(dtAttribute);
                        #region
                        //ConnectionInstance.Open();
                        //int i = CommandInstance.ExecuteNonQuery();
                        //return true; 
                        #endregion
                    }
                }
            }
            catch (Exception ex) 
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return dtAttribute;
        }

        public static List<ProductBE.ProductImage> GetProductImages(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
           List<ProductBE.ProductImage> ProductBEInstance = new List<ProductBE.ProductImage>();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            // Fills Basic Product Details
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    ProductBEInstance.Add((ProductBE.ProductImage)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductImage));
                                }


                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }
     

      

     
        #endregion


        public static YouMayAlsoLikeMappingBE getCollectionYouMayLikeTypeMapping(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            YouMayAlsoLikeMappingBE YouMayAlsoLikeMappingBE = null;
            List<YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory> YouMayAlsoLikecategoryBE = null;
            List<YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice> YouMayAlsoLikepriceBE = null;
            List<YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct> YouMayAlsoLikeproductBE = null;
            

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            #region YouMayAlsoLikeMappingBE
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                YouMayAlsoLikeMappingBE = new YouMayAlsoLikeMappingBE();
                                while (ReaderInstance.Read())
                                {
                                    YouMayAlsoLikeMappingBE = ((YouMayAlsoLikeMappingBE)fillObject(ReaderInstance, lstColNames, Constants.YouMayAlsoLikeMappingBE));
                                    
                                }

                                
                            }
                            #endregion

                            #region YouMayAlsoLikecategory
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    YouMayAlsoLikecategoryBE = new List<YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory>();
                                    while (ReaderInstance.Read())
                                    {
                                        YouMayAlsoLikecategoryBE.Add((YouMayAlsoLikeMappingBE.YouMayAlsoLikecategory)fillObject(ReaderInstance, lstColNames, Constants.YouMayAlsoLikeMappingBEYouMayAlsoLikecategory));
                                    }

                                    YouMayAlsoLikeMappingBE.lstYouMayAlsoLikecategory = YouMayAlsoLikecategoryBE;
                                }
                            }
                            #endregion


                            #region YouMayAlsoLikeprice
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    YouMayAlsoLikepriceBE = new List<YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice>();
                                    while (ReaderInstance.Read())
                                    {
                                        YouMayAlsoLikepriceBE.Add((YouMayAlsoLikeMappingBE.YouMayAlsoLikeprice)fillObject(ReaderInstance, lstColNames, Constants.YouMayAlsoLikeMappingBEYouMayAlsoLikeprice));
                                    }

                                    YouMayAlsoLikeMappingBE.lstYouMayAlsoLikeprice = YouMayAlsoLikepriceBE;
                                }
                            }
                            #endregion
                            #region YouMayAlsoLikeproduct
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    YouMayAlsoLikeproductBE = new List<YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct>();
                                    while (ReaderInstance.Read())
                                    {
                                        YouMayAlsoLikeproductBE.Add((YouMayAlsoLikeMappingBE.YouMayAlsoLikeproduct)fillObject(ReaderInstance, lstColNames, Constants.YouMayAlsoLikeMappingBEYouMayAlsoLikeproduct));
                                    }
                                    YouMayAlsoLikeMappingBE.lstYouMayAlsoLikeproduct = YouMayAlsoLikeproductBE;
                                }
                            }
                            #endregion

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return YouMayAlsoLikeMappingBE;
        }



    }
}
