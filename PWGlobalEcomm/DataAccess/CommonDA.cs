﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    class CommonDA : DataAccessBase
    {
        #region Static Methods

        public static List<Object> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString, string objectType)
        {
            List<object> objectInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                objectInstance = new List<object>();
                                while (ReaderInstance.Read())
                                {
                                    objectInstance.Add((object)fillObject(ReaderInstance, lstColNames, objectType));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return objectInstance;
        }

        public static T GetInHeaderProperty<T>() where T : new()
        {
            dynamic result = new T();
            result.CompanyId = "Test";
            result.UserId = "Test";
            result.Password = "Test";
            result.MessageId = " ";
            return (T)result;
        }

        /// <summary>
        /// gets single specified object
        /// </summary>
        /// <param name="spName"> name of store procedure to be executed</param>
        /// <param name="param"> collection of paramaters of store procedure </param>
        /// <param name="objectName"> Specify the name of the entity to be filled.</param>
        /// <param name="IsTemplateConnectionString">Which connection string to use Template or Corporate Store</param>
        /// <returns>object</returns>
        public static object getItem(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, string objectName, bool IsStoreConnectionString) 
        {
            object objectInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    objectInstance = fillObject(ReaderInstance, lstColNames, objectName);
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return objectInstance;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 29/09/2015
        /// Scope   : Gets Collection based on Type Passed
        /// </summary>
        /// <returns>T</returns>
        public static List<T> getCollectionItem<T>(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<T> objectInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                objectInstance = new List<T>();
                                while (ReaderInstance.Read())
                                {
                                    objectInstance.Add((T)fillObject(ReaderInstance, lstColNames, typeof(T).ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return objectInstance;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 29/09/2015
        /// Scope   : Gets Collection based on Type Passed
        /// </summary>
        /// <returns>T</returns>
        public static List<T> getCollectionItem<T>(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            List<T> objectInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                objectInstance = new List<T>();
                                while (ReaderInstance.Read())
                                {
                                    objectInstance.Add((T)fillObject(ReaderInstance, lstColNames, typeof(T).ToString()));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return objectInstance;
        }

        public static List<Object> getCollectionItem(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString, string objectType)
        {
            List<object> objectInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                objectInstance = new List<object>();
                                while (ReaderInstance.Read())
                                {
                                    objectInstance.Add((object)fillObject(ReaderInstance, lstColNames, objectType));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return objectInstance;
        }

        public static void SaveBulkData(string spName, Dictionary<string, DataTable> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;



                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, DataTable> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        // SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();
                        int IsSaved = CommandInstance.ExecuteNonQuery();
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        paramOutClone["IsSaved"] = Convert.ToString(IsSaved > 0 ? true : false);

                        paramOutClone["IsSaved"] = Convert.ToString(IsSaved > 0 ? true : false);
                        // foreach (KeyValuePair<string, string> para in paramOut)
                        //{
                        //paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));

                        //}
                        paramOut = paramOutClone;
                        paramOutClone = null;

                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        //public static List<FreightManagementBE> getFreightManagementItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        //{
        //    FreightManagementBE FreightManagementBEInstance = new FreightManagementBE();
        //    List<FreightManagementBE> FreightManagementBEInstanceLST = new List<FreightManagementBE>();
        //    List<FreightManagementBE.FreightMultiplerCurrency> FreightMultiplerCurrencyInstance = new List<FreightManagementBE.FreightMultiplerCurrency>();

        //    string connectionString = string.Empty;
        //    try
        //    {
        //        if (IsStoreConnectionString)
        //            connectionString = Constants.strStoreConnectionString;
        //        else
        //            connectionString = Constants.strMCPConnectionString;

        //        using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
        //        {
        //            using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
        //            {
        //                CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
        //                CommandInstance.CommandType = CommandType.StoredProcedure;
        //                if (param != null)
        //                {
        //                    foreach (KeyValuePair<string, string> para in param)
        //                    {
        //                        CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
        //                    }
        //                }
        //                ConnectionInstance.Open();
        //                using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
        //                {
        //                    #region "FreightManagementBE"
        //                    if (ReaderInstance.HasRows)
        //                    {
        //                        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
        //                        List<string> lstColNames = new List<string>();

        //                        foreach (DataRow drRow in dtSchemaTable.Rows)
        //                        {
        //                            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
        //                        }

        //                        while (ReaderInstance.Read())
        //                        {
        //                            FreightManagementBEInstanceLST.Add((FreightManagementBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FreightManagementBE));
        //                        }
        //                    }
        //                    #endregion
        //                    #region "FreightMultiplerCurrency"
        //                    if (ReaderInstance.NextResult())
        //                    {
        //                        if (ReaderInstance.HasRows)
        //                        {
        //                            DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
        //                            List<string> lstColNames = new List<string>();

        //                            foreach (DataRow drRow in dtSchemaTable.Rows)
        //                            {
        //                                lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
        //                            }
        //                            FreightMultiplerCurrencyInstance = new List<FreightManagementBE.FreightMultiplerCurrency>();
        //                            while (ReaderInstance.Read())
        //                            {
        //                                FreightMultiplerCurrencyInstance.Add((FreightManagementBE.FreightMultiplerCurrency)fillObject(ReaderInstance, lstColNames, Constants.Entity_FreightManagementBEFreightMultiplerCurrency));
        //                            }
        //                            FreightManagementBEInstance.lstFreightMultiplerCurrency = FreightMultiplerCurrencyInstance;
        //                        }
        //                    }
        //                    #endregion
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //    return FreightManagementBEInstanceLST;
        //}

        #endregion





    }
}
