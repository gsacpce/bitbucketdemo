﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    class MichelinSmileSSODA : DataAccessBase
    {
        public static bool AddPGTValue(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return status;
        }

        public static DataTable GetPGTValue(string spName, string strpgtIou, bool IsStoreConnectionString)
        {
            DataTable dt = new DataTable();
            //List<int> CouponBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        CommandInstance.Parameters.AddWithValue("@strpgtIou", strpgtIou);
                        ConnectionInstance.Open();
                        SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                        da.Fill(dt);
                    }
                }

            }
            catch (Exception ex) 
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return dt;
        }

    }
}
