﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;

namespace PWGlobalEcomm.DataAccess
{
    public class UserTypesDA : DataAccessBase
    {
        public static UserTypesBE getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            UserTypesBE UserTypesBEInstance = null;
            List<UserTypesBE.UserTypeDetails> UserTypesDetailsBEInstance = null;
            List<UserTypesBE.UserTypeLanguages> UserTypeLanguagesBEInstance = null;
            List<UserTypesBE.UserTypeCatalogue> UserTypeCatalogueBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            #region UserTypesBE

                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                UserTypesBEInstance = new UserTypesBE();
                                while (ReaderInstance.Read())
                                {
                                    UserTypesBEInstance = ((UserTypesBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypesBE));
                                }
                            }

                            #endregion

                            #region UserTypesDetails

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserTypesDetailsBEInstance = new List<UserTypesBE.UserTypeDetails>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserTypesDetailsBEInstance.Add((UserTypesBE.UserTypeDetails)fillObject(ReaderInstance, lstColNames, Constants.UserTypesBEUserTypeDetails));
                                    }

                                    UserTypesBEInstance.lstUserTypeDetails = UserTypesDetailsBEInstance;
                                }
                            }

                            #endregion

                            #region UserTypeLanguages

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserTypeLanguagesBEInstance = new List<UserTypesBE.UserTypeLanguages>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserTypeLanguagesBEInstance.Add((UserTypesBE.UserTypeLanguages)fillObject(ReaderInstance, lstColNames, Constants.UserTypesBEUserTypesLanguages));
                                    }

                                    UserTypesBEInstance.lstUserTypeLanguages = UserTypeLanguagesBEInstance;
                                }
                            }

                            #endregion

                            #region UserTypeCatalogue

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserTypeCatalogueBEInstance = new List<UserTypesBE.UserTypeCatalogue>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserTypeCatalogueBEInstance.Add((UserTypesBE.UserTypeCatalogue)fillObject(ReaderInstance, lstColNames, Constants.UserTypesBEUserTypeCatalogue));
                                    }
                                    UserTypesBEInstance.lstUserTypeCatalogue = UserTypeCatalogueBEInstance;
                                }
                            }

                            #endregion
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypesBEInstance;
        }

        public static List<UserTypesDetailsBE> GetAllUserTypeDetailsList(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<UserTypesDetailsBE> UserTypeDetailsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                UserTypeDetailsBEInstance = new List<UserTypesDetailsBE>();
                                while (ReaderInstance.Read())
                                {
                                    UserTypeDetailsBEInstance.Add((UserTypesDetailsBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypesDetailsBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypeDetailsBEInstance;
        }

        public static UserTypesDetailsBE GetAllUserTypeDetailsByUserTypeID(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            UserTypesDetailsBE UserTypesDetailsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                UserTypesDetailsBEInstance = new UserTypesDetailsBE();
                                while (ReaderInstance.Read())
                                {
                                    UserTypesDetailsBEInstance = ((UserTypesDetailsBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypesDetailsBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypesDetailsBEInstance;
        }

        public static UserTypeMappingBE getCollectionItemUserTypeMapping(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            UserTypeMappingBE UserTypeMappingBEInstance = null;
            List<UserTypeMappingBE.UserTypeDetails> UserTypeDetailsBEInstance = null;
            List<UserTypeMappingBE.UserTypeLanguages> UserTypeLanguagesBEInstance = null;
            List<UserTypeMappingBE.UserTypeMasterOptions> UserTypeMasterOptionsBEInstance = null;
            List<UserTypeMappingBE.UserTypeEmailValidation> UserTypeEmailValidationBEInstance = null;
            List<UserTypeMappingBE.CustomUserTypes> CustomUserTypesBEInstance = null;
            List<UserTypeMappingBE.Country> CountryBEInstance = null;
            List<UserTypeMappingBE.RegistrationFieldsConfigurationBE> RegistrationFieldsConfigurationBEInstance = null;
            List<UserTypeMappingBE.RegistrationFeildTypeMasterBE> RegistrationFeildTypeMasterBEInstance = null;
            List<UserTypeMappingBE.UserTypeCustomFieldMapping> UserTypeCustomFieldMappingInstance = null;
            List<UserTypeMappingBE.RegistrationLanguagesBE> RegistrationLanguagesBEInstance = null;
            List<UserTypeMappingBE.RegistrationFieldDataMasterBE> RegistrationFieldDataMasterBEInstance = null;
            List<UserTypeMappingBE.RegistrationFieldDataMasterValidationBE> RegistrationFieldDataMasterValidationBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            #region UserTypeMappingBE
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                UserTypeMappingBEInstance = new UserTypeMappingBE();
                                while (ReaderInstance.Read())
                                {
                                    UserTypeMappingBEInstance = ((UserTypeMappingBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBE));
                                }
                            }
                            #endregion
                            #region UserTypesDetails
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserTypeDetailsBEInstance = new List<UserTypeMappingBE.UserTypeDetails>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserTypeDetailsBEInstance.Add((UserTypeMappingBE.UserTypeDetails)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBEUserTypeDetails));
                                    }

                                    UserTypeMappingBEInstance.lstUserTypeDetails = UserTypeDetailsBEInstance;
                                }
                            }
                            #endregion
                            #region UserTypeLanguages
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserTypeLanguagesBEInstance = new List<UserTypeMappingBE.UserTypeLanguages>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserTypeLanguagesBEInstance.Add((UserTypeMappingBE.UserTypeLanguages)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBEUserTypesLanguages));
                                    }

                                    UserTypeMappingBEInstance.lstUserTypeLanguages = UserTypeLanguagesBEInstance;
                                }
                            }
                            #endregion
                            #region UserTypeMasterOptions
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserTypeMasterOptionsBEInstance = new List<UserTypeMappingBE.UserTypeMasterOptions>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserTypeMasterOptionsBEInstance.Add((UserTypeMappingBE.UserTypeMasterOptions)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBEUserTypeMasterOptions));
                                    }
                                    UserTypeMappingBEInstance.lstUserTypeMasterOptions = UserTypeMasterOptionsBEInstance;
                                }
                            }
                            #endregion
                            #region UserTypeEmailValidation
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserTypeEmailValidationBEInstance = new List<UserTypeMappingBE.UserTypeEmailValidation>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserTypeEmailValidationBEInstance.Add((UserTypeMappingBE.UserTypeEmailValidation)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBEUserTypeEmailValidation));
                                    }
                                    UserTypeMappingBEInstance.lstUserTypeEmailValidation = UserTypeEmailValidationBEInstance;
                                }
                            }
                            #endregion
                            #region CustomUserTypes
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CustomUserTypesBEInstance = new List<UserTypeMappingBE.CustomUserTypes>();
                                    while (ReaderInstance.Read())
                                    {
                                        CustomUserTypesBEInstance.Add((UserTypeMappingBE.CustomUserTypes)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBECustomUserTypes));
                                    }
                                    UserTypeMappingBEInstance.lstCustomUserTypes = CustomUserTypesBEInstance;
                                }
                            }
                            #endregion
                            #region Country
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CountryBEInstance = new List<UserTypeMappingBE.Country>();
                                    while (ReaderInstance.Read())
                                    {
                                        CountryBEInstance.Add((UserTypeMappingBE.Country)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBECountry));
                                    }
                                    UserTypeMappingBEInstance.lstCountry = CountryBEInstance;
                                }
                            }
                            #endregion
                            #region RegistrationFieldsConfiguration
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFieldsConfigurationBEInstance = new List<UserTypeMappingBE.RegistrationFieldsConfigurationBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationFieldsConfigurationBEInstance.Add((UserTypeMappingBE.RegistrationFieldsConfigurationBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBERegistrationFieldsConfigurationBE));
                                    }
                                    UserTypeMappingBEInstance.lstRegistrationFieldsConfigurationBE = RegistrationFieldsConfigurationBEInstance;
                                }
                            }
                            #endregion
                            #region RegistrationFeildTypeMasterBE
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFeildTypeMasterBEInstance = new List<UserTypeMappingBE.RegistrationFeildTypeMasterBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationFeildTypeMasterBEInstance.Add((UserTypeMappingBE.RegistrationFeildTypeMasterBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBERegistrationFeildTypeMasterBE));
                                    }
                                    UserTypeMappingBEInstance.lstRegistrationFeildTypeMasterBE = RegistrationFeildTypeMasterBEInstance;
                                }
                            }
                            #endregion
                            #region UserTypeCustomFieldMapping
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserTypeCustomFieldMappingInstance = new List<UserTypeMappingBE.UserTypeCustomFieldMapping>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserTypeCustomFieldMappingInstance.Add((UserTypeMappingBE.UserTypeCustomFieldMapping)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBEUserTypeCustomFieldMapping));
                                    }
                                    UserTypeMappingBEInstance.lstUserTypeCustomFieldMapping = UserTypeCustomFieldMappingInstance;
                                }
                            }
                            #endregion
                            #region RegistrationLanguagesBE
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationLanguagesBEInstance = new List<UserTypeMappingBE.RegistrationLanguagesBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationLanguagesBEInstance.Add((UserTypeMappingBE.RegistrationLanguagesBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBERegistrationLanguagesBE));
                                    }
                                    UserTypeMappingBEInstance.lstRegistrationLanguagesBE = RegistrationLanguagesBEInstance;
                                }
                            }
                            #endregion
                            #region RegistrationFieldDataMasterBE
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFieldDataMasterBEInstance = new List<UserTypeMappingBE.RegistrationFieldDataMasterBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationFieldDataMasterBEInstance.Add((UserTypeMappingBE.RegistrationFieldDataMasterBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBERegistrationFieldDataMasterBE));
                                    }
                                    UserTypeMappingBEInstance.lstRegistrationFieldDataMasterBE = RegistrationFieldDataMasterBEInstance;
                                }
                            }
                            #endregion
                            #region RegistrationFieldDataMasterValidationBE
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();
                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFieldDataMasterValidationBEInstance = new List<UserTypeMappingBE.RegistrationFieldDataMasterValidationBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationFieldDataMasterValidationBEInstance.Add((UserTypeMappingBE.RegistrationFieldDataMasterValidationBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBERegistrationFieldDataMasterValidationBE));
                                    }
                                    UserTypeMappingBEInstance.lstRegistrationFieldDataMasterValidationBE = RegistrationFieldDataMasterValidationBEInstance;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypeMappingBEInstance;
        }

        public static List<UserTypesDetailsBE> GetAlluserTypeNames(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<UserTypesDetailsBE> UserTypeDetailsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                UserTypeDetailsBEInstance = new List<UserTypesDetailsBE>();
                                while (ReaderInstance.Read())
                                {
                                    UserTypeDetailsBEInstance.Add((UserTypesDetailsBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypesDetailsBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypeDetailsBEInstance;
        }

        public static List<UserTypeMappingBE.UserTypeMasterOptions> GetAllUserTypeMasterOptions(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<UserTypeMappingBE.UserTypeMasterOptions> UserTypeDetailsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                UserTypeDetailsBEInstance = new List<UserTypeMappingBE.UserTypeMasterOptions>();
                                while (ReaderInstance.Read())
                                {
                                    UserTypeDetailsBEInstance.Add((UserTypeMappingBE.UserTypeMasterOptions)fillObject(ReaderInstance, lstColNames, Constants.UserTypeMappingBEUserTypeMasterOptions));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypeDetailsBEInstance;
        }

        public static UserTypeCountryMaster GetAllUserTypeCountryMaster(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            UserTypeCountryMaster UserTypeCountryMasterBEInstance = null;
            List<UserTypeCountryMaster.CountryMasterBE> objlstCountryMasterBE = null;
            List<UserTypeCountryMaster.UserTypeLanguagesBE> objlstUserTypeLanguagesBE = null;


            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            #region "UserTypeCountryMaster"
                            if (ReaderInstance.HasRows)
                            {

                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                UserTypeCountryMasterBEInstance = new UserTypeCountryMaster();
                                while (ReaderInstance.Read())
                                {
                                    UserTypeCountryMasterBEInstance = ((UserTypeCountryMaster)fillObject(ReaderInstance, lstColNames, Constants.UserTypeCountryMaster));
                                }
                            }
                            #endregion
                            #region CountryMasterBE
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    objlstCountryMasterBE = new List<UserTypeCountryMaster.CountryMasterBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        objlstCountryMasterBE.Add((UserTypeCountryMaster.CountryMasterBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypeCountryMasterCountryMasterBE));
                                    }

                                    UserTypeCountryMasterBEInstance.lstCountryMasterBE = objlstCountryMasterBE;
                                }
                            }
                            #endregion

                            #region UserTypeLanguagesBE
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    objlstUserTypeLanguagesBE = new List<UserTypeCountryMaster.UserTypeLanguagesBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        objlstUserTypeLanguagesBE.Add((UserTypeCountryMaster.UserTypeLanguagesBE)fillObject(ReaderInstance, lstColNames, Constants.UserTypeCountryMasterUserTypeLanguagesBE));
                                    }

                                    UserTypeCountryMasterBEInstance.lstUserTypeLanguagesBE = objlstUserTypeLanguagesBE;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypeCountryMasterBEInstance;
        }

        public static void SetCountryMaster(string spName, Dictionary<string, DataTable> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, DataTable> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static List<UserTypeCustomFieldDataValue> GetAllCustomFieldDataByFieldID(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<UserTypeCustomFieldDataValue> UserTypeCustomFieldDataValueBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                UserTypeCustomFieldDataValueBEInstance = new List<UserTypeCustomFieldDataValue>();
                                while (ReaderInstance.Read())
                                {
                                    UserTypeCustomFieldDataValueBEInstance.Add((UserTypeCustomFieldDataValue)fillObject(ReaderInstance, lstColNames, Constants.UserTypeCustomFieldDataValueBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypeCustomFieldDataValueBEInstance;
        }

        public static List<UserTypeCustomMapping> GetCustomDetailsValue(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<UserTypeCustomMapping> UserTypeDetailsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                UserTypeDetailsBEInstance = new List<UserTypeCustomMapping>();
                                while (ReaderInstance.Read())
                                {
                                    UserTypeDetailsBEInstance.Add((UserTypeCustomMapping)fillObject(ReaderInstance, lstColNames, Constants.UserTypeCustomMapping));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypeDetailsBEInstance;
        }

        public static List<RegistrationFieldDataMasterValidationBE> GetListCustomByRegID(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<RegistrationFieldDataMasterValidationBE> UserTypeDetailsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                UserTypeDetailsBEInstance = new List<RegistrationFieldDataMasterValidationBE>();
                                while (ReaderInstance.Read())
                                {
                                    UserTypeDetailsBEInstance.Add((RegistrationFieldDataMasterValidationBE)fillObject(ReaderInstance, lstColNames, Constants.RegistrationFieldDataMasterValidationBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserTypeDetailsBEInstance;
        }

        #region "commented and added new of Ganesh
        //public static void UpdateUserTypeRegistrationCustomFields(string spName, Dictionary<string, string> param, Dictionary<string, DataTable> dtparam, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        //{
        //    string connectionString = string.Empty;
        //    try
        //    {
        //        if (IsStoreConnectionString)
        //            connectionString = Constants.strStoreConnectionString;
        //        else
        //            connectionString = Constants.strMCPConnectionString;

        //        using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
        //        {
        //            using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
        //            {
        //                CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
        //                CommandInstance.CommandType = CommandType.StoredProcedure;

        //                if (param != null)
        //                {
        //                    foreach (KeyValuePair<string, string> para in param)
        //                    {
        //                        CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
        //                    }
        //                }

        //                if (dtparam != null)
        //                {
        //                    foreach (KeyValuePair<string, DataTable> dtpara in dtparam)
        //                    {
        //                        CommandInstance.Parameters.AddWithValue(string.Format("@{0}", dtpara.Key), dtpara.Value);
        //                    }
        //                }

        //                if (paramOut != null)
        //                {
        //                    foreach (KeyValuePair<string, string> para in paramOut)
        //                    {
        //                        SqlParameter parameterInstance = new SqlParameter();
        //                        parameterInstance.ParameterName = string.Format("@{0}", para.Key);
        //                        TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
        //                        parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

        //                        parameterInstance.Direction = ParameterDirection.Output;
        //                        CommandInstance.Parameters.Add(parameterInstance);
        //                    }
        //                }
        //                ConnectionInstance.Open();
        //                SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

        //                Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
        //                foreach (KeyValuePair<string, string> para in paramOut)
        //                {
        //                    paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
        //                }
        //                paramOut = paramOutClone;
        //                paramOutClone = null;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //}
        #endregion
        public static void UpdateUserTypeRegistrationCustomFields(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }

                        // Commented by SHRIGANESH SINGH 22 July 2016
                        //if (dtparam != null)
                        //{
                        //    foreach (KeyValuePair<string, DataTable> dtpara in dtparam)
                        //    {
                        //        CommandInstance.Parameters.AddWithValue(string.Format("@{0}", dtpara.Key), dtpara.Value);
                        //    }
                        //}

                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        #region Code Commented by SHRIGANESH SINGH as this code is no longer required
        /*
        public static void DATATYPETEST(string spName, Dictionary<string, DataTable> dtparam, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        //if (param != null)
                        //{
                        //    foreach (KeyValuePair<string, string> para in param)
                        //    {
                        //        CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        //    }
                        //}

                        if (dtparam != null)
                        {
                            foreach (KeyValuePair<string, DataTable> dtpara in dtparam)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", dtpara.Key), dtpara.Value);
                            }
                        }

                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        } 
         */
        #endregion

        public static List<StoreAndUserTypeCatalogueDetailsBE> GetAllStoreAndUserTypeCatalogueDetailsByUserTypeID(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<StoreAndUserTypeCatalogueDetailsBE> StoreAndUserTypeCatalogueDetailsBEBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                StoreAndUserTypeCatalogueDetailsBEBEInstance = new List<StoreAndUserTypeCatalogueDetailsBE>();
                                while (ReaderInstance.Read())
                                {
                                    StoreAndUserTypeCatalogueDetailsBEBEInstance.Add((StoreAndUserTypeCatalogueDetailsBE)fillObject(ReaderInstance, lstColNames, Constants.StoreAndUserTypeCatalogueDetailsBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreAndUserTypeCatalogueDetailsBEBEInstance;
        }

        public static void UpdateUserTypeCatalogueDetails(string spName, Dictionary<string, DataTable> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;



                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, DataTable> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;

                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static bool UpdateCustomFieldNames(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return status;
        }

        public static void UpdateUserTypeSequence(string spName, Dictionary<string, DataTable> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                {
                    connectionString = Constants.strStoreConnectionString;
                }
                else
                {
                    connectionString = Constants.strMCPConnectionString;
                }

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, DataTable> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;

                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        public static UserTypeMappingBE.RegistrationFieldDataMasterValidationBE GetAllCustomFieldValidationRule(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            UserTypeMappingBE.RegistrationFieldDataMasterValidationBE CustomFieldValidationBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                {
                    connectionString = Constants.strStoreConnectionString;
                }
                else
                {
                    connectionString = Constants.strMCPConnectionString;
                }

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                CustomFieldValidationBEInstance = new UserTypeMappingBE.RegistrationFieldDataMasterValidationBE();
                                while (ReaderInstance.Read())
                                {
                                    CustomFieldValidationBEInstance = ((UserTypeMappingBE.RegistrationFieldDataMasterValidationBE)fillObject(ReaderInstance, lstColNames, Constants.CustomFieldValidationBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return CustomFieldValidationBEInstance;
        }
        public static void InsertCustomFieldValues(string spName, Dictionary<string, string> param, Dictionary<string, DataTable> dtparam, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }

                        if (dtparam != null)
                        {
                            foreach (KeyValuePair<string, DataTable> dtpara in dtparam)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", dtpara.Key), dtpara.Value);
                            }
                        }

                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  :   ShriGanesh Singh
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="param"></param>
        /// <param name="IsStoreConnectionString"></param>
        /// <returns></returns>
        public static List<SSOUserTypeCatalogueDetailsBE> GetSSOUserTypeCatalogueDetails(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<SSOUserTypeCatalogueDetailsBE> SSOUserTypeCatalogueDetailsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                SSOUserTypeCatalogueDetailsBEInstance = new List<SSOUserTypeCatalogueDetailsBE>();
                                while (ReaderInstance.Read())
                                {
                                    SSOUserTypeCatalogueDetailsBEInstance.Add((SSOUserTypeCatalogueDetailsBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_SSOUserTypeCatalogueDetailsBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return SSOUserTypeCatalogueDetailsBEInstance;
        }

        /// <summary>
        /// Author  :   ShriGanesh Singh
        /// Date     :   30 June 2017
        /// Description :   To Determine whether Point is Enable or not for the given User Type
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="param"></param>
        /// <param name="IsStoreConnectionString"></param>
        /// <returns> boolean value based on UT id </returns>
        public static void IsPointOrCurrencyOrBothEnabledForUT(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }

                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

    }
}
