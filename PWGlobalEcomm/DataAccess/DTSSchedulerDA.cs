﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class DTSSchedulerDA
    {
        public static DataSet getDTSDetails(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            int intValue = 0;
            DataSet dsDTSDetails = new DataSet();
            SqlDataAdapter dtAdapter = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        dtAdapter = new SqlDataAdapter(CommandInstance);
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();

                        intValue = dtAdapter.Fill(dsDTSDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return dsDTSDetails;
        }

        /// <summary>
        /// return bool value 
        /// </summary>
        /// <param name="spName"> name of store procedure to be executed</param>
        /// <param name="param"> collection of paramaters of store procedure </param>
        /// <returns>bool value</returns>
        public static bool TransferProducts(string spName, Dictionary<string, object> param, string NewStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            try
            {
                using (SqlConnection ConnectionInstance = new SqlConnection(NewStoreConnectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, object> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }

                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return status;
        }
    }
}
