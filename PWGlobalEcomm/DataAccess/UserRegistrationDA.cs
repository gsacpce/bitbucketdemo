﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class UserRegistrationDA : DataAccessBase
    {
        public static UserRegistrationBE getItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            UserRegistrationBE UserRegistrationBEInstance = new UserRegistrationBE();
            List<UserRegistrationBE.UserHierarchyBE> UserRegistrationHierarchyBEInstance = new List<UserRegistrationBE.UserHierarchyBE>();
            List<UserRegistrationBE.UserRegistrationHierarchyCustomFieldMappingsBE> UserRegistrationHierarchyCustomFieldMappingsBEInstance = new List<UserRegistrationBE.UserRegistrationHierarchyCustomFieldMappingsBE>();
            List<UserRegistrationBE.RegistrationFieldsConfigurationBE> RegistrationFieldsConfigurationBEInstance = new List<UserRegistrationBE.RegistrationFieldsConfigurationBE>();
            List<UserRegistrationBE.RegistrationFeildTypeMasterBE> RegistrationFeildTypeMasterBEInstance = new List<UserRegistrationBE.RegistrationFeildTypeMasterBE>();
            List<UserRegistrationBE.RegistrationFieldDataMasterBE> RegistrationFieldDataMasterBEInstance = new List<UserRegistrationBE.RegistrationFieldDataMasterBE>();
            List<UserRegistrationBE.RegistrationLanguagesBE> RegistrationLanguagesBEInstance = new List<UserRegistrationBE.RegistrationLanguagesBE>();
            List<UserRegistrationBE.RegistrationFilterBE> RegistrationFilterBEInstance = new List<UserRegistrationBE.RegistrationFilterBE>();
            List<UserRegistrationBE.CountryBE> CountryBEInstance = new List<UserRegistrationBE.CountryBE>();

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            #region "UserHierarchyBE"
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                UserRegistrationHierarchyBEInstance = new List<UserRegistrationBE.UserHierarchyBE>();
                                while (ReaderInstance.Read())
                                {
                                    UserRegistrationHierarchyBEInstance.Add((UserRegistrationBE.UserHierarchyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserRegistrationBEUserHierarchyBE));
                                }
                                UserRegistrationBEInstance.UserHierarchyLst = UserRegistrationHierarchyBEInstance;
                            }
                            #endregion
                            #region "UserRegistrationHierarchyCustomFieldMappingsBE"
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UserRegistrationHierarchyCustomFieldMappingsBEInstance = new List<UserRegistrationBE.UserRegistrationHierarchyCustomFieldMappingsBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        UserRegistrationHierarchyCustomFieldMappingsBEInstance.Add((UserRegistrationBE.UserRegistrationHierarchyCustomFieldMappingsBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserRegistrationBEUserRegistrationHierarchyCustomFieldMappingsBE));
                                    }
                                    UserRegistrationBEInstance.UserRegistrationHierarchyCustomFieldMappingsLst = UserRegistrationHierarchyCustomFieldMappingsBEInstance;
                                }
                            }
                            #endregion
                            #region "RegistrationFieldsConfigurationBE"
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFieldsConfigurationBEInstance = new List<UserRegistrationBE.RegistrationFieldsConfigurationBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationFieldsConfigurationBEInstance.Add((UserRegistrationBE.RegistrationFieldsConfigurationBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserRegistrationBERegistrationFieldsConfigurationBE));
                                    }
                                    UserRegistrationBEInstance.UserRegistrationFieldsConfigurationLst = RegistrationFieldsConfigurationBEInstance;
                                }
                            }
                            #endregion
                            #region "RegistrationFeildTypeMasterBE"
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFeildTypeMasterBEInstance = new List<UserRegistrationBE.RegistrationFeildTypeMasterBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationFeildTypeMasterBEInstance.Add((UserRegistrationBE.RegistrationFeildTypeMasterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserRegistrationBERegistrationFeildTypeMasterBE));
                                    }
                                    UserRegistrationBEInstance.UserRegistrationFeildTypeMasterLst = RegistrationFeildTypeMasterBEInstance;
                                }
                            }
                            #endregion
                            #region "RegistrationFieldDataMasterBE"
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFieldDataMasterBEInstance = new List<UserRegistrationBE.RegistrationFieldDataMasterBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationFieldDataMasterBEInstance.Add((UserRegistrationBE.RegistrationFieldDataMasterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserRegistrationBERegistrationFieldDataMasterBE));
                                    }
                                    UserRegistrationBEInstance.UserRegistrationFieldDataMasterLst = RegistrationFieldDataMasterBEInstance;
                                }
                            }
                            #endregion
                            #region "RegistrationLanguagesBE"
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationLanguagesBEInstance = new List<UserRegistrationBE.RegistrationLanguagesBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationLanguagesBEInstance.Add((UserRegistrationBE.RegistrationLanguagesBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserRegistrationBERegistrationLanguagesBE));
                                    }
                                    UserRegistrationBEInstance.UserRegistrationLanguagesLst = RegistrationLanguagesBEInstance;
                                }
                            }
                            #endregion
                            #region "RegistrationFilterBE"
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFilterBEInstance = new List<UserRegistrationBE.RegistrationFilterBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        RegistrationFilterBEInstance.Add((UserRegistrationBE.RegistrationFilterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserRegistrationBERegistrationFilterBE));
                                    }
                                    UserRegistrationBEInstance.UserRegistrationFilterLst = RegistrationFilterBEInstance;
                                }
                            }
                            #endregion
                            #region "CountryBE"
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CountryBEInstance = new List<UserRegistrationBE.CountryBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        CountryBEInstance.Add((UserRegistrationBE.CountryBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UserRegistrationBECountryBE));
                                    }
                                    UserRegistrationBEInstance.CountryLst = CountryBEInstance;
                                }
                            }
                            #endregion

                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureBEInstance = new List<FeatureBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                            //        }
                            //    }
                            //}

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return UserRegistrationBEInstance;
        }
    }
}
