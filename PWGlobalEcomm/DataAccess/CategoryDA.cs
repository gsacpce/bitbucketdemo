﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class CategoryDA : DataAccessBase
    {
        #region Static Methods

        public static List<CategoryBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<CategoryBE> CategoryBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                CategoryBEInstance = new List<CategoryBE>();
                                while (ReaderInstance.Read())
                                {
                                    CategoryBEInstance.Add((CategoryBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_CategoryBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return CategoryBEInstance;
        }

        public static List<CategoryBE> getCollectionItem(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            List<CategoryBE> CategoryBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                CategoryBEInstance = new List<CategoryBE>();
                                while (ReaderInstance.Read())
                                {
                                    CategoryBEInstance.Add((CategoryBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_CategoryBE));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return CategoryBEInstance;
        }


        public static void SetSequence(string spName, Dictionary<string, DataTable> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;



                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, DataTable> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;

                    }
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 24-07-15
        /// Scope   : to insert data into the data base get the sql  result set and also getting the result as boolean value
        /// insert assigne category and get the data
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>Returns category list</returns>

        public static List<ProductBE> AssignProductToCategory(string spName, int CategoryId, string ProductIds, string Action, int LanguageId, int UserId, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            try
            {
                string connectionString = string.Empty;

                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                List<ProductBE> ProductBEInstance = new List<ProductBE>();

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        ConnectionInstance.Open();


                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }



                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;

                        if (ReaderInstance.HasRows)
                        {
                            DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            List<string> lstColNames = new List<string>();

                            foreach (DataRow drRow in dtSchemaTable.Rows)
                            {
                                lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            }

                            ProductBEInstance = new List<ProductBE>();
                            while (ReaderInstance.Read())
                            {
                                ProductBEInstance.Add((ProductBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ProductBE));
                            }

                        }


                    }
                }
                return ProductBEInstance;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        //pruduct sequence issue
        public static bool SetProductSequence(DataTable dtProduct, ref bool Result, bool IsStoreConnectionString, string spName)
        {
            //*************************************************************************
            // Purpose : It set the category sequence
            // Inputs  : DataTable dtDataW, DataTable dtDataM, DataTable dtDataT, ref bool Result
            // Return  : void
            //*************************************************************************

            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        CommandInstance.Parameters.AddWithValue("@DataW", dtProduct);
                        CommandInstance.Parameters.AddWithValue("@Result", Result);

                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return status;
        }



        #endregion


    }
}
