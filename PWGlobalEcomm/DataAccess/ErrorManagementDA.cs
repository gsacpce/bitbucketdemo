﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess 
{
    public class ErrorManagementDA : DataAccessBase
    {
        public static ErrorManagementBE getSingleObjectItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            ErrorManagementBE BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            { CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value); }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();
                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                { lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower()); }
                                BEInstance = new ErrorManagementBE();
                                while (ReaderInstance.Read())
                                { BEInstance = ((ErrorManagementBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ErrorManagementBE)); }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            return BEInstance;
        }

        public static List<ErrorManagementBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<ErrorManagementBE> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            { CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value); }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                { lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower()); }
                                BEInstance = new List<ErrorManagementBE>();
                                while (ReaderInstance.Read())
                                { BEInstance.Add((ErrorManagementBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ErrorManagementBE)); }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return BEInstance;
        }
    }
}
