﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWGlobalEcomm.BusinessEntity;

namespace PWGlobalEcomm.DataAccess
{
   public class RoleManagementDA : DataAccessBase
    {
        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all Role for search, List show and Edit
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all coupon for search , List show and Edit</returns>
        public static List<RoleManagementBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<RoleManagementBE> RoleManagementBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]));
                                }

                                RoleManagementBEInstance = new List<RoleManagementBE>();
                                while (ReaderInstance.Read())
                                {
                                    RoleManagementBEInstance.Add((RoleManagementBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_RoleManagementBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return RoleManagementBEInstance;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : For adding and updating the Role
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>For adding and updating the Role</returns>
        public static int InsertUpdateRole(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteScalar());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intValue;
        }

      
    }
}
