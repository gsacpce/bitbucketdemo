﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class NotesDA : DataAccessBase
    {
        public static bool InsertUpdateNotes(string spName, Dictionary<string, string> param, string NewStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                using (SqlConnection ConnectionInstance = new SqlConnection(NewStoreConnectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return status;
        }


        public static string getNoteNo(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            string Value = string.Empty;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        Value = Convert.ToString(CommandInstance.ExecuteScalar());
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return Value;
        }


        public static List<NoteBE> GetNoteDetails(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<NoteBE> NoteBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                NoteBEInstance = new List<NoteBE>();

                                while (ReaderInstance.Read())
                                {
                                    NoteBEInstance.Add((NoteBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_NotesBE));
                                }
                            }

                            #region Commeneted

                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureBEInstance = new List<FeatureBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                            //        }
                            //    }
                            //}
                            //if (ReaderInstance.NextResult())
                            //{
                            //    if (ReaderInstance.HasRows)
                            //    {
                            //        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            //        List<string> lstColNames = new List<string>();

                            //        foreach (DataRow drRow in dtSchemaTable.Rows)
                            //        {
                            //            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            //        }
                            //        FeatureValueBEInstance = new List<FeatureBE.FeatureValueBE>();
                            //        while (ReaderInstance.Read())
                            //        {

                            //            FeatureValueBEInstance.Add((FeatureBE.FeatureValueBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureValueBE));
                            //        }
                            //        if (FeatureValueBEInstance.Count > 0)
                            //        {

                            //            foreach (var store in StoreBEInstance)
                            //            {
                            //                List<FeatureBE.FeatureValueBE> FeatureValues = FeatureValueBEInstance.FindAll(x => x.StoreId == store.StoreId);

                            //                foreach (var item in FeatureBEInstance)
                            //                {
                            //                    item.FeatureValues = FeatureValues.FindAll(x => x.FeatureId == item.FeatureId);
                            //                }

                            //                store.StoreFeatures = FeatureBEInstance;
                            //            }
                            //        }
                            //    }
                            //} 
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return NoteBEInstance;
        }
    }
}
