﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class RegistrationCustomFieldDA : DataAccessBase
    {
        #region Static Methods

        public static List<RegistrationCustomFieldBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<RegistrationCustomFieldBE> RegistrationCustomFieldBEInstance = null;
            List<RegistrationCustomFieldBE.RegistrationFeildDataMasterBE> RegistrationFeildDataMasterBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                RegistrationCustomFieldBEInstance = new List<RegistrationCustomFieldBE>();
                                while (ReaderInstance.Read())
                                {
                                    RegistrationCustomFieldBEInstance.Add((RegistrationCustomFieldBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_RegistrationCustomFieldBE));
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFeildDataMasterBEInstance = new List<RegistrationCustomFieldBE.RegistrationFeildDataMasterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        RegistrationFeildDataMasterBEInstance.Add((RegistrationCustomFieldBE.RegistrationFeildDataMasterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_RegistrationFeildDataMasterBE));
                                    }
                                    if (RegistrationFeildDataMasterBEInstance.Count > 0)
                                    {
                                        foreach (var item in RegistrationCustomFieldBEInstance)
                                        {
                                            item.CustomFieldData = RegistrationFeildDataMasterBEInstance.FindAll(x => x.RegistrationFieldsConfigurationId == item.RegistrationFieldsConfigurationId && x.LanguageId == item.LanguageId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return RegistrationCustomFieldBEInstance;
        }

        public static List<RegistrationCustomFieldBE> getCollectionItem(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            List<RegistrationCustomFieldBE> RegistrationCustomFieldBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                RegistrationCustomFieldBEInstance = new List<RegistrationCustomFieldBE>();
                                while (ReaderInstance.Read())
                                {
                                    RegistrationCustomFieldBEInstance.Add((RegistrationCustomFieldBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_RegistrationCustomFieldBE));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return RegistrationCustomFieldBEInstance;
        }

        public static List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE> getFieldTypeMasterCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE> RegistrationFeildTypeMasterBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                RegistrationFeildTypeMasterBEInstance = new List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE>();
                                while (ReaderInstance.Read())
                                {
                                    RegistrationFeildTypeMasterBEInstance.Add((RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_RegistrationFeildTypeMasterBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return RegistrationFeildTypeMasterBEInstance;
        }

        #region "/*User type*/
        public static List<RegistrationCustomFieldBE> GetAllCustomFieldsByUserTypeID(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<RegistrationCustomFieldBE> RegistrationCustomFieldBEInstance = null;
            List<RegistrationCustomFieldBE.RegistrationFeildDataMasterBE> RegistrationFeildDataMasterBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                RegistrationCustomFieldBEInstance = new List<RegistrationCustomFieldBE>();
                                while (ReaderInstance.Read())
                                {
                                    RegistrationCustomFieldBEInstance.Add((RegistrationCustomFieldBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_RegistrationCustomFieldBE));
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    RegistrationFeildDataMasterBEInstance = new List<RegistrationCustomFieldBE.RegistrationFeildDataMasterBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        RegistrationFeildDataMasterBEInstance.Add((RegistrationCustomFieldBE.RegistrationFeildDataMasterBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_RegistrationFeildDataMasterBE));
                                    }
                                    if (RegistrationFeildDataMasterBEInstance.Count > 0)
                                    {
                                        foreach (var item in RegistrationCustomFieldBEInstance)
                                        {
                                            item.CustomFieldData = RegistrationFeildDataMasterBEInstance.FindAll(x => x.RegistrationFieldsConfigurationId == item.RegistrationFieldsConfigurationId && x.LanguageId == item.LanguageId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return RegistrationCustomFieldBEInstance;
        }

        /// <summary>		
        /// 		
        /// </summary>		
        /// <returns></returns>		
        public static bool CheckForExistingCustomFieldName(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteScalar());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return status;
        }

        public static void ValidateRegistrationCustomFields(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            Int16 intLogId = 0;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                {
                    connectionString = Constants.strStoreConnectionString;
                }
                else
                {
                    connectionString = Constants.strMCPConnectionString;
                }

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;

                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
        #endregion
        #endregion
    }
}
