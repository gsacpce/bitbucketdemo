﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
namespace PWGlobalEcomm.DataAccess
{
    public static class ZoneManagementDA
    {
        //public static List<CountryBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        //{
        //    List<CountryBE> BEInstance = null;
        //    string connectionString = string.Empty;
        //    try
        //    {
        //        if (IsStoreConnectionString)
        //            connectionString = Constants.strStoreConnectionString;
        //        else
        //            connectionString = Constants.strMCPConnectionString;

        //        using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
        //        {
        //            using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
        //            {
        //                CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
        //                CommandInstance.CommandType = CommandType.StoredProcedure;
        //                if (param != null)
        //                {
        //                    foreach (KeyValuePair<string, string> para in param)
        //                    {
        //                        CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
        //                    }
        //                }
        //                ConnectionInstance.Open();
        //                using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
        //                {
        //                    if (ReaderInstance.HasRows)
        //                    {
        //                        DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
        //                        List<string> lstColNames = new List<string>();

        //                        foreach (DataRow drRow in dtSchemaTable.Rows)
        //                        {
        //                            lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
        //                        }

        //                        BEInstance = new List<CountryBE>();
        //                        while (ReaderInstance.Read())
        //                        {
        //                            BEInstance.Add((CountryBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_StaticPageManagementBE));
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        //    }
        //    return BEInstance;
        //}
       
        public static List<CountryBE> getCollectionItem(string spName, Dictionary<string, DataTable> param, bool IsStoreConnectionString)
        {
            List<CountryBE> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, DataTable> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                BEInstance = new List<CountryBE>();
                                while (ReaderInstance.Read())
                                {
                                    BEInstance.Add((CountryBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_CountryBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return BEInstance;
        }

        /// <summary>
        /// returns specified object by reading SqlDataReader instance
        /// </summary>
        /// <param name="readerInstance">SqlDataReader value</param>
        /// <returns>object</returns>
        public static object fillObject(SqlDataReader readerInstance, List<string> lstColNames, string objectName)
        {
            object objectInstance = Activator.CreateInstance(Type.GetType(objectName));
            try
            {
                Type t = Type.GetType(objectName);
                foreach (var item in lstColNames)
                {
                    PropertyInfo fi = t.GetProperty(item, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    object value = null;
                    if (fi != null)
                    {
                        try
                        {
                            value = Convert.ChangeType(readerInstance[item], fi.PropertyType);
                        }
                        catch (InvalidCastException) { }
                        fi.SetValue(objectInstance, value, null);
                    }
                }
            }
            catch (Exception)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return objectInstance;
        }

    }
}
