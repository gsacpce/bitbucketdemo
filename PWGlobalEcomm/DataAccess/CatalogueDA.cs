﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PWGlobalEcomm.DataAccess
{
    public class CatalogueDA : DataAccessBase
    {
       public static CatalogueBE getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            CatalogueBE CatalogueBEInstance = new CatalogueBE();
            List<CatalogueBE.Division> CatalogueDivisionBEInstance = null;
            List<CatalogueBE.Key_Groups> CatalogueKey_GroupsBEInstance = null;
            List<CatalogueBE.Catalogues> CatalogueCataloguesBEInstance = null;
            List<CatalogueBE.Source_Codes> CatalogueSource_CodesBEInstance = null;
            List<CatalogueBE.Web_Search_Accounts> CatalogueWeb_Search_AccountsBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            #region Catalogue
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }
                                while (ReaderInstance.Read())
                                {
                                    CatalogueBEInstance = ((CatalogueBE)fillObject(ReaderInstance, lstColNames, Constants.CatalogueBE));
                                }
                            }
                            #endregion
                            #region Division
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CatalogueDivisionBEInstance = new List<CatalogueBE.Division>();
                                    while (ReaderInstance.Read())
                                    {
                                        CatalogueDivisionBEInstance.Add((CatalogueBE.Division)fillObject(ReaderInstance, lstColNames, Constants.CatalogueBEDivision));
                                    }
                                    CatalogueBEInstance.lstDivision = CatalogueDivisionBEInstance;
                                }
                            }
                            #endregion
                            #region Key_Groups
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CatalogueKey_GroupsBEInstance = new List<CatalogueBE.Key_Groups>();
                                    while (ReaderInstance.Read())
                                    {
                                        CatalogueKey_GroupsBEInstance.Add((CatalogueBE.Key_Groups)fillObject(ReaderInstance, lstColNames, Constants.CatalogueBEKeyGroups));
                                    }

                                    CatalogueBEInstance.lstKey_Groups = CatalogueKey_GroupsBEInstance;

                                }
                            }
                            #endregion
                            #region Catalogues
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CatalogueCataloguesBEInstance = new List<CatalogueBE.Catalogues>();
                                    while (ReaderInstance.Read())
                                    {
                                        CatalogueCataloguesBEInstance.Add((CatalogueBE.Catalogues)fillObject(ReaderInstance, lstColNames, Constants.CatalogueBECatalogues));
                                    }

                                    CatalogueBEInstance.lstCatalogues = CatalogueCataloguesBEInstance;

                                }
                            }
                            #endregion
                            #region Source_Codes
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CatalogueSource_CodesBEInstance = new List<CatalogueBE.Source_Codes>();
                                    while (ReaderInstance.Read())
                                    {
                                        CatalogueSource_CodesBEInstance.Add((CatalogueBE.Source_Codes)fillObject(ReaderInstance, lstColNames, Constants.CatalogueBESource_Codes));
                                    }

                                    CatalogueBEInstance.lstSource_Codes = CatalogueSource_CodesBEInstance;

                                }
                            }
                            #endregion
                            #region Web_Search_Accounts
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CatalogueWeb_Search_AccountsBEInstance = new List<CatalogueBE.Web_Search_Accounts>();
                                    while (ReaderInstance.Read())
                                    {
                                        CatalogueWeb_Search_AccountsBEInstance.Add((CatalogueBE.Web_Search_Accounts)fillObject(ReaderInstance, lstColNames, Constants.CatalogueBEWeb_Search_Accounts));
                                    }

                                    CatalogueBEInstance.lstWeb_Search_Accounts = CatalogueWeb_Search_AccountsBEInstance;

                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return CatalogueBEInstance;
        }
    }
}
