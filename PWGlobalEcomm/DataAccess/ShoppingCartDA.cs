﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class ShoppingCartDA : DataAccessBase
    {
        public static CustomerOrderBE getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            CustomerOrderBE CustomerOrderBEInstance = null;
            List<CustomerOrderBE.CustomerOrderProductsBE> CustomerOrderProductsBEInstance = null;
            List<CustomerOrderBE.UDFValuesBE> UDFValuesBEInstance = null;
            List<CustomerOrderBE.CustomerOrderGiftCertificateBE> CustomerOrderGiftCertificateInstance = null;
            string connectionString = string.Empty;
            //CustomerOrderBEInstance = new CustomerOrderBE();//Added by Sripal
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                CustomerOrderBEInstance = new CustomerOrderBE();//Commenetd by Sripal
                                while (ReaderInstance.Read())
                                {
                                    CustomerOrderBEInstance = ((CustomerOrderBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_CustomerOrderBE));
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CustomerOrderProductsBEInstance = new List<CustomerOrderBE.CustomerOrderProductsBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        CustomerOrderProductsBEInstance.Add((CustomerOrderBE.CustomerOrderProductsBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_CustomerOrderProductsBE));
                                    }
                                    CustomerOrderBEInstance.CustomerOrderProducts = CustomerOrderProductsBEInstance;
                                }
                            }

                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    UDFValuesBEInstance = new List<CustomerOrderBE.UDFValuesBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        UDFValuesBEInstance.Add((CustomerOrderBE.UDFValuesBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_UDFValuesBE));
                                    }
                                    CustomerOrderBEInstance.UDFValues = UDFValuesBEInstance;
                                }
                            }

                            #region "GiftCertificate"
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    CustomerOrderGiftCertificateInstance = new List<CustomerOrderBE.CustomerOrderGiftCertificateBE>();
                                    while (ReaderInstance.Read())
                                    {
                                        CustomerOrderGiftCertificateInstance.Add((CustomerOrderBE.CustomerOrderGiftCertificateBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_CustomerOrderGiftCertificateBE));
                                    }
                                    CustomerOrderBEInstance.CustomerOrderGiftCertificate = CustomerOrderGiftCertificateInstance;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return CustomerOrderBEInstance;
        }

        public static AdFlexTransactionsBE GetAdFlexTransactionDetails(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            AdFlexTransactionsBE AdFlexTransactionsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                AdFlexTransactionsBEInstance = new AdFlexTransactionsBE();
                                while (ReaderInstance.Read())
                                {
                                    AdFlexTransactionsBEInstance = ((AdFlexTransactionsBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_AdFlexTransactionsBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return AdFlexTransactionsBEInstance;
        }

        public static TeleCashTransactionsBE GetTeleCashTransactionDetails(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            TeleCashTransactionsBE TeleCashTransactionsBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                TeleCashTransactionsBEInstance = new TeleCashTransactionsBE();
                                while (ReaderInstance.Read())
                                {
                                    TeleCashTransactionsBEInstance = ((TeleCashTransactionsBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_TeleCashTransactionsBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return TeleCashTransactionsBEInstance;
        }


        public static TeleCashCurrencyBE GetCurrencynumber(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            TeleCashCurrencyBE TeleCashCurrencyBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                TeleCashCurrencyBEInstance = new TeleCashCurrencyBE();
                                while (ReaderInstance.Read())
                                {
                                    TeleCashCurrencyBEInstance = ((TeleCashCurrencyBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_TeleCashCurrencyBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return TeleCashCurrencyBEInstance;
        }


        public static List<ShoppingCartAdditionalBE> getadditionaldata(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<ShoppingCartAdditionalBE> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            { CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value); }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                { lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower()); }
                                BEInstance = new List<ShoppingCartAdditionalBE>();
                                while (ReaderInstance.Read())
                                { BEInstance.Add((ShoppingCartAdditionalBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_ShoppingCartAdditionalBE)); }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return BEInstance;
        }
    
    }
}
