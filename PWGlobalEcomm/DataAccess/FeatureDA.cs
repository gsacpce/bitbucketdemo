﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class FeatureDA : DataAccessBase
    {
        public static List<FeatureBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<FeatureBE> FeatureBEInstance = null;
            List<FeatureBE.FeatureValueBE> FeatureValueBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                FeatureBEInstance = new List<FeatureBE>();
                                while (ReaderInstance.Read())
                                {
                                    FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                                }
                            }
                            while (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    FeatureValueBEInstance = new List<FeatureBE.FeatureValueBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        FeatureValueBEInstance.Add((FeatureBE.FeatureValueBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureValueBE));
                                    }
                                    if (FeatureValueBEInstance.Count > 0)
                                    {
                                        foreach (var item in FeatureBEInstance)
                                        {
                                            item.FeatureValues = FeatureValueBEInstance.FindAll(x => x.FeatureId == item.FeatureId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return FeatureBEInstance;
        }
        public static List<FeatureBE> getCollectionItem(string spName, Dictionary<string, string> param, string IsStoreConnectionString)
        {
            List<FeatureBE> FeatureBEInstance = null;
            List<FeatureBE.FeatureValueBE> FeatureValueBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                using (SqlConnection ConnectionInstance = new SqlConnection(IsStoreConnectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                FeatureBEInstance = new List<FeatureBE>();
                                while (ReaderInstance.Read())
                                {
                                    FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                                }
                            }
                            while (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    FeatureValueBEInstance = new List<FeatureBE.FeatureValueBE>();
                                    while (ReaderInstance.Read())
                                    {

                                        FeatureValueBEInstance.Add((FeatureBE.FeatureValueBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureValueBE));
                                    }
                                    if (FeatureValueBEInstance.Count > 0)
                                    {
                                        foreach (var item in FeatureBEInstance)
                                        {
                                            item.FeatureValues = FeatureValueBEInstance.FindAll(x => x.FeatureId == item.FeatureId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return FeatureBEInstance;
        }
        public static List<FeatureBE> getCollectionItem(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString)
        {
            List<FeatureBE> FeatureBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        if (paramOut != null)
                        {
                            foreach (KeyValuePair<string, string> para in paramOut)
                            {
                                SqlParameter parameterInstance = new SqlParameter();
                                parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                                TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                                parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                                parameterInstance.Direction = ParameterDirection.Output;
                                CommandInstance.Parameters.Add(parameterInstance);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                FeatureBEInstance = new List<FeatureBE>();
                                while (ReaderInstance.Read())
                                {
                                    FeatureBEInstance.Add((FeatureBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_FeatureBE));
                                }
                            }
                        }
                        Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        }
                        paramOut = paramOutClone;
                        paramOutClone = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return FeatureBEInstance;
        }
    }
}
