﻿using PWGlobalEcomm.BusinessEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class CouponDA : DataAccessBase
    {
        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all coupon for search, List show and Edit
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all coupon for search , List show and Edit</returns>
        public static List<CouponBE> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<CouponBE> CouponBEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                CouponBEInstance = new List<CouponBE>();
                                while (ReaderInstance.Read())
                                {
                                    CouponBEInstance.Add((CouponBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_CouponBE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return CouponBEInstance;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : for Bulk insert of Uploaded records
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>for Bulk insert of Uploaded records</returns>
        public static bool BulkInsertWhiteList(DataTable dt, bool IsStoreConnectionString, string TableName)
        {
            bool res = false;
            string connectionString = string.Empty;
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                DataTable table = dt;
                DataRow[] rowArray = table.Select();


                using (SqlBulkCopy bulkcopy = new SqlBulkCopy(con, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers  | SqlBulkCopyOptions.UseInternalTransaction, null))
                {
                    bulkcopy.DestinationTableName ="dbo."+ TableName;
                    //bulkcopy.DestinationTableName = "dbo.test";
                    try
                    {
                        bulkcopy.WriteToServer(rowArray);
                       
                        SqlBulkCopyOptions.FireTriggers.ToString();
                        
                        res = true;
                    }
                    catch (Exception ex)
                    {
                        Exceptions.WriteExceptionLog(ex);
                        res = false;
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get Id of all uploaded records of email/domain from csv file
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns> Get Id of all uploaded records of email/domain from csv file</returns>
         public static DataTable GetIdByCSV(string spName,string Param ,bool IsStoreConnectionString)
         {
           DataTable dt=new DataTable();
           //List<int> CouponBEInstance = null;
           string connectionString = string.Empty;
           try
           {
               if (IsStoreConnectionString)
                   connectionString = Constants.strStoreConnectionString;
               else
                   connectionString = Constants.strMCPConnectionString;

               using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
               {
                   using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                   {
                       CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                       CommandInstance.CommandType = CommandType.StoredProcedure;
                       CommandInstance.Parameters.AddWithValue("@colvalue",Param);
                       ConnectionInstance.Open();
                       SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                        da.Fill(dt);
                   }
               }

           }
             catch(Exception ex){}
           return dt;
         }
         public static DataTable GetIdByCSV(string spName, Dictionary<string, string> Param, bool IsStoreConnectionString)
         {
             DataTable dt = new DataTable();
             //List<int> CouponBEInstance = null;
             string connectionString = string.Empty;
             try
             {
                 if (IsStoreConnectionString)
                     connectionString = Constants.strStoreConnectionString;
                 else
                     connectionString = Constants.strMCPConnectionString;

                 using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                 {
                     using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                     {
                         CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                         CommandInstance.CommandType = CommandType.StoredProcedure;
                         if (Param != null)
                         {
                             foreach (KeyValuePair<string, string> para in Param)
                             {
                                 CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                             }
                         }
                         ConnectionInstance.Open();
                         SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                         da.Fill(dt);
                     }
                 }

             }
             catch (Exception ex) { }
             return dt;
         }
         /// <summary>
         /// Author  : Vikram Singh
         /// Date    : 07 10 2015
         /// Scope   : fetch unmatched records from uploaded csv file before bulk insert
         /// List 
         /// </summary>
         /// <param name="Code"></param>
         /// <param name="pagesize"></param>
         /// <param name="pageindex"></param>
         /// <returns>fetch unmatched records from uploaded csv file before bulk insert</returns>
         public static List<string> GetUnMatchedRecords(string spName, string Param, bool IsStoreConnectionString)
         {
             List<string> dt=new List<string>();
             //List<int> CouponBEInstance = null;
             string connectionString = string.Empty;
             try
             {
                 if (IsStoreConnectionString)
                     connectionString = Constants.strStoreConnectionString;
                 else
                     connectionString = Constants.strMCPConnectionString;

                 using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                 {
                     using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                     {
                         CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                         CommandInstance.CommandType = CommandType.StoredProcedure;
                         CommandInstance.Parameters.AddWithValue("@colvalue", Param);
                         ConnectionInstance.Open();
                         SqlDataReader dr = CommandInstance.ExecuteReader();
                         while(dr.Read())
                         {
                             dt.Add(dr["items"].ToString());
                         }
                     }
                 }
             }
             catch (Exception ex) { }
             return dt;
         }

         /// <summary>
         /// Author  : Vikram Singh
         /// Date    : 07 10 2015
         /// Scope   : For adding the coupon
         /// List 
         /// </summary>
         /// <param name="Code"></param>
         /// <param name="pagesize"></param>
         /// <param name="pageindex"></param>
         /// <returns>For adding the coupon</returns>
         public static int InsertCouponCode(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
         {
             int intValue = 0;
             bool status = false;
             string connectionString = string.Empty;
             try
             {
                 if (IsStoreConnectionString)
                     connectionString = Constants.strStoreConnectionString;
                 else
                     connectionString = Constants.strMCPConnectionString;

                 using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                 {
                     using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                     {
                         CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                         CommandInstance.CommandType = CommandType.StoredProcedure;
                         if (param != null)
                         {
                             foreach (KeyValuePair<string, string> para in param)
                             {
                                 CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                             }
                         }
                         ConnectionInstance.Open();
                         intValue = Convert.ToInt32(CommandInstance.ExecuteScalar());
                         if (intValue > 0)
                             status = true;
                     }
                 }
             }
             catch (Exception ex)
             {
                 //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
             }
             return intValue;
         }

        

    }
}