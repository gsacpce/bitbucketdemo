﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using PWGlobalEcomm.BusinessEntity;
using System.ComponentModel;

namespace PWGlobalEcomm.DataAccess
{
    class IndeedSalesDA : DataAccessBase
    {

        public static List<IndeedSalesBE.BA_SALESGRANDTOTAL> getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<IndeedSalesBE.BA_SALESGRANDTOTAL> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                BEInstance = new List<IndeedSalesBE.BA_SALESGRANDTOTAL>();
                                while (ReaderInstance.Read())
                                {
                                    BEInstance.Add((IndeedSalesBE.BA_SALESGRANDTOTAL)fillObject(ReaderInstance, lstColNames, Constants.Entity_BA_SALESGRANDTOTAL));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return BEInstance;
        }

        public static List<IndeedSalesBE.BA_SALESBYGROUP> getCollectionItems(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<IndeedSalesBE.BA_SALESBYGROUP> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                BEInstance = new List<IndeedSalesBE.BA_SALESBYGROUP>();
                                while (ReaderInstance.Read())
                                {
                                    BEInstance.Add((IndeedSalesBE.BA_SALESBYGROUP)fillObject(ReaderInstance, lstColNames, Constants.Entity_BA_SALESBYGROUP));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return BEInstance;
        }

        public static DataTable getSalesOrder(string spName,string ValueRange,int CurrId,string option, bool IsStoreConnectionString)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        CommandInstance.Parameters.Add("@option", SqlDbType.VarChar).Value = option;
                        CommandInstance.Parameters.Add("@ValueRange", SqlDbType.VarChar).Value = ValueRange;
                        CommandInstance.Parameters.Add("@CurrId", SqlDbType.Int).Value = CurrId;
                        ConnectionInstance.Open();
                        SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex) { }
            return dt;
        }

        public static DataTable getProducts(string spName,string option, string ValueRange, int CurrId,int Filter, bool IsStoreConnectionString)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        CommandInstance.Parameters.Add("@option", SqlDbType.VarChar).Value = option;
                        CommandInstance.Parameters.Add("@ValueRange", SqlDbType.VarChar).Value = ValueRange;
                        CommandInstance.Parameters.Add("@CurrId", SqlDbType.Int).Value = CurrId;
                        CommandInstance.Parameters.Add("@Filter", SqlDbType.Int).Value = Filter;
                        ConnectionInstance.Open();
                        SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex) { }
            return dt;
        }

        public static DataTable getOrderUsage(string spName, string ValueRange, bool IsStoreConnectionString)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        //  CommandInstance.Parameters.Add("@option", SqlDbType.VarChar).Value = option;
                        CommandInstance.Parameters.Add("@ValueRange", SqlDbType.VarChar).Value = ValueRange;
                      //  CommandInstance.Parameters.Add("@CurrId", SqlDbType.Int).Value = CurrId;
                        ConnectionInstance.Open();
                        SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex) { }
            return dt;
        }


        public static DataTable getPointsTransaction(string spName,int userid, bool IsStoreConnectionString)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;                       
                        CommandInstance.Parameters.Add("@UserId", SqlDbType.Int).Value = userid;
                        ConnectionInstance.Open();
                        SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception ex) { }
            return dt;
        }

        public static List<UserBE.BA_USERFILE> getUserFileCollectionItems(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<UserBE.BA_USERFILE> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                BEInstance = new List<UserBE.BA_USERFILE>();
                                while (ReaderInstance.Read())
                                {
                                    BEInstance.Add((UserBE.BA_USERFILE)fillObject(ReaderInstance, lstColNames, Constants.Entity_BA_USERFILE));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return BEInstance;
        }

        public static List<IndeedSalesBE.BA_UserAddress> getUserAddressCollectionItems(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<IndeedSalesBE.BA_UserAddress> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                BEInstance = new List<IndeedSalesBE.BA_UserAddress>();
                                while (ReaderInstance.Read())
                                {
                                    BEInstance.Add((IndeedSalesBE.BA_UserAddress)fillObject(ReaderInstance, lstColNames, Constants.Entity_BA_UserAddress));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return BEInstance;
        }

        public static List<IndeedSalesBE.BA_UserAddress> UpdateUserAddress(string spName,Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            try
            {
                string connectionString = string.Empty;

                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                List<IndeedSalesBE.BA_UserAddress> ProductBEInstance = new List<IndeedSalesBE.BA_UserAddress>();

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        ConnectionInstance.Open();


                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        //if (paramOut != null)
                        //{
                        //    foreach (KeyValuePair<string, string> para in paramOut)
                        //    {
                        //        SqlParameter parameterInstance = new SqlParameter();
                        //        parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                        //        TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                        //        parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                        //        parameterInstance.Direction = ParameterDirection.Output;
                        //        CommandInstance.Parameters.Add(parameterInstance);
                        //    }
                        //}



                        SqlDataReader ReaderInstance = CommandInstance.ExecuteReader();

                        //Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                        //foreach (KeyValuePair<string, string> para in paramOut)
                        //{
                        //    paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                        //}
                        //paramOut = paramOutClone;
                        //paramOutClone = null;

                        if (ReaderInstance.HasRows)
                        {
                            DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            List<string> lstColNames = new List<string>();

                            foreach (DataRow drRow in dtSchemaTable.Rows)
                            {
                                lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            }

                            ProductBEInstance = new List<IndeedSalesBE.BA_UserAddress>();
                            while (ReaderInstance.Read())
                            {
                                ProductBEInstance.Add((IndeedSalesBE.BA_UserAddress)fillObject(ReaderInstance, lstColNames, Constants.Entity_BA_UserAddress));
                            }

                        }


                    }
                }
                return ProductBEInstance;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static List<IndeedSalesBE.BA_Points> getUserPointsCollectionItems(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<IndeedSalesBE.BA_Points> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                BEInstance = new List<IndeedSalesBE.BA_Points>();
                                while (ReaderInstance.Read())
                                {
                                    BEInstance.Add((IndeedSalesBE.BA_Points)fillObject(ReaderInstance, lstColNames, Constants.Entity_BA_UserPoints));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return BEInstance;
        }

        public static List<IndeedSalesBE.BA_Budget> getBudgetCollectionItems(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            List<IndeedSalesBE.BA_Budget> BEInstance = null;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                BEInstance = new List<IndeedSalesBE.BA_Budget>();
                                while (ReaderInstance.Read())
                                {
                                    BEInstance.Add((IndeedSalesBE.BA_Budget)fillObject(ReaderInstance, lstColNames, Constants.Entity_BA_Budget));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return BEInstance;
        }

    
    }
}
