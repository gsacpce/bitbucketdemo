﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.DataAccess
{
    public class GiftCertificateDA : DataAccessBase
    {
        public static bool Update(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            int intValue = 0;
            bool status = false;
            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                        if (intValue > 0)
                            status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
            }
            return status;
        }

        public static GiftCertificateOrderBE getItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            GiftCertificateOrderBE GiftCertificateOrderBEInstance = null;
            List<GiftCertificateOrderBE.GiftOrderDetailsBE> GiftOrderDetailsBEInstance = null;

            string connectionString = string.Empty;
            try
            {
                if (IsStoreConnectionString)
                    connectionString = Constants.strStoreConnectionString;
                else
                    connectionString = Constants.strMCPConnectionString;

                using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
                {
                    using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                    {
                        CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                        CommandInstance.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (KeyValuePair<string, string> para in param)
                            {
                                CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                            }
                        }
                        ConnectionInstance.Open();
                        using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                        {
                            if (ReaderInstance.HasRows)
                            {
                                DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                List<string> lstColNames = new List<string>();

                                foreach (DataRow drRow in dtSchemaTable.Rows)
                                {
                                    lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                }

                                while (ReaderInstance.Read())
                                {
                                    GiftCertificateOrderBEInstance = (GiftCertificateOrderBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_GiftCertificateOrderBE);
                                }
                            }
                            if (ReaderInstance.NextResult())
                            {
                                if (ReaderInstance.HasRows)
                                {
                                    DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                                    List<string> lstColNames = new List<string>();

                                    foreach (DataRow drRow in dtSchemaTable.Rows)
                                    {
                                        lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                                    }
                                    GiftOrderDetailsBEInstance = new List<GiftCertificateOrderBE.GiftOrderDetailsBE>() ;
                                    while (ReaderInstance.Read())
                                    {
                                        GiftOrderDetailsBEInstance.Add((GiftCertificateOrderBE.GiftOrderDetailsBE)fillObject(ReaderInstance, lstColNames, Constants.Entity_GiftCertificateOrderDetailsBE));
                                    }
                                    if (GiftOrderDetailsBEInstance.Count > 0)
                                    {
                                        GiftCertificateOrderBEInstance.lstGiftOrderDetails= GiftOrderDetailsBEInstance.FindAll(x => x.GiftOrderID == GiftCertificateOrderBEInstance.GiftOrderID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return GiftCertificateOrderBEInstance;
        }

    }
}
