﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace PWGlobalEcomm.BusinessLogic
{
    class CatalogueImageBL
    {
       public static bool InsertCatalogueImage(AddCatalogueImageBE objImageCatalogueBE)
        {
            bool Result=false;
            try
            {
                Dictionary<string, string> CatalogueImageInstance = new Dictionary<string, string>();
                CatalogueImageInstance.Add("ImageName", objImageCatalogueBE.ImageName);
                CatalogueImageInstance.Add("ImageExtension", objImageCatalogueBE.ImageExtension);
                CatalogueImageInstance.Add("URL", objImageCatalogueBE.URL);
                Result = CatalogueImageDA.InsertCatalogueImage(Constants.USP_InsertCatalogueImages, CatalogueImageInstance, true);
            }
           catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Result;
        }
       public static List<AddCatalogueImageBE> GetAllHomePageContentDetails()
       {
           try
           {
               Dictionary<string, string> CalalogueImageobj = new Dictionary<string, string>();
               return CatalogueImageDA.GetCatalogueImages(Constants.USP_GetAllCatalogueImages, CalalogueImageobj, true);
           }
           catch(Exception ex)
           {
               Exceptions.WriteExceptionLog(ex);
               return null;
           }
       }
        public static bool DeleteCatalogueImages(AddCatalogueImageBE objaddCatalogueImageBE)
       {
           try
           {
               bool i;
               Dictionary<string, string> CalalogueImageobj = new Dictionary<string, string>();
               CalalogueImageobj.Add("Link", objaddCatalogueImageBE.URL);
               i = CommonDA.Delete(Constants.usp_deletecatalogueimages, CalalogueImageobj, true);
               return i;
           }
            catch(Exception ex)
           {
               Exceptions.WriteExceptionLog(ex);
               return false;
           }
       }
    }
}
