﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace PWGlobalEcomm.BusinessLogic
{
    public class LanguageBL
    {
        /// <summary>
        /// gets all languages
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of LanguageBE objects</returns>
        public static List<LanguageBE> GetAllLanguageDetails(bool IsStoreConnectionString = true)
        {
            List<LanguageBE> getAllLanguages = null;
            try
            {
                //if (HttpRuntime.Cache["LanguageMaster"] == null)
                //{
                    getAllLanguages = LanguageDA.getCollectionItem(Constants.USP_GetAllLanguageDetails, null, IsStoreConnectionString);
                    //HttpRuntime.Cache.Insert("LanguageMaster", getAllLanguages, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/LanguageMasterCaching.txt")));
                //}
                //else
                //    getAllLanguages = (List<LanguageBE>)HttpRuntime.Cache["LanguageMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return getAllLanguages;
        }

        public static List<LanguageCodeMaster> GetAlllstLanguageCodeMaster(string LanguageCode)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("LanguageCode", Convert.ToString(LanguageCode));
            return LanguageDA.GetAlllstLanguageCodeMaster(Constants.USP_GetAlllstLanguageCodeMaster, dictionaryInstance, true);
        }        
    }
}
