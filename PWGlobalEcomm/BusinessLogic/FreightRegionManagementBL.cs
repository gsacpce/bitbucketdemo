﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWGlobalEcomm.DataAccess;
using PWGlobalEcomm.BusinessEntity;

namespace PWGlobalEcomm.BusinessLogic
{
    class FreightRegionManagementBL
    {
        public static FreightRegionManagementBE GetShipmentMethodDetails()
        {
            FreightRegionManagementBE getFreightRegionManagementBE = null;
            try
            {
                getFreightRegionManagementBE = FreightRegionDA.getCollectionItem(Constants.USP_GetALLShipmentMethodFreight, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
            return getFreightRegionManagementBE;

        }

        public static int InsertFreightRegion(FreightRegionManagementBE.FreightRegionsBE objFreightRegionBE, ref int RegionId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RegionName", Convert.ToString(objFreightRegionBE.FreightRegionName));

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("MaxFreightRegionId", typeof(int).ToString());

                FreightRegionDA.Insert(Constants.USP_InsertFreightRegion, dictionaryInstance, ref dictionaryOutputInstance, true);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "MaxFreightRegionId")
                        RegionId = Convert.ToInt32(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return RegionId;

        }

        public static FreightRegionManagementBE GetShipmentServiceMethod(int RegionId, int LangId)
        {
            FreightRegionManagementBE getFreightRegionManagementBE = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RegionID", Convert.ToString(RegionId));
                dictionaryInstance.Add("languageID", Convert.ToString(LangId));

                getFreightRegionManagementBE = FreightRegionDA.getCollectionItem(Constants.USP_GetFreightShipmentServiceMethod, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
            return getFreightRegionManagementBE;

        }

        public static int InserFreightModeLanguage(FreightRegionManagementBE obFreightRegionManagementBE,int i, ref int MaxId)
        {
            try
            {


                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                if (obFreightRegionManagementBE.FreightShipmentMethodLanguagesBELst.Count > 0)
                {
                    dictionaryInstance.Add("FreightModeName", obFreightRegionManagementBE.FreightShipmentMethodLanguagesBELst[i].FreightModeName);
                    dictionaryInstance.Add("FreightModeId", Convert.ToString(obFreightRegionManagementBE.FreightShipmentMethodLanguagesBELst[i].FreightModeId));
                    dictionaryInstance.Add("languageID", Convert.ToString(obFreightRegionManagementBE.FreightShipmentMethodLanguagesBELst[i].LanguageId));

                }
                if (obFreightRegionManagementBE.FreightRegionServiceShipmentBELst.Count > 0)
                {
                    dictionaryInstance.Add("RegionID", Convert.ToString(obFreightRegionManagementBE.FreightRegionServiceShipmentBELst[i].RegionId));
                    dictionaryInstance.Add("IsActive", Convert.ToString(obFreightRegionManagementBE.FreightRegionServiceShipmentBELst[i].IsActive));
                }

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("MaxRegionId", typeof(int).ToString());
                ProductDA.Insert(Constants.USP_InsertFreightModeAsPerRegion, dictionaryInstance, ref dictionaryOutputInstance, true);
                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "MaxRegionId")
                        MaxId = Convert.ToInt32(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return MaxId;
        }
        public static bool UpdateFreightRegionName(FreightRegionManagementBE.FreightRegionsBE objFreightManagementBE)
        {
            bool checkUpdate;
            Dictionary<string, string> ObjRegionName = new Dictionary<string, string>();
            ObjRegionName.Add("FreightRegionId", Convert.ToString(objFreightManagementBE.FreightRegionId));
            ObjRegionName.Add("FreightRegionName", objFreightManagementBE.FreightRegionName);
            ObjRegionName.Add("OldRegionName", objFreightManagementBE.OldRegionName);
            ObjRegionName.Add("Action", Convert.ToString(Convert.ToInt16(DBAction.Update)));

            checkUpdate = FreightRegionDA.Update(Constants.USP_UpdateFreightRegionName, ObjRegionName, true);
            return checkUpdate;
        }
        public static bool DeleteFreightRegionName(FreightRegionManagementBE.FreightRegionsBE objFreightManagementBE)
        {
            Dictionary<string, string> ObjRegionName = new Dictionary<string, string>();
            ObjRegionName.Add("FreightRegionId", Convert.ToString(objFreightManagementBE.FreightRegionId));
            ObjRegionName.Add("FreightRegionName", objFreightManagementBE.FreightRegionName);
            ObjRegionName.Add("Action", Convert.ToString(Convert.ToInt16(DBAction.Delete)));

            bool a = FreightRegionDA.Update(Constants.USP_UpdateFreightRegionName, ObjRegionName, true);
            if (a == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
