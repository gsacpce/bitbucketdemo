﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class CouponBL
    {

        CouponDA objCouponDA;
        public CouponBL()
        { }

        public static List<CouponBE> GetListCouponMaster(string sp, Dictionary<string, string> dictionaryInstance)
        {
            return CouponDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all coupon for search and List show
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all coupon for search and List show</returns>


        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all coupon for search and List show
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all coupon for search and List show</returns>
        public static List<CouponBE> GetListCouponMaster(string sp, int PageSize, string SearchCode, int PageIndex)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("Code", Convert.ToString(SearchCode));
            dictionaryInstance.Add("pagesize", Convert.ToString(PageSize));
            dictionaryInstance.Add("pageindex", Convert.ToString(PageIndex));
            return CouponDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all User for Coupon and List show
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// 
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all coupon for search and List show</returns>
        public static List<CouponBE> GetUserListCouponMaster(string sp, int CouponID, string UsrType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
           // dictionaryInstance.Add("pagesize", Convert.ToString(PageSize));
           // dictionaryInstance.Add("pageindex", Convert.ToString(PageIndex));
            dictionaryInstance.Add("couponId", CouponID.ToString());
            dictionaryInstance.Add("usertype", UsrType);
            //dictionaryInstance.Add("usertype", objCouponBE.UserType.ToString());
            return CouponDA.getCollectionItem(sp, dictionaryInstance, true);
        }
        public static List<CouponBE> GetUserListCouponMaster(string sp, int CouponID)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            // dictionaryInstance.Add("pagesize", Convert.ToString(PageSize));
            // dictionaryInstance.Add("pageindex", Convert.ToString(PageIndex));
            dictionaryInstance.Add("couponId", CouponID.ToString());
            //dictionaryInstance.Add("usertype", objCouponBE.UserType.ToString());
            return CouponDA.getCollectionItem(sp, dictionaryInstance, true);
        }
        public static DataTable GetUserListCouponMasterNew(string sp, int CouponID,string UsrType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("couponId", CouponID.ToString());
            dictionaryInstance.Add("usertype", UsrType);
            return CouponDA.GetIdByCSV(sp,dictionaryInstance,true);
        }
        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of  of Coupon Behaviuor
        /// List 
        /// </summary>
        /// <returns>Get Detail of coupon for Edit</returns>
        public static List<CouponBE> GetDiscountBehaviour(string sp)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            return CouponDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get Detail of coupon for Edit
        /// List 
        /// </summary>
        /// <returns>Get Detail of coupon for Edit</returns>
        public static CouponBE GetCouponMasterByCatIDCouponCode(string sp, CouponBE objCouponBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CouponID", objCouponBE.CouponID.ToString());
            return (CouponBE)CouponDA.getItem(sp, dictionaryInstance, Constants.Entity_CouponBE, true);
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Delete coupon by Id
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Delete coupon by Id</returns>
        public static bool DeleteCouponCode(string SPName, CouponBE objCouponBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CouponID", objCouponBE.CouponID.ToString());
            bool IsDeleted = false;
            try
            {
                IsDeleted = CouponDA.Delete(SPName, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsDeleted;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : for Bulk insert of Uploaded records
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>for Bulk insert of Uploaded records</returns>
        public bool BulkInsertWhiteList(DataTable dt, string TableName)
        {
            try
            {
                objCouponDA = new CouponDA();
                return CouponDA.BulkInsertWhiteList(dt, true, TableName);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : For adding the coupon
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>For adding the coupon</returns>
        public static int InsertCouponMaster(string spname, CouponBE objCouponBE, string mode,bool ischangeUserlist)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            #region Parameters and Values
            dictionaryInstance.Add("CouponID", objCouponBE.CouponID.ToString());
            dictionaryInstance.Add("CouponCode", objCouponBE.CouponCode.ToString());
           // dictionaryInstance.Add("MinOrder", objCouponBE.MinimumOrderValue.ToString());
           // dictionaryInstance.Add("MaxOrder", objCouponBE.MaximumOrderValue.ToString());
            dictionaryInstance.Add("Behaviour", objCouponBE.BehaviourId.ToString());
            dictionaryInstance.Add("BehaviourType", objCouponBE.BehaviourType.ToString());
            dictionaryInstance.Add("DiscountPercentage", objCouponBE.DiscountPercentage.ToString());
            DateTime dtstart = objCouponBE.StartDate;
            string StartDate = dtstart.ToString("yyyy-MM-dd");
            DateTime dtEnd = objCouponBE.EndDate;
            string EndDate = dtEnd.ToString("yyyy-MM-dd");
            dictionaryInstance.Add("StartDate", StartDate);
            dictionaryInstance.Add("EndDate", EndDate);
            Int16 status = 0;
            if (objCouponBE.IsActive)
            {
                status = 1;
            }
            else
            {
                status = 0;
            }
            dictionaryInstance.Add("IsActive", status.ToString());
            dictionaryInstance.Add("HitCount", objCouponBE.Usage.ToString());
            dictionaryInstance.Add("Region", objCouponBE.Region.ToString());
            dictionaryInstance.Add("ShipmentType", objCouponBE.ShipmentType.ToString());
            if (mode == "add")
                dictionaryInstance.Add("userId", objCouponBE.CreatedBy.ToString());
            else
                dictionaryInstance.Add("userId", objCouponBE.ModifiedBy.ToString());
            dictionaryInstance.Add("UserType", objCouponBE.UserType.ToString());
            dictionaryInstance.Add("ischangeUserlist",Convert.ToString(ischangeUserlist));
            #endregion
            int intStoreId = 0;
            try
            {
                intStoreId = CouponDA.InsertCouponCode(spname, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intStoreId;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get Id of all uploaded records of email/domain from csv file
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns> Get Id of all uploaded records of email/domain from csv file</returns>
        public static DataTable GetIdByCSV(string sp, string Param)
        {
            return CouponDA.GetIdByCSV(sp, Param, true);
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : fetch unmatched records from uploaded csv file before bulk insert
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>fetch unmatched records from uploaded csv file before bulk insert</returns>
        public static List<string> GetUnMatchedRecords(string sp, string Param)
        {
            return CouponDA.GetUnMatchedRecords(sp, Param, true);
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 14-10-15
        /// Scope   : Validate coupon code
        /// </summary>
        /// <param name="objCouponBE"></param>
        /// <returns>returns list of CouponBE objects</returns>
        public static List<CouponBE> ValidateCouponCode(CouponBE objCouponBE, UserBE objUserBE, Int16 intCurrencyId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", Convert.ToString(objUserBE.UserId));
                dictionaryInstance.Add("CouponCode", Convert.ToString(objCouponBE.CouponCode));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(intCurrencyId));
                dictionaryInstance.Add("OrderValue", Convert.ToString(objCouponBE.OrderValue).Replace(",", "."));
                dictionaryInstance.Add("Region", Convert.ToString(objCouponBE.Region));
                dictionaryInstance.Add("ShippingMethod", Convert.ToString(objCouponBE.ShipmentType));
                dictionaryInstance.Add("GuestEmailId", Convert.ToString(objUserBE.EmailId));

                return CouponDA.getCollectionItem(Constants.USP_ValidateCouponCode, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Hardik Gohil
        /// Date    : 27/09/2016
        /// </summary>
        /// <typeparam name="CouponId"></typeparam>
        /// <returns></returns>
        public static List<T> GetAllMinMaxValueCoupon<T>(int CouponId)
        {
            List<T> LstCouponValueConf = new List<T>();
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("CouponId", Convert.ToString(CouponId));
            LstCouponValueConf = CommonDA.getCollectionItem<T>(Constants.USP_GetAllMinMaxCouponValues, dictParams, true);
            return LstCouponValueConf;
        }

        /// <summary>
        /// Author  : Hardik Gohil
        /// Date    : 28/09/2016
        /// </summary>
        /// <param name="dtCouponMinMax"></param>
        /// <returns></returns>
        public static bool AED_CouponMinMaxValue(DataTable dtCouponMinMax)
        {
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("CouponMinMaxCurrencyTableType", dtCouponMinMax);
                return CouponDA.Insert(Constants.USP_AED_MinMaxCouponCurrencyValues, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

    }
}
