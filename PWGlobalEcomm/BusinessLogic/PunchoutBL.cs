﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWGlobalEcomm.BusinessLogic
{
    public class PunchoutBL
    {

        public static string CreateCxmlForm(String sURL, String sValue)
        {
            StringBuilder cXML_Form = new StringBuilder();
            try
            {
                string strReturn = Microsoft.VisualBasic.Constants.vbCrLf;
                //
                //1. javascript to submit the form 
                //
                cXML_Form.Append("<!DOCTYPE html>" + strReturn);
                cXML_Form.Append("<html lang='en'>" + strReturn);
                cXML_Form.Append("<HEAD>" + strReturn);
                cXML_Form.Append("<script type='text/javascript' language='javascript'>" + strReturn);
                cXML_Form.Append(" function SubmitForm(){" + strReturn);
                cXML_Form.Append(" document.FormCXML.submit();" + strReturn);
                cXML_Form.Append(" }" + strReturn + strReturn);
                cXML_Form.Append(" </script>" + strReturn);
                cXML_Form.Append("</HEAD>" + strReturn + strReturn + strReturn);

                //Submit the form when the page loads
                cXML_Form.Append("<BODY onload='SubmitForm();'> " + strReturn);
                cXML_Form.Append("<FORM method='post'" + " name='FormCXML'" + " action ='" + sURL + "'>");

                //3.Put CXML data here 
                //
                //cXML_Form = cXML_Form + "<input type='hidden' name='" + "cXML-urlencoded" + "'" + " Value='" + sValue + "'>";
                cXML_Form.Append("<textarea name='cXML-urlencoded' style='display:none'>" + sValue + "</textarea>");

                //End of the form 
                cXML_Form.Append("</FORM>" + strReturn + strReturn + strReturn);
                cXML_Form.Append("</BODY>" + strReturn);
                cXML_Form.Append("</HTML>" + strReturn);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return cXML_Form.ToString();
        }
        public static bool SetPunchoutMarker(PunchoutMarkerBE objPunchoutMarkerBE, DBAction dbAction)
        {
            bool res = false;
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(dbAction).ToString());
                switch (dbAction)
                {
                    case DBAction.Insert:
                    case DBAction.Update:
                        dictParams.Add("ID", Convert.ToString(objPunchoutMarkerBE.ID));
                        dictParams.Add("Marker", objPunchoutMarkerBE.Marker);

                        if (dbAction.Equals(DBAction.Insert))
                            CommonDA.Insert(Constants.USP_PunchoutMarkerSAED, dictParams, true);
                        else
                            CommonDA.Update(Constants.USP_PunchoutMarkerSAED, dictParams, true);
                        break;

                    case DBAction.Delete:
                        break;
                    case DBAction.Select:
                        break;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return res;
        }
        public static bool AddPunchoutDetails(PunchoutDetailsBE objPunchoutDetailsBE, DBAction dbAction)
        {
            bool res = false;
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(dbAction).ToString());
                switch (dbAction)
                {
                    case DBAction.Insert:
                    case DBAction.Update:
                        dictParams.Add("PunchoutID", Convert.ToString(objPunchoutDetailsBE.PunchoutID));
                        dictParams.Add("BuyerCookie", objPunchoutDetailsBE.BuyerCookie);
                        dictParams.Add("PunchoutRequest", objPunchoutDetailsBE.PunchoutRequest);
                        dictParams.Add("PunchoutResponse", objPunchoutDetailsBE.PunchoutResponse);
                        dictParams.Add("PunchoutResponseBody", objPunchoutDetailsBE.PunchoutResponseBody);

                        if (dbAction.Equals(DBAction.Insert))
                            CommonDA.Insert(Constants.USP_PunchoutDetailsSAED, dictParams, true);
                        else
                            CommonDA.Update(Constants.USP_PunchoutDetailsSAED, dictParams, true);
                        break;

                    case DBAction.Delete:
                        break;
                    case DBAction.Select:
                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return res;
        }
        public static PunchoutDetailsBE GetPunchoutID(PunchoutDetailsBE objPunchoutDetailsBE)
        {
            PunchoutDetailsBE objPunchoutDetailsBE1 = new PunchoutDetailsBE();
            try
            {
                if (objPunchoutDetailsBE.BuyerCookie != null || Convert.ToString(objPunchoutDetailsBE.BuyerCookie) != "")
                {
                    Dictionary<string, string> dictParams = new Dictionary<string, string>();
                    dictParams.Add("BuyerCookie", objPunchoutDetailsBE.BuyerCookie);
                    objPunchoutDetailsBE1 = (PunchoutDetailsBE)CommonDA.getItem(Constants.USP_PunchoutDetailsTopDesc, dictParams, Constants.PunchoutDetailsBE, true);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return objPunchoutDetailsBE1;
        }
        public static PunchoutDetailsBE GetPunchoutDetailsByID(PunchoutDetailsBE objPunchoutDetailsBE)
        {
            PunchoutDetailsBE objPunchoutDetailsBE1 = new PunchoutDetailsBE();
            try
            {
                if (objPunchoutDetailsBE.BuyerCookie != null || Convert.ToString(objPunchoutDetailsBE.BuyerCookie) != "")
                {
                    Dictionary<string, string> dictParams = new Dictionary<string, string>();
                    dictParams.Add("PunchOutID", Convert.ToString(objPunchoutDetailsBE.PunchoutID));
                    objPunchoutDetailsBE1 = (PunchoutDetailsBE)CommonDA.getItem(Constants.USP_GetPunchoutDetailsByID, dictParams, Constants.PunchoutDetailsBE, true);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return objPunchoutDetailsBE1;
        }
    }
}
