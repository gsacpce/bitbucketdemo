﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    public class FeatureBL
    {
        /// <summary>
        /// gets all Features
        /// </summary>
        /// <returns>returns list of FeatureBE objects</returns>
        public static List<FeatureBE> GetAllFeatures(Int16 StoreId, bool IsStoreConnectionString = false)
        {
            List<FeatureBE> getAllFeatures = null;
            try
            {
                //if (HttpRuntime.Cache["AllFeatures"] == null)
                //{
                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                    getAllFeatures = FeatureDA.getCollectionItem(Constants.USP_GetAllFeatureDetails, dictionaryInstance, IsStoreConnectionString);
                //    HttpRuntime.Cache.Insert("AllFeatures", getAllFeatures, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/AllFeaturesCaching.txt")));
                //}
                //else
                //    getAllFeatures = (List<FeatureBE>)HttpRuntime.Cache["AllFeatures"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllFeatures;
        }
        public static List<FeatureBE> GetAllFeatures(Int16 StoreId, string IsStoreConnectionString)
        {
            List<FeatureBE> getAllFeatures = null;
            try
            {
                //if (HttpRuntime.Cache["AllFeatures"] == null)
                //{
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                getAllFeatures = FeatureDA.getCollectionItem(Constants.USP_GetAllFeatureDetails, dictionaryInstance, IsStoreConnectionString);
                //    HttpRuntime.Cache.Insert("AllFeatures", getAllFeatures, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/AllFeaturesCaching.txt")));
                //}
                //else
                //    getAllFeatures = (List<FeatureBE>)HttpRuntime.Cache["AllFeatures"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllFeatures;
        }
        internal static List<FeatureBE> GetLandingPageDetails(string CategoryName)
        {
            List<FeatureBE> StoreFeatures = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CategoryName", Convert.ToString(CategoryName));
                dictionaryInstance.Add("Action", Convert.ToString("select"));
                StoreFeatures = FeatureDA.getCollectionItem(Constants.USP_UpdateLandingPage, dictionaryInstance, true);


            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return StoreFeatures;
        }
        /// <summary>
        /// to  update landing page data
        /// </summary>
        /// <returns>returns bool value for data been updated or not</returns>
        internal static bool UpdateLandingPageData(List<Int16> TypeIds, string Action)
        {
            bool IsUpdated = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DisplayTypeId", Convert.ToString(TypeIds[0]));
                dictionaryInstance.Add("ControlTypeId", Convert.ToString(TypeIds[1]));
                dictionaryInstance.Add("Action", Convert.ToString("edit"));
                IsUpdated = FeatureDA.Update(Constants.USP_UpdateLandingPage, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                
            }
            return IsUpdated;
        }

        /// <summary>
        /// to list all currencies
        /// </summary>
        /// <returns></returns>
        internal static List<FeatureBE> GetAllCurrencies()
        {
            List<FeatureBE> Currencies = new List<FeatureBE>();
            try
            {
                Currencies = FeatureDA.getCollectionItem(Constants.USP_GetAllCurrencies, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Currencies;
        }

        /// <summary>
        /// to list all countries
        /// </summary>
        /// <returns></returns>

        internal static List<FeatureBE> GetAllCountries()
        {
            List<FeatureBE> Countries = new List<FeatureBE>();
            try
            {
                Countries = FeatureDA.getCollectionItem(Constants.USP_GetAllCountries, null, true);
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
            return Countries;
        }
    }
}
