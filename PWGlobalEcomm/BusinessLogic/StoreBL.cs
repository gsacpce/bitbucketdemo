﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    public class StoreBL
    {
        /// <summary>
        /// gets all Store details
        /// </summary>
        /// <returns>returns list of StoreBE objects</returns>
        public static List<StoreBE> GetAllStoreDetails()
        {
            List<StoreBE> getAllStores = null;
            try
            {
                /*if (HttpContext.Current.Session["AllStores"] == null)
                {*/
                    UserBE objUserBE = new UserBE();
                    objUserBE = HttpContext.Current.Session["User"] as UserBE;
                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("UserId", Convert.ToString(objUserBE.UserId));
                    getAllStores = StoreDA.getCollectionItem(Constants.USP_GetAllStoreDetails, dictionaryInstance, false);
                /*    HttpContext.Current.Session["AllStores"] = getAllStores;
                }
                else
                    getAllStores = (List<StoreBE>)HttpContext.Current.Session["AllStores"];
                 */ 
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllStores;
        }

        /// <summary>
        /// gets Store details
        /// </summary>
        /// <returns>returns StoreBE objects</returns>
        public static StoreBE GetStoreDetails(Int16 StoreId)
        {
            StoreBE getStores = null;
            try
            {
                //if (HttpContext.Current.Session["StoreDetailsMCP"] == null)
                //{

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                getStores = StoreDA.getItem(Constants.USP_GetAllStoreDetails, dictionaryInstance, false);
                //HttpContext.Current.Session["StoreDetailsMCP"] = getStores;
                //}
                //else
                //    getStores = (StoreBE)HttpContext.Current.Session["StoreDetailsMCP"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getStores;
        }

        /// <summary>
        /// gets Store details
        /// </summary>
        /// <returns>returns StoreBE objects</returns>
        public static StoreBE GetStoreDetails()
        {
            StoreBE getStores = null;
            try
            {
                /*Sachin Chauhan Start : 24 02 2016 : Uncommented below lines of code as it is required for current web application request & not http runtime */
                if (HttpContext.Current.Cache["StoreDetails"] == null)
                {
                    getStores = StoreDA.getItem(Constants.USP_GetAllStoreDetails, null, true);
                    HttpContext.Current.Cache["StoreDetails"] = getStores;
                }
                else
                    getStores = (StoreBE)HttpContext.Current.Cache["StoreDetails"];

                /**/

                //if (HttpRuntime.Cache["StoreDetails"] == null)
                //{
                //    getStores = StoreDA.getItem(Constants.USP_GetAllStoreDetails, null, true);
                //    HttpRuntime.Cache.Insert("StoreDetails", getStores);
                //}
                //else
                //    getStores = (StoreBE)HttpRuntime.Cache["StoreDetails"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getStores;
        }

        /// <summary>
        /// Checks weather store with specified id is present or not
        /// </summary>
        /// <param name="strStoreName"></param>
        /// <returns>returns true or false</returns>
        public static bool IsStoreExists(string StoreName)
        {
            bool IsExists = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StoreName", Convert.ToString(StoreName));
                IsExists = StoreDA.IsExists(Constants.USP_IsStoreExists, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsExists;
        }

        /// <summary>
        /// insert store detail
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>insert StoreBE object</returns>
        public static Int16 InsertStore(Dictionary<string, string> StoreDetail)
        {
            Int16 intStoreId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("StoreId", typeof(Int16).ToString());

                StoreDA.Insert(Constants.USP_InsertStore, StoreDetail, ref DictionaryOutParameterInstance, false);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "StoreId")
                        intStoreId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intStoreId;
        }

        /// <summary>
        /// update store detail
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>update StoreBE object</returns>
        public static bool UpdateStore(Dictionary<string, string> StoreDetail)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.USP_UpdateStore, StoreDetail, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// <summary>
        /// update is punchout
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>update StoreBE object</returns>
        public static bool UpdateIsPunchout(Dictionary<string, string> StoreDetail)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.USP_UpdateIsPunchout, StoreDetail, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// <summary>
        /// update store template
        /// </summary>
        /// <param name="StoreDetail"></param>
        /// <returns>update store template</returns>
        public static bool UpdateStoreTemplate(Dictionary<string, string> StoreTemplate)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.USP_UpdateStoreTemplate, StoreTemplate, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// <summary>
        /// update store IsCreated value
        /// </summary>
        /// <param name="StoreIsCreated"></param>
        /// <returns>update store IsCreated</returns>
        public static bool UpdateIsStoreCreated(Dictionary<string, string> StoreIsCreated)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.USP_UpdateStoreCreated, StoreIsCreated, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// <summary>
        /// update store IsUpdated value
        /// </summary>
        /// <param name="StoreIsCreated"></param>
        /// <returns>update UpdateNewlyCreatedStoreDetails</returns>
        public static bool UpdateNewlyCreatedStoreDetails(Dictionary<string, string> StoreIsCreated, string DatabaseUserName)
        {
            bool IsUpdated = false;
            string NewDatabaseConnection = Convert.ToString(ConfigurationManager.AppSettings["NewDatabaseConnection"]);
            try
            {
                IsUpdated = StoreDA.UpdateNewlyCreatedStoreDetails(Constants.USP_UpdateStoreCreated, StoreIsCreated, NewDatabaseConnection.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + DatabaseUserName));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// <summary>
        /// insert store language
        /// </summary>
        /// <param name="StoreLanguageDetail"></param>
        /// <returns>insert StoreLanguageBE object and returns bool value</returns>
        public static bool InsertStoreLanguage(Dictionary<string, string> StoreLanguageDetail)
        {
            bool IsInserted = false;
            try
            {
                IsInserted = StoreDA.Insert(Constants.USP_InsertStoreLanguage, StoreLanguageDetail, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsInserted;
        }

        /// <summary>
        /// insert store currency
        /// </summary>
        /// <param name="StoreCurrencyDetail"></param>
        /// <returns>insert StoreCurrencyBE object and returns bool value</returns>
        public static bool InsertStoreCurrency(Dictionary<string, string> StoreCurrencyDetail)
        {
            bool IsInserted = false;
            try
            {
                IsInserted = StoreDA.Insert(Constants.USP_InsertStoreCurrency, StoreCurrencyDetail, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsInserted;
        }

        /// <summary> 
        /// Database backup/restore in SQL server. 
        /// </summary> 
        /// <param name="masterDatabaseWithAdminLoginConnectionString">Connection string for accessing master database with admin login credentials.</param> 
        /// <param name="databaseName">New database name to be created.</param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool CreateDatabase(Dictionary<string, string> CopyDatabaseDetail)
        {
            bool IsRestored = false;
            try
            {
                IsRestored = StoreDA.Insert(Constants.USP_CreateDatabase, CopyDatabaseDetail, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsRestored;
        }

        ///// <summary>
        ///// Create database
        ///// </summary>
        ///// <param name="CopyDatabaseDetail"></param>
        ///// <returns>bool value</returns>
        //public static bool CreateDatabase(Dictionary<string, string> CopyDatabaseDetail)
        //{
        //    bool IsRestored = false;
        //    try
        //    {
        //        IsRestored = StoreDA.Insert(Constants.USP_CreateDatabase, CopyDatabaseDetail, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //    return IsRestored;
        //}

        /// <summary>
        /// Create database
        /// </summary>
        /// <param name="CopyDatabaseDetail"></param>
        /// <returns>bool value</returns>
        public static bool CreateCloneDatabase(Dictionary<string, string> CopyDatabaseDetail)
        {
            bool IsRestored = false;
            try
            {
                Exceptions.WriteInfoLog("Enter in CreateCloneDatabase method. time -> " + DateTime.Now.ToString());
                IsRestored = StoreDA.Insert(Constants.USP_CreateCloneDatabase, CopyDatabaseDetail, false);
                Exceptions.WriteInfoLog("After calling USP_CreateCloneDatabase sp in CreateCloneDatabase method. time -> " + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("Error Message -> " + ex.Message.ToString() + ", Error Stacktrace -> " + ex.StackTrace.ToString());
                Exceptions.WriteExceptionLog(ex);
            }
            return IsRestored;
        }

        /// <summary>
        /// generate database script
        /// </summary>
        /// <returns>returns script data</returns>
        public static string GenerateScript()
        {
            StringBuilder resultScript = new StringBuilder(string.Empty);
            //try
            //{
            //    #region Database Connection Settings

            //    ServerConnection objServerConnection = new ServerConnection(ConfigurationManager.AppSettings["MasterDBServerName"], ConfigurationManager.AppSettings["MasterDBUserName"], ConfigurationManager.AppSettings["MasterDBPassword"]);
            //    Server server = new Server(objServerConnection);
            //    Database database = server.Databases[ConfigurationManager.AppSettings["DefaultDBName"]];

            //    #endregion

            //    #region Loop tables

            //    foreach (Microsoft.SqlServer.Management.Smo.Table table in database.Tables)
            //    {
            //        // Generate script
            //        // Include primairykey, foreignkey, constaints, defaults etc.
            //        // Exclude collation information

            //        // Only create table when it does not exist
            //        resultScript.AppendFormat("IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'U'))", table.Name).Append(Environment.NewLine);
            //        resultScript.AppendLine("BEGIN");

            //        ScriptingOptions options = new ScriptingOptions();
            //        //options.DriAll = true;
            //        options.DriPrimaryKey = true;
            //        options.DriDefaults = true;
            //        options.DriUniqueKeys = true;
            //        options.DriIndexes = true;
            //        options.DriForeignKeys = false;
            //        options.SchemaQualifyForeignKeysReferences = false;
            //        options.NoCollation = true;
            //        StringCollection scriptLines = table.Script(options);

            //        // Add table schema script to file content
            //        foreach (string scriptLine in scriptLines)
            //        {
            //            string line = scriptLine;
            //            line = line.Replace("SET ANSI_NULLS ON", string.Empty);
            //            line = line.Replace("SET QUOTED_IDENTIFIER ON", string.Empty);
            //            line = line.Replace("SET ANSI_NULLS OFF", string.Empty);
            //            line = line.Replace("SET QUOTED_IDENTIFIER OFF", string.Empty);
            //            resultScript.AppendLine(line.Trim());
            //        }
            //        resultScript.AppendLine("END");
            //    }

            //    #endregion

            //    #region Loop tables for content

            //    foreach (Microsoft.SqlServer.Management.Smo.Table table in database.Tables)
            //    {
            //        // Generate script
            //        // Include content in script

            //        Scripter scripter = new Scripter(server);
            //        ScriptingOptions options = new ScriptingOptions();
            //        options.DriAll = false;
            //        options.ScriptSchema = false;
            //        options.ScriptData = true;
            //        scripter.Options = options;

            //        if (scripter.EnumScript(new Urn[] { table.Urn }).Count() > 0)
            //        {
            //            // Only insert data when table is empty
            //            resultScript.AppendFormat("IF NOT EXISTS (SELECT 1 FROM [dbo].[{0}])", table.Name).Append(Environment.NewLine);
            //            resultScript.AppendLine("BEGIN");
            //        }

            //        // Add tables content script to file content
            //        foreach (string scriptLine in scripter.EnumScript(new Urn[] { table.Urn }))
            //        {
            //            string line = scriptLine;
            //            line = line.Replace("SET ANSI_NULLS ON", string.Empty);
            //            line = line.Replace("SET QUOTED_IDENTIFIER ON", string.Empty);
            //            line = line.Replace("SET ANSI_NULLS OFF", string.Empty);
            //            line = line.Replace("SET QUOTED_IDENTIFIER OFF", string.Empty);
            //            resultScript.AppendLine(line.Trim());
            //        }

            //        if (scripter.EnumScript(new Urn[] { table.Urn }).Count() > 0)
            //        {
            //            resultScript.AppendLine("END");
            //        }
            //    }

            //    #endregion

            //    #region Loop UserDefinedTableTypes for content

            //    foreach (UserDefinedTableType tableType in database.UserDefinedTableTypes)
            //    {
            //        resultScript.AppendLine("BEGIN");

            //        ScriptingOptions options = new ScriptingOptions();
            //        //options.DriAll = true;
            //        options.DriPrimaryKey = true;
            //        options.DriDefaults = true;
            //        options.DriUniqueKeys = true;
            //        options.DriIndexes = true;
            //        options.DriForeignKeys = false;
            //        options.SchemaQualifyForeignKeysReferences = false;
            //        options.NoCollation = true;
            //        StringCollection scriptLines = tableType.Script(options);

            //        // Add table schema script to file content
            //        foreach (string scriptLine in scriptLines)
            //        {
            //            string line = scriptLine;
            //            line = line.Replace("SET ANSI_NULLS ON", string.Empty);
            //            line = line.Replace("SET QUOTED_IDENTIFIER ON", string.Empty);
            //            line = line.Replace("SET ANSI_NULLS OFF", string.Empty);
            //            line = line.Replace("SET QUOTED_IDENTIFIER OFF", string.Empty);
            //            resultScript.AppendLine(line.Trim());
            //        }
            //        resultScript.AppendLine("END");
            //    }

            //    #endregion

            //    #region Loop stored procedures

            //    foreach (StoredProcedure storedProcedure in database.StoredProcedures)
            //    {
            //        // Exclude system stored procedures
            //        if (!storedProcedure.IsSystemObject)
            //        {
            //            // Generate script
            //            // Include drop statements

            //            // Append drop statement
            //            resultScript.AppendFormat("IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'P', N'PC'))", storedProcedure.Name).Append(Environment.NewLine);
            //            resultScript.AppendLine("BEGIN");
            //            resultScript.AppendFormat("    DROP PROCEDURE [dbo].[{0}]", storedProcedure.Name).Append(Environment.NewLine);
            //            resultScript.AppendLine("END");
            //            resultScript.AppendLine("GO");

            //            // Add Stored Procedures script to file content
            //            foreach (string scriptLine in storedProcedure.Script())
            //            {
            //                string line = scriptLine;
            //                line = line.Replace("SET ANSI_NULLS ON", string.Empty);
            //                line = line.Replace("SET QUOTED_IDENTIFIER ON", string.Empty);
            //                line = line.Replace("SET ANSI_NULLS OFF", string.Empty);
            //                line = line.Replace("SET QUOTED_IDENTIFIER OFF", string.Empty);
            //                resultScript.AppendLine(line.Trim());
            //            }
            //        }
            //    }

            //    #endregion

            //    #region Loop UserDefinedFunctions

            //    foreach (UserDefinedFunction function in database.UserDefinedFunctions)
            //    {

            //        // Exclude system functions
            //        if (!function.IsSystemObject)
            //        {
            //            // Generate script
            //            // Include drop statements

            //            // Append drop statement
            //            resultScript.AppendLine("GO");
            //            resultScript.AppendFormat("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))", function.Name).Append(Environment.NewLine);
            //            resultScript.AppendLine("BEGIN");
            //            resultScript.AppendFormat("    DROP FUNCTION [dbo].[{0}]", function.Name).Append(Environment.NewLine);
            //            resultScript.AppendLine("END");
            //            resultScript.AppendLine("GO");

            //            // Add userDefined functions script to file content
            //            foreach (string scriptLine in function.Script())
            //            {
            //                string line = scriptLine;
            //                line = line.Replace("SET ANSI_NULLS ON", string.Empty);
            //                line = line.Replace("SET QUOTED_IDENTIFIER ON", string.Empty);
            //                line = line.Replace("SET ANSI_NULLS OFF", string.Empty);
            //                line = line.Replace("SET QUOTED_IDENTIFIER OFF", string.Empty);
            //                resultScript.AppendLine(line.Trim());
            //            }
            //        }
            //    }

            //    #endregion

            //    #region Loop tables for Foreign keys

            //    StringCollection ForeighKeysScriptCollection = new StringCollection();
            //    ScriptingOptions optionsForeignKeys = new ScriptingOptions();
            //    optionsForeignKeys.DriForeignKeys = true;
            //    optionsForeignKeys.SchemaQualifyForeignKeysReferences = true;

            //    resultScript.AppendLine("GO");

            //    foreach (Microsoft.SqlServer.Management.Smo.Table table in database.Tables)
            //    {
            //        foreach (ForeignKey key in table.ForeignKeys)
            //        {
            //            ForeighKeysScriptCollection = key.Script(optionsForeignKeys);
            //            foreach (string s in ForeighKeysScriptCollection)
            //            {
            //                resultScript.AppendLine(s.Trim());
            //            }
            //        }
            //    }

            //    #endregion
            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}


            return resultScript.ToString();
        }

        /// <summary>
        /// Execute database script
        /// </summary>
        /// <param name="Script"></param>
        /// <param name="StoreName"></param>
        /// <returns>bool value</returns>
        public static bool ExecuteScript(DataSet StoreDetails, string StoreName)
        {
            bool IsRestored = false;
            string NewDatabaseConnection = Convert.ToString(ConfigurationManager.AppSettings["NewDatabaseConnection"]);
            try
            {
                Exceptions.WriteInfoLog("enter ExecuteScript method");
                Exceptions.WriteInfoLog("store details tables count -> " + Convert.ToString(StoreDetails.Tables.Count));
                //IsRestored = StoreDA.ExecuteNewDatabaseScript(Script, NewDatabaseConnection.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName));
                //if (IsRestored)
                //{
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("FeaturesTable", StoreDetails.Tables[0]);
                dictionaryInstance.Add("FeatureValuesTable", StoreDetails.Tables[1]);
                dictionaryInstance.Add("GlobalSettingsTable", StoreDetails.Tables[2]);
                dictionaryInstance.Add("GlobalSettingDetailsTable", StoreDetails.Tables[3]);
                dictionaryInstance.Add("StoreStatusTable", StoreDetails.Tables[4]);
                dictionaryInstance.Add("UsersTableType", StoreDetails.Tables[5]);
                dictionaryInstance.Add("StoresTable", StoreDetails.Tables[6]);
                dictionaryInstance.Add("StoreDetailsTable", StoreDetails.Tables[7]);
                dictionaryInstance.Add("StoreLanguageMappingsTable", StoreDetails.Tables[8]);
                dictionaryInstance.Add("StoreFeatureDetailsTable", StoreDetails.Tables[9]);
                dictionaryInstance.Add("TemplatesTable", StoreDetails.Tables[10]);
                Exceptions.WriteInfoLog("before executing CopyStoreDetailsFromMCPToStore method");
                IsRestored = StoreDA.CopyStoreDetailsFromMCPToStore(Constants.USP_CopyStoreDetailsFromMCPToStore, dictionaryInstance, NewDatabaseConnection.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName), Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName);
                Exceptions.WriteInfoLog("after executing CopyStoreDetailsFromMCPToStore method. IsRestored -> " + Convert.ToString(IsRestored));
                //}
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsRestored;
        }

        /// <summary>
        /// Execute database script
        /// </summary>
        /// <param name="Script"></param>
        /// <param name="StoreName"></param>
        /// <returns>bool value</returns>
        public static bool ExecuteCloneDatabaseScript(DataSet StoreDetails, string StoreName)
        {
            bool IsRestored = false;
            string NewDatabaseConnection = Convert.ToString(ConfigurationManager.AppSettings["NewDatabaseConnection"]);
            try
            {
                Exceptions.WriteInfoLog("enter ExecuteScript method");
                Exceptions.WriteInfoLog("store details tables count -> " + Convert.ToString(StoreDetails.Tables.Count));
                //IsRestored = StoreDA.ExecuteNewDatabaseScript(Script, NewDatabaseConnection.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName));
                //if (IsRestored)
                //{
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();

                dictionaryInstance.Add("UsersTableType", StoreDetails.Tables[0]);
                dictionaryInstance.Add("StoresTable", StoreDetails.Tables[1]);
                dictionaryInstance.Add("StoreDetailsTable", StoreDetails.Tables[2]);
                dictionaryInstance.Add("StoreLanguageMappingsTable", StoreDetails.Tables[3]);

                Exceptions.WriteInfoLog("before executing CopyStoreDetailsFromMCPToStore method");
                IsRestored = StoreDA.CopyStoreDetailsFromMCPToStore(Constants.USP_CopyCloneStoreDetailsFromMCPToStore, dictionaryInstance, NewDatabaseConnection.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName), Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName);
                Exceptions.WriteInfoLog("after executing CopyStoreDetailsFromMCPToStore method. IsRestored -> " + Convert.ToString(IsRestored));
                //}
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsRestored;
        }

        /// <summary>
        /// Execute database script
        /// </summary>
        /// <param name="Script"></param>
        /// <param name="StoreName"></param>
        /// <returns>bool value</returns>
        public static bool ExecuteCloneDatabaseData(string BaseStoreName, string CloneStoreName)
        {
            bool IsRestored = false;
            string NewDatabaseConnection = Convert.ToString(ConfigurationManager.AppSettings["NewDatabaseConnection"]);
            try
            {
                Exceptions.WriteInfoLog("enter ExecuteScript method");
               // Exceptions.WriteInfoLog("store details tables count -> " + Convert.ToString(StoreDetails.Tables.Count));
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("basestore", BaseStoreName);
                dictionaryInstance.Add("clonestore", CloneStoreName);

                Exceptions.WriteInfoLog("before executing CreateCloneData method");
             //   IsRestored = StoreDA.ExecuteNewDatabaseScript(Script, NewDatabaseConnection.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + StoreName));

            
                IsRestored = StoreDA.CloneDatabaseData(Constants.USP_CreateCloneData, dictionaryInstance, NewDatabaseConnection.Replace("$DatabaseName$", CloneStoreName),CloneStoreName);
                Exceptions.WriteInfoLog("after executing CreateCloneData method. IsRestored -> " + Convert.ToString(IsRestored));
                //}
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsRestored;
        }

        /// <summary> 
        /// Checks whether database exists or not 
        /// </summary> 
        /// <param name="masterDatabaseWithAdminLoginConnectionString">Master database connection string</param> 
        /// <param name="databaseName">Database name to check</param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool IsSiteDatabaseExists(string DatabaseName)
        {
            bool IsExists = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DatabaseName", Convert.ToString(DatabaseName));
                IsExists = StoreDA.IsExists(Constants.USP_IsDatabaseExists, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsExists;
        }

        /// <summary>
        /// Drop database
        /// </summary>
        /// <param name="CopyDatabaseDetail"></param>
        /// <returns>bool value</returns>
        public static bool DropDatabase(string DatabaseName)
        {
            bool IsDeleted = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DBName", Convert.ToString(DatabaseName));
                IsDeleted = StoreDA.Delete(Constants.USP_DropDatabase, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsDeleted;
        }

        /// <summary>
        /// Update Store Detail
        /// </summary>
        /// <param name="StoreLanguageDetail"></param>
        /// <returns>returns bool value</returns>
        public static bool UpdateStoreDetail(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString = false)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.USP_UpdateStoreDetails, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// Snehal Jadhav - 22 09 2016
        /// Update Store Punchout
        /// </summary>
        /// <param name="StoreLanguageDetail"></param>
        /// <returns>returns bool value</returns>
        public static bool UpdateStorePunchout(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString = false)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.USP_UpdateStorePunchout, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// <summary>
        /// Checks weather store with specified id is created or not
        /// </summary>
        /// <param name="strStoreName"></param>
        /// <returns>returns true or false</returns>
        public static bool IsStoreCreated(Int16 StoreId)
        {
            bool IsCreated = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
                IsCreated = StoreDA.IsExists(Constants.USP_IsStoreCreated, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsCreated;
        }

        /// <summary>
        /// gets Store details
        /// </summary>
        /// <returns>returns StoreBE objects</returns>
        public static DataSet GetMCPStoreDetails(string storeName)
        {
            DataSet getStoreDetails = null;
            try
            {
                //if (HttpContext.Current.Session["StoreDetailsMCP"] == null)
                //{

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StoreName", Convert.ToString(storeName));
                getStoreDetails = StoreDA.getMCPStoreDetails(Constants.USP_GetMasterStoreDetails, dictionaryInstance, false);
                //HttpContext.Current.Session["StoreDetailsMCP"] = getStores;
                //}
                //else
                //    getStores = (StoreBE)HttpContext.Current.Session["StoreDetailsMCP"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getStoreDetails;
        }

        /// <summary>
        /// gets Store details
        /// </summary>
        /// <returns>returns StoreBE objects</returns>
        public static DataSet GetCloneMCPStoreDetails(string storeName)
        {
            DataSet getStoreDetails = null;
            try
            {
                //if (HttpContext.Current.Session["StoreDetailsMCP"] == null)
                //{

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StoreName", Convert.ToString(storeName));
                getStoreDetails = StoreDA.getMCPStoreDetails(Constants.USP_GetCloneMasterStoreDetails, dictionaryInstance, false);
                //HttpContext.Current.Session["StoreDetailsMCP"] = getStores;
                //}
                //else
                //    getStores = (StoreBE)HttpContext.Current.Session["StoreDetailsMCP"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getStoreDetails;
        }

        /// <summary>
        /// update GA Code
        /// </summary>
        /// <param name="StoreId, CurrencyId, GACode"></param>
        /// <returns>update store GA code</returns>
        public static bool UpdateGACode(Int16 StoreId, Int16 CurrencyId, string GACode)
        {
            bool IsUpdated = false;
            try
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("StoreId", Convert.ToString(StoreId));
                dictionary.Add("Code", GACode);
                dictionary.Add("CurrencyId", Convert.ToString(CurrencyId));
                IsUpdated = StoreDA.Update(Constants.USP_InsertGACode, dictionary, true);
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return IsUpdated;
        }

        /// <summary>
        /// Create DataBase User
        /// UserName is same as StoreName
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns>Create DataBase User and returns password</returns>
        public static string CreateDBUser(string UserName)
        {
            string Password = string.Empty;
            string NewDatabaseConnection = Convert.ToString(ConfigurationManager.AppSettings["NewDatabaseConnection"]);
            try
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("Username", Convert.ToString(UserName));

                Password = StoreDA.GetDatabasePassword(Constants.USP_CreateDBUser, dictionary, NewDatabaseConnection.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + UserName));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Password;
        }

        /// <summary>
        /// Create DataBase User
        /// UserName is same as StoreName
        /// </summary>
        /// <param name="StoreId, CurrencyId, GACode"></param>
        /// <returns>Create DataBase User and returns password</returns>
        public static string CreateCloneDBUser(string UserName, string ExistingUserNameToBeRemoved)
        {
            string Password = string.Empty;
            string NewDatabaseConnection = Convert.ToString(ConfigurationManager.AppSettings["NewDatabaseConnection"]);
            try
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("Username", Convert.ToString(UserName));
                dictionary.Add("ExistingUserName", Convert.ToString(ExistingUserNameToBeRemoved));

                Password = StoreDA.GetDatabasePassword(Constants.USP_CreateCloneDBUser, dictionary, NewDatabaseConnection.Replace("$DatabaseName$", Convert.ToString(ConfigurationManager.AppSettings["DBPrefix"]) + UserName));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Password;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 29-12-15
        /// Scope   : Insert / Update Contact Info
        /// </summary>
        /// <param name="objContactBE"></param>
        /// <returns>returns inserted / updated ContactId</returns>
        public static bool InsertUpdateContactInfo(StoreBE.ContactBE objContactBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objContactBE.Action));
                dictionaryInstance.Add("ContactId", Convert.ToString(objContactBE.ContactId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objContactBE.ContactLanguageId));
                dictionaryInstance.Add("HelpDeskEmail", Convert.ToString(objContactBE.HelpDeskEmail));
                dictionaryInstance.Add("HelpDeskPhone", Convert.ToString(objContactBE.HelpDeskPhone));
                dictionaryInstance.Add("HelpDeskContactPerson", Convert.ToString(objContactBE.HelpDeskContactPerson));
                dictionaryInstance.Add("HelpDeskText", Convert.ToString(objContactBE.HelpDeskText));
                dictionaryInstance.Add("Enteredby", Convert.ToString(objContactBE.ModifiedBy));

                

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                StoreDA.Insert(Constants.USP_ManageContactUs, dictionaryInstance, ref dictReturn, true);

                if (Convert.ToInt16(dictReturn["return"]) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   :  GetContact Info
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns Contact Info </returns>
        public static StoreBE.ContactBE GetContactInfo(StoreBE.ContactBE objContactBE)
        {
            StoreBE.ContactBE ContactBE = new StoreBE.ContactBE();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objContactBE.Action));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objContactBE.LanguageId));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                ContactBE = (StoreBE.ContactBE)CommonDA.getItem(Constants.USP_ManageContactUs, dictionaryInstance, ref dictReturn, Constants.Entity_ContactBE, true);
                return ContactBE;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objContactBE = null;
                return ContactBE;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 29-12-15
        /// Scope   : Insert / Update MetaTags Info
        /// </summary>
        /// <param name="objContactBE"></param>
        /// <returns>returns inserted / updated ContactId</returns>
        public static bool InsertUpdateMetaTagContents(StoreBE.MetaTags objMetaTags)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objMetaTags.Action));
                dictionaryInstance.Add("MetaContentId", Convert.ToString(objMetaTags.MetaContentId));
                dictionaryInstance.Add("PageName", Convert.ToString(objMetaTags.PageName));
                dictionaryInstance.Add("MetaContentTitle", Convert.ToString(objMetaTags.MetaContentTitle));
                dictionaryInstance.Add("MetaKeyword", Convert.ToString(objMetaTags.MetaKeyword));
                dictionaryInstance.Add("MetaDescription", Convert.ToString(objMetaTags.MetaDescription));
                dictionaryInstance.Add("Enteredby", Convert.ToString(objMetaTags.ModifiedBy));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objMetaTags.LanguageId));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                StoreDA.Insert(Constants.USP_ManageMetaTags, dictionaryInstance, ref dictReturn, true);

                if (Convert.ToInt16(dictReturn["return"]) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   :  Get Meta Tags Info
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns Contact Info </returns>
        public static StoreBE.MetaTags GetMetaTagContents(StoreBE.MetaTags objMetaTags)
        {
            StoreBE.MetaTags MetaTags = new StoreBE.MetaTags();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objMetaTags.Action));
                dictionaryInstance.Add("MetaContentId", Convert.ToString(objMetaTags.MetaContentId));
                dictionaryInstance.Add("PageName", Convert.ToString(objMetaTags.PageName));
                dictionaryInstance.Add("LanguageId",Convert.ToString(objMetaTags.LanguageId));
                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                MetaTags = (StoreBE.MetaTags)CommonDA.getItem(Constants.USP_ManageMetaTags, dictionaryInstance, ref dictReturn, "StoreBE+MetaTags", true);
                return MetaTags;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                MetaTags = null;
                return MetaTags;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   :  Get Meta Tags List
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns Contact Info </returns>

        public static List<StoreBE.MetaTags> GetListMetaTagContents(StoreBE.MetaTags objMetaTags)
        {
            List<StoreBE.MetaTags> lstMetaTags = new List<StoreBE.MetaTags>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objMetaTags.Action));
                dictionaryInstance.Add("MetaContentId", Convert.ToString(objMetaTags.MetaContentId));
                dictionaryInstance.Add("PageName", Convert.ToString(objMetaTags.PageName));
                dictionaryInstance.Add("LanguageId",Convert.ToString(GlobalFunctions.GetLanguageId()));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                #region stored in chache
                // if (HttpRuntime.Cache["MetaTags"] == null)
               // {
                 //   lstMetaTags = CommonDA.getCollectionItem<StoreBE.MetaTags>(Constants.USP_ManageMetaTags, dictionaryInstance, ref dictReturn, true);
                 //   HttpRuntime.Cache.Insert("MetaTags", lstMetaTags);
                //}
                //else
                //{ lstMetaTags = (List<StoreBE.MetaTags>)HttpRuntime.Cache["MetaTags"]; }
                #endregion

                lstMetaTags = CommonDA.getCollectionItem<StoreBE.MetaTags>(Constants.USP_ManageMetaTags, dictionaryInstance, ref dictReturn, true);
                if (lstMetaTags != null)
                {
                    return lstMetaTags;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstMetaTags = null;
                return lstMetaTags;
            }
        }

        public static bool InsertStoreCurrency(Dictionary<string, string> StoreCurrencyDetail,bool bCon)
        {
            bool IsInserted = false;
            try
            {
                IsInserted = StoreDA.Insert(Constants.USP_InsertStoreCurrencyMultiple, StoreCurrencyDetail, bCon);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsInserted;
        }
        public static int InsertStoreCurrencyCatalogue(Dictionary<string, string> StoreCurrencyDetail, bool bCon)
        {
            int i = 0;
            try
            {
                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                StoreDA.Insert(Constants.USP_InsertStoreCurrencyMultiple_catalogue, StoreCurrencyDetail, ref dictReturn, bCon);
                foreach (KeyValuePair<string, string> item in dictReturn)
                {
                    if (item.Key == "return")
                        i = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return i;
        }

        public static int InsertStoreCurrencyCatalogueMCP(Dictionary<string, string> StoreCurrencyDetail, bool bCon)
        {
            int i = 0;
            try
            {
                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                StoreDA.Insert(Constants.USP_InsertStoreCurrencyMultiple_catalogue_New, StoreCurrencyDetail, ref dictReturn, bCon);
                foreach (KeyValuePair<string, string> item in dictReturn)
                {
                    if (item.Key == "return")
                        i = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return i;
        }

        /// <summary>
        /// Get All currencies from tbl_CurrencyMaster
        /// </summary>
        /// <returns>Returns List of active Currency</returns>
        public static List<StoreBE.StoreCurrencyBE> GetCurrency()
        {
            List<StoreBE.StoreCurrencyBE> getCurrency = null;
            try
            {
                //Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                //dictionaryInstance.Add("StoreId", Convert.ToString());
                getCurrency = StoreDA.getSingleItem(Constants.USP_GetCurrency, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getCurrency;
        }
        public static List<StoreBE.StoreCurrencyBE> GetCurrency(string str)
        {
            List<StoreBE.StoreCurrencyBE> getCurrency = null;
            try
            {
                //Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                //dictionaryInstance.Add("StoreId", Convert.ToString());
                getCurrency = StoreDA.getSingleItem(Constants.USP_GetCurrency, null, str);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getCurrency;
        }

        /// <summary>
        /// Update Store Detail
        /// </summary>
        /// <param name="StoreLanguageDetail"></param>
        /// <returns>returns bool value</returns>
        public static bool UpdateStoreCurrencyAdflexDetail(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString = false)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.USP_UpdateStoreCurrencyAdflexDetails, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// <summary>
        /// gets Store details
        /// </summary>
        /// <returns>returns StoreBE objects</returns>
        public static List<StoreBE> GetStoreDetailsSearch(string strSearch)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("str", strSearch);
                return StoreDA.getCollectionItem(Constants.usp_getStoreBySearch, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
        public static List<StoreReferral.referralurlBE> Getreferralurl()
        {
            List<StoreReferral.referralurlBE> getreferralurl = null;
            
            try
            {
                getreferralurl = StoreDA.getrefferal(Constants.USP_getreferralurl, null, true);
               

                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getreferralurl;
        }

        #region Indeed code
        
               //To update Currency Point Invoice Details		
        //Swapnil Bagkar 31-01-2017		
        public static bool UpdateCurrencyPointInvoice(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.Usp_UpdateCurrencyPointInvoiceDetails, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }


        /// <summary>		
        /// Snehal Jadhav		
        /// 22 03 2017		
        /// Update Projected sale - Indeed Store		
        /// </summary>  		
        public static bool UpdateProjectedSale(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.Usp_Indeed_UpdateProjectedSaleDetails, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        #endregion


    }
}
