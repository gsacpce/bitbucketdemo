﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using PWGlobalEcomm.DataAccess;
using System.Globalization;
using PWGlobalEcomm.BusinessEntity;


namespace PWGlobalEcomm.BusinessLogic
{
    public class FreightManagementBL
    {
        public static List<T> GetAllFreightModes<T>()
        {
            return CommonDA.getCollectionItem<T>(Constants.USP_GetFreightModes, null, true);
        }

        public static List<T> GetAllFreightTables<T>()
        {
            return CommonDA.getCollectionItem<T>(Constants.USP_GetFreightTables, null, true);
        }
        
        public static List<T> GetAllFreightMethods<T>()
        {
            return CommonDA.getCollectionItem<T>(Constants.USP_GetFreightMethods, null, true);
        }

        public static List<T> GetAllFreightRegions<T>()
        {
            return CommonDA.getCollectionItem<T>(Constants.USP_GetFreightRegions, null, true);
        }

        public static List<T> GetAllFreightConfiguration<T>()
        {
            List<T> LstFreightConfiguration = new List<T>();
            if (HttpContext.Current.Cache["FreightConfigurations"] == null)
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(DBAction.Select).ToString());
                //LstFreightConfiguration = CommonDA.getCollectionItem<T>(Constants.USP_FreightManagementSAED, dictParams, true);
                LstFreightConfiguration = CommonDA.getCollectionItem<T>(Constants.USP_FreightManagementSAED, dictParams, true);
                //LstFreightConfiguration = CommonDA.getFreightManagementItem<FreightManagementBE>(Constants.USP_FreightManagementSAED, dictParams, true);
                if (LstFreightConfiguration != null)
                    HttpContext.Current.Cache.Insert("FreightConfigurations", LstFreightConfiguration);

            }
            else
                return (List<T>)HttpContext.Current.Cache["FreightConfigurations"];
            return LstFreightConfiguration;
        }

        public static List<T> GetAllFreightConfigurationPerBand<T>()
        {
            List<T> LstFreightConfiguration = new List<T>();
            //if (HttpContext.Current.Cache["FreightConfigurations"] == null)
            //{
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(DBAction.Select).ToString());
                //LstFreightConfiguration = CommonDA.getCollectionItem<T>(Constants.USP_FreightManagementSAED, dictParams, true);
                LstFreightConfiguration = CommonDA.getCollectionItem<T>(Constants.USP_FreightManagementPerBand, dictParams, true);
                //LstFreightConfiguration = CommonDA.getFreightManagementItem<FreightManagementBE>(Constants.USP_FreightManagementSAED, dictParams, true);
                //if (LstFreightConfiguration != null)
                //    HttpContext.Current.Cache.Insert("FreightConfigurations", LstFreightConfiguration);

            //}
            //else
            //    return (List<T>)HttpContext.Current.Cache["FreightConfigurations"];
            return LstFreightConfiguration;
        }

        // Added By Hardik on "30/May/2017"
        public static List<T> GetAllFreightConfigurationPerBandMultipleCountry<T>()
        {
            List<T> LstFreightConfigurationMultipleCountry = new List<T>();
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("Action", Convert.ToInt16(DBAction.Select).ToString());
            LstFreightConfigurationMultipleCountry = CommonDA.getCollectionItem<T>(Constants.USP_FreightManagementPerBand_MultipleCountrySrc, dictParams, true);
            return LstFreightConfigurationMultipleCountry;
        }

        public static List<T> GetAllFreightPerBand<T>()
        {
            List<T> LstFreightConfiguration = new List<T>();
            //if (HttpContext.Current.Cache["FreightConfigurations"] == null)
            //{
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("Action", Convert.ToInt16("5").ToString());
            //LstFreightConfiguration = CommonDA.getCollectionItem<T>(Constants.USP_FreightManagementSAED, dictParams, true);
            LstFreightConfiguration = CommonDA.getCollectionItem<T>(Constants.USP_FreightManagementPerBand, dictParams, true);
            //LstFreightConfiguration = CommonDA.getFreightManagementItem<FreightManagementBE>(Constants.USP_FreightManagementSAED, dictParams, true);
            //if (LstFreightConfiguration != null)
            //    HttpContext.Current.Cache.Insert("FreightConfigurations", LstFreightConfiguration);

            //}
            //else
            //    return (List<T>)HttpContext.Current.Cache["FreightConfigurations"];
            return LstFreightConfiguration;
        }

        public static bool FreightManagementSAED(FreightManagementBE freightConfiguration, DBAction dbAction)
        {
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(dbAction).ToString());

                switch (dbAction)
                {
                    case DBAction.Insert:
                    case DBAction.Update:
                        {
                            dictParams.Add("FreightConfigurationId", freightConfiguration.FreightConfigurationId.ToString());
                            dictParams.Add("FreightRegionId", freightConfiguration.FreightRegionId.ToString());
                            dictParams.Add("FreightMethodId", freightConfiguration.FreightMethodId.ToString());
                            dictParams.Add("FreightModeId", freightConfiguration.FreightModeId.ToString());
                            dictParams.Add("CarrierServiceId", freightConfiguration.CarrierServiceId.ToString());
                            dictParams.Add("CarrierServiceText", freightConfiguration.CarrierServiceText.ToString());
                            dictParams.Add("TransitTime", freightConfiguration.TransitTime.ToString());
                            dictParams.Add("MinOrderValue", freightConfiguration.MinOrderValueCM.ToString());
                            if (Convert.ToInt16(freightConfiguration.FreightMethodId) == 3)
                            {
                                dictParams.Add("CurrencyMultipler", freightConfiguration.ValueAppliedAboveCM.ToString());
                                dictParams.Add("ValueAppliedAbove", "");
                            }
                            else
                            {
                                dictParams.Add("CurrencyMultipler", "");
                                dictParams.Add("ValueAppliedAbove", freightConfiguration.ValueAppliedAboveCM.ToString());
                            }
                            dictParams.Add("ValueAppliedBelow", freightConfiguration.ValueAppliedBelowCM.ToString());
                            dictParams.Add("PerBoxPerOrder", freightConfiguration.PerBoxPerOrder);
                            dictParams.Add("DisallowOrderBelowMOV", freightConfiguration.DisAllowOrderBelowMOV.Equals(true) ? "1" : "0");

                            if (dbAction.Equals(DBAction.Insert))
                            {
                                dictParams.Add("CreatedBy", freightConfiguration.CreatedBy.ToString());
                                dictParams.Add("CreatedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                dictParams.Add("CreatedBy", freightConfiguration.CreatedBy.ToString());
                                dictParams.Add("CreatedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }

                            if (dbAction.Equals(DBAction.Update))
                            {
                                dictParams.Add("Modifiedby", freightConfiguration.ModifiedBy.ToString());
                                dictParams.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                dictParams.Add("Modifiedby", freightConfiguration.ModifiedBy.ToString());
                                dictParams.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }



                            if (dbAction.Equals(DBAction.Insert))
                                CommonDA.Insert(Constants.USP_FreightManagementSAED, dictParams, true);
                            else
                                CommonDA.Update(Constants.USP_FreightManagementSAED, dictParams, true);
                            break;
                        }
                    case DBAction.Delete:
                        {
                            dictParams.Add("FreightConfigurationId", freightConfiguration.FreightConfigurationId.ToString());
                            CommonDA.Insert(Constants.USP_FreightManagementSAED, dictParams, true);
                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }


            return true;
        }

        public static bool DeleteFreightCombination(FreightManagementBE freightConfiguration)
        {
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16("6").ToString());
                dictParams.Add("FreightRegionId", freightConfiguration.FreightRegionId.ToString());
                dictParams.Add("FreightModeId", freightConfiguration.FreightModeId.ToString());
                dictParams.Add("WeightParameter", freightConfiguration.WeightParameter.ToString());
                dictParams.Add("CurrencyParameter", freightConfiguration.CurrencyParameter.ToString());
                CommonDA.Insert(Constants.USP_FreightManagementPerBand, dictParams, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);

            }


            return true;
        }

        public static bool FreightManagementPerBand(FreightManagementBE freightConfiguration, DBAction dbAction)
        {
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(dbAction).ToString());

                switch (dbAction)
                {
                    case DBAction.Insert:
                    case DBAction.Update:
                        {
                            dictParams.Add("FreightConfigurationId", freightConfiguration.FreightConfigurationId.ToString());
                            dictParams.Add("FreightRegionId", freightConfiguration.FreightRegionId.ToString());
                            dictParams.Add("FreightMethodId", freightConfiguration.FreightMethodId.ToString());
                            dictParams.Add("FreightModeId", freightConfiguration.FreightModeId.ToString());
                            dictParams.Add("CarrierServiceId", freightConfiguration.CarrierServiceId.ToString());
                            dictParams.Add("CarrierServiceText", freightConfiguration.CarrierServiceText.ToString());
                            dictParams.Add("TransitText", freightConfiguration.TransitTime.ToString());
                            dictParams.Add("CustomText", freightConfiguration.CustomText.ToString());
                            dictParams.Add("WeightParameter", freightConfiguration.WeightParameter.ToString());
                            dictParams.Add("CurrencyParameter", freightConfiguration.CurrencyParameter.ToString());
                            dictParams.Add("WeightFromToValue", freightConfiguration.WeightFromToValueCM.ToString());
                            dictParams.Add("CurrencyFromToValue", freightConfiguration.CurrencyFromToValueCM.ToString());
                            dictParams.Add("MinOrderValue", freightConfiguration.MinOrderValueCM.ToString());
                            dictParams.Add("IsBandActive", freightConfiguration.IsActive.ToString());
                            if (Convert.ToInt16(freightConfiguration.FreightMethodId) == 3)
                            {
                                dictParams.Add("CurrencyMultipler", freightConfiguration.ValueAppliedAboveCM.ToString());
                                dictParams.Add("ValueAppliedAbove", "");
                                dictParams.Add("FreightTableId", freightConfiguration.FreightTableId.ToString());

                            }
                            else
                            {
                                dictParams.Add("CurrencyMultipler", "");
                                dictParams.Add("ValueAppliedAbove", freightConfiguration.ValueAppliedAboveCM.ToString());
                            }
                            dictParams.Add("BandName", freightConfiguration.BandName.ToString());
                            dictParams.Add("WeightBand", freightConfiguration.WeightBand.ToString());
                            dictParams.Add("LanguageId", freightConfiguration.LanguageId.ToString());

                            dictParams.Add("ValueAppliedBelow", freightConfiguration.ValueAppliedBelowCM.ToString());
                            dictParams.Add("PerBoxPerOrder", freightConfiguration.PerBoxPerOrder);
                            dictParams.Add("DisallowOrderBelowMOV", freightConfiguration.DisAllowOrderBelowMOV.Equals(true) ? "1" : "0");

                            if (dbAction.Equals(DBAction.Insert))
                            {
                                dictParams.Add("CreatedBy", freightConfiguration.CreatedBy.ToString());
                                dictParams.Add("CreatedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                dictParams.Add("CreatedBy", freightConfiguration.CreatedBy.ToString());
                                dictParams.Add("CreatedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }

                            if (dbAction.Equals(DBAction.Update))
                            {
                                dictParams.Add("Modifiedby", freightConfiguration.ModifiedBy.ToString());
                                dictParams.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                dictParams.Add("Modifiedby", freightConfiguration.ModifiedBy.ToString());
                                dictParams.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }                            

                            if (dbAction.Equals(DBAction.Insert))
                                CommonDA.Insert(Constants.USP_FreightManagementPerBand, dictParams, true);
                            else
                                CommonDA.Update(Constants.USP_FreightManagementPerBand, dictParams, true);
                            break;
                        }
                    case DBAction.Delete:
                        {
                            dictParams.Add("FreightConfigurationId", freightConfiguration.FreightConfigurationId.ToString());
                            CommonDA.Insert(Constants.USP_FreightManagementPerBand, dictParams, true);
                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return true;
        }

        public static bool FreightManagementPerBandMultipleSourceCountry(FreightManagementBE freightConfiguration, DBAction dbAction)
        {
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(dbAction).ToString());

                switch (dbAction)
                {
                    case DBAction.Insert:
                    case DBAction.Update:
                        {
                            dictParams.Add("FreightConfigurationId", freightConfiguration.FreightConfigurationId.ToString());
                            dictParams.Add("FreightRegionId", freightConfiguration.FreightRegionId.ToString());
                            dictParams.Add("FreightMethodId", freightConfiguration.FreightMethodId.ToString());
                            dictParams.Add("FreightModeId", freightConfiguration.FreightModeId.ToString());
                            dictParams.Add("CarrierServiceId", freightConfiguration.CarrierServiceId.ToString());
                            dictParams.Add("CarrierServiceText", freightConfiguration.CarrierServiceText.ToString());
                            dictParams.Add("TransitText", freightConfiguration.TransitTime.ToString());
                            dictParams.Add("CustomText", freightConfiguration.CustomText.ToString());
                            dictParams.Add("WeightParameter", freightConfiguration.WeightParameter.ToString());
                            dictParams.Add("CurrencyParameter", freightConfiguration.CurrencyParameter.ToString());
                            dictParams.Add("WeightFromToValue", freightConfiguration.WeightFromToValueCM.ToString());
                            dictParams.Add("CurrencyFromToValue", freightConfiguration.CurrencyFromToValueCM.ToString());
                            dictParams.Add("MinOrderValue", freightConfiguration.MinOrderValueCM.ToString());
                            dictParams.Add("IsBandActive", freightConfiguration.IsActive.ToString());
                            if (Convert.ToInt16(freightConfiguration.FreightMethodId) == 3)
                            {
                                dictParams.Add("CurrencyMultipler", freightConfiguration.ValueAppliedAboveCM.ToString());
                                dictParams.Add("ValueAppliedAbove", "");
                                dictParams.Add("FreightTableId", freightConfiguration.FreightTableId.ToString());
                            }
                            else
                            {
                                dictParams.Add("CurrencyMultipler", "");
                                dictParams.Add("ValueAppliedAbove", freightConfiguration.ValueAppliedAboveCM.ToString());
                            }
                            dictParams.Add("BandName", freightConfiguration.BandName.ToString());
                            dictParams.Add("WeightBand", freightConfiguration.WeightBand.ToString());
                            dictParams.Add("LanguageId", freightConfiguration.LanguageId.ToString());

                            dictParams.Add("ValueAppliedBelow", freightConfiguration.ValueAppliedBelowCM.ToString());
                            dictParams.Add("PerBoxPerOrder", freightConfiguration.PerBoxPerOrder);
                            dictParams.Add("DisallowOrderBelowMOV", freightConfiguration.DisAllowOrderBelowMOV.Equals(true) ? "1" : "0");

                            if (dbAction.Equals(DBAction.Insert))
                            {
                                dictParams.Add("CreatedBy", freightConfiguration.CreatedBy.ToString());
                                dictParams.Add("CreatedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                dictParams.Add("CreatedBy", freightConfiguration.CreatedBy.ToString());
                                dictParams.Add("CreatedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }

                            if (dbAction.Equals(DBAction.Update))
                            {
                                dictParams.Add("Modifiedby", freightConfiguration.ModifiedBy.ToString());
                                dictParams.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                dictParams.Add("Modifiedby", freightConfiguration.ModifiedBy.ToString());
                                dictParams.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd"));
                            }

                            dictParams.Add("CountryCode", freightConfiguration.FreightSourceCountryCode);

                            if (dbAction.Equals(DBAction.Insert))
                                CommonDA.Insert(Constants.USP_FreightManagementPerBand_MultipleCountrySrc, dictParams, true);
                            else
                                CommonDA.Update(Constants.USP_FreightManagementPerBand_MultipleCountrySrc, dictParams, true);
                            break;
                        }
                    case DBAction.Delete:
                        {
                            dictParams.Add("FreightConfigurationId", freightConfiguration.FreightConfigurationId.ToString());
                            CommonDA.Insert(Constants.USP_FreightManagementPerBand_MultipleCountrySrc, dictParams, true);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return true;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 24-09-15
        /// Scope   : Gets Freight values
        /// </summary>
        /// <param name="objFreight"></param>
        /// <returns>returns FreightManagementBE objects</returns>
        public static List<FreightManagementBE> GetFreightDetails(FreightManagementBE objFreightBE)
        {
            try
            {
                List<object> lstFreight = new List<object>();
                StoreBE objStoreBE = new StoreBE();
                objStoreBE = StoreBL.GetStoreDetails();
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                {
                    lstFreight = GetNewFreightDetails(objFreightBE);
                }
                else
                {
                    lstFreight = GetMutliFreightDetails(objFreightBE);
                }
                
                List<FreightManagementBE> lstFreightBE = new List<FreightManagementBE>();
                lstFreightBE = lstFreight.Cast<FreightManagementBE>().ToList();

                if (lstFreightBE[0].IsSZCalculateGridMatrix || lstFreightBE[0].IsEZCalculateGridMatrix || lstFreightBE[0].IsPZCalculateGridMatrix)
                {
                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    List<object> lstOldFreight = new List<object>();

                    dictionaryInstance.Add("CountryCode", objFreightBE.CountryCode.ToString(CultureInfo.InvariantCulture));
                    dictionaryInstance.Add("WeightUnits", objFreightBE.WeightUnits.ToString(CultureInfo.InvariantCulture));
                    dictionaryInstance.Add("CurrencyMultiplier", objFreightBE.CurrencyMultiplier.ToString(CultureInfo.InvariantCulture));
                    dictionaryInstance.Add("FreightMultiplier", objFreightBE.FreightMultiplier.ToString(CultureInfo.InvariantCulture));                   
                    dictionaryInstance.Add("SFreightMultiplier", objFreightBE.StandardFreightMultiplier.ToString(CultureInfo.InvariantCulture));
                    dictionaryInstance.Add("PFreightMultiplier", objFreightBE.PalletShipmentFreightMultiplier.ToString(CultureInfo.InvariantCulture));

                    if(lstFreightBE[0].IsSZCalculateGridMatrix)
                    { 
                        dictionaryInstance.Add("SZFreightConfigurationId", lstFreightBE[0].S1configuration.ToString());
                    }
                    else
                    {
                        dictionaryInstance.Add("SZFreightConfigurationId", "0");
                    }
                    if (lstFreightBE[0].IsEZCalculateGridMatrix)
                    {
                        dictionaryInstance.Add("EZFreightConfigurationId", lstFreightBE[0].S2configuration.ToString());
                    }
                    else
                    {
                        dictionaryInstance.Add("EZFreightConfigurationId", "0");
                    }
                    if (lstFreightBE[0].IsPZCalculateGridMatrix)
                    {
                        dictionaryInstance.Add("PZFreightConfigurationId", lstFreightBE[0].S3configuration.ToString());
                    }
                    else
                    {
                        dictionaryInstance.Add("PZFreightConfigurationId", "0");
                    }                       

                    //dictionaryInstance.Add("LanguageId", Convert.ToString(objFreightBE.LanguageId)); // SHRIGANESH 08 November 2016                  

                    #region
                    // dictionaryInstance.Add("SFreightMultiplier", objFreightBE.StandardFreightMultiplier.ToString(CultureInfo.InvariantCulture));
                    //dictionaryInstance.Add("CountryCode", Convert.ToString(objFreightBE.CountryCode));
                    //dictionaryInstance.Add("WeightUnits", Convert.ToString(objFreightBE.WeightUnits));
                    //dictionaryInstance.Add("FreightMultiplier", Convert.ToString(objFreightBE.FreightMultiplier));
                    //dictionaryInstance.Add("CurrencyMultiplier", Convert.ToString(objFreightBE.CurrencyMultiplier)); 
                    #endregion

                    if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName.ToLower() == "freightsrccountry").FeatureValues[0].IsEnabled)
                    {
                        lstOldFreight = CommonDA.getCollectionItem(Constants.USP_GetFreightDetails, dictionaryInstance, true, Constants.Entity_FreightManageMentBE);
                    }
                    else
                    {
                        lstOldFreight = CommonDA.getCollectionItem(Constants.USP_GetFreightDetails_Multiwarehouse, dictionaryInstance, true, Constants.Entity_FreightManageMentBE);
                    }

                    
                    List<FreightManagementBE> lstOldFreightBE = new List<FreightManagementBE>();
                    lstOldFreightBE = lstOldFreight.Cast<FreightManagementBE>().ToList();                  
                   
                 
                 

                    if (lstFreightBE[0].IsSZCalculateGridMatrix)
                        lstFreightBE[0].SZPrice = lstOldFreightBE[0].SZPrice;
                    if (lstFreightBE[0].IsEZCalculateGridMatrix)
                        lstFreightBE[0].EZPrice = lstOldFreightBE[0].EZPrice;
                    if (lstFreightBE[0].IsPZCalculateGridMatrix)
                        lstFreightBE[0].PSPrice = lstOldFreightBE[0].PSPrice;
                }
                return lstFreightBE;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static void TruncateTables()
        {
            CommonDA.Delete("Usp_TruncateTablesForFreight", null, true);
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 29-10-15
        /// Scope   : Gets New Freight values
        /// </summary>
        /// <param name="objFreight"></param>
        /// <returns>returns FreightManagementBE objects</returns>
        
        public static List<FreightManagementBE> GetAllFreights()
        {
            List<object> lstObj = new List<object>();
            List<FreightManagementBE> objFreight = new List<FreightManagementBE>();
            lstObj=CommonDA.getCollectionItem(Constants.usp_GCRegions, null, true, Constants.Entity_FreightManageMentBE);
            objFreight = lstObj.Cast<FreightManagementBE>().ToList();
            return objFreight;
        }

        public static List<object> GetNewFreightDetails(FreightManagementBE objFreightBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("FreightRegionName", Convert.ToString(objFreightBE.FreightRegionName));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objFreightBE.LanguageId));
                //dictionaryInstance.Add("CurrencyMultiplier", objFreightBE.CurrencyMultiplier.ToString(CultureInfo.InvariantCulture));
                //dictionaryInstance.Add("FreightMultiplier", objFreightBE.FreightMultiplier.ToString(CultureInfo.InvariantCulture));
                dictionaryInstance.Add("OrderValue", objFreightBE.OrderValue.ToString(CultureInfo.InvariantCulture));
                dictionaryInstance.Add("Weight", Convert.ToString(objFreightBE.Weight));
                //dictionaryInstance.Add("WeightPerBox", Convert.ToString(objFreightBE.WeightPerBox));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));

                return CommonDA.getCollectionItem(Constants.USP_GetFreightCharges, dictionaryInstance, true, Constants.Entity_FreightManageMentBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// method for freight of multiwarehouse
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FreightMehodId"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
          public static List<object> GetMutliFreightDetails(FreightManagementBE objFreightBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("FreightRegionName", Convert.ToString(objFreightBE.FreightRegionName));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objFreightBE.LanguageId));
                dictionaryInstance.Add("OrderValue", objFreightBE.OrderValue.ToString(CultureInfo.InvariantCulture));
                dictionaryInstance.Add("Weight", Convert.ToString(objFreightBE.Weight));                
                dictionaryInstance.Add("CurrencyId", Convert.ToString(GlobalFunctions.GetCurrencyId()));
                return CommonDA.getCollectionItem(Constants.USP_GetFreightCharges_mutliwarehouse, dictionaryInstance, true, Constants.Entity_FreightManageMentBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }



        public static List<T> GetAllFreightMultiplerCurrency<T>(Int16 FreightMehodId, string Type)
        {
            List<T> LstFreightConfiguration = new List<T>();
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("Action", Convert.ToInt16(DBAction.Select).ToString());
            dictParams.Add("FreightMehodId", Convert.ToInt16(FreightMehodId).ToString());
            dictParams.Add("Type", Convert.ToString(Type));
            LstFreightConfiguration = CommonDA.getCollectionItem<T>(Constants.USP_GetAllFreightMultiplerCurrency, dictParams, true);
            return LstFreightConfiguration;
        }

        public static List<T> GetAllFreightConfiguration<T>(string countrycode, string FreightRegionName, int LanguageId)
        {
            List<T> LstFreightConfiguration = new List<T>();
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("countrycode", countrycode);
            dictParams.Add("FreightRegionName", FreightRegionName);
            dictParams.Add("LanguageId", Convert.ToInt16(LanguageId).ToString()); // Modified this paramater from FreightModeID to LanguageId 11 nov 2016 SHRIGANESH
            LstFreightConfiguration = CommonDA.getCollectionItem<T>(Constants.USP_GETFreightConfigurations, dictParams, true);
            return LstFreightConfiguration;
        }

        public static List<Freight_Carrier_ServiceBE> GetAllFreightCarrierService()
        {
            List<Freight_Carrier_ServiceBE> lstFreightCarrierService = new List<Freight_Carrier_ServiceBE>();
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                lstFreightCarrierService = CommonDA.getCollectionItem<Freight_Carrier_ServiceBE>(Constants.USP_GetFreight_Carrier_Service, dictParams, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return lstFreightCarrierService;
        }

        public static FreightSourceCountryMasterBE GetFreightSourceCountryMaster(string countrycode)
        {
            FreightSourceCountryMasterBE objFreightSourceCountryMaster = new FreightSourceCountryMasterBE();
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("countrycode", countrycode);
                objFreightSourceCountryMaster = (FreightSourceCountryMasterBE)CommonDA.getItem(Constants.USP_GetAllFreightSourceCountryMaster, dictParams, "FreightSourceCountryMasterBE", true);
                return objFreightSourceCountryMaster;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objFreightSourceCountryMaster = null;
                return objFreightSourceCountryMaster;
            }
        }

        public static List<object> GetFreightSourceCountryMasterList()
        {
            FreightSourceCountryMasterBE objFreightSourceCountryMaster = new FreightSourceCountryMasterBE();
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                return CommonDA.getCollectionItem(Constants.USP_GetAllFreightSourceCountryMaster, dictParams, true, "FreightSourceCountryMasterBE");
                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
               // return objFreightSourceCountryMaster;
            }
        }
        public static int InsertFreightSourceCountry(FreightSourceCountryMasterBE objFreight, Dictionary<string, string> dictParams)
        {
            Int16 i = 0;

            try
            {
                dictParams.Add("CountryCode", objFreight.FreightSourceCountryCode);
                dictParams.Add("CountryName", objFreight.FreightSourceCountryName);
                dictParams.Add("FreightTableID", objFreight.FreightTableID);
                dictParams.Add("IsDefault", Convert.ToString(objFreight.IsDefault));
                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));
                CommonDA.Insert(Constants.USP_InsertFreightSourceCountryMaster, dictParams, ref dictReturn, true);
                foreach (KeyValuePair<string, string> item in dictReturn)
                {
                        if (item.Key == "return")
                        i = Convert.ToInt16(item.Value);
                }
            }
            catch(Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return i;
        }

        /// <summary>
        /// Added By Hardik on - 19/May/2017        
        /// To delete FreightSourceCountry from tbl_FreightSourceCountryMaster
        /// </summary>        
        /// <returns> True or False</returns>
        public static int DeleteFreightSourceCountry(FreightSourceCountryMasterBE objFreight, Dictionary<string,string> dictParams)
        {
            Int16 i = 0;
            try
            {
                dictParams.Add("CountryCode", objFreight.FreightSourceCountryCode);
                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));
                CommonDA.Delete(Constants.USP_DeleteFreightSourceCountryMaster, dictParams, ref dictReturn, true);
                foreach (KeyValuePair<string, string> item in dictReturn)
                {
                    if (item.Key == "return")
                        i = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return i;
        }

        public static int UpdateIsDefault(FreightSourceCountryMasterBE objFreight, Dictionary<string, string> dictParams)
        {
            Int16 i = 0;
            try
            {
                dictParams.Add("Id", Convert.ToString(objFreight.ID));
                dictParams.Add("IsDefault", Convert.ToString(objFreight.IsDefault));
                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));
                CommonDA.Update("USP_UpdateFreightSourceCountryMasterIsDefault", dictParams, ref dictReturn, true);
                foreach (KeyValuePair<string, string> item in dictReturn)
                {
                    if (item.Key == "return")
                        i = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return i;
        }

        //Added By Hardik To get FreightTableID [',' seperated] on 23/May/2017
        public static List<T> GetAllFreightTableID<T>(string countrycode)
        {
            
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("CountryCode", Convert.ToString(countrycode));
                return CommonDA.getCollectionItem<T>(Constants.USP_GetFreightTableID, dictParams, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
            
        }
    }
}
