﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWGlobalEcomm.DataAccess;

namespace PWGlobalEcomm.BusinessLogic
{
    public partial class FreightBL
    {
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 24-09-15
        /// Scope   : Gets Freight values
        /// </summary>
        /// <param name="objFreight"></param>
        /// <returns>returns FreightBE objects</returns>
        public static List<object> GetFreightDetails(FreightBE objFreightBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CountryCode", Convert.ToString(objFreightBE.CountryCode));
                dictionaryInstance.Add("WeightUnits", Convert.ToString(objFreightBE.WeightUnits));
                dictionaryInstance.Add("FreightMultiplier", Convert.ToString(objFreightBE.FreightMultiplier));
                dictionaryInstance.Add("CurrencyMultiplier", Convert.ToString(objFreightBE.CurrencyMultiplier));
                return CommonDA.getCollectionItem(Constants.USP_GetFreightDetails, dictionaryInstance, false,Constants.Entity_FreightBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
    }
}