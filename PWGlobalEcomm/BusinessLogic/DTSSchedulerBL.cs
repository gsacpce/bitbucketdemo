﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    public class DTSSchedulerBL
    {
        /// <summary>
        /// gets DTS details
        /// </summary>
        /// <returns>returns Dataset</returns>
        public static DataSet GetDTSDetails(string StoreId)
        {
            Exceptions.WriteInfoLog("DTSSchedulerBL : GetDTSDetails started");
            Exceptions.WriteInfoLog("StoreId = " + StoreId);
            DataSet getDTSDetails = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StoreId", StoreId);
                Exceptions.WriteInfoLog("DTSSchedulerBL : GetDTSDetails -> getDTSDetails before start");
                getDTSDetails = DTSSchedulerDA.getDTSDetails(Constants.USP_GetDTSDetails, dictionaryInstance, false);
                Exceptions.WriteInfoLog("DTSSchedulerBL : GetDTSDetails -> getDTSDetails after start");
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("DTSSchedulerBL : GetDTSDetails Exception");
                Exceptions.WriteExceptionLog(ex);
            }
            Exceptions.WriteInfoLog("DTSSchedulerBL : GetDTSDetails end");
            return getDTSDetails;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>returns bool value</returns>
        public static bool TransferProducts(int UserId, DataTable CorpProgProduct, DataTable StoreLanguages, DataTable StoreCurrencies, DataTable StoreInvoiceData,
            DataTable dtFreightStandard_Germany, DataTable dtFreightExpress_Germany, DataTable dtFreightCountryZones_Germany,
            DataTable dtFreightStandard, DataTable dtFreightExpress, DataTable dtFreightCountryZones,
            DataTable dtBA_POINTS, DataTable dtBA_USERFILE, DataTable dtBA_SALESBYGROUP, DataTable dtBA_SALESGRANDTOTAL, DataTable dtBA_SALESBYPRODUCT, DataTable dtBA_BUDGETS,
            string StoreName, string NewStoreConnectionString)
        {
            Exceptions.WriteInfoLog("DTSSchedulerBL : TransferProducts started");
            bool IsTransfered = false;
            try
            {
                Dictionary<string, object> dictionaryInstance = new Dictionary<string, object>();
                dictionaryInstance.Add("UserId", UserId);
                dictionaryInstance.Add("Table", CorpProgProduct);
                dictionaryInstance.Add("StoreLanguageTable", StoreLanguages);
                dictionaryInstance.Add("StoreCurrencyTable", StoreCurrencies);
                dictionaryInstance.Add("StoreInvoiceData", StoreInvoiceData);

                dictionaryInstance.Add("FreightStandard_Germany", dtFreightStandard_Germany);
                dictionaryInstance.Add("FreightExpress_Germany", dtFreightExpress_Germany);
                dictionaryInstance.Add("FreightCountryZones_Germany", dtFreightCountryZones_Germany);

                dictionaryInstance.Add("FreightStandard", dtFreightStandard);
                dictionaryInstance.Add("FreightExpress", dtFreightExpress);
                dictionaryInstance.Add("FreightCountryZones", dtFreightCountryZones);


                #region "Indeed"
                dictionaryInstance.Add("dtBA_POINTS", dtBA_POINTS);
                dictionaryInstance.Add("dtBA_USERFILE", dtBA_USERFILE);
                dictionaryInstance.Add("dtBA_SALESBYGROUP", dtBA_SALESBYGROUP);
                dictionaryInstance.Add("dtBA_SALESGRANDTOTAL", dtBA_SALESGRANDTOTAL);
                dictionaryInstance.Add("dtBA_SALESBYPRODUCT", dtBA_SALESBYPRODUCT);
                dictionaryInstance.Add("dtBA_BUDGETS", dtBA_BUDGETS);
                dictionaryInstance.Add("StoreName", StoreName);
                #endregion

                IsTransfered = DTSSchedulerDA.TransferProducts(Constants.USP_TransferProducts, dictionaryInstance, NewStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("DTSSchedulerBL : TransferProducts Exception");
                Exceptions.WriteExceptionLog(ex);
            }
            Exceptions.WriteInfoLog("DTSSchedulerBL : TransferProducts end");
            return IsTransfered;
        }

        public static bool RunDTS(int UserId, string StoreId, ref int ProductCount)
        {
            Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS started");
            bool IsTransfered = false;
            try
            {
                Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> before GetDTSDetails call");
                Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> StoreId = " + StoreId);
                DataSet getDTSDetails = DTSSchedulerBL.GetDTSDetails(StoreId);

                try
                {
                    if (getDTSDetails.Tables[0].Rows.Count > 0)
                    {
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> drStores has rows count = " + getDTSDetails.Tables[0].Rows.Count);
                    }
                    if (getDTSDetails.Tables[1].Rows.Count > 0)
                    {
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> dtStoreProducts has rows count = " + getDTSDetails.Tables[1].Rows.Count);
                    }
                    if (getDTSDetails.Tables[2].Rows.Count > 0)
                    {
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> dtStoreLanguages has rows count = " + getDTSDetails.Tables[2].Rows.Count);
                    }
                    if (getDTSDetails.Tables[3].Rows.Count > 0)
                    {
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> dtStoreCurrencies has rows count = " + getDTSDetails.Tables[3].Rows.Count);
                    }
                    if (getDTSDetails.Tables[4].Rows.Count > 0)
                    {
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> dtStoreCurrencies has rows count = " + getDTSDetails.Tables[4].Rows.Count);
                    }
                    if (getDTSDetails.Tables[5].Rows.Count > 0)
                    {
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> dtInvoiceData has rows count = " + getDTSDetails.Tables[5].Rows.Count);
                    }
                }
                catch { }
                Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> after GetDTSDetails call");
                DataRow[] drStores = getDTSDetails.Tables[0].Select("IsBASYS = 1 and IsCreated = 1");


                if (UserId == 0)
                { UserId = Convert.ToInt32(getDTSDetails.Tables[4].Rows[0]["UserId"]); }

                Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> UserId = " + Convert.ToString(UserId));
                foreach (DataRow store in drStores)
                {
                    try
                    {
                        string StoreName = store["StoreName"].ToString().ToLower();
                        // if (store["storename"].ToString().ToLower() == "jcbdealer" || store["storename"].ToString().ToLower() == "ee" || store["storename"].ToString().ToLower() == "thebodyshop" || store["storename"].ToString().ToLower() == "vattenfall" || store["storename"].ToString().ToLower() == "aviva")
                        //if (store["storename"].ToString().ToLower() == "exxondistributors")
                        //{
                        #region
                        DataTable dtStoreProducts = null;
                        DataTable dtStoreLanguages = null;
                        DataTable dtStoreCurrencies = null;
                        DataTable dtInvoiceData = null;

                        DataTable dtFreightCountryZones_Germany = null;
                        DataTable dtFreightExpress_Germany = null;
                        DataTable dtFreightStandard_Germany = null;
                        DataTable dtFreightCountryZones = null;
                        DataTable dtFreightExpress = null;
                        DataTable dtFreightStandard = null;

                        #region "For indeed store special"
                        DataTable dtBA_POINTS = null;
                        DataTable dtBA_USERFILE = null;
                        DataTable dtBA_SALESBYGROUP = null;
                        DataTable dtBA_SALESGRANDTOTAL = null;
                        DataTable dtBA_SALESBYPRODUCT = null;
                        DataTable dtBA_BUDGETS = null;
                        #endregion

                        #region
                        if (string.IsNullOrEmpty(StoreId))
                        {
                            #region
                            dtStoreLanguages = getDTSDetails.Tables[2].Select("StoreId = " + store["StoreId"] + "").CopyToDataTable();
                            dtStoreCurrencies = getDTSDetails.Tables[3].Select("StoreId = " + store["StoreId"] + "").CopyToDataTable();

                            int i = 0;
                            string CatalogueIds = "";
                            #region
                            foreach (DataRow storeCurrency in dtStoreCurrencies.Rows)
                            {
                                if (i == 0)
                                {
                                    CatalogueIds = Convert.ToString(storeCurrency["CatalogueId"]);
                                    i++;
                                }
                                else
                                    CatalogueIds += "," + Convert.ToString(storeCurrency["CatalogueId"]);
                            }
                            #endregion
                            Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> CatalogueIds = " + CatalogueIds);
                            DataRow[] drArr = getDTSDetails.Tables[1].Select("CATALOGUE_ID in (" + CatalogueIds + ")");
                            if (drArr.Count() > 0)
                            {
                                dtStoreProducts = getDTSDetails.Tables[1].Select("CATALOGUE_ID in (" + CatalogueIds + ")").CopyToDataTable();
                            }
                            else
                            {
                                dtStoreProducts = getDTSDetails.Tables[6];
                            }
                            #endregion
                        }
                        else
                        {
                            dtStoreLanguages = getDTSDetails.Tables[2];
                            dtStoreCurrencies = getDTSDetails.Tables[3];
                            dtStoreProducts = getDTSDetails.Tables[1];
                        }
                        #endregion

                        dtInvoiceData = getDTSDetails.Tables[5];

                        dtFreightCountryZones_Germany = getDTSDetails.Tables[7];
                        dtFreightExpress_Germany = getDTSDetails.Tables[8];
                        dtFreightStandard_Germany = getDTSDetails.Tables[9];

                        dtFreightCountryZones = getDTSDetails.Tables[10];
                        dtFreightExpress = getDTSDetails.Tables[11];
                        dtFreightStandard = getDTSDetails.Tables[12];

                        try
                        {
                            #region "For indeed store special"
                            dtBA_POINTS = getDTSDetails.Tables[13];
                            dtBA_USERFILE = getDTSDetails.Tables[14];
                            dtBA_SALESBYGROUP = getDTSDetails.Tables[15];
                            dtBA_SALESGRANDTOTAL = getDTSDetails.Tables[16];
                            dtBA_SALESBYPRODUCT = getDTSDetails.Tables[17];
                            dtBA_BUDGETS = getDTSDetails.Tables[18];
                            #endregion
                        }
                        catch (Exception) { }

                        DataView dvStoreLanguages = new DataView(dtStoreLanguages);
                        dtStoreLanguages = dvStoreLanguages.ToTable(false, new string[] { "LanguageId" });

                        DataView dvStoreCurrencies = new DataView(dtStoreCurrencies);
                        dtStoreCurrencies = dvStoreCurrencies.ToTable(false, new string[] { "CatalogueId", "CurrencyId" });

                        string StoreConnectionString = Convert.ToString(ConfigurationManager.AppSettings["DynamicDatabaseConnection"])
                                                 .Replace("$DatabaseServer$", Convert.ToString(store["DBServerName"]))
                                                 .Replace("$DatabaseName$", Convert.ToString(store["DBName"]))
                                                 .Replace("$UserName$", Convert.ToString(store["DBUserName"]))
                                                 .Replace("$Password$", Convert.ToString(store["DBPassword"]));

                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> StoreConnectionString = " + StoreConnectionString);

                        #region
                        //if (dtStoreProducts != null)
                        //{
                        //    if (dtStoreProducts.Rows.Count > 0)
                        //    {
                        ProductCount = dtStoreProducts.Rows.Count;
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> before TransferProducts call");
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> store => storename =" + store["storename"]);
                        IsTransfered = DTSSchedulerBL.TransferProducts(UserId, dtStoreProducts, dtStoreLanguages, dtStoreCurrencies, dtInvoiceData, dtFreightStandard_Germany,
                            dtFreightExpress_Germany, dtFreightCountryZones_Germany, dtFreightStandard, dtFreightExpress, dtFreightCountryZones,
                            dtBA_POINTS, dtBA_USERFILE, dtBA_SALESBYGROUP, dtBA_SALESGRANDTOTAL, dtBA_SALESBYPRODUCT, dtBA_BUDGETS, StoreName,
                            StoreConnectionString);
                        Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS -> after TransferProducts call");
                        if (!IsTransfered)
                        {
                            string strBody = "DTS failure for Store <br> StoreName = " + store["storename"].ToString();
                            SendEmail("reena.ravindran@powerweave.com", "", "", "sripal.amballa@powerweave.com", "", "DTS for store failure.", strBody, "", "", true);
                        }

                        //    }
                        //}
                        #endregion
                        #endregion
                        //}
                    }
                    catch (Exception)
                    {
                        string strBody = "DTS failure for Store <br> StoreName = " + store["storename"].ToString();
                        SendEmail("reena.ravindran@powerweave.com", "", "", "sripal.amballa@powerweave.com", "", "DTS for store failure.", strBody, "", "", true);
                        //SendEmail("sripal.amballa@powerweave.com", "", "", "", "", "DTS for store failure.", strBody, "", "", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS Exception");
                Exceptions.WriteExceptionLog(ex);
            }
            Exceptions.WriteInfoLog("DTSSchedulerBL : RunDTS end");
            return IsTransfered;
        }

        public static bool SendEmail(string ToMail, string FromMail, string ReplyEmail, string CCMail, string BCCMail,
       string Subject, string Message, string AttachmentPath, string FileName, bool IsBodyHTML = true)
        {
            bool _Return = false;
            try
            {
                string SMTPHost = "smtp.emailsrvr.com";
                string SMTPUserName = "ba.error@powerweave.com";
                string SMTPPassword = "power02";
                Int32 SMTPPort = 587;
                //FromMail = ConfigurationManager.AppSettings["From_Email"];
                if (String.IsNullOrEmpty(FromMail))
                    FromMail = "no_reply@brand-estore.com";
                if (SMTPUserName.ToString().Length == 0)
                    return _Return;
                using (MailMessage objm = new MailMessage())
                {
                    objm.From = new MailAddress(FromMail);

                    if (!string.IsNullOrEmpty(ToMail))
                        foreach (string item in ToMail.Split(','))
                            objm.To.Add(item);

                    if (!string.IsNullOrEmpty(CCMail))
                        objm.CC.Add(CCMail);

                    if (!string.IsNullOrEmpty(BCCMail))
                        objm.Bcc.Add(BCCMail);

                    if (!string.IsNullOrEmpty(FromMail))
                        objm.ReplyToList.Add(new MailAddress(FromMail));

                    objm.Subject = Subject;
                    objm.Body = HttpUtility.HtmlDecode(Message);
                    objm.IsBodyHtml = IsBodyHTML;

                    if (!string.IsNullOrEmpty(ReplyEmail))
                        objm.ReplyToList.Add(ReplyEmail);

                    string sendEmailsFrom = SMTPUserName;
                    string sendEmailsFromPassword = SMTPPassword;

                    if (!string.IsNullOrEmpty(AttachmentPath))
                    {
                        Attachment attachment = new Attachment(AttachmentPath);
                        attachment.ContentDisposition.FileName = Path.GetFileName(AttachmentPath);
                        objm.Attachments.Add(attachment);
                    }
                    NetworkCredential cred = new NetworkCredential(sendEmailsFrom, sendEmailsFromPassword);

                    SmtpClient mailClient = new SmtpClient(SMTPHost, SMTPPort);
                    mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    mailClient.UseDefaultCredentials = false;
                    mailClient.Timeout = 20000; // Set timeout
                    mailClient.Credentials = cred;
                    mailClient.Send(objm);
                    _Return = true;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return _Return;
        }
    }
}
