﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWGlobalEcomm.DataAccess;

namespace PWGlobalEcomm.BusinessLogic
{
    class InvoiceBL
    {
        public static List<InvoiceBE> GetdisplayUDFSDetails(string UDFS_Id)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("InvoiceId", Convert.ToString(UDFS_Id));
            return InvoiceDA.getCollectionItem(Constants.USP_GetdisplayUDFSDetails, dictionaryInstance, true);
        }
    }
}
