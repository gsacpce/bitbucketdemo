﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;

namespace PWGlobalEcomm.BusinessLogic
{
   public class CatalogueBL
    {
       public static List<CatalogueBE.Division> GetAllDivision()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getAllDivision, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.CataloguesID> GetAllCatalogue()
       {
           try
           {
               return CommonDA.getCollectionItem<CatalogueBE.CataloguesID>(Constants.USP_GetAllCatalogue, null, true);
           }
           catch (Exception ex)
           {
               Exceptions.WriteExceptionLog(ex);
               return null;
           }
       }

       public static List<CatalogueBE.Division> GetAllKey_Groups()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getAllKey_Groups, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Division> GetAllCatalogues()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getAllCatalogues, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Division> GetAllSource_Codes()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getAllSource_Codes, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Division> GetAllWeb_Search_Accounts()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getAllWeb_Search_Accounts, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Division> GetDivisionsByDivID(CatalogueBE.Division objDivision)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DIVISION_ID", Convert.ToString(objDivision.DIVISION_ID));
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getDivisionsByID, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Division> GetWeb_Search_AccountsByDivID(CatalogueBE.Division objDivision)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DIVISION_ID", Convert.ToString(objDivision.DIVISION_ID));
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getWeb_Search_AccountsByDIVISIONID, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Division> GetKey_GroupsByID(CatalogueBE.Key_Groups objKey_Groups)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DIVISION_ID", Convert.ToString(objKey_Groups.DIVISION_ID));
                dictionaryInstance.Add("GROUP_ID", Convert.ToString(objKey_Groups.GROUP_ID));
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getKey_GroupsById, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Division> GetCataloguesById(CatalogueBE.Catalogues objCatalogues)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DIVISION_ID", Convert.ToString(objCatalogues.DIVISION_ID));
                dictionaryInstance.Add("CATALOGUE_ID", Convert.ToString(objCatalogues.CATALOGUE_ID));
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getCataloguesById, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Division> GetSource_CodesById(CatalogueBE.Source_Codes objSource_Codes)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("SOURCE_CODE_ID", Convert.ToString(objSource_Codes.SOURCE_CODE_ID));
                dictionaryInstance.Add("CATALOGUE_ID", Convert.ToString(objSource_Codes.CATALOGUE_ID));
                return CommonDA.getCollectionItem<CatalogueBE.Division>(Constants.USP_getSource_CodesById, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Source_Codes> GetSource_CodesByCatId(CatalogueBE.Source_Codes objSource_Codes)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CATALOGUE_ID", Convert.ToString(objSource_Codes.CATALOGUE_ID));
                return CommonDA.getCollectionItem<CatalogueBE.Source_Codes>(Constants.USP_getSource_CodesByCatId, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

       public static List<CatalogueBE.Web_Search_Accounts> GetWeb_Search_AccountsBySourceId(CatalogueBE.Web_Search_Accounts objWeb_Search_Accounts)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("SOURCE_CODE_ID", Convert.ToString(objWeb_Search_Accounts.SOURCE_CODE_ID));
                return CommonDA.getCollectionItem<CatalogueBE.Web_Search_Accounts>(Constants.USP_getWeb_Search_AccountsBySourceId, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


       public static List<CatalogueBE.Key_Groups> GetKey_GroupsByDivId(CatalogueBE.Key_Groups objKey_Groups)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DIVISION_ID", Convert.ToString(objKey_Groups.DIVISION_ID));
                return CommonDA.getCollectionItem<CatalogueBE.Key_Groups>(Constants.USP_getKey_GroupsByDivId, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
       public static List<CatalogueBE.Web_Search_Accounts> GetWeb_SearchDatabyCustomerId(CatalogueBE.Web_Search_Accounts obj_webSearch)
       {
           try
           {
               Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
               dictionaryInstance.Add("CustomerId", Convert.ToString(obj_webSearch.CUSTOMER_ID));
               return CommonDA.getCollectionItem<CatalogueBE.Web_Search_Accounts>(Constants.USP_getWeb_Search_AccountsByCustomerId, dictionaryInstance, false);
           }
           catch (Exception ex)
           {
               Exceptions.WriteExceptionLog(ex);
               return null;
           }
       }

        //USP_GetAllNewCatalogueDetail
       public static CatalogueBE getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            return CatalogueDA.getCollectionItem(spName, param, false);
        }
    }
}
