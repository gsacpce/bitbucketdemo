﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using PWGlobalEcomm.DataAccess;

namespace PWGlobalEcomm.BusinessLogic
{
    public static class ZoneManagementBL
    {
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-16
        /// Scope   : get/update all the Resource data for all the pages
        /// </summary>        
        /// <param name="intCurrencyId"></param>
        /// <returns>returns list of static page objects</returns>
        public static List<CountryBE> ManageResourceData_SAED(DBAction action, DataTable dtResourceData)
        {
            try
            {

                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("ZoneDataTableType", dtResourceData);
                return ZoneManagementDA.getCollectionItem(Constants.USP_ManageZoneManagement, dictionaryInstance, true);


            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
    }
}
