﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;

namespace PWGlobalEcomm.BusinessLogic
{
    public class RoleManagementBL
    {

        // QueryType 
        // Insert - I
        // Update - U
        // Select All - SA
        // Select All Active - SAA
        // Delete - D
        // Role Assign Menu - RAM
        // select all menu - SAM

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all Role List show
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all coupon for search and List show</returns>
        public static List<RoleManagementBE> GetListRole(string sp)
        {
            List<RoleManagementBE> lstRole;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("QueryType", Convert.ToString("SA"));
            try
            {
                lstRole = RoleManagementDA.getCollectionItem(sp, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstRole = null;
            }
            return lstRole;

        }

        public static List<RoleManagementBE> ReadRole(string sp,RoleManagementBE objRole)
        {
            List<RoleManagementBE> lstRole;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("RoleId",Convert.ToString(objRole.RoleID));
            dictionaryInstance.Add("QueryType", Convert.ToString("S"));
            try
            {
                lstRole = RoleManagementDA.getCollectionItem(sp, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstRole = null;
            }
            return lstRole;

        }
        public static List<RoleManagementBE> GetActiveRole(string sp)
        {
            List<RoleManagementBE> lstRole;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("QueryType", Convert.ToString("SAA"));
            try
            {
                lstRole = RoleManagementDA.getCollectionItem(sp, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstRole = null;
            }
            return lstRole;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all Role List show
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all coupon for search and List show</returns>
        public static List<RoleManagementBE> SearchRole(string sp, RoleManagementBE objRoleManagementBE)
        {
            List<RoleManagementBE> lstRole;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("QueryType", Convert.ToString("Search"));
            dictionaryInstance.Add("RoleName", Convert.ToString(objRoleManagementBE.RoleName));

            try
            {
                lstRole = RoleManagementDA.getCollectionItem(sp, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstRole = null;
            }
            return lstRole;
        }


        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all assigned menu
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all assigned menu</returns>
        public static List<RoleManagementBE> GetAssignedMenu(string sp, RoleManagementBE objRoleManagementBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("QueryType", Convert.ToString("RAM"));
            dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
            return RoleManagementDA.getCollectionItem(sp, dictionaryInstance, false);
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : For adding and updating the Role
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>For adding and updating the Role</returns>
        public static int UpdateRolestatus(string spname, RoleManagementBE objRoleManagementBE)
        {
            int res = 0;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            #region Parameters and Values
            dictionaryInstance.Add("QueryType", Convert.ToString("US"));
            dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
            dictionaryInstance.Add("IsActive", objRoleManagementBE.IsActive.ToString());
            #endregion
            try
            {
                res = RoleManagementDA.InsertUpdateRole(spname, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return res;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : Get List of all Menu List show
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>Get List of all coupon for search and List show</returns>
        public static List<RoleManagementBE> GetMenuList(string sp, RoleManagementBE objRoleManagementBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("QueryType", Convert.ToString("SAM"));
            dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
            return RoleManagementDA.getCollectionItem(sp, dictionaryInstance, false);
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : For adding and updating the Role
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>For adding and updating the Role</returns>
        public static int InsertUpdateRoles(string spname, RoleManagementBE objRoleManagementBE, string mode)
        {
            int RoleId = 0;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            #region Parameters and Values
            if (mode == "a")
                dictionaryInstance.Add("QueryType", Convert.ToString("I"));
            else
                dictionaryInstance.Add("QueryType", Convert.ToString("U"));
            dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
            dictionaryInstance.Add("RoleName", objRoleManagementBE.RoleName.ToString());
            dictionaryInstance.Add("IsActive", objRoleManagementBE.IsActive.ToString());
            dictionaryInstance.Add("UserId", objRoleManagementBE.CreatedBy.ToString());
            #endregion
            try
            {
                RoleId = RoleManagementDA.InsertUpdateRole(spname, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return RoleId;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 07 10 2015
        /// Scope   : For adding and updating the Role
        /// List 
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="pagesize"></param>
        /// <param name="pageindex"></param>
        /// <returns>For adding and updating the Role</returns>
        public static bool InsertUpdateMenu(string spname, RoleManagementBE objRoleManagementBE, string mode)
        {
            bool res = false;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            #region Parameters and Values
            if (mode == "a")
                dictionaryInstance.Add("QueryType", Convert.ToString("I"));
            else
                dictionaryInstance.Add("QueryType", Convert.ToString("U"));
            dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
            dictionaryInstance.Add("Isactive", objRoleManagementBE.IsActive.ToString());
            dictionaryInstance.Add("UserId", objRoleManagementBE.CreatedBy.ToString());
            dictionaryInstance.Add("MenuId", objRoleManagementBE.MenuIdCSV.ToString());
            #endregion
            try
            {
                res = RoleManagementDA.Insert(spname, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return res;
        }
    }


    ///// <summary>
    ///// Author  : Vikram Singh
    ///// Date    : 07 10 2015
    ///// Scope   : Get List of all Role List show
    ///// List 
    ///// </summary>
    ///// <param name="Code"></param>
    ///// <param name="pagesize"></param>
    ///// <param name="pageindex"></param>
    ///// <returns>Get List of all coupon for search and List show</returns>
    //public static List<RoleManagementBE> GetListRole(RoleManagementBE objRoleManagementBE)
    //{
    //    List<RoleManagementBE> lstRole;
    //    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    //    dictionaryInstance.Add("Action", Convert.ToString(objRoleManagementBE.Action));
    //    dictionaryInstance.Add("RoleName", Convert.ToString(objRoleManagementBE.RoleName));
    //    try
    //    {
    //        lstRole = RoleManagementDA.getCollectionItem(Constants.usp_ManageRole, dictionaryInstance, false);
    //    }
    //    catch (Exception ex)
    //    {
    //        Exceptions.WriteExceptionLog(ex);
    //        lstRole = null;
    //    }
    //    return lstRole;
    //}

    ////public static List<RoleManagementBE> GetActiveRole(RoleManagementBE objRoleManagementBE)
    ////{
    ////    List<RoleManagementBE> lstRole;
    ////    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    ////    dictionaryInstance.Add("Action", Convert.ToString(objRoleManagementBE.Action));
    ////    try
    ////    {
    ////        lstRole = RoleManagementDA.getCollectionItem(Constants.usp_ManageRole, dictionaryInstance, false);
    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        Exceptions.WriteExceptionLog(ex);
    ////        lstRole = null;
    ////    }
    ////    return lstRole;
    ////}

    /////// <summary>
    /////// Author  : Vikram Singh
    /////// Date    : 07 10 2015
    /////// Scope   : Get List of all Role List show
    /////// List 
    /////// </summary>
    /////// <param name="Code"></param>
    /////// <param name="pagesize"></param>
    /////// <param name="pageindex"></param>
    /////// <returns>Get List of all coupon for search and List show</returns>
    ////public static List<RoleManagementBE> SearchRole( RoleManagementBE objRoleManagementBE)
    ////{
    ////    List<RoleManagementBE> lstRole;
    ////    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    ////    dictionaryInstance.Add("Action", Convert.ToString("Search"));
    ////    dictionaryInstance.Add("RoleName", Convert.ToString(objRoleManagementBE.RoleName));

    ////    try
    ////    {
    ////         lstRole = RoleManagementDA.getCollectionItem(sp, dictionaryInstance, false);
    ////    }
    ////    catch(Exception ex)
    ////    {
    ////        Exceptions.WriteExceptionLog(ex);
    ////        lstRole = null;
    ////    }
    ////    return lstRole;
    ////}


    ///// <summary>
    ///// Author  : Vikram Singh
    ///// Date    : 07 10 2015
    ///// Scope   : Get List of all assigned menu
    ///// List 
    ///// </summary>
    ///// <param name="Code"></param>
    ///// <param name="pagesize"></param>
    ///// <param name="pageindex"></param>
    ///// <returns>Get List of all assigned menu</returns>
    //public static List<RoleManagementBE> GetAssignedMenu(RoleManagementBE objRoleManagementBE)
    //{
    //    try
    //    {
    //        Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    //        dictionaryInstance.Add("Action", Convert.ToString(objRoleManagementBE.Action));
    //        dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
    //        return RoleManagementDA.getCollectionItem(Constants.usp_ManageMenu, dictionaryInstance, false);
    //    }
    //    catch (Exception ex)
    //    {
    //        Exceptions.WriteExceptionLog(ex);
    //        return null;
    //    }
    //}

    /////// <summary>
    /////// Author  : Vikram Singh
    /////// Date    : 07 10 2015
    /////// Scope   : For adding and updating the Role
    /////// List 
    /////// </summary>
    /////// <param name="Code"></param>
    /////// <param name="pagesize"></param>
    /////// <param name="pageindex"></param>
    /////// <returns>For adding and updating the Role</returns>
    ////public static int UpdateRolestatus(string spname, RoleManagementBE objRoleManagementBE)
    ////{
    ////    int res = 0;
    ////    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    ////    #region Parameters and Values
    ////    dictionaryInstance.Add("QueryType", Convert.ToString("US"));
    ////    dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
    ////    dictionaryInstance.Add("IsActive", objRoleManagementBE.IsActive.ToString());
    ////    #endregion
    ////    try
    ////    {
    ////        res = RoleManagementDA.InsertUpdateRole(spname, dictionaryInstance, false);
    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        Exceptions.WriteExceptionLog(ex);
    ////    }
    ////    return res;
    ////}

    ///// <summary>
    ///// Author  : Vikram Singh
    ///// Date    : 07 10 2015
    ///// Scope   : Get List of all Menu List show
    ///// List 
    ///// </summary>
    ///// <param name="Code"></param>
    ///// <param name="pagesize"></param>
    ///// <param name="pageindex"></param>
    ///// <returns>Get List of all coupon for search and List show</returns>
    //public static List<RoleManagementBE> GetMenuList(RoleManagementBE objRoleManagementBE)
    //{
    //    try
    //    {
    //        Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    //        dictionaryInstance.Add("Action", Convert.ToString(objRoleManagementBE.Action));
    //        dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
    //        return RoleManagementDA.getCollectionItem(Constants.usp_ManageMenu, dictionaryInstance, false);
    //    }
    //    catch (Exception ex)
    //    {
    //        Exceptions.WriteExceptionLog(ex);
    //        return null;
    //    }
    //}

    ///// <summary>
    ///// Author  : Vikram Singh
    ///// Date    : 07 10 2015
    ///// Scope   : For adding and updating the Role
    ///// List 
    ///// </summary>
    ///// <param name="Code"></param>
    ///// <param name="pagesize"></param>
    ///// <param name="pageindex"></param>
    ///// <returns>For adding and updating the Role</returns>
    //public static int InsertUpdateRoles(RoleManagementBE objRoleManagementBE)
    //{
    //    try
    //    {
    //        Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    //        #region Parameters and Values
    //        dictionaryInstance.Add("Action", Convert.ToString(objRoleManagementBE.Action));
    //        dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
    //        dictionaryInstance.Add("RoleName", objRoleManagementBE.RoleName.ToString());
    //        dictionaryInstance.Add("IsActive", objRoleManagementBE.IsActive.ToString());
    //        dictionaryInstance.Add("UserId", objRoleManagementBE.CreatedBy.ToString());

    //        Dictionary<string, string> RoleId = new Dictionary<string, string>();
    //        RoleId.Add("return", Convert.ToString(typeof(Int16)));
    //        #endregion

    //        CommonDA.Insert(Constants.usp_ManageRole, dictionaryInstance, ref RoleId, false);

    //        if (Convert.ToInt16(objRoleManagementBE.Action) == 1)  // Insert
    //        {
    //            return Convert.ToInt16(RoleId["return"]);
    //        }
    //        else                                                 // Update
    //        {
    //            return objRoleManagementBE.RoleID;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Exceptions.WriteExceptionLog(ex);
    //        return 0;
    //    }
    //}

    ///// <summary>
    ///// Author  : Vikram Singh
    ///// Date    : 07 10 2015
    ///// Scope   : For adding and updating the Role
    ///// List 
    ///// </summary>
    ///// <param name="Code"></param>
    ///// <param name="pagesize"></param>
    ///// <param name="pageindex"></param>
    ///// <returns>For adding and updating the Role</returns>
    //public static bool InsertUpdateMenu(RoleManagementBE objRoleManagementBE)
    //{
    //    bool res = false;
    //    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
    //    #region Parameters and Values
    //    dictionaryInstance.Add("Action", Convert.ToString(objRoleManagementBE.Action));
    //    dictionaryInstance.Add("RoleId", objRoleManagementBE.RoleID.ToString());
    //    dictionaryInstance.Add("Isactive", objRoleManagementBE.IsActive.ToString());
    //    dictionaryInstance.Add("UserId", objRoleManagementBE.CreatedBy.ToString());
    //    dictionaryInstance.Add("MenuId", objRoleManagementBE.MenuIdCSV.ToString());
    //    #endregion
    //    try
    //    {
    //        res = CommonDA.Insert(Constants.usp_ManageMenu, dictionaryInstance, false);
    //    }
    //    catch (Exception ex)
    //    {
    //        Exceptions.WriteExceptionLog(ex);
    //        return false;
    //    }
    //    return res;
    //}
}
