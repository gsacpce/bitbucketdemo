﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class GiftCertificateBL
    {
        public static int Update(GiftCertificateOrderBE objGiftCertificateOrderBE)
        {
            int i = 0;
            try
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("GiftOrderID", Convert.ToString(objGiftCertificateOrderBE.GiftOrderID));
                dictionary.Add("GiftOASISID", Convert.ToString(objGiftCertificateOrderBE.GiftOASISID));

                GiftCertificateDA.Update(Constants.USP_UpdateGiftCertificate, dictionary, true);
                i = 1;

            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return i;
        }

        public static GiftCertificateOrderBE getItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            GiftCertificateOrderBE GiftCertificateOrderBEInstance = null;
            try
            {
                GiftCertificateOrderBEInstance = GiftCertificateDA.getItem(spName, param, IsStoreConnectionString);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return GiftCertificateOrderBEInstance;
        }

    }
}
