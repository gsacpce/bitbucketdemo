﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    public class StaticPageManagementBL
    {
        public static List<StaticPageManagementBE> GetAllSectionDetails()
        {
            Dictionary<string, string> dictionaryInstance = null;
            return StaticPageManagementDA.getCollectionItem(Constants.USP_GetSectionDetails, dictionaryInstance, true);
        }

        public static List<StaticPageManagementBE> GetAllDetails(string sp, StaticPageManagementBE objBE, string actionType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("Type", actionType);
            dictionaryInstance.Add("PageName", objBE.PageName);
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            dictionaryInstance.Add("ReturnId", "0");
            return StaticPageManagementDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        public static List<StaticPageManagementBE> GetStaticPageContent(string sp, StaticPageManagementBE objBE, string actionType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("Type", actionType);
            dictionaryInstance.Add("PageName", objBE.PageName);
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            dictionaryInstance.Add("staticpageId", Convert.ToString(objBE.StaticPageId));
            dictionaryInstance.Add("ReturnId", "0");
            return StaticPageManagementDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        public static List<StaticPageManagementBE> GetSinglePageDetails(string sp, StaticPageManagementBE objBE, string actionType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("Type", actionType);
            dictionaryInstance.Add("StaticPageId", objBE.StaticPageId.ToString());
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            dictionaryInstance.Add("ReturnId", "0");
            return StaticPageManagementDA.getCollectionItem(sp, dictionaryInstance, true);
            //StaticPageManagementBE currStaticPage = (StaticPageManagementBE)StaticPageManagementDA.fillObject(sp, dictionaryInstance, true);
        }

        public static Int16 ExecutePageDetails(string sp, StaticPageManagementBE objBE, string actionType)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Type", actionType);
                dictionaryInstance.Add("StaticPageId", Convert.ToString(objBE.StaticPageId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
                dictionaryInstance.Add("IsActive", (objBE.IsActive.ToString().ToLower() == "true" ? "1" : "0"));

                StaticPageManagementDA.Insert(sp, dictionaryInstance, ref DictionaryOutParameterInstance, true);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intId;
        }

        public static Int16 CheckPageDetails(string sp, StaticPageManagementBE objBE, string actionType)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Type", actionType);
                dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
                dictionaryInstance.Add("PageName", Convert.ToString(objBE.PageName));

                StaticPageManagementDA.Insert(sp, dictionaryInstance, ref DictionaryOutParameterInstance, true);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intId;
        }

        public static Int16 InsertUpdatePageDetails(string sp, StaticPageManagementBE objBE, string actionType)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
                dictionaryInstance.Add("PageURL", Convert.ToString(objBE.PageURL));
                dictionaryInstance.Add("NewWindow", Convert.ToString(objBE.NewWindow));
                dictionaryInstance.Add("ParentStaticPageId", Convert.ToString(objBE.ParentStaticPageId));
                dictionaryInstance.Add("IsLoginBased", Convert.ToString(objBE.IsLoginBased).ToLower() == "true" ? "1" : "0");
                dictionaryInstance.Add("IsParent", Convert.ToString(objBE.IsParent).ToLower() == "true" ? "1" : "0");
                dictionaryInstance.Add("PageType", Convert.ToString(objBE.PageType));
                dictionaryInstance.Add("IsActive", (objBE.IsActive.ToString().ToLower() == "true" ? "1" : "0"));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(objBE.CreatedBy));
                dictionaryInstance.Add("WebsiteContent", Convert.ToString(objBE.WebsiteContent));
                dictionaryInstance.Add("PageName", Convert.ToString(objBE.PageName));
                dictionaryInstance.Add("PageTitle", Convert.ToString(objBE.PageTitle));
                dictionaryInstance.Add("MetaKeyword", Convert.ToString(objBE.MetaKeyword));
                dictionaryInstance.Add("MetaDescription", Convert.ToString(objBE.MetaDescription));
                dictionaryInstance.Add("StaticPageId", Convert.ToString(objBE.StaticPageId));
                dictionaryInstance.Add("Type", actionType);

                StaticPageManagementDA.Insert(sp, dictionaryInstance, ref DictionaryOutParameterInstance, true);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intId;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-16
        /// Scope   : get/update all the Resource data for all the pages
        /// </summary>        
        /// <param name="intCurrencyId"></param>
        /// <returns>returns list of static page objects</returns>
        public static List<StaticPageManagementBE> ManageResourceData_SAED(DBAction action, DataTable dtResourceData)
        {
            try
            {
                if (action == DBAction.Select)
                {
                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("ActionType", Convert.ToString(action));
                    return StaticPageManagementDA.getCollectionItem(Constants.USP_ManageResourceData_SAED, dictionaryInstance, true);
                }
                else
                {
                    Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                    dictionaryInstance.Add("ResourceData", dtResourceData);
                    return StaticPageManagementDA.getCollectionItem(Constants.USP_ManageResourceData_SAED, dictionaryInstance, true);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 12-08-16
        /// Scope   : get/update all the Resource data for all the pages
        /// </summary>        
        /// <param name="intCurrencyId"></param>
        /// <returns>returns list of product filter objects</returns>
        public static List<StaticPageManagementBE> GetAllResourceData()
        {
            List<StaticPageManagementBE> lstGetAllResourceData = null;
            try
            {               
                if (HttpRuntime.Cache["AllResourceData"] == null)
                {
                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("ActionType", Convert.ToString(DBAction.Select));
                    lstGetAllResourceData = StaticPageManagementDA.getCollectionItem(Constants.USP_ManageResourceData_SAED, dictionaryInstance, true);
                    HttpRuntime.Cache.Insert("AllResourceData", lstGetAllResourceData);
                }
                else
                    lstGetAllResourceData = (List<StaticPageManagementBE>)HttpRuntime.Cache["AllResourceData"];

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
            return lstGetAllResourceData;
        }

        /// <summary>
        /// Author  : Hardik Gohil
        /// Date    : 02/MARCH/2017
        /// Scope   : get all the data for Danskebank Custom UDF's
        /// </summary>
        public static List<CustomUDFRegDataDanske> GetAllCustomUDFRegDataDanske()
        {
            List<CustomUDFRegDataDanske> lstRegData_Danskebank = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();             
                lstRegData_Danskebank = StaticPageManagementDA.getCustomUDFRegDataDanske(Constants.usp_GetRegistrationDataDanske, dictionaryInstance, true);
                return lstRegData_Danskebank;
            }
            catch (Exception ex) { throw; }
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 23-07-15
        /// Scope   : to set the category sequence
        /// insert category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category object after getting category details</returns>
        public static bool SetStaticPageSequence(DataTable dtDisplayOrder, DataTable dtDisplayLocation)
        {
            bool IsSequenceSet = false;
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("DataW", dtDisplayOrder);
                dictionaryInstance.Add("DataV", dtDisplayLocation);
                Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
                SequenceInstance.Add("IsSequenceSet", Convert.ToString(typeof(bool)));
                StaticPageManagementDA.SetSequence(Constants.USP_SetStaticPageSequence, dictionaryInstance, ref SequenceInstance, true);
                IsSequenceSet = Convert.ToBoolean(SequenceInstance["IsSequenceSet"]);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsSequenceSet;
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 22-09-15
        /// Scope   : get/update all the Resource data for all the pages
        /// </summary>        
        /// <param name="intCurrencyId"></param>
        /// <returns>returns list of product filter objects</returns>
        public static List<StaticPageManagementBE> GetStaticPageAllLanguage(Int16 StaticPageId)
        {
            List<StaticPageManagementBE> lstStaticPageAllLanguage = new List<StaticPageManagementBE>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StaticPageId", StaticPageId.ToString());
                lstStaticPageAllLanguage = StaticPageManagementDA.getCollectionItem(Constants.USP_GetStaticPageAllLanguage, dictionaryInstance, true);

            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
            return lstStaticPageAllLanguage;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 03/11/2015
        /// Scope   : get all the Static Pages
        /// </summary>        
        /// <param name="intCurrencyId"></param>
        /// <returns>returns list of all static pages</returns>
        public static List<T> GetAllStaticPages<T>(Int16 LanguageId)
        {
            List<T> lst;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                lst = CommonDA.getCollectionItem<T>(Constants.USP_GetAllStaticPages, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lst = null;
            }
            return lst;
        }
    }
}
