﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    class MichelinSmileSSOBL 
    {       
        public bool AddPGTValue(string sp_Name, string strpgtIou, string strpgtId)
        {
            bool blnAddGuestRecord = false;
            try
            {
                
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("strpgtIou", strpgtIou);
                dictionaryInstance.Add("strpgtId", strpgtId);
                blnAddGuestRecord = MichelinSmileSSODA.AddPGTValue(sp_Name, dictionaryInstance, true);
                return blnAddGuestRecord;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return blnAddGuestRecord;
            }   
        }

        public DataTable GetPGTValue(string sp_Name, string strpgtIou)
        {
            DataTable dttblProductDetails = new DataTable();
            try
            {
                dttblProductDetails = MichelinSmileSSODA.GetPGTValue(sp_Name, strpgtIou, true);
                return dttblProductDetails;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return dttblProductDetails;
            }
         
        }
    }
}
