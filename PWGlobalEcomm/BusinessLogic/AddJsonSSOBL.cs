﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class AddJsonSSOBL
    {
        public static bool InsertAddJsonSSO(AddJsonSSOBE objAddJsonSSOBE)
        {
            bool res = false;
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("jsonemail", Convert.ToString(objAddJsonSSOBE.jsonemail));
                dictParams.Add("jsonuserinfo", objAddJsonSSOBE.jsonuserinfo);
                CommonDA.Insert(Constants.usp_AddJson, dictParams, true);
                res = true;
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return res;
        }
    }
}
