﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PWGlobalEcomm.BusinessLogic
{
    partial class ProductBL
    {
        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 23/07/2015
        /// Scope   : Returns If Product Code Exists
        /// </summary>
        /// <param name="ProductCode"></param>
        /// <returns>bool</returns>
        /// 
        public static bool CheckProductCodeExists(string ProductCode, int ProductId = 0)
        {
            bool success = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductCode", Convert.ToString(ProductCode));
                if (ProductId != 0)
                    dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                success = ProductDA.IsExists(Constants.USP_CheckProductCodeExists, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                success = false;
            }

            return success;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 23/07/2015
        /// Scope   : Returns If Product Code Exists
        /// </summary>
        /// <param name="objProductBE"></param>
        /// <returns></returns>
        public static int AEDProductBasicDetails(ProductBE objProductBE, ref int ProductId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductId", Convert.ToString(objProductBE.ProductId));
                dictionaryInstance.Add("ProductCode", objProductBE.ProductCode);
                dictionaryInstance.Add("ProductVirtualSampleLink", objProductBE.ProductVirtualSampleLink);
                dictionaryInstance.Add("VideoLink", objProductBE.VideoLink);
                dictionaryInstance.Add("IsActive", Convert.ToString(objProductBE.IsActive));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(objProductBE.CreatedBy));
                dictionaryInstance.Add("ProductName", objProductBE.ProductName);
                dictionaryInstance.Add("LanguageId", Convert.ToString(objProductBE.LanguageId));
                dictionaryInstance.Add("ProductDescription", objProductBE.ProductDescription);
                dictionaryInstance.Add("FurtherDescription", objProductBE.FurtherDescription);
                dictionaryInstance.Add("ImageAltTag", objProductBE.ImageAltTag);
                dictionaryInstance.Add("TagAttributes", objProductBE.TagAttributes);
                dictionaryInstance.Add("PageTitle", objProductBE.PageTitle);
                dictionaryInstance.Add("MetaKeyword", objProductBE.MetaKeyword);
                dictionaryInstance.Add("MetaDescription", objProductBE.MetaDescription);
                dictionaryInstance.Add("Action", Convert.ToString(objProductBE.Action));

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("ProductIdMax", typeof(int).ToString());
                ProductDA.Insert(Constants.USP_ProductDetails_AED, dictionaryInstance, ref dictionaryOutputInstance, true);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "ProductIdMax")
                        ProductId = Convert.ToInt32(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductId;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 23/07/2015
        /// Scope   : Assign Categories to Product
        /// </summary>
        /// <param name="ProductCode"></param>
        /// <returns>bool</returns>
        public static bool AssignProductCategories(string RelatedCategoryIds, int ProductId, int CreatedBy, int DefaultCatId)
        {
            bool success = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RelatedCategoryIds", RelatedCategoryIds);
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(CreatedBy));
                dictionaryInstance.Add("DefaultCatId", Convert.ToString(DefaultCatId));
                success = ProductDA.IsExists(Constants.USP_AssignProductToCategories, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                success = false;
            }
            return success;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 23/07/2015
        /// Scope   : To Add,Edit and Delete product price break
        /// </summary>
        /// <param name="objProductPriceBreakBO"></param>
        /// <param name="PriceBreakId"></param>
        /// <returns>PriceBreakId</returns>
        public static int AEDProductPriceBreak(ProductBE objProductBE, ref int PriceBreakId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                if (objProductBE.PropPriceBreaks.Action == 'D')
                {
                    dictionaryInstance.Add("PriceBreakId", Convert.ToString(objProductBE.PropPriceBreaks.PriceBreakId));
                    dictionaryInstance.Add("Action", Convert.ToString(objProductBE.PropPriceBreaks.Action));
                }
                else
                {
                    dictionaryInstance.Add("PriceBreakId", Convert.ToString(objProductBE.PropPriceBreaks.PriceBreakId));
                    dictionaryInstance.Add("Action", Convert.ToString(objProductBE.PropPriceBreaks.Action));
                    dictionaryInstance.Add("ExpirationDate", Convert.ToString(objProductBE.PropPriceBreaks.ExpirationDate));
                    dictionaryInstance.Add("MaximumQuantity", Convert.ToString(objProductBE.PropPriceBreaks.MaximumQuantity));
                    dictionaryInstance.Add("EnableStrikePrice", Convert.ToString(objProductBE.PropPriceBreaks.EnableStrikePrice));
                    dictionaryInstance.Add("IsRegularPrice", Convert.ToString(objProductBE.PropPriceBreaks.IsRegularPrice));
                    dictionaryInstance.Add("IsCallForPrice", Convert.ToString(objProductBE.PropPriceBreaks.IsCallForPrice));
                    dictionaryInstance.Add("IsShowPriceAndEnquiry", Convert.ToString(objProductBE.PropPriceBreaks.IsShowPriceAndEnquiry));
                    dictionaryInstance.Add("LanguageId", Convert.ToString(objProductBE.PropPriceBreaks.LanguageId));
                    dictionaryInstance.Add("PriceBreakName", objProductBE.PropPriceBreaks.PriceBreakName);
                    dictionaryInstance.Add("StrikePriceBreakName", objProductBE.PropPriceBreaks.StrikePriceBreakName);
                    dictionaryInstance.Add("NoteText", objProductBE.PropPriceBreaks.NoteText);
                    dictionaryInstance.Add("QuantityUnit", objProductBE.PropPriceBreaks.QuantityUnit);
                    dictionaryInstance.Add("PriceUnit", objProductBE.PropPriceBreaks.PriceUnit);
                    dictionaryInstance.Add("ProductionTime", objProductBE.PropPriceBreaks.ProductionTime);
                    dictionaryInstance.Add("FOBPoints", objProductBE.PropPriceBreaks.FOBPoints);
                    dictionaryInstance.Add("CreatedBy", Convert.ToString(objProductBE.PropPriceBreaks.CreatedBy));
                    dictionaryInstance.Add("ModifiedBy", Convert.ToString(objProductBE.PropPriceBreaks.ModifiedBy));
                }
                dictionaryInstance.Add("ProductId", Convert.ToString(objProductBE.PropPriceBreaks.ProductId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objProductBE.PropPriceBreaks.CurrencyId));

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("NewPriceBreakId", typeof(int).ToString());
                ProductDA.Insert(Constants.USP_ProductPriceBreak_AED, dictionaryInstance, ref dictionaryOutputInstance, true);
                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "NewPriceBreakId")
                        PriceBreakId = Convert.ToInt32(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return PriceBreakId;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 23/07/2015
        /// Scope   : To Add,Edit product price break details
        /// </summary>
        /// <param name="objProductPriceBreakDetailsBO"></param>
        /// <param name="PriceBreakDetailId"></param>
        /// <returns>PriceBreakDetailId</returns>
        public static int AEProductPriceBreakDetails(ProductBE objProductBE, ref int PriceBreakDetailId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("PriceBreakDetailId", Convert.ToString(objProductBE.PropPriceBreakDetails.PriceBreakDetailId));
                dictionaryInstance.Add("PriceBreakId", Convert.ToString(objProductBE.PropPriceBreakDetails.PriceBreakId));
                dictionaryInstance.Add("BreakFrom", Convert.ToString(objProductBE.PropPriceBreakDetails.BreakFrom));
                dictionaryInstance.Add("BreakTill", Convert.ToString(objProductBE.PropPriceBreakDetails.BreakTill));
                dictionaryInstance.Add("Price", Convert.ToString(objProductBE.PropPriceBreakDetails.Price));
                dictionaryInstance.Add("StrikePrice", Convert.ToString(objProductBE.PropPriceBreakDetails.StrikePrice));
                dictionaryInstance.Add("DiscountCode", Convert.ToString(objProductBE.PropPriceBreakDetails.DiscountCode));
                dictionaryInstance.Add("IsActive", Convert.ToString(objProductBE.PropPriceBreakDetails.IsActive));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(objProductBE.PropPriceBreakDetails.CreatedBy));
                dictionaryInstance.Add("ModifiedBy", Convert.ToString(objProductBE.PropPriceBreakDetails.ModifiedBy));
                dictionaryInstance.Add("Action", Convert.ToString(objProductBE.PropPriceBreakDetails.Action));

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("NewPriceBreakDetailId", typeof(int).ToString());
                ProductDA.Insert(Constants.USP_ProductPriceDetails_AED, dictionaryInstance, ref dictionaryOutputInstance, true);
                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Value != "")
                    {
                        if (item.Key == "NewPriceBreakDetailId")
                            PriceBreakDetailId = Convert.ToInt32(item.Value);
                    }
                    else
                    { PriceBreakDetailId = 0; }
                }
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
            return PriceBreakDetailId;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 29/07/2015
        /// Scope   : To get product details
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>ProductBE</returns>
        public static ProductBE GetProductDetailsAdmin(int ProductId, Int16 LangugaeId, Int16 CurrencyId)
        {
            ProductBE ProductBEInstance = new ProductBE();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LangugaeId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                ProductBEInstance = ProductDA.getProductDetailsAdmin(Constants.USP_GetProductDetailAdmin, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 29/07/2015
        /// Scope   : To get product details
        /// </summary>
        /// <param name="priceBreakId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static void UpdateDefaultPrice(int priceBreakId, int productId, Int16 CurrencyId, ref bool Result)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("PriceBreakId", Convert.ToString(priceBreakId));
                dictionaryInstance.Add("ProductId", Convert.ToString(productId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("Result", typeof(bool).ToString());
                ProductDA.Update(Constants.USP_SetDefaultPrice, dictionaryInstance, ref dictionaryOutputInstance, true);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        Result = Convert.ToBoolean(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 30/07/2015
        /// Scope   : To get product Varinat Type
        /// </summary>
        /// <param name="LangugaeId"></param>
        /// <returns>ProductBE</returns>
        public static ProductBE GetProductVaiantType(Int16 LanguageId)
        {
            ProductBE ProductBEInstance = new ProductBE();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                ProductBEInstance = ProductDA.getProductVariantType(Constants.USP_ProductVariantType, dictionaryInstance, true);
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }

        internal static List<ProductBE> GetSubCatImage(int CategoryId, int SubCategoryId, int SubSubCategoryId, int LanguageId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
                dictionaryInstance.Add("SubCategoryId", Convert.ToString(SubCategoryId));
                dictionaryInstance.Add("SubSubCategoryId", Convert.ToString(SubSubCategoryId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                List<ProductBE> categories = ProductDA.getCollectionItem(Constants.USP_GetSubCatImage, dictionaryInstance, true);
                return categories;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        public static string UpdateVairantProductVariant(ProductBE objProductBE, ref string ImageName)
        {
            string Image = "";
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ActionType", "update");
                dictionaryInstance.Add("ProductVariantTypeId", Convert.ToString(objProductBE.PropProductVariantType.ProductVariantTypeId));
                dictionaryInstance.Add("ProductVariantId", Convert.ToString(objProductBE.PropProductVariants.ProductVariantId));
                dictionaryInstance.Add("ProductVariantTypeName", objProductBE.PropProductVariantType.ProductVariantTypeName);
                dictionaryInstance.Add("ProductId", Convert.ToString(objProductBE.PropProductVariants.ProductId));
                dictionaryInstance.Add("ProductVariantName", objProductBE.PropProductVariants.ProductVariantName);
                dictionaryInstance.Add("ImageName", objProductBE.PropProductVariants.ImageName);
                dictionaryInstance.Add("IsActive", Convert.ToString(objProductBE.PropProductVariants.IsActive));
                dictionaryInstance.Add("IsActiveVariantType", Convert.ToString(objProductBE.PropProductVariantType.IsActive));
                dictionaryInstance.Add("ShowVariant", Convert.ToString(objProductBE.PropProductVariantType.ShowVariant));
                dictionaryInstance.Add("ModifiedBy", Convert.ToString(objProductBE.PropProductVariants.ModifiedBy));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(objProductBE.PropProductVariants.CreatedBy));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objProductBE.PropProductVariants.LanguageId));

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("Result", typeof(string).ToString());
                ProductDA.Update(Constants.USP_ProductVariant_AED, dictionaryInstance, ref dictionaryOutputInstance, true, 100);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        Image = item.Value;
                }
            }
            catch (Exception ex)
            {
                Image = "";
                Exceptions.WriteExceptionLog(ex);
            }
            return Image;
        }

        public static string InsertVairantProductVariant(ProductBE objProductBE, ref string ImageName)
        {
            string Image = "";
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ActionType", "insert");
                dictionaryInstance.Add("ProductVariantTypeId", Convert.ToString(objProductBE.PropProductVariantType.ProductVariantTypeId));
                dictionaryInstance.Add("ProductVariantTypeName", objProductBE.PropProductVariantType.ProductVariantTypeName);
                dictionaryInstance.Add("ProductId", Convert.ToString(objProductBE.PropProductVariants.ProductId));
                dictionaryInstance.Add("ProductVariantName", objProductBE.PropProductVariants.ProductVariantName);
                dictionaryInstance.Add("ImageName", objProductBE.PropProductVariants.ImageName);
                dictionaryInstance.Add("IsActive", Convert.ToString(objProductBE.PropProductVariants.IsActive));
                dictionaryInstance.Add("IsActiveVariantType", Convert.ToString(objProductBE.PropProductVariantType.IsActive));
                dictionaryInstance.Add("ShowVariant", Convert.ToString(objProductBE.PropProductVariantType.ShowVariant));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(objProductBE.PropProductVariants.CreatedBy));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objProductBE.PropProductVariants.LanguageId));

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("Result", typeof(string).ToString());
                ProductDA.Insert(Constants.USP_ProductVariant_AED, dictionaryInstance, ref dictionaryOutputInstance, true, 100);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        Image = item.Value;
                }
            }
            catch (Exception ex)
            {
                Image = "";
                Exceptions.WriteExceptionLog(ex);
            }
            return Image;
        }

        public static ProductBE GetProductVaiants(int ProductId, Int16 LanguageId)
        {
            ProductBE objProductBE = new ProductBE();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                objProductBE = ProductDA.getProductVariant(Constants.USP_GetProductVairants, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
            return objProductBE;
        }

        /// <summary>
        /// To Update product default image name
        /// </summary>
        /// <param name="DefaultImageName"></param>
        /// <param name="ProductId"></param>
        /// <returns></returns>
        public static int UpdateDefaultImage(string DefaultImageName, int ProductId, ref int Result)
        {
            int result = 0;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DefaultImageName", DefaultImageName);
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("Result", typeof(int).ToString());
                ProductDA.Update(Constants.USP_UpdateProductDefaultImage, dictionaryInstance, ref dictionaryOutputInstance, true);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        result = Convert.ToInt32(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return result;
        }

        public static void UpdateShowVariant(int ProductVariantTypeId, bool ShowVariant, ref bool Result)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductVariantTypeId", Convert.ToString(ProductVariantTypeId));
                dictionaryInstance.Add("ShowVariant", Convert.ToString(ShowVariant));
                dictionaryInstance.Add("Action", "USV");

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("Result", typeof(bool).ToString());

                ProductDA.Update(Constants.USP_UpdateProductVariant, dictionaryInstance, ref dictionaryOutputInstance, true);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                    {
                        Result = Convert.ToBoolean(item.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static void DeleteVariantByVariantTypeId(int ProductVariantTypeId, int ProductId, ref bool Result)
        {
            try
            {

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductVariantTypeId", Convert.ToString(ProductVariantTypeId));
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("Action", "DV");

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("Result", typeof(bool).ToString());

                ProductDA.Delete(Constants.USP_UpdateProductVariant, dictionaryInstance, ref dictionaryOutputInstance, true);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                    {
                        Result = Convert.ToBoolean(item.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static void UpdateVariantTypeName(int ProductVariantTypeId, string ProductVariantTypeName, Int16 LanguageId, ref bool Result)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductVariantTypeId", Convert.ToString(ProductVariantTypeId));
                dictionaryInstance.Add("ProductVariantTypeName", Convert.ToString(ProductVariantTypeName));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryInstance.Add("Action", "UVTN");

                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("Result", typeof(bool).ToString());

                ProductDA.Update(Constants.USP_UpdateProductVariant, dictionaryInstance, ref dictionaryOutputInstance, true);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                    {
                        Result = Convert.ToBoolean(item.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// To delete product Vairant 
        /// </summary>
        /// <param name="objProductVariant"></param>
        /// <param name="Result"></param>
        /// <returns></returns>
        public static string DeleteVairant(ProductBE objProductBE, ref string Result)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ActionType", "deletevariant");
                dictionaryInstance.Add("ProductVariantId", Convert.ToString(objProductBE.PropProductVariants.ProductVariantId));
                dictionaryInstance.Add("ProductId", Convert.ToString(objProductBE.PropProductVariants.ProductId));
                Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
                dictionaryOutputInstance.Add("Result", typeof(string).ToString());
                ProductDA.Delete(Constants.USP_ProductVariant_AED, dictionaryInstance, ref dictionaryOutputInstance, true, 100);

                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        Result = item.Value;
                }
            }
            catch (Exception ex)
            {
                Result = "";
                Exceptions.WriteExceptionLog(ex);
            }
            return Result;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 05/08/2015
        /// Scope   : Get Product Id by Name
        /// </summary>
        /// <param name="ProductName"></param>
        /// <param name="LanguageId"></param>
        /// <returns>int</returns>
        public static int GetProductIdByName(string strProductName, Int16 LanguageId)
        {
            int ProductId;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductName", strProductName);
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                ProductId = ProductDA.getIntValue(Constants.USP_GetProductIdByName, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                ProductId = 0;
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductId;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 07/08/2015
        /// Scope   : Get Product Details By Id
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="CurrencyId"></param>
        /// <returns>ProductBE</returns>
        /// /*User Type*/ added UserTypeID
        public static ProductBE GetProductDetails(int ProductId, Int16 LanguageId, Int16 CurrencyId, Int16 UserTypeID, int RelatedProductCount = 0, string RelatedBy = "", int Percentage = 0)
        {
            ProductBE objProductBE;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                if (!string.IsNullOrEmpty(RelatedBy))
                {
                    dictionaryInstance.Add("RelatedProductCount", Convert.ToString(RelatedProductCount));
                    dictionaryInstance.Add("RelatedBy", RelatedBy);
                    dictionaryInstance.Add("Percentage", Convert.ToString(Percentage));
                }
                objProductBE = ProductDA.getProductDetails(Constants.USP_GetProductDetail, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                objProductBE = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return objProductBE;
        }

        ///<summary>
        /// Author  : Vinit Falgunia
        /// Date    : 12/08/2015
        /// Scope   : Insert Recently Viewed Products
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="UserId"></param>
        /// <param name="SessionId"></param>
        /// <returns></returns>
        public static bool AddRecentlyViewedProducts(int ProductId, int UserId, string SessionId)
        {
            bool Result = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("UserId", Convert.ToString(UserId));
                dictionaryInstance.Add("SessionId", SessionId);
                //dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));

                Result = ProductDA.Insert(Constants.USP_AddRecentlyViewedProducts, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Result = false;
                Exceptions.WriteExceptionLog(ex);
            }
            return Result;
        }

        ///<summary>
        /// Author  : Vinit Falgunia
        /// Date    : 12/08/2015
        /// Scope   : Get Recently Viewed Products
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SessionId"></param>
        /// <param name="CurrencyId"></param>
        /// <param name="LanguageId"></param>
        /// <returns>List<RecentlyViewedProducts></returns>
        public static List<ProductBE.RecentlyViewedProducts> GetRecentlyViewedProducts(int UserId, string SessionId, int ProductId, Int16 CurrencyId, Int16 LanguageId, Int16 UserTypeID)
        {
            List<ProductBE.RecentlyViewedProducts> lstGetAllRecentlyViewedProducts = new List<ProductBE.RecentlyViewedProducts>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", Convert.ToString(UserId));
                dictionaryInstance.Add("SessionId", SessionId);
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));/*User Type*/
                lstGetAllRecentlyViewedProducts = ProductDA.getRecentlyViewedProducts(Constants.USP_GetRecentlyViewedProducts, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstGetAllRecentlyViewedProducts = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstGetAllRecentlyViewedProducts;
        }

        ///<summary>
        /// Author  : Farooq Shaikh
        /// Date    : 24/08/2015
        /// Scope   : Get GetProductDetails By Category
        /// </summary>

        /// <param name="CurrencyId"></param>
        /// <param name="LanguageId"></param>
        /// <returns>List<RecentlyViewedProducts></returns>
        /// /*User Type*/ added UserTypeID

        public static List<ProductBE> GetProductDetailsByCategory(short CategoryId, short LanguageId, short CurrencyId, Int16 UserTypeID)
        {
            List<ProductBE> lstProducts = new List<ProductBE>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();

                dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));
                lstProducts = ProductDA.GetProductByCategoryId(Constants.USP_GetProductByCategoryId, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProducts = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProducts;
        }
        public static List<ProductBE> GetProductDetailsByCategory_ProdSeq(short CategoryId, short LanguageId, short CurrencyId)
        {
            List<ProductBE> lstProducts = new List<ProductBE>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();

                dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                lstProducts = ProductDA.GetProductByCategoryId(Constants.USP_GetProductByCategoryId_prodSeq, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProducts = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProducts;
        }


        ///<summary>
        /// Author  : Vikram Singh
        /// Date    : 24/08/2015
        /// Scope   : Get GetProductDetails By Category
        /// </summary>

        /// <param name="CurrencyId"></param>
        /// <param name="LanguageId"></param>
        /// <returns>List<RecentlyViewedProducts></returns>

        public static List<ProductBE> GetTranslatedProductByCategory(int CategoryId, int LanguageId)
        {
            List<ProductBE> lstProducts = new List<ProductBE>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();

                dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                lstProducts = ProductDA.GetProductByCategoryId(Constants.USP_GetTranslatedProductByCategory, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProducts = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProducts;
        }

        ///<summary>
        /// Author  : Farooq Shaikh
        /// Date    : 25/08/2015
        /// Scope   : Get GetProductDetails By Category
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SessionId"></param>
        /// <param name="CurrencyId"></param>
        /// <param name="LanguageId"></param>
        /// <returns>List<ProductBE></returns>        
        public static List<ProductBE> GetAllProducts(short LanguageId, short CurrencyId, short PageNo)
        {
            List<ProductBE> lstProducts = new List<ProductBE>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryInstance.Add("PageNo", Convert.ToString(PageNo));

                lstProducts = ProductDA.GetAllProducts(Constants.USP_GetAllProducts, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProducts = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProducts;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 27/08/2015
        /// Scope   : To get compare products details
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>ProductBE</returns>
        public static List<ProductBE> GetCompareProductsDetail(string strProductIds, Int16 intLangugaeId, Int16 intCurrencyId)
        {
            List<ProductBE> ProductBEInstance = new List<ProductBE>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductIds", Convert.ToString(strProductIds));
                dictionaryInstance.Add("LanguageId", Convert.ToString(intLangugaeId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(intCurrencyId));
                ProductBEInstance = ProductDA.GetCompareProductsDetail(Constants.USP_GetCompareProductsDetail, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductBEInstance;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 1/09/2015
        /// Scope   : To get all products of all languages and currency
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>ProductBE</returns>
        public static List<object> GetAllProductsForLucene()
        {
            List<object> ProductSearchBEInstance = new List<object>();
            try
            {
                ProductSearchBEInstance = CommonDA.getCollectionItem(Constants.USP_GetAllProductsForLucene, null, true, Constants.Entity_ProductSearchBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return ProductSearchBEInstance;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 04/09/2015
        /// Scope   : To get all productSKUs
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>ProductBE</returns>
        public static List<ProductBE.ProductSKU> GetAllProductSKU(int ProductId)
        {
            List<ProductBE.ProductSKU> lstProductSKU = new List<ProductBE.ProductSKU>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            List<object> lst = new List<object>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                lst = CommonDA.getCollectionItem(Constants.USP_GetAllProductSKU, dictionaryInstance, true, Constants.Entity_ProductSKU);
                lstProductSKU = lst.Cast<ProductBE.ProductSKU>().ToList();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            finally
            {
                lst = null;
            }
            return lstProductSKU;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 04/09/2015
        /// Scope   : To Add,Edit and Delete product SKUs
        /// </summary>
        /// <param name="objProductSKU"></param>
        /// <param name="Action"></param>
        /// <returns>int</returns>
        public static bool AEDProductSKU(ProductBE.ProductSKU objProductSKU, char Action)
        {
            bool result;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ProductSKUId", Convert.ToString(objProductSKU.ProductSKUId));
                dictionaryInstance.Add("ProductId", Convert.ToString(objProductSKU.ProductId));
                dictionaryInstance.Add("SKU", objProductSKU.SKU);
                dictionaryInstance.Add("SKUName", objProductSKU.SKUName);
                dictionaryInstance.Add("VatCode", objProductSKU.VatCode);
                dictionaryInstance.Add("DimensionalWeight", Convert.ToString(objProductSKU.DimensionalWeight));
                dictionaryInstance.Add("MinimumOrderQuantity", Convert.ToString(objProductSKU.MinimumOrderQuantity));
                dictionaryInstance.Add("MaximumOrderQuantity", Convert.ToString(objProductSKU.MaximumOrderQuantity));
                dictionaryInstance.Add("Action", Convert.ToString(Action));
                result = ProductDA.Insert(Constants.USP_ProductSKU_AED, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }
            return result;
        }

        /// Author  : Vinit Falgunia
        /// Date    : 16/09/2015
        /// Scope   : To Get All Product Attributes by Type
        /// </summary>
        /// <param name="objProductSKU"></param>
        /// <param name="Action"></param>
        /// <returns>List<typeparam name="T"></typeparam></returns>
        public static List<T> GetAllProductAttributesByType<T>(ProductAttributeType AttributeType, Int16 LanguageId)
        {
            //T objet = default(T);
            //Type objType = typeof(T);
            List<object> lst = new List<object>();
            List<T> objList = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));

            if (AttributeType == ProductAttributeType.Size)
            {
                dictionaryInstance.Add("AttributeType", Convert.ToString(AttributeType));
                lst = CommonDA.getCollectionItem(Constants.USP_GetProductAttributes, dictionaryInstance, true, Constants.Entity_ProductSize);
                objList = lst.Cast<T>().ToList();
            }

            if (AttributeType == ProductAttributeType.Element)
            {
                dictionaryInstance.Add("AttributeType", Convert.ToString(AttributeType));
                lst = CommonDA.getCollectionItem(Constants.USP_GetProductAttributes, dictionaryInstance, true, Constants.Entity_ProductElements);
                objList = lst.Cast<T>().ToList();
            }

            if (AttributeType == ProductAttributeType.UOM)
            {
                dictionaryInstance.Add("AttributeType", Convert.ToString(AttributeType));
                lst = CommonDA.getCollectionItem(Constants.USP_GetProductAttributes, dictionaryInstance, true, Constants.Entity_ProductUOM);
                objList = lst.Cast<T>().ToList();
            }

            return objList;
        }

        /// Author : Vikram Singh
        /// Date : 16/09/2015
        ///  Scope   : To Get Product Attributes 
        public static List<ProductBE.ProductAttributes> GetListProductAttribute(string sp, Dictionary<string, string> dictionaryInstance)
        {
            return ProductDA.getAttributeCollectionItem(sp, dictionaryInstance, true);
        }

        /// Author : Vikram Singh
        /// Date : 16/09/2015
        ///  Scope   : To Get Truncate Attributes 
        public bool TruncateProductAttribute(string sp)
        {
            return ProductDA.TruncateProductAttribute(sp, true);
        }

        /// Author : Vikram Singh
        /// Date : 18/09/2015
        ///  Scope   : To Add Product Attributes 
        public bool BulkInsertProductAttribute(DataTable dt)
        {
            return ProductDA.BulkInsertProductAttribute(dt, true);
        }

        /// Author : Vikram Singh
        /// Date : 18/09/2015
        ///  Scope   : To Add Product Attributes 
        ///  
        public static List<ProductBE> GetProductNameAllLanguages(int productId)
        {
            List<ProductBE> lstProductNameAllLang = new List<ProductBE>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(productId));
                lstProductNameAllLang = CommonDA.getCollectionItem(Constants.USP_GetProductIdNameAllLanguage, dictionaryInstance, true, Constants.Entity_ProductBE).Cast<ProductBE>().ToList();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
            return lstProductNameAllLang;
        }

        /// Author : Vikram Singh
        /// Date : 18/09/2015
        ///  Scope   : To Save Product Attributes 
        public static int SaveUpdateProductAttribute(DataSet ds)
        {
            return ProductDA.SaveUpdateProductAttribute(ds, true);
        }

        /// Author : Vikram Singh
        /// Date : 18/09/2015
        ///  Scope   : To Save Product Attributes 
        public static DataTable GettableRowProductAttribute()
        {
            return ProductDA.GettableRowProductAttribute(Constants.USP_GetTbl_ProductAttributeManagement, true);
        }

        /// Author : Vikram Singh
        /// Date : 18/11/2015
        ///  Scope   : Get Product Translation Data
        public static DataTable GetProductTranslationData(string Param)
        {
            return ProductDA.GetDataByCSV(Constants.USP_GetProductDataTranslation, Param, true);
        }

        /// Author : Vikram Singh
        /// Date : 18/11/2015
        ///  Scope   : Store Product Import History
        public static bool InsertProductImportHistory(ProductBE.ProductImportHistory objProductImportHistory)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("FailedRecords", objProductImportHistory.FailedRecords.ToString());
                dictionaryInstance.Add("uploadFilePath", objProductImportHistory.UploadedFileName.ToString());
                dictionaryInstance.Add("ErrorFilePath", objProductImportHistory.ErrorFileName.ToString());
                dictionaryInstance.Add("Createdby", objProductImportHistory.CreatedBy.ToString());
                return ProductDA.Insert(Constants.USP_InsertProductImportHistory, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// Author : Vikram Singh
        /// Date : 18/09/2015
        ///  Scope   : To Upload Product in Database

        public static bool UpdateProductTranslation()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return ProductDA.Update(Constants.USP_UpdateProductTranslation, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        ///  Get UnAssigned Products for related Product by ProductId and CategoryId
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        public static List<ProductBE> GetUnMatchedProductFromImport()
        {
            List<ProductBE> lstProductBE = new List<ProductBE>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                lstProductBE = ProductDA.getCollectionItem(Constants.USP_GetUnMatchedProductForImport, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                lstProductBE = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProductBE;
        }
        public static List<ProductBE.ProductImportHistory> GetProductImportHistory()
        {
            List<ProductBE.ProductImportHistory> lstProductBE = new List<ProductBE.ProductImportHistory>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                lstProductBE = ProductDA.getImportHistoryCollectionItem(Constants.USP_GetProductImportHistory, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProductBE = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProductBE;
        }

        /// Author : Vikram Singh
        /// Date : 18/09/2015
        ///  Scope   : To Upload Product in Database
        public static bool BulkImportExcel(DataTable dt, string TableName)
        {
            return ProductDA.BulkInsert(dt, true, TableName);
        }

        /// <summary>
        ///  Get UnAssigned Products for related Product by ProductId and CategoryId
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        public static List<ProductBE> GetUnAssignedRelatedProducts(int ProductId, Int16 CategoryId)
        {
            List<ProductBE> lstProductBE = new List<ProductBE>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("ListType", "UA");
                dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(1));//1 is for english Language
                lstProductBE = ProductDA.getCollectionItem(Constants.USP_ManageRelatedProducts, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProductBE = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProductBE;
        }

        /// <summary>
        /// Get Assigned related Products by Productid
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns></returns>
        public static List<ProductBE> GetAssignedRelatedProducts(int ProductId)
        {
            List<ProductBE> lstProductBE = new List<ProductBE>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("ListType", "A");
                dictionaryInstance.Add("LanguageId", Convert.ToString(1));//1 is for english Language
                lstProductBE = ProductDA.getCollectionItem(Constants.USP_ManageRelatedProducts, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProductBE = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProductBE;
        }

        /// <summary>
        /// Insert Related Product Details into database 
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="MapProductId"></param>
        /// <param name="UserId"></param>
        /// <param name="Result"></param>
        /// <returns></returns>
        public static Int16 InsertRelatedProductDetails(int productId, string rProductIds, int userId)
        {
            //*************************************************************************
            // Purpose : It will insert the data into flyer track
            // Inputs  : FlyersTrackBO objFlyersTrackBO
            // Return  : int
            //*************************************************************************
            Int16 result = 0;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(productId));
                dictionaryInstance.Add("ListType", "ARP");
                dictionaryInstance.Add("ProductIds", rProductIds);
                dictionaryInstance.Add("CreatedBy", Convert.ToString(userId));
                dictionaryOutputInstance.Add("Result", typeof(Int16).ToString());
                ProductDA.Insert(Constants.USP_ManageRelatedProducts, dictionaryInstance, ref dictionaryOutputInstance, true);
                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        result = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                result = 0;
                Exceptions.WriteExceptionLog(ex);
            }
            return result;
        }

        public static Int16 InsertShoppingCartDetails(string sp, Dictionary<string, string> ShoppingCartDetails, bool IsStoreConnectionString)
        {
            Int16 intShopId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());
                ProductDA.Insert(sp, ShoppingCartDetails, ref DictionaryOutParameterInstance, IsStoreConnectionString);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intShopId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return intShopId;
        }

        public static bool DeleteProductPriceData(int ProductId, Int16 CurrencyId)
        {
            bool result = false;
            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
            try
            {
                DictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                DictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                result = ProductDA.Delete(Constants.USP_DeleteProductPriceData, DictionaryInstance, true);
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.WriteExceptionLog(ex);
            }
            return result;
        }

        public static List<T> GetAllProductsData<T>(Int16 LanguageId)
        {
            List<T> lstObject = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                lstObject = CommonDA.getCollectionItem<T>(Constants.USP_GetAllProductsData, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return lstObject;
        }

        /// <summary>
        /// Get Assigned Product Color Variant Mapping by Productid
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>List<ProductBE></returns>
        public static List<ProductBE> GetAssignedProductColorVariantMapping(int ProductId)
        {
            List<ProductBE> lstProductBE = new List<ProductBE>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("ListType", "A");
                dictionaryInstance.Add("LanguageId", Convert.ToString(1));//1 is for english Language
                lstProductBE = ProductDA.getCollectionItem(Constants.USP_ManageProductColorVariantMapping, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProductBE = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProductBE;
        }

        /// <summary>
        ///  Get UnAssigned Products for Product Color Variant Mapping by ProductId
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>List<ProductBE></returns>
        public static List<ProductBE> GetUnAssignedProductColorVariantMapping(int ProductId)
        {
            List<ProductBE> lstProductBE = new List<ProductBE>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("ListType", "UA");
                dictionaryInstance.Add("LanguageId", Convert.ToString(1));//1 is for english Language
                lstProductBE = ProductDA.getCollectionItem(Constants.USP_ManageProductColorVariantMapping, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                lstProductBE = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return lstProductBE;
        }

        /// <summary>
        /// Insert Product Color Variant Mapping into database 
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="MapProductId"></param>
        /// <param name="UserId"></param>
        /// <param name="Result"></param>
        /// <returns>int16</returns>
        public static Int16 InsertProductColorVariantMapping(int productId, string rProductIds, string ProductColorGroupName, int userId)
        {
            //*************************************************************************
            // Purpose : It will insert the data into flyer track
            // Inputs  : FlyersTrackBO objFlyersTrackBO
            // Return  : int
            //*************************************************************************
            Int16 result = 0;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(productId));
                dictionaryInstance.Add("ListType", "ARP");
                dictionaryInstance.Add("ProductIds", rProductIds);
                dictionaryInstance.Add("ProductColorGroupName", ProductColorGroupName);
                dictionaryInstance.Add("CreatedBy", Convert.ToString(userId));
                dictionaryOutputInstance.Add("Result", typeof(Int16).ToString());
                ProductDA.Insert(Constants.USP_ManageProductColorVariantMapping, dictionaryInstance, ref dictionaryOutputInstance, true);
                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        result = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                result = 0;
                Exceptions.WriteExceptionLog(ex);
            }
            return result;
        }

        public static Int16 UpdateProductColorGroupName(int ProductId, string ProductColorGroupName, Int16 LanguageId)
        {
            Int16 result = 0;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("ListType", "E");
                dictionaryInstance.Add("ProductColorGroupName", ProductColorGroupName);
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryOutputInstance.Add("Result", typeof(Int16).ToString());
                ProductDA.Insert(Constants.USP_ManageProductColorVariantMapping, dictionaryInstance, ref dictionaryOutputInstance, true);
                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        result = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return result;
        }

        public static string GetProductColorGroupName(int ProductId, Int16 LanguageId)
        {
            string Result;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                Result = ProductDA.getStringValue(Constants.USP_GetProductColorGroupName, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                Result = null;
            }
            return Result;
        }

        public static bool AddProductEnquiry(ProductBE objProductBE)
        {
            bool result = false;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("UserId", Convert.ToString(objProductBE.PropProductEnquiry.UserId));
                dictionaryInstance.Add("ProductId", Convert.ToString(objProductBE.PropProductEnquiry.ProductId));
                dictionaryInstance.Add("EmailAddress", objProductBE.PropProductEnquiry.EmailAddress);
                dictionaryInstance.Add("QuantityRequired", objProductBE.PropProductEnquiry.QuantityRequired);
                DateTime dt = objProductBE.PropProductEnquiry.DeliveryDateRequired;
                string DeliveryDate = dt.ToString("yyyy-MM-dd");
                dictionaryInstance.Add("DeliveryDateRequired", DeliveryDate);
                dictionaryInstance.Add("Branding", objProductBE.PropProductEnquiry.Branding);
                dictionaryInstance.Add("LogoFileName", objProductBE.PropProductEnquiry.LogoFileName);
                dictionaryInstance.Add("AdditionalInformation", objProductBE.PropProductEnquiry.AdditionalInformation);
                dictionaryInstance.Add("CustomColumn1", objProductBE.PropProductEnquiry.CustomColumn1);
                dictionaryInstance.Add("CustomColumn2", objProductBE.PropProductEnquiry.CustomColumn2);
                dictionaryInstance.Add("CustomColumn3", objProductBE.PropProductEnquiry.CustomColumn3);
                dictionaryInstance.Add("CustomColumn4", objProductBE.PropProductEnquiry.CustomColumn4);
                dictionaryInstance.Add("CustomColumn5", objProductBE.PropProductEnquiry.CustomColumn5);
                result = ProductDA.Insert(Constants.USP_InsertProductEnquiry, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return result;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 01/10/2015
        /// Scope   : Returns List of Product Enquiry Fields Configuration
        /// </summary>
        /// <param name="LanguageId"></param>
        /// <returns><list<T></returns>
        /// 
        public static List<T> GetProductEnquiryFieldsConfiguration<T>(Int16 LanguageId)
        {
            List<T> objList = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                objList = CommonDA.getCollectionItem<T>(Constants.USP_GetProductEnquiryFieldsConfiguration, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objList = null;
            }
            return objList;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 14/10/2015
        /// Scope   : Returns List of Product List
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="LanguageId"></param>
        /// <returns><list<T></returns>
        /// 
        public static List<T> GetProductList<T>(Int16 LanguageId, Int16 PageIndex, Int16 PageSize, string SearchText = "")
        {
            List<T> objList = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("SearchText", SearchText);
                dictionaryInstance.Add("PageIndex", Convert.ToString(PageIndex));
                dictionaryInstance.Add("PageSize", Convert.ToString(PageSize));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                objList = CommonDA.getCollectionItem<T>(Constants.USP_GetProductList, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objList = null;
            }
            return objList;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 14/10/2015
        /// Scope   : Returns List of Uncategorise Product
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="LanguageId"></param>
        /// <returns><list<T></returns>
        /// 
        public static List<T> GetUncategoriseProducts<T>(Int16 LanguageId, Int16 PageIndex, Int16 PageSize, string SearchText = "")
        {
            List<T> objList = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("SearchText", SearchText);
                objList = CommonDA.getCollectionItem<T>(Constants.USP_GetUncategorizeProducts, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objList = null;
            }

            return objList;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 14/10/2015
        /// Scope   : Returns List of Uncategorise Product
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="LanguageId"></param>
        /// <returns><list<T></returns>
        /// 
        public static List<ProductBE.ProductImage> ProductWithoutImages<T>(Int16 LanguageId, Int16 PageIndex, Int16 PageSize, string SearchText = "")
        {
            List<ProductBE.ProductImage> objList = new List<ProductBE.ProductImage>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("SearchText", SearchText);
                dictionaryInstance.Add("PageIndex", Convert.ToString(PageIndex));
                dictionaryInstance.Add("PageSize", Convert.ToString(PageSize));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                objList = ProductDA.GetProductImages(Constants.USP_GetProductWithoutImages, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objList = null;
            }
            return objList;
        }


        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 15/10/2015
        /// Scope   : Update Product Status
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="LanguageId"></param>
        /// <returns><list<T></returns>
        /// 
        public static bool UpdateProductStatus(ProductBE objProductBE)
        {
            bool result;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(objProductBE.ProductId));
                dictionaryInstance.Add("IsActive", Convert.ToString(objProductBE.IsActive));
                result = ProductDA.Update(Constants.USP_UpdateProductStatus, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 65/10/2015
        /// Scope   : Get Product Attribute Management by Product Id
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>list<T></returns>
        /// 
        public static List<T> GetProductAttributeManagementByProductId<T>(int ProductId)
        {
            List<T> lst = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                lst = CommonDA.getCollectionItem<T>(Constants.USP_GetProductAttributeManagementByProductId, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lst = null;
            }
            return lst;
        }

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 65/10/2015
        /// Scope   : Get Product Color Varinat Mapping by Product Id
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>list<T></returns>
        /// 
        public static List<T> GetProductColorVariantMappingByProductId<T>(int ProductId)
        {
            List<T> lst = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                lst = CommonDA.getCollectionItem<T>(Constants.USP_GetProductColorVariantMappingByProductId, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lst = null;
            }
            return lst;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 65/10/2015
        /// Scope   : Delete Product Translation Before Import
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>list<T></returns>
        /// 
        public static bool DeleteProductTranslation(string SPName, ProductBE objProductBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("ProductLanguageIdCSV", objProductBE.ProductLanguageIdCSV.ToString());
            bool IsDeleted = false;
            try
            {
                IsDeleted = CouponDA.Delete(SPName, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsDeleted;
        }

        public static bool AddProductReview(ProductBE.ReviewRating objReviewRating)
        {
            bool result = false;
            Dictionary<string, string> dictionaryIndtance = new Dictionary<string, string>();
            try
            {
                dictionaryIndtance.Add("EmailId", objReviewRating.EmailId);
                dictionaryIndtance.Add("ProductId", Convert.ToString(objReviewRating.ProductId));
                dictionaryIndtance.Add("Rating", Convert.ToString(objReviewRating.Rating));
                dictionaryIndtance.Add("ReviewTitle", objReviewRating.ReviewTitle);
                dictionaryIndtance.Add("Review", objReviewRating.Review);
                dictionaryIndtance.Add("Action", Convert.ToString(objReviewRating.Action));
                result = ProductDA.Insert(Constants.USP_AddProductReview, dictionaryIndtance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }
            return result;
        }

        public static List<T> GetProductReviewByProductId<T>(int ProductId, Int16 PageIndex, Int16 PageSize)
        {
            List<T> lst = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
                dictionaryInstance.Add("PageIndex", Convert.ToString(PageIndex));
                dictionaryInstance.Add("PageSize", Convert.ToString(PageSize));
                lst = CommonDA.getCollectionItem<T>(Constants.USP_GetProductReviewByProductId, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return lst;
        }

        public static int GetProductCount()
        {
            int count = 0;
            try
            {
                count = ProductDA.getIntValue(Constants.USP_GetProductCount, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                count = 0;
            }
            return count;
        }

        public static bool InsertProductCustomRequest(ProductBE objProductBE)
        {
            bool result = false;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ContactName", objProductBE.PropCustomRequest.ContactName);
                dictionaryInstance.Add("EmailId", objProductBE.PropCustomRequest.EmailId);
                dictionaryInstance.Add("PhoneNumber", objProductBE.PropCustomRequest.PhoneNumber);
                dictionaryInstance.Add("Event", objProductBE.PropCustomRequest.Event);
                dictionaryInstance.Add("TargetAudience", objProductBE.PropCustomRequest.TargetAudience);
                dictionaryInstance.Add("Ideas", objProductBE.PropCustomRequest.Ideas);
                dictionaryInstance.Add("Quantity", objProductBE.PropCustomRequest.Quantity);
                dictionaryInstance.Add("Budget", objProductBE.PropCustomRequest.Budget);
                dictionaryInstance.Add("Colour", objProductBE.PropCustomRequest.Colour);
                dictionaryInstance.Add("Branding", objProductBE.PropCustomRequest.Branding);
                dictionaryInstance.Add("DeliveryAddress", objProductBE.PropCustomRequest.DeliveryAddress);
                dictionaryInstance.Add("DeliveryDate", Convert.ToString(objProductBE.PropCustomRequest.DeliveryDate));
                dictionaryInstance.Add("AdditionalComments", objProductBE.PropCustomRequest.AdditionalComments);
                result = ProductDA.Insert(Constants.USP_InsertProductCustomRequest, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }
            return result;
        }

        public static List<T> GetAllProductReviewAndRating<T>(Int16 LanguageId, Int16 PageIndex, Int16 PageSize)
        {
            List<T> lst = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryInstance.Add("PageIndex", Convert.ToString(PageIndex));
                dictionaryInstance.Add("PageSize", Convert.ToString(PageSize));
                lst = CommonDA.getCollectionItem<T>(Constants.USP_GetAllProductReviewAndRating, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lst = null;
            }
            return lst;
        }

        public static bool UpdateProductReviewStatus(int ReviewRatingId, string Action, int ApprovedBy)
        {
            bool result = false;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ReviewRatingId", Convert.ToString(ReviewRatingId));
                dictionaryInstance.Add("Action", Convert.ToString(Action));
                dictionaryInstance.Add("ApprovedBy", Convert.ToString(ApprovedBy));
                result = ProductDA.Update(Constants.USP_UpdateProductReviewStatus, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }
            return result;
        }

        public static ProductBE.ReviewRating GetProductReviewByReviewId(int ReviewRatingId)
        {
            ProductBE.ReviewRating objReviewRating = new ProductBE.ReviewRating();
            object obj;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ReviewRatingId", Convert.ToString(ReviewRatingId));
                obj = ProductDA.getItem(Constants.USP_GetProductReviewByReviewId, dictionaryInstance, Constants.Entity_ReviewRating, true);
                objReviewRating = (ProductBE.ReviewRating)obj;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objReviewRating = null;
            }
            return objReviewRating;
        }

        public static List<ProductBE> GetRandom(int LanguageId, int CurrencyId, Int16 UserTypeID)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));
                return SectionDA.GetCustomizeSectionProduct(Constants.USP_GetRandomProducts, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Creating the self mapping in the tbl_productcolorvariantmapping
        /// as well as delete the color name from the unmapped products.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="rProductIds"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static Int16 InsertProductColorVariantMappingSelf(int productId, string UnMappedProductIds, int userId, int LanguageId)
        {
            //*************************************************************************
            // Purpose : It will insert the data into flyer track
            // Inputs  : FlyersTrackBO objFlyersTrackBO
            // Return  : int
            //*************************************************************************
            Int16 result = 0;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryOutputInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("productid", Convert.ToString(productId));
                dictionaryInstance.Add("unmappedproductids", UnMappedProductIds);
                dictionaryInstance.Add("createdby", Convert.ToString(userId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                dictionaryOutputInstance.Add("Result", typeof(Int16).ToString());
                ProductDA.Insert(Constants.USP_InsertUpdateProductSelfColorVariantMapping, dictionaryInstance, ref dictionaryOutputInstance, true);
                foreach (KeyValuePair<string, string> item in dictionaryOutputInstance)
                {
                    if (item.Key == "Result")
                        result = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                result = 0;
                Exceptions.WriteExceptionLog(ex);
            }
            return result;
        }

        public static bool UpdateColorVariantmapping(string strProductsIDs, int languageID)
        {
            bool bRes = false;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("ProductId", strProductsIDs);
                dictionaryInstance.Add("LanguageId", Convert.ToString(languageID));

                DataAccessBase.Update(Constants.USP_DeleteGroupName, dictionaryInstance, true);
                bRes = true;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return bRes;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Get MCP Users
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns List of Users </returns>
        public static List<T> GetWishList<T>(ProductBE.ProductWishList objWishList, int UserId)
        {

            //List<UserBE> lstUser;
            List<T> lstWishList = new List<T>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objWishList.Action));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objWishList.LanguageId));
                dictionaryInstance.Add("UserId", Convert.ToString(UserId));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                lstWishList = CommonDA.getCollectionItem<T>(Constants.USP_ManageWishList, dictionaryInstance, ref dictReturn, true);
                return lstWishList;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstWishList = null;
                return lstWishList;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Add and Update WishList
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns newly added User ID and Existing Id in case of edit</returns>
        public static int ManageWishListItem(ProductBE.ProductWishList objWishList, int Userid)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                #region Parameters and Values
                dictionaryInstance.Add("UserId", Convert.ToString(Userid));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objWishList.LanguageId));
                dictionaryInstance.Add("Action", Convert.ToString(objWishList.Action));
                dictionaryInstance.Add("ProductSKUId", Convert.ToString(objWishList.ProductSKUId));
                dictionaryInstance.Add("WishListId", Convert.ToString(objWishList.WishListId));

                Dictionary<string, string> WishListId = new Dictionary<string, string>();
                WishListId.Add("return", Convert.ToString(typeof(Int16)));

                #endregion
                ProductDA.Insert(Constants.USP_ManageWishList, dictionaryInstance, ref WishListId, true);
                if (objWishList.Action == Convert.ToInt16(DBAction.Insert))
                {
                    return Convert.ToInt16(WishListId["return"]);
                }
                else
                {
                    return objWishList.WishListId;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }
        }
    }
}


