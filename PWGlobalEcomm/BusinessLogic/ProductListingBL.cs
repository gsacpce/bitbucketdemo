﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;

namespace PWGlobalEcomm.BusinessLogic
{
    public partial class ProductBL
    {
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-07-16
        /// Scope   : gets products data of a selected category for the product listing page
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="strCategoryName"></param>
        /// <param name="strSubCategoryName"></param>
        /// <returns>returns list of product objects</returns>
        public static List<ProductBE> GetProductData(ProductBE objProductBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(objProductBE.LanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objProductBE.CurrencyId));
                dictionaryInstance.Add("CategoryName", Convert.ToString(objProductBE.CategoryName));
                dictionaryInstance.Add("SubCategoryName", Convert.ToString(objProductBE.SubCategoryName));
                dictionaryInstance.Add("SubSubCategoryName", Convert.ToString(objProductBE.SubSubCategoryName));
                dictionaryInstance.Add("PageNo", Convert.ToString(objProductBE.PageNo));
                dictionaryInstance.Add("PageSize", Convert.ToString(objProductBE.PageSize));
                dictionaryInstance.Add("MinPrice", Convert.ToString(objProductBE.MinPrice));
                dictionaryInstance.Add("MaxPrice", Convert.ToString(objProductBE.MaxPrice));
                dictionaryInstance.Add("Colors", Convert.ToString(objProductBE.Colors));
                dictionaryInstance.Add("SortName", Convert.ToString(objProductBE.SortName));

                dictionaryInstance.Add("CustomFilter1", Convert.ToString(objProductBE.CustomFilter1));
                dictionaryInstance.Add("CustomFilter2", Convert.ToString(objProductBE.CustomFilter2));
                dictionaryInstance.Add("CustomFilter3", Convert.ToString(objProductBE.CustomFilter3));
                dictionaryInstance.Add("CustomFilter4", Convert.ToString(objProductBE.CustomFilter4));
                dictionaryInstance.Add("CustomFilter5", Convert.ToString(objProductBE.CustomFilter5));
                dictionaryInstance.Add("SectionIds", Convert.ToString(objProductBE.SectionIds));
                dictionaryInstance.Add("SectionName", Convert.ToString(objProductBE.SectionName));
                dictionaryInstance.Add("SearchProductIds", Convert.ToString(objProductBE.SearchProductIds));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(objProductBE.UserTypeID));
                return ProductDA.getCollectionItem(Constants.USP_GetProductData, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-07-16
        /// Scope   : gets products data of a selected category for the product listing page
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="strCategoryName"></param>
        /// <param name="strSubCategoryName"></param>
        /// <returns>returns list of product objects</returns>
        public static List<ProductBE> GetProductDataStockCk(ProductBE objProductBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(objProductBE.LanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objProductBE.CurrencyId));
                dictionaryInstance.Add("CategoryName", Convert.ToString(objProductBE.CategoryName));
                dictionaryInstance.Add("SubCategoryName", Convert.ToString(objProductBE.SubCategoryName));
                dictionaryInstance.Add("SubSubCategoryName", Convert.ToString(objProductBE.SubSubCategoryName));
                dictionaryInstance.Add("PageNo", Convert.ToString(objProductBE.PageNo));
                dictionaryInstance.Add("PageSize", Convert.ToString(objProductBE.PageSize));
                dictionaryInstance.Add("MinPrice", Convert.ToString(objProductBE.MinPrice));
                dictionaryInstance.Add("MaxPrice", Convert.ToString(objProductBE.MaxPrice));
                dictionaryInstance.Add("Colors", Convert.ToString(objProductBE.Colors));
                dictionaryInstance.Add("SortName", Convert.ToString(objProductBE.SortName));

                dictionaryInstance.Add("CustomFilter1", Convert.ToString(objProductBE.CustomFilter1));
                dictionaryInstance.Add("CustomFilter2", Convert.ToString(objProductBE.CustomFilter2));
                dictionaryInstance.Add("CustomFilter3", Convert.ToString(objProductBE.CustomFilter3));
                dictionaryInstance.Add("CustomFilter4", Convert.ToString(objProductBE.CustomFilter4));
                dictionaryInstance.Add("CustomFilter5", Convert.ToString(objProductBE.CustomFilter5));
                dictionaryInstance.Add("SectionIds", Convert.ToString(objProductBE.SectionIds));
                dictionaryInstance.Add("SectionName", Convert.ToString(objProductBE.SectionName));
                dictionaryInstance.Add("SearchProductIds", Convert.ToString(objProductBE.SearchProductIds));

                return ProductDA.getCollectionItem(Constants.USP_GetProductDataStockCk, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }



        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-07-16
        /// Scope   : gets products data of a selected category for the product listing page
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="strCategoryName"></param>
        /// <param name="strSubCategoryName"></param>
        /// <returns>returns list of product objects</returns>
        public static List<ProductBE> GetProductDataStock(ProductBE objProductBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(objProductBE.LanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objProductBE.CurrencyId));
                dictionaryInstance.Add("CategoryName", Convert.ToString(objProductBE.CategoryName));
                dictionaryInstance.Add("SubCategoryName", Convert.ToString(objProductBE.SubCategoryName));
                dictionaryInstance.Add("SubSubCategoryName", Convert.ToString(objProductBE.SubSubCategoryName));
                dictionaryInstance.Add("PageNo", Convert.ToString(objProductBE.PageNo));
                dictionaryInstance.Add("PageSize", Convert.ToString(objProductBE.PageSize));
                dictionaryInstance.Add("MinPrice", Convert.ToString(objProductBE.MinPrice));
                dictionaryInstance.Add("MaxPrice", Convert.ToString(objProductBE.MaxPrice));
                dictionaryInstance.Add("Colors", Convert.ToString(objProductBE.Colors));
                dictionaryInstance.Add("SortName", Convert.ToString(objProductBE.SortName));

                dictionaryInstance.Add("CustomFilter1", Convert.ToString(objProductBE.CustomFilter1));
                dictionaryInstance.Add("CustomFilter2", Convert.ToString(objProductBE.CustomFilter2));
                dictionaryInstance.Add("CustomFilter3", Convert.ToString(objProductBE.CustomFilter3));
                dictionaryInstance.Add("CustomFilter4", Convert.ToString(objProductBE.CustomFilter4));
                dictionaryInstance.Add("CustomFilter5", Convert.ToString(objProductBE.CustomFilter5));
                dictionaryInstance.Add("SectionIds", Convert.ToString(objProductBE.SectionIds));
                dictionaryInstance.Add("SectionName", Convert.ToString(objProductBE.SectionName));
                dictionaryInstance.Add("SearchProductIds", Convert.ToString(objProductBE.SearchProductIds));
                return ProductDA.getCollectionItem(Constants.USP_GetProductDataStock, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 28-07-16
        /// Scope   : gets filter data of a selected category for the product listing page
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="strCategoryName"></param>
        /// <param name="strSubCategoryName"></param>
        /// <returns>returns list of category objects</returns>
        /// /*User Type*/ UserTypeID
        public static ProductBE GetProductFilterData(ProductBE objProductBE, Int16 UserTypeID)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(objProductBE.LanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objProductBE.CurrencyId));
                dictionaryInstance.Add("CategoryName", Convert.ToString(objProductBE.CategoryName));
                dictionaryInstance.Add("SubCategoryName", Convert.ToString(objProductBE.SubCategoryName));
                dictionaryInstance.Add("SubSubCategoryName", Convert.ToString(objProductBE.SubSubCategoryName));
                dictionaryInstance.Add("MinPrice", Convert.ToString(objProductBE.MinPrice));
                dictionaryInstance.Add("MaxPrice", Convert.ToString(objProductBE.MaxPrice));
                dictionaryInstance.Add("Colors", Convert.ToString(objProductBE.Colors));

                dictionaryInstance.Add("CustomFilter1", Convert.ToString(objProductBE.CustomFilter1));
                dictionaryInstance.Add("CustomFilter2", Convert.ToString(objProductBE.CustomFilter2));
                dictionaryInstance.Add("CustomFilter3", Convert.ToString(objProductBE.CustomFilter3));
                dictionaryInstance.Add("CustomFilter4", Convert.ToString(objProductBE.CustomFilter4));
                dictionaryInstance.Add("CustomFilter5", Convert.ToString(objProductBE.CustomFilter5));
                dictionaryInstance.Add("SectionIds", Convert.ToString(objProductBE.SectionIds));
                dictionaryInstance.Add("SectionName", Convert.ToString(objProductBE.SectionName));
                dictionaryInstance.Add("SearchProductIds", Convert.ToString(objProductBE.SearchProductIds));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));

                return ProductDA.getFilterCollectionItem(Constants.USP_GetProductFilterData, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 04-08-16
        /// Scope   : gets all the product detail attributes with the setting displayed in the filter
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <returns>returns list of product filter objects</returns>
        public static List<ProductBE> GetProductListingFilterSettings(Int16 intLanguageId, Int16 intCurrencyId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(intLanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(intCurrencyId));
                dictionaryInstance.Add("ActionType", "F");
                return ProductDA.getCollectionItem(Constants.USP_GetProductListingFilterSettings, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 04-08-16
        /// Scope   : save the product attribute filter setting
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns>returns list of product objects</returns>
        public static List<ProductBE> SaveProductListingFilterSettings(string strSetting)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Setting", strSetting);
                dictionaryInstance.Add("ActionType", "U");
                return ProductDA.getCollectionItem(Constants.USP_GetProductListingFilterSettings, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        public static List<T> getProductIDwithVariant<T>(string productid)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("productid", productid);
                                
                return CommonDA.getCollectionItem<T>(Constants.USP_chkProductVariantExists, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


    }
}
