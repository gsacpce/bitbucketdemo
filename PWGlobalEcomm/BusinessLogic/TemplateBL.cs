﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class TemplateBL
    {
        /// <summary>
        /// gets all Template details
        /// </summary>
        /// <returns>returns list of TemplateBE objects</returns>
        public static List<TemplateBE> GetAllTemplateDetails()
        {
            List<TemplateBE> getAllTemplates = null;
            try
            {
                getAllTemplates = TemplateDA.getCollectionItem(Constants.USP_GetAllTemplateDetails, null, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllTemplates;
        }

        /// <summary>
        /// insert template detail
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>insert TemplateBE object</returns>
        public static bool InsertTemplate(Dictionary<string, string> TemplateDetail)
        {
            bool IsInserted = false;
            try
            {
                IsInserted = TemplateDA.Insert(Constants.USP_InsertTemplate, TemplateDetail, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsInserted;
        }

        /// <summary>
        /// update template detail
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>update TemplateBE object</returns>
        public static bool UpdateTemplate(Dictionary<string, string> TemplateDetail)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = TemplateDA.Update(Constants.USP_UpdateTemplate, TemplateDetail, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        /// <summary>
        /// Checks weather template with specified id is present or not
        /// </summary>
        /// <param name="TemplateName"></param>
        /// <returns>returns true or false</returns>
        public static bool IsTemplateExists(string TemplateName)
        {
            bool IsExists = false;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("TemplateName", Convert.ToString(TemplateName));
                IsExists = TemplateDA.IsExists(Constants.USP_IsTemplateExists, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsExists;
        }
    }
}
