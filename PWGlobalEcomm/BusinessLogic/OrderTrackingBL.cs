﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class OrderTrackingBL
    {
        /// <summary>
        /// Author: Faruq Shaikh
        /// Date: 24/07/2015
        /// gets all orders by user id
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>returns list of orders</returns>
        public static List<OrderTrackingManagmentBE> GetOrderTrackingList(int UserId,int LanguagueId,int OrderId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("UserId", Convert.ToString(UserId));
            dictionaryInstance.Add("LanguageId", Convert.ToString(LanguagueId));
            dictionaryInstance.Add("OrderId", Convert.ToString(OrderId));
            List<OrderTrackingManagmentBE> Orders = null;

            var OrdersObj = CommonDA.getCollectionItem(Constants.USP_GetOrderTrackingList, dictionaryInstance, true, "OrderTrackingManagmentBE");
            if (OrdersObj != null)
            { 
                if(OrdersObj.Count > 0)
                {
                    Orders = new List<OrderTrackingManagmentBE>();
                    foreach (OrderTrackingManagmentBE OrderObj in OrdersObj)
                    {
                        Orders.Add(new OrderTrackingManagmentBE()
                        {
                                                                    ProductSKUId = OrderObj.ProductSKUId,
                                                                    ProductName = OrderObj.ProductName,
                                                                    DispatchId = OrderObj.DispatchId,
                                                                    TrackingUrl = OrderObj.TrackingUrl,
                                                                    ConsignmentNo = OrderObj.ConsignmentNo,
                                                                    DeliveredQty = OrderObj.DeliveredQty,
                                                                    ShippingMethod = OrderObj.ShippingMethod,
                                                                    OrderTrackingDetailId = OrderObj.OrderTrackingDetailId,
                                                                    OrderId = OrderObj.OrderId,
                                                                    OrderedBy = OrderObj.OrderedBy,
                        }); ;
                    }

                }
            }
            return Orders;


           
        }
        /// <summary>
        /// Author: Faruq Shaikh
        /// Date: 24/07/2015
        /// gets all orders by user id
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>returns list of orders</returns>
        public static List<OrderTrackingManagmentBE> GetOrdersByEmailId(string EmailId)
        {
            List<OrderTrackingManagmentBE> Orders = null;

            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("EmailId", Convert.ToString(EmailId));
              
                var OrdersObj = CommonDA.getCollectionItem(Constants.USP_GetOrdersByEmailId, dictionaryInstance, true, Constants.Entity_OrderTrackingManagmentBE);
                if (OrdersObj != null)
                {
                    if (OrdersObj.Count > 0)
                    {
                        Orders = new List<OrderTrackingManagmentBE>();
                        foreach (OrderTrackingManagmentBE OrderObj in OrdersObj)
                        {
                            Orders.Add(new OrderTrackingManagmentBE()
                            {
                                OrderId = OrderObj.OrderId,
                                OrderedBy = OrderObj.OrderedBy

                            }); ;
                        }

                    }
                }
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
            
            return Orders;

        }
        /// <summary>
        /// Author: Faruq Shaikh
        /// Date: 24/07/2015
        /// to insert all orders tracking details
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>returns boolean value</returns>
        public static bool InsertOrderTrackingManagement(DataTable dtOrders)
        {
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("OrderTrackingDetails", dtOrders);

                Dictionary<string, string> OrderInsertInstance = new Dictionary<string, string>();
                OrderInsertInstance.Add("IsSaved", Convert.ToString(typeof(bool)));

                CommonDA.SaveBulkData(Constants.USP_InsertOrderTrackingManagement, dictionaryInstance, ref OrderInsertInstance, true);
                return Convert.ToBoolean(Convert.ToString(OrderInsertInstance["IsSaved"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }
       

        //PRAWAL - FUNCTION TO GET ProductSKUId

        public static Int16 GetProductSKUId(string sp, bool IsStoreConnectionString, string ProductCode)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("SKUName", ProductCode);

                UserDA.Insert(sp, dictionary, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

    }
}
