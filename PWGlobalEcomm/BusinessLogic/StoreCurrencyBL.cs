﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class StoreCurrencyBL
    {
        public static List<StoreBE.StoreCurrencyBE> GetStoreCurrencyDetails()
        {
            List<StoreBE.StoreCurrencyBE> GetStoreCurrencyDetails = null;
            try
            {
                //if (HttpRuntime.Cache["LanguageMaster"] == null)
                //{
                bool IsStoreConnectionString = true;
               //GetStoreCurrencyDetails = StoreCurrencyDA.getCollectionItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
                GetStoreCurrencyDetails = StoreDA.getStoreCurrencyItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
                //HttpRuntime.Cache.Insert("LanguageMaster", getAllLanguages, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/LanguageMasterCaching.txt")));
                //}
                //else
                //    getAllLanguages = (List<LanguageBE>)HttpRuntime.Cache["LanguageMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return GetStoreCurrencyDetails;
        }

        /// <summary>
        /// Update Store Currency Detail
        /// </summary>
        /// <param name="StoreLanguageDetail"></param>
        /// <returns>returns bool value</returns>
        public static bool UpdateDefaultStoreCurrency(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString = false)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.usp_UpdateStoreDetails_IsDefault, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

    }
}
