﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;
using Lucene.Net.Analysis;

namespace PWGlobalEcomm.BusinessLogic
{
    public static class ProductSearchBL
    {
        // properties
        public static string _luceneDir =
            Path.Combine(GlobalFunctions.GetPhysicalFolderPath(), @"Search\\lucene_index");

        private static FSDirectory _directoryTemp;

        private static FSDirectory _directory
        {
            get
            {
                if (_directoryTemp == null) _directoryTemp = FSDirectory.Open(new DirectoryInfo(_luceneDir));
                if (IndexWriter.IsLocked(_directoryTemp)) IndexWriter.Unlock(_directoryTemp);
                var lockFilePath = Path.Combine(_luceneDir, "write.lock");
                if (File.Exists(lockFilePath)) File.Delete(lockFilePath);
                return _directoryTemp;
            }
        }

        // search methods
        public static IEnumerable<ProductSearchBE> GetAllIndexRecords()
        {
            // validate search index
            if (!System.IO.Directory.EnumerateFiles(_luceneDir).Any()) return new List<ProductSearchBE>();

            // set up lucene searcher
            var searcher = new IndexSearcher(_directory, false);
            var reader = IndexReader.Open(_directory, false);
            var docs = new List<Document>();
            var term = reader.TermDocs();
            // v 2.9.4: use 'hit.Doc()'
            // v 3.0.3: use 'hit.Doc'
            while (term.Next()) docs.Add(searcher.Doc(term.Doc));
            reader.Dispose();
            searcher.Dispose();
            return _mapLuceneToDataList(docs);
        }

        public static IEnumerable<ProductSearchBE> Search(string input, string fieldName = "")
        {
            if (string.IsNullOrEmpty(input)) return new List<ProductSearchBE>();

            string[] args = input.Split(' ');
            string newInput = string.Empty;

            for (int i = 0; i < args.Length; i++)
            {
                var s = new PorterStemmer();
                if (s.Stem(args[i]) != "-")//added by Sripal if Condition
                {
                    #region Split keyword by hypen Added by anoop
                    string[] innerargs = args[i].Split('-');
                    if (innerargs.Length > 1)
                    {
                        for (int j = 0; j < innerargs.Length; j++)
                        {
                            if (innerargs[j].Length > 1)
                                newInput += s.Stem(innerargs[j]) + "* ";//anoop
                        }
                    }
                    else
                    #endregion
                        newInput += "*" + s.Stem(args[i]) + "* ";//anoop  
                    //newInput += s.Stem(args[i]) + ' ';
                }
            }
            input = newInput.Trim();

            //var terms = input.Trim().Replace("-", " ").Split(' ')
            //    .Where(x => !string.IsNullOrEmpty(x)).Select(x => x.Trim() + "*");
            //input = string.Join(" ", terms);

            return _search(input, fieldName);
        }

        public static IEnumerable<ProductSearchBE> SearchDefault(string input, string fieldName = "")
        {
            return string.IsNullOrEmpty(input) ? new List<ProductSearchBE>() : _search(input, fieldName);
        }

        // main search method
        private static IEnumerable<ProductSearchBE> _search(string searchQuery, string searchField = "")
        {
            // validation
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", ""))) return new List<ProductSearchBE>();
            if (!string.IsNullOrEmpty(searchQuery))
            {
               // searchQuery = searchQuery.TrimStart('*');
                searchQuery = searchQuery.Replace("?", "");
            }

            // set up lucene searcher
            using (var searcher = new IndexSearcher(_directory, false))
            {
                var hits_limit = 1000;

                ISet<string> stopTable;
                stopTable = StopFilter.MakeStopSet(stopWords);
                var analyzer = new StandardAnalyzer(Version.LUCENE_30, stopTable);

                // search by single field
                if (!string.IsNullOrEmpty(searchField))
                {
                    var parser = new QueryParser(Version.LUCENE_30, searchField, analyzer);

                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, hits_limit).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
                // search by multiple fields (ordered by RELEVANCE)
                else
                {
                    var parser = new MultiFieldQueryParser
                        (Version.LUCENE_30, new[] { "ProductName", "ProductCode" }, analyzer);
                    parser.AllowLeadingWildcard = true;
                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, null, hits_limit, Sort.RELEVANCE).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
            }
        }

        private static Query parseQuery(string searchQuery, QueryParser parser)
        {
            Query query;
            try
            {
                query = parser.Parse(searchQuery.Trim());
            }
            catch (ParseException)
            {
                query = parser.Parse(QueryParser.Escape(searchQuery.Trim()));
            }
            return query;
        }

        // map Lucene search index to data
        private static IEnumerable<ProductSearchBE> _mapLuceneToDataList(IEnumerable<Document> hits)
        {
            return hits.Select(_mapLuceneDocumentToData).ToList();
        }

        private static IEnumerable<ProductSearchBE> _mapLuceneToDataList(IEnumerable<ScoreDoc> hits, IndexSearcher searcher)
        {
            // v 2.9.4: use 'hit.doc'
            // v 3.0.3: use 'hit.Doc'
            return hits.Select(hit => _mapLuceneDocumentToData(searcher.Doc(hit.Doc))).ToList();
        }

        private static ProductSearchBE _mapLuceneDocumentToData(Document doc)
        {
            return new ProductSearchBE
            {
                RowNumber = Convert.ToInt32(doc.Get("RowNumber")),
                ProductId = Convert.ToInt32(doc.Get("ProductId")),
                ProductName = doc.Get("ProductName"),
                ProductCode = doc.Get("ProductCode"),
                LanguageId = Convert.ToInt16(doc.Get("LanguageId")),
                CurrencyId = Convert.ToInt16(doc.Get("CurrencyId"))
            };
        }

        // add/update/clear search index data 
        public static void AddUpdateLuceneIndex(ProductSearchBE sampleData)
        {
            AddUpdateLuceneIndex(new List<ProductSearchBE> { sampleData });
        }

        public static void AddUpdateLuceneIndex(IEnumerable<ProductSearchBE> sampleDatas)
        {
            // init lucene
            //var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            ISet<string> stopTable;
            stopTable = StopFilter.MakeStopSet(stopWords);
            var analyzer = new StandardAnalyzer(Version.LUCENE_30, stopTable);

            //var analyzer = new CustomAnalyzer();

            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                writer.SetRAMBufferSizeMB(10);
                //writer.setm
                // add data to lucene search index (replaces older entries if any)
                foreach (var sampleData in sampleDatas) _addToLuceneIndex(sampleData, writer);

                // close handles
                analyzer.Close();
                writer.Dispose();
            }
        }

        public static void ClearLuceneIndexRecord(int record_id)
        {
            // init lucene
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                // remove older index entry
                var searchQuery = new TermQuery(new Term("RowNumber", record_id.ToString()));
                writer.DeleteDocuments(searchQuery);

                // close handles
                analyzer.Close();
                writer.Dispose();
            }
        }

        public static bool ClearLuceneIndex()
        {
            try
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                using (var writer = new IndexWriter(_directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    // remove older index entries
                    writer.DeleteAll();

                    // close handles
                    analyzer.Close();
                    writer.Dispose();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static void Optimize()
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                analyzer.Close();
                writer.Optimize();
                writer.Dispose();
            }
        }

        private static void _addToLuceneIndex(ProductSearchBE sampleData, IndexWriter writer)
        {
            // remove older index entry
            var searchQuery = new TermQuery(new Term("RowNumber", sampleData.RowNumber.ToString()));
            writer.DeleteDocuments(searchQuery);

            // add new index entry
            var doc = new Document();

            // add lucene fields mapped to db fields
            doc.Add(new Field("RowNumber", sampleData.RowNumber.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("ProductId", sampleData.ProductId.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("ProductName", sampleData.ProductName, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("ProductCode", sampleData.ProductCode, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("CurrencyId", sampleData.CurrencyId.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("LanguageId", sampleData.LanguageId.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            // add entry to index
            writer.AddDocument(doc);
        }

        public static bool UpdateLucene()
        {
            bool IsUpdated = false;
            try
            {
                List<object> objProductSearchBE = ProductBL.GetAllProductsForLucene();
                List<ProductSearchBE> lstProductSearchBE = new List<ProductSearchBE>();
                lstProductSearchBE = objProductSearchBE.Cast<ProductSearchBE>().ToList();

                if (lstProductSearchBE != null && lstProductSearchBE.Count > 0)
                {
                    ProductSearchBL.ClearLuceneIndex();
                    ProductSearchBL.AddUpdateLuceneIndex(lstProductSearchBE);
                    IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();
                    searchResults = ProductSearchBL.GetAllIndexRecords();
                    IsUpdated = true;
                }
                else
                    IsUpdated = false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return IsUpdated;
        }

        /// <summary>
        /// A rather short list of stop words that is fine for basic search use.
        /// </summary>
        private static readonly string[] stopWords = new[]{
        "0", "1", "2", "3", "4", "5", "6", "7", "8","9", "000", "$", "£","about", "after", "all", "also", "an", "and","another", "any", "are", "as", "at", "be",
        "because", "been", "before", "being", "between","both", "but", "by", "came", "can", "come","could", "did", "do", "does", "each", "else","for", "from", 
        "get", "got", "has", "had","he", "have", "her", "here", "him", "himself","his", "how","if", "in", "into", "is", "it","its", "just", "like", "make", "many", 
        "me","might", "more", "most", "much", "must", "my","never", "now", "of", "on", "only", "or","other", "our", "out", "over", "re", "said","same", "see", 
        "should", "since", "so", "some","still", "such", "take", "than", "that", "the","their", "them", "then", "there", "these","they", "this", "those","through", 
        "to", "too","under", "up", "use", "very", "want", "was","way", "we", "well", "were", "what", "when","where", "which", "while", "who", "will","with", 
        "would", "you", "your","a", "b", "c", "d", "e", "f", "g", "h", "i","j", "k", "l", "m", "n", "o", "p", "q", "r","s", "t", "u", "v", "w", "x", "y", "z"};
    }
}