﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWGlobalEcomm.DataAccess;
using System.Data;
namespace PWGlobalEcomm.BusinessLogic
{
    public partial class ReportsBL
    {

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 28-10-2015
        /// Scope   : Returns List of abandoned carts
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="LanguageId"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns><list<T></returns>
        public static List<T> GetProductList<T>(Int16 LanguageId, Int16 PageIndex, Int16 PageSize, string SearchText = "")
        {
            List<T> objList = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("SearchText", SearchText);
                dictionaryInstance.Add("PageIndex", Convert.ToString(PageIndex));
                dictionaryInstance.Add("PageSize", Convert.ToString(PageSize));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                objList = CommonDA.getCollectionItem<T>(Constants.USP_GetAbandonedCartList, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objList = null;
            }
            return objList;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 03-11-2015
        /// Scope   : Returns List of Orders
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="LanguageId"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns><list<T></returns>
        public static List<T> GetOrdersList<T>(Int16 LanguageId, Int16 PageIndex, Int16 PageSize, string strFilterType = "", string FromDate = "",
            string ToDate = "", string FilterValue = "", string FromOrderValue = "", string ToOrderValue = "")
        {
            List<T> objList = new List<T>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("PageIndex", Convert.ToString(PageIndex));
                dictionaryInstance.Add("PageSize", Convert.ToString(PageSize));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));

                dictionaryInstance.Add("FilterType", strFilterType);
                dictionaryInstance.Add("FromDate", FromDate);
                dictionaryInstance.Add("ToDate", ToDate);
                dictionaryInstance.Add("FilterValue", FilterValue);
                dictionaryInstance.Add("FromOrderValue", FromOrderValue);
                dictionaryInstance.Add("ToOrderValue", ToOrderValue);
                objList = CommonDA.getCollectionItem<T>(Constants.USP_GetOrdersList, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                objList = null;
            }
            return objList;
        }
        public static DataTable GetOrderDataForExportView()
        {
            return UserDA.GetDataforCustomerViewExcel("usp_GetOrdersForExpoet", true);
        }
    }
}
