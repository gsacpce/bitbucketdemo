﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class UserBL
    {

        //Added By snehal 28 09 2016 - For API
        public static Int16 IsExistUser(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Int16 intExist = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("EmailId", objBE.EmailId);

                UserDA.Insert(sp, dictionary, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intExist = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intExist;

        }

        //Added By snehal 20 12 2016 - IsPunchout User
        public static Int16 IsPunchoutUser(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Int16 intExist = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("EmailId", objBE.EmailId);

                UserDA.Insert(sp, dictionary, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intExist = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intExist;

        }

        //Added By snehal 06 01 2017 - IsTelecash Active On Store
        public static Int16 IsTelecashActiveOnSite(string sp, bool IsStoreConnectionString)
        {
            Int16 intExist = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, null, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intExist = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intExist;

        }

        public static UserBE GetMCPUserDetails(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("EmailId", objBE.EmailId);
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            dictionaryInstance.Add("CurrencyId", Convert.ToString(objBE.CurrencyId));
            return UserDA.getUserDetails(sp, dictionaryInstance, IsStoreConnectionString);
        }


        public static UserBE GetSingleObjectDetails(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CustomField", objBE.EmailId);//handle both condition of email address and custom login field.
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            dictionaryInstance.Add("CurrencyId", Convert.ToString(objBE.CurrencyId));
            if (Convert.ToInt16(objBE.CountryId) > 0)
            {
                dictionaryInstance.Add("countryId", Convert.ToString(objBE.CountryId));
            }
            return UserDA.getUserDetails(sp, dictionaryInstance, IsStoreConnectionString);
        }
        public static UserBE GetSingleObjectDetails(bool IsStoreConnectionString, UserBE objBE, DataSet ds)
        {
            //Dictionary<string, DataTable> dictionaryInstanceTable = new Dictionary<string, DataTable>();
            Dictionary<string, object> dictionaryInstance = new Dictionary<string, object>();
            dictionaryInstance.Add("EmailId", objBE.EmailId);
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            dictionaryInstance.Add("CurrencyId", Convert.ToString(objBE.CurrencyId));
            dictionaryInstance.Add("StoreId", objBE.StoreId);
            dictionaryInstance.Add("UsersTableType", ds.Tables[0]);
            dictionaryInstance.Add("StoreUserMappingTableType", ds.Tables[1]);

            return UserDA.getAdminUserDetails(Constants.USP_ValidateLogin, dictionaryInstance, IsStoreConnectionString);
        }
        public static UserBE GetMCPLoginDetailFromCP(bool IsStoreConnectionString, UserBE objBE)
        {
            //Dictionary<string, DataTable> dictionaryInstanceTable = new Dictionary<string, DataTable>();
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("EmailId", objBE.EmailId);
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            dictionaryInstance.Add("CurrencyId", Convert.ToString(objBE.CurrencyId));
            dictionaryInstance.Add("StoreId", Convert.ToString(objBE.StoreId));
            return UserDA.getUserDetails(Constants.USP_GetMCPLoginDetailsFromCP, dictionaryInstance, IsStoreConnectionString);
        }

        public static Int16 ExecuteAdminLogin(string sp, bool IsStoreConnectionString, UserBE objBE, int wrongAttempts, int waitTime)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("EmailId", objBE.EmailId);
                dictionary.Add("Password", objBE.Password);
                //dictionary.Add("Role", Role);
                dictionary.Add("IPAddress", objBE.IPAddress);
                dictionary.Add("CurrencyId", Convert.ToString(objBE.CurrencyId));
                dictionary.Add("LoginWrongAttempts", Convert.ToString(wrongAttempts));
                dictionary.Add("LoginWaitTime", Convert.ToString(waitTime));
                dictionary.Add("StoreId", Convert.ToString(objBE.StoreId));
                UserDA.Insert(sp, dictionary, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static Int16 ExecuteLoginDetails(string sp, bool IsStoreConnectionString, UserBE objBE, string Role, int wrongAttempts, int waitTime)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("EmailId", objBE.EmailId);
                dictionary.Add("Password", objBE.Password);
                dictionary.Add("Role", Role);
                dictionary.Add("IPAddress", objBE.IPAddress);
                dictionary.Add("CurrencyId", Convert.ToString(objBE.CurrencyId));
                dictionary.Add("LoginWrongAttempts", Convert.ToString(wrongAttempts));
                dictionary.Add("LoginWaitTime", Convert.ToString(waitTime));

                UserDA.Insert(sp, dictionary, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }


        public static Int16 ExecuteCustomFieldLoginDetails(string sp, bool IsStoreConnectionString, UserBE objBE, string Role, int wrongAttempts, int waitTime)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("CustomField", objBE.EmailId);
                dictionary.Add("Password", objBE.Password);
                dictionary.Add("Role", Role);
                dictionary.Add("IPAddress", objBE.IPAddress);
                dictionary.Add("CurrencyId", Convert.ToString(objBE.CurrencyId));
                dictionary.Add("LoginWrongAttempts", Convert.ToString(wrongAttempts));
                dictionary.Add("LoginWaitTime", Convert.ToString(waitTime));

                UserDA.Insert(sp, dictionary, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }


        public static Int16 ExecuteResetDetails(string sp, bool IsStoreConnectionString, UserBE objBE, string actionType)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("UserId", objBE.UserId.ToString());
                dictionary.Add("Password", objBE.Password);
                dictionary.Add("Action", actionType);

                UserDA.Insert(sp, dictionary, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static Int16 ExecuteRegisterDetails(string sp, bool IsStoreConnectionString, UserBE objBE, string roleType)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("EmailId", objBE.EmailId);
                DictionaryInstance.Add("Password", objBE.Password);
                DictionaryInstance.Add("FirstName", objBE.FirstName);
                Exceptions.WriteInfoLog("UserBL method -> Fname = " + objBE.FirstName);
                DictionaryInstance.Add("LastName", objBE.LastName);
                Exceptions.WriteInfoLog("UserBL method -> lname = " + objBE.LastName);
                DictionaryInstance.Add("CurrencyId", objBE.CurrencyId.ToString());
                DictionaryInstance.Add("ContactName", objBE.PredefinedColumn1);
                DictionaryInstance.Add("Company", objBE.PredefinedColumn2);
                DictionaryInstance.Add("AddressLine1", objBE.PredefinedColumn3);
                DictionaryInstance.Add("AddressLine2", objBE.PredefinedColumn4);
                DictionaryInstance.Add("Town", objBE.PredefinedColumn5);
                DictionaryInstance.Add("County", objBE.PredefinedColumn6);
                DictionaryInstance.Add("Postcode", objBE.PredefinedColumn7);
                DictionaryInstance.Add("CountryId", objBE.PredefinedColumn8);
                DictionaryInstance.Add("Phone", objBE.PredefinedColumn9);

                #region "Delivery Address"
                DictionaryInstance.Add("AddressTitle", !string.IsNullOrEmpty(objBE.UserDeliveryAddress[0].AddressTitle) ? objBE.UserDeliveryAddress[0].AddressTitle : "Default");
                DictionaryInstance.Add("DelContactName", objBE.UserDeliveryAddress[0].PreDefinedColumn1);
                DictionaryInstance.Add("DelCompany", objBE.UserDeliveryAddress[0].PreDefinedColumn2);
                DictionaryInstance.Add("DelAddressLine1", objBE.UserDeliveryAddress[0].PreDefinedColumn3);
                DictionaryInstance.Add("DelAddressLine2", objBE.UserDeliveryAddress[0].PreDefinedColumn4);
                DictionaryInstance.Add("DelTown", objBE.UserDeliveryAddress[0].PreDefinedColumn5);
                DictionaryInstance.Add("DelCounty", objBE.UserDeliveryAddress[0].PreDefinedColumn6);
                DictionaryInstance.Add("DelPostcode", objBE.UserDeliveryAddress[0].PreDefinedColumn7);
                DictionaryInstance.Add("DelCountryId", objBE.UserDeliveryAddress[0].PreDefinedColumn8);
                DictionaryInstance.Add("DelPhone", objBE.UserDeliveryAddress[0].PreDefinedColumn9);
                #endregion
                DictionaryInstance.Add("IpAddress", objBE.IPAddress);
                DictionaryInstance.Add("Role", roleType);
                DictionaryInstance.Add("IsGuestUser", Convert.ToString(objBE.IsGuestUser));
                DictionaryInstance.Add("IsMarketing", Convert.ToString(objBE.IsMarketing));
                DictionaryInstance.Add("UserTypeID", Convert.ToString(objBE.UserTypeID));/*User Type*/
                DictionaryInstance.Add("IsPunchoutUser", Convert.ToString(objBE.IsPunchoutUser));/*Punchout User*/
                #region "CustomColumn"
                #region "Custom Column"
                DictionaryInstance.Add("CustomColumn1", objBE.CustomColumn1);
                DictionaryInstance.Add("CustomColumn2", objBE.CustomColumn2);
                DictionaryInstance.Add("CustomColumn3", objBE.CustomColumn3);
                DictionaryInstance.Add("CustomColumn4", objBE.CustomColumn4);
                DictionaryInstance.Add("CustomColumn5", objBE.CustomColumn5);
                DictionaryInstance.Add("CustomColumn6", objBE.CustomColumn6);
                DictionaryInstance.Add("CustomColumn7", objBE.CustomColumn7);
                DictionaryInstance.Add("CustomColumn8", objBE.CustomColumn8);
                DictionaryInstance.Add("CustomColumn9", objBE.CustomColumn9);
                DictionaryInstance.Add("CustomColumn10", objBE.CustomColumn10);
                DictionaryInstance.Add("CustomColumn11", objBE.CustomColumn11);
                DictionaryInstance.Add("CustomColumn12", objBE.CustomColumn12);
                DictionaryInstance.Add("CustomColumn13", objBE.CustomColumn13);
                DictionaryInstance.Add("CustomColumn14", objBE.CustomColumn14);
                DictionaryInstance.Add("CustomColumn15", objBE.CustomColumn15);
                DictionaryInstance.Add("CustomColumn16", objBE.CustomColumn16);
                DictionaryInstance.Add("CustomColumn17", objBE.CustomColumn17);
                DictionaryInstance.Add("CustomColumn18", objBE.CustomColumn18);
                DictionaryInstance.Add("CustomColumn19", objBE.CustomColumn19);
                DictionaryInstance.Add("CustomColumn20", objBE.CustomColumn20);
                DictionaryInstance.Add("CustomColumn21", objBE.CustomColumn21);
                DictionaryInstance.Add("CustomColumn22", objBE.CustomColumn22);
                DictionaryInstance.Add("CustomColumn23", objBE.CustomColumn23);
                DictionaryInstance.Add("CustomColumn24", objBE.CustomColumn24);
                DictionaryInstance.Add("CustomColumn25", objBE.CustomColumn25);
                DictionaryInstance.Add("CustomColumn26", objBE.CustomColumn26);
                DictionaryInstance.Add("CustomColumn27", objBE.CustomColumn27);
                DictionaryInstance.Add("CustomColumn28", objBE.CustomColumn28);
                DictionaryInstance.Add("CustomColumn29", objBE.CustomColumn29);
                DictionaryInstance.Add("CustomColumn30", objBE.CustomColumn30);
                DictionaryInstance.Add("IndeedPoints", Convert.ToString(objBE.Points));
                DictionaryInstance.Add("UserTitle", Convert.ToString(objBE.UserTitle)); 
                #endregion
                #endregion

                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        /// <summary>
        // Moved by sachin on 04-12-2015 from UserRegistrationBL
        // modified by:
        // modified date: 
        /// </summary>
        public static UserRegistrationBE GetUserRegistrationDetails()
        {
            UserRegistrationBE getUserRegistrationBE = null;
            try
            {
                getUserRegistrationBE = UserRegistrationDA.getItem(Constants.USP_GetRegistrationDetails, null, true);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return getUserRegistrationBE;
        }

        public static List<T> GetHeirarchyUsers<T>(Int16 UserHeirarchyId)
        {

            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("UserHeirarchyId", UserHeirarchyId.ToString());

            return CommonDA.getCollectionItem<T>(Constants.USP_HeirarchyWiseUsers, dictionaryInstance, true);
        }
        /// <summary>
        // Added by vikram on 09-11-2015 to add subscription
        // modified by:
        // modified date: 
        /// </summary>
        public static bool AddSubscriptionDetail(string sp, UserBE objBE)
        {
            bool res = false;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("EmailId", objBE.EmailId);

                res = UserDA.Insert(sp, DictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return res;
        }

        public static Int16 ExecuteBASYSDetails(string sp, bool IsStoreConnectionString, Int16 UserId, Int16 CurrencyId, int BASYS_OASIS)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictInstance = new Dictionary<string, string>();
                DictInstance.Add("UserId", Convert.ToString(UserId));
                DictInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
                DictInstance.Add("BASYSCustomerId", Convert.ToString(BASYS_OASIS));

                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static Int16 ExecuteProfileDeliveryAddressDetails(string sp, bool IsStoreConnectionString, UserBE objBE, string actionType)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("DelId", objBE.UserDeliveryAddress[0].DeliveryAddressId.ToString());
                DictionaryInstance.Add("UserId", objBE.UserDeliveryAddress[0].UserId.ToString());
                DictionaryInstance.Add("DelContactName", objBE.UserDeliveryAddress[0].PreDefinedColumn1);
                DictionaryInstance.Add("DelCompany", objBE.UserDeliveryAddress[0].PreDefinedColumn2);
                DictionaryInstance.Add("DelAddressLine1", objBE.UserDeliveryAddress[0].PreDefinedColumn3);
                DictionaryInstance.Add("DelAddressLine2", objBE.UserDeliveryAddress[0].PreDefinedColumn4);
                DictionaryInstance.Add("DelTown", objBE.UserDeliveryAddress[0].PreDefinedColumn5);
                DictionaryInstance.Add("DelCounty", objBE.UserDeliveryAddress[0].PreDefinedColumn6);
                DictionaryInstance.Add("DelPostcode", objBE.UserDeliveryAddress[0].PreDefinedColumn7);
                DictionaryInstance.Add("DelCountryId", objBE.UserDeliveryAddress[0].PreDefinedColumn8);
                DictionaryInstance.Add("DelPhone", objBE.UserDeliveryAddress[0].PreDefinedColumn9);
                DictionaryInstance.Add("Action", actionType);
                DictionaryInstance.Add("IsDefault", objBE.UserDeliveryAddress[0].IsDefault.ToString().ToLower() == "true" ? "true" : "false");
                DictionaryInstance.Add("AddressTitle", objBE.UserDeliveryAddress[0].AddressTitle);

                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static Int16 ExecuteProfileDetails(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("UserId", objBE.UserId.ToString());
                DictionaryInstance.Add("Password", objBE.Password);
                DictionaryInstance.Add("FirstName", objBE.FirstName);
                DictionaryInstance.Add("LastName", objBE.LastName);
                DictionaryInstance.Add("ContactName", objBE.PredefinedColumn1);
                DictionaryInstance.Add("Company", objBE.PredefinedColumn2);
                DictionaryInstance.Add("AddressLine1", objBE.PredefinedColumn3);
                DictionaryInstance.Add("AddressLine2", objBE.PredefinedColumn4);
                DictionaryInstance.Add("Town", objBE.PredefinedColumn5);
                DictionaryInstance.Add("County", objBE.PredefinedColumn6);
                DictionaryInstance.Add("Postcode", objBE.PredefinedColumn7);
                DictionaryInstance.Add("CountryId", objBE.PredefinedColumn8);
                DictionaryInstance.Add("Phone", objBE.PredefinedColumn9);
                DictionaryInstance.Add("IpAddress", objBE.IPAddress);
                DictionaryInstance.Add("IsGuestUser", Convert.ToString(objBE.IsGuestUser));
                DictionaryInstance.Add("IsMarketing", Convert.ToString(objBE.IsMarketing));

                //DictionaryInstance.Add("IpAddress", objBE.IPAddress);
                //DictionaryInstance.Add("Role", roleType);
                //DictionaryInstance.Add("IsGuestUser", Convert.ToString(objBE.IsGuestUser));
                //DictionaryInstance.Add("IsMarketing", Convert.ToString(objBE.IsMarketing));
                DictionaryInstance.Add("UserTypeID", Convert.ToString(objBE.UserTypeID));/*User Type*/
                DictionaryInstance.Add("IsPunchoutUser", Convert.ToString(objBE.IsPunchoutUser));/*Punchout User*/ 
                #region "CustomColumn"

                DictionaryInstance.Add("CustomColumn1", objBE.CustomColumn1);
                DictionaryInstance.Add("CustomColumn2", objBE.CustomColumn2);
                DictionaryInstance.Add("CustomColumn3", objBE.CustomColumn3);
                DictionaryInstance.Add("CustomColumn4", objBE.CustomColumn4);
                DictionaryInstance.Add("CustomColumn5", objBE.CustomColumn5);
                DictionaryInstance.Add("CustomColumn6", objBE.CustomColumn6);
                DictionaryInstance.Add("CustomColumn7", objBE.CustomColumn7);
                DictionaryInstance.Add("CustomColumn8", objBE.CustomColumn8);
                DictionaryInstance.Add("CustomColumn9", objBE.CustomColumn9);
                DictionaryInstance.Add("CustomColumn10", objBE.CustomColumn10);
                DictionaryInstance.Add("CustomColumn11", objBE.CustomColumn11);
                DictionaryInstance.Add("CustomColumn12", objBE.CustomColumn12);
                DictionaryInstance.Add("CustomColumn13", objBE.CustomColumn13);
                DictionaryInstance.Add("CustomColumn14", objBE.CustomColumn14);
                DictionaryInstance.Add("CustomColumn15", objBE.CustomColumn15);
                DictionaryInstance.Add("CustomColumn16", objBE.CustomColumn16);
                DictionaryInstance.Add("CustomColumn17", objBE.CustomColumn17);
                DictionaryInstance.Add("CustomColumn18", objBE.CustomColumn18);
                DictionaryInstance.Add("CustomColumn19", objBE.CustomColumn19);
                DictionaryInstance.Add("CustomColumn20", objBE.CustomColumn20);
                DictionaryInstance.Add("CustomColumn21", objBE.CustomColumn21);
                DictionaryInstance.Add("CustomColumn22", objBE.CustomColumn22);
                DictionaryInstance.Add("CustomColumn23", objBE.CustomColumn23);
                DictionaryInstance.Add("CustomColumn24", objBE.CustomColumn24);
                DictionaryInstance.Add("CustomColumn25", objBE.CustomColumn25);
                DictionaryInstance.Add("CustomColumn26", objBE.CustomColumn26);
                DictionaryInstance.Add("CustomColumn27", objBE.CustomColumn27);
                DictionaryInstance.Add("CustomColumn28", objBE.CustomColumn28);
                DictionaryInstance.Add("CustomColumn29", objBE.CustomColumn29);
                DictionaryInstance.Add("CustomColumn30", objBE.CustomColumn30);
                DictionaryInstance.Add("UserTitle", Convert.ToString(objBE.UserTitle));

                #endregion

                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        /// <summary>
        /// Author      :   SHRIGANESH SINGH
        /// Date        :   15 Feb 2017
        /// Description :   This method only updates the user related data except the Usertype custom fields
        /// Note        :   Please do not make any changes without consulting
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="IsStoreConnectionString"></param>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public static Int16 ExecuteEditProfileDetails(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("UserId", objBE.UserId.ToString());
                DictionaryInstance.Add("FirstName", objBE.FirstName);
                DictionaryInstance.Add("LastName", objBE.LastName);
                DictionaryInstance.Add("ContactName", objBE.PredefinedColumn1);
                DictionaryInstance.Add("Company", objBE.PredefinedColumn2);
                DictionaryInstance.Add("AddressLine1", objBE.PredefinedColumn3);
                DictionaryInstance.Add("AddressLine2", objBE.PredefinedColumn4);
                DictionaryInstance.Add("Town", objBE.PredefinedColumn5);
                DictionaryInstance.Add("County", objBE.PredefinedColumn6);
                DictionaryInstance.Add("Postcode", objBE.PredefinedColumn7);
                DictionaryInstance.Add("CountryId", objBE.PredefinedColumn8);
                DictionaryInstance.Add("Phone", objBE.PredefinedColumn9);
                DictionaryInstance.Add("IsMarketing", Convert.ToString(objBE.IsMarketing));
                DictionaryInstance.Add("UserTypeID", Convert.ToString(objBE.UserTypeID));/*User Type*/
                DictionaryInstance.Add("UserTitle", Convert.ToString(objBE.UserTitle));
               //DictionaryInstance.Add("IsPunchoutUser", Convert.ToString(objBE.IsPunchoutUser));/*Punchout User*/ 

                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static Int16 ExecuteSSOProfileDetails(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("EmailId", objBE.EmailId);
                DictionaryInstance.Add("FirstName", objBE.FirstName);
                DictionaryInstance.Add("LastName", objBE.LastName);
                DictionaryInstance.Add("ContactName", objBE.PredefinedColumn1);
                DictionaryInstance.Add("Company", objBE.PredefinedColumn2);
                DictionaryInstance.Add("AddressLine1", objBE.PredefinedColumn3);
                DictionaryInstance.Add("AddressLine2", objBE.PredefinedColumn4);
                DictionaryInstance.Add("Town", objBE.PredefinedColumn5);
                DictionaryInstance.Add("County", objBE.PredefinedColumn6);
                DictionaryInstance.Add("Postcode", objBE.PredefinedColumn7);
                DictionaryInstance.Add("CountryId", objBE.PredefinedColumn8);
                DictionaryInstance.Add("Phone", objBE.PredefinedColumn9);

                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static Int16 UpdateReferrelUserTypeId(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Int16 intUserTypeId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("EmailId", objBE.EmailId.ToString());
                DictionaryInstance.Add("UserTypeId", objBE.UserTypeID.ToString());

                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnUserId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnUserId")
                        intUserTypeId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intUserTypeId;
        }


        public static UserBE.UserDeliveryAddressBE GetOrderDeliveryAddress(string sp, bool IsStoreConnectionString, string OrderId, string CurrencyId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("OrderId", Convert.ToString(OrderId));
            dictionaryInstance.Add("CurrencyId", Convert.ToString(CurrencyId));
            return UserDA.getOrderAddressDetails(sp, dictionaryInstance, IsStoreConnectionString);
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 16-09-16
        /// Scope   : add the new delivery address in tbl_usersDeliveryAddress table
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns newly added DeliveryAddress ID</returns>
        public static Int16 ExecuteLoginDetails(UserBE objUserBE, DBAction type)
        {
            Int16 intLogId = 0;
            try
            {
                //Input
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("DelId", Convert.ToString(objUserBE.UserDeliveryAddress[0].DeliveryAddressId));
                dictionaryInstance.Add("UserId", Convert.ToString(objUserBE.UserId));
                dictionaryInstance.Add("DelContactName", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn1));
                dictionaryInstance.Add("DelCompany", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn2));
                dictionaryInstance.Add("DelAddressLine1", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn3));
                dictionaryInstance.Add("DelAddressLine2", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn4));
                dictionaryInstance.Add("DelTown", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn5));
                dictionaryInstance.Add("DelCounty", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn6));
                dictionaryInstance.Add("DelPostcode", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn7));
                dictionaryInstance.Add("DelCountryId", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn8));
                dictionaryInstance.Add("DelPhone", Convert.ToString(objUserBE.UserDeliveryAddress[0].PreDefinedColumn9));
                dictionaryInstance.Add("AddressTitle", Convert.ToString(objUserBE.UserDeliveryAddress[0].AddressTitle));
                dictionaryInstance.Add("IsDefault", Convert.ToString(objUserBE.UserDeliveryAddress[0].IsDefault));
                dictionaryInstance.Add("Action", Convert.ToString(type));
                //Output
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(Constants.USP_InsertUpdateDeleteUserDeliveryAddress, dictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        /// <summary>
        /// gets categories listing
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of CategoryBE objects</returns>

        public static List<UserBE> GetUserDetails()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return UserDA.getCollectionItem(Constants.USP_GetUserDetails, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static List<UserBE> GetUserDetailsForExport()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return UserDA.getCollectionItem(Constants.USP_GetUserDetailsForExport, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static List<UserBE> GetUnregisteredUsersDetails()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return UserDA.getCollectionItem(Constants.USP_GetUnRegisteredUserDetails, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static List<UserBE.UserBudgetBE> GetUserBudgetDetails()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return UserDA.getCollectionItemBudget(Constants.USP_GetUsersBudgetDetail, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static bool DeleteBudgetRecord(string Emailid)
        {
            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
            DictionaryInstance.Add("EmailId", Convert.ToString(Emailid));
            return UserDA.Delete(Constants.USP_DeleteBudgetRecord, DictionaryInstance, true);
        }


        public static List<UserBE> GetUserPoints(string searchtext)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("SearchText", searchtext);

                return UserDA.getCollectionItem(Constants.USP_GetUserPointDetails, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static List<UserBE.UserDeliveryAddressBE> GetUserDeliveryAddres(UserBE.UserDeliveryAddressBE objUserBe)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", objUserBe.UserId.ToString());
                dictionaryInstance.Add("DelAddId", objUserBe.DeliveryAddressId.ToString());
                return UserDA.getCollectionItemDelAddress(Constants.USP_BindUserDeliveryAddress, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>	 public static bool UpdatePoints(DataTable dtUpdatePoint)
        /// Author  :   Snehal Jadhav		
        /// Date    :   21 September 2016		
        /// Scope   :   To update Registered users points from the Excel sheet		
        /// </summary>		
        /// <param name="dtUpdatePoint"></param>		
        /// <returns>boolean value</returns>		
        public static bool UpdateRegisteredUsersPoints(DataTable dtUpdatePoint)
        {
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("PointsUpdate", dtUpdatePoint);
                Dictionary<string, string> PointsUpdatedInstance = new Dictionary<string, string>();
                PointsUpdatedInstance.Add("IsPointUpdated", Convert.ToString(typeof(bool)));
                UserDA.UpdatePoints(Constants.USP_UpdateRegisteredUsersPoints, dictionaryInstance, ref PointsUpdatedInstance, true);
                return Convert.ToBoolean(Convert.ToString(PointsUpdatedInstance["IsPointUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }
        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   07 September 2016
        /// Scope   :   To update Un registered users points from the Excel sheet
        /// </summary>
        /// <param name="dtUpdatePoint"></param>
        /// <returns>boolean value</returns>
        public static bool UpdateUnregisteredUsersPoints(DataTable dtUpdatePoint)
        {
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("PointsUpdate", dtUpdatePoint);

                Dictionary<string, string> PointsUpdatedInstance = new Dictionary<string, string>();
                PointsUpdatedInstance.Add("IsPointUpdated", Convert.ToString(typeof(bool)));

                UserDA.UpdatePoints(Constants.USP_UpdateUnregisteredUsersPoints, dictionaryInstance, ref PointsUpdatedInstance, true);
                return Convert.ToBoolean(Convert.ToString(PointsUpdatedInstance["IsPointUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        public static bool UpdateUserBudget(DataTable dtUpdateBudget)
        {
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("BudgetUpdate", dtUpdateBudget);

                Dictionary<string, string> BudgetUpdatedInstance = new Dictionary<string, string>();
                BudgetUpdatedInstance.Add("IsBudgetUpdated", Convert.ToString(typeof(bool)));

                UserDA.UpdatePoints(Constants.USP_UpdateUserBudgetDetail, dictionaryInstance, ref BudgetUpdatedInstance, true);
                return Convert.ToBoolean(Convert.ToString(BudgetUpdatedInstance["IsBudgetUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        public static bool UpdatePoints(DataTable dtUpdatePoint)
        {
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("PointsUpdate", dtUpdatePoint);

                Dictionary<string, string> PointsUpdatedInstance = new Dictionary<string, string>();
                PointsUpdatedInstance.Add("IsPointUpdated", Convert.ToString(typeof(bool)));

                UserDA.UpdatePoints(Constants.USP_UpdateUserPoints, dictionaryInstance, ref PointsUpdatedInstance, true);
                return Convert.ToBoolean(Convert.ToString(PointsUpdatedInstance["IsPointUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        public static bool UpdateImportDataToOriginalTable()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();


                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("IsPointUpdated", Convert.ToString(typeof(bool)));

                UserDA.Update(Constants.USP_UpdateImportDataToOriginalTable, dictionaryInstance, ref dictReturn, true);

                return Convert.ToBoolean(Convert.ToString(dictReturn["IsPointUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        public static int GetCustomerCount()
        {
            int count = 0;
            try
            {
                count = UserDA.getIntValue(Constants.USP_GetCustomerCount, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                count = 0;
            }
            return count;
        }

        #region User Hierarchy's Method

        public static List<UserBE.UserHierarchyBE> GetUserHierarchies()
        {
            try
            {
                return UserDA.getUserHierarchies(Constants.USP_GetUserHierarchies, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static bool InsertUserHierarchy(UserBE.UserHierarchyBE objUserHierarchyBE)
        {
            bool IsInserted = false;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("HierarchyName", Convert.ToString(objUserHierarchyBE.HierarchyName));
                DictionaryInstance.Add("IncludeVAT", Convert.ToString(objUserHierarchyBE.IncludeVAT));
                DictionaryInstance.Add("CreatedBy", Convert.ToString(objUserHierarchyBE.CreatedBy));
                DictionaryInstance.Add("ParentHierarchy", Convert.ToString(objUserHierarchyBE.ParentHierarchy));

                IsInserted = UserDA.Insert(Constants.USP_InsertUserHierarchy, DictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsInserted;
        }

        public static bool UpdateUserHierarchy(UserBE.UserHierarchyBE objUserHierarchyBE, int Case, int HierarchyLevel)
        {
            bool IsUpdated = false;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("Case", Convert.ToString(Case));
                DictionaryInstance.Add("HierarchyLevel", Convert.ToString(HierarchyLevel));
                DictionaryInstance.Add("UserHierarchyId", Convert.ToString(objUserHierarchyBE.UserHierarchyId));
                DictionaryInstance.Add("HierarchyName", Convert.ToString(objUserHierarchyBE.HierarchyName));
                DictionaryInstance.Add("IncludeVAT", Convert.ToString(objUserHierarchyBE.IncludeVAT));
                DictionaryInstance.Add("ModifiedBy", Convert.ToString(objUserHierarchyBE.ModifiedBy));

                IsUpdated = UserDA.Update(Constants.USP_UpdateUserHierarchy, DictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        public static bool IsUserHierarchyLevelExists(byte UserHierarchyLevel)
        {
            bool IsExists = false;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("UserHierarchyLevel", Convert.ToString(UserHierarchyLevel));

                IsExists = UserDA.IsExists(Constants.USP_IsUserHierarchyLevelExists, DictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsExists;
        }



        #endregion

        #region "Added by Sripal"
        public static Int16 ExecuteRegisterDetailsSSO(string sp, bool IsStoreConnectionString, UserBE objBE, string roleType)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("EmailId", objBE.EmailId);
                DictionaryInstance.Add("Password", objBE.Password);
                DictionaryInstance.Add("FirstName", objBE.FirstName);
                DictionaryInstance.Add("LastName", objBE.LastName);
                DictionaryInstance.Add("CurrencyId", objBE.CurrencyId.ToString());
                DictionaryInstance.Add("ContactName", objBE.PredefinedColumn1);
                DictionaryInstance.Add("Company", objBE.PredefinedColumn2);
                DictionaryInstance.Add("AddressLine1", objBE.PredefinedColumn3);
                DictionaryInstance.Add("AddressLine2", objBE.PredefinedColumn4);
                DictionaryInstance.Add("Town", objBE.PredefinedColumn5);
                DictionaryInstance.Add("County", objBE.PredefinedColumn6);
                DictionaryInstance.Add("Postcode", objBE.PredefinedColumn7);
                DictionaryInstance.Add("CountryId", objBE.PredefinedColumn8);
                DictionaryInstance.Add("Phone", objBE.PredefinedColumn9);
                DictionaryInstance.Add("DelContactName", objBE.UserDeliveryAddress[0].PreDefinedColumn1);
                DictionaryInstance.Add("DelCompany", objBE.UserDeliveryAddress[0].PreDefinedColumn2);
                DictionaryInstance.Add("DelAddressLine1", objBE.UserDeliveryAddress[0].PreDefinedColumn3);
                DictionaryInstance.Add("DelAddressLine2", objBE.UserDeliveryAddress[0].PreDefinedColumn4);
                DictionaryInstance.Add("DelTown", objBE.UserDeliveryAddress[0].PreDefinedColumn5);
                DictionaryInstance.Add("DelCounty", objBE.UserDeliveryAddress[0].PreDefinedColumn6);
                DictionaryInstance.Add("DelPostcode", objBE.UserDeliveryAddress[0].PreDefinedColumn7);
                DictionaryInstance.Add("DelCountryId", objBE.UserDeliveryAddress[0].PreDefinedColumn8);
                DictionaryInstance.Add("DelPhone", objBE.UserDeliveryAddress[0].PreDefinedColumn9);
                DictionaryInstance.Add("IpAddress", objBE.IPAddress);
                DictionaryInstance.Add("Role", roleType);
                DictionaryInstance.Add("IsGuestUser", Convert.ToString(objBE.IsGuestUser));

                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }
        #endregion

        /// <summary>
        /// Author  : Vinit Falgunia
        /// Date    : 19/11/2015
        /// Scope   : insert all the Registration Filter Data
        /// </summary>        
        /// <param name="Filter Data Table"></param>
        /// <param name="Filter Type"></param>
        /// <returns>returns count of Table</returns>
        public static bool InsertRegistrationFilterList(RegistrationFilter action, DataTable dtRegistrationFilter)
        {
            Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
            try
            {
                if (action == RegistrationFilter.WhiteList)
                {
                    dictionaryInstance.Add("RegistrationFilterData", dtRegistrationFilter);
                    return UserDA.Insert(Constants.USP_InsertRegistrationWhiteList, dictionaryInstance, true);
                }
                else
                {
                    dictionaryInstance.Add("RegistrationFilterData", dtRegistrationFilter);
                    return UserDA.Insert(Constants.USP_InsertRegistrationBlackList, dictionaryInstance, true);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        public static List<T> GetAllRegistrationFilter<T>(RegistrationFilter filterType)
        {
            List<T> lst = null;
            try
            {
                if (filterType == RegistrationFilter.WhiteList)
                {
                    lst = CommonDA.getCollectionItem<T>(Constants.USP_GetAllRegistrationWhiteList, null, true);
                }
                else
                {
                    lst = CommonDA.getCollectionItem<T>(Constants.USP_GetAllRegistrationBlackList, null, true);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lst = null;
            }
            return lst;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Add and Update Users
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns newly added User ID and Existing Id in case of edit</returns>
        public static int InsertUpdateMCPUser(UserBE objUserBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                #region Parameters and Values
                dictionaryInstance.Add("UserId", Convert.ToString(objUserBE.UserId));
                dictionaryInstance.Add("FirstName", Convert.ToString(objUserBE.FirstName));
                dictionaryInstance.Add("MiddleName", Convert.ToString(objUserBE.MiddleName));
                dictionaryInstance.Add("LastName", Convert.ToString(objUserBE.LastName));
                dictionaryInstance.Add("EmailId", Convert.ToString(objUserBE.EmailId));
                dictionaryInstance.Add("Password", Convert.ToString(objUserBE.Password));
                dictionaryInstance.Add("RoleId", Convert.ToString(objUserBE.RoleId));
                dictionaryInstance.Add("IsActive", Convert.ToString(objUserBE.IsActive));
                dictionaryInstance.Add("Phone", Convert.ToString(objUserBE.Phone));
                dictionaryInstance.Add("Action", Convert.ToString(objUserBE.Action));
                dictionaryInstance.Add("Enteredby", Convert.ToString(objUserBE.ModifiedBy));

                Dictionary<string, string> UserId = new Dictionary<string, string>();
                UserId.Add("return", Convert.ToString(typeof(Int16)));

                #endregion
                UserDA.Insert(Constants.USP_ManageUser, dictionaryInstance, ref UserId, false);
                if (objUserBE.Action == Convert.ToInt16(DBAction.Insert))
                {
                    return Convert.ToInt16(UserId["return"]);
                }
                else
                {
                    return objUserBE.UserId;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }

        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Add and Update Customer Group
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns newly added Group ID and Existing Id in case of edit</returns>
        public static int InsertUpdateCustomerGroup(UserBE.CustomerGroupBE objCustomerGroup)
        {
            try
            {
                string ValidityStartDate = "";
                string ValidityEndDate = "";

                DateTime dtstart = objCustomerGroup.ValidityStartDate;
                ValidityStartDate = dtstart.ToString("yyyy-MM-dd");
                DateTime dtEnd = objCustomerGroup.ValidityEndDate;
                ValidityEndDate = dtEnd.ToString("yyyy-MM-dd");

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                #region Parameters and Values

                if (objCustomerGroup.GroupName != null)
                {
                    dictionaryInstance.Add("ValidityStartDate", Convert.ToString(ValidityStartDate));
                    dictionaryInstance.Add("ValidityEndDate", Convert.ToString(ValidityEndDate));
                }
                dictionaryInstance.Add("GroupId", Convert.ToString(objCustomerGroup.GroupId));
                dictionaryInstance.Add("GroupName", Convert.ToString(objCustomerGroup.GroupName));
                dictionaryInstance.Add("Budget", Convert.ToString(objCustomerGroup.Budget));
                dictionaryInstance.Add("PendingAmt", Convert.ToString(objCustomerGroup.PendingAmt));
                dictionaryInstance.Add("IsActive", Convert.ToString(objCustomerGroup.IsActive));
                dictionaryInstance.Add("Action", Convert.ToString(objCustomerGroup.Action));
                dictionaryInstance.Add("Enteredby", Convert.ToString(objCustomerGroup.ModifiedBy));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                #endregion
                UserDA.Insert(Constants.USP_ManageCustomerGroup, dictionaryInstance, ref dictReturn, true);
                if (objCustomerGroup.Action == Convert.ToInt16(DBAction.Insert))
                {
                    return Convert.ToInt16(dictReturn["return"]);
                }
                else
                {
                    return objCustomerGroup.GroupId;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Add and Update Store to User
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns newly added User ID and Existing Id in case of edit</returns>
        public static bool SaveAssignStore(UserBE objUserBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                #region Parameters and Values
                dictionaryInstance.Add("UserId", objUserBE.UserId.ToString());
                dictionaryInstance.Add("IsActive", objUserBE.IsActive.ToString());
                dictionaryInstance.Add("Action", objUserBE.Action.ToString());
                dictionaryInstance.Add("StoreId", objUserBE.StoreIdCSV.ToString());
                dictionaryInstance.Add("enteredby", objUserBE.CreatedBy.ToString());

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                #endregion
                UserDA.Insert(Constants.USP_ManageUserStores, dictionaryInstance, ref dictReturn, false);
                if (Convert.ToInt16(dictReturn["return"]) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }

        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Get MCP Users
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns List of Users </returns>
        public static List<UserBE> GetMCPUsers(UserBE objUserBE)
        {

            List<UserBE> lstUser;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objUserBE.Action));
                dictionaryInstance.Add("UserId", Convert.ToString(objUserBE.UserId));
                dictionaryInstance.Add("FirstName", Convert.ToString(objUserBE.FirstName));
                dictionaryInstance.Add("SearchText", Convert.ToString(objUserBE.FirstName));
                lstUser = UserDA.getCollectionItem(Constants.USP_ManageUser, dictionaryInstance, false);
                return lstUser;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstUser = null;
                return lstUser;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Get MCP Users
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns List of Users </returns>
        public static List<T> GetMCPUsers<T>(UserBE objUserBE)
        {

            //List<UserBE> lstUser;
            List<T> lstUser = new List<T>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objUserBE.Action));
                dictionaryInstance.Add("UserId", Convert.ToString(objUserBE.UserId));
                dictionaryInstance.Add("FirstName", Convert.ToString(objUserBE.FirstName));
                dictionaryInstance.Add("SearchText", Convert.ToString(objUserBE.FirstName));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                lstUser = CommonDA.getCollectionItem<T>(Constants.USP_ManageUser, dictionaryInstance, ref dictReturn, false);
                return lstUser;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstUser = null;
                return lstUser;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Get MCP Users
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns List of Users </returns>
        public static List<UserBE> GetMCPUsers()
        {

            List<UserBE> lstUser;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", "");

                lstUser = UserDA.getCollectionItem(Constants.USP_ManageUser, dictionaryInstance, false);
                return lstUser;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstUser = null;
                return lstUser;
            }
        }


        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Get Assigned Stores
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns List of Stores </returns>
        public static List<StoreBE> GetAssignedStores(UserBE objUserBE)
        {

            List<StoreBE> lstStore;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", Convert.ToString(objUserBE.UserId));
                dictionaryInstance.Add("Action", Convert.ToString(objUserBE.Action));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                lstStore = StoreDA.getCollectionItem(Constants.USP_ManageUserStores, dictionaryInstance, ref dictReturn, false);

                return lstStore;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstStore = null;
                return lstStore;
            }
        }

        /// <summary>
        /// gets Store USer Mapping details
        /// </summary>
        /// <returns>returns Dataset objects</returns>
        public static DataSet GetStoreUserMappingDetails()
        {
            DataSet getStoreUserMappingDetails = null;
            try
            {
                //if (HttpContext.Current.Session["StoreDetailsMCP"] == null)
                //{

                getStoreUserMappingDetails = UserDA.GetStoreUserMappingDetails(Constants.USP_GetStoreUserMapping, null, false);
                //HttpContext.Current.Session["StoreDetailsMCP"] = getStores;
                //}
                //else
                //    getStores = (StoreBE)HttpContext.Current.Session["StoreDetailsMCP"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getStoreUserMappingDetails;
        }

        public List<UserBE.HeirarchyUser> GetHierarchyUser(Int16 UserHierarchyId)
        {
            List<UserBE.HeirarchyUser> lstHierarchyUser;
            try
            {
                lstHierarchyUser = CommonDA.getCollectionItem<UserBE.HeirarchyUser>(Constants.USP_HeirarchyWiseUsers, null, true);
                return lstHierarchyUser;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstHierarchyUser = null;
                return lstHierarchyUser;
            }

        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Get Groups Lsit 
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns List of Group </returns>
        public static List<UserBE.CustomerGroupBE> GetCustomerGroup(UserBE.CustomerGroupBE objCustomerGroupBE)
        {
            List<UserBE.CustomerGroupBE> lstObjCustomerGroup;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objCustomerGroupBE.Action));
                dictionaryInstance.Add("GroupId", Convert.ToString(objCustomerGroupBE.GroupId));
                dictionaryInstance.Add("SearchText", Convert.ToString(objCustomerGroupBE.GroupName));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                //lstObjCustomerGroup = CommonDA.getCollectionItem(Constants.USP_ManageCustomerGroup, dictionaryInstance,ref dictReturn, true, Constants.Entity_CustomerGroupBE );

                lstObjCustomerGroup = UserDA.getCustomerGroup(Constants.USP_ManageCustomerGroup, dictionaryInstance, ref dictReturn, true);

                return lstObjCustomerGroup;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstObjCustomerGroup = null;
                return lstObjCustomerGroup;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   : Get Assign Customer Email Id for Group
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns List of Email Id </returns>
        public static List<UserBE.CustomerGroupBE> GetAssignedCustomerGroupEmail(UserBE.CustomerGroupBE objCustomerGroupBE)
        {
            List<UserBE.CustomerGroupBE> lstObjCustomerGroup;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objCustomerGroupBE.Action));
                dictionaryInstance.Add("GroupId", Convert.ToString(objCustomerGroupBE.GroupId));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                lstObjCustomerGroup = UserDA.getCustomerGroup(Constants.USP_ManageCustomerGroupWithUser, dictionaryInstance, ref dictReturn, true);

                return lstObjCustomerGroup;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstObjCustomerGroup = null;
                return lstObjCustomerGroup;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 21-11-15
        /// Scope   :  Get Assign Customer Email Id for Group
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns>returns List of Email Id </returns>
        public static List<UserBE.CustomerGroupBE> GetAllUnAssignedCustomerGroup(UserBE.CustomerGroupBE objCustomerGroupBE)
        {

            List<UserBE.CustomerGroupBE> lstObjCustomerGroup;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objCustomerGroupBE.Action));
                dictionaryInstance.Add("GroupId", Convert.ToString(objCustomerGroupBE.GroupId));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));
                if (objCustomerGroupBE.Action == Convert.ToInt32(DBAction.Insert))
                    lstObjCustomerGroup = UserDA.getCustomerGroup(Constants.USP_ManageCustomerGroupWithUser, dictionaryInstance, ref dictReturn, true);
                else
                    lstObjCustomerGroup = UserDA.getCustomerGroup(Constants.USP_GetAllUnAssignedCustomerGroup, dictionaryInstance, ref dictReturn, true);

                return lstObjCustomerGroup;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstObjCustomerGroup = null;
                return lstObjCustomerGroup;
            }
        }

        public static bool InsertUpdateCustomerGroupedUser(UserBE.CustomerGroupBE objCustomerGroupBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(objCustomerGroupBE.Action));
                dictionaryInstance.Add("GroupId", Convert.ToString(objCustomerGroupBE.GroupId));
                dictionaryInstance.Add("Enteredby", Convert.ToString(objCustomerGroupBE.ModifiedBy));
                dictionaryInstance.Add("UserId", Convert.ToString(objCustomerGroupBE.UserIdCSV));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                UserDA.Insert(Constants.USP_ManageCustomerGroupWithUser, dictionaryInstance, ref dictReturn, true);

                if (Convert.ToInt16(dictReturn["return"]) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// Author : Vikram Singh
        /// Date : 18/09/2015
        ///  Scope   : To Upload Data in Table
        public static bool BulkImportExcel(DataTable dt, string TableName)
        {
            return UserDA.BulkInsert(dt, true, TableName);
        }

        ///Author: Sripal Amballa
        public static List<UserBE> GetUserDetailsSearch(string strSearch)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("str", strSearch);
                return UserDA.getCollectionItem(Constants.USP_GetUserDetailsBySearch, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        public static bool UpdatePassword(UserBE objBE)
        {
            bool res = false;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("UserId", Convert.ToString(objBE.UserId));
                DictionaryInstance.Add("Password", Convert.ToString(objBE.Password));
                res = CommonDA.Update(Constants.USP_UpdatePassword, DictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return res;
        }

        /// <summary>
        /// Author  : sanchit patne
        /// Date    : 20/01/2016
        /// Scope   : Update Customer Status
        /// </summary>
        /// <returns><bool</returns>
        /// 
        public static bool UpdateCustomerStatus(UserBE objUserBE)
        {
            bool result;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("UserId", Convert.ToString(objUserBE.UserId));
                dictionaryInstance.Add("IsActive", Convert.ToString(objUserBE.IsActive));
                result = UserDA.Update(Constants.USP_UpdateCustomerStatus, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }

            return result;
        }

        public static Int16 ChkUserExists(string sp, bool IsStoreConnectionString, string strUserEmail)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("EmailId", strUserEmail);
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static Int16 CheckGuestUserExists(string sp, string strUserEmail, string UserSessionID)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("EmailId", strUserEmail);
                DictionaryInstance.Add("UserSessionID", UserSessionID);
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static List<UserBE> GetSubsribedUserList(string strSearch)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("SearchText", strSearch);
                return UserDA.getCollectionItem(Constants.USP_GetReportNewLetterSubScription, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static string GetCustomColumnValue(string sp, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            return UserDA.GetCustomColumnValue(sp, param, IsStoreConnectionString);
        }


        public static List<UserBE> GetMCPUserDetails()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return UserDA.getCollectionItem(Constants.USP_GetMCPUserDetails, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /* Tripti for order history*/

        public static UserBE Getorderhistory(string sp, bool IsStoreConnectionString, UserBE objBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("userid", Convert.ToString(objBE.UserId));
            dictionaryInstance.Add("CurrencyId", Convert.ToString(objBE.CurrencyId));
            return UserDA.getUserDetails(sp, dictionaryInstance, IsStoreConnectionString);
        }


        /// Author : Snehal Jadhav
        /// Date : 12 01 2017
        ///  Scope   : Get Customer Data
        public static DataTable GetUserDataForExportView()
        {
            return UserDA.GetDataforCustomerViewExcel(Constants.USP_GetUserDetailsForExport, true);
        }

        #region /*User Type*/
        /// <summary>
        /// Author  :   Nilesh Borhade
        /// Date    :   22 June 2016
        /// Scope   :   Update Custom Fields for User Types
        /// </summary>
        /// <returns>returns bool value </returns>
        public static bool UpdateCustomColumnValue(Dictionary<string, string> dictionaryInstance)
        {
            bool bresult;
            try
            {
                Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
                bresult = UserDA.UpdateCustomColumnValue(Constants.USP_UpdateCustomColumnValue, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                bresult = false;
            }
            return bresult;
        }

        #endregion
        public static bool InsertNameBadges(UserBE.NameBadgeBE objNameBAdgeBE)
        {
            bool bresult;
            try
            {
                Dictionary<string, string> NameBadgeInstance = new Dictionary<string, string>();
                NameBadgeInstance.Add("NAME", objNameBAdgeBE.Name);
                NameBadgeInstance.Add("DEPARTMENT_NAME", objNameBAdgeBE.DepartmentName);
                NameBadgeInstance.Add("PHONE_NUMBER", objNameBAdgeBE.PhoneNumber);
                NameBadgeInstance.Add("EMAIL_ID", objNameBAdgeBE.Email_id);
                NameBadgeInstance.Add("DELIVERY_NAME", objNameBAdgeBE.Delivery_Name);
                NameBadgeInstance.Add("DELIVERY_ADDRESS", objNameBAdgeBE.Delivery_Address);
                NameBadgeInstance.Add("NUMBEROFNAMES", Convert.ToString(objNameBAdgeBE.NumberOfNames));
                bresult = UserDA.Insert(Constants.USP_INSERTNAMEBADGCUSTINFO, NameBadgeInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                bresult = false;
            }
            return bresult;
        }


        #region Indeed Code

        // Added by vivek for indeed SSO		
        public static Int16 ChkEmployeeExists(string sp, bool IsStoreConnectionString, string strEmployeeID)
        {
            Int16 intLogId = 0;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("Employee_ID", strEmployeeID);
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                UserDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, true);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intLogId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intLogId;
        }

        public static UserBE.BA_USERFILE GetIndeedSSOUserDetails(string strEmployeeID)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Employee_ID", strEmployeeID);

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                return (UserBE.BA_USERFILE)CommonDA.getItem(Constants.USP_GetBAUserProfileDetails, dictionaryInstance, ref dictReturn, Constants.Entity_UserFileBE, true);

                // ContactBE = (StoreBE.ContactBE)CommonDA.getItem(Constants.USP_ManageContactUs, dictionaryInstance, ref dictReturn, Constants.Entity_ContactBE, true);		

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        public static List<UserBE> GetRegUnregUserDetails()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return UserDA.getCollectionItem(Constants.USP_GetRegUnregUserDetails, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
        public static List<UserBE> GetRegUnregExportUserDetails()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return UserDA.getCollectionItem(Constants.USP_GetRegUnregExportUserDetails, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
        //Added by Swapnil Bagkar on 02-02-2017		
        public static bool UpdateRegUnregUserPoints(DataTable dtUpdatePoint)
        {
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("PointsUpdate", dtUpdatePoint);
                Dictionary<string, string> PointsUpdatedInstance = new Dictionary<string, string>();
                PointsUpdatedInstance.Add("IsPointUpdated", Convert.ToString(typeof(bool)));
                UserDA.UpdatePoints(Constants.USP_UpdateRegUnregUserPoints, dictionaryInstance, ref PointsUpdatedInstance, true);
                return Convert.ToBoolean(Convert.ToString(PointsUpdatedInstance["IsPointUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }


        //Added by Swapnil Bagkar on 02-02-2017		
        public static bool UpdateRegUnregUserPointsForEdit(string EmailId, string Points)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("EmailId", EmailId);
                dictionaryInstance.Add("Points", Points);
                Dictionary<string, string> PointsUpdatedInstance = new Dictionary<string, string>();
                PointsUpdatedInstance.Add("IsPointUpdated", Convert.ToString(typeof(bool)));
                UserDA.UpdatePointsForEdit(Constants.USP_UpdateRegUnregUserPointsForEdit, dictionaryInstance, ref PointsUpdatedInstance, true);
                return Convert.ToBoolean(Convert.ToString(PointsUpdatedInstance["IsPointUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        public static bool InsertUpdateEmployeeSAML(string employeeID, string SAMLPostMessage)
        {
            bool bresult;
            try
            {
                Dictionary<string, string> IndeedSAMLInstance = new Dictionary<string, string>();
                IndeedSAMLInstance.Add("EmployeeID", employeeID);
                IndeedSAMLInstance.Add("SAMLPostMessage", SAMLPostMessage);
                bresult = UserDA.Insert(Constants.USP_InsertUpdateEmployeeSAML, IndeedSAMLInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                bresult = false;
            }
            return bresult;
        }

        public static List<UserBE.BA_USERFILE> GetSAMLbyEmployeeID(string employeeID)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Employee_ID", employeeID);
                return UserDA.GetSAMLbyEmployeeID(Constants.USP_GetSAMLByEmployeeID, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        #endregion

        /// <summary>
        /// Author  :   ShriGanesh Singh
        /// </summary>
        /// <param name="LanguageId"></param>
        /// <returns></returns>
        public static List<UserBE.Titles> GetTitles(Int16 LanguageId)
        {
            List<UserBE.Titles> GetTitles = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                GetTitles = UserDA.GetTitles(Constants.USP_GetTitles, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return GetTitles;

        }
        public static UserBE.BA_USERFILE GetUserDetailsByEmailID(string strSearch)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("EmailId", strSearch);
                return UserDA.getSingleObjectItemUserFiles(Constants.USP_UserEmailForSSO, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
    }
}
