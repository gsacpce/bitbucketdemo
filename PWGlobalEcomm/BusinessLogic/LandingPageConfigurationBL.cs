﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class LandingPageConfigurationBL
    {
        public static bool AddLandingPageConfiguration(LandingPageConfigurationBE objLandingPageConfigurationBE, DBAction dbAction)
        {
            bool res = false;
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(dbAction).ToString());
                switch (dbAction)
                {
                    case DBAction.Insert:
                        dictParams.Add("LandingPageText", objLandingPageConfigurationBE.LandingPageText);
                        dictParams.Add("LanguageID", Convert.ToString(objLandingPageConfigurationBE.LanguageID));
                        dictParams.Add("ImagePath", objLandingPageConfigurationBE.ImagePath);
                        if (dbAction.Equals(DBAction.Insert))
                        {
                            CommonDA.Insert(Constants.USP_LandingPageConfiguration_SAE, dictParams, true);
                            res = true;
                        }
                        else
                        {
                            CommonDA.Update(Constants.USP_LandingPageConfiguration_SAE, dictParams, true);
                            res = true;
                        }
                        break;

                    case DBAction.Update:
                        break;
                    case DBAction.Delete:
                        break;
                    case DBAction.Select:
                        break;
                }
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return res;
        }

        public static List<LandingPageConfigurationBE> GetListLandingPageConfiguration(LandingPageConfigurationBE objLandingPageConfigurationBE)
        {
            try
            {
                //Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                //dictionaryInstance.Add("ID", Convert.ToString(objLandingPageConfigurationBE.ID));
                //dictionaryInstance.Add("LandingPageText", Convert.ToString(objLandingPageConfigurationBE.LandingPageText));
                //dictionaryInstance.Add("IsActive", Convert.ToString(objLandingPageConfigurationBE.IsActive));

                return CommonDA.getCollectionItem<LandingPageConfigurationBE>(Constants.USP_GetListLandingPageConfiguration, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
    }
}
