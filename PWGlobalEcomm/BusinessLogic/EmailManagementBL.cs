﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    public class EmailManagementBL
    {
        public static List<EmailManagementBE> GetAllTemplates(string sp, EmailManagementBE objBE)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
            dictionaryInstance.Add("EmailTemplateId", objBE.EmailTemplateId.ToString());
            dictionaryInstance.Add("ActionType", "A");
            return EmailManagementDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        public static EmailManagementBE GetSingleEmailObject(string sp, EmailManagementBE objBE, bool IsStoreConnectionString)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("EmailTemplateId", objBE.EmailTemplateId.ToString());
            dictionaryInstance.Add("ActionType", "S");
            dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
            dictionaryInstance.Add("EmailTemplateName", objBE.EmailTemplateName);
            return EmailManagementDA.getSingleObjectItem(sp, dictionaryInstance, IsStoreConnectionString);
        }

        public static Int16 UpdateEmailDetails(string sp, EmailManagementBE objBE, bool IsStoreConnectionString)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("EmailTemplateId", objBE.EmailTemplateId.ToString());
                dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
                dictionaryInstance.Add("EmailTemplateName", objBE.EmailTemplateName);
                dictionaryInstance.Add("FromEmailId", objBE.FromEmailId);
                dictionaryInstance.Add("CCEmailId", objBE.CCEmailId);
                dictionaryInstance.Add("BCCEmailId", objBE.BCCEmailId);
                dictionaryInstance.Add("Subject", objBE.Subject);
                dictionaryInstance.Add("Body", objBE.Body);
                dictionaryInstance.Add("ModifiedBy", objBE.ModifiedBy.ToString());
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());
                EmailManagementDA.Update(sp, dictionaryInstance, ref DictionaryOutParameterInstance, IsStoreConnectionString);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return intId;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 01-10-15
        /// Scope   : Gets email templates
        /// </summary>
        /// <param name="objEmailMangementBE"></param>
        /// <param name="strAction"></param>
        /// <returns>returns list of EmailMangementBE objects</returns>
        public static List<EmailManagementBE> GetEmailTemplates(EmailManagementBE objEmailMangementBE, string strAction)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("EmailTemplateId", Convert.ToString(objEmailMangementBE.EmailTemplateId));
                dictionaryInstance.Add("ActionType", strAction);
                dictionaryInstance.Add("LanguageId", Convert.ToString(objEmailMangementBE.LanguageId));
                dictionaryInstance.Add("EmailTemplateName", objEmailMangementBE.EmailTemplateName);

                return EmailManagementDA.getCollectionItem(Constants.USP_GetEmailTemplate, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author : Hardik Gohil 
        /// Date   : 08-03-2017
        /// Scope  : Insert Displayname for Email
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public static bool GetFromEmailIdAlias(string sp, EmailManagementBE.FromEmailBE objBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("FromEmailIdAlias", objBE.FromEmailIdAlias);
                EmailManagementDA.Insert(sp, dictionaryInstance, true);
                HttpContext.Current.Cache.Remove("StoreDetails");
                StoreBE getStores = null;
                if (HttpContext.Current.Cache["StoreDetails"] == null)
                {
                    getStores = StoreDA.getItem(Constants.USP_GetAllStoreDetails, null, true);
                    HttpContext.Current.Cache["StoreDetails"] = getStores;
                }
                else
                    getStores = (StoreBE)HttpContext.Current.Cache["StoreDetails"];

                return true;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return false;
        }
    }
}
