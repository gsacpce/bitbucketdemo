﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class StaticTextKeyBL
    {
        public static List<StaticTextKeyBE> GetAllKeys(string sp, StaticTextKeyBE objBE, string actionType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("StaticTextId", Convert.ToString(objBE.StaticTextId));
            dictionaryInstance.Add("ActionType", actionType);
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            return StaticTextKeyDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        public static StaticTextKeyBE GetSingleObjectDetails(string sp, bool IsStoreConnectionString, StaticTextKeyBE objBE, string actionType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("StaticTextId", Convert.ToString(objBE.StaticTextId));
            dictionaryInstance.Add("ActionType", actionType);
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
            return StaticTextKeyDA.getSingleObjectItem(sp, dictionaryInstance, IsStoreConnectionString);
        }

        public static Int16 UpdateStaticTextKeyDetails(string sp, Dictionary<string, string> LoginDetail, bool IsStoreConnectionString)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                StaticTextKeyDA.Update(sp, LoginDetail, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return intId;
        }

        public static Int16 DeleteStaticTextKeyDetails(string sp, bool IsStoreConnectionString, StaticTextKeyBE objBE, int actionType)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("CreatedBy", objBE.CreatedBy.ToString());
                DictionaryInstance.Add("LanguageId", Convert.ToString(objBE.LanguageId));
                DictionaryInstance.Add("StaticTextId", Convert.ToString(objBE.StaticTextId));
                DictionaryInstance.Add("ActionType", Convert.ToString(actionType));
                DictionaryInstance.Add("StaticTextKey", objBE.StaticTextKey);
                DictionaryInstance.Add("StaticTextValue", objBE.StaticTextValue);
                DictionaryInstance.Add("IsActive", objBE.IsActive.ToString().ToLower() == "true" ? "1" : "0");

                StaticTextKeyDA.Delete(sp, DictionaryInstance, ref DictionaryOutParameterInstance, IsStoreConnectionString);

                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return intId;
        }

        public static Int16 InsertStaticTextKeyDetails(string sp, bool IsStoreConnectionString, StaticTextKeyBE objBE, int actionType)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("ActionType", actionType.ToString());
                DictionaryInstance.Add("StaticTextId", Convert.ToString(objBE.StaticTextId));
                DictionaryInstance.Add("StaticTextKey", objBE.StaticTextKey);
                DictionaryInstance.Add("CreatedBy", Convert.ToString(objBE.CreatedBy));
                DictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
                DictionaryInstance.Add("StaticTextValue", objBE.StaticTextValue);
                DictionaryInstance.Add("IsActive", objBE.IsActive.ToString().ToLower() == "true" ? "1" : "0");
                
                StaticTextKeyDA.Insert(sp, DictionaryInstance, ref DictionaryOutParameterInstance, IsStoreConnectionString);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return intId;
        }
    }
}
 