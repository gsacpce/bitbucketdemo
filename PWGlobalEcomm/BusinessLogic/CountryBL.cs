﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class CountryBL
    {
        public static List<CountryBE> GetAllCountries()
        {
            return CountryDA.getCollectionItem(Constants.USP_GetAllCountries, null, true); 
        }

        public static List<CountryBE> GetAllInvoiceCountries()
        {
            return CountryDA.getCollectionItem(Constants.USP_GetAllInvoiceCountries, null, true);
        }

        public static List<CountryBE> GetDisplayUDFS(string Country, string currencyid)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("Country", Convert.ToString(Country));
            dictionaryInstance.Add("currency", Convert.ToString(currencyid));

            return CountryDA.getCollectionItem(Constants.USP_GetDisplayUDFS, dictionaryInstance, true);
        }

        // Added By Snehal - 24 10 2016
        public static List<CountryBE> GetAllFreightCountry(string RegionCode)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("RegionCode", Convert.ToString(RegionCode));
            return CountryDA.getCollectionItem(Constants.USP_GetAllCountries, dictionaryInstance, true);
        }

        #region Indeed Code

        public static List<CountryBE.CountryCurrencyMapping> GetAllCountryCurrencyMapping(string Ship_To_Location)
        {
            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
            DictionaryInstance.Add("ShipToLocation", Ship_To_Location);
            return CountryDA.GetAllCountryCurrencyMapping(Constants.USP_GetAllCountryCurrencyMapping, DictionaryInstance, true);
        }
        #endregion

        /// <summary>
        /// Author      :   SHRIGANESH SINGH
        /// Date        :   06 Feb 2017
        /// Description :   To get the Currecy details for the given country code
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        public static List<CountryBE.CountryWiseCurrencyMapping> GetCountryWiseCurrencyMapping(string CountryCode)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CountryCode", Convert.ToString(CountryCode));
            return CountryDA.GetCountryWiseCurrencyMapping(Constants.USP_GetCountryWiseCurrencyMapping, null, true);
        }

        public static List<CountryBE.CountryCurrencyMapping> GetAllCountryCurrencyMappings()
        {
            return CountryDA.GetAllCountryCurrencyMapping(Constants.USP_GetAllCurruncyCountryMappings, null, true);
        }

    }
}
