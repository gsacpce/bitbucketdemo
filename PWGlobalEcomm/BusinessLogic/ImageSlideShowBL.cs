﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using PWGlobalEcomm.DataAccess;
using System.Data;
using System.IO;

namespace PWGlobalEcomm.BusinessLogic
{
    public class ImageSlideShowBL
    {
        public static string Host = GlobalFunctions.GetVirtualPath();
        #region Public Methods
        public static List<ImageSlideShowBE> GetAllImageSliders()
        {
            List<ImageSlideShowBE> lstAllImageSliders = new List<ImageSlideShowBE>();
        
            try
            {
                //if (HttpRuntime.Cache["ImageSlideShow"] == null)
                //{
                    Dictionary<string, string> dictParams = new Dictionary<string, string>();
                    dictParams.Add("Action", Convert.ToInt16(DBAction.Select).ToString());
                    dictParams.Add("IsActive", "0");

                    lstAllImageSliders = CommonDA.getCollectionItem<ImageSlideShowBE>(Constants.USP_ManageSlideShow_SAED, dictParams, true);

                    if (lstAllImageSliders != null)
                    {
                        foreach (var obj in lstAllImageSliders)
                        {
                            ImageSlideShowBE CurrImageSlideShow = obj as ImageSlideShowBE;
                            List<ImageSlideShowBE.SlideShowImage> lstAllSliderImages = GetAllSliderImages<ImageSlideShowBE.SlideShowImage>(CurrImageSlideShow.SlideShowId );
                            CurrImageSlideShow.LstSlideShowImages = lstAllSliderImages;
                        }
                  //      HttpRuntime.Cache.Insert("ImageSlideShow", lstAllImageSliders);
                    }

                //}
                //else
                //    lstAllImageSliders = (List<ImageSlideShowBE>)HttpRuntime.Cache["ImageSlideShow"];
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }

            return lstAllImageSliders;
        }

        public static Int16 ManageSlideShow_SAED(ImageSlideShowBE SlideShow, DBAction action)
        {


            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("Action", Convert.ToInt16(action).ToString() );
            Dictionary<string, string> SlideShowId = new Dictionary<string, string>();
            SlideShowId.Add("NewSSId", Convert.ToString(typeof(Int16)));
            try 
            {
                dictParams.Add("SlideShowId", SlideShow.SlideShowId.ToString());
                dictParams.Add("SlideShowAnimationMasterId", SlideShow.SlideShowMasterId.ToString());
                dictParams.Add("SlideShowName", SlideShow.SlideShowName);
                dictParams.Add("SlideDelayTime", SlideShow.SlideDelayTime.ToString());
                dictParams.Add("SlideEffectTime", SlideShow.SlideEffectTime.ToString());
                dictParams.Add("IsActive", SlideShow.IsActive.Equals(true) ? "1" : "0");
                dictParams.Add("IsOverLayText", SlideShow.IsOverLayText.Equals(true) ? "1" : "0");
                dictParams.Add("FontId", SlideShow.FontId.ToString());
                dictParams.Add("TextForeColor", SlideShow.TextForeColor.ToString());
                dictParams.Add("TextBgColor", SlideShow.TextBgColor.ToString());
                dictParams.Add("Height", SlideShow.Height.ToString());
                dictParams.Add("ShowPagination", SlideShow.ShowPagination.ToString());

                #region MyRegion BELOW LOC IS ADDED BY SHRIGANESH 29 JAN 2016 FOR OVERLAY SETTINGS
                dictParams.Add("OverlayFontSize", Convert.ToString(SlideShow.OverlayFontSize));
                dictParams.Add("OverlayHeight", Convert.ToString(SlideShow.OverlayHeight));
                dictParams.Add("OverlayForeColor", Convert.ToString(SlideShow.OverlayForeColor));
                dictParams.Add("OverlayBGColor", Convert.ToString(SlideShow.OverlayBGColor));
                dictParams.Add("OverlayPosition", Convert.ToString(SlideShow.OverlayPosition));                
                #endregion

                switch (action)
                {
                    
                    case DBAction.Insert:
                        {
                            CommonDA.Insert(Constants.USP_ManageSlideShow_SAED, dictParams, ref SlideShowId, true);        
                            break;
                        }
                    case DBAction.Update:
                        {
                            CommonDA.Update(Constants.USP_ManageSlideShow_SAED, dictParams, ref SlideShowId, true);        
                            break;
                        }
                    case DBAction.Delete:
                        {
                            /*Sachin Chauhan : 27 10 2015 : Delete Physical files from the Slide Show Image Directory */
                            List<ImageSlideShowBE.SlideShowImage> lstAllSliderImages = GetAllSliderImages<ImageSlideShowBE.SlideShowImage>(SlideShow.SlideShowId);
                            String imgFilePath = HttpContext.Current.Server.MapPath("~/Images/SlideShow");
                            DirectoryInfo dirSSImgs = new DirectoryInfo(imgFilePath);
                            
                            if (lstAllSliderImages != null && lstAllSliderImages.Count > 0)
                            {
                                foreach (ImageSlideShowBE.SlideShowImage image in lstAllSliderImages)
                                {
                                    if (File.Exists(imgFilePath + "\\" + image.SlideShowImageId + "" + image.ImageExtension))
                                    {
                                        File.Delete(imgFilePath + "\\" + image.SlideShowImageId + "" + image.ImageExtension);
                                        File.Delete(imgFilePath + "\\" + image.SlideShowImageId + "_Thumb" + image.ImageExtension);
                                    }
                                    if (File.Exists(imgFilePath + "\\temp\\" + image.FilePath))
                                    {
                                        File.Delete(imgFilePath + "\\temp\\" + image.FilePath);
                                    }
                                }
                            }
                            /*Sachin Chauhan : 27 10 2015*/
                            
                            CommonDA.Delete(Constants.USP_ManageSlideShow_SAED, dictParams, ref SlideShowId, true);
                        }
                        break;
                    default:
                        break;
                }
                 HttpRuntime.Cache.Remove("ImageSlideShow");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Convert.ToInt16(SlideShowId["NewSSId"]);
        }

        public static List<object> GetAllSlideShowMaster()
        {
            List<object> lstAllSlideShowMaster = new List<object>();
            lstAllSlideShowMaster = CommonDA.getCollectionItem(Constants.USP_GetAllSlideShowMaster, null, true, Constants.Entity_SlideShowMaster);

            return lstAllSlideShowMaster;
        }

        #endregion

        #region Private Methods
        private static List<T> GetAllSliderImages<T>(Int16 SliderId)
        {
            List<T> lstAllSliderImages = new List<T>();
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("SliderId", SliderId.ToString());
                dictParams.Add("LanguageId", GlobalFunctions.GetLanguageId().ToString() );
                lstAllSliderImages = CommonDA.getCollectionItem<T>(Constants.USP_GetAllSliderImages, dictParams, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            return lstAllSliderImages;
        }
        #endregion

        public static List<object> GetAllFonts()
        {
            List<object> Fonts = new List<object>();
            try
            {
                Fonts = CommonDA.getCollectionItem("USP_GetAllFonts", null, true, Constants.Entity_ImageSlideShowBE);
                
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                //throw;
            }
            return Fonts;
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 10-08-15
        /// Scope   : to get all slide show images details
        /// Return  : List<ImageSlideShowBE.SlideShowImage> 
        /// </summary>

        internal static List<object> GetAllSliderImagesDetails(Int16 Id)
        {
            List<object> lstAllSlideShowMaster = new List<object>();
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("SlideShowId", Convert.ToInt16(Id).ToString());
            lstAllSlideShowMaster = CommonDA.getCollectionItem(Constants.USP_GetAllSlideShowDetails, dictParams, true, Constants.Entity_SlideImage);

            return lstAllSlideShowMaster;
        }

        internal static bool InsertSlideImagesDetails(DataTable dtSliderImageDetails)
        {
            bool IsSaved = false;
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("DataW", dtSliderImageDetails);
                Dictionary<string, string> SliderInstance = new Dictionary<string, string>();
                SliderInstance.Add("IsSaved", Convert.ToString(typeof(bool)));
                CommonDA.SaveBulkData(Constants.USP_InsertSlideImagesDetails, dictionaryInstance, ref SliderInstance, true);
                if (SliderInstance["IsSaved"] != "")
                {
                    IsSaved = false;
                }
                IsSaved = Convert.ToBoolean(SliderInstance["IsSaved"]);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsSaved;
        }

        internal static string GetOverLayText(string LanguageId, string SlideShowImagId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", LanguageId);
                dictionaryInstance.Add("SlideShowImageId", SlideShowImagId);

                return CommonDA.getStringValue(Constants.USP_GetOverLayText, dictionaryInstance, true);

            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
            return "";
        }

        internal static string SetOverLayText(string LanguageId, string SlideShowImagId, string OverLayText)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", LanguageId);
                dictionaryInstance.Add("SlideShowImageId", SlideShowImagId);
                dictionaryInstance.Add("OverLayText", OverLayText);

                if (CommonDA.Update(Constants.USP_SetOverLayText, dictionaryInstance, true))
                {
                    return "Updated Successfully";

                }
                else
                {
                    return "Error Occured";

                }

            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
            return "";
        }
    }
}
