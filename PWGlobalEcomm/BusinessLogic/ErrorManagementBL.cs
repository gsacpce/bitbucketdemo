﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class ErrorManagementBL
    {
        public static List<ErrorManagementBE> GetAllTemplates(string sp, ErrorManagementBE objBE, string actionType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
            dictionaryInstance.Add("ErrorTemplateId", objBE.ErrorTemplateId.ToString());
            dictionaryInstance.Add("ActionType", actionType);
            return ErrorManagementDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        public static ErrorManagementBE GetSingleErrorObject(string sp, ErrorManagementBE objBE, bool IsStoreConnectionString)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("ErrorTemplateId", objBE.ErrorTemplateId.ToString());
            dictionaryInstance.Add("ActionType", "S");
            dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
            return ErrorManagementDA.getSingleObjectItem(sp, dictionaryInstance, IsStoreConnectionString);
        }

        public static Int16 UpdateErrorDetails(string sp, ErrorManagementBE objBE, bool IsStoreConnectionString)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ErrorTemplateId", objBE.ErrorTemplateId.ToString());
                dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
                dictionaryInstance.Add("ErrorTemplateName", objBE.ErrorTemplateName);
                dictionaryInstance.Add("Subject", objBE.Subject);
                dictionaryInstance.Add("Body", objBE.Body);
                dictionaryInstance.Add("ModifiedBy", objBE.CreatedBy.ToString());
                
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                ErrorManagementDA.Update(sp, dictionaryInstance, ref DictionaryOutParameterInstance, IsStoreConnectionString);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return intId;
        }

        public static Int16 InsertErrorDetails(string sp, ErrorManagementBE objBE, bool IsStoreConnectionString)
        {
            Int16 intId = 0;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ErrorTemplateName", objBE.ErrorTemplateName);
                dictionaryInstance.Add("Subject", objBE.Subject);
                dictionaryInstance.Add("Body", objBE.Body);
                dictionaryInstance.Add("CreatedBy", objBE.CreatedBy.ToString());
                
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());

                ErrorManagementDA.Insert(sp, dictionaryInstance, ref DictionaryOutParameterInstance, IsStoreConnectionString);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            { Exceptions.WriteExceptionLog(ex); }
            return intId;
        }
 
    }
}
