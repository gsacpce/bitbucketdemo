﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    class BreadCrumbManagementBL
    {

        internal static List<BreadCrumbManagementBE> GetAllBreadCrumbDetails(Int16 LanguageId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
            return BreadCrumbManagementDA.getCollectionItem(Constants.USP_GetAllBreadCrumbsDetails, dictionaryInstance, true);
        }



        internal static bool UpdateBreadCrumbDetails(BreadCrumbManagementBE objBreadCrumb, DBAction dBAction)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();

            dictionaryInstance.Add("BreadCrumbId", objBreadCrumb.BreadCrumbId.ToString());
            dictionaryInstance.Add("BreadCrumbName", objBreadCrumb.BreadCrumbName);
            dictionaryInstance.Add("Link", objBreadCrumb.Link);
            dictionaryInstance.Add("BreadCrumbLanguageId", Convert.ToString(objBreadCrumb.BreadCrumbLanguageId));
            dictionaryInstance.Add("LanguageId", Convert.ToString(objBreadCrumb.LanguageId));
            dictionaryInstance.Add("ModifiedBy", Convert.ToString(objBreadCrumb.ModifiedBy));
           return BreadCrumbManagementDA.Update(Constants.USP_UpdateBreadCrumbDetails, dictionaryInstance, true);
        }

        internal static List<BreadCrumbManagementBE> GetBreadCrumb()
        {
            List<BreadCrumbManagementBE> getBreadCrumbs = null;
            try
            {
                //if (HttpRuntime.Cache["BreadCrumbs"] == null)
                //{
                //    //getBreadCrumbs = BreadCrumbManagementDA.getCollectionItem(Constants.USP_GetBreadCrumb, null, true);
                //    getBreadCrumbs = BreadCrumbManagementDA.getCollectionItem("USP_GetBreadCrumb", null, true);
                //    HttpRuntime.Cache.Insert("BreadCrumbs", getBreadCrumbs);
                //}
                //else
                //    getBreadCrumbs = (List<BreadCrumbManagementBE>)HttpRuntime.Cache["BreadCrumbs"];

                if (HttpRuntime.Cache["BreadCrumbs"] == null)
                {
                    getBreadCrumbs = BreadCrumbManagementDA.getCollectionItem("USP_GetBreadCrumb", null, true);
                    HttpRuntime.Cache.Insert("BreadCrumbs", getBreadCrumbs);
                }
                else
                    getBreadCrumbs = (List<BreadCrumbManagementBE>)HttpRuntime.Cache["BreadCrumbs"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getBreadCrumbs;


        }

        



    }
}
