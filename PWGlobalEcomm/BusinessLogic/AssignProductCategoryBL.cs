﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public partial class CategoryBL
    {
        /// <summary>
        /// Author: Vinit Falgunia
        /// Date: 24/07/2015
        /// gets all categories details
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>returns list of CategoryBE objects</returns>
        public static List<CategoryBE> GetCategroyTree(int ProductId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("ProductId", Convert.ToString(ProductId));
            return CategoryDA.getCollectionItem(Constants.USP_GetCategoryTree, dictionaryInstance, true);
        }
    }
}
