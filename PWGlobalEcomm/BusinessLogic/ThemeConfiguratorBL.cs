﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class ThemeConfiguratorBL
    {
        public static List<ThemeConfiguratorBE> GetAllThemeConfiguratorElemnts()
        {
            List<ThemeConfiguratorBE> lstThemeConfiguratorBE = new List<ThemeConfiguratorBE>();
            List<object> lst = new List<object>();
            try
            {
                lst = CommonDA.getCollectionItem(Constants.USP_GetAllThemeConfiguratorElemnts, null, true, Constants.Entity_ThemeConfiguratorBE);
                lstThemeConfiguratorBE = lst.Cast<ThemeConfiguratorBE>().ToList();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);    
            }
            return lstThemeConfiguratorBE;
        }

        public static void UpdateThemeConfiguratorElements(string KeyName,string KeyValue)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("KeyName", KeyName);
                dictionaryInstance.Add("KeyValue", KeyValue);
                DataAccessBase.Update(Constants.USP_UpdateThemeConfiguratorElements, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }
}
