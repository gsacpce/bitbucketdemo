﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class UserRegistrationBL
    {
        public static UserRegistrationBE GetUserRegistrationDetails()
        {
            UserRegistrationBE getUserRegistrationBE = null;
            try
            {
                getUserRegistrationBE = UserRegistrationDA.getItem(Constants.USP_GetRegistrationDetails, null, true);
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
            return getUserRegistrationBE;
        }

        public static List<T> GetRegistrationAllLanguages<T>(UserRegistrationBE.RegistrationLanguagesBE objUserRegistrationBE)
        {

            //List<UserBE> lstUser;
            List<T> lstUser = new List<T>();
            try
            {

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("ActionType", Convert.ToString(objUserRegistrationBE.Action));
                DictionaryInstance.Add("UserId", Convert.ToString(objUserRegistrationBE.ModifiedBy));
                DictionaryInstance.Add("RegistrationLanguageId", Convert.ToString(objUserRegistrationBE.RegistrationLanguageId));
                DictionaryInstance.Add("LabelTitle", Convert.ToString(objUserRegistrationBE.LabelTitle));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));

                lstUser = CommonDA.getCollectionItem<T>("USP_ManageRegistrationData_SAED", DictionaryInstance, ref dictReturn, true);
                return lstUser;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstUser = null;
                return lstUser;
            }
        }

        public static bool AEDRegistrationData(UserRegistrationBE.RegistrationLanguagesBE objUserRegistrationBE)
        {
            List<UserRegistrationBE.RegistrationLanguagesBE> getRegistrationCustomField = null;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("ActionType", Convert.ToString(objUserRegistrationBE.Action));
                DictionaryInstance.Add("UserId", Convert.ToString(objUserRegistrationBE.ModifiedBy));
                DictionaryInstance.Add("RegistrationLanguageId", Convert.ToString(objUserRegistrationBE.RegistrationLanguageId));
                DictionaryInstance.Add("LabelTitle", Convert.ToString(objUserRegistrationBE.LabelTitle));

                Dictionary<string, string> dictReturn = new Dictionary<string, string>();
                dictReturn.Add("return", Convert.ToString(typeof(Int16)));


                UserDA.Update(Constants.USP_ManageRegistrationData_SAED, DictionaryInstance, ref dictReturn, true);
                if (objUserRegistrationBE.Action == Convert.ToInt16(DBAction.Update))
                {
                    return Convert.ToBoolean(dictReturn["return"]);
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }
    }
}
