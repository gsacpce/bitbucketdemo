﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public partial class BrandColorManagementBL
    {
        /// <summary>
        /// Author: Vinit Falgunia
        /// Date: 24/07/2015
        /// gets all Brand Colors
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns>returns list of BrandColorManagement objects</returns>
        public static List<BrandColorManagementBE> GetAllBrandColors()
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            return BrandColorManagementDA.getCollectionItem(Constants.USP_GetAllBrandColors, dictionaryInstance, true);
        }
        
        internal static bool ManageBrandColor_AED(BrandColorManagementBE objBrandColor, DBAction dBAction)
        {
            
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("Action", Convert.ToInt16(dBAction).ToString());
            dictionaryInstance.Add("BrandColorId", objBrandColor.BrandColorId.ToString());
            dictionaryInstance.Add("ColorName", objBrandColor.ColorName);
            dictionaryInstance.Add("ColorHexCode", objBrandColor.ColorHexCode);
            dictionaryInstance.Add("IsActive", Convert.ToString(objBrandColor.IsActive));
            dictionaryInstance.Add("CreatedBy", Convert.ToString(objBrandColor.CreatedBy));
            dictionaryInstance.Add("ExColorHexCode", Convert.ToString(objBrandColor.ExColorCode));
            return BrandColorManagementDA.Insert(Constants.USP_ManageBrandColorManagement_AED, dictionaryInstance, true);
            
        }
    }
}
