﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class StoreLanguageBL
    {

        public static List<StoreBE.StoreLanguageBE> GetStoreLanguageDetails()
        {
            List<StoreBE.StoreLanguageBE> GetStoreLanguageDetails = null;
            try
            {
                //if (HttpRuntime.Cache["LanguageMaster"] == null)
                //{
                bool IsStoreConnectionString = true;
                //GetStoreCurrencyDetails = StoreCurrencyDA.getCollectionItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
                GetStoreLanguageDetails = StoreDA.getStoreLanguageItem(Constants.usp_GetStoreLanguageDetails, null, IsStoreConnectionString);
                //HttpRuntime.Cache.Insert("LanguageMaster", getAllLanguages, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/LanguageMasterCaching.txt")));
                //}
                //else
                //    getAllLanguages = (List<LanguageBE>)HttpRuntime.Cache["LanguageMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return GetStoreLanguageDetails;
        }

        /// <summary>
        /// Update Store Default Languages 
        /// </summary>
        /// <param name="StoreLanguageDetail"></param>
        /// <returns>returns bool value</returns>
        public static bool UpdateDefaultStoreLanguageMapping(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString = false)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.usp_UpdateStoreLanguageMappings_IsDefault, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }
        public static bool UpdateDefaultStoreLanguageMappingMCP(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString)
        {
            bool IsUpdated = false;
            try
            {
                IsUpdated = StoreDA.Update(Constants.usp_UpdateStoreLanguageMappings_IsDefaultMCP, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsUpdated;
        }

        public static List<StoreBE.StoreLanguageBE> GetActiveStoreLanguageDetails()
        {
            List<StoreBE.StoreLanguageBE> GetStoreLanguageDetails = null;
            try
            {
                bool IsStoreConnectionString = true;
                GetStoreLanguageDetails = StoreDA.getStoreLanguageItem(Constants.usp_GetActiveStoreLanguageDetails, null, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return GetStoreLanguageDetails;
        }
    }
}
