﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace PWGlobalEcomm.BusinessLogic
{
    public partial class ShoppingCartBL
    {
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 09-09-15
        /// Scope   : Gets User and currency specific basket details
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="intUserId"></param>
        /// <returns>returns list of ShoppingcartBE objects</returns>
        public static List<object> GetBasketProductsData(Int16 intLanguageId, Int16 intCurrencyId, Int32 intUserId, string strUserSessionId, bool IsCheckoutPage = false)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(intLanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(intCurrencyId));
                dictionaryInstance.Add("UserId", Convert.ToString(intUserId));
                dictionaryInstance.Add("UserSessionId", strUserSessionId);
                dictionaryInstance.Add("IsCheckoutPage", Convert.ToString(IsCheckoutPage));
                return CommonDA.getCollectionItem(Constants.USP_GetBasketProductsData, dictionaryInstance, true, Constants.Entity_ShoppingCartBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// Author  : Tripti
        /// Date    : 05-01-17
        /// Scope   : get additional freight applied on the products
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="intUserId"></param>
        /// <returns>returns list of ShoppingcartBE objects</returns>
        public static List<ShoppingCartAdditionalBE> GetAdditionalData( Int16 intCurrencyId, Int32 intUserId, string strUserSessionId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();               
                dictionaryInstance.Add("CurrencyId", Convert.ToString(intCurrencyId));
                dictionaryInstance.Add("UserId", Convert.ToString(intUserId));
                dictionaryInstance.Add("UserSessionId", strUserSessionId);                
                return ShoppingCartDA.getadditionaldata(Constants.USP_Getadditionalcharges, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 10-09-15
        /// Scope   : Remove product from basket
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="intUserId"></param>
        /// <returns>returns list of ShoppingcartBE objects</returns>
        public static List<object> RemoveProductsFromBasket(Int32 ShoppingCartProductId, Int32 intUserId, string strUserSessionId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ShoppingCartProductId", Convert.ToString(ShoppingCartProductId));
                dictionaryInstance.Add("UserId", Convert.ToString(intUserId));
                dictionaryInstance.Add("UserSessionId", strUserSessionId);
                return CommonDA.getCollectionItem(Constants.USP_RemoveProductsFromBasket, dictionaryInstance, true, Constants.Entity_ShoppingCartBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 09-02-2015
        /// Scope   : Remove All product from anonymous basket. This method should not be used for logged in user's basket
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="intUserId"></param>
        /// <returns>returns list of ShoppingcartBE objects</returns>
        public static List<object> RemoveAllBasketProducts(string strUserSessionId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserSessionId", strUserSessionId);
                return CommonDA.getCollectionItem(Constants.USP_RemoveAllBasketProducts, dictionaryInstance, true, Constants.Entity_ShoppingCartBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 10-09-15
        /// Scope   : Remove product from basket
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="intUserId"></param>
        /// <returns>returns list of ShoppingcartBE objects</returns>
        public static List<object> UpdateBasketData(string strShoppingCartProductIds, Int32 intUserId, string strUserSessionId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("ShoppingCartProductIds", Convert.ToString(strShoppingCartProductIds));
                dictionaryInstance.Add("UserId", Convert.ToString(intUserId));
                dictionaryInstance.Add("UserSessionId", strUserSessionId);
                return CommonDA.getCollectionItem(Constants.USP_UpdateBasketData, dictionaryInstance, true, Constants.Entity_ShoppingCartBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 18-09-15
        /// Scope   : Add/Update customer order information
        /// </summary>
        /// <param name="objCustomerOrderBE"></param>
        /// <returns>returns list of CustomerOrderBE objects</returns>
        public static CustomerOrderBE CustomerOrder_SAE(CustomerOrderBE objCustomerOrderBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", Convert.ToString(objCustomerOrderBE.OrderedBy));
                dictionaryInstance.Add("CustomerOrderId", Convert.ToString(objCustomerOrderBE.CustomerOrderId));
                dictionaryInstance.Add("ApprovedBy", Convert.ToString(objCustomerOrderBE.ApprovedBy));
                dictionaryInstance.Add("InvoiceCompany", Convert.ToString(objCustomerOrderBE.InvoiceCompany));
                dictionaryInstance.Add("InvoiceContactPerson", Convert.ToString(objCustomerOrderBE.InvoiceContactPerson));
                dictionaryInstance.Add("InvoiceAddress1", Convert.ToString(objCustomerOrderBE.InvoiceAddress1));
                dictionaryInstance.Add("InvoiceAddress2", Convert.ToString(objCustomerOrderBE.InvoiceAddress2));
                dictionaryInstance.Add("InvoiceCity", Convert.ToString(objCustomerOrderBE.InvoiceCity));
                dictionaryInstance.Add("InvoicePostalCode", Convert.ToString(objCustomerOrderBE.InvoicePostalCode));
                dictionaryInstance.Add("InvoiceCountry", Convert.ToString(objCustomerOrderBE.InvoiceCountry));
                dictionaryInstance.Add("InvoiceCounty", Convert.ToString(objCustomerOrderBE.InvoiceCounty));
                dictionaryInstance.Add("InvoicePhone", Convert.ToString(objCustomerOrderBE.InvoicePhone));
                dictionaryInstance.Add("InvoiceFax", Convert.ToString(objCustomerOrderBE.InvoiceFax));
                dictionaryInstance.Add("InvoiceEmail", Convert.ToString(objCustomerOrderBE.InvoiceEmail));
                dictionaryInstance.Add("PaymentMethod", Convert.ToString(objCustomerOrderBE.PaymentMethod));
                dictionaryInstance.Add("OrderRefNo", Convert.ToString(objCustomerOrderBE.OrderRefNo));
                dictionaryInstance.Add("AuthorizationId_OASIS", Convert.ToString(objCustomerOrderBE.AuthorizationId_OASIS));
                dictionaryInstance.Add("OrderId_OASIS", Convert.ToString(objCustomerOrderBE.OrderId_OASIS));
                dictionaryInstance.Add("TxnRefGUID", Convert.ToString(objCustomerOrderBE.TxnRefGUID));
                dictionaryInstance.Add("IsOrderCompleted", Convert.ToString(objCustomerOrderBE.IsOrderCompleted));
                dictionaryInstance.Add("DeliveryCompany", Convert.ToString(objCustomerOrderBE.DeliveryCompany));
                dictionaryInstance.Add("DeliveryAddressId", Convert.ToString(objCustomerOrderBE.DeliveryAddressId));
                dictionaryInstance.Add("DeliveryContactPerson", Convert.ToString(objCustomerOrderBE.DeliveryContactPerson));
                dictionaryInstance.Add("DeliveryAddress1", Convert.ToString(objCustomerOrderBE.DeliveryAddress1));
                dictionaryInstance.Add("DeliveryAddress2", Convert.ToString(objCustomerOrderBE.DeliveryAddress2));
                dictionaryInstance.Add("DeliveryCity", Convert.ToString(objCustomerOrderBE.DeliveryCity));
                dictionaryInstance.Add("DeliveryPostalCode", Convert.ToString(objCustomerOrderBE.DeliveryPostalCode));
                dictionaryInstance.Add("DeliveryCountry", Convert.ToString(objCustomerOrderBE.DeliveryCountry));
                dictionaryInstance.Add("DeliveryCounty", Convert.ToString(objCustomerOrderBE.DeliveryCounty));
                dictionaryInstance.Add("DeliveryPhone", Convert.ToString(objCustomerOrderBE.DeliveryPhone));
                dictionaryInstance.Add("DeliveryFax", Convert.ToString(objCustomerOrderBE.DeliveryFax));
                dictionaryInstance.Add("stCardScheme", Convert.ToString(objCustomerOrderBE.stCardScheme));
                dictionaryInstance.Add("SpecialInstructions", Convert.ToString(objCustomerOrderBE.SpecialInstructions));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objCustomerOrderBE.CurrencyId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objCustomerOrderBE.LanguageId));                
                dictionaryInstance.Add("StandardCharges", Convert.ToString(objCustomerOrderBE.StandardCharges, CultureInfo.InvariantCulture));
                dictionaryInstance.Add("ExpressCharges", Convert.ToString(objCustomerOrderBE.ExpressCharges, CultureInfo.InvariantCulture));
                dictionaryInstance.Add("PalletCharges", Convert.ToString(objCustomerOrderBE.PalletCharges, CultureInfo.InvariantCulture));
                dictionaryInstance.Add("TotalTax", Convert.ToString(objCustomerOrderBE.TotalTax, CultureInfo.InvariantCulture));
                dictionaryInstance.Add("Action", Convert.ToString(objCustomerOrderBE.action));
                dictionaryInstance.Add("OrderCarrierId", Convert.ToString(objCustomerOrderBE.OrderCarrierId));
                dictionaryInstance.Add("OrderCarrierName", Convert.ToString(objCustomerOrderBE.OrderCarrierName));
                dictionaryInstance.Add("CouponCode", Convert.ToString(objCustomerOrderBE.CouponCode));
                dictionaryInstance.Add("handlingfee", Convert.ToString(objCustomerOrderBE.handlingfee, CultureInfo.InvariantCulture));
                return ShoppingCartDA.getCollectionItem(Constants.USP_CustomerOrder_SAE, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-09-15
        /// Scope   : Get all payment types
        /// </summary>
        /// <returns>returns list of payment types objects</returns>
        public static List<object> GetAllPaymentTypes()
        {
            try
            {
                return CommonDA.getCollectionItem(Constants.USP_GetAllPaymentTypes, null, true, Constants.Entity_PaymentTypesBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 22-09-15
        /// Scope   : Gets adflex Transaction details
        /// </summary>
        /// <param name="intStoreId"></param>
        /// <param name="intOrderId"></param>
        /// <returns>returns AdFlexTransactionsBE objects</returns>
        public static AdFlexTransactionsBE GetAdFlexTransactionDetails(Int16 intStoreId, Int16 intOrderId, string strTxRefGUID)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("StoreId", Convert.ToString(intStoreId));
                dictionaryInstance.Add("OrderId", Convert.ToString(intOrderId));
                dictionaryInstance.Add("TxRefGUID", strTxRefGUID);
                return ShoppingCartDA.GetAdFlexTransactionDetails(Constants.USP_GetAdFlexTransactionDetails, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 28-09-15
        /// Scope   : Get UDF values
        /// </summary>
        /// <param name="intPaymentTypeId"></param>
        /// <returns>returns list of udf objects</returns>
        /// /*User Type*/ added LanguageID
        public static List<object> GetUDFS(Int32 intPaymentTypeId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("PAYMENTTYPEID", Convert.ToString(intPaymentTypeId));
                dictionaryInstance.Add("SequenceNo", "0");
                dictionaryInstance.Add("LanguageID", Convert.ToString(GlobalFunctions.GetLanguageId()));
                return CommonDA.getCollectionItem(Constants.USP_GetUDFS, dictionaryInstance, true, Constants.Entity_UDFBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 28-09-15
        /// Scope   : Get UDF values
        /// </summary>
        /// <param name="intPaymentTypeId"></param>
        /// <returns>returns list of udf objects</returns>
        public static List<object> GetUDFSValues(Int32 intPaymentTypeId, Int32 intSequenceNo = 0)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("PAYMENTTYPEID", Convert.ToString(intPaymentTypeId));
                dictionaryInstance.Add("SequenceNo", Convert.ToString(intSequenceNo));
                return CommonDA.getCollectionItem(Constants.USP_GetUDFS, dictionaryInstance, true, Constants.Entity_PickListItemsBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 7-10-15
        /// Scope   : Remove product from basket
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="intUserId"></param>
        /// <returns>returns list of ShoppingcartBE objects</returns>
        public static bool UpdateUDFValues(string strUDFValues, Int32 intCustomerOrderId)
        {
            try
            {
                List<object> lstObjUDFValues = new List<object>();
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UDFValues", strUDFValues);
                dictionaryInstance.Add("CustomerOrderId", Convert.ToString(intCustomerOrderId));
                lstObjUDFValues = CommonDA.getCollectionItem(Constants.USP_UpdateUDFValues, dictionaryInstance, true, Constants.Entity_UDFValuesBE);
                if (lstObjUDFValues != null && lstObjUDFValues.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 19-10-15
        /// Scope   : Add/Update Coupon code used against a user
        /// </summary>
        /// <param name="objCustomerOrderBE"></param>
        /// <returns>returns list of CustomerOrderBE objects</returns>
        public static CustomerOrderBE UpdateUsedCouponCode(CustomerOrderBE objCustomerOrderBE)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", Convert.ToString(objCustomerOrderBE.OrderedBy));
                dictionaryInstance.Add("CustomerOrderId", Convert.ToString(objCustomerOrderBE.CustomerOrderId));
                dictionaryInstance.Add("CouponCode", Convert.ToString(objCustomerOrderBE.CouponCode));
                return ShoppingCartDA.getCollectionItem(Constants.USP_UpdateUsedCouponCode, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 20-10-15
        /// Scope   : Move product data from user session to users account
        /// </summary>
        /// <param name="intUserId"></param>
        /// <param name="strUserSessionId"></param>
        /// <returns>returns list of objects</returns>
        public static List<object> UpdateBasketSessionProducts(Int32 intUserId, string strUserSessionId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", Convert.ToString(intUserId));
                dictionaryInstance.Add("UserSessionId", strUserSessionId);
                return CommonDA.getCollectionItem(Constants.USP_UpdateBasketSessionProducts, dictionaryInstance, true, Constants.Entity_ShoppingCartBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 18-09-15
        /// Scope   : Add/Update customer order information
        /// </summary>
        /// <param name="objCustomerOrderBE"></param>
        /// <returns>returns list of CustomerOrderBE objects</returns>
        public static CustomerOrderBE CustomerOrderPunchout_SAE(CustomerOrderBE objCustomerOrderBE)
        {
            try
            {
                #region
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", Convert.ToString(objCustomerOrderBE.OrderedBy));
                dictionaryInstance.Add("CustomerOrderId", Convert.ToString(objCustomerOrderBE.CustomerOrderId));
                dictionaryInstance.Add("ApprovedBy", Convert.ToString(objCustomerOrderBE.ApprovedBy));
                dictionaryInstance.Add("InvoiceCompany", Convert.ToString(objCustomerOrderBE.InvoiceCompany));
                dictionaryInstance.Add("InvoiceContactPerson", Convert.ToString(objCustomerOrderBE.InvoiceContactPerson));
                dictionaryInstance.Add("InvoiceAddress1", Convert.ToString(objCustomerOrderBE.InvoiceAddress1));
                dictionaryInstance.Add("InvoiceAddress2", Convert.ToString(objCustomerOrderBE.InvoiceAddress2));
                dictionaryInstance.Add("InvoiceCity", Convert.ToString(objCustomerOrderBE.InvoiceCity));
                dictionaryInstance.Add("InvoicePostalCode", Convert.ToString(objCustomerOrderBE.InvoicePostalCode));
                dictionaryInstance.Add("InvoiceCountry", Convert.ToString(objCustomerOrderBE.InvoiceCountry));
                dictionaryInstance.Add("InvoiceCounty", Convert.ToString(objCustomerOrderBE.InvoiceCounty));
                dictionaryInstance.Add("InvoicePhone", Convert.ToString(objCustomerOrderBE.InvoicePhone));
                dictionaryInstance.Add("InvoiceFax", Convert.ToString(objCustomerOrderBE.InvoiceFax));
                dictionaryInstance.Add("InvoiceEmail", Convert.ToString(objCustomerOrderBE.InvoiceEmail));
                dictionaryInstance.Add("PaymentMethod", Convert.ToString(objCustomerOrderBE.PaymentMethod));
                dictionaryInstance.Add("OrderRefNo", Convert.ToString(objCustomerOrderBE.OrderRefNo));
                dictionaryInstance.Add("AuthorizationId_OASIS", Convert.ToString(objCustomerOrderBE.AuthorizationId_OASIS));
                dictionaryInstance.Add("OrderId_OASIS", Convert.ToString(objCustomerOrderBE.OrderId_OASIS));
                dictionaryInstance.Add("TxnRefGUID", Convert.ToString(objCustomerOrderBE.TxnRefGUID));
                dictionaryInstance.Add("IsOrderCompleted", Convert.ToString(objCustomerOrderBE.IsOrderCompleted));
                dictionaryInstance.Add("DeliveryCompany", Convert.ToString(objCustomerOrderBE.DeliveryCompany));
                dictionaryInstance.Add("DeliveryAddressId", Convert.ToString(objCustomerOrderBE.DeliveryAddressId));
                dictionaryInstance.Add("DeliveryContactPerson", Convert.ToString(objCustomerOrderBE.DeliveryContactPerson));
                dictionaryInstance.Add("DeliveryAddress1", Convert.ToString(objCustomerOrderBE.DeliveryAddress1));
                dictionaryInstance.Add("DeliveryAddress2", Convert.ToString(objCustomerOrderBE.DeliveryAddress2));
                dictionaryInstance.Add("DeliveryCity", Convert.ToString(objCustomerOrderBE.DeliveryCity));
                dictionaryInstance.Add("DeliveryPostalCode", Convert.ToString(objCustomerOrderBE.DeliveryPostalCode));
                dictionaryInstance.Add("DeliveryCountry", Convert.ToString(objCustomerOrderBE.DeliveryCountry));
                dictionaryInstance.Add("DeliveryCounty", Convert.ToString(objCustomerOrderBE.DeliveryCounty));
                dictionaryInstance.Add("DeliveryPhone", Convert.ToString(objCustomerOrderBE.DeliveryPhone));
                dictionaryInstance.Add("DeliveryFax", Convert.ToString(objCustomerOrderBE.DeliveryFax));
                dictionaryInstance.Add("stCardScheme", Convert.ToString(objCustomerOrderBE.stCardScheme));
                dictionaryInstance.Add("SpecialInstructions", Convert.ToString(objCustomerOrderBE.SpecialInstructions));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objCustomerOrderBE.CurrencyId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objCustomerOrderBE.LanguageId));
                dictionaryInstance.Add("StandardCharges", Convert.ToString(objCustomerOrderBE.StandardCharges));
                dictionaryInstance.Add("ExpressCharges", Convert.ToString(objCustomerOrderBE.ExpressCharges));
                dictionaryInstance.Add("TotalTax", Convert.ToString(objCustomerOrderBE.TotalTax));
                dictionaryInstance.Add("Action", Convert.ToString(objCustomerOrderBE.action));
                dictionaryInstance.Add("OrderCarrierId", Convert.ToString(objCustomerOrderBE.OrderCarrierId));
                dictionaryInstance.Add("OrderCarrierName", Convert.ToString(objCustomerOrderBE.OrderCarrierName));
                dictionaryInstance.Add("CouponCode", Convert.ToString(objCustomerOrderBE.CouponCode));
                return ShoppingCartDA.getCollectionItem(Constants.USP_CustomerOrderPunchout_SAE, dictionaryInstance, true);
                #endregion
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        public static int UpdateAdFlexTransactionDetails(AdFlexTransactionsBE obAdFlexTransactionsBE)
        {
            int i = 0;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("inStoreId", Convert.ToString(obAdFlexTransactionsBE.inStoreId));
                dictionaryInstance.Add("inOrderId", Convert.ToString(obAdFlexTransactionsBE.inOrderId));
                dictionaryInstance.Add("stTxRefGUID", obAdFlexTransactionsBE.stTxRefGUID);
                CommonDA.Insert(Constants.UpdateAdflexGiftCertificate, dictionaryInstance, false);
                i = 1;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return i;
        }


        public static bool UpdateUsedGiftCertificateCode(string strGC, int CustomerOrderId, string GCAmount)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("GiftCertificateCode", Convert.ToString(strGC));
                dictionaryInstance.Add("CustomerOrderId", Convert.ToString(CustomerOrderId));
                dictionaryInstance.Add("GCAmount", Convert.ToString(GCAmount));

                return CommonDA.Insert(Constants.USP_InsertCustomerOrderGiftCertificate, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        ///Sripal
        public static List<T> GetGuestUserRegisteration<T>(string sp, Int16 LanguageID)
        {
            try
            {
                List<T> LstUserReg = new List<T>();

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageID));
                //return CommonDA.getCollectionItem(sp, dictionaryInstance, true, Constants.Entity_UserPreDefinedColumns);
                LstUserReg = CommonDA.getCollectionItem<T>(sp, dictionaryInstance, true);
                return LstUserReg;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        #region /*User Type*/
        public static List<object> GetAllPaymentTypesByLanguageID(int LanguageID, Int16 UserTypeID)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageID", Convert.ToString(LanguageID));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));
                return CommonDA.getCollectionItem(Constants.USP_GetAllPaymentTypesbyLanguageID, dictionaryInstance, true, Constants.Entity_PaymentTypesBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
        #endregion

        #region Telecash 
        

        public static bool TeleCashTransactions(string OrderId, string approval_code, string ccbrand, string cccountry, string chargetotal, string currencycode, string fail_rc, string fail_reason, string hash_algorithm, string ipgTransactionId,
            string oid, string response_hash, string status, string timezone, string txndatetime, string txntype, string txndate_processed, string cardnumber, string bname)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("OrderId", Convert.ToString(OrderId));
                dictionaryInstance.Add("approval_code", Convert.ToString(approval_code));
                dictionaryInstance.Add("ccbrand", Convert.ToString(ccbrand));
                dictionaryInstance.Add("cccountry", Convert.ToString(cccountry));
                dictionaryInstance.Add("chargetotal", Convert.ToString(chargetotal));
                dictionaryInstance.Add("currencycode", Convert.ToString(currencycode));
                dictionaryInstance.Add("fail_rc", Convert.ToString(fail_rc));
                dictionaryInstance.Add("fail_reason", Convert.ToString(fail_reason));
                dictionaryInstance.Add("hash_algorithm", Convert.ToString(hash_algorithm));
                dictionaryInstance.Add("ipgTransactionId", Convert.ToString(ipgTransactionId));
                dictionaryInstance.Add("oid", Convert.ToString(oid));
                dictionaryInstance.Add("response_hash", Convert.ToString(response_hash));
                dictionaryInstance.Add("status", Convert.ToString(status));
                dictionaryInstance.Add("timezone", Convert.ToString(timezone));
                dictionaryInstance.Add("txndatetime", Convert.ToString(txndatetime));
                dictionaryInstance.Add("txntype", Convert.ToString(txntype));
                dictionaryInstance.Add("txndate_processed", Convert.ToString(txndate_processed));
                dictionaryInstance.Add("cardnumber", Convert.ToString(cardnumber));
                dictionaryInstance.Add("bname", Convert.ToString(bname));

                foreach(var item in dictionaryInstance)
                {
                    Exceptions.WriteInfoLog(item.Key + ":" + item.Value+"<br/>");
                }


                return CommonDA.Insert(Constants.USP_TeleCashTransactions, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }


        /// <summary>
        /// Author  : Tripti Shukla
        /// Date    : 22-09-15
        /// Scope   : Gets Telecash Transaction details
        /// </summary>
        /// <param name="intStoreId"></param>
        /// <param name="intOrderId"></param>
        /// <returns>returns TelecashTransactionsBE objects</returns>
        public static TeleCashTransactionsBE GetTeleCashTransactionDetails(Int16 intOrderId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();              
                dictionaryInstance.Add("OrderId", Convert.ToString(intOrderId));              
                return ShoppingCartDA.GetTeleCashTransactionDetails(Constants.USP_GetTelecashTransactionDetails, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// Author  : Tripti Shukla
        /// Date    : 22-09-15
        /// Scope   : Gets Telecash Currency Number
        /// </summary>
        /// <param name="intStoreId"></param>
        /// <param name="intOrderId"></param>
        /// <returns>returns TeleCashCurrencyBE objects</returns>
        public static TeleCashCurrencyBE GetCurrencynumber(Int16 currencyid)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("@currencyId", Convert.ToString(currencyid));
                return ShoppingCartDA.GetCurrencynumber(Constants.USP_GetCurrencynumber, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        #endregion




        /// <Email Counter>
        /// Author  : Hardik Gohil  
        /// Date    : 14-10-16
        /// Scope   : Update Email Counter
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns></returns>
        public static int UpdateEmailCounter()
        {
            int counterno=0;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
               // dictionaryInstance.Add("Counter", Convert.ToString(objShoppingCartBE));

                Dictionary<string, string> Counter = new Dictionary<string, string>();
                Counter.Add("return", Convert.ToString(typeof(Int16)));
            try
            {
                
                UserDA.Insert(Constants.USP_updateEmailCounter, dictionaryInstance,ref Counter,  true);
                 if(Counter["return"]!=null)
                 {
                    return counterno= Convert.ToInt16(Counter["return"]);
                 }
                 else
                 {
                     return counterno=0;
                 }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return counterno;
        }

        /// <Email Counter>
        /// Author  : Vikram Singh
        /// Date    : 14-02-17
        /// Scope   : Vikram for Legal Entity by PaymentMethod
        /// </summary>
        /// <param name="objUserBE"></param>
        /// <returns></returns>
        public static List<object> GetPaymentdetailsById(Int16 PaymentTypeId,Int16 LanguageId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("PaymentTypeId", Convert.ToString(PaymentTypeId));
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                return CommonDA.getCollectionItem("USP_GetPaymentdetailsById", dictionaryInstance, true, Constants.Entity_PaymentTypesBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        #region Indeed code
        /// <summary>
        /// Author  : Tripti Shukla
        /// Date    : 24-03-17
        /// </summary>
        /// <param name="paymentid"></param>
        /// <returns></returns>
        public static List<object> getpoints(int paymentid)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("paymentid", Convert.ToString(paymentid));
                dictionaryInstance.Add("LanguageID", Convert.ToString(GlobalFunctions.GetLanguageId()));
                return CommonDA.getCollectionItem(Constants.USP_getpoints, dictionaryInstance, true, Constants.Entity_PaymentTypesBE);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
        #endregion

      
    }
}
