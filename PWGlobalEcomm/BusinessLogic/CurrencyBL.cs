﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    public class CurrencyBL
    {
        /// <summary>
        /// gets all Currencies
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of CurrencyBE objects</returns>
        public static List<CurrencyBE> GetAllCurrencyDetails()
        {
            List<CurrencyBE> getAllCurrencies = null;
            try
            {
                //if (HttpRuntime.Cache["CurrencyMaster"] == null)
                //{
                    getAllCurrencies = CurrencyDA.getCollectionItem(Constants.USP_GetAllCurrencyDetails, null, false);
                //    HttpRuntime.Cache.Insert("CurrencyMaster", getAllCurrencies, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/CurrencyMasterCaching.txt")));
                //}
                //else
                //    getAllCurrencies = (List<CurrencyBE>)HttpRuntime.Cache["CurrencyMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllCurrencies;
        }

        public static List<CurrencyBE> GetAllStoreCurrencyDetails()
        {
            List<CurrencyBE> getAllCurrencies = null;
            try
            { 
                getAllCurrencies = CurrencyDA.getCollectionItem(Constants.USP_GetAllCurrencyDetails, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllCurrencies;
        }

        public static List<CurrencyBE> GetAllStoreCurrencyDetailsByDivID()
        {
            List<CurrencyBE> getAllCurrencies = null;
            try
            {
                getAllCurrencies = CurrencyDA.getCollectionItem(Constants.USP_GetAllCurrencyDetails, null, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllCurrencies;
        }

        #region Indeed Code

        public static List<CurrencyBE> GetCurrencyPointInvoiceDetails()
        {
            List<CurrencyBE> getCurrencyPointInvoice = null;
            try
            {
                //if (HttpRuntime.Cache["CurrencyMaster"] == null)
                //{
                getCurrencyPointInvoice = CurrencyDA.getCollectionItem(Constants.USP_GetCurrencyPointInvoiceDetails, null, true);
                //    HttpRuntime.Cache.Insert("CurrencyMaster", getAllCurrencies, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/CurrencyMasterCaching.txt")));
                //}
                //else
                //    getAllCurrencies = (List<CurrencyBE>)HttpRuntime.Cache["CurrencyMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getCurrencyPointInvoice;
        }

        /// <summary>
        /// Snehal Jadhav
        /// 09 02 2017
        /// gets all Currencies - Indeed Store
        /// </summary>      
        /// <returns>returns list of CurrencyBE objects</returns>
        public static List<CurrencyBE> Indeed_GetAllCurrencyDetails()
        {
            List<CurrencyBE> getAllCurrencies = null;
            try
            {
                getAllCurrencies = CurrencyDA.getCollectionItem(Constants.USP_Indeed_GetCurrencies, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllCurrencies;
        }
        public static IndeedSalesBE.BA_Budget USP_GetBudgetdetails(string IsBudget, Int16 DivisionID)
        {
            // List<CurrencyBE> 
            IndeedSalesBE.BA_Budget GetBudgetdetails = null;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("DivsionID", Convert.ToString(DivisionID));
            dictionaryInstance.Add("EmployeeID", Convert.ToString(IsBudget));
            //  dictionaryInstance.Add("StoreId", Convert.ToString(StoreId));
            try
            {
                GetBudgetdetails = (IndeedSalesBE.BA_Budget)CurrencyDA.getItem(Constants.USP_GetBudgetdetails, dictionaryInstance, Constants.Entity_BA_Budget, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return GetBudgetdetails;
        }

        public static UserBE.UserBudgetBE USP_GetUserBudgetdetailsPayment(string Emailid, int CurrId)
        {
            // List<CurrencyBE> 
            UserBE.UserBudgetBE GetBudgetdetails = null;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("emailid", Convert.ToString(Emailid));
            dictionaryInstance.Add("CurrId", Convert.ToString(CurrId));
            try
            {
                GetBudgetdetails = (UserBE.UserBudgetBE)CurrencyDA.getItem(Constants.USP_GetUserBudgetDetailForPayment, dictionaryInstance, Constants.Entity_UserBudgetBE, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return GetBudgetdetails;
        }

        /// <summary>
        /// Snehal Jadhav
        /// 22 03 2017
        /// gets projected sales - Indeed Store
        /// </summary>      
        public static List<CurrencyBE> GetProjectedSaleDetails()
        {
            List<CurrencyBE> getProjectedSale = null;
            try
            {
                getProjectedSale = CurrencyDA.getCollectionItem(Constants.USP_Indeed_GetProjectedSales, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getProjectedSale;
        }

        #endregion

    }
}
