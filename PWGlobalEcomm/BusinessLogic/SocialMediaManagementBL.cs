﻿using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
   public class SocialMediaManagementBL
    {
        /// <summary>
        /// to get social media listing
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of social media objects</returns>

        public static List<SocialMediaManagementBE> ListSocialMediaManagement()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", "0");

                Dictionary<string, string> SocialMediaIdInstance = new Dictionary<string, string>();
                SocialMediaIdInstance.Add("SMediaOut", Convert.ToString(typeof(Int16)));
                return SocialMediaManagementDA.getCollectionItem(Constants.USP_ManageSocialMedia, dictionaryInstance,ref SocialMediaIdInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// to save social media details
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of social media objects</returns>

        public static int ManageSocialMedia(SocialMediaManagementBE SocialMedia)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("SocialMediaId", Convert.ToString(SocialMedia.SocialMediaId));
                dictionaryInstance.Add("SocialMediaPlatform", Convert.ToString(SocialMedia.SocialMediaPlatform));
                dictionaryInstance.Add("IconExtension", Convert.ToString(SocialMedia.IconExtension));
                dictionaryInstance.Add("HoverIconExtension", Convert.ToString(SocialMedia.HoverIconExtension));
                dictionaryInstance.Add("SocialMediaUrl", Convert.ToString(SocialMedia.SocialMediaUrl));
                dictionaryInstance.Add("IsActive", Convert.ToString(SocialMedia.IsActive));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(SocialMedia.CreatedBy));
                dictionaryInstance.Add("Action", Convert.ToString(SocialMedia.Action));

                Dictionary<string, string> SocialMediaIdInstance = new Dictionary<string, string>();
                SocialMediaIdInstance.Add("SMediaOut", Convert.ToString(typeof(Int16)));
                SocialMediaManagementDA.Insert(Constants.USP_ManageSocialMedia, dictionaryInstance, ref SocialMediaIdInstance, true);
                return Convert.ToInt16(SocialMediaIdInstance["SMediaOut"].ToString());
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }
        }


        public static bool SetSocialMediaSequence(DataTable dtDisplayOrder)
        {
            bool IsSequenceSet = false;
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("DataW", dtDisplayOrder);
                Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
                SequenceInstance.Add("IsSequenceSet", Convert.ToString(typeof(bool)));
                SocialMediaManagementDA.SetSequence(Constants.USP_SetSocialMediaSequence, dictionaryInstance, ref SequenceInstance, true);
                IsSequenceSet = Convert.ToBoolean(SequenceInstance["IsSequenceSet"]);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsSequenceSet;
        }

       public static List<T> GetAllSocialMedia<T>()
        {
            List<T> lst = new List<T>();
            try
            {
                lst = CommonDA.getCollectionItem<T>(Constants.USP_GetAllSocailMedia, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lst = null;
            }
            return lst;
        }
    }
}
