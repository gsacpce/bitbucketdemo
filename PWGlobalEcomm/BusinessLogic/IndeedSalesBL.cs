﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PWGlobalEcomm.BusinessEntity;

namespace PWGlobalEcomm.BusinessLogic
{
    class IndeedSalesBL : DataAccessBase
    {
        public static List<IndeedSalesBE.BA_SALESGRANDTOTAL> GetAllTotalOrderDetails()
        {
            Dictionary<string, string> dictionaryInstance = null;
            return IndeedSalesDA.getCollectionItem(Constants.USP_Indeed_TotalOrders, dictionaryInstance, true);
        }
        public static List<IndeedSalesBE.BA_SALESGRANDTOTAL> GetAllTotalSaleDetails(int CurrId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CurrId", Convert.ToString(CurrId));
            return IndeedSalesDA.getCollectionItem(Constants.USP_Indeed_TotalSales, dictionaryInstance, true);
        }
        public static DataTable GetAllTotalSaleOrderByRegion(string option,string ValueRange, int CurrId)
        {
            return IndeedSalesDA.getSalesOrder(Constants.USP_Indeed_SalesOrderByRegion, ValueRange,CurrId,option, true);
        }
        public static DataTable GetAllNewProduct(string option,string ValueRange, int CurrId,int Filter)
        {
            return IndeedSalesDA.getProducts(Constants.USP_Indeed_ProductDashboard,option, ValueRange, CurrId,Filter, true);
        }
        public static DataTable GetAllOrderUsage(string ValueRange)
        {
            return IndeedSalesDA.getOrderUsage(Constants.USP_Indeed_OrderUsage, ValueRange, true);
        }

        public static List<UserBE.BA_USERFILE> GetAllContactInformationDetails(string CustomColumn2)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CustomColumn2", Convert.ToString(CustomColumn2));
            return IndeedSalesDA.getUserFileCollectionItems(Constants.USP_Indeed_ContactInformation, dictionaryInstance, true);
        }
        public static List<UserBE.BA_USERFILE> GetContactInformation(int userid)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("userid", Convert.ToString(userid));
            return IndeedSalesDA.getUserFileCollectionItems(Constants.USP_ContactInformation, dictionaryInstance, true);
        }
        public static Int32 ChangePassword(int userid,string oldpass,string newpass)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("useid", Convert.ToString(userid));
            dictionaryInstance.Add("newpass", Convert.ToString(newpass));
            Dictionary<string, string> Id = new Dictionary<string, string>();
            Id.Add("Id", Convert.ToString(typeof(Int32)));

            IndeedSalesDA.Insert(Constants.USP_Indeed_ChangePassword, dictionaryInstance, ref Id, true);
            return Convert.ToInt16(Id["Id"]);
        }

        public static List<IndeedSalesBE.BA_UserAddress> GetAllCustomerAddress(int userid, string mode)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("UserId", Convert.ToString(userid));
            dictionaryInstance.Add("Mode", Convert.ToString(mode));

            return IndeedSalesDA.getUserAddressCollectionItems(Constants.USP_Indeed_GetTotalCustomerAddress, dictionaryInstance, true);
        }

        public static List<UserBE.UserDeliveryAddressBE> GetUserDeliveryAddresDetails(int id, string option)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("UserId", Convert.ToString(id));
            dictionaryInstance.Add("Option", Convert.ToString(option));

            return UserDA.getCollectionItemDelAddress(Constants.USP_Indeed_GetCustomerAddressDetails, dictionaryInstance, true);
        }
        public static List<IndeedSalesBE.BA_UserAddress> UpdateUserDeliveryAddresDetails(int id, string option, UserBE.UserDeliveryAddressBE objAddress)
        {
            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
            DictionaryInstance.Add("UserId", Convert.ToString(id));
            DictionaryInstance.Add("Option", Convert.ToString(option));
            DictionaryInstance.Add("Company", objAddress.PreDefinedColumn2);
            DictionaryInstance.Add("AddressLine1", objAddress.PreDefinedColumn3);
            DictionaryInstance.Add("AddressLine2", objAddress.PreDefinedColumn4);
            DictionaryInstance.Add("Town", objAddress.PreDefinedColumn5);
            DictionaryInstance.Add("County", objAddress.PreDefinedColumn6);
            DictionaryInstance.Add("Postcode", objAddress.PreDefinedColumn7);
            DictionaryInstance.Add("CountryId", objAddress.PreDefinedColumn8);

            return IndeedSalesDA.UpdateUserAddress(Constants.USP_Indeed_UpdateCustomerAddressDetails, DictionaryInstance,true);
        }
        public static bool DeleteUserDeliveryAddress(int id)
        {
            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
            DictionaryInstance.Add("UserId", Convert.ToString(id));
            return IndeedSalesDA.Delete(Constants.USP_Indeed_DeleteCustomerAddressDetails, DictionaryInstance, true);
        }

        public static bool UpdateMarketPreference(UserBE objBE)
        {
            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
            DictionaryInstance.Add("UserId", Convert.ToString(objBE.UserId));
            DictionaryInstance.Add("IsMarket", Convert.ToString(objBE.IsMarketing));
            return IndeedSalesDA.Update(Constants.USP_Indeed_UpdateMarketPreference, DictionaryInstance, true);
        }

        public static List<IndeedSalesBE.BA_Points> GetUsersPoint(int userid)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("UserId", Convert.ToString(userid));
            return IndeedSalesDA.getUserPointsCollectionItems(Constants.USP_Indeed_GetPointsData, dictionaryInstance, true);
        }

        public static DataTable GetPointsTransactionDetails(int userid)
        {           
            return IndeedSalesDA.getPointsTransaction(Constants.USP_Indeed_GetPointsTransactioDetails, userid, true);
        }

        public static List<IndeedSalesBE.BA_Budget> GetBudgetPerUser(int userid,int CurrId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("UserId", Convert.ToString(userid));
            dictionaryInstance.Add("CurrId", Convert.ToString(CurrId));
            return IndeedSalesDA.getBudgetCollectionItems(Constants.USP_Indeed_GetBudgetDetails, dictionaryInstance, true);
        }

        public static List<IndeedSalesBE.BA_Budget> GetBudgetUser(string emailid, int CurrId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("emailid", Convert.ToString(emailid));
            dictionaryInstance.Add("CurrId", Convert.ToString(CurrId));
            return IndeedSalesDA.getBudgetCollectionItems(Constants.USP_GetBudgetDetailsMyProfile, dictionaryInstance, true);
        }

        public static List<IndeedSalesBE.BA_Budget> GetBudgetTransactionPerUser(int userid)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("UserId", Convert.ToString(userid));
            return IndeedSalesDA.getBudgetCollectionItems(Constants.USP_Indeed_GetBudgeTransactionDetails, dictionaryInstance, true);
        }

        public static List<IndeedSalesBE.BA_Budget> GetBudgetTransactionUser(string emailid)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("emailid", Convert.ToString(emailid));
            return IndeedSalesDA.getBudgetCollectionItems(Constants.USP_GetBudgeTransaction, dictionaryInstance, true);
        }
    }
}
