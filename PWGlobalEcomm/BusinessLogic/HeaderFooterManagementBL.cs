﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    public class HeaderFooterManagementBL 
    {
        public static List<object> GetAllHeaderFooterElements(bool EmptyCache, string type)
        {
            List<object> lstAllHeaderFooterElements = new List<object>();

            //if (EmptyCache.Equals(true))
            //{
            //    if (HttpRuntime.Cache["HeaderFooterManagement"] != null)
            //        HttpRuntime.Cache.Remove("HeaderFooterManagement");
            //}
            try
            {
                //if (HttpRuntime.Cache["HeaderFooterManagement"] == null)
                //{
                string Constant = "";
                if (type.ToLower().Equals("header"))
                    Constant = Constants.USP_HeaderConfiguration_SAED;
                else
                    Constant = Constants.USP_FooterConfiguration_SAED;

                    Dictionary<string, string> dictParams = new Dictionary<string, string>();
                    dictParams.Add("Action", Convert.ToInt16(DBAction.Select).ToString());
                    dictParams.Add("CreatedBy", "0");
                    lstAllHeaderFooterElements = HeaderFooterManagementDA.getCollectionItem(Constant, dictParams, true, Constants.Entity_HeaderFooterManagement);
                //    HttpRuntime.Cache.Insert("HeaderFooterManagement", lstAllHeaderFooterElements, 
                //        new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/HeaderFooterManagementCaching.txt")));
                //}
                //else
                //    lstAllHeaderFooterElements = (List<object>)HttpRuntime.Cache["HeaderFooterManagement"];
            }
            catch (Exception ex)
            {

            }
            return lstAllHeaderFooterElements;
        }

        public static void HeaderFooterConfiguration(HeaderFooterManagementBE HF, DBAction action, string type) 
        {
            string spName = "" ;
            if (type.ToLower().Equals("header"))
                spName = Constants.USP_HeaderConfiguration_SAED;
            else
                spName = Constants.USP_FooterConfiguration_SAED;
                

            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("Action", Convert.ToInt16(action).ToString());
            Dictionary<string, string> Id = new Dictionary<string, string>();
            switch (action)
            {
                case DBAction.Insert:
                    {
                        dictParams.Add("ConfigSectionName", HF.ConfigSectionName);
                        dictParams.Add("ConfigElementName", HF.ConfigElementName);
                        dictParams.Add("ConfigElementId", HF.ConfigElementID);
                        HeaderFooterManagementDA.Insert(spName, dictParams, ref Id, true);
                        break;
                    }
                case DBAction.Delete:
                    {
                        HeaderFooterManagementDA.Delete(spName, dictParams, ref Id, true);
                    }
                    break;
                default:
                    break;
            }
            //GetAllHeaderFooterElements(true, Const);
        }
    }
}
