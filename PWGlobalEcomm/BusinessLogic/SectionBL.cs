﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;

namespace PWGlobalEcomm.BusinessLogic
{
    public class SectionBL
    {

        public static List<SectionBE> GetAllSectionDetails()
        {
            return SectionDA.getCollectionItem(Constants.USP_GetSectionDetails, null, true);
        }

        public static List<SectionBE> GetAllDetails(string sp, SectionBE objBE, string ActionType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("SectionId", objBE.SectionId.ToString());
            dictionaryInstance.Add("ActionType", ActionType);
            dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
            return SectionDA.getCollectionItem(sp, dictionaryInstance, true);
        }

        public static bool IsSectionExists(string strSectionName)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("SectionName", Convert.ToString(strSectionName));

            return SectionDA.IsExists(Constants.USP_IsSectionExists, dictionaryInstance, true);
        }

        public static Int16 InsertUpdateSectionDetails(SectionBE objBE)
        {
            Int16 intSectionId = 0;
            try
            {
                Dictionary<string, string> DictionaryOutParameterInstance = new Dictionary<string, string>();
                DictionaryOutParameterInstance.Add("ReturnId", typeof(Int16).ToString());
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("SectionName",Convert.ToString(objBE.SectionName));
                dictionaryInstance.Add("IconImageAltText",Convert.ToString(objBE.IconImageAltText));
                dictionaryInstance.Add("BannerImageAltText",Convert.ToString(objBE.BannerImageAltText));
                dictionaryInstance.Add("SectionDescription",Convert.ToString(objBE.SectionDescription));
                dictionaryInstance.Add("PageTitle",Convert.ToString(objBE.PageTitle));
                dictionaryInstance.Add("MetaDescription",Convert.ToString(objBE.MetaDescription));
                dictionaryInstance.Add("MetaKeyword",Convert.ToString(objBE.MetaKeyword));
                dictionaryInstance.Add("ProCatIds",Convert.ToString(objBE.ProductIdCSV));
                dictionaryInstance.Add("IconImageExtension",Convert.ToString(objBE.IconImageExtension));
                dictionaryInstance.Add("BannerImageExtension",Convert.ToString(objBE.BannerImageExtension));
                dictionaryInstance.Add("SectionId",Convert.ToString(objBE.SectionId));
                dictionaryInstance.Add("Enteredby",Convert.ToString( objBE.CreatedBy));
                dictionaryInstance.Add("LanguageId",Convert.ToString(objBE.LanguageId));
                dictionaryInstance.Add("IsActive",Convert.ToString(objBE.IsActive.ToString().ToLower() == "true" ? "1" : "0"));
                dictionaryInstance.Add("ActionType",Convert.ToString(objBE.ActionType));
                SectionDA.Insert(Constants.USP_InsertUpdateSection, dictionaryInstance, ref DictionaryOutParameterInstance, true);
                foreach (KeyValuePair<string, string> item in DictionaryOutParameterInstance)
                {
                    if (item.Key == "ReturnId")
                        intSectionId = Convert.ToInt16(item.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return intSectionId;
        }

        public static List<SectionBE> GetAllSectionCategoriesDetails(string sp, SectionBE objBE, string ActionType, Int16 CurrencyId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CurrencyId", CurrencyId.ToString());
            dictionaryInstance.Add("CategoryId", objBE.CatalogueId.ToString());
            dictionaryInstance.Add("ActionType", ActionType);
            dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
            return SectionDA.getCollectionItem(sp, dictionaryInstance, true);
        }


        public static List<SectionBE> GetAllSectionProductsDetails(string sp, SectionBE objBE) 
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CurrencyId", objBE.CurrencyId.ToString());
            dictionaryInstance.Add("ActionType", objBE.ActionType.ToString());
            dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
            dictionaryInstance.Add("SectionId", objBE.SectionId.ToString());
            #region commented 
            //dictionaryInstance.Add("CategoryName", Convert.ToString(objBE.CategoryName));
            //dictionaryInstance.Add("SubCategoryName",Convert.ToString(objBE.subCategoryname));
            //dictionaryInstance.Add("SubSubCategoryName",Convert.ToString(objBE.subSubcategoryname));
            #endregion
            dictionaryInstance.Add("ProductIdCSV",Convert.ToString(objBE.ProductIdCSV)); 
            return SectionDA.getCollectionItem(sp, dictionaryInstance, true);
        }
       
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 4-08-16
        /// Scope   : Gets the section image and banner image path & extension
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="strSectionName"></param>
        /// <returns>returns list of SectionBE objects</returns>
        public static List<SectionBE> GetSectionBannerImageExtension(Int16 intLanguageId, string strSectionName)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(intLanguageId));
                dictionaryInstance.Add("SectionName", Convert.ToString(strSectionName));
                return SectionDA.getCollectionItem(Constants.USP_GetSectionBannerImageExtension, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 27-10-15
        /// Scope   : gets products data  for the Home page
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="strCategoryName"></param>
        /// <param name="strSubCategoryName"></param>
        /// <returns>returns list of product objects as per section</returns>
        /// /*User Type*/ Modified added parameters
        public static List<ProductBE> GetHomePageProductSectionData(SectionBE.CustomSection objCustomSectionBE, Int16 UserTypeID)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("SectionIds", Convert.ToString(objCustomSectionBE.SectionIds));
                dictionaryInstance.Add("LanguageId", Convert.ToString(objCustomSectionBE.LanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objCustomSectionBE.CurrencyId));
                dictionaryInstance.Add("toprow", Convert.ToString(objCustomSectionBE.toprow));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));
                return SectionDA.GetCustomizeSectionProduct(Constants.Usp_GetSectionProductsForhomepage, dictionaryInstance, true);
            }
            catch (Exception ex) 
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        public static void InsertUpdateHandlingFeesSection(SectionBE.HandlingFeesSection objHandlingFeesSectionBE,int UserId ,string InsUpdFlag)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("SectionId", Convert.ToString(objHandlingFeesSectionBE.SectionId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(objHandlingFeesSectionBE.CurrencyId));
                dictionaryInstance.Add("HandlingFees", Convert.ToString(objHandlingFeesSectionBE.HandlingFees));
                dictionaryInstance.Add("PerOrderStatus", Convert.ToString(objHandlingFeesSectionBE.PerOrderStatus));
                dictionaryInstance.Add("IsActive", Convert.ToString(objHandlingFeesSectionBE.IsActive));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(UserId));
                dictionaryInstance.Add("CreatedDate", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss"));
                dictionaryInstance.Add("ModifiedBy", Convert.ToString(UserId));
                dictionaryInstance.Add("ModifiedDate", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss"));
                dictionaryInstance.Add("InsUpdFlag", Convert.ToString(InsUpdFlag));
                SectionDA.Insert(Constants.usp_InsertUpdateSectionHandlingFees, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static SectionBE GetAllSectionRelatedDetails(string sp, SectionBE objBE, string ActionType)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("SectionId", objBE.SectionId.ToString());
            dictionaryInstance.Add("ActionType", ActionType);
            dictionaryInstance.Add("LanguageId", objBE.LanguageId.ToString());
            return SectionDA.getCollectionItems(sp, dictionaryInstance, true);
        }
    }
}
