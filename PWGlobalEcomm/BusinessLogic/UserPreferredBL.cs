﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    class UserPreferredBL
    {
        public static void InsertUpdateUserPreference(string sp, bool IsStoreConnectionString, UserPreferredBE objUserPreferredBE)
        {
            try
            {
                Dictionary<string, int> DictionaryInstance = new Dictionary<string, int>();
                DictionaryInstance.Add("UserId",objUserPreferredBE.UserId);
                DictionaryInstance.Add("PreferredLanguageId", objUserPreferredBE.PreferredLanguageId);
                DictionaryInstance.Add("PreferredCurrencyId",objUserPreferredBE.PreferredCurrencyId);
                UserPreferredDA.InsertUserPreferred(sp, DictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public static List<UserPreferredBE.UserTypeCurrency> GetCurrencyUserTypeWise(int UserTypeId)
        {
            List<UserPreferredBE.UserTypeCurrency> GetCurrencyUserTypeWise = null;
            try
            {
                //if (HttpRuntime.Cache["LanguageMaster"] == null)
                //{
                bool IsStoreConnectionString = true;
                //GetStoreCurrencyDetails = StoreCurrencyDA.getCollectionItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserTypeId", Convert.ToString(UserTypeId));
                GetCurrencyUserTypeWise = UserPreferredDA.GetCurrencyUserTypeWise(Constants.usp_GetCurrencyUserTypeWise, dictionaryInstance, IsStoreConnectionString);
                //HttpRuntime.Cache.Insert("LanguageMaster", getAllLanguages, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/LanguageMasterCaching.txt")));
                //}
                //else
                //    getAllLanguages = (List<LanguageBE>)HttpRuntime.Cache["LanguageMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return GetCurrencyUserTypeWise;
        }

        public static List<UserPreferredBE> GetUserPreferred(int UserId)
        {
            List<UserPreferredBE> GetUserPreferred = null;
            try
            {
                //if (HttpRuntime.Cache["LanguageMaster"] == null)
                //{
                bool IsStoreConnectionString = true;
                //GetStoreCurrencyDetails = StoreCurrencyDA.getCollectionItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserId", Convert.ToString(UserId));
                GetUserPreferred = UserPreferredDA.GetUserPreferred(Constants.usp_GetUserPreferredUserIdWise, dictionaryInstance, IsStoreConnectionString);
                //HttpRuntime.Cache.Insert("LanguageMaster", getAllLanguages, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/LanguageMasterCaching.txt")));
                //}
                //else
                //    getAllLanguages = (List<LanguageBE>)HttpRuntime.Cache["LanguageMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return GetUserPreferred;
        }
    }
