﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class RegistrationCustomFieldBL
    {
        /// <summary>
        /// gets Registration Custom Fields details
        /// </summary>
        /// <returns>returns RegistrationCustomFieldBE objects</returns>
        public static List<RegistrationCustomFieldBE> GetAllCustomFields()
        {
            List<RegistrationCustomFieldBE> getRegistrationCustomField = null;
            try
            {
                getRegistrationCustomField = RegistrationCustomFieldDA.getCollectionItem(Constants.USP_GetAllCustomFields, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getRegistrationCustomField;
        }

        /// <summary>
        /// gets Registration Field Type
        /// </summary>
        /// <returns>returns RegistrationCustomFieldBE objects</returns>
        public static List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE> GetFieldTypeMasterDetails()
        {
            List<RegistrationCustomFieldBE.RegistrationFeildTypeMasterBE> getFieldTypeMasterDetails = null;
            try
            {
                getFieldTypeMasterDetails = RegistrationCustomFieldDA.getFieldTypeMasterCollectionItem(Constants.USP_FieldTypeMasterDetails, null, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getFieldTypeMasterDetails;
        }

        /// <summary>
        /// Add/Edit/Delete operation of custom fields
        /// </summary>
        /// <returns>returns bool value</returns>
        public static bool AEDRegistrationCustomField(Dictionary<string, string> dictionaryInstance)
        {
            bool result;
            try
            {
                result = RegistrationCustomFieldDA.Insert(Constants.USP_RegistrationCustomField_AED, dictionaryInstance, true);
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
                result = false;
            }
            return result;
        }

        #region /*User Type*/
        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   22 June 2016
        /// Scope   :   Add/Edit/Delete Custom Fields for User Types
        /// </summary>
        /// <returns>returns bool value </returns>
        public static bool AEDUserTypeRegistrationCustomFields(Dictionary<string, string> dictionaryInstance)
        {
            bool bresult;
            try
            {
                Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
                SequenceInstance.Add("IsSequenceSet", Convert.ToString(typeof(bool)));

                bresult = RegistrationCustomFieldDA.Insert(Constants.USP_RegistrationUserTypeCustomField_AED, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                bresult = false;
            }
            return bresult;
        }

        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   22 June 2016
        /// Scope   :   To Get All Custom Fields by User Type ID
        /// </summary>
        /// <returns>returns List<RegistrationCustomFieldBE></returns>
        public static List<RegistrationCustomFieldBE> GetAllCustomFieldsByUserTypeID(Dictionary<string, string> dictionaryInstance)
        {
            List<RegistrationCustomFieldBE> getRegistrationCustomField = null;
            try
            {
                getRegistrationCustomField = RegistrationCustomFieldDA.GetAllCustomFieldsByUserTypeID(Constants.USP_GetAllCustomFieldsByUserTypeID, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getRegistrationCustomField;
        }

        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   30 June 2016
        /// Scope   :   To Check for Existing Custon Field Name
        /// </summary>
        /// <returns>returns bool value </returns>
        public static bool CheckForExistingCustomFieldName(string customFieldName, string UserTypeID)
        {
            bool bResult;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("CustomFieldName", customFieldName);
                DictionaryInstance.Add("UserTypeID", UserTypeID);

                bResult = RegistrationCustomFieldDA.CheckForExistingCustomFieldName(Constants.USP_CheckForExistingCustomFieldName, DictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                bResult = false;
            }
            return bResult;
        }

        /// <summary>
        /// Author  :   SNEHAL JADHAV
        /// Date    :   07 Sep 2016
        /// Scope   :   To Check for Existing Login Id
        /// </summary>
        /// <returns>returns bool value </returns>
        public static bool CheckForExistingCustomLoginId(string UserTypeID,int RegId)
        {
            bool bResult;
            try
            {
                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("UserTypeID", UserTypeID);
                DictionaryInstance.Add("RegId", Convert.ToString(RegId));
                bResult = RegistrationCustomFieldDA.CheckForExistingCustomFieldName(Constants.USP_CheckForExistingCustomLoginId, DictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                bResult = false;
            }
            return bResult;
        }

        public static int ValidateRegistrationCustomFields(object value, string RegistrationFieldsConfigurationId)
        {
            try
            {
                Dictionary<string, string> dictionaryinstance = new Dictionary<string, string>();
                dictionaryinstance.Add("RegistrationFieldsConfigurationId", RegistrationFieldsConfigurationId);
                dictionaryinstance.Add("CustomFieldValue", Convert.ToString(value));

                Dictionary<string, string> RegistrationCustomFieldsValidated = new Dictionary<string, string>();
                RegistrationCustomFieldsValidated.Add("IsRegistrationCustomFieldsValidated", Convert.ToString(typeof(Int16)));

                RegistrationCustomFieldDA.ValidateRegistrationCustomFields(Constants.USP_ValidateRegistrationCustomFields, dictionaryinstance, ref RegistrationCustomFieldsValidated, true);
                return Convert.ToInt16(Convert.ToString(RegistrationCustomFieldsValidated["IsRegistrationCustomFieldsValidated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }
        }
        #endregion

        #region Added By Snehal to check Custom Login Field while editing Profile - 14 10 2016
        public static int ValidateRegistrationCustomFieldsEditProfile(object value, string RegistrationFieldsConfigurationId,int userid)
        {
            try
            {
                Dictionary<string, string> dictionaryinstance = new Dictionary<string, string>();
                dictionaryinstance.Add("RegistrationFieldsConfigurationId", RegistrationFieldsConfigurationId);
                dictionaryinstance.Add("CustomFieldValue", Convert.ToString(value));
                dictionaryinstance.Add("userid", Convert.ToString(userid));

                Dictionary<string, string> RegistrationCustomFieldsValidated = new Dictionary<string, string>();
                RegistrationCustomFieldsValidated.Add("IsRegistrationCustomFieldsValidated", Convert.ToString(typeof(Int16)));

                RegistrationCustomFieldDA.ValidateRegistrationCustomFields(Constants.USP_ValidateRegistrationCustomFieldsEditProfile, dictionaryinstance, ref RegistrationCustomFieldsValidated, true);
                return Convert.ToInt16(Convert.ToString(RegistrationCustomFieldsValidated["IsRegistrationCustomFieldsValidated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }
        }
        #endregion
    }
}
