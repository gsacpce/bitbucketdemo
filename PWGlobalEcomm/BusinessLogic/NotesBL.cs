﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class NotesBL
    {
        //public static List<StoreBE.StoreCurrencyBE> GetStoreCurrencyDetails()
        //{
        //    List<StoreBE.StoreCurrencyBE> GetStoreCurrencyDetails = null;
        //    try
        //    {
        //        //if (HttpRuntime.Cache["LanguageMaster"] == null)
        //        //{
        //        bool IsStoreConnectionString = true;
        //        //GetStoreCurrencyDetails = StoreCurrencyDA.getCollectionItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
        //        GetStoreCurrencyDetails = StoreDA.getStoreCurrencyItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
        //        //HttpRuntime.Cache.Insert("LanguageMaster", getAllLanguages, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/LanguageMasterCaching.txt")));
        //        //}
        //        //else
        //        //    getAllLanguages = (List<LanguageBE>)HttpRuntime.Cache["LanguageMaster"];
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }

        //    return GetStoreCurrencyDetails;
        //}

        /// <summary>
        /// Update Store Currency Detail
        /// </summary>
        /// <param name="StoreLanguageDetail"></param>
        /// <returns>returns bool value</returns>
        public static bool InsertUpdateNotes(Dictionary<string, string> StoreDetail, bool IsStoreConnectionString = false)
        {
            bool IsSaved = false;
            try
            {
                IsSaved = NotesDA.Insert(Constants.usp_InsertUpdateNotes, StoreDetail, IsStoreConnectionString);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsSaved;
        }

        public static string GetNoteNo()
        {
            string strNoteNo = string.Empty;
            try
            {
                //if (HttpRuntime.Cache["LanguageMaster"] == null)
                //{
                bool IsStoreConnectionString = true;
                //GetStoreCurrencyDetails = StoreCurrencyDA.getCollectionItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
                strNoteNo = NotesDA.getNoteNo(Constants.usp_GetNoteNo, null, IsStoreConnectionString);
                //HttpRuntime.Cache.Insert("LanguageMaster", getAllLanguages, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/LanguageMasterCaching.txt")));
                //}
                //else
                //    getAllLanguages = (List<LanguageBE>)HttpRuntime.Cache["LanguageMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return strNoteNo;
        }


        public static List<NoteBE> GetNoteDetails()
        {
            List<NoteBE> GetNoteDetails = null;
            try
            {
                //if (HttpRuntime.Cache["LanguageMaster"] == null)
                //{
                bool IsStoreConnectionString = true;
                //GetStoreCurrencyDetails = StoreCurrencyDA.getCollectionItem(Constants.usp_GetStoreCurrencyDetails, null, IsStoreConnectionString);
                GetNoteDetails = NotesDA.GetNoteDetails(Constants.usp_GetNoteDetails, null, IsStoreConnectionString);
                //HttpRuntime.Cache.Insert("LanguageMaster", getAllLanguages, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/LanguageMasterCaching.txt")));
                //}
                //else
                //    getAllLanguages = (List<LanguageBE>)HttpRuntime.Cache["LanguageMaster"];
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }

            return GetNoteDetails;
        }


    }
}
