﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessLogic
{
    public class MenuBL
    {
        /// <summary>
        /// gets all menu details
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of MenuBE objects</returns>
        public static List<MenuBE> GetAllMenuDetails()
        {
            List<MenuBE> MenuBEInstance = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RoleId", Convert.ToString(1));
                MenuBEInstance = MenuDA.getCollectionItem(Constants.USP_GetMenuDetails, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return MenuBEInstance;
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 20-08-16
        /// Scope   : list of MenuBE objects
        /// </summary>
        /// <returns>list of MenuBE objects</returns>
        public static List<MenuBE> GetAllStoreMenuDetails(Int16 intRoleId,bool IsMCP)
        {
            List<MenuBE> MenuBEInstance = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RoleId", Convert.ToString(intRoleId));
                dictionaryInstance.Add("IsMCP",Convert.ToString(IsMCP));
                //MenuBEInstance = MenuDA.getCollectionItem(Constants.USP_GetMenuDetails, dictionaryInstance, true);
                MenuBEInstance = MenuDA.getCollectionItem(Constants.USP_GetMenuDetails_New, dictionaryInstance, false);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return MenuBEInstance;
        }
    }
}
