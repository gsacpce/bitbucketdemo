﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PWGlobalEcomm.BusinessLogic
{
    public partial class CategoryBL
    {
        /// <summary>
        /// gets all categories details
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of CategoryBE objects</returns>
        public static List<CategoryBE> GetAllCategories()
        {

            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CategoryId", Convert.ToString(0));
                return CategoryDA.getCollectionItem(Constants.USP_GetAllCategoriesWithProductCount, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// gets all categories details
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of CategoryBE objects</returns>
        public static List<CategoryBE> GetAllCategoriesbyDefaultLanguage()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                //dictionaryInstance.Add("CategoryId", Convert.ToString(0));
                return CategoryDA.getCollectionItem(Constants.USP_GetCategoriesforImportExport, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 18-09-15
        /// Scope   : Get All Category / Sub Category Names for All the Languages
        /// Return  : Returns a dictionary object containing LanguageName as key & List of category names respective to language
        /// </summary>
        /// 
        public static Dictionary<int, List<CategoryBE>> GetAllCategoriesAllLanguages()
        {
            Dictionary<int, List<CategoryBE>> DictAllCategoriesAllLanguages = new Dictionary<int, List<CategoryBE>>();
            try
            {
                List<StoreBE.StoreLanguageBE> lstStoreLangauages = StoreBL.GetStoreDetails().StoreLanguages;
                foreach (StoreBE.StoreLanguageBE storeLanguage in lstStoreLangauages)
                {
                    List<CategoryBE> lstCategoriesLanguagewise = GetCategoryDetailsLanguageWise(storeLanguage.LanguageId);
                    DictAllCategoriesAllLanguages.Add(storeLanguage.LanguageId, lstCategoriesLanguagewise);
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
            return DictAllCategoriesAllLanguages;
        }

        /// <summary>
        /// gets categories listing
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>returns list of CategoryBE objects</returns>

        public static List<CategoryBE> ListCategories()
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                return CategoryDA.getCollectionItem(Constants.USP_GetCategoryList, dictionaryInstance, true);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 22-07-15
        /// Scope   : insert categories into tables
        /// insert category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category id after insert</returns>
        public static Int16 InsertCategory(CategoryBE category)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CategoryId", Convert.ToString(category.CategoryId));
                dictionaryInstance.Add("Name", Convert.ToString(category.CategoryName));
                dictionaryInstance.Add("ParentCategoryId", Convert.ToString(category.ParentCategoryId));
                dictionaryInstance.Add("ImageExtension", category.ImageExtension);
                dictionaryInstance.Add("Description", GlobalFunctions.RemoveSanitisedPrefixes(category.DescriptionText));
                dictionaryInstance.Add("BannerText", category.BannerText);
                dictionaryInstance.Add("BannerLink", category.BannerLink);
                dictionaryInstance.Add("BannerExtension", category.BannerImageExtension);
                dictionaryInstance.Add("MenuImageExtension", category.MenuImageExtension); // added by vikram to save menu exntension
                dictionaryInstance.Add("MenuMouseoverExtension", category.MenuMouseoverExtension);
                dictionaryInstance.Add("LanguageId", Convert.ToString(category.LanguageId));
                dictionaryInstance.Add("CreatedBy", Convert.ToString(category.CreatedBy));
                dictionaryInstance.Add("ModifiedBy", Convert.ToString(category.ModifiedBy));
                dictionaryInstance.Add("MetaTitle", Convert.ToString(category.MetaTitle));
                dictionaryInstance.Add("MetaDescription", Convert.ToString(category.MetaDescription));
                dictionaryInstance.Add("MetaKeywords", Convert.ToString(category.MetaKeywords));
                dictionaryInstance.Add("Action", category.Action);
                dictionaryInstance.Add("RelatedCategoryId", Convert.ToString(category.RelatedCategoryId));

                Dictionary<string, string> CategoryId = new Dictionary<string, string>();
                CategoryId.Add("CatId", Convert.ToString(typeof(Int16)));

                CategoryDA.Insert(Constants.USP_Category_AED, dictionaryInstance, ref CategoryId, true);
                if (category.Action == "Edit")
                {
                    return category.CategoryId;
                }
                else
                {
                    return Convert.ToInt16(CategoryId["CatId"]);
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return 0;
            }
        }


        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 23-07-15
        /// Scope   : to get the category details
        /// get category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category object after getting category details</returns>


        internal static CategoryBE GetCategoryDetails(int CategoryId, int LanguageId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
                List<CategoryBE> categories = CategoryDA.getCollectionItem(Constants.USP_GetAllCategoriesWithProductCount, dictionaryInstance, true);
                CategoryBE category = new CategoryBE();
                category = categories.Where(c => c.LanguageId == LanguageId).FirstOrDefault();
                return category;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 23-07-15
        /// Scope   : to get the category details
        /// get category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category object after getting category details</returns>


        internal static List<CategoryBE> GetCategoryDetailsLanguageWise(int LanguageId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                List<CategoryBE> categories = CategoryDA.getCollectionItem(Constants.USP_GetAllCategoriesLanguageWise, dictionaryInstance, true);
                return categories;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }


        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 23-07-15
        /// Scope   : to set the category sequence
        /// insert category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category object after getting category details</returns>
        public static bool SetCategorySequence(DataTable dtSequence)
        {
            bool IsSequenceSet = false;
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("DataW", dtSequence);
                Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
                SequenceInstance.Add("IsSequenceSet", Convert.ToString(typeof(bool)));
                CategoryDA.SetSequence(Constants.USP_SetCategorySequence, dictionaryInstance, ref SequenceInstance, true);
                IsSequenceSet = Convert.ToBoolean(SequenceInstance["IsSequenceSet"]);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsSequenceSet;
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 23-07-15
        /// Scope   : to Assign product to the category
        /// assign category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category list object after getting category details</returns>
        public static List<ProductBE> AssignProductToCategory(int CategoryId, string ProductIds, string Action, int DefaultLanguage, ref int Result, int UserId)
        {

            List<ProductBE> Products = new List<ProductBE>();
            try
            {
                Dictionary<string, string> _Result = new Dictionary<string, string>();
                _Result.Add("Result", Convert.ToString(typeof(Int16)));

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
                dictionaryInstance.Add("ProductIds", ProductIds);
                dictionaryInstance.Add("Action", Action);
                dictionaryInstance.Add("LanguageId", Convert.ToString(DefaultLanguage));
                dictionaryInstance.Add("UserId", Convert.ToString(UserId));

                Products = CategoryDA.AssignProductToCategory(Constants.USP_AssignProductToCategory, CategoryId, ProductIds, Action, DefaultLanguage, UserId, dictionaryInstance, ref _Result, true);
                //  Result = Convert.ToInt16(_Result["Result"]);
                return Products;
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
                return Products = null;
            }


        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 26-07-15
        /// Scope   : to get categories in reporting excel file
        /// get categories
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category list object after getting category details</returns>
        internal static List<CategoryBE> GetCategoriesForReporting()
        {
            List<CategoryBE> Categories = new List<CategoryBE>();
            Categories = CategoryDA.getCollectionItem(Constants.USP_GetRelationIdForCategories, null, true);
            return Categories;
        }

        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 29-07-15
        /// Scope   : to check the categories name exist in the categorie hierarchy
        /// Check category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category list object after getting category details</returns>
        public static int CheckCategoryExists(string CategoryName, int ParentCategoryId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CategoryName", CategoryName);
            // dictionaryInstance.Add("ExcludeCategoryId", Convert.ToString(ExcludeCategoryId));
            dictionaryInstance.Add("ParentCategoryId", Convert.ToString(ParentCategoryId));
            return CategoryDA.getIntValue(Constants.USP_CheckCategoryExists, dictionaryInstance, true);
        }


        /// <summary>
        /// Author  : Farooq Shaikh
        /// Date    : 29-07-15
        /// Scope   : to set status the categories status to active inactive
        /// set status
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category list object after getting category details</returns>
        internal static bool SetCategoryStatus(int CategoryId, bool Status)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
            dictionaryInstance.Add("Status", Convert.ToString(Status));



            return CategoryDA.Insert(Constants.USP_MarkCategoryActiveInactive, dictionaryInstance, true);
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 04-01-17
        /// Scope   : to set status the categories status to active inactive
        /// set status
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category list object after getting category details</returns>
        internal static bool DeleteCategories(int CategoryId)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
            return CategoryDA.Insert(Constants.USP_DeleteCategories, dictionaryInstance, true);
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-16
        /// Scope   : gets all Sub categories of a category
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <param name="strCategoryName"></param>
        /// <param name="strSubCategoryName"></param>
        /// <returns>returns list of CategoryBE objects</returns>
        public static List<CategoryBE> GetSubCategories(Int16 intLanguageId, Int16 intCurrencyId, string strCategoryName, string strSubCategoryName)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(intLanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(intCurrencyId));
                dictionaryInstance.Add("CategoryName", Convert.ToString(strCategoryName));
                dictionaryInstance.Add("SubCategoryName", Convert.ToString(strSubCategoryName));
                return CategoryDA.getCollectionItem(Constants.USP_GetSubCategories, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 21-07-16
        /// Scope   : Gets all menu details
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <returns>returns list of CategoryBE objects</returns>
        /// /*User Type*/ added UserTypeID
        public static List<CategoryBE> GetAllCategories(Int16 intLanguageId, Int16 intCurrencyId, Int16 UserTypeID)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(intLanguageId));
                dictionaryInstance.Add("CurrencyId", Convert.ToString(intCurrencyId));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(UserTypeID));
                return CategoryDA.getCollectionItem(Constants.USP_GetAllCategories, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  : Sachin Chauhan
        /// Date    : 21-07-16
        /// Scope   : Gets all Category Names Language Wise
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <returns>returns list of CategoryBE objects</returns>
        public static List<CategoryBE> GetAllCategories(Int16 intLanguageId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(intLanguageId));
                return CategoryDA.getCollectionItem(Constants.USP_GetAllCategoriesLanguageWise, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
        /// <summary>
        /// Author  : Anoop Gupta
        /// Date    : 23-07-16
        /// Scope   : Gets the category image and banner image path & extension
        /// </summary>
        /// <param name="intLanguageId"></param>
        /// <param name="intCurrencyId"></param>
        /// <returns>returns list of CategoryBE objects</returns>
        public static List<CategoryBE> GetCategoryBannerImageExtension(Int16 intLanguageId, string strCategoryName, string strSubCategoryName, string strSubSubCategoryName)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(intLanguageId));
                dictionaryInstance.Add("CategoryName", Convert.ToString(strCategoryName));
                dictionaryInstance.Add("SubCategoryName", Convert.ToString(strSubCategoryName));
                dictionaryInstance.Add("SubSubCategoryName", Convert.ToString(strSubSubCategoryName));
                return CategoryDA.getCollectionItem(Constants.USP_GetCategoryBannerImageExtension, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static CategoryBE GetCategoryDetails(int CategoryId, int SubCategoryId, int SubSubCategoryId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CategoryId", Convert.ToString(CategoryId));
                dictionaryInstance.Add("SubCategoryId", Convert.ToString(SubCategoryId));
                dictionaryInstance.Add("SubSubCategoryId", Convert.ToString(SubSubCategoryId));

                return (CategoryBE)CategoryDA.getItem(Constants.USP_GetRelatedIdbyCatSubCatSubSubCat, dictionaryInstance, Constants.Entity_CategoryBE, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
        public static bool SetProductSequence(DataTable dtProduct, ref bool Result, bool IsStoreConnectionString, string spName)
        {
            try
            {
                return CategoryDA.SetProductSequence(dtProduct, ref Result, true, Constants.USP_SetProductSequence);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        public static List<CategoryBE> GetAllCategoriesList(Int32 UserTypeId)
        {
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("CategoryId", Convert.ToString(0));
                dictionaryInstance.Add("UserTypeId", Convert.ToString(UserTypeId));
                return CategoryDA.getCollectionItem(Constants.USP_GetAllCategoriesWithProductCountList, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }
    }
}

