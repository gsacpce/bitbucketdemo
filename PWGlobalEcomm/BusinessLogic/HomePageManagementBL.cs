﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using PWGlobalEcomm.DataAccess;

namespace PWGlobalEcomm.BusinessLogic
{

    public class HomePageManagementBL
    {
        public static List<object> GetAllHomePageElements(bool EmptyCache)
        {
            List<object> lstAllHomePageElements = new List<object>();

            /*if (EmptyCache.Equals(true))
            {
                if (HttpRuntime.Cache["HomePageManagement"] != null)
                    HttpRuntime.Cache.Remove("HomePageManagement");
            }*/

            try
            {
               // if (HttpRuntime.Cache["HomePageManagement"] == null)
               // {
                    Dictionary<string, string> dictParams = new Dictionary<string, string>();
                    dictParams.Add("Action", Convert.ToInt16(DBAction.Select).ToString());
                    dictParams.Add("LanguageId", GlobalFunctions.GetLanguageId().ToString() );

                    lstAllHomePageElements = CommonDA.getCollectionItem(Constants.USP_HomePageConfiguration_SAED, dictParams, true, Constants.Entity_HomePageManagement);


                    //HttpRuntime.Cache.Insert("HomePageManagement", lstAllHomePageElements, new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/Caching Files/HomePageManagementCaching.txt")));
                //}
                //else
                //   lstAllHomePageElements = (List<object>)HttpRuntime.Cache["HomePageManagement"];
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }

            return lstAllHomePageElements;
        }

        public static void HomePageConfiguration_SAED(HomePageManagementBE HomePage, DBAction action)
        {
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("Action", Convert.ToInt16(action).ToString());

            Dictionary<string, string> HomePageId = new Dictionary<string, string>();
            HomePageId.Add("NewSSId", Convert.ToString(typeof(Int16)));

            switch (action)
            {
                //case Action.Select:
                //    {

                //    }
                //    break;
                case DBAction.Insert:
                    {
                        dictParams.Add("ConfigSectionName", HomePage.ConfigSectionName);
                        dictParams.Add("ConfigElementName", HomePage.ConfigElementName);
                        dictParams.Add("ConfigElementId", Convert.ToString(HomePage.ConfigElementID));

                        CommonDA.Insert(Constants.USP_HomePageConfiguration_SAED, dictParams, true);
                        break;
                    }
                case DBAction.Delete:
                    {
                        CommonDA.Delete(Constants.USP_HomePageConfiguration_SAED, dictParams, true);
                    }
                    break;
                default:
                    break;
            }

            GetAllHomePageElements(true);
            //return Convert.ToInt16(SlideShowId["NewSSId"].ToString());
        }
        public static List<HomePageManagementBE> GetAllHomePageContentDetails()
        {
            List<HomePageManagementBE> HomePageContentList = new List<HomePageManagementBE>();
            var HomePageContentListObj = CommonDA.getCollectionItem(Constants.USP_GETAllHomePageContentListing, null, true, Constants.Entity_HomePageManagement);
            if (HomePageContentListObj != null)
            {
                foreach (var ObjHomePageContent in HomePageContentListObj)
                {
                    HomePageManagementBE HomePageContent = new HomePageManagementBE();
                    HomePageContent = (HomePageManagementBE)ObjHomePageContent;
                    HomePageContentList.Add(HomePageContent);
                }
            
            }
            
            return HomePageContentList;

        }
        public static HomePageManagementBE GetAllHomePageContentDetails(string Title)
        {
            HomePageManagementBE HomePageContent = new HomePageManagementBE();
            Dictionary<string, string> Dinstance = new Dictionary<string, string>();
            Dictionary<string, string> InstanceOut = new Dictionary<string, string>();
            Dinstance.Add("Title",Title);
            var HomePageContentListObj = CommonDA.getItem(Constants.USP_getAllContentItemsStaticImage,Dinstance,ref InstanceOut,Constants.Entity_HomePageManagement,true);
            if (HomePageContentListObj != null)
            {
               HomePageContent = (HomePageManagementBE)HomePageContentListObj;
            }
            return HomePageContent;
        }

        public static Int16 HomePageStaticContentAED(HomePageManagementBE HomePageContent, DBAction action)
        {

            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                dictParams.Add("Action", Convert.ToInt16(action).ToString());
                Dictionary<string, string> StaticImageId = new Dictionary<string, string>();
                StaticImageId.Add("NewSSId", Convert.ToString(typeof(Int16)));

                switch (action)
                {
                    //case Action.Select:
                    //    {

                    //    }
                    //    break;
                    case DBAction.Insert:
                        {


                            dictParams.Add("Title", HomePageContent.Title == null ? "" : HomePageContent.Title);
                            dictParams.Add("ImageExtension", HomePageContent.ImageExtension == null ? "" : HomePageContent.ImageExtension);
                            dictParams.Add("URL", HomePageContent.URL == null ? "#" : HomePageContent.URL);
                            dictParams.Add("ContentType", HomePageContent.ContentType == null ? "" : HomePageContent.ContentType);
                            dictParams.Add("Content", HomePageContent.Content == null ? "" : HomePageContent.Content);
                            dictParams.Add("Width", HomePageContent.Width == null ? "" : Convert.ToString(HomePageContent.Width));
                            dictParams.Add("WidthUnit", HomePageContent.WidthUnit == null ? "" : HomePageContent.WidthUnit);
                            dictParams.Add("Height", HomePageContent.Height == null ? "" : Convert.ToString(HomePageContent.Height));
                            dictParams.Add("HeightUnit", HomePageContent.HeightUnit == null ? "" : HomePageContent.HeightUnit);
                            CommonDA.Insert(Constants.USP_HomePageStaticContentAED, dictParams, ref StaticImageId, true);

                            //return Convert.ToInt16(StaticImageId["NewSSId"]);
                            break;
                           
                        }
                    case DBAction.Update:
                        {
                            dictParams.Add("HomePageStaticContentId", Convert.ToString(HomePageContent.HomePageStaticContentId));
                            dictParams.Add("Title", HomePageContent.Title == null ? "" : HomePageContent.Title);
                            dictParams.Add("ImageExtension", HomePageContent.ImageExtension == null ? "" : HomePageContent.ImageExtension);
                            dictParams.Add("URL", HomePageContent.URL == null ? "#" : HomePageContent.URL);
                            dictParams.Add("ContentType", HomePageContent.ContentType == null ? "" : HomePageContent.ContentType);
                            dictParams.Add("Content", HomePageContent.Content == null ? "" : HomePageContent.Content);
                            dictParams.Add("Width", HomePageContent.Width == null ? "" : Convert.ToString(HomePageContent.Width));
                            dictParams.Add("WidthUnit", HomePageContent.WidthUnit == null ? "" : HomePageContent.WidthUnit);
                            dictParams.Add("Height", HomePageContent.Height == null ? "" : Convert.ToString(HomePageContent.Height));
                            dictParams.Add("HeightUnit", HomePageContent.HeightUnit == null ? "" : HomePageContent.HeightUnit);
                            dictParams.Add("LangId", HomePageContent.LanguageId == 0 ? GlobalFunctions.GetLanguageId().ToString() : Convert.ToString(HomePageContent.LanguageId));

                            CommonDA.Update(Constants.USP_HomePageStaticContentAED, dictParams, ref StaticImageId, true);
                            break;
                        }
                    case DBAction.Delete:
                        {
                            dictParams.Add("HomePageStaticContentId", HomePageContent.HomePageStaticContentId.ToString());
                            CommonDA.Delete(Constants.USP_HomePageStaticContentAED, dictParams, ref StaticImageId, true);
                        }
                        break;
                    default:
                        break;
                }


                return Convert.ToInt16(StaticImageId["NewSSId"].ToString());
                //return 1; 
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                throw ex;
            }

            
        }

        
    }
}
