﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Xml;

namespace PWGlobalEcomm.BusinessLogic
{
    public class PaymentTypesBL
    {
        /// <summary>
        /// This is to save Payment types in database
        /// </summary>
        /// <param name="ndPaymentType"></param>
        /// <returns></returns>
        public static void SavePaymentTypeNode(XmlNode ndPaymentType, int DivisionId)
        {
            #region Remove all PaymentTypes

            PaymentTypesDA.Delete(Constants.USP_RemoveAllPaymentTypes, null, true);

            #endregion

            XmlNodeList xmlPaymentTypeList = ndPaymentType.ChildNodes;

            for (int cntPay = 0; cntPay < xmlPaymentTypeList.Count; cntPay++)
            {
                #region Insert PaymentType

                try
                {
                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                    DictionaryInstance.Add("PaymentTypeID", Convert.ToString(xmlPaymentTypeList[cntPay]["PaymentTypeID"].InnerText));
                    DictionaryInstance.Add("PaymentTypeRef", Convert.ToString(xmlPaymentTypeList[cntPay]["PaymentTypeRef"].InnerText));
                    DictionaryInstance.Add("IsDefault", Convert.ToString(xmlPaymentTypeList[cntPay]["IsDefault"].InnerText));
                    DictionaryInstance.Add("PaymentMethod", Convert.ToString(xmlPaymentTypeList[cntPay]["PaymentMethod"].InnerText));
                    DictionaryInstance.Add("CardDigitLength1", string.IsNullOrEmpty(xmlPaymentTypeList[cntPay]["CardDigitLength1"].InnerText) ? "-1" : Convert.ToString(xmlPaymentTypeList[cntPay]["CardDigitLength1"].InnerText));
                    DictionaryInstance.Add("CardDigitLength2", string.IsNullOrEmpty(xmlPaymentTypeList[cntPay]["CardDigitLength2"].InnerText) ? "-1" : Convert.ToString(xmlPaymentTypeList[cntPay]["CardDigitLength2"].InnerText));
                    DictionaryInstance.Add("CardDigitLength3", string.IsNullOrEmpty(xmlPaymentTypeList[cntPay]["CardDigitLength3"].InnerText) ? "-1" : Convert.ToString(xmlPaymentTypeList[cntPay]["CardDigitLength3"].InnerText));
                    DictionaryInstance.Add("IsShowOnWebsite", "0");
                    DictionaryInstance.Add("DivisionId", Convert.ToString(DivisionId));

                    PaymentTypesDA.Insert(Constants.USP_InsertPaymenttype, DictionaryInstance, true);
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }

                #endregion

                if (Convert.ToString(xmlPaymentTypeList[cntPay]["UDFS"].Name).ToUpper().Trim() == "UDFS")
                {
                    XmlNodeList xmlUDFSNodeList = xmlPaymentTypeList[cntPay]["UDFS"].ChildNodes;

                    for (int cntudfs = 0; cntudfs < xmlUDFSNodeList.Count; cntudfs++)
                    {
                        #region Insert UDFS

                        try
                        {
                            Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                            DictionaryInstance.Add("SequenceNo", Convert.ToString(xmlUDFSNodeList[cntudfs]["SequenceNo"].InnerText));
                            DictionaryInstance.Add("PaymentTypeID", Convert.ToString(xmlPaymentTypeList[cntPay]["PaymentTypeID"].InnerText));
                            DictionaryInstance.Add("Caption", Convert.ToString(xmlUDFSNodeList[cntudfs]["Caption"].InnerText));
                            DictionaryInstance.Add("RuleType", string.IsNullOrEmpty(xmlUDFSNodeList[cntudfs]["RuleType"].InnerText) ? "-1" : Convert.ToString(xmlUDFSNodeList[cntudfs]["RuleType"].InnerText));
                            DictionaryInstance.Add("CaseRule", xmlUDFSNodeList[cntudfs]["CaseRule"] != null ? Convert.ToString(xmlUDFSNodeList[cntudfs]["CaseRule"].InnerText) : "N");
                            DictionaryInstance.Add("MinimumLength", string.IsNullOrEmpty(xmlUDFSNodeList[cntudfs]["MinimumLength"].InnerText) ? "-1" : Convert.ToString(xmlUDFSNodeList[cntudfs]["MinimumLength"].InnerText));
                            DictionaryInstance.Add("MaximumLength", string.IsNullOrEmpty(xmlUDFSNodeList[cntudfs]["MaximumLength"].InnerText) ? "-1" : Convert.ToString(xmlUDFSNodeList[cntudfs]["MaximumLength"].InnerText));
                            DictionaryInstance.Add("MinimumValue", string.IsNullOrEmpty(xmlUDFSNodeList[cntudfs]["MinimumValue"].InnerText) ? "-1" : Convert.ToString(xmlUDFSNodeList[cntudfs]["MinimumValue"].InnerText));
                            DictionaryInstance.Add("MaximumValue", string.IsNullOrEmpty(xmlUDFSNodeList[cntudfs]["MaximumValue"].InnerText) ? "-1" : Convert.ToString(xmlUDFSNodeList[cntudfs]["MaximumValue"].InnerText));
                            DictionaryInstance.Add("DecimalPlaces", string.IsNullOrEmpty(xmlUDFSNodeList[cntudfs]["DecimalPlaces"].InnerText) ? "-1" : Convert.ToString(xmlUDFSNodeList[cntudfs]["DecimalPlaces"].InnerText));
                            DictionaryInstance.Add("IsVisible", Convert.ToString(true));
                            DictionaryInstance.Add("DivisionId", Convert.ToString(DivisionId));
                            DictionaryInstance.Add("IsMandatory", Convert.ToString(false));

                            PaymentTypesDA.Insert(Constants.USP_InsertUDFS, DictionaryInstance, true);
                        }
                        catch (Exception ex)
                        {
                            Exceptions.WriteExceptionLog(ex);
                        }

                        #endregion

                        if (Convert.ToString(xmlUDFSNodeList[cntudfs]["PickListItems"].Name).Trim() == "PickListItems")
                        {
                            XmlNodeList xmlPickListItemsNodeList = xmlUDFSNodeList[cntudfs]["PickListItems"].ChildNodes;

                            for (int cntPickListItems = 0; cntPickListItems < xmlPickListItemsNodeList.Count; cntPickListItems++)
                            {
                                #region Insert PickListItem

                                try
                                {
                                    Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                                    DictionaryInstance.Add("PaymentTypeID", string.IsNullOrEmpty(xmlPaymentTypeList[cntPay]["PaymentTypeID"].InnerText) ? "-1" : Convert.ToString(xmlPaymentTypeList[cntPay]["PaymentTypeID"].InnerText));
                                    DictionaryInstance.Add("SequenceNo", string.IsNullOrEmpty(xmlUDFSNodeList[cntudfs]["SequenceNo"].InnerText) ? "-1" : Convert.ToString(xmlUDFSNodeList[cntudfs]["SequenceNo"].InnerText));
                                    DictionaryInstance.Add("Value", Convert.ToString(xmlPickListItemsNodeList[cntPickListItems]["Value"].InnerText));
                                    DictionaryInstance.Add("IsDefault", Convert.ToString(xmlPickListItemsNodeList[cntPickListItems]["IsDefault"].InnerText));
                                    DictionaryInstance.Add("DivisionId", Convert.ToString(DivisionId));

                                    PaymentTypesDA.Insert(Constants.USP_InsertPickListItem, DictionaryInstance, true);
                                }
                                catch (Exception ex)
                                {
                                    Exceptions.WriteExceptionLog(ex);
                                }

                                #endregion
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// gets all Payment Type details
        /// </summary>
        /// <returns>returns list of PaymentTypesBE objects</returns>
        /// added LanguageId /*User Type*/
        public static List<PaymentTypesBE> GetAllPaymentTypesDetails(int LanguageId)
        {
            List<PaymentTypesBE> getAllPaymentTypes = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(LanguageId));
                getAllPaymentTypes = PaymentTypesDA.getCollectionItem(Constants.USP_GetAllPaymentTypesDetails, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllPaymentTypes;
        }

        /// <summary>
        /// Author  : sanchit patne
        /// Date    : 22/01/2016
        /// Scope   : Update Payment Types
        /// </summary>
        /// <returns><bool</returns>
        /// added LanguageId /*User Type*/
        public static bool UpdatePaymentTypes(int PaymentTypeID, bool IsShowOnWebsite, string PaymentTypeRef, int Languageid, bool InvoiceAccountId, bool rbDefault, bool rbBudget, bool rbPoint)
        {
            bool result;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                //dictionaryInstance.Add("Id", Convert.ToString(PaymentTypeID));/*User Type*/
                dictionaryInstance.Add("PaymentTypeID", Convert.ToString(PaymentTypeID));
                dictionaryInstance.Add("IsShowOnWebsite", Convert.ToString(IsShowOnWebsite));
                dictionaryInstance.Add("PaymentTypeRef", Convert.ToString(PaymentTypeRef));
                dictionaryInstance.Add("LanguageId", Convert.ToString(Languageid));
                dictionaryInstance.Add("InvoiceAccountId", Convert.ToString(InvoiceAccountId));
                dictionaryInstance.Add("rbDefault", Convert.ToString(rbDefault));
                #region Indeed Code
                dictionaryInstance.Add("rbBudget", Convert.ToString(rbBudget));
                dictionaryInstance.Add("rbPoint", Convert.ToString(rbPoint));
                #endregion
               
                result = PaymentTypesDA.Update(Constants.USP_UpdatePaymentTypes, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// gets all UDFS details
        /// </summary>
        /// <returns>returns list of UDFBE objects</returns>
        /// added LanguageId /*User Type*/
        public static List<T> GetAllUDFSDetails<T>(int Languageid)
        {
            List<T> getAllUDFS = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(Languageid));
                getAllUDFS = CommonDA.getCollectionItem<T>(Constants.USP_GetAllUDFSDetails, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllUDFS;
        }
        public static List<T> GetAllUDFSDetails<T>(int Languageid, int paymentTypeID)
        {
            List<T> getAllUDFS = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(Languageid));
                dictionaryInstance.Add("PaymentTypeID", Convert.ToString(paymentTypeID));
                getAllUDFS = CommonDA.getCollectionItem<T>(Constants.USP_GetAllUDFSDetailspaymentTypeID, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllUDFS;
        }


        /// <summary>
        /// Author  : sanchit patne
        /// Date    : 22/01/2016
        /// Scope   : Update UDFS IsMandatory
        /// </summary>
        /// <returns><bool</returns>
        /// added LanguageId,Caption /*User Type*/
        public static bool UpdateUDFSIsMandatory(int UDFOasisID, bool IsMandatory, bool IsVisible, int Languageid, string Caption, bool IsBudget, bool IsPoints)
        {
            bool result;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("UDFOasisID", Convert.ToString(UDFOasisID));
                dictionaryInstance.Add("IsMandatory", Convert.ToString(IsMandatory));
                dictionaryInstance.Add("IsVisible", Convert.ToString(IsVisible));
                dictionaryInstance.Add("Caption", Convert.ToString(Caption));
                dictionaryInstance.Add("Languageid", Convert.ToString(Languageid));
                #region indeed code
                 dictionaryInstance.Add("IsPoints", Convert.ToString(IsPoints));
                dictionaryInstance.Add("IsBudget", Convert.ToString(IsBudget));
                #endregion
                result = PaymentTypesDA.Update(Constants.USP_UpdateUDFSIsMandatory, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }
            return result;
        }


        /// <summary>
        /// Author  : Snehal Jadhav
        /// Date    : 05 01 2017
        /// </summary>        
        public static List<T> GetCustRefUDFSDetails<T>(int Languageid, int PaymentTypeID)
        {
            List<T> getAllUDFS = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(Languageid));
                dictionaryInstance.Add("PaymentTypeID", Convert.ToString(PaymentTypeID));
                getAllUDFS = CommonDA.getCollectionItem<T>(Constants.USP_GetCustomerRefUDFSDetails, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllUDFS;
        }

        /// <summary>
        /// Author  : Snehal Jadhav
        /// Date    : 05 01 2017
        /// Scope   : Update Customer Ref UDFS IsMandatory
        /// </summary>
        /// <returns><bool</returns>
        /// added LanguageId,Caption /*User Type*/
        public static bool UpdateCustomerRefUDFSIsMandatory(int CustomerRefID, bool IsMandatory, bool IsVisible, int Languageid, string Caption, int PaymentTypeID)
        {
            bool result;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("CustomerRefID", Convert.ToString(CustomerRefID));
                dictionaryInstance.Add("IsMandatory", Convert.ToString(IsMandatory));
                dictionaryInstance.Add("IsVisible", Convert.ToString(IsVisible));
                dictionaryInstance.Add("Caption", Convert.ToString(Caption));
                dictionaryInstance.Add("Languageid", Convert.ToString(Languageid));
                dictionaryInstance.Add("PaymentTypeID", Convert.ToString(PaymentTypeID));
                result = PaymentTypesDA.Update(Constants.USP_UpdateCustomerRefUDFSIsMandatory, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                result = false;
            }
            return result;
        }

        #region "/*User Type*/"
        /// <summary>		
        /// gets all UDFS details		
        /// </summary>		
        /// <returns>returns list of UDFBE objects</returns>		
        public static List<T> GetAllPickListPaymentItems<T>(int Languageid, int PaymentTypeId)
        {
            List<T> getAllPickList = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(Languageid));
                dictionaryInstance.Add("PaymentTypeIdId", Convert.ToString(PaymentTypeId));
                getAllPickList = CommonDA.getCollectionItem<T>(Constants.USP_GetAllPaymentPickListItem, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllPickList;
        }


        public static List<T> GetAllPickListPaymentItemsPID<T>(int Languageid, int PaymentTypeId)
        {
            List<T> getAllPickList = null;
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("LanguageId", Convert.ToString(Languageid));
                dictionaryInstance.Add("PaymentTypeIdId", Convert.ToString(PaymentTypeId));
                getAllPickList = CommonDA.getCollectionItem<T>(Constants.USP_GetAllPaymentPickListItemPaymentTypeId, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return getAllPickList;
        }

        /// <summary>		
        /// Author  : sanchit patne		
        /// Date    : 22/01/2016		
        /// Scope   : Update UDFS IsMandatory		
        /// </summary>		
        /// <returns><bool</returns>		
        /// 		
        public static bool UpdatePickListPaymentItems(int AutoId, string Payvalue)
        {
            bool result = false;
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            try
            {
                dictionaryInstance.Add("Id", Convert.ToString(AutoId));
                dictionaryInstance.Add("PayValue", Convert.ToString(Payvalue));
                result = PaymentTypesDA.Update(Constants.USP_UpdatePickListPaymentItem, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return result;
        }

        #endregion
    }
}