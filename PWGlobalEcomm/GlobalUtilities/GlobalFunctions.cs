﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data.OleDb;
using System.Globalization;

/*Sachin Chauhan Start : 05 02 2015 : Added bitly name space*/
//using BitlyDotNET.Interfaces;
//using BitlyDotNET.Implementations;
/*Sachin Chauhan End : 05 02 2015*/

public static class GlobalFunctions
{
    #region Declarations
    private static string PhysicalApplicationPath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath);
    #endregion
    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 16-07-15
    /// Scope   : Returns a string representing application's virtual path by considering current request
    /// </summary>
    /// <returns>Virtual url of the current page</returns>
    /// <remarks></remarks>
    public static string GetCurrentRequestURLOfVirtualPath()
    {
        try
        {
            return HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Authority + "/" + HttpContext.Current.Request.RawUrl.Split('/').GetValue(1);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return string.Empty;
        }
    }

    /// <summary>
    /// File type will be used during uploading files from client.
    /// </summary>
    /// <remarks></remarks>
    public enum FileType : int
    {
        Image, //jpg,gif,bmp,ico,jpeg,png,tiff,tga
        Word, //only doc
        Excel, //only xls
        Text, //only txt
        PDF, //only pdf files
        Video, //only video files
        Artwork //jpg,gif,bmp,ico,jpeg,png,tiff,tga,cdr,pdf,ai
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   :Virtual path of application Admin
    /// </summary>
    /// <returns>Virtual path of application Admin</returns>
    public static string GetPhysicalFolderPathAdmin()
    {


        //if (System.Configuration.ConfigurationManager.AppSettings["IsServer"].ToString() == "0")
        //    vPath = System.Configuration.ConfigurationManager.AppSettings["WebFolderPath"];
        //else
        //{
        string vPath = GetPhysicalFolderPath() + "Admin/";

        //}
        return vPath;
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 16-07-15
    /// Scope   : Returns a string representing application's virtual path
    /// </summary>
    /// <returns>Virtual url of the link</returns>
    /// <remarks></remarks>
    public static string GetVirtualPath()
    {
        try
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["StoreURL"])))
                return Convert.ToString(ConfigurationManager.AppSettings["StoreURL"]);
            else
                return HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Authority + "/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return string.Empty;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia    
    /// Date    : 23/07/2015
    /// Scope   : Returns a string representing application's Admin virtual path
    /// </summary>
    /// <returns>Virtual url of the link</returns>
    /// <remarks></remarks>
    public static string GetVirtualPathAdmin()
    {
        try
        {
            return GetVirtualPath() + "Admin/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return string.Empty;
        }
    }

    /*
    Author : Farooq Shaikh
    Date   : 20-07-2015  
    Scope  : Short or Breif description about the Functionality.....
    */

    /// <summary>
    /// to remove special character from a string
    /// </summary>
    /// <param name="Value"></param>
    /// <returns></returns>
    public static string RemoveSpecialCharacters(string Value)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in Value)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return Value;
        }

    }


    /// <summary>
    /// To check uploaded file type, its accepts only jpg file.
    /// </summary>
    /// <param name="FileControlId"></param>
    /// <param name="FileTypeToCheck"></param>
    /// <returns></returns>
    public static bool CheckUploadJPGFileType(System.Web.UI.WebControls.FileUpload FileControlId, string FileTypeToCheck)
    {

        bool blnReturn = false;
        HttpPostedFile ObjFile = null;
        string ClientFilename = null;
        string ActualFilename = null;

        ObjFile = FileControlId.PostedFile;
        ClientFilename = ObjFile.FileName;

        //get the physical filename
        ActualFilename = Path.GetFileName(ClientFilename);
        int IndexPos = 0;

        string FileExtension = null;
        IndexPos = ActualFilename.LastIndexOf(".") + 1;
        FileExtension = ActualFilename.Remove(0, IndexPos);

        string UploadAllowedFileTypes = null;

        if (FileTypeToCheck == "Image")
        {
            UploadAllowedFileTypes = "jpg,gif,png,jpeg";
        }
        //if file type is as of above mentioned then return true else false
        if ((UploadAllowedFileTypes.ToUpper().IndexOf(FileExtension.ToUpper(), 0) + 1) != 0)
        {
            blnReturn = true;
        }
        else
        {
            blnReturn = false;
        }

        ObjFile = null;
        ClientFilename = null;
        ActualFilename = null;
        FileExtension = null;
        UploadAllowedFileTypes = null;

        return blnReturn;
    }

    public static bool CheckFileExtension(System.Web.UI.WebControls.FileUpload FileUpload, string[] AllowedFileTypes)
    {
        bool result = false;
        string strFileType = System.IO.Path.GetExtension(FileUpload.FileName);
        foreach (string str in AllowedFileTypes)
        {
            if (strFileType.ToLower() == str.ToLower())
            {
                result = true;
                break;
            }
        }
        return result;

    }

    public static bool CheckFileMIMEType(System.Web.UI.WebControls.FileUpload FileUpload, string[] AllowedFileMIMETypes)
    {
        bool result = false;
        string strFileMIMEType = FileUpload.PostedFile.ContentType;
        foreach (string str in AllowedFileMIMETypes)
        {
            if (strFileMIMEType.ToLower() == str.ToLower())
            {
                result = true;
                break;
            }
        }
        return result;

    }
    /// <summary>
    /// function to show the javascript alert messages.
    /// </summary>
    /// <param name="pageId">Page id from where this function will be called</param>
    /// <param name="message">Alert message</param>
    /// <remarks></remarks>
    public static void ShowGeneralAlertMessages(Page pageId, string message)
    {
        //pageId.ClientScript.RegisterStartupScript(pageId.GetType(), "showAlertMessage", "<script>alert('" + System.Web.HttpUtility.JavaScriptStringEncode(message) + "');</script>");
    }

    /// <summary>
    /// function to show the modal alert messages.
    /// </summary>
    /// <param name="pageId">Page id from where this function will be called</param>
    /// <param name="message">Alert message</param>
    /// <param name="alertType">alert Type</param>
    /// <remarks></remarks>
    public static void ShowModalAlertMessages(Page pageId, string message, AlertType alertType)
    {
        if (alertType == AlertType.Success)
            pageId.ClientScript.RegisterStartupScript(pageId.GetType(), "showAlertMessage", "<script>$('#mySuccessModal').find('#SuccessMessage').html('" + System.Web.HttpUtility.JavaScriptStringEncode(message) + "');$('#mySuccessModal').modal('show');</script>");
        else if (alertType == AlertType.Warning)
            pageId.ClientScript.RegisterStartupScript(pageId.GetType(), "showAlertMessage", "<script>$('#myWarningModal').find('#WarningMessage').html('" + System.Web.HttpUtility.JavaScriptStringEncode(message) + "');$('#myWarningModal').modal('show');</script>");
        else if (alertType == AlertType.Failure)
            pageId.ClientScript.RegisterStartupScript(pageId.GetType(), "showAlertMessage", "<script>$('#myErrorModal').find('#ErrorMessage').html('" + System.Web.HttpUtility.JavaScriptStringEncode(message) + "');$('#myErrorModal').modal('show');</script>");
    }

    /// <summary>
    /// function to show the modal alert messages.
    /// </summary>
    /// <param name="pageId">Page id from where this function will be called</param>
    /// <param name="message">Alert message</param>
    /// <param name="redirectPage">redirect Page</param>
    /// <param name="alertType">alert Type</param>
    /// <remarks></remarks>
    public static void ShowModalAlertMessages(Page pageId, string message, string redirectPage, AlertType alertType)
    {
        if (alertType == AlertType.Success)
            pageId.ClientScript.RegisterStartupScript(pageId.GetType(), "showAlertMessage", "<script>$('#mySuccessModal').find('#SuccessMessage').html('" + System.Web.HttpUtility.JavaScriptStringEncode(message) + "');$('#mySuccessModal').find('#btnSuccessOK').click(function(){window.location.href='" + redirectPage + "';});$('#mySuccessModal').modal('show');</script>");
        else if (alertType == AlertType.Warning)
            pageId.ClientScript.RegisterStartupScript(pageId.GetType(), "showAlertMessage", "<script>$('#myWarningModal').find('#WarningMessage').html('" + System.Web.HttpUtility.JavaScriptStringEncode(message) + "');$('#myWarningModal').find('#btnWarningOK').click(function(){window.location.href='" + redirectPage + "';});$('#myWarningModal').modal('show');</script>");
        else if (alertType == AlertType.Failure)
            pageId.ClientScript.RegisterStartupScript(pageId.GetType(), "showAlertMessage", "<script>$('#myErrorModal').find('#ErrorMessage').html('" + System.Web.HttpUtility.JavaScriptStringEncode(message) + "');$('#myErrorModal').find('#btnErrorOK').click(function(){window.location.href='" + redirectPage + "';});$('#myErrorModal').modal('show');</script>");
    }


    /// <summary>
    /// Author  : Sachin Chauhan
    /// Date    : 03-08-15
    /// Scope   : Display javascript alert and redirect to page
    /// </summary>
    /// <param name="pageId">Page id from where this function will be called</param>
    /// <param name="message">Alert message</param>
    /// <remarks></remarks>
    public static void ShowGeneralAlertMessages(Page pageId, string message, string redirectPage)
    {
        pageId.ClientScript.RegisterStartupScript(pageId.GetType(), "showAlertMessage", "<script>alert('" + System.Web.HttpUtility.JavaScriptStringEncode(message) + "');window.location.href='" + redirectPage + "'</script>");
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 21-07-15
    /// Scope   : Encode the special characters with the predefined text for each special character
    /// </summary>
    /// <param name="Value"></param>
    /// <returns>Encoded value</returns>
    public static string EncodeCategoryURL(string Value)
    {
        try
        {
            return Value.Replace("'", "-quote-").Replace("&", "-and-").Replace("+", "-plus-").Replace("/", "-slash-").Replace(".", "-dot-").Replace("\"", "-dbq-").Replace("<", "-lt-").Replace(">", "-gt-").Replace(":", "-col-").Replace("#", "-hash-");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return Value;
        }

    }
    public static string EncodeCategoryURLLanguage(string Value)
    {
        try
        {
            return Value.Replace("'", "-quote-").Replace("-and-", "&").Replace("+", "-plus-").Replace("/", "-slash-").Replace(".", "-dot-").Replace("\"", "-dbq-").Replace("<", "-lt-").Replace(">", "-gt-").Replace(":", "-col-").Replace("#", "-hash-");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return Value;
        }

    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 21-07-15
    /// Scope   : Decode the special characters with the predefined text for each special character
    /// </summary>
    /// <param name="Value"></param>
    /// <returns>Decoded value</returns>
    public static string DecodeCategoryURL(string Value)
    {
        try
        {
            return Value.ToLower().Replace("-quote-", "'").Replace("-and-", "&amp;").Replace("-plus-", "+").Replace("-slash-", "/").Replace("-dot-", ".").Replace("-dbq-", "\"").Replace("-lt-", "<").Replace("-gt-", ">").Replace("-col-", ":").Replace("-hash-", "#");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return Value;
        }
    }
    public static string DecodeUrlBredCrumb(string value)
    {
        try
        {
            return value.Replace("&amp;", "-and-").Replace("&amp", "-and-").Replace("amp;", "").Replace("/", "-slash-").Replace("'", "-quote-").Replace("+", "&#43;").Replace("\"", "-dbq-").Replace("<", "-lt-").Replace(">", "-gt-").Replace(":", "-col-").Replace("#", "-hash-");
        }
        catch(Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return value;
        }
    }
    public static string EncodeUrlBredCrumb(string value)
    {
        try
        {
            return value.Replace("-and-", "&").Replace("-slash-", "/").Replace("-quote-", "'").Replace("&#43;", "+").Replace("-hash-", "#").Replace("-col-",":").Replace("&amp;","&");
        }
        catch(Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return value;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 22-07-15
    /// Scope   : Get Virtual path of Thumbnail size Product Image.
    /// </summary>
    /// <returns>virtual path of Thumbnail size product images</returns>
    public static string GetThumbnailProductImagePath()
    {
        try
        {
            return GetVirtualPath() + "Images/Products/Thumbnail/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 22-07-15
    /// Scope   : Get Virtual path of medium size Product Image.
    /// </summary>
    /// <returns>virtual path of medium size product images</returns>
    public static string GetMediumProductImagePath()
    {
        try
        {
            return GetVirtualPath() + "Images/Products/Medium/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 22-07-15
    /// Scope   : Get Virtual path of large size Product Image.
    /// </summary>
    /// <returns>virtual path of large size product images</returns>
    public static string GetLargeProductImagePath()
    {
        try
        {
            return GetVirtualPath() + "Images/Products/Large/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   : Get Virtual path of Hi-Res size Product Image.
    /// </summary>
    /// <returns>virtual path of Hi-Res size product images</returns>
    public static string GetHiResProductImagePath()
    {
        try
        {
            return GetVirtualPath() + "Images/Products/Hi-Res/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 22-07-15
    /// Scope   : Get Physical path of Thumbnail size Product Image.
    /// </summary>
    /// <returns>Physical path of Thumbnail size product images</returns>
    public static string GetPhysicalThumbnailProductImagePath()
    {
        try
        {
            return GetPhysicalFolderPath() + "Images/Products/Thumbnail/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 22-07-15
    /// Scope   : Get Physical path of medium size Product Image.
    /// </summary>
    /// <returns>Physical path of medium size product images</returns>
    public static string GetPhysicalMediumProductImagePath()
    {
        try
        {
            return GetPhysicalFolderPath() + "Images/Products/Medium/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 22-07-15
    /// Scope   : Get Physical path of Enquiry Logo Image.
    /// </summary>
    /// <returns>Physical  path of Enquiry Logo Image.</returns>
    public static string GetPhysicalEnquiryImagePath()
    {
        try
        {
            return GetPhysicalFolderPath() + "Images/EnquiryLogo/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }


    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 18/11/2015
    /// Scope   : Get Physical path of Fonts Upload.
    /// </summary>
    /// <returns>Physical  path of Fonts Upload.</returns>
    public static string GetPhysicalFontsUploadPath()
    {
        try
        {
            return GetPhysicalFolderPath() + "FontsUpload/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 18/11/2015
    /// Scope   : Get Physical path of Fonts.
    /// </summary>
    /// <returns>Physical  path of Fonts.</returns>
    public static string GetPhysicalFontsPath()
    {
        try
        {
            return GetPhysicalFolderPath() + "Fonts/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 22-07-15
    /// Scope   : Get Physical path of large size Product Image.
    /// </summary>
    /// <returns>virtual path of large size product images</returns>
    public static string GetPhysicalLargeProductImagePath()
    {
        try
        {
            return GetPhysicalFolderPath() + "Images/Products/Large/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   : Get Physical path of Hi-Res size Product Image.
    /// </summary>
    /// <returns>virtual path of Hi-Res size product images</returns>
    public static string GetPhysicalHiResProductImagePath()
    {
        try
        {
            return GetPhysicalFolderPath() + "Images/Products/Hi-Res/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   : Get Physical path of Product Pdfs.
    /// </summary>
    /// <returns>Physical path of Product Pdfs.</returns>
    public static string GetPhysicalProductPdfsPath()
    {
        try
        {
            return GetPhysicalFolderPath() + "Images/ProductPdfs/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   : Get Virtaul path of Product Pdfs.
    /// </summary>
    /// <returns>Virtaul path of Product Pdfs.</returns>
    public static string GetProductPdfsPath()
    {
        try
        {
            return GetVirtualPath() + "Images/ProductPdfs/";
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }


    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   : Show Alert Message.
    /// </summary>
    /// <param name="objPage"></param>
    /// <param name="Message"></param>
    public static void ShowAlertMsg(Page objPage, string Message)
    {
        objPage.ClientScript.RegisterStartupScript(objPage.GetType(), "Message", "alert('" + Message + "');", true);
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   : Show Alert Message.
    /// </summary>
    /// <param name="objPage"></param>
    /// <param name="Message"></param>
    /// <param name="Title"></param>
    public static void ShowAlertMsg(Page objPage, string Message, string Title)
    {
        objPage.ClientScript.RegisterStartupScript(objPage.GetType(), Title, "<script>alert('" + Message + "');</script>");
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 22-07-15
    /// Scope   : This function will remove all the html tags.
    /// </summary>
    /// <returns>This function will remove all the html tags</returns>
    public static string RemoveHtmlTags(string source)
    {
        try
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return source;
        }
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   : It will return the data from settings xml file
    /// </summary>
    /// <param name="SettingName"></param>
    /// <returns>It will return the data from settings xml file</returns>

    public static string GetSetting(string SettingName)
    {
        string FileName = GetPhysicalFolderPath() + "Settings.config";
        XmlDocument XMLSettings;
        string SettingType = "", SettingValueToReturn = "";
        string SelectedDropdownValue = "";

        XMLSettings = new XmlDocument();
        XMLSettings.Load(FileName);

        XmlNode XNSettingNode = XMLSettings.SelectSingleNode("settings/section/setting[@name = \"" + SettingName + "\" ]");
        SettingType = XNSettingNode.Attributes["type"].Value;
        if (SettingType == "text" || SettingType == "boolean")
            SettingValueToReturn = XNSettingNode.InnerText;
        else if (SettingType == "dropdown")
        {
            SelectedDropdownValue = XNSettingNode.Attributes["selectedvalue"].Value;
            SettingValueToReturn = SelectedDropdownValue;
        }
        return SettingValueToReturn;
    }

    /// <summary>
    /// Gets a converted string from byte size
    /// </summary>
    /// <param name="sizeToConvert">Number of bytes, this will be converted into appropriate size type</param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static string ConvertSizeFromBytes(long sizeToConvert)
    {

        string strSizeInDesiredType = "";

        if (sizeToConvert >= 1073741824) //out put will be in GB
        {
            strSizeInDesiredType = (sizeToConvert / 1024 / 1024 / 1024).ToString("#0.00") + " GB";
        }
        else if (sizeToConvert >= 1048576) //out put will be in MB
        {
            strSizeInDesiredType = (sizeToConvert / 1024 / 1024).ToString("#0.00") + " MB";
        }
        else if (sizeToConvert >= 1024) //out put will be in KB
        {
            strSizeInDesiredType = (sizeToConvert / 1024).ToString("#0.00") + " KB";
        }
        else if (sizeToConvert < 1024) //out put will be in Bytes
        {
            strSizeInDesiredType = Convert.ToInt32(sizeToConvert) + " Bytes";
        } //end of If sizeToConvert >= 1073741824


        return strSizeInDesiredType;

    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   :Virtual path of application
    /// </summary>
    /// <returns>Virtual path of application</returns>
    public static string GetPhysicalFolderPath()
    {
        return PhysicalApplicationPath + "\\";
    }


    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 23-07-15
    /// Scope   : Checks the specified file size with the uploaded file
    /// </summary>
    /// <param name="fileControlId">File upload control's id</param>
    /// <param name="requiredFileSizeInBytesToValidate">File size that will be checked against uploaded file</param>
    /// <returns> Checks the specified file size with the uploaded file</returns>
    public static bool IsFileSizeValid(System.Web.UI.WebControls.FileUpload FileControlId, long RequiredFileSizeInBytesToValidate)
    {

        bool blnResult = false;
        //will capture uploaded file for validation
        if (FileControlId.PostedFile.ContentLength <= RequiredFileSizeInBytesToValidate)
            blnResult = true;
        else
            blnResult = false;

        return blnResult;

    }

    #region OBJECT

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 28-07-15
    /// Scope   : Checks for IsNull
    /// </summary>
    /// <param name="Value">Object Value </param>
    /// <returns> Bool</returns>
    private static bool IsNull(object Value)
    {
        bool Return = false;
        if ((Value == null || Value is DBNull))
        {
            Return = true;
        }
        return Return;
    }

    #endregion
    #region INT

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 28-07-15
    /// Scope   : Converts Value to Int 32
    /// </summary>
    /// <param name="Value">Object input </param>
    /// <returns> int</returns>
    public static int To_Int32(this object input)
    {
        int _Return = 0;
        if (!IsNull(input) && input != "")
        {
            switch (input.GetType().Name.ToLower().Trim())
            {
                case "string":
                    _Return = Convert.ToString(input).Contains('.') ? (int)Math.Ceiling(Convert.ToDecimal(input)) : Convert.ToInt32(input);
                    break;
                default:
                    _Return = IsNull(input) ? 0 : Convert.ToInt32(input);
                    break;
            }
        }

        return _Return;
    }

    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 28-07-15
    /// Scope   : Converts Value to Int 16
    /// </summary>
    /// <param name="Value">Object input </param>
    /// <returns> int16</returns>
    public static short To_Int16(this object input)
    {
        short _Return = 0;
        _Return = IsNull(input) ? Convert.ToInt16(0) : Convert.ToInt16(input);
        return _Return;
    }

    /// <summary>
    /// Author  : Sachin Chauhan
    /// Date    : 10-09-15
    /// Scope   : Converts Value to Double
    /// </summary>
    /// <param name="Value">Object input </param>
    /// <returns> int16</returns>
    public static double To_Float(this object input)
    {
        double _Return = 0;
        _Return = IsNull(input) ? Convert.ToDouble(0) : Convert.ToDouble(input);
        return _Return;
    }


    /// <summary>
    /// Author  : Vinit Falgunia
    /// Date    : 28-07-15
    /// Scope   : Converts Value to Int 64
    /// </summary>
    /// <param name="Value">Object input </param>
    /// <returns> int64</returns>
    public static long To_Int64(this object input)
    {
        long _Return = 0;
        _Return = IsNull(input) ? 0 : Convert.ToInt64(input);
        return _Return;
    }

    public static DateTime? To_DateTime(this object input)
    {
        DateTime? _Return = null;
        _Return = IsNull(input) ? (DateTime?)null : Convert.ToDateTime(input);
        return _Return;
    }

    #endregion

    /// <summary>
    /// To export the data into excel file
    /// </summary>
    /// <param name="dtExportExcel">Data Table of excel data</param>
    /// <param name="FileName">File Name</param>
    public static void ExportToExcel(DataTable dtExportExcel, string FileName)
    {
        DataTable dt = dtExportExcel;
        string attachment = "attachment; filename=" + FileName.ToString().Trim() + ".xls";
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.AddHeader("content-disposition", attachment);
        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
        //HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename="+attachment);
    

        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {
            HttpContext.Current.Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        HttpContext.Current.Response.Write("\n");

        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                HttpContext.Current.Response.Write(tab + dr[i].ToString());
                tab = "\t";
            }
            HttpContext.Current.Response.Write("\n");
        }
        //HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath("~/UploadExcelError/") + FileName + ".xls");
        HttpContext.Current.Response.End();
    }
    public static void ExportToCSV(DataTable dtExportExcel, string FileName)
    {
        DataTable dt = dtExportExcel;
        string attachment = "attachment; filename=" + FileName.ToString() + ".csv";
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.AddHeader("content-disposition", attachment);
        HttpContext.Current.Response.ContentType = "Excel/csv";
        //HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename="+attachment);


        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {
            HttpContext.Current.Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        HttpContext.Current.Response.Write("\n");

        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                HttpContext.Current.Response.Write(tab + dr[i].ToString());
                tab = "\t";
            }
            HttpContext.Current.Response.Write("\n");
        }
        //HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath("~/UploadExcelError/") + FileName + ".xls");
        HttpContext.Current.Response.End();
    }
    //<summary>
    //To export the data into excel file
    //</summary>
    //<param name="dtExportExcel">Data Table of excel data</param>
    //<param name="FileName">File Name</param>
    public static DataTable To_DataTable<T>(this IList<T> input)
    {
        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        foreach (PropertyDescriptor prop in properties)
        {
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        }
        foreach (T item in input)
        {
            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
            {
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            }
            table.Rows.Add(row);
        }
        return table;
    }

    /// <summary>
    /// Returns the status of image file dimension's validity
    /// </summary>
    /// <param name="FileControlId">File upload id</param>
    /// <param name="ImageWidth">Image width to validate with uploaded image file</param>
    /// <param name="ImageHeight">Image height to validate with uploaded image file</param>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public static bool IsImageDimensionGreaterValid(System.Web.UI.WebControls.FileUpload FileControlId, int ImageWidth, int ImageHeight)
    {
        System.Drawing.Image ObjUploadedImageFile = null;
        bool blnResult = false;

        //Will capture uploaded image file for validation demensions

        ObjUploadedImageFile = System.Drawing.Image.FromStream(FileControlId.PostedFile.InputStream);
        if ((ObjUploadedImageFile.Width >= ImageWidth) && (ObjUploadedImageFile.Height >= ImageHeight))
            blnResult = true;
        else
            blnResult = false;
        ObjUploadedImageFile = null;
        return blnResult;

    }

    /// <summary>
    /// Returns the status of image file dimension's validity
    /// </summary>
    /// <param name="FileControlId">File upload id</param>
    /// <param name="ImageWidth">Image width to validate with uploaded image file</param>
    /// <param name="ImageHeight">Image height to validate with uploaded image file</param>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public static bool IsImageDimensionValid(System.Web.UI.WebControls.FileUpload FileControlId, int ImageWidth, int ImageHeight)
    {
        System.Drawing.Image ObjUploadedImageFile = null;
        bool blnResult = false;

        //Will capture uploaded image file for validation demensions

        ObjUploadedImageFile = System.Drawing.Image.FromStream(FileControlId.PostedFile.InputStream);
        if ((ObjUploadedImageFile.Width <= ImageWidth) && (ObjUploadedImageFile.Height <= ImageHeight))
            blnResult = true;
        else
            blnResult = false;
        ObjUploadedImageFile = null;
        return blnResult;

    }

    /// <summary>
    /// Uploads file from client and stores on server
    /// </summary>
    /// <param name="FileControlId">File control id</param>
    /// <param name="FolderNameToStoreFile">Folder name to store file and the folder name should end with front slash(/)</param>
    /// <param name="FileNameToSaveAs">File name to save as while saving on server</param>
    /// <param name="FileTypeToCheck">Type of file to check before uploading</param>
    /// <param name="IsFileExtentionProvided">A check weather file extension is provided or not, provide false if passed file name has extension</param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static string FileUpload(System.Web.UI.WebControls.FileUpload FileControlId, string FolderNameToStoreFile, string FileNameToSaveAs, FileType FileTypeToCheck, bool IsFileExtentionProvided)
    {
        string strFileName = null;
        string strFileNameOnServer = null;
        string strBaseLocation = null;
        HttpPostedFile objFile = null;
        string sClientFilename = null;
        string sActualFilename = null;
        string sActualNewFilename = null;

        objFile = FileControlId.PostedFile;
        sClientFilename = objFile.FileName;
        //get the physical filename

        sActualFilename = Path.GetFileName(sClientFilename);

        int indexPos = 0;
        string sFileExtension = null;
        indexPos = sActualFilename.LastIndexOf(".") + 1;
        sFileExtension = sActualFilename.Remove(0, indexPos);

        string strUploadAllowedFileTypes = null;

        if (FileTypeToCheck == FileType.Image == true)
        {
            strUploadAllowedFileTypes = ConfigurationManager.AppSettings["ValidUploadImageFileExtensions"].ToString();
        }
        else if (FileTypeToCheck == FileType.Excel == true)
        {
            strUploadAllowedFileTypes = "xls";
        }
        //else if (FileTypeToCheck == FileType.CSV == true)
        //{
        //    strUploadAllowedFileTypes = "csv";
        //}
        else if (FileTypeToCheck == FileType.Word == true)
        {
            strUploadAllowedFileTypes = "doc";
        }
        else if (FileTypeToCheck == FileType.Text == true)
        {
            strUploadAllowedFileTypes = "txt";
        }
        else if (FileTypeToCheck == FileType.PDF == true)
        {
            strUploadAllowedFileTypes = "pdf";
        }


        //*******************************************************************
        //if file type is as of above mentioned then return true else false
        //*******************************************************************
        if ((strUploadAllowedFileTypes.ToUpper().IndexOf(sFileExtension.ToUpper(), 0) + 1) != 0)
        {

            Array arrExtension = null;
            arrExtension = sActualFilename.Split('.');

            int intUBound = 0;
            intUBound = arrExtension.Length;
            arrExtension.GetValue(intUBound - 1);


            if (arrExtension.GetValue(intUBound - 1) == "")
            {

                sActualNewFilename = "";
                strFileName = "";

            }
            else
            {

                sActualNewFilename = FileNameToSaveAs + ".";

                arrExtension.GetValue(intUBound - 1);

                string strFileExtention = arrExtension.GetValue(intUBound - 1).ToString();
                strFileName = sActualNewFilename;

                //Enter folder name here form this function's parameter
                strBaseLocation = FolderNameToStoreFile;

                string strTest = strBaseLocation + sActualNewFilename + strFileExtention;
                string strOnlyFileName = sActualNewFilename + strFileExtention;

                FileStream objFS = null;

                try
                {

                    if (IsFileExtentionProvided == false)
                    {

                        //will 1st remove the file, if any with same name
                        CheckAndDeleteFile(strBaseLocation + sActualNewFilename + strFileExtention);
                        //next will save the file
                        objFile.SaveAs(strBaseLocation + sActualNewFilename + strFileExtention);

                        strFileNameOnServer = sActualNewFilename + strFileExtention;
                    }
                    else
                    {
                        //if IsFileExtentionProvided is true then the
                        //filenametosaveas variable in the parameter
                        //contains the file extention

                        //will 1st remove the file, if any with same name
                        CheckAndDeleteFile(strBaseLocation + FileNameToSaveAs);
                        //next will save the file
                        objFile.SaveAs(strBaseLocation + FileNameToSaveAs);
                        strFileNameOnServer = FileNameToSaveAs;

                    }

                }
                catch
                {
                    strFileNameOnServer = "";
                }
                finally
                {
                    objFS = null;
                }


            }

            if (sActualNewFilename == "")
            {
                strFileNameOnServer = "";
            }
        }
        else
        {
            strFileNameOnServer = "";
            //HttpContext.Current.Response.Redirect("cstmerr.aspx?id=13", true); // File format not supported
        }

        objFile = null;
        sClientFilename = null;
        sActualFilename = null;
        //indexPos = null;
        sFileExtension = null;
        strUploadAllowedFileTypes = null;

        return strFileNameOnServer;

    }

    /// <summary>
    /// Checks for existance of file and if present it will be deleted
    /// </summary>
    /// <param name="FullFilePath">fully qulified file path</param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static bool CheckAndDeleteFile(string FullFilePath)
    {

        bool blnResult = false;
        if (File.Exists(FullFilePath) == true)
        {
            //*************************************
            //code to uncheck file from read only
            FileInfo objFS = new FileInfo(FullFilePath);
            if (objFS.IsReadOnly == true)
            {
                objFS.IsReadOnly = false;
            }//end of if (objFS.IsReadOnly == true)
            objFS = null;
            //*************************************
            File.Delete(FullFilePath);
            blnResult = true;
        }

        return blnResult;

    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 06-08-15
    /// Scope   : Set languageId of a store
    /// </summary>
    /// <returns></returns>
    /// <remarks></remarks>
    public static void SetLanguageId(Int16 intLanguageId)
    {
        try
        {
            HttpContext.Current.Session["LanguageId"] = intLanguageId;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 06-08-15
    /// Scope   : Get languageId of a store
    /// </summary>
    /// <returns>languageId of a store</returns>
    /// <remarks></remarks>
    public static Int16 GetLanguageId()
    {
        try
        {
            return Convert.ToInt16(HttpContext.Current.Session["LanguageId"]);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return 0;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 06-08-15
    /// Scope   : Set CurrencyId of a store
    /// </summary>
    /// <returns></returns>
    /// <remarks></remarks>
    public static void SetCurrencyId(Int16 intCurrencyId)
    {
        try
        {
            HttpContext.Current.Session["CurrencyId"] = intCurrencyId;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 06-08-15
    /// Scope   : Get CurrencyId of a store
    /// </summary>
    /// <returns>CurrencyId of a store</returns>
    /// <remarks></remarks>
    public static Int16 GetCurrencyId()
    {
        try
        {
            return Convert.ToInt16(HttpContext.Current.Session["CurrencyId"]);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return 0;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 06-08-15
    /// Scope   : Set CurrencySymbol of a store
    /// </summary>
    /// <returns></returns>
    /// <remarks></remarks>
    public static void SetCurrencySymbol(string strCurrencySymbol)
    {
        try
        {
            HttpContext.Current.Session["CurrencySymbol"] = strCurrencySymbol;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 06-08-15
    /// Scope   : Get CurrencySymbol of a store
    /// </summary>
    /// <returns>CurrencySymbol of a store</returns>
    /// <remarks></remarks>
    public static string GetCurrencySymbol()
    {
        try
        {
            return Convert.ToString(HttpContext.Current.Session["CurrencySymbol"]);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return string.Empty;
        }
    }

    //public static bool SendEmail(string subject, string mailBody, string fromEmailAdd, string toEmailAddress, string ccEmailAddress, string bccEmailAddress, ArrayList alstAttachments)
    //{
    //    bool _Return = false;
    //    try
    //    {
    //        using (MailMessage mm = new MailMessage(fromEmailAdd, toEmailAddress))
    //        {
    //            string host = ConfigurationManager.AppSettings["CS_SITESETTING_SMTPSERVER"];
    //            string username = ConfigurationManager.AppSettings["CS_SITESETTING_SMTPSERVER_USERNAME"];
    //            string password = ConfigurationManager.AppSettings["CS_SITESETTING_SMTPSERVER_PASSWORD"];
    //            mm.Subject = subject;
    //            mm.IsBodyHtml = true;
    //            mm.Body = mailBody;
    //            if (alstAttachments != null && alstAttachments.Count > 0)
    //            {
    //                foreach (HttpPostedFile file in alstAttachments)
    //                {
    //                    mm.Attachments.Add(new Attachment(file.InputStream, Path.GetFileName(file.FileName), file.ContentType));
    //                }
    //            }
    //            SmtpClient smtp = new SmtpClient();
    //            smtp.Host = host;
    //            smtp.EnableSsl = false;
    //            NetworkCredential NetworkCred = new NetworkCredential(username, password);
    //            smtp.UseDefaultCredentials = true;
    //            smtp.Credentials = NetworkCred;
    //            smtp.Port = 587;
    //            smtp.Send(mm);
    //            _Return = true;
    //        }
    //    }
    //    catch { }
    //    return _Return;
    //}

    /// <summary>
    /// Used to send Email
    /// </summary>
    /// <param name="ToMail">To Mail Id</param>
    /// <param name="FromMail">From Mail Id</param>
    /// <param name="CCMail">Cc Mail Id</param>
    /// <param name="BCCMail">Bcc Mail Id</param>
    /// <param name="Subject">Email Subject</param>
    /// <param name="Message">Email Body</param>
    ///<param name="AttachmentPath">Physical path for of file</param>
    ///<param name="FileName">Optional New File Name</param>
    public static bool SendEmail(string ToMail, string FromMail, string ReplyEmail, string CCMail, string BCCMail,
        string Subject, string Message, string AttachmentPath, string FileName, bool IsBodyHTML = true)
    {
        bool _Return = false;
        try
        {
            ToMail = HttpUtility.HtmlDecode(ToMail);
            ReplyEmail = HttpUtility.HtmlDecode(ReplyEmail);
            CCMail = HttpUtility.HtmlDecode(CCMail);
            BCCMail = HttpUtility.HtmlDecode(BCCMail);
            string SMTPHost = ConfigurationManager.AppSettings["CS_SITESETTING_SMTPSERVER"];
            string SMTPUserName = ConfigurationManager.AppSettings["CS_SITESETTING_SMTPSERVER_USERNAME"];
            string SMTPPassword = ConfigurationManager.AppSettings["CS_SITESETTING_SMTPSERVER_PASSWORD"];
            Int32 SMTPPort = Convert.ToInt32(ConfigurationManager.AppSettings["CS_SITESETTING_SMTPSERVER_SMTPPort"]);
            FromMail = ConfigurationManager.AppSettings["From_Email"];

            // Added by Snehal - 03 02 2017
            StoreBE objStore = new StoreBE();
            objStore = StoreBL.GetStoreDetails();

            if (String.IsNullOrEmpty(FromMail))
                FromMail = "no_reply@brand-estore.com";
            if (SMTPUserName.ToString().Length == 0)
                return _Return;
            using (MailMessage objm = new MailMessage())
            {
                string strFromEmail = "";
                EmailManagementBE.FromEmailBE objEmailBE = objStore.FromEmailIdAlias.FirstOrDefault(x => x.Id > 0);
                if (objEmailBE != null)
                {
                    strFromEmail = objEmailBE.FromEmailIdAlias;
                }
                objm.From = new MailAddress(FromMail, strFromEmail); // Snehal 03 02 2017

                if (!string.IsNullOrEmpty(ToMail))
                    foreach (string item in ToMail.Split(','))
                        objm.To.Add(item);

                if (!string.IsNullOrEmpty(CCMail))
                    objm.CC.Add(CCMail);

                if (!string.IsNullOrEmpty(BCCMail))
                    objm.Bcc.Add(BCCMail);

                if (!string.IsNullOrEmpty(FromMail))
                    objm.ReplyToList.Add(new MailAddress(FromMail, objStore.StoreName.Trim())); // Snehal 03 02 2017

                objm.Subject = Subject;
                objm.Body = HttpUtility.HtmlDecode(Message);
                objm.IsBodyHtml = IsBodyHTML;

                if (!string.IsNullOrEmpty(ReplyEmail))
                    objm.ReplyToList.Add(ReplyEmail);

                string sendEmailsFrom = SMTPUserName;
                string sendEmailsFromPassword = SMTPPassword;

                if (!string.IsNullOrEmpty(AttachmentPath))
                {
                    Attachment attachment = new Attachment(AttachmentPath);
                    //string[] spt = AttachmentPath.Split('\\');
                    //if (spt.Length > 0)
                    //{
                    //    string fname = spt[spt.Length - 1];
                    //    string fileExtn = Path.GetExtension(AttachmentPath);
                    //    attachment.ContentDisposition.FileName = fname;
                    //    objm.Attachments.Add(attachment);
                    //}
                    //string fileExtn = Path.GetExtension(AttachmentPath);
                    attachment.ContentDisposition.FileName = Path.GetFileName(AttachmentPath);
                    objm.Attachments.Add(attachment);
                }
                NetworkCredential cred = new NetworkCredential(sendEmailsFrom, sendEmailsFromPassword);

                SmtpClient mailClient = new SmtpClient(SMTPHost, SMTPPort);
                mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                mailClient.UseDefaultCredentials = false;
                mailClient.Timeout = 20000; // Set timeout
                mailClient.Credentials = cred;
                mailClient.Send(objm);
                _Return = true;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return _Return;
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 13-08-15
    /// Scope   : Return discount code in brackets 
    /// </summary>
    /// <param name="strDiscCode"></param>
    /// <returns>discount code in brackets </returns>
    /// <remarks></remarks>
    public static string FindFormattedDiscountCode(string strDiscCode)
    {
        string strActualDiscountCode = "";
        try
        {
            string strDiscountCode = strDiscCode;
            int intStartIndex = 0;
            int codeCounter = 0;
            int intCurCode = 0;
            string prevCode = "";
            string curCode = "";


            strDiscountCode = strDiscountCode.Trim().Replace(")", "").Replace("(", "").Replace(" ", "");
            if (strDiscountCode == "")
            {
                return strActualDiscountCode;
            }

            bool blnDataAvailable = true;

            prevCode = strDiscountCode.Substring(intStartIndex, 1);
            while (blnDataAvailable)
            {
                if (intStartIndex >= strDiscountCode.Length)
                {
                    blnDataAvailable = false;
                }
                else
                {
                    curCode = strDiscountCode.Substring(intStartIndex, 1);
                    if (int.TryParse(curCode, out intCurCode) == true)
                    {
                        strActualDiscountCode = "(" + strDiscountCode + ")";
                        return strActualDiscountCode;
                    }
                    else
                    {
                        if (curCode == prevCode)
                        {
                            codeCounter++;
                        }
                        else
                        {
                            if (codeCounter > 1)
                            {
                                strActualDiscountCode = strActualDiscountCode + codeCounter.ToString() + prevCode;
                            }
                            else
                            {
                                strActualDiscountCode = strActualDiscountCode + prevCode;
                            }
                            codeCounter = 1;
                        }
                    }
                    prevCode = curCode;
                    intStartIndex++;
                }
            }
            if (codeCounter > 1)
            {
                strActualDiscountCode = strActualDiscountCode + codeCounter.ToString() + prevCode;
            }
            else
            {
                strActualDiscountCode = strActualDiscountCode + prevCode;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return strActualDiscountCode;
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 14-08-15
    /// Scope   : Return true if file exists else false
    /// </summary>
    /// <param name="strFilePath"></param>
    /// <returns>True / False </returns>
    /// <remarks></remarks>
    public static bool IsFileExists(string strFilePath)
    {
        try
        {
            return File.Exists(strFilePath);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }

    /// <summary>
    /// Author  : Sanchit Patne
    /// Date    : 17-08-15
    /// Scope   : Returns a string representing Image's virtual path
    /// </summary>
    /// <returns>Virtual url of the link</returns>
    /// <remarks></remarks>
    public static string GetImageVirtualPath()
    {
        try
        {
            return Convert.ToString(ConfigurationManager.AppSettings["ImgURL"]);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return string.Empty;
        }
    }

    /// <summary>
    /// Author  : Sachin Chauhan
    /// Date    : 19-08-15
    /// Scope   : Returns a string after reading a file from start to end
    /// </summary>
    /// <returns>Virtual url of the link</returns>
    /// <remarks></remarks>
    public static string ReadFile(string strFilePath)
    {
        string strData = "";
        StreamReader srData = null;
        try
        {
            srData = new StreamReader(strFilePath);
            strData = srData.ReadToEnd();
        }
        catch { }
        finally
        {
            if (srData != null)
            {
                srData.Close();
                srData.Dispose();
            }
            srData = null;
        }

        return strData;
    }

    public static bool CopyDirectory(string sourceFullyQulifiedDirectoryPath, string destinationFullyQulifiedDirectoryPath, bool overwriteFiles)
    {
        FileInfo[] objSourceFolderFiles;
        int intCounter;
        FileInfo objFileInfo;
        DirectoryInfo objSourceDirectoryInfo = new DirectoryInfo(sourceFullyQulifiedDirectoryPath);
        DirectoryInfo objDestinationDirectoryInfo = new DirectoryInfo(destinationFullyQulifiedDirectoryPath);
        bool blnResult = false;

        if (objSourceDirectoryInfo.Exists == true)
        {
            try
            {
                //*****************************************

                DirectorySecurity objDirectorySecurity = null;
                FileSystemAccessRule objFileSystemAccessRule = null;

                if (objDestinationDirectoryInfo.Exists == false)
                {
                    objDestinationDirectoryInfo.Create();
                    //*************************************************************
                    //code to add the access level for folder and its contents
                    //this is done so that it can be deleted programmetically
                    objDirectorySecurity = new System.Security.AccessControl.DirectorySecurity();
                    objFileSystemAccessRule = new FileSystemAccessRule("IIS_IUSRS", FileSystemRights.Read, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);

                    objDirectorySecurity = objDestinationDirectoryInfo.GetAccessControl(AccessControlSections.Access);
                    objDirectorySecurity.AddAccessRule(objFileSystemAccessRule);
                    objDestinationDirectoryInfo.SetAccessControl(objDirectorySecurity);

                    objDirectorySecurity = null;
                    objFileSystemAccessRule = null;
                    //*************************************************************

                }

                //*****************************************
                //code to copy files within parent directory
                objSourceFolderFiles = objSourceDirectoryInfo.GetFiles();

                if (objSourceFolderFiles != null)
                {
                    for (intCounter = 0; intCounter < objSourceFolderFiles.Length; intCounter++)
                    {
                        objFileInfo = (FileInfo)objSourceFolderFiles.GetValue(intCounter);

                        if (overwriteFiles == true)
                        {
                            if (objFileInfo.IsReadOnly == true)
                                objFileInfo.IsReadOnly = false;

                            objFileInfo.CopyTo(Path.Combine(objDestinationDirectoryInfo.FullName, objFileInfo.Name), true);
                        }
                        else
                        {
                            if (File.Exists(Path.Combine(objDestinationDirectoryInfo.FullName, objFileInfo.Name)) == false)
                            {
                                if (objFileInfo.IsReadOnly == true)
                                    objFileInfo.IsReadOnly = false;
                                objFileInfo.CopyTo(Path.Combine(objDestinationDirectoryInfo.FullName, objFileInfo.Name), false);
                            }
                        }//end of else of if (overwriteFiles == true)

                        objFileInfo = null;

                    }//end of for (intCounter = 0; intCounter < objSourceFolderFiles.Length; intCounter++)

                }//end of if (objSourceFolderFiles != null)
                //*****************************************

                //*****************************************
                //code to copy directories within parent directory
                DirectoryInfo[] objSourceSubDirectoryInfo;
                DirectoryInfo objDirectoryInfo;
                objSourceSubDirectoryInfo = objSourceDirectoryInfo.GetDirectories();

                if (objSourceSubDirectoryInfo != null)
                {
                    for (intCounter = 0; intCounter < objSourceSubDirectoryInfo.Length; intCounter++)
                    {
                        objDirectoryInfo = (DirectoryInfo)objSourceSubDirectoryInfo.GetValue(intCounter);
                        blnResult = CopyDirectory(objDirectoryInfo.FullName, Path.Combine(objDestinationDirectoryInfo.FullName, objDirectoryInfo.Name), overwriteFiles);

                    }//end of for (intCounter = 0; intCounter < objSourceSubDirectoryInfo.Length; intCounter++)

                }//end of if (objSourceSubDirectoryInfo != null)
                //*****************************************
                blnResult = true;
            }
            catch (Exception ex)
            {
                blnResult = false;
                throw ex;
            }
        }//end of if (objSourceDirectoryInfo.Exists == true)
        else
        {
            blnResult = true;
        }//end of else of if (objSourceDirectoryInfo.Exists == true)

        return blnResult;
    }

    /// <summary>
    /// Author  : Prajwal Hegde
    /// Date    : 04-09-15
    /// Scope   : Returns a list of all countries from database
    /// </summary>
    /// <returns>List of countries</returns>
    /// <remarks></remarks>
    public static List<CountryBE> GetCountries()
    {
        List<CountryBE> lstCountry = new List<CountryBE>();
        lstCountry = CountryBL.GetAllCountries();
        return lstCountry;
    }


    /// <summary>
    /// Author  : Prajwal Hegde
    /// Date    : 08-09-15
    /// Scope   : Returns IP Address (IPv4) of client 
    /// </summary>
    /// <returns>List of countries</returns>
    /// <remarks></remarks>
    public static string GetIpAddress()
    {
        string ip = "";
        IPAddress[] ipv4Addresses = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
        ip = ipv4Addresses[0].ToString();
        return ip;
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 14-09-15
    /// Scope   : Returns a true if input is valid numeric
    /// </summary>
    /// <returns>Returns a true if input is valid numeric</returns>
    /// <remarks></remarks>
    public static bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
    {
        Double result;
        return Double.TryParse(val, NumberStyle,
            System.Globalization.CultureInfo.CurrentCulture, out result);
    }

    /// <summary>
    /// Decrypt query string.
    /// </summary>
    /// <param name="encryptedStrings">Encrypted query string.</param>
    /// <param name="key">Key, being used to decrypt.</param>
    /// <remarks>The query string object replaces '+' character with an empty character.</remarks>
    /// <returns></returns>
    public static string DecryptQueryStrings(string encryptedStrings, string key)
    {
        return Encryption64.Decrypt(encryptedStrings.Replace(" ", "+"), key);
    }

    //added for parameter Encryption...
    public static string EncryptURL(Hashtable hstParam, string strPageName)
    {
        NameValueCollection queryStrings = new NameValueCollection();

        IDictionaryEnumerator di = hstParam.GetEnumerator();

        while (di.MoveNext())
        {
            queryStrings.Add(Convert.ToString(di.Key), Convert.ToString(di.Value));
        }

        // Encrypt query strings
        string encryptedString = EncryptQueryStrings(queryStrings, WebConfigurationManager.AppSettings["CryptoKey"]);

        return string.Concat(strPageName, encryptedString);
    }

    /*Sachin Chauhan Start : 04 02 2015*/
    /// <summary>
    /// Returns application level Crypto Key
    /// </summary>
    /// <param name="unencryptedStrings">Unencrypted query strings.</param>
    /// <param name="key">Key, being used to encrypt.</param>
    /// <returns></returns>
    public static string GetCryptoKey()
    {
        return WebConfigurationManager.AppSettings["CryptoKey"].ToString();
    }

    /// <summary>
    /// Returns Short URL which can be shared easliy on Social Media Platforms
    /// </summary>
    /// <param name="long url">Long url which requires to be shortened</param>
    /// <returns>Short URL to be shared on Social Media platform</returns>
    /// 
    public static string GetBitlyURLForSocialMedia(string longUrl)
    {
        string bitlyUser = WebConfigurationManager.AppSettings["BitlyUser"].ToString();
        string bitlyAPIKey = WebConfigurationManager.AppSettings["BitlyAPIKey"].ToString();
        //BitlyService s = new BitlyService(bitlyUser, bitlyAPIKey);
        string bitlyShortUrl = String.Empty;

        try
        {
            longUrl = Uri.EscapeUriString(longUrl);
            string reqUri =
                String.Format("http://api.bit.ly/v3/shorten?" +
                "login={0}&apiKey={1}&format={2}&longUrl={3}",
                bitlyUser, bitlyAPIKey, "xml", longUrl);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(reqUri);
            req.Timeout = 10000; // 10 seconds

            // if the function fails and format==txt throws an exception
            Stream stm = req.GetResponse().GetResponseStream();


            XmlDocument doc = new XmlDocument();
            doc.Load(stm);

            // error checking for xml
            if (doc["response"]["status_code"].InnerText != "200")
                throw new WebException(doc["response"]["status_txt"].InnerText);

            bitlyShortUrl = doc["response"]["data"]["url"].InnerText;
        }
        catch (Exception bEx)
        {
            Exceptions.WriteExceptionLog(bEx);
        }

        return bitlyShortUrl;
    }

    /// <summary>
    /// Returns Short URL which can be shared easliy on Social Media Platforms
    /// </summary>
    /// <param name="long url">Long url which requires to be shortened</param>
    /// <returns>Short URL to be shared on Social Media platform</returns>
    /// 
    public static string GetGoogleURLForSocialMedia(string longUrl)
    {
        WebRequest request = WebRequest.Create("https://www.googleapis.com/urlshortener/v1/url?key=" + WebConfigurationManager.AppSettings["GoogleURLShortnerAPIKey"].ToString() + "");
        request.Method = "POST";
        request.ContentType = "application/json";
        string requestData = string.Format(@"{{""longUrl"": ""{0}""}}", longUrl);
        byte[] requestRawData = Encoding.ASCII.GetBytes(requestData);
        request.ContentLength = requestRawData.Length;
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(requestRawData, 0, requestRawData.Length);
        requestStream.Close();

        WebResponse response = request.GetResponse();
        StreamReader responseReader = new StreamReader(response.GetResponseStream());
        string responseData = responseReader.ReadToEnd();
        responseReader.Close();

        var deserializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        GoogleURLShortner results = deserializer.Deserialize<GoogleURLShortner>(responseData);

        return results.id;
    }
    /*Sachin Chauhan End : 04 02 2015 */

    /// <summary>
    /// Encrypt query strings from name value collection.
    /// </summary>
    /// <param name="unencryptedStrings">Unencrypted query strings.</param>
    /// <param name="key">Key, being used to encrypt.</param>
    /// <returns></returns>
    public static string EncryptQueryStrings(NameValueCollection unencryptedStrings, string key)
    {
        StringBuilder strings = new StringBuilder();

        foreach (string stringKey in unencryptedStrings.Keys)
        {
            if (strings.Length > 0) strings.Append("&");
            strings.Append(string.Format("{0}={1}", stringKey, unencryptedStrings[stringKey]));
        }

        return string.Concat("request=", Encryption64.Encrypt(strings.ToString(), key));
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 30-09-15
    /// Scope   : Returns true if regex matches else false
    /// </summary>    
    /// <param name="strvalue"></param>
    /// <returns>true / false</returns>
    /// <remarks></remarks>
    public static bool IsEmpty(string strvalue)
    {
        try
        {
            Regex reg = new Regex("^$");
            return reg.IsMatch(strvalue.Trim());
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 30-09-15
    /// Scope   : Returns true if regex matches else false
    /// </summary>
    /// <param name="intLength"></param>
    /// <param name="strvalue"></param>
    /// <returns>true / false</returns>
    /// <remarks></remarks>
    public static bool IsAngularBrackets(string strvalue, int intLength)
    {
        try
        {
            if (intLength == 0)
            {
                Regex reg = new Regex("^[^<>]$");
                return reg.IsMatch(strvalue.Trim());
            }
            else
            {
                Regex reg = new Regex("^[^<>]{1," + intLength + "}$");
                return reg.IsMatch(strvalue.Trim());
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }
    
    /// <summary>
    /// Author  :   Vikram Singh
    /// Scope   :   To test whether String has Special characters
    /// </summary>
    /// <param name="strvalue"></param>
    /// <returns>Boolean</returns>
    public static bool IsSpecialCharacters(string strvalue)
    {
        try
        {
            if (strvalue != "")
            {
                string Registrationspecialcharacters = GlobalFunctions.GetSetting("Registrationspecialcharacters");
                // Commented by SHRIGANESH 19 Sept 2016
                //Regex reg = new Regex("^[" + Registrationspecialcharacters + "]$");
                //return reg.IsMatch(strvalue.Trim());
                Regex reg = new Regex(Registrationspecialcharacters);
                return reg.IsMatch(strvalue.Trim());
            }
            else
            {
                return true;
            }
            //return System.Text.RegularExpressions.Regex.IsMatch(strvalue.Trim(), "^[a-zA-Z0-9]+$"); //reg.IsMatch(strvalue.Trim());
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }

    public static bool IsSpecialCharactersCoupon(string strvalue)
    {
        try
        {
            if (strvalue != "")
            {
                string Couponcodespecialcharacters = GlobalFunctions.GetSetting("Couponcodespecialcharacters");
                Regex reg = new Regex(Couponcodespecialcharacters);
                return reg.IsMatch(strvalue.Trim());
            }
            else
            {
                return true;
            }
            //return System.Text.RegularExpressions.Regex.IsMatch(strvalue.Trim(), "^[a-zA-Z0-9]+$"); //reg.IsMatch(strvalue.Trim());
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }




    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 30-09-15
    /// Scope   : Returns true if regex matches else false
    /// </summary>
    /// <param name="intLength"></param>
    /// <param name="strvalue"></param>
    /// <returns>true / false</returns>
    /// <remarks></remarks>
    public static bool IsValidPostCode(string strvalue, int intLength)
    {
        try
        {
            if (intLength == 0)
            {
                Regex reg = new Regex("^[A-Za-z0-9 --]$");
                //Regex reg = new Regex("^[A-Za-z\d\s_-]+$");
                return reg.IsMatch(strvalue.Trim());
            }
            else
            {
                Regex reg = new Regex("^[A-Za-z0-9 --]{1," + intLength + "}$");
                //Regex reg = new Regex("^[A-Za-z\d\s_-]+$]{1," + intLength + "}$");
                return reg.IsMatch(strvalue.Trim());
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 30-09-15
    /// Scope   : Returns true if regex matches else false
    /// </summary>
    /// <param name="intLength"></param>
    /// <param name="strvalue"></param>
    /// <returns>true / false</returns>
    /// <remarks></remarks>
    public static bool IsValidPhone(string strvalue, int intLength)
    {
        try
        {
            if (intLength == 0)
            {
                Regex reg = new Regex("^[0-9 +)(-]$");
                return reg.IsMatch(strvalue.Trim());
            }
            else
            {
                Regex reg = new Regex("^[0-9 +)(-]{1," + intLength + "}$");
                return reg.IsMatch(strvalue.Trim());
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }

    /// <summary>
    /// Author  : Prajwal Hegde
    /// Date    : 10-07-15
    /// Scope   : Returns Encrypted text
    /// </summary>
    /// <param name="clearText"></param>
    /// <returns>Encrypted text</returns>
    public static string Encrypt(string clearText)
    {
        try
        {
            string EncryptionKey = "BA0PV0JD070V30";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return clearText;
    }

    /// <summary>
    /// Author  : Prajwal Hegde
    /// Date    : 10-07-15
    /// Scope   : Returns Decrypted text
    /// </summary>
    /// <param name="clearText"></param>
    /// <returns>Decrypted text</returns>
    public static string Decrypt(string cipherText)
    {
        try
        {
            string EncryptionKey = "BA0PV0JD070V30";
            byte[] cipherBytes = Convert.FromBase64String(cipherText.Replace(' ', '+'));
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return "";
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 15-10-15
    /// Scope   : Returns the discounted value
    /// </summary>
    /// <param name="dAmount"></param>
    /// <param name="dDiscountPercent"></param>
    /// <returns>double</returns>
    /// <remarks></remarks>
    public static double DiscountedAmount(double dAmount, double dDiscountPercent)
    {
        try
        {
            return (dAmount - ((dAmount * dDiscountPercent) / 100));
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return dAmount;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 16-10-15
    /// Scope   : Returns the converted value with precision
    /// </summary>
    /// <param name="valueToConvert"></param>
    /// <param name="precisionAfterDecimal"></param>
    /// <returns>string</returns>
    /// <remarks></remarks>
    public static string ConvertToDecimalPrecision(decimal valueToConvert)
    {
        string strFinalValue = null;
        string strValue = "";
        string[] astrValue = null;
        char Delimiter = '.';
        int precisionAfterDecimal = Convert.ToInt16(ConfigurationManager.AppSettings["precisionAfterDecimal"]);
        try
        {
            astrValue = Convert.ToString(valueToConvert).Trim().Split(Delimiter);
            if (astrValue.Length > 1)
            {
                strValue = astrValue.GetValue(1).ToString();

                switch (precisionAfterDecimal)
                {
                    case 2:
                        if (strValue.Length == 1)
                        {
                            strValue = strValue + "0";
                        }
                        break;
                    case 3:
                        if (strValue.Length == 1)
                        {
                            strValue = strValue + "00";
                        }
                        else if (strValue.Length == 2)
                        {
                            strValue = strValue + "0";
                        }
                        break;
                    case 4:
                        if (strValue.Length == 1)
                        {
                            strValue = strValue + "000";
                        }
                        else if (strValue.Length == 2)
                        {
                            strValue = strValue + "00";
                        }
                        else if (strValue.Length == 3)
                        {
                            strValue = strValue + "0";
                        }
                        break;
                    case 5:
                        if (strValue.Length == 1)
                        {
                            strValue = strValue + "0000";
                        }
                        else if (strValue.Length == 2)
                        {
                            strValue = strValue + "000";
                        }
                        else if (strValue.Length == 3)
                        {
                            strValue = strValue + "00";
                        }
                        else if (strValue.Length == 4)
                        {
                            strValue = strValue + "0";
                        }
                        break;
                }
            }
            else if (astrValue.Length == 1)
            {
                switch (precisionAfterDecimal)
                {
                    case 2:
                        strValue = "00";
                        break;
                    case 3:
                        strValue = "000";
                        break;
                    case 4:
                        strValue = "0000";
                        break;
                    case 5:
                        strValue = "00000";
                        break;
                }
            }

            if (astrValue.Length != 0)
            {
                strFinalValue = astrValue.GetValue(0) + "." + strValue;
            }
            decimal value = Convert.ToDecimal(strFinalValue);
            value = Math.Round(value, precisionAfterDecimal, MidpointRounding.AwayFromZero);
            string strPrice = Convert.ToString(value);
            return strPrice;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return Convert.ToString(valueToConvert);
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 26-10-15
    /// Scope   : Returns true if password is valid
    /// </summary>
    /// <param name="regExp"></param>
    /// <param name="strPassword"></param>
    /// <returns>bool</returns>
    /// <remarks></remarks>
    public static bool ValidatePassword(string regExp, string strPassword)
    {
        try
        {
            Regex reg = new Regex(regExp); //(regExp, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(strPassword.Trim());
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }

    public static bool SendRegistrationMail(string strEmail, string strFirstName, string strLastName)
    {
        bool bStatus = false;
        try     
        {
            string Subject = "";
            StringBuilder MailBody = new StringBuilder();
            string ToAddress = strEmail.Trim();
            string FromAddress = string.Empty;
            string CC = string.Empty;
            string BCC = string.Empty;
            bool IsBodyHTML = true;
            List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
            EmailManagementBE objEmail = new EmailManagementBE();
            objEmail.EmailTemplateName = "Registration Successful";
            objEmail.LanguageId = GlobalFunctions.GetLanguageId();
            lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
            if (lstEmail != null && lstEmail.Count > 0)
            {
                Subject = lstEmail[0].Subject;
                FromAddress = lstEmail[0].FromEmailId;
                CC = lstEmail[0].CCEmailId;
                BCC = lstEmail[0].BCCEmailId;
                IsBodyHTML = lstEmail[0].IsHtml;
                MailBody.Append(lstEmail[0].Body.Replace("@UserName", strFirstName.Trim() + " ").Replace("@Email", strEmail.Trim()));
                GlobalFunctions.SendEmail(ToAddress, FromAddress, "", CC, BCC, Subject, MailBody.ToString(), "", "", IsBodyHTML);
                bStatus = true;
            }
            else
                bStatus = false;

            return bStatus;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return bStatus;
        }
    }

    public static bool SendMail(string strEmail, string strFirstName, string strLastName)
    {
        bool bStatus = false;
        try
        {
            string Subject = "";
            StringBuilder MailBody = new StringBuilder();
            string ToAddress = strEmail.Trim();
            string FromAddress = string.Empty;
            string CC = string.Empty;
            string BCC = string.Empty;
            bool IsBodyHTML = true;
            List<EmailManagementBE> lstEmail = new List<EmailManagementBE>();
            EmailManagementBE objEmail = new EmailManagementBE();
            objEmail.EmailTemplateName = "Registration Successful";
            objEmail.LanguageId = GlobalFunctions.GetLanguageId();
            lstEmail = EmailManagementBL.GetEmailTemplates(objEmail, "S");
            if (lstEmail != null && lstEmail.Count > 0)
            {
                Subject = lstEmail[0].Subject;
                FromAddress = lstEmail[0].FromEmailId;
                CC = lstEmail[0].CCEmailId;
                BCC = lstEmail[0].BCCEmailId;
                IsBodyHTML = lstEmail[0].IsHtml;
                MailBody.Append(lstEmail[0].Body.Replace("@UserName", strFirstName.Trim() + " " + strLastName.Trim()).Replace("@Email", strEmail.Trim()));
                GlobalFunctions.SendEmail(ToAddress, FromAddress, "", CC, BCC, Subject, MailBody.ToString(), "", "", IsBodyHTML);
                bStatus = true;
            }
            else
                bStatus = false;

            return bStatus;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return bStatus;
        }
    }

    public static void writePunchoutXML(string strxml)
    {
        try
        {
            TextWriter txtwr = new StreamWriter(GlobalFunctions.GetPhysicalFolderPath() + "XML\\Punchout\\PunchoutOrder.txt", true);
            txtwr.WriteLine("\r\n\r\n");
            txtwr.WriteLine("****************************Punchout Information - **********Start*************** " + DateTime.Now.ToString() + "************\r\n\r\n");
            txtwr.WriteLine(strxml);
            txtwr.WriteLine("****************************Punchout Information**********End***************************\r\n\r\n");
            txtwr.Close();
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    public static void writePunchoutLog(string strxml)
    {
        try
        {
            TextWriter txtwr = new StreamWriter(GlobalFunctions.GetPhysicalFolderPath() + "XML\\Punchout\\Punchoutlog.txt", true);
            txtwr.WriteLine("\r\n\r\n");
            txtwr.WriteLine("****************************Punchout Information - **********Start*************** " + DateTime.Now.ToString() + "************\r\n\r\n");
            txtwr.WriteLine(strxml);
            txtwr.WriteLine("****************************Punchout Information**********End***************************\r\n\r\n");
            txtwr.Close();
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    #region Code commented due to integartion of indeed
    
    
    /*
    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 21-11-15
    /// Scope   : Returns price or points based on the store setting
    /// </summary>
    /// <param name="strPrice"></param>
    /// <returns>string</returns>
    /// <remarks></remarks>
    /// 
   
    public static string DisplayPriceOrPoints(string strPrice, string strCurrencySymbol, Int16 intLanguageId)
    {
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled)
                {
                    //string test=objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureDefaultValue;
                    //double test1=Convert.ToDouble(test,CultureInfo.InvariantCulture);
                    double dConversionFactor = Convert.ToDouble(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureDefaultValue, CultureInfo.InvariantCulture.NumberFormat);
                    strPrice = strPrice.Replace(",", ".");
                    double DPrice = Convert.ToDouble(strPrice, CultureInfo.InvariantCulture.NumberFormat);
                    // strPrice = Convert.ToString(Math.Ceiling(Convert.ToDouble(strPrice) * dConversionFactor)) + " Points";
                    string[] values = null;
                    if (strPrice.Contains(","))
                    {
                        values = strPrice.Split(',');
                    }
                    else
                    {
                        values = strPrice.Split('.');
                    }

                    Double firstValue = double.Parse(values[0]);
                    Double secondValue = 00;
                    if (values.Length > 1)
                    {
                        secondValue = double.Parse(values[1]);
                    }
                    // Below LOC is added by SHRIGANESH to modify the conversion formula 19 August 2016

                    strPrice = Convert.ToString(Math.Ceiling(DPrice / dConversionFactor));
                    #region CODE COMMENTED BY SHRIGANESH TO MODIFY THE CONVERSION FORMULAE 19 AUGUST 2016
                    //if (secondValue == 00)
                    //{
                    //    strPrice = Convert.ToString(Math.Round(firstValue * dConversionFactor));
                    //}
                    //else if (secondValue >= 51)
                    //{
                    //    strPrice = Convert.ToString(Math.Round(firstValue * dConversionFactor) + 2);
                    //}
                    //else
                    //{
                    //    strPrice = Convert.ToString(Math.Round(firstValue * dConversionFactor) + 1);
                    //} 
                    #endregion
                    if (!string.IsNullOrEmpty(strCurrencySymbol))
                    {
                        strPrice = strPrice + PointsText(intLanguageId);
                    }
                }
                else
                    strPrice = strCurrencySymbol + strPrice;
            }
            else
                strPrice = strCurrencySymbol + strPrice;

            return strPrice;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return strPrice;
        }
    }
    */

    #endregion

    #region Indeed code added for point and currency    
    
    public static string DisplayPriceOrPoints(string strPrice, string strCurrencySymbol, Int16 intLanguageId)
    {
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled && GlobalFunctions.IsPointsEnbled())
                {
                    CurrencyBE objCurrencyCD = new CurrencyBE();
                    List<CurrencyBE> objCurrencyBE = null;
                    objCurrencyBE = CurrencyBL.GetCurrencyPointInvoiceDetails();
                    for (int i = 0; i < objCurrencyBE.Count; i++)
                    {

                        if (objCurrencyBE[i].CurrencySymbol.Trim() == strCurrencySymbol)
                        {
                            Double PointValues = double.Parse(objCurrencyBE[i].PointValue.ToString());
                            Double strPrice1 = double.Parse(strPrice.ToString());
                            Double calculateNewValue = PointValues * strPrice1;


                            //string test=objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureDefaultValue;
                            //double test1=Convert.ToDouble(test,CultureInfo.InvariantCulture);
                            //  double dConversionFactor = Convert.ToDouble(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureDefaultValue, CultureInfo.InvariantCulture.NumberFormat);
                            // strPrice = strPrice.Replace(",", ".");
                            //   double DPrice = Convert.ToDouble(calculateNewValue, CultureInfo.InvariantCulture.NumberFormat);
                            // strPrice = Convert.ToString(Math.Ceiling(Convert.ToDouble(strPrice) * dConversionFactor)) + " Points";
                            //string[] values = null;
                            //if (strPrice.Contains(","))
                            //{
                            //    values = strPrice.Split(',');
                            //}
                            //else
                            //{
                            //    values = strPrice.Split('.');
                            //}

                            //Double firstValue = double.Parse(values[0]);
                            //Double secondValue = 00;
                            //if (values.Length > 1)
                            //{
                            //    secondValue = double.Parse(values[1]);
                            //}
                            // Below LOC is added by SHRIGANESH to modify the conversion formula 19 August 2016

                            // strPrice = Convert.ToString(Math.Ceiling(DPrice / dConversionFactor));
                            strPrice = calculateNewValue.ToString("0.00");
                            #region CODE COMMENTED BY SHRIGANESH TO MODIFY THE CONVERSION FORMULAE 19 AUGUST 2016
                            //if (secondValue == 00)
                            //{
                            //    strPrice = Convert.ToString(Math.Round(firstValue * dConversionFactor));
                            //}
                            //else if (secondValue >= 51)
                            //{
                            //    strPrice = Convert.ToString(Math.Round(firstValue * dConversionFactor) + 2);
                            //}
                            //else
                            //{
                            //    strPrice = Convert.ToString(Math.Round(firstValue * dConversionFactor) + 1);
                            //} 
                        }

                    }
                            #endregion
                    if (!string.IsNullOrEmpty(strCurrencySymbol))
                    {
                        strPrice = strPrice + "  <span>" + PointsText(intLanguageId);
                    }
                }
                else
                    strPrice = strCurrencySymbol + strPrice;
            }
            else
                strPrice = strCurrencySymbol + strPrice;

            return strPrice;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return strPrice;
        }
    }
    public static string DisplayPriceAndPoints(string strPrice, string strCurrencySymbol, Int16 intLanguageId)
    {
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled && GlobalFunctions.IsPointsEnbled())
                {
                    strPrice = strCurrencySymbol + strPrice;
                }
                else
                    strPrice = strCurrencySymbol + strPrice;
            }
            else
                strPrice = strCurrencySymbol + strPrice;

            return strPrice;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return strPrice;
        }
    }
    public static string DisplayRoundOfPoints(string strPrice, string strCurrencySymbol, Int16 intLanguageId)
    {
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled && GlobalFunctions.IsPointsEnbled())
                {
                    CurrencyBE objCurrencyCD = new CurrencyBE();
                    List<CurrencyBE> objCurrencyBE = null;
                    objCurrencyBE = CurrencyBL.GetCurrencyPointInvoiceDetails();
                    for (int i = 0; i < objCurrencyBE.Count; i++)
                    {

                        if (objCurrencyBE[i].CurrencySymbol.Trim() == strCurrencySymbol)
                        {
                            Double PointValues = double.Parse(objCurrencyBE[i].PointValue.ToString());
                            if (strPrice.Contains(","))
                            {
                            strPrice= strPrice.Replace(",",".");

                            }
                            Double strPrice1 = double.Parse(strPrice.ToString(), CultureInfo.InvariantCulture);
                            Double calculateNewValue = PointValues * strPrice1;


                            //string test=objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureDefaultValue;
                            //double test1=Convert.ToDouble(test,CultureInfo.InvariantCulture);
                            //  double dConversionFactor = Convert.ToDouble(objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues.FirstOrDefault(v => v.IsEnabled == true).FeatureDefaultValue, CultureInfo.InvariantCulture.NumberFormat);
                            // strPrice = strPrice.Replace(",", ".");
                            //   double DPrice = Convert.ToDouble(calculateNewValue, CultureInfo.InvariantCulture.NumberFormat);
                            // strPrice = Convert.ToString(Math.Ceiling(Convert.ToDouble(strPrice) * dConversionFactor)) + " Points";
                            strPrice = calculateNewValue.ToString("0.00");
                            string[] values = null;
                            if (strPrice.Contains(","))
                            {
                                values = strPrice.Split(',');
                            }
                            else
                            {
                                values = strPrice.Split('.');
                            }

                            Double firstValue = double.Parse(values[0]);
                            Double secondValue = 00;
                            if (values.Length > 1)
                            {
                                secondValue = double.Parse(values[1]);
                            }
                            // Below LOC is added by SHRIGANESH to modify the conversion formula 19 August 2016

                            // strPrice = Convert.ToString(Math.Ceiling(DPrice / dConversionFactor));

                            #region CODE COMMENTED BY SHRIGANESH TO MODIFY THE CONVERSION FORMULAE 19 AUGUST 2016
                            if (secondValue == 00)
                            {
                                strPrice = Convert.ToString(Math.Round(firstValue));
                            }
                            else if (secondValue >= 51)
                            {
                                strPrice = Convert.ToString(Math.Round(firstValue) + 1);
                            }
                            else
                            {
                                strPrice = Convert.ToString(Math.Round(firstValue) + 1);
                            }
                        }

                    }
                            #endregion
                    if (!string.IsNullOrEmpty(strCurrencySymbol))
                    {
                        strPrice = strPrice + "  <span>" + PointsText(intLanguageId);
                    }
                }
                else
                    strPrice = strCurrencySymbol + strPrice;
            }
            else
                strPrice = strCurrencySymbol + strPrice;

            return strPrice;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return strPrice;
        }
    }
    public static string RemovePointsSpanIntheText(string value)
    {
        try
        {
            value = value.Replace(GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()), "");
            value = value.Replace("</span>", "");
            value = value.Replace("<span>", "").Trim();
            return value;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return value;
        }

    }
    
    #endregion
    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 21-11-15
    /// Scope   : Returns true if the website is based on points else false
    /// </summary>
    /// <returns>bool</returns>
    /// <remarks></remarks>
    public static bool IsPointsEnbled()
    {
        bool bStatus = false;
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SS_EnablePoints").FeatureValues[0].IsEnabled)
                {
                    if (HttpContext.Current.Session["User"] != null)
                    {
                        UserBE lstUser = lstUser = new UserBE();
                        lstUser = HttpContext.Current.Session["User"] as UserBE;
                        bStatus = UserTypesBL.IsPointEnabledForUT(lstUser.UserTypeID);
                    }
                    else if (HttpContext.Current.Session["GuestUser"] != null)
                    {
                        bStatus = UserTypesBL.IsPointEnabledForUT(1);
                    }
                    else
                    {
                        bStatus = UserTypesBL.IsPointEnabledForUT(1);
                    }
                    // bStatus = true;
                }
            }
            return bStatus;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return bStatus;
        }
    }

    public static bool IsOnlyCurrencyEnabled()
    {
        bool bStatus = false;
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (HttpContext.Current.Session["User"] != null)
                {
                    UserBE lstUser = lstUser = new UserBE();
                    lstUser = HttpContext.Current.Session["User"] as UserBE;
                    bStatus = UserTypesBL.IsOnlyCurrencyEnabledForUT(lstUser.UserTypeID);
                }
                else if (HttpContext.Current.Session["GuestUser"] != null)
                {
                    bStatus = UserTypesBL.IsOnlyCurrencyEnabledForUT(1);
                }
                else
                {
                    bStatus = UserTypesBL.IsOnlyCurrencyEnabledForUT(1);
                }
            }
            return bStatus;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return bStatus;
        }
    }

    public static bool IsBothCurrencyAndPointsEnabled()
    {
        bool bStatus = false;
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (HttpContext.Current.Session["User"] != null)
                {
                    UserBE lstUser = lstUser = new UserBE();
                    lstUser = HttpContext.Current.Session["User"] as UserBE;
                    bStatus = UserTypesBL.IsBothCurrencyAndPointsEnabled(lstUser.UserTypeID);
                }
                else if (HttpContext.Current.Session["GuestUser"] != null)
                {
                    bStatus = UserTypesBL.IsBothCurrencyAndPointsEnabled(1);
                }
                else
                {
                    bStatus = UserTypesBL.IsBothCurrencyAndPointsEnabled(1);
                }
            }
            return bStatus;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return bStatus;
        }
    }

    /// <summary>
    /// Author  : Hardik Gohil
    /// Date    : 04-10-16
    /// Scope   : Returns true if the website has Email basket button enabled else false
    /// </summary>
    /// <returns>bool</returns>
    /// <remarks></remarks>
    public static bool IsEmailMeBasketbtnEnabled()
    {
        bool bStatus = false;
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_Email_Me_Basket").FeatureValues[0].IsEnabled)
                {
                    bStatus = true;
                }
            }
            return bStatus;
        }
        catch(Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return bStatus;
        }
    }

    /// <summary>
    /// Author  : Hardik Gohil
    /// Date    : 21-FEB-2017
    /// Scope   : Returns true if the website has AllowPlaceOrder Checked(IsActive) else false
    /// </summary>
    /// <returns>bool</returns>
    public static bool SubmitEnquiry()
    {
        bool bAllowPlaceOrder = false;
        try
        {
            StoreBE objStoreBE = StoreBL.GetStoreDetails();
            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "SC_Submit_Enquiry").FeatureValues[0].IsEnabled)
                {
                    bAllowPlaceOrder = true;
                }
            }
            return bAllowPlaceOrder;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return bAllowPlaceOrder;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 21-11-15
    /// Scope   : Returns Points text translation in different language
    /// </summary>
    /// <param name="intLanguageId"></param>
    /// <returns>string</returns>
    /// <remarks></remarks>
    public static string PointsText(Int16 intLanguageId)
    {

        string strPointsText = string.Empty;
        try
        {
            if (GlobalFunctions.IsPointsEnbled())
            {
                List<StaticPageManagementBE> lstStaticPageBE = StaticPageManagementBL.GetAllResourceData();
                if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                {
                    lstStaticPageBE = lstStaticPageBE.FindAll(x => x.LanguageId == intLanguageId);
                    if (lstStaticPageBE != null && lstStaticPageBE.Count > 0)
                    {
                        strPointsText = lstStaticPageBE.FirstOrDefault(x => x.ResourceKey == "Generic_Points_Text").ResourceValue;
                    }
                    else
                    {
                        strPointsText = "Points";
                    }
                }
            }
            return strPointsText;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return strPointsText;
        }
    }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 21-11-15
    /// Scope   : Returns Points text translation in different language
    /// </summary>
    /// <param name="intLanguageId"></param>
    /// <returns>string</returns>
    /// <remarks></remarks>
    public static string GetCurrencySymbolByCurrencyId(Int16 intCurrencyId)
    {
        string strCurrencySymbol = string.Empty;
        try
        {
            if (!GlobalFunctions.IsPointsEnbled())
            {
                StoreBE objStoreBE = StoreBL.GetStoreDetails();
                strCurrencySymbol = objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == intCurrencyId).CurrencySymbol;
            }

            return strCurrencySymbol;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return strCurrencySymbol;
        }
    }

    /// <summary>
    /// Author : Sripal
    /// Added to remove the x_ which is appended by the Sanitizer MS to avoid XSS attacks.
    /// </summary>
    /// <param name="html"></param>
    /// <returns></returns>
    public static string RemoveSanitisedPrefixes(string html)
    {
        Match match = Regex.Match(html, "(id|class)=\"?(x_).+\"?", RegexOptions.IgnoreCase);
        if (match.Success)
        {
            string key = match.Groups[2].Value;
            html = html.Replace(key, "");
            html = html.Replace("\"", "");
            html = html.Replace(System.Environment.NewLine, " ");
        }
        return html;
    }

    public static bool IsAuthorized(string URLSegment)
    {
        bool IsAuthorize = false;
        UserBE user = null;
        RoleManagementBE objRoleManagementBE = new RoleManagementBE();
        try
        {
            user = (UserBE)HttpContext.Current.Session["User"];

            List<MenuBE> objMenuBE = MenuBL.GetAllStoreMenuDetails(user != null ? user.RoleId : Convert.ToInt16(0), true);
            MenuBE authorizeMenu = objMenuBE.FirstOrDefault(x => x.MenuUrl.ToLower().Contains(URLSegment.ToLower()));
            if (authorizeMenu != null)
                IsAuthorize = true;
            else
                HttpContext.Current.Response.Redirect("~/UnAuthorized.html");
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        finally
        {
            user = null;
            objRoleManagementBE = null;
        }
        return IsAuthorize;
    }

    public static bool IsValidImageNameWithExtension(string FileName)
    {
        bool IsAllowed = false;
        try
        {

            string FileExt = Path.GetExtension(FileName);
            string strFileName = Path.GetFileNameWithoutExtension(FileName);
            string strUploadAllowedFileTypes = ConfigurationManager.AppSettings["ValidUploadImageFileExtensions"].ToString();
            if (Convert.ToBoolean(strUploadAllowedFileTypes.Contains(FileExt.Replace(".", ""))))
            {
                //if (Regex.IsMatch(strFileName.ToString(), @"^[a-zA-Z0-9]{1,}$"))
                //{
                IsAllowed = true;
                //}
                //else
                //{
                //    IsAllowed = false;
                //}
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return IsAllowed;
        //return (FileName.EndsWith(".png") || FileName.EndsWith(".jpg") || FileName.EndsWith(".jpeg") || FileName.EndsWith(".gif"));
    }

    #region Added To check The Content of File for Security

    public static bool IsValidMIMEImageType(string UploadedFilePath)
    {
        bool IsValid = false;
        try
        {
            // DICTIONARY OF ALL IMAGE FILE HEADER
            Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();
            imageHeader.Add("JPG", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });
            imageHeader.Add("JPEG", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });
            imageHeader.Add("PNG", new byte[] { 0x89, 0x50, 0x4E, 0x47 });
            imageHeader.Add("TIF", new byte[] { 0x49, 0x49, 0x2A, 0x00 });
            imageHeader.Add("TIFF", new byte[] { 0x49, 0x49, 0x2A, 0x00 });
            imageHeader.Add("GIF", new byte[] { 0x47, 0x49, 0x46, 0x38 });
            imageHeader.Add("BMP", new byte[] { 0x42, 0x4D });
            imageHeader.Add("ICO", new byte[] { 0x00, 0x00, 0x01, 0x00 });
            imageHeader.Add("SVG", new byte[] { });
            byte[] header;

            if (Convert.ToString(UploadedFilePath) != "")
            {
                // GET FILE EXTENSION

                string fileExt;
                fileExt = UploadedFilePath.Substring(UploadedFilePath.LastIndexOf('.') + 1).ToUpper();
                if (fileExt == "SVG")
                {
                    IsValid = true;
                }
                else
                {
                    string FileName = Path.GetFileName(UploadedFilePath);

                    bool res = IsValidImageNameWithExtension(FileName);
                    if (res)
                    {
                        // CUSTOM VALIDATION GOES HERE BASED ON FILE EXTENSION IF ANY
                        byte[] tmp = imageHeader[fileExt];
                        header = new byte[tmp.Length];
                        // GET HEADER INFORMATION OF UPLOADED FILE
                        byte[] uploadedFileContent = File.ReadAllBytes(UploadedFilePath);

                        if (uploadedFileContent.Length > 0)
                        {
                            if (CompareArray(tmp, uploadedFileContent))
                            {
                                // VALID HEADER INFORMATION 
                                IsValid = true;
                            }
                            else
                            {
                                // INVALID HEADER INFORMATION
                                IsValid = false;
                            }
                        }
                        else
                        {
                            IsValid = false;
                        }
                    }
                    else
                    {
                        IsValid = false;
                    }
                }
            }
            else
            {
                IsValid = false;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return IsValid;
    }


    public static bool CompareArray(byte[] a1, byte[] a2)
    {

        bool res = false;
        int Count = 0;
        int j = 0;
        try
        {
            for (int i = 0; i < a1.Length; i++)
            {
                res = Convert.ToBoolean(a2.FirstOrDefault(x => x.ToString().Equals(a1[i].ToString())));
                if (res)
                {
                    Count++;
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        if (Count == 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    public static void WriteActivityLog(string EmailId, string ModuleName, string Activity, string ContentName, string IPAddress, bool IsMCP)
    {

        if (EmailId != null)
        {
            try
            {

                string ExceptionLogFolderPath = "";
                if (IsMCP)
                    ExceptionLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "\\ActivityLog";
                else
                    ExceptionLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "\\Admin\\ActivityLog";

                if (!Directory.Exists(ExceptionLogFolderPath))
                    Directory.CreateDirectory(ExceptionLogFolderPath);

                if (Directory.Exists(ExceptionLogFolderPath))
                {
                    //Create month wise exception log file.
                    string date = string.Format("{0:dd}", DateTime.Now);
                    string month = string.Format("{0:MMM}", DateTime.Now);
                    string year = string.Format("{0:yyyy}", DateTime.Now);

                    string folderName = month + year; //Feb2013
                    string monthFolder = ExceptionLogFolderPath + "\\" + folderName;
                    if (!Directory.Exists(monthFolder))
                        Directory.CreateDirectory(monthFolder);

                    string ActivityLogFolderName = monthFolder +
                           "\\ActivityLog_" + date + month + ".txt"; //ActivityLog_04Feb.txt

                    using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(ActivityLogFolderName, true))
                    {
                        strmWriter.WriteLine();
                        strmWriter.WriteLine();
                        string msg = "";
                        if (ContentName != "")
                        {
                            msg = "-----------------------" + DateTime.Now.ToString() + "---------------------------------------------------------" + Environment.NewLine;
                            msg = msg + EmailId + " has " + Activity + " " + ContentName + " in the " + ModuleName + " from  IP " + IPAddress + Environment.NewLine;
                            msg = msg + "-----------------------" + "End" + "--------------------------------------------------------------------------";
                        }
                        else
                        {
                            msg = "-----------------------" + DateTime.Now.ToString() + "--------------------------------------" + Environment.NewLine;
                            msg = msg + EmailId + " has " + Activity + " " + ContentName + ModuleName + " from the IP " + IPAddress + Environment.NewLine;
                            msg = msg + "-----------------------" + "End" + "-------------------------------------------------------------";
                        }
                        //msg = msg.Replace("\n", "");
                        strmWriter.WriteLine(msg.Trim());
                    }
                }
                //else
                //throw new DirectoryNotFoundException("Acitivity log folder not found in the specified path");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }
    }

    public static string ReplaceSingleQuote(string strMsg)
    {
        try
        {
            strMsg = strMsg.Replace('\'', '\'');
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return strMsg;
    }
    public static void AddDropdownItem(ref DropDownList ddl)
    {
        try
        {
            ddl.Items.Insert(0, new ListItem("Please select", "0"));
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }
    public static void BindAllLanguage(StoreBE objStoreBE, ref DropDownList ddlLanguage)
    {
        try
        {
            if (objStoreBE.StoreLanguages.Count > 0)
            {
                ddlLanguage.DataSource = objStoreBE.StoreLanguages;
                ddlLanguage.DataTextField = "LanguageName";
                ddlLanguage.DataValueField = "LanguageId";
                ddlLanguage.DataBind();
                ddlLanguage.SelectedValue = Convert.ToString(objStoreBE.StoreLanguages.Find(x => x.IsDefault == true).LanguageId);
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }


    /// <summary>
    /// Author  :   SHRIGANESH SINGH
    /// Date    :   07 September 2016
    /// Scope   :   To convert the given data in Excel to DataTable
    /// </summary>
    /// <param name="FileName"></param>
    /// <returns> return DataTable dtResult </returns>
    public static DataTable ConvertExcelToDataTable(string FileName)
    {
        DataTable dtResult = null;
        try
        {
            int totalSheet = 0; //No of sheets on excel file  
          string ConString = string.Empty;
          ConString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';";

            //string ext = System.IO.Path.GetExtension(FileName);
            //if (ext.ToLower() == ".xls")
            //{
            //    ConString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;';";
            //}
            //else
            //{
            //    if (ext.ToLower() == ".xlsx")
            //    {
                    //ConString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';";
            //    }
            //}
             using (OleDbConnection objConn = new OleDbConnection(ConString))
             {
                objConn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dt != null)
                {
                    var tempDataTable = (from dataRow in dt.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    dt = tempDataTable;
                    totalSheet = dt.Rows.Count;
                    sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = objConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                dtResult = ds.Tables["excelData"];
                objConn.Close();
            }
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
        return dtResult; //Returning DataTable 
    }


    /// <summary>
    /// Method to write application wide TAX log. 
    /// </summary>
    /// <param name="strXML">String object</param>
    public static void WriteGiftLog(String strXML)
    {
        string GCLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\GCLogs";
        try
        {
            if (!Directory.Exists(GCLogFolderPath))
                Directory.CreateDirectory(GCLogFolderPath);

            if (Directory.Exists(GCLogFolderPath))
            {
                //Create month wise exception log file.
                string date = string.Format("{0:dd}", DateTime.Now);
                string month = string.Format("{0:MMM}", DateTime.Now);
                string year = string.Format("{0:yyyy}", DateTime.Now);

                string folderName = month + year; //Feb2013
                string monthFolder = GCLogFolderPath + "\\" + folderName;
                if (!Directory.Exists(monthFolder))
                    Directory.CreateDirectory(monthFolder);

                string GCLogFileName = monthFolder +
                    "\\GCLogs" + date + month + ".txt"; //TAXLogs_04Feb.txt

                using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(GCLogFileName, true))
                {
                    strmWriter.WriteLine("On " + DateTime.Now.ToString());
                    strmWriter.WriteLine("XML: " + strXML);
                    strmWriter.WriteLine("-------------------------------------------------------------------------------");
                }
            }
            else
            {
                throw new DirectoryNotFoundException("GC log folder not found in the specified path");
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    public static string ReplaceEmailIdspecialCharactersbyDecoding(string strEmail)
    {
        string newstr;
        strEmail = strEmail.Replace("&amp;", "&");
        strEmail = strEmail.Replace("&#39;", "'");
        strEmail = strEmail.Replace("&#228;", "ä");
        strEmail = strEmail.Replace("&#252;", "ü");
        strEmail = strEmail.Replace("&#246;", "ö");
        strEmail = strEmail.Replace("&#223;", "ß");
        return strEmail;
    }

    public static bool IsValidEamilId(string strEmailId)
    {
        string stringEmailPattern = GlobalFunctions.GetSetting("EmailPattern");
        Regex regex = new Regex(stringEmailPattern);
        Match match = regex.Match(strEmailId);
        if (match.Success)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static string AllowedEmailPattern()
    {
        string stringEmailPattern = GlobalFunctions.GetSetting("EmailPattern");
        return stringEmailPattern;
    }
}

