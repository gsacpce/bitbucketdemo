﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.VisualBasic;
using PWGlobalEcomm.BusinessLogic;
using System.Globalization;
using System.Web;


public class ValidateWebServices_OASIS
{
    public ValidateWebServices_OASIS()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private static string GetStringMaxLength(string FieldValues, int MaxLength)
    {
        string strFieldValues;
        try
        {
            strFieldValues = FieldValues.Trim();
            if (strFieldValues != string.Empty & strFieldValues.Length >= MaxLength)
            {
                strFieldValues.Remove(MaxLength - 1);
            }
            return strFieldValues;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return string.Empty;
        }
    }

    public string ValidateOrderItems(ArrayList arrOrderItems)
    {
        try
        {
            bool bNumeric = true;
            string strOrderItems = "";
            //int intFieldCount = 0;
            for (int intItems = 0; intItems < arrOrderItems.Count; intItems++)
            {
                strOrderItems += arrOrderItems[intItems] + "|*|";
            }

            if (bNumeric)
            {
                return strOrderItems;
            }
            else
            {
                return "";
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return "";
        }
    }

    string GetUDFString(List<CustomerOrderBE.UDFValuesBE> lstUDF)
    {
        string strUDFFields = string.Empty;
        try
        {
            //List<UDFBE> lstUDFBE = new List<UDFBE>();
            //if (lstUDF != null && lstUDF.Count > 0)
            //{
            //    lstUDFBE = lstUDF.Cast<UDFBE>().ToList();
            //}
            for (int intUdf = 0; intUdf < lstUDF.Count; intUdf++)
            {
                if (strUDFFields != "")
                    strUDFFields += "|*|";
                if (Convert.ToString(lstUDF[intUdf].UDFValue) != "")
                    strUDFFields += Convert.ToString(lstUDF[intUdf].SequenceNo) + "|*|";
                strUDFFields += Convert.ToString(lstUDF[intUdf].UDFValue);
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return strUDFFields;
    }

    private static bool GetPositiveInteger(string FieldValues)
    {

        try
        {
            int intFieldValue = Convert.ToInt32(FieldValues);
            if (intFieldValue > 0)
            {
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;

        }
    }

    public bool ValidateOrder(string DivisionId, string KeyGroupId, string CustomerId, string CustomerContactId, string PaymentTypeId, string AuthorisationCode,
        string GoodsTotal, string Fright, CustomerOrderBE lstCustomerOrderBE, bool lbIsAdflexOrder)
    {
        try
        {


            if (DivisionId == string.Empty && GetPositiveInteger(DivisionId))
            {
                return false;
            }

            if (KeyGroupId == string.Empty && GetPositiveInteger(KeyGroupId))
            {
                return false;
            }

            if (CustomerId == string.Empty && GetPositiveInteger(CustomerId))
            {
                return false;
            }

            if (CustomerContactId == string.Empty && GetPositiveInteger(CustomerContactId))
            {
                return false;
            }

            if (PaymentTypeId == string.Empty && GetPositiveInteger(PaymentTypeId))
            {
                return false;
            }

            if (!lbIsAdflexOrder)
            {
                if (AuthorisationCode == string.Empty)
                {
                    return false;
                }
            }

            if (GoodsTotal == string.Empty && Information.IsNumeric(GoodsTotal))
            {
                return false;
            }

            if (Fright == string.Empty && Information.IsNumeric(Fright))
            {
                return false;
            }

            if (Convert.ToString(lstCustomerOrderBE.TotalTax) == string.Empty && Information.IsNumeric(lstCustomerOrderBE.TotalTax))
            {
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }

    public bool ValidateOrderDetails(string GoodsTotal, ArrayList ProductValues,
        ref int Oasis_orderid, string TaxNumber, string VendorTaxCode, CustomerOrderBE lstCustomerOrderBE, UserBE lstUser, string strTxRefGUID, string strCardNumber,
        bool IsCreditCardOrder, string AuthorisationCode, string strGiftCertificateValue, string CardToken, string MerchantId, string CustRef)
    {
        try
        {
            string strAuthorisationCode = GetStringMaxLength(AuthorisationCode, 9);
            string strBillContactName = GetStringMaxLength(lstCustomerOrderBE.InvoiceContactPerson, 50);
            string strBillAddress = GetStringMaxLength(lstCustomerOrderBE.InvoiceAddress1, 30);
            string strBillCity = GetStringMaxLength(lstCustomerOrderBE.InvoiceCity, 30);
            string strBillPostalCode = GetStringMaxLength(lstCustomerOrderBE.InvoicePostalCode, 12);
            string strBillCounty = GetStringMaxLength(lstCustomerOrderBE.InvoiceCounty, 30);
            string strOrderItems = ValidateOrderItems(ProductValues);
            bool bValidateShip = true;
            int intOrderID = 0;

            int intPaymentTypeId = 0;
            if (!String.IsNullOrEmpty(lstCustomerOrderBE.PaymentMethod))
                intPaymentTypeId = Convert.ToInt16(lstCustomerOrderBE.PaymentMethod);

            string strAuthDate = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
            string Freight = lstCustomerOrderBE.StandardCharges == 0 ? lstCustomerOrderBE.ExpressCharges.ToString("#####0.#0", CultureInfo.InvariantCulture) : lstCustomerOrderBE.StandardCharges.ToString("#####0.#0", CultureInfo.InvariantCulture);


            string strUDFValues = GetUDFString(lstCustomerOrderBE.UDFValues);

            if (lstCustomerOrderBE.InvoiceContactPerson == string.Empty)
            {
                bValidateShip = false;
            }
            if (lstCustomerOrderBE.InvoiceAddress1 == string.Empty)
            {
                bValidateShip = false;
            }
            if (lstCustomerOrderBE.InvoiceCity == string.Empty)
            {
                bValidateShip = false;
            }
            if (lstCustomerOrderBE.InvoicePostalCode == string.Empty)
            {
                bValidateShip = false;
            }
            if (lstCustomerOrderBE.InvoiceCounty == string.Empty)
            {
                bValidateShip = false;
            }

            string strCardToken = string.Empty;
            string strMerchantId = string.Empty;
            string strDivisionId = string.Empty;
            string strGroupId = string.Empty;
            string strSourceCode = string.Empty;
            string strSourceCodeId = string.Empty;
            string CustomerRef = string.Empty;
            string OrderNotes = string.Empty;
            string strOrderCurrency = string.Empty;

            if (intPaymentTypeId == 58)
            {
                TeleCashTransactionsBE objTeleCashtrans = new TeleCashTransactionsBE();
                objTeleCashtrans = ShoppingCartBL.GetTeleCashTransactionDetails(Convert.ToInt16(lstCustomerOrderBE.CustomerOrderId));

                if (objTeleCashtrans != null)
                {
                    CustomerRef = objTeleCashtrans.oid;
                }
            }
            else
            {
                CustomerRef = CustRef;
            }


            if (string.IsNullOrEmpty(lstCustomerOrderBE.CouponCode))
            {
                OrderNotes = ".";
            }
            else
            {
                OrderNotes = "Coupon code " + lstCustomerOrderBE.CouponCode + " applied. " + lstCustomerOrderBE.DiscountPercentage + "% discount on " + (lstCustomerOrderBE.BehaviourType.ToLower() == "freeshipping" ? "Shipping" : lstCustomerOrderBE.BehaviourType) + "";
            }

            StoreBE objStoreBE = StoreBL.GetStoreDetails();

            if (objStoreBE != null)
            {
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                {
                    if (TaxNumber.Contains("User Input"))
                    {
                        string strtaxnumber = TaxNumber;
                        OrderNotes = "Tax Number : " + strtaxnumber.Replace("User Input", "");
                    }
                }
            }

            strCardToken = CardToken;
            strMerchantId = MerchantId;

            // Below LOC added by SHRIGANESH to add Order Instructions to the Order XML 30 August 2016
            if (!string.IsNullOrEmpty(lstCustomerOrderBE.SpecialInstructions))
            {
                OrderNotes = " Instructions : " + lstCustomerOrderBE.SpecialInstructions;
            }

            if (objStoreBE != null)
            {
                strDivisionId = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId); //!string.IsNullOrEmpty(strGiftCertificateValue) ? "248" :Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).DivisionId);
                strGroupId = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).GroupId);//!string.IsNullOrEmpty(strGiftCertificateValue) ? "236" : Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).GroupId);
                strSourceCode = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeName);//!string.IsNullOrEmpty(strGiftCertificateValue) ? "GGLMRKEURWEB" : Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeName);//"GGLMRKEURWEB"
                strSourceCodeId = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeId);// !string.IsNullOrEmpty(strGiftCertificateValue) ? "7778" : Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).SourceCodeId);
                strOrderCurrency = Convert.ToString(objStoreBE.StoreCurrencies.FirstOrDefault(x => x.CurrencyId == GlobalFunctions.GetCurrencyId()).CurrencyCode);// Added By SHRIGANESH SINGH 21 Sept 2016
            }
            lstUser.BASYS_CustomerContactId = lstUser.BASYS_CustomerContactId;// !string.IsNullOrEmpty(strGiftCertificateValue) ? "1006981" : lstUser.BASYS_CustomerContactId;
            if (ValidateOrder(strDivisionId, strGroupId, lstUser.BASYS_CustomerContactId, lstUser.BASYS_CustomerContactId, lstCustomerOrderBE.PaymentMethod, AuthorisationCode, GoodsTotal, Freight, lstCustomerOrderBE, IsCreditCardOrder) && (bValidateShip) && (strOrderItems.Length > 0))
            {
                if (TaxNumber.Contains("User Input"))
                {
                    TaxNumber = string.Empty;
                }

                StringBuilder strbldrOrderHeaderValues = new StringBuilder();
                StringBuilder strbldrBillToValues = new StringBuilder();
                StringBuilder strbldrOrderItemValues = new StringBuilder();
                Webservice_OASIS objCreateXML = new Webservice_OASIS();
                if (IsCreditCardOrder)
                {
                    #region FOR CREDIT CARD
                    if (string.IsNullOrEmpty(TaxNumber))
                    {
                        //As per Harley Email on 25-Jan-2016 --Subject : Brand Estore Test Order -- removed CustomerID tag, OrderCurrency
                        if (strMerchantId == "" && strCardToken == "")
                        {
                            strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                            + lstCustomerOrderBE.PaymentMethod + "|*|" + strTxRefGUID + "|*|" + strCardNumber + "|*|" + strAuthDate + "|*|"
                            + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency + "|*|" + GoodsTotal + "|*|" + Freight + "|*|"
                            + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture) + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|"
                            + VendorTaxCode);
                        }
                        else
                        {
                            strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                         + lstCustomerOrderBE.PaymentMethod + "|*|" + strTxRefGUID + "|*|" + strCardNumber + "|*|" + strAuthDate + "|*|" + strCardToken + "|*|" + strMerchantId + "|*|"
                         + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency + "|*|" + GoodsTotal + "|*|" + Freight + "|*|"
                         + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture) + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|"
                         + VendorTaxCode);
                        }
                    }
                    else
                    {
                        //As per Harley Email on 25-Jan-2016 --Subject : Brand Estore Test Order -- removed CustomerID tag, OrderCurrency
                        if (strMerchantId == "" && strCardToken == "")
                        {
                            strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                            + lstCustomerOrderBE.PaymentMethod + "|*|" + strTxRefGUID + "|*|" + strCardNumber + "|*|" + strAuthDate + "|*|"
                            + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency + "|*|" + GoodsTotal + "|*|" + Freight + "|*|"
                            + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture) + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|"
                            + TaxNumber + "|*|" + VendorTaxCode);
                        }
                        else
                        {
                            strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                         + lstCustomerOrderBE.PaymentMethod + "|*|" + strTxRefGUID + "|*|" + strCardNumber + "|*|" + strAuthDate + "|*|" + strCardToken + "|*|" + strMerchantId + "|*|"
                         + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency + "|*|" + GoodsTotal + "|*|" + Freight + "|*|"
                         + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture) + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|"
                         + TaxNumber + "|*|" + VendorTaxCode);
                        }
                    }

                    strbldrBillToValues.Append(lstCustomerOrderBE.InvoiceContactPerson + "|*|" + lstCustomerOrderBE.InvoiceAddress1 + "|*|"
                    + lstCustomerOrderBE.InvoiceAddress2 + "|*||*|" + lstCustomerOrderBE.InvoiceCity + "|*|" + lstCustomerOrderBE.InvoicePostalCode + "|*|"
                    + lstCustomerOrderBE.InvoiceCounty + "|*|" + lstCustomerOrderBE.InvoiceCountryName + "|*|" + lstCustomerOrderBE.InvoiceCountryCode + "|*|"
                    + lstCustomerOrderBE.InvoicePhone);
                    
                    if (string.IsNullOrEmpty(TaxNumber))
                    {
                        //As per Harley Email on 25-Jan-2016 --Subject : Brand Estore Test Order -- removed CustomerID tag, OrderCurrency
                        if (strMerchantId == "" && strCardToken == "")
                        {
                            intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|Last4Digits|*|AuthorisationDate|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|VendorTaxCode",
                                strbldrOrderHeaderValues.ToString(),
                                "GiftCertificateReference|*|Amount",
                                strGiftCertificateValue,
                                "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                                "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone",
                                Convert.ToString(strbldrBillToValues), "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                                strOrderItems,
                                "SequenceNo|*|Value",
                                strUDFValues,
                                lstCustomerOrderBE,
                                lstUser,
                                IsCreditCardOrder);
                        }
                        else
                        {
                            intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|Last4Digits|*|AuthorisationDate|*|CardToken|*|MerchantID|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|VendorTaxCode",
                                strbldrOrderHeaderValues.ToString(),
                                "GiftCertificateReference|*|Amount",
                                strGiftCertificateValue,
                                "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                                "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone",
                                Convert.ToString(strbldrBillToValues), "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                                strOrderItems,
                                "SequenceNo|*|Value",
                                strUDFValues,
                                lstCustomerOrderBE,
                                lstUser,
                                IsCreditCardOrder);
                        }
                    }
                    else
                    {
                        //As per Harley Email on 25-Jan-2016 --Subject : Brand Estore Test Order -- removed CustomerID tag, OrderCurrency
                        if (strMerchantId == "" && strCardToken == "")
                        {
                            intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|Last4Digits|*|AuthorisationDate|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|TaxNumber|*|VendorTaxCode",
                                strbldrOrderHeaderValues.ToString(),
                                "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                                "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                                "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|InvoiceAccountID",
                                Convert.ToString(strbldrBillToValues),
                                "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                                strOrderItems,
                                "SequenceNo|*|Value",
                                strUDFValues,
                                lstCustomerOrderBE,
                                lstUser,
                                IsCreditCardOrder);
                        }
                        else
                        {
                            intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|Last4Digits|*|AuthorisationDate|*|CardToken|*|MerchantID|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|TaxNumber|*|VendorTaxCode",
                                strbldrOrderHeaderValues.ToString(),
                                "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                                "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                                "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|InvoiceAccountID",
                                Convert.ToString(strbldrBillToValues),
                                "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                                strOrderItems,
                                "SequenceNo|*|Value",
                                strUDFValues,
                                lstCustomerOrderBE,
                                lstUser,
                                IsCreditCardOrder);
                        }
                    } 
                    #endregion
                }
                else if (objStoreBE.StoreId == 471)
                {
                    strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                                             + lstCustomerOrderBE.PaymentMethod + "|*|" + AuthorisationCode + "|*|" + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency
                                             + "|*|" + GoodsTotal + "|*|" + Freight + "|*|" + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture)
                                             + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|" + VendorTaxCode);

                    strbldrBillToValues.Append(lstCustomerOrderBE.InvoiceContactPerson + "|*|" + lstCustomerOrderBE.InvoiceAddress1 + "|*|"
                   + lstCustomerOrderBE.InvoiceAddress2 + "|*||*|" + lstCustomerOrderBE.InvoiceCity + "|*|" + lstCustomerOrderBE.InvoicePostalCode
                   + "|*|" + lstCustomerOrderBE.InvoiceCounty + "|*|" + lstCustomerOrderBE.InvoiceCountryName + "|*|" + lstCustomerOrderBE.InvoiceCountryCode
                   + "|*|" + lstCustomerOrderBE.InvoicePhone);

                    intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|VendorTaxCode", strbldrOrderHeaderValues.ToString(),
                               "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                               "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                               "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|InvoiceAccountID",
                               Convert.ToString(strbldrBillToValues),
                               "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                               strOrderItems,
                               "SequenceNo|*|Value",
                               strUDFValues,
                               lstCustomerOrderBE,
                               lstUser,
                               IsCreditCardOrder);
                }
                else if (objStoreBE.StoreId == 532)
                {
                    strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                                             + lstCustomerOrderBE.PaymentMethod + "|*|" + AuthorisationCode + "|*|" + strSourceCode + "|*|" 
                                             + strSourceCodeId + "|*|" + strOrderCurrency + "|*|" + GoodsTotal + "|*|" + Freight + "|*|" 
                                             + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture)
                                             + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|" + VendorTaxCode);

                    strbldrBillToValues.Append(lstCustomerOrderBE.InvoiceContactPerson + "|*|" + lstCustomerOrderBE.InvoiceAddress1 + "|*|"
                   + lstCustomerOrderBE.InvoiceAddress2 + "|*||*|" + lstCustomerOrderBE.InvoiceCity + "|*|" + lstCustomerOrderBE.InvoicePostalCode
                   + "|*|" + lstCustomerOrderBE.InvoiceCounty + "|*|" + lstCustomerOrderBE.InvoiceCountryName + "|*|" + lstCustomerOrderBE.InvoiceCountryCode
                   + "|*|" + lstCustomerOrderBE.InvoicePhone);

                    intOrderID = objCreateXML.OrderService_OASIS(
                        "DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|VendorTaxCode",
                        strbldrOrderHeaderValues.ToString(),
                         "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                         "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                         "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|InvoiceAccountID",
                          Convert.ToString(strbldrBillToValues),
                          "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                               strOrderItems,
                               "SequenceNo|*|Value",
                               strUDFValues,
                               lstCustomerOrderBE,
                               lstUser,
                               IsCreditCardOrder);
                }
                else
                {                                      
                    #region "ADDED by Sripal 2016-04-22"
                    AuthorisationCode = "";                 

                    if (string.IsNullOrEmpty(TaxNumber))
                    {
                        //As per Harley Email on 25-Jan-2016 --Subject : Brand Estore Test Order -- removed CustomerID tag, OrderCurrency
                        if (strMerchantId == "" && strCardToken == "")
                        {
                            strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                            + lstCustomerOrderBE.PaymentMethod + "|*|" + AuthorisationCode + "|*|" + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency
                            + "|*|" + GoodsTotal + "|*|" + Freight + "|*|" + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture)
                            + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|" + VendorTaxCode);
                        }
                        else
                        {
                            strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                        + lstCustomerOrderBE.PaymentMethod + "|*|" + AuthorisationCode + "|*|" + strCardToken + "|*|" + strMerchantId + "|*|" + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency
                        + "|*|" + GoodsTotal + "|*|" + Freight + "|*|" + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture)
                        + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|" + VendorTaxCode);
                        }
                    }
                    else
                    {
                        //As per Harley Email on 25-Jan-2016 --Subject : Brand Estore Test Order -- removed CustomerID tag, OrderCurrency
                        if (strMerchantId == "" && strCardToken == "")
                        {
                            strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                            + lstCustomerOrderBE.PaymentMethod + "|*|" + AuthorisationCode + "|*|" + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency
                            + "|*|" + GoodsTotal + "|*|" + Freight + "|*|" + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture)
                            + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|" + TaxNumber + "|*|" + VendorTaxCode);
                        }
                        else
                        {
                            strbldrOrderHeaderValues.Append(strDivisionId + "|*|" + strGroupId + "|*|" + lstUser.BASYS_CustomerContactId + "|*|"
                         + lstCustomerOrderBE.PaymentMethod + "|*|" + AuthorisationCode + "|*|" + strCardToken + "|*|" + strMerchantId + "|*|" + strSourceCode + "|*|" + strSourceCodeId + "|*|" + strOrderCurrency
                         + "|*|" + GoodsTotal + "|*|" + Freight + "|*|" + lstCustomerOrderBE.TotalTax.ToString("#####0.#0", CultureInfo.InvariantCulture)
                         + "|*|" + CustomerRef + "|*|" + OrderNotes + "|*|" + TaxNumber + "|*|" + VendorTaxCode);
                        }
                    }

                    strbldrBillToValues.Append(lstCustomerOrderBE.InvoiceContactPerson + "|*|" + lstCustomerOrderBE.InvoiceAddress1 + "|*|"
                    + lstCustomerOrderBE.InvoiceAddress2 + "|*||*|" + lstCustomerOrderBE.InvoiceCity + "|*|" + lstCustomerOrderBE.InvoicePostalCode
                    + "|*|" + lstCustomerOrderBE.InvoiceCounty + "|*|" + lstCustomerOrderBE.InvoiceCountryName + "|*|" + lstCustomerOrderBE.InvoiceCountryCode
                    + "|*|" + lstCustomerOrderBE.InvoicePhone);
                    #endregion                                  
                                        
                    #region "ADDED by Sripal 2016-04-22"
                    if (!string.IsNullOrEmpty(strGiftCertificateValue))
                    {
                        if (string.IsNullOrEmpty(TaxNumber))
                        {
                            if (strMerchantId == "" && strCardToken == "")
                            {
                                intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|VendorTaxCode",
                                strbldrOrderHeaderValues.ToString(),
                               "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                               "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                               "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone",
                               Convert.ToString(strbldrBillToValues),
                               "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                               strOrderItems,
                               "SequenceNo|*|Value",
                               strUDFValues,
                               lstCustomerOrderBE,
                               lstUser,
                               IsCreditCardOrder);
                            }
                            else
                            {
                                intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|CardToken|*|MerchantID|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|VendorTaxCode",
                              strbldrOrderHeaderValues.ToString(),
                             "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                             "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                             "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone",
                             Convert.ToString(strbldrBillToValues),
                             "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                             strOrderItems,
                             "SequenceNo|*|Value",
                             strUDFValues,
                             lstCustomerOrderBE,
                             lstUser,
                             IsCreditCardOrder);
                            }
                        }
                        else
                        {
                            if (strMerchantId == "" && strCardToken == "")
                            {
                                intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|TaxNumber|*|VendorTaxCode", strbldrOrderHeaderValues.ToString(),
                               "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                               "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                               "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|InvoiceAccountID",
                               Convert.ToString(strbldrBillToValues),
                               "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                               strOrderItems,
                               "SequenceNo|*|Value",
                               strUDFValues,
                               lstCustomerOrderBE,
                               lstUser,
                               IsCreditCardOrder);
                            }
                            else
                            {
                                intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|CardToken|*|MerchantID|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|TaxNumber|*|VendorTaxCode", strbldrOrderHeaderValues.ToString(),
                              "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                              "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                              "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|InvoiceAccountID",
                              Convert.ToString(strbldrBillToValues),
                              "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                              strOrderItems,
                              "SequenceNo|*|Value",
                              strUDFValues,
                              lstCustomerOrderBE,
                              lstUser,
                              IsCreditCardOrder);
                            }
                        }
                    }
                    else
                    {
                        /* In below code Authorizationcode is replaced by TransactionRef as per call with Harley on 16 Sept 16
                         to remove Authorizationcode tag and pass empty TransactionRef tag
                         */

                        if (string.IsNullOrEmpty(TaxNumber))
                        {
                            if (strMerchantId == "" && strCardToken == "")
                            {
                                intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|VendorTaxCode",
                                    strbldrOrderHeaderValues.ToString(),
                                   "GiftCertificateReference|*|Amount",
                                   strGiftCertificateValue,
                                   "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                                   "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone",
                                   Convert.ToString(strbldrBillToValues),
                                   "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                                   strOrderItems,
                                   "SequenceNo|*|Value",
                                   strUDFValues,
                                   lstCustomerOrderBE,
                                   lstUser,
                                   IsCreditCardOrder);
                            }
                            else
                            {
                                intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|CardToken|*|MerchantID|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|VendorTaxCode",
                                  strbldrOrderHeaderValues.ToString(),
                                 "GiftCertificateReference|*|Amount",
                                 strGiftCertificateValue,
                                 "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                                 "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone",
                                 Convert.ToString(strbldrBillToValues),
                                 "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef",
                                 strOrderItems,
                                 "SequenceNo|*|Value",
                                 strUDFValues,
                                 lstCustomerOrderBE,
                                 lstUser,
                                 IsCreditCardOrder);
                            }
                        }
                        else
                        {
                            if (strMerchantId == "" && strCardToken == "")
                            {
                                intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|TaxNumber|*|VendorTaxCode",
                                    strbldrOrderHeaderValues.ToString(),
                                    "GiftCertificateReference|*|Amount",
                                    strGiftCertificateValue,
                                    "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                                    "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|InvoiceAccountID", Convert.ToString(strbldrBillToValues), "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef", strOrderItems, "SequenceNo|*|Value", strUDFValues, lstCustomerOrderBE, lstUser, IsCreditCardOrder);
                            }
                            else
                            {
                                intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|TransactionRef|*|CardToken|*|MerchantID|*|SourceCode|*|SourceCodeID|*|OrderCurrency|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|TaxNumber|*|VendorTaxCode",
                                  strbldrOrderHeaderValues.ToString(),
                                  "GiftCertificateReference|*|Amount",
                                  strGiftCertificateValue,
                                  "CompanyName|*|ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text",
                                  "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|InvoiceAccountID", Convert.ToString(strbldrBillToValues), "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText|*|OrderLinePORef", strOrderItems, "SequenceNo|*|Value", strUDFValues, lstCustomerOrderBE, lstUser, IsCreditCardOrder);
                            }
                        }

                    }
                    #endregion

                    //As per Harley Email on 5-April-2016 --Subject : RE: Gift certificate check on New platform DEMO environment removed the AuthorisationCode
                    //intOrderID = objCreateXML.OrderService_OASIS("DivisionID|*|KeyGroupID|*|CustomerContactID|*|PaymentTypeID|*|SourceCode|*|SourceCodeID|*|GoodsTotal|*|Freight|*|Tax|*|CustomerRef|*|OrderNotes|*|TaxNumber|*|VendorTaxCode", strbldrOrderHeaderValues.ToString(),
                    //    "GiftCertificateReference|*|Amount", strGiftCertificateValue,
                    //    "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone|*|Carrier_Service_ID|*|Carrier_Service_Text", "ContactName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|Telephone", Convert.ToString(strbldrBillToValues), "CatalogueID|*|CatalogueAlias|*|UnitPrice|*|LineQty|*|ProductID|*|BaseColour|*|TrimColour|*|SupplierID|*|VatInvoiceText", strOrderItems, "SequenceNo|*|Value", strUDFValues, lstCustomerOrderBE, lstUser, IsCreditCardOrder);
                }
                Oasis_orderid = intOrderID;
            }
            else
            {
                return false;
            }
            if (intOrderID > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return false;
        }
    }

    public static int BuildCreditCardDetails(string Amount, string CustomerContactId, string MerchantNumber, string CardNumber, string ExpiryDate, string CV2,
        string CardHolderName, string Address1, string Address2, string Address3, string City, string PostCode, string County, string Country, string countryCode,
        string strPaymentType)
    {
        string strAmount = Amount;
        string strCustomerContactId = CustomerContactId;
        string strMerchantNumber = MerchantNumber;
        string strCardNumber = GetStringMaxLength(CardNumber, 16);
        //string strCardType = CardType;
        string strExpiryDate = ExpiryDate;
        string strCV2 = GetStringMaxLength(CV2, 3);
        string strCardHolderName = GetStringMaxLength(CardHolderName, 30);
        string strAddress1 = GetStringMaxLength(Address1, 30);
        string strAddress2 = GetStringMaxLength(Address2, 30);
        string strAddress3 = GetStringMaxLength(Address3, 30);
        string strCity = GetStringMaxLength(City, 30);
        string strPostCode = GetStringMaxLength(PostCode, 12);
        string strCounty = GetStringMaxLength(County, 30);
        string strCountryCode = countryCode;

        int intCrediCardID = 0;
        try
        {
            XmlNode ndCredit = null;
            StringBuilder strbldrCreditCardFields = new StringBuilder();
            //  strbldrCreditCardFields.Append("Amount|*|MerchantNo|*|CardNumber|*|CardType|*|StartDate|*|ExpiryDate|*|CV2|*|IssueNo|*|CardHolderName|*|Address1|*|Address2|*|Address3|*|City|*|PostCode|*|County|*|Country|*|countryCode|*|CustomerContactId");
            StringBuilder strbldrCreditCardValues = new StringBuilder();
            //strbldrCreditCardValues.Append(strAmount + "|*|" + strMerchantNumber + "|*|" + strCardNumber + "|*|" + strCardType + "|*||*|" + strExpiryDate + "|*|" + strCV2 + "|*||*|" + strCardHolderName + "|*|" + strAddress1 + "|*|" + strAddress2 + "|*|" + strAddress3 + "|*|" + strCity + "|*|" + strPostCode + "|*|" + strCounty + "|*|" + Country +  "|*|" + strCountryCode + "|*|" + strCustomerContactId );
            StringBuilder strbldrCreditCardDetailsFields = new StringBuilder();
            StringBuilder strbldrCreditCardDetailsValues = new StringBuilder();

            XmlNode ndCreditDetails = null;
            strbldrCreditCardFields.Append("CustomerContactID|*|Amount|*|PaymentTypeID");
            //Have to Add Payment Type ID in Values.
            strbldrCreditCardValues.Append(strCustomerContactId + "|*|" + strAmount + "|*|" + strPaymentType);

            strbldrCreditCardDetailsFields.Append("MerchantNo|*|CardNumber|*|ExpiryDate|*|CV2|*|CardHolderName|*|Address1|*|Address2|*|Address3|*|City|*|County|*|PostCode|*|Country|*|countryCode");
            strbldrCreditCardDetailsValues.Append(strMerchantNumber + "|*|" + strCardNumber + "|*|" + strExpiryDate + "|*|" + strCV2 + "|*|" + strCardHolderName + "|*|" + strAddress1 + "|*|" + strAddress2 + "|*|" + strAddress3 + "|*|" + strCity + "|*|" + strCounty + "|*|" + strPostCode + "|*|" + Country + "|*|" + strCountryCode);

            Webservice_OASIS objCreateXML = new Webservice_OASIS();

            ndCredit = objCreateXML.Service_OASIS(Convert.ToString(strbldrCreditCardFields), Convert.ToString(strbldrCreditCardValues), "Payment");
            ndCreditDetails = objCreateXML.Service_OASIS(Convert.ToString(strbldrCreditCardDetailsFields), Convert.ToString(strbldrCreditCardDetailsValues), "CreditCardDetails");
            ndCredit.InnerXml += ndCreditDetails.OuterXml;
            //call credit webservice

            if (HttpContext.Current.Session["sbGift"] == null)
            {
                #region call credit webservice
                if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
                {
                    PWGlobalEcomm.PaymentLive.Payment objPayment = new PWGlobalEcomm.PaymentLive.Payment();
                    System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    objPayment.Credentials = objNetworkCredentials;
                    intCrediCardID = objPayment.AuthorisePayment(ndCredit);
                }
                else
                {
                    #region Payment Authorisation disabled by client for test webservice
                    //PWGlobalEcomm.Payment.Payment objPayment = new PWGlobalEcomm.Payment.Payment();
                    //System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                    //objPayment.Credentials = objNetworkCredentials;
                    //intCrediCardID = objPayment.AuthorisePayment(ndCredit);
#endregion
                }
                #endregion
            }
            else
            {
                //This section should get excuted only in case of GC applied and to avoid to call AuthorisePayment function as on 14th June 2016
                intCrediCardID = 1;

            }
            return intCrediCardID;
        }
        catch (Exception ex)
        {

            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Faliure In BuildCreditCardDetails", ex + "USER ID- " + lstUserBE.UserId + "USER EMAIL- " + lstUserBE.EmailId, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            return intCrediCardID;
        }
    }

}