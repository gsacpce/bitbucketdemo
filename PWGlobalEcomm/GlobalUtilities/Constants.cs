﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

public class Constants
{
    #region Constructor

    public Constants()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #endregion

    #region ConnectionStrings

    /// <summary>
    ///  gets MCPConnectionString name from web.config
    /// </summary>
    public static string MCPConnectionStringName
    {
        get
        {
            return ConfigurationManager.AppSettings["MCPConnectionStringName"];
        }
    }

    /// <summary>
    ///  gets StoreConnectionString name from web.config
    /// </summary>
    public static string StoreConnectionStringName
    {
        get
        {
            return ConfigurationManager.AppSettings["StoreConnectionStringName"];
        }
    }

    /// <summary>
    /// gets connectionstring value of CorporateStore from web.config using above CorporateStoreConnectionStringName property.
    /// </summary>
    public static string strMCPConnectionString
    {
        get
        {
            return Convert.ToString(ConfigurationManager.AppSettings[MCPConnectionStringName]);
        }
    }

    /// <summary>
    /// gets connectionstring value of template(store) from web.config using above TemplateConnectionStringName property.
    /// </summary>
    public static string strStoreConnectionString
    {
        get
        {
            return Convert.ToString(ConfigurationManager.AppSettings[StoreConnectionStringName]);
        }
    }

    #endregion

    #region EntityNames

    public static string Entity_MenuBE = "MenuBE";
    public static string Entity_StoreBE = "StoreBE";
    public static string Entity_LanguageBE = "LanguageBE";
    public static string Entity_CurrencyBE = "CurrencyBE";
    public static string Entity_UserBE = "UserBE";
    public static string Entity_UserBudgetBE = "UserBE+UserBudgetBE";
    public static string Entity_ContactBE = "StoreBE+ContactBE";
    #region Indeed code
    public static string Entity_UserFileBE = "UserBE+BA_USERFILE";
    #endregion
    public static string Entity_UserDeliveryAddress = "UserBE+UserDeliveryAddressBE";
    public static string Entity_UserPreDefinedColumns = "UserBE+UserPreDefinedColumnsBE";
    public static string Entity_UserHierarchyBE = "UserBE+UserHierarchyBE";
    public static string Entity_RegistrationCustomFieldBE = "RegistrationCustomFieldBE";
    public static string Entity_RegistrationFeildTypeMasterBE = "RegistrationCustomFieldBE+RegistrationFeildTypeMasterBE";
    public static string Entity_RegistrationFeildDataMasterBE = "RegistrationCustomFieldBE+RegistrationFeildDataMasterBE";
    public static string Entity_CustomUDFRegDataDanske = "CustomUDFRegDataDanske";
    public static string Entity_StoreLanguageBE = "StoreBE+StoreLanguageBE";
    public static string Entity_StoreCurrencyBE = "StoreBE+StoreCurrencyBE";
    public static string Entity_StoreReferral = "StoreReferral";
    public static string Entity_StorereferralurlBE = "StoreReferral+referralurlBE";
    public static string Entity_CategoryBE = "CategoryBE";
    public static string Entity_SectionBE = "SectionBE";
    public static string Entity_FromEmailBE = "EmailManagementBE+FromEmailBE"; //Added By Hardik

    public static string Entity_TitleBE = "UserBE+Titles";
    public static string Entity_EmailManagementBE = "EmailManagementBE";
    public static string Entity_StaticTextKeyBE = "StaticTextKeyBE";

    public static string Entity_ProductBE = "ProductBE";
    public static string Entity_CustomerGroupBE = "UserBE+CustomerGroupBE";
    public static string Entity_ProductCategoryFilterBE = "ProductBE+ProductCategoryFilterBE";
    public static string Entity_ProductPriceFilterBE = "ProductBE+ProductPriceFilterBE";
    public static string Entity_ProductColorFilterBE = "ProductBE+ProductColorFilterBE";
    public static string Entity_ProductSectionFilterBE = "ProductBE+ProductSectionFilterBE";
    public static string Entity_ProductCustomFilterBE = "ProductBE+ProductCustomFilterBE";
    public static string Entity_PriceBreakBE = "ProductBE+PriceBreaks";
    public static string Entity_PriceBreakDetailsBE = "ProductBE+PriceBreakDetails";
    public static string Entity_ProductVariantType = "ProductBE+ProductVariantType";
    public static string Entity_ProductVariant = "ProductBE+ProductVariants";
    public static string Entity_ProductDetailGroups = "ProductBE+ProductDetailGroups";
    public static string Entity_ProductDetailGroupMappings = "ProductBE+ProductDetailGroupMappings";
    public static string Entity_ProductDetails = "ProductBE+ProductDetails";
    public static string Entity_RelatedProducts = "ProductBE+RelatedProducts";
    public static string Entity_ProductSKU = "ProductBE+ProductSKU";
    public static string Entity_RecentlyViewedProducts = "ProductBE+RecentlyViewedProducts";
    public static string Entity_ProductImage = "ProductBE+ProductImage";
    public static string Entity_ProductWishList = "ProductBE+ProductWishList";
    public static string Entity_ProductSearchBE = "ProductSearchBE";
    public static string Entity_ProductSize = "ProductBE+ProductSize";
    public static string Entity_ProductElements = "ProductBE+ProductElements";
    public static string Entity_ProductUOM = "ProductBE+ProductUOM";
    public static string ProductAttributes = "ProductBE+ProductAttributes";
    public static string ProductImportHistory = "ProductBE+ProductImportHistory";
    public static string Entity_ProductColorVariantMappping = "ProductBE+ProductColorVariantMappping";

    public static string Entity_YouMayAlsoLike = "ProductBE+YouMayAlsoLike";
    public static string Entity_ReviewRating = "ProductBE+ReviewRating";

    public static string Entity_FeatureBE = "FeatureBE";
    public static string Entity_FeatureValueBE = "FeatureBE+FeatureValueBE";

    public static string Entity_ImageSlideShowBE = "ImageSlideShowBE";
    public static string Entity_SlideImage = "ImageSlideShowBE+SlideShowImage";
    public static string Entity_SlideShowMaster = "ImageSlideShowBE+SlideShowMaster";

    public static string Entity_StaticPageManagementBE = "StaticPageManagementBE";

    public static string Entity_TemplateBE = "TemplateBE";

    public static string Entity_HeaderFooterManagement = "HeaderFooterManagementBE";

    public static string Entity_ThemeConfiguratorBE = "ThemeConfiguratorBE";

    public static string Entity_BrandColorManagementBE = "BrandColorManagementBE";
    public static string Entity_BreadCrumbManagementBE = "BreadCrumbManagementBE";

    public static string Entity_InvoiceBE = "InvoiceBE";
    public static string Entity_CountryBE = "CountryBE";
    #region indeed code
    public static string Entity_CountryCurrencyMapping = "CountryBE+CountryCurrencyMapping";
    #endregion
    public static string Entity_ShoppingCartBE = "ShoppingCartBE";
    public static string Entity_CustomerOrderBE = "CustomerOrderBE";
    public static string Entity_CustomerOrderProductsBE = "CustomerOrderBE+CustomerOrderProductsBE";
    public static string Entity_UDFValuesBE = "CustomerOrderBE+UDFValuesBE";
    public static string Entity_PaymentTypesBE = "PaymentTypesBE";
    public static string Entity_AdFlexTransactionsBE = "AdFlexTransactionsBE";
    public static string Entity_TeleCashTransactionsBE = "TeleCashTransactionsBE";
    public static string Entity_TeleCashCurrencyBE = "TeleCashCurrencyBE";
    public static string Entity_UDFBE = "UDFBE";
    public static string Entity_PickListItemsBE = "PickListItemsBE";

    public static string Entity_SocialMediaManagementBE = "SocialMediaManagementBE";
    public static string Entity_OrderTrackingManagmentBE = "OrderTrackingManagmentBE";
    public static string Entity_ProductAttributesBE = "ProductAttributes";

    public static string Entity_FreightBE = "FreightBE";
    public static string Entity_FreightManageMentBE = "FreightManagementBE";
    public static string Entity_FreightMode = "FreightMangementBE+FreightMode";
    public static string Entity_FreightModeLanguage = "FreightMangementBE+FreightMode+FreightModeLanguage";
    public static string Entity_FreightMethod = "FreightMangementBE+FreightMethod";
    public static string Entity_FreightRegion = "FreightMangementBE+FreightRegion";
    public static string Entity_CatalogueImage = "AddCatalogueImageBE";
    public static string Entity_CouponBE = "CouponBE";
    public static string Entity_RoleManagementBE = "RoleManagementBE";

    public static string Entity_ErrorManagementBE = "ErrorManagementBE";

    public static string Entity_LandingPageConfigurationBE = "LandingPageConfigurationBE";

    public static string Entity_FreightManagementBE = "FreightManagementBE";
    public static string Entity_FreightManagementBEFreightMultiplerCurrency = "FreightManagementBE+FreightMultiplerCurrency";
    public static string Entity_Freight_CARRIER_SERVICE = "Freight_Carrier_Service";

    public static string Entity_NotesBE = "NoteBE";

    public static string Entity_SectionHandlingFees = "SectionBE+HandlingFeesSection";

    public static string Entity_ShoppingCartAdditionalBE = "ShoppingCartAdditionalBE";

    public static string Entity_UserPreferredCurrencyBE = "UserPreferredBE+UserTypeCurrency";
    public static string Entity_UserPreferredBE = "UserPreferredBE";

    public static string Entity_CountryWiseCurrencyMapping = "CountryBE+CountryWiseCurrencyMapping";
    public static string Entity_LanguageCodeMaster = "LanguageCodeMaster";
    public static string Entity_SSOUserTypeCatalogueDetailsBE = "SSOUserTypeCatalogueDetailsBE";   

    #region "Punchout"
    public static string PunchoutDetailsBE = "PunchoutDetailsBE";
    public static string PunchoutMarkerBE = "PunchoutMarkerBE";
    #endregion

    #region "G+ SSO"
    public static string Entity_tbl_AddJson = "tbl_AddJson";
    #endregion

    #region "Registration"
    public static string Entity_UserRegistrationBEUserHierarchyBE = "UserRegistrationBE+UserHierarchyBE";
    public static string Entity_UserRegistrationBEUserRegistrationHierarchyCustomFieldMappingsBE = "UserRegistrationBE+UserRegistrationHierarchyCustomFieldMappingsBE";
    public static string Entity_UserRegistrationBERegistrationFieldsConfigurationBE = "UserRegistrationBE+RegistrationFieldsConfigurationBE";
    public static string Entity_UserRegistrationBERegistrationFeildTypeMasterBE = "UserRegistrationBE+RegistrationFeildTypeMasterBE";
    public static string Entity_UserRegistrationBERegistrationFieldDataMasterBE = "UserRegistrationBE+RegistrationFieldDataMasterBE";
    public static string Entity_UserRegistrationBERegistrationLanguagesBE = "UserRegistrationBE+RegistrationLanguagesBE";
    public static string Entity_UserRegistrationBERegistrationFilterBE = "UserRegistrationBE+RegistrationFilterBE";
    public static string Entity_UserRegistrationBECountryBE = "UserRegistrationBE+CountryBE";
    #endregion

    #region "Gift Certificate"
    public static string Entity_GiftCertificateOrderBE = "GiftCertificateOrderBE";
    public static string Entity_GiftCertificateOrderDetailsBE = "GiftCertificateOrderBE+GiftOrderDetailsBE";

    public static string Entity_CustomerOrderGiftCertificateBE = "CustomerOrderBE+CustomerOrderGiftCertificateBE";
    #endregion
    #endregion

    #region Add Exception to Database Procedure

    public static string usp_AddException = "usp_AddException";

    #endregion

    #region MenuDA Procedures

    public static string USP_GetMenuDetails = "USP_GetMenuDetails";
    public static string USP_GetMenuDetails_New = "USP_GetMenuDetails_New";

    #endregion

    #region RoleManagement Procedure
    public static string usp_ManageRole = "usp_ManageRole";
    public static string usp_ManageMenu = "usp_ManageMenu";
    #endregion

    #region StoreDA Procedures

    public static string USP_GetdisplayUDFSDetails = "USP_GetdisplayUDFSDetails"; //Created by snehal on 12 sep 2016
    public static string USP_GetDisplayUDFS = "USP_GetDisplayUDFS"; //Created by snehal on 12 sep 2016
    public static string USP_GetAllInvoiceCountries = "USP_GetAllInvoiceCountries"; //Created by snehal on 12 sep 2016
    public static string USP_ValidateCustomFieldLoginDetails = "USP_ValidateCustomFieldLoginDetails"; //Created by snehal on 9 sep 2016
    public static string USP_CheckForExistingCustomLoginId = "USP_CheckForExistingCustomLoginId";     //Created by snehal on 7 sep 2016
    public static string USP_GetAllCatalogue = "USP_GetAllCatalogue";
    public static string USP_GetAllStoreDetails = "USP_GetAllStoreDetails";
    public static string USP_IsStoreExists = "USP_IsStoreExists";
    public static string USP_InsertStore = "USP_InsertStore";
    public static string USP_InsertStoreLanguage = "USP_InsertStoreLanguage";
    public static string USP_InsertStoreCurrency = "USP_InsertStoreCurrency";
    public static string USP_CreateDatabase = "USP_CreateDatabase";
    public static string USP_CreateCloneDatabase = "USP_CreateCloneDatabase";
    public static string USP_IsDatabaseExists = "USP_IsDatabaseExists";
    public static string USP_DropDatabase = "USP_DropDatabase";
    public static string USP_UpdateStoreDetails = "USP_UpdateStoreDetails";
    public static string USP_IsStoreCreated = "USP_IsStoreCreated";
    public static string USP_UpdateStore = "USP_UpdateStore";
    public static string USP_UpdateStoreTemplate = "USP_UpdateStoreTemplate";
    public static string USP_CopyStoreDetailsFromMCPToStore = "USP_CopyStoreDetailsFromMCPToStore";
    public static string USP_CreateCloneData = "USP_CreateCloneData";
    public static string USP_CopyCloneStoreDetailsFromMCPToStore = "USP_CopyCloneStoreDetailsFromMCPToStore";
    public static string USP_GetMasterStoreDetails = "USP_GetMasterStoreDetails";
    public static string USP_GetCloneMasterStoreDetails = "USP_GetCloneMasterStoreDetails";
    public static string USP_UpdateStoreCreated = "USP_UpdateStoreCreated";
    public static string USP_GetDTSDetails = "USP_GetDTSDetails";
    public static string USP_TransferProducts = "USP_TransferProducts";
    public static string USP_InsertGACode = "USP_InsertGACode";
    public static string USP_CreateDBUser = "USP_CreateDBUser";
    public static string USP_CreateCloneDBUser = "USP_CreateCloneDBUser";
    public static string USP_UpdateIsPunchout = "USP_UpdateIsPunchout";
    public static string USP_GetStoreUserMapping = "USP_GetStoreUserMapping";
    public static string USP_ManageContactUs = "USP_ManageContactUs";
    public static string USP_UpdateStorePunchout = "USP_UpdateStorePunchOut"; // Added By Snehal 22 09 2016

    public static string usp_GetStoreCurrencyDetails = "usp_GetStoreCurrencyDetails";
    public static string usp_GetStoreLanguageDetails = "usp_GetStoreLanguageDetails";
    public static string usp_UpdateStoreDetails_IsDefault = "usp_UpdateStoreDetails_IsDefault";
    public static string usp_UpdateStoreLanguageMappings_IsDefault = "usp_UpdateStoreLanguageMappings_IsDefault";
    public static string USP_GetMCPUserDetails = "USP_GetMCPUserDetails";
    public static string USP_InsertStoreCurrencyMultiple_catalogue_New = "USP_InsertStoreCurrencyMultiple_catalogue_New";
    public static string usp_UpdateStoreLanguageMappings_IsDefaultMCP = "usp_UpdateStoreLanguageMappings_IsDefaultMCP";

    public static string usp_InsertUpdateNotes = "usp_InsertUpdateNotes";
    public static string usp_GetNoteNo = "usp_GetNoteNo";
    public static string usp_GetNoteDetails = "usp_GetNoteDetails";
    public static string USP_GetAllCountryCurrencyMapping = "USP_GetAllCountryCurrencyMapping"; // indeed code
    public static string USP_InsertCatalogueImages = "USP_InsertCatalogueImages";
    public static string USP_GetAllCatalogueImages = "USP_GetAllCatalogueImages";
    #endregion

    #region BrandCoorManagement Procedures
    public static string USP_GetAllBrandColors = "USP_GetAllBrandColors";
    public static string USP_ManageBrandColorManagement_AED = "USP_ManageBrandColorManagement_AED";

    #endregion

    #region LanguageDA Procedures

    public static string USP_GetAllLanguageDetails = "USP_GetAllLanguageDetails";

    #endregion

    #region CurrencyDA Procedures

    public static string USP_GetAllCurrencyDetails = "USP_GetAllCurrencyDetails";
    public static string USP_GetCurrencyPointInvoiceDetails = "USP_GetCurrencyPointInvoiceDetails";//indeed code
    public static string Usp_UpdateCurrencyPointInvoiceDetails = "Usp_UpdateCurrencyPointInvoiceDetails"; //Added By Swapnil Bagkar 31-01-2017 indeed code

    #endregion

    #region UserDA Procedures
    public static string USP_UserEmailForSSO = "USP_UserEmailForSSO";
    public static string USP_GetUserDetailsForExport = "USP_GetUserDetailsForExport";
    public static string USP_GetUserDetails = "USP_GetUserDetails";
    public static string USP_GetRegUnregUserDetails = "USP_GetRegUnregUserDetails";//Added By Swapnil Bagkar 03-02-2017	indeed code	
    public static string USP_GetRegUnregExportUserDetails = "USP_GetRegUnregExportUserDetails";//Added By Swapnil Bagkar 03-02-2017 indeed code
    public static string USP_GetUserPointDetails = "USP_GetUserPointDetails";
    public static string USP_UpdateImportDataToOriginalTable = "USP_UpdateImportDataToOriginalTable";
    public static string USP_UpdateUserPoints = "USP_UpdateUserPoints";
    public static string USP_GetCustomerCount = "USP_GetCustomerCount";
    public static string USP_GetUserId = "USP_GetUserId";
    public static string USP_BindUserDeliveryAddress = "USP_BindUserDeliveryAddress";
    public static string USP_InsertUserHierarchy = "USP_InsertUserHierarchy";
    public static string USP_UpdateUserHierarchy = "USP_UpdateUserHierarchy";
    public static string USP_IsUserHierarchyLevelExists = "USP_IsUserHierarchyLevelExists";
    public static string USP_GetUserHierarchies = "USP_GetUserHierarchies";
    public static string USP_InsertRegistrationWhiteList = "USP_InsertRegistrationWhiteList";
    public static string USP_InsertRegistrationBlackList = "USP_InsertRegistrationBlackList";
    public static string USP_GetAllRegistrationWhiteList = "USP_GetAllRegistrationWhiteList";
    public static string USP_GetAllRegistrationBlackList = "USP_GetAllRegistrationBlackList";
    public static string USP_ManageUser = "USP_ManageUser";
    public static string USP_ManageUserStores = "USP_ManageUserStores";
    public static string USP_HeirarchyWiseUsers = "USP_HeirarchyWiseUsers";
    public static string USP_UpdateCustomerStatus = "USP_UpdateCustomerStatus";
    public static string USP_CheckUserExists = "USP_CheckUserExists";
    public static string USP_CheckGuestUserExists = "USP_CheckGuestUserExists";

    //added by vivek		
    public static string USP_CheckUserExistInCustonColumn = "USP_CheckUserExistInCustonColumn"; //indeed code
    public static string USP_CheckEmployeeExists = "USP_CheckEmployeeExist";//indeed code
    public static string USP_GetBAUserProfileDetails = "USP_GetBAUserProfileDetails";//indeed code
    //Added by Sripal
    public static string USP_GetUserDetailsBySearch = "USP_GetUserDetailsBySearch";
    public static string USP_ManageRegistrationData_SAED = "USP_ManageRegistrationData_SAED";
    public static string USP_GetReportNewLetterSubScription = "USP_GetReportNewLetterSubScription";



    public static string USP_GetUnRegisteredUserDetails = "USP_GetUnRegisteredUserDetails";
    public static string USP_UpdateUnregisteredUsersPoints = "USP_UpdateUnregisteredUserPoints";
    public static string USP_UpdateUserBudgetDetail = "USP_UpdateUserBudgetDetail";
    public static string USP_UpdateRegisteredUsersPoints = "USP_UpdateRegisteredUserPoints"; // Added By Snehal 21 09 2016
    public static string USP_UpdateRegUnregUserPoints = "USP_UpdateRegUnregUserPoints";//indeed code
    public static string USP_UpdateRegUnregUserPointsForEdit = "USP_UpdateRegUnregUserPointsForEdit"; //indeed code
    public static string USP_GetUsersBudgetDetail = "USP_GetUsersBudgetDetail";
 
    //Added by Tripti for non registered users import
    public static string USP_UpdateUnregisteredUsersPointsAdd = "USP_UpdateUnregisteredUserPoints_add";
    public static string USP_UpdateUnregisteredUsersPointsUpdate = "USP_UpdateUnregisteredUserPoints_update";
    public static string USP_INSERTNAMEBADGCUSTINFO = "USP_INSERTNAMEBADGCUSTINFO";
    #endregion

    #region UserHierarchy Procedures

    public static string USP_GetAllCustomFields = "USP_GetAllCustomFields";
    public static string USP_FieldTypeMasterDetails = "USP_FieldTypeMasterDetails";
    public static string USP_RegistrationCustomField_AED = "USP_RegistrationCustomField_AED";

    #endregion

    #region CouponDA Procedure
    public static string USP_GetListCouponMaster = "USP_GetListCouponMaster";
    public static string USP_GetListCouponMasterByCouponID = "USP_GetListCouponMasterByCouponID";
    public static string USP_DeleteCouponbyCouponID = "USP_DeleteCouponbyCouponID";
    public static string USP_GetListDiscountMaster = "USP_GetListDiscountMaster";
    public static string USP_DeleteAllowedUser = "USP_DeleteAllowedUser";
    public static string USP_DeleteAllowedDomain = "USP_DeleteAllowedDomain";
    public static string USP_GetDominIdfromName = "USP_GetDominIdfromName";
    public static string USP_GetUserIdfromEmail = "USP_GetUserIdfromEmail";
    public static string USP_GetUnMatcheduserIdfromEmail = "USP_GetUnMatcheduserIdfromEmail";
    public static string USP_GetUnMatcheduserIdfromDomain = "USP_GetUnMatcheduserIdfromDomain";
    public static string USP_GetListCouponMasterByName = "USP_GetListCouponMasterByName";
    public static string USP_InsertUpdateCouponMaster = "USP_InsertUpdateCouponMaster";
    public static string USP_ValidateCouponCode = "USP_ValidateCouponCode";
    public static string USP_GetUserCouponList = "USP_GetUserCouponList";
    /*Added By Hardik*/
    public static string USP_GetAllMinMaxCouponValues = "USP_GetAllMinMaxCouponValues";
    public static string USP_AED_MinMaxCouponCurrencyValues = "USP_AED_MinMaxCouponCurrencyValues";
    #endregion

    #region Categories

    public static string USP_GetAllCategoriesWithProductCount = "USP_GetAllCategoriesWithProductCount";

    public static string USP_GetCategoryList = "USP_GetCategoryList";

    public static string USP_Category_AED = "USP_Category_AED";

    public static string USP_GetAllCategories = "USP_GetAllCategories";

    public static string USP_GetSubCategories = "USP_GetSubCategories";

    public static string USP_GetCategoryBannerImageExtension = "USP_GetCategoryBannerImageExtension";

    public static string USP_SetCategorySequence = "USP_SetCategorySequence";

    public static string USP_AssignProductToCategory = "USP_AssignProductToCategory";

    public static string USP_GetCategoryTree = "USP_GetCategoryTree";

    public static string USP_GetRelationIdForCategories = "USP_GetRelationIdForCategories";

    public static string USP_CheckCategoryExists = "USP_CheckCategoryExists";

    public static string USP_MarkCategoryActiveInactive = "USP_MarkCategoryActiveInactive";
    public static string USP_DeleteCategories = "USP_DeleteCategories";

    public static string USP_GetAllCategoriesLanguageWise = "USP_GetAllCategoriesLanguageWise";

    public static string USP_GetCategoriesforImportExport = "USP_GetCategoriesforImportExport";

    public static string USP_GetSubCatImage = "USP_GetSubCatImage";

    #endregion

    #region Section Procedures

    public static string USP_GetSectionDetails = "USP_GetSectionDetails";
    public static string USP_IsSectionExists = "USP_IsSectionExists";
    public static string USP_InsertUpdateSection = "USP_InsertUpdateSection";
    public static string USP_GetSectionCategories = "USP_GetSectionCategories";
    public static string USP_GetAssignedSectionCategories = "USP_GetAssignedSectionCategories";
    public static string USP_GetSectionProducts = "USP_GetSectionProducts";
    public static string USP_GetSectionBannerImageExtension = "USP_GetSectionBannerImageExtension";
    public static string Usp_GetSectionProductsForhomepage = "Usp_GetSectionProductsForhomepage";
    #endregion

    #region Login Procedures
    public static string USP_GetMCPLoginDetailsFromCP = "USP_GetMCPLoginDetailsFromCP";
    public static string USP_ValidateAdminLogin = "USP_ValidateAdminLogin";
    public static string USP_GetLoginDetails = "USP_GetLoginDetails";
    public static string USP_ValidateLoginDetails = "USP_ValidateLoginDetails";
    public static string USP_InsertLoginDetails = "USP_InsertLoginDetails";
    public static string USP_InsertResetLoginDetails = "USP_InsertResetLoginDetails";
    public static string USP_InserSubscriberData = "USP_InserSubscriberData";
    public static string USP_ResetMCPPassword = "USP_ResetMCPPassword";
    public static string USP_ValidateLogin = "USP_ValidateLogin";
    public static string USP_IsExistUserAPI= "USP_IsExistUserAPI"; //Added By Snehal "29/09/2016"
    public static string USP_IsPunchoutUser = "USP_IsPunchoutUser"; //Added By Snehal "20/12/2016"
    public static string USP_IsPunchoutBASYS = "USP_IsPunchoutBASYS"; //Added By Snehal "21/12/2016"
   

    #endregion

    #region Static Page Management Procedures
    public static string usp_GetStaticPageContent = "usp_GetStaticPageContent";
    public static string usp_CheckStaticPageExists = "usp_CheckStaticPageExists";
    public static string usp_ExecuteStaticPageDetails = "usp_ExecuteStaticPageDetails";
    public static string USP_ManageResourceData_SAED = "USP_ManageResourceData_SAED";
    public static string USP_SetStaticPageSequence = "USP_SetStaticPageSequence";
    public static string USP_GetStaticPageAllLanguage = "USP_GetStaticPageAllLanguage";
    public static string USP_GetAllStaticPages = "USP_GetAllStaticPages";

    #endregion

    #region ProductDA Procedures

    public static string USP_GetProductDataStockCk = "USP_GetProductDataStockCk";
    public static string USP_GetProductData = "USP_GetProductData";
    public static string USP_GetProductDataStock = "USP_GetProductDataStock";
    public static string USP_GetProductFilterData = "USP_GetProductFilterData";
    public static string USP_CheckProductCodeExists = "USP_CheckProductCodeExists";
    public static string USP_ProductDetails_AED = "USP_ProductDetails_AED";
    public static string USP_AssignProductToCategories = "USP_AssignProductToCategories";
    public static string USP_ProductPriceBreak_AED = "USP_ProductPriceBreak_AED";
    public static string USP_ProductPriceDetails_AED = "USP_ProductPriceDetails_AED";
    public static string USP_GetProductDetailAdmin = "USP_GetProductDetailAdmin";
    public static string USP_SetDefaultPrice = "USP_SetDefaultPrice";
    public static string USP_ProductVariantType = "USP_ProductVariantType";
    public static string USP_ProductVariant_AED = "USP_ProductVariant_AED";
    public static string USP_GetProductVairants = "USP_GetProductVairants";
    public static string USP_UpdateProductDefaultImage = "USP_UpdateProductDefaultImage";
    public static string USP_UpdateProductVariant = "USP_UpdateProductVariant";
    public static string USP_GetProductListingFilterSettings = "USP_GetProductListingFilterSettings";
    public static string USP_GetProductIdByName = "USP_GetProductIdByName";
    public static string USP_GetProductDetail = "USP_GetProductDetail";
    public static string USP_AddRecentlyViewedProducts = "USP_AddRecentlyViewedProducts";
    public static string USP_GetRecentlyViewedProducts = "USP_GetRecentlyViewedProducts";
    public static string Entity_HomePageManagement = "HomePageManagementBE";
    public static string USP_GetCompareProductsDetail = "USP_GetCompareProductsDetail";
    public static string USP_GetAllProductsForLucene = "USP_GetAllProductsForLucene";
    public static string USP_GetAllProductSKU = "USP_GetAllProductSKU";
    public static string USP_ProductSKU_AED = "USP_ProductSKU_AED";
    public static string USP_GetProductAttributes = "USP_GetProductAttributes";
    public static string USP_ManageRelatedProducts = "USP_ManageRelatedProducts";
    public static string USP_GetProductIdNameAllLanguage = "USP_GetProductIdNameAllLanguage";
    public static string USP_DeleteProductPriceData = "USP_DeleteProductPriceData";
    public static string USP_GetAllProductsData = "USP_GetAllProductsData";
    public static string USP_ManageProductColorVariantMapping = "USP_ManageProductColorVariantMapping";
    public static string USP_InsertUpdateProductSelfColorVariantMapping = "USP_InsertUpdateProductSelfColorVariantMapping";
    public static string USP_DeleteGroupName = "USP_DeleteGroupName";
    public static string USP_GetProductWithoutImages = "USP_GetProductWithoutImages";
    public static string USP_GetProductColorGroupName = "USP_GetProductColorGroupName";
    public static string USP_InsertProductEnquiry = "USP_InsertProductEnquiry";
    public static string USP_GetProductEnquiryFieldsConfiguration = "USP_GetProductEnquiryFieldsConfiguration";
    public static string USP_GetSingleProductSKUId = "USP_GetSingleProductSKUId";
    public static string USP_GetProductList = "USP_GetProductList";
    public static string USP_UpdateProductStatus = "USP_UpdateProductStatus";
    public static string USP_GetProductAttributeManagementByProductId = "USP_GetProductAttributeManagementByProductId";
    public static string USP_GetProductColorVariantMappingByProductId = "USP_GetProductColorVariantMappingByProductId";
    public static string USP_AddProductReview = "USP_AddProductReview";
    public static string USP_GetProductReviewByProductId = "USP_GetProductReviewByProductId";
    public static string USP_GetProductCount = "USP_GetProductCount";
    public static string USP_InsertProductCustomRequest = "USP_InsertProductCustomRequest";
    public static string USP_GetAllProductReviewAndRating = "USP_GetAllProductReviewAndRating";
    public static string USP_UpdateProductReviewStatus = "USP_UpdateProductReviewStatus";
    public static string USP_GetProductReviewByReviewId = "USP_GetProductReviewByReviewId";
    public static string USP_GetListProductAttribute = "USP_GetListProductAttribute";
    public static string USP_TruncateProductAttribute = "USP_TruncateProductAttribute";
    public static string USP_GetTbl_ProductAttributeManagement = "USP_GetTbl_ProductAttributeManagement";
    public static string USP_GetProductDataTranslation = "USP_GetProductDataTranslation";
    public static string USP_DeleteProductTranslation = "USP_DeleteProductTranslation";
    public static string USP_GetTranslatedProductByCategory = "USP_GetTranslatedProductByCategory";
    public static string USP_UpdateProductTranslation = "USP_UpdateProductTranslation";
    public static string USP_InsertProductImportHistory = "USP_InsertProductImportHistory";
    public static string USP_GetProductImportHistory = "USP_GetProductImportHistory";
    public static string USP_GetUnMatchedProductForImport = "USP_GetUnMatchedProductForImport";
    public static string USP_InsertShoppingCartProducts = "USP_InsertShoppingCartProducts";
    public static string USP_GetShoppingCartProducts = "USP_GetShoppingCartProducts";
    public static string USP_chkProductVariantExists = "USP_chkProductVariantExists";
    public static string USP_GetUncategorizeProducts = "USP_GetUncategorizeProducts";
    public static string USP_ManageWishList = "USP_ManageWishList";
    public static string USP_InsertFreightSourceCountryMaster = "USP_InsertFreightSourceCountryMaster";
    public static string usp_getStoreBySearch = "usp_getStoreBySearch";
    public static string USP_DeleteFreightSourceCountryMaster = "USP_DeleteFreightSourceCountryMaster";
   
    #endregion

    #region Theme Configuration Procedures
    public static string USP_GetAllThemeConfiguratorElemnts = "USP_GetAllThemeConfiguratorElemnts";
    public static string USP_UpdateThemeConfiguratorElements = "USP_UpdateThemeConfiguratorElements";
    #endregion

    #region SlideShow / SliderImage Procedures

    public static string USP_ManageSlideShow_SAED = "USP_ManageSlideShow_SAED";

    public static string USP_GetAllSliderImages = "USP_GetAllSliderImages";

    public static string USP_GetAllSlideShowMaster = "USP_GetAllSlideShowMaster";

    public static string USP_GetAllSlideShowDetails = "USP_GetAllSlideShowDetails";

    public static string USP_InsertSlideImagesDetails = "USP_InsertSlideImagesDetails";

    public static string USP_GetOverLayText = "USP_GetOverLayText";

    public static string USP_SetOverLayText = "USP_SetOverLayText";
    #endregion

    #region FeatureDA

    public static string USP_GetAllFeatureDetails = "USP_GetAllFeatureDetails";

    #endregion

    #region LandingPage
    public static string USP_UpdateLandingPage = "USP_UpdateLandingPage";
    public static string USP_GetAllCurrencies = "USP_GetAllCurrencies";
    public static string USP_GetAllCountries = "USP_GetAllCountries";
    public static string USP_LandingPageConfiguration_SAE = "USP_LandingPageConfiguration_SAE";//Added by Sripal 
    public static string USP_GetListLandingPageConfiguration = "USP_GetListLandingPageConfiguration";//Added by Sripal 
    #endregion

    #region TemplateDA

    public static string USP_GetAllTemplateDetails = "USP_GetAllTemplateDetails";
    public static string USP_InsertTemplate = "USP_InsertTemplate";
    public static string USP_UpdateTemplate = "USP_UpdateTemplate";
    public static string USP_IsTemplateExists = "USP_IsTemplateExists";

    #endregion

    #region HomepageManagement
    public static string USP_HomePageConfiguration_SAED = "USP_HomePageConfiguration_SAED";
    public static string USP_ManageMetaTags = "USP_ManageMetaTags";
    #endregion

    #region HeaderFooterManagement Procedure
    public static string USP_HeaderConfiguration_SAED = "USP_HeaderConfiguration_SAED";
    public static string USP_FooterConfiguration_SAED = "USP_FooterConfiguration_SAED";
    #endregion

    #region StaticImage
    public static string USP_GETAllHomePageContentListing = "USP_GETAllHomePageContentListing";
    public static string USP_HomePageStaticContentAED = "USP_HomePageStaticContentAED";
    #endregion

    #region QuickShop
    public static string USP_GetProductByCategoryId = "USP_GetProductByCategoryId";
    public static string USP_GetAllProducts = "USP_GetAllProducts";
    #endregion

    #region Static Text Key Management
    public static string USP_GetStaticTextDetails = "USP_GetStaticTextDetails";
    public static string USP_InsertUpdateDeleteStaticText = "USP_InsertUpdateDeleteStaticText";
    #endregion

    #region Email Management
    public static string USP_GetEmailTemplate = "USP_GetEmailTemplate";
    public static string USP_UpdateEmailTemplate = "USP_UpdateEmailTemplate";
    #endregion

    #region Bread Crumb Management
    public static string USP_GetAllBreadCrumbsDetails = "USP_GetAllBreadCrumbsDetails";
    public static string USP_UpdateBreadCrumbDetails = "USP_UpdateBreadCrumbDetails";
    #endregion

    #region ShoppingCart Procedures
    public static string USP_UpdateBasketSessionProducts = "USP_UpdateBasketSessionProducts";
    public static string USP_GetBasketProductsData = "USP_GetBasketProductsData";
    public static string USP_RemoveProductsFromBasket = "USP_RemoveProductsFromBasket";
    public static string USP_UpdateBasketData = "USP_UpdateBasketData";
    public static string USP_CustomerOrder_SAE = "USP_CustomerOrder_SAE";
    public static string USP_GetAllPaymentTypes = "USP_GetAllPaymentTypes";
    public static string USP_GetAdFlexTransactionDetails = "USP_GetAdFlexTransactionDetails";
    public static string USP_GetTelecashTransactionDetails = "USP_GetTelecashTransactionDetails";
    public static string USP_GetCurrencynumber = "USP_GetCurrencynumber";
    public static string USP_GetUDFS = "USP_GetUDFS";
    public static string USP_UpdateUDFValues = "USP_UpdateUDFValues";
    public static string USP_UpdateUsedCouponCode = "USP_UpdateUsedCouponCode";

    public static string USP_TeleCashTransactions = "USP_TeleCashTransactions";
    public static string USP_Getadditionalcharges = "USP_Getadditionalcharges";

    #endregion

    public static string USP_GetCurrency = "USP_GetCurrency";
    public static string USP_UpdateStoreCurrencyAdflexDetails = "USP_UpdateStoreCurrencyAdflexDetails";

    public static string USP_UpdatePunchoutUserRegistrationDetails = "USP_UpdatePunchoutUserRegistrationDetails"; // Added By snehal - 21 12 2016
    public static string USP_InsertUserRegistrationDetails = "USP_InsertUserRegistrationDetails";
    public static string USP_InsertUpdateDeleteUserDeliveryAddress = "USP_InsertUpdateDeleteUserDeliveryAddress";
    public static string USP_InsertUserProfileDetails = "USP_InsertUserProfileDetails";
    public static string USP_InsertUserEDITProfileDetails = "USP_InsertUserEDITProfileDetails";
    public static string USP_InsertUserBASYSDetails = "USP_InsertUserBASYSDetails";
    public static string Usp_GetOrderDeliveryAddress = "Usp_GetOrderDeliveryAddress";
    public static string USP_CheckTelecashActiveOnSite = "USP_CheckTelecashActiveOnSite"; // Added By snehal - 06 01 2017
    /*Sachin Chauhan Start : 09 02 2016 : Constant for clearing all products from shopping cart*/
    public static string USP_RemoveAllBasketProducts = "USP_RemoveAllBasketProducts";
    public static string USP_getAllContentItemsStaticImage = "USP_getAllContentItemsStaticImage";
    public static string usp_deletecatalogueimages = "usp_deletecatalogueimages";
    /*Sachin Chauhan End : 09 02 2016*/

    #region SocialMedia
    public static string USP_ManageSocialMedia = "USP_ManageSocialMedia";
    public static string USP_SetSocialMediaSequence = "USP_SetSocialMediaSequence";
    public static string USP_GetAllSocailMedia = "USP_GetAllSocailMedia";
    #endregion

    #region CustomerGroup Procedure
    public static string USP_UpdateNoofUserCustomerGroup = "USP_UpdateNoofUserCustomerGroup";// called in nested Procedure
    public static string USP_ManageCustomerGroup = "USP_ManageCustomerGroup";
    public static string USP_GetAllUnAssignedCustomerGroup = "USP_GetAllUnAssignedCustomerGroup";
    public static string USP_ManageCustomerGroupWithUser = "USP_ManageCustomerGroupWithUser";

    #endregion

    #region OrderTracking
    public static string USP_GetOrdersByEmailId = "USP_GetOrdersByEmailId";
    public static string USP_InsertOrderTrackingManagement = "USP_InsertOrderTrackingManagement";
    public static string USP_GetOrderTrackingList = "USP_GetOrderTrackingList";
    #endregion

    #region Freight
    public static string USP_GetFreightDetails = "USP_GetFreightDetails";
    public static string USP_GetFreightDetails_Multiwarehouse = "USP_GetFreightDetails_Multiwarehouse";
    public static string USP_GetFreightCharges = "USP_GetFreightCharges";
    public static string USP_GetFreightCharges_mutliwarehouse = "USP_GetFreightCharges_mutliwarehouse";    
    public static string USP_GetAllFreightMultiplerCurrency = "USP_GetAllFreightMultiplerCurrency";
    public static string USP_GETFreightConfigurations = "USP_GETFreightConfigurations";
    #endregion

    #region FreightManagement
    public static string USP_GetFreightRegions = "USP_GetFreightRegions";
    public static string USP_GetFreightModes = "USP_GetFreightModes";
    public static string USP_GetFreightMethods = "USP_GetFreightMethods";
    public static string USP_FreightManagementSAED = "USP_FreightManagementSAED";
    public static string USP_GetFreight_Carrier_Service = "USP_GetFreight_Carrier_Service";
    public static string USP_UpdateFreightRegionName = "USP_UpdateFreightRegionName";
    public static string usp_GCRegions="usp_GCRegions";
    #endregion

    #region Error Management

    public static string USP_UpdateErrorTemplate = "USP_UpdateErrorTemplate";
    public static string USP_GetErrorTemplate = "USP_GetErrorTemplate";
    public static string USP_InsertErrorTemplate = "USP_InsertErrorTemplate";

    #endregion

    #region "ZoneManagement"
    public static string USP_ManageZoneManagement = "USP_ManageZoneManagement";
    #endregion

    #region "Reports"
    public static string USP_GetAbandonedCartList = "USP_GetAbandonedCartList";
    public static string USP_GetOrdersList = "USP_GetOrdersList";
    public static string USP_GetGCOrdersList = "USP_GetOrdersList_GiftCertificate"; // Added By Snehal - GC Report - 15 12 2016
    #endregion

    #region "Punchout"
    public static string USP_PunchoutMarkerSAED = "USP_PunchoutMarkerSAED";
    public static string USP_PunchoutDetailsSAED = "USP_PunchoutDetailsSAED";
    public static string USP_PunchoutDetailsTopDesc = "USP_PunchoutDetailsTopDesc";
    public static string USP_GetPunchoutDetailsByID = "USP_GetPunchoutDetailsByID";
    public static string USP_CustomerOrderPunchout_SAE = "USP_CustomerOrderPunchout_SAE";
    //public static string USP_IsPunchoutUser = "USP_IsPunchoutUser"; //Added By Snehal "20/12/2016"		
    //public static string USP_IsPunchoutBASYS = "USP_IsPunchoutBASYS"; //Added By Snehal "21/12/2016"
    #endregion

    #region "G+ SSO"
    public static string usp_AddJson = "USP_AddJson";
    public static string USP_UpdateTbl_UsersSSO = "USP_UpdateTbl_UsersSSO";
    #endregion

    #region "UserRegistration"
    public static string USP_GetRegistrationDetails = "USP_GetRegistrationDetails";
    public static string USP_UpdatePassword = "USP_UpdatePassword";
    public static string USP_GuestRegDetails = "USP_GuestRegDetails";
    #endregion

    #region "Gift Certificate"
    public static string USP_InsertGiftCertificate = "USP_InsertGiftCertificate";
    public static string USP_InsertGiftOrderDetails = "USP_InsertGiftOrderDetails";
    public static string USP_UpdateGiftCertificate = "USP_UpdateGiftCertificate";
    public static string USP_GetGiftCertificateDetails = "USP_GetGiftCertificateDetails";

    public static string UpdateAdflexGiftCertificate = "UpdateAdflexGiftCertificate";
    public static string USP_InsertCustomerOrderGiftCertificate = "USP_InsertCustomerOrderGiftCertificate";
    #endregion

    #region "Random Products"
    public static string USP_GetRandomProducts = "USP_GetRandomProducts";// 4 products    
    #endregion

    #region PaymentTypes/UDFS/PickListItems Procedures

    public static string USP_InsertPaymenttype = "USP_InsertPaymenttype";
    public static string USP_InsertUDFS = "USP_InsertUDFS";
    public static string USP_InsertPickListItem = "USP_InsertPickListItem";
    public static string USP_RemoveAllPaymentTypes = "USP_RemoveAllPaymentTypes";
    public static string USP_GetAllPaymentTypesDetails = "USP_GetAllPaymentTypesDetails";
    public static string USP_UpdatePaymentTypes = "USP_UpdatePaymentTypes";
    public static string USP_GetAllUDFSDetails = "USP_GetAllUDFSDetails";
    public static string USP_UpdateUDFSIsMandatory = "USP_UpdateUDFSIsMandatory";
    public static string USP_UpdateCustomerRefUDFSIsMandatory = "USP_UpdateCustomerRefUDFSIsMandatory";
    //public static string USP_CheckTelecashActiveOnSite = "USP_CheckTelecashActiveOnSite"; // Added By snehal - 06 01 2017

    #region "/*User type*/"
    public static string USP_GetAllPaymentPickListItem = "USP_GetAllPaymentPickListItem";
    public static string USP_UpdatePickListPaymentItem = "USP_UpdatePickListPaymentItem";
    #endregion

    public static string USP_GetRelatedIdbyCatSubCatSubSubCat = "USP_GetRelatedIdbyCatSubCatSubSubCat";
    public static string USP_GetProductByCategoryId_prodSeq = "USP_GetProductByCategoryId_prodSeq";
    public static string USP_SetProductSequence = "usp_SetProductSequence";
    public static string USP_InsertStoreCurrencyMultiple = "USP_InsertStoreCurrencyMultiple";
    public static string USP_InsertStoreCurrencyMultiple_catalogue = "USP_InsertStoreCurrencyMultiple_catalogue";
    public static string USP_GetAllCategoriesWithProductCountList = "USP_GetAllCategoriesWithProductCountList";
    #endregion
    #region "User Types"
    //public static string UserTypesBE = "UserTypesBE";
    //public static string UserTypesBEDivision = "UserTypesBE+Division";
    //public static string UserTypesBEKeyGroups = "UserTypesBE+Key_Groups";
    //public static string UserTypesBECatalogues = "UserTypesBE+Catalogues";
    //public static string UserTypesBESource_Codes = "UserTypesBE+Source_Codes";
    //public static string UserTypesBEWeb_Search_Accounts = "UserTypesBE+Web_Search_Accounts";

    //public static string USP_getAllDivision = "USP_getAllDivision";
    //public static string USP_getAllWeb_Search_Accounts = "USP_getAllWeb_Search_Accounts";
    //public static string USP_getAllKey_Groups = "USP_getAllKey_Groups";
    //public static string USP_getAllCatalogues = "USP_getAllCatalogues";
    //public static string USP_getAllSource_Codes = "USP_getAllSource_Codes";
    //public static string USP_getDivisionsByID = "USP_getDivisionsByID";
    //public static string USP_getWeb_Search_AccountsByDIVISIONID = "USP_getWeb_Search_AccountsByDIVISIONID";
    //public static string USP_getKey_GroupsById = "USP_getKey_GroupsById";
    //public static string USP_getCataloguesById = "USP_getCataloguesById";
    //public static string USP_getSource_CodesById = "USP_getSource_CodesById";
    //public static string USP_getSource_CodesByCatId = "USP_getSource_CodesByCatId";
    //public static string USP_getWeb_Search_AccountsBySourceId = "USP_getWeb_Search_AccountsBySourceId";
    //public static string USP_getKey_GroupsByDivId = "USP_getKey_GroupsByDivId";
    //public static string USP_GetAllNewCatalogueDetail = "USP_GetAllNewCatalogueDetail";

    #region Entity
    public static string UserTypesBE = "UserTypesBE";
    public static string UserTypesBEUserTypeDetails = "UserTypesBE+UserTypeDetails";
    public static string UserTypesBEUserTypesLanguages = "UserTypesBE+UserTypeLanguages";
    public static string UserTypesBEUserTypeCatalogue = "UserTypesBE+UserTypeCatalogue";
    public static string UserTypesDetailsBE = "UserTypesDetailsBE";
    public static string UserTypeMappingBE = "UserTypeMappingBE";
    public static string UserTypeMappingBEUserTypeDetails = "UserTypeMappingBE+UserTypeDetails";
    public static string UserTypeMappingBEUserTypesLanguages = "UserTypeMappingBE+UserTypeLanguages";
    public static string UserTypeMappingBEUserTypeMasterOptions = "UserTypeMappingBE+UserTypeMasterOptions";
    public static string UserTypeMappingBEUserTypeEmailValidation = "UserTypeMappingBE+UserTypeEmailValidation";
    public static string UserTypeMappingBECustomUserTypes = "UserTypeMappingBE+CustomUserTypes";
    public static string UserTypeMappingBECountry = "UserTypeMappingBE+Country";
    public static string UserTypeMappingBERegistrationFieldsConfigurationBE = "UserTypeMappingBE+RegistrationFieldsConfigurationBE";
    public static string UserTypeMappingBERegistrationFeildTypeMasterBE = "UserTypeMappingBE+RegistrationFeildTypeMasterBE";
    public static string UserTypeMappingBEUserTypeCustomFieldMapping = "UserTypeMappingBE+UserTypeCustomFieldMapping";
    public static string UserTypeMappingBERegistrationLanguagesBE = "UserTypeMappingBE+RegistrationLanguagesBE";
    public static string UserTypeMappingBERegistrationFieldDataMasterBE = "UserTypeMappingBE+RegistrationFieldDataMasterBE";
    public static string UserTypeMappingBERegistrationFieldDataMasterValidationBE = "UserTypeMappingBE+RegistrationFieldDataMasterValidationBE";
    public static string UserTypeCountryMaster = "UserTypeCountryMaster";
    public static string UserTypeCountryMasterCountryMasterBE = "UserTypeCountryMaster+CountryMasterBE";
    public static string UserTypeCountryMasterUserTypeLanguagesBE = "UserTypeCountryMaster+UserTypeLanguagesBE";
    public static string UserTypeCustomFieldDataValueBE = "UserTypeCustomFieldDataValue";
    public static string UserTypeCustomMapping = "UserTypeCustomMapping";
    public static string RegistrationFieldDataMasterValidationBE = "RegistrationFieldDataMasterValidationBE";
    public static string StoreAndUserTypeCatalogueDetailsBE = "StoreAndUserTypeCatalogueDetailsBE";
    public static string CustomFieldValidationBE = "UserTypeMappingBE+RegistrationFieldDataMasterValidationBE";/*User Type*/
    #endregion
    #region Store Procedures
    public static string USP_ManageUserType_SAED = "USP_ManageUserType_SAED";
    public static string USP_GetAllUserTypeDetailForEdit = "USP_GetAllUserTypeDetailForEdit";
    public static string USP_GetAllUserTypeDetailsByUserTypeID = "USP_GetAllUserTypeDetailsByUserTypeID";
    public static string USP_UpdateUserTyepDetailsByUserTypeID = "USP_UpdateUserTyepDetailsByUserTypeID";
    public static string USP_UserTypeMappingDetails = "USP_UserTypeMappingDetails";
    public static string USP_GetAllUserTypes = "USP_GetAllUserTypes";
    public static string USP_GetAllCustomFieldsByUserTypeID = "USP_GetAllCustomFieldsByUserTypeID";
    public static string USP_RegistrationUserTypeCustomField_AED = "USP_RegistrationUserTypeCustomField_AED";
    public static string USP_GetAllUserTypeMasterOptions = "USP_GetAllUserTypeMasterOptions";
    public static string USP_UpdateUserTypeMasterOptions = "USP_UpdateUserTypeMasterOptions";
    public static string USP_GetUserTypeCountryMaster = "USP_GetUserTypeCountryMaster";
    public static string USP_GetAllCurruncyCountryMappings = "USP_GetAllCurruncyCountryMappings";
    public static string USP_SetCountryMasterUserType = "USP_SetCountryMasterUserType";
    public static string USP_ManageUserTypeEmailValidation = "USP_ManageUserTypeEmailValidation";
    public static string USP_GetUserTypePaymentOption = "USP_GetUserTypePaymentOption";
    public static string USP_InsertUserTypeEmailImportList = "USP_InsertUserTypeEmailImportList";
    public static string USP_AEDUserTypePaymentOption = "USP_AEDUserTypePaymentOption";
    public static string USP_GetAllCustomFieldDataByFieldID = "USP_GetAllCustomFieldDataByFieldID";
    public static string USP_GetAllRegistrationFieldDataMasterValidation = "USP_GetAllRegistrationFieldDataMasterValidation";
    public static string USP_UpdateUserTypeRegistrationCustomFields = "USP_UpdateUserTypeRegistrationCustomFields";
    public static string USP_CheckForExistingCustomFieldName = "USP_CheckForExistingCustomFieldName";
    public static string USP_InsertUserTypeCustomMappingImport = "USP_InsertUserTypeCustomMappingImport";
    public static string USP_ManageUserTypeCategories = "USP_ManageUserTypeCategories";
    public static string USP_GetAllStoreAndUserTypeCatalogueDetailsByUserTypeID = "USP_GetAllStoreAndUserTypeCatalogueDetailsByUserTypeID";
    public static string USP_UpdateUserTypeCatalogues = "USP_UpdateUserTypeCatalogues";
    public static string USP_GetDefaultCustomField = "USP_GetDefaultCustomField";
    public static string USP_UpdateCustomFieldNames = "USP_UpdateCustomFieldNames";
    public static string USP_UpdateUserTypeSequences = "USP_UpdateUserTypeSequences";
    public static string USP_ValidateRegistrationCustomFields = "USP_ValidateRegistrationCustomFields";
    public static string USP_ValidateRegistrationCustomFieldsEditProfile = "USP_ValidateRegistrationCustomFieldsEditProfile"; // Added By snehal 14 10 2016
    public static string USP_GetAllCustomFieldValidationRule = "USP_GetAllCustomFieldValidationRule";/*User Type*/
    public static string USP_InsertCustomFieldValues = "USP_InsertCustomFieldValues";/*User Type*/
    public static string USP_IsPointEnabledForUT = "USP_IsPointEnabledForUT"; // Added by ShriGanesh 30 June 2017
    public static string USP_IsOnlyCurrencyEnabled = "USP_IsOnlyCurrencyEnabled";// Added by ShriGanesh 03 July 2017
    public static string USP_IsBothCurrencyAndPointsEnabled = "USP_IsBothCurrencyAndPointsEnabled";// Added by ShriGanesh 03 July 2017
    #endregion
    #region "Catalogue"
    #region Entity
    public static string CatalogueBE = "CatalogueBE";
    public static string CatalogueBEDivision = "CatalogueBE+Division";
    public static string CatalogueBEKeyGroups = "CatalogueBE+Key_Groups";
    public static string CatalogueBECatalogues = "CatalogueBE+Catalogues";
    public static string CatalogueBESource_Codes = "CatalogueBE+Source_Codes";
    public static string CatalogueBEWeb_Search_Accounts = "CatalogueBE+Web_Search_Accounts";
    #endregion
    #endregion
    #region Store Procedure
    public static string USP_getAllDivision = "USP_getAllDivision";
    public static string USP_getAllWeb_Search_Accounts = "USP_getAllWeb_Search_Accounts";
    public static string USP_getAllKey_Groups = "USP_getAllKey_Groups";
    public static string USP_getAllCatalogues = "USP_getAllCatalogues";
    public static string USP_getAllSource_Codes = "USP_getAllSource_Codes";
    public static string USP_getDivisionsByID = "USP_getDivisionsByID";
    public static string USP_getWeb_Search_AccountsByDIVISIONID = "USP_getWeb_Search_AccountsByDIVISIONID";
    public static string USP_getKey_GroupsById = "USP_getKey_GroupsById";
    public static string USP_getCataloguesById = "USP_getCataloguesById";
    public static string USP_getSource_CodesById = "USP_getSource_CodesById";
    public static string USP_getSource_CodesByCatId = "USP_getSource_CodesByCatId";
    public static string USP_getWeb_Search_AccountsBySourceId = "USP_getWeb_Search_AccountsBySourceId";
    public static string USP_getKey_GroupsByDivId = "USP_getKey_GroupsByDivId";
    public static string USP_GetAllNewCatalogueDetail = "USP_GetAllNewCatalogueDetail";
    public static string USP_GetAllPaymentTypesbyLanguageID = "USP_GetAllPaymentTypesbyLanguageID";
    public static string USP_getWeb_Search_AccountsByCustomerId = "USP_getWeb_Search_AccountsByCustomerId";

    #endregion
    #endregion

    #region "Email Counter"
    public static string USP_updateEmailCounter = "USP_updateEmailCounter"; 
    #endregion

    public static string USP_GetAllUDFSDetailspaymentTypeID = "USP_GetAllUDFSDetailspaymentTypeID";
    public static string USP_GetCustomerRefUDFSDetails = "USP_GetCustomerRefUDFSDetails"; // Added By Snehal 05 01 2017

    public static string USP_GetAllPaymentPickListItemPaymentTypeId = "USP_GetAllPaymentPickListItemPaymentTypeId";
    public static string USP_UserTypeMappingDetailsByCID = "USP_UserTypeMappingDetailsByCID";

    public static string FreightSourceCountryMaster = "FreightSourceCountryMaster";
    public static string USP_GetAllFreightSourceCountryMaster = "USP_GetAllFreightSourceCountryMaster";

    public static string USP_GetCustomColumnValue = "USP_GetCustomColumnValue";
    public static string USP_UpdateCustomColumnValue = "USP_UpdateCustomColumnValues";


    #region Added By Snehal 20 10 2016 - New FreightRegion Module

    #region FreightRegion Module Entity
    public static string Entity_FreightRegionManagementBE = "FreightRegionManagementBE";
    public static string Entity_ShipmentMethodMasterBE = "FreightRegionManagementBE+ShipmentMethodMasterBE";
    public static string Entity_ShipmentMethodLanguagesBE = "FreightRegionManagementBE+ShipmentMethodLanguagesBE";
    public static string Entity_RegionServiceShipmentBE = "FreightRegionManagementBE+RegionServiceShipmentBE";
    public static string Entity_FreightRegionsBE = "FreightRegionManagementBE+FreightRegionsBE";
    public static string Entity_FreightSourceCountryMasterBE = "FreightSourceCountryMasterBE";
    #endregion

    #region SP - Freight Module
    public static string USP_GetALLShipmentMethodFreight = "USP_GetALLShipmentMethodFreight";
    public static string USP_InsertFreightRegion = "USP_InsertFreightRegion";
    public static string USP_GetFreightShipmentServiceMethod = "USP_GetFreightShipmentServiceMethod";
    public static string USP_InsertFreightModeAsPerRegion = "USP_InsertFreightModeAsPerRegion";
    public static string USP_GetFreightTables = "USP_GetFreightTables";
    public static string USP_FreightManagementPerBand = "USP_FreightManagementPerBand";
    public static string USP_GetFreightTableID = "USP_GetFreightTableID"; //Added By Hardik on 23/May/2017
    public static string USP_FreightManagementPerBand_MultipleCountrySrc = "USP_FreightManagementPerBand_MultipleCountrySrc"; //Added By Hardik on 30/May/2017
    #endregion

    //Added By Snehal on 05 12 2016 - Google Referrel URL
    public static string USP_UpdateUserTypeForReferrelURL = "USP_UpdateUserTypeForReferrelURL";

    #endregion

    public static string usp_InsertUpdateSectionHandlingFees = "usp_InsertUpdateSectionHandlingFees";

    public static string usp_GetActiveStoreLanguageDetails = "usp_GetActiveStoreLanguageDetails";
    public static string usp_InsertUpdateUserPreferred = "usp_InsertUpdateUserPreferred";
    public static string usp_GetCurrencyUserTypeWise = "usp_GetCurrencyUserTypeWise";
    public static string usp_GetUserPreferredUserIdWise = "usp_GetUserPreferredUserIdWise";

    public static string USP_GetOrdershistory = "USP_GetOrdershistory"; //Added By Tripti "4/1/2017"

    public static string USP_UpdateSSOUserProfileDetails = "USP_UpdateSSOUserProfileDetails";
    public static string usp_GetRegistrationDataDanske = "usp_GetRegistrationDataDanske"; //Added By Hardik on 02/MARCH/2017
    public static string USP_GetFromEmailIdAlias = "USP_GetFromEmailIdAlias";    //Added By Hardik on 28/FEB/2017

    #region Added by tripti For product preferences 10/02/2017
    public static string YouMayAlsoLikeMappingBE = "YouMayAlsoLikeMappingBE";
    public static string YouMayAlsoLikeMappingBEYouMayAlsoLikecategory = "YouMayAlsoLikeMappingBE+YouMayAlsoLikecategory";
    public static string YouMayAlsoLikeMappingBEYouMayAlsoLikeprice = "YouMayAlsoLikeMappingBE+YouMayAlsoLikeprice";
    public static string YouMayAlsoLikeMappingBEYouMayAlsoLikeproduct = "YouMayAlsoLikeMappingBE+YouMayAlsoLikeproduct";
    public static string USP_Getyoumaylikeproductdetail = "USP_Getyoumaylikeproductdetail";
    public static string USP_getreferralurl = "USP_getreferralurl";
    #endregion

    public static string USP_GetTitles = "USP_GetTitles";

    #region MICHELIN SSO
    public static string USP_GetCountryWiseCurrencyMapping = "USP_GetCountryWiseCurrencyMapping";
    public static string USP_GetAlllstLanguageCodeMaster = "USP_GetAlllstLanguageCodeMaster";
    public static string USP_GetSSOUserTypeCatalogueDetails = "USP_GetSSOUserTypeCatalogueDetails";
    public static string USP_IsExistUser_MichelinSSO = "USP_IsExistUser_MichelinSSO";
    public static string USP_GETPGTVALUE = "USP_GETPGTVALUE";
    public static string USP_INSERTPGTVALUE = "USP_INSERTPGTVALUE";
    #endregion

    #region Indeed Module SP Entity - Added By Snehal
    public static string Entity_BA_SALESGRANDTOTAL = "IndeedSalesBE+BA_SALESGRANDTOTAL";
    public static string Entity_BA_SALESBYGROUP = "IndeedSalesBE+BA_SALESBYGROUP";
    public static string Entity_BA_USERFILE = "UserBE+BA_USERFILE";
    public static string Entity_BA_UserAddress = "IndeedSalesBE+BA_UserAddress";
    public static string Entity_BA_UserPoints = "IndeedSalesBE+BA_Points";
    public static string Entity_BA_Budget = "IndeedSalesBE+BA_Budget";

    public static string USP_Indeed_TotalOrders = "USP_Indeed_TotalOrders";
    public static string USP_Indeed_TotalSales = "USP_Indeed_TotalSales";
    public static string USP_Indeed_GetCurrencies = "USP_Indeed_GetCurrencies";
    public static string USP_Indeed_SalesOrderByRegion = "USP_Indeed_SalesOrderByRegion";
    public static string USP_Indeed_ProductDashboard = "USP_Indeed_ProductDashboard";
    public static string USP_Indeed_OrderUsage = "USP_Indeed_OrderUsage";
    public static string USP_Indeed_ContactInformation = "USP_Indeed_ContactInformation";
    public static string USP_Indeed_ChangePassword = "USP_Indeed_ChangePassword";
    public static string USP_Indeed_GetTotalCustomerAddress = "USP_Indeed_GetTotalCustomerAddress";
    public static string USP_Indeed_GetCustomerAddressDetails = "USP_Indeed_GetCustomerAddressDetails";
    public static string USP_Indeed_UpdateCustomerAddressDetails = "USP_Indeed_UpdateCustomerAddressDetails";
    public static string USP_Indeed_DeleteCustomerAddressDetails = "USP_Indeed_DeleteCustomerAddressDetails";
    public static string USP_Indeed_UpdateMarketPreference = "USP_Indeed_UpdateMarketPreference";
    public static string USP_Indeed_GetPointsData = "USP_Indeed_GetPointsData";
    public static string USP_Indeed_GetPointsTransactioDetails = "USP_Indeed_GetPointsTransactioDetails";
    public static string USP_Indeed_GetBudgetDetails = "USP_Indeed_GetBudgetDetails";
    public static string USP_Indeed_GetBudgeTransactionDetails = "USP_Indeed_GetBudgeTransactionDetails";
    public static string USP_GetBudgetdetails = "USP_GetBudgetdetails";
    public static string USP_GetUserBudgetDetailForPayment = "USP_GetUserBudgetDetailForPayment";
    public static string Usp_Indeed_UpdateProjectedSaleDetails = "Usp_Indeed_UpdateProjectedSaleDetails";
    public static string USP_Indeed_GetProjectedSales = "USP_Indeed_GetProjectedSales";
    public static string USP_InsertUpdateEmployeeSAML = "USP_InsertUpdateEmployeeSAML";
    public static string USP_GetSAMLByEmployeeID = "USP_GetSAMLByEmployeeID";
    public static string USP_getpoints = "USP_getpoints";
    public static string USP_ContactInformation = "USP_ContactInformation";
    public static string USP_GetBudgetDetailsMyProfile = "USP_GetBudgetDetailsMyProfile";
    #endregion

    public static string USP_DeleteBudgetRecord = "USP_DeleteBudgetRecord";
    public static string USP_CheckBudgetIsActive = "USP_CheckBudgetIsActive";
    public static string USP_GetBudgeTransaction = "USP_GetBudgeTransaction";
}