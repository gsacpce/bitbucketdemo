﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.IO;
using System.Configuration;

public static class Exceptions
{
    #region WRITEEXCEPTIONLOG

    /// <summary>
    /// Method to write application wide exception log. 
    /// </summary>
    /// <param name="ex">Object of Class Exception</param>
    public static void WriteExceptionLog(Exception ex)
    {
        System.Threading.ThreadAbortException exception = ex as System.Threading.ThreadAbortException;
        if (exception == null)
        {
            //string ExceptionLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "ExceptionLogs";
            string ExceptionLogFolderPath = "";
            //if (HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost"))
            //{
                ExceptionLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "ExceptionLogs"; //For MCP Site
            //}
            //else
            //{
                //ExceptionLogFolderPath = @"F:\DTS_Scheduler\ExceptionLogs";
            //}
            //ExceptionLogFolderPath = @"D:\DTS_Scheduler\ExceptionLogs";//For Schedular

            try
            {
                if (!Directory.Exists(ExceptionLogFolderPath))
                    Directory.CreateDirectory(ExceptionLogFolderPath);

                if (Directory.Exists(ExceptionLogFolderPath))
                {
                    //Create month wise exception log file.
                    string date = string.Format("{0:dd}", DateTime.Now);
                    string month = string.Format("{0:MMM}", DateTime.Now);
                    string year = string.Format("{0:yyyy}", DateTime.Now);

                    string folderName = month + year; //Feb2013
                    string monthFolder = ExceptionLogFolderPath + "\\" + folderName;
                    if (!Directory.Exists(monthFolder))
                        Directory.CreateDirectory(monthFolder);

                    string ExceptionLogFileName = monthFolder +
                        "\\ExceptionLog_" + date + month + ".txt"; //ExceptionLog_04Feb.txt

                    using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(ExceptionLogFileName, true))
                    {
                        strmWriter.WriteLine("On " + DateTime.Now.ToString() +
                            ", following error occured in the application:");
                        strmWriter.WriteLine("Message: " + ex.Message);
                        //strmWriter.WriteLine("Inner Exception: " + ex.InnerException.Message);
                        //strmWriter.WriteLine("Inner Exception(2): " + ex.InnerException.InnerException.Message);
                        strmWriter.WriteLine("Source: " + ex.Source);
                        strmWriter.WriteLine("Stack Trace: " + ex.StackTrace);
                        strmWriter.WriteLine("HelpLink: " + ex.HelpLink);
                        strmWriter.WriteLine("-------------------------------------------------------------------------------");
                    }
                }
                else
                    throw new DirectoryNotFoundException("Exception log folder not found in the specified path");
            }
            catch
            {

            }
        }
    }

    #endregion WRITEEXCEPTIONLOG

    #region WRITEINFOLOG
    /// <summary>
    /// Method to write application wide Information log. 
    /// </summary>
    /// <param name="ex">string message parameter</param>
    public static void WriteInfoLog(string message)
    {
        //string ExceptionLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "ExceptionLogs";
        string ExceptionLogFolderPath = "";
        //if (HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost"))
        //{
              ExceptionLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "ExceptionLogs"; //For MCP Site
        //}
        //else
        //{
            //ExceptionLogFolderPath = @"F:\DTS_Scheduler\ExceptionLogs";
        //}
        //ExceptionLogFolderPath = @"D:\DTS_Scheduler\ExceptionLogs";//For Schedular

        try
        {
            if (!Directory.Exists(ExceptionLogFolderPath))
                Directory.CreateDirectory(ExceptionLogFolderPath);

            if (Directory.Exists(ExceptionLogFolderPath))
            {
                //Create month wise exception log file.
                string date = string.Format("{0:dd}", DateTime.Now);
                string month = string.Format("{0:MMM}", DateTime.Now);
                string year = string.Format("{0:yyyy}", DateTime.Now);

                string folderName = month + year; //Feb2013
                string monthFolder = ExceptionLogFolderPath + "\\" + folderName;
                if (!Directory.Exists(monthFolder))
                    Directory.CreateDirectory(monthFolder);

                string ExceptionLogFileName = monthFolder +
                    "\\InfoLog_" + date + month + ".txt"; //ExceptionLog_04Feb.txt

                using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(ExceptionLogFileName, true))
                {
                    strmWriter.WriteLine("On " + DateTime.Now.ToString() + ",");
                    strmWriter.WriteLine("Message: " + message);
                    strmWriter.WriteLine("-------------------------------------------------------------------------------");
                }
            }
            else
                throw new DirectoryNotFoundException("Exception log folder not found in the specified path");
        }
        catch
        {

        }
    }

    #endregion

    /// <summary>
    /// Method to get messages from exceptionmessages.xml. 
    /// </summary>
    /// <param name="Key">Key</param>
    public static string GetException(string Key)
    {
        try
        {
            string message;
            XmlDocument doc = new XmlDocument();
            doc.Load(GlobalFunctions.GetPhysicalFolderPathAdmin() + "ExceptionMessages.xml");
            //doc.Load(@"F:\DTS_Scheduler\ExceptionLogs" + "ExceptionMessages.xml"); //Added By Sripal for DTS_Schedular testing
            XmlNodeList docNodeList = doc.SelectNodes("/Exception/" + Key);
            message = docNodeList.Item(0).InnerText;
            return message;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return "";
            // throw ex;
        }
    }

    #region WRITESSOLOG
    /// <summary>
    /// Method to write application wide SSO log. 
    /// </summary>
    /// <param name="ex">string message parameter</param>
    public static void WriteSSOLog(string message,string FolderPath)
    {              
       try
        {
            if (!Directory.Exists(FolderPath))
                Directory.CreateDirectory(FolderPath);

            if (Directory.Exists(FolderPath))
            {
                //Create month wise exception log file.
                string date = string.Format("{0:dd}", DateTime.Now);
                string month = string.Format("{0:MMM}", DateTime.Now);
                string year = string.Format("{0:yyyy}", DateTime.Now);

                string folderName = month + year; //Feb2013
                string monthFolder = FolderPath + "\\" + folderName;
                if (!Directory.Exists(monthFolder))
                    Directory.CreateDirectory(monthFolder);

                string SSPLogFileName = monthFolder +
                    "\\SSPLog_" + date + month + ".txt"; //ExceptionLog_04Feb.txt

                using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(SSPLogFileName, true))
                {
                    strmWriter.WriteLine("On " + DateTime.Now.ToString() + ",");
                    strmWriter.WriteLine("Message: " + message);
                    strmWriter.WriteLine("-------------------------------------------------------------------------------");
                }
            }
            else
                throw new DirectoryNotFoundException("SSO log folder not found in the specified path");
        }
        catch
        {

        }
    }

    #endregion
}
