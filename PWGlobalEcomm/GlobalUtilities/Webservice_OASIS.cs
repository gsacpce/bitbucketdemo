﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using System.Security;
using System.Xml;
using System.Net;
using System.Configuration;
using System.IO;
using System.Web;
using System.Xml.Serialization;
using System.Xml.Linq;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.BusinessEntity;
using PWGlobalEcomm.IsMarketingLive;
using System.Globalization;

public class Webservice_OASIS
{
    StoreBE objStoreBE = new StoreBE();

    public XmlNode Service_OASIS(string strFields, string strFieldValues, string strNodeName)
    {
        int intID = 0;
        XmlNode ndService = null;
        try
        {
            ndService = getXmlNode(strFields, strFieldValues, strNodeName);
            return ndService;
        }
        catch (Exception ex)
        {
            return ndService;
        }
        finally
        {
        }
    }

    public int OrderService_OASIS(string strOrderHeaderFields,
        string strOrderHeaderValues,
        string strGiftCertificateHeaders,
        string strGiftCertValue,
        string strShipToFields,
        string strBillToFields,
        string strBillToValues,
        string OrderItemFields,
        string OrderItemValues,
        string strUDFields,
        string strUDFValues,
        CustomerOrderBE lstCustomerOrderBE,
        UserBE lstUser,
        bool IsCreditCardOrder)
    {
        lstUser = HttpContext.Current.Session["User"] as UserBE;
        XmlNode ndOrder = null;
        XmlNode nGiftCertificate = null;
        XmlNode ndShip = null;
        XmlNode ndBill = null;
        XmlNode ndOrderLine = null;
        XmlNode ndOrderHeader = null;
        XmlNode ndUDF = null;

        XmlNode ndPowerWeaveID = null;
        XmlNode ndOrderDate = null;
        XmlDataDocument xmlDoc = null;
        StringBuilder strbldrshipValues = new StringBuilder();
        objStoreBE = StoreBL.GetStoreDetails();
        int intOrderID = 0;
        try
        {
            xmlDoc = new XmlDataDocument();
            ndOrderHeader = xmlDoc.CreateNode(XmlNodeType.Element, "Order", "");
            ndOrder = getXmlNode(strOrderHeaderFields, strOrderHeaderValues, "OrderHeader");

            // Below LOC is Commented by SHRIGANESH 28 September 2016 to pass Delivery Company to Order XML
            //if (IsCreditCardOrder)
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryCompany + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryContactPerson + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryAddress1 + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryAddress2 + "|*|");
            strbldrshipValues.Append("|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryCity + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryPostalCode + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryCounty + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryCountryName + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryCountryCode + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.DeliveryPhone + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.OrderCarrierId + "|*|");
            strbldrshipValues.Append(lstCustomerOrderBE.OrderCarrierName);

            if (!string.IsNullOrEmpty(strGiftCertValue))
            {
                string[] arr = strGiftCertValue.Split('|');
                if (HttpContext.Current.Session["amount"] != null)
                {
                    arr[2] = HttpContext.Current.Session["amount"].ToString().Replace(',','.');
                }
                strGiftCertValue = arr[0] + "|*|" + arr[2];
                nGiftCertificate = getXmlProductNode(strGiftCertificateHeaders, strGiftCertValue, "GiftCertificate", "GiftCertificates");
                ndOrder.InnerXml += nGiftCertificate.OuterXml;
            }
            ndShip = getXmlNode(strShipToFields, strbldrshipValues.ToString(), "ShipTo");
            ndOrder.InnerXml += ndShip.OuterXml;
            strbldrshipValues.Remove(0, strbldrshipValues.Length);

            if (!string.IsNullOrEmpty(strBillToFields))
            {
                #region ADDED BY SHRIGANESH 15 Sept 16
                if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
                {
                    List<InvoiceBE> lstSelectedEntityDetails = new List<InvoiceBE>();
                    lstSelectedEntityDetails = HttpContext.Current.Session["SelectedEntityDetails"] as List<InvoiceBE>;

                    if (lstSelectedEntityDetails != null)
                    {
                        if (HttpContext.Current.Session["IndeedInvoiceAccointID"] != null)
                        {
                            strBillToValues = strBillToValues + "|*|" + Convert.ToString(HttpContext.Current.Session["IndeedInvoiceAccointID"]);
                        }
                        else
                        {
                            strBillToValues = strBillToValues + "|*|" + Convert.ToString(lstSelectedEntityDetails[0].Customer_Id);
                        }                       
                        ndBill = getXmlNode(strBillToFields, strBillToValues, "BillTo");
                    }
                    else
                    {
                        strBillToValues = strBillToValues + "|*|" + "";
                        ndBill = getXmlNode(strBillToFields, strBillToValues, "BillTo");
                    }
                }
                #endregion
                #region Added By Hardik "3/MARCH/2017"
                else if (objStoreBE.StoreId == 471) //382 for local and 471 for Danskebank
                {
                    int ICurrencyId = GlobalFunctions.GetCurrencyId();
                    CustomUDFRegDataDanske lstRegData_DanskebankBE = new CustomUDFRegDataDanske();
                    lstRegData_DanskebankBE = HttpContext.Current.Session["RegData_DanskebankBE"] as CustomUDFRegDataDanske;
                    string strAccountId_CurrCode = string.Empty;
                    if (ICurrencyId == 3)
                    {
                        strAccountId_CurrCode = Convert.ToString(lstRegData_DanskebankBE.EUR);
                    }
                    if (ICurrencyId == 4)
                    {
                        strAccountId_CurrCode = Convert.ToString(lstRegData_DanskebankBE.DKK);
                    }
                    if (ICurrencyId == 5)
                    {
                        strAccountId_CurrCode = Convert.ToString(lstRegData_DanskebankBE.NOK);
                    }
                    if (ICurrencyId == 6)
                    {
                        strAccountId_CurrCode = Convert.ToString(lstRegData_DanskebankBE.SEK);
                    }
                    strBillToValues = strBillToValues + "|*|" + strAccountId_CurrCode;
                    ndBill = getXmlNode(strBillToFields, strBillToValues, "BillTo");
                } 
                #endregion
                #region ADDED BY SHRIGANESH TO PASS INVOICE ACCOUNTID IN INDEED EMPLOYEE ORDER XML
                else if (HttpContext.Current.Session["IndeedEmployeeInvoiceAccointID"] != "" && HttpContext.Current.Session["IndeedEmployeeSSO_User"] != null)
                {
                    strBillToFields = strBillToFields + "|*|" + "InvoiceAccountID";
                    strBillToValues = strBillToValues + "|*|" + Convert.ToString(HttpContext.Current.Session["IndeedEmployeeInvoiceAccointID"]);
                    ndBill = getXmlNode(strBillToFields, strBillToValues, "BillTo");
                }
                #endregion
                else
                {
                    ndBill = getXmlNode(strBillToFields, strBillToValues, "BillTo");
                }                
            }

            ndOrderLine = getXmlOrderNode(OrderItemFields, OrderItemValues, "OrderLine");

            if (!string.IsNullOrEmpty(strUDFields.Trim()) && !string.IsNullOrEmpty(strUDFValues.Trim()))
                ndUDF = getXmlProductNode(strUDFields.Trim(), strUDFValues.Trim(), "UserDefinedField", "UserDefinedFields");

            if (ndBill != null)
            {
                if (!string.IsNullOrEmpty(ndBill.OuterXml))
                    ndOrder.InnerXml += ndBill.OuterXml;
            }

            ndPowerWeaveID = xmlDoc.CreateNode(XmlNodeType.Attribute, "powerWeaveID", "");
            ndOrderDate = xmlDoc.CreateNode(XmlNodeType.Attribute, "orderDate", "");

            ndOrder.Attributes.SetNamedItem(ndPowerWeaveID);
            ndOrder.Attributes.SetNamedItem(ndOrderDate);

            ndOrderHeader.InnerXml = ndOrder.OuterXml;
            ndOrderHeader.InnerXml += ndOrderLine.OuterXml;

            if (ndUDF != null)
            {
                if (!string.IsNullOrEmpty(ndUDF.OuterXml))
                    ndOrderHeader.InnerXml += ndUDF.OuterXml;
            }

            getXmlFile(Convert.ToString(ndOrderHeader.OuterXml), Convert.ToString(lstCustomerOrderBE.CustomerOrderId));

            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                PWGlobalEcomm.OrderLive.Order order = new PWGlobalEcomm.OrderLive.Order();
                System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                    ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                order.Credentials = objNetworkCredentials;
                intOrderID = order.CreateOrder(ndOrderHeader);
            }
            else
            {
                PWGlobalEcomm.Order.Order order = new PWGlobalEcomm.Order.Order();
                System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(),
                    ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                order.Credentials = objNetworkCredentials;
                intOrderID = order.CreateOrder(ndOrderHeader);
            }
        }
        catch (Exception ex)
        {
            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In OrderService_OASIS", ex +"USER ID- "+lstUser.UserId+"USER EMAIL- "+lstUser.UserName, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            return -1;
        }
        return intOrderID;
    }

    public XmlNode getXmlOrderNode(string[] arrFields, string[] arrFieldValues, string strNode)
    {
        XmlElement ndXmlString = null;
        XmlNode ndOrderLine = null;
        StringBuilder strbldrXmlString = new StringBuilder();
        XmlDocument xmlDoc = null;
        int intNodeCounter = 0;
        try
        {
            xmlDoc = new XmlDocument();
            ndOrderLine = xmlDoc.CreateNode(XmlNodeType.Element, "OrderLines", "");
            intNodeCounter = arrFieldValues.Length / arrFields.Length;
            int intXml = 0;
            int intFields = 0;
            for (int intNode = 0; intNode < intNodeCounter; intNode++)
            {
                intFields = 0;
                for (; intXml < arrFieldValues.Length; intXml++)
                {
                    if (intFields >= arrFields.Length)
                        break;
                    if (arrFields[intFields].ToLower() == "country")
                    {
                        strbldrXmlString.Append("<");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append(" countryCode=\"");
                        strbldrXmlString.Append(arrFieldValues[intXml + 1]);
                        strbldrXmlString.Append("\">");
                        strbldrXmlString.Append(arrFieldValues[intNode]);
                        strbldrXmlString.Append("</");
                        strbldrXmlString.Append(arrFields[intXml]);
                        strbldrXmlString.Append(">");
                        continue;
                    }
                    if (arrFields[intFields].ToLower() == "countrycode")
                    {
                        continue;
                    }
                    if (arrFieldValues[intXml] == "")
                    {
                        strbldrXmlString.Append("<");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append("/>");
                    }
                    else
                    {
                        strbldrXmlString.Append("<");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append(">");
                        strbldrXmlString.Append(arrFieldValues[intXml]);
                        strbldrXmlString.Append("</");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append(">");
                    }
                    intFields++;
                }

                ndXmlString = xmlDoc.CreateElement(strNode);
                ndXmlString.SetAttribute("lineNumber", (intNode + 1).ToString());
                ndXmlString.InnerXml = strbldrXmlString.ToString();
                ndOrderLine.AppendChild(ndXmlString);

                if (strbldrXmlString.Length > 0)
                {
                    strbldrXmlString.Replace(strbldrXmlString.ToString(), "");
                }
            }
            return ndOrderLine;
        }
        catch (Exception ex)
        {
            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Faliure In getXmlOrderNode", ex.Message+"USER ID- "+lstUserBE.UserId+"USER EMAIL- "+lstUserBE.EmailId, "", "", false);
            return null;
        }
        finally
        {
        }
    }

    public XmlNode getXmlOrderNode(string strFields, string strFieldValues, string strNode)
    {
        string[] arrFields;
        string[] arrValues;
        try
        {
            arrFields = SplitXml(strFields);
            arrValues = SplitXml(strFieldValues);
            return getXmlOrderNode(arrFields, arrValues, strNode);
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
        }
    }

    public static void getXmlFile(string strxml, string lstrOrderId)
    {       
        string OrderLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\OrderXMLs";
        try
        {
            if (!Directory.Exists(OrderLogFolderPath))
                Directory.CreateDirectory(OrderLogFolderPath);

            if (Directory.Exists(OrderLogFolderPath))
            {                
                string date = string.Format("{0:dd}", DateTime.Now);
                string month = string.Format("{0:MMM}", DateTime.Now);
                string year = string.Format("{0:yyyy}", DateTime.Now);

                string folderName = month + year;
                string monthFolder = OrderLogFolderPath + "\\" + folderName;
                if (!Directory.Exists(monthFolder))
                    Directory.CreateDirectory(monthFolder);

                string OrderLogFileName = monthFolder +
                    "\\OrderXMLs" + date + month + ".txt";

                using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(OrderLogFileName, true))
                {
                    strmWriter.WriteLine("\r\n\r\n");
                    strmWriter.WriteLine("****************************Order Information - Order Id -- " + lstrOrderId + " **********Start*************** " + DateTime.Now.ToString() + "************\r\n\r\n");
                    strmWriter.WriteLine(strxml);
                    strmWriter.WriteLine("****************************Order Information**********End***************************\r\n\r\n");                    
                    strmWriter.WriteLine("-------------------------------------------------------------------------------");
                    strmWriter.Close();
                }
            }
            else
            {
                throw new DirectoryNotFoundException("Order log folder not found in the specified path");
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    public XmlNode getXmlNode(string strFields, string strFieldValues, string strNode)
    {
        string[] arrFields;
        string[] arrValues;
        try
        {
            arrFields = SplitXml(strFields);
            arrValues = SplitXml(strFieldValues);
            return getXmlNode(arrFields, arrValues, strNode);
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
        }
    }

    public XmlNode getXmlNode(string[] arrFields, string[] arrFieldValues, string strNode)
    {
        XmlNode ndXmlString = null;
        StringBuilder strbldrXmlString = new StringBuilder();
        XmlDocument xmlDoc = null;
        try
        {
            xmlDoc = new XmlDocument();
            for (int intXml = 0; intXml < arrFields.Length; intXml++)
            {
                if (arrFields[intXml].ToLower() == "country")
                {
                    strbldrXmlString.Append("<");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(" countryCode=\"");
                    strbldrXmlString.Append(arrFieldValues[intXml + 1]);
                    strbldrXmlString.Append("\">");
                    strbldrXmlString.Append(arrFieldValues[intXml]);
                    strbldrXmlString.Append("</");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(">");
                    continue;
                }
                if (arrFields[intXml].ToLower() == "countrycode")
                {
                    continue;
                }
                if (arrFieldValues[intXml] == "")
                {
                    strbldrXmlString.Append("<");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append("/>");
                }
                else
                {
                    strbldrXmlString.Append("<");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(">");
                    strbldrXmlString.Append(arrFieldValues[intXml]);
                    strbldrXmlString.Append("</");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(">");
                }
            }
            ndXmlString = xmlDoc.CreateNode(XmlNodeType.Element, strNode, "");
            ndXmlString.InnerXml = strbldrXmlString.ToString();
            return ndXmlString;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
        }
    }

    public DataTable getTaxDetails(string CustomerContactID, string strTaxNumber, string strDestinationCountry, string strCountryCode, string strFrieght,
        ArrayList ProductValues, long lngShoppingID)
    {
        try
        {
            objStoreBE = StoreBL.GetStoreDetails();
            string strHeaderTaxFields = "";
            string strHeaderTaxValues = "";

            if (objStoreBE.StoreFeatures.FirstOrDefault(s => s.FeatureName == "IA_InvoiceAccount").FeatureValues[0].IsEnabled)
            {
                if (string.IsNullOrEmpty(strTaxNumber))
                {
                    strHeaderTaxFields = "CustomerContactID";
                    strHeaderTaxValues = CustomerContactID;
                }
                else
                {
                    strHeaderTaxFields = "CustomerContactID|*|TaxNumber";
                    strHeaderTaxValues = CustomerContactID + "|*|" + strTaxNumber + "|*|";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strTaxNumber))
                {
                    strHeaderTaxFields = "CustomerContactID";
                    strHeaderTaxValues = CustomerContactID;
                }
                else
                {
                    strHeaderTaxFields = "CustomerContactID|*|TaxNumber";
                    strHeaderTaxValues = CustomerContactID + "|*|" + strTaxNumber + "|*|";
                }
            }

            Exceptions.WriteInfoLog("Inside GetTaxDetails()");

            /*FreightSourceCountryMaster*/
            FreightSourceCountryMasterBE objFreightSourceCountryMaster = FreightManagementBL.GetFreightSourceCountryMaster(strCountryCode);
            string fscountryName = objFreightSourceCountryMaster.FreightSourceCountryName;
            string fsCountryCode = objFreightSourceCountryMaster.FreightSourceCountryCode;

            string strDestinationFields = "DestinationCountry|*|countryCode|*|Freight|*|FreightSourceCountry|*|countryCode";
            string strDestinationValues = SecurityElement.Escape(strDestinationCountry) + "|*|" + strCountryCode + "|*|" + strFrieght + "|*|" + fscountryName + "|*|" + fsCountryCode;

            string strProductsFields = "ProductID|*|TotalSales|*|SourceCountry|*|countryCode";
            string strProductsValues = GetProducts(ProductValues);

            XmlDocument xmlDoc = new XmlDocument();
            XmlNode ndDestinations = xmlDoc.CreateElement("Destinations");
            XmlNode ndHeaderXmlNode, ndDestinationxmlNode, ndProductsxmlNode, ndRtnNode;

            ndHeaderXmlNode = getTaxXmlNode(strHeaderTaxFields, strHeaderTaxValues, "Tax");
            ndDestinationxmlNode = getTaxXmlNode(strDestinationFields, strDestinationValues, "Destination");
            ndProductsxmlNode = getXmlProductNode(strProductsFields, strProductsValues, "Product", "Products");
            ndDestinations.InnerXml += ndDestinationxmlNode.OuterXml;
            ndDestinations["Destination"].InnerXml += ndProductsxmlNode.OuterXml;

            ndHeaderXmlNode.InnerXml += ndDestinations.OuterXml;
            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                Exceptions.WriteInfoLog("Before Calling LIVE TAX Web service()");
                PWGlobalEcomm.TaxLive.Tax objTax = new PWGlobalEcomm.TaxLive.Tax();
                System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                objTax.Credentials = objNetworkCredentials;
                WriteTAXLog(Convert.ToString("TAX REQUEST : " + ndHeaderXmlNode.OuterXml));
                ndRtnNode = objTax.CalculateTaxV(ndHeaderXmlNode);
                WriteTAXLog(Convert.ToString("TAX RESPONSE : " + ndRtnNode.OuterXml));
                Exceptions.WriteInfoLog("After Calling LIVE TAX Web service()");
            }
            else
            {
                Exceptions.WriteInfoLog("Before Calling BETA TAX Web service()");
                PWGlobalEcomm.Tax.Tax objTax = new PWGlobalEcomm.Tax.Tax();
                System.Net.NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                objTax.Credentials = objNetworkCredentials;
                WriteTAXLog(Convert.ToString("TAX REQUEST : " + ndHeaderXmlNode.OuterXml));
                ndRtnNode = objTax.CalculateTaxV(ndHeaderXmlNode);
                WriteTAXLog(Convert.ToString("TAX RESPONSE : " + ndRtnNode.OuterXml));
                Exceptions.WriteInfoLog("After Calling BETA TAX Web service()");
            }
            DataTable dttblTaxDetails = new DataTable();
            dttblTaxDetails = SplitXml_Tax(ndRtnNode);
            Exceptions.WriteInfoLog("Exiting GetTaxDetails()");
            return dttblTaxDetails;
        }
        catch (Exception ex)
        {
            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In getTaxDetails", ex +"USER ID- "+lstUserBE.UserId+"USER EMAIL- "+lstUserBE.EmailId, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
    }

    public DataTable SplitXml_Tax(XmlNode ndNode)
    {
        DataTable dttblTax = new DataTable();
        try
        {
            dttblTax.Columns.Add("Freight");
            dttblTax.Columns.Add("FreightTax");
            dttblTax.Columns.Add("FreightTaxRate");
            dttblTax.Columns.Add("ProductID");
            dttblTax.Columns.Add("TotalSales");
            dttblTax.Columns.Add("TotalSalesTax");
            dttblTax.Columns.Add("TotalSalesTaxRate");
            dttblTax.Columns.Add("VendorTaxCode");
            dttblTax.Columns.Add("VatInvoiceText");

            XmlNodeList ndList = ndNode.ChildNodes;
            DataRow drTax = null;
            ndList = ndNode["Destinations"]["Destination"]["Products"].ChildNodes;
            for (int intNdlist = 0; intNdlist < ndList.Count; intNdlist++)
            {
                drTax = dttblTax.NewRow();

                if (ndNode["Destinations"]["Destination"]["Freight"] != null)
                    drTax["Freight"] = ndNode["Destinations"]["Destination"]["Freight"].InnerText;
                else
                    drTax["Freight"] = 0;

                drTax["FreightTax"] = ndNode["Destinations"]["Destination"]["FreightTax"].InnerText;
                drTax["FreightTaxRate"] = ndNode["Destinations"]["Destination"]["FreightTaxRate"].InnerText;
                drTax["ProductID"] = ndList[intNdlist]["ProductID"].InnerText;
                drTax["TotalSales"] = ndList[intNdlist]["TotalSales"].InnerText;
                drTax["TotalSalesTax"] = ndList[intNdlist]["TotalSalesTax"].InnerText;
                drTax["TotalSalesTaxRate"] = ndList[intNdlist]["TotalSalesTaxRate"].InnerText;

                if (ndNode["VendorTaxCode"] != null)
                    drTax["VendorTaxCode"] = ndNode["VendorTaxCode"].InnerText;
                else
                    drTax["VendorTaxCode"] = string.Empty;

                if (ndList[intNdlist]["VatInvoiceText"] != null)

                    drTax["VatInvoiceText"] = ndList[intNdlist]["VatInvoiceText"].InnerText;
                else
                    drTax["VatInvoiceText"] = string.Empty;

                dttblTax.Rows.Add(drTax);
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return dttblTax;
    }

    public XmlNode getTaxXmlNode(string strFields, string strFieldValues, string strNode)
    {
        string[] arrFields;
        string[] arrValues;
        try
        {
            arrFields = SplitXml(strFields);
            arrValues = SplitXml(strFieldValues);
            return getXmlTaxNode(arrFields, arrValues, strNode);
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
        finally { }
    }

    public XmlNode getXmlProductNode(string strFields, string strFieldValues, string strNode, string strParentNode)
    {
        string[] arrFields;
        string[] arrValues;
        try
        {
            arrFields = SplitXml(strFields);
            arrValues = SplitXml(strFieldValues);
            return getXmlProductNode(arrFields, arrValues, strNode, strParentNode);
        }
        catch (Exception ex)
        {
            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Faliure In getXmlProductNode", ex.Message, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
        finally
        {
        }
    }

    public XmlNode getXmlProductNode(string[] arrFields, string[] arrFieldValues, string strNode, string strParentNode)
    {
        XmlElement ndXmlString = null;
        XmlNode ndOrderLine = null;
        StringBuilder strbldrXmlString = new StringBuilder();
        XmlDocument xmlDoc = null;
        int intNodeCounter = 0;
        try
        {
            xmlDoc = new XmlDocument();
            ndOrderLine = xmlDoc.CreateNode(XmlNodeType.Element, strParentNode, "");
            intNodeCounter = arrFieldValues.Length / arrFields.Length;
            int intXml = 0;
            int intFields = 0;
            for (int intNode = 0; intNode < intNodeCounter; intNode++)
            {
                intFields = 0;
                for (; intXml < arrFieldValues.Length; intXml++)
                {

                    if (intFields >= arrFields.Length)
                        break;

                    if (arrFields[intFields].ToLower() == "country")
                    {
                        strbldrXmlString.Append("<");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append(" countryCode=\"");
                        strbldrXmlString.Append(arrFieldValues[intXml + 1]);
                        strbldrXmlString.Append("\">");
                        strbldrXmlString.Append(arrFieldValues[intNode]);
                        strbldrXmlString.Append("</");
                        strbldrXmlString.Append(arrFields[intXml]);
                        strbldrXmlString.Append(">");
                        continue;
                    }

                    if (arrFields[intFields].ToLower() == "sourcecountry")
                    {
                        strbldrXmlString.Append("<");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append(" countryCode=\"");
                        strbldrXmlString.Append(arrFieldValues[intXml + 1]);
                        strbldrXmlString.Append("\">");
                        strbldrXmlString.Append(arrFieldValues[intXml]);
                        strbldrXmlString.Append("</");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append(">");
                        intFields++;
                        continue;
                    }

                    if (arrFields[intFields].ToLower() == "countrycode")
                    {
                        intFields++;
                        continue;
                    }
                    if (arrFieldValues[intXml] == "")
                    {
                        strbldrXmlString.Append("<");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append("/>");
                    }
                    else
                    {
                        strbldrXmlString.Append("<");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append(">");
                        strbldrXmlString.Append(arrFieldValues[intXml]);
                        strbldrXmlString.Append("</");
                        strbldrXmlString.Append(arrFields[intFields]);
                        strbldrXmlString.Append(">");
                    }
                    intFields++;
                }

                ndXmlString = xmlDoc.CreateElement(strNode);
                ndXmlString.InnerXml = strbldrXmlString.ToString();
                ndOrderLine.AppendChild(ndXmlString);

                if (strbldrXmlString.Length > 0)
                {
                    strbldrXmlString.Replace(strbldrXmlString.ToString(), "");
                }
            }
            return ndOrderLine;
        }
        catch (Exception ex)
        {
            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Faliure In getXmlProductNode", ex.Message+"USER ID- "+lstUserBE.UserId+"USER EMAIL- "+lstUserBE.EmailId, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
        finally
        {
        }
    }

    public XmlNode getXmlTaxNode(string[] arrFields, string[] arrFieldValues, string strNode)
    {
        XmlNode ndXmlString = null;
        StringBuilder strbldrXmlString = new StringBuilder();
        XmlDocument xmlDoc = null;
        try
        {
            xmlDoc = new XmlDocument();
            for (int intXml = 0; intXml < arrFields.Length; intXml++)
            {
                if (arrFields[intXml].ToLower() == "destinationcountry")
                {
                    strbldrXmlString.Append("<");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(" countryCode=\"");
                    strbldrXmlString.Append(arrFieldValues[intXml + 1]);
                    strbldrXmlString.Append("\">");
                    strbldrXmlString.Append(arrFieldValues[intXml]);
                    strbldrXmlString.Append("</");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(">");
                    continue;
                }
                if (arrFields[intXml].ToLower() == "sourcecountry")
                {
                    strbldrXmlString.Append("<");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(" countryCode=\"");
                    strbldrXmlString.Append(arrFieldValues[intXml + 1]);
                    strbldrXmlString.Append("\">");
                    strbldrXmlString.Append(arrFieldValues[intXml]);
                    strbldrXmlString.Append("</");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(">");
                    continue;
                }
                if (arrFields[intXml].ToLower() == "freightsourcecountry")
                {
                    strbldrXmlString.Append("<");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(" countryCode=\"");
                    strbldrXmlString.Append(arrFieldValues[intXml + 1]);
                    strbldrXmlString.Append("\">");
                    strbldrXmlString.Append(arrFieldValues[intXml]);
                    strbldrXmlString.Append("</");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(">");
                    continue;
                }
                if (arrFields[intXml].ToLower() == "countrycode")
                {
                    continue;
                }
                if (arrFieldValues[intXml] == "")
                {
                    strbldrXmlString.Append("<");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append("/>");
                }
                else
                {
                    strbldrXmlString.Append("<");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(">");
                    strbldrXmlString.Append(arrFieldValues[intXml]);
                    strbldrXmlString.Append("</");
                    strbldrXmlString.Append(arrFields[intXml]);
                    strbldrXmlString.Append(">");
                }
            }
            ndXmlString = xmlDoc.CreateNode(XmlNodeType.Element, strNode, "");
            ndXmlString.InnerXml = strbldrXmlString.ToString();
            return ndXmlString;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
        finally
        {
        }
    }

    private string[] SplitXml(string strNodes)
    {
        try
        {
            string[] arrDelim = { "|*|" };
            string strWords = strNodes;
            string[] arrSplit = null;
            arrSplit = strWords.Split(arrDelim, StringSplitOptions.None);

            return arrSplit;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return null;
        }
        finally { }
    }

    public string GetProducts(ArrayList arrOrderItems)
    {
        bool bNumeric = true;
        string strOrderItems = "";
        int intFieldCount = 0;
        for (int intItems = 0; intItems < arrOrderItems.Count; intItems++)
        {
            strOrderItems += arrOrderItems[intItems] + "|*|";
        }
        return strOrderItems;
    }

    /// <summary>
    /// Method to write application wide TAX log. 
    /// </summary>
    /// <param name="strXML">String object</param>
    public static void WriteTAXLog(String strXML)
    {
        string TAXLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\TAXLogs";
        try
        {
            if (!Directory.Exists(TAXLogFolderPath))
                Directory.CreateDirectory(TAXLogFolderPath);

            if (Directory.Exists(TAXLogFolderPath))
            {
                //Create month wise exception log file.
                string date = string.Format("{0:dd}", DateTime.Now);
                string month = string.Format("{0:MMM}", DateTime.Now);
                string year = string.Format("{0:yyyy}", DateTime.Now);

                string folderName = month + year; //Feb2013
                string monthFolder = TAXLogFolderPath + "\\" + folderName;
                if (!Directory.Exists(monthFolder))
                    Directory.CreateDirectory(monthFolder);

                string TAXLogFileName = monthFolder +
                    "\\TAXLogs_" + date + month + ".txt"; //TAXLogs_04Feb.txt

                using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(TAXLogFileName, true))
                {
                    strmWriter.WriteLine("On " + DateTime.Now.ToString());
                    strmWriter.WriteLine("XML: " + strXML);
                    strmWriter.WriteLine("-------------------------------------------------------------------------------");
                }
            }
            else
            {
                throw new DirectoryNotFoundException("Tax log folder not found in the specified path");
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

}

//--- PRAJWAL 
public class RegisterCustomer_OASIS
{
    public static int BuildCustContact_OASIS(UserBE userInstance)
    {
        int intCustID = 0;
        try
        {
            XDocument xUserRegistrationDocument = new XDocument(new XElement("CustomerContact", new XElement("FirstName", WebUtility.HtmlDecode(userInstance.FirstName))
                                                                                              , new XElement("LastName", WebUtility.HtmlDecode(userInstance.LastName))
                                                                                              , new XElement("CompanyName", WebUtility.HtmlDecode(userInstance.PredefinedColumn2))
                                                                                              , new XElement("Address1", WebUtility.HtmlDecode(userInstance.PredefinedColumn3))
                                                                                              , new XElement("Address2", WebUtility.HtmlDecode(userInstance.PredefinedColumn4))
                                                                                              , new XElement("Address3", string.Empty)
                                                                                              , new XElement("City", WebUtility.HtmlDecode(userInstance.PredefinedColumn5))
                                                                                              , new XElement("PostCode", WebUtility.HtmlDecode(userInstance.PredefinedColumn7))
                                                                                              , new XElement("County", WebUtility.HtmlDecode(userInstance.PredefinedColumn6))
                                                                                              , new XElement("Country", userInstance.CountryName, new XAttribute("countryCode", ""))//Country Code is not , new XElement("Country", userInstance.CountryName, new XAttribute("countryCode", userInstance.CustomColumn8))
                                                                                              , new XElement("Telephone", WebUtility.HtmlDecode(userInstance.PredefinedColumn9))
                                                                                              , new XElement("TelephoneExtension", string.Empty)
                                                                                              , new XElement("Fax", string.Empty)
                                                                                              , new XElement("Email", userInstance.EmailId)
                                                                                              , new XElement("SourceCode", string.Empty)
                                                                                              , new XElement("SourceCodeID", userInstance.SourceCodeId)));

            try
            {
                getXmlFileCustomer(Convert.ToString(xUserRegistrationDocument), Convert.ToString(userInstance.UserId));
            }
            catch (Exception) { }
            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                PWGlobalEcomm.CustomerLive.Customer objCustomer = new PWGlobalEcomm.CustomerLive.Customer();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                objCustomer.Credentials = objNetworkCredentials;
                intCustID = objCustomer.CreateContact(new XmlDocument().ReadNode(xUserRegistrationDocument.Root.CreateReader()), userInstance.DefaultCustId);
            }
            else
            {
                PWGlobalEcomm.Customer.Customer objCustomer = new PWGlobalEcomm.Customer.Customer();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                objCustomer.Credentials = objNetworkCredentials;
                intCustID = objCustomer.CreateContact(new XmlDocument().ReadNode(xUserRegistrationDocument.Root.CreateReader()), userInstance.DefaultCustId);

            }
            return intCustID;
        }
        catch (Exception ex)
        {
            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In BuildCustContact_OASIS", ex +"USER ID- "+lstUserBE.UserId+"USER EMAIL- "+lstUserBE.EmailId, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            return intCustID;
        }
    }
    #region Create customer webservice for indeed


    public static int BuildCreateCustomer_OASIS(UserBE userInstance, string InvoiceAccountID, string PaymantTypeID)
    {
        int intCustID = 0;
        int intCustomerID = 0;        
        try
        {
            XDocument xUserRegistrationDocument = new XDocument(new XElement("Customer", new XElement("DivisionID", WebUtility.HtmlDecode(Convert.ToString(userInstance.DivisionID)))
            , new XElement("Currency", WebUtility.HtmlDecode(userInstance.CurrencyCode))
            , new XElement("SourceCode", string.Empty)
            , new XElement("SourceCodeID", userInstance.SourceCodeId)
            , new XElement("MailOptOut", string.Empty)
            , new XElement("FirstName", WebUtility.HtmlDecode(userInstance.FirstName))
            , new XElement("LastName", WebUtility.HtmlDecode(userInstance.LastName))
            , new XElement("CompanyName", WebUtility.HtmlDecode(userInstance.PredefinedColumn2))
            , new XElement("Address1", WebUtility.HtmlDecode(userInstance.PredefinedColumn3))
            , new XElement("Address2", WebUtility.HtmlDecode(userInstance.PredefinedColumn4))
            , new XElement("Address3", WebUtility.HtmlDecode(userInstance.CustomColumn30))
            , new XElement("City", WebUtility.HtmlDecode(userInstance.PredefinedColumn5))
            , new XElement("PostCode", WebUtility.HtmlDecode(userInstance.PredefinedColumn7))
            , new XElement("County", WebUtility.HtmlDecode(userInstance.PredefinedColumn6))
            , new XElement("Country", userInstance.CountryName, new XAttribute("countryCode", ""))//Country Code is not , new XElement("Country", userInstance.CountryName, new XAttribute("countryCode", userInstance.CustomColumn8))		
            , new XElement("Telephone", WebUtility.HtmlDecode(userInstance.PredefinedColumn9))
            , new XElement("TelephoneExtension", string.Empty)
            , new XElement("Fax", string.Empty)
            , new XElement("Email", userInstance.EmailId)
            ,new XElement("Alias",userInstance.CustomColumn2)
            , new XElement("PaymentTypeID", PaymantTypeID)
            , new XElement("BillTo", new XElement("FirstName", WebUtility.HtmlDecode(userInstance.FirstName))
            , new XElement("LastName", WebUtility.HtmlDecode(userInstance.LastName))
            , new XElement("CompanyName", WebUtility.HtmlDecode(userInstance.PredefinedColumn2))
            , new XElement("Address1", WebUtility.HtmlDecode(userInstance.PredefinedColumn3))
            , new XElement("Address2", WebUtility.HtmlDecode(userInstance.PredefinedColumn4))
            , new XElement("Address3", string.Empty)
            , new XElement("City", WebUtility.HtmlDecode(userInstance.PredefinedColumn5))
            , new XElement("PostCode", WebUtility.HtmlDecode(userInstance.PredefinedColumn7))
            , new XElement("County", WebUtility.HtmlDecode(userInstance.PredefinedColumn6))
            , new XElement("Country", userInstance.CountryName, new XAttribute("countryCode", ""))//Country Code is not , new XElement("Country", userInstance.CountryName, new XAttribute("countryCode", userInstance.CustomColumn8))		
            , new XElement("Telephone", WebUtility.HtmlDecode(userInstance.PredefinedColumn9))
            , new XElement("TelephoneExtension", string.Empty)
            , new XElement("Fax", string.Empty)
            , new XElement("Email", userInstance.EmailId)
            , new XElement("InvoiceAccountID",InvoiceAccountID)
            )
            ));

            try
            {
                getXmlFileCustomer(Convert.ToString(xUserRegistrationDocument), Convert.ToString(userInstance.UserId));
            }
            catch (Exception) { }
            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                PWGlobalEcomm.CustomerLive.Customer objCustomer = new PWGlobalEcomm.CustomerLive.Customer();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                objCustomer.Credentials = objNetworkCredentials;
                intCustID = objCustomer.CreateCustomer(new XmlDocument().ReadNode(xUserRegistrationDocument.Root.CreateReader()),out intCustomerID);
            }
            else
            {
                PWGlobalEcomm.Customer.Customer objCustomer = new PWGlobalEcomm.Customer.Customer();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                objCustomer.Credentials = objNetworkCredentials;
                intCustID = objCustomer.CreateCustomer(new XmlDocument().ReadNode(xUserRegistrationDocument.Root.CreateReader()), out intCustomerID);
            }
            return intCustomerID;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return intCustomerID;
        }
    }
    #endregion

    //Added By Hardik For Marketing Opt in
    #region Added By Hardik For Marketing Opt - "31/JAN/2017"

    public static string IsMarketingInsertContact(UserBE userInstance)
    {
        StoreBE objStoreBE = StoreBL.GetStoreDetails();
        Exceptions.WriteInfoLog("Inside IsMarketingInsertContact() - Communigator");
        string strIsMkt = string.Empty;
        int CommunigatorGroupId = Convert.ToInt16(objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CommunigatorGroupId").FeatureValues[0].FeatureDefaultValue);
        try
        {
            XDocument xIsMarketingDocument = new XDocument(new XElement("sbCommContent", new XElement("sbBOName", "Person"), new XElement("sbBOAttrs", new XElement("EmailLogin", userInstance.EmailId)
                                                                                                  , new XElement("FirstName", WebUtility.HtmlDecode(userInstance.FirstName))
                                                                                                  , new XElement("LastName", WebUtility.HtmlDecode(userInstance.LastName))
                                                                                                  , new XElement("CompanyName", WebUtility.HtmlDecode(userInstance.PredefinedColumn2))
                                                                                                  , new XElement("Country", userInstance.CountryName)
                                                                                                  , new XElement("County", WebUtility.HtmlDecode(userInstance.PredefinedColumn6))
                                                                                                  , new XElement("Currency", userInstance.CurrencySymbol))
                                                                                                  , new XElement("sbContacts_RV", "")));

            try
            {
                getXmlFileIsMarketingInsert(Convert.ToString(xIsMarketingDocument));
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }

            PWGlobalEcomm.IsMarketingLive.SDK objMarketing = new PWGlobalEcomm.IsMarketingLive.SDK();

            var auth = new AuthHeader
            {
                Username = ConfigurationManager.AppSettings["CommunigatorUsername"].ToString(),
                Password = ConfigurationManager.AppSettings["CommunigatorPassword"].ToString()
            };
            objMarketing.Url = "https://www.communigatormail.co.uk/brandadditionlz/SDK.asmx";
            objMarketing.AuthHeaderValue = auth;

            strIsMkt = objMarketing.InsertContact(Convert.ToString(xIsMarketingDocument));

            if (userInstance.IsMarketing == true)
            {
                if (Convert.ToInt32(strIsMkt) > 0)
                {
                    Exceptions.WriteInfoLog("IsMarketing - true // AddGroupInclusionExclusionContact()");
                    objMarketing.AddGroupInclusionExclusionContact(CommunigatorGroupId, Convert.ToInt32(strIsMkt), true);
                }
            }
            else
            {
                Exceptions.WriteInfoLog("IsMarketing - false // AddGroupInclusionExclusionContact()");
                objMarketing.AddGroupInclusionExclusionContact(CommunigatorGroupId, Convert.ToInt32(strIsMkt), false);
            }
            Exceptions.WriteInfoLog("End of IsMarketingInsertContact() - Communigator" + strIsMkt);

            return strIsMkt;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return strIsMkt;
        }
    }

    public static string IsMarketingUpdateContact(UserBE userInstance)
    {
        StoreBE objStoreBE = StoreBL.GetStoreDetails();
        Exceptions.WriteInfoLog("Inside IsMarketingUpdateContact() - Communigator");
        string strIsMkt = string.Empty;
        int CommunigatorGroupId = Convert.ToInt16(objStoreBE.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CommunigatorGroupId").FeatureValues[0].FeatureDefaultValue);
        try
        {
            XDocument xIsMarketingDocument = new XDocument(new XElement("sbCommContent", new XElement("sbBOName", "Person"), new XElement("sbBOAttrs", new XElement("EmailLogin", userInstance.EmailId)
                                                                                                  , new XElement("FirstName", WebUtility.HtmlDecode(userInstance.FirstName))
                                                                                                  , new XElement("LastName", WebUtility.HtmlDecode(userInstance.LastName))
                                                                                                  , new XElement("CompanyName", WebUtility.HtmlDecode(userInstance.PredefinedColumn2))
                                                                                                  , new XElement("Country", userInstance.CountryName)
                                                                                                  , new XElement("County", WebUtility.HtmlDecode(userInstance.PredefinedColumn6))
                                                                                                  , new XElement("Currency", userInstance.CurrencySymbol))
                                                                                                  , new XElement("sbContacts_RV", "")));

            try
            {
                getXmlFileIsMarketingUpdate(Convert.ToString(xIsMarketingDocument));
            }
            catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }

            PWGlobalEcomm.IsMarketingLive.SDK objMarketing = new PWGlobalEcomm.IsMarketingLive.SDK();

            var auth = new AuthHeader
            {
                Username = ConfigurationManager.AppSettings["CommunigatorUsername"].ToString(),
                Password = ConfigurationManager.AppSettings["CommunigatorPassword"].ToString()
            };
            objMarketing.Url = "https://www.communigatormail.co.uk/brandadditionlz/SDK.asmx";
            objMarketing.AuthHeaderValue = auth;

            strIsMkt = objMarketing.UpdateContact(Convert.ToString(xIsMarketingDocument));

            if (userInstance.IsMarketing == true)
            {
                if (Convert.ToInt32(strIsMkt) > 0)
                {
                    Exceptions.WriteInfoLog("IsMarketing - true // AddGroupInclusionExclusionContact()");
                    objMarketing.AddGroupInclusionExclusionContact(CommunigatorGroupId, Convert.ToInt32(strIsMkt), true);
                }
            }
            else
            {
                Exceptions.WriteInfoLog("IsMarketing - false // AddGroupInclusionExclusionContact()");
                objMarketing.AddGroupInclusionExclusionContact(CommunigatorGroupId, Convert.ToInt32(strIsMkt), false);
            }
            Exceptions.WriteInfoLog("End of IsMarketingUpdateContact() - Communigator" + strIsMkt);
            return strIsMkt;
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
            return strIsMkt;
        }
    }

    public static void getXmlFileIsMarketingInsert(string strxml)
    {
        try
        {
            if (!Directory.Exists(GlobalFunctions.GetPhysicalFolderPath() + "XML\\IsMarketingXMLs"))
            {
                Directory.CreateDirectory(GlobalFunctions.GetPhysicalFolderPath() + "\\XML\\IsMarketingXMLs");
            }

            TextWriter txtwr = new StreamWriter(GlobalFunctions.GetPhysicalFolderPath() + "\\XML\\IsMarketingXMLs\\IsMarketingInsert.txt", true);
            txtwr.WriteLine("\r\n\r\n");
            txtwr.WriteLine("************** IsMarketing Start *************** " + DateTime.Now.ToString() + " ************\r\n\r\n");
            txtwr.WriteLine(strxml);
            txtwr.WriteLine("**************************** IsMarketing End ***************************\r\n\r\n");
            txtwr.Close();
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    public static void getXmlFileIsMarketingUpdate(string strxml)
    {
        try
        {
            if (!Directory.Exists(GlobalFunctions.GetPhysicalFolderPath() + "XML\\IsMarketingXMLs"))
            {
                Directory.CreateDirectory(GlobalFunctions.GetPhysicalFolderPath() + "\\XML\\IsMarketingXMLs");
            }

            TextWriter txtwr = new StreamWriter(GlobalFunctions.GetPhysicalFolderPath() + "\\XML\\IsMarketingXMLs\\IsMarketingUpdate.txt", true);
            txtwr.WriteLine("\r\n\r\n");
            txtwr.WriteLine("************** IsMarketing Start *************** " + DateTime.Now.ToString() + " ************\r\n\r\n");
            txtwr.WriteLine(strxml);
            txtwr.WriteLine("**************************** IsMarketing End ***************************\r\n\r\n");
            txtwr.Close();
        }
        catch (Exception ex) { Exceptions.WriteExceptionLog(ex); }
    }

    #endregion

    public static void getXmlFileCustomer(string strxml, string strUserID)
    {
        try
        {
            if (!Directory.Exists(GlobalFunctions.GetPhysicalFolderPath() + "XML\\RegisterXMLs"))
            {
                Directory.CreateDirectory(GlobalFunctions.GetPhysicalFolderPath() + "XML\\RegisterXMLs");
            }
            TextWriter txtwr = new StreamWriter(GlobalFunctions.GetPhysicalFolderPath() + "XML\\RegisterXMLs\\Register.txt", true);
            txtwr.WriteLine("\r\n\r\n");
            txtwr.WriteLine("**************************** Register Id -- " + strUserID + " **********Start*************** " + DateTime.Now.ToString() + "************\r\n\r\n");
            txtwr.WriteLine(strxml);
            txtwr.WriteLine("**************************** Register End***************************\r\n\r\n");
            txtwr.Close();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
}

public class UserOrder_OASIS
{
    public static SalesOrders GetAllUserOrders(int OasisContactId)
    {
        SalesOrders objOrder;
        XmlNode orderNode;
        try
        {
            objOrder = new SalesOrders();
            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                PWGlobalEcomm.OrderLive.Order obj = new PWGlobalEcomm.OrderLive.Order();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                orderNode = obj.GetSalesOrdersForContact(OasisContactId);
                objOrder = SalesOrders.FromXmlString(orderNode.OuterXml.ToString());
            }
            else
            {
                PWGlobalEcomm.Order.Order obj = new PWGlobalEcomm.Order.Order();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                orderNode = obj.GetSalesOrdersForContact(OasisContactId);
                objOrder = SalesOrders.FromXmlString(orderNode.OuterXml.ToString());
            }
            return objOrder;
        }
        catch (Exception ex)
        {
            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In GetAllUserOrders", ex + "USER ID- " + lstUserBE.UserId + "USER EMAIL- " + lstUserBE.EmailId, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            objOrder = new SalesOrders(); 
            return objOrder; 
        }
    }

    /// <summary>
    /// Added By Hardik
    /// </summary>
    /// <param name="OasisContactId"></param>
    /// <param name="StartDate"></param>
    /// <returns>objOrderStartDate</returns>
    public static SalesOrders GetAllUserOrderStartDate(Int32 OasisContactId, DateTime StartDate)
    {
        SalesOrders objOrderStartDate;
        XmlNode orderNodeStartDate;
        try
        {
            objOrderStartDate = new SalesOrders();
            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                PWGlobalEcomm.OrderLive.Order obj = new PWGlobalEcomm.OrderLive.Order();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                orderNodeStartDate = obj.GetSalesOrdersForContactFromStartDate(OasisContactId, StartDate.Date);

                /*Changes done by Snehal - For Error Node*/
                if (!orderNodeStartDate.OuterXml.Contains("Error"))
                {
                    objOrderStartDate = SalesOrders.FromXmlString(orderNodeStartDate.OuterXml.ToString());
                }
                /*END*/
            }
            else
            {
                PWGlobalEcomm.Order.Order obj = new PWGlobalEcomm.Order.Order();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                orderNodeStartDate = obj.GetSalesOrdersForContactFromStartDate(OasisContactId, StartDate);
                /*Changes done by Snehal - For Error Node*/
                if (!orderNodeStartDate.OuterXml.Contains("Error"))
                {
                    objOrderStartDate = SalesOrders.FromXmlString(orderNodeStartDate.OuterXml.ToString());
                }
                /*END*/
            }
            return objOrderStartDate;
        }
        catch (Exception ex)
        {

            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In GetAllUserOrderStartDate", ex + "USER ID- " + lstUserBE.UserId + "USER EMAIL- " + lstUserBE.EmailId, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            objOrderStartDate = new SalesOrders();
            return objOrderStartDate;
        }
    }
}

public class CustomerOrder_OASIS
{
    public static CustomerSalesOrder GetUserOrder(int ContactOrderId)
    {
        CustomerSalesOrder objOrder;
        XmlNode orderNode;
        try
        {
            objOrder = new CustomerSalesOrder();
            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                PWGlobalEcomm.OrderLive.Order obj = new PWGlobalEcomm.OrderLive.Order();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                orderNode = obj.GetSalesOrderDetails(ContactOrderId);
            }
            else
            {
                PWGlobalEcomm.Order.Order obj = new PWGlobalEcomm.Order.Order();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                orderNode = obj.GetSalesOrderDetails(ContactOrderId);
            }

            objOrder = CustomerSalesOrder.FromXmlString(orderNode.OuterXml.ToString());
            return objOrder;
        }
        catch (Exception ex)
        {
            UserBE lstUserBE;
            lstUserBE = HttpContext.Current.Session["User"] as UserBE;
            GlobalFunctions.SendEmail(GlobalFunctions.GetSetting("ExceptionEmail"), ConfigurationManager.AppSettings["From_Email"], "", "", "", "Failure In GetUserOrder", ex + "USER ID- " + lstUserBE.UserId + "USER EMAIL- " + lstUserBE.EmailId, "", "", false);
            Exceptions.WriteExceptionLog(ex);
            objOrder = new CustomerSalesOrder();
            return objOrder;
        }
    }
}


//Added By Snehal - For Indeed - 09 03 2017
public class Budget_OASIS
{
    public static Budgets GetBudgetDetails(string BudgetCode, int DivisionId, string CurrSymbol)
    {
        Budgets objBudget;
        XmlNode BudgetNode;
        try
        {
            Exceptions.WriteInfoLog("public static Budgets GetBudgetDetails(string BudgetCode, int DivisionId, string CurrSymbol)");
            objBudget = new Budgets();
            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                Exceptions.WriteInfoLog("Before Calling LIVE BUDGET Web service()");
                PWGlobalEcomm.BudgetLive.Budget obj = new PWGlobalEcomm.BudgetLive.Budget();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                WriteBudgetLog(Convert.ToString("BUDGET Live REQUEST Parameter : " + BudgetCode + "," + DivisionId + "," + CurrSymbol));
                BudgetNode = obj.GetBABudgetCall(BudgetCode, DivisionId, CurrSymbol);
                WriteBudgetLog(Convert.ToString("BUDGET Live RESPONSE : " + BudgetNode.OuterXml));
                Exceptions.WriteInfoLog("After Calling LIVE BUDGET Web service()");
            }
            else
            {
                Exceptions.WriteInfoLog("Before Calling BETA BUDGET Web service()");
                PWGlobalEcomm.Budget.Budget obj = new PWGlobalEcomm.Budget.Budget();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                WriteBudgetLog(Convert.ToString("BUDGET BETA REQUEST Parameter : " + BudgetCode + "," + DivisionId + "," + CurrSymbol));
                BudgetNode = obj.GetBABudgetCall(BudgetCode, DivisionId, CurrSymbol);
                WriteBudgetLog(Convert.ToString("BUDGET BETA RESPONSE : " + BudgetNode.OuterXml));
                Exceptions.WriteInfoLog("After Calling BETA BUDGET Web service()");     
            }
            objBudget = Budgets.FromXmlString(BudgetNode.OuterXml.ToString());
            Exceptions.WriteInfoLog("Exiting GetBudgetDetails()");
            return objBudget;
        }
        catch (Exception ex)
        { Exceptions.WriteExceptionLog(ex); objBudget = new Budgets(); return objBudget; }
    }

    public static SalesOrders GetBudgetTransactionDetails(int DivisionId, int UDFSeqNo, string UDFValue)
    {
        SalesOrders objBudgetTransaction;
        XmlNode BudgetNode;
        try
        {
            Exceptions.WriteInfoLog("public static SalesOrders GetBudgetTransactionDetails(int DivisionId, int UDFSeqNo, string UDFValue)");

            objBudgetTransaction = new SalesOrders();
            if (ConfigurationManager.AppSettings["WebServiceRefernceStatus"].ToLower() == "live")
            {
                Exceptions.WriteInfoLog("Before Calling LIVE BUDGET Transaction Web service()");
                PWGlobalEcomm.OrderLive.Order obj = new PWGlobalEcomm.OrderLive.Order();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                WriteBudgetLog(Convert.ToString("BUDGET Transaction Live REQUEST Parameter : " + DivisionId + "," + UDFSeqNo + "," + UDFValue));
                BudgetNode = obj.GetSalesOrdersForDivisionAndUDF(DivisionId, UDFSeqNo, UDFValue);
                WriteBudgetLog(Convert.ToString("BUDGET Transaction Live RESPONSE : " + BudgetNode.OuterXml));
                Exceptions.WriteInfoLog("After Calling LIVE BUDGET Transaction Web service()");
            }
            else
            {
                Exceptions.WriteInfoLog("Before Calling BETA BUDGET Transaction Web service()");
                PWGlobalEcomm.Order.Order obj = new PWGlobalEcomm.Order.Order();
                NetworkCredential objNetworkCredentials = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUsername"].ToString(), ConfigurationManager.AppSettings["WebServicePassword"].ToString());
                obj.Credentials = objNetworkCredentials;
                WriteBudgetLog(Convert.ToString("BUDGET Transaction BETA REQUEST Parameter : " + DivisionId + "," + UDFSeqNo + "," + UDFValue));
                BudgetNode = obj.GetSalesOrdersForDivisionAndUDF(DivisionId, UDFSeqNo, UDFValue);
                WriteBudgetLog(Convert.ToString("BUDGET Transaction BETA RESPONSE : " + BudgetNode.OuterXml));
                Exceptions.WriteInfoLog("After Calling BETA BUDGET Transaction Web service()");
            }
            /*Changes done by Snehal - For Error Node*/
            if (!BudgetNode.OuterXml.Contains("Error"))
            {
                objBudgetTransaction = SalesOrders.FromXmlString(BudgetNode.OuterXml.ToString());
            }
            /*END*/
            Exceptions.WriteInfoLog("Exiting GetBudgetTransactionDetails()");
            return objBudgetTransaction;
        }
        catch (Exception ex)
        { Exceptions.WriteExceptionLog(ex); objBudgetTransaction = new SalesOrders(); return objBudgetTransaction; }
    }

    /// <summary>
    /// Method to write Budget log. 
    /// </summary>
    /// <param name="strXML">String object</param>
    public static void WriteBudgetLog(String strXML)
    {
        string BudgetLogFolderPath = GlobalFunctions.GetPhysicalFolderPath() + "XML\\BudgetLogs";
        try
        {
            CultureInfo objCulture = new CultureInfo("en-GB");

            if (!Directory.Exists(BudgetLogFolderPath))
                Directory.CreateDirectory(BudgetLogFolderPath);

            if (Directory.Exists(BudgetLogFolderPath))
            {
                //Create month wise exception log file.
                string date = string.Format("{0:dd}", DateTime.Now);
                string month = string.Format("{0:MMM}", DateTime.Now).ToString(objCulture);
               // string month = string.Format("{0:MMM}", DateTime.Now).ToString();
                string year = string.Format("{0:yyyy}", DateTime.Now);

                string folderName = month + year; //Feb2013
                string monthFolder = BudgetLogFolderPath + "\\" + folderName;
                if (!Directory.Exists(monthFolder))
                    Directory.CreateDirectory(monthFolder);

                string BudgetLogFileName = monthFolder +
                    "\\BudgetLogs_" + date + month + ".txt"; //BudgetLogs_04Feb.txt

                using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(BudgetLogFileName, true))
                {
                    strmWriter.WriteLine("On " + DateTime.Now.ToString());
                    strmWriter.WriteLine("XML: " + strXML);
                    strmWriter.WriteLine("-------------------------------------------------------------------------------");
                }
            }
            else
            {
                throw new DirectoryNotFoundException("Budget log folder not found in the specified path");
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }
}


[XmlRoot("Budgets")]
public class Budgets
{
    [XmlElement("Budget", Type = typeof(BudgetNode))]
    public BudgetNode[] BudgetNodes { get; set; }

    public Budgets()
    {
        BudgetNodes = null;
    }

    public static Budgets FromXmlString(string xmlString)
    {
        var reader = new StringReader(xmlString);
        XmlReader rdr = XmlReader.Create(reader);
        var serializer = new XmlSerializer(typeof(Budgets));
        var instance = (Budgets)serializer.Deserialize(rdr);
        return instance;
    }
}

[Serializable]
public class BudgetNode
{
    [XmlElement("BudgetCode")]
    public string BudgetCode { get; set; }

    [XmlElement("BudgetCurrencySymbol")]
    public string BudgetCurrencySymbol { get; set; }

    [XmlElement("BudgetCurrTotalBudget")]
    public string BudgetCurrTotalBudget { get; set; }

    [XmlElement("BudgetCurrOrdersNotInvoiced")]
    public string BudgetCurrOrdersNotInvoiced { get; set; }

    [XmlElement("BudgetCurrOrdersInvoiced")]
    public string BudgetCurrOrdersInvoiced { get; set; }

    [XmlElement("BudgetCurrOutstandingBudget")]
    public string BudgetCurrOutstandingBudget { get; set; }

    [XmlElement("WebCurrencySymbol")]
    public string WebCurrencySymbol { get; set; }

    [XmlElement("WebCurrTotalBudget")]
    public string WebCurrTotalBudget { get; set; }

    [XmlElement("WebCurrOrdersNotInvoiced")]
    public string WebCurrOrdersNotInvoiced { get; set; }

    [XmlElement("WebCurrOrdersInvoiced")]
    public string WebCurrOrdersInvoiced { get; set; }

    [XmlElement("WebCurrOutstandingBudget")]
    public string WebCurrOutstandingBudget { get; set; }

    [XmlElement("StartPeriod")]
    public string StartPeriod { get; set; }

    [XmlElement("StartYear")]
    public string StartYear { get; set; }

    [XmlElement("EndPeriod")]
    public string EndPeriod { get; set; }

    [XmlElement("YearEnd")]
    public string YearEnd { get; set; }

    [XmlElement("BudgeStatus")]
    public string BudgeStatus { get; set; }

    public BudgetNode()
    { }
}


[XmlRoot("SalesOrders")]
public class SalesOrders
{
    [XmlElement("OrderHeader", Type = typeof(OrderHeaders))]
    public OrderHeaders[] orderHeaders { get; set; }

    public SalesOrders()
    { orderHeaders = null; }

    public static SalesOrders FromXmlString(string xmlString)
    {
        var reader = new StringReader(xmlString);
        XmlReader rdr = XmlReader.Create(reader);
        var serializer = new XmlSerializer(typeof(SalesOrders));
        var instance = (SalesOrders)serializer.Deserialize(rdr);        
        return instance;
    }
}

[Serializable]
public class OrderHeaders
{
    [XmlElement("SalesOrderID")]
    public string SalesOrderID { get; set; }

    [XmlElement("OrderDate")]
    public DateTime OrderDate { get; set; }

    [XmlElement("OrderValue")]
    public string OrderValue { get; set; }

    [XmlElement("OrderStatus")]
    public string OrderStatus { get; set; }

    [XmlElement("GoodsValue")]
    public string GoodsValue { get; set; }

    [XmlElement("TaxValue")]
    public string TaxValue { get; set; }

    [XmlElement("FreightValue")]
    public string FreightValue { get; set; }

    [XmlElement("OrderCurrency")]
    public string OrderCurrency { get; set; }

    public OrderHeaders()
    { }
}

[XmlRoot("SalesOrder")]
public class CustomerSalesOrder
{
    [XmlElement("OrderHeader", Type = typeof(CustomerOrderHeader))]
    public CustomerOrderHeader orderheaders { get; set; }

    [XmlElement("DeliveryAddresses", Type = typeof(DeliveryAddresses))]
    public DeliveryAddresses deliveryaddresses { get; set; }

    [XmlElement("OrderLines", Type = typeof(OrderLines))]
    public OrderLines orderlines { get; set; }

    [XmlElement("BillingAddress", Type = typeof(BillingAddress))]
    public BillingAddress billingaddress { get; set; }

    [XmlElement("Despatches", Type = typeof(Despatches))]
    public Despatches dispatches { get; set; }

    public CustomerSalesOrder()
    {
        orderheaders = null;
        deliveryaddresses = null;
        orderlines = null;
        billingaddress = null;
        dispatches = null;
    }

    public static CustomerSalesOrder FromXmlString(string xmlString)
    {
        var reader = new StringReader(xmlString);
        XmlReader rdr = XmlReader.Create(reader);
        var serializer = new XmlSerializer(typeof(CustomerSalesOrder));
        var instance = (CustomerSalesOrder)serializer.Deserialize(rdr);
        return instance;
    }
}

[Serializable]
public class CustomerOrderHeader
{
    [XmlElement("SalesOrderID")]
    public string SalesOrderID { get; set; }

    [XmlElement("CustomerID")]
    public string CustomerID { get; set; }

    [XmlElement("OrderDate")]
    public DateTime OrderDate { get; set; }

    [XmlElement("OrderValue")]
    public double OrderValue { get; set; }

    [XmlElement("OrderStatus")]
    public string OrderStatus { get; set; }

    [XmlElement("PaymentType")]
    public string PaymentType { get; set; }

    [XmlElement("GoodsValue")]
    public string GoodsValue { get; set; }

    [XmlElement("TaxValue")]
    public string TaxValue { get; set; }

    [XmlElement("FreightValue")]
    public string FreightValue { get; set; }

    [XmlElement("CustomerRef")]
    public string CustomerRef { get; set; }

    public CustomerOrderHeader()
    {
    }
}

[XmlRoot("DeliveryAddresses")]
[Serializable]
public class DeliveryAddresses
{
    [XmlElement("DeliveryAddress", Type = typeof(DeliveryAddress))]
    public DeliveryAddress[] deliveryaddress { get; set; }

    public DeliveryAddresses()
    {
        deliveryaddress = null;
    }
}

[Serializable]
public class DeliveryAddress
{
    [XmlElement("ContactName")]
    public string ContactName { get; set; }

    [XmlElement("CompanyName")]
    public string CompanyName { get; set; }

    [XmlElement("DelAddressID")]
    public string DelAddressID { get; set; }

    [XmlElement("Address1")]
    public string Address1 { get; set; }

    [XmlElement("City")]
    public string City { get; set; }

    [XmlElement("County")]
    public string County { get; set; }

    [XmlElement("PostCode")]
    public string PostCode { get; set; }

    [XmlElement("Country")]
    public string Country { get; set; }

    public DeliveryAddress()
    {
    }
}

[XmlRoot("OrderLines")]
[Serializable]
public class OrderLines
{
    [XmlElement("OrderLine", Type = typeof(OrderLine))]
    public OrderLine[] orderline { get; set; }

    public OrderLines()
    {
        orderline = null;
    }
}

[Serializable]
public class OrderLine
{
    [XmlElement("OrderLineID")]
    public string OrderLineID { get; set; }

    [XmlElement("ProductCode")]
    public string ProductCode { get; set; }

    [XmlElement("ProductDescription")]
    public string ProductDescription { get; set; }

    [XmlElement("OrderQuantity")]
    public Int16 OrderQuantity { get; set; }

    [XmlElement("BackOrderQuantity")]
    public Int16 BackOrderQuantity { get; set; }

    [XmlElement("UnitPrice")]
    public double UnitPrice { get; set; }

    [XmlElement("LineValue")]
    public double LineValue { get; set; }

    [XmlElement("DeliveryAddressLines", Type = typeof(DeliveryAddressLines))]
    public DeliveryAddressLines deliveryaddresslines { get; set; }

    public OrderLine()
    {
        deliveryaddresslines = null;
    }
}

[XmlRoot("DeliveryAddressLines")]
[Serializable]
public class DeliveryAddressLines
{
    [XmlElement("DeliveryAddressLine", Type = typeof(DeliveryAddressLine))]
    public DeliveryAddressLine deliveryaddressline { get; set; }

    public DeliveryAddressLines()
    {
        deliveryaddressline = null;
    }
}

[Serializable]
public class DeliveryAddressLine
{
    [XmlElement("DelAddressID")]
    public string DelAddressID { get; set; }

    [XmlElement("Quantity")]
    public string Quantity { get; set; }

    public DeliveryAddressLine()
    {
    }
}

[Serializable]
public class BillingAddress
{
    [XmlElement("ContactName")]
    public string ContactName { get; set; }

    [XmlElement("CompanyName")]
    public string CompanyName { get; set; }

    [XmlElement("Address1")]
    public string Address1 { get; set; }

    [XmlElement("City")]
    public string City { get; set; }

    [XmlElement("County")]
    public string County { get; set; }

    [XmlElement("PostCode")]
    public string PostCode { get; set; }

    [XmlElement("Country")]
    public string Country { get; set; }

    public BillingAddress()
    {
    }
}

[XmlRoot("Despatches")]
[Serializable]
public class Despatches
{
    [XmlElement("Despatch", Type = typeof(Despatch))]
    public Despatch[] despatch { get; set; }

    public Despatches()
    {
        despatch = null;
    }
}

[XmlRoot("Despatch")]
[Serializable]
public class Despatch
{
    [XmlElement("DespatchID")]
    public string DespatchID { get; set; }

    [XmlElement("DespatchDate")]
    public DateTime DespatchDate { get; set; }

    [XmlElement("ShippingMethod")]
    public string ShippingMethod { get; set; }

    [XmlElement("TrackingURL")]
    public string TrackingURL { get; set; }

    [XmlElement("ConsignmentNumbers")]
    public string ConsignmentNumbers { get; set; }

    [XmlElement("DelAddressID")]
    public string DelAddressID { get; set; }

    [XmlElement("DespatchLines", Type = typeof(DespatchLines))]
    public DespatchLines despatchlines { get; set; }

    public Despatch()
    {
        //shippingaddress = null;
        despatchlines = null;
    }

}

[XmlRoot("DespatchLines")]
[Serializable]
public class DespatchLines
{
    [XmlElement("DespatchLine", Type = typeof(DespatchLine))]
    public DespatchLine[] despatchline { get; set; }

    public DespatchLines()
    {
        despatchline = null;
    }
}

[Serializable]
public class DespatchLine
{
    [XmlElement("OrderLineID")]
    public string OrderLineID { get; set; }

    [XmlElement("DespatchLineNo")]
    public Int16 DespatchLineNo { get; set; }

    [XmlElement("DeliveredQuantity")]
    public Int16 DeliveredQuantity { get; set; }

    public DespatchLine()
    {
    }
}