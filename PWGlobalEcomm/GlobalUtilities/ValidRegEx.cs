﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Text;

/// <summary>
/// Author  : Sachin Chauhan
/// Date    : 10-09-2015
/// Scope   : Project & Application level
/// </summary>
/// Define all allowed regular expression which are going to be used for input validation 
/// Across admin control panel & web site level pages

public static class ValidRegEx
{
    #region Valid Regular Expressions

    /// <summary>
    /// Author : Sachin Chauhan
    /// Date   : 10-09-2015
    /// Scope  : To validate single digit number
    /// </summary>
    public static string RegEx_ValidSingleDigitNumber = @"^[1-9]{1}?$"; //@"^[1-9]${1}";

    /// <summary>
    /// Author : Sachin Chauhan
    /// Date   : 10-09-2015
    /// Scope  : To validate decimal number
    /// </summary>
    public static string RegEx_ValidMultiDigitNo(Int16 len)
    {
        StringBuilder sbMaxLen = new StringBuilder();
        for (Int16 i = 1; i < len+1 ; i++)
        {
            sbMaxLen.Append( i + ",");
        }

        return @"^[0-9]{1," + len + "}?$";
    }

    /// <summary>
    /// Author : Sachin Chauhan
    /// Date   : 10-09-2015
    /// Scope  : To validate Alpahnumeric String with Certain Safe Special Characters
    /// </summary>
    public static string RegEx_ValidAlpanumericStringWithSafeSpecialChars = @"^[ A-Za-z0-9_@/#&+-\[\](){} ]*$";

    /// <summary>
    /// Author : Sachin Chauhan
    /// Date   : 10-09-2015
    /// Scope  : To validate Number with with two deciaml points
    /// </summary>
    public static string RegEx_ValidDecimalWithTwoDigit = @"^[0-9]{1,6}([.][0-9]{1,2})?$";

    public static string RegEx_ValidDecimalWithFourDigit = @"^[0-9]{1,6}([.][0-9]{1,4})?$";

    public static string RegEx_ValidFreightDecimalWithFourDigit = @"^[0-9]{1,15}([.][0-9]{1,4})?$"; //Added By Snehal for Freight Band Parameter - 02 12 2016

    public static string RegEx_ValidAlpanumerichyphen = @"^[ A-Za-z0-9-]*$";

    #endregion
}

