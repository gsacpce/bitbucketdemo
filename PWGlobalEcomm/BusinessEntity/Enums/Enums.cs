﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public enum StoreMode
{
    Create = 1,
    Edit = 2
}

public enum TimeOutSetting
{
    SetTime = 500
}

public enum StoreStatus
{
    Ready = 1,
    In_Development = 2,
    Requested = 3,
    Live = 4
}

/// <summary>
/// Elements Of Store to be created
/// </summary>
public enum SiteElements
{
    None,
    Site,
    Database,
    SiteAndDatabase
}

public enum DBAction
{ 
    Select = 0,
    Insert = 1,
    Update = 2,
    Delete = 3    
}

public enum AlertType
{
    Success,
    Warning,
    Failure
}

public enum ProductAttributeType
{
    Size,
    Element,
    UOM
}

public enum RegistrationFilter
{
    WhiteList,
    BlackList
}

