﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public partial class ProductBE : EntityBase
{
    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 22-07-16
    /// Scope   : This Entity represents Product Data for product listing page
    /// </summary>
    
    public decimal AsLowAsPrice { get; set; }
    public decimal AsLowAsstrikePrice { get; set; }
    public Int16 CurrencyId { get; set; }
    public string CategoryName { get; set; }
    public string SubCategoryName { get; set; }
    public string SubSubCategoryName { get; set; }
    public Int16 PageNo { get; set; }
    public decimal MinPrice { get; set; }
    public decimal MaxPrice { get; set; }
    public string Colors { get; set; }
    public string SectionIds { get; set; }
    public string SortName { get; set; }
    public string SortBy { get; set; }
    public string DiscountCode { get; set; }
    public Int16 PageSize { get; set; }
    public bool IsShowInFilter { get; set; }
    public Int32 ProductCount { get; set; }
    public Int32 ProductDetailGroupMappingId { get; set; }
    public string SectionName { get; set; }
    public string Name { get; set; }
    public string SearchProductIds { get; set; }
    public Int16 UserTypeID { get; set; }

    public Int32 ProductSKUId { get; set; }
    public Int32 MinimumOrderQuantity { get; set; }
    public Int32 MaximumOrderQuantity { get; set; }
    public Int32 Inventory { get; set; }

    public List<ProductCategoryFilterBE> ProductCategoryFilter { get; set; }
    public List<ProductPriceFilterBE> ProductPriceFilter { get; set; }
    public List<ProductColorFilterBE> ProductColorFilter { get; set; }
    public List<ProductSectionFilterBE> ProductSectionFilter { get; set; }

    public List<ProductCustomFilterBE> ProductCustomFilter1 { get; set; }
    public List<ProductCustomFilterBE> ProductCustomFilter2 { get; set; }
    public List<ProductCustomFilterBE> ProductCustomFilter3 { get; set; }
    public List<ProductCustomFilterBE> ProductCustomFilter4 { get; set; }
    public List<ProductCustomFilterBE> ProductCustomFilter5 { get; set; }

    public string CustomFilter1 { get; set; }
    public string CustomFilter2 { get; set; }
    public string CustomFilter3 { get; set; }
    public string CustomFilter4 { get; set; }
    public string CustomFilter5 { get; set; }

    [Serializable]
    public class ProductCategoryFilterBE : EntityBase
    {
        public string CategoryName { get; set; }
        public Int32 ProductCount{ get; set; }
    }

    [Serializable]
    public class ProductPriceFilterBE : EntityBase
    {
        public decimal MinPrice{ get; set; }
        public decimal MaxPrice { get; set; }
    }

    [Serializable]
    public class ProductSectionFilterBE : EntityBase
    {
        public Int16 SectionId { get; set; }
        public string SectionName { get; set; }
        public string IconImageExtension { get; set; }
        public Int32 ProductCount { get; set; }
    }

    [Serializable]
    public class ProductColorFilterBE : EntityBase
    {
        public Int16 ColorId{ get; set; }
        public string ColorName { get; set; }
        public Int32 ProductCount { get; set; }
    }

    [Serializable]
    public class ProductCustomFilterBE : EntityBase
    {
        public string FilterName { get; set; }
        public string FilterData { get; set; }
        public Int32 ProductCount { get; set; }
    }    
}

