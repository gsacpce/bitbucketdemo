﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class GiftOrderDetailsBE
{
    public int GiftOrderDetailsID { get; set; }
    public int GiftOrderID { get; set; }
    public decimal Amount { get; set; }
    public int Quantity { get; set; }
    public string RecipientEmail { get; set; }
    public string PersonalMessage { get; set; }
    public int CreatedBy { get; set; }
    public int ModifiedBy { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime ModifiedDate { get; set; }
}
