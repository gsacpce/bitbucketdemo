﻿using System;
using System.Collections.Generic;

public class GiftCertificateOrderXMLEntity
{
    public GiftCertificateOrderXMLEntity()
    {
        //OrderLineGiftLst = new List<OrderLineGift>();
        //UserDefinedFieldLst = new List<UserDefinedField>();
    }

    #region Properties
    public string CustomerContactId { get; set; }
    public int DivisionId { get; set; }
    public string SkipPipeline { get; set; }
    public string OrderCurrency { get; set; }
    public int PaymentTypeId { get; set; }
    public string TransactionRef { get; set; }
    public string Last4Digits { get; set; }
    public decimal CollectedAmount { get; set; }
    public DateTime CollectedDate { get; set; }
    public string CollectedFlag { get; set; }
    public decimal GoodsTotal { get; set; }
    public string CustomerRef { get; set; }
    public string OrderNotes { get; set; }
    public string SourceCode { get; set; }
    public string SourceCodeId { get; set; }
    public string cardtoken { get; set; }
    public string merchantID { get; set; }
    //public BillTo BillToInstance { get; set; }
    //List<OrderLineGift> OrderLineGiftLst { get; set; }
    //List<UserDefinedField> UserDefinedFieldLst { get; set; }
    #endregion

    public class BillTo
    {
        public BillTo() { }
        public string ContactName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Telephone { get; set; }
    }

    public class OrderLineGift
    {
        public OrderLineGift()
        { }
        public int LineQty { get; set; }
        public decimal LineValue { get; set; }
        public string LineMessage { get; set; }
        public string RecipientEmail { get; set; }
    }

    //public class UserDefinedField
    //{
    //    public UserDefinedField()
    //    { }
    //    public int SequenceNo { get; set; }
    //    public string Value { get; set; }
    //}
}

