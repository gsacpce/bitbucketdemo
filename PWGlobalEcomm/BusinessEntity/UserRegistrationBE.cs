﻿using System;
using System.Collections.Generic;

[Serializable]
public class UserRegistrationBE : EntityBase
{
    #region
    public List<RegistrationFieldsConfigurationBE> UserRegistrationFieldsConfigurationLst { get; set; }
    public List<RegistrationFeildTypeMasterBE> UserRegistrationFeildTypeMasterLst { get; set; }
    public List<RegistrationFieldDataMasterBE> UserRegistrationFieldDataMasterLst { get; set; }
    public List<RegistrationLanguagesBE> UserRegistrationLanguagesLst { get; set; }
    public List<RegistrationFilterBE> UserRegistrationFilterLst { get; set; }
    public List<UserHierarchyBE> UserHierarchyLst { get; set; }
    public List<CountryBE> CountryLst { get; set; }
    public List<UserRegistrationHierarchyCustomFieldMappingsBE> UserRegistrationHierarchyCustomFieldMappingsLst { get; set; }
    #endregion
    public UserRegistrationBE()
    {
        UserHierarchyLst = new List<UserHierarchyBE>();
        UserRegistrationHierarchyCustomFieldMappingsLst = new List<UserRegistrationHierarchyCustomFieldMappingsBE>();
        UserRegistrationFieldsConfigurationLst = new List<RegistrationFieldsConfigurationBE>();
        UserRegistrationFeildTypeMasterLst = new List<RegistrationFeildTypeMasterBE>();
        UserRegistrationFieldDataMasterLst = new List<RegistrationFieldDataMasterBE>();
        UserRegistrationLanguagesLst = new List<RegistrationLanguagesBE>();
        UserRegistrationFilterLst = new List<RegistrationFilterBE>();
        CountryLst = new List<CountryBE>();
    }

    [Serializable]
    public class RegistrationFieldsConfigurationBE : EntityBase
    {
        public Int16 RegistrationFieldsConfigurationId { get; set; }
        public Int16 FieldType { get; set; }
        public Int16 ParentFieldGroup { get; set; }
        public string SystemColumnName { get; set; }
        public bool IsVisible { get; set; }
        public Int16 DisplayOrder { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsActive { get; set; }
        public int MaxLength { get; set; }
    }
    public List<RegistrationFieldsConfigurationBE> UserRegistrationFieldsConfiguration { get; set; }

    [Serializable]
    public class RegistrationFeildTypeMasterBE
    {
        public Int16 FieldTypeId { get; set; }
        public string FieldTypeName { get; set; }
        public Int16 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int16 ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public List<RegistrationFeildTypeMasterBE> UserRegistrationFeildTypeMaster { get; set; }

    [Serializable]
    public class RegistrationFieldDataMasterBE
    {
        public int RegistrationFieldDataMasterId { get; set; }
        public Int16 RegistrationFieldsConfigurationId { get; set; }
        public Int16 RegistrationFieldDataId { get; set; }
        public string RegistrationFieldDataValue { get; set; }
        public bool IsActive { get; set; }
        public Int16 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int16 ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Int16 LanguageID { get; set; }
    }
    public List<RegistrationFieldDataMasterBE> UserRegistrationFieldDataMaster { get; set; }

    [Serializable]
    public class RegistrationLanguagesBE
    {
        public Int16 RegistrationLanguageId { get; set; }
        public Int16 LanguageId { get; set; }
        public Int16 RegistrationFieldsConfigurationId { get; set; }
        public string LabelTitle { get; set; }
        public bool IsActive { get; set; }
        public Int16 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int16 ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string LabelEnglish { get; set; }
        public int Action { get; set; }
    }
    public List<RegistrationLanguagesBE> UserRegistrationLanguages { get; set; }

    [Serializable]
    public class RegistrationFilterBE
    {
        public string Domain { get; set; }
        public string EmailId { get; set; }
        public string FilterType { get; set; }
    }
    public List<RegistrationFilterBE> UserRegistrationFilter { get; set; }

    [Serializable]
    public class UserHierarchyBE
    {
        public byte UserHierarchyId { get; set; }
        public string HierarchyName { get; set; }
        public bool IncludeVAT { get; set; }
        public bool IsActive { get; set; }
        public byte ParentHierarchy { get; set; }
    }
    public List<UserHierarchyBE> UserHierarchy { get; set; }

    [Serializable]
    public class CountryBE
    {
        public Int16 CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string RegionCode { get; set; }
        public string CountryAlias { get; set; }
        public bool IsActive { get; set; }
        public bool DisplayDutyMessage { get; set; }
    }
    public List<CountryBE> Country { get; set; }

    [Serializable]
    public class UserRegistrationHierarchyCustomFieldMappingsBE
    {
        public Int16 HierarchyCustomFieldMappingId { get; set; }
        public Int16 UserHierarchyId { get; set; }
        public Int16 RegistrationFieldsConfigurationId { get; set; }
    }
    public List<UserRegistrationHierarchyCustomFieldMappingsBE> UserRegistrationHierarchyCustomFieldMappings { get; set; }


    [Serializable]
    public class UserPredefineColumns
    {
        public string PredefinedColumn1 { get; set; }
        public string PredefinedColumn2 { get; set; }
        public string PredefinedColumn3 { get; set; }
        public string PredefinedColumn4 { get; set; }
        public string PredefinedColumn5 { get; set; }
        public string PredefinedColumn6 { get; set; }
        public string PredefinedColumn7 { get; set; }
        public string PredefinedColumn8 { get; set; }
        public string PredefinedColumn9 { get; set; }
    }

}

