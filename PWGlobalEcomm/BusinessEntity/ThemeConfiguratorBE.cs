﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public partial class ThemeConfiguratorBE:EntityBase
{
    public string KeyName { get; set; }
    public string KeyValue { get; set; }
}
