﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class RoleManagementBE : EntityBase
{
    public int RoleID { get; set; }
    public int MenuID { get; set; }
    public string MenuIdCSV { get; set; }
    public int ParentMenuId { get; set; }
    public string RoleName { get; set; }
    public string MenuName { get; set; }
    public string MenuUrl { get; set; }
    public string Action { get; set; }
    public int NoOfusers { get; set; }
    public bool IsActive { get; set; }
    public int TotalRow { get; set; }
    //public int Action { get; set; }
    public bool IsSys { get; set; } // This Column will be allowed to show particular menu to all user e.g login,logout,dashboard
    public bool IsMCP { get; set; }
    public bool IsMenuActive { get; set; }
}

