﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public class FreightSourceCountryMasterBE : EntityBase
    {
        public int ID { get; set; }
        public string FreightSourceCountryCode { get; set; }
        public string FreightSourceCountryName { get; set; }
        public string FreightTableID { get; set; }
        public bool IsDefault { get; set; }
    }
