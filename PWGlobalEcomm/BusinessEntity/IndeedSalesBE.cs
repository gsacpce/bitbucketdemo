﻿using System;


public class IndeedSalesBE
{
    public DateTime date { get; set; }
    public class BA_SALESGRANDTOTAL
    {
        public int Division_ID { get; set; }
        public int Cur_Qtr_Total_Orders { get; set; }
        public decimal Cur_Qtr_Sales_GBP { get; set; }
        public decimal Cur_Qtr_Sales_EUR { get; set; }
        public decimal Cur_Qtr_Sales_USD { get; set; }
        public int Lst_Qtr_Total_Orders { get; set; }
        public decimal Lst_Qtr_Sales_GBP { get; set; }
        public decimal Lst_Qtr_Sales_EUR { get; set; }
        public decimal Lst_Qtr_Sales_USD { get; set; }
        public int Ytd_Total_Orders { get; set; }
        public decimal Ytd_Sales_GBP { get; set; }
        public decimal Ytd_Sales_EUR { get; set; }
        public decimal Ytd_Sales_USD { get; set; }
        public decimal Percent_Qty_Diff_Qtr { get; set; }
        public decimal Percent_Sales_Diff_Qtr { get; set; }
        public int Percent { get; set; }
        public string CurrencySymbol { get; set; }
        public string ColName { get; set; }
        public string Cur_Qtr_Sales { get; set; }
        public string FeatureDefaultValue { get; set; }
    }

    public class BA_SALESBYGROUP
    {
        public int Division_ID { get; set; }
        public string Group_Type { get; set; }
        public string Group_Name { get; set; }
        public string Groupvalue { get; set; }
        public string Value_Range { get; set; }
        public int Totalorders { get; set; }
        public decimal GBP_Value { get; set; }
        public decimal EUR_Value { get; set; }
        public decimal USD_Value { get; set; }
        public decimal Percent_Total_Qty { get; set; }
        public decimal Percent_Total_Sales { get; set; }
        public string TotalSales { get; set; }
    }

    public class BA_SALESBYPRODUCT
    {
        public string ProductId { get; set; }
        public string ProductCode { get; set; }
        public int Units { get; set; }
        public string ProductName { get; set; }
        public int Sales { get; set; }
        public string DefaultImageName { get; set; }
    }
    public class BA_UserAddress
    {
        public int Id { get; set; }
        public int userid { get; set; }
        public string AddressTitle { get; set; }
        public string Address { get; set; }
        public string flag { get; set; }
    }

    public class BA_Points
    {
        public int points { get; set; }
        public int Anniversary { get; set; }
        public int Birthday { get; set; }
        public string birth_date { get; set; }
        public string Hire_Date { get; set; }
        public string Pointsbirth_date { get; set; }
        public string PointsHire_Date { get; set; }
        public string dateProcessed { get; set; }
        public DateTime date_Processed { get; set; }
        public string Reason { get; set; }
    }

    public class BA_Budget
    {
        public int Division_ID { get; set; }
        public string Budget_Code { get; set; }
        public string Currency_Symbol { get; set; }
        public string Year_Start { get; set; }
        public string Period_Start { get; set; }
        public string Year_End { get; set; }
        public string Period_End { get; set; }
        public string Employee_ID { get; set; }
        public DateTime Date_Created { get; set; }
        public DateTime Date_Processed { get; set; }
        public int SequenceNo { get; set; }
        public string Reports_To_Employee_ID { get; set; }
        public string Name { get; set; }
        public string work_email { get; set; }
        public string Team { get; set; }

    }
}
