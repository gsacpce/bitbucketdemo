﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


  public class UserBudgetBE
    {
        public string EmailId { get; set; }
        public string BudgetCode { get; set; }
        public string CurrencySymbol { get; set; }
        public string ApproverName { get; set; }
        public string ApproverEmailid { get; set; }
        public DateTime Date_Created { get; set; }
    }

