﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class CatalogueBE
{
    public int DivisionID { get; set; }
    public List<Division> lstDivision { get; set; }
    public List<Key_Groups> lstKey_Groups { get; set; }
    public List<Catalogues> lstCatalogues { get; set; }
    public List<CataloguesID> lstCataloguesID { get; set; }
    public List<Source_Codes> lstSource_Codes { get; set; }
    public List<Web_Search_Accounts> lstWeb_Search_Accounts { get; set; }

    public class Division
    {
        public int DIVISION_ID { get; set; }
        public string DIVISION_CODE { get; set; }
    }

    public class Key_Groups
    {
        public int GROUP_ID { get; set; }
        public string GROUP_REF { get; set; }
        public int DIVISION_ID { get; set; }
    }

    public class Catalogues
    {
        public int CATALOGUE_ID { get; set; }
        public string CATALOGUE_REF { get; set; }
        public string CURRENCY_COUNTRY { get; set; }
        public int DIVISION_ID { get; set; }
    }

    public class CataloguesID
    {
        public int CatalogueId { get; set; }
        public string CatalogueName { get; set; }
    }

    public class Source_Codes
    {
        public int SOURCE_CODE_ID { get; set; }
        public string SOURCE_CODE { get; set; }
        public string SOURCE_CODE_DESC { get; set; }
        public int CATALOGUE_ID { get; set; }
    }

    public class Web_Search_Accounts
    {
        public int CUSTOMER_ID { get; set; }
        public string NAME { get; set; }
        public int DIVISION_ID { get; set; }
        public int SALESPERSON_ID { get; set; }
        public string CURRENCY_COUNTRY { get; set; }
        public int SOURCE_CODE_ID { get; set; }
        public int Customer_Contact_ID { get; set; }        
    }
}

