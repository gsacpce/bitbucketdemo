﻿using System;

[Serializable]
public class PunchoutMarkerBE : EntityBase
{
    public int ID { get; set; }
    public DateTime date { get; set; }
    public string Marker { get; set; }
}

