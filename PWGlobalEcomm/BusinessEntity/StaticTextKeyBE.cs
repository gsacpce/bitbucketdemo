﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class StaticTextKeyBE : EntityBase
{
    public Int16 StaticTextId { get; set; }
    public string StaticTextKey { get; set; }
    public bool IsActive { get; set; }
    public Int16 StaticTextIdLanguageId { get; set; }
    public Int16 LanguageId { get; set; }
    public string StaticTextValue { get; set; }
}

