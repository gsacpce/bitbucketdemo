﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class UserBE : EntityBase
{

    public UserBE()
    {
        UserDeliveryAddress = new List<UserDeliveryAddressBE>();
        UserPreDefinedColumns = new List<UserPreDefinedColumnsBE>();
    }
    public string UserTypeName { get; set; }
    public Int16 UserId { get; set; }
    public string EmailId { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
    public string Phone { get; set; }
    public bool IsActive { get; set; }
    public Int16 RoleId { get; set; }
    public string IPAddress { get; set; }
    public Int16 LoginAttempts { get; set; }
    public DateTime LastLogin { get; set; }
    public DateTime LoginAttemptDate { get; set; }
    public DateTime CurrentTimeStamp { get; set; }
    public Int16 StatusId { get; set; }
    public Int16 ApproverUserId { get; set; }
    public Int16 CurrencyId { get; set; }
    public string CurrencySymbol { get; set; }
    public string CurrencyCode { get; set; }
    public Int16 SecurityQuestionId { get; set; }
    public string SecurityAnswer { get; set; }
    public Int16 StoreId { get; set; }

    public DateTime SubscribedDate { get; set; }
    public string SubscribedStatus { get; set; }
    public int Action { get; set; }
    public string RoleName { get; set; }
    public string StoreIdCSV { get; set; }
    public string UserName { get; set; }

    public Int16 CountryId { get; set; }

    //public string BASYSCustomerContactId { get; set; }

    public Int32 Points { get; set; }
    public Int32 PreviousPoints { get; set; }
    public DateTime PointsUpdateDate { get; set; }

    public string ResetCode { get; set; }
    public DateTime ResetTime { get; set; }
    public bool IsResetActive { get; set; }


    public string PredefinedColumn1 { get; set; }
    public string PredefinedColumn2 { get; set; }
    public string PredefinedColumn3 { get; set; }
    public string PredefinedColumn4 { get; set; }
    public string PredefinedColumn5 { get; set; }
    public string PredefinedColumn6 { get; set; }
    public string PredefinedColumn7 { get; set; }
    public string PredefinedColumn8 { get; set; }
    public string PredefinedColumn9 { get; set; }
    public string PredefinedColumn10 { get; set; }
    public string PredefinedColumn11 { get; set; }
    public string PredefinedColumn12 { get; set; }
    public string PredefinedColumn13 { get; set; }
    public string PredefinedColumn14 { get; set; }
    public string PredefinedColumn15 { get; set; }
    public string PredefinedColumn16 { get; set; }
    public string PredefinedColumn17 { get; set; }
    public string PredefinedColumn18 { get; set; }
    public string PredefinedColumn19 { get; set; }
    public string PredefinedColumn20 { get; set; }
    public string PredefinedColumn21 { get; set; }
    public string PredefinedColumn22 { get; set; }
    public string PredefinedColumn23 { get; set; }
    public string PredefinedColumn24 { get; set; }
    public string PredefinedColumn25 { get; set; }
    public string PredefinedColumn26 { get; set; }
    public string PredefinedColumn27 { get; set; }
    public string PredefinedColumn28 { get; set; }
    public string PredefinedColumn29 { get; set; }
    public string PredefinedColumn30 { get; set; }
    public string CustomColumn1 { get; set; }
    public string CustomColumn2 { get; set; }
    public string CustomColumn3 { get; set; }
    public string CustomColumn4 { get; set; }
    public string CustomColumn5 { get; set; }
    public string CustomColumn6 { get; set; }
    public string CustomColumn7 { get; set; }
    public string CustomColumn8 { get; set; }
    public string CustomColumn9 { get; set; }
    public string CustomColumn10 { get; set; }
    public string CustomColumn11 { get; set; }
    public string CustomColumn12 { get; set; }
    public string CustomColumn13 { get; set; }
    public string CustomColumn14 { get; set; }
    public string CustomColumn15 { get; set; }
    public string CustomColumn16 { get; set; }
    public string CustomColumn17 { get; set; }
    public string CustomColumn18 { get; set; }
    public string CustomColumn19 { get; set; }
    public string CustomColumn20 { get; set; }
    public string CustomColumn21 { get; set; }
    public string CustomColumn22 { get; set; }
    public string CustomColumn23 { get; set; }
    public string CustomColumn24 { get; set; }
    public string CustomColumn25 { get; set; }
    public string CustomColumn26 { get; set; }
    public string CustomColumn27 { get; set; }
    public string CustomColumn28 { get; set; }
    public string CustomColumn29 { get; set; }
    public string CustomColumn30 { get; set; }

    //public string ContactName { get; set; }
    //public string Company { get; set; }
    //public string AddressLine1 { get; set; }
    //public string AddressLine2 { get; set; }
    //public string Town { get; set; }
    //public string County { get; set; }
    //public string Postcode { get; set; }
    //public Int16 CountryId { get; set; }
    //public string RegPhone { get; set; }
    public string CountryName { get; set; }

    public string BASYS_CustomerContactId { get; set; }
    public int DefaultCustId { get; set; }
    public int SourceCodeId { get; set; }
    public bool IsGuestUser { get; set; }
    public bool IsPunchoutUser { get; set; } /* Added By Snehal - punchout user*/
    public Int16 UserHierarchyId { get; set; }
    public Int32 HierarchyUser { get; set; }
    public bool IsApproved { get; set; }

    public bool IsMarketing { get; set; }
    public Int16 UserTypeID { get; set; }/*User Type*/

    public String UserTitle { get; set; } /* Added By Hardik - for Title */
    public Int16 UserPreferredLanguageID { get; set; } // Added by SHRIGANESH For Getting User Preferred Details 09 Feb 2017		
    public Int16 UserPreferredCurrencyID { get; set; }

    public List<UserDeliveryAddressBE> UserDeliveryAddress { get; set; }

    [Serializable]
    public class UserDeliveryAddressBE : EntityBase
    {
        public Int16 DeliveryAddressId { get; set; }
        public Int16 UserId { get; set; }
        public string PreDefinedColumn1 { get; set; }
        public string PreDefinedColumn2 { get; set; }
        public string PreDefinedColumn3 { get; set; }
        public string PreDefinedColumn4 { get; set; }
        public string PreDefinedColumn5 { get; set; }
        public string PreDefinedColumn6 { get; set; }
        public string PreDefinedColumn7 { get; set; }
        public string PreDefinedColumn8 { get; set; }
        public string PreDefinedColumn9 { get; set; }
        public string AddressTitle { get; set; }
        public bool IsDefault { get; set; }
        public string CountryName { get; set; }
    }
    [Serializable]
    public class CustomerGroupBE : EntityBase
    {
        public int Action { get; set; }
        public Int16 GroupId { get; set; }
        public string GroupName { get; set; }
        public bool IsActive { get; set; }
        public DateTime ValidityStartDate { get; set; }
        public DateTime ValidityEndDate { get; set; }
        public decimal Budget { get; set; }
        public decimal PendingAmt { get; set; }
        public int NoOfUser { get; set; }
        public string EmailId { get; set; }
        public Int32 UserId { get; set; }
        public string UserIdCSV { get; set; }
    }

    public List<UserPreDefinedColumnsBE> UserPreDefinedColumns { get; set; }

    [Serializable]
    public class UserPreDefinedColumnsBE : EntityBase
    {
        public string SystemColumnName { get; set; }
        public string ColumnName { get; set; }
        public string FieldTypeName { get; set; }
        public Int16 ParentFieldGroup { get; set; }
        public bool IsVisible { get; set; }
        public Int16 DisplayOrder { get; set; }
        public bool IsMandatory { get; set; }
    }

    [Serializable]
    public class UserHierarchyBE : EntityBase
    {
        public byte UserHierarchyId { get; set; }
        public string HierarchyName { get; set; }
        public bool IncludeVAT { get; set; }
        public bool IsActive { get; set; }
        public byte ParentHierarchy { get; set; }
    }

    [Serializable]
    public class RegistrationFilter : EntityBase
    {
        public string Domain { get; set; }
        public string EmailId { get; set; }
    }

    [Serializable]
    public class HeirarchyUser : EntityBase
    {
        public int UserId { get; set; }
        public string EmailId { get; set; }
        public string UserHierarchyId { get; set; }
    }
    [Serializable]

    public class NameBadgeBE : EntityBase
    {
        public string Name { get; set; }
        public string DepartmentName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email_id { get; set; }
        public string Delivery_Name { get; set; }
        public string Delivery_Address { get; set; }
        public int NumberOfNames { get; set; }
    }

    public class UserBudgetBE
    {
        public int DivisionID { get; set; }
        public string EmailId { get; set; }
        public string BudgetCode { get; set; }
        public string CurrencySymbol { get; set; }
        public string Name { get; set; }
        public DateTime Date_Created { get; set; }
    }

    #region Indeed Code
    /*code added by hardik*/
    public class Titles : EntityBase
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public Int16 LanguageId { get; set; }
        public bool IsActive { get; set; }
    }
    #region REGION ADDED BY SHRIGANESH FOR CREATECUSTOMER
    public Int16 DivisionID { get; set; }
    public char MailOptOut { get; set; }
    #endregion
    /*Entity Added by SHRIGANESH for Indeed SSO Users 17 FEB 2017*/
    public class BA_USERFILE : EntityBase
    {
        public string Employee_ID { get; set; }
        public Int16 Division_ID { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Work_Email { get; set; }
        public string Reports_To_Employee_ID { get; set; }
        public DateTime Hire_Date { get; set; }
        public string Birth_Date { get; set; }
        public string Department { get; set; }
        public string Team { get; set; }
        public string Group_Name { get; set; }
        public string Cost_Center { get; set; }
        public string Address_Line_1 { get; set; }
        public string Address_Line_2 { get; set; }
        public string Address_Line_3 { get; set; }
        public string Address_City { get; set; }
        public string Address_State { get; set; }
        public string Address_Zip { get; set; }
        public string Address_Country { get; set; }
        public string ISO3_Country_Code { get; set; }
        public string Work_PhoneNumber { get; set; }
        public string User_View { get; set; }
        public string Address_Description { get; set; }
        public string Ship_To_Location { get; set; }

        /* Added By Snehal for JSON - 21 02 2017*/
        public string Name { get; set; }
        public string AddLine1 { get; set; }
        public string AddLine3 { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        /* Added By Snehal for JSON*/

        public string SAMLPostMessage { get; set; }
        public int UserPoints { get; set; }
    } 
    #endregion
}
