﻿using System;
using System.Collections.Generic;

[Serializable]
public class UserTypesBE
{
    public List<UserTypeDetails> lstUserTypeDetails { get; set; }
    public List<UserTypeLanguages> lstUserTypeLanguages { get; set; }
    public List<UserTypeCatalogue> lstUserTypeCatalogue { get; set; }

    public UserTypeDetails objUserTypeDetails { get; set; }
    public UserTypeLanguages objUserTypeLanguages { get; set; }
    public UserTypeCatalogue objUserTypeCatalogue { get; set; }

    public class UserTypeDetails
    {
        public Int16 UserTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Int32 ModifiedBy { get; set; }
        public bool IsWhiteList { get; set; }
    }

    // UserTypeLanguages Properties : SHRIGANESH SINGH : 14 June 2016
    public class UserTypeLanguages
    {
        public Int16 UserTypeLanguageID { get; set; }
        public Int16 UserTypeID { get; set; }
        public Int16 LanguageID { get; set; }
        public string UserTypeName { get; set; }
        public bool IsActive { get; set; }
    }

    // UserTypeCatalogue Properties : SHRIGANESH SINGH : 14 June 2016
    public class UserTypeCatalogue
    {
        public Int64 UserTypeCatalogueID { get; set; }
        public Int16 UserTypeID { get; set; }
        public Int16 CatalogueID { get; set; }
        public bool IsActive { get; set; }
        public bool IsVatDisplay { get; set; }
    }

    public class CustomUserTypes
    {
        public Int64 TotalRow { get; set; }
        public Int64 UserTypeID { get; set; }
        public string UserTypeName { get; set; }
        public bool IsActive { get; set; }
        public int LanguageId { get; set; }
        public Int32 CreatedBy { get; set; }
        public bool IsWhiteList { get; set; }
        public string EmailId { get; set; }
        public string Domain { get; set; }
        public Int64 UserTypeSequence { get; set; }
    }
    public class UserTypePaymentDetails
    {
        public Int16 Languageid { get; set; }
        public string PaymentTypeRef { get; set; }
        public string PaymentTypeLabelName { get; set; }
        public string UserTypeName { get; set; }
        public Int16 PaymentTypeID { get; set; }
        public Int32 UserTypeID { get; set; }
        public Int32 IsShowOnWebsite { get; set; }
        public bool IsActive { get; set; }
    }
}

public class UserTypesDetailsBE
{
    public Int16 UserTypeID { get; set; }
    public Int64 UserTypeCatalogueID { get; set; }
    public bool VatDisplay { get; set; }
    public decimal Vatrate { get; set; }
    public string UserTypeName { get; set; }
    public Int16 CurrencyId { get; set; }
    public string CurrencyCode { get; set; }
    public string UserCurrenciescsv { get; set; }
    public string UserCataloguecsv { get; set; }
}

public class UserTypeMappingBE
{
    public List<UserTypeDetails> lstUserTypeDetails { get; set; }
    public List<UserTypeLanguages> lstUserTypeLanguages { get; set; }
    public List<UserTypeMasterOptions> lstUserTypeMasterOptions { get; set; }
    public List<UserTypeEmailValidation> lstUserTypeEmailValidation { get; set; }
    public List<CustomUserTypes> lstCustomUserTypes { get; set; }
    public List<Country> lstCountry { get; set; }
    public List<RegistrationFieldsConfigurationBE> lstRegistrationFieldsConfigurationBE { get; set; }
    public List<RegistrationFeildTypeMasterBE> lstRegistrationFeildTypeMasterBE { get; set; }
    public List<UserTypeCustomFieldMapping> lstUserTypeCustomFieldMapping { get; set; }
    public List<RegistrationLanguagesBE> lstRegistrationLanguagesBE { get; set; }
    public List<RegistrationFieldDataMasterBE> lstRegistrationFieldDataMasterBE { get; set; }
    public List<RegistrationFieldDataMasterValidationBE> lstRegistrationFieldDataMasterValidationBE { get; set; }

    public class UserTypeDetails
    {
        public Int16 UserTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Int32 ModifiedBy { get; set; }
        public bool IsWhiteList { get; set; }
        public Int16 UserTypeSequence { get; set; }
    }
    public class UserTypeLanguages
    {
        public Int16 UserTypeLanguageID { get; set; }
        public Int16 UserTypeID { get; set; }
        public Int16 LanguageID { get; set; }
        public string UserTypeName { get; set; }
        public bool IsActive { get; set; }
    }
    public class UserTypeMasterOptions
    {
        public Int16 UserTypeMappingTypeID { get; set; }
        public string TypeName { get; set; } 
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Int32 ModifiedBy { get; set; }
        public Int16 Sequence { get; set; }
    }
    public class UserTypeEmailValidation
    {
        public int UserTypeEmailValidationId { get; set; }
        public Int16 UserTypeId { get; set; }
        public bool IsActive { get; set; }
        public string EmailId { get; set; }
        public Int32 CreatedBy { get; set; }
        public string Domain { get; set; }
        public Int16 UserTypeSequence { get; set; }
        public bool IsWhiteList { get; set; }
    }
    public class CustomUserTypes
    {
        public Int16 UserTypeID { get; set; }
        public string UserTypeName { get; set; }
        public bool IsActive { get; set; }
        public bool IsWhiteList { get; set; }
        public Int16 LanguageID { get; set; }
    }
    public class Country
    {
        public Int16 CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string RegionCode { get; set; }
        public string CountryAlias { get; set; }
        public bool IsActive { get; set; }
        public bool IsShow { get; set; }
        public Int16 CurrencyId { get; set; }
        public Int16 CatalogueId { get; set; }
        public bool DisplayDutyMessage { get; set; }
        public Int16 UserTypeID { get; set; }
    }
    public class RegistrationFieldsConfigurationBE : EntityBase
    {
        public Int16 RegistrationFieldsConfigurationId { get; set; }
        public Int16 FieldType { get; set; }
        public Int16 ParentFieldGroup { get; set; }
        public string SystemColumnName { get; set; }
        public bool IsVisible { get; set; }
        public Int16 DisplayOrder { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsActive { get; set; }
        public bool IsStoreDefault { get; set; }
        public bool IsLoginField { get; set; }
        public bool IsUserEditable { get; set; }
    }
    public class RegistrationFeildTypeMasterBE
    {
        public Int16 FieldTypeId { get; set; }
        public string FieldTypeName { get; set; }
        public Int16 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int16 ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public class UserTypeCustomFieldMapping
    {
        public int UserTypeCustomFieldMappingID { get; set; }
        public Int16 UserTypeID { get; set; }
        public Int16 RegistrationFieldsConfigurationId { get; set; }
    }
    public class RegistrationLanguagesBE
    {
        public Int16 RegistrationLanguageId { get; set; }
        public Int16 LanguageId { get; set; }
        public Int16 RegistrationFieldsConfigurationId { get; set; }
        public string LabelTitle { get; set; }
        public bool IsActive { get; set; }
        public Int16 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int16 ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string LabelEnglish { get; set; }
        public int Action { get; set; }
    }
    public class RegistrationFieldDataMasterBE
    {
        public int RegistrationFieldDataMasterId { get; set; }
        public Int16 RegistrationFieldsConfigurationId { get; set; }
        public Int16 RegistrationFieldDataId { get; set; }
        public string RegistrationFieldDataValue { get; set; }
        public bool IsActive { get; set; }
        public Int16 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int16 ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Int16 LanguageID { get; set; }
    }
    public class RegistrationFieldDataMasterValidationBE
    {
        public Int64 RegistrationFieldDataMasterId { get; set; }
        public Int16 RegistrationFieldsConfigurationId { get; set; }
        public Int16 RegistrationFieldDataId { get; set; }
        public string RegistrationFieldDataValue { get; set; }
        public bool IsActive { get; set; }
        public Int16 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int16 ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Int16 LanguageID { get; set; }
        public int UserTypeID { get; set; }
        #region SHRIGANESH  30 July 2016
        public bool RegisterAsDefault { get; set; }
        public bool DeclineRegister { get; set; } 
        #endregion
    }
}

public class UserTypeCountryMaster
{
    public List<UserTypeLanguagesBE> lstUserTypeLanguagesBE { get; set; }
    public List<CountryMasterBE> lstCountryMasterBE { get; set; }

    public class CountryMasterBE
    {
        public Int16 CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string RegionCode { get; set; }
        public string CountryAlias { get; set; }
        public bool IsActive { get; set; }
        public bool IsShow { get; set; }
        public Int16 CurrencyId { get; set; }
        public Int16 CatalogueId { get; set; }
        public bool DisplayDutyMessage { get; set; }
        public Int16 UserTypeID { get; set; }
    }
    public class UserTypeLanguagesBE
    {
        public Int16 UserTypeLanguageID { get; set; }
        public Int16 UserTypeID { get; set; }
        public Int16 LanguageID { get; set; }
        public string UserTypeName { get; set; }
        public bool IsActive { get; set; }
    }
}

public class UserTypeCustomFieldDataValue
{
    public string RegistrationFieldDataValue { get; set; }
}

public class UserTypeCustomMapping
{
    public Int16 UserTypeID { get; set; }
    public string UserTypeName { get; set; }
    public Int16 RegistrationFieldsConfigurationId { get; set; }
    public string LabelTitle { get; set; }
    public bool RegisterAsDefault { get; set; }
    public bool DeclineRegister { get; set; }
}

public class RegistrationFieldDataMasterValidationBE
{
    public Int64 RegistrationFieldDataMasterId { get; set; }
    public Int16 RegistrationFieldsConfigurationId { get; set; }
    public Int16 RegistrationFieldDataId { get; set; }
    public string RegistrationFieldDataValue { get; set; }
    public bool IsActive { get; set; }
    public Int16 CreatedBy { get; set; }
    public DateTime CreatedDate { get; set; }
    public Int16 ModifiedBy { get; set; }
    public DateTime ModifiedDate { get; set; }
    public Int16 LanguageID { get; set; }
    public int UserTypeID { get; set; }
}

public class StoreAndUserTypeCatalogueDetailsBE
{
    public Int16 StoreCatalogueIds { get; set; }
    public string CurrencyCode { get; set; }
    public Int16 CurrencyId { get; set; }
    public bool ActiveStoreCatalogue { get; set; }
    public Int16 UserTypeCatalogueIDs { get; set; }
    public bool ActiveUserTypeCatalogue { get; set; }
    public bool IsVatDisplay { get; set; }
    public Int16 UserTypeID { get; set; }
    public decimal Vatrate { get; set; }
    public bool IsSSOUT { get; set; }
    public bool SelectedUserTypeCatalogue { get; set; }
    #region Indeed Code
    public bool IsPointsEnabled { get; set; }
    public bool IsCurrencyEnabled { get; set; } 
    #endregion
    public bool IsBudgetEnabled { get; set; } 
}

public class SSOUserTypeCatalogueDetailsBE
{
    public Int16 UserTypeID { get; set; }
    public string UserTypeName { get; set; }
    public bool IsWhiteList { get; set; }
    public Int16 UserTypeSequence { get; set; }
    public Int16 CatalogueID { get; set; }
    public bool IsActive { get; set; }
    public bool IsVatDisplay { get; set; }
    public decimal VatRate { get; set; }
    public bool IsSSOUT { get; set; }
    public Int16 CurrencyId { get; set; }
}