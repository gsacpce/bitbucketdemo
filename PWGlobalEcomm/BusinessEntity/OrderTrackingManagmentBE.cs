﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class OrderTrackingManagmentBE : EntityBase
{
    public int OrderTrackingDetailId { get; set; }

    public int OrderId { get; set; }

    public int ProductSKUId { get; set; }

    public string DispatchId { get; set; }

    public string ProductName { get; set; }

    public string TrackingUrl { get; set; }

    public string ConsignmentNo { get; set; }

    public int DeliveredQty { get; set; }

    public string ShippingMethod { get; set; }

    public int OrderedBy { get; set; }

}

