﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessEntity
{
    public class StoreCurrencyBE:EntityBase
    {
        #region Properties

        public Int16 StoreDetailId { get; set; }
        public Int16 StoreId { get; set; }
        public Int16 CurrencyId { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }

        #endregion
    }
}
