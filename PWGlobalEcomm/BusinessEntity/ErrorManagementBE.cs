﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class ErrorManagementBE : EntityBase
{
    public Int16 ErrorTemplateId { get; set; }
    public string ErrorTemplateName { get; set; }
    public bool IsHtml { get; set; }
    public bool IsActive { get; set; }
    public Int16 ErrorTemplateLanguageId { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
    
}
