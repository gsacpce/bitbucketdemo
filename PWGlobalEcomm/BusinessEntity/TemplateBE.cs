﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class TemplateBE : EntityBase
{
    public Int16 TemplateId { get; set; }
    public string TemplateName { get; set; }
    public bool IsActive { get; set; }
    public string CreatedByName { get; set; }
    public Int16 NoOfStores { get; set; }
}

