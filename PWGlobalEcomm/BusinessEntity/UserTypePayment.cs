﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessEntity
{
    public class UserTypePayment
    {
        public Int16 UserTypePaymentID { get; set; }
        public Int16 UserTypeID { get; set; }
        public Int32 PaymentTypeID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 CreatedBy { get; set; }
        public Int32 ModifyBy { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}
