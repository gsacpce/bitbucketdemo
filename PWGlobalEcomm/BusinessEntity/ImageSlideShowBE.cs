﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class ImageSlideShowBE : EntityBase
{

    #region Properties

    public Int16 SlideShowId { get; set; }
    public string SlideShowName { get; set; }
    public Int16 SlideShowAnimationMasterId { get; set; }

    public Int16 SlideShowMasterId { get; set; }
    public string SlideShowMasterName { get;set; }
    public string JSFilePath { get; set; }
    public Int16 SlideDelayTime { get; set; }
    public Int16 SlideEffectTime { get; set; }
    public Int16 Height { get; set; }
    public bool ShowPagination { get; set; }

    
    public List<SlideShowImage> LstSlideShowImages { get; set; }

    public bool IsDefault { get; set; }
    public bool IsActive { get; set; }

    public Int16 FontId { get; set; }
    public string FontName { get; set; }
    public string FontValue { get; set; }
    public string TextForeColor { get; set; }
    public string TextBgColor { get; set; }
    public bool IsOverLayText { get; set; }
    public int OverlayFontSize { get; set; }
    public string OverlayHeight { get; set; }
    public string OverlayForeColor { get; set; }
    public string OverlayBGColor { get; set; }
    public string OverlayPosition { get; set; }
    
    //public SlideShowImage PropSlideShowImage { get; set; }

    #endregion

    [Serializable]
    public class SlideShowImage
    {
        public Int16 SlideShowImageId { get; set; }
        public string ImageExtension { get; set; }
        public string NavigateUrl { get; set; }
        public byte DisplayOrder { get; set; }
        public bool IsNewWindow { get; set; }
        public bool IsActive { get; set; }
        public string AltText { get; set; }
      
        public string ActiveThumb { get; set; }
        public string TempImageName { get; set; }
        public int WebOrderBy { get; set; }
        public string OverLayText { get; set; }
        public bool NewWindow { get; set; }
        public string FilePath { get; set; }

        #region MyRegion BELOW PROPERTIES ARE ADDED BY SHRIGANESH 29 JAN 2016 FOR OVERLAY SETTINGS
        public int OverlayFontSize { get; set; }
        public string OverlayHeight { get; set; }
        public string OverlayForeColor { get; set; }
        public string OverlayBGColor { get; set; }
        public string OverlayPosition { get; set; } 
        #endregion
    }

    [Serializable]
    public class SlideShowMaster
    {
        public Int16 SlideShowMasterId { get; set; }
        public string SlideShowMasterName { get; set; }
        public string JSFilepath { get; set; }
    }



  
}

/*public class SlideShowImage
{
    public Int16 SlideShowImageId { get; set; }
    public string ImageExtension { get; set; }
    public string NavigateUrl { get; set; }
    public byte DisplayOrder { get; set; }
    public bool IsNewWindow { get; set; }
    public bool IsActive { get; set; }

}*/