﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public partial class ProductBE : EntityBase
{
    #region Properties

    public int ProductId { get; set; }
    public string ProductCode { get; set; }
    public string ProductVirtualSampleLink { get; set; }
    public string VideoLink { get; set; }
    public string DefaultImageName { get; set; }
    public int Sequence { get; set; }
    public string Catalogue { get; set; }
    public bool IsActive { get; set; }
    //public int ProductLanguageId { get; set; }
    public string ProductName { get; set; }
    public string ProductColorGroupName { get; set; }
    public string ProductDescription { get; set; }
    public string FurtherDescription { get; set; }
    public string ImageAltTag { get; set; }
    public string TagAttributes { get; set; }
    public string PageTitle { get; set; }
    public string MetaKeyword { get; set; }
    public string MetaDescription { get; set; }
    public string Action { get; set; }
    public string Result { get; set; }
    public string ProductLanguageIdCSV { get; set; }
    public string ProductNameWithCode { get; set; }
    public string ProductSKUName { get; set; }
    public string SKUcode { get; set; }
    public decimal rating { get; set; }
    public int stocklevel { get; set; }
    public string CatalogueId_csv { get; set; }
    /*Sachin Chauhan : Start : 05 04 2016 : Taking out below properties from Product pricebreak level to product entity level.
                                            Both properties are used for filtering out relevent products from quickshop product listing.
     */
    public bool IsShowPriceAndEnquiry { get; set; }
    public bool IsCallForPrice { get; set; }
    /*Sachin Chauhan : End : 05 04 2016*/

    #region Indeed Code
    /* Added By Snehal  START- Indeed Product User Input - 16 03 2017 */
    public bool IsUserInput { get; set; }
    public bool IsMandatory { get; set; }
    /* END */
    
    #endregion

    public ProductBE()
    {
        PropPriceBreaks = new PriceBreaks();
        PropPriceBreakDetails = new PriceBreakDetails();
        PropGetAllVariantType = new List<ProductVariantType>();
        PropProductVariantType = new ProductVariantType();
        PropProductVariants = new ProductVariants();
        PropProductSKU = new ProductSKU();
        PropGetAllProductVariants = new List<ProductVariants>();
        PropGetAllProductSKU = new List<ProductSKU>();
        PropGetAllPriceBreak = new List<PriceBreaks>();
        PropGetAllPriceBreakDetails = new List<PriceBreakDetails>();
        PropGetAllProductDetailGroupMappings = new List<ProductDetailGroupMappings>();
        PropGetAllProductDetailGroups = new List<ProductDetailGroups>();
        PropGetAllProductDetails = new List<ProductDetails>();
        PropGetAllRelatedProducts = new List<RelatedProducts>();
        PropGetAllRecentlyViewedProducts = new List<RecentlyViewedProducts>();
        PropGetAllProductColorVariantMapping = new List<ProductColorVariantMappping>();
        PropProductEnquiry = new ProductEnquiry();
        PropGetAllProdictEnquiryFieldsConfiguration = new List<ProductEnquiryFieldsConfiguration>();
        PropGetAllYouMayAlsoLikeProducts = new List<YouMayAlsoLike>();
        PropCustomRequest = new CustomRequest();
    }

    public ProductVariantType PropProductVariantType { get; set; }
    public ProductVariants PropProductVariants { get; set; }
    public PriceBreaks PropPriceBreaks { get; set; }
    public PriceBreakDetails PropPriceBreakDetails { get; set; }
    public ProductDetailGroups PropProductDetailGroups { get; set; }
    public ProductDetailGroupMappings PropProductDetailGroupMappings { get; set; }
    public ProductDetails PropProductDetails { get; set; }
    public RelatedProducts PropRelatedProducts { get; set; }
    public ColorMaster PropColorMaster { get; set; }
    public ProductColorMapping PropProductColorMapping { get; set; }
    public ProductCurrencyMapping PropProductCurrencyMapping { get; set; }
    public ProductSKU PropProductSKU { get; set; }
    public List<PriceBreakDetails> PropGetAllPriceBreakDetails { get; set; }
    public List<PriceBreaks> PropGetAllPriceBreak { get; set; }
    public List<ProductVariantType> PropGetAllVariantType { get; set; }
    public List<ProductVariants> PropGetAllProductVariants { get; set; }
    public List<ProductSKU> PropGetAllProductSKU { get; set; }
    public List<ProductDetailGroups> PropGetAllProductDetailGroups { get; set; }
    public List<ProductDetailGroupMappings> PropGetAllProductDetailGroupMappings { get; set; }
    public List<ProductDetails> PropGetAllProductDetails { get; set; }
    public List<RelatedProducts> PropGetAllRelatedProducts { get; set; }
    public List<RecentlyViewedProducts> PropGetAllRecentlyViewedProducts { get; set; }
    public List<ProductSize> PropGetAllProductSize { get; set; }
    public List<ProductElements> PropGetAllProductElements { get; set; }
    public List<ProductUOM> PropGetAllProductUOM { get; set; }
    public List<ProductAttributes> PropGetAllProductAttributes { get; set; }
    public List<ProductColorVariantMappping> PropGetAllProductColorVariantMapping { get; set; }
    public ProductEnquiry PropProductEnquiry { get; set; }
    public List<ProductEnquiryFieldsConfiguration> PropGetAllProdictEnquiryFieldsConfiguration { get; set; }
    public List<YouMayAlsoLike> PropGetAllYouMayAlsoLikeProducts { get; set; }
    public CustomRequest PropCustomRequest { get; set; }
    public List<ProductImage> PropGetAllProductImages { get; set; }
    public string ProductCost { get; set; }


    public partial class Product : EntityBase
    {


    }

    [Serializable]
    public partial class ProductVariantType : EntityBase
    {
        public Int16 ProductVariantTypeId { get; set; }
        public bool ShowVariant { get; set; }
        public bool IsHiRes { get; set; }
        public Int16 Sequence { get; set; }
        public bool IsActive { get; set; }
        //public Int16 ProductVariantTypeLanguageId { get; set; }
        public string ProductVariantTypeName { get; set; }
        public bool ShowVariant1 { get; set; }
        public string VideoLink { get; set; }
    }

    [Serializable]
    public partial class ProductVariants : EntityBase
    {
        public int ProductVariantId { get; set; }
        public int ProductId { get; set; }
        public Int16 ProductVariantTypeId { get; set; }
        public string ImageName { get; set; }
        public bool IsActive { get; set; }
        //public int ProductVariantLanguageId { get; set; }
        public string ProductVariantName { get; set; }
        public string VideoLink { get; set; }
    }

    [Serializable]
    public partial class PriceBreaks : EntityBase
    {
        public int PriceBreakId { get; set; }
        public int ProductId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int MaximumQuantity { get; set; }
        public bool EnableStrikePrice { get; set; }
        public bool IsRegularPrice { get; set; }
        //public int PriceBreakLanguageId { get; set; }
        public string PriceBreakName { get; set; }
        public string StrikePriceBreakName { get; set; }
        public string NoteText { get; set; }
        public string QuantityUnit { get; set; }
        public string PriceUnit { get; set; }
        public string ProductionTime { get; set; }
        public string FOBPoints { get; set; }
        public Int16 CurrencyId { get; set; }
        public bool IsCallForPrice { get; set; }
        public bool IsShowPriceAndEnquiry { get; set; }
        public char Action { get; set; }
    }

    [Serializable]
    public partial class PriceBreakDetails : EntityBase
    {
        public int PriceBreakDetailId { get; set; }
        public int PriceBreakId { get; set; }
        public int BreakFrom { get; set; }
        public int BreakTill { get; set; }
        public decimal Price { get; set; }
        public decimal StrikePrice { get; set; }
        public string DiscountCode { get; set; }
        public bool IsActive { get; set; }
        public char Action { get; set; }
    }

    [Serializable]
    public partial class ProductDetailGroups : EntityBase
    {
        public int ProductDetailGroupId { get; set; }
        public bool IsActive { get; set; }
        //public int ProductDetailGroupLanguageId { get; set; }
        public string ProductDetailGroupName { get; set; }
    }

    [Serializable]
    public partial class ProductDetailGroupMappings : EntityBase
    {
        public int ProductDetailGroupMappingId { get; set; }
        public int ProductDetailGroupId { get; set; }
        public int Sequence { get; set; }
        //public int ProductDetailGroupMappingLanguageId { get; set; }
        public string DetailTitle { get; set; }
        public int MyProperty { get; set; }

    }

    [Serializable]
    public partial class ProductDetails : EntityBase
    {
        public int ProductDetailId { get; set; }
        public int ProductId { get; set; }
        public int ProductDetailGroupMappingId { get; set; }
        //public int ProductDetailLanguageId { get; set; }
        public string DetailDescription { get; set; }
    }

    [Serializable]
    public partial class RelatedProducts : EntityBase
    {
        public int RPId { get; set; }
        public int ProductId { get; set; }
        public int RelatedProductId { get; set; }
    }

    [Serializable]
    public partial class ColorMaster : EntityBase
    {
        public Int16 ColorMasterId { get; set; }
        public bool IsActive { get; set; }
        //public int ColorMasterLanguageId { get; set; }
        public string ColorName { get; set; }
    }

    [Serializable]
    public partial class ProductColorMapping : EntityBase
    {
        public int ProductColorMappingId { get; set; }
        public Int16 ColorMasterId { get; set; }
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
        public string ColorHexValue { get; set; }
        public string ImageName { get; set; }
        public bool IsActive { get; set; }
    }

    [Serializable]
    public partial class ProductCurrencyMapping : EntityBase
    {
        public int ProductCurrencyMappingId { get; set; }
        public int ProductId { get; set; }
        public Int16 CurrencyId { get; set; }
        public bool IsActive { get; set; }
    }

    [Serializable]
    public partial class ProductSKU : EntityBase
    {
        public int ProductSKUId { get; set; }
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal MaxPrice { get; set; }
        public decimal MaxstrikePrice { get; set; }
        public string SKU { get; set; }
        public string SKUName { get; set; }
        public Int16 GroupId { get; set; }
        public int BASYSProductId { get; set; }
        public string CatalogueAlias { get; set; }
        public Int16 SupplierId { get; set; }
        public int LanguageId { get; set; }
        public Int16 TrimColorId { get; set; }
        public Int16 BaseColorId { get; set; }
        public string VatCode { get; set; }
        public decimal DimensionalWeight { get; set; }
        public string UNSPSC_Code { get; set; }
        public int MinimumOrderQuantity { get; set; }
        public int MaximumOrderQuantity { get; set; }
        public int Inventory { get; set; }
    }

    [Serializable]
    public partial class RecentlyViewedProducts : EntityBase
    {
        public int UserId { get; set; }
        public string SessionId { get; set; }
        public Int16 CurrencyId { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MinstrikePrice { get; set; }
        public string DiscountCode { get; set; }
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string DefaultImageName { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string SubSubCategoryName { get; set; }
        public string SectionIds { get; set; }
        public string SKUcode { get; set; }
    }

    [Serializable]
    public partial class SizeGuide : EntityBase
    {
        public int ProductSizeGuideId { get; set; }
        public int ProductId { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }
        public string Column5 { get; set; }
        public string Column6 { get; set; }
        public string Column7 { get; set; }
        public string Column8 { get; set; }
        public string Column9 { get; set; }
        public string Column10 { get; set; }
    }
    #endregion

    [Serializable]
    public partial class ProductSize : EntityBase
    {
        public int SizeId { get; set; }
        public string Size { get; set; }
        public bool IsActive { get; set; }
    }

    [Serializable]
    public partial class ProductElements : EntityBase
    {
        public int ElementId { get; set; }
        public string Element { get; set; }
        public bool IsActive { get; set; }
    }

    [Serializable]
    public partial class ProductUOM : EntityBase
    {
        public int UOMId { get; set; }
        public string UOM { get; set; }
        public bool IsActive { get; set; }
    }

    [Serializable]
    public partial class ProductAttributes : EntityBase
    {
        public int ProductSKUId { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public int SizeId { get; set; }
        public int ElementId1 { get; set; }
        public int UOMIdA1 { get; set; }
        public string ValueA1 { get; set; }
        public int UOMIdA2 { get; set; }
        public string ValueA2 { get; set; }
        public int ElementId2 { get; set; }
        public int UOMIdB1 { get; set; }
        public string ValueB1 { get; set; }
        public int UOMIdB2 { get; set; }
        public string ValueB2 { get; set; }
        public bool IsActive { get; set; }
        public int TotalRow { get; set; }
    }
    [Serializable]
    public partial class ProductImportHistory : EntityBase
    {
        public int TotalRecords { get; set; }
        public int FailedRecords { get; set; }
        public string UploadedFileName { get; set; }
        public string ErrorFileName { get; set; }
        public DateTime ImportDate { get; set; }
    }


    [Serializable]
    public partial class ProductEnquiry : EntityBase
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public string EmailAddress { get; set; }
        public string QuantityRequired { get; set; }
        public DateTime DeliveryDateRequired { get; set; }
        public string Branding { get; set; }
        public string LogoFileName { get; set; }
        public string AdditionalInformation { get; set; }
        public string CustomColumn1 { get; set; }
        public string CustomColumn2 { get; set; }
        public string CustomColumn3 { get; set; }
        public string CustomColumn4 { get; set; }
        public string CustomColumn5 { get; set; }
        public string Color { get; set; }
    }

    [Serializable]
    public partial class ProductEnquiryFieldsConfiguration : EntityBase
    {
        public Int16 FieldType { get; set; }
        public string SystemColumnName { get; set; }
        public bool IsVisible { get; set; }
        public bool IsMandatory { get; set; }
        public Int16 DisplayOrder { get; set; }
        public string FieldTypeName { get; set; }
        public Int16 LanguageId { get; set; }
        public string LabelTitle { get; set; }
    }

    [Serializable]
    public partial class YouMayAlsoLike : EntityBase
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string DefaultImageName { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MinstrikePrice { get; set; }
        public string SectionIds { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string SubSubCategoryName { get; set; }

    }






    [Serializable]
    public partial class ProductColorVariantMappping : EntityBase
    {
        public int ProductId { get; set; }
        public int MappedProductId { get; set; }
    }

    [Serializable]
    public partial class ReviewRating : EntityBase
    {
        public string EmailId { get; set; }
        public int ProductId { get; set; }
        public decimal Rating { get; set; }
        public string ReviewTitle { get; set; }
        public string Review { get; set; }
        public char Action { get; set; }
        public int ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int TotalReviewCount { get; set; }
        public int RowNumber { get; set; }
        public decimal AvgRating { get; set; }
        public int TotalCount { get; set; }
        public string ProductName { get; set; }
        public int ReviewRatingId { get; set; }

    }

    [Serializable]
    public partial class CustomRequest : EntityBase
    {
        public string ContactName { get; set; }
        public string EmailId { get; set; }
        public string PhoneNumber { get; set; }
        public string Event { get; set; }
        public string TargetAudience { get; set; }
        public string Ideas { get; set; }
        public string Quantity { get; set; }
        public string Budget { get; set; }
        public string Colour { get; set; }
        public string Branding { get; set; }
        public string DeliveryAddress { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string AdditionalComments { get; set; }
    }

    [Serializable]
    public partial class ProductImage : EntityBase
    {
        public int ProductCount { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public Int16 ProductVariantTypeId { get; set; }
        public bool IsActive { get; set; }
        public string ProductVariantTypeName { get; set; }
        public int ProductVariantId { get; set; }
        public int ProductId { get; set; }
        public string ImageName { get; set; }
        public string ProductVariantName { get; set; }
        public string Thumbnail { get; set; }
        public string Medium { get; set; }
        public string Large { get; set; }
        public string HiRes { get; set; }
    }
    [Serializable]
    public partial class ProductWishList : EntityBase
    {
        public int WishListId { get; set; }
        public int Action { get; set; }
        public int ProductCount { get; set; }
        public string ProductCode { get; set; }
        public int ProductSKUId { get; set; }
        public string ProductName { get; set; }
        public Int16 ProductVariantTypeId { get; set; }
        public bool IsActive { get; set; }
        public string ProductVariantTypeName { get; set; }
        public int ProductVariantId { get; set; }
        public int ProductId { get; set; }
        public string ImageName { get; set; }
        public string ProductVariantName { get; set; }
        public int MinimumOrderQuantity { get; set; }
        public int MaximumOrderQuantity { get; set; }
        public int Quantity { get; set; }
        public string Note { get; set; }
        public Boolean Processed { get; set; }
    }

}


public partial class YouMayAlsoLikeMappingBE
{
    public List<YouMayAlsoLikecategory> lstYouMayAlsoLikecategory { get; set; }
    public List<YouMayAlsoLikeprice> lstYouMayAlsoLikeprice { get; set; }
    public List<YouMayAlsoLikeproduct> lstYouMayAlsoLikeproduct { get; set; }
   



    public class YouMayAlsoLikecategory
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string DefaultImageName { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MinstrikePrice { get; set; }
        public string SectionIds { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string SubSubCategoryName { get; set; }

    }


    public class YouMayAlsoLikeproduct
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string DefaultImageName { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MinstrikePrice { get; set; }
        public string SectionIds { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string SubSubCategoryName { get; set; }

    }


    public class YouMayAlsoLikeprice
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string DefaultImageName { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MinstrikePrice { get; set; }
        public string SectionIds { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string SubSubCategoryName { get; set; }

    }

}