﻿using System;
using System.Collections.Generic;


public class GiftCertificateOrderBE
{
    public GiftCertificateOrderBE()
    {
        lstGiftOrderDetails = new List<GiftOrderDetailsBE>();
    }
    public int GiftOrderID { get; set; }
    public string PreDefinedColumn1 { get; set; }
    public string PreDefinedColumn2 { get; set; }
    public string PreDefinedColumn3 { get; set; }
    public string PreDefinedColumn4 { get; set; }
    public string PreDefinedColumn5 { get; set; }
    public string PreDefinedColumn6 { get; set; }
    public string PreDefinedColumn7 { get; set; }
    public string PreDefinedColumn8 { get; set; }
    public string PreDefinedColumn9 { get; set; }
    public string FriendlyName { get; set; }
    public bool IsActive { get; set; }
    public int CreatedBy { get; set; }
    public int ModifiedBy { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime ModifiedDate { get; set; }
    public string IPAddress { get; set; }
    public int GiftOASISID { get; set; }

    public List<GiftOrderDetailsBE> lstGiftOrderDetails { get; set; }
    public class GiftOrderDetailsBE
    {
        public int GiftOrderDetailsID { get; set; }
        public int GiftOrderID { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
        public string RecipientEmail { get; set; }
        public string PersonalMessage { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}

