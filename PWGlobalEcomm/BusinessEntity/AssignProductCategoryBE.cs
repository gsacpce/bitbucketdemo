﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    
    public partial class CategoryBE : EntityBase
    {
        public int ProductCategory { get; set; }
        public bool IsDefault { get; set; }
        public int CategoryCount { get; set; }
    }
