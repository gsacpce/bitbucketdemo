﻿using System;

[Serializable]
public class LandingPageConfigurationBE
{
    #region "Properties"
    public int ID { get; set; }
    public string LandingPageText { get; set; }
    public string ImagePath { get; set; }
    public int LanguageID { get; set; }
    public bool IsActive { get; set; }
    #endregion
}

