﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public partial class FeatureBE:EntityBase
{
    public FeatureBE()
    {
 
    }
    public Int16 FeatureId { get; set; }
    public string FeatureName { get; set; }
    public string FeatureAlias { get; set; }
    public string FeatureDescription { get; set; }
    public Int16 FeatureParentId { get; set; }
    public Int16 DependentFeatureId { get; set; }
    public bool IsActive { get; set; }
    public List<FeatureValueBE> FeatureValues { get; set; }

    [Serializable]
    public class FeatureValueBE : EntityBase
    {
        public Int64 StoreFeatureDetailId { get; set; }
        public Int16 FeatureValueId { get; set; }
        public Int16 FeatureId { get; set; }
        public string FeatureValue { get; set; }
        public string DefaultValue { get; set; }
        public bool IsActive { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDefault { get; set; }
        public string FeatureDefaultValue { get; set; }
        public Int16 StoreId { get; set; }
    }
}


