﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class BrandColorManagementBE : EntityBase
{
    #region Properties
    public int BrandColorId { get; set; }
    public string ColorName { get; set; }
    public string ColorHexCode { get; set; }
    public bool IsActive { get; set; }
    public Int16 rownumber { get; set; }
    #endregion


    public string ExColorCode { get; set; }
}
