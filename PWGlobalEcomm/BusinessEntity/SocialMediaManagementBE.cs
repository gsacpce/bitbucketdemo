﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class SocialMediaManagementBE : EntityBase
{

    public Int16 SocialMediaId { get; set; }
    public string SocialMediaPlatform { get; set; }
    public string IconExtension { get; set; }
    public string HoverIconExtension { get; set; }
    public string SocialMediaUrl { get; set; }
    public bool IsActive { get; set; }
 
    public Int16 Action { get; set; }


  
}

public class SocialManagementSequenceBE
{
    public Int16 SocialMediaId { get; set; }
    public Int16 DisplayOrder { get; set; }
}