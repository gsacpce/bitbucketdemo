﻿using System;

[Serializable]
public partial class CategoryBE : EntityBase
{
    public Int16 CategoryId { get; set; }

    public Int16 SubCategoryId { get; set; }

    public Int16 SubSubCategoryId { get; set; }

    public Int16 ParentCategoryId { get; set; }

    public Int16 RelatedCategoryId { get; set; }
   
    public string ImageExtension { get; set; }

    public Int16 Sequence { get; set; }

    public bool IsActive { get; set; }

    public string ParentCategoryName { get; set; }

    public string CategoryName { get; set; }

    public int ProductCount { get; set; }/*User Type*//*Modified the datatype*/

    public string Type { get; set; }

    public string CategoryRelation { get; set; }

    public string BannerText { get; set; }

    public string BannerLink { get; set; }

    public string MenuImageExtension { get; set; }

    public string MenuMouseoverExtension { get; set; }

    public string BannerImageExtension { get; set; }
    
    public string DescriptionText { get; set; }

    public string MetaTitle { get; set; }

    public string MetaDescription { get; set; }

    public string MetaKeywords { get; set; }

    public string Action { get; set; }

    public int CatId { get; set; }
    
    
    public Int16 RCategoryId { get; set; }


    public bool UserTypeStatus { get; set; }
    public Int32 UserTypeId { get; set; }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 05-08-16
    /// Scope   : properties of a category
    /// </summary>

    public Int16 R_CategoryId { get; set; }
    public Int16 ChildCategoriesCount { get; set; }
    public string ParentCategoryImageName { get; set; }
    public string ParentCategoryImgExt { get; set; }
    public string ParentCategoryBannerImageName { get; set; }
    public string ParentCategoryBannerImgExt { get; set; }
}

