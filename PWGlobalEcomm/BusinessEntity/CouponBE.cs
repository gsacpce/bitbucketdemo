﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


[Serializable]
public partial class CouponBE : EntityBase
{

    public int CouponID { get; set; }
    // public int CatalogueID { get; set; }
    public string CouponCode { get; set; }
    public float MinimumOrderValue { get; set; }
    public float MaximumOrderValue { get; set; }
    public string BehaviourDescription { get; set; }
    public string BehaviourType { get; set; }
    public string DiscountPercentage { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public bool IsActive { get; set; }
    public string Usage { get; set; }
    public string Region { get; set; }
    public string ShipmentType { get; set; }
    public int ReCalculateTax { get; set; }
    public int BehaviourId { get; set; }
    public int PageCount { get; set; }
    public int TotalRow { get; set; }
    public int PageIndex { get; set; }
    public string User { get; set; }
    public string UserType { get; set; }
    //Added by Anoop on 13th Oct
    public bool IsExpired { get; set; }
    public bool IsValidOrderValue { get; set; }
    public bool IsValidRegion { get; set; }
    public bool IsValidShipmentType { get; set; }
    public float DiscountAmount { get; set; }
    public float OrderValue { get; set; }

    public bool IsReadOnly { get; set; }
    public bool IsDeletable { get; set; }

    public partial class CouponCurrencyBE : EntityBase
    {
        public int CouponMinMaxId { get; set; }
        public int CouponID { get; set; }
        public double MinimumOrderValue { get; set; }
        public double MaximumOrderValue { get; set; }
        public int CurrencyId { get; set; }
    }
   
}

