﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class CurrencyBE:EntityBase
{
    #region Properties

    public Int16 CurrencyId { get; set; }
    public string CurrencyName { get; set; }
    public string CurrencyCode { get; set; }
    public string AdflexCurrencyCode { get; set; }
    public string CurrencySymbol { get; set; }
    public bool IsDefault { get; set; }
    public bool IsActive { get; set; }

    #region Indeed Code
    public decimal PointValue { get; set; }
    public string InvoiceID { get; set; }
    public int ProjectedSale { get; set; } 
    #endregion

    #endregion
}
