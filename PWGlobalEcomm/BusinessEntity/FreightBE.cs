﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public partial class FreightBE : EntityBase
{
    public float WeightUnits { get; set; }
    public float FreightMultiplier { get; set; }
    public float CurrencyMultiplier { get; set; }
    public string ActualWeight { get; set; }
    public string CountryCode { get; set; }
    public string CountryName { get; set; }
    public string StandardZone { get; set; }
    public string ExpressZone { get; set; }
    public string SZPrice { get; set; }
    public string EZPrice { get; set; }
    public string CSZWeight { get; set; }
    public string CEZWeight { get; set; }
}
