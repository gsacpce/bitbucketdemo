﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class EmailManagementBE : EntityBase
{
    public Int16 EmailTemplateId { get; set; }
    public string EmailTemplateName { get; set; }
    public string FromEmailId { get; set; }
    public string CCEmailId { get; set; }
    public string BCCEmailId { get; set; }
    public string SettingValue { get; set; }
    public bool IsHtml { get; set; }
    public bool IsActive { get; set; }
    public Int16 EmailTemplateLanguageId { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
    public string AttachmentFile { get; set; }
    public string TemplateFileName { get; set; }

    public class FromEmailBE
    {
        /* Added By Hardik on 28/FEB/2017 */
        public int Id { get; set; }
        public string FromEmailIdAlias { get; set; }
    }

}

