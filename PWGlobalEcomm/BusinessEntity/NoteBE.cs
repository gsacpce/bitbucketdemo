﻿using System;
using System.Collections.Generic;


[Serializable]
public class NoteBE
{
    public Int16 NoteId { get; set; }
    public Int16 NoteNo { get; set; }
    public string NoteDate { get; set; }
    public string Note { get; set; }
    public bool Status { get; set; }
    public String CreatedBy { get; set; }
}
