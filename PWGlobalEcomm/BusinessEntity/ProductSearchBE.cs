﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class ProductSearchBE
{
    public Int32 RowNumber { get; set; }
    public Int32 ProductId { get; set; }
    public string ProductCode { get; set; }
    public string ProductName { get; set; }
    public Int16 CurrencyId { get; set; }
    public Int16 LanguageId { get; set; }

    public decimal AsLowAsPrice { get; set; }
    public string DefaultImageName { get; set; }
}
