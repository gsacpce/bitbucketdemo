﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
class GoogleURLShortner
{
    public string Kind { get; set; }
    public string id { get; set; }
    public string LongUrl { get; set; }
}

