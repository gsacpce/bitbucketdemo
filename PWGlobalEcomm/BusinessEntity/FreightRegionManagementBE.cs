﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    [Serializable]
    public partial class FreightRegionManagementBE : EntityBase
    {
        #region
        public List<ShipmentMethodMasterBE> FreightShipmentMethodMasterLst { get; set; }
        public List<ShipmentMethodLanguagesBE> FreightShipmentMethodLanguagesBELst { get; set; }
        public List<RegionServiceShipmentBE> FreightRegionServiceShipmentBELst { get; set; }
        public List<FreightRegionsBE> FreightRegionsBELst { get; set; }

        #endregion

        public FreightRegionManagementBE()
        {
            FreightShipmentMethodMasterLst = new List<ShipmentMethodMasterBE>();
            FreightShipmentMethodLanguagesBELst = new List<ShipmentMethodLanguagesBE>();
            FreightRegionServiceShipmentBELst = new List<RegionServiceShipmentBE>();
            FreightRegionsBELst = new List<FreightRegionsBE>();
        }
        
        [Serializable]
        public partial class ShipmentMethodMasterBE 
        {
            public int FreightModeId { get; set; }
            public string FreightModeName { get; set; }
            public string ServiceName { get; set; }
            public bool IsActive { get; set; }

        }
        public List<ShipmentMethodMasterBE> ShipmentMethodMaster { get; set; }


        [Serializable]
        public partial class ShipmentMethodLanguagesBE
        {
            #region Properties
            public int FreightModeLangaugeId { get; set; }
            public int RegionId { get; set; }
            public Int16 FreightModeId { get; set; }
            public Int16 LanguageId { get; set; }
            public string FreightModeName { get; set; }
            #endregion
        }
        public List<ShipmentMethodLanguagesBE> ShipmentMethodLanguages { get; set; }



        [Serializable]
        public partial class RegionServiceShipmentBE
        {
            public int ID { get; set; }
            public int FreightModeId { get; set; }
            public int RegionId { get; set; }
            //public string ShipmentName { get; set; }
            public DateTime CreatedDate { get; set; }
            public bool IsActive { get; set; }
        }
        public List<RegionServiceShipmentBE> RegionServiceShipment { get; set; }

        [Serializable]
        public partial class FreightRegionsBE
        {
            public int FreightRegionId { get; set; }
            public string FreightRegionName { get; set; }
            public bool IsActive { get; set; }
            public string OldRegionName { get; set; }
        }
        public List<FreightRegionsBE> FreightRegions { get; set; }
    }

