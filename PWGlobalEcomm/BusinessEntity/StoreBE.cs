﻿using System;
using System.Collections.Generic;

[Serializable]
public class StoreBE : EntityBase
{
    #region Properties

    public Int16 StoreId { get; set; }
    public string StoreName { get; set; }
    public string StoreAlias { get; set; }
    public string StoreLink { get; set; }
    public string DBName { get; set; }
    public string DBServerName { get; set; }
    public string DBUserName { get; set; }
    public string DBPassword { get; set; }
    public Int16 TemplateId { get; set; }
    public bool IsPunchOut { get; set; }
    public bool IsBASYS { get; set; }
    public Int16 CloneStoreId { get; set; }
    public Int16 ThumbnailsWidth { get; set; }
    public Int16 ThumbnailsHeight { get; set; }
    public Int16 Mediumwidth { get; set; }
    public Int16 Mediumheight { get; set; }
    public Int16 Zoomwidth { get; set; }
    public Int16 Zoomheight { get; set; }
    public string Description { get; set; }
    public byte StatusId { get; set; }
    public bool IsActive { get; set; }
    public bool IsCreated { get; set; }
    public string CloneStoreName { get; set; }
    public List<StoreLanguageBE> StoreLanguages { get; set; }
    public List<StoreCurrencyBE> StoreCurrencies { get; set; }
    public List<FeatureBE> StoreFeatures { get; set; }
    public List<EmailManagementBE.FromEmailBE> FromEmailIdAlias { get; set; }
    public string Version { get; set; }
    #endregion

    [Serializable]
    public class StoreLanguageBE
    {
        public Int16 StoreLanguageMappingId { get; set; }
        public Int16 StoreId { get; set; }
        public Int16 LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string LanguageAlias { get; set; }
        public string CultureCode { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public bool IsBASYSDefault { get; set; }
    }

    [Serializable]
    public class StoreCurrencyBE : EntityBase
    {
        public Int16 StoreDetailId { get; set; }
        public Int16 StoreId { get; set; }
        public Int16 CurrencyId { get; set; }
        public string GlobalCostCentre { get; set; }
        public Int16 GroupId { get; set; }
        public Int16 DivisionId { get; set; }
        public int DefaultCustomerId { get; set; }
        public string CatalogueAlias { get; set; }
        public Int16 KeyGroupId { get; set; }
        public Int16 SourceCodeId { get; set; }
        public string HelpDeskMailID { get; set; }
        public string DivisionName { get; set; }
        public string CatalogueName { get; set; }
        public string KeyGroupName { get; set; }
        public string SourceCodeName { get; set; }
        public Int16 SalesPersonId { get; set; }
        public bool IsDefault { get; set; }
        public int DefaultCompanyId { get; set; }
        public bool IsActive { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public string AdflexCurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal CurrencyConversionFactor { get; set; }
        public string GACode { get; set; }
        public int Default_CUST_CONTACT_ID { get; set; }
        public Int16 CatalogueId { get; set; }/*User Type*/
        public bool ValidForAdflex { get; set; }
    }

    public class ContactBE : EntityBase
    {
        public int Action { get; set; }
        public Int16 ContactId { get; set; }
        public Int16 ContactLanguageId { get; set; }
        public string HelpDeskEmail { get; set; }
        public string HelpDeskPhone { get; set; }
        public string HelpDeskContactPerson { get; set; }
        public string HelpDeskText { get; set; }
    }

    [Serializable]
    public class MetaTags : EntityBase
    {
        public string MetaContentTitle { get; set; }
        public string PageName { get; set; }
        public string MetaDescription { get; set; }
        public int MetaContentId { get; set; }
        public string MetaKeyword { get; set; }
        public int Action { get; set; }
    }

    public class StoreCurrency
    {
        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
    }


}


public partial class StoreReferral
{
    public List<referralurlBE> lstreferralurlBE { get; set; }


    public class referralurlBE
    {
        public int UsertypeID { get; set; }
        public string ReferralURL { get; set; }
    }
}

