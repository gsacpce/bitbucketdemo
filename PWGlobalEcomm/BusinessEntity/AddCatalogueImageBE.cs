﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
class AddCatalogueImageBE : EntityBase
    {
        public Int16 ID { get; set; }
        public string ImageName { get; set; }
        public string ImageExtension { get; set; }
        public string URL { get; set; }
        public string ThumbNail { get; set; }
    }

