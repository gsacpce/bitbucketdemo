﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public partial class FeatureBE : EntityBase 
{
    public bool IsDefault { get; set; }
    public bool IsEnabled { get; set; }
    public Int16 FeatureValueId { get; set; }
    public string FeatureValue { get; set; }

    public Int16 CurrencyId { get; set; }
    public string CurrencyName { get; set; }
    public string CurrencyCode { get; set; }

    public Int16 CountryId { get; set; }
    public string CountryName { get; set; }
    
}
