﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWGlobalEcomm.BusinessEntity
{
    public class StoreLanguageBE:EntityBase
    {
        #region Properties

        public Int16 StoreLanguageMappingId { get; set; }
        public Int16 StoreId { get; set; }
        public Int16 LanguageId { get; set; }
        public bool IsDefault { get; set; }

        #endregion
    }
}
