﻿using System;
using System.Collections.Generic;

[Serializable]
public class RegistrationCustomFieldBE : EntityBase
{
    public Int16 RegistrationFieldsConfigurationId { get; set; }
    public byte FieldType { get; set; }
    public byte ParentFieldGroup { get; set; }
    public string SystemColumnName { get; set; }
    public bool IsVisible { get; set; }
    public byte DisplayOrder { get; set; }
    public bool IsMandatory { get; set; }
    public bool IsActive { get; set; }
    public byte UserHierarchyId { get; set; }
    public string HierarchyName { get; set; }
    public string LabelTitle { get; set; }
    public string FieldTypeName { get; set; }
    public List<RegistrationFeildDataMasterBE> CustomFieldData { get; set; }
    #region UserType 30 July 2016
    public bool IsStoreDefault { get; set; }/*User Type*/
    public bool IsLoginField { get; set; }
    public bool IsUserEditable { get; set; } 
    #endregion
    [Serializable]
    public class RegistrationFeildDataMasterBE : EntityBase
    {
        public int RegistrationFieldDataMasterId { get; set; }
        public Int16 RegistrationFieldsConfigurationId { get; set; }
        public string RegistrationFieldDataValue { get; set; }
        public bool IsActive { get; set; }
    }

    [Serializable]
    public class RegistrationFeildTypeMasterBE : EntityBase
    {
        public byte FieldTypeId { get; set; }
        public string FieldTypeName { get; set; }
    }
}
