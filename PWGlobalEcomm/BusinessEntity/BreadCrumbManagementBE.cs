﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



public class BreadCrumbManagementBE : EntityBase
{
    public Int16 BreadCrumbLanguageId { get; set; }
    public Int16 BreadCrumbId { get; set; }
    public string BreadCrumbName { get; set; }
    public string BreadCrumb { get; set; }
    public string ParentCrumbName { get; set; }
    
    public Int16 ParentBreadCrumbId { get; set; }
    public string Link { get; set; }

    public string Key { get; set; }
}

