﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class UserPreferredBE : EntityBase
{
    public int UserPreferredId { get; set; }
    public int UserId { get; set; }
    public int PreferredLanguageId { get; set; }
    public int PreferredCurrencyId { get; set; }
    public string CurrencyName { get; set; }
    public string CurrencyCode { get; set; }
    public string CurrencySymbol { get; set; }

    public class UserTypeCurrency
    {
        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
    }
}
