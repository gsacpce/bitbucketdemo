﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class Freight_Carrier_ServiceBE : EntityBase
{
    public int SrNo { get; set; }
    public int CARRIER_SERVICE_ID { get; set; }
    public string CARRIER_SERVICE_TEXT { get; set; }
    public bool IsActive { get; set; }
    public DateTime CreatedDate { get; set; }
}

