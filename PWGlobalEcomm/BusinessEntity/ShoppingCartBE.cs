﻿using System;
using System.Collections.Generic;

[Serializable]
public partial class ShoppingCartBE
{
    public Int32 ShoppingCartProductId { get; set; }
    public Int32 ProductSKUId { get; set; }
    public Int32 ProductId { get; set; }
    public string ProductCode { get; set; }
    public string ProductName { get; set; }
    public string SKUName { get; set; }
    public string SKU { get; set; }
    public string DefaultImageName { get; set; }
    public Int32 Quantity { get; set; }
    public Int32 Inventory { get; set; }
    public Int32 MinimumOrderQuantity { get; set; }
    public Int32 MaximumOrderQuantity { get; set; }
    public Int32 MOV { get; set; }
    public Int32 AllowOrderIfOutOfStock { get; set; }
    public decimal Price { get; set; }
    public bool IsBackOrderAllowed { get; set; }
    public string UserSessionId { get; set; }
    public Int32 BASYSProductId { get; set; }
    public Int16 BaseColorId { get; set; }
    public Int16 TrimColorId { get; set; }
    public string DimensionalWeight { get; set; }
    public Int16 SupplierId { get; set; }//Added by Sripal
    public string FurtherDescription { get; set; }//Added by Sripal

}

[Serializable]
public partial class ShoppingCartAdditionalBE
{
    public double Additionalcharges { get; set; }//Added by Sripal
}


[Serializable]
public partial class ShoppingCartAddressesBE
{
    public Int32 ShoppingCartProductId { get; set; }
    public Int32 ProductSKUId { get; set; }
    public Int32 ProductId { get; set; }
    public string ProductCode { get; set; }
    public string ProductName { get; set; }
    public string SKUName { get; set; }
    public string SKU { get; set; }
    public string DefaultImageName { get; set; }
    public Int32 Quantity { get; set; }
    public Int32 Inventory { get; set; }
    public Int32 MinimumOrderQuantity { get; set; }
    public Int32 MaximumOrderQuantity { get; set; }
    public Int32 MOV { get; set; }
    public Int32 AllowOrderIfOutOfStock { get; set; }
    public decimal Price { get; set; }
    public bool ValidateInventory { get; set; }
    public string UserSessionId { get; set; }
    public Int32 BASYSProductId { get; set; }
    public Int16 BaseColorId { get; set; }
    public Int16 TrimColorId { get; set; }
}

[Serializable]
public partial class CustomerOrderBE
{
    public Int32 CustomerOrderId { get; set; }
    public Int32 OrderedBy { get; set; }
    public Int32 ApprovedBy { get; set; }
    public string InvoiceCompany { get; set; }
    public string InvoiceContactPerson { get; set; }
    public string InvoiceAddress1 { get; set; }
    public string InvoiceAddress2 { get; set; }
    public string InvoiceCity { get; set; }
    public string InvoicePostalCode { get; set; }
    public string InvoiceCountry { get; set; }
    public string InvoiceCountryCode { get; set; }
    public string InvoiceCountryName { get; set; }
    public string InvoiceCounty { get; set; }
    public string InvoicePhone { get; set; }
    public string InvoiceFax { get; set; }
    public string InvoiceEmail { get; set; }
    public string PaymentMethod { get; set; }
    public string PaymentMethodType { get; set; }
    public string PaymentTypeRef { get; set; }
    public string OrderRefNo { get; set; }
    public string AuthorizationId_OASIS { get; set; }
    public Int32 OrderId_OASIS { get; set; }
    public string TxnRefGUID { get; set; }
    public bool IsOrderCompleted { get; set; }
    public string DeliveryCompany { get; set; }
    public string DeliveryContactPerson { get; set; }
    public Int32 DeliveryAddressId { get; set; }
    public string DeliveryAddress1 { get; set; }
    public string DeliveryAddress2 { get; set; }
    public string DeliveryCity { get; set; }
    public string DeliveryPostalCode { get; set; }
    public string DeliveryCountry { get; set; }
    public string DeliveryCountryCode { get; set; }
    public string DeliveryCountryName { get; set; }
    public string DeliveryCounty { get; set; }
    public string DeliveryPhone { get; set; }
    public string DeliveryFax { get; set; }
    public string SpecialInstructions { get; set; }
    public string stCardScheme { get; set; }
    public string OrderCarrierId { get; set; }
    public string OrderCarrierName { get; set; }
    public Int16 CurrencyId { get; set; }
    public Int16 LanguageId { get; set; }
    public double StandardCharges { get; set; }
    public double ExpressCharges { get; set; }
    public double PalletCharges { get; set; }
    public double TotalTax { get; set; }
    public double handlingfee { get; set; }
    public DBAction action { get; set; }
    public string CouponCode { get; set; }
    public string BehaviourType { get; set; }
    public double DiscountPercentage { get; set; }
    public Int32 ProductCount { get; set; }
    public Int32 RowNumber { get; set; }
    public string OrderValue { get; set; }

    #region Added By Snehal For GC Report - 15 12 2016
    public string GiftCertificateCode { get; set; }
    public decimal GCAmount { get; set; }
    public string OtherPayment { get; set; }
    public decimal PaidAmount { get; set; }
    #endregion

    public DateTime CreatedDate { get; set; }
    public List<CustomerOrderProductsBE> CustomerOrderProducts { get; set; }
    public List<UDFValuesBE> UDFValues { get; set; }
    public List<CustomerOrderGiftCertificateBE> CustomerOrderGiftCertificate { get; set; }


    [Serializable]
    public partial class CustomerOrderProductsBE
    {
        public Int32 CustomerOrderProductId { get; set; }
        public Int32 CustomerOrderId { get; set; }
        public Int32 ProductSKUId { get; set; }
        public Int32 Quantity { get; set; }
        public decimal Price { get; set; }
        public string SKU { get; set; }
        public string ProductName { get; set; }
        public string DefaultImageName { get; set; }
        public string SKUName { get; set; }
        public string ProductCode { get; set; }
        public Int32 BASYSProductId { get; set; }
        public string BaseColorId { get; set; }
        public string TrimColorId { get; set; }
        public string SupplierId { get; set; }
        public string CatalogueAlias { get; set; }      
        public string UserInputText { get; set; } /* Indeed Code */
    }

    [Serializable]
    public partial class UDFValuesBE
    {
        public Int32 UDFValueID { get; set; }
        public Int32 UDFOasisID { get; set; }
        public string UDFValue { get; set; }
        public Int32 SequenceNo { get; set; }
        public Int32 CustomerOrderId { get; set; }
        public Int16 LanguageId { get; set; }/*User Type*/
        public bool IsPoints { get; set; } /* Indeed Code */
    }

    [Serializable]
    public partial class CustomerOrderGiftCertificateBE
    {
        public int CustomerOrderGiftCertificateId { get; set; }
        public int CustomerOrderId { get; set; }
        public string GiftCertificateCode { get; set; }
        public decimal GCAmount { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

[Serializable]
public partial class PaymentTypesBE
{
    public Int32 ID { get; set; }
    public Int32 PaymentTypeID { get; set; }
    public string PaymentTypeRef { get; set; }/*User Type*/
    public string DisplayName { get; set; }
    public string IsDefault { get; set; }
    public string PaymentMethod { get; set; }
    public Int32 CardDigitLength1 { get; set; }
    public Int32 CardDigitLength2 { get; set; }
    public Int32 CardDigitLength3 { get; set; }
    public bool IsShowOnWebsite { get; set; }
    public bool InvoiceAccountId { get; set; }
    public Int32 DivisionId { get; set; }
    public bool IsBudget { get; set; } /* Indeed Code */
    public bool IsPoint { get; set; } /* Indeed Code */
}

[Serializable]
public partial class AdFlexTransactionsBE
{
    public Int32 inTransId { get; set; }
    public Int32 inStoreId { get; set; }
    public Int32 inOrderId { get; set; }
    public Int32 inStatusCode { get; set; }
    public string stStatusMessage { get; set; }
    public string stTransRef { get; set; }
    public string stTxRefGUID { get; set; }
    public string stTxDate { get; set; }
    public string stAuthCode { get; set; }
    public string stCardNumber { get; set; }
    public string stCardScheme { get; set; }
    public DateTime dtCreationDateTime { get; set; }
    public string MID { get; set; }
    public Int32 EnhancedDataType { get; set; }
    public string PaymentTypeID { get; set; }
    public string ReqCardToken { get; set; }
}


[Serializable]
public partial class TeleCashTransactionsBE
{
    public int id { get; set; }
    public string OrderId { get; set; }
    public string approval_code { get; set; }
    public string ccbrand { get; set; }
    public string cccountry { get; set; }
    public string chargetotal { get; set; }
    public string currencycode { get; set; }
    public string fail_rc { get; set; }
    public string fail_reason { get; set; }
    public string hash_algorithm { get; set; }
    public string ipgTransactionId { get; set; }
    public string oid { get; set; }
    public string response_hash { get; set; }
    public string status { get; set; }
    public string timezone { get; set; }
    public string txndatetime { get; set; }
    public string txntype { get; set; }
    public string txndate_processed { get; set; }
    public string cardnumber { get; set; }
    public string bname { get; set; }
    
}

[Serializable]
public partial class TeleCashCurrencyBE
{
    public int CurrencyId { get; set; }
    public string Currencycode { get; set; }
    public string TeleCashcurrency { get; set; }   

}



[Serializable]
public partial class UDFBE
{
    public Int32 UDFOasisID { get; set; }
    public Int32 SequenceNo { get; set; }
    public Int32 PaymentTypeID { get; set; }
    public string Caption { get; set; }
    public string DisplayName { get; set; }/*User Type*/
    public string RuleType { get; set; }
    public string CaseRule { get; set; }
    public Int32 MinimumLength { get; set; }
    public Int32 MaximumLength { get; set; }
    public Int32 MinimumValue { get; set; }
    public Int32 MaximumValue { get; set; }
    public Int32 DecimalPlaces { get; set; }
    //public Int32 ID { get; set; }
    public bool IsVisible { get; set; }
    public Int32 DivisionId { get; set; }
    public bool IsMandatory { get; set; }
    public Int16 LanguageId { get; set; }/*User Type*/
    #region Indeed Code
    public bool IsBudget { get; set; }
    public bool IsPoint { get; set; }
    public bool PaymentIsBudget { get; set; }
    public bool PaymentIsPoint { get; set; } 
    #endregion
}

/* Added By Snehal - 05 01 2016 - Customer Ref UDF */
[Serializable]
public partial class UDFCustRefBE
{
    public Int32 CustomerRefID { get; set; }
    public string Caption { get; set; }
    public string DisplayName { get; set; }/*User Type*/
    public string CustomerRefName { get; set; }
    public Int32 MinimumLength { get; set; }
    public Int32 MaximumLength { get; set; }
    public bool IsVisible { get; set; }
    public bool IsMandatory { get; set; }
    public Int16 LanguageId { get; set; }
}

[Serializable]
public partial class PickListItemsBE
{
    public Int32 ID { get; set; }
    public Int32 PaymentTypeID { get; set; }
    public string DisplayName { get; set; }/*User Type*/
    public Int32 SequenceNo { get; set; }
    public string PayValue { get; set; }
    public string IsDefault { get; set; }
    public Int32 DivisionId { get; set; }
}

public partial class AbandonedShoppingCartBE
{
    public Int32 AbandonedShoppingCartProductId { get; set; }
    public Int32 ShoppingCartProductId { get; set; }
    public Int32 UserId { get; set; }
    public Int32 Quantity { get; set; }
    public Int16 CurrencyId { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime AbandonedDate { get; set; }
    public Int32 ProductSKUId { get; set; }
    public Int32 ProductCount { get; set; }
    public Int32 ProductId { get; set; }
    public string ProductCode { get; set; }
    public string ProductName { get; set; }
    public string SKUName { get; set; }
    public string SKU { get; set; }
    public string DefaultImageName { get; set; }
    public string Price { get; set; }
    public string TotalPrice { get; set; }
    public string UserSessionId { get; set; }
    public string EmailId { get; set; }
}

 