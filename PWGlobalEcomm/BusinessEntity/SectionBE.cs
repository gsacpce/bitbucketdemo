﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public  class SectionBE : EntityBase
{
    public bool IsActive { get; set; }
    public Char SectionType { get; set; }
    public Int16 SectionId { get; set; }
    public Int16 CurrencyId { get; set; }
    public Int16 SectionDataId { get; set; }
    public Int16 Id { get; set; }
    public Int16 SectionLanguageId { get; set; }
    public string IconImageExtension { get; set; }
    public string BannerImageExtension { get; set; }
    public string SectionName { get; set; }
    public string SectionDescription { get; set; }
    public string PageTitle { get; set; }
    public string MetaDescription { get; set; }
    public string MetaKeyword { get; set; }
    public string IconImageAltText { get; set; }
    public string BannerImageAltText { get; set; }
    public string LanguageName { get; set; }
    public string LanguageAlias { get; set; }
    public string CategoryId { get; set; }
    public string CategoryName { get; set; }
    public string ParentCategoryId { get; set; }
    public string r_categoryid { get; set; }
    public string Product { get; set; } 
    public string ProductId { get; set; }
    public string ProductIdCSV { get; set; }
    public string ActionType { get; set; }
    public string subCategoryname { get; set; }
    public string subSubcategoryname { get; set; }
    public string IconExt { get; set; }

    public List<SectionBE.HandlingFeesSection> HandlingFeesSections { get; set; }

    
    [Serializable]
    public partial class CustomSection : EntityBase
    {
        public string SectionIds { get; set; }
        //public int LanguageId { get; set; }
        public int CurrencyId { get; set; }
        public int toprow { get; set; }
    }

    [Serializable]
    public partial class HandlingFeesSection : EntityBase
    {
        public int HandlingFeeId  {get;set;}
        public int SectionId { get; set; }
        public int CurrencyId { get; set; }
        public decimal HandlingFees { get; set; }
        public Int16 PerOrderStatus { get; set; }
        public bool IsActive { get; set; }
    }
   
}