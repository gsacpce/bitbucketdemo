﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class StaticPageManagementBE : EntityBase
{
    public Int16 StaticPageId { get; set; }
    public string PageURL { get; set; }
    public Int16 NewWindow { get; set; }
    public Int16 ParentStaticPageId { get; set; }
    public bool IsLoginBased { get; set; }
    public Int16 PageType { get; set; }
    public bool IsActive { get; set; }
    public Int16 StaticPageLanguageId { get; set; }
    public string PageName { get; set; }
    public string WebsiteContent { get; set; }
    public string PageTitle { get; set; }
    public string MetaKeyword { get; set; }
    public string MetaDescription { get; set; }
    public string PageTypeText { get; set; }
    public bool IsParent { get; set; }
    public Int16 DisplayOrder { get; set; }
    public string DisplayLocation { get; set; }
    public string PageAliasName { get; set; }
    public bool IsSystemLink { get; set; }

    /// <summary>
    /// Author  : Anoop Gupta
    /// Date    : 12-08-16
    /// Scope   : Properties for the resource data
    /// </summary>        
    public Int32 ResourceId { get; set; }
    public string ResourceKey { get; set; }
    public string ResourceValue { get; set; }
    public Int32 ResourceLanguageId { get; set; }
    //public Int16 LanguageId { get; set; }

    
}
public class StaticPageManagementSequenceBE
{
    public Int16 DisplayOrder { get; set; }
    public Int16 StaticPageId { get; set; }
    public string DisplayLocation { get; set; }
}

/// <summary>
/// Author  : Hardik Gohil
/// Date    : 02/MARCH/2017
/// Scope   : Properties for the Danskebank Custom UDF's
/// </summary>
public class CustomUDFRegDataDanske
{
    public string Reg_Number { get; set; }
    public Int64 EAN_Number { get; set; }
    public Int64 DKK { get; set; }
    public Int64 NOK { get; set; }
    public Int64 SEK { get; set; }
    public Int64 EUR { get; set; }    	
}