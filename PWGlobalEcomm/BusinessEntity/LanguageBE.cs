﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class LanguageBE : EntityBase
{
    #region Properties

    public Int16 LanguageId { get; set; }
    public string LanguageName { get; set; }
    public string LanguageAlias { get; set; }
    public string CultureCode { get; set; }
    public bool IsDefault { get; set; }
    public bool IsActive { get; set; }

    #endregion
}

public class LanguageCodeMaster
{
    public string LanguageCode { get; set; }
    public Int16 LanguageId { get; set; }
}
