﻿using System;

[Serializable]
public class PunchoutDetailsBE : EntityBase
{
    public int PunchoutID { get; set; }
    public string BuyerCookie { get; set; }
    public string PunchoutRequest { get; set; }
    public string PunchoutResponse { get; set; }
    public DateTime PunchoutDateTime { get; set; }
    public bool IsPunchoutExpired { get; set; }
    public string PunchoutResponseBody { get; set; }
}
