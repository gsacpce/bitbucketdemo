﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class InvoiceBE : EntityBase
    { 
    public Int16 Id { get; set; }
    public int Division_Id { get; set; }
    public int Customer_Id { get; set; }
    public string Currency_Country { get; set; }
    public string Name { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string Town { get; set; }
    public string Country { get; set; }
    public string Post_Code { get; set; }
    public string Tax_Number { get; set; }
    public string Display_UDF { get; set; }
    public int Contact_Id { get; set; }


    }
