﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Reflection;
using System.Data.SqlClient;

/// <summary>
/// Summary description for EntityBase
/// </summary>
[Serializable]
public class EntityBase
{
    #region Properties

    /// <summary>
    ///  gets or sets CatalogueId
    /// </summary>
    public Int16 CatalogueId { get; set; }

    /// <summary>
    ///  gets or sets LanguageId
    /// </summary>
    public Int16 LanguageId { get; set; }

    /// <summary>
    ///  gets or sets CreatedBy
    /// </summary>
    public int CreatedBy { get; set; }

    /// <summary>
    ///  gets or sets CreatedDate
    /// </summary>
    public DateTime CreatedDate { get; set; }

    /// <summary>
    ///  gets or sets ModifiedBy
    /// </summary>
    public int ModifiedBy { get; set; }

    /// <summary>
    ///  gets or sets ModifiedDate
    /// </summary>
    public DateTime ModifiedDate { get; set; }

    #endregion
}
