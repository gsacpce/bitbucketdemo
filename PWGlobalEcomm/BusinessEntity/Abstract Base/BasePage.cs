﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Web.Configuration;
using System.Security;
using System.Reflection;
using PWGlobalEcomm.BusinessLogic;
using PWGlobalEcomm.DataAccess;

/// <summary>
/// Summary description for BasePage
/// </summary>
namespace Presentation
{
    public partial class BasePage : System.Web.UI.Page
    {

        public static bool AccessGoogle = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            int i = 0;
            i++;
        }

        #region Culture Setting

        /// <summary>
        /// The name of the culture selection dropdown list in the common header.
        /// </summary>
        public const string LnkBtnLanguageID = "lnkSiteLanguage";

        /*Sachin Chauhan Start : 09 02 2016 : Added constant for Site Currency Change*/
        public const string LnkBtnCurrencyId = "lnkSiteCurrency";
        /*Sachin Chauhan End : 0902 2016*/

        /// <summary>
        /// The name of the PostBack event target field in a posted form.  You can use this to see which
        /// control triggered a PostBack:  Request.Form[PostBackEventTarget] .
        /// </summary>
        public const string PostBackEventTarget = "__EVENTTARGET";

        /// <SUMMARY>
        /// Overriding the InitializeCulture method to set the user selected
        /// option in the current thread. Note that this method is called much
        /// earlier in the Page lifecycle and we don't have access to any controls
        /// in this stage, so have to use Form collection.
        /// </SUMMARY>
        protected override void InitializeCulture()
        {
            ///<remarks><REMARKS>
            ///Check if PostBack occured. Cannot use IsPostBack in this method
            ///as this property is not set yet.
            ///</remarks>
            ///
            StoreBE CurrentStore;
            if (HttpRuntime.Cache["StoreDetails"] == null)
            {
                /*Sachin Chauhan Start : 26 02 2016 : Changed below Store Details cache object creation from BL rather than DL */
                CurrentStore = StoreBL.GetStoreDetails();
                HttpRuntime.Cache.Insert("StoreDetails", CurrentStore);
                /*Sachin Chauhan End : 26 02 2016 */
            }
            else
                CurrentStore = (StoreBE)HttpRuntime.Cache["StoreDetails"];

            /*Sachin Chauhan Start : 26 02 2016 : Moved below code from home page and removed overhead object creations*/
            //List<StoreBE.StoreCurrencyBE> Storecurrency = lstStoreDetail.StoreCurrencies.FindAll(x => x.IsActive == true);
            if (Request[PostBackEventTarget] == null)
            {
                string strStoreAccess = CurrentStore.StoreFeatures.FirstOrDefault(x => x.FeatureName == "CR_StoreAccess").FeatureValues.FirstOrDefault(y => y.IsEnabled == true).FeatureValue;
                if (strStoreAccess.ToLower().Equals("anonymous login"))
                {
                    if (CurrentStore.StoreCurrencies.Count > 0 && Session["IsAnonymous"] == null)
                    {
                        if (Session["User"] == null)
                            Session["IsAnonymous"] = 1;
                    }
                    else if (CurrentStore.StoreCurrencies.Count.Equals(1))
                    {
                        GlobalFunctions.SetCurrencyId(CurrentStore.StoreCurrencies[0].CurrencyId);
                        GlobalFunctions.SetCurrencySymbol(CurrentStore.StoreCurrencies[0].CurrencySymbol);
                        Session["IsAnonymous"] = 0;

                    }
                }
                else
                {
                    if (Session["User"] == null)//2016_03_16
                    {
                        if (CurrentStore.StoreCurrencies.Count > 1)
                        {
                            GlobalFunctions.SetCurrencyId(CurrentStore.StoreCurrencies.FirstOrDefault(x => x.IsDefault.Equals(true) && x.IsActive.Equals(true)).CurrencyId);
                            GlobalFunctions.SetCurrencySymbol(CurrentStore.StoreCurrencies.FirstOrDefault(x => x.IsDefault.Equals(true) && x.IsActive.Equals(true)).CurrencySymbol);
                        }
                    }
                }


            }
            /*Sachin Chauhan End : 26 02 2016*/

            if (Request[PostBackEventTarget] != null)
            {
                string controlID = Request[PostBackEventTarget];

                if (controlID.Contains(LnkBtnLanguageID))
                {
                    string[] controlIdArr = controlID.Split('$');

                    string strLanguageId = controlIdArr[controlIdArr.Length - 1].ToString().Replace("lnkSiteLanguage", "");

                    /*Sachin chauhan Start : 21-11-15*/
                    //string selectedValue = Request.Form[Request[PostBackEventTarget]].ToString();
                    /*Sachin chauhan end : 21-11-15*/

                    GlobalFunctions.SetLanguageId(Convert.ToInt16(strLanguageId));

                    string cultureName, localeName;

                    //StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    cultureName = CurrentStore.StoreLanguages.Find(x => x.LanguageId.ToString().Equals(strLanguageId)).CultureCode;
                    localeName = cultureName;

                    SetCulture(cultureName, localeName);
                }

                /*Sachin Chauhan Start : 09 02 2016 : Change currency when user opts from top currency menu item*/
                if (controlID.Contains(LnkBtnCurrencyId))
                {
                    string[] controlIdArr = controlID.Split('$');

                    string strCurrencyParts = controlIdArr[controlIdArr.Length - 1].ToString().Replace("lnkSiteCurrency", "");

                    /*Controls id contains currency id & currency symbol | i.e. PIPE character seprated*/
                    string[] strCurrencyPart = strCurrencyParts.Split('|');


                    //string strCurrencySymbol = CurrentStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyCode.Equals(strCurrencyPart[1])).CurrencySymbol;
                    /*Only change currency if the selected currency is different than previous*/
                    if (!GlobalFunctions.GetCurrencyId().Equals(strCurrencyPart[0].ToString()))
                    {
                        Session["PrevCurrencyId"] = GlobalFunctions.GetCurrencyId();
                        GlobalFunctions.SetCurrencyId(strCurrencyPart[0].To_Int16());
                        GlobalFunctions.SetCurrencySymbol(CurrentStore.StoreCurrencies.FirstOrDefault(x => x.CurrencyCode.Equals(strCurrencyPart[1])).CurrencySymbol);
                    }

                    //string cultureName, localeName;
                    //StoreBE objStoreBE = StoreBL.GetStoreDetails();
                    //cultureName = objStoreBE.StoreLanguages.Find(x => x.LanguageId.ToString().Equals(strLanguageId)).CultureCode;
                    //localeName = cultureName;

                    //SetCulture(cultureName, localeName);
                }
                /*Sachin chauhan End : 09 02 2016*/

            }
            ///<remarks>
            ///Get the culture from the session if the control is tranferred to a
            ///new page in the same application.
            ///</remarks>
            if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
            {
                Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
                Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
            }
            base.InitializeCulture();
        }
        protected void CreateActivityLog(string PageName, string Activity, string ContentName)
        {
            try
            {
                UserBE objUSerBE = Session["StoreUser"] as UserBE;
                if (objUSerBE != null)
                {
                    if (objUSerBE.UserId > 0)
                    {
                        string EmailId = objUSerBE.EmailId;
                        GlobalFunctions.WriteActivityLog(EmailId, PageName, Activity, ContentName, objUSerBE.IPAddress, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        /// <Summary>
        /// Sets the current UICulture and CurrentCulture based on
        /// the arguments
        /// </Summary>
        /// <PARAM name="name"></PARAM>
        /// <PARAM name="locale"></PARAM>
        protected void SetCulture(string name, string locale)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(name);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(locale);
            ///<remarks>
            ///Saving the current thread's culture set by the User in the Session
            ///so that it can be used across the pages in the current application.
            ///</remarks>
            Session["MyUICulture"] = Thread.CurrentThread.CurrentUICulture;
            Session["MyCulture"] = Thread.CurrentThread.CurrentCulture;
        }

        #endregion
    }
}
