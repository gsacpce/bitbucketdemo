﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using PWGlobalEcomm.DataAccess;

/// <summary>
/// Summary description for DataAccessBase
/// </summary>
public abstract class DataAccessBase
{
    #region Constructor

    public DataAccessBase()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #endregion

    #region Concrete Method

    /// <summary>
    /// gets single specified object
    /// </summary>
    /// <param name="spName"> name of store procedure to be executed</param>
    /// <param name="param"> collection of paramaters of store procedure </param>
    /// <param name="objectName"> Specify the name of the entity to be filled.</param>
    /// <param name="IsTemplateConnectionString">Which connection string to use Template or Corporate Store</param>
    /// <returns>object</returns>
    public static object getItem(string spName, Dictionary<string, string> param, string objectName, bool IsStoreConnectionString)
    {
        object objectInstance = null;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    using (SqlDataReader ReaderInstance = CommandInstance.ExecuteReader())
                    {
                        if (ReaderInstance.HasRows)
                        {
                            DataTable dtSchemaTable = ReaderInstance.GetSchemaTable();
                            List<string> lstColNames = new List<string>();

                            foreach (DataRow drRow in dtSchemaTable.Rows)
                            {
                                lstColNames.Add(Convert.ToString(drRow["columnname"]).ToLower());
                            }

                            while (ReaderInstance.Read())
                            {
                                objectInstance = fillObject(ReaderInstance, lstColNames, objectName);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return objectInstance;
    }

    /// <summary>
    /// check in database whether particular thing is present or not 
    /// </summary>
    /// <param name="spName"> name of store procedure to be executed</param>
    /// <param name="param"> collection of paramaters of store procedure </param>
    /// <returns>bool value (true or false)</returns>
    public static bool IsExists(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
    {
        bool IsExists = false;
        string connectionString = string.Empty;
        try
        {
            int NumberOfRows;
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    NumberOfRows = Convert.ToInt32(CommandInstance.ExecuteScalar());
                    if (NumberOfRows == 0)
                        IsExists = false;
                    else
                        IsExists = true;
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return IsExists;
    }

    /// <summary>
    /// return int value 
    /// </summary>
    /// <param name="spName"> name of store procedure to be executed</param>
    /// <param name="param"> collection of paramaters of store procedure </param>
    /// <returns>Int value</returns>
    public static int getIntValue(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
    {
        int Value = 0;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    Value = Convert.ToInt32(CommandInstance.ExecuteScalar());
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return Value;
    }

    /// <summary>
    /// return string value 
    /// </summary>
    /// <param name="spName"> name of store procedure to be executed</param>
    /// <param name="param"> collection of paramaters of store procedure </param>
    /// <returns>string value</returns>
    public static string getStringValue(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
    {
        string strValue = string.Empty;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    strValue = Convert.ToString(CommandInstance.ExecuteScalar());
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return strValue;
    }

    /// <summary>
    /// return bool value 
    /// </summary>
    /// <param name="spName"> name of store procedure to be executed</param>
    /// <param name="param"> collection of paramaters of store procedure </param>
    /// <returns>bool value</returns>
    public static bool Insert(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
    {
        int intValue = 0;
        bool status = false;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                    if (intValue > 0)
                        status = true;
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return status;
    }

    public static bool Insert(string spName, Dictionary<string, DataTable> param, bool IsStoreConnectionString)
    {
        int intValue = 0;
        bool status = false;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, DataTable> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                    if (intValue > 0)
                        status = true;
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return status;
    }

    /// <summary>
    /// return bool value 
    /// </summary>
    /// <param name="spName"> name of store procedure to be executed</param>
    /// <param name="param"> collection of paramaters of store procedure </param>
    /// <returns>bool value</returns>
    public static void Insert(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString, int OutputSize = 0)
    {
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    if (paramOut != null)
                    {
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            SqlParameter parameterInstance = new SqlParameter();
                            parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                            if (OutputSize != 0)
                                parameterInstance.Size = OutputSize;
                            TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                            parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                            parameterInstance.Direction = ParameterDirection.Output;
                            CommandInstance.Parameters.Add(parameterInstance);
                        }
                    }
                    ConnectionInstance.Open();

                    CommandInstance.ExecuteReader();

                    Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                    foreach (KeyValuePair<string, string> para in paramOut)
                    {
                        paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                    }
                    paramOut = paramOutClone;
                    paramOutClone = null;
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    /// <summary>
    /// return string value 
    /// </summary>
    /// <param name="spName"> name of store procedure to be executed</param>
    /// <param name="param"> collection of paramaters of store procedure </param>
    /// <returns>string value</returns>
    public static bool Update(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
    {
        int intValue = 0;
        bool status = false;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                    if (intValue > 0)
                        status = true;
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return status;
    }


    public static void Update(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString, int OutputSize = 0)
    {
        Int32 intValue = 0;
        bool status = false;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }

                    if (paramOut != null)
                    {
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            SqlParameter parameterInstance = new SqlParameter();
                            parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                            if (OutputSize != 0)
                                parameterInstance.Size = OutputSize;
                            TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                            parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                            parameterInstance.Direction = ParameterDirection.Output;
                            CommandInstance.Parameters.Add(parameterInstance);
                        }
                    }

                    ConnectionInstance.Open();
                    intValue = CommandInstance.ExecuteNonQuery();


                    Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                    foreach (KeyValuePair<string, string> para in paramOut)
                    {
                        paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                    }
                    paramOut = paramOutClone;
                    paramOutClone = null;
                    //if (intValue > 0)
                    //    status = true;

                    //return intValue;

                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        //return status;
        //return intValue;
    }
    /// <summary>
    /// return string value 
    /// </summary>
    /// <param name="spName"> name of store procedure to be executed</param>
    /// <param name="param"> collection of paramaters of store procedure </param>
    /// <returns>string value</returns>
    public static bool Delete(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
    {
        int intValue = 0;
        bool status = false;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    ConnectionInstance.Open();
                    intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());
                    if (intValue > 0)
                        status = true;
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return status;
    }

    public static Int32 Delete(string spName, Dictionary<string, string> param, ref Dictionary<string, string> paramOut, bool IsStoreConnectionString, int OutputSize = 0)
    {
        int intValue = 0;
        bool status = false;
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                    {
                        foreach (KeyValuePair<string, string> para in param)
                        {
                            CommandInstance.Parameters.AddWithValue(string.Format("@{0}", para.Key), para.Value);
                        }
                    }
                    if (paramOut != null)
                    {
                        foreach (KeyValuePair<string, string> para in paramOut)
                        {
                            SqlParameter parameterInstance = new SqlParameter();
                            parameterInstance.ParameterName = string.Format("@{0}", para.Key);
                            if (OutputSize != 0)
                                parameterInstance.Size = OutputSize;
                            TypeConverter tc = TypeDescriptor.GetConverter(parameterInstance.DbType);
                            parameterInstance.DbType = (DbType)tc.ConvertFrom(Type.GetType(para.Value).Name);

                            parameterInstance.Direction = ParameterDirection.Output;
                            CommandInstance.Parameters.Add(parameterInstance);
                        }
                    }
                    ConnectionInstance.Open();
                    intValue = Convert.ToInt32(CommandInstance.ExecuteNonQuery());


                    Dictionary<string, string> paramOutClone = new Dictionary<string, string>();
                    foreach (KeyValuePair<string, string> para in paramOut)
                    {
                        paramOutClone.Add(para.Key, Convert.ToString(CommandInstance.Parameters[string.Format("@{0}", para.Key)].Value));
                    }
                    paramOut = paramOutClone;
                    paramOutClone = null;
                    if (intValue > 0)
                        status = true;

                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        //return status;
        return intValue;
    }

    /// <summary>
    /// returns specified object by reading SqlDataReader instance
    /// </summary>
    /// <param name="readerInstance">SqlDataReader value</param>
    /// <returns>object</returns>
    public static object fillObject(SqlDataReader readerInstance, List<string> lstColNames, string objectName)
    {
        object objectInstance = Activator.CreateInstance(Type.GetType(objectName));
        try
        {
            Type t = Type.GetType(objectName);
            foreach (var item in lstColNames)
            {
                PropertyInfo fi = t.GetProperty(item, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                object value = null;
                if (fi != null)
                {
                    try
                    {
                        value = Convert.ChangeType(readerInstance[item], fi.PropertyType);
                    }
                    catch (InvalidCastException)
                    {

                    }
                    fi.SetValue(objectInstance, value, null);
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorManager.AddExceptionToDB(string.Format("{0} --> {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), ex.Message, ex.StackTrace);
        }
        return objectInstance;
    }

    public static bool BulkInsert(DataTable dt, bool IsStoreConnectionString, string TableName)
    {
        string connectionString = string.Empty;
        if (IsStoreConnectionString)
            connectionString = Constants.strStoreConnectionString;
        else
            connectionString = Constants.strMCPConnectionString;
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            con.Open();
            DataTable table = dt;
            DataRow[] rowArray = table.Select();


            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(con, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null))
            {
                bulkcopy.DestinationTableName = TableName;
                //bulkcopy.DestinationTableName = "dbo.test";
                try
                {
                    bulkcopy.WriteToServer(rowArray);
                    SqlBulkCopyOptions.FireTriggers.ToString();
                }
                catch (Exception ex)
                {
                    Exceptions.WriteExceptionLog(ex);
                }
            }
            return true;
        }
    }

    /// <summary>
    /// Scope   : Get Data by csv file
    /// List 
    /// </summary>
    /// <param name="Code"></param>
    /// <param name="pagesize"></param>
    /// <param name="pageindex"></param>
    /// <returns>Get Data by csv file</returns>
    public static DataTable GetDataByCSV(string spName, string Param, bool IsStoreConnectionString)
    {
        DataTable dt = new DataTable();
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    CommandInstance.Parameters.AddWithValue("@colvalue", Param);
                    ConnectionInstance.Open();
                    SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                    da.Fill(dt);
                }
            }
        }
        catch (Exception ex) { }
        return dt;
    }

    internal static string selectQuery(DataTable dt)
    {
        if (dt != null && dt.Columns.Count > 0)
        {
            string strQuery = "select top 1 ";
            foreach (DataColumn dc in dt.Columns)
            {
                strQuery += ",[" + dc.Caption + "]";
            }
            strQuery = strQuery.Replace("select top 1 ,", "select top 1 ");
            strQuery += " from " + dt.TableName;
            return strQuery;
        }
        else
            return "select top 1 * from " + dt.TableName + " with (readpast)";
    }

    public static int BatchUpdate(DataSet dsData, bool IsStoreConnectionString)
    {
        string connectionString = string.Empty;
        if (IsStoreConnectionString)
            connectionString = Constants.strStoreConnectionString;
        else
            connectionString = Constants.strMCPConnectionString;
        int res = 1;
        if (dsData == null) return -1;
        using (SqlConnection myConnection = new SqlConnection(connectionString))
        {
            myConnection.Open();
            SqlTransaction transaction = myConnection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
            try
            {
                foreach (DataTable dt in dsData.Tables)
                {

                    if (dt != null && dt.GetChanges() != null && dt.GetChanges().Rows.Count > 0)
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        string strQuery = selectQuery(dt);
                        SqlCommand cmd = new SqlCommand(strQuery, myConnection, transaction);
                        da.SelectCommand = cmd;
                        //da.UpdateCommand = new SqlCommandBuilder(da).GetUpdateCommand();
                        SqlCommandBuilder cmb = new SqlCommandBuilder();
                        cmb.DataAdapter = da;
                        da.UpdateBatchSize = dt.Rows.Count;
                        da.Update(dt);
                    }
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                res = -1;
                transaction.Rollback();
            }
            finally
            {
                if (myConnection != null && myConnection.State == ConnectionState.Open)
                    myConnection.Close();
            }
        }
        return res;
    }

    public static DataTable GetDataforCustomerViewExcel(string spName, bool IsStoreConnectionString)
    {
        DataTable dt = new DataTable();
        string connectionString = string.Empty;
        try
        {
            if (IsStoreConnectionString)
                connectionString = Constants.strStoreConnectionString;
            else
                connectionString = Constants.strMCPConnectionString;

            using (SqlConnection ConnectionInstance = new SqlConnection(connectionString))
            {
                using (SqlCommand CommandInstance = new SqlCommand(spName, ConnectionInstance))
                {
                    CommandInstance.CommandTimeout = Convert.ToInt32(TimeOutSetting.SetTime);
                    CommandInstance.CommandType = CommandType.StoredProcedure;
                    ConnectionInstance.Open();
                    SqlDataAdapter da = new SqlDataAdapter(CommandInstance);
                    da.Fill(dt);
                }
            }
        }
        catch (Exception ex) { }
        return dt;
    }

    #endregion
}
