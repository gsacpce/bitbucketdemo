﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class GMAttribute
{
    public string itemtype { get; set; }
    public string datavalue { get; set; }
    public string runat { get; set; }
    public string id { get; set; }
    
}

