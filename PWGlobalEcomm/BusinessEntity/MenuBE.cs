﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class MenuBE : EntityBase
{
    #region Properties

    public Int16 MenuId { get; set; }
    public string MenuName { get; set; }
    public Int16 ParentMenuId { get; set; }
    public string MenuUrl { get; set; }
    public byte Level { get; set; }
    public string IconName { get; set; }
    public Int16 Sequence { get; set; }
    public bool IsVisible { get; set; }
    public bool IsActive { get; set; }

    #endregion
}
