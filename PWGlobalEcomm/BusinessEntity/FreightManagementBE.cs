﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class FreightManagementBE : EntityBase
{

    #region Constructor
    public FreightManagementBE()
    {
        //lstFreightMultiplerCurrency = new List<FreightMultiplerCurrency>();
        /*FreightModeId = new FreightMode();
        FreightRegionId = new FreightRegion();
        FreightMethodId = new FreightMethod();*/
    }
    #endregion

    #region Properties

    public int FreightConfigurationId { get; set; }
    public Int16 FreightRegionId { get; set; }
    public Int16 FreightMethodId { get; set; }
    public Int16 FreightModeId { get; set; }

    public Int16 CarrierServiceId { get; set; }
    public string CarrierServiceText { get; set; }
    //public Int16 TransitTime { get; set; }
    public string TransitTime { get; set; }
    public string CustomText { get; set; }
    public double MinOrderValue { get; set; }
    public double ValueAppliedAbove { get; set; }
    public double ValueAppliedBelow { get; set; }
    public double WeightBand { get; set; }
    public string PerBoxPerOrder { get; set; }
    public bool DisAllowOrderBelowMOV { get; set; }

    public float WeightUnits { get; set; }
    public float FreightMultiplier { get; set; }
    public float StandardFreightMultiplier { get; set; }
    public float PalletShipmentFreightMultiplier { get; set; }
    public float CurrencyMultiplier { get; set; }
    public string ActualWeight { get; set; }
    public string CountryCode { get; set; }
    public string CountryName { get; set; }
    public string StandardZone { get; set; }
    public string ExpressZone { get; set; }
    public string PalletShipmentZone { get; set; } // SHRIGANESH 11 Nov 2016
    public string SZPrice { get; set; }
    public string EZPrice { get; set; }
    public string PSPrice { get; set; } // SHRIGANESH 11 Nov 2016
    public string CSZWeight { get; set; }
    public string CEZWeight { get; set; }
    public string FreightRegionName { get; set; }
    public double OrderValue { get; set; }
    public float Weight { get; set; }
    public float WeightPerBox { get; set; }
    public Int32 BandId { get; set; }
    public string BandName { get; set; }
    public bool IsActive { get; set; }
    public Int32 FreightTableId { get; set; }

    public string EZCarrierServiceId { get; set; }
    public string EZCarrierServiceText { get; set; }
    public string EZTransitTime { get; set; }
    public string EZCustomText { get; set; }
    public bool EZDisallowOrder { get; set; }
    public string SZCarrierServiceId { get; set; }
    public string SZCarrierServiceText { get; set; }
    public string SZTransitTime { get; set; }
    public string SZCustomText { get; set; }
    public bool SZDisallowOrder { get; set; }
    public string PZCarrierServiceId { get; set; }
    public string PZCarrierServiceText { get; set; }
    public string PZTransitTime { get; set; }
    public string PZCustomText { get; set; }
    public bool PZDisallowOrder { get; set; }
    public bool IsStandard { get; set; }
    public bool IsExpress { get; set; }
    public bool IsPalletShipment { get; set; } // SHRIGANESH 11 Nov 2016
    public bool IsSZCalculateGridMatrix { get; set; }
    public bool IsEZCalculateGridMatrix { get; set; }
    public bool IsPZCalculateGridMatrix { get; set; }
    public string StandardMethodName { get; set; }
    public string ExpressMethodName { get; set; }
    public string palletMethodName { get; set; }
    public bool DisplayDutyMessage { get; set; }

    public string S1configuration { get; set; }
    public string S2configuration { get; set; }
    public string S3configuration { get; set; }



    public string S1sourcecountryid { get; set; }
    public string S2sourcecountryid { get; set; }
    public string S3sourcecountryid { get; set; }

    public bool WeightParameter { get; set; }
    public bool CurrencyParameter { get; set; }


    public string WeightFromToValueCM { get; set; }
    public string CurrencyFromToValueCM { get; set; }
    public string MinOrderValueCM { get; set; }
    public string ValueAppliedAboveCM { get; set; }
    public string ValueAppliedBelowCM { get; set; }

    public string FreightSourceCountryCode { get; set; }

    /*public FreightMode FreightModeId { get; set; }
        public FreightRegion FreightRegionId { get; set; }
        public FreightMethod FreightMethodId { get; set; }
        */
    //public List<FreightMultiplerCurrency> lstFreightMultiplerCurrency { get; set; }
    #endregion

    #region Nested Classes

    [Serializable]
    public class FreightMode
    {
        #region Properties
        public Int16 FreightModeId { get; set; }
        public string FreightModeName { get; set; }
        public Int32 RegionId { get; set; }
        public Int32 Languageid { get; set; }
        public List<FreightModeLanguage> LstFreightModeLangauges { get; set; }
        #endregion

        [Serializable]
        public class FreightModeLanguage
        {
            #region Properties
            public Int16 FreightModeLangaugeId { get; set; }
            public Int16 FreightModeId { get; set; }
            public Int16 LanguageId { get; set; }
            public string FreightModeName { get; set; }
            #endregion
        }
    }

    [Serializable]
    public class FreightRegion
    {
        #region Properties
        public Int16 FreightRegionId { get; set; }
        public string FreightRegionName { get; set; }
        #endregion
    }

    [Serializable]
    public class FreightMethod
    {
        #region Properties
        public Int16 FreightMethodId { get; set; }
        public string FreightMethodName { get; set; }
        #endregion
    }

    [Serializable]
    public class FreightTables
    {
        #region Properties
        public Int32 FreightTableId { get; set; }
        public Int32 CountryID { get; set; }
        public string TableName { get; set; }
        public bool IsActive { get; set; }
        #endregion
    }
    #endregion

    [Serializable]
    public class FreightMultiplerCurrency
    {
        public int MultiplierID { get; set; }
        public int FreightConfigurationId { get; set; }
        public int CurrencyID { get; set; }
        public decimal CValue { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
    }

    [Serializable]
    public class FreightConfiguration
    {
       
        public int FreightConfigurationId { get; set; }
        public int FreightModeId { get; set; }
      
    }

    [Serializable]
    public class FreightCurrencyBandParameters
    {
        public int CurrencyId { get; set; }
        public decimal CurrencyFrom { get; set; }
        public decimal CurrencyTo { get; set; }
        public int BandId { get; set; }
        public int CreatedBy { get; set; }
        public int FreightConfigurationId { get; set; }

    }

    [Serializable]
    public class FreightWeightBandParameters
    {
        public int WeightId { get; set; }
        public decimal WeightFrom { get; set; }
        public decimal WeightTo { get; set; }
        public int BandId { get; set; }
        public int CreatedBy { get; set; }
        public int FreightConfigurationId { get; set; }
    }

    [Serializable]
    public class FreightTransitTime
    {
        public int TransitTimeId { get; set; }
        public int FreightConfigurationId { get; set; }
        public int BandId { get; set; }
        public string TransitTimeName { get; set; }
        public string CustomText { get; set; }
        public int LanguageId { get; set; }
    }

    [Serializable]
    public class FreightBand
    {        
        public int BandId { get; set; }
        public string BandName { get; set; }
        public bool IsActive { get; set; }
    }

       [Serializable]
    public class FreightAtiveRegionBand
    {
        public int id { get; set; }
        public int BandId { get; set; }
        public int FreightRegionId { get; set; }
        public int FreightModeId { get; set; }
        public bool IsActive { get; set; }
    }
}


