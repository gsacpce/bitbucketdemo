﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class HeaderFooterManagementBE : EntityBase
{
    public string ConfigSectionName { get; set; }
    public string ConfigElementName { get; set; }
    public string ConfigElementID { get; set; }
    public string ConfigElementValue { get; set; }
}

  