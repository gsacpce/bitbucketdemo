﻿using System;

[Serializable]
public class CountryBE : EntityBase
{
    public Int16 CountryId { get; set; }
    public string CountryName { get; set; }
    public string CountryCode { get; set; }
    public string RegionCode { get; set; }
    public string CountryAlias { get; set; }
    public bool IsActive { get; set; }
    public bool IsShow { get; set; }
    public Int16 CurrencyId { get; set; }
    public Int16 CatalogueId { get; set; }
    public bool DisplayDutyMessage { get; set; }
    public Int16 UserTypeID { get; set; }/*User Type*/
    public string Country { get; set; }
    public Int16 Id { get; set; }
    public string Name { get; set; }

    #region Indeed Code
    public class CountryCurrencyMapping
    {
        public Int16 CountryCurrencyMappingID { get; set; }
        public string CountryName { get; set; }
        public string Country_ISO_Code { get; set; }
        public string Currency { get; set; }
        public string CurrencyName { get; set; }
        public string BillingEntity { get; set; }
        public Int32 InvoiceAccountID { get; set; }
        public Int16 UserTypeCatalogueID { get; set; }
        public Int16 UserTypeID { get; set; }
        public Int16 CatalogueID { get; set; }
        public bool IsActive { get; set; }
        public bool IsVatDisplay { get; set; }
        public float VatRate { get; set; }
        public Int16 CurrencyID { get; set; }
        public string CurrencySymbol { get; set; }
        //public Int16 DC_Country_Code { get; set; }
        //public string country_code { get; set; }
    } 
    #endregion

    public class CountryWiseCurrencyMapping
    {
        public string CountryCode { get; set; }
        public string CurrencyCode { get; set; }
        public Int16 CurrencyId { get; set; }
    }
}

