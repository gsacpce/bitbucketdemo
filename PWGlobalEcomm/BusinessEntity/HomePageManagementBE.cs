﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class HomePageManagementBE : EntityBase
{


    #region Properties

    public string ConfigSectionName { get; set; }
    public string ConfigElementName { get; set; }
    public string ConfigElementValue { get; set; }
    public Int32 ConfigElementID { get; set; }
    

    public Int16 HomePageStaticContentId { get; set; }

    public string Title { get; set; }

    public string ImageExtension { get; set; }

    public string URL { get; set; }

    public string ContentType { get; set; }

    public string Content { get; set; }

    public string ThumbNail { get; set; }

    

    public Int16 Width { get; set; }

    public Int16 Height { get; set; }

    public Int16 ConfigId { get; set; }


    public string WidthUnit { get; set; }

    public string HeightUnit { get; set; }

  
    public int Action { get; set; }
    

    #endregion

}

