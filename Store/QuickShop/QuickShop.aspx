﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.QuickShop" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=host %>js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        var status = false;
        var IsLazyLoad = '<%= IsCategoryHeaderEnabled %>';



        $(document).ready(function () {
            $('#hidPageNo').val("1");
            var pageNo = 1;
            if (IsLazyLoad == 'False') {
                if (pageNo == 1) {
                    GetProductData(pageNo);
                }
                $(window).scroll(function () {
                    var docHeight = $(document).height();
                    var winHeight = $(window).height();
                    var footerHeight = $('#footer').height() / 2;
                    //alert(footerHeight);
                    if ($(window).scrollTop() + footerHeight >= (docHeight - winHeight)) {
                        if (status == 'true' || status == true) {
                            $('#hidPageNo').val(parseInt($('#hidPageNo').val()) + 1);
                            pageNo = $('#hidPageNo').val();
                            //alert(pageNo);
                            status = false;
                            GetProductData(pageNo);
                        }
                    }
                });
            }
        });

        function GetProductData(pageNo) {
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'QuickShop/QuickShop.aspx/GetAllProducts',
                    dataType: 'JSON',
                    // data: '{pageNo:' + pageNo + '}',
                    data: JSON.stringify({ pageNo: pageNo }),
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: true,
                    beforeSend: function (request) {
                        $.blockUI({ message: $('#dvLoader') });
                    },
                    error: function (jqxHR, errorThrown, errorStatus) {
                        //alert(jqxHR.responseText);
                    },
                    //success: OnSuccess
                });
            }
            catch (e) {
            }
            finally { $.unblockUI(); }
        }

        //function OnSuccess(response) {

        //    if (response.d == "") {
        //        status = false;
        //        if ($('#hidPageNo').val() == "1") {
        //            $("#tblProductLazy").empty();
        //            $("#tblProductLazy").append("<tr><td>No Record Found </td><td>");
        //        }
        //        else {
        //            status = false;
        //        }
        //    }
        //    else {
        //        //$("#tblProductLazy tbody").is(":empty")
        //        //{
        //        //    $("#tblProductLazy").append("<tr> <th>Product Code</th><th>Product Title </th><th>Cost   </th><th>Quantity</th></tr>");
        //        //}
        //        //$("#tblProductLazy").empty();
        //        $('#divLazy').css('display', 'block');
        //        $.each(response.d, function (i, val) {
        //            // $(document).append("<input type='hidden' id='hdnProductCode" + i + "' value = '" + val.ProductCode + "' /> ");
        //            $("#tblProductLazy").append("<tr><td>" + val.ProductCode + "</td><td>" + val.ProductName +
        //                " <input type='hidden' id='hdnProductCode" + i + "' value = '" + val.ProductCode + "' /></td><td>" + val.ProductCost + "</td><td> <input type='text' id='txtQty" + i + "'/> </td><tr>");
        //        });
        //        status = true;
        //        $.unblockUI();
        //    }
        //    // $("#loader").hide();
        //}


    </script>
    <script>
        //$(document).ready(function () {
        //    $('#quickshop_sticky').affix({
        //        offset: {
        //            top: $('#quickshop_sticky').offset().top
        //        }
        //    });
        //});

        function add_button() {
            $("#quickbasket").stop();
            $("#quickbasket").fadeIn(3000, function () { $("#quickbasket").fadeOut(3000); });

            $("#quickbasket").mouseover(function () {
                $("#quickbasket").stop();
                $("#quickbasket").css("opacity", "1");
            });
            $("#quickbasket").mouseout(function () {
                $("#quickbasket").stop();
                $("#quickbasket").fadeIn(3000, function () { $("#quickbasket").fadeOut(3000); });
            });
            //$("#quickbasket").fadeIn(1500);
            //$("#quickbasket").fadeIn(3000, function () { $("#quickbasket").fadeOut(3000); });
        }

        function quickshop_scrollto(selected_div) {
            var element = document.getElementById(selected_div);
            $('html, body').animate({ scrollTop: $(element).offset().top }, 1500);
        }
    </script>

    <div id="divMainContent" class="qkDiv">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="pageTitle qkTitle"><%=QuickShop_PgTtl%></h1>
                </div>
            </div>

            <div class="row" id="divNoDataMsg" runat="server" visible="false">
                <div class="col-xs-12">
                    <asp:Literal ID="ltrlNoData" runat="server"></asp:Literal>
                </div>
            </div>

            <div class="row" id="divCategory" clientidmode="static" runat="server">
                <div class="col-xs-12">
                    <asp:Repeater ID="rptCategory" runat="server" OnItemDataBound="rptCategory_ItemDataBound">
                        <ItemTemplate>
                            <div class="quickshop_links_outer">
                                <div>
                                    <asp:HiddenField ID="hdnRelatedCategoryId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"RelatedCategoryId") %>' />
                                    <a id="anchorCategoryLink" class="quickshop_links pageScrollLinks" runat="server" clientidmode="static">
                                        <asp:Literal ID="ltlCategoryWithLink" runat="server"></asp:Literal>
                                    </a>
                                </div>
                            </div>
                            <asp:Repeater ID="rptSubCategory" runat="server" OnItemDataBound="rptSubCategory_ItemDataBound">
                                <ItemTemplate>
                                    <div class="quickshop_links_outer">
                                        <div>
                                            <asp:HiddenField ID="hdnRelatedCategoryId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"RelatedCategoryId") %>' />
                                            <a id="anchorCategoryLink" class="quickshop_links pageScrollLinks" runat="server" clientidmode="static">
                                                <asp:Literal ID="ltlCategoryWithLink" runat="server"></asp:Literal>
                                            </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="row" id="divProductInCategory" clientidmode="static" runat="server">
                <div class="col-md-12">
                    <input type="hidden" id="hidPageNo" runat="server" clientidmode="static" />



                    <div id="quickshop_sticky" runat="server" clientidmode="static" class="row quickshop_sticky affix-top">
                        <div class="container" style="padding-top: 15px; padding-bottom: 15px;">
                            <div class="row">
                                <div class="col-xs-12 form-inline" style="text-align: center;">
                                    <label>
                                        <input type="checkbox" id="noStockCheckbox" runat="server" />
                                    </label>
                                    <asp:Label ID="lblCheckboxText" runat="server"></asp:Label>
                                    <asp:LinkButton ID="lbtnAddBasket" class="btn btn-primary customActionBtn" runat="server" OnClick="lbtnAddBasket_Click" OnClientClick="javascript:return validateItem();"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <table class="table ProductsTable">
                    <asp:Repeater ID="rptParentCategory" runat="server" OnItemDataBound="rptParentCategory_ItemDataBound">

                        <ItemTemplate>
                            <tr>
                                <td colspan="100%">
                                    <div class="quickshop_headings customTableHeaderTitle" id="quickshop_headings" runat="server" clientidmode="static">
                                        <asp:Literal ID="ltlCategory" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <asp:HiddenField ID="hdnRelatedCategoryId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"RelatedCategoryId") %>' />
                            <asp:Repeater ID="rptProducts" runat="server" OnItemDataBound="rptProducts_ItemDataBound">
                                <HeaderTemplate>
                                    <tr>
                                        <th class="quickshop_header_image customTableHead"><%=QuickShop_HeaderImage %></th>
                                        <th class="quickshop_header_code customTableHead"><%=QuickShop_HeaderProduct %></th>
                                        <th class="quickshop_header_stock customTableHead"><%=strStockDueText %></th>
                                        <th class="quickshop_header_stock customTableHead"><%=QuickShop_HeaderStock %></th>
                                        <th class="quickshop_header_price customTableHead"><%=QuickShop_HeaderPrice %></th>
                                        <th class="quickshop_header_quantity customTableHead"><%=QuickShop_HeaderQuantity %></th>
                                    </tr>
                                </HeaderTemplate>

                                <ItemTemplate>

                                    <tr class="ProductRow">
                                        <td class="quickshop_column_image">
                                            <img class="quickshop_image" id="imgProduct" runat="server" />
                                        </td>
                                        <td class="quickshop_column_product">
                                            <span class="quickshop_product_code customTableHeadingText">
                                                <asp:Literal ID="ltlProductCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductSkuName") %>'></asp:Literal></span><br>
                                            <%--<asp:Literal ID="ltlProductCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Literal></span><br>--%>
                                            <span class="quickshop_product_title customTableText">
                                                <asp:Literal ID="ltlProductName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductName") %>'></asp:Literal></span>
                                        </td>
                                        <td class="quickshop_column_stock customTableText">
                                            <asp:Label ID="lblStockStatus" runat="server" CssClass="customTableText Stock"></asp:Label>
                                        </td>
                                        <td class="quickshop_column_stock customTableText">
                                            <asp:Label ID="lblStock" runat="server" CssClass="customTableText StockFigure"></asp:Label>
                                        </td>
                                        <td class="quickshop_column_price customTableText">
                                            <%--<asp:Literal ID="ltlCurrencySymbol" runat="server" Text="<%#GlobalFunctions.GetCurrencySymbol()%>"></asp:Literal>--%>
                                            <asp:Literal ID="ltlCost" runat="server"></asp:Literal>
                                        </td>
                                        <td class="quickshop_column_quantity pull-right">
                                            <asp:HiddenField ID="hdnProductSKU" runat="server" Value='<%# Eval("ProductSKUId")%>' />
                                            <asp:HiddenField ID="hdnMaximumOrderQuantity" runat="server" Value='<%# Eval("MaximumOrderQuantity")%>' />
                                            <asp:HiddenField ID="hdnMinimumQuantity" runat="server" Value='<%#Eval("MinimumOrderQuantity")%>' />
                                            <asp:TextBox ID="txtQuantity" Width="120" placeholder="0" runat="server" class="form-control input-sm quickshop_input_width customInput" Text="" onkeydown="return isNumeric(event)" onblur='<%# string.Format("ValidateQuantity(this.id, \"{0}\", \"{1}\");", Eval("MinimumOrderQuantity"), Eval("MaximumOrderQuantity")) %>'></asp:TextBox>
                                        </td>

                                    </tr>

                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="rptSubCategory_header" runat="server" OnItemDataBound="rptSubCategory_header_ItemDataBound">

                            <ItemTemplate>
                            <tr>
                                <td colspan="100%">
                                    <div class="quickshop_headings customTableHeaderTitle" id="quickshop_subheadings" runat="server" clientidmode="static">
                                        <asp:Literal ID="ltlSubCategory" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <asp:HiddenField ID="hdnRelatedCategoryId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"RelatedCategoryId") %>' />
                            <asp:Repeater ID="rptProducts" runat="server" OnItemDataBound="rptProducts_ItemDataBound">
                                <HeaderTemplate>
                                    <tr>
                                        <th class="quickshop_header_image customTableHead"><%=QuickShop_HeaderImage %></th>
                                        <th class="quickshop_header_code customTableHead"><%=QuickShop_HeaderProduct %></th>
                                        <th class="quickshop_header_stock customTableHead"><%=strStockDueText %></th>
                                        <th class="quickshop_header_stock customTableHead"><%=QuickShop_HeaderStock %></th>
                                        <th class="quickshop_header_price customTableHead"><%=QuickShop_HeaderPrice %></th>
                                        <th class="quickshop_header_quantity customTableHead"><%=QuickShop_HeaderQuantity %></th>
                                    </tr>
                                </HeaderTemplate>

                                <ItemTemplate>

                                    <tr class="ProductRow">
                                        <td class="quickshop_column_image">
                                            <img class="quickshop_image" id="imgProduct" runat="server" />
                                        </td>
                                        <td class="quickshop_column_product">
                                            <span class="quickshop_product_code customTableHeadingText">
                                                <asp:Literal ID="ltlProductCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductSkuName") %>'></asp:Literal></span><br>
                                            <%--<asp:Literal ID="ltlProductCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Literal></span><br>--%>
                                            <span class="quickshop_product_title customTableText">
                                                <asp:Literal ID="ltlProductName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductName") %>'></asp:Literal></span>
                                        </td>
                                        <td class="quickshop_column_stock customTableText">
                                            <asp:Label ID="lblStockStatus" runat="server" CssClass="customTableText Stock"></asp:Label>
                                        </td>
                                        <td class="quickshop_column_stock customTableText">
                                            <asp:Label ID="lblStock" runat="server" CssClass="customTableText StockFigure"></asp:Label>
                                        </td>
                                        <td class="quickshop_column_price customTableText">
                                            <%--<asp:Literal ID="ltlCurrencySymbol" runat="server" Text="<%#GlobalFunctions.GetCurrencySymbol()%>"></asp:Literal>--%>
                                            <asp:Literal ID="ltlCost" runat="server"></asp:Literal>
                                        </td>
                                        <td class="quickshop_column_quantity pull-right">
                                            <asp:HiddenField ID="hdnProductSKU" runat="server" Value='<%# Eval("ProductSKUId")%>' />
                                            <asp:HiddenField ID="hdnMaximumOrderQuantity" runat="server" Value='<%# Eval("MaximumOrderQuantity")%>' />
                                            <asp:HiddenField ID="hdnMinimumQuantity" runat="server" Value='<%#Eval("MinimumOrderQuantity")%>' />
                                            <asp:TextBox ID="txtQuantity" Width="120" placeholder="0" runat="server" class="form-control input-sm quickshop_input_width customInput" Text="" onkeydown="return isNumeric(event)" onblur='<%# string.Format("ValidateQuantity(this.id, \"{0}\", \"{1}\");", Eval("MinimumOrderQuantity"), Eval("MaximumOrderQuantity")) %>'></asp:TextBox>
                                        </td>

                                    </tr>

                                </ItemTemplate>
                            </asp:Repeater>

                           </ItemTemplate>
                         </asp:Repeater>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>


        <div class="row">
            <div class="col-md-08" id="divLazy" clientidmode="static" runat="server" style="display: none">
                <table id="tblProductLazy" style="width: 100%; border: none">
                    <tr>
                        <th>Product Code</th>
                        <th>Product Title </th>
                        <th>Cost   </th>
                        <th>Quantity</th>
                    </tr>
                </table>
            </div>
        </div>

    </div>

    <script>

        var _counter = 0;

        function ValidateQuantity(elm, minQ, maxQ) {
            var valQ = document.getElementById(elm).value;
            if (valQ != "") {
                if (parseInt(valQ) < parseInt(minQ) || parseInt(valQ) > parseInt(maxQ)) {
                    var spn = document.getElementById('WarningMessage');
                    if (parseInt(valQ) < parseInt(minQ))
                        error = "<%=QuickShop_MinQtyWarning%> " + minQ;
                    if (parseInt(valQ) < parseInt(minQ))
                        error = "<%=QuickShop_MaxQtyWarning%> " + maxQ;
                    spn.innerHTML = error;
                    document.getElementById(elm).value = "";
                    $('#myWarningModal').modal('show');
                    return false;
                }
                else {
                    _counter += 1;
                }
            }
            //return true;
        }

        function validateItem() {

            if (_counter == 0) {
                var spn = document.getElementById('WarningMessage');
                error = "<%=QuickShop_NoItemsWarning%>";
                spn.innerHTML = error;
                $('#myWarningModal').modal('show');
                return false;
            }
            return true;
        }




        $(document).ready(function () {
            $('#ContentPlaceHolder1_noStockCheckbox').on("click", function () {

                if ($(this).prop('checked') == true) {

                    $(".ProductsTable .ProductRow").each(function (i) {
                        //alert(i)
                        //debugger;

                        var stock = $(this).find(".StockFigure").text();
                        //alert(stock);

                        if (stock == "0") {
                            $(this).hide();
                            //$(this).parent().hide();
                        }


                    });
                }
                else {
                    $(".ProductsTable .ProductRow").each(function (i) {
                        $(this).show();
                        //$(this).parent().show();
                    });
                }


            });
        });




    </script>

    <script>

        $(document).ready(function () {

            window.onresize = sticky();
            $(window).scroll(function () { sticky() });


            function sticky() {
                var mobile_vertical_offset = document.getElementById("ALL_HEADER").clientHeight;
                //console.log(mobile_vertical_offset);
                var LargeMedia = false;
                if ($(window).width() >= 768) {
                    LargeMedia = true;
                    mobile_vertical_offset = 0;
                }

                var $headers = $("#quickshop_sticky");
                var scrollTop = $(window).scrollTop();


                if (scrollTop <= 0) $headers.css({ position: "relative", top: "0px" }); // reset all
                else {
                    $headers.each(function (index, $el) {
                        var $curHeader = $($headers).eq(index);
                        var curTop = $curHeader.offset().top - mobile_vertical_offset;
                        var curHeight = $curHeader.height();
                        var isRelative = ($el.isFixed && scrollTop <= $el.exTop); // scroll up
                        var isFixed = (curTop <= scrollTop); // scroll down
                        var position = "";
                        var top = 0;

                        if (isRelative) {
                            positon = "relative"; // reset
                            top = 0;
                            $el.isFixed = false;
                            $('#quickshop_sticky').css('background', 'none');
                        }
                        else if (isFixed) {
                            position = "fixed";
                            scrollTop += curHeight;
                            if (!$el.isFixed) {
                                $el.isFixed = true;
                                $el.exTop = curTop;
                                $('#quickshop_sticky').css('background', '#ededed');
                            }
                            $($el).css({ left: "0px" });
                            top = mobile_vertical_offset;
                        }
                        $($el).css({ position: position, top: top + "px" });
                    });
                }
            }

            //...............................................................................................................................


        })
        function quickshop_scrollto(selected_div, category_index) {

            // var element = document.getElementById(selected_div);
            // var sticky_height = $("#quickshop_sticky").outerHeight(false);
            //var header_height = $("#ALL_HEADER").outerHeight(false);
            //alert(header_height);
            // var mobile_vertical_offset;
            // if ($(window).width() >= 768) {
            //     mobile_vertical_offset = 130;
            //     if (category_index == 0) mobile_vertical_offset -= (sticky_height - 15);
            // }
            //  else {
            //      mobile_vertical_offset = 230;
            //      if (category_index == 0) mobile_vertical_offset -= (sticky_height - 15);
            //  }

            //  $('html, body').animate({ scrollTop: $(element).offset().top - mobile_vertical_offset }, 1500);
        }

        $(function () {

            var h = $('#quickshop_sticky').innerHeight();

            $('.quickshop_links_outer a').each(function () {
                var n = $(this).html();
                var m = n.replace(/[^A-Z0-9]+/ig, "_");
                //alert(m);
                //$(this).addClass(m);
                $(this).attr('data-go', m);
            });


            $('.customTableHeaderTitle').each(function () {
                var n = $(this).html();
                var m = n.replace(/[^A-Z0-9]+/ig, "_");
                //alert(m);

                $(this).addClass(m);

            });

            $('#divCategory a').click(function () {
                var c = $(this).attr('data-go');
                var off = $('.customTableHeaderTitle.' + c).offset().top;

                $('html, body').animate({ scrollTop: off - 180 }, 1500);
            });


        });
    </script>


</asp:Content>

