﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.MyAccount_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[type=text]').keypress(function (e) {
                var regex = new RegExp("^[a-zA-Z0-9+-@]+$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                else {
                    if (e.which == "32" || e.which == "8" || e.which == "0") {
                        return true;
                    }
                    else {
                        e.preventDefault();
                        return false;
                    }
                }
            });
        });
    </script>
    <section id="REGISTER-PAGE">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <h2 id="header" class="pageSubTitle" runat="server"></h2>
                    <p id="subHeader" class="pageText" runat="server"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default form-horizontal">
                        <div class="panel-heading customPanel  panel-heading-a">
                            <h4 class="panel-title" id="Register_PersonalDetails" runat="server"></h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label customLabel" for="name" id="lblEmailText" runat="server"></label>
                                <div class="col-sm-6">
                                    <asp:Label ID="lblEmail" disabled="" runat="server" Text="Undefined" class="form-control customInput"></asp:Label>
                                </div>
                                <div class="col-sm-4"><span class="help-block pageSmlText" id="accountEmailHelp"><%=Profile_ChangeEmail_Message %> <a href="#ModalContactUs" class="hyperLinkSml" data-toggle="modal"><%=SiteLinks_ContactusTitle%></a></span></div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2 customLabel lebal-align" for="name" id="lblTitle" runat="server"></label>
                                <div class="col-sm-6">
                                    <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtTitles" autocomplete="off" />
                                    <asp:RequiredFieldValidator ID="reqtxtTitles" runat="server" ControlToValidate="txtTitles" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2 customLabel lebal-align" for="name" id="lblFName" runat="server"></label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtFirstName" class="form-control customInput" runat="server" ValidationGroup="Reg"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTxtFirstName" runat="server" ControlToValidate="txtFirstName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-2 control-label customLabel lebal-align" for="name" id="lblLName" runat="server"></label>
                                <div class="col-sm-6 clearfix">
                                    <asp:TextBox ID="txtLastName" class="form-control customInput" runat="server" ValidationGroup="Reg"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTxtLastName" runat="server" ControlToValidate="txtLastName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 customLabel lebal-align" for="name" id="lblCPass" runat="server"></label>
                                <div class="col-md-6 col-sm-6 ">
                                    <%--<asp:CheckBox ID="chkChangePassword" runat="server" Visible="false" onchange="javascript:return ChangePassFunc();" />--%>
                                    <asp:Button ID="btnChangePassword" runat="server" class="btn btn-default  customButton" OnClientClick="return ChangePassFunc()" ValidationGroup="Edit" Text="" />
                                </div>
                            </div>
                            <div id="dvPassword" style="display: none" runat="server">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading customPanel  panel-heading-a">
                            <h4 class="panel-title" id="section3" runat="server">
                                <%=Register_RegisteredAddress_Text%>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse in">
                            <div class="panel-body form-horizontal">
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRName" runat="server"></label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtRegContactName" class="form-control customInput" runat="server" ValidationGroup="Reg" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRCompany" runat="server"></label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtRegCompany" class="form-control customInput" runat="server" ValidationGroup="Reg" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRAddress1" runat="server"></label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtRegAddressLine1" class="form-control customInput" runat="server" ValidationGroup="Reg" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvtxtRegAddressLine1" runat="server" ControlToValidate="txtRegAddressLine1" ValidationGroup="Edit" Style="display: none"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRAddress2" runat="server"></label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtRegAddressLine2" class="form-control customInput" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRTown" runat="server"></label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtRegTown" class="form-control customInput" runat="server" ValidationGroup="Reg" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRCounty" runat="server"></label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtRegCounty" class="form-control customInput" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRPostCode" runat="server"></label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtRegPostCode" class="form-control customInput" runat="server" ValidationGroup="Reg" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRCountry" runat="server"></label>
                                    <div class="col-sm-6">
                                        <select class="form-control customInput" id="ddlRegCountry" runat="server"></select>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2 customLabel" for="name" id="lblRPhone" runat="server"></label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtRegPhone" class="form-control customInput" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading customPanel  panel-heading-a">
                            <h4 class="panel-title">
                                <%=Register_DeliveryAddress_Text%>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="panel-body form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label customLabel"><%=Profile_PreferredDeliveryAddress%></label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlPreferredDelAddress" runat="server" ClientIDMode="Static" class="form-control customInput" AutoPostBack="true" OnSelectedIndexChanged="ddlPreferredDelAddress_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div id="collapse_delivery_address" class="collapse">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label customLabel" id="lblEditDeliveryContact" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditContactName" runat="server" class="form-control customInput" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label customLabel" id="lblEditDeliveryAddressTitle" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditAddressTitle" runat="server" class="form-control customInput" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label customLabel" id="lblEditIsDefault" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:CheckBox ID="chkEditIsDefault" runat="server" class="form-control customInput" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountCompany" class="col-sm-2 control-label customLabel" id="lblEditDeliveryComapny" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditDeliveryCompany" runat="server" class="form-control customInput" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountAddress1" class="col-sm-2 control-label customLabel" id="lblEditDeliveryAdd1" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditDeliveryAdd1" runat="server" class="form-control customInput" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountAddress2" class="col-sm-2 control-label customLabel" id="lblEditDeliveryAdd2" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditDeliveryAdd2" runat="server" class="form-control customInput" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountCity" class="col-sm-2 control-label customLabel" id="lblEditDeliveryCity" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditDeliveryCity" runat="server" class="form-control customInput" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountCounty" class="col-sm-2 control-label customLabel" id="lblEditDeliveryCounty" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditDeliveryCounty" runat="server" class="form-control customInput" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountPostcode" class="col-sm-2 control-label customLabel" id="lblEditDeliveryPostCode" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditDeliveryPostal" runat="server" class="form-control customInput" MaxLength="20" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountCountry" class="col-sm-2 control-label customLabel" id="lblEditDeliveryCountry" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlEditCountry" runat="server" class="form-control customInput"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountPhone" class="col-sm-2 control-label customLabel" id="lblEditDeliveryPhone" runat="server"></label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtEditPhone" runat="server" class="form-control customInput" MaxLength="20" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="accountPostcode" class="col-sm-2 control-label customLabel hidden-xs">&nbsp;</label>
                                            <div class="col-sm-6">
                                                <asp:Button ID="btnUpdateAddress" runat="server" class="btn btn-default  customButton" OnClientClick="return ValidateUpdatedAddress();" OnClick="btnUpdateAddress_Click" ValidationGroup="Edit" Text="Update address" />
                                                <asp:Button ID="btnRemoveAddress" runat="server" class="btn btn-default  customButton" OnClientClick="javascript:return confirm('Are you sure you want to delete this Address?');" OnClick="btnRemoveAddress_Click" Text="Remove address" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="accountPostcode" class="col-sm-2 control-label customLabel hidden-xs">&nbsp;</label>
                                        <div class="col-sm-6" style="margin-top: 20px">
                                            <asp:Button ID="btnAddmodal" runat="server" Text="" class="btn btn-default customButton" data-toggle="modal" OnClick="btnAddmodal_Click" />
                                        </div>
                                    </div>
                                </div>
                                <!-- PANEL BODY END -->
                            </div>
                        </div>
                        <div class="panel-heading customPanel  panel-heading-a">
                            <h4 class="panel-title" id="hMarketing_Preferences" runat="server"></h4>
                        </div>
                        <div class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group">
                                    <div class="col-sm-3 col-md-2 text-right">
                                        <asp:Label ID="lblPreferredLanguage" CssClass="control-label customLabel" Visible="false" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-3 col-md-6">
                                        <asp:DropDownList ID="drpdwnPreferredLanguage" CssClass="form-control customInput" runat="server" Visible="false"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-3 col-md-2 text-right">
                                        <asp:Label ID="lblPreferredCurrency" CssClass="col-sm-3 col-md-4 control-label customLabel" Visible="false" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-3 col-md-6">
                                        <asp:DropDownList ID="drpdwnPreferredCurrency" CssClass="form-control customInput" runat="server" Visible="false"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <asp:Repeater ID="rptUserTypeCustomFields" runat="server" OnItemDataBound="rptUserTypeCustomFields_OnItemDataBound">
                                        <HeaderTemplate>
                                            <div id="divInnerCustomFields">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="lblCustomFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
                                                <asp:HiddenField ID="hdnRegistrationFieldsConfigurationId" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
                                                <div class="col-sm-9 col-md-8 form-group">
                                                    <asp:HiddenField ID="hdfControl" runat="server" />
                                                    <asp:HiddenField ID="hdfColumnName" runat="server" />
                                                    <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCustomFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" />
                                                    <asp:DropDownList ID="ddlCustomValue" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <asp:CheckBoxList ID="chkCustomValue" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
                                                    <asp:RequiredFieldValidator ID="reqtxtCustomFieldName" runat="server" ControlToValidate="txtCustomFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="reqddlCustomValue" runat="server" ControlToValidate="ddlCustomValue" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="panel-body form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-2">
                                            <div class="radio">
                                                <asp:RadioButton ID="rdProfile_MarketingOption1" runat="server" GroupName="optionsRadios" />
                                            </div>
                                            <div class="radio">
                                                <asp:RadioButton ID="rdProfile_MarketingOption2" runat="server" GroupName="optionsRadios" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- PANEL BODY END -->
                            </div>
                        </div>
                        <div class="panel-heading customPanel panel-heading-a hide">
                            <h4 class="panel-title"><%=Register_Validate_Captcha%></h4>
                        </div>
                        <div class="panel-collapse collapse in hide">
                            <div class="panel-body">
                                <div class="panel-body form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-2 col-md-2  hidden-xs">&nbsp;</div>
                                        <div class="col-sm-6">
                                            <img id="CaptchaImg" src="" runat="server" ondragstart="return false;" ondrop="return false;" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 col-md-2 control-label customLabel" id="lblVerification" runat="server"></label>
                                        <div class="col-sm-6">
                                            <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCaptcha" autocomplete="off" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <asp:Button ID="btnConfirm" runat="server" OnClick="btnConfirm_Click" OnClientClick="return Validate();" CssClass="btn btn-primary btn-block customActionBtn" Text="" />
                        </div>
                        <div class="panel-heading customPanel  panel-heading-a hide">
                            <h4 class="panel-title" id="section4" runat="server"></h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="dvDelAddress" runat="server" class="modal fade" role="dialog" data-backdrop="static" data-keyword="false" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="sectionModal1" runat="server"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDAddressName" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtAddressTitle" autocomplete="off" class="form-control" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDIsDefault" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:CheckBox ID="chkIsDefault" class="form-control" runat="server" BorderStyle="None" />
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDName" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtDelContactName" autocomplete="off" class="form-control" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDCompany" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtDelCompany" autocomplete="off" class="form-control" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDAddress1" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtDelAddressLine1" autocomplete="off" class="form-control" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDAddress2" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtDelAddressLine2" autocomplete="off" class="form-control" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDTown" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtDelTown" autocomplete="off" class="form-control" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDCounty" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtDelCounty" autocomplete="off" class="form-control" runat="server" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDPostCode" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtDelPostCode" autocomplete="off" class="form-control" runat="server" MaxLength="20" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDCountry" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <select id="ddlDelCountry" runat="server" class="form-control"></select>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblDPhone" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtDelPhone" autocomplete="off" class="form-control" runat="server" MaxLength="20" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                            </div>
                        </div>
                        <input type="hidden" id="hdnId" runat="server" />
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="dvAdd" runat="server" style="display: none">
                        <asp:Button CssClass="btn center-block" ID="btnAdd" OnClientClick="return ValidateDeliveryAddress();" runat="server" ValidationGroup="Add" OnClick="btnAdd_Click" Text="Add"></asp:Button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="dvModalPassword" class="modal fade" role="dialog" data-backdrop="static" data-keyword="false" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="ModalPassword" runat="server"><%=Profile_ChangePassword%></h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group clearfix">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblOPass" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtOldPassword" class="form-control customInput" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtOldPassword" runat="server" ControlToValidate="txtOldPassword" CssClass="text-danger" Display="Dynamic"
                                    SetFocusOnError="true" ValidationGroup="PwdGroup"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblNPass" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtNewPassword" class="form-control customInput" runat="server" TextMode="Password" onblur="return CheckTextbox(this.id)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtNewPassword" runat="server" ControlToValidate="txtNewPassword" CssClass="text-danger" Display="Dynamic"
                                    SetFocusOnError="true" ValidationGroup="PwdGroup"></asp:RequiredFieldValidator>
                                <div style="display: none">
                                    <asp:RegularExpressionValidator ID="regexPwdLentxtPassword" runat="server" ControlToValidate="txtNewPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="PwdGroup" CssClass="text-danger"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="regexAlphaNumtxtPassword" runat="server" ControlToValidate="txtNewPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="PwdGroup" CssClass="text-danger"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="regexAlphaNumSymtxtPassword" runat="server" ControlToValidate="txtNewPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="PwdGroup" CssClass="text-danger"></asp:RegularExpressionValidator>
                                </div>
                                <asp:HiddenField ID="hdnPasswordPolicyType" runat="server" />
                                <asp:HiddenField ID="hdnMinPasswordLength" runat="server" />
                                <asp:Label ID="lblPasswordPolicyMessage" runat="server" CssClass="text-danger"></asp:Label>
                                <!-- Region Added by SHRIGANESH SINGH for Password Policy END 17 May 2016 -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-4 control-label customLabel" for="name" id="lblCNPass" runat="server"></label>
                            <div class="col-sm-9 col-md-8">
                                <asp:TextBox ID="txtConfirmPassword" class="form-control customInput" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" CssClass="text-danger" Display="Dynamic"
                                    SetFocusOnError="true" ValidationGroup="PwdGroup"></asp:RequiredFieldValidator>
                                <!-- Below code is commented by SHRIGANESH SINGH 17 May 2016 -->
                                <%--<asp:RegularExpressionValidator ID="regtxtConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="PwdGroup" CssClass="text-danger"></asp:RegularExpressionValidator>--%>

                                <asp:CompareValidator ID="cmpValidatorPwd" runat="server" ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="PwdGroup" CssClass="text-danger"></asp:CompareValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lblPwdLength" runat="server" CssClass="col-sm-12 col-md-9 control-label customLabel"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <asp:Button ID="btnPwdSave" runat="server" Text="Save" OnClick="btnPwdSave_Click" OnClientClick="return CheckOldNewPassword()" ValidationGroup="PwdGroup" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function showPwd() {
            $('#dvModalPassword').modal('show');
        }
        function hidePwd() {
            $('#dvModalPassword').modal('hide');
        }
        function ChangePassFunc() {
            var dv = document.getElementById('<%=dvPassword.ClientID%>');
			<%--var chk = document.getElementById('<%=chkChangePassword.ClientID%>');--%>
		    document.getElementById('<%=txtOldPassword.ClientID%>').value = "";
		    document.getElementById('<%=txtNewPassword.ClientID%>').value = "";
		    document.getElementById('<%=txtConfirmPassword.ClientID%>').value = "";
		    $('#dvModalPassword').modal('show');
		    dv.style.display = "block";
		    return false;
		}

		function createCode() {
		    var random;
		    var temp = "";
		    for (var i = 0; i < 5; i++) {
		        temp += Math.round(Math.random() * 8);
		    }
		    document.getElementById('<%= CaptchaImg.ClientID %>').src = '<%= GlobalFunctions.GetVirtualPath()%>Login/JpegImage.aspx?code=' + temp;
        }
        window.onload = createCode();

        function ValidateDeliveryAddress() {
            debugger;
            var ErrorString = "";
            var txtAddressTitle = document.getElementById('<%=txtAddressTitle.ClientID %>');
            var txtDelContactName = document.getElementById('<%=txtDelContactName.ClientID %>');
		    var txtDelAddress1 = document.getElementById('<%=txtDelAddressLine1.ClientID %>');
		    var txtDelTown = document.getElementById('<%=txtDelTown.ClientID %>');
		    var txtDelPostCode = document.getElementById('<%=txtDelPostCode.ClientID %>');
            var ddlDelCountry = document.getElementById('<%=ddlDelCountry.ClientID %>');
            if (txtAddressTitle.value.trim() == "") {
                txtAddressTitle.value = "";
                ErrorString = ErrorString + ("<%=Generic_AddressTitle_Req_Message %>") + "</br>";
            }
		    if (txtDelContactName.value.trim() == "") {
		        txtDelContactName.value = "";
		        ErrorString=ErrorString+("<%=errorD1 %>")+"</br>";
			}
            if (txtDelAddress1.value.trim() == "") {
                txtDelAddress1.value = "";
                ErrorString = ErrorString + ("<%=errorD2 %>") + "</br>";
			}
            if (txtDelTown.value.trim() == "") {
                txtDelTown.value = "";
                ErrorString = ErrorString + ("<%=errorD3 %>") + "</br>";
			}
            if (txtDelPostCode.value.trim() == "") {
                txtDelPostCode.value = "";
                ErrorString = ErrorString + ("<%=errorD4 %>") + "</br>";
			}
            if (ddlDelCountry.selectedIndex == 0) {
                ErrorString = ErrorString + ("<%=errorD5 %>") + "</br>";
            }
            if (ErrorString != "")
            {
                $('#myWarningModal').find('#WarningMessage').html(ErrorString);
                $('#myWarningModal').modal('show');
                return false;
            }
            return true;
        }
        function CheckTextbox(clientId) {
            debugger;
            var obj = document.getElementById(clientId);
            var iChars = "=|/! "
            for (var i = 0; i < obj.value.length; i++) {
                if (iChars.indexOf(obj.value.charAt(i)) != -1) {
                    var spn = document.getElementById('WarningMessage');
                    spn.innerHTML = '<%=strErrormsg_specialChar%>';
                    obj.value = "";
                    $('#myWarningModal').modal('show');
                    return false;
                }
            }
            return true;
        }

        function ValidateUpdatedAddress() {
            debugger;
            var strError = "";
            var txtEditAddressTitle = document.getElementById('<%= txtEditAddressTitle.ClientID%>');
		    var txtEditContactName = document.getElementById('<%= txtEditContactName.ClientID%>');
		    var txtEditDeliveryAdd1 = document.getElementById('<%= txtEditDeliveryAdd1.ClientID%>');
		    var txtEditDeliveryCity = document.getElementById('<%= txtEditDeliveryCity.ClientID%>');
		    var txtEditDeliveryPostal = document.getElementById('<%= txtEditDeliveryPostal.ClientID%>');
		    var ddlEditCountry = document.getElementById('<%=ddlEditCountry.ClientID%>');

		    if (txtEditAddressTitle.value.trim() == "") {
		        txtEditAddressTitle.value = "";
		        strError=strError+("<%=Generic_AddressTitle_Req_Message %>")+"</br>";
			}

            if (txtEditContactName.value.trim() == "") {
                txtEditContactName.value = "";
                strError=strError+("<%=Generic_DelContactName_Req_Message %>")+"</br>";
			}

            if (txtEditDeliveryAdd1.value.trim() == "") {
                txtEditDeliveryAdd1.value = "";
                strError=strError+("<%=Generic_DelAddressLine1_Req_Message %>")+"</br>";
			}

            if (txtEditDeliveryCity.value.trim() == "") {
                txtEditDeliveryCity.value = "";
               strError=strError+("<%=Generic_DelTown_Req_Message %>")+"</br>";
			}

            if (txtEditDeliveryPostal.value.trim() == "") {
                txtEditDeliveryPostal.value = "";
                strError=strError+("<%=Generic_DelPostalCode_Req_Message %>")+"</br>";
			}

            if (ddlEditCountry.selectedIndex == 0) {
                strError=strError+("<%=errorD5 %>")+"</br>";
            }
            if (strError != "")
            {
                $('#myWarningModal').find('#WarningMessage').html(strError);
                $('#myWarningModal').modal('show');
                return false;
            }
            return true;
        }

        function AddFunc() {

            document.getElementById('<%=txtAddressTitle.ClientID %>').value = "";
		    document.getElementById('<%=chkIsDefault.ClientID %>').checked = false;
		    document.getElementById('<%=txtDelContactName.ClientID %>').value = "";
		    document.getElementById('<%=txtDelCompany.ClientID %>').value = "";
		    document.getElementById('<%=txtDelAddressLine1.ClientID %>').value = "";
		    document.getElementById('<%=txtDelAddressLine2.ClientID %>').value = "";
		    document.getElementById('<%=txtDelTown.ClientID %>').value = "";
		    document.getElementById('<%=txtDelCounty.ClientID %>').value = "";
		    document.getElementById('<%=txtDelPostCode.ClientID %>').value = "";

		    document.getElementById('<%=txtDelPhone.ClientID %>').value = "";
		    document.getElementById('<%=hdnId.ClientID %>').value = "0";
		    document.getElementById('<%=dvAdd.ClientID %>').style.display = 'block';
		    $('#ContentPlaceHolder1_dvDelAddress').modal('show');
		    deliveryAddressChanged();
		    return false;
		}

        function Validate() {
            debugger;
		    <%-- Personal Details --%>
		    var strCombo="";
	        var txtFirstName = document.getElementById('<%=txtFirstName.ClientID %>');
	        var txtLastName = document.getElementById('<%=txtLastName.ClientID %>');
	        <%--var chkChangePassword = document.getElementById('<%=chkChangePassword.ClientID %>');--%>
	        var txtOldPassword = document.getElementById('<%=txtOldPassword.ClientID %>');
	        var txtNewPassword = document.getElementById('<%=txtNewPassword.ClientID %>');
	        var txtConfirmPassword = document.getElementById('<%=txtConfirmPassword.ClientID %>');

			<%-- Registered Address --%>
	        var txtRegContactName = document.getElementById('<%=txtRegContactName.ClientID %>');
	        var txtRegCompany = document.getElementById('<%=txtRegCompany.ClientID %>');
	        var txtRegAddress1 = document.getElementById('<%=txtRegAddressLine1.ClientID %>');
	        var txtRegTown = document.getElementById('<%=txtRegTown.ClientID %>');
	        var txtRegPostCode = document.getElementById('<%=txtRegPostCode.ClientID %>');
	        var ddlRegCountry = document.getElementById('<%=ddlRegCountry.ClientID %>');

			<%-- Delivery Address --%>
	        var ddlPreferredDelAddress = document.getElementById('<%=ddlPreferredDelAddress.ClientID%>');
	        var txtEditContactName = document.getElementById('<%=txtEditContactName.ClientID%>');
	        var txtEditAddressTitle = document.getElementById('<%=txtEditAddressTitle.ClientID%>');
	        var txtEditDeliveryAdd1 = document.getElementById('<%=txtEditDeliveryAdd1.ClientID%>');
	        var txtEditDeliveryCity = document.getElementById('<%=txtEditDeliveryCity.ClientID%>');
	        var txtEditDeliveryPostal = document.getElementById('<%=txtEditDeliveryPostal.ClientID%>');
		    var ddlEditCountry = document.getElementById('<%=ddlEditCountry.ClientID%>')
		    var drpdwnPreferredCurrency = document.getElementById('<%=drpdwnPreferredCurrency.ClientID%>');
	        if (txtFirstName.value.trim() == "") {
	            console.log("in");
	            txtFirstName.value = "";
	            strCombo=strCombo+("<%=error1 %>")+"</br>";
			}
            if (txtLastName.value.trim() == "") {
                txtLastName.value = "";
                strCombo=strCombo+("<%=error2 %>")+"</br>";
			}
		    <%--if (chkChangePassword.checked) {

				if (txtOldPassword.value.trim() == "" || txtNewPassword.value.trim() == "" || txtConfirmPassword.value.trim() == "") {
					$('#myWarningModal').modal('show');
					$('#myWarningModal').find('#WarningMessage').html("<%=error3 %>");
					return false;
				}
				else {
					if (txtNewPassword.value.trim() != txtConfirmPassword.value.trim()) {
						txtNewPassword.value = "";
						txtConfirmPassword.value = "";
						$('#myWarningModal').modal('show');
						$('#myWarningModal').find('#WarningMessage').html("<%=error4 %>");
						return false;
					}
				}
			}--%>
	        if (txtRegContactName.value.trim() == "") {
	            txtRegContactName.value = "";
	            strCombo=strCombo+("<%=errorR1 %>")+"</br>";
			}
            if (txtRegCompany.value.trim() == "") {
                txtRegCompany.value = "";
               strCombo=strCombo+("<%=errorR2 %>")+"</br>";
			}
            if (txtRegAddress1.value.trim() == "") {
                txtRegAddress1.value = "";
                strCombo=strCombo+("<%=errorR3 %>")+"</br>";
			}
		    if (drpdwnPreferredCurrency.selectedIndex == 0)
		    {
		        strCombo=strCombo+("<%=strRegister_PreferredCurrency_Message %>")+"</br>";
            }
            if (txtRegTown.value.trim() == "") {
                txtRegTown.value = "";
                strCombo=strCombo+("<%=errorR4 %>")+"</br>";
			}
            if (txtRegPostCode.value.trim() == "") {
                txtRegPostCode.value = "";
                strCombo=strCombo+("<%=errorR5 %>")+"</br>";
			}
            if (ddlRegCountry.selectedIndex == 0) {
                strCombo=strCombo+("<%=errorR6 %>")+"</br>";
			}

            if (ddlPreferredDelAddress.selectedIndex != 0) {

                if (txtEditContactName.value.trim() == "") {
                    txtEditContactName.value = "";
                    strCombo=strCombo+("<%=Generic_DelContactName_Req_Message %>")+"</br>";
				}

                if (txtEditAddressTitle.value.trim() == "") {
                    txtEditAddressTitle.value = "";
                    strCombo=strCombo+("<%=Generic_AddressTitle_Req_Message %>")+"</br>";
				}
                if (txtEditDeliveryAdd1.value.trim() == "") {
                    txtEditDeliveryAdd1.value = "";
                    strCombo=strCombo+("<%=Generic_DelAddressLine1_Req_Message %>")+"</br>";
				}
                if (txtEditDeliveryCity.value.trim() == "") {
                    txtEditDeliveryCity.value = "";
                    strCombo=strCombo+("<%=Generic_DelTown_Req_Message %>")+"</br>";
				}
                if (txtEditDeliveryPostal.value.trim() == "") {
                    txtEditDeliveryPostal.value = "";
                    strCombo=strCombo+("<%=Generic_DelPostalCode_Req_Message %>")+"</br>";
				}
                if(ddlEditCountry.selectedIndex==0)
                {
                    strCombo=strCombo+("select delivery country")+"</br>";
                }
            }
            if (strCombo != "")
            {
                $('#myWarningModal').find('#WarningMessage').html(strCombo);
                $('#myWarningModal').modal('show');
                return false;
            }

            return true;
        }

        function deliveryAddressChanged() {
            var address_selected = document.getElementById("ddlPreferredDelAddress").value;
            if (address_selected == 0) {
                $('#collapse_delivery_address').collapse('hide');
            }
            else {
                $('#collapse_delivery_address').collapse('show');
            }
        }

        // Region Added by SHRIGANESH SINGH 31 May 2016 for Password Policy START
        function CheckOldNewPassword() {
            var currentpassword = document.getElementById("ContentPlaceHolder1_txtOldPassword").value;
            var newpassword = document.getElementById("ContentPlaceHolder1_txtNewPassword").value;
            var confirmpassword = document.getElementById("ContentPlaceHolder1_txtConfirmPassword").value;

            if (currentpassword == "") {
                $('#myWarningModal').find('#WarningMessage').html('Please Enter Old Password');
                $('#myWarningModal').modal('show');
                return false;
            }

            if (newpassword == "") {
                $('#myWarningModal').find('#WarningMessage').html('Please Enter new Password');
                $('#myWarningModal').modal('show');
                return false;
            }

            if (confirmpassword == "") {
                $('#myWarningModal').find('#WarningMessage').html('Please Enter Confirm Password');
                $('#myWarningModal').modal('show');
                return false;
            }

            if (currentpassword == newpassword) {
                $('#myWarningModal').find('#WarningMessage').html('Old Password and New Password cannot be same');
                $('#myWarningModal').modal('show');
                return false;
            }
            else {
                return true;
            }
        }
        // Region Added by SHRIGANESH SINGH 31 May 2016 for Password Policy END

        // Region Added by SHRIGANESH SINGH 11 May 2016 for Password Policy START
        $(document).ready(function () {

            $("#ContentPlaceHolder1_txtNewPassword").blur(function () {
                var MinimumPasswordLength = $('#ContentPlaceHolder1_hdnMinPasswordLength').val();
                var PasswordPolicyType = $('#ContentPlaceHolder1_hdnPasswordPolicyType').val();
                var password = $("#ContentPlaceHolder1_txtNewPassword").val();
                var passwordLength = $("#ContentPlaceHolder1_txtNewPassword").val().length;
                var MinPassLenErrorMsg = '<%=strMinPassLenErrorMsg%>';
				var AlphaNumPassReqErrorMsg = '<%=strAlphaNumPassReqErrorMsg%>';
			    var AlphaNumSymPassReqErrorMsg = '<%=strAlphaNumSymPassReqErrorMsg%>';
			    $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text("");

			    if (passwordLength != 0) {
			        if (passwordLength < MinimumPasswordLength) {
			            $("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
			            $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(MinPassLenErrorMsg);
			        } else {

			            $("#ContentPlaceHolder1_lblPasswordPolicyMessage").hide();

			            // Validate against Alpha Numeric Regular Expression
			            if (PasswordPolicyType == "Must Contain Alpha Numeric") {
			                var AlphaNumRegex = /^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%&.+=*)(_-]/;
			                if (AlphaNumRegex.test(password)) {
			                    //alert("Success");
			                }
			                else {
			                    $("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
			                    $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(AlphaNumPassReqErrorMsg);
			                }
			            }

			                // Validate against Alpha Numeric and Symbol Regular Expression
			            else if (PasswordPolicyType == "Must contain Alphabets Numbers and Symbols") {
			                var AlphaNumSymRegex = /^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]/;

			                if (AlphaNumSymRegex.test(password)) {
			                }
			                else {
			                    $("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
			                    $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(AlphaNumSymPassReqErrorMsg);
			                }
			            }
			        }
			    }
			});
		});
        // Region Added by SHRIGANESH SINGH 11 May 2016 for Password Policy END
    </script>
</asp:Content>
