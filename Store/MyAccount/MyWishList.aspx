﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true"  Inherits="MyAccount_MyWishList" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="divMainContent">
        <section id="STANDARD_BASKET">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="pageTitle" id="hPageHeading" runat="server"></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" id="divBasketListing">
                        <asp:Repeater ID="rptWishListing" runat="server" OnItemDataBound="rptWishListing_ItemDataBound">
                            <HeaderTemplate>
                                <div class="standard_basket_titles">
                                    <div class="standard_basket_image customTableHead" id="divProductText" runat="server"></div>
                                    <div class="standard_basket_text customTableHead" id="divTitleText" runat="server"></div>
                                    <div class="standard_basket_plus_minus customTableHead" id="divQuantityText" runat="server"></div>
<%--                                    <div class="standard_basket_unit_price customTableHead" id="divUnitCostText" runat="server"></div>
                                    <div class="standard_basket_line_total customTableHead" id="divLineTotalText" runat="server"></div>--%>
                                    <%--<div class="standard_basket_Note customTableHead" id="divNotelabel" runat="server"></div>--%>
                                    <div class="standard_basket_remove customTableHead" id="divRemoveText" runat="server"></div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div id="dvBasketRowContainer" runat="server" class="standard_basket_row">
                                    <!-- PRODUCT 1 -->
                                    <div class="standard_basket_image">
                                        <img class="image_bg" id="imgProduct" runat="server" style="width: 60px;" alt="" />
                                    </div>
                                    <div id="dvProductName" runat="server" class="standard_basket_text customTableText">
                                        <asp:Literal ID="ltrSKUName" runat="server" /><br>
                                        <asp:Literal ID="ltrProductName" runat="server" />
                                    </div>
                                    <div class="standard_basket_plus_minus">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-btn"><a class="btn btn-default active aMinus" role="button">&ndash;</a></span>
                                            <input type="text" style="text-align: center"
                                                class="form-control quantity customInput" placeholder="" maxlength="8" id="txtQuantity" runat="server"  onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);">
                                            <span class="input-group-btn"><a class="btn btn-default active aPlus" role="button">+</a></span>
                                        </div>
                                    </div>
                                  <%--  <div id="dvUnitPrice" runat="server" class="standard_basket_unit_price customTableText"></div>
                                    <div id="dvTotalPrice" runat="server" class="standard_basket_line_total customTableText"></div>
                                  --%>
                                      <%--<div id="divNote" runat="server" class="standard_basket_text customTableText">
                                        <asp:TextBox ID="txtNote" runat="server" Rows="2" Columns="50" TextMode="MultiLine"></asp:TextBox>
                                    </div> --%>
                                     <div class="standard_basket_remove">
                                        <asp:LinkButton ID="lnkDeleteProduct" runat="server" CssClass="customClose" CommandArgument='<% #Eval("WishListId")%>' OnClick="lnkDeleteProduct_Click">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
                                    </div>
                                    <input type="hidden" id="hdnMinimumOrderQuantity" runat="server" value="0" />
                                    <input type="hidden" id="hdnMaximumOrderQuantity" runat="server" value="0" />
                                    <%--<input type="hidden" id="hdnStockStatus" runat="server" value="0" />--%>
                                    <input type="hidden" id="hdnWishListId" runat="server" />
                                     <input type="hidden" id="hdnSKUId" runat="server" />
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                <div class="standard_basket_row">
                                    <!-- TOTAL ROW -->
                                    <div class="standard_basket_image">&nbsp;</div>
                                    <div class="standard_basket_text">&nbsp;</div>
                                    <div class="standard_basket_plus_minus">&nbsp;</div>
                                    <div class="standard_basket_unit_price bolder_weight customTableText" id="divTotalText" runat="server"></div>
                                    <div class="standard_basket_remove"></div>
                                </div>
                                <div class="standard_basket_row text-right">
                                     <asp:Button ID="imgbtnAddtoCart" runat="server" CssClass="btn btn-primary customActionBtn" OnClick="imgbtnAddtoCart_Click"/>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>

                    </div>
                </div>
            </div>
            <!-- COTAINER END -->
        </section>
    </div>
<script src="<%=host %>js/basket.js"></script>
   <script type="text/javascript">
       function ShowBasket() {
           $("#quickbasket").stop();
           $("#quickbasket").fadeIn(50, function () { $("#quickbasket").fadeOut(15000); });
       }

       $(function () {

           $('.BasketLink').mouseenter(function () {
               $('#quickbasket').css({
                   'opacity': '1',
                   'display': 'block'
               });
           });
           $('.BasketLink').mouseleave(function () {
               $('#quickbasket').css({
                   'opacity': '0',
                   'display': 'none'
               });
           });
           $("#quickbasket").mouseover(function () {
               $("#quickbasket").stop();
               $("#quickbasket").css("opacity", "1");
           });
           $("#quickbasket").mouseout(function () {
               $("#quickbasket").stop();
               $("#quickbasket").fadeIn(50, function () { $("#quickbasket").fadeOut(50); });
           });

           if ($(window).width() <= 767) {
               $('.productBtnPanel').appendTo('.formobile');
           }
           $(window).resize(function () {
               if ($(window).width() <= 767) {
                   $('.productBtnPanel').appendTo('.formobile');
               }
               if ($(window).width() > 767) {
                   $('.productBtnPanel').appendTo('.blank');
               }
           });
       });
        </script>

</asp:Content>

