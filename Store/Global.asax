﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>

<script RunAt="server">
    
 

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        RegisterRoutes(RouteTable.Routes);
    }

    void RegisterRoutes(RouteCollection routes)
    {
        //routes.Ignore("{*alljpg}", new { alljpg = @".*\.jpg(/.*)?" });
        //routes.Ignore("{*pathInfo}", new { pathInfo = @"^.*(ChartImg.axd)$" });

        //Login
        //Do not change the routename of the below url because it has been used for the redirection in many pages.
        routes.MapPageRoute("login-page", "SignIn", "~/Login/Login.aspx", true);
        routes.MapPageRoute("Login-SSOPage", "LoginSSO", "~/Login/IndeedSSOLoginPage.aspx");
        /*Sachin Chauhan Start : 24 03 2016 : Added missing url rewrite route below*/
        routes.MapPageRoute("Forgot-Password", "ForgotPassword", "~/Login/ResetPassword.aspx");
        routes.MapPageRoute("Register", "UserRegistration", "~/Login/Register.aspx");
        routes.MapPageRoute("logout-page", "Signout", "~/Login/Logout.aspx");
        routes.MapPageRoute("My-Account", "MyAccount/Profile", "~/MyAccount/Profile.aspx");
        routes.MapPageRoute("MyWishList", "MyAccount/WishList", "~/MyAccount/MyWishList.aspx");
        /*Sachin Chauhan End : 24 03 2016*/

        /*Snehal START - Indeed 15 03 2017 */
        routes.MapPageRoute("MyAccount2", "info/my_account_2", "~/Dashboard/MyAccount2.aspx");
        routes.MapPageRoute("Dashboard", "info/Dashboard", "~/Dashboard/DashboardIndeed.aspx");
        routes.MapPageRoute("AddressBook", "info/address_book", "~/Dashboard/AddressBook.aspx");
        routes.MapPageRoute("BudgetTransactions", "info/budget_transactions", "~/Dashboard/BudgetTransaction.aspx");
        routes.MapPageRoute("PointTransactions", "info/point_transactions", "~/Dashboard/PointsTransaction.aspx");
        /*Snehal END - Indeed 15 03 2017 */
        
        //Index/Home page
        //Do not change the routename of the below url because it has been used for the redirection in many pages.
        routes.MapPageRoute("Index", "Index", "~/Home/Home.aspx");

        //Category listing pages
        routes.MapPageRoute("CategoryListing2", "SubCategories/{categoryname}/{subcategoryname}", "~/Products/CategoryListing.aspx");
        routes.MapPageRoute("CategoryListing1", "SubCategories/{categoryname}", "~/Products/CategoryListing.aspx");

        //Product listing pages
        routes.MapPageRoute("ProductListingLevel1", "Category/{categoryname}", "~/Products/ProductListing.aspx");
        routes.MapPageRoute("ProductListingLevel2", "SubCategory/{categoryname}/{subcategoryname}", "~/Products/ProductListing.aspx");
        routes.MapPageRoute("ProductListingLevel3", "SubCategory/{categoryname}/{subcategoryname}/{subsubcategoryname}", "~/Products/ProductListing.aspx");
        routes.MapPageRoute("SectionListing", "IconCategory/{sectionname}", "~/Products/ProductListing.aspx");

        routes.MapPageRoute("SearchProducts", "search/{keyword}", "~/Products/ProductListing.aspx");

        //Product detail page
        routes.MapPageRoute("ProductDetail1", "details/{categoryname}/{ProductSKUId}_{productname}", "~/Products/ProductDetails.aspx");
        routes.MapPageRoute("ProductDetail2", "details/{categoryname}/{subcategoryname}/{ProductSKUId}_{productname}", "~/Products/ProductDetails.aspx");
        routes.MapPageRoute("ProductDetail3", "details/{categoryname}/{subcategoryname}/{subsubcategoryname}/{ProductSKUId}_{productname}", "~/Products/ProductDetails.aspx");
        routes.MapPageRoute("SaveImage", "SaveImage/{imagename}", "~/Products/SaveImage.aspx");
        routes.MapPageRoute("CustomRequestForm", "CustomRequestForm", "~/Products/Products_CustomRequest.aspx");

        //Static management detail page
        routes.MapPageRoute("static-page", "info/{pageUrl}", "~/StaticPage/StaticPage.aspx", false);
        routes.MapPageRoute("static-page-preview", "info/{pageUrl}/{preview}", "~/StaticPage/StaticPage.aspx");

        //Do not change the routename of the below url because it has been used for the redirection in many pages.
        routes.MapPageRoute("PageNotFound", "page-not-found", "~/errors/pagenotfound.aspx");
        routes.MapPageRoute("PageNotFound1", "SubCategories", "~/errors/pagenotfound.aspx");
        routes.MapPageRoute("PageNotFound2", "Category", "~/errors/pagenotfound.aspx");
        routes.MapPageRoute("PageNotFound3", "SubCategory", "~/errors/pagenotfound.aspx");
        routes.MapPageRoute("PageNotFound4", "IconCategory", "~/errors/pagenotfound.aspx");
        routes.MapPageRoute("PageNotFound5", "search", "~/errors/pagenotfound.aspx");
        routes.MapPageRoute("PageNotFound6", "details", "~/errors/pagenotfound.aspx");
        routes.MapPageRoute("PageNotFound7", "MyAccount", "~/errors/pagenotfound.aspx");
            
        //Shopping cart pages
        //Do not change the routename of the below url because it has been used for the redirection in many pages.
        routes.MapPageRoute("BasketPage", "Checkout/ShoppingCart", "~/ShoppingCart/Basket.aspx");
        routes.MapPageRoute("checkoutPage", "checkout", "~/ShoppingCart/payment.aspx");
        routes.MapPageRoute("checkoutPagePurchase", "checkout/Purchase", "~/ShoppingCart/creditcardpayment.aspx");
        routes.MapPageRoute("OrderConfirmation", "checkout/OrderConfirmation", "~/ShoppingCart/OrderConfirmation.aspx");
        routes.MapPageRoute("checkoutPagePayment", "checkout/Payment", "~/ShoppingCart/TelecashPayment.aspx");   
        
        //Shopping cart for Punchout
        routes.MapPageRoute("BasketPagePunchout", "Checkout/ShoppingCartPunchout", "~/ShoppingCart/BasketPunchout.aspx");
        
        /*Sachin Chauhan Start: 09 11 2015 :*/
        routes.MapPageRoute("Logout", "Logout", "~/Login/logout.aspx");
        routes.MapPageRoute("QuickShop", "QuickShop/QuickShop", "~/QuickShop/QuickShop.aspx");
        routes.MapPageRoute("MyOrders", "MyAccount/OrderHistory", "~/Orders/OrderHistory.aspx");
        /*Sachin Chauhan End:*/

        // SSO Pages Section
        routes.MapPageRoute("MaerskSSO", "MaerskSSO", "~/Login/MaerskSSOLogin.aspx");
        routes.MapPageRoute("MaerskOilSSO", "MaerskOilSSO", "~/Login/MaerskOil.aspx");
        routes.MapPageRoute("MaerskDrillSSO", "MaerskDrillSSO", "~/Login/MaerskDrillSSO.aspx");
        routes.MapPageRoute("SiteMap", "Site-Map", "~/SiteMap/SiteMap.aspx");

        routes.MapPageRoute("SKFLanding", "SKFLanding", "~/Home/SKF_Landing.aspx");

        // SSO Page Section for Indeed Store
        routes.MapPageRoute("IndeedBrandStoreSSO", "IndeedBrandStoreSSO", "~/Login/IndeedSSO.aspx");
        routes.MapPageRoute("IndeedEmployeeStoreSSO", "IndeedEmployeeStoreSSO", "~/Login/IndeedEmployeeStoreSSO.aspx");
        routes.MapPageRoute("StoreSelection", "StoreSelection", "~/MyAccount/StoreSelection.aspx");
                
        // SSO Page for Michelin
        routes.MapPageRoute("MichelinSSO", "MichelinSSO", "~/Login/MichelinSSO.aspx");            
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        Exception exUnhandled = HttpContext.Current.Server.GetLastError();
        Exceptions.WriteExceptionLog(exUnhandled);
        //Response.Redirect("~/page-not-found");
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
    void Application_BeginRequest(object sender, EventArgs e)
    {
        HttpContext.Current.Response.AddHeader("x-frame-options", "SAMEORIGIN");
        HttpApplication application = ((HttpApplication)(sender));
        HttpRequest request = application.Request;
        HttpResponse responce = application.Response;
        string absoluteUri = request.Url.AbsoluteUri;

        //if (!absoluteUri.ToLower().Contains("localhost"))
        //{
        //    if (!absoluteUri.ToLower().Contains("punchout") && !absoluteUri.ToLower().Contains("customerror")
        //    && !absoluteUri.ToLower().Contains("/webresource.axd?") && !absoluteUri.ToLower().Contains("/scriptresource.axd?"))
        //    {
        //        if (absoluteUri.ToLower().Contains("http://"))
        //        {
        //            responce.Redirect(absoluteUri.Replace("http://", "https://"), true);
        //        }
        //    }
        //}
    }
    
</script>
