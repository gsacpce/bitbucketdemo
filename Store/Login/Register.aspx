﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Login_RegistrationNew" MasterPageFile="~/Master/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script src="https://apis.google.com/js/platform.js"></script>
	   <meta name="google-signin-client_id" content="599052459038-cvf866gu2v91q528m7aghk6l95ebsjau.apps.googleusercontent.com">
	<script type="text/javascript">
		function onSignIn(googleUser) {
			var profile = googleUser.getBasicProfile();
			var loc = hostV + 'Home/callback.aspx?name=' + profile.getName() + '&email=' + profile.getEmail() + '&firstname=' + profile.getGivenName() + '&lastname=' + profile.getFamilyName() + ' &Case=Register';
			window.location.href = loc;

		}
</script>
	<script type="text/javascript">


		$(document).ready(function () {
			// $('.form-control').bind('cut copy paste drop', function (e) {		
			// $('input[type=text]').bind('cut copy paste drop', function (e) {		
			//     e.preventDefault();		
			// })		

			//$('.form-control').keypress(function (e) {		
			$('input[type=text]').keypress(function (e) {
				var regex = new RegExp("^[a-zA-Z0-9+-',@]+$");
				var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
				if (regex.test(str)) {
					return true;
				}
				else {
					if (e.which == "32" || e.which == "8" || e.which == "0") {
						return true;
					}
					else {
						e.preventDefault();
						return false;
					}
				}
			});
		});
	</script>

	<div id="divMainContent">
		<section id="REGISTER-PAGE">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-9">
						<h2 id="header" class="pageSubTitle" runat="server"></h2>
						<p id="subHeader" class="pageText" runat="server"></p>
					</div>
				</div>
			   <div class="row" runat="server" id="divSocialLoginRegister" visible="false">
					<div class="container" id="divGPlusLogin" runat="server" clientidmode="Static">
						<div class="g-signin2" data-onsuccess="onSignIn"></div>
					</div>
				</div>
				<br>
				<br>
				
				<div class="row" id="divRegister" runat="server" visible="true">
					<div class="col-xs-12">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel customPanel  panel-default">
								<!-- PANEL 1 PERSONAL DETAILS -->
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" id="a1">
									<div class="customAccordions panel-heading  panel-heading-a" role="tab" id="headingOne">
										<h4 class="panel-title customAccordionsText" id="Register_PersonalDetails" runat="server"></h4>
									</div>
								</a>

								<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body stausdiv">
										<div class=" col-md-6 form-horizontal" style="margin-bottom: 15px">
											<div class="panel panel-info">
												<div class="panel-heading customPanel" id="subSection1" runat="server"></div>
												<div class="panel-body">
													<div class="form-group">
														<asp:Panel ID="pnlUserType" runat="server">
															<div id="divddlUserType" runat="server">
																<asp:Label ID="lblUserTypes" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
																<div class="col-sm-9 col-md-8">
																	<asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
																	<asp:RequiredFieldValidator ID="reqddlUserType" runat="server" ControlToValidate="ddlUserType" CssClass="text-danger" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit"></asp:RequiredFieldValidator>
																</div>
															</div>

															<asp:Repeater ID="rptCustomMapping" runat="server" OnItemDataBound="rptUserTypeCustomFields_OnItemDataBound">
																<HeaderTemplate>
																	<div id="divInnerCustomFields" class="form-group"></div>
																</HeaderTemplate>
																<ItemTemplate>
																	<asp:Label ID="lblCustomFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
																	<asp:HiddenField ID="hdnRegistrationFieldsConfigurationId" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
																	<div class="col-sm-9 col-md-8 ">
																		<asp:HiddenField ID="hdfControl" runat="server" />
																		<asp:HiddenField ID="hdfColumnName" runat="server" />
																		<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCustomFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" OnTextChanged="txtCustomFieldName_TextChanged" />
																		<asp:DropDownList ID="ddlCustomValue" runat="server" CssClass="form-control">
																		</asp:DropDownList>
																		<asp:CheckBoxList ID="chkCustomValue" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
																		<asp:RequiredFieldValidator ID="reqtxtCustomFieldName" runat="server" ControlToValidate="txtCustomFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																		<asp:RequiredFieldValidator ID="reqddlCustomValue" runat="server" ControlToValidate="ddlCustomValue" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																	</div>
																</ItemTemplate>
																<FooterTemplate>
																</FooterTemplate>

															</asp:Repeater>
														</asp:Panel>
													</div>

													<div class="form-group">
														<label for="" class="col-sm-3 col-md-4 control-label customLabel" id="lblTitle" runat="server"></label>
														<div class="col-sm-9 col-md-8">
															<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtTitles" autocomplete="off" />
															<asp:RequiredFieldValidator ID="reqtxtTitles" runat="server" ControlToValidate="txtTitles" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>															
														</div>
													</div>

													<div class="form-group">
														<label for="" class="col-sm-3 col-md-4 control-label customLabel" id="lblFirstName" runat="server"></label>
														<div class="col-sm-9 col-md-8">
															<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtFirstName" autocomplete="off" onblur="return CheckTextbox(this.id)" />
															<asp:RequiredFieldValidator ID="reqtxtFirstName" runat="server" ControlToValidate="txtFirstName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
														</div>
													</div>

													<div class="form-group">
														<label for="" class="col-sm-3 col-md-4 control-label customLabel" id="lblLastName" runat="server"></label>
														<div class="col-sm-9 col-md-8">
															<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtLastName" autocomplete="off" onblur="return CheckTextbox(this.id)" />
															<asp:RequiredFieldValidator ID="reqtxtLastName" runat="server" ControlToValidate="txtLastName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
														</div>
													</div>
													<div class="form-group">
														<label for="" class="col-sm-3 col-md-4 control-label customLabel" id="lblEmail" runat="server"></label>
														<div class="col-sm-9 col-md-8">
															<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtEmail" autocomplete="off" />
															<asp:RequiredFieldValidator ID="reqtxtEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
														</div>
													</div>
													<div class="form-group">
														<label for="" class="col-sm-3 col-md-4 control-label customLabel" id="lblPassword" runat="server"></label>
														<div class="col-sm-9 col-md-8">
															<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtPassword" autocomplete="off" TextMode="Password" />
															<asp:RequiredFieldValidator ID="reqtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>

															<div style="display: none">
																<asp:RegularExpressionValidator ID="regexPwdLentxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
																<asp:RegularExpressionValidator ID="regexAlphaNumtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
																<asp:RegularExpressionValidator ID="regexAlphaNumSymtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
															</div>
															<asp:HiddenField ID="hdnPasswordPolicyType" runat="server" />
															<asp:HiddenField ID="hdnMinPasswordLength" runat="server" />
															<asp:Label ID="lblPasswordPolicyMessage" runat="server" CssClass="text-danger"></asp:Label>
														</div>
													</div>
													<div class="form-group">
														<label for="" class="col-sm-3 col-md-4 control-label customLabel" id="lblConfirmPassword" runat="server"></label>
														<div class="col-sm-9 col-md-8">
															<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtConfirmPassword" TextMode="Password" autocomplete="off" />
															<asp:RequiredFieldValidator ID="reqtxtConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
															<asp:CompareValidator ID="cmpValidatorPwd" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:CompareValidator>
														</div>
													</div>
													<div class="form-group">
														<asp:Label ID="lblPwdLength" runat="server" CssClass="col-xs-12 col-sm-12 col-md-9 control-label customLabel custom"></asp:Label>
													</div>
												</div>
											</div>
										</div>
										<!-- COLUMN END -->
										<div class=" col-md-6">
											<p id="loginInfo" class="pageText" runat="server"></p>
										</div>
										<div class="col-xs-12" style="text-align: right; margin-top: 15px">
											<div class="button_section">
												<asp:LinkButton ID="btnC1" PostBackUrl="~/Index" runat="server" CssClass="btn btn-default customButton"></asp:LinkButton>
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" class="btn btn-primary collapsed customActionBtn" aria-controls="collapseTwo"><%=Register_Next_Button%></a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="panel customPanel  panel-default">
								<!-- PANEL 2 ADDRESSES -->
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" id="a2" aria-controls="collapseTwo">
									<div class="customAccordions panel-heading  panel-heading-a" role="tab" id="headingTwo">
										<h4 class="panel-title customAccordionsText" id="section2" runat="server"></h4>
									</div>
								</a>
								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6 form-horizontal">
												<div class="panel panel-info">
													<div class="panel-heading customPanel" id="sub1" runat="server"></div>
													<div class="panel-body" id="divInvoiceAddress">
														<asp:Repeater ID="rptInvoiceAddress" runat="server" OnItemDataBound="rptAddress_ItemDataBound">
															<HeaderTemplate>
																<div id="divInnerInvoiceAddress">
															</HeaderTemplate>
															<ItemTemplate>
																<div class="form-group">
																	<asp:Label ID="lblRegistrationFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
																	<div class="col-sm-9 col-md-8">
																		<asp:HiddenField ID="hdfColumnName" runat="server" />
																		<asp:TextBox CssClass="form-control customInput" runat="server" onkeyup="javascript:IsBoxChecked();" ID="txtRegistrationFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" OnChange="CheckForAddress(this.id)"/>
                                                                        <asp:Label ID="lblValidates" runat="server" style="display: none;"></asp:Label>
																		<asp:DropDownList ID="ddlRegisterCountry" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlRegisterCountry_SelectedIndexChanged">
																		</asp:DropDownList>
																		<asp:RequiredFieldValidator ID="reqtxtRegistrationFieldName" runat="server" ControlToValidate="txtRegistrationFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																		<asp:RequiredFieldValidator ID="reqddlRegisterCountry" runat="server" ControlToValidate="ddlRegisterCountry" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																	</div>
																</div>
															</ItemTemplate>
															<FooterTemplate>
																</div>
															</FooterTemplate>
														</asp:Repeater>
													</div>
													<div class="col-sm-12">
														<div class=" checkbox-inline" style="width: 100%; height: 45px; padding-left: 5px">
															<asp:CheckBox ID="chkRegSameAsDelv" CssClass="customLabel" runat="server" onchange="setInvoiceAddress(this.checked);" />
														</div>
													</div>
												</div>
											</div>
											<!-- COLUMN END -->
											<div class="col-sm-12 col-md-6">
												<div class="panel panel-info form-horizontal" id="divRegDeliveryAddress">
													<div class="panel-heading customPanel" id="sub2" runat="server"></div>
													<div class="panel-body" id="divDeliveryAddress">
														<asp:Repeater ID="rptDeliveryAddress" runat="server" OnItemDataBound="rptAddress_ItemDataBound">
															<HeaderTemplate>
																<div id="divInnerDeliveryAddress">
															</HeaderTemplate>
															<ItemTemplate>
																<div class="form-group">
																	<asp:Label ID="lblRegistrationFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
																	<div class="col-sm-9 col-md-8">
																		<asp:HiddenField ID="hdfColumnName" runat="server" />
																		<asp:TextBox CssClass="form-control customInput" runat="server" onkeyup="javascript:IsBoxChecked();" ID="txtRegistrationFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" OnChange="CheckForAddress(this.id)"/>
                                                                        <asp:Label ID="lblValidates" runat="server" style="display: none;"></asp:Label>
																		<asp:DropDownList ID="ddlRegisterCountry" runat="server" CssClass="form-control">
																		</asp:DropDownList>
																		<asp:RequiredFieldValidator ID="reqtxtRegistrationFieldName" runat="server" ControlToValidate="txtRegistrationFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																		<asp:RequiredFieldValidator ID="reqddlRegisterCountry" runat="server" ControlToValidate="ddlRegisterCountry" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																	</div>
																</div>
															</ItemTemplate>
															<FooterTemplate>
																</div>
															</FooterTemplate>
														</asp:Repeater>
													</div>
												</div>
											</div>
											<!-- COLUMN END -->
											<div class="col-xs-12" style="text-align: right; margin-top: 15px">
												<div class="button_section">
													<asp:LinkButton ID="btnC2" PostBackUrl="~/Index" runat="server" CssClass="btn btn-default customButton"></asp:LinkButton>
													<a id="aLnkUserType" runat="server" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseUserType" aria-expanded="false" class="btn btn-primary collapsed customActionBtn" aria-controls="collapseThree"><%=Register_Next_Button%></a>
													<a id="alnkMkt" runat="server" visible="false" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMkt" aria-expanded="false" class="btn btn-primary collapsed customActionBtn" aria-controls="collapseThree"><%=Register_Next_Button%></a>
												</div>
											</div>
										</div>
										<!-- ROW END -->
									</div>
								</div>
							</div>

							<div class="panel customPanel  panel-default" id="divUserType" runat="server">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" id="aUserType" href="#collapseUserType" aria-expanded="false" aria-controls="collapseThree">
									<div class="customAccordions panel-heading  panel-heading-a" role="tab">
										<h4 class="panel-title customAccordionsText" id="headingUserType" runat="server"></h4>
									</div>
								</a>
								<div id="collapseUserType" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body panelpadding">

										<div class="panel-body panelpadding">
											<div class="col-sm-12 col-md-6">
												
											<div class="row form-group">
												<div class="col-sm-2 text-right">
													<asp:Label ID="lblPreferredLanguage" CssClass=" control-label customLabel" runat="server" Visible="false"></asp:Label>
												</div>
												<div class="col-sm-6">
													<asp:DropDownList ID="drpdwnPreferredLanguage" class="form-control col-sm-6" runat="server" Visible="false"></asp:DropDownList>
												</div>
											</div>
											<div class="row form-group">
												<div class="col-sm-2 text-right">
													<asp:Label ID="lblPreferredCurrency" CssClass="control-label customLabel" runat="server" Visible="false"></asp:Label>
												</div>
												<div class="col-sm-6">
													<asp:DropDownList ID="drpdwnPreferredCurrency" class="form-control" runat="server" Visible="false"></asp:DropDownList>
												</div>
											</div>
												<asp:Panel ID="pnlHierarchy" runat="server">
												<div class="form-group" id="divHierarchy" runat="server">
													<asp:Label ID="lblUserType" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
													<div class="col-sm-9 col-md-8">
														<asp:DropDownList ID="ddlHierarchy" runat="server" AutoPostBack="true" Visible="false" ></asp:DropDownList>
													</div>
												</div>
												<div class="form-group" id="divHierarchyUserlist" runat="server" visible="false">
													<asp:Label ID="lblHeirarchyUserList" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
													<div class="col-sm-9 col-md-8">
														<asp:DropDownList ID="ddlHierarchyUserList" runat="server" AutoPostBack="true"></asp:DropDownList>
													</div>
												</div>
												<div class="form-group" id="divCustomFields">
													<asp:Repeater ID="rptCustomFields" runat="server" OnItemDataBound="rptCustomFields_OnItemDataBound">
														<HeaderTemplate>
															<div id="divInnerCustomFields">
														</HeaderTemplate>
														<ItemTemplate>
															<div class="form-group">
																<asp:Label ID="lblCustomFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>

																<div class="col-sm-9 col-md-8">
																	<asp:HiddenField ID="hdfControl" runat="server" />
																	<asp:HiddenField ID="hdfColumnName" runat="server" />
																	<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCustomFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" />
																	<asp:DropDownList ID="ddlCustomValue" runat="server" CssClass="form-control">
																	</asp:DropDownList>
																	<asp:CheckBoxList ID="chkCustomValue" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
																	<asp:RequiredFieldValidator ID="reqtxtCustomFieldName" runat="server" ControlToValidate="txtCustomFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																	<asp:RequiredFieldValidator ID="reqddlCustomValue" runat="server" ControlToValidate="ddlCustomValue" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																</div>
															</div>
														</ItemTemplate>
														<FooterTemplate>
															</div>
														</FooterTemplate>
													</asp:Repeater>
												</div>
											</asp:Panel>
											<!--OLD LOCATION of HEIRARCHY PANEL -->	
											<!-- OLD LOCATION OF pnlUserType, NOW MOVED TO FIRST PANEL BY SHRIGANESH SINGH 14 JULY 2016 -->

											<asp:Repeater ID="rptUserTypeCustomFields" runat="server" OnItemDataBound="rptUserTypeCustomFields_OnItemDataBound">
													<HeaderTemplate>
														<div id="divInnerCustomFields">
													</HeaderTemplate>
													<ItemTemplate>
														<div class="form-group">
															<asp:Label ID="lblCustomFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
															<asp:HiddenField ID="hdnRegistrationFieldsConfigurationId" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
															<div class="col-sm-9 col-md-8 form-group">
																<asp:HiddenField ID="hdfControl" runat="server" />
																<asp:HiddenField ID="hdfColumnName" runat="server" />
																<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCustomFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" />
																<asp:DropDownList ID="ddlCustomValue" runat="server" CssClass="form-control">
																</asp:DropDownList>
																<asp:CheckBoxList ID="chkCustomValue" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
																<asp:RequiredFieldValidator ID="reqtxtCustomFieldName" runat="server" ControlToValidate="txtCustomFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
																<asp:RequiredFieldValidator ID="reqddlCustomValue" runat="server" ControlToValidate="ddlCustomValue" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
															</div>
														</div>
													</ItemTemplate>
													<FooterTemplate>
														</div>
													</FooterTemplate>
												</asp:Repeater>

											<asp:Repeater ID="rptCustomFieldsAll" runat="server" OnItemDataBound="rptUserTypeCustomFields_OnItemDataBound">
													<HeaderTemplate>
														<div id="divInnerCustomFields">
													</HeaderTemplate>
													<ItemTemplate>
														<asp:Label ID="lblCustomFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
														<asp:HiddenField ID="hdnRegistrationFieldsConfigurationId" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
														<div class="col-sm-9 col-md-8 form-group">
															<asp:HiddenField ID="hdfControl" runat="server" />
															<asp:HiddenField ID="hdfColumnName" runat="server" />
															<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCustomFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" />
															<asp:DropDownList ID="ddlCustomValue" runat="server" CssClass="form-control">
															</asp:DropDownList>
															<asp:CheckBoxList ID="chkCustomValue" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
															<asp:RequiredFieldValidator ID="reqtxtCustomFieldName" runat="server" ControlToValidate="txtCustomFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
															<asp:RequiredFieldValidator ID="reqddlCustomValue" runat="server" ControlToValidate="ddlCustomValue" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
														</div>
													</ItemTemplate>
													<FooterTemplate>
														</div>
													</FooterTemplate>
												</asp:Repeater>

											</div>
										</div>
										<div class="radio">
											<asp:RadioButton ID="rdProfile_MarketingOption1" runat="server" GroupName="optionsRadios" Checked="true" />
										</div>
										<div class="radio">
											<asp:RadioButton ID="rdProfile_MarketingOption2" runat="server" GroupName="optionsRadios" />
										</div>
										<br />
										<div style="text-align: right; margin-top: 15px" class="col-xs-12">
											<div class="button_section">
												<asp:LinkButton ID="lnkUserTypeCancel" runat="server" PostBackUrl="~/Index" Text="" CssClass="btn btn-default customButton"></asp:LinkButton>
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" class="btn btn-primary collapsed customActionBtn" aria-controls="collapseThree"><%=Register_Next_Button%></a>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="panel customPanel  panel-default hide">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" id="aMk" href="#collapseMkt" aria-expanded="false" aria-controls="collapseThree">
									<div class="customAccordions panel-heading  panel-heading-a" role="tab" id="headingMkt">
										<h4 class="panel-title customAccordionsText" id="hMarketing_Preferences" runat="server"></h4>
									</div>
								</a>
								<div id="collapseMkt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body panelpadding">


										<div style="text-align: right; margin-top: 15px" class="col-xs-12">
											<div class="button_section">
												<asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Index" Text="" CssClass="btn btn-default customButton"></asp:LinkButton>
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" class="btn btn-primary collapsed customActionBtn" aria-controls="collapseThree"><%=Register_Next_Button%></a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="panel customPanel  panel-default marginTop">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" id="a3" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									<div class="customAccordions panel-heading  panel-heading-a" role="tab" id="headingThree">
										<h4 class="panel-title customAccordionsText" id="section3" runat="server"></h4>
									</div>
								</a>
								<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body">
										<div class=" col-md-6 form-horizontal">
											<div class="panel panel-info">
												<div class="panel-heading customPanel" id="subSection3" runat="server"></div>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-sm-3 col-md-4 hidden-xs">&nbsp;</div>
														<div class="col-xs-12 col-sm-9 col-md-8">
															<img id="CaptchaImg" src="" runat="server" ondragstart="return false;" ondrop="return false;" />
														</div>
													</div>
													<div class="form-group">
														<label for="" class="col-sm-3 col-md-4 control-label customLabel" id="lblVerification" runat="server"></label>
														<div class="col-sm-9 col-md-8">
															<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCaptcha" autocomplete="off" onblur="return CheckTextbox(this.id)" />
															<asp:RequiredFieldValidator ID="reqtxtCaptcha" runat="server" ControlToValidate="txtCaptcha" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- COLUMN END -->
										<div class="col-xs-12" style="text-align: right; margin-top: 15px">
											<div class="button_section">
												<asp:LinkButton ID="btnC3" runat="server" PostBackUrl="~/Index" CssClass="btn btn-default customButton"></asp:LinkButton>
												<asp:LinkButton ID="btnR1" runat="server" PostBackUrl="~/UserRegistration" CssClass="btn btn-default customButton"></asp:LinkButton>
												<asp:Button ID="btnConfirm" runat="server" OnClientClick="ValidateDetails();" OnClick="btnConfirm_Click" CssClass="btn btn-primary collapsed customActionBtn buttonmargin" Text="Confirm registration" ValidationGroup="OnClickSubmit" /><%-- OnClientClick="return Validate();" --%>
												<asp:ValidationSummary ID="vSumm" runat="server" DisplayMode="List" ShowMessageBox="true" ValidationGroup="OnClickSubmit" ShowSummary="false" />
											</div>
										</div>
										<!-- COLUMN END -->
									</div>
								</div>
							</div>

						</div>
					</div>
					<!-- PANEL GROUP END -->
				</div>
				<!-- COLUMN END -->
			</div>
			<!-- ROW END -->

			<!-- CONTAINER END -->
		</section>
	</div>

	<%--<script type="text/javascript" src="../JS/jquery.js"></script>--%>
	<script type="text/javascript">
		function CheckTextbox(clientId) {
			var obj = document.getElementById(clientId);
			var iChars = "^|:<>?"
			for (var i = 0; i < obj.value.length; i++) {
				if (iChars.indexOf(obj.value.charAt(i)) != -1) {
					var spn = document.getElementById('WarningMessage');
					spn.innerHTML = "Text has special characters. \nThese are not allowed.\n Please remove them and try again.";
					obj.value = "";
					$('#myWarningModal').modal('show');
					return false;
				}
			}
			return true;
		}

		function createCode() {
			var random;
			var temp = "";
			for (var i = 0; i < 5; i++) {
				temp += Math.round(Math.random() * 8);
			}
			document.getElementById('<%= CaptchaImg.ClientID %>').src = "Login/JpegImage.aspx?code=" + temp;
			//IsBoxChecked();
		}
	    window.onload = createCode();

	    function CheckForAddress(clientId) {
	        debugger;
	        if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value == "") {
	            document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'inherit';
	        }
	        else {
	            document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'none';
	        }
	        if (clientId == 'ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_1') {
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_2') {
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_4') {
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_2').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_4').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_5') {
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_2').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_4').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_4').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_4').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_5').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_6') {
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_2').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_4').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_4').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_4').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_5').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_5').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_5').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_8').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_8').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_8').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_6').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_8') {
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_1').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_2').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_2').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_4').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_4').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_4').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_5').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_5').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_5').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_6').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_6').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_lblValidates_8').style.display = 'none';
	            }
	        }
	        if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_0').value == "") {
	            document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'inherit';
	        }
	        else {
	            document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'none';
	        }
	        if (clientId == 'ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_1') {
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_2') {
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_4') {
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_2').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_4').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_5') {
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_2').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_4').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_4').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_4').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_5').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_6') {
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddresss_txtRegistrationFieldName_2').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_4').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_4').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_4').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_5').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_5').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_5').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_8').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_8').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_8').style.display = 'none';
	            }
	            document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_6').style.display = 'none'
	        }
	        if (clientId == 'ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_8') {
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_0').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_0').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_1').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_1').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_2').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_2').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_4').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_4').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_4').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_5').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_5').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_5').style.display = 'none';
	            }
	            if (document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_6').value == "") {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_6').style.display = 'inherit';
	            }
	            else {
	                document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_lblValidates_8').style.display = 'none';
	            }
	        }
	    }

		function setInvoiceAddress(objChecked) {
			var chkRegSameAsDelv = document.getElementById('<%= chkRegSameAsDelv.ClientID %>');
			if (chkRegSameAsDelv.checked) {
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_0').value;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_1').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_1').value;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_2').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_2').value;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_3').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_3').value;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_4').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_4').value;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_5').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_5').value;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_6').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_6').value;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_ddlRegisterCountry_7').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_ddlRegisterCountry_7').value;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_8').value = document.getElementById('ContentPlaceHolder1_rptInvoiceAddress_txtRegistrationFieldName_8').value;
				$('#divRegDeliveryAddress').hide();
			}
			else {
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_0').value = "";
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_1').value = "";
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_2').value = "";
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_3').value = "";
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_4').value = "";
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_5').value = "";
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_6').value = "";
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_ddlRegisterCountry_7').selectedIndex = 0;
				document.getElementById('ContentPlaceHolder1_rptDeliveryAddress_txtRegistrationFieldName_8').value = "";
				$('#divRegDeliveryAddress').show();
			}
		}

		function CheckTextbox(clientId) {
			var obj = document.getElementById(clientId);
			var iChars = "^|:<>?"
			for (var i = 0; i < obj.value.length; i++) {
				if (iChars.indexOf(obj.value.charAt(i)) != -1) {
					var spn = document.getElementById('WarningMessage');
					spn.innerHTML = '<%= strRegister_txtSplChar_Message %>';
					obj.value = "";
					$('#myWarningModal').modal('show');
					return false;
				}
			}
			return true;
		}

		// Region Added by SHRIGANESH SINGH 11 May 2016 for Password Policy START
		$(document).ready(function () {
			$("#ContentPlaceHolder1_txtPassword").blur(function () {
				////debugger;
				var MinimumPasswordLength = $('#ContentPlaceHolder1_hdnMinPasswordLength').val();
				var PasswordPolicyType = $('#ContentPlaceHolder1_hdnPasswordPolicyType').val();
				var password = $("#ContentPlaceHolder1_txtPassword").val();
				var passwordLength = $("#ContentPlaceHolder1_txtPassword").val().length;
				var MinPassLenErrorMsg = '<%=strMinPassLenErrorMsg%>';
				var AlphaNumPassReqErrorMsg = '<%=strAlphaNumPassReqErrorMsg%>';
				var AlphaNumSymPassReqErrorMsg = '<%=strAlphaNumSymPassReqErrorMsg%>';
				$("#ContentPlaceHolder1_lblPasswordPolicyMessage").text("");

				if (passwordLength != 0) {
					if (passwordLength < MinimumPasswordLength) {
						$("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
						$("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(MinPassLenErrorMsg);
					} else {

						$("#ContentPlaceHolder1_lblPasswordPolicyMessage").hide();

						// Validate against Alpha Numeric Regular Expression
						if (PasswordPolicyType == "Must Contain Alpha Numeric") {
							var AlphaNumRegex = /^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]/;
							if (AlphaNumRegex.test(password)) {

							}
							else {
								$("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
								$("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(AlphaNumPassReqErrorMsg);
							}
						}

							// Validate against Alpha Numeric and Symbol Regular Expression
						else if (PasswordPolicyType == "Must contain Alphabets Numbers and Symbols") {
							var AlphaNumSymRegex = /^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]/;

							if (AlphaNumSymRegex.test(password)) {

							}

							else {
								$("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
								$("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(AlphaNumSymPassReqErrorMsg);
							}
						}

					}
				}

			})
		})
		// Region Added by SHRIGANESH SINGH 11 May 2016 for Password Policy END
		function showUserType() {

			$('#aUserType').click();
			//$('ContentPlaceHolder1_aLnkUserType').click();
		}
		//function ShowPnl1() {
		//    $('#a1').click();
		//}
		function ShowPnl2() {
			$('#a2').click();

		}
		function CopyInvoiceAddressToDelivery() {
			function CopyInvoiceAddressToDelivery() {
				var chkRegSameAsDelv = document.getElementById('<%= chkRegSameAsDelv.ClientID %>'); var chkRegSameAsDelv = document.getElementById('<%= chkRegSameAsDelv.ClientID %>');
				if (chkRegSameAsDelv.checked) {
					if (chkRegSameAsDelv.checked) {
						setInvoiceAddress(chkRegSameAsDelv.checked); setInvoiceAddress(chkRegSameAsDelv.checked);
					}
				}
			}
		}
		function ValidateEmail() {
			if (!IsValidEmailId($("#ContentPlaceHolder1_txtEmail").val())) {
				$('#myWarningModal').find('#WarningMessage').html('' + strVaildEmailErrorMsg + '');
				$('#myWarningModal').modal('show');
				return false;
			}
		}
		function ValidateDetails() {
			//debugger;
			CopyInvoiceAddressToDelivery();
			ValidateEmail();
		}
		// vikram for apostrophy

		var hostV = "<%=hostV %>";
		var strVaildEmailErrorMsg = "<%=strVaildEmailErrorMsg%>"

	</script>
		<script src="<%=hostV %>/JS/GooglePlusAPI.js"></script>
		<script src="<%=hostV %>/JS/custom.js"></script>

</asp:Content>
