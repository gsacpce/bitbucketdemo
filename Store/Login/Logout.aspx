﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Login_UserLogout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	  <script src="https://apis.google.com/js/platform.js"></script>
    <script src="<%=host %>/JS/jquery.js" type="text/javascript"></script>
   <meta name="google-signin-client_id" content="599052459038-cvf866gu2v91q528m7aghk6l95ebsjau.apps.googleusercontent.com"/>
    <script type="text/javascript">
        $(document).ready(function () {
            gapi.load('auth2', function () {
                var auth2 = gapi.auth2.init({
                    client_id: '599052459038-cvf866gu2v91q528m7aghk6l95ebsjau.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin'
                });
                auth2.then(function () {
                    console.log('User signed out.');
                    // this get called right after token manager is started
                    auth2.signOut();
                    auth2.disconnect();

                });
            });

        });

        $(window).load(function () {
            // executes when complete page is fully loaded, including all frames, objects and images           
            console.log('window load.');
            var host = '<%=host %>';
           var loc = host + 'Signin';
           window.location.href = loc;

        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
