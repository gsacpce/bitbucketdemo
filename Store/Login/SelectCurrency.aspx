﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Login_SelectCurrency"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" ClientIDMode="Static" runat="Server">
    <asp:Literal ID="litCSSnJS" runat="server"></asp:Literal>
    <div id="divMainContent">

        <!-- PAGE CONTENT ------------------------------------------------------------------------------------------------------------------------------------------ -->
        <section id="LOGIN_PAGE" class="container clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <asp:Image ID="imgLanding" runat="server" CssClass="img-responsive"/>
                        <%--<img src="<%=host %>Images/UI/nerve_slider_3.jpg" width="100%">--%>
                    </div>
                </div>

                <div class="row" id="LOGIN_PAGE_HEADING">
                    <div class="col-xs-12">
                        <div class="heading1 pageHeading1">
                            <asp:Literal ID="ltrwelcomehead1" runat="server"></asp:Literal>
                        </div>
                        <div class="heading2 pageHeading2">
                            <asp:Literal ID="ltrwelcomehead2" runat="server"></asp:Literal>
                        </div>
                        <div class="heading3 pageHeading3">
                            <asp:Literal ID="ltrwelcomehead3" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div id="collapseCurrency" style="margin-bottom: 20px" class="collapse in">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="pageSubTitle ">
                                <asp:Literal ID="ltrSelectCurrency" runat="server"></asp:Literal></h3>
                        </div>
                    </div>
                    <div id="divCurrencyArea" runat="server" class="row">
                        <asp:Repeater ID="repaterCurrency" runat="server">
                            <%-- OnItemDataBound="repaterCurrency_ItemDataBound"--%>
                            <ItemTemplate>
                                <div id="divDynamicCurrency" class="col-xs-12 col-md-4">
                                    <asp:LinkButton ID="lnkShowLoginModal" runat="server" CommandName="GetCurrency" CommandArgument='<%#Eval("CurrencyId")+","+ Eval("CurrencySymbol")%>' OnClick="lnkShowLoginModal_Click">
                                        <div class="login_currency_outer customToggle">
                                            <div class="login_currency customCurrencyIcon">
                                                <asp:Literal ID="ltrcurrency" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CurrencySymbol") %>'></asp:Literal>
                                            </div>
                                            <div class="login_currenct_description customCurrencyText hide">
                                                District <%# Container.ItemIndex + 1 %> Optional
                                    <asp:Literal ID="ltrDistrict" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </asp:LinkButton>
                                    <%--</a>--%>
                                </div>
                            </ItemTemplate>

                        </asp:Repeater>
                    </div>

                    <div id="divDropDownCurrency" runat="server" class="row">
                        <div class="col-xs-12 col-sm-4">
                            <asp:DropDownList ID="ddlCurrency" runat="server" Style="width: 100%" CssClass="form control" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="hdnCurr" runat="server" clientidmode="static" value="0" />
            </div>
            <!-- CONTAINER END -->
        </section>
        <!-- PAGE CONTENT ENDS ------------------------------------------------------------------------------------------------------------------------------------- -->
    </div>
</asp:Content>
