﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Login_IndeedSSOLoginPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
        .marginBottom{margin-bottom:10px;}
        </style>
     <div id="divMainContent">
        <section id="INDEEDLOGIN-PAGE">
            <div class="container">
     <div class="form-group row">
         <div class="col-sm-1 control-label customLabel text-right">
      <asp:Label ID="lblEmail" runat="server"></asp:Label>
             </div>
     <div class="col-sm-3">
    <asp:TextBox ID="txtEmailAddress" CssClass="form-control customInput" runat="server" onblur="return ValidateEmail();"></asp:TextBox>
         <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmailAddress" ValidationGroup="UserLogin"
          ForeColor="Red"></asp:RequiredFieldValidator>
         </div>
     </div>
    <div class="form-group row">
        <div class="col-md-1 control-label customLabel text-right">
 <asp:Label ID="lblCountry" runat="server" CssClass="col-sm-3 control-label customLabel"></asp:Label>
             </div>
        <div class="col-md-3">
            <asp:DropDownList runat="server" CssClass="form-control customInput" ID="ddlCountry"></asp:DropDownList>
                </div>
       
         </div>
                </div>
            </section>
         </div>
    <div class="row form-group">
        <div class="col-md-12 text-center marginBottom">
    <asp:Button ID="btnLogin" runat="server" CssClass="btn btn-primary" ValidationGroup="UserLogin" OnClientClick="javascript:return checkTexts();" OnClick="btnLogin_Click"/>
            </div>
</div>
   
    <script type="text/javascript">

        function ValidateEmail() {
            if (!IsValidEmailId($("#ContentPlaceHolder1_txtEmailAddress").val())) {
                $('#myWarningModal').find('#WarningMessage').html('' + strVaildEmailErrorMsg + '');
                $('#myWarningModal').modal('show');
                return false;
            }
        }
        var strVaildEmailErrorMsg = "<%=strVaildEmailErrorMsg%>"
        var strEmailValid="<%=Email_Empty%>"
        function checkTexts() {
            if(document.getElementById("ContentPlaceHolder1_txtEmailAddress").value == "")
            {
                $('#myWarningModal').find('#WarningMessage').html('' + strEmailValid + '');
                $('#myWarningModal').modal('show');
                return false;
            }
        }
        </script>

</asp:Content>

