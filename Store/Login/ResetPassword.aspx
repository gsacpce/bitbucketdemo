﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Login_UserResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="RESET_PASSWORD" class="container">
        <div class="container">
            <!--Forgot Password Form-->
            <div runat="server" id="dvForgot" visible="false" style="margin-top: 25px" class="row">
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        <h1 class="pageSubTitle"><%=Forgot_Password_Text1%></h1>
                        <p class="pageText"><%=Forgot_Password_Text2%></p>
                    </div>
                </div>
                <div class="row form-horizontal">
                    <div style="margin-bottom: 15px" class=" col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading customPanel"><%=Forgot_Password_PanelHead%></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-4 control-label customLabel" for=""><%=Forgot_Password_Email_Lbl%><span style="color: red">*</span></label>
                                    <div class="col-sm-9 col-md-8">
                                        <asp:TextBox ID="txtForgotEmail" autocomplete="off" runat="server" CssClass="form-control customInput" placeholder="Email Address" MaxLength="100"></asp:TextBox>
                                        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtForgotEmail"
                                            ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None"
                                            CssClass="errortext" ForeColor="" SetFocusOnError="True" ValidationGroup="ForgotInfoValidate"></asp:RegularExpressionValidator>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-4 control-label customLabel" for=""><%=Forgot_Password_Captcha_Text %><span style="color: red">*</span></label>
                                    <div class="col-sm-9 col-md-8">
                                        <img id="CaptchaImg" src="" runat="server" ondragstart="return false;" ondrop="return false;" />
                                        <div style="margin-top: 15px;">
                                            <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCaptcha" placeholder="Captcha Text" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="form-group row">
                                    <label class="col-sm-3 col-md-4 control-label" for=""></label>
                                    <div class="col-sm-9 col-md-8">
                                        <asp:Button ID="btnSubmitForgot" runat="server" OnClick="btnSubmitForgot_Click"
                                            CssClass="btn btn-default customActionBtn " OnClientClick="return Validate('1');" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-default customButton" OnClick="btnCancel_Click" />
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div id="dvMsg" runat="server" visible="false">
                    <p class="well pageText"><%=strSuccessfulEmailMessage %></></p>
                </div>
                <div id="dvError" runat="server" visible="false">
                    <p class="well pageText"><%=strRequestFailureMEssage %></p>
                </div>

            </div>
            <!--Forgot Password Form-->
            <!--Reset Password Form-->
            <div id="dvReset" runat="server" visible="true" style="margin-top: 25px" class="row">

                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        <h1 class="pageSubTitle"><%=strResetPassword %></h1>
                        <p class="pageText"><%=strDesirePasswordText %></p>
                    </div>
                </div>

                <div class="row form-horizontal">
                    <div style="margin-bottom: 15px" class=" col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading customPanel"><%=strResetPassword %></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-4 control-label customLabel" for=""><%=strPassword %><span style="color: red">*</span></label>
                                    <div class=" col-sm-9 col-md-8 ">
                                        <asp:TextBox ID="txtPassword" autocomplete="off" runat="server" CssClass="form-control customInput" placeholder="Password" TextMode="Password"></asp:TextBox>

                                        <!-- Code Added by SHRIGANESH SINGH for Password Policy START 13 May 2016  -->

                                        <asp:RequiredFieldValidator ID="reqtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                        <div style="display: none">
                                            <asp:RegularExpressionValidator ID="regexPwdLentxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regexAlphaNumtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regexAlphaNumSymtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                        </div>

                                        <asp:HiddenField ID="hdnPasswordPolicyType" runat="server" />
                                        <asp:HiddenField ID="hdnMinPasswordLength" runat="server" />
                                        <asp:Label ID="lblPasswordPolicyMessage" runat="server" CssClass="text-danger"></asp:Label>

                                        <!-- Code Added by SHRIGANESH SINGH for Password Policy END 13 May 2016 -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-4 control-label customLabel" for=""><%=strConfirmPassword %><span style="color: red">*</span></label>
                                    <div class=" col-sm-9 col-md-8 ">
                                        <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtConfirmPassword" placeholder="Confirm Password" autocomplete="off" TextMode="Password" />

                                        <!-- Code Added by SHRIGANESH SINGH for Password Policy START 13 May 2016 -->
                                        <asp:RequiredFieldValidator ID="reqtxtConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cmpValidatorPwd" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:CompareValidator>
                                        <!-- Code Added by SHRIGANESH SINGH for Password Policy START 13 May 2016 -->

                                    </div>
                                </div>
                                <br />
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-4 control-label customLabel" for=""></label>
                                    <div class=" col-sm-9 col-md-8 customInput">
                                        <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                            CssClass="btn btn-primary customActionBtn" ValidationGroup="OnClickSubmit" OnClientClick="return Validate('2');" />

                                    </div>
                                    <div class="button_section">
                                        <asp:ValidationSummary ID="vSumm" runat="server" DisplayMode="List" ShowMessageBox="true" ValidationGroup="OnClickSubmit" ShowSummary="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!--Reset Password Form-->
        </div>
    </section>
    <script>

        function createCode() {
            var random;
            var temp = "";
            for (var i = 0; i < 5; i++) {
                temp += Math.round(Math.random() * 8);
            }

            console.log(window.location.href.indexOf("Reset"));
            console.log(window.location.href.indexOf("Forgot"));

            if (window.location.href.indexOf("Forgot") > -1) {
                $('#ContentPlaceHolder1_CaptchaImg').attr('src', 'Login/JpegImage.aspx?code=' + temp);
            }
            if (window.location.href.indexOf("Reset") > -1) {

                $('#ContentPlaceHolder1_CaptchaImg').attr('src', '../Login/JpegImage.aspx?code=' + temp);
            }
        }
        window.onload = createCode();


        function Validate(inp) {
            var spn = document.getElementById('WarningMessage');
            var EmptyEmailAddressMessage = "<%= strEmptyEmailAddressMessage%>";
            var EmptyCaptchaMessage = "<%=strEmptyCaptchaMessage%>";
            var EmptyPasswordAndConfirmPasswordMessage = "<%=strEmptyPasswordAndConfirmPasswordMessage%>";

            if (inp == 1) {
                //debugger;
                var emailId = document.getElementById('<%= txtForgotEmail.ClientID %>');
                var captcha = document.getElementById('<%= txtCaptcha.ClientID %>');
                if (emailId.value == "") {
                    spn.innerHTML = EmptyEmailAddressMessage;
                    $('#myWarningModal').modal('show');
                    return false;
                }

                //Email Validation Start
                var x = emailId.value;
                var a = 0;
                var atpos = x.indexOf("@");
                var dotpos = x.lastIndexOf(".");
                if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                    a = 1;
                }
                if (a == 1) {
                    spn.innerHTML = EmptyEmailAddressMessage;
                    $('#myWarningModal').modal('show');
                    return false;
                }
                //Email Validation End

                if (captcha.value == "") {
                    spn.innerHTML = EmptyCaptchaMessage;
                    $('#myWarningModal').modal('show');
                    return false;
                }
            }
            if (inp == 2) {
                var pass1 = document.getElementById('<%= txtPassword.ClientID %>');
                var pass2 = document.getElementById('<%= txtConfirmPassword.ClientID %>');
                if (pass1.value == "" || pass2.value == "") {
                    spn.innerHTML = EmptyPasswordAndConfirmPasswordMessage;
                    $('#myWarningModal').modal('show');
                    return false;
                }
            }
            return true;
        }

        // Region Added by SHRIGANESH SINGH 13 May 2016 for Password Policy START
        $(document).ready(function () {

            $("#ContentPlaceHolder1_txtPassword").blur(function () {
                //debugger;
                var MinimumPasswordLength = $('#ContentPlaceHolder1_hdnMinPasswordLength').val();
                var PasswordPolicyType = $('#ContentPlaceHolder1_hdnPasswordPolicyType').val();
                var password = $("#ContentPlaceHolder1_txtPassword").val();
                var passwordLength = $("#ContentPlaceHolder1_txtPassword").val().length;
                var MinPassLenErrorMsg = "<%=strMinPassLenErrorMsg%>";
                var AlphaNumPassReqErrorMsg = "<%=strAlphaNumPassReqErrorMsg%>";
                var AlphaNumSymPassReqErrorMsg = "<%=strAlphaNumSymPassReqErrorMsg%>";
                $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text("");

                if (passwordLength != 0) {
                    if (passwordLength < MinimumPasswordLength) {
                        $("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
                        $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(MinPassLenErrorMsg);
                    } else {

                        $("#ContentPlaceHolder1_lblPasswordPolicyMessage").hide();

                        // Validate against Alpha Numeric Regular Expression
                        if (PasswordPolicyType == "Must Contain Alpha Numeric") {
                            var AlphaNumRegex = /^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]/;
                            if (AlphaNumRegex.test(password)) {
                            }
                            else {
                                $("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
                                $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(AlphaNumPassReqErrorMsg);
                            }
                        }

                            // Validate against Alpha Numeric and Symbol Regular Expression
                        else if (PasswordPolicyType == "Must contain Alphabets Numbers and Symbols") {
                            var AlphaNumSymRegex = /^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]/;

                            if (AlphaNumSymRegex.test(password)) {
                                // $("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
                                // $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text("Success");
                            }

                            else {
                                $("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
                                $("#ContentPlaceHolder1_lblPasswordPolicyMessage").text(AlphaNumSymPassReqErrorMsg);
                            }
                        }

                    }
                }
            });
        });
        // Region Added by SHRIGANESH SINGH 13 May 2016 for Password Policy END



    </script>
</asp:Content>
