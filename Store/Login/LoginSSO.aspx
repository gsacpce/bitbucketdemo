﻿<%@ Page Title="Login SSO" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Login_LoginSSO" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" ClientIDMode="Static" runat="Server">
    <script type="text/javascript">
        (function () {
            var po = document.createElement('script');
            po.type = 'text/javascript'; po.async = true;
            po.src = 'https://plus.google.com/js/client:plusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();
        /**
* Calls the helper method that handles the authentication flow.
*
* @param {Object} authResult An Object which contains the access token and
*   other authentication information.
*/
        function onSignInCallback(authResult) {
            //helper.onSignInCallback(authResult);
            if (authResult['access_token']) {
                // The user is signed in
                var loc = '<%=Page.ResolveUrl("~/Login/callback.aspx?accessToken=")%>' + authResult['access_token'];
                window.location.href = loc;
            } else if (authResult['error']) {
                // There was an error, which means the user is not signed in.
                // As an example, you can troubleshoot by writing to the console:
                //alert('There was an error: ' + authResult['error']);
            }
               
        }
    </script>


    <div class="container">
        <h1 class="page-heading">
            <asp:Label ID="lblCategoryDescription" runat="server" CssClass="PageHeading2NormalText"
                Text="Click on below button to Sign in using Google+" ToolTip="Login"></asp:Label>
        </h1>
        <div id="plcHolder" clientidmode="Static" runat="server"></div>


    </div>

</asp:Content>
