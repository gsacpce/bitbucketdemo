﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Login_UserLogin" EnableEventValidation="false" %>

<script runat="server">

   
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" ClientIDMode="Static" runat="Server">
 <script src="https://apis.google.com/js/platform.js"></script>
    <meta name="google-signin-client_id" content="599052459038-cvf866gu2v91q528m7aghk6l95ebsjau.apps.googleusercontent.com">
    <script type="text/javascript">
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            var id_token = googleUser.getAuthResponse().id_token;
            var loc = host + 'Home/callback.aspx?name=' + profile.getName() + '&email=' + profile.getEmail() + '&firstname=' + profile.getGivenName() + '&lastname=' + profile.getFamilyName() + ' &Case=Login';
            window.location.href = loc;

        }
        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
            });
        }

    </script>
    <div id="divMainContent">
        <!-- PAGE CONTENT ------------------------------------------------------------------------------------------------------------------------------------------ -->
        <section id="LOGIN_PAGE" class="clearfix">
            <div class="container">
                <div class="row" id="Login_Country_heading" visible="false" runat="server">
                    <%--<div class="col-xs-12">
                        <div class="heading1 pageHeading1">
                            <%=Heading1%>
                        </div>
                        <div class="heading2 pageHeading2">
                            <%=Heading2%>
                        </div>
                        <div class="heading3 pageHeading3">
                            <%=Heading3%>
                        <//div>
                    </div>--%>
                </div>

                <div id="divCountryLandingImg" runat="server" visible="false" class="row">
                    <div class="col-xs-12">
                        <asp:Image ID="imgLandingCountry" runat="server" Width="100%" CssClass="img-responsive" />
                    </div>
                </div>

                <div id="divCurrencyLandingImg" class="row" visible="false" runat="server">
                    <div class="col-xs-12">
                        <asp:Image ID="imgLandingCurrency" runat="server" Width="100%" CssClass="img-responsive" />
                    </div>
                </div>

                <div class="row" id="Login_Currency_heading" runat="server">
                    <div class="col-xs-12">
                        <div class="heading1 pageHeading1">
                            <%=Heading1%>
                        </div>
                        <div class="heading2 pageHeading2">
                            <%=Heading2%>
                        </div>
                        <div class="heading3 pageHeading3">
                            <%=Heading3%>
                        </div>
                    </div>
                </div>

                <div id="collapseCurrency" style="margin-bottom: 20px;" class="collapse in" runat="server">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="pageSubTitle ">
                                <asp:Literal ID="ltrSelectCurrency" runat="server"></asp:Literal>
                            </h3>
                        </div>
                    </div>
                    <div id="divCurrencyArea" runat="server" class="row">
                        <asp:Repeater ID="repaterCurrency" runat="server">
                            <%--OnItemDataBound="repaterCurrency_ItemDataBound"--%>
                            <ItemTemplate>
                                <div id="divDynamicCurrency" class="col-xs-12 col-md-4">
                                    <asp:LinkButton ID="lnkShowLoginModal" runat="server" CommandName="GetCurrency" CommandArgument='<%#Eval("CurrencyId")+","+ Eval("CurrencySymbol")%>' OnClick="lnkShowLoginModal_Click">
                                        <div class="login_currency_outer customToggle">
                                            <div class="login_currency customCurrencyIcon">
                                                <asp:Literal ID="ltrcurrency" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CurrencySymbol") %>'></asp:Literal>
                                            </div>
                                            <div class="login_currenct_description customCurrencyText">
                                                <%--District <%# Container.ItemIndex + 1 %> Optional--%>
                                                <asp:Literal ID="ltrDistrict" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </asp:LinkButton>
                                    <%--</a>--%>
                                </div>
                            </ItemTemplate>

                        </asp:Repeater>
                    </div>
                    <div id="divDropDownCurrency" runat="server" class="row">
                        <div class="col-xs-12 col-sm-4">
                            <asp:DropDownList ID="ddlCurrency" EnableViewState="true" runat="server" Style="width: 100%" CssClass="form control" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div id="div1" runat="server" class="row">
                    <asp:Repeater ID="Repeater1" runat="server">
                        <%--OnItemDataBound="repaterCurrency_ItemDataBound"--%>
                        <ItemTemplate>
                            <div id="divDynamicCurrency" class="col-xs-12 col-md-4">
                                <asp:LinkButton ID="lnkShowLoginModal" runat="server" CommandName="GetCurrency" CommandArgument='<%#Eval("CurrencyId")+","+ Eval("CurrencySymbol")%>' OnClick="lnkShowLoginModal_Click">
                                    <div class="login_currency_outer customToggle">
                                        <div class="login_currency customCurrencyIcon">
                                            <asp:Literal ID="ltrcurrency" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CurrencySymbol") %>'></asp:Literal>
                                        </div>
                                        <div class="login_currenct_description customCurrencyText">
                                            <%--District <%# Container.ItemIndex + 1 %> Optional--%>
                                            <asp:Literal ID="ltrDistrict" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                                <%--</a>--%>
                            </div>
                        </ItemTemplate>

                    </asp:Repeater>
                </div>





                <!-- MODAL OPT IN -->
                <div class="modal fade" id="ModalModalOptInOut" tabindex="-1" role="dialog" aria-labelledby="ModalModalOptInOut" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <asp:Panel ID="pnlSubscribers" runat="server" DefaultButton="btnSaveEmailSubscription">
                                <div class="customModal modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        <asp:Literal ID="ltrSubscibemsg1" runat="server"></asp:Literal></h4>
                                </div>
                                <div class="modal-body">
                                    <div role="search">
                                        <p class="pageText">
                                            <asp:Literal ID="ltrSubscibemsg2" runat="server"></asp:Literal>
                                        </p>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtEmailSubscription" CssClass="form-control customInput" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmailSubscription" ValidationGroup="AddSubscribe"
                                                ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please Enter Valid Email ID" ValidationGroup="AddSubscribe" ControlToValidate="txtEmailSubscription"
                                                CssClass="requiredFieldValidateStyle" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                            </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="modal-footer">
                                <asp:Button ID="btnSaveEmailSubscription" runat="server" Text="Submit" OnClientClick="return validateSubscribersLogin();" OnClick="btnSaveEmailSubscription_Click" ValidationGroup="AddSubscribe" class="btn btn-primary customActionBtn" />
                            </div>
                        </div>
                    </div>
                </div>

                <div id="collapseLogin" class="collapse">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="pageSubTitle"><%=SignIn_Title%>
                                <span id="dvCurrencySel" runat="server">
                                    <%=SignIn_ChangeCurrency%> </span>
                            </h3>
                        </div>
                    </div>
                    <div class="col-xs-12 connect_buttons_outer_login" runat="server" id="divSocialLogIn" visible="false">
                        <p>Connect with your existing social media accounts.</p>
                        <ul>
                            <li>
                                <div class="container" id="divGPlusLogin" runat="server" clientidmode="Static">
                                    <div class="g-signin2" data-onsuccess="onSignIn" ></div>
                                    
                                </div>
                            </li>
                           
                            <%--<li><a>Connect with Facebook</a></li>
                            <li><a>Connect with Twitter</a></li>
                            <li><a>Connect with LinkedIn</a></li>--%>
                        </ul>
                       
                       
                        <p class="connect_paragraphs" runat="server" id="panelLogInMsg" visible="true">Or Log in to the site below.</p>
                    </div>

                    <div class="row form-horizontal" runat="server" id="divLogIn" visible="true">
                        <div class="col-xs-12">
                            <asp:Panel DefaultButton="btnSubmit" runat="server">
                                <div class="customPanel panel panel-info">
                                    <div class="customPanel panel-heading "><%=SignIn_Login%></div>
                                    <div class="panel-body form-horizontal">
                                        <div id="dvCountry" class="form-group" runat="server">
                                            <label for="" class="col-sm-3 control-label customLabel">Country</label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlCountry" runat="server" class="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label customLabel"><%=SignIn_Email%></label>
                                            <div class="col-sm-9">
                                                <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtUserName" autocomplete="off" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label customLabel"><%=SignIn_Password%></label>
                                            <div class="col-sm-9">
                                                <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtPassword" TextMode="Password" autocomplete="off" />
                                            </div>
                                        </div>
                                        <div class="form-group" id="dvCaptcha" runat="server" visible="false">
                                            <label for="" class="col-sm-3 control-label customLabel"><%=SignIn_VerificationCode%></label>
                                            <div class="col-sm-9">
                                                <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCaptcha" placeholder="Captcha Text" autocomplete="off" />
                                            </div>
                                        </div>
                                        <div class="login_panel_right">
                                            <div id="dvCaptcha1" runat="server" visible="false">
                                                <img id="CaptchaImg" src="" runat="server" ondragstart="return false;" ondrop="return false;" />
                                            </div>
                                            <br>
                                            <asp:LinkButton ID="lnkChangeDestion" runat="server" class="btn btn-default customButton" OnClick="lnkChangeDestion_Click" Style="display: none;">Change Destination</asp:LinkButton>
                                            <asp:LinkButton ID="lnkChangeCurrency" runat="server" class="btn btn-default customButton" Visible="false" OnClick="lnkChangeCurrency_Click" Style="display: none;"><%=SignIn_ChangeCurrencyBtn%></asp:LinkButton>
                                            <%-- <a href="javascript:show_currency()" class="btn btn-default customButton">Change currency</a>--%>
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary customActionBtn" Text="Log in" OnClick="btnSubmit_Click" OnClientClick="return Validate();" />
                                        </div>



                                        <div class="login_panel_left">
                                            <a class="pageScrollLinks" href="<%=host %>UserRegistration"><%=SignIn_Register%></a><br>
                                            <a class="pageScrollLinks" href="<%=host %>ForgotPassword"><%=SignIn_ForgotPassword%></a><br>
                                            <a href="#ModalModalOptInOut" data-toggle="modal" class="pageScrollLinks"><%=SignIn_ReceiveCommunication%></a>
                                        </div>
                                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- COLUMN END -->
                    </div>
                    <!-- ROW END -->
                </div>
                <input type="hidden" id="hdnCurr" runat="server" clientidmode="static" value="0" />
            </div>
            <!-- CONTAINER END -->
        </section>
        <!-- PAGE CONTENT ENDS ------------------------------------------------------------------------------------------------------------------------------------- -->
    </div>

    <script type="text/javascript">
        var host = '<%=host %>';


        function Validate() {
            var name = document.getElementById('<%= txtUserName.ClientID %>');
            var pass = document.getElementById('<%= txtPassword.ClientID %>');
            var dv = document.getElementById('<%= dvCaptcha.ClientID %>');
            var spn = document.getElementById('WarningMessage');
            if (name.value == "" || pass.value == "") {
                spn.innerHTML = "Enter Username & Password !!!";
                $('#myWarningModal').modal('show');
                return false;
            }
            if (dv != "" || dv != null) {
                var captcha = document.getElementById('<%= txtCaptcha.ClientID %>');
                if (captcha.value == "") {
                    spn.innerHTML = "Captcha text can not be empty !!!";
                    $('#myWarningModal').modal('show');
                    return false;
                }
            }
            return true;
        }

        function createCode() {
            if ('<%= curryInt %>' == '1') {
                show_login();
            }

            var random;
            var temp = "";
            for (var i = 0; i < 5; i++) {
                temp += Math.round(Math.random() * 8);
            }
            //document.all.CaptchaImg.src = '<%=host %>' + "Login/JpegImage.aspx?code=" + temp;
            if (document.getElementById('<%= CaptchaImg.ClientID %>') != null) {
                document.getElementById('<%= CaptchaImg.ClientID %>').src = '<%=host %>' + "Login/JpegImage.aspx?code=" + temp;
            }
        }
        window.onload = createCode();

        function show_login() {
            $('#collapseLogin').collapse('show');
            $('#collapseCurrency').collapse('hide');

            $('html, body').animate({ scrollTop: $("#collapseLogin").offset().top }, 1500);
        }
        function show_currency() {
            $('#collapseLogin').collapse('hide');
            $('#collapseCurrency').collapse('show');
            $('html, body').animate({ scrollTop: $("#collapseCurrency").offset().top }, 1500);
        }
        function show_suscription() {
        }
        // vikram for apostrophy
        function validateSubscribersLogin() {
            if (IsValidEmailId($("#txtEmailSubscription").val()) == false) {
                $('#myWarningModal').find('#WarningMessage').html('Please enter valid Email Address');
                $('#myWarningModal').modal('show');
                return false;
            }
            else {
                return true;
            }
        }
        // vikram for apostrophy


    </script>
   <script src="<%=host %>/JS/custom.js"></script>

   
</asp:Content>

