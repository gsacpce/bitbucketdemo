﻿$(document).ready(function () {

    //$(".chkShow input[type=checkbox]").click(function () {

    //    if ($(this).is(":checked") == true) {
    //        $(this).parent().parent().parent().find('#dvDefaultLanguage').css('display', 'block');
    //    } else {
    //        $(this).parent(0).parent(0).parent(0).find('#dvDefaultLanguage').css('display', 'none');
    //        $(this).parent().parent().parent().find('#dvDefaultLanguage').find('input:radio').prop("checked", false);
    //    }
    //})

    //$(".chkShowCurrency input[type=checkbox]").click(function () {

    //    if ($(this).is(":checked") == true) {
    //        $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'block');
    //    } else {
    //        $(this).parent(0).parent(0).parent(0).find('#dvDefaultCurrency').css('display', 'none');
    //        $(this).parent().parent().parent().find('#dvDefaultCurrency').find('input:radio').prop("checked", false);
    //    }
    //})

    $(".chkShow input[type=checkbox]").each(function () {
        //debugger;
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultLanguage').css('display', 'block');
        }
        else {
            $(this).parent().parent().parent().find('#dvDefaultLanguage').css('display', 'none');
        }
    })

    $(".chkShowCurrency input[type=checkbox]").each(function () {
        //debugger;
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'block');
            if ($("#rblStoreType_0").is(":checked") == true)
                $(this).parent().parent().parent().find('#dvBASYSDetails').css('display', 'block');
        }
        else {
            $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'none');
        }
    })

    // $(".chkPLDisplayType input[type=checkbox]").click(function () {

        // if ($(this).is(":checked") == true) {
            // $(this).parent().parent().parent().find('#dvDefaultDisplayType').css('display', 'block');
        // } else {
            // $(this).parent(0).parent(0).parent(0).find('#dvDefaultDisplayType').css('display', 'none');
            // $(this).parent().parent().parent().find('#dvDefaultDisplayType').find('input:radio').prop("checked", false);
        // }
    // })

	$(".chkPLDisplayType input[type=checkbox]").each(function () {
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultDisplayType').css('display', 'block');
        }
        else {
            $(this).parent().parent().parent().find('#dvDefaultDisplayType').css('display', 'none');
        }
    })

    $(".chkPLDisplayType input[type=checkbox]").click(function () {

        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultDisplayType').css('display', 'block');
            SetFirstDefaultRadioButton('DefaultDisplayType', $(this).parent().parent().parent().find('#dvDefaultDisplayType').find('input:radio'), $(this).parent().attr("class"),true);
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvDefaultDisplayType').css('display', 'none');
            $(this).parent().parent().parent().find('#dvDefaultDisplayType').find('input:radio').prop("checked", false);
            SetFirstDefaultRadioButton('DefaultDisplayType', $(this).parent().parent().parent().find('#dvDefaultDisplayType').find('input:radio'), $(this).parent().attr("class"),false);
        }
    })
	
	
    $("#rblProductPagination_0").click(function () {
        $('#txtProductsPerPage').removeAttr("disabled");
        //ValidatorEnable($('#cvProductsPerPage'), true);
    })

    $("#rblProductPagination_1").click(function () {
        $('#txtProductsPerPage').attr("disabled", "disabled");
        $('#txtProductsPerPage').val("");
        //ValidatorEnable($('#cvProductsPerPage'), false);
    })

    $(".chkPLSorting input[type=checkbox]").click(function () {

        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvPLSorting').css('display', 'block');
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvPLSorting').css('display', 'none');
            $(this).parent().parent().parent().find('#dvPLSorting').find('input:radio').prop("checked", false);
        }
    })

    $(".chkSCPaymentGateway input[type=checkbox]").click(function () {

        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvSCPaymentGateway').css('display', 'block');
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvSCPaymentGateway').css('display', 'none');
            $(this).parent().parent().parent().find('#dvSCPaymentGateway').find('input:radio').prop("checked", false);
        }
    })

    $(".chkShow input[type=checkbox]").click(function () {
        ////debugger;
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultLanguage').css('display', 'block');
            SetFirstDefaultRadioButton('DefaultLanguage', $(this).parent().parent().parent().find('#dvDefaultLanguage').find('input:radio'), $(this).parent().attr("class"), true);
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvDefaultLanguage').css('display', 'none');
            $(this).parent().parent().parent().find('#dvDefaultLanguage').find('input:radio').prop("checked", false);
            SetFirstDefaultRadioButton('DefaultLanguage', $(this).parent().parent().parent().find('#dvDefaultLanguage').find('input:radio'), $(this).parent().attr("class"), false);
        }
    })

    $(".chkShowCurrency input[type=checkbox]").click(function () {
        //debugger;
        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvDefaultCurrency').css('display', 'block');

            SetFirstDefaultRadioButton('DefaultCurrency', $(this).parent().parent().parent().find('#dvDefaultCurrency').find('input:radio'), $(this).parent().attr("class"), true);
            if ($("#rblStoreType_0").is(":checked") == true) {
                $(this).parent().parent().parent().find('#dvBASYSDetails').css('display', 'block');
                $(this).parent().parent().parent().find('.BASYSPopup').modal('show');
            }
            else {
                $(this).parent().parent().parent().find('#dvBASYSDetails').css('display', 'none');
            }
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvDefaultCurrency').css('display', 'none');
            $(this).parent(0).parent(0).parent(0).find('#dvBASYSDetails').css('display', 'none');
            $(this).parent().parent().parent().find('#dvDefaultCurrency').find('input:radio').prop("checked", false);
            SetFirstDefaultRadioButton('DefaultCurrency', $(this).parent().parent().parent().find('#dvDefaultCurrency').find('input:radio'), $(this).parent().attr("class"), false);
        }

        if ($("#rblStoreType_0").is(":checked") == true) {
            var chkCount = $(".chkShowCurrency input[type=checkbox]:checked").length;
            if (chkCount == 0)
                $(this).parent().parent().parent().parent().find('#dvBASYSDetailHeading').css('display', 'none');
            else
                $(this).parent().parent().parent().parent().find('#dvBASYSDetailHeading').css('display', 'block');
        }
    })

    $(".chkSCAlternativePayment input[type=checkbox]").click(function () {

        if ($(this).is(":checked") == true) {
            $(this).parent().parent().parent().find('#dvSCAlternativePayment').css('display', 'block');
        } else {
            $(this).parent(0).parent(0).parent(0).find('#dvSCAlternativePayment').css('display', 'none');
            $(this).parent().parent().parent().find('#dvSCAlternativePayment').find('input:radio').prop("checked", false);
        }
    })

    $("#rblCRCustomerType_0").click(function () {
        $('#ddlCRCustomerHierarchyLevel').attr("disabled", false);
        $('#dvApprovalRequired').css('display', '');
        $('#dvUserTypesDetails').css('display', 'none');
        $('#dvUserType1').css('display', 'none');
        $('#dvUserType2').css('display', 'none');
        $('#dvUserType3').css('display', 'none');
    })

    $("#rblCRCustomerType_1").click(function () {
        $('#ddlCRCustomerHierarchyLevel').attr("disabled", true);
        $('#ddlCRCustomerHierarchyLevel').val('0');
        $('#dvApprovalRequired').css('display', 'none');
        $('#dvUserTypesDetails').css('display', 'none');
        $('#dvUserType1').css('display', 'none');
        $('#dvUserType2').css('display', 'none');
        $('#dvUserType3').css('display', 'none');
        var rfvUserType1 = $("[id*=rfvUserType1]");
        var rfvUserType2 = $("[id*=rfvUserType2]");
        var rfvUserType3 = $("[id*=rfvUserType3]");
        ValidatorEnable(rfvUserType1[0], false);
        ValidatorEnable(rfvUserType2[0], false);
        ValidatorEnable(rfvUserType3[0], false);
        var cvUserType1CatalogueIds = $("[id*=cvUserType1CatalogueIds]");
        var cvUserType2CatalogueIds = $("[id*=cvUserType2CatalogueIds]");
        var cvUserType3CatalogueIds = $("[id*=cvUserType3CatalogueIds]");
        ValidatorEnable(cvUserType1CatalogueIds[0], false);
        ValidatorEnable(cvUserType2CatalogueIds[0], false);
        ValidatorEnable(cvUserType3CatalogueIds[0], false);
    })

    $("#rblCRApprovalRequired_0").click(function () {
        $('#txtCRNotificationEmail').removeAttr("disabled");
    })

    $("#rblCRApprovalRequired_1").click(function () {
        $('#txtCRNotificationEmail').attr("disabled", "disabled");
        $('#txtCRNotificationEmail').val('');
    })

    $("#chkCLEnableCookie").click(function () {
        if ($(this).is(":checked") == true) {
            $("#txtCLCookieLifetime").removeAttr("disabled");
            $("#txtCLCookieMessage").removeAttr("disabled");
        }
        else {
            $("#txtCLCookieLifetime").attr("disabled", "disabled");
            $("#txtCLCookieMessage").attr("disabled", "disabled");
            $("#txtCLCookieLifetime").val('');
            $("#txtCLCookieMessage").val('');
        }
    })

    $("#chkSCInternationalOrders").click(function () {
        if ($(this).is(":checked") == true)
            $("#txtSCMessage").removeAttr("disabled");
        else {
            $("#txtSCMessage").attr("disabled", "disabled");
            $("#txtSCMessage").val('');
        }
    })

    $("#chkCREnableDomainValidation").click(function () {
        if ($(this).is(":checked") == true) {
            $("#dvCREnableDomainValidation").css("display", "");
            $("#rblCREnableDomainValidation_0").attr("checked", true);
        }
        else
            $("#dvCREnableDomainValidation").css("display", "none");
    })

    $("#chkCREnableEmailValidation").click(function () {
        if ($(this).is(":checked") == true) {
            $("#dvCREnableEmailValidation").css("display", "");
            $("#rblCREnableEmailValidation_0").attr("checked", true);
        }
        else
            $("#dvCREnableEmailValidation").css("display", "none");
    })


    $('#pdffile').change(function () {
        $('#subfile').val($(this).val());
    });

    $("#chkEnablePoints").click(function () {
        //if ($(this).is(":checked") == true) {
            //$("#dvPoints").css("display", "");
            //$("#liPaymentGatewayHeading").css("display", "none");
            //$('#txtPoints').removeAttr("disabled");
        //}
        //else {
            //$("#dvPoints").css("display", "none");
            //$("#liPaymentGatewayHeading").css("display", "");
            //$('#txtPoints').attr("disabled", "disabled");
            //$('#txtPoints').val("");
        //}
	if ($(this).is(":checked") == true) {
            $("#dvPoints").css("display", "");
            $("#liPaymentGatewayHeading").css("display", "none");
            $('#txtPoints').removeAttr("disabled");
            var validatorObject = document.getElementById('ContentPlaceHolder1_cvSCPaymentGateway');
            validatorObject.enabled = false;
            validatorObject.isvalid = true;
            ValidatorUpdateDisplay(validatorObject);
            var validatorObject1 = document.getElementById('ContentPlaceHolder1_cvPointData');
            validatorObject1.enabled = true;
            validatorObject1.isvalid = true;
            ValidatorUpdateDisplay(validatorObject1);
        }
        else {
            $("#dvPoints").css("display", "none");
            $("#liPaymentGatewayHeading").css("display", "");
            $('#txtPoints').attr("disabled", "disabled");
            $('#txtPoints').val("");
            var validatorObject = document.getElementById('ContentPlaceHolder1_cvSCPaymentGateway');
            validatorObject.enabled = true;
            validatorObject.isvalid = true;
            ValidatorUpdateDisplay(validatorObject);
            var validatorObject1 = document.getElementById('ContentPlaceHolder1_cvPointData');
            validatorObject1.enabled = false;
            validatorObject1.isvalid = true;
            ValidatorUpdateDisplay(validatorObject1);
        }
    })

    $('.BASYSCancel').click(function () {
        var IsConfirmed = confirm("Do you want to uncheck this currency. If yes, click OK, else fill the mandatory details and click save.")
        if (IsConfirmed) {
            debugger;
            $('.BASYSPopup').modal('hide');
            $(this).parent().parent().parent().parent().parent().parent().find('.chkShowCurrency input[type=checkbox]').prop("checked", false);
            $(this).parent().parent().parent().parent().parent().parent().find('#dvDefaultCurrency').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('#rbDefaultCurrency').prop("checked", false);
            $(this).parent().parent().parent().parent().parent().parent().find('#dvBASYSDetails').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvCatalogueId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvGroupId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvDivisionId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvDefaultCustomerId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvCatalogueAliasId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvKeyGroupId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvSourceCodeId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvSalesPersonId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvDefaultCompanyId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.rfvHelpDeskEmailId').css("display", "none");
            $(this).parent().parent().parent().parent().parent().parent().find('.revHelpDeskEmailId').css("display", "none");

            var chkCount = $(".chkShowCurrency input[type=checkbox]:checked").length;
            if (chkCount == 0)
                $(this).parent().parent().parent().parent().parent().parent().find('#dvBASYSDetailHeading').css('display', 'none');
            else
                $(this).parent().parent().parent().parent().parent().parent().find('#dvBASYSDetailHeading').css('display', 'block');
        }
        //alert($(this).parent().parent().parent().parent().find('#txtCatalogueId').val());
    })

    $('.BASYSDetail').click(function () {
        $(this).parent().find('.BASYSPopup').modal('show');
    })

    $("#rblCRStoreType_0").click(function () {
        $('#divVatPercentage').css('display', 'none');
    })

    $("#rblCRStoreType_1").click(function () {
        $('#txtB2CVatPercentage').val('');
        $('#divVatPercentage').css('display', '');
    })

    $('#chkEnableFilter').click(function () {
        if ($(this).is(":checked") == true) {
            $(".chkPLFilter input[type=checkbox]").prop("checked", true);
        }
        else {
            $(".chkPLFilter input[type=checkbox]").prop("checked", false);
        }
    })

})

function SetUniqueRadioButton(nameregex, current) {
    re = new RegExp(nameregex);
    for (i = 0; i < document.forms[0].elements.length; i++) {
        elm = document.forms[0].elements[i]
        if (elm.type == 'radio') {
            if (re.test(elm.name)) {
                elm.checked = false;
            }
        }
    }
    current.checked = true;
}

function SetFirstDefaultRadioButton(nameregex, current, chkClass, action) {
    debugger;
    var isChecked = false;
    re = new RegExp(nameregex);
    var chkCount = $("." + chkClass + " input[type=checkbox]:checked").length;

    if (chkCount == 1) {
        for (i = 0; i < document.forms[0].elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'radio') {
                if (re.test(elm.name)) {
                    if ($(elm).is(":checked") == true) {
                        isChecked = true;
                        return;
                    }
                }
            }
        }
        if (action) {
            if (!isChecked) {
                $(current).prop("checked", true);
            }
        }
        else {
            if (!isChecked) {
                $("." + chkClass + " input[type=checkbox]:checked").parent().parent().parent().find('input:radio').prop("checked", true);
            }
        }
    }
}

function ValidateLanguage(sender, args) {
    var inputsC = $("#dvLanguages input:checkbox");
    var inputsR = $("#dvLanguages input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateCurrency(sender, args) {
    var inputsC = $("#dvCurrencies input:checkbox");
    var inputsR = $("#dvCurrencies input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateDisplayType(sender, args) {
    var inputsC = $("#dvDisplayType input:checkbox");
    var inputsR = $("#dvDisplayType input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateProductsPerPage(sender, args) {

    if ($("#rblProductPagination_0").is(":checked") == true) {
        if ($("#txtProductsPerPage").val() == "")
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidatePointData(sender, args) {

    if ($("#chkEnablePoints").is(":checked") == true) {
        if ($.trim($("#txtPoints").val()) == "" || $.trim($("#txtPoints").val()) == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}


function ValidateSorting(sender, args) {
    var inputsC = $("#dvPLSorting input:checkbox");
    var inputsR = $("#dvPLSorting input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateSCPaymentGateway(sender, args) {
    var inputsC = $("#liPaymentGatewayHeading input:checkbox");
    var inputsR = $("#liPaymentGatewayHeading input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateSCAlternativePayments(sender, args) {
    var inputsC = $("#dvSCAlternativePayments input:checkbox");
    var inputsR = $("#dvSCAlternativePayments input:radio");
    for (var i = 0; i < inputsC.length; i++) {
        if (inputsC[i].checked) {
            for (var j = 0; j < inputsR.length; j++) {
                if (inputsR[j].checked) {
                    args.IsValid = true;
                    return;
                }
            }
        }
    }
    args.IsValid = false;
}

function ValidateCRCustomerHierarchyLevel(sender, args) {

    if ($("#rblCRCustomerType_0").is(":checked") == true) {
        if ($("#ddlCRCustomerHierarchyLevel").get(0).selectedIndex == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateUserType1CatalogueIds(sender, args) {

    if ($("#rblCRCustomerType_0").is(":checked") == true) {
        if ($("#ddlUserType1CatalogueIds").get(0).selectedIndex == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateUserType2CatalogueIds(sender, args) {

    if ($("#rblCRCustomerType_0").is(":checked") == true) {
        if ($("#ddlUserType2CatalogueIds").get(0).selectedIndex == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateUserType3CatalogueIds(sender, args) {

    if ($("#rblCRCustomerType_0").is(":checked") == true) {
        if ($("#ddlUserType3CatalogueIds").get(0).selectedIndex == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateCLCookieLifetime(sender, args) {

    if ($("#chkCLEnableCookie").is(":checked") == true) {
        if ($("#txtCLCookieLifetime").val() == '')
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function ValidateCLCookieMessage(sender, args) {

    if ($("#chkCLEnableCookie").is(":checked") == true) {
        if ($("#txtCLCookieMessage").val() == '')
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

function isNumeric(keyCode) {
    if (keyCode == 16 || keyCode == 18)
        isShift = true;

    return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 190 || keyCode == 9 || keyCode == 14 || keyCode == 15 || keyCode == 37 || keyCode == 39 || (keyCode >= 96 && keyCode <= 105)) && isShift == false);
}
var isShift = false;
function keyUP(keyCode) {
    if (keyCode == 16 || keyCode == 18)
        isShift = false;
}

$("#chkCREnableDomainValidation").click(function () {
    if ($(this).is(":checked") == true) {
        $("#dvCREnableDomainValidation").css("display", "");
        $("#rblCREnableDomainValidation_0").attr("checked", true);
    }
    else
        $("#dvCREnableDomainValidation").css("display", "none");
})

$("#chkCREnableEmailValidation").click(function () {
    if ($(this).is(":checked") == true) {
        $("#dvCREnableEmailValidation").css("display", "");
        $("#rblCREnableEmailValidation_0").attr("checked", true);
    }
    else
        $("#dvCREnableEmailValidation").css("display", "none");
})

function ValidateB2CVatPercentage(sender, args) {

    if ($("#rblCRStoreType_1").is(":checked") == true) {
        if ($.trim($("#txtB2CVatPercentage").val()) == "" || $.trim($("#txtB2CVatPercentage").val()) == 0)
            return args.IsValid = false;
        else
            return args.IsValid = true;
    }
}

