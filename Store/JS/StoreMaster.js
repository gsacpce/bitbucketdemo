//// DOCUMENT READY ////
$(document).ready(function () {

    console.log(document.referrer);

//////////////////////////////////// CUSTOM REQUEST PAGE MODIFICATIONS ///////////////////////////
    if($('#CUSTOM_REQUEST_PAGE').length > 0) {
        var contact = $('#ContentPlaceHolder1_lblContactName').parent();
        var email = $('#ContentPlaceHolder1_lblEmail').parent();
        var phone = $('#ContentPlaceHolder1_lblPhoneNumber').parent();
        var event = $('#ContentPlaceHolder1_lblEvent').parent();
        var audience = $('#ContentPlaceHolder1_lblTargetAud').parent();
        var ideas = $('#ContentPlaceHolder1_lblIdeas').parent();
        var quantity = $('#ContentPlaceHolder1_lblQty').parent();
        var budget = $('#ContentPlaceHolder1_lblBudget').parent();
        var colour = $('#ContentPlaceHolder1_lblColour').parent();
        var branding = $('#ContentPlaceHolder1_lblBranding').parent();
        var address = $('#ContentPlaceHolder1_lblDelAdd').parent();
        var date = $('#ContentPlaceHolder1_lblDelDate').parent();
        var comments = $('#ContentPlaceHolder1_lblAddCommnets').parent();
    
        $(event).find('input').val('.')
        $(event).css('display', 'none');
        $(audience).find('input').val('.')
        $(audience).css('display', 'none');
        $(ideas).find('input').val('.')
        $(ideas).css('display', 'none');
        $(quantity).find('input').val('.')
        $(quantity).css('display', 'none');
        $(budget).find('input').val('.')
        $(budget).css('display', 'none');
        $(colour).find('input').val('.')
        $(colour).css('display', 'none');
        $(branding).find('input').val('.')
        $(branding).css('display', 'none');
        $(address).find('input').val('.')
        $(address).css('display', 'none');
    };
//////////////////////////////////// CUSTOM REQUEST PAGE MODIFICATIONS ///////////////////////////

//////////////////////////////////// HIDE REGISTER LINK ///////////////////////////
    var registerLink = $('a[href="https://your.googlemerchandisestore.com/UserRegistration"]');
    $(registerLink).css('display', 'none');
//////////////////////////////////// HIDE REGISTER LINK ///////////////////////////

//////////////////////////////////// HOST REF FUNCTIONS ///////////////////////////
    var hostRef = $('.logo').parent().attr('href');
         hostRef = hostRef.substring(0, hostRef.length - 6);

    $('a.hostRef').each(function () {
        var linkRef = $(this).attr('href');
        var newRef = hostRef + linkRef;
        $(this).attr('href', newRef);
    })

    $('img.hostRef').each(function () {
        var linkRef = $(this).attr('src');
        var newRef = hostRef + linkRef;
        $(this).attr('src', newRef);
    })

    ////FOOTER LINKS
    $("#ucFooter1_ctl01_ulFooterLinks").children().clone().appendTo("#FOOTER_LINKS");
    $('#FOOTER_LINKS').find('a').removeClass('footerLink');

//////////////////////////////////// HOST REF FUNCTIONS ///////////////////////////

    setTimeout(resetMain, 5);
    lastScrollTop = 0

    //// HIDE CURRENCY SELECTOR IF ONLY ONE CURRENCY ////
    var currCount = $('ul.dropdown-menu.sitelinks.scroll > li').length;
    if (currCount < 2) {
        $('ul.nav.navbar-nav.navbar-right.ellipsis.nomargin > li:nth-last-of-type(1)').css('display', 'none');
    }
    //// HIDE CURRENCY SELECTOR IF ONLY ONE CURRENCY ////

    //// PRODUCT DETAIL PAGE THUMBNAIL MOVE ////
    var thumbs1 = $('#ContentPlaceHolder1_rptVariantTypeList_productVariantTitle_0');
    var thumbs2 = $('#ContentPlaceHolder1_rptVariantTypeList_productVariantTitle_0').next();
    var swatches = $('.product_text_panel_top > .row');
    var thumbTo = $('#zoom > p');

    $(thumbs2).detach().insertAfter(thumbTo);
    $(thumbs1).detach().insertAfter(thumbTo);
    $(swatches).detach().appendTo('#pdDetailsPID');
    //// PRODUCT DETAIL PAGE THUMBNAIL MOVE ////

    //// FIXED HEADER PAGE BUMP ////
    var hHeight = $('#ALL_HEADER').outerHeight(true);
    $('.maincontainer').css('margin-top', hHeight);
    $(window).on('resize', function () {
        var hHeight = $('#ALL_HEADER').outerHeight(true);
        $('.maincontainer').css('margin-top', hHeight);
    });
    //// FIXED HEADER PAGE BUMP ////

    //// COLLAPSE SIZE CHART ON PRODUCT PAGE AND ADD A BUTTON TO SHOW IT (PHIL) ////
    if ($('#ContentPlaceHolder1_dvProductSize > div.product_size_table').length > 0) {
        $("#ContentPlaceHolder1_dvProductSize").addClass("collapse");
        $("#ContentPlaceHolder1_dvSocialMedia").after("<div style='display:block; float:left; margin-bottom:10px'><a class='reviewContainerLink' role='button' data-toggle='collapse' href='#ContentPlaceHolder1_dvProductSize' aria-expanded='false' aria-controls='ContentPlaceHolder1_dvProductSize'>View size guide</a></div>");
    }
    //// COLLAPSE SIZE CHART ON PRODUCT PAGE AND ADD A BUTTON TO SHOW IT (PHIL) ////

    //// HB SIDEBAR ////
    $('span#spnUserNamenmsg').after('<div class="side"><div class="overlay"></div><nav class="sidebar"><div id="side-close" class="close customClose">x</div><a href="https://www.brand-estore.com/googlestore/index"><img src = "https://www.brand-estore.com/googlestore/Images/StaticImage/39.png" /></a></nav></div>'); ////////ADD SIDEBAR
    $('#ucHeader1_ctl01_rptHeaderLinks_ulChildSiteLinks_3').clone().appendTo('nav.sidebar').removeClass('dropdown-menu').attr('id', 'SideMyAccount'); ////////CLONE MYACCOUNT INTO SIDEBAR
    $('<li><a href="https://www.brand-estore.com/googlestore/info/faqs">Help</a></li>').appendTo($('ul#SideMyAccount')); ////////ADD HELP LINK TO SIDEBAR

    $('ul#SideMyAccount li').each(function () {
        $(this).removeClass('dropdown sitelink');
    });// REMOVE UNNEEDED CLASSES

    $('ul#SideMyAccount li a').each(function () {
        $(this).removeClass('dropdown-toggle headerLink');
    });// REMOVE UNNEEDED CLASSES

    $('ul#SideMyAccount li ul').remove(); // REMOVE EXTRANEOUS UL

    $('<div id="Hamburger"><a href="javascript:void(0)"><i class="fa fa-bars" aria-hidden="true"></i></a></div>').prependTo($('img.logo').closest('div.column')); // ADD HAMBURGER ELEMENT
    $('#Hamburger').parent().addClass('HB_Logo');
    //// HB SIDEBAR ////

    //// HB STICKY STACKY HEADER SETUP ////
    if ($('section#CHECKOUT').length) {
        $('header#header').addClass('hb_checkout');
    } else {
        $('header#header').addClass('hb_fixed');
    };

    
    $('div.searchlink').after('<div id = "ScrollBasket"></div>')
    $('#COOKIES').appendTo('#ucFooter1_divFooter');
    resetMain;

    var cHeight = $('#COOKIES').height();
    $('#STANDARD_FOOTER').css('margin-bottom', cHeight)
    //// HB STICKY STACKY HEADER SETUP ////

    //// SIDEBAR CLICK HANDLERS ////
    $(document.body).on('click', '#Hamburger', function (event) {
        $('nav.sidebar').addClass('hb_on');
        $('#side-close').addClass('hb_on');
        $('div.overlay').addClass('hb_on');
    });

    $(document.body).on('click', '#side-close', function (event) {
        $('nav.sidebar').removeClass('hb_on');
        $('#side-close').removeClass('hb_on');
        $('div.overlay').removeClass('hb_on');
    });

    $(document.body).on('click', '.overlay', function (event) {
        $('nav.sidebar').removeClass('hb_on');
        $('#side-close').removeClass('hb_on');
        $('div.overlay').removeClass('hb_on');
    });
    //// SIDEBAR CLICK HANDLERS ////

});


//// AJAX COMPLETE ////
$(document).ajaxComplete(function () {

    //// CATEGORY BANNER MOVE ////
    var category_image_added = false;
    if ($('#rptProducts_liProducts_-1').length == 0) {
        setTimeout(function () {
            if (!category_image_added && $('#hrefBannerLink').length) {
                var LiToAdd = '<li id="rptProducts_liProducts_-1" class="col-lg-8 col-md-8 col-sm-12 col-xs-12"></li>';
                $(LiToAdd).prependTo('#productdata');
                $('#hrefBannerLink > img').detach().appendTo('#rptProducts_liProducts_-1');
                category_image_added = true;
            }
        }, 10);
    };
    //// CATEGORY BANNER MOVE ////

    //// PRODUCT LIST PAGE USE MEDIUM IMAGES ////
    if ($('#PRODUCT_LIST_PAGE').length > 0) {
        $('img.prod_list_image_inner').each(function () {
            var source = $(this).attr('src');
            var newSource = source.replace('Thumbnail', 'Medium');
            $(this).attr('src', newSource)
         });
    }
    //// PRODUCT LIST PAGE USE MEDIUM IMAGES ////

});


//// WINDOW SCROLL ////
$(window).scroll(function () {
    var st = $(window).scrollTop();
    if (st > lastScrollTop) {
        $('header#header').addClass('hb_on');
        if (!$('div#ScrollBasket> li.BasketLink').length) {
            $('li.BasketLink').clone().appendTo($('div#ScrollBasket'));
        };
        $('div.searchlink').addClass('hb_on');
        $('div#ScrollBasket').addClass('hb_on');
        $('div#Hamburger').parent().addClass('hb_on');
        $('#ucHeader1_ctl01_ulHeaderLinks').addClass('hb_on');
    } else {
        $('header#header').removeClass('hb_on');
        $('div.searchlink').removeClass('hb_on');
        $('div#ScrollBasket').removeClass('hb_on');
        setTimeout(function () { $('div#ScrollBasket> li.BasketLink').detach() }, 200);
        $('div#Hamburger').parent().removeClass('hb_on');
        $('#ucHeader1_ctl01_ulHeaderLinks').removeClass('hb_on');
    }
    lastScrollTop = st;

    if ($('#COOKIES').hasClass('in')) {
        var cHeight = $('#COOKIES').outerHeight(true);
        $('#STANDARD_FOOTER').css('margin-bottom', cHeight);
    } else {
        $('#STANDARD_FOOTER').css('margin-bottom', 0);
    };

});

//// GLOBAL FUNCTION ////
function resetMain() {
    if ($(this).width() > 768) {
        var hHeight = $('header#header').outerHeight(true);
        $('div.maincontainer').css('margin-top', hHeight);
    }
}