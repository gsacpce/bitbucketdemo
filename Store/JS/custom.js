function fnShowLoader() {
    $('#dvLoader').show();
}
function fnHideLoader() {
    $('#dvLoader').hide();
}

function EncodeName(Name) {
    //debugger;
    Name = replaceAll(Name, ".", "-dot-");
    Name = replaceAll(Name, "'", "-quote-");
    Name = replaceAll(Name, "&", "-and-");
    Name = replaceAll(Name, "+", "-plus-");
    Name = replaceAll(Name, "/", "-slash-");
    Name = replaceAll(Name, "\"", "-dbq-");
    Name = replaceAll(Name, "*", "");
    Name = replaceAll(Name, "<", "-lt-");
    Name = replaceAll(Name, ">", "-gt-");
    Name = replaceAll(Name, ":", "-col-");
    Name = replaceAll(Name, "#", "-hash-");
    //Name = Name.trim().replace("%", "-");
    return Name;
};

function EncodeSearchName(Name) {
    //debugger; 
    Name = replaceAll(Name, "*", "");
    Name = replaceAll(Name, "&", "-and-");
    Name = replaceAll(Name, "/", "-slash-");
    Name = replaceAll(Name, "\"", "-dbq-");
    Name = replaceAll(Name, "<", "-lt-");
    Name = replaceAll(Name, ">", "-gt-");
    Name = replaceAll(Name, "#", "-hash-");
    Name = replaceAll(Name, ":", "-");
    Name = replaceAll(Name, "%", "-");
    Name = replaceAll(Name, ".", "-");
    return Name;
};

function replaceAll(string, find, replace) {
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(string) {
    return string.replace(/([.*%+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function SetFocusOnButton(e, id) {
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    if (keyCode == 13) {
        $(id).focus();
    }
}

function IsValidAlphaNumeric(value, len) {

    //only alphanumeric
    var regex;
    if (len != 0) {
        regex = new RegExp('^[a-zA-Z0-9]\{1,' + len + '\}$', 'gi');
    }
    else {
        regex = new RegExp('^[a-zA-Z0-9]$', 'gi');
    }
    return regex.test(value.trim());
}

function IsEmpty(value) {
    //empty string
    var regex = /^$/;
    return regex.test(value.trim());
}

//function IsValidAddress(value, len) {
//    //only alphabets and numbers with ['] [.] [-] [blank space] [,] [\] [/]
//    //var regex = /^[A-Za-z0-9'\.\-\s\,]$/;
//    var regex;
//    if (len != 0) {
//        regex = new RegExp('^[A-Za-z0-9\'.\\-\\s,\\\/]\{1,' + len + '\}$', 'gi');
//    }
//    else {
//        regex = new RegExp('^[A-Za-z0-9\'.\\-\\s,\\\/]$', 'gi');
//    }
//    return regex.test(value.trim());
//}

function IsAngularBrackets(value, len) {
    //Disallow angular brackets [<] [>]
    //var regex = /^[^<>]{1,200}$/;
    var regex;
    if (len != 0) {
        regex = new RegExp('^[^<>]\{1,' + len + '\}$', 'gi');
    }
    else {
        regex = new RegExp('^[^<>]$', 'gi');
    }
    return regex.test(value.trim());
}

function IsValidPostCode(value, len) {
    //Disallow angular brackets [<] [>]
    var regex;
    if (len != 0) {
        regex = new RegExp('^[A-Za-z0-9 --]\{1,' + len + '\}$', 'gi');
    }
    else {
        regex = new RegExp('^[A-Za-z0-9 --]$', 'gi');
    }
    return regex.test(value.trim());
}

function IsValidPhone(value, len) {
    //allow alphanumeric with blank
    var regex;
    if (len != 0) {
        regex = new RegExp('^[0-9 +)(-]\{1,' + len + '\}$', 'gi');
    }
    else {
        regex = new RegExp('^[0-9 +)(-]$', 'gi');
    }
    return regex.test(value.trim());
}

function IsValidEmail(value) {
    //allow valid email address
    var regex = /^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/;
    return regex.test(value.trim());
}

function isNumeric(e) {

    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;

    if (keyCode == 16 || keyCode == 18)
        isShift = true;

    return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || (keyCode >= 96 && keyCode <= 105)) && isShift == false);
}
var isShift = false;
function keyUP(e) {
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    if (keyCode == 16 || keyCode == 18)
        isShift = false;
}

/*code for cookies*/
$(document).ready(function () {
   if ($(window).width() < 768) {
        var hHeight = $('#ALL_HEADER').outerHeight(true);
        $('.maincontainer').css('margin-top', hHeight);
    };
});

$(window).on('resize', function () {
    if ($(this).width() < 768) {
        var hHeight = $('#ALL_HEADER').outerHeight(true);
        $('.maincontainer').css('margin-top', hHeight);
    } else {
        $('.maincontainer').css('margin-top', 0);
    };
});



/*Cookies*/
function ShowCookieDiv() {
    $('#COOKIES').addClass("in");
    $('.maincontainer').addClass('paddingTop')

}
function hideCookieDiv() {
    $('#COOKIES').addClass("out");
    $('.maincontainer').removeClass('paddingTop')

}
function cookie_close() { $('#COOKIES').collapse('hide'); $('.maincontainer').removeClass('paddingTop') }
function createCookie(name, value, days) {   
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
    cookie_close();
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
function eraseCookie(name) {
    createCookie(name, "", -1);
}

/*WOW js initialization*/

$(document).ready(function () {
    new WOW().init();
    console.log("wow initialization done");
    imageExists();
});

function imageExists() {
    debugger;
    var http = new XMLHttpRequest();
    var image_url = "../Favicon.ico";
    http.open('HEAD', image_url, false);
    http.send();
    if (http.status != "404") {
        a = document.getElementById('lnkIcon');
        a.setAttribute("href", "../Favicon.ico");
    }
    else {
        a = document.getElementById('lnkIcon');
        a.setAttribute("href", "../Favicon.png");
    }

}

/* Added By Hardik on 8/FEB/2017 for Communigator API - START */

/* For Infinitihomepage banner button linked to static page with FName, LName, Email */
$(document).on('click', '#btnFooterSubmit1', function () {

    debugger;
    //var e_id = document.getElementById('txtEmailsubscribe').value;
    var e_id_1 = document.getElementById('inputEmail').value;
    var f_name = document.getElementById('inputFirstName').value;
    var l_name = document.getElementById('inputLastName').value;

    var obj = {};
    obj.email_id = e_id_1;
    obj.fname = f_name;
    obj.lname = l_name;

    var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (f_name == '' || l_name == '' || e_id_1 == '') {
        $('#myWarningModal').find('#WarningMessage').html(strEnterDetails);
        $('#myWarningModal').modal('show');
        document.getElementById('inputFirstName').focus();
        return false;
    } else {

        if (!e_id_1.match(reg) || e_id_1 == '') {
            $('#myWarningModal').find('#WarningMessage').html(strInvalidEmail);
            $('#myWarningModal').modal('show');
            document.getElementById('inputEmail').value = '';
            document.getElementById('inputEmail').focus();
            return false;
        }
        else {
            $.ajax({
                type: "POST",
                url: host + 'Login/Login.aspx/IsMarketingFooterInsert',
                //data: "{'email_id': '" + e_id + "'}",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                async: false,

                success: function (response) {
                    var Res = parseInt(response.d);

                    if (Res == 0) {
                        document.getElementById('inputFirstName').value = '';
                        document.getElementById('inputLastName').value = '';
                        document.getElementById('inputEmail').value = '';
                        $('#myWarningModal').find('#WarningMessage').html(strAlreadySubscribed);
                        $('#myWarningModal').modal('show');
                    }
                    else if (Res > 0) {
                        document.getElementById('inputFirstName').value = '';
                        document.getElementById('inputLastName').value = '';
                        document.getElementById('inputEmail').value = '';
                        $('#mySuccessModal').find('#SuccessMessage').html(strNewSubscribed);
                        $('#mySuccessModal').modal('show');
                    }
                    else {
                        document.getElementById('inputFirstName').value = '';
                        document.getElementById('inputLastName').value = '';
                        document.getElementById('inputEmail').value = '';
                        $('#myFailureModal').find('#WarningMessage').html(response.d);
                        $('#myFailureModal').modal('show');
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
});

/* For Google Footer Functionality Only Email */

$(document).on('click', '#btnFooterSubmit', function () {

    debugger;
    var e_id = document.getElementById('txtEmailsubscribe').value;
    var f_name = '.';
    var l_name = '.';

    var obj = {};
    obj.email_id = e_id;
    obj.fname = f_name;
    obj.lname = l_name;

    var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (e_id == '') {
        $('#myWarningModal').find('#WarningMessage').html(strEnterDetails);
        $('#myWarningModal').modal('show');
        document.getElementById('txtEmailsubscribe').focus();
        return false;
    } else {

        if (!e_id.match(reg) || e_id == '') {
            $('#myWarningModal').find('#WarningMessage').html(strInvalidEmail);
            $('#myWarningModal').modal('show');
            document.getElementById('txtEmailsubscribe').value = '';
            document.getElementById('txtEmailsubscribe').focus();
            return false;
        }
        else {
            $.ajax({
                type: "POST",
                url: host + 'Login/Login.aspx/IsMarketingFooterInsert',
                //data: "{'email_id': '" + e_id + "'}",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                async: false,

                success: function (response) {
                    var Res = parseInt(response.d);

                    if (Res == 0) {                        
                        document.getElementById('txtEmailsubscribe').value = '';
                        $('#myWarningModal').find('#WarningMessage').html(strAlreadySubscribed);
                        $('#myWarningModal').modal('show');
                    }
                    else if (Res > 0) {                        
                        document.getElementById('txtEmailsubscribe').value = '';
                        $('#mySuccessModal').find('#SuccessMessage').html(strNewSubscribed);
                        $('#mySuccessModal').modal('show');
                    }
                    else {                        
                        document.getElementById('txtEmailsubscribe').value = '';
                        $('#myFailureModal').find('#WarningMessage').html(response.d);
                        $('#myFailureModal').modal('show');
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
});

function IsValidEmailId(value) {
    debugger;
    var regex = /^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/;
    if (regex.test(value.trim()))
        return true;
    else
        return false;
}


function ReplaceEmailIdspecialCharactersbyEncoding(emailid) {
    //debugger;

    emailid = replaceAll(emailid, "&", "&amp;");
    emailid = replaceAll(emailid, "'", "&#39;");
    emailid = replaceAll(emailid, "ä", "&#228;");
    emailid = replaceAll(emailid, "ü", "&#252;");
    emailid = replaceAll(emailid, "ö", "&#246;");
    emailid = replaceAll(emailid, "ß", "&#223;");
    return emailid;
}

/* Added By Hardik on 8/FEB/2017 for Communigator API - END */
/*Added By Ravi For Vriables*/
