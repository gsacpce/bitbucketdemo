﻿$(document).ready(function () {
    if ($('.standard_basket_row').length != 0) {
        $('#dvEmptyBasket').hide();
    }
    else {
        $('#dvEmptyBasket').show();
    }

    $('.aPlus').click(function (e) {
        debugger;
        e.preventDefault();
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());
        if (sp == "") {
            parseFloat($(this).parent(0).parent(0).find('input').val("0"));
        }
        if (isNaN(sp)) {
            parseFloat($(this).parent(0).parent(0).find('input').val("0"));
        }
        if (parseInt(sp) <= 0) {
            parseFloat($(this).parent(0).parent(0).find('input').val("0"));
        }
        if (sp < 9999) {
            var obj = {};
            var totalQty = parseFloat($(this).parent(0).parent(0).find('input').val());
            obj.totalQty = sp + 1;
            try {
                $.ajax({
                    type: "POST",
                    url: host + 'Products/ProductDetails.aspx/getPriceBreakforProducts',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    async: false,

                    success: function (response) {
                        var Res = response.d;
                        $(".FpriceBreak").html(strCurrencySymbol + Res);
                        //return Res;
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            } catch (e) { }

            $(this).parent(0).parent(0).find('input').val(sp + 1);
            console.log($(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html());
            var aPdtUnitPrice = $(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html();
            var realprice = $(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html();
            if (realprice.indexOf(",") > -1) {
                realprice = realprice.replace(',', '.');
                console.log(realprice);
            }
            var Price = ((sp + 1) * realprice);
            console.log(Price);
            console.log(bIsPointsEnabled);
            if (bIsPointsEnabled == 'abc') {
            }
            else {
                $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(Price.toFixed(2));

                /*aPdtUnitPrice is for comparsion to check Globalization (Comman / Dot)*/
                if (aPdtUnitPrice.indexOf(",") > -1) {
                    /*Below portion is to work accordingly to Globalization (Comman / Dot) */
                    var CommPrice = Price.toFixed(2).toString();
                    CommPrice = CommPrice.replace('.', ',');
                    $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(CommPrice);
                    $(this).parent(0).parent(0).parent(0).parent(0).find('#hidBasketTot').val(CommPrice);

                }
                else {
                    $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(Price.toFixed(2));
                    $(this).parent(0).parent(0).parent(0).parent(0).find('#hidBasketTot').val(Price.toFixed(2));

                }
            }
        }
        else
            return false;
        updateTotalPrice();

        if ($('#ddlDCountry').val() > 0) {
            if ($('#rbStandard').is(':checked')) {
                calculatefreightvalue(1);
            }
            if ($('#rbExpress').is(':checked')) {
                calculatefreightvalue(2);
            }
        }
    });

    $('.aMinus').click(function (e) {
        debugger;
        e.preventDefault();
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());
        var minPB = 0;        
        var spn = document.getElementById('WarningMessage');        
        minPB = $('#hdnFirstPriceBrk').val();
        MinPriceBreak = MinPriceBreak.replace("{minP}", minPB);
        
        if (sp > 1) {
            if (sp > minPB) {
                $(this).parent(0).parent(0).find('input').val(sp - 1);
                console.log($(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html());
                var aComma1 = false;
                var aPdtUnitPrice = $(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html();
                var realprice = $(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html();
                if (realprice.indexOf(",") > -1) {
                    realprice = realprice.replace(',', '.');
                    aComma1 = true;
                    console.log(realprice);
                }
                var Price = ((sp - 1) * realprice);
                console.log(Price);
                console.log(bIsPointsEnabled);
                if (bIsPointsEnabled == 'True')
                    /* changes by vivek for Points*/ {
                    if (aComma1 == true) {
                        var aunitPP = Price.toFixed(2);
                        aunitPP = aunitPP.replace('.', ',');
                    }
                    else if (aComma1 == false) {
                        var aunitPP = Price.toFixed(2);

                    }
                    $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(aunitPP);
                }
                else {
                    if (aComma1 == false) {
                        var aunitPP = Price.toFixed(2);

                    }
                    else if (aComma1 == true) {
                        var aunitPP = Price.toFixed(2);
                        aunitPP = aunitPP.replace(',', '.');
                    }
                    $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(aunitPP);
                    if (aPdtUnitPrice.indexOf(",") > -1) {
                        /*Below portion is to work accordingly to Globalization (Comman / Dot) */
                        var CommPrice = Price.toFixed(2).toString();
                        CommPrice = CommPrice.replace('.', ',');
                        $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(CommPrice);
                    }
                    else {
                        $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(Price.toFixed(2));
                    }
                }
            }
            else {
                //Added By Hardik on "11/May/2017"
                error = MinPriceBreak;
                spn.innerHTML = error;
                $('#myWarningModal').modal('show');
                return false;
            }
        }
        else
            return false;
        updateTotalPrice();
        if ($('#ddlDCountry').val() > 0) {
            if ($('#rbStandard').is(':checked')) {
                calculatefreightvalue(1);
            }
            if ($('#rbExpress').is(':checked')) {
                calculatefreightvalue(2);
            }
        }
    });

    $('.quantity').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    function updateTotalPrice() {
        var totalPrice = 0;
        var PricePoint = 0;
        var aComma = false;
        for (var i = 0; i < $('#dvTotalPrice span').length; i++) {
            console.log($('#dvTotalPrice span')[i].textContent);
            var price = $('#dvTotalPrice span')[i].textContent;
            console.log("before" + price);
            if (price.indexOf(",") > -1) {
                aComma = true;
                price = price.replace(',', '.');
                console.log(price);
            }
            console.log(totalPrice);
            totalPrice = (parseFloat(totalPrice) + parseFloat(price));
            console.log(totalPrice);
        }
        if (bIsPointsEnabled == 'True') {
            /* changes by vivek for Indeed Points*/

            if (aComma == true) {
                var a = totalPrice.toFixed(2);
                a = a.toString();
                a = a.replace('.', ',');
                $('#dvTotal span').html(a);
                $('#DivPoint').html(a);
            }
            else {
                $('#dvTotal span').html(totalPrice.toFixed(2));
            }

            var i = 0;
            if (i < $('#dvTotalPrice span').length) {
                PricePoint = $('#Divhidden')[i].textContent;
                newprice = PricePoint.replace(',', '.');
            }

            var pointvalue = (totalPrice * newprice);
            totalPrice = (parseFloat(pointvalue));
            var final = Math.ceil(totalPrice);
            console.log(totalPrice);
            $('#DivPoint').html(final + ' ' + strGenericPointsText);

        }
        else {
            var advUnitPrice = $(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html();
            if (aComma == true) {
                var a = totalPrice.toFixed(2);
                a = a.toString();
                a = a.replace('.', ',');
                $('#dvTotal span').html(a);

            }
            else {
                $('#dvTotal span').html(totalPrice.toFixed(2));
            }
        }

        $('#hidBasketTot').val(totalPrice);
    }

    $(document).on('click', '#aRemoveProducts', function () {
        $('#hidShoppingCartProductId').val($(this).attr('rel'));
        $('#myConfirmModal').find('#ConfirmationMessage').html(strRemoveItemsFromCart);
        $('#myConfirmModal').attr('rel', 'updatebasket');
        $('#myConfirmModal').modal('show');

    });


    //Changes Done By Snehal 23 09 2016 to Check Max Min Quantity value
    $(document).on('click', '#aCheckOut, #aContinueShopping', function () {
        var errorMessage = '';
        var Quantity = 0;
        var StockStatus = 0;
        var ProductName = '';
        var minQty = 0;
        var maxQty = 0;
        var MaxVal = 0;
        console.log("Before loop = " + Quantity);
        for (var i = 0; i < $('div[id^=dvBasketRowContainer]').find('#txtQuantity').length; i++) {
            console.log("inside loop = " + Quantity);
            Quantity = parseInt($('div[id^=dvBasketRowContainer]').find('#txtQuantity')[i].value);
            ProductName = $('div[id^=dvBasketRowContainer]').find('#dvProductName')[i].innerHTML.replace('<br>', '-');
            StockStatus = parseFloat($('div[id^=dvBasketRowContainer]').find('#hdnStockStatus')[i].value);
            minQty = parseInt($('div[id^=dvBasketRowContainer]').find('#hdnMinimumOrderQuantity')[i].value);
            maxQty = parseInt($('div[id^=dvBasketRowContainer]').find('#hdnMaximumOrderQuantity')[i].value);

            if (Quantity != "") {

                if (IsMaxQtyEnabled == 'True') {
                    MaxVal = parseInt(intStoreFeatureDefaultValue);
                }
                else {
                    MaxVal = maxQty;
                }
            }

            if (!isNaN(Quantity)) {
                if (Quantity <= 0) {
                    errorMessage += ProductName + ': ' + strQuantityNotZero;
                }
            }
            else {
                errorMessage += ProductName + ': ' + strQuantityNotZero;
            }
            if (minQty > 0 && MaxVal > 0) {
                if (Quantity < minQty || Quantity > MaxVal) {
                    errorMessage += ProductName + ': ' + strMinMaxQuantity.replace('{min}', minQty).replace('{max}', MaxVal) + '.<br>';
                }
            }
            //Check for the inventory of the products           
            if (bBackOrderAllowed == 'False' && StockStatus > 0) {
                if (Quantity > StockStatus) {
                    errorMessage += ProductName + ': ' + strQuantityNotMoreThanStock + StockStatus;
                }
            }
        }
        if (errorMessage != '') {
            $('#myWarningModal').find('#WarningMessage').html(errorMessage);
            $('#myWarningModal').modal('show');
            return false;
        }
        $('#dvLoader').show();
        return true;
    });

    $(document).on('click', '#myModal #aLogin', function () {
        var message = '';
        if (IsEmpty($('#myModal #txtEmail').val())) {
            message += MsgEmailBlank + '.<br>';
        }
        else {
            if (isEmailValid == 'True') //Changes Done By snehal 16-09-2016 For IsLogin Custom Field
            {
                if (!IsValidEmailId($('#myModal #txtEmail').val())) {
                    message += MsgInvalidEmail + '.<br>';
                }
            }
        }
        if (IsEmpty($('#myModal #txtPassword').val())) {
            message += MsgPasswordBlank + '.<br>';
        }

        if (message != '') {
            $('#myWarningModal').find('#WarningMessage').html(message);
            $('#myWarningModal').modal('show');
            return false;
        }
        else {
            $('#dvLoader').show();
            var EmailidwithoutSpecialchar = ReplaceEmailIdspecialCharactersbyEncoding($('#myModal #txtEmail').val());
            $.ajax({
                url: host + 'login/login.aspx/CheckLogin',
                data: "{'strEmail': '" + EmailidwithoutSpecialchar + "','strPassword':'" + $('#myModal #txtPassword').val() + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
            }).done(function (data) {
                $('#dvLoader').hide();
                if (data.d == "success") {
                    window.location.href = host + 'checkout';
                }
                else {
                    var Basket_InvalidEmailPassword = $('#hdnBasket_InvalidEmailPassword').val();
                    $('#myErrorModal').find('#ErrorMessage').html('' + Basket_InvalidEmailPassword + '');
                    $('#myErrorModal').modal('show');
                    $('#myErrorModal').find('#btnErrorOK').click(function () { window.location.href = host + 'SignIn'; });
                }
            });
        }
    });

    $(document).on('click', '#myModal #btnGuestCheckOut', function () {
        saveRegGuest();
        return false;
    });

    $(document).on('click', '#btnConfirmNo', function () {
        //alert('No');
        if ($('#myConfirmModal').attr('rel') == 'addbasket') {
            $('#dvLoader').show();
            // add previously added products into basket
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/UpdateBasketProducts',
                data: "{'action': 'n'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
            }).done(function (data) {
                $('#dvLoader').hide();
                if (data.d == true) {
                    window.location.href = host + 'checkout';
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html($('#hdnBasket_ErrorUpdatingBasket').val());
                    $('#myErrorModal').modal('show');
                }
            });
        }
    });

    $(document).on('click', '#btnConfirmYes', function () {
        if ($('#myConfirmModal').attr('rel') == 'addbasket') {
            $('#dvLoader').show();
            // add previously added products into basket
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/UpdateBasketProducts',
                data: "{'action': 'y'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
            }).done(function (data) {
                $('#dvLoader').hide();
                if (data.d == true) {

                    $('#mySuccessModal').find('#SuccessMessage').html($('#hdnBasket_PreviousProductAdded').val());
                    $('#mySuccessModal').modal('show');
                    window.location.href = host + 'Checkout/ShoppingCart';
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html($('#hdnBasket_ErrorUpdatingBasket').val());
                    $('#myErrorModal').modal('show');
                }
            });
        }
        else {
            $('#dvLoader').show();
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/RemoveProductsFromBasket',
                data: "{'ShoppingCartProductId': " + $('#hidShoppingCartProductId').val() + ",'intCurrencyId':" + intCurrencyId + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
            }).done(function (data) {
                $('#dvLoader').hide();
                if (data.d == true) {
                    $('#dvBasketRowContainer' + $('#hidShoppingCartProductId').val()).remove();
                    $('#mySuccessModal').find('#SuccessMessage').html(strProductRemovedFromCart);
                    $('#mySuccessModal').modal('show');
                    if ($('.standard_basket_row').length != 2) {
                        $('#dvEmptyBasket').hide();
                    }
                    else {
                        $('#dvEmptyBasket').show();
                        $('.standard_basket_titles').hide();
                        $('.standard_basket_row').hide();
                    }
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html(strBasketErrorWhileRemovingProduct);
                    $('#myErrorModal').modal('show');
                }
            });
            updateTotalPrice();
        }
    });

    $(document).on('focusout', '#txtQuantity', function () {

        if ($(this).val() == "") {
            $(this).val("0");
        }
        if (isNaN($(this).val())) {
            $(this).val(0);
        }
        if (parseInt($(this).val()) <= 0) {
            $(this).val(0);
        }

        var input = $(this).val();
        var aPdtUnitPrice = $(this).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html();
        var realprice = $(this).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html();
        if (realprice.indexOf(",") > -1) {
            realprice = realprice.replace(',', '.');
            console.log(realprice);
        }
        var Price = input * realprice;
        if (aPdtUnitPrice.indexOf(",") > -1) {
            /*Below portion is to work accordingly to Globalization (Comman / Dot) */
            var CommPrice = Price.toFixed(2).toString();
            CommPrice = CommPrice.replace('.', ',');
            $(this).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(CommPrice);
        }
        else {
            $(this).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(Price.toFixed(2));
        }
        updateTotalPrice();
    });
});
function isNumeric(keyCode) {
    if (keyCode == 16 || keyCode == 18)
        isShift = true;

    return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 190 || keyCode == 9 || keyCode == 14 || keyCode == 15 || keyCode == 37 || keyCode == 39 || (keyCode >= 96 && keyCode <= 105)) && isShift == false);
}
var isShift = false;
function keyUP(keyCode) {
    if (keyCode == 16 || keyCode == 18 || keyCode == 9)
        isShift = false;
}


/* -------------------------------- Custom Punchout -------------------------------- */

function calculatefreightvalue(id) {
    //debugger;
    var shippingPrice = 0;
    var Baskettotal = 0;
    var Total = 0;
    var DelChargeId = 0;

    if (id == "1") {
        DelChargeId = 1;
        Baskettotal = parseFloat($('#hidBasketTot').val());
        shippingPrice = parseFloat($('#hidStandard').val());
        $('#hidSpnShippingPrice').val(shippingPrice);
        $('#hidDeliveryCharge').val(DelChargeId);
        Total = Baskettotal + shippingPrice;
        $('#dvTotal span').html(Total);
    }
    else if (id == "2") {
        DelChargeId = 2;
        Baskettotal = parseFloat($('#hidBasketTot').val());
        shippingPrice = parseFloat($('#hidExpress').val());
        $('#hidSpnShippingPrice').val(shippingPrice);
        $('#hidDeliveryCharge').val(DelChargeId);
        Total = Baskettotal + shippingPrice;
        $('#dvTotal span').html(Total);
    }

    $('#spnShippingPrice').html(shippingPrice);
}

function calculatefreight(id) {
    var shippingPrice = 0;
    var Baskettotal = 0;
    var Total = 0;

    if (id.value == "1") {
        Baskettotal = parseFloat($('#hidBasketTot').val());
        shippingPrice = parseFloat($('#hidStandard').val());
        $('#hidSpnShippingPrice').val(shippingPrice);
        Total = Baskettotal + shippingPrice;
        $('#dvTotal span').html(Total);
    }
    else if (id.value == "2") {
        Baskettotal = parseFloat($('#hidBasketTot').val());
        shippingPrice = parseFloat($('#hidExpress').val());
        $('#hidSpnShippingPrice').val(shippingPrice);
        Total = Baskettotal + shippingPrice;
        $('#dvTotal span').html(Total);
    }

    $('#spnShippingPrice').html(shippingPrice);
}

$(document).ready(function () {
    $('#dvRemoveCode').hide();
    $(document).on('change', '#ddlDAddressTitle', function () {
        $('#dvLoader').show();
        if (this.value != "0")
            $('#txtAddressTitle').show();
        else
            $('#txtAddressTitle').hide();
        $.ajax({
            url: host + 'shoppingcart/payment.aspx/changeaddress',
            data: "{'DeliveryAddressId': " + this.value + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            cache: false,
            async: true
        }).done(function (data) {
            if (data.d != "[]") {
                var json = $.parseJSON(data.d);
                $('#txtDContact_Name').val(json[0].PreDefinedColumn1);
                $('#txtDCompany_Name').val(json[0].PreDefinedColumn2); // Added by SHRIGANESH 27 July 2016 to display Company Name in Delivery Address section
                $('#txtDAddress1').val(json[0].PreDefinedColumn3);
                $('#txtDAddress2').val(json[0].PreDefinedColumn4);
                $('#txtDTown').val(json[0].PreDefinedColumn5);
                $('#txtDState__County').val(json[0].PreDefinedColumn6);
                $('#txtDPostal_Code').val(json[0].PreDefinedColumn7);
                $('#ddlDCountry').val(json[0].PreDefinedColumn8);
                $('#txtDPhone').val(json[0].PreDefinedColumn9);
                $('.myCheckbox').prop('checked', true);
                $('#chkIsDefault').prop('checked', (json[0].IsDefault));
                $('#txtAddressTitle').val($("#ddlDAddressTitle option:selected").text());
                $('#ddlDCountry').change();
            }
            else {
                $('#txtDContact_Name').val('');
                $('#txtDCompany_Name').val(''); // Added by SHRIGANESH 27 July 2016 to display Company Name in Delivery Address section
                $('#txtDAddress1').val('');
                $('#txtDAddress2').val('');
                $('#txtDTown').val('');
                $('#txtDState__County').val('');
                $('#txtDPostal_Code').val('');
                $('#ddlDCountry').val(0);
                $('#txtDPhone').val('');
                $('#txtAddressTitle').val('');
                $('#chkIsDefault').prop('checked', false);
            }
            $('#dvLoader').hide();
        });

    });

    $(document).on('change', '#ddlCountry', function () {
        if (document.getElementById('addressCheckbox').checked) {
            $('#ddlDCountry').val($('#ddlCountry').val());
            $('#ddlDCountry').change();
            $('#header2_space').prop("display", true);
            $('#divShipping').prop("display", true);
        }
    });

    $(document).on('change', '#ddlDCountry', function () {
        try {
            if (this.value != "0") {
                $('#header2_space').show();
                $('#divShipping').show();
                var selectedCountry = $('#ddlDCountry').val();
                $('#hidddlDCountry').val(selectedCountry);
                $('#dvLoader').show();
                $.ajax({
                    url: host + 'shoppingcart/BasketPunchout.aspx/CalculateFreightCharges',
                    data: "{'intCountryId': " + this.value + "}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    cache: false,
                    async: true
                }).done(function (data) {
                    if (data.d != "[]") {
                        var json = $.parseJSON(data.d);
                        var SZPrice = parseFloat(json[0].SZPrice).toFixed(2);
                        var EZPrice = parseFloat(json[0].EZPrice).toFixed(2);
                        var SZtax = parseFloat(json[0].StandardZone).toFixed(2);
                        var EZtax = parseFloat(json[0].ExpressZone).toFixed(2);

                        $('#hidStandardTax').val(SZtax);
                        $('#hidExpressTax').val(EZtax);
                        $('#hidStandard').val(SZPrice);
                        $('#hidExpress').val(EZPrice);
                        $('#rbStandard').attr('checked', false);
                        $('#rbExpress').attr('checked', false);

                        $('#divStandardCont').hide();
                        $('#divExpressCont').hide();

                        if (json[0].IsStandard) {
                            $('#divStandardCont').show();
                        }
                        if (json[0].IsExpress) {
                            $('#divExpressCont').show();
                        }
                        if (SZPrice == 0) {
                            $('#rbStandard').attr('disabled', 'disabled');
                            $('#rbStandard').attr('checked', false);
                            calculatefreightvalue(2);
                        }
                        else {
                            $('#rbStandard').removeAttr('disabled');
                            $('#rbStandard')[0].checked = true;
                            calculatefreightvalue(1);
                        }
                        $('#spnShippingPrice').html(SZPrice);
                        $('#spnstandard').html(SZPrice);

                        if (EZPrice == 0) {
                            $('#rbExpress').attr('disabled', 'disabled');
                            $('#rbExpress').attr('checked', false);
                            calculatefreightvalue(1);
                        }
                        else {
                            $('#rbExpress').removeAttr('disabled');
                            if (!$('#rbStandard').is(':checked')) {
                                $('#rbExpress')[0].checked = true;
                                calculatefreightvalue(2);
                                $('#spnShippingPrice').html(EZPrice);
                            }
                        }
                        $('#spnexpress').html(EZPrice);
                        $('#divMOVMessage').hide();
                        if ($('#rbExpress')[0].checked && json[0].EZDisallowOrder) {
                            $('#divMOVMessage').show();
                        }
                        else if (json[0].EZDisallowOrder) {
                            $('#divMOVMessage').show();
                        }

                        if (json[0].DisplayDutyMessage)
                            $('#divDutyMessage').show();
                        else
                            $('#divDutyMessage').hide();

                        $('#spnStandardText').html(json[0].StandardMethodName + " (" + json[0].SZTransitTime + " days)");
                        $('#spnExpressText').html(json[0].ExpressMethodName + " (" + json[0].EZTransitTime + " days)");
                    }
                    else {
                        console.log(data.d);
                    }
                    $('#dvLoader').hide();

                });
            }
        } catch (e) { }
        finally { }
    });



    $(document).on('click', '#aProceed', function () {
        return ValidatePaymentDetails();
    });
});

/* -------------------------------- Custom Punchout -------------------------------- */

function saveRegGuest() {
    //Validate Email address and password
    debugger;
    var message = '';
    if (IsEmpty($('#myModal #txtGuestEmail').val())) {
        message += MsgEmailBlank + '.<br>';
    }
    else {
        if (!IsValidEmailId($('#myModal #txtGuestEmail').val())) {
            message += MsgInvalidEmail + '.<br>';
        }
    }

    if (message != '') {
        $('#myWarningModal').find('#WarningMessage').html(message);
        $('#myWarningModal').modal('show');
        return false;
    }
    else {
        var EncodedEmailId = ReplaceEmailIdspecialCharactersbyEncoding($('#myModal #txtGuestEmail').val());
        $('#dvLoader').show();
        $.ajax({
            url: host + 'login/Register.aspx/GuestRegistration',
            data: "{'strEmail': '" + EncodedEmailId + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            cache: false,
            async: true
        }).done(function (data) {
            $('#dvLoader').hide();
            if (data.d == "success") {
                window.location.href = host + 'checkout';
                return true;

            }
            else if (data.d == "GuestUser") {
                var hdnBasket_AddPreviousProducts = $('#hdnBasket_AddPreviousProducts').val();
                $('#myConfirmModal').find('#ConfirmationMessage').html('' + hdnBasket_AddPreviousProducts + '');
                $('#myConfirmModal').attr('rel', 'addbasket');
                $('#myConfirmModal').modal('show');
                return false;
            }
            else {
                $('#myErrorModal').find('#ErrorMessage').html(data.d);
                $('#myErrorModal').modal('show');
                return false;
            }
        });
    }
}


function ReplaceEmailIdspecialCharactersbyEncoding(emailid) {

    emailid = replaceAll(emailid, "&", "&amp;");
    emailid = replaceAll(emailid, "'", "&#39;");
    emailid = replaceAll(emailid, "ä", "&#228;");
    emailid = replaceAll(emailid, "ü", "&#252;");
    emailid = replaceAll(emailid, "ö", "&#246;");
    emailid = replaceAll(emailid, "ß", "&#223;");
    return emailid;
}