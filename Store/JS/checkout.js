﻿function updateTotalPrice() {
    //debugger;
    var totalPrice = 0;

    for (var i = 0; i < $('#dvTotalPrice span').length; i++) {
        var Price = $('#dvTotalPrice span')[i].textContent;
        Price = Price.replace(',', '.');
        totalPrice = (parseFloat(totalPrice) + parseFloat(Price)).toFixed(2);
    }
    if (pIsPointsEnbled == 'True') {
        totalPrice = parseFloat(totalPrice.toString());
    }

    var spnShippingPrice = $('#spnShippingPrice').html();
    if (spnShippingPrice.indexOf(",") > -1) {
        spnShippingPrice = spnShippingPrice.replace(',', '.');
    }
    spnShippingPrice = parseFloat(spnShippingPrice);

    var spnadditionalcharge = $('#spnadditionalcharge').html(); 
    if (spnadditionalcharge == undefined) {
        spnadditionalcharge = 0;
    }
    else {
        spnadditionalcharge = spnadditionalcharge.replace(',', '.');
    }
    var totalnet = 0;
    totalnet = (parseFloat(totalPrice) + parseFloat(spnShippingPrice) + parseFloat(spnadditionalcharge)).toFixed(2);
    if (cultureCode != 'en-GB') {
        $('#spntotalnet').html(totalnet.replace('.', ','));
    }
    else {
        $('#spntotalnet').html(totalnet);
    }
    $('#dvTotal span').html(totalPrice);
    var Tax = $('#spnTaxPrice').html();
    Tax = Tax.replace(',', '.');
    totalPrice = (parseFloat(totalPrice) + parseFloat(Tax) + parseFloat(spnShippingPrice) + parseFloat(spnadditionalcharge)).toFixed(2);

    if (pIsPointsEnbled == 'True') {
        totalPrice = (parseFloat(totalPrice) /*+ parseFloat($('#spnTaxPrice').html()) + parseFloat($('#spnShippingPrice').html())).toFixed(2)*/);
        totalPrice = parseFloat(totalPrice.toString());
    }
    $('#spnTotalPrice').html(totalPrice);
    updateGC(totalPrice);

}
function updateGC(totalPrice) {
    //debugger;
    var aSesTotal = astr_SessionGCAmount;
    if (astr_SessionGCAmount != "") {
        if (parseFloat(astr_SessionGCAmount)) {
            var aGC = parseFloat(astr_SessionGCAmount);
            var aTP = parseFloat(totalPrice);
            var aDiff = 0;
            if (aGC < aTP) {
                aDiff = aTP - aGC;
                var b = aDiff.toFixed(2);
                $('#spnTotalPrice').html(aDiff.toFixed(2));
                $('#spnGiftCertificateAmount').html("-" + aGC);
            }
            else if (aGC > aTP) {
                aDiff = aGC - aTP;
                $('#spnTotalPrice').html("0.00");
                $('#spnGiftCertificateAmount').html("-" + aTP);
                $('#ddlPaymentTypes').val("1|I").attr("selected", "selected");
                //$("#ddlPaymentTypes").hide();
            }
            else if (aGC == aTP) {
                $('#spnTotalPrice').html("0.00");
                $('#spnGiftCertificateAmount').html("-" + aTP);
                $('#ddlPaymentTypes').val("1|I").attr("selected", "selected");
                //$("#ddlPaymentTypes").hide();
            }
        }

    }
}
function updateTax() {
    var totalPrice = $('#dvTotal span').html();
    if (totalPrice.indexOf(",") > -1) {
        totalPrice = totalPrice.replace(',', '.');
    }
    var atotalPrice = parseFloat(totalPrice);
    var aspnTaxPrice = $('#spnTaxPrice').html();
    if (aspnTaxPrice.indexOf(",") > -1) {
        aspnTaxPrice = aspnTaxPrice.replace(',', '.');
    }
    aspnTaxPrice = parseFloat(aspnTaxPrice);

    var spnShippingPrice = $('#spnShippingPrice').html();
    if (spnShippingPrice.indexOf(",") > -1) {
        spnShippingPrice = spnShippingPrice.replace(',', '.');
    }
    spnShippingPrice = parseFloat(spnShippingPrice);
   
    var spnadditionalcharge = $('#spnadditionalcharge').html(); 
    if (spnadditionalcharge == undefined) {
        spnadditionalcharge = 0;
    }
    else {
        spnadditionalcharge = spnadditionalcharge.replace(',', '.');
    }
    var totalnet = 0;
    totalnet = (parseFloat(totalPrice) + parseFloat(spnShippingPrice) + parseFloat(spnadditionalcharge)).toFixed(2);
    if (cultureCode != 'en-GB') {
        $('#spntotalnet').html(totalnet.replace('.', ','));
    }
    else {
        $('#spntotalnet').html(totalnet);
    }
   totalPrice = (parseFloat(totalPrice) + aspnTaxPrice + spnShippingPrice + parseFloat(spnadditionalcharge)).toFixed(2);
    $('#spnTotalPrice').html(totalPrice);
    updateGC(totalPrice);
   
}

function calculatefreight(id) {
    // debugger;
    var shippingPrice = 0;
    var tax = 0;
    console.log(id);

    if (id == "1") {
        shippingPrice = $('#spnService1').html();
        console.log(shippingPrice);
        tax = $('#hidService1Tax').val();
    }
    else if (id == "2") {
        shippingPrice = $('#spnService2').html();
        tax = $('#hidService2Tax').val();
    }
        // SHRIGANESH 11 Nov 2016 
    else if (id == "3") {
        shippingPrice = $('#spnService3').html();
        tax = $('#hidService3Tax').val();
    }

    if (id.value == "1") {
        shippingPrice = $('#spnService1').html();
        tax = $('#hidService1Tax').val();
    }
    else if (id.value == "2") {
        shippingPrice = $('#spnService2').html();
        tax = $('#hidService2Tax').val();
    }
        // SHRIGANESH 11 Nov 2016 
    else if (id.value == "3") {
        shippingPrice = $('#spnService3').html();
        tax = $('#hidService3Tax').val();
    }


    $('#spnShippingPrice').html(shippingPrice);
    if (pIsPointsEnbled == 'abc') {
        var tax = 0;
        $('#spnTaxPrice').html(tax);
    }
    else {
        $('#spnTaxPrice').html(tax);
    }

    if (document.getElementById('rbService1').checked) {

        var customtext = document.getElementById("hdncustom1").value;
        $('#divCustomMessage').html(customtext);
        $('#divCustomMessage').show();

    }
    else if (document.getElementById('rbService2').checked) {
        var customtext = document.getElementById("hdncustom2").value;
        $('#divCustomMessage').html(customtext);
        $('#divCustomMessage').show();
    }
    else if (document.getElementById('rbService3').checked) {
        var customtext = document.getElementById("hdncustom3").value;
        $('#divCustomMessage').html(customtext);
        $('#divCustomMessage').show();
    }
    //$('#spnTaxPrice').html(tax);
    if (pIsPointsEnbled == 'True') {
        //updateTax();
    }
    else { updateTax(); }
}

function addressCheckboxChanged() {
    console.log("start");
    if (document.getElementById('addressCheckbox').checked) {
        $('#collapseAddress').collapse('hide');
        $('#txtDContact_Name').val('');
        $('#txtDAddress1').val('');
        $('#txtDAddress2').val('');
        $('#txtDTown').val('');
        $('#txtDState__County').val('');
        $('#txtDPostal_Code').val('');
        $('#txtDPhone').val('');
        $('#ddlDAddressTitle').val(0);
        $('#txtAddressTitle').val('');
        $('#chkIsDefault').prop('checked', false);
        console.log("IsInvoiceAccountEnabled = " + IsInvoiceAccountEnabled);
        if (IsInvoiceAccountEnabled != 'False') {
            console.log("in");
            //vikram for legal entity
            if (IsTotalvalueZeroafterGC == "False") {
                if ($('#hdisLegalEntity').val() == "false" || $('#hdisLegalEntity').val() == "") {
                    $('#ddlDCountry').val($('#ddlCountry').val());
                }
                else {
                    $('#ddlDCountry').val($('#ddlInvoiceCountry').val());
                }
            }
            else {
                $('#ddlDCountry').val($('#ddlCountry').val());
            }
            //vikram for legal entity
        }
        else {
            $('#ddlDCountry').val($('#ddlCountry').val());
        }
        $('#ddlDCountry').change();
    }
    else {
        
        $('#dvLoader').show();
        /*$('#dvLoader').show();*/

        $.ajax({
            url: host + 'shoppingcart/payment.aspx/PopulateAddess',
            //data: "{'DeliveryAddressId': " + this.value + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            cache: false,
            async: true
            //beforeSend: function (request) {
            //    $.blockUI({ message: $('#dvLoader') });
            //}
        }).done(function (data) {
            //debugger;
            if (data.d != null) {
                var json = $.parseJSON(data.d);
                //$('#ddlDAddressTitle').val(json[0].AddressTitle);
                //alert(json[0].DeliveryAddressId);
                $('#ddlDAddressTitle').val(json[0].DeliveryAddressId);
                //$('#ddlDAddressTitle').find('option:selected').html(json[0].AddressTitle);
                $('#txtDContact_Name').val(json[0].PreDefinedColumn1);
                $('#txtDCompany_Name').val(json[0].PreDefinedColumn2);
                $('#txtDAddress1').val(json[0].PreDefinedColumn3);
                $('#txtDAddress2').val(json[0].PreDefinedColumn4);
                $('#txtDTown').val(json[0].PreDefinedColumn5);
                $('#txtDState__County').val(json[0].PreDefinedColumn6);
                $('#txtDPostal_Code').val(json[0].PreDefinedColumn7);
                $('#ddlDCountry').val(json[0].PreDefinedColumn8);
                $('#txtDPhone').val(json[0].PreDefinedColumn9);
                $('.myCheckbox').prop('checked', false);
                $('#chkIsDefault').val(json[0].IsDefault);
                $('#txtAddressTitle').val($("#ddlDAddressTitle option:selected").text());
                $('#ddlDCountry').change();
            }
            else {
                $('#txtDContact_Name').val('');
                $('#txtDCompany_Name').val('');
                $('#txtDAddress1').val('');
                $('#txtDAddress2').val('');
                $('#txtDTown').val('');
                $('#txtDState__County').val('');
                $('#txtDPostal_Code').val('');
                $('#ddlDCountry').val(0);
                $('#txtDPhone').val('');
                $('#txtAddressTitle').val('');
                $('#chkIsDefault').prop('checked', true);
            }
            //$.unblockUI();
            $('#collapseAddress').collapse('show');
            $('#dvLoader').hide();
        });
    }
}

$(function () {
    $("#ddl142").change(function () {
        var selectedText = $(this).find("option:selected").text();
        if (selectedText == "Other") {
            document.getElementById('txt152').value = "";
            document.getElementById('EventId').style.display = '';
            
           
        }
        else { document.getElementById('EventId').style.display = 'none'; }
    });
});

$(document).ready(function () {
    $('#dvRemoveCode').hide();

    //updateTotalPrice();
    paymentChanged();

    //var control1 = document.getElementById("rbService1");   
    //var control2 = document.getElementById("rbService2");
    //var control3 = document.getElementById("rbService3");	
    //if($('#rbService1').is(':visible'))
    //{
    //	console.log("Service1");
    //	$('#rbService1')[0].checked = true;	
    //	calculatefreight(1);
    //}
    //else if($('#rbService2').is(':visible'))
    //{
    //	console.log("Service2");
    //	$('#rbService2')[0].checked = true;	
    //	calculatefreight(2);
    //}
    //else if($('#rbService3').is(':visible'))
    //{
    //	console.log("Service3");
    //	$('#rbService3')[0].checked = true;	
    //	calculatefreight(3);
    //}

    if (document.getElementById('rbService1').checked) {
        var customtext = document.getElementById("hdncustom1").value;
        $('#divCustomMessage').html(customtext);
        $('#divCustomMessage').show();

    }
    else if (document.getElementById('rbService2').checked) {
        var customtext = document.getElementById("hdncustom2").value;
        $('#divCustomMessage').html(customtext);
        $('#divCustomMessage').show();
    }
    else if (document.getElementById('rbService3').checked) {
        var customtext = document.getElementById("hdncustom3").value;
        $('#divCustomMessage').html(customtext);
        $('#divCustomMessage').show();
    }
    $(document).on('change', '#ddlDAddressTitle', function () {
        $('#dvLoader').show();
        if (this.value == "0")
            $('#txtAddressTitle').show();
        else
            $('#txtAddressTitle').hide();
        $.ajax({
            url: host + 'shoppingcart/payment.aspx/changeaddress',
            data: "{'DeliveryAddressId': " + this.value + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            cache: false,
            async: true
            //beforeSend: function (request) {
            //    $.blockUI({ message: $('#dvLoader') });
            //}
        }).done(function (data) {
            if (data.d != "[]") {
                var json = $.parseJSON(data.d);
                $('#txtDContact_Name').val(json[0].PreDefinedColumn1);
                $('#txtDCompany_Name').val(json[0].PreDefinedColumn2);
                $('#txtDAddress1').val(json[0].PreDefinedColumn3);
                $('#txtDAddress2').val(json[0].PreDefinedColumn4);
                $('#txtDTown').val(json[0].PreDefinedColumn5);
                $('#txtDState__County').val(json[0].PreDefinedColumn6);
                $('#txtDPostal_Code').val(json[0].PreDefinedColumn7);
                $('#ddlDCountry').val(json[0].PreDefinedColumn8);
                $('#txtDPhone').val(json[0].PreDefinedColumn9);
                $('.myCheckbox').prop('checked', true);
                $('#chkIsDefault').prop('checked', (json[0].IsDefault));
                $('#txtAddressTitle').val($("#ddlDAddressTitle option:selected").text());
                $('#ddlDCountry').change();
            }
            else {
                $('#txtDContact_Name').val('');
                $('#txtDAddress1').val('');
                $('#txtDAddress2').val('');
                $('#txtDTown').val('');
                $('#txtDState__County').val('');
                $('#txtDPostal_Code').val('');
                $('#ddlDCountry').val(0);
                $('#txtDPhone').val('');
                $('#txtAddressTitle').val('');
                $('#chkIsDefault').prop('checked', false);
            }
            //$.unblockUI();
            $('#dvLoader').hide();
        });

    });

    $(document).on('change', '#ddlDCountry', function () {
        debugger;
        try {
            console.log("in");
            if ($('#hidCouponCode').val() != '') {
                $('#myWarningModal').find('#WarningMessage').html(aSC_Reapply_Coupon);
                //$('#myWarningModal').find('#WarningMessage').html('Your previous coupon code will be cleared. Please apply coupon code again.');
                $('#myWarningModal').modal('show');

                $('#hidCouponCode').val('');
                $('#txtCouponCode').val('');
                $('#dvApplyCode').show();
                $('#dvRemoveCode').hide();

            }
            //$('#dvLoader').show();
            console.log(IsInvoiceAccountEnabled);
            //console.log(this.value);
            console.log(pIsPointsEnbled);
            if (this.value != "0" && pIsPointsEnbled == 'False' && IsMultifreight == 'False') {
                $('#dvLoader').show();
                $.ajax({
                    url: host + 'shoppingcart/payment.aspx/CalculateFreightCharges',
                    data: "{'intCountryId': " + this.value + "}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    cache: false,
                    async: true
                    //beforeSend: function (request) {
                    //    $.blockUI({ message: $('#dvLoader') });
                    //}
                }).done(function (data) {
                    if (data.d != "[]") {
                        var json = $.parseJSON(data.d);
                        //console.log(json);
                        var SZPrice = parseFloat(json[0].SZPrice).toFixed(2);
                        var EZPrice = parseFloat(json[0].EZPrice).toFixed(2);
                        var PSPrice = parseFloat(json[0].PSPrice).toFixed(2); // SHRIGANESH 11 Nov 2016
                        var SZtax = parseFloat(json[0].StandardZone).toFixed(2);
                        var EZtax = parseFloat(json[0].ExpressZone).toFixed(2);
                        var PZtax = parseFloat(json[0].PalletShipmentZone).toFixed(2); // SHRIGANESH 11 Nov 2016
                        var isstandard = (json[0].IsStandard);
                        console.log(parseFloat(json[0].StandardZone));
                        console.log(SZtax);
                        console.log(EZtax);
                        console.log(PSPrice);
                        // vikram for legal entity
                        if (cultureCode != 'en-GB') {
                            SZPrice = SZPrice.replace('.', ',');
                            EZPrice = EZPrice.replace('.', ',');
                            PSPrice = PSPrice.replace('.', ',');
                            SZtax = SZtax.replace('.', ',');
                            EZtax = EZtax.replace('.', ',');
                            PZtax = PZtax.replace('.', ',');
                        }
                        // vikram for legal entity
                        $('#hidService1Tax').val(SZtax);
                        $('#hidService2Tax').val(EZtax);
                        $('#hidService3Tax').val(PZtax); // SHRIGANESH 11 Nov 2016
                        $('#hidService1').val(SZPrice);
                        $('#hidService2').val(EZPrice);
                        $('#hidService3').val(PSPrice); // SHRIGANESH 11 Nov 2016
                        $('#rbService1').attr('checked', false);
                        $('#rbService2').attr('checked', false);
                        $('#rbService3').attr('checked', false); // SHRIGANESH 11 Nov 2016

                        $('#divService1Cont').hide();
                        $('#divService2Cont').hide();
                        $('#divService3Cont').hide();// SHRIGANESH 11 Nov 2016

                        if (json[0].IsStandard) {
                            $('#divService1Cont').show();
                        }
                        if (json[0].IsExpress) {
                            $('#divService2Cont').show();
                        }
                        if (json[0].IsPalletShipment) {
                            $('#divService3Cont').show();
                        }
                        if (SZPrice == 0) {
                            if (isstandard) {
                                $('#rbService1')[0].checked = true;
                            }

                        }
                        else {
                            $('#rbService1').removeAttr('disabled');
                            $('#rbService1')[0].checked = true;
                            //$('#rbStandard').attr('checked', 'checked');
                        }
                        $('#spnShippingPrice').html(SZPrice);
                        $('#spnService1').html(SZPrice);
                        $('#spnTaxPrice').html(SZtax);

                        if (EZPrice == 0) {

                        }
                        else {
                            $('#rbService2').removeAttr('disabled');
                            if (!$('#rbService1').is(':checked')) {
                                console.log("ex");
                                $('#rbService2')[0].checked = true;
                                //$('#rbExpress').attr('checked', 'checked');
                                $('#spnShippingPrice').html(EZPrice);
                                $('#spnTaxPrice').html(EZtax);
                            }
                        }
                        $('#spnService2').html(EZPrice);

                        if (PSPrice == 0) {

                        }
                        else {
                            $('#rbService3').removeAttr('disabled');
                            if (!$('#rbService1').is(':checked')) {
                                console.log("ex");
                                $('#rbService3')[0].checked = true;
                                $('#spnShippingPrice').html(PSPrice);
                                $('#spnTaxPrice').html(PZtax);
                            }
                        }
                        $('#spnService3').html(PSPrice);

                        $('#divMOVMessage').hide();
                        if ($('#rbService2')[0].checked && json[0].EZDisallowOrder) {
                            $('#divMOVMessage').show();
                        }
                        else if (json[0].EZDisallowOrder) {
                            $('#divMOVMessage').show();
                        }

                        if ($('#rbService3')[0].checked && json[0].PZDisallowOrder) {
                            $('#divMOVMessage').show();
                        }
                        else if (json[0].EZDisallowOrder) {
                            $('#divMOVMessage').show();
                        }



                        if (json[0].DisplayDutyMessage)
                            $('#divDutyMessage').show();
                        else
                            $('#divDutyMessage').hide();

                        $('#spnService1Text').html(json[0].StandardMethodName + " (" + json[0].SZTransitTime + ")");
                        $('#spnService2Text').html(json[0].ExpressMethodName + " (" + json[0].EZTransitTime + ")");
                        $('#spnService3Text').html(json[0].palletMethodName + " (" + json[0].PZTransitTime + ")");

                        updateTotalPrice();
                    }
                    else {
                        alert(data.d);
                    }
                    //$.unblockUI();
                    $('#dvLoader').hide();
                });
            }
        } catch (e) { }
        finally { }
    });
    $(document).on('change', '#ddlCountry', function () {
        if (document.getElementById('addressCheckbox').checked) {
            $('#ddlDCountry').val($('#ddlCountry').val());
            $('#ddlDCountry').change();
        }
    });

    $(document).on('change', '#ddlInvoiceCountry', function () {
        if (document.getElementById('addressCheckbox').checked) {
            $('#ddlDCountry').val($('#ddlInvoiceCountry').val());
            $('#ddlDCountry').change();
        }
    });




    $(document).on('click', '#aProceed', function () {
        return ValidatePaymentDetails();
    });

    $(document).on('click', '#btnRemoveCoupon', function () {
        console.log("in");
        $('#hidCouponCode').val('');
        $('#txtCouponCode').val('');
        $('#ddlDCountry').change();
        $('#dvApplyCode').show();
        $('#dvRemoveCode').hide();
    });

    $(document).on('click', '#btnApplyCoupon', function () {
        //debugger;
        var message = '';

        if (IsEmpty($('#txtCouponCode').val())) {
            message += aSC_Require_Coupon;// 'Please enter coupon code.<br>';
        }
        else {
            if (!IsValidAlphaNumeric($('#txtCouponCode').val(), 50)) {
                message += astrSC_Invalid_Coupon_Code;//'Please enter valid coupon code.<br>';
            }
        }

        if (message != '') {
            $('#myWarningModal').find('#WarningMessage').html(message);
            $('#myWarningModal').modal('show');
            return false;
        }
        else {
            var ShippingMethod = 0;
            if ($('#rbService2')[0].checked)
                ShippingMethod = 2;
            else if ($('#rbService1')[0].checked)
                ShippingMethod = 1;
            else if ($('#rbService3')[0].checked)
                ShippingMethod = 3;


            //$('#dvLoader').show();
            $.ajax({
                url: host + 'shoppingcart/payment.aspx/ApplyCoupon',
                data: "{'CouponCode': '" + $('#txtCouponCode').val() + "','ShipmentType':'" + ShippingMethod + "','CountryId':" + $('#ddlDCountry').val() + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                var json = $.parseJSON(data.d);
                if (json[0].Message == "Success") {
                    $('#hidCouponCode').val($('#txtCouponCode').val().trim());
                    $('#dvApplyCode').hide();
                    $('#dvRemoveCode').show();
                    $('#dvTotal span').html(parseFloat(json[0].OrderValueAfterDiscount).toFixed(2));

                    if (json[0].ShipmentType == "1") {
                        $("#rbService2").attr("disabled", true);
                        var SZPrice = parseFloat(json[0].StandardFreightValueAfterDiscount).toFixed(2);
                        var SZtax = parseFloat(json[0].StandardTaxValueAfterDiscount).toFixed(2);
                        $('#hidStandardTax').val(SZtax);
                        $('#hidStandard').val(SZPrice);
                        $('#spnstandard').html(SZPrice);


                    }
                    if (json[0].ShipmentType == "2") {
                        $("#rbService1").attr("disabled", true);
                        var EZPrice = parseFloat(json[0].ExpressFreightValueAfterDiscount).toFixed(2);
                        var EZtax = parseFloat(json[0].ExpressTaxValueAfterDiscount).toFixed(2);
                        $('#hidExpressTax').val(EZtax);
                        $('#hidExpress').val(EZPrice);
                        $('#spnexpress').html(EZPrice);

                    }
                    if (json[0].ShipmentType == "0") {
                        var SZPrice = parseFloat(json[0].StandardFreightValueAfterDiscount).toFixed(2);
                        var EZPrice = parseFloat(json[0].ExpressFreightValueAfterDiscount).toFixed(2);
                        var SZtax = parseFloat(json[0].StandardTaxValueAfterDiscount).toFixed(2);
                        var EZtax = parseFloat(json[0].ExpressTaxValueAfterDiscount).toFixed(2);
                        $('#hidStandardTax').val(SZtax);
                        $('#hidExpressTax').val(EZtax);
                        $('#hidStandard').val(SZPrice);
                        $('#hidExpress').val(EZPrice);
                        $('#spnstandard').html(SZPrice);
                        $('#spnexpress').html(EZPrice);
                    }





                    $('#spnShippingPrice').html(SZPrice);
                    $('#spnTaxPrice').html(SZtax);

                    if (!$('#rbService1').is(':checked')) {
                        $('#spnShippingPrice').html(EZPrice);
                        $('#spnTaxPrice').html(EZtax);
                    }
                    updateTax();
                }
                else {
                    $('#myWarningModal').find('#WarningMessage').html(json[0].Message);
                    $('#myWarningModal').modal('show');
                }
                //$.unblockUI();
                $('#dvLoader').hide();
            });
        }
    });

    if (document.getElementById('addressCheckbox').checked) {
        $('#collapseAddress').collapse('hide');
    }
    else {
        $('#collapseAddress').collapse('show');
    }
    if (bDeliveryAddress == 'True')
        $('#collapseAddress').collapse('show');
    else
        $('#collapseAddress').collapse('hide');

    if ($('#txtCouponCode').val() != '') {
        $('#btnApplyCoupon').click();
    }
});

function paymentChanged() {
    var paymentMethod = $("#ddlPaymentTypes option:selected").val();
    var IsGC = $("#hdfGC").val();
    if (paymentMethod != null) {
        console.log(paymentMethod);
        var strPayment = paymentMethod.split('|');
        $('.paymentid').hide();
        if (strPayment[1] == 'C') {
            $('.paymentid' + strPayment[0]).show();
            $('#aProceed').val(astrSC_Enter_Card_Details);
            //$('#aProceed').val('Enter Card Details');//Commented by Sripal
            //$('#dvApplyGiftCertificate').show();
        }
        else {
            $('.paymentid' + strPayment[0]).show();
            $('#aProceed').val(astrSC_Place_Order);
            //$('#aProceed').val('Place order');//Commented by Sripal
            //$('#dvApplyGiftCertificate').hide();
        }
        //if (IsGC != '0') {
        //    if (strPayment != '0')
        //    { $('#dvApplyGiftCertificate').show(); }
        //    else
        //    {
        //        $('#dvApplyGiftCertificate').hide();
        //    }
        //}
        //else {
        //    $('#dvApplyGiftCertificate').hide();
        //}

        var btn = document.getElementById("aProceed");
        btn.onclick = function () {
            WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("aProceed", "", true, strPayment[0], "", false, false));
        }
    }
}

function ValidatePaymentDetails() {
    var message = '';
    var DeliveryErrorMsg = '';
    var InvoiceErrorMsg = '';

    //Validate billing contact name
    //Invoice Address Start
    if ($('#txtContact_Name').attr('rel') == 'm') {
        if (IsEmpty($('#txtContact_Name').val())) {
            InvoiceErrorMsg += $('#lblContactName').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter ' + $('#lblContactName').html() + '<br>';
        }
        else {
            if (!IsAngularBrackets($('#txtContact_Name').val(), 100)) {
                InvoiceErrorMsg += stValid + ' ' + $('#lblContactName').html() + '<br>';
                //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblContactName').html() + '<br>';
            }
        }
    }
    else if (IsEmpty($('#txtContact_Name').val())) {
        if (!IsAngularBrackets($('#txtContact_Name').val(), 100)) {
            InvoiceErrorMsg += stValid + ' ' + $('#lblContactName').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblContactName').html() + '<br>';
        }
    }

    //Validate billing txtCompany name
    if ($('#txtCompany').attr('rel') == 'm') {
        if (IsEmpty($('#txtCompany').val())) {
            InvoiceErrorMsg += $('#lblCompany').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter ' + $('#lblCompany').html() + '<br>';
        }
        else {
            if (!IsAngularBrackets($('#txtCompany').val(), 100)) {
                InvoiceErrorMsg += stValid + ' ' + $('#lblCompany').html() + '<br>';
                //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblCompany').html() + '<br>';
            }
        }
    }
    else if (IsEmpty($('#txtCompany').val())) {
        if (!IsAngularBrackets($('#txtCompany').val(), 100)) {
            InvoiceErrorMsg += stValid + ' ' + $('#lblCompany').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblCompany').html() + '<br>';
        }
    }
    //Validate billing Address line 1
    if ($('#txtAddress1').attr('rel') == 'm') {
        if (IsEmpty($('#txtAddress1').val())) {
            InvoiceErrorMsg += $('#lblAddress1').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter ' + $('#lblAddress1').html() + '<br>';
        }
        else {
            if (!IsAngularBrackets($('#txtAddress1').val(), 200)) {
                InvoiceErrorMsg += stValid + ' ' + $('#lblAddress1').html() + '<br>';
                //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblAddress1').html() + '<br>';
            }
        }
    }
    else if (!IsEmpty($('#txtAddress1').val())) {
        if (!IsAngularBrackets($('#txtAddress1').val(), 200)) {
            InvoiceErrorMsg += stValid + ' ' + $('#lblAddress1').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblAddress1').html() + '<br>';
        }
    }
    //Validate billing Address line 2
    if ($('#txtAddress2').attr('rel') == 'm') {
        if (IsEmpty($('#txtAddress2').val())) {
            InvoiceErrorMsg += $('#lblAddress2').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter ' + $('#lblAddress2').html() + '<br>';
        }
        else {
            if (!IsAngularBrackets($('#txtAddress2').val(), 200)) {
                InvoiceErrorMsg += stValid + ' ' + $('#lblAddress2').html() + '<br>';
                //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblAddress2').html() + '<br>';
            }
        }
    }
    else if (!IsEmpty($('#txtAddress2').val())) {
        if (!IsAngularBrackets($('#txtAddress2').val(), 200)) {
            InvoiceErrorMsg += stValid + ' ' + $('#lblAddress2').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblAddress2').html() + '<br>';
        }
    }

    //Validate billing Town
    if ($('#txtTown').attr('rel') == 'm') {
        if (IsEmpty($('#txtTown').val())) {
            InvoiceErrorMsg += $('#lblTown').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter ' + $('#lblTown').html() + '<br>';
        }
        else {
            if (!IsAngularBrackets($('#txtTown').val(), 100)) {
                InvoiceErrorMsg += stValid + ' ' + $('#lblTown').html() + '<br>';
                //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblTown').html() + '<br>';
            }
        }
    }
    else if (!IsEmpty($('#txtTown').val())) {
        if (!IsAngularBrackets($('#txtTown').val(), 100)) {
            InvoiceErrorMsg += stValid + ' ' + $('#lblTown').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblTown').html() + '<br>';
        }
    }
    //Validate billing State/County
    if ($('#txtState__County').attr('rel') == 'm') {
        if (IsEmpty($('#txtState__County').val())) {
            InvoiceErrorMsg += $('#lblState__County').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter ' + $('#lblState__County').html() + '<br>';
        }
        else {
            if (!IsAngularBrackets($('#txtState__County').val(), 100)) {
                InvoiceErrorMsg += stValid + ' ' + $('#lblState__County').html() + '<br>';
                //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblState__County').html() + '<br>';
            }
        }
    }
    else if (!IsEmpty($('#txtState__County').val())) {
        if (!IsAngularBrackets($('#txtState__County').val(), 100)) {
            InvoiceErrorMsg += stValid + ' ' + $('#lblState__County').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblState__County').html() + '<br>';
        }
    }
    //Validate billing Postal Code
    if ($('#txtPostal_Code').attr('rel') == 'm') {
        if (IsEmpty($('#txtPostal_Code').val())) {
            InvoiceErrorMsg += $('#lblPostalCode').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter ' + $('#lblPostalCode').html() + '<br>';
        }
        else {
            if (!IsValidPostCode($('#txtPostal_Code').val(), 20)) {
                InvoiceErrorMsg += stValid + ' ' + $('#lblPostalCode').html() + '<br>';
                //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblPostalCode').html() + '<br>';
            }
        }
    }
    else if (!IsEmpty($('#txtPostal_Code').val())) {
        if (!IsValidPostCode($('#txtPostal_Code').val(), 20)) {
            InvoiceErrorMsg += stValid + ' ' + $('#lblPostalCode').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblPostalCode').html() + '<br>';
        }
    }

    //Validate billing Country
    if ($('#ddlDCountry').attr('rel') == 'm') {
        if ($('#ddlDCountry').val() == 0) {
            InvoiceErrorMsg += $('#lblCountry').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please choose ' + $('#lblCountry').html() + '<br>';
        }
    }
    //Validate billing Phone
    if ($('#txtPhone').attr('rel') == 'm') {
        if (IsEmpty($('#txtPhone').val())) {
            InvoiceErrorMsg += $('#lblPhone').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter ' + $('#lblPhone').html() + '<br>';
        }
        else {
            if (!IsValidPhone($('#txtPhone').val(), 20)) {
                InvoiceErrorMsg += stValid + ' ' + $('#lblPhone').html() + '<br>';
                //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblPhone').html() + '<br>';
            }
        }
    }
    else if (!IsEmpty($('#txtPhone').val())) {
        if (!IsValidPhone($('#txtPhone').val(), 20)) {
            InvoiceErrorMsg += stValid + ' ' + $('#lblPhone').html() + '<br>';
            //message += $('#pBillingTitle').html() + ': ' + 'Please enter valid ' + $('#lblPhone').html() + '<br>';
        }
    }

    if (InvoiceErrorMsg.length > 0) {
        message += "<b>" + $('#pBillingTitle').html() + "</b>" + ': <br>' + InvoiceErrorMsg;
    }
    //Invoice Address End

    //Delivery Address start
    //validate delivery address if it is not same as billing address
    if ($('#addressCheckbox')[0].checked == false) {
        //Validate delivery adress title
        if ($('#ddlDAddressTitle').val() == 0) {
            if (IsEmpty($('#txtAddressTitle').val())) {
                DeliveryErrorMsg += Atitle + ' <br>';
                //message += 'Please enter delivery address title <br>';
            }
            else {
                if (!IsAngularBrackets($('#txtAddressTitle').val(), 50)) {
                    DeliveryErrorMsg += stValid + ' ' + Atitle + ' <br>';
                    //message += 'Please enter valid delivery address title <br>';
                }
            }
        }
        //Validate delivery contact name
        if ($('#txtDContact_Name').attr('rel') == 'm') {
            if (IsEmpty($('#txtDContact_Name').val())) {
                DeliveryErrorMsg += $('#lblDContactName').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter ' + $('#lblDContactName').html() + '<br>';
            }
            else {
                if (!IsAngularBrackets($('#txtDContact_Name').val(), 100)) {
                    DeliveryErrorMsg += stValid + ' ' + $('#lblDContactName').html() + '<br>';
                    //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDContactName').html() + '<br>';
                }
            }
        }
        else if (IsEmpty($('#txtDContact_Name').val())) {
            if (!IsAngularBrackets($('#txtDContact_Name').val(), 100)) {
                DeliveryErrorMsg += stValid + ' ' + $('#lblDContactName').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDContactName').html() + '<br>';
            }
        }
        //Validate delivery Address line 1
        if ($('#txtDAddress1').attr('rel') == 'm') {
            if (IsEmpty($('#txtDAddress1').val())) {
                DeliveryErrorMsg += $('#lblDAddress1').html() + '<br>';
                // message += $('#pDeliveryTitle').html() + ': ' + 'Please enter ' + $('#lblDAddress1').html() + '<br>';
            }
            else {
                if (!IsAngularBrackets($('#txtDAddress1').val(), 200)) {
                    DeliveryErrorMsg += stValid + ' ' + $('#lblDAddress1').html() + '<br>';
                    //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDAddress1').html() + '<br>';
                }
            }
        }
        else if (!IsEmpty($('#txtDAddress1').val())) {
            if (!IsAngularBrackets($('#txtDAddress1').val(), 200)) {
                DeliveryErrorMsg += stValid + ' ' + $('#lblDAddress1').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDAddress1').html() + '<br>';
            }
        }
        //Validate delivery Address line 2
        if ($('#txtDAddress2').attr('rel') == 'm') {
            if (IsEmpty($('#txtDAddress2').val())) {
                DeliveryErrorMsg += $('#lblDAddress2').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter ' + $('#lblDAddress2').html() + '<br>';
            }
            else {
                if (!IsAngularBrackets($('#txtDAddress2').val(), 200)) {
                    DeliveryErrorMsg += stValid + ' ' + $('#lblDAddress2').html() + '<br>';
                    //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDAddress2').html() + '<br>';
                }
            }
        }
        else if (!IsEmpty($('#txtDAddress2').val())) {
            if (!IsAngularBrackets($('#txtDAddress2').val(), 200)) {
                DeliveryErrorMsg += stValid + ' ' + $('#lblDAddress2').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDAddress2').html() + '<br>';
            }
        }

        //Validate delivery Town
        if ($('#txtDTown').attr('rel') == 'm') {
            if (IsEmpty($('#txtDTown').val())) {
                DeliveryErrorMsg += $('#lblDTown').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter ' + $('#lblDTown').html() + '<br>';
            }
            else {
                if (!IsAngularBrackets($('#txtDTown').val(), 100)) {
                    DeliveryErrorMsg += stValid + ' ' + $('#lblDTown').html() + '<br>';
                    //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDTown').html() + '<br>';
                }
            }
        }
        else if (!IsEmpty($('#txtDTown').val())) {
            if (!IsAngularBrackets($('#txtDTown').val(), 100)) {
                DeliveryErrorMsg += stValid + ' ' + $('#lblDTown').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDTown').html() + '<br>';
            }
        }
        //Validate delivery State/County
        if ($('#txtDState__County').attr('rel') == 'm') {
            if (IsEmpty($('#txtDState__County').val())) {
                DeliveryErrorMsg += $('#lblDState__County').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter ' + $('#lblDState__County').html() + '<br>';
            }
            else {
                if (!IsAngularBrackets($('#txtDState__County').val(), 100)) {
                    DeliveryErrorMsg += stValid + ' ' + $('#lblDState__County').html() + '<br>';
                    //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDState__County').html() + '<br>';
                }
            }
        }
        else if (!IsEmpty($('#txtDState__County').val())) {
            if (!IsAngularBrackets($('#txtDState__County').val(), 100)) {
                DeliveryErrorMsg += stValid + ' ' + $('#lblDState__County').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDState__County').html() + '<br>';
            }
        }
        //Validate delivery Postal Code
        if ($('#txtDPostal_Code').attr('rel') == 'm') {
            if (IsEmpty($('#txtDPostal_Code').val())) {
                DeliveryErrorMsg += $('#lblDPostalCode').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter ' + $('#lblDPostalCode').html() + '<br>';
            }
            else {
                if (!IsValidPostCode($('#txtDPostal_Code').val(), 20)) {
                    DeliveryErrorMsg += stValid + ' ' + $('#lblDPostalCode').html() + '<br>';
                    //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDPostalCode').html() + '<br>';
                }
            }
        }
        else if (!IsEmpty($('#txtDPostal_Code').val())) {
            if (!IsValidPostCode($('#txtDPostal_Code').val(), 20)) {
                DeliveryErrorMsg += stValid + ' ' + $('#lblDPostalCode').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDPostalCode').html() + '<br>';
            }
        }

        //Validate delivery Country
        if ($('#ddlDCountry').attr('rel') == 'm') {
            if ($('#ddlDCountry').val() == 0) {
                DeliveryErrorMsg += $('#lblDCountry').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please choose ' + $('#lblDCountry').html() + '<br>';
            }
        }
        //Validate delivery Phone
        if ($('#txtDPhone').attr('rel') == 'm') {
            if (IsEmpty($('#txtDPhone').val())) {
                DeliveryErrorMsg += $('#lblDPhone').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter ' + $('#lblDPhone').html() + '<br>';
            }
            else {
                if (!IsValidPhone($('#txtDPhone').val(), 20)) {
                    DeliveryErrorMsg += stValid + ' ' + $('#lblDPhone').html() + '<br>';
                    //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDPhone').html() + '<br>';
                }
            }
        }
        else if (!IsEmpty($('#txtDPhone').val())) {
            if (!IsValidPhone($('#txtDPhone').val(), 20)) {
                DeliveryErrorMsg += stValid + ' ' + $('#lblDPhone').html() + '<br>';
                //message += $('#pDeliveryTitle').html() + ': ' + 'Please enter valid ' + $('#lblDPhone').html() + '<br>';
            }
        }
    }
    if (DeliveryErrorMsg.length > 0) {
        message += "<b>" + $('#pDeliveryTitle').html() + "</b>" + ': <br>' + DeliveryErrorMsg;
    }
    //Delivery Address end

    //Check for payment method
    if (IsTotalvalueZeroafterGC == "False") {//vikram for legal entity
        if ($("#ddlPaymentTypes option:selected").val() == 0) {
            message += ASC_Payment_Types + '<br>';
            //message += 'Please select payment method.<br>';
        }
    }

    //check for the special instructions
    if (!IsEmpty($('#txtInstruction').val())) {
        if (!IsAngularBrackets($('#txtInstruction').val(), 200)) {
            message += stValid + ' ' + ASC_Special_Instruction + ' <br>';
            //message += 'Please enter valid special instructions. <br>';
        }
    }

    //Check for the inventory of the products
    if (bBackOrderAllowed == 'False') {
        for (var i = 0; i < $('#hdnInventory').length; i++) {
            if (bBASysStore == 'False') {
                if (parseFloat($('#txtQuantity')[i].value) > parseFloat($('#hdnInventory')[i].value)) {
                    message += $('#dvProductName').html() + ': ' + ABasket_Quantity_Not_More_Than_Stock_Message + ' ' + $('#hdnInventory')[i].value;
                    //message += $('#dvProductName').html() + ': ' + ' Can not order more than ' + $('#hdnInventory')[i].value;
                }
            }
            else {
                if (parseFloat($('#txtQuantity')[i].value) > parseFloat($('#hdnStockStatus')[i].value)) {
                    message += $('#dvProductName').html() + ': ' + ABasket_Quantity_Not_More_Than_Stock_Message + ' ' + $('#hdnStockStatus')[i].value;
                    //message += $('#dvProductName').html() + ': ' + ' Can not order more than ' + $('#hdnStockStatus')[i].value;
                }
            }
        }
    }

    if (message != '') {
        var aMsg = ARequired.fontcolor("red") + '<br><br>' + message
        $('#myWarningModal').find('#WarningMessage').html(aMsg);
        $('#myWarningModal').modal('show');
        return false;
    }
    else {
        var paymentMethod = $("#ddlPaymentTypes option:selected").val();
        var strPayment = paymentMethod.split('|');
        if (Page_ClientValidate(strPayment[0])) {
            $('#dvLoader').show();
            return true;
        }
        else {
            return false;
        }
    }
}



//function calculate_totals() {
//    var line_price_1 = parseFloat(document.getElementById('product_total_1').innerHTML);
//    var line_price_2 = parseFloat(document.getElementById('product_total_2').innerHTML);
//    var line_price_3 = parseFloat(document.getElementById('product_total_3').innerHTML);
//    var basket_subtotal = line_price_1 + line_price_2 + line_price_3;
//    document.getElementById('basket_price').innerHTML = basket_subtotal.toFixed(2);

//    var postage;
//    if (document.getElementById("optionsRadios1").checked) postage = parseFloat(document.getElementById('shipping_standard').innerHTML);
//    else postage = parseFloat(document.getElementById('shipping_express').innerHTML);
//    document.getElementById('postage_price').innerHTML = postage.toFixed(2);

//    var tax = (basket_subtotal + postage) * 0.2;
//    document.getElementById('tax_price').innerHTML = tax.toFixed(2);

//    var total = basket_subtotal + postage + tax;
//    document.getElementById('total_price').innerHTML = total.toFixed(2);
//}

$(document).ready(function () {

    $("#header1").click(function () {
        if ($(window).width() >= 992) return;
        $('html, body').animate({ scrollTop: $("#jump1").offset().top + 1 }, 1000);
    });

    $("#header2").click(function () {
        if ($(window).width() >= 992) return;
        $('html, body').animate({ scrollTop: $("#jump2").offset().top - 30 }, 1000);
    });

    $("#header3").click(function () {
        if ($(window).width() >= 992) return;
        $('html, body').animate({ scrollTop: $("#jump3").offset().top + $("#header2_space").height() - 5 }, 1000);
    });

    $("#header4").click(function () {
        if ($(window).width() >= 992) return;
        $('html, body').animate({ scrollTop: $("#jump4").offset().top - 100 }, 1000);
    });

    //$("#product_quantity_1").on('change keydown paste input', function () {
    //    product_quantity = parseInt(document.getElementById('product_quantity_1').value);
    //    line_price = parseFloat(document.getElementById('product_price_1').innerHTML);
    //    line_total = line_price * product_quantity;
    //    document.getElementById('product_total_1').innerHTML = line_total.toFixed(2);
    //    calculate_totals();
    //});

    //$("#product_quantity_2").on('change keydown paste input', function () {
    //    product_quantity = parseInt(document.getElementById('product_quantity_2').value);
    //    line_price = parseFloat(document.getElementById('product_price_2').innerHTML);
    //    line_total = line_price * product_quantity;
    //    document.getElementById('product_total_2').innerHTML = line_total.toFixed(2);
    //    calculate_totals();
    //});

    //$("#product_quantity_3").on('change keydown paste input', function () {
    //    product_quantity = parseInt(document.getElementById('product_quantity_3').value);
    //    line_price = parseFloat(document.getElementById('product_price_3').innerHTML);
    //    line_total = line_price * product_quantity;
    //    document.getElementById('product_total_3').innerHTML = line_total.toFixed(2);
    //    calculate_totals();
    //});

});
//...............................................................................................................................
window.onresize = sticky();
$(window).scroll(function () { sticky() });
//...............................................................................................................................
function sticky() {
    var LargeMedia = false;
    if ($(window).width() >= 992) {
        LargeMedia = true;
    }

    var $headers = $(".header");
    var scrollTop = $(window).scrollTop();

    if (scrollTop <= 0) $headers.css({ position: "relative", top: "0px" }); // reset all
    else {
        $headers.each(function (index, $el) {
            var $curHeader = $($headers).eq(index);
            var curTop = $curHeader.offset().top;
            var curHeight = $curHeader.height();
            var isRelative = ($el.isFixed && scrollTop <= $el.exTop); // scroll up
            var isFixed = (curTop <= scrollTop); // scroll down
            var position = "";
            var top = 0;

            if (LargeMedia) {
                isFixed = false;
                isRelative = true;
            }

            if (isRelative) {
                positon = "relative"; // reset
                top = 0;
                $el.isFixed = false;
                switch (index) {
                    case 0: $("#header1_space").css("margin-top", 15 + "px"); break;
                    case 1: $("#header2_space").css("margin-top", 15 + "px"); break;
                    case 2: $("#header3_space").css("margin-top", 15 + "px"); break;
                    case 3: $("#header4_space").css("margin-top", 15 + "px"); break;
                }
            }
            else if (isFixed) {
                position = "fixed";
                if (0 < index) {
                    for (var i = 0; i < index; i++) { top += $($headers).eq(i).height(); }
                }
                scrollTop += curHeight;
                if (!$el.isFixed) {
                    $el.isFixed = true;
                    $el.exTop = curTop;
                }
                $($el).css({ left: "0px" });
                switch (index) {
                    case 0: $("#header1_space").css("margin-top", 50 + "px"); break;
                    case 1: $("#header2_space").css("margin-top", 50 + "px"); break;
                    case 2: $("#header3_space").css("margin-top", 50 + "px"); break;
                    case 3: $("#header4_space").css("margin-top", 50 + "px"); break;
                }
            }
            $($el).css({ position: position, top: top + "px" });
        });
    }
}
