﻿$(document).ready(function () {

    $(document).on('click', '#aGCGuestLogin', function () {
	// debugger;
    //Validate Email address and password abc
    var message = '';

    if (IsEmpty($('#txtGuestGCEmail').val())) {
        message += MsgEmailBlank + '.<br>';
    }
    else {
        if (!IsValidEmail($('#txtGuestGCEmail').val())) {
            message += MsgInvalidEmail + '.<br>';
        }
    }

    if (message != '') {
        $('#myWarningModal').find('#WarningMessage').html(message);
        $('#myWarningModal').modal('show');
        return false;
    }
    else {
        $('#dvLoader').show();
        $.ajax({
            url: host + 'login/Register.aspx/GuestRegistration',
            data: "{'strEmail': '" + $('#txtGuestGCEmail').val() + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            cache: false,
            async: true
            //beforeSend: function (request) {
            //    $.blockUI({ message: $('#dvLoader') });
            //}
        }).done(function (data) {
            //$.unblockUI();
            $('#dvLoader').hide();
            if (data.d == "success") {
                window.location.href = host + 'GiftCertificate/GiftCertificate.aspx';
            }
            else if (data.d == "GuestUser") {
                 window.location.href = host + 'GiftCertificate/GiftCertificate.aspx';
            }
            else {
                $('#myErrorModal').find('#ErrorMessage').html(data.d);
                $('#myErrorModal').modal('show');
                return false;
            }
        });
    }
    });

    

    $(document).on('click', '#aGCLogin', function () {
        //debugger;
        //Validate Email address and password

                var message = '';
        if (IsEmpty($('#txtGCEmail').val())) {
            message += MsgEmailBlank + '.<br>';
        }
        else {
            //if (!IsValidEmail($('#myModal #txtEmail').val())) {
            //    message += 'Please enter valid email address.<br>';
            if (isEmailValid == 'True') //Changes Done By snehal 16-09-2016 For IsLogin Custom Field
            {
                if (!IsValidEmail($('#txtGCEmail').val())) {
                    message += MsgInvalidEmail + '.<br>';
                }
            }
        }
        if (IsEmpty($('#txtGCPassword').val())) {
            message += MsgPasswordBlank + '.<br>';
        }

        if (message != '') {
            $('#myWarningModal').find('#WarningMessage').html(message);
            $('#myWarningModal').modal('show');
            return false;
        }
        else {
            $('#dvLoader').show();
            $.ajax({
                url: host + 'login/login.aspx/CheckLogin',
                data: "{'strEmail': '" + $('#txtGCEmail').val() + "','strPassword':'" + $('#txtGCPassword').val() + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                $('#dvLoader').hide();
                if (data.d == "success") {
                    window.location.href = host + 'GiftCertificate/GiftCertificate.aspx';
                }
                else {
                    $('#myWarningModal').find('#WarningMessage').html(MsgInvalidUsenamePass);
                    $('#myWarningModal').modal('show');
                    //$('#myErrorModal').find('#btnErrorOK').click(function () { window.location.href = host + 'SignIn'; });
                }
            });
        }
    });
});