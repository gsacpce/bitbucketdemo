﻿// JScript File

//*****************************************************************
//This function is used to open a new browser window with different name
function PopUpPageWithDifferentName(PageURL, PageName, ScreenWidth, ScreenHeight) {

    if (ScreenWidth == null) {
        ScreenWidth = 560;
    }
    if (ScreenHeight == null) {
        ScreenHeight = 350;
    }
    widths = ScreenWidth; //560;
    heights = ScreenHeight; //250;
    /*lefts=(screen.width/2)-280;
    tops=(screen.height/2)-230;*/
    lefts = (screen.width - widths) / 2;
    tops = (screen.height - heights) / 2;
    thepage = PageURL;
    thepagename = PageName;
    //alert(thepagename);
    newpage = window.open(thepage, thepagename, 'scrollbars=yes,resizable=yes,Left=' + lefts + ',width=' + widths + ',height=' + heights + ',top=' + tops + '');
    newpage.focus();
}
//*****************************************************************
//This function is used to open a new browser window with different name
function PopUpPageWithDifferentNameNotResizable(PageURL, PageName, ScreenWidth, ScreenHeight) {
    if (ScreenWidth == null) {
        ScreenWidth = 560;
    }
    if (ScreenHeight == null) {
        ScreenHeight = 350;
    }
    widths = ScreenWidth; //560;
    heights = ScreenHeight; //250;
    /*lefts=(screen.width/2)-280;
    tops=(screen.height/2)-230;*/
    lefts = (screen.width - widths) / 2;
    tops = (screen.height - heights) / 2;
    thepage = PageURL;
    thepagename = PageName;
    //alert(thepagename);
    newpage = window.open(thepage, thepagename, 'menubar=no,scrollbars=no,resizable=no,Left=' + lefts + ',width=' + widths + ',height=' + heights + ',top=' + tops + '');
    newpage.focus();
}
//*****************************************************************

//*****************************************************************
//function to store the value in specified hidden field as per
//confirm box.
function showConfirmAndStoreValueOpenUrl(controlIdToStoreValue, valueToStoreInControl, message, url, pageName, screenWidth, screenHeight, loadSomeCodeBehindControlId) {
    var controlForStoringValue;
    controlForStoringValue = document.getElementById(controlIdToStoreValue);

    var confirmBoxAnswer;
    confirmBoxAnswer = confirm(message);

    var someControlId;
    someControlId = document.getElementById(loadSomeCodeBehindControlId);

    if (confirmBoxAnswer == true) {
        controlForStoringValue.value = '';
        controlForStoringValue.value = 'yes' + valueToStoreInControl;
        if (url != 'nourl') {
            PopUpPageWithDifferentName(url, pageName, screenWidth, screenHeight);

        }
        else if (url == 'nourl') {
            //here in this if statment there will be a linkbutton
            //id
            if (loadSomeCodeBehindControlId != null) {
                //someControlId = document.getElementById(loadSomeCodeBehindControlId);
                someControlId.click();
            }
        }

    }
    else {
        controlForStoringValue.value = '';
        controlForStoringValue.value = 'no' + valueToStoreInControl;
        //alert(someControlId);
        someControlId.click();

    }

}
//*****************************************************************

//*****************************************************************
//function to set focus on specified control
function setFocus(controlId) {
    var controlToFocus;
    controlToFocus = document.getElementById(controlId);

    controlToFocus.focus();
}
//*****************************************************************

//*****************************************************************
//function to highlight text inside textbox
function highlightText(controlId) {
    var controlToFocus;
    controlToFocus = document.getElementById(controlId);
    if (controlToFocus != null)
        controlToFocus.select();
}
//*****************************************************************

//*****************************************************************
//showConfirm is just to show the confirm box on mousedown and if 
//user clicks ok or yes then control's server event will be fired if any
function showConfirm(controlId, hiddenFieldId, messageToDisplay) {

    var control;
    control = document.getElementById(controlId);

    var confirmAnswer;
    confirmAnswer = confirm(messageToDisplay);

    if (confirmAnswer == true) {
        document.getElementById(hiddenFieldId).value = "true";
        control.click();
    }
    else {
        document.getElementById(hiddenFieldId).value = "false";
    }
}
//*****************************************************************

//*****************************************************************
//showConfirmOnKeyPress is just to show the confirm box onkeypress 
//and if user clicks ok or yes then control's server event will be 
//fired if any
function showConfirmOnKeyPress(controlId, hiddenFieldId, messageToDisplay) {

    if (event.keyCode == 13) {
        var control;
        control = document.getElementById(controlId);

        var confirmAnswer;
        confirmAnswer = confirm(messageToDisplay);

        //alert(confirmAnswer);
        if (confirmAnswer == true) {
            document.getElementById(hiddenFieldId).value = "true";
            control.click();
        }
        else {
            document.getElementById(hiddenFieldId).value = "false";
        }
    }
    else {
        document.getElementById(hiddenFieldId).value = "false";
    }

}
//*****************************************************************

//*****************************************************************
//function will just show hide the other control as per the 1st
//control's selected status
//checkReverse : this is used so that one can check flag in reverse
//manner also, false - normal and true - reverse
function showHideControlsOnDemand(flagControlId, showHideControlId, checkReverse) {
    var parentControl;
    var showHideControl;

    parentControl = document.getElementById(flagControlId);
    showHideControl = document.getElementById(showHideControlId);

    if (checkReverse = false) {
        if (parentControl.checked == true) {
            showHideControl.style.visibility = 'visible';
        }
        else {
            showHideControl.style.visibility = 'hidden';
        }
    }
    else {
        if (parentControl.checked == true) {
            showHideControl.style.visibility = 'hidden';
        }
        else {
            showHideControl.style.visibility = 'visible';
        }
    }
}
//*****************************************************************


//*********************************************************************
//function below named CheckNumericWithDecimal() allows user to input
//only the numeric values with or without decimals in textbox
function CheckNumericWithDecimal() {
    if (window.event.keyCode == 46) {
        window.event.keyCode = 46;
    }
    else if (!(window.event.keyCode >= 48 && window.event.keyCode <= 57)) {
        window.event.keyCode = 0;
    }
}
//*********************************************************************

//*********************************************************************
//function below named CheckNumeric() allows user to input
//only the numeric values decimals in textbox
function CheckNumeric() {
    if
    (!(window.event.keyCode >= 48 && window.event.keyCode <= 57)) {
        window.event.keyCode = 0;
    }
}
//*********************************************************************

//*********************************************************************
//function CheckNumericWithOneDecimal allows user to input only
//numeric values and only one decimal point
function CheckNumericWithOneDecimal(controlId) {
    //* only allow numbers to be entered*//
    var controlToCheck = document.getElementById(controlId);
    var checkOK = "0123456789.";
    var checkStr = controlToCheck.value;
    var allValid = true;
    var allNum = "";
    var intNumberOfDecimalPoints;
    intNumberOfDecimalPoints = 0;
    for (i = 0; i < checkStr.length; i++) {
        ch = checkStr.charAt(i);
        for (j = 0; j < checkOK.length; j++)
            if (ch == checkOK.charAt(j))
                break;
        if (j == checkOK.length) {
            allValid = false;
            break;
        }
        if (ch != ",")
            allNum += ch;

        if (ch == ".") {
            if (intNumberOfDecimalPoints == 0) {
                intNumberOfDecimalPoints += 1
            }
            else {
                allValid = false;
                break;
            }
        }

    }
    if (!allValid) {
        alert("Please enter numeric values only.");
        intNumberOfDecimalPoints = 0;
        controlToCheck.value = '';
        controlToCheck.focus();
        return (false);
    }
}
//*********************************************************************

//*********************************************************************
//function below named fixDecimals is used to get the fix number of
//decimal numbers after the point.
function fixDecimals(numberValue, numberOfPrecisions) {
    var numberWithPrecision;
    //alert(numberOfPrecisions);
    numberWithPrecision = (parseFloat(numberValue)).toFixed(numberOfPrecisions);
    //alert(numberWithPrecision);
    if (isNaN(numberWithPrecision) == true) {
        numberWithPrecision = 0;
    }

    return numberWithPrecision
}
//*********************************************************************

//*********************************************************************
//function below named setFixDecimals is used to set the return value
//to the control whose id is passed.
function setFixDecimals(controlIdToSet, numberValue, numberOfPrecisions) {
    var controlToSetValue;
    controlToSetValue = document.getElementById(controlIdToSet);
    //alert(fixDecimals(numberValueControlId, numberOfPrecisions));
    controlToSetValue.value = fixDecimals(numberValue, numberOfPrecisions);

}
//*********************************************************************

//*********************************************************************
//funtion to check the field are empty or not
function checkEmptyFields(controlId, message, buttonControl) {
    var controlToFind;
    controlToFind = document.getElementById(controlId);

    var button;
    button = document.getElementById(buttonControl);

    if (controlToFind.value.length == 0) {
        alert(message);
    }
    else {
        button.click();
    }
}
//*********************************************************************

//*********************************************************************
//funtion to check the dropdownlist fields are empty or not
function checkEmptyListItems(controlId, message, buttonControl, invalidIndexToCheck, hiddenFieldId, confirmMessage) {
    var controlToFind;
    controlToFind = document.getElementById(controlId);

    var button;
    button = document.getElementById(buttonControl);

    if (controlToFind.options[invalidIndexToCheck].selected == true) {
        alert(message);
    }
    else {
        if (confirmMessage != '' && confirmMessage != '') {
            showConfirm(buttonControl, hiddenFieldId, confirmMessage);
        }
        else {
            button.click();
        }
    }
}
//*********************************************************************

//*********************************************************************
//function MoveOption is used to move listbox item from source to
//destination listbox
function MoveOption(fromControlId, toControlId) {
    var objSourceElement;
    var objTargetElement;

    objSourceElement = document.getElementById(fromControlId);
    objTargetElement = document.getElementById(toControlId);

    var aryTempSourceOptions = new Array();
    var x = 0;
    //looping through source element to find selected options        
    for (var i = 0; i < objSourceElement.length; i++) {//start of for
        if (objSourceElement.options[i].selected) {//start of if within for
            //need to move this option to target element
            var intTargetLen = objTargetElement.length++;
            objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;
            objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;
        } //end of if within for          
        else//else of if within for
        {//start of else of if within for
            //storing options that stay to recreate select element 
            var objTempValues = new Object();
            objTempValues.text = objSourceElement.options[i].text;
            objTempValues.value = objSourceElement.options[i].value;
            aryTempSourceOptions[x] = objTempValues;
            x++;
        } //end of else of if within for
    } //end of for
    //resetting length of source  
    objSourceElement.length = aryTempSourceOptions.length;
    //looping through temp array to recreate source select element
    for (var i = 0; i < aryTempSourceOptions.length; i++) {//start of for
        objSourceElement.options[i].text = aryTempSourceOptions[i].text;
        objSourceElement.options[i].value = aryTempSourceOptions[i].value;
        objSourceElement.options[i].selected = false;
    } //end of for

} //end of function
//*********************************************************************

//*********************************************************************
//function StoreIds is used to store ids into hidden field 
//from the control specified
function StoreIds(leftListBoxId, rightListBoxId, hiddenFieldIdLeft, hiddenFieldIdRight) {
    var leftListId;
    leftListId = document.getElementById(leftListBoxId);

    var rightListId;
    rightListId = document.getElementById(rightListBoxId);

    var hiddenFieldLeft;
    hiddenFieldLeft = document.getElementById(hiddenFieldIdLeft);

    var hiddenFieldRight;
    hiddenFieldRight = document.getElementById(hiddenFieldIdRight);

    hiddenFieldLeft.value = "";
    hiddenFieldRight.value = "";

    //For left side control
    for (var i = 0; i < leftListId.length; i++) {
        if (hiddenFieldLeft.value == "") {
            hiddenFieldLeft.value = leftListId.options[i].value;
        }
        else {
            hiddenFieldLeft.value += "," + leftListId.options[i].value;
        }
    }

    //For right side control
    for (var i = 0; i < rightListId.length; i++) {
        if (hiddenFieldRight.value == "") {
            hiddenFieldRight.value = rightListId.options[i].value;
        }
        else {
            hiddenFieldRight.value += "," + rightListId.options[i].value;
        }
    }

}

function StoreNameNIdPipe(leftListBoxId, rightListBoxId, hiddenFieldIdLeft, hiddenFieldIdRight) {
    var leftListId;
    leftListId = document.getElementById(leftListBoxId);

    var rightListId;
    rightListId = document.getElementById(rightListBoxId);

    var hiddenFieldLeft;
    hiddenFieldLeft = document.getElementById(hiddenFieldIdLeft);

    var hiddenFieldRight;
    hiddenFieldRight = document.getElementById(hiddenFieldIdRight);

    hiddenFieldLeft.value = "";
    hiddenFieldRight.value = "";

    //For left side control
    for (var i = 0; i < leftListId.length; i++) {
        if (hiddenFieldLeft.value == "") {
            hiddenFieldLeft.value = leftListId.options[i].text + '|' + leftListId.options[i].value;
        }
        else {
            hiddenFieldLeft.value += "," + leftListId.options[i].text + '|' + leftListId.options[i].value;
        }
    }

    //For right side control
    for (var i = 0; i < rightListId.length; i++) {
        if (hiddenFieldRight.value == "") {
            hiddenFieldRight.value = rightListId.options[i].text + '|' + rightListId.options[i].value;
        }
        else {
            hiddenFieldRight.value += "," + rightListId.options[i].text + '|' + rightListId.options[i].value;
        }
    }
}

function ReloadListValues(ListBoxId, hiddenFieldId) {
    var ListId = document.getElementById(ListBoxId);
    var hiddenField = document.getElementById(hiddenFieldId);

    //var split = hiddenField.value.split('|');
    AddItems(ListId, 'test', '1');
}

function AddItems(ListBoxId, Text, Value) {
    var ListId = ListBoxId;
    var myOption = document.createElement("Option");
    myOption.text = Text;
    myOption.value = Value;
    ListId.add(myOption);
}

//*********************************************************************

//*********************************************************************
// function moveUpWard is used to move listitem in a listbox towards up
function moveUpWard(listBoxControlId) {
    var listField;
    listField = document.getElementById(listBoxControlId);

    if (listField.length == -1) {  // If the list is empty
        alert("There are no values which can be moved!");
    } else {
        var selected = listField.selectedIndex;
        if (selected == -1) {
            alert("You must select an entry to be moved!");
        } else {  // Something is selected 
            if (listField.length == 0) {  // If there's only one in the list
                alert("There is only one entry!\nThe one entry will remain in place.");
            } else {  // There's more than one in the list, rearrange the list order
                if (selected == 0) {
                    alert("The first entry in the list cannot be moved up.");
                } else {
                    // Get the text/value of the one directly above the hightlighted entry as
                    // well as the highlighted entry; then flip them
                    var moveText1 = listField[selected - 1].text;
                    var moveText2 = listField[selected].text;
                    var moveValue1 = listField[selected - 1].value;
                    var moveValue2 = listField[selected].value;
                    listField[selected].text = moveText1;
                    listField[selected].value = moveValue1;
                    listField[selected - 1].text = moveText2;
                    listField[selected - 1].value = moveValue2;
                    listField.selectedIndex = selected - 1; // Select the one that was selected before
                }  // Ends the check for selecting one which can be moved
            }  // Ends the check for there only being one in the list to begin with
        }  // Ends the check for there being something selected
    }  // Ends the check for there being none in the list
}
//*********************************************************************

//*********************************************************************
// function moveDownWard is used to move listitem in a listbox towards down
function moveDownWard(listBoxControlId) {
    var listField;
    listField = document.getElementById(listBoxControlId);

    if (listField.length == -1) {  // If the list is empty
        alert("There are no values which can be moved!");
    } else {
        var selected = listField.selectedIndex;
        if (selected == -1) {
            alert("You must select an entry to be moved!");
        } else {  // Something is selected 
            if (listField.length == 0) {  // If there's only one in the list
                alert("There is only one entry!\nThe one entry will remain in place.");
            } else {  // There's more than one in the list, rearrange the list order
                if (selected == listField.length - 1) {
                    alert("The last entry in the list cannot be moved down.");
                } else {
                    // Get the text/value of the one directly below the hightlighted entry as
                    // well as the highlighted entry; then flip them
                    var moveText1 = listField[selected + 1].text;
                    var moveText2 = listField[selected].text;
                    var moveValue1 = listField[selected + 1].value;
                    var moveValue2 = listField[selected].value;
                    listField[selected].text = moveText1;
                    listField[selected].value = moveValue1;
                    listField[selected + 1].text = moveText2;
                    listField[selected + 1].value = moveValue2;
                    listField.selectedIndex = selected + 1; // Select the one that was selected before
                }  // Ends the check for selecting one which can be moved
            }  // Ends the check for there only being one in the list to begin with
        }  // Ends the check for there being something selected
    }  // Ends the check for there being none in the list
}
//*********************************************************************

//*********************************************************************
//function MoveOption is used to store ids into hidden field 
//from the control specified
function StoreManulDisplayOrderIds(listBoxControlId, hiddenFieldControlId) {
    var listBoxId;
    listBoxId = document.getElementById(listBoxControlId);

    var hiddenFieldId;
    hiddenFieldId = document.getElementById(hiddenFieldControlId);

    hiddenFieldId.value = "";

    //For list box control
    for (var i = 0; i < listBoxId.length; i++) {
        if (hiddenFieldId.value == "") {
            hiddenFieldId.value = listBoxId.options[i].value;
        }
        else {
            hiddenFieldId.value += "," + listBoxId.options[i].value;
        }
    }
}
//*********************************************************************

//*********************************************************************
// selectOne function is used to select only one checkbox/radio button with in
// a grid
//value for radioOrCheckBoxId will be radio/checkbox client id
//value for gridName will be datagrid or any datacontrol's client id
//value for markCheckedSelected will be true(mark clicked control checked), false(mark clicked control unchecked)
//value for radioOrCheckBox will be 1(radio) or 2(checkbox)
var strPreviousControlId;
strPreviousControlId = '';
function selectOne(radioOrCheckBoxId, gridName, markCheckedSelected, radioOrCheckBox) {//start of function

    var controlId;
    controlId = document.getElementById(radioOrCheckBoxId);
    var controlInitialState;
    controlInitialState = controlId.checked;

    all = document.getElementsByTagName("input"); /* Getting an array of all the "INPUT" controls on the form.*/

    for (i = 0; i < all.length; i++) {//start of for 1
        if (radioOrCheckBox == 1)//if statement for radiobutton
        {//start of radiobutton's if
            if (all[i].type == "radio")/*Checking if it is a radio button*/
            {//start of if 1
                var count = all[i].id.indexOf(gridName + '_ctl'); /*I have added '_ctl' ASP.NET adds '_ctl' to all the controls of DataGrid.*/
                if (count != -1) {//start of if 2
                    all[i].checked = false;
                } //end of if 2
            } //end of if 1    
        } //end of radiobutton's if
        else if (radioOrCheckBox == 2)//if statement for checkbox
        {
            if (all[i].type == "checkbox")/*Checking if it is a checkbox*/
            {//start of if 1
                var count = all[i].id.indexOf(gridName + '_ctl'); /*I have added '_ctl' ASP.NET adds '_ctl' to all the controls of DataGrid.*/
                if (count != -1) {//start of if 2
                    all[i].checked = false;
                } //end of if 2
            } //end of if 1    
        }
    } //end of for 1

    if (markCheckedSelected == true) {//start of if (markCheckedSelected == true)
        controlId.checked = true; /* Finally making the selected radiobutton/checkbox CHECKED */
    } //end of if (markCheckedSelected == true)
    else {//start of else of (markCheckedSelected == true)
        if (strPreviousControlId == controlId.id) {//start of if (strPreviousControlId==controlId.id)
            if (controlInitialState == true) {//start of if (controlInitialState==true)
                controlId.checked = true;
                strPreviousControlId = controlId.id;
            } //end of if (controlInitialState==true)
            else {//start of else of (controlInitialState==true)
                controlId.checked = false;
                strPreviousControlId = controlId.id;
            } //end of else of (controlInitialState==true)
        } //end of if (strPreviousControlId==controlId.id)
        else {//start of else of (strPreviousControlId==controlId.id)
            controlId.checked = true;
            strPreviousControlId = controlId.id;
        } //end of else of (strPreviousControlId==controlId.id)
    } //end of else of (markCheckedSelected == true)

} //end of function
//*********************************************************************

//*********************************************************************
// function validatePresentOrNewData will check weather user has selected
// or enter values or not.
// dropdownlistId: dropdownlist control id
// textboxId: textbox control id
// alertMessage: alert message to show
function validatePresentOrNewData(dropdownlistId, textboxId, alertMessage) {
    var ddlId;
    var txtId;
    ddlId = document.getElementById(dropdownlistId);
    txtId = document.getElementById(textboxId);

    if (ddlId.selectedIndex == 0 && trim(txtId.value) == '') {
        alert(alertMessage);
        setFocus(ddlId.id);
        return false;
    }
    else if (ddlId.selectedIndex != 0 && trim(txtId.value) != '') {
        alert(alertMessage);
        setFocus(ddlId.id);
        return false;
    }
    else
    { return true; }
}
//*********************************************************************

//*********************************************************************
// function validatePresentData will check weather user has selected
// value or not.
// dropdownlistId: dropdownlist control id
// alertMessage: alert message to show
function validatePresentData(dropdownlistId, alertMessage) {
    var ddlId;
    ddlId = document.getElementById(dropdownlistId);

    if (ddlId.selectedIndex == 0) {
        alert(alertMessage);
        setFocus(ddlId.id);
        return false;
    }
    else
    { return true; }

}
//*********************************************************************

//*********************************************************************
// trim() function will be used to remove leading or trailing space from
// the provided string
function trim(stringToTrim) {
    stringToTrim = stringToTrim.replace(/^\s+|\s+$/g, '');
    return stringToTrim;
}
//*********************************************************************

//*********************************************************************
// setDefaultTextBoxValue() and removeDefaultTextBoxValue() will be used
// for setting default text within textbox and to remove from the textbox
// respectivly
function setDefaultTextBoxValue(controlId, valueToSet) {
    var textBoxControl;
    textBoxControl = document.getElementById(controlId);
    if (textBoxControl.value == '')
        textBoxControl.value = valueToSet;
}
function removeDefaultTextBoxValue(controlId, valueToSet) {
    var textBoxControl;
    textBoxControl = document.getElementById(controlId);
    if (textBoxControl.value == valueToSet)
        textBoxControl.value = '';
}
//*********************************************************************
//*********************************************************************
//This function is used to check "OR" condition of controls in CustomEflyerClientDetails.aspx.cs
//on keypress & mousedown events of button preview and Save & email
function checkForm(ClientnameId, Textid, FileUploadLogoImageId, EflyerNameId) {
    var textBoxClientnameControl;
    var textBoxTextControl;
    var fileUploadControl;
    var textBoxEflyerNameControl;
    textBoxClientnameControl = document.getElementById(ClientnameId);
    textBoxTextControl = document.getElementById(Textid);
    fileUploadControl = document.getElementById(FileUploadLogoImageId);
    textBoxEflyerNameControl = document.getElementById(EflyerNameId);

    if (textBoxClientnameControl.value == "" && textBoxTextControl.value == "") {
        alert("Please select any one option - Either enter name and upload a logo OR  Enter text for the Eflyer!");
        return false;
    }
    else if (textBoxClientnameControl.value != "" && fileUploadControl.value != "" && textBoxTextControl.value != "") {
        alert("Please select any one option - Either enter name and upload a logo OR  Enter text for the Eflyer!");
        return false;
    }
    else if (textBoxClientnameControl.value != "" && fileUploadControl.value == "") {
        alert("Please select any one option - Either enter name and upload a logo OR  Enter text for the Eflyer!");
        return false;
    }
    else if (textBoxEflyerNameControl.value == "") //as Eflyer name is compulsory
    {
        alert("Please enter a name for the Eflyer");
        return false;
    }
    else {
        return true;
    }
}
//*********************************************************************

//*********************************************************************
//*********************************************************************
//function below named changeProductImage is used to show product image
//but on click of the variation name of a variant
function changeProductImage(imageControlId, fullyQulifiedImageName, hiddenFieldId, hiddenFieldValueToSet, hiddenFieldForCurrentImageURL, hdnZoomImgUrl, zoomImgUrl) {
    var objProductImage;
    objProductImage = document.getElementById(imageControlId);
    objProductImage.src = fullyQulifiedImageName;

    var objhdnZoomImgUrl = document.getElementById(hdnZoomImgUrl);

    var hiddenControlForCurrentImageURL;
    hiddenControlForCurrentImageURL = document.getElementById(hiddenFieldForCurrentImageURL);
    hiddenControlForCurrentImageURL.value = fullyQulifiedImageName;

    var imageName;
    imageName = fullyQulifiedImageName.split('/');

    var imageId;
    imageId = imageName[imageName.length - 1].split('.')

    var hiddenControl;
    hiddenControl = document.getElementById(hiddenFieldId);
    if ((fullyQulifiedImageName.indexOf('Variant') != -1) || (fullyQulifiedImageName.indexOf('variant') != -1)) {
        hiddenControl.value = hiddenFieldValueToSet + '&vid=' + imageId[0];
    }
    else {
        hiddenControl.value = hiddenFieldValueToSet;
    }
    objhdnZoomImgUrl.value = zoomImgUrl;
}
//*********************************************************************

//*********************************************************************
//function below named popUpProductImage is used to show product's zoom image
function popUpProductImage(hiddenFieldId) {
    var hiddenControl;
    hiddenControl = document.getElementById(hiddenFieldId);

    PopUpPageWithDifferentName('ProductZoomImage.aspx' + hiddenControl.value, 'ViewProductZoomImage', 570, 580);
}
//*********************************************************************
//*********************************************************************
//function below named showColorPicker is used to show color picker
function showColorPicker(colorPickerParentDirectoryPath, textboxControlId) {
    var controlId;
    controlId = document.getElementById(textboxControlId);

    if (controlId != null)
        CP.popup(colorPickerParentDirectoryPath + 'Scripts/ColorPicker/ColorPicker.html', controlId, 1);
}
//*********************************************************************

//*********************************************************************
//function below named RefreshParentCloseChild will refresh opener parent
function RefreshParentCloseChild(parentUrl) {
    //window.opener.document.forms[0].action= a 
    //window.opener.document.forms[0].submit();
    window.opener.location.href = parentUrl;
    self.close();
}
//*********************************************************************
//*********************************************************************
// To check the minimum quantity for Order Cart page
//*********************************************************************
function CheckMinimumQuantity(minval, clientId) {
    //solving template3 issues
    var obj = document.getElementById(clientId);
    var qnty = obj.value;

    if (parseInt(qnty) < parseInt(minval) || qnty == "") {
        alert("Please enter quantity greater than or equal to the minimum quantity !! ");
        obj.focus();
        return false;
    }
    return true;
}
//*********************************************************************

//*********************************************************************
//*********************************************************************
// function below named go_etracking is redirecting to order tracking site
//*********************************************************************
function go_etracking() {
    var where_to = confirm("This will take you an external site.Please close the window to return to original site.Do you wish to continue??");
    if (where_to == true) {
        window.open("http://wwwapps.ups.com/etracking/tracking.cgi");
    }
    else {

    }
}
//*********************************************************************

//*********************************************************************
//*********************************************************************
// function below named go_etracking is redirecting to order tracking site
//*********************************************************************
function go_eFedEx() {
    var where_to = confirm("This will take you an external site.Please close the window to return to original site.Do you wish to continue??");
    if (where_to == true) {
        window.open("http://www.fedex.com/Tracking");
    }
    else {

    }
}
function checkpaymentinformation(paymenttype) {
    //if (paymenttype == 'purchase order')
    //{
    alert(' Purchase Order Number cannot be Empty !!');
    return false;
    // }
    // else
    //    return true;

}

//*********************************************************************
//*********************************************************************
// function below named telnumchk is validating the telephone number
//*********************************************************************

function telnumchk() {
    event.keyCode = TelDisallowChar(event.keyCode)

}
//*********************************************************************

//*********************************************************************
//*********************************************************************
// function below named TelDisallowChar is disallowing characters
//*********************************************************************

function TelDisallowChar(keyPressed) {
    if (keyPressed >= 48 && keyPressed <= 57 || keyPressed == 45) {
    }
    else {
        alert("Please Enter Numbers only");
        keyPressed = 0
    }
    return keyPressed
}
//*********************************************************************

//*********************************************************************
//*********************************************************************
// function below named CheckIsNumber is validating the input as number
//*********************************************************************

function CheckIsNumber(obj) {
    if (isNaN(obj.value)) {
        obj.value = "";
        alert("Enter valid Number");
        return false;
    }
    return true;
}
//*********************************************************************
// To Print the Page
//*********************************************************************
function windowprint(productid, x) {
    //window.open("printpage.asp");
    window.print();
}
//*********************************************************************

//*********************************************************************

/////////////////////////////////////////////////////////////////////
/// THIS select_deselectAll FUNCTION IS FOR SELECTING/DESELECTING //
/// THE CHECKBOX FOR SELECTING/DESELECTING ALL CHECKBOXES IN GRID /
//////////////////////////////////////////////////////////////////
// way to implement
// <INPUT id="checkboxPickAll" onclick="javascript: return select_deselectAll (this.checked, this.id,'checkboxContact');" type="checkbox" name="checkboxPickAll">

function select_deselectAll(chkVal, idVal, idChildVal) {

    var frm = document.forms[0];
    // Loop through all elements

    for (i = 0; i < frm.length; i++) {

        var str = frm.elements[i].id;
        var abc = str.split("_")
        //alert(abc[4]); --abc[4] means server checkbox id

        // Look for our Header Template's Checkbox
        if (idVal.indexOf('checkboxPickAll') != -1) {

            // Check if main checkbox is checked, then select or deselect datagrid checkboxes 
            if (chkVal == true) {
                if (abc[4] == idChildVal) {
                    frm.elements[i].checked = true;
                }
            }
            else {
                if (abc[4] == idChildVal) {
                    frm.elements[i].checked = false;
                }
            }
            // Work here with the Item Template's multiple checkboxes
        }
        else if (idVal.indexOf('checkPickAll') != -1) {
            // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
            if (chkVal == true) {
                if (abc[4] == idChildVal) {
                    frm.elements[i].checked = true;
                }
            }
            else {
                if (abc[4] == idChildVal) {
                    frm.elements[i].checked = false;
                }
            }
        }
    }

}
//*********************************************************************

function BrowsekeyChk() {
    var frm = document.forms[0];
    alert("Please click on BROWSE to select the file !!!");
    document.frm.fleupdLogoImage.PostedFile.FileName = "";
    //document.frm.fleupdLogoImage.blur();
}
function keyval1() {
    if ((event.keyCode == 8) || (event.keyCode == 46)) {
        event.returnValue = false;

    }
}
//This function is used to display mouseover image
function showMouseOverImage(strImgPath, objId, e, tipwidth) {
    if (strImgPath == "") return;
    obj = document.getElementById(objId);
    objDiv = document.getElementById("id_divMouseOverImg");
    objImg = document.getElementById("id_MouseOverImg");
    objDiv.style.left = (obj.offsetWidth) + getposOffset(obj, "right") + "px";
    objDiv.style.top = getposOffset(obj, "top") + "px";
    objDiv.style.visibility = "visible";
    objImg.src = strImgPath;
    obj.onmouseout = HideMouseOverImage;
}
//This function is used to disable mouseover image
function HideMouseOverImage() {
    objDiv = document.getElementById("id_divMouseOverImg");
    objDiv.style.visibility = "hidden";
}
//This is to find the exact position of an object
function getposOffset(what, offsettype) {
    var totaloffset = (offsettype == "left") ? what.offsetLeft : what.offsetTop;
    var parentEl = what.offsetParent;
    while (parentEl != null) {
        totaloffset = (offsettype == "left") ? totaloffset + parentEl.offsetLeft : totaloffset + parentEl.offsetTop;
        parentEl = parentEl.offsetParent;
    }
    return totaloffset;
}

function iecompat() {
    return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body
}

//This is to find the exact position of an object
function getposOffset(what, offsettype) {
    var totaloffset = (offsettype == "left") ? what.offsetLeft : what.offsetTop;
    var parentEl = what.offsetParent;
    while (parentEl != null) {
        totaloffset = (offsettype == "left") ? totaloffset + parentEl.offsetLeft : totaloffset + parentEl.offsetTop;
        parentEl = parentEl.offsetParent;
    }
    return totaloffset;
}
//This is used to set the opacity
function setOpacity(obj, opacityvalue) {
    var ie = (document.all) ? 1 : 0;
    var opacityelement = (ie) ? "filter" : "MozOpacity";
    opacityvalue = (ie) ? "alpha(opacity=" + opacityvalue + ")" : opacityvalue / 100;
    obj.style[opacityelement] = opacityvalue; alert(opacityvalue);
}

function checkuncheckAll(field, val) {
    //var chk=document.getElementsByName(field); 
    //for (i = 0; i < chk.length; i++)
    //    chk[i].checked = val ;

    var frm = document.forms[0];
    for (i = 0; i < frm.elements.length; i++) {
        if (frm.elements[i].type == "checkbox") {
            frm.elements[i].checked = val;
        }
    }
}
function uncheckAll(field, val) {
    var chk = document.getElementsByName(field);
    if (val == false) {
        chk[0].checked = val;
    }
}
function validatePage(fieldVal) {
    if (fieldVal == null)
        fieldVal = '';

    result = confirm('Are you sure ' + fieldVal + '?');
    return result;
}
function validateListPage(chk, msgchk, msg) {
    var frm = document.forms[0];
    for (i = 0; i < frm.elements.length; i++) {
        if (frm.elements[i].type == "checkbox" && frm.elements[i].checked == true) {
            if (msg != "") {
                result = confirm(msg);
                return result;
            }
            else {
                return;
            }
        }
    }
    alert(msgchk);
    return false;
}

function ValidateEmail(email) {
    //var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var reg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*$/;
    if (reg.test(email) == false) {
        return "invalid";
    }
    else {
        return "valid"
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}