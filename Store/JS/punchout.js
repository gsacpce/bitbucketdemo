﻿function updateTotalPrice() {
    //debugger;
    var totalPrice = 0;

    for (var i = 0; i < $('#dvTotalPrice span').length; i++) {
        totalPrice = (parseFloat(totalPrice) + parseFloat($('#dvTotalPrice span')[i].textContent)).toFixed(2);
    }
    //if (IsPointsEnbled == 'True') {
    //    totalPrice = parseFloat(totalPrice.toString());
    //}

    $('#dvTotal span').html(totalPrice);
    
    totalPrice = (parseFloat(totalPrice) + parseFloat($('#spnShippingPrice').html())).toFixed(2);
    //parseFloat($('#spnTaxPrice').html()) +
    //if (IsPointsEnbled == 'True') {
    //    totalPrice = parseFloat(totalPrice.toString());
    //}

    $('#spnTotalPrice').html(totalPrice);
    //updateGC(totalPrice);

}



function calculatefreightvalue(id) {
    //debugger;
    var shippingPrice = 0;
    var Baskettotal = 0;
    var Total = 0;
    var DelChargeId = 0;

    if (id == "1") {
        //shippingPrice = $('#spnstandard').html();
        //Total = $('#dvTotal span').html();
        DelChargeId = 1;
        Baskettotal = parseFloat($('#hidBasketTot').val());
        shippingPrice = parseFloat($('#hidStandard').val());
        $('#hidSpnShippingPrice').val(shippingPrice);
        $('#hidDeliveryCharge').val(DelChargeId);
        Total = Baskettotal + shippingPrice;
        $('#dvTotal span').html(Total);
    }
    else if (id == "2") {
        DelChargeId = 2;
        Baskettotal = parseFloat($('#hidBasketTot').val());
        shippingPrice = parseFloat($('#hidExpress').val());
        $('#hidSpnShippingPrice').val(shippingPrice);
        $('#hidDeliveryCharge').val(DelChargeId);
        Total = Baskettotal + shippingPrice;
        $('#dvTotal span').html(Total);
        //shippingPrice = $('#spnexpress').html();
        //Total = $('#dvTotal span').html();
    }

    $('#spnShippingPrice').html(shippingPrice);
    //$('#spnTaxPrice').html(tax);
    //updateTax();
}

function calculatefreight(id) {
    //debugger;
    var shippingPrice = 0;
    var Baskettotal = 0;
    var Total = 0;

    if (id.value == "1") {
        //shippingPrice = $('#spnstandard').html();
        //Total = $('#dvTotal span').html();
        Baskettotal = parseFloat($('#hidBasketTot').val());
        shippingPrice = parseFloat($('#hidStandard').val());
        $('#hidSpnShippingPrice').val(shippingPrice);
        Total = Baskettotal + shippingPrice;
        $('#dvTotal span').html(Total);
    }
    else if (id.value == "2") {
        Baskettotal = parseFloat($('#hidBasketTot').val());
        shippingPrice = parseFloat($('#hidExpress').val());
        $('#hidSpnShippingPrice').val(shippingPrice);
        Total = Baskettotal + shippingPrice;
        $('#dvTotal span').html(Total);
        //shippingPrice = $('#spnexpress').html();
        //Total = $('#dvTotal span').html();
    }

    $('#spnShippingPrice').html(shippingPrice);
    //$('#spnTaxPrice').html(tax);
    //updateTax();
}


$(document).ready(function () {
    $('#dvRemoveCode').hide();

    //updateTotalPrice();
    //paymentChanged();
    $(document).on('change', '#ddlDAddressTitle', function () {
        $('#dvLoader').show();
        if (this.value != "0")
            $('#txtAddressTitle').show();
        else
            $('#txtAddressTitle').hide();
        $.ajax({
            url: host + 'shoppingcart/payment.aspx/changeaddress',
            data: "{'DeliveryAddressId': " + this.value + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            cache: false,
            async: true
            //beforeSend: function (request) {
            //    $.blockUI({ message: $('#dvLoader') });
            //}
        }).done(function (data) {
            if (data.d != "[]") {
                var json = $.parseJSON(data.d);
                $('#txtDContact_Name').val(json[0].PreDefinedColumn1);
                $('#txtDCompany_Name').val(json[0].PreDefinedColumn2); // Added by SHRIGANESH 27 July 2016 to display Company Name in Delivery Address section
                $('#txtDAddress1').val(json[0].PreDefinedColumn3);
                $('#txtDAddress2').val(json[0].PreDefinedColumn4);
                $('#txtDTown').val(json[0].PreDefinedColumn5);
                $('#txtDState__County').val(json[0].PreDefinedColumn6);
                $('#txtDPostal_Code').val(json[0].PreDefinedColumn7);
                $('#ddlDCountry').val(json[0].PreDefinedColumn8);
                $('#txtDPhone').val(json[0].PreDefinedColumn9);
                $('.myCheckbox').prop('checked', true);
                $('#chkIsDefault').prop('checked', (json[0].IsDefault));
                $('#txtAddressTitle').val($("#ddlDAddressTitle option:selected").text());
                $('#ddlDCountry').change();
            }
            else {
                $('#txtDContact_Name').val('');
                $('#txtDCompany_Name').val(''); // Added by SHRIGANESH 27 July 2016 to display Company Name in Delivery Address section
                $('#txtDAddress1').val('');
                $('#txtDAddress2').val('');
                $('#txtDTown').val('');
                $('#txtDState__County').val('');
                $('#txtDPostal_Code').val('');
                $('#ddlDCountry').val(0);
                $('#txtDPhone').val('');
                $('#txtAddressTitle').val('');
                $('#chkIsDefault').prop('checked', false);
            }
            //$.unblockUI();
            $('#dvLoader').hide();
        });

    });

    $(document).on('change', '#ddlCountry', function () {
        if (document.getElementById('addressCheckbox').checked) {
            $('#ddlDCountry').val($('#ddlCountry').val());
            $('#ddlDCountry').change();
        }
    });

    $(document).on('change', '#ddlDCountry', function () {
        //debugger;
        try {
        //    if ($('#hidCouponCode').val() != '') {
        //        $('#myWarningModal').find('#WarningMessage').html(aSC_Reapply_Coupon);
        //        //$('#myWarningModal').find('#WarningMessage').html('Your previous coupon code will be cleared. Please apply coupon code again.');
        //        $('#myWarningModal').modal('show');

        //        $('#hidCouponCode').val('');
        //        $('#txtCouponCode').val('');
        //        $('#dvApplyCode').show();
        //        $('#dvRemoveCode').hide();
        //    }
            //$('#dvLoader').show();

            if (this.value != "0") {
                $('#header2_space').show();
                $('#divShipping').show();
                var selectedCountry = $('#ddlDCountry').val();
                $('#hidddlDCountry').val(selectedCountry);
                $('#dvLoader').show();
                $.ajax({
                    url: host + 'shoppingcart/BasketPunchout.aspx/CalculateFreightCharges',
                    data: "{'intCountryId': " + this.value + "}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    cache: false,
                    async: true
                    //beforeSend: function (request) {
                    //    $.blockUI({ message: $('#dvLoader') });
                    //}
                }).done(function (data) {
                    if (data.d != "[]") {
                        var json = $.parseJSON(data.d);
                        var SZPrice = parseFloat(json[0].SZPrice).toFixed(2);
                        var EZPrice = parseFloat(json[0].EZPrice).toFixed(2);
                        var SZtax = parseFloat(json[0].StandardZone).toFixed(2);
                        var EZtax = parseFloat(json[0].ExpressZone).toFixed(2);

                        $('#hidStandardTax').val(SZtax);
                        $('#hidExpressTax').val(EZtax);
                        $('#hidStandard').val(SZPrice);
                        $('#hidExpress').val(EZPrice);
                        $('#rbStandard').attr('checked', false);
                        $('#rbExpress').attr('checked', false);

                        $('#divStandardCont').hide();
                        $('#divExpressCont').hide();

                        if (json[0].IsStandard) {
                            $('#divStandardCont').show();
                        }
                        if (json[0].IsExpress) {
                            $('#divExpressCont').show();
                        }
                        if (SZPrice == 0) {
                            $('#rbStandard').attr('disabled', 'disabled');
                            $('#rbStandard').attr('checked', false);
                            calculatefreightvalue(2);
                        }
                        else {
                            $('#rbStandard').removeAttr('disabled');
                            $('#rbStandard')[0].checked = true;
                            calculatefreightvalue(1);
                            //$('#rbStandard').attr('checked', 'checked');
                        }
                        $('#spnShippingPrice').html(SZPrice);
                        $('#spnstandard').html(SZPrice);
                        //$('#spnTaxPrice').html(SZtax);

                        if (EZPrice == 0) {
                            $('#rbExpress').attr('disabled', 'disabled');
                            $('#rbExpress').attr('checked', false);
                            calculatefreightvalue(1);
                        }
                        else {
                            $('#rbExpress').removeAttr('disabled');
                            if (!$('#rbStandard').is(':checked')) {
                                $('#rbExpress')[0].checked = true;
                                calculatefreightvalue(2);
                                //$('#rbExpress').attr('checked', 'checked');
                                $('#spnShippingPrice').html(EZPrice);
                                //$('#spnTaxPrice').html(EZtax);
                            }
                        }
                        $('#spnexpress').html(EZPrice);
                        $('#divMOVMessage').hide();
                        if ($('#rbExpress')[0].checked && json[0].EZDisallowOrder) {
                            $('#divMOVMessage').show();
                        }
                        else if (json[0].EZDisallowOrder) {
                            $('#divMOVMessage').show();
                        }

                        if (json[0].DisplayDutyMessage)
                            $('#divDutyMessage').show();
                        else
                            $('#divDutyMessage').hide();

                        $('#spnStandardText').html(json[0].StandardMethodName + " (" + json[0].SZTransitTime + " days)");
                        $('#spnExpressText').html(json[0].ExpressMethodName + " (" + json[0].EZTransitTime + " days)");

                        //updateTotalPrice();
                    }
                    else {
                        alert(data.d);
                    }
                    //$.unblockUI();
                    $('#dvLoader').hide();
                });
            }
        } catch (e) { }
        finally { }
    });

    $(document).on('click', '#aProceed', function () {
        return ValidatePaymentDetails();
    });

    

    //if (bDeliveryAddress == 'True')
    //    $('#collapseAddress').collapse('show');
    //else
    //    $('#collapseAddress').collapse('hide');

    //if ($('#txtCouponCode').val() != '') {
    //    $('#btnApplyCoupon').click();
    //}
});

$(document).ready(function () {
    //debugger;
    $('#header2_space').hide();
    $('#divShipping').hide();
    $("#header1").click(function () {
        if ($(window).width() >= 992) return;
        $('html, body').animate({ scrollTop: $("#jump1").offset().top + 1 }, 1000);
    });

    $("#header2").click(function () {
        if ($(window).width() >= 992) return;
        $('html, body').animate({ scrollTop: $("#jump2").offset().top - 30 }, 1000);
    });

    $("#header3").click(function () {
        if ($(window).width() >= 992) return;
        $('html, body').animate({ scrollTop: $("#jump3").offset().top + $("#header2_space").height() - 5 }, 1000);
    });

    $("#header4").click(function () {
        if ($(window).width() >= 992) return;
        $('html, body').animate({ scrollTop: $("#jump4").offset().top - 100 }, 1000);
    });

});
