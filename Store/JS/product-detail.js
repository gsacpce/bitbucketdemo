// JavaScript Document
	$(document).ready(function(){
	    $('#productZoomImage').zoom({ on: 'grab' });

	    $(document).on('click', '#aVideoBox', function (e) {
	        BindVideoBox();
	    });


	    function BindVideoBox() {
	        //debugger;
	        $('#myModal').find('.modal-body').html('');
	        try {
	            $.ajax({
	                type: "POST",
	                url: host + 'Products/ProductDetails.aspx/BindVideoBox',
	                contentType: "application/json",
	                cache: false,
	                async: false,
                    //success: function(response){Test(response)}
	                beforeSend: function (request) {
	                    $.blockUI({ message: '<h2><img class="newloader" src="' + host + 'images/UI/ajax-loader.gif" /> Just a moment...</h2>' });
                      
	                }
	            }).done(function (response) {
	                $('#myModal').find('.modal-body').html(response.d);
	                $('#myModal').find('.modal-title').html(VideoBoxTitle);
	                $('#myModal').find('.modal-footer').hide();
	                $('#myModal').modal('show');
	            });
	        } catch (e) { }
	        finally { $.unblockUI(); }
	    }

	    //function Test(response) {
	    //    //debugger;
	    //    $('#myModal').find('.modal-body').html(response.d);
	    //    $('#myModal').find('.modal-title').html(VideoBoxTitle);
	    //    $('#myModal').find('.modal-footer').hide();
	    //    //$('#myModal').modal('show');
	    //}


	   

	
	 $('.starRatingSml').rating({
              min: 0,
              max: 5,
              step: 1,
              size: 'sm',
              showClear: false
           }); 
    $('.starRatingBig').rating({
              min: 0,
              max: 5,
              step: 1,
              size: 'sm',
              showClear: false
    });


    var owl = $("#owl-demo");
    owl.owlCarousel({
        items: 4,
        autoPlay: false,
        navigation: true,
        singleItem: false,
        navigation: true,
        navigationText: [
"<i class='glyphicon glyphicon-chevron-left'></i>",
"<i class='glyphicon glyphicon-chevron-right'></i>"
        ],
        responsiveRefreshRate: 30,
        slideSpeed: 1000,
        paginationSpeed: 4000,
        transitionStyle: "fade",
        pagination:false
    });
    
		   
	});

	$(document).on('click', '#aPrintProductDetails', function () {
	    //var mywindow = window.open('', 'my div', 'height=1,width=1');
		var mywindow = window.open('', 'my div', 'height=1,width=1,resizable=yes'); // Changes Done by Snehal 14 02 2017
	    mywindow.document.write('<html><head><style>.noprint{display:none;}</style>');
	    /*optional stylesheet*/
	    mywindow.document.write('<link rel="stylesheet" href="' + host + 'CSS/style.css" type="text/css" />');
	    mywindow.document.write('<link rel="stylesheet" href="' + host + 'CSS/product-detail.css" type="text/css" />');
	    //mywindow.document.write('<link rel="stylesheet" href="' + host + 'CSS/bootstrap.min.css" type="text/css" />');
	    
	    mywindow.document.write('</head><body onload="window.print()">');
mywindow.document.write($('#divcontent').html()); // Changes Done by Snehal 14 02 2017
	    //mywindow.document.write($('#divmaincontent').html().replace('comparisonMain', ''));
	    mywindow.document.write('</body></html>');

	   // mywindow.document.close(); // necessary for IE >= 10
	    mywindow.focus(); // necessary for IE >= 10

	    mywindow.print();
	    //mywindow.close();
	});


	function BindRecentlyViewedProducts() {
	    //debugger;
	    $('#dvRecentlyViewContainer').html('');
	    try {
	        $.ajax({
	            type: "POST",
	            url: host + 'Products/Products_RecentlyViewedProducts.aspx',
	            data: { ProductId: ProductId },
	            //contentType: "application/json",
	            cache: false,
	            async: false,
	            //success: function (response) { Test2(response) }
	            beforeSend: function (request) {
	                $.blockUI({ message: '<h2><img class="newloader" src="' + host + 'images/UI/ajax-loader.gif" /> Just a moment...</h2>' });
	            }
	        }).done(function (response) {
	            $('#dvRecentlyViewContainer').html(response);
	        });
	    }
	    catch (e) { }
	    finally { $.unblockUI(); }
	}

	function ValidateEmailProdEnq() {
	    if (!IsValidEmailId($("#ContentPlaceHolder1_txtEmailAddress").val())) {
	        debugger;
	        $('#myWarningModal').find('#WarningMessage').html('' + strStarRating1 + '');
	        $('#myWarningModal').modal('show');
	        return false;
	    }
	}

	function BindYouMayAlsoLike() {
	    $('#dvYouMayAlsoLike').html('');
	    try {
	        $.ajax({
	            type: "POST",
	            url: host + 'Products/Products_YouMayAlsoLike.aspx',
	            data: { ProductId: ProductId },
	            cache: true,
	            async: true,
	            beforeSend: function (request) {
	                $.blockUI({ message: '<h2><img class="newloader" src="' + host + 'images/UI/ajax-loader.gif" /> Just a moment...</h2>' });
	            }
	        }).done(function (response) {
	            $('#dvYouMayAlsoLike').html(response);
	        });
	    }
	    catch (e) { }
	    finally { $.unblockUI();}
	}

	//function Test2(response) {
	//    debugger;
	//    $('#dvRecentlyViewContainer').html(response);

	//    //$('#myModal').modal('show');
//}

	