﻿$(document).ready(function () {
    //if ($('.standard_basket_row').length != 0) {
    //    $('#dvEmptyBasket').hide();
    //}
    //else {
    //    $('#dvEmptyBasket').show();
    //}

    $('.aGCPlus1').click(function (e) {

        e.preventDefault();
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());
        if (sp < 9999) {
            $(this).parent(0).parent(0).find('input').val(sp + 1);
        }
    });
    $('.aGCMinus1').click(function (e) {
        e.preventDefault();
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());
        if (sp > 1) {
            $(this).parent(0).parent(0).find('input').val(sp - 1);
        }
        else
            return false;
    });

    $('.aGCPlus').click(function (e) {
		debugger;
        e.preventDefault();
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());
        //var asp1 = parseFloat($(this).parent(0).parent(0).find('#dvUnitPrice span').html());
        //var asp2 = parseFloat($(this).parent(0).find('#dvUnitPrice span').html());
        //var asp3 = parseFloat($(this).parent(0).find('#dvUnitPrice span').val());
        //var asp4 = parseFloat($(this).parent(0).find('input').val());
        //var asp5 = parseFloat($(this).find('input').val());
        //var asp6 = parseFloat($(this).find('#dvUnitPrice span').html());
        //var asp7 = parseFloat($(this).find('#dvUnitPrice span').val());

        if (sp < 9999) {
            $(this).parent(0).parent(0).find('input').val(sp + 1);
            var Price = ((sp + 1) * $(this).parent(0).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice input').val());
            var a = $(this).attr('rel');
            var b = $('#dvListQty .qty' + a);
            $('#dvLstOuter').find($('.lstQtyOuter .qty' + a)).html(sp + 1);
            //var aPrice = ((sp + 1) * parseFloat($(this).parent(0).find('input').val()));

            $('#dvLstOuter').find($('.lstQtyOuterRight .prc' + a)).html(Price.toFixed(2));
            //$('#dvTotal span').html(Price.toFixed(2));
        }
        else
            return false;
        updateTotalPrice();
    });

    $('.aGCMinus').click(function (e) {
        e.preventDefault();
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());
        if (sp > 1) {
            $(this).parent(0).parent(0).find('input').val(sp - 1);

            var Price = ((sp - 1) * $(this).parent(0).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice input').val());
            var a = $(this).attr('rel');
            var b = $('#dvListQty .qty' + a);
            $('#dvLstOuter').find($('.lstQtyOuter .qty' + a)).html(sp - 1);
            //var aPrice = ((sp + 1) * parseFloat($(this).parent(0).find('input').val()));

            $('#dvLstOuter').find($('.lstQtyOuterRight .prc' + a)).html(Price.toFixed(2));
        }
        else
            return false;
        updateTotalPrice();
    });

    $('.quantity').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    function updateTotalPrice() {

        var totalPrice = 0;
        for (var i = 0; i < $('#dvLstOuter .lstQtyOuterRight span').length; i++) {
            totalPrice = (parseFloat(totalPrice) + parseFloat($('#dvLstOuter .lstQtyOuterRight span')[i].textContent));
        }
        $('#dvTotal span').html(totalPrice);
        $('#dvTotalPrice span').html(totalPrice);
        //if (bIsPointsEnabled == 'True')
        //    $('#dvTotal span').html(parseFloat(totalPrice.toString()));
        //else
        // $('#dvTotal span').html(totalPrice.toFixed(2));
    }

    $(document).on('click', '#aRemoveProducts', function () {
        $('#hidShoppingCartProductId').val($(this).attr('rel'));
        $('#myConfirmModal').find('#ConfirmationMessage').html(strRemoveItemsFromCart);
        $('#myConfirmModal').attr('rel', 'updatebasket');
        $('#myConfirmModal').modal('show');

    });

    $(document).on('click', '#aCheckOut, #aContinueShopping', function () {
        var errorMessage = '';
        var Quantity = 0;
        var StockStatus = 0;
        var ProductName = '';
        var minQty = 0;
        var maxQty = 0;
        for (var i = 0; i < $('div[id^=dvBasketRowContainer]').find('#txtQuantity').length; i++) {
            Quantity = parseFloat($('div[id^=dvBasketRowContainer]').find('#txtQuantity')[i].value);
            ProductName = $('div[id^=dvBasketRowContainer]').find('#dvProductName')[i].innerHTML.replace('<br>', '-');
            StockStatus = parseFloat($('div[id^=dvBasketRowContainer]').find('#hdnStockStatus')[i].value);
            minQty = parseFloat($('div[id^=dvBasketRowContainer]').find('#hdnMinimumOrderQuantity')[i].value);
            maxQty = parseFloat($('div[id^=dvBasketRowContainer]').find('#hdnMaximumOrderQuantity')[i].value);

            if (!isNaN(Quantity)) {
                if (Quantity <= 0) {
                    errorMessage += ProductName + ': ' + strQuantityNotZero;
                }
            }
            else {
                errorMessage += ProductName + ': ' + strQuantityNotZero;

            }
            if (minQty > 0 && maxQty > 0) {
                if (Quantity < minQty || Quantity > maxQty) {
                    errorMessage += ProductName + ': ' + strMinMaxQuantity.replace('{min}', minQty).replace('{max}', maxQty) + '.<br>';
                }
            }
            //Check for the inventory of the products           
            if (bBackOrderAllowed == 'False' && StockStatus > 0) {
                if (Quantity > StockStatus) {
                    errorMessage += ProductName + ': ' + strQuantityNotMoreThanStock + StockStatus;
                }
            }
        }
        if (errorMessage != '') {
            $('#myWarningModal').find('#WarningMessage').html(errorMessage);
            $('#myWarningModal').modal('show');
            return false;
        }
        $('#dvLoader').show();
        return true;
    });

     $(document).on('click', '#aGCGuestLogin', function () {
		 debugger;
        //Validate Email address and password abc
        var message = '';

        if (IsEmpty($('#txtGuestGCEmail').val())) {
            message += MsgEmailBlank + '.<br>';
        }
        else {
            if (!IsValidEmail($('#txtGuestGCEmail').val())) {
                message += MsgInvalidEmail + '.<br>';
            }
        }

        if (message != '') {
            $('#myWarningModal').find('#WarningMessage').html(message);
            $('#myWarningModal').modal('show');
            return false;
        }
        else {
            $('#dvLoader').show();
            $.ajax({
                url: host + 'login/Register.aspx/GuestRegistration',
                data: "{'strEmail': '" + $('#txtGuestGCEmail').val() + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                //$.unblockUI();
                $('#dvLoader').hide();
                if (data.d == "success") {
                    window.location.href = host + 'GiftCertificate/GiftCertificate.aspx';
                }
                else if (data.d == "GuestUser") {
                    //$('#myConfirmModal').find('#ConfirmationMessage').html('#hdnBasket_AddPreviousProducts').val();
                    //$('#myConfirmModal').attr('rel', 'addbasket');
                    //$('#myConfirmModal').modal('show');
                    return false;
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html(data.d);
                    $('#myErrorModal').modal('show');
                    return false;
                }
            });
        }
    });



    $(document).on('click', '#aGCLogin', function () {
        debugger;
        //Validate Email address and password

        var message = '';
        if (IsEmpty($('#txtGCEmail').val())) {
            message += MsgEmailBlank + '.<br>';
        }
        else {
            //if (!IsValidEmail($('#myModal #txtEmail').val())) {
            //    message += 'Please enter valid email address.<br>';
            if (isEmailValid == 'True') //Changes Done By snehal 16-09-2016 For IsLogin Custom Field
            {
                if (!IsValidEmail($('#txtGCEmail').val())) {
                    message += MsgInvalidEmail + '.<br>';
                }
            }
        }
        if (IsEmpty($('#txtGCPassword').val())) {
            message += MsgPasswordBlank + '.<br>';
        }

        if (message != '') {
            $('#myWarningModal').find('#WarningMessage').html(message);
            $('#myWarningModal').modal('show');
            return false;
        }
        else {
            $('#dvLoader').show();
            $.ajax({
                url: host + 'login/login.aspx/CheckLogin',
                data: "{'strEmail': '" + $('#txtGCEmail').val() + "','strPassword':'" + $('#txtGCPassword').val() + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                $('#dvLoader').hide();
                if (data.d == "success") {
                    window.location.href = host + 'GiftCertificate/GiftCertificate.aspx';
                }
                else {
                    //$('#myErrorModal').find('#ErrorMessage').html('Invalid email or password. Please try again.');
                    //var Basket_InvalidEmailPassword = $('#hdnBasket_InvalidEmailPassword').val();
                    //$('#myErrorModal').find('#ErrorMessage').html('' + Basket_InvalidEmailPassword + '');
                    //$('#myErrorModal').modal('show');
                    //$('#myErrorModal').find('#btnErrorOK').click(function () { window.location.href = host + 'SignIn'; });
                }
            });
        }
    });

    $(document).on('click', '#btnConfirmNo', function () {
        if ($('#myConfirmModal').attr('rel') == 'addbasket') {
            $('#dvLoader').show();
            // add previously added products into basket
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/UpdateBasketProducts',
                data: "{'action': 'n'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                //$.unblockUI();
                $('#dvLoader').hide();
                if (data.d == true) {
                    window.location.href = host + 'checkout';
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html('There is some error in updating your basket products. Please try after sometime.');
                    $('#myErrorModal').modal('show');
                }
            });
        }
    });

    $(document).on('click', '#btnConfirmYes', function () {
        if ($('#myConfirmModal').attr('rel') == 'addbasket') {
            $('#dvLoader').show();
            // add previously added products into basket
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/UpdateBasketProducts',
                data: "{'action': 'y'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                //$.unblockUI();
                $('#dvLoader').hide();
                if (data.d == true) {
                    $('#mySuccessModal').find('#SuccessMessage').html('Your previous products added to your cart successfully.');
                    $('#mySuccessModal').modal('show');
                    window.location.href = host + 'Checkout/ShoppingCart';
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html('There is some error in updating your basket products. Please try after sometime.');
                    $('#myErrorModal').modal('show');
                }
            });
        }
        else {
            $('#dvLoader').show();
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/RemoveProductsFromBasket',
                data: "{'ShoppingCartProductId': " + $('#hidShoppingCartProductId').val() + ",'intCurrencyId':" + intCurrencyId + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                //$.unblockUI();
                $('#dvLoader').hide();
                if (data.d == true) {
                    $('#dvBasketRowContainer' + $('#hidShoppingCartProductId').val()).remove();
                    $('#mySuccessModal').find('#SuccessMessage').html(strProductRemovedFromCart);
                    $('#mySuccessModal').modal('show');
                    if ($('.standard_basket_row').length != 2) {
                        $('#dvEmptyBasket').hide();
                    }
                    else {
                        $('#dvEmptyBasket').show();
                        $('.standard_basket_titles').hide();
                        $('.standard_basket_row').hide();
                    }
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html(strBasketErrorWhileRemovingProduct);
                    $('#myErrorModal').modal('show');
                }
            });
            updateTotalPrice();
        }
    });

    $(document).on('focusout', '#txtQuantity', function () {
        if (isNaN($(this).val())) {
            $(this).val(0);
        }
        $(this).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(($(this).val() * $(this).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html()).toFixed(2));
        updateTotalPrice();
    });

    //$(document).on('focusout', '.MyVal', function () {
    $(document).on('keyup', '.MyVal', function () {
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());

        if (sp < 9999) {
            $(this).parent(0).parent(0).find('input').val(sp);
            var Price = ((sp) * $(this).parent(0).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice input').val());
            var a = $(this).attr('rel');
            var b = $('#dvListQty .qty' + a);
            $('#dvLstOuter').find($('.lstQtyOuter .qty' + a)).html(sp);
            //var aPrice = ((sp + 1) * parseFloat($(this).parent(0).find('input').val()));

            $('#dvLstOuter').find($('.lstQtyOuterRight .prc' + a)).html(Price.toFixed(2));
            //$('#dvTotal span').html(Price.toFixed(2));
        }
        else
            return false;
        updateTotalPrice();
    });

    $(document).on('keyup', '.MyAmt', function () { 
        var sp = parseFloat($(this).parent(0).parent(0).parent(0).find('input').val()); 
        if (sp < 9999) {
            $(this).parent(0).parent(0).find('input').val(sp);
            var Price = ((sp) * $(this).parent(0).parent(0).parent(0).parent(0).parent(0).find('#dvUnitQty input').val());
            var a = $(this).attr('rel');
            var b = $('#dvListQty .uamt' + a);
            $('#dvLstOuter').find($('.lstQtyOuter .uamt' + a)).html(sp);
            $('#dvLstOuter').find($('.lstQtyOuterRight .prc' + a)).html(Price.toFixed(2));
        }
        else
            return false;
        updateTotalPrice();
    });
});