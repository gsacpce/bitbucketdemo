﻿$(document).ready(function () {
    if ($('.standard_basket_row').length != 0) {
        $('#dvEmptyBasket').hide();
    }
    else {
        $('#dvEmptyBasket').show();
    }

    $('.aPlus').click(function (e) {
        e.preventDefault();
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());
        if (sp < 9999) {
            $(this).parent(0).parent(0).find('input').val(sp + 1);
            var Price = ((sp + 1) * $(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html());
            if (bIsPointsEnabled == 'True')
                $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(parseFloat(Price.toString()));
            else
                $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(Price.toFixed(2));
        }
        else
            return false;
        updateTotalPrice();
    });

    $('.aMinus').click(function (e) {
        e.preventDefault();
        var sp = parseFloat($(this).parent(0).parent(0).find('input').val());
        if (sp > 1) {
            $(this).parent(0).parent(0).find('input').val(sp - 1);
            var Price = ((sp - 1) * $(this).parent(0).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html());
            if (bIsPointsEnabled == 'True')
                $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(parseFloat(Price.toString()));
            else
                $(this).parent(0).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(Price.toFixed(2));
        }
        else
            return false;
        updateTotalPrice();
    });

    $('.quantity').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    function updateTotalPrice() {
        var totalPrice = 0;
        for (var i = 0; i < $('#dvTotalPrice span').length; i++) {
            totalPrice = (parseFloat(totalPrice) + parseFloat($('#dvTotalPrice span')[i].textContent));
        }
        if (bIsPointsEnabled == 'True')
            $('#dvTotal span').html(parseFloat(totalPrice.toString()));
        else
            $('#dvTotal span').html(totalPrice.toFixed(2));
    }

    $(document).on('click', '#aRemoveProducts', function () {
        $('#hidShoppingCartProductId').val($(this).attr('rel'));
        $('#myConfirmModal').find('#ConfirmationMessage').html(strRemoveItemsFromCart);
        $('#myConfirmModal').attr('rel', 'updatebasket');
        $('#myConfirmModal').modal('show');

    });

    $(document).on('click', '#aCheckOut, #aContinueShopping', function () {
        var errorMessage = '';
        var Quantity = 0;
        var StockStatus = 0;
        var ProductName = '';
        var minQty = 0;
        var maxQty = 0;
        for (var i = 0; i < $('div[id^=dvBasketRowContainer]').find('#txtQuantity').length; i++) {
            Quantity = parseFloat($('div[id^=dvBasketRowContainer]').find('#txtQuantity')[i].value);
            ProductName = $('div[id^=dvBasketRowContainer]').find('#dvProductName')[i].innerHTML.replace('<br>', '-');
            StockStatus = parseFloat($('div[id^=dvBasketRowContainer]').find('#hdnStockStatus')[i].value);
            minQty = parseFloat($('div[id^=dvBasketRowContainer]').find('#hdnMinimumOrderQuantity')[i].value);
            maxQty = parseFloat($('div[id^=dvBasketRowContainer]').find('#hdnMaximumOrderQuantity')[i].value);

            if (!isNaN(Quantity)) {
                if (Quantity <= 0) {
                    errorMessage += ProductName + ': ' + strQuantityNotZero;
                }
            }
            else {
                errorMessage += ProductName + ': ' + strQuantityNotZero;

            }
            if (minQty > 0 && maxQty > 0) {
                if (Quantity < minQty || Quantity > maxQty) {
                    errorMessage += ProductName + ': ' + strMinMaxQuantity.replace('{min}', minQty).replace('{max}', maxQty) + '.<br>';
                }
            }
            //Check for the inventory of the products           
            if (bBackOrderAllowed == 'False' && StockStatus > 0) {
                if (Quantity > StockStatus) {
                    errorMessage += ProductName + ': ' + strQuantityNotMoreThanStock + StockStatus;
                }
            }
        }
        if (errorMessage != '') {
            $('#myWarningModal').find('#WarningMessage').html(errorMessage);
            $('#myWarningModal').modal('show');
            return false;
        }
        $('#dvLoader').show();
        return true;
    });

    $(document).on('click', '#myModal #aLogin', function () {
        //Validate Email address and password
        var message = '';
        if (IsEmpty($('#myModal #txtEmail').val())) {
            message += 'Please enter email address.<br>';
        }
        else {
            if (!IsValidEmail($('#myModal #txtEmail').val())) {
                message += 'Please enter valid email address.<br>';
            }
        }
        if (IsEmpty($('#myModal #txtPassword').val())) {
            message += 'Please enter password.<br>';
        }

        if (message != '') {
            $('#myWarningModal').find('#WarningMessage').html(message);
            $('#myWarningModal').modal('show');
            return false;
        }
        else {
            $('#dvLoader').show();
            $.ajax({
                url: host + 'login/login.aspx/CheckLogin',
                data: "{'strEmail': '" + $('#myModal #txtEmail').val() + "','strPassword':'" + $('#myModal #txtPassword').val() + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                $('#dvLoader').hide();
                if (data.d == "success") {
                    window.location.href = host + 'checkout';
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html('Invalid email or password. Please try again.');
                    $('#myErrorModal').modal('show');
                    $('#myErrorModal').find('#btnErrorOK').click(function () { window.location.href = host + 'SignIn'; });
                }
            });
        }
    });

    $(document).on('click', '#myModal #aGuestCheckOut', function () {
        //Validate Email address and password
        var message = '';
        if (IsEmpty($('#myModal #txtGuestEmail').val())) {
            message += 'Please enter email address.<br>';
        }
        else {
            if (!IsValidEmail($('#myModal #txtGuestEmail').val())) {
                message += 'Please enter valid email address.<br>';
            }
        }

        if (message != '') {
            $('#myWarningModal').find('#WarningMessage').html(message);
            $('#myWarningModal').modal('show');
            return false;
        }
        else {
            $('#dvLoader').show();
            $.ajax({
                url: host + 'login/Register.aspx/GuestRegistration',
                data: "{'strEmail': '" + $('#myModal #txtGuestEmail').val() + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                //$.unblockUI();
                $('#dvLoader').hide();
                if (data.d == "success") {
                    window.location.href = host + 'checkout';
                }
                else if (data.d == "GuestUser") {
                    $('#myConfirmModal').find('#ConfirmationMessage').html('Do you want to add previously added products into your basket?');
                    $('#myConfirmModal').attr('rel', 'addbasket');
                    $('#myConfirmModal').modal('show');
                    return false;
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html(data.d);
                    $('#myErrorModal').modal('show');
                    return false;
                }
            });
        }
    });

    $(document).on('click', '#btnConfirmNo', function () {
        if ($('#myConfirmModal').attr('rel') == 'addbasket') {
            $('#dvLoader').show();
            // add previously added products into basket
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/UpdateBasketProducts',
                data: "{'action': 'n'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                //$.unblockUI();
                $('#dvLoader').hide();
                if (data.d == true) {
                    window.location.href = host + 'checkout';
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html('There is some error in updating your basket products. Please try after sometime.');
                    $('#myErrorModal').modal('show');
                }
            });
        }
    });

    $(document).on('click', '#btnConfirmYes', function () {
        if ($('#myConfirmModal').attr('rel') == 'addbasket') {
            $('#dvLoader').show();
            // add previously added products into basket
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/UpdateBasketProducts',
                data: "{'action': 'y'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                //$.unblockUI();
                $('#dvLoader').hide();
                if (data.d == true) {
                    $('#mySuccessModal').find('#SuccessMessage').html('Your previous products added to your cart successfully.');
                    $('#mySuccessModal').modal('show');
                    window.location.href = host + 'Checkout/ShoppingCart';
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html('There is some error in updating your basket products. Please try after sometime.');
                    $('#myErrorModal').modal('show');
                }
            });
        }
        else {
            $('#dvLoader').show();
            $.ajax({
                url: host + 'shoppingcart/basket.aspx/RemoveProductsFromBasket',
                data: "{'ShoppingCartProductId': " + $('#hidShoppingCartProductId').val() + ",'intCurrencyId':" + intCurrencyId + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                //$.unblockUI();
                $('#dvLoader').hide();
                if (data.d == true) {
                    $('#dvBasketRowContainer' + $('#hidShoppingCartProductId').val()).remove();
                    $('#mySuccessModal').find('#SuccessMessage').html(strProductRemovedFromCart);
                    $('#mySuccessModal').modal('show');
                    if ($('.standard_basket_row').length != 2) {
                        $('#dvEmptyBasket').hide();
                    }
                    else {
                        $('#dvEmptyBasket').show();
                        $('.standard_basket_titles').hide();
                        $('.standard_basket_row').hide();
                    }
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html(strBasketErrorWhileRemovingProduct);
                    $('#myErrorModal').modal('show');
                }
            });
            updateTotalPrice();
        }
    });

    $(document).on('focusout', '#txtQuantity', function () {
        if (isNaN($(this).val())) {
            $(this).val(0);
        }
        $(this).parent(0).parent(0).parent(0).find('#dvTotalPrice span').html(($(this).val() * $(this).parent(0).parent(0).parent(0).find('#dvUnitPrice span').html()).toFixed(2));
        updateTotalPrice();
    });
});
function isNumeric(keyCode) {
    if (keyCode == 16 || keyCode == 18)
        isShift = true;

    return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 190 || keyCode == 9 || keyCode == 14 || keyCode == 15 || keyCode == 37 || keyCode == 39 || (keyCode >= 96 && keyCode <= 105)) && isShift == false);
}
var isShift = false;
function keyUP(keyCode) {
    if (keyCode == 16 || keyCode == 18 || keyCode == 9)
        isShift = false;
}