var colCount = 0;
var colWidth = 272;
var margin = 10;
var spaceLeft = 0;
var windowWidth = 0;
var blocks = [];
var wwidth = 0;

$(function () {
    //alert("Hello");
	$(window).resize(setupBlocks);
});

function setupBlocks() {
    windowWidth = $('#dvDashboard').width();
    wwidth = $(window).width();
    blocks = [];

	// Calculate the margin so the blocks are evenly spaced within the window
	if (wwidth > 1199) colCount = Math.floor((windowWidth / (colWidth + margin * 2)) + 1);
	else colCount = Math.floor(windowWidth / (colWidth + margin * 2));
	//spaceLeft = (windowWidth - ((colWidth * colCount) + (margin * (colCount - 1)))) / 2;
	spaceLeft = -10;
	//console.log(spaceLeft);
	
	for(var i=0;i<colCount;i++){
		blocks.push(margin);
	}
	positionBlocks();
}

function positionBlocks() {
	$('.block').each(function(i){
		var min = Array.min(blocks);
		var index = $.inArray(min, blocks);
		var leftPos = margin + (index * (colWidth + margin));
		var total = leftPos + colWidth;
	    /* Added by Rujuta 
        if (total > 1200) {
		    console.log(leftPos);
		    leftPos = 0;
		    console.log(leftPos);
		}*/
		$(this).css({
			'left':(leftPos+spaceLeft)+'px',
			'top':min+'px'
		});
		blocks[index] = min+$(this).outerHeight()+margin;
	});	
}

// Function to get the Min value in Array
Array.min = function(array) {
    return Math.min.apply(Math, array);
};