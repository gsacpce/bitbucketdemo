﻿window.onbeforeunload = fnShowLoader;


$(document).ready(function () {
	console.log("test in");
    // With JQuery   
    //Show filter in mobile view
    $('body').on('click', '#aMobileFilter', function (e) {
        $('#myModal').find('.modal-body').html("<div id='MODAL_FILTER_POSITION'></div>");
        $('#STANDARD_FILTER').insertAfter('#myModal #MODAL_FILTER_POSITION');
        $('#myModal').find('.modal-title').hide();
        $('#myModal').find('.modal-footer').hide();
        $('#myModal').modal('show');
    });
    //Hide filter in mobile view
    $('body').on('click', '#myModal .close', function (e) {
        $('#STANDARD_FILTER').insertAfter('#DESKTOP_FILTER_POSITION');
        $('#myModal').modal('hide');
    })


    $('body').on('click', '#alstView', function (e) {
        //$('.ProductListingContainer ul li').removeClass('listView');
        $('.prod_list_text_desc').show();
        //$('#alstView').removeClass('listViewcls');
        //$('#agrdView').removeClass('gridViewcls');
        blnDefaultListView = 'False';

        $('#dvProductData').animate({ opacity: 0 }, 1000, function () {
            $('#dvProductData').removeClass('grid');
            $('#dvProductData').addClass('list');
            $('#dvProductData li').removeClass('col-lg-4 col-md-4 col-sm-6');
            $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
            $('#agrdView').removeClass('hidden');
            $('#alstView').addClass('hidden');
            //debugger;
            $('#hdcurview').val("list");
            //alert($('#hdcurview').val());
            //$('#<% =hdcurview.ClientID %>').val('list');
        });
    });



    $('body').on('click', '#agrdView', function (e) {
        //$('.ProductListingContainer ul li').addClass('listView');
        $('.prod_list_text_desc').hide();
        //$('#alstView').addClass('listViewcls');
        //$('#agrdView').addClass('gridViewcls');
        blnDefaultListView = 'True';

        $('#dvProductData').animate({ opacity: 0 }, 1000, function () {
            $('#dvProductData').removeClass('list');
            $('#dvProductData').addClass('grid');
            $('#dvProductData li').addClass('col-lg-4 col-md-4 col-sm-6');
            $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
            $('#alstView').removeClass('hidden');
            $('#agrdView').addClass('hidden');
            //debugger;
            $('#hdcurview').val("grid");
            //alert($('#hdcurview').val());
            //$('#<% =hdcurview.ClientID %>').val('grid');
        });
    });
    setTimeout(function () { $(window).resize(); }, 2000);


});

//Added by Sripal to maintain the view of the productListing on Postback(lazy loading / Postback)
function ShowProductListingPageView() {
    //alert($('#hdcurview').val());
    if ($('#hdcurview').val() == "grid") {
        $('#dvProductData').removeClass('list');
        $('#dvProductData').addClass('grid');
        $('#dvProductData li').addClass('col-lg-4 col-md-4 col-sm-6');
        $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
        $('#alstView').removeClass('hidden');
        $('#agrdView').addClass('hidden');
        $('#hdcurview').val("grid");
        //alert($('#hdcurview').val());
        //$('#dvProductData').animate({ opacity: 0 }, 1000, function () {
        //    //$('#<% =hdcurview.ClientID %>').val('grid');
        //});
    }
    else {
        $('#dvProductData').removeClass('grid');
        $('#dvProductData').addClass('list');
        $('#dvProductData li').removeClass('col-lg-4 col-md-4 col-sm-6');
        $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
        $('#agrdView').removeClass('hidden');
        $('#alstView').addClass('hidden');
        $('#hdcurview').val("list");
        //alert($('#hdcurview').val());
        //$('#dvProductData').animate({ opacity: 0 }, 1000, function () {
        //    //$('#<% =hdcurview.ClientID %>').val('list');
        //});
    }

}


var IsBindFilterData = true;
var status = false;
$('#hidPageNo').val("1");
$('#hidColorIds').val("");
$('#hidSectionIds').val("")
$('#hidCustomFilter1').val("");
$('#hidCustomFilter2').val("");
$('#hidCustomFilter3').val("");
$('#hidCustomFilter4').val("");
$('#hidCustomFilter5').val("");
$('#hidCompareProducts').val("");
if (IsGoogleMixIt == 'True') {
    $('#hidPageNo').val("0");
    $(function () {
        $('#dvProductData').mixitup({
            targetSelector: '.mix-target',
            filterSelector: '.filter-btn',
            //sortSelector: '.sort',
            buttonEvent: 'click',
            effects: ['scale'],
            listEffects: null,
            easing: 'snap',
            layoutMode: 'grid',
            targetDisplayGrid: 'inline-block',
            targetDisplayList: 'block',
            gridClass: '',
            listClass: '',
            transitionSpeed: 1500,
            showOnLoad: 'all',
            sortOnLoad: ['data-cat', 'desc'],
            multiFilter: false,
            filterLogic: 'or',
            resizeContainer: true,
            minHeight: 0,
            failClass: 'fail',
            perspectiveDistance: '3000',
            perspectiveOrigin: '50% 50%',
            animateGridList: true,
            onMixLoad: null,
            onMixStart: null,
            onMixEnd: null
        });
    });
}
BindProducts();

function JumpToPage() {
    var jumpTo = $('#currentpage').val();
    if (jumpTo != '' && !isNaN(jumpTo)) {
        if (jumpTo < 1) {
            jumpTo = 1;
        }
        if (jumpTo > intTotalPages) {
            jumpTo = intTotalPages;
        }
        $('#hidPageNo').val(jumpTo);
        BindProducts();
    }
}

function ChangeSorting(ddlSortOrder) {
    status = true;
    SortName = ddlSortOrder.options[ddlSortOrder.selectedIndex].value;
    $('#hidPageNo').val(1);
    if (IsInfiniteScroll == 'False') {
        $('#dvPagination').show();
        $('#dvPageNo').show();
        $('#records').show();
    }

    BindProducts();

}



$(document).on('click', '#aQuickView', function (e) {
    var id = $(this).attr("productid");
    $('#myModal').find('.modal-body').html('');
    $('#dvLoader').show();
    try {
        $.ajax({
            type: "POST",
            url: host + 'Products/QuickView_ajax.aspx',
            data: {
                productid: id,
                languageid: LanguageId,
                currencyid: CurrencyId,
                currencysymbol: CurrencySymbol
            },
            cache: false,
            async: true
        }).done(function (response) {
            $('#dvLoader').hide();
            $('#myModal').find('.modal-dialog').addClass('modal-lg');
            $('#myModal').find('.modal-body').html(response);
            $('#myModal').find('.modal-title').html($('#myModal').find('.modal-body').find('#hidProductName').val());
            $('#myModal').find('.modal-footer').hide();
            $('#myModal').modal('show');
        });
    } catch (e) { }
    finally { $('#dvLoader').hide(); }
});

function BindProducts() {
    try {
        //debugger;
        $('#productsRandom').hide();
        //Hide filter popup in mobile view
        if ($('#myModal').css('display') != 'none') {
            $('#STANDARD_FILTER').insertAfter('#DESKTOP_FILTER_POSITION');
            $('#myModal').modal('hide');
        }

        var asyncprop = true;
        if (IsGoogleMixIt == 'True') {
            asyncprop = false;
        }
        var PageNo = $('#hidPageNo').val();
        //var loadingMessage = $('#dvLoader').html();
        $('#dvLoader').show();
        $.ajax({
            type: "POST",
            url: host + 'Products/Products_ajax.aspx',
            data: {
                categoryname: CategoryName,
                subcategoryname: SubCategoryName,
                subsubcategoryname: SubSubCategoryName,
                sectionname: SectionName,
                pageno: PageNo,
                pagesize: PageSize, sortname: SortName,
                isfinitescroll: IsInfiniteScroll,
                isgooglemixit: IsGoogleMixIt,
                minprice: MiniumPrice,
                maxprice: MaximumPrice,
                languageid: LanguageId,
                currencyid: CurrencyId,
                currencysymbol: CurrencySymbol,
                colorids: $('#hidColorIds').val(),
                sectionids: $('#hidSectionIds').val(),
                customfilter1: $('#hidCustomFilter1').val(),
                customfilter2: $('#hidCustomFilter2').val(),
                customfilter3: $('#hidCustomFilter3').val(),
                customfilter4: $('#hidCustomFilter4').val(),
                customfilter5: $('#hidCustomFilter5').val(),
                productids: $('#hidCompareProducts').val(),
                isenablefilters: showfilters,
                searchproductids: SearchProductIds
            },
            //beforeSend: function (request) {
            //    $.blockUI({ message: $('#dvLoader') });
            //},
            cache: false,
            async: asyncprop,
        }).done(function (response) {
            $('#dvSearchMessage').show();
            if (IsInfiniteScroll == 'True') {
                $('#dvPagination').hide();
                $('#dvPageNo').hide();
                $('#aViewAll').hide();
                if (response == "norecords") {
                    if ($('#hidPageNo').val() == 1) {
                        $('#SortingDataContainer').hide();
                        $('#compareBtn').hide();
                        if (SearchProductIds != '') {
                            //debugger;
                            $('#dvProductData').html(NoResultFoundMessage + ' "' + SearchKeyword + '"<br>' + NoResultFoundMessage1 + '<br>');
                            $('#dvSearchMessage').hide();
                            $('#productsRandom').show();
                        }
                        else {
                            //debugger;
                            $('#dvProductData').html("<span>No records found.</span>");
                            $('#productsRandom').hide();
                        }
                        $('#dvFilterContainer').hide();
                        IsBindFilterData = false;
                    }
                    status = false;
                }
                else {
                    if ($('#hidPageNo').val() == 1) {
                        $('#dvProductData').html(response);
                        $('#totalitems').html($('#dvProductData').find('.totalrecords').val());
                        $('#ddlSorting').val('');
                        $('#ddlSorting').val(SortName);
						// if (SearchProductIds != '')
							
                        if (SearchProductIds != ''&& SearchProductIds != '0') { // vikram 
                            //debugger;
                            $('#dvSearchMessage').html(ResultsFoundMessage.replace("{keyword}", ' "' + SearchKeyword + '"').replace("{count}", $('#dvProductData').find('.totalrecords').val()));
                            $('#productsRandom').hide();
                        }
						// vikram start
						else
                        {
                            $('#dvSearchMessage').html(NoResultFoundMessage1);
							// $('#SortingDataContainer').hide();
                        }
						// vikram end
                    }
                    else {
                        $('#dvProductData').append(response);
                    }
                    if (blnDefaultListView == 'True') {
                        $('.ProductListingContainer ul li').addClass('listView');
                        $('.productDescription').show();
                        $('#alstView').addClass('listViewcls');
                        $('#agrdView').addClass('gridViewcls');
                    }
                    status = true;
                }
                if ($('#hidPageNo').val() == 1) {
                    $(window).scroll(function () {
                        var footerHeight = ($('#footer').height() / 2) + 900;
                        if ($(window).scrollTop() + footerHeight >= ($(document).height()) - ($(window).height())) {
                            if (status == 'true' || status == true) {
                                status = false;
                                $('#hidPageNo').val(parseInt($('#hidPageNo').val()) + 1);
                                BindProducts();
                            }
                        }
                    });
                }
            }
            else {
                // if infinte scroll feature is disabled
                if (response != "norecords") {
                    $('#dvProductData').html(response);
                    if ($('#hidPageNo').val() == 1) {
                        $('#ddlSorting').val('');
                        $('#ddlSorting').val(SortName);
                    }
                    if (SearchProductIds != '' && ($('#hidPageNo').val() == 1 || $('#hidPageNo').val() == 0))
                        $('#dvSearchMessage').html(ResultsFoundMessage.replace("{keyword}", ' "' + SearchKeyword + '"').replace("{count}", $('#dvProductData').find('.totalrecords').val()));
                    if (blnDefaultListView == 'True') {
                        $('.ProductListingContainer ul li').addClass('listView');
                        $('.productDescription').show();
                        $('#alstView').addClass('listViewcls');
                        $('#agrdView').addClass('gridViewcls');
                    }

                    Paging($('#hidPageNo').val(), PageSize, $('#dvProductData').find('.totalrecords').val());
                }
                else {
                    $('#SortingDataContainer').hide();
                    $('#compareBtn').hide();
                    if (SearchProductIds != '') {
                        //debugger;
                        $('#dvProductData').html(NoResultFoundMessage + ' "' + SearchKeyword + '" <br>' + NoResultFoundMessage1 + '<br>');
                        $('#dvSearchMessage').hide();
                        $('#productsRandom').show();
                    }
                    else {
                        //debugger;
                        $('#dvProductData').html("<span>No records found.</span>");
                        $('#productsRandom').hide();
                    }
                    $('#dvFilterContainer').hide();
                    IsBindFilterData = false;
                }
            }
            if (IsBindFilterData) {
                if ((showfilters == 'True') || (IsGoogleMixIt == 'True')) {
                    BindFilterData();
                    if (IsGoogleMixIt == 'True') {
                        $('.PaginationContainer ').removeClass('col-md-5');
                        $('.PaginationContainer ').addClass('col-md-10');
                        $('#aMobileFilter').hide();
                    }
                }
                else {
                    $('#dvLoader').hide();
                    //$.unblockUI();
                }
            }
            else {
                $('#dvLoader').hide();
                //$.unblockUI();
            }
            if ($('#hdcurview').val() == "") { }
            else {
                ShowProductListingPageView();//added by sripal
            }
        });
    } catch (e) { }
    finally { }
}

function Paging(PageNo, PageSize, TotalRecords) {
    try {

        intTotalPages = TotalRecords;
        $('#totalitems').html(TotalRecords);
        if (PageNo == 0) {
            $('#dvPagination').hide();
            $('#dvPageNo').hide();
            $('#records').hide();
            $('#aViewAll').hide();
        }
        else {
            var intFromRecord = ((parseInt(PageNo) - 1) * PageSize) + 1;
            var intToRecord = parseInt(PageNo) * (PageSize);
            //var intTotalPages = 0;

            if (intToRecord > TotalRecords) {
                intToRecord = TotalRecords;
            }
            intTotalPages = Math.ceil(TotalRecords / PageSize);

            $('#records').html(intFromRecord + " - " + intToRecord + " of " + TotalRecords);

            $('#currentpage').val(PageNo);
            $('#spnTotalPageCount').html(intTotalPages);
            if (intTotalPages == 1) {
                $('#aViewAll').hide();
                $('#dvPagination').hide();
                $('#dvPageNo').hide();
                $('#records').hide();
            }
            else {
                $('#aViewAll').show();
                $('#dvPagination').show();
                $('#dvPageNo').show();
                $('#records').show();
            }

            if (parseInt(PageNo) == 1) {
                $('#aFirst').addClass("disabled");
                $('#aFirst').removeClass("disabled");
            }
            else {
                $('#aFirst').attr("onclick", "BindPage(1)");
                $('#aPrev').removeAttr("onclick");
            }
            if (parseInt(PageNo) > 1) {
                $('#aPrev').removeClass("disabled");
                $('#aPrev').attr("onclick", "BindPage(" + (parseInt(PageNo) - 1) + ")");
            }
            else {
                $('#aPrev').addClass("disabled");
                $('#aPrev').removeAttr("onclick");
            }
            if (parseInt(PageNo) == intTotalPages) {
                $('#aNext').addClass("disabled");
                $('#aNext').removeAttr("onclick");
            }
            else {
                $('#aNext').removeClass("disabled");
                $('#aNext').attr("onclick", "BindPage(" + (parseInt(PageNo) + 1) + ")");
            }
            if (parseInt(PageNo) == intTotalPages) {
                $('#aLast').addClass("disabled");
                $('#aLast').removeAttr("onclick");
            }
            else {
                $('#aLast').removeClass("disabled");
                $('#aLast').attr("onclick", "BindPage(" + intTotalPages + ")");
            }
        }
    } catch (e) {

    }
}

function BindPage(NewPageNo) {
    $('#hidPageNo').val(NewPageNo);
    BindProducts();
}

function ViewAllProducts() {
    $('#hidPageNo').val(0);
    BindProducts();
}

function BindFilterData() {
    try {
        var pageNo = $('#hidPageNo').val();
        if (pageNo == "1" || IsGoogleMixIt == 'True') {
            $.ajax({
                type: "POST",
                url: host + 'Products/Filter_ajax.aspx',
                data: {
                    categoryname: CategoryName,
                    subcategoryname: SubCategoryName,
                    subsubcategoryname: SubSubCategoryName,
                    sectionname: SectionName,
                    minprice: MiniumPrice,
                    maxprice: MaximumPrice,
                    isgooglemixit: IsGoogleMixIt,
                    languageid: LanguageId,
                    currencyid: CurrencyId,
                    currencysymbol: CurrencySymbol,
                    colorids: $('#hidColorIds').val(),
                    sectionids: $('#hidSectionIds').val(),
                    customfilter1: $('#hidCustomFilter1').val(),
                    customfilter2: $('#hidCustomFilter2').val(),
                    customfilter3: $('#hidCustomFilter3').val(),
                    customfilter4: $('#hidCustomFilter4').val(),
                    customfilter5: $('#hidCustomFilter5').val(),
                    searchproductids: SearchProductIds
                },
                cache: false,
                async: false
            }).done(function (response) {
                if (IsGoogleMixIt == 'True') {
                    $('#dvSectionIconContainer').html(response);
                }
                else {
                    $('#dvFilterData').html(response);
                }
                if ($(window).width() < 768) {
                    $('.listingFilterableMenu a.btn').addClass('collapsed');
                    $('.listingFilterableMenu .listingFilterDetailCont').removeClass('in');
                }
                if ($("#priceRange").length > 0) {
                    $('#priceRange').slider().on('slideStop', function (ev) {
                        var newVal = $('#priceRange').data('slider').getValue();
                        if (newVal.length > 0) {
                            MiniumPrice = newVal[0];
                            MaximumPrice = newVal[1];
                        }
                        else {
                            MiniumPrice = 0;
                            MaximumPrice = 0;
                        }
                        $('#hidPageNo').val("1");
                        BindProducts();
                    });
                }

            });
        }
    } catch (e) { }
    finally {
        $('#dvLoader').hide();
        //$.unblockUI(); 
    }
}

$(document).on('click', '#btnCompare', function (e) {
    $('#myModal').find('.modal-body').html('');
    var ProductIds = $('#hidCompareProducts').val();
    if (ProductIds != "") {
        //ProductIds = ProductIds.replace(/\'/g, "");
        ProductIds = ProductIds.substring(0, ProductIds.length - 1);
        var products = ProductIds.split(',');
        if (products.length < 2) {
            $('#myWarningModal').find('#WarningMessage').html(MinimumProductMsg);
            $('#myWarningModal').modal('show');
            //alert(MinimumProductMsg);
            return false;
        }
        else if (products.length > 10) {
            $('#myWarningModal').find('#WarningMessage').html(MaximumProductMsg);
            $('#myWarningModal').modal('show');
            //alert(MaximumProductMsg);
            return false;
        }
    }
    else {
        $('#myWarningModal').find('#WarningMessage').html(MinimumProductMsg);
        $('#myWarningModal').modal('show');
        //alert(MinimumProductMsg);
        return false;
    }
    $('#dvLoader').show();
    try {
        $.ajax({
            type: "POST",
            url: host + 'Products/CompareProduct_ajax.aspx',
            data: {
                productids: ProductIds,
                languageid: LanguageId,
                currencyid: CurrencyId,
                currencysymbol: CurrencySymbol
            },
            cache: false,
            async: true
        }).done(function (response) {
            $('#dvLoader').hide();
            $('#myModal').find('.modal-dialog').addClass('modal-lg');
            $('#myModal').find('.modal-body').html(response);
            $('#myModal').find('.modal-title').html(ProductComparison);
            $('#myModal').find('.modal-footer').hide();
            $('#myModal').modal('show');
        });
    } catch (e) { }
    finally { }
});

$(document).on('change', '#chkCompare', function () {
    var ProductId = $(this).attr('data-value');
    if ($(this)[0].checked) {
        var ProductIds = $('#hidCompareProducts').val();
        if (ProductIds != "") {
            ProductIds = ProductIds.replace(/\'/g, "");
            ProductIds = ProductIds.substring(0, ProductIds.length - 1);
            var products = ProductIds.split(',');
            if (products.length >= 10) {
                $('#myWarningModal').find('#WarningMessage').html(MaximumProductMsg);
                $('#myWarningModal').modal('show');
                //alert(MaximumProductMsg);
                $(this)[0].checked = false;
                return false;
            }
        }
        if ($('#hidCompareProducts').val().indexOf("," + ProductId + ",") == -1) {
            $('#hidCompareProducts').val($('#hidCompareProducts').val() + "'" + ProductId + "',");
        }
    }
    else {
        if ($('#hidCompareProducts').val().indexOf("'" + ProductId + "',") != -1) {
            $('#hidCompareProducts').val($('#hidCompareProducts').val().replace(("'" + ProductId + "',"), ''));
        }
    }
});

$(document).on('change', '#chkColorName', function () {
    var colorid = $(this)[0].value;
    if ($(this)[0].checked) {
        if ($('#hidColorIds').val().indexOf("," + colorid + ",") == -1) {
            $('#hidColorIds').val($('#hidColorIds').val() + "'" + colorid + "',");
        }
    }
    else {
        if ($('#hidColorIds').val().indexOf("'" + colorid + "',") != -1) {
            $('#hidColorIds').val($('#hidColorIds').val().replace(("'" + colorid + "',"), ''));
        }
    }
    $('#hidPageNo').val("1");
    BindProducts();
});

$(document).on('change', '#chkSectionName', function () {
    var sectionid = $(this)[0].value;
    if ($(this)[0].checked) {

        if ($('#hidSectionIds').val().indexOf("," + sectionid + ",") == -1) {
            $('#hidSectionIds').val($('#hidSectionIds').val() + "'" + sectionid + "',");
        }
    }
    else {
        if ($('#hidSectionIds').val().indexOf("'" + sectionid + "',") != -1) {
            $('#hidSectionIds').val($('#hidSectionIds').val().replace(("'" + sectionid + "',"), ''));
        }
    }
    $('#hidPageNo').val("1");
    BindProducts();
});

$(document).on('change', '#chkCustomFilter1', function () {
    var name = $(this)[0].value;
    if ($(this)[0].checked) {
        if ($('#hidCustomFilter1').val().indexOf("," + name + ",") == -1) {
            $('#hidCustomFilter1').val($('#hidCustomFilter1').val() + "'" + name + "'|");
        }
    }
    else {
        if ($('#hidCustomFilter1').val().indexOf("'" + name + "'|") != -1) {
            $('#hidCustomFilter1').val($('#hidCustomFilter1').val().replace(("'" + name + "'|"), ''));
        }
    }
    $('#hidPageNo').val("1");
    BindProducts();
});

$(document).on('change', '#chkCustomFilter2', function () {
    var name = $(this)[0].value;
    if ($(this)[0].checked) {
        if ($('#hidCustomFilter2').val().indexOf("," + name + ",") == -1) {
            $('#hidCustomFilter2').val($('#hidCustomFilter2').val() + "'" + name + "'|");
        }
    }
    else {
        if ($('#hidCustomFilter2').val().indexOf("'" + name + "'|") != -1) {
            $('#hidCustomFilter2').val($('#hidCustomFilter2').val().replace(("'" + name + "'|"), ''));
        }
    }
    $('#hidPageNo').val("1");
    BindProducts();
});

$(document).on('change', '#chkCustomFilter3', function () {
    var name = $(this)[0].value;
    if ($(this)[0].checked) {
        if ($('#hidCustomFilter3').val().indexOf("," + name + ",") == -1) {
            $('#hidCustomFilter3').val($('#hidCustomFilter3').val() + "'" + name + "'|");
        }
    }
    else {
        if ($('#hidCustomFilter3').val().indexOf("'" + name + "'|") != -1) {
            $('#hidCustomFilter3').val($('#hidCustomFilter3').val().replace(("'" + name + "'|"), ''));
        }
    }
    $('#hidPageNo').val("1");
    BindProducts();
});

$(document).on('change', '#chkCustomFilter4', function () {
    var name = $(this)[0].value;
    if ($(this)[0].checked) {
        if ($('#hidCustomFilter4').val().indexOf("," + name + ",") == -1) {
            $('#hidCustomFilter4').val($('#hidCustomFilter4').val() + "'" + name + "'|");
        }
    }
    else {
        if ($('#hidCustomFilter4').val().indexOf("'" + name + "'|") != -1) {
            $('#hidCustomFilter4').val($('#hidCustomFilter4').val().replace(("'" + name + "'|"), ''));
        }
    }
    $('#hidPageNo').val("1");
    BindProducts();
});

$(document).on('change', '#chkCustomFilter5', function () {
    var name = $(this)[0].value;
    if ($(this)[0].checked) {
        if ($('#hidCustomFilter5').val().indexOf("," + name + ",") == -1) {
            $('#hidCustomFilter5').val($('#hidCustomFilter5').val() + "'" + name + "'|");
        }
    }
    else {
        if ($('#hidCustomFilter5').val().indexOf("'" + name + "'|") != -1) {
            $('#hidCustomFilter5').val($('#hidCustomFilter5').val().replace(("'" + name + "'|"), ''));
        }
    }
    $('#hidPageNo').val("1");
    BindProducts();
});

$(document).on('click', '#resetFilters', function () {
    $('#hidPageNo').val("1");
    $('#hidColorIds').val("");
    $('#hidSectionIds').val("");
    $('#hidCustomFilter1').val("");
    $('#hidCustomFilter2').val("");
    $('#hidCustomFilter3').val("");
    $('#hidCustomFilter4').val("");
    $('#hidCustomFilter5').val("");

    SortName = defaultSortName;
    MiniumPrice = 0;
    MaximumPrice = 0;
    BindProducts();
});

$(document).on('click', '#aRemoveProduct', function () {
    var ProductId = $(this).attr('rel');
    if ($('#hidCompareProducts').val().indexOf("'" + ProductId + "',") != -1) {
        $('#hidCompareProducts').val($('#hidCompareProducts').val().replace(("'" + ProductId + "',"), ''));
        if ($('.compareproduct' + ProductId) != undefined)
            $('.compareproduct' + ProductId).attr('checked', false);
    }

    var ProductIds = $('#hidCompareProducts').val();
    $('#dvLoader').show();
    try {
        $.ajax({
            type: "POST",
            url: host + 'Products/CompareProduct_ajax.aspx',
            data: {
                productids: ProductIds,
                languageid: LanguageId,
                currencyid: CurrencyId,
                currencysymbol: CurrencySymbol
            },
            cache: false,
            async: true
            //beforeSend: function (request) {
            //    $.blockUI({ message: $('#dvLoader') });
            //}
        }).done(function (response) {
            $('#myModal').find('.modal-body').html(response);
            $('#myModal').find('.modal-title').html(ProductComparison);
            $('#myModal').find('.modal-footer').hide();
            $('#myModal').modal('show');
        });
    } catch (e) { }
    finally { $('#dvLoader').hide(); }
});

$(document).on('click', '#aPrintCompareProduct', function () {
    var mywindow = window.open('', 'my div', 'height=1,width=1');
    mywindow.document.write('<html><head><style>.noprint{display:none;}@media print { a[href]:after { content: none !important; }}</style>');
    /*optional stylesheet*/
    mywindow.document.write('<link rel="stylesheet" href="' + host + 'CSS/bootstrap-theme.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="' + host + 'CSS/bootstrap.min.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="' + host + 'CSS/modified.css" type="text/css" />');
    mywindow.document.write('</head><body onload="window.print()">');

    mywindow.document.write($('#divCompareProducts').html().replace('comparisonMain', ''));
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();
});
