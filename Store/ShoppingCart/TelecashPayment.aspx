﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/MasterPage.master"   Inherits="Presentation.ShoppingCart_TelecashPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="CHECKOUT">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="pageTitle">Checkout</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <iframe id="TransFrame" runat="server" src="TeleCashRequest.aspx" width="800px" height="800px"
                         scrolling="no" frameborder="0" visible="true"></iframe>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
