﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.ShoppingCart_Basket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" ClientIDMode="Static">
    <!-- PAGE CONTENT ------------------------------------------------------------------------------------------------------------------------------------------ -->
    <!-- Add By Geeti ------------------------------------------------------------------------------------------------------------------------------------------ -->
    <script src="https://apis.google.com/js/platform.js"></script>
    <meta name="google-signin-client_id" content="816498735044-v6algoj7l49s9l4e1j7e1o9gfbt267nl.apps.googleusercontent.com">
    <script type="text/javascript">
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            if (Querystring == 'False') {
                var loc = host + 'ShoppingCart/Basket.aspx?name=' + profile.getName() + '&email='+profile.getEmail()+'&firstname='+profile.getGivenName()+'&lastname='+profile.getFamilyName()+' &Case=Basket';
                window.location.href = loc;
            }
  
        }           
    </script>
    <style>
        @media (min-width: 768px) {
            .modal-dialog {
                min-width: 860px;
            }

            #spnModalText div:last-child {
                border: 0;
            }

            .modal-dialog.width50p {
                width: 50%;
                min-width: 50%;
            }
        }
    </style>
    <script type="text/javascript">
        function divcount(){
            var divlen = $("#divLoginBeforeCheckout > div").length
            if(divlen==2)
            {
                $('#divBasketRegisteredLogin,#divBasketGuestLogin').removeClass('col-md-4');
                $('#divBasketRegisteredLogin,#divBasketGuestLogin').addClass('col-md-6');

                $('.modal-body').removeClass('modalBottom');
                $('.modal-dialog').addClass('width50p');
            }
            else if(divlen==3){
              
                $('.modal-body').removeClass('modalBottom');
                
            }
          
           
        }
        $(window).load(function(){
            var hasclass = $('#myModal').attr('class');
            //alert(hasclass);
            if(hasclass == 'modal fade in'){
                $('body').addClass('modal-open');
            }
        });
        $(function(){
            divcount();
            var hasclass = $('#myModal').attr('class');
            $('#btnWarningOK').click(function(){
                setTimeout(function(){
                    if(hasclass == 'modal fade in'){
                        $('body').addClass('modal-open');
                    }
                },600);
            });
            $('#myWarningModal .close').click(function(){
                setTimeout(function(){
                    if(hasclass == 'modal fade in'){
                        $('body').addClass('modal-open');
                    }
                },600);
            });
        });

        //Added by Snehal 16 09 2016		
        var isEmailValid = "<%=isEmailValid %>";		
        var MsgEmailBlank = "<%=MsgEmailBlank %>";		
        var MsgPasswordBlank = "<%=MsgPasswordBlank %>";		
        var MsgInvalidEmail = "<%=MsgInvalidEmail %>";		
        var MsgInvalidUsenamePass="<%=MsgInvalidUsenamePass%>";        

    </script>
    <!-- END ------------------------------------------------------------------------------------------------------------------------------------------ -->

    <div id="divMainContent">
        <section id="STANDARD_BASKET">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="pageTitle" id="hPageHeading" runat="server"></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" id="divBasketListing">
                        <asp:Repeater ID="rptBasketListing" runat="server" OnItemDataBound="rptBasketListing_ItemDataBound">
                            <HeaderTemplate>
                                <div class="standard_basket_titles">
                                    <div class="standard_basket_image customTableHead" id="divProductText" runat="server"></div>
                                    <div class="standard_basket_text customTableHead" id="divTitleText" runat="server"></div>
                                    <div class="standard_basket_plus_minus customTableHead" id="divQuantityText" runat="server"></div>
                                    <div class="standard_basket_unit_price customTableHead" id="divUnitCostText" runat="server"></div>
                                    <div class="standard_basket_line_total customTableHead" id="divLineTotalText" runat="server"></div>
                                    <div class="standard_basket_remove customTableHead" id="divRemoveText" runat="server"></div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div id="dvBasketRowContainer" runat="server" class="standard_basket_row">
                                    <!-- PRODUCT 1 -->
                                    <div class="standard_basket_image">
                                        <img class="image_bg" id="imgProduct" runat="server" style="width: 60px;" alt="" />
                                    </div>
                                    <div id="dvProductName" runat="server" class="standard_basket_text customTableText">
                                        <asp:Literal ID="ltrSKUName" runat="server" /><br>
                                        <asp:Literal ID="ltrProductName" runat="server" />
                                    </div>
                                    <div class="standard_basket_plus_minus">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-btn"><a class="btn btn-default active aMinus" role="button">&ndash;</a></span>
                                            <input type="text" style="text-align: center"
                                                class="form-control quantity customInput" placeholder="" maxlength="8" id="txtQuantity" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);">
                                            <span class="input-group-btn"><a class="btn btn-default active aPlus" role="button">+</a></span>
                                        </div>
                                    </div>                                                                       
                                    <div id="dvUnitPrice" runat="server" class="standard_basket_unit_price customTableText FpriceBreak"></div>
                                    <div id="dvTotalPrice" runat="server" class="standard_basket_line_total customTableText"></div>
                                    <div class="standard_basket_remove">
                                        <asp:LinkButton ID="lnkDeleteProduct" runat="server" CssClass="customClose" CommandArgument='<% #Eval("ShoppingCartProductId")%>'
                                            OnClick="lnkDeleteProduct_Click">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>

                                        <a id="aRemoveProducts" runat="server" href="javascript:void(0);" class="customClose" style="display: none;">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                    </div>
                                    <input type="hidden" id="hdnIsBackOrderAllowed" runat="server" value="0" />
                                    <input type="hidden" id="hdnMinimumOrderQuantity" runat="server" value="0" />
                                    <input type="hidden" id="hdnMaximumOrderQuantity" runat="server" value="0" />
                                    <input type="hidden" id="hdnStockStatus" runat="server" value="0" />
                                    <input type="hidden" id="hdnShoppingCartProductId" runat="server" />
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                <div class="standard_basket_row">
                                    <!-- TOTAL ROW -->
                                    <div class="standard_basket_image">&nbsp;</div>
                                    <div class="standard_basket_text">&nbsp;</div>
                                    <div class="standard_basket_plus_minus">&nbsp;</div>
                                    <div class="standard_basket_unit_price bolder_weight customTableText" id="divTotalText" runat="server"></div>
                                    <div class="standard_basket_line_total  bolder_weight">
                                        <div id="dvTotal" runat="server" style="display: block;" class="customTableText"></div>

                                        <div id="Divor" runat="server" style="text-align: right; display: inline-block;" class="bolder_weight customTableText">
                                        </div>
                                        <div id="DivPoint" runat="server" style="text-align: left; display: inline-block;" class="bolder_weight customTableText">
                                        </div>

                                        <div id="Divhidden" runat="server" style="text-align: left; display: none;" class="bolder_weight customTableText">
                                        </div>
                                    </div>
                                    <div class="standard_basket_remove"></div>
                                </div>
                                <div class="standard_basket_row text-right">
                                    <a href="javascript:void(0);" id="aContinueShopping" runat="server" onserverclick="aCheckOut_ServerClick"
                                        class="btn btn-primary customActionBtn shoppingbtn"></a>
                                    <a href="javascript:void(0);" id="aCheckOut" runat="server" onserverclick="aCheckOut_ServerClick"
                                        class="btn btn-primary customActionBtn"></a>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div id="divBasketNote" runat="server" class="standard_basket_row"></div>
                        <div id="dvEmptyBasket" runat="server"></div>
                    </div>
                </div>
            </div>
            <!-- COTAINER END -->
        </section>
    </div>
    <!-- PAGE CONTENT ENDS ------------------------------------------------------------------------------------------------------------------------------------- -->
    <input type="hidden" id="hidShoppingCartProductId" value="0" />


    <!-- Login modal popup starts -->
    <div id="divLoginBeforeCheckout" class="row" style="padding-bottom: 15px; display: none;">

        <div class="col-xs-12  pre_checkout_border form-horizontal col-md-4" runat="server" id="divBasketRegisteredLogin" visible="true">
            <h3 class="pageSubTitle"><%=Basket_HaveAnAccount %></h3>
            <div class="standard_basket_modal_block">
                <div class="form-group">
                    <label class="col-xs-12" for="inputEmail3"><%=Basket_Email%></label>
                    <div class="col-xs-12">
                        <input type="email" placeholder="<%=Basket_Email%>" onkeydown='SetFocusOnButton(event,aLogin)' id="txtEmail" class="form-control customInput">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12" for="inputPassword3"><%=Basket_Password %></label>
                    <div class="col-xs-12">
                        <input type="password" placeholder="<%=Basket_Password%>" onkeydown='SetFocusOnButton(event,aLogin)' id="txtPassword" class="form-control customInput">
                    </div>
                </div>
            </div>
            <div><a href="javascript:void(0);" id="aLogin" class="btn btn-primary customActionBtn"><%=Basket_SignIntoYourAccount %></a></div>
            <div class="standard_basket_modal_forgot"><a href="<%=host %>Login/ResetPassword.aspx" class="smlHyperLink"><%=Basket_ForgotPassword%></a></div>
            <div class="hidden-sm hidden-md hidden-lg">
                <hr>
            </div>
        </div>

        <div class="col-xs-12  pre_checkout_border form-horizontal col-md-4" runat="server" id="divBasketGuestLogin" visible="false">
            <h3 class="pageSubTitle"><%=Basket_NewToOurSite%></h3>
            <div class="standard_basket_modal_block form-horizontal">
                <div class="form-group">
                    <label class="col-xs-12" for="inputEmail3"><%=Basket_Email%></label>
                    <div class="col-xs-12">
                        <input type="email" placeholder="<%=Basket_Email%>" onkeydown='SetFocusOnButton(event,aGuestCheckOut)' id="txtGuestEmail" class="form-control customInput">
                    </div>
                </div>
                <div class="standard_basket_modal_message pageSmlText"><%=Basket_OrderConfirmationMessage%></div>
            </div>
            <%--<div><a href="javascript:void(0);" id="aGuestCheckOut" class="btn btn-primary customActionBtn"><%=Basket_CheckoutAsGuest%></a></div>--%>
            <asp:Button ID="btnGuestCheckOut" runat="server" class="btn btn-primary customActionBtn" />
        </div>

        <%-- Added By Snehal- For Social Media Login 29 09 2016 --%>


        <div class="col-xs-12  form-horizontal  col-md-4" runat="server" id="divSocialLogin" visible="true">
            <h3 class="pageSubTitle">Connect With Social Media</h3>
            <ul>
                <li>
                    <div class="container" id="divGPlusLogin" runat="server" clientidmode="Static">
                        <div class="g-signin2" data-onsuccess="onSignIn"></div>
                    </div>
                </li>

            </ul>

        </div>


        <%-- End Social Login--%>
    </div>
    <!--Sachin Chauhan Start : 28 03 2016 :  Hidden fields to store resource language specific message data-->
    <input type="hidden" id="hdnBasket_InvalidEmailPassword" runat="server" />
    <input type="hidden" id="hdnBasket_AddPreviousProducts" runat="server" />
    <input type="hidden" id="hdnBasket_ErrorUpdatingBasket" runat="server" />
    <input type="hidden" id="hdnBasket_PreviousProductAdded" runat="server" />
    <!--Sachin Chauhan End : 28 03 2016 -->

    <!--Added By Hardik To check MOQ - 12/May/2017 -->
    <input type="hidden" ID="hdnFirstPriceBrk" runat="server" />

    <!-- Login modal popup ends -->

    <!--Added By Snehal To check max min qty 23 09 2016 -->
    <input type="hidden" id="hdnMin" runat="server" value="0" />
         <input type="hidden" id="hdnpoint" runat="server" value="0" />
    <input type="hidden" id="hdnMax" runat="server" value="0" />
    <!--End -->

    <script type="text/javascript">
        var strCurrencySymbol = "<%=strCurrencySymbol %>";
        var IsMaxQtyEnabled = "<%=IsMaxQtyEnabled %>";
        var intStoreFeatureDefaultValue = "<%=intStoreFeatureDefaultValue %>";
        var Querystring = "<%=Querystring %>";
        var host = "<%=host %>";
        var strGenericPointsText="<%=strGenericPointsText%>";
        var intCurrencyId = <%=intCurrencyId %>;
        var bBackOrderAllowed = "<%=bBackOrderAllowed %>"; 
        var strRemoveItemsFromCart ="<%= strRemoveItemsFromCart %>"; 
        var strQuantityNotZero ="<%= strQuantityNotZero %>"; 
        var strMinMaxQuantity ="<%= strMinMaxQuantity %>";
        var strQuantityNotMoreThanStock ="<%= strQuantityNotMoreThanStock %>";
        var strProductRemovedFromCart ="<%= strProductRemovedFromCart %>";
        var strBasketErrorWhileRemovingProduct ="<%= strBasketErrorWhileRemovingProduct %>";        
        var bIsPointsEnabled ="<%= bIsPointsEnabled %>"; 
        function LoginUser() {
            try {
                $('#myModal').find('#spnModalText').html($('#divLoginBeforeCheckout').html());
                $('#myModal').find('#spnModalTitle').html("<%=Basket_BeforeYouCheckout%>");
                $('#myModal').find('.modal-footer').hide();
                $('#myModal').modal('show');
            } catch (e) { }
        }

        //Added By Snehal 28 09 2016
        function LoginAPI()
        {
            var EmailId= '<%= Session["APIEmailId"]%>';
            $.ajax({
                url: host + 'login/Register.aspx/GuestRegistration',
                data: "{'strEmail': '" + EmailId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true
                //beforeSend: function (request) {
                //    $.blockUI({ message: $('#dvLoader') });
                //}
            }).done(function (data) {
                if (data.d == "success") {
                    window.location.href = host + 'checkout';
                }
                else if (data.d == "GuestUser") {
                  
                    $('#myConfirmModal').find('#ConfirmationMessage').html('#hdnBasket_AddPreviousProducts').val();
                    $('#myConfirmModal').attr('rel', 'addbasket');
                    $('#myConfirmModal').modal('show');
                    return false;
                }
                else {
                    $('#myErrorModal').find('#ErrorMessage').html(data.d);
                    $('#myErrorModal').modal('show');
                    return false;
                }
            });

            $(document).on('click', '#btnConfirmNo', function () {
                if ($('#myConfirmModal').attr('rel') == 'addbasket') {
                    $('#dvLoader').show();
                    // add previously added products into basket
                    $.ajax({
                        url: host + 'shoppingcart/basket.aspx/UpdateBasketProducts',
                        data: "{'action': 'n'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        cache: false,
                        async: true
                        //beforeSend: function (request) {
                        //    $.blockUI({ message: $('#dvLoader') });
                        //}
                    }).done(function (data) {
                        //$.unblockUI();
                        $('#dvLoader').hide();
                        if (data.d == true) {
                            window.location.href = host + 'checkout';
                        }
                        else {
                            $('#myErrorModal').find('#ErrorMessage').html($('#hdnBasket_ErrorUpdatingBasket').val());
                            $('#myErrorModal').modal('show');
                        }
                    });
                }
            });

            $(document).on('click', '#btnConfirmYes', function () {
                if ($('#myConfirmModal').attr('rel') == 'addbasket') {
                    $('#dvLoader').show();
                    // add previously added products into basket
                    $.ajax({
                        url: host + 'shoppingcart/basket.aspx/UpdateBasketProducts',
                        data: "{'action': 'y'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        cache: false,
                        async: true
                        //beforeSend: function (request) {
                        //    $.blockUI({ message: $('#dvLoader') });
                        //}
                    }).done(function (data) {
                        //$.unblockUI();
                        $('#dvLoader').hide();
                        if (data.d == true) {
                            $('#mySuccessModal').find('#SuccessMessage').html($('#hdnBasket_PreviousProductAdded').val());
                            $('#mySuccessModal').modal('show');
                            window.location.href = host + 'Checkout/ShoppingCart';
                        }
                        else {
                            $('#myErrorModal').find('#ErrorMessage').html($('#hdnBasket_ErrorUpdatingBasket').val());
                            $('#myErrorModal').modal('show');
                        }
                    });
                }
                else {
                    $('#dvLoader').show();
                    $.ajax({
                        url: host + 'shoppingcart/basket.aspx/RemoveProductsFromBasket',
                        data: "{'ShoppingCartProductId': " + $('#hidShoppingCartProductId').val() + ",'intCurrencyId':" + intCurrencyId + "}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        cache: false,
                        async: true
                        //beforeSend: function (request) {
                        //    $.blockUI({ message: $('#dvLoader') });
                        //}
                    }).done(function (data) {
                        //$.unblockUI();
                        $('#dvLoader').hide();
                        if (data.d == true) {
                            $('#dvBasketRowContainer' + $('#hidShoppingCartProductId').val()).remove();
                            $('#mySuccessModal').find('#SuccessMessage').html(strProductRemovedFromCart);
                            $('#mySuccessModal').modal('show');
                            if ($('.standard_basket_row').length != 2) {
                                $('#dvEmptyBasket').hide();
                            }
                            else {
                                $('#dvEmptyBasket').show();
                                $('.standard_basket_titles').hide();
                                $('.standard_basket_row').hide();
                            }
                        }
                        else {
                            $('#myErrorModal').find('#ErrorMessage').html(strBasketErrorWhileRemovingProduct);
                            $('#myErrorModal').modal('show');
                        }
                    });
                    updateTotalPrice();
                }
            });
        }
    </script>
    <script type="text/javascript">
        //debugger;
        var MinPriceBreak = "<%=MinPriceBreak%>"
    </script>
    <%--<script src="<%=host %>js/jquery.blockUI.js"></script>--%>
    <script src="<%=host %>js/basket.js"></script>
    <script src="<%=host %>/JS/GooglePlusAPI.js"></script>
    <script src="<%=host %>/JS/custom.js"></script>
</asp:Content>

