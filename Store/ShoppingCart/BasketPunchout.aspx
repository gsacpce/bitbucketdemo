﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.ShoppingCart_BasketPunchout" MasterPageFile="~/Master/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" ClientIDMode="Static">
    <!-- PAGE CONTENT ------------------------------------------------------------------------------------------------------------------------------------------ -->
    <section id="STANDARD_BASKET">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="pageTitle" id="hPageHeading" runat="server"></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <asp:Repeater ID="rptBasketListing" runat="server" OnItemDataBound="rptBasketListing_ItemDataBound">
                        <HeaderTemplate>
                            <div class="standard_basket_titles">
                                <div class="standard_basket_image customTableHead" id="divProductText" runat="server"></div>
                                <div class="standard_basket_text customTableHead" id="divTitleText" runat="server"></div>
                                <div class="standard_basket_plus_minus customTableHead" id="divQuantityText" runat="server"></div>
                                <div class="standard_basket_unit_price customTableHead" id="divUnitCostText" runat="server"></div>
                                <div class="standard_basket_line_total customTableHead" id="divLineTotalText" runat="server"></div>
                                <div class="standard_basket_remove customTableHead" id="divRemoveText" runat="server"></div>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div id="dvBasketRowContainer" runat="server" class="standard_basket_row">
                                <!-- PRODUCT 1 -->
                                
                                <div class="standard_basket_image">
                                    <img class="image_bg" id="imgProduct" runat="server" style="width: 60px;" alt="" />
                                </div>
                                <div id="dvProductName" runat="server" class="standard_basket_text customTableText">
                                    <asp:Literal ID="ltrSKUName" runat="server" /><br>
                                    <asp:Literal ID="ltrProductName" runat="server" />
                                </div>
                                <div class="standard_basket_plus_minus">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-btn"><a class="btn btn-default active aMinus" role="button">&ndash;</a></span>
                                        <input type="text" style="text-align: center"
                                            class="form-control quantity" placeholder="" maxlength="8" id="txtQuantity" runat="server">
                                        <span class="input-group-btn"><a class="btn btn-default active aPlus" role="button">+</a></span>
                                    </div>
                                </div>
                                <div id="dvUnitPrice" runat="server" class="standard_basket_unit_price customTableText"></div>
                                <div id="dvTotalPrice" runat="server" class="standard_basket_line_total customTableText"></div>
                                <div class="standard_basket_remove">
                                    <a id="aRemoveProducts" runat="server" href="javascript:void(0);">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                </div>
                                <input type="hidden" id="hdnIsBackOrderAllowed" runat="server" value="0" />
                                <input type="hidden" id="hdnMinimumOrderQuantity" runat="server" value="0" />
                                <input type="hidden" id="hdnMaximumOrderQuantity" runat="server" value="0" />
                                <input type="hidden" id="hdnStockStatus" runat="server" value="0" />
                                <input type="hidden" id="hdnShoppingCartProductId" runat="server" />
                            
                                </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            <div class="standard_basket_row row">
                                <div class="col-md-4 col-md-offset-8">
                                <!-- TOTAL ROW -->

                                <% if (bCustomPunchout == true)
                                   { %>
                                <%--Added By Hardik ***** START ***** 9-Aug-2016 --%>
                                <div id="countryFrght" runat="server" class="">
                                    <asp:Label ID="lblSelectCountry" runat="server" Text="Select Delivery Country"></asp:Label>
                                    <asp:DropDownList ID="ddlDCountry" runat="server" DataTextField="CountryName" DataValueField="CountryId">
                                        <%--OnSelectedIndexChanged="ddlDCountry_SelectedIndexChanged"--%>
                                    </asp:DropDownList>
                                </div>
                                <div id="header2_space" runat="server" style="display: none;">
                                    <div class="checkout_shipping_outer" id="divStandardCont" runat="server">
                                        <div class="checkout_shipping_left">
                                            <label class="radio-inline pageSmlText">
                                                <input onclick="calculatefreight(this)" type="radio" name="optionsRadios" id="rbStandard" runat="server" value="1"><span id="spnStandardText" runat="server" class="bolder_weight"></span>
                                            </label>
                                        </div>
                                        <div class="checkout_shipping_right pageSmlText"><%=strCurrencySymbol %><span id="spnstandard" runat="server"></span></div>
                                    </div>
                                    <div class="checkout_shipping_outer" id="divExpressCont" runat="server">
                                        <div class="checkout_shipping_left">
                                            <label class="radio-inline pageSmlText">
                                                <input onclick="calculatefreight(this)" type="radio" name="optionsRadios" id="rbExpress" runat="server" value="2"><span class="bolder_weight" id="spnExpressText" runat="server"></span>
                                            </label>
                                        </div>
                                        <div class="checkout_shipping_right pageSmlText"><%=strCurrencySymbol %><span id="spnexpress" runat="server"></span></div>
                                    </div>
                                    <div class="checkout_shipping_outer pageSmlText" id="divMOVMessage" runat="server" style="display: none;"></div>
                                    <div class="checkout_shipping_outer pageSmlText" id="divDutyMessage" runat="server" style="display: none;"></div>
                                </div>
                                <div class="checkout_totals_outer" id="divShipping" runat="server" style="display: none;">
                                    <div id="divFreightText" runat="server" class="checkout_totals_left bolder_weight pageSmlText">Freight</div>
                                    <div id="dvFreight" runat="server" class="checkout_totals_right bolder_weight pageSmlText"></div>
                                    <div class="checkout_totals_left pageSmlText"><%=strShipping%></div>
                                    <div class="checkout_totals_right pageSmlText"><%=strCurrencySymbol%><span id="spnShippingPrice" runat="server">0</span></div>
                                </div>
                                <%--Added By Hardik ***** END ***** 9-Aug-2016 --%>
                                <% } %>

                                <% if (bCustomPunchout == false)
                                   { %>
                                <div class="standard_basket_image">&nbsp;</div>
                                <div class="standard_basket_text">&nbsp;</div>
                                <div class="standard_basket_plus_minus">&nbsp;</div>
                                <% } %>
                                     <div class="checkout_totals_outer">
                                <div class="standard_basket_unit_price bolder_weight customTableText checkout_totals_left" id="divTotalText" runat="server"></div>
                                <div id="dvTotal" runat="server" class="standard_basket_line_total bolder_weight customTableText checkout_totals_right pageSmlText"></div>
                                   </div>
                                <div class="standard_basket_remove"></div>
                                    </div>
                            </div>
                            <div class="standard_basket_row text-right">
                                <a href="javascript:void(0);" id="aContinueShopping" runat="server" onserverclick="aCheckOut_ServerClick"
                                    class="btn btn-primary customActionBtn"></a>
                                <a href="javascript:void(0);" id="aCheckOut" runat="server" onserverclick="aCheckOut_ServerClick"
                                    class="btn btn-primary customActionBtn"></a>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div id="dvEmptyBasket" runat="server"></div>
                </div>
            </div>
        </div>
        <!-- COTAINER END -->
    </section>
    <!-- PAGE CONTENT ENDS ------------------------------------------------------------------------------------------------------------------------------------- -->
    <input type="hidden" id="hidShoppingCartProductId" value="0" />
    <% if (bCustomPunchout == true)
       { %>
    <input type="hidden" id="hidcountyid" runat="server" />
    <% } %>

    <!-- Login modal popup starts -->
    <div id="divLoginBeforeCheckout" style="display: none;">
        <div class="col-xs-12 col-sm-6 pre_checkout_border">
            <h3><%=Basket_HaveAnAccount%></h3>
            <div class="standard_basket_modal_block">
                <div class="form-group">
                    <label class="col-xs-12 control-label" for="inputEmail3"><%=EmailId%></label>
                    <div class="col-xs-12">
                        <input type="email" placeholder="Email" onkeypress='SetFocusOnButton(event,aLogin)' id="txtEmail" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 control-label" for="inputPassword3"><%=Password%></label>
                    <div class="col-xs-12">
                        <input type="password" placeholder="Password" onkeypress='SetFocusOnButton(event,aLogin)' id="txtPassword" class="form-control">
                    </div>
                </div>
            </div>
            <div><a href="javascript:void(0);" id="aLogin" class="btn btn-primary"><%=SignIn_to_Account%></a></div>
            <div class="standard_basket_modal_forgot"><a href="<%=host %>Login/ResetPassword.aspx"><%=Forgot_password%></a></div>
            <div class="hidden-sm hidden-md hidden-lg">
                <hr>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h3><%=Basket_NewToOurSite%></h3>
            <div class="standard_basket_modal_block">
                <div class="form-group">
                    <label class="col-xs-12 control-label" for="inputEmail3"><%=EmailId%></label>
                    <div class="col-xs-12">
                        <input type="email" placeholder="Email" id="txtGuestEmail" class="form-control">
                    </div>
                </div>
                <div class="standard_basket_modal_message"><%=Basket_OrderConfirmation%> </div>
            </div>
            <div><a href="javascript:void(0);" id="aGuestCheckOut" class="btn btn-primary"><%=Checkout_as_Guest%></a></div>
        </div>
    </div>
    <!-- Login modal popup ends -->

    <% if (bCustomPunchout == true)
       { %>
    <input type="hidden" value="0" id="hidBasketTot" runat="server" />
    <input type="hidden" value="0" id="hidStandard" runat="server" />
    <input type="hidden" value="0" id="hidExpress" runat="server" />
    <input type="hidden" value="0" id="hidSpnShippingPrice" runat="server" />
    <input type="hidden" value="0" id="hidddlDCountry" runat="server" />
    <input type="hidden" value="0" id="hidDeliveryCharge" runat="server" />
    <% } %>

    <script type="text/javascript">
        var host = '<%=host %>';
        var intCurrencyId = '<%=intCurrencyId %>';
        var bBackOrderAllowed = '<%=bBackOrderAllowed %>';
        var strRemoveItemsFromCart = '<%= strRemoveItemsFromCart %>';
        var strQuantityNotZero = '<%= strQuantityNotZero %>';
        var strMinMaxQuantity = '<%= strMinMaxQuantity %>';
        var strQuantityNotMoreThanStock = '<%= strQuantityNotMoreThanStock %>';
        var strProductRemovedFromCart = '<%= strProductRemovedFromCart %>';
        var strBasketErrorWhileRemovingProduct = '<%= strBasketErrorWhileRemovingProduct %>';
        var bIsPointsEnabled = '<%= bIsPointsEnabled %>';
        function LoginUser() {
            try {
                $('#myModal').find('#spnModalText').html($('#divLoginBeforeCheckout').html());
                $('#myModal').find('#spnModalTitle').html('Before you checkout');
                $('#myModal').find('.modal-footer').hide();
                $('#myModal').modal('show');
            } catch (e) { }
        }
    </script>
    <%--<script src="<%=host %>js/jquery.blockUI.js"></script>--%>
   
     <script src="<%=host %>js/basket.js"></script>
   
  
</asp:Content>

