﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.ShoppingCart_OrderDetailsGiftCertificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="ORDER_CONFIRM_PAGE">
        <div class="container" id="divOrderDetails">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="pageTitle"><%=strConfirmation %></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p class="pageText"><%=strOrderPlaceNote %></p>
                    <p class="pageText"><%=strOrderEmailConfirmationNote %></p>
                </div>
            </div>
            <div class="row" id="divGuestCheckout" runat="server">
                <div class="col-xs-12">
                    <div class="well well-sm order_confirm_save_details">
                        <div class="row">
                            <div class="col-xs-12 col-md-5">
                                <h4 class="pageSubSubTitle"><%=OrderDetails_SaveDetails_FutureUse_Note%></h4>
                                <ul>
                                    <li class="pageText"><%=OrderDetails_CheckOut_Faster%></li>
                                    <li class="pageText"><%=Generic_View_Order_History%></li>
                                    <li class="pageText"><%=Generic_WriteReviews%></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-7">
                                <h4 class="pageSubSubTitle"><%=Order_Details_Set_Password_Reset_Text%></h4>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-4 control-label customLabel" for=""><%=Register_Password_Text%></label>
                                    <div class="col-sm-9 col-md-8">
                                        <asp:TextBox ID="txtODPassword" autocomplete="off" runat="server" CssClass="form-control customInput" placeholder="Password" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqtxtPassword" runat="server" ControlToValidate="txtODPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                        <div style="display: none">
                                            <asp:RegularExpressionValidator ID="regexPwdLentxtPassword" runat="server" ControlToValidate="txtODPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regexAlphaNumtxtPassword" runat="server" ControlToValidate="txtODPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regexAlphaNumSymtxtPassword" runat="server" ControlToValidate="txtODPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                        </div>

                                        <asp:HiddenField ID="hdnPasswordPolicyType" runat="server" />
                                        <asp:HiddenField ID="hdnMinPasswordLength" runat="server" />
                                        <asp:Label ID="lblPasswordPolicyMessage" runat="server" CssClass="text-danger"></asp:Label>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-4 control-label customLabel" for=""><%=Register_ConfirmPassword_Text%></label>
                                    <div class="col-sm-9 col-md-8">
                                        <asp:TextBox CssClass="form-control customInput" runat="server" ID="txtODConfirmPassword" placeholder="Confirm Password" autocomplete="off" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqtxtConfirmPassword" runat="server" ControlToValidate="txtODConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cmpValidatorPwd" runat="server" ControlToCompare="txtODPassword" ControlToValidate="txtODConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:CompareValidator>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-4 control-label customLabel hidden-xs" for="">&nbsp;</label>
                                    <div class="col-sm-9 col-md-8 order_conform_save_button">
                                        <asp:Button ID="btnCreateAccount" runat="server" OnClick="btnCreateAccount_OnClick" CssClass="btn btn-primary collapsed customActionBtn buttonmargin" ValidationGroup="OnClickSubmit"/>
                                        <asp:ValidationSummary ID="vSumm" runat="server" DisplayMode="List"  ShowMessageBox="true" ValidationGroup="OnClickSubmit" ShowSummary="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 order_confirm_order_number">
                    <h3 class="pageSubTitle"><%=strOrderNumberText %><span class="bolder_weight">
                        <asp:Literal ID="ltrOrderNo" runat="server" /></span>
                        <a href="javascript:void(0);" id="aPrintOrder" class="btn btn-primary customActionBtn pull-right noprint"><%=strPrintText %></a>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <asp:Repeater ID="rptOrderDetails" runat="server" OnItemDataBound="rptOrderDetails_ItemDataBound">
                        <HeaderTemplate>
                            <div class="order_confirm_titles">
                                <div class="customTableHead col-xs-2"><%=strAmountText %></div>
                                <div class="customTableHead  col-xs-2"><%=strQuantityText %></div>
                                <div class="ustomTableHead  col-xs-3"><%=strRecipientEmailText %></div>
                                <div class="customTableHead  col-xs-5"><%=strPersonalMessageText %></div>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="order_confirm_row">
                                <div class="customTableText  col-xs-2"><%# DataBinder.Eval(Container.DataItem, "Amount") %></div>
                                <div class="customTableText  col-xs-2"><%# DataBinder.Eval(Container.DataItem, "Quantity") %></div>
                                <div class="customTableText  col-xs-3">
                                    <%# DataBinder.Eval(Container.DataItem, "RecipientEmail") %>
                                </div>
                                <div class="customTableText  col-xs-5">
                                    <%# DataBinder.Eval(Container.DataItem, "PersonalMessage") %>
                                </div>
                            </div>
                            <%--  <asp:Label ID="lblrptAmount" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                            <asp:Label ID="lblrptQuantity" runat="server" Text='<%# Eval("Quantity") %>'></asp:Label>
                            <asp:Label ID="lblrptRecipientEmail" runat="server" Text='<%# Eval("RecipientEmail") %>'></asp:Label>
                            <asp:Label ID="lblrptPersonalMessage" runat="server" Text='<%# Eval("PersonalMessage") %>'></asp:Label>--%>
                        </ItemTemplate>
                        <FooterTemplate>
                            <div class="order_confirm_totals_row">
                                <div class="order_confirm_totals customTableText"><%=strSubTotalText %></div>
                                <div id="dvSubTotal" runat="server" class="order_confirm_line_total customTableText"></div>
                            </div>
                            <div class="order_confirm_totals_row" id="divShipping" runat="server" style="display:none;">
                                <div class="order_confirm_totals customTableText"><%=strShippingText %></div>
                                <div id="dvShippingCharges" runat="server" class="order_confirm_line_total customTableText"></div>
                            </div>
                            <div class="order_confirm_totals_row" id="divTax" runat="server" style="display: none;">
                                <div class="order_confirm_totals customTableText"><%=strTaxText %></div>
                                <div id="dvTaxes" runat="server" class="order_confirm_line_total customTableText"></div>
                            </div>
                            <div class="order_confirm_grand_total">
                                <div class="order_confirm_totals bolder_weight customTableText"><strong><%=strTotalText %></strong></div>
                                <div id="dvTotal" runat="server" class="order_confirm_line_total bolder_weight customTableText"><strong></strong></div>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="order_addresses_heading customTableHead"><%=strBillingAddressText %></div>
                    <div class="order_addresses_text customTableText">
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="order_addresses_heading customTableHead"><%=strPaymentMethodText %></div>
                    <div class="order_addresses_text customTableText">
                        <asp:Literal ID="ltrPaymentMethod" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <a id="aPrintOrder" href="javascript:void(0);" class="btn btn-primary customActionBtn pull-right noprint"><%=strPrintText %></a>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        var host = '<%=host %>';
        $(document).on('click', '#aPrintOrder', function () {
            var mywindow = window.open('', 'my div', 'height=1,width=1');
            mywindow.document.write('<html><head><style>.noprint{display:none;}</style>');
            /*optional stylesheet*/
            mywindow.document.write('<link rel="stylesheet" href="' + host + 'CSS/bootstrap-theme.css" type="text/css" /><link rel="stylesheet" href="' + host + 'CSS/bootstrap.css" type="text/css" />');
            mywindow.document.write('</head><body onload="window.print()">');

            mywindow.document.write($('#divOrderDetails').html());
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            mywindow.print();
            mywindow.close();
        });
    </script>
</asp:Content>

