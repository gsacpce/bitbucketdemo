﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.ShoppingCart_NameBadge" %>

<script runat="server">

</script>





<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

      <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center"><h1>name badge order form</h1></div>
        </div>
       
            <div  class="row">
                <div class="col-xs-12">
                    <p>
                        Please fill out all the information below and then select the submit request button at the bottom of the page.
                    </p>
                    <p>
                        This will generate a request to our support team who will provide a quotation for your approval.
                    </p>
                    <p>
                        If you have any problems during the process, please contact our support team.
                    </p>
                    <p>
                        Email: <a href="mailto:infiniti@brandaddition.com">infiniti@brandaddition.com
                        </a>
                    </p>
                    <p>
                        Tel: +44 (0) 161 786 0435
                    </p>
                </div>
            </div>
          <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <p>
                        Front
                    </p>
                    <img src="../Images/Infiniti/Infiniti_name_badge_01_v2.png" height="200" width="200"/>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <p>
                        Back
                    </p>
                    <img src="../Images/Infiniti/Infiniti_name_badge_02_v2.png" height="200" width="200"/>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <p>
                        Magnetic fitting
                    </p>
                    <img src="../Images/Infiniti/Infiniti_name_badge_03_v2.png" height="200" width="200" />
                </div>

            </div>
            <div  class="row">
                <div class="col-xs-12 col-md-6">
                    <h3>Purchaser’s Details</h3>
                    <div class="row form-group">
                        <div class="col-xs-12 col-md-4"><asp:Label ID="lblDate" CssClass="label-control" runat="server" Text="Date:"></asp:Label></div>
                        <div class="col-xs-12 col-md-6">
                            <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                          
                        </div>
                    </div>
                    <div class="row form-group">
                         <div class="col-xs-12 col-md-4"><asp:Label ID="lblName" CssClass="label-control" runat="server" Text="Name:*"></asp:Label></div>
                        <div class="col-xs-12 col-md-6">
                            <asp:TextBox ID="txtinput_badges_name" CssClass="form-control" runat="server" MaxLength="30">

                            </asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="reqName" runat="server" ErrorMessage="*" ControlToValidate="txtinput_badges_name"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12 col-md-4"><asp:Label ID="lblinput_badges_department" CssClass="label-control" runat="server" Text="Department Name:"></asp:Label></div>
                        <div class="col-xs-12 col-md-6">
                            <asp:TextBox ID="txtinput_badges_department" CssClass="form-control" runat="server" MaxLength="30"></asp:TextBox>

                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12 col-md-4"><asp:Label ID="lblTelephone" CssClass="label-control" runat="server" Text="Telephone*:"></asp:Label></div>
                        <div class="col-xs-12 col-md-6">
                            <asp:TextBox ID="txtinput_badges_number" MaxLength="15" CssClass="form-control" runat="server"></asp:TextBox>
                            
                        </div>
                        <asp:RequiredFieldValidator ID="reqNumber" runat="server" ErrorMessage="*" ControlToValidate="txtinput_badges_number"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12 col-md-4"><asp:Label ID="lblEmail" CssClass="label-control" runat="server" Text="Email*:"></asp:Label></div>
                        <div class="col-xs-12 col-md-6">
                            <asp:TextBox ID="txtinput_badges_email" CssClass="form-control" runat="server" MaxLength="30"></asp:TextBox>
                            
                        </div>
                          <asp:RequiredFieldValidator ID="reqEmail" runat="server" ErrorMessage="*" ControlToValidate="txtinput_badges_email"></asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter Valid Email Id" ControlToValidate="txtinput_badges_email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                           
                    </div>
               </div>
                <div class="col-xs-12 col-md-6">
                    <h3>Delivery Address Details</h3>
                    <div class="row form-group">
                    <div class="col-xs-12 col-md-4">
                        <asp:Label ID="lblDeliveryName" CssClass="label-control" runat="server" Text="Name*:"></asp:Label>
                     </div>
                    <div class="col-xs-12 col-md-5">
                            <asp:TextBox ID="txtDeliveryName" CssClass="form-control" runat="server" MaxLength="30"></asp:TextBox>
                        
                  </div>
                        <asp:RequiredFieldValidator ID="reqDeliveryName" runat="server" ErrorMessage="*" ControlToValidate="txtDeliveryName"></asp:RequiredFieldValidator>
                </div>
                    <div class="row form-group">
                        <div class="col-xs-12 col-md-4">
                        <asp:Label ID="lblDeliveryAddress" CssClass="label-control"  runat="server" Text="Delivery Address*:"></asp:Label>
                        </div>
                    <div class="col-xs-12 col-md-4">
                        <asp:TextBox ID="txtDeliveryAddress" CssClass="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txtDeliveryAddress"></asp:RequiredFieldValidator>
                </div>
                    </div>
            </div>
    

            <div  class="row">
                <div class="col-xs-12 col-md-6">
                    <h3>Badge Details</h3>
                    <p>Minimum order is 2 badges per name.</p>
                    <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <asp:Label ID="lblNumOfNames" CssClass="label-control" runat="server" Text="Number of names*:"></asp:Label>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <asp:TextBox ID="txtNumOfNames" CssClass="form-control" runat="server" TextMode="Number" MaxLength="30"></asp:TextBox>
                    </div>
               </div>
                    <p>
                    Names required to be supplied on separate list -  
                    <asp:LinkButton ID="lnkButton" runat="server" Text="Click here" OnClick="lnkButton_Click" CausesValidation="false"></asp:LinkButton>
                    <br />
                   
                    <%--<asp:Button ID="btnClick12" runat="server" Text="Click Here" OnClick="btnClick12_Click" CausesValidation="false"/>--%>
                    after entering  the number of names above.
                </p>
                    <p>Lead time for delivery is 10-15 working days from receipt of this order.</p>
                    <p>Carriage will be POA.</p>
                </div>

                <div class="col-xs-12 col-md-6">
                <h3>Price (including magnetic fitting)</h3>
                <table class="table" width:100%>
                    <tbody>
                        <tr>
                            <th>QTY</th>
                            <th>Sell €</th>
                        </tr>
                        <tr>
                            <td>1+</td>
                            <td>€8.45</td>
                        </tr>
                        <tr>
                            <td>5+</td>
                            <td>€7.55</td>
                        </tr>
                        <tr>
                        <td>10+</td>
                        <td>€7.25</td>
                        </tr>
                         <tr>
                        <td>25+</td>
                        <td>€7.15</td>
                            </tr>
                         <tr>
                        <td>50+</td>

                        <td>€5.75</td>
                            </tr>
                         <tr>
                        <td>100+</td>

                        <td>€4.55</td>
                            </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <div  class="row">
                <div class="col-xs-12">
                <p>
                    Payment will be required by CPC card or credit card.
                        You will be contacted by the Infiniti Helpdesk by phone within 24 hours to take these details, to ensure security. 
                        If you will not be available over the next 24 hours please email 
                        <a href="mailto:infiniti@brandaddition.com">infiniti@brandaddition.com</a>
                    alternative contact who will have payment card details.
                </p>
            </div>
                <div class="col-xs-12">
                <asp:Button ID="Submit12" runat="server" Text="Submit Order" OnClick="Submit12_Click" CausesValidation="false"/>
            </div>
           
             </div>
         </div>
   
    <%--Ravi Gohil--%>
           <div class="modal fade" id="ModalTextRegion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content customModal">
                <div class="customModal modal-header customPanel">
                    <button type="button" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title pageSubSubTitle" id="myModalLabelUpdate">Name Badges</h4>
                    
                </div>
               
                <div class="modal-body clearfix">
                    <div class="form-group clearfix">
                         <div class="col-xs-12">
                       
                            
                        
                        <p style="font-size:13px;">
                            Please enter the names in the textboxes below. Names should be entered<br>
                            in capital letters and need only be entered once, although two badges<br>
                            per name will be supplied.
                        </p>
                        <asp:Repeater ID="rptText" runat="server">
                           
                <ItemTemplate>
                  <div class="row form-group" style="margin-top:5px;">
                      <div class="col-xs-12 col-md-3">
                          <asp:Label ID="Name" runat="server" CssClass="control-label" Text="Name" ClientIDMode="Static"></asp:Label>
                      </div>
                      <div class="col-xs-12 col-md-5">
                          <asp:TextBox ID="txt1" runat="server" CssClass="form-control" ClientIDMode="Static" MaxLength="30"></asp:TextBox>
                      </div>
                      <div class="col-xs-12 col-md-4">
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="please Enter Names" ControlToValidate="txt1"></asp:RequiredFieldValidator>
                      </div>
                  </div>
             
          </ItemTemplate>
                        </asp:Repeater>
                             <div class="text-center" style="margin-top:10px;">
                                 <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click" ClientIDMode="Static" CausesValidation="false"/>
                             </div>

                         </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


          <script type="text/javascript">


              function ShowModal() {
                  $('#ModalTextRegion').modal('show')
              }
    </script>
</asp:Content>

