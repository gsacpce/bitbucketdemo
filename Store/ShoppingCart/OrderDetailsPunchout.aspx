﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.ShoppingCart_OrderDetailsPunchout" MasterPageFile="~/Master/MasterPage.master" %>

<asp:Content ID="Content1" ClientIDMode="Static" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="ORDER_CONFIRM_PAGE">
        <div class="container" id="divOrderDetails">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="pageTitle">Confirmation</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p class="pageText">Your order has been placed, thank you.</p>
                    <p class="pageText">Details of the order are below and you will receive an order confirmation by email shortly.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 order_confirm_order_number">
                    <h3 class="pageSubTitle">Order number <span class="bolder_weight">
                        <asp:Literal ID="ltrOrderNo" runat="server" /></span>
                        <a href="javascript:void(0);" id="aPrintOrder" class="btn btn-primary customActionBtn pull-right noprint">Print</a>

                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <asp:Repeater ID="rptBasketListing" runat="server" OnItemDataBound="rptBasketListing_ItemDataBound">
                        <HeaderTemplate>
                            <div class="order_confirm_titles">
                                <div class="order_confirm_image customTableHead">Product</div>
                                <div class="order_confirm_text customTableHead">Title</div>
                                <div class="order_confirm_qty customTableHead">Quantity</div>
                                <div class="order_confirm_unit_price customTableHead">Unit cost</div>
                                <div class="order_confirm_line_total customTableHead">Line total</div>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="order_confirm_row">
                                <div class="order_confirm_image">
                                    <img id="imgProduct" runat="server" alt="" style="width: 60px;" class="image_bg" />
                                </div>
                                <div class="order_confirm_text customTableText">
                                    <%# DataBinder.Eval(Container.DataItem, "SKU") %><br>
                                    <%# DataBinder.Eval(Container.DataItem, "ProductName") %>
                                </div>
                                <div class="order_confirm_qty customTableText"><%# DataBinder.Eval(Container.DataItem, "Quantity") %></div>
                                <div id="dvUnitPrice" runat="server" class="order_confirm_unit_price customTableText"></div>
                                <div id="dvTotalPrice" runat="server" class="order_confirm_line_total customTableText"></div>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            <div class="order_confirm_totals_row">
                                <div class="order_confirm_totals customTableText">Subtotal</div>
                                <div id="dvSubTotal" runat="server" class="order_confirm_line_total customTableText"></div>
                            </div>

                            <div class="order_confirm_totals_row">
                                <div class="order_confirm_totals customTableText">Shipping</div>
                                <div id="dvShippingCharges" runat="server" class="order_confirm_line_total customTableText"></div>
                            </div>

                            <div class="order_confirm_totals_row">
                                <div class="order_confirm_totals customTableText">Tax</div>
                                <div id="dvTaxes" runat="server" class="order_confirm_line_total customTableText"></div>
                            </div>

                            <div class="order_confirm_grand_total">
                                <div class="order_confirm_totals bolder_weight customTableText"><strong>Total</strong></div>
                                <div id="dvTotal" runat="server" class="order_confirm_line_total bolder_weight customTableText"><strong></strong></div>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <a id="aPrintOrder" href="javascript:void(0);" class="btn btn-primary customActionBtn pull-right noprint">Print</a>
                </div>
            </div>
        </div>
        <!-- COTAINER END -->
    </section>
    <script type="text/javascript">


        var host = '<%=host %>';
        $(document).on('click', '#aPrintOrder', function () {
            var mywindow = window.open('', 'my div', 'height=1,width=1');
            mywindow.document.write('<html><head><style>.noprint{display:none;}</style>');
            /*optional stylesheet*/
            mywindow.document.write('<link rel="stylesheet" href="' + host + 'CSS/bootstrap-theme.css" type="text/css" />');
            mywindow.document.write('</head><body onload="window.print()">');

            mywindow.document.write($('#divOrderDetails').html());
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            mywindow.print();
            mywindow.close();
        });
    </script>
</asp:Content>
