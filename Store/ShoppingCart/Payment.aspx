﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true"
	Inherits="Presentation.ShoppingCart_Payment" EnableEventValidation="false" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" ClientIDMode="Static" runat="Server">

   <%-- <script type="text/javascript">
	 
		function ShowModal()
		{
			$('#ModalOrdeTotal').modal('show')
		}

</script>--%>
  <%--  <style>
		.modal-dialogCustom {
			width: 350px;
			margin: 250px auto;
		}

		.modal-headerCustom {
			min-height: 16.42857143px;
			padding: 8px;
			border-bottom: 1px solid #e5e5e5;
			background-color:beige;
		}

		.modal-bodyCustom {
			position: relative;
			padding: 10px;
		}

		.container-fluidCustom {
			padding-right: 10px;
			padding-left: 10px;
			margin-right: auto;
			margin-left: auto;
			background-color: #dff0d8;
		}

		.alertCustom {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 5px;
		}
		.DivBottomBorder
		{
			border-bottom: 1px solid #cccccc;
		}
	</style>--%>

	<script type="text/javascript">
		$('input[type=text]').keypress(function (e) {
			var regex = new RegExp("^[a-zA-Z0-9+-@]+$");
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			else {
				if (e.which == "32" || e.which == "8" || e.which == "0") {
					return true;
				}
				else {
					e.preventDefault();
					return false;
				}		
			}		
		});		
		</script>

	<asp:HiddenField ID="hdfGC" runat="server" />
	<!-- PAGE CONTENT ............................................................................................................................... -->
	<div id="divMainContent">
		<section id="CHECKOUT">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="pageTitle"><span id="spnPageTitle" runat="server"></span></h1>
					</div>
				</div>
				<div class="row">
					<div id="divAddressDetails" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" runat="server">
						<div id="jump1"></div>
						 <p id="pBillingTitle" class="pageText" style="display:none"><span id="spanBillingTitle" runat="server"></span></p>
						<div class="header pageHeading" id="header1">1.<span id="spnRegisterAddress" runat="server"></span></div>
						<div id="header1_space">
							  <div id="divHeader_Invoice" runat="server" visible="false">
								<div class="form-group bolder_weight">
									<asp:Literal ID="ltrInvoiceAddress" runat="server"/>
								</div>
							   <div class="form-group customLabel">
								<asp:Literal ID="ltrInvoiceCountry" runat="server" />
								<asp:DropDownList ID="ddlInvoiceCountry" AutoPostBack="true" runat="server" class="form-control input-sm customInput" OnSelectedIndexChanged="ddlInvoiceCountry_SelectedIndexChanged">
								</asp:DropDownList>
							</div>
								<div class="form-group customLabel">
								<asp:Literal ID="ltrLegalEntity" runat="server"/>
								<asp:DropDownList ID="ddlLegalEntity" runat="server"  AutoPostBack="true" class="form-control input-sm customInput" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
								</asp:DropDownList><asp:HiddenField ID="hiddenLegal" runat="server" />
							</div>
							<div class="form-group bolder_weight">
								   <br />
								</div>
							</div>
							<div class="form-group">
								<asp:Literal ID="Literal1" runat="server" />
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrColumnName" runat="server" />
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrContact_Name" runat="server" />
								<input type="text" id="txtContact_Name" maxlength="100" runat="server" class="form-control input-sm customInput">
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrCompany" runat="server" />
								<input type="text" id="txtCompany" runat="server" maxlength="100" class="form-control input-sm customInput">
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrAddress1" runat="server" />
								<input type="text" id="txtAddress1" runat="server" maxlength="200" class="form-control input-sm customInput">
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrAddress2" runat="server" />
								<input type="text" id="txtAddress2" runat="server" maxlength="200" class="form-control input-sm customInput">
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrTown" runat="server" />
								<input type="text" id="txtTown" runat="server" maxlength="100" class="form-control input-sm customInput">
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrState__County" runat="server" />
								<input type="text" id="txtState__County" runat="server" maxlength="100" class="form-control input-sm customInput">
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrPostalCode" runat="server" />
								<input type="text" id="txtPostal_Code" runat="server" maxlength="20" class="form-control input-sm customInput">
							</div>
							<div class="form-group" id="divCountry" runat="server">
								<asp:Literal ID="ltrCountry" runat="server" />
								<asp:DropDownList ID="ddlCountry" runat="server" class="form-control input-sm customInput">
								</asp:DropDownList>
							</div>
							<div class="form-group">
								<asp:Literal ID="ltrPhone" runat="server" />
								<input type="text" id="txtPhone" runat="server" maxlength="15" class="form-control input-sm customInput">
							</div>
						   <div class="form-group customLabel" id="divTaxNo" runat="server" visible="false">
								<asp:Literal ID="ltrTaxNo" runat="server" />
								<input type="text" id="txtTaxNo" runat="server" class="form-control input-sm customInput">
							</div>
							<div class="checkbox">
								<label class="customLabel">
									<input type="checkbox" id="addressCheckbox" runat="server" onchange="addressCheckboxChanged();" checked  /><span id="lblChkAddress" runat="server"></span>
								</label>
							</div>
							<div id="collapseAddress" class="collapse" style="margin-top: 15px; margin-bottom: 15px">
								<p id="pDeliveryTitle" class="pageText"><span id="spnDelivery_Address" runat="server"></span></p>
								<div class="form-group">
									<label class="customLabel"><span id="spnDelivery_AddressTitle" runat="server"></span></label>
									<asp:DropDownList ID="ddlDAddressTitle" runat="server" class="form-control input-sm customInput">
									</asp:DropDownList>
									<input type="text" id="txtAddressTitle" maxlength="50" runat="server" />
									<label style="display: none;" id="lblSetDefault" runat="server">
										<%-- set as default--%>
										<input type="checkbox" id="chkIsDefault" runat="server" /></label>
								</div>
								<div class="form-group">
									<asp:Literal ID="ltrDContact_Name" runat="server" />
									<input type="text" id="txtDContact_Name" runat="server" maxlength="100" class="form-control input-sm customInput">
								</div>
								<%-- Below Code added by SHRIGANESH 27 July 2016 to display company field in Delivery Address section START--%>
								<div class="form-group">
									<asp:Literal ID="ltrDCompany_Name" runat="server"></asp:Literal>
									<input type="text" id="txtDCompany_Name" runat="server" maxlength="100" class="form-control input-sm customInput">
								</div>
								<%-- Below Code added by SHRIGANESH 27 July 2016 to display company field in Delivery Address section END--%>
								<div class="form-group">
									<asp:Literal ID="ltrDAddress1" runat="server" />
									<input type="text" id="txtDAddress1" runat="server" maxlength="200" class="form-control input-sm customInput">
								</div>
								<div class="form-group">
									<asp:Literal ID="ltrDAddress2" runat="server" />
									<input type="text" id="txtDAddress2" runat="server" maxlength="200" class="form-control input-sm customInput">
								</div>
								<div class="form-group">
									<asp:Literal ID="ltrDTown" runat="server" />
									<input type="text" id="txtDTown" runat="server" maxlength="100" class="form-control input-sm customInput">
								</div>
								<div class="form-group">
									<asp:Literal ID="ltrdState__County" runat="server" />
									<input type="text" id="txtDState__County" runat="server" maxlength="100" class="form-control input-sm customInput">
								</div>
								<div class="form-group">
									<asp:Literal ID="ltrDPostalCode" runat="server" />
									<input type="text" id="txtDPostal_Code" runat="server" maxlength="20" class="form-control input-sm customInput">
								</div>
								<div class="form-group">
									<asp:Literal ID="ltrDCountry" runat="server" />
									<asp:DropDownList ID="ddlDCountry" runat="server" class="form-control input-sm customInput">
									</asp:DropDownList>
								</div>
								<div class="form-group">
									<asp:Literal ID="ltrDPhone" runat="server" />
									<input type="text" id="txtDPhone" runat="server" maxlength="15" class="form-control input-sm customInput">
								</div>
							</div>
							<!-- COLLAPSE END -->
						</div>
					</div>
					<div id="divShipPayMethod" class="col-xs-12 col-sm-12 col-md-4 col-lg-4" runat="server">
						<div id="jump2"></div>
						<div class="header pageHeading" id="header2" runat="server">2. <span id="spnShipping_Method" runat="server"></span></div>
						<div id="header2_space" runat="server">

							<div class="checkout_shipping_outer" id="divService1Cont" runat="server">
								<div class="checkout_shipping_left">
									<label class="radio-inline pageSmlText">
										<input onclick="calculatefreight(this)" type="radio" name="optionsRadios" id="rbService1" runat="server" value="1">
										<span id="spnService1Text" runat="server" class="bolder_weight"></span>
									</label>
								</div>
								<div class="checkout_shipping_right pageSmlText"><%=strCurrencySymbol %><span id="spnService1" runat="server"></span></div>
								<div class="checkout_shipping_left"><span id="spncustom1text" runat="server" class="radio-inline pageSmlText" ></span></div>
							</div>

							<div class="checkout_shipping_outer" id="divService2Cont" runat="server">
								<div class="checkout_shipping_left">
									<label class="radio-inline pageSmlText">
										<input onclick="calculatefreight(this)" type="radio" name="optionsRadios" id="rbService2" runat="server" value="2">
										<span class="bolder_weight" id="spnService2Text" runat="server"></span>
									</label>
								</div>
								<div class="checkout_shipping_right pageSmlText"><%=strCurrencySymbol %><span id="spnService2" runat="server"></span></div>
								<div class="checkout_shipping_left"><span id="spncustom2text" runat="server" class="radio-inline pageSmlText" ></span></div>
							</div>

							<!-- SHRIGANESH 11 Nov 2016 START -->
							<div class="checkout_shipping_outer" id="divService3Cont" runat="server">
								<div class="checkout_shipping_left">
									<label class="radio-inline pageSmlText">
										<input onclick="calculatefreight(this)" type="radio" name="optionsRadios" id="rbService3" runat="server" value="3">
										<span class="bolder_weight" id="spnService3Text" runat="server"></span>
									</label>
								</div>
								<div class="checkout_shipping_right pageSmlText"><%=strCurrencySymbol %><span id="spnService3" runat="server"></span></div>
								<div class="checkout_shipping_left"><span id="spncustom3text" runat="server" class="radio-inline pageSmlText" ></span></div>
							</div>
							<!-- SHRIGANESH 11 Nov 2016 END -->
							 
							<div class="checkout_shipping_outer pageSmlText" id="divCustomMessage" runat="server" style="display: none;"></div>
							<div class="checkout_shipping_outer pageSmlText" id="divMOVMessage" runat="server" style="display: none;"></div>
							<div class="checkout_shipping_outer pageSmlText" id="divDutyMessage" runat="server" style="display: none;"></div>
						</div>
						<div id="jump3"></div>
						<div class="header pageHeading" id="header3">3. <span id="spnPaymentMethod" runat="server"></span></div>
						<div id="header3_space">
							<div id="dvApplyGiftCertificate" runat="server" class="form-group" style="margin-bottom: 15px;" visible="false">
								<label id="lblGC" runat="server" class="customLabel"></label>
								<asp:TextBox ID="txtGiftCertificateCode" runat="server" CssClass="form-control input-sm customInput" MaxLength="50"></asp:TextBox>
								<div class="clearfix" style="margin-top: 15px; margin-bottom: 15px">
									<asp:Button ID="btnApplyGiftCertificate" runat="server" CssClass="btn btn-primary pull-right customActionBtn" Text="Apply" OnClick="btnApplyGiftCertificate_Click" />
									<asp:Button ID="btnCancelGiftCertificate" runat="server" CssClass="btn btn-primary pull-right customActionBtn" Text="Cancel" OnClick="btnCancelGiftCertificate_OnClick" />
								</div>
							</div>
							<div style="margin-bottom: 15px" class="form-group">
								<asp:DropDownList ID="ddlPaymentTypes" AutoPostBack="false" OnSelectedIndexChanged="ddlPaymentTypes_SelectedIndexChanged" runat="server" class="form-control input-sm">
								</asp:DropDownList>
							</div>
							<asp:Table ID="tblUDFS" runat="server" BorderWidth="0" Visible="false"
								CellPadding="2" CellSpacing="0" Width="100%" HorizontalAlign="Center">
							</asp:Table>
							<%-- Added By Snehal - for Customer Ref START--%>
							<asp:Table ID="tblCustRefUDFS" runat="server" BorderWidth="0" Visible="false"
								CellPadding="2" CellSpacing="0" Width="100%" HorizontalAlign="Center">
							</asp:Table>
							 <%-- END --%>
							<div class="form-group" style="margin-bottom: 15px">
								<label class="customLabel" id="lblInstruction" runat="server"></label>
								<textarea rows="4" id="txtInstruction" runat="server" maxlength="200" class="form-control input-sm customInput"></textarea>
							</div>
							<div class="form-group" style="margin-bottom: 15px; display: none;">
								<label class="customLabel" id="lblUploadFile" runat="server"></label>
								<span class="customLabel">(Upload only '.jpg', '.jpeg', '.gif', '.png', '.ai', '.pdf', '.eps')</span>
								<asp:FileUpload ID="fuDoc" runat="server" class="form-control" />
							</div>
							<div id="dvApplyCode" runat="server" class="form-group" style="margin-bottom: 15px">
								<label class="customLabel" id="lblCouponCode" runat="server"></label>
								<input type="text" class="form-control input-sm customInput" maxlength="50" id="txtCouponCode" runat="server">
								<div class="clearfix" style="margin-top: 15px; margin-bottom: 15px">
									<input type="button" class="btn btn-primary pull-right customActionBtn" id="btnApplyCoupon" runat="server" /><%--value="Apply Coupon"--%>
								</div>
							</div>
							<div id="dvRemoveCode" class="form-group" style="margin-bottom: 15px">
								<label class="customLabel" id="lblSucCoupon" runat="server"></label>
								<input type="button" class="btn btn-primary pull-right customActionBtn" id="btnRemoveCoupon" runat="server" /><%-- value="Remove Coupon"--%>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div id="jump4"></div>
						<div class="header pageHeading" id="header4">4. <span id="spnReviewNConfirm" runat="server"></span></div>
						<div id="header4_space">
							<asp:Repeater ID="rptBasketListing" runat="server" OnItemDataBound="rptBasketListing_ItemDataBound">
								<ItemTemplate>
									<div id="dvBasketRowContainer" runat="server" class="checkout_product_top standard_basket_row ">
										<div class="checkout_product_top">
											<div class="checkout_product_left">
												<img id="imgProduct" runat="server" class="checkout_product_image">
											</div>

											<div id="divSKUCode" runat="server" class="checkout_product_title pageSmlText" visible="false">
												<%# DataBinder.Eval(Container.DataItem, "SKU") %><br>
											</div>
											<div id="divSKUName" runat="server" class="checkout_product_title pageSmlText" visible="false">
												<%# DataBinder.Eval(Container.DataItem, "ProductName") %><br>
											</div>
											<div id="dvProductName" runat="server" class="checkout_product_title pageSmlText">
												<%# DataBinder.Eval(Container.DataItem, "SKU") %><br>
												<%# DataBinder.Eval(Container.DataItem, "ProductName") %>
											</div>
										</div>
										<div class="checkout_product_bottom">
											<div class="checkout_product_left">
												<%-- <input style="text-align: center" value='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'
													id="txtQuantity" runat="server" disabled="disabled" class="form-control quantity">--%>
												<span id="txtQuantity" runat="server" class="quantity"><%# DataBinder.Eval(Container.DataItem, "Quantity") %>
												</span>
											</div>
											<div class="checkout_product_prices pageSmlText">
												<span id="dvUnitPrice" runat="server"></span>
												<span id="dvTotalPrice" runat="server"></span>
											</div>
										</div>
										<input type="hidden" id="hdnInventory" runat="server" value="0" />
										<input type="hidden" id="hdnStockStatus" runat="server" value="0" />
										<input type="hidden" id="hdnShoppingCartProductId" runat="server" />
									</div>
								</ItemTemplate>
								<FooterTemplate>
									<div class="checkout_totals_outer">
										<div class="checkout_totals_left pageSmlText"><span id="spnBsktSubTotal" runat="server"></span></div>
										<div id="dvTotal" runat="server" class="checkout_totals_right pageSmlText"></div>
									</div>
									<div class="checkout_totals_outer" id="divShipping" runat="server">
										<div class="checkout_totals_left pageSmlText"><%=strShipping%></div>
										<div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><span id="spnShippingPrice" runat="server">0</span></div>
									</div>
									<div class="checkout_totals_outer" id="divaddtionalcharges" runat="server" >
										<div class="checkout_totals_left pageSmlText"><%=strAddtionalcharges%></div>
										<div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><span id="spnadditionalcharge" runat="server">0</span></div>
									</div>
									  <div class="checkout_totals_outer" id="divtotalnet" runat="server" style="display:none;" >
										<div class="checkout_totals_left pageSmlText"><%=strtotalnet%></div>
										<div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><span id="spntotalnet" runat="server">0</span></div>
									</div>
									<div class="checkout_totals_outer" id="divTax" runat="server">
										<div class="checkout_totals_left pageSmlText"><%=strTax%> </div>
										<div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><span id="spnTaxPrice" runat="server">0</span><%=strPointsText%></div>
									</div>
									<div class="checkout_totals_outer" id="divGiftCertificate" runat="server">
										<div class="checkout_totals_left bolder_weight pageSmlText"><%=strGiftCertificateTitle%></div>
										<div class="checkout_totals_right bolder_weight pageSmlText"><%= strCurrencySymbol %><span id="spnGiftCertificateAmount" runat="server"></span><%=strPointsText %></div>
									</div>
									<div class="checkout_totals_outer">
										<div class="checkout_totals_left bolder_weight pageSmlText"><%=strTotal_Text%></div>
										<div class="checkout_totals_right bolder_weight pageSmlText"><%= strCurrencySymbol %><span id="spnTotalPrice" runat="server"></span><%=strPointsText %></div>
									</div>
									 <div class="checkout_totals_outer">
									   <div class="checkout_totals_left pageSmlText"><span id="spnbudget" runat="server"></span></div>
									 <div id="dvbudget" runat="server" class="checkout_totals_right pageSmlText"></div>
								</div>

									<%--<div class="checkout_totals_outer" id="divIncludVAT" runat="server">
										<div class="checkout_totals_left bolder_weight pageSmlText"><%=str_Order_Total_Include_VAT%></div>
										<div class="checkout_totals_right bolder_weight pageSmlText"><asp:LinkButton ID="lnkModalPopup"  runat="server" OnClick="lnkModalPopup_Click"><%= str_See_Details%></asp:LinkButton>
										  </div>
									</div>--%>
									<div class="checkout_totals_outer">
										<%--Added By Hardik "05/Oct/2016"--%>
										<asp:Button class="btn btn-primary customActionBtn" ID="aEmail" runat="server" OnClick="aEmail_ServerClick"/>
										<asp:Button class="btn btn-primary pull-right customActionBtn" ID="aProceed" runat="server" OnClick="aProceed_ServerClick"/>
									</div>
								</FooterTemplate>
							</asp:Repeater>
							<div class="checkout_totals_outer pageSmlText" id="divStatictext" runat="server"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>


   <%-- <div class="modal fade" id="ModalOrdeTotal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<!-- MODAL DIALOG -->
		<div class="modal-dialogCustom ">
			<!-- MODAL CONTENT -->
			<div class="modal-content customModal">
				<div class="modal-headerCustom">
					<button type="button" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title pageSubSubTitle"><i class="fa fa-2x text-success"></i></h4>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-bodyCustom">
					<div class="container-fluidCustom">
						<div class="row product_outer">
							<div class="alertCustom alert-success text-center fade in">
								<h4><span id="SuccessMessage"><%= str_Generic_Order_Summary%></span></h4>
							
								<div class="checkout_totals_outer" id="divTax" runat="server">
									<div class="checkout_totals_left pageSmlText"><%= strGeneric_Items%> : </div>
									<div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><asp:Label ID="lblItemPrice" runat="server" Text=""></asp:Label></div>
								</div>
							  <div class="checkout_totals_outer DivBottomBorder" id="div1" runat="server">
									<div class="checkout_totals_left pageSmlText"><%= str_Generic_Postage_Packing%> : </div>
									<div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><asp:Label ID="lblPostagePacking" runat="server" Text=""></asp:Label></div>
								</div>
									<div class="checkout_totals_outer" id="div2" runat="server">
									<div class="checkout_totals_left pageSmlText"><%= str_Generic_Total_Before_VAT%> : </div>
									<div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><asp:Label ID="lblTotalBeforeVAT" runat="server" Text=""></asp:Label></div>
								</div>
							  <div class="checkout_totals_outer DivBottomBorder" id="div3" runat="server">
									<div class="checkout_totals_left pageSmlText"><%= str_Generic_VAT%> : </div>
									<div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><asp:Label ID="lblVAT" runat="server" Text=""></asp:Label></div>
								</div>
									  <div class="checkout_totals_outer" id="div4" runat="server">
									<div class="checkout_totals_left pageSmlText"><b><%= str_Generic_Order_Total%> : </b></div>
									<div class="checkout_totals_right pageSmlText"><b><%= strCurrencySymbol %><asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></b></div>
								</div>
							</div>
						</div>
					</div>
					</div>
					<!-- CONTAINER END -->
				</div>
				<!-- MODAL BODY END -->
				<div class="modal-footer">
					<button type="button" class="btn btn-success center-block customActionBtn" id="btnSuccessOK" data-dismiss="modal"></button>
				</div>
			</div>
			<!-- MODAL CONTENT END -->
		</div>--%>
		<!-- MODAL DIALOG END -->
	</div>

	<input type="hidden" id="hidShoppingCartProductId" value="0" />
	<input type="hidden" value="" id="hidDeliveryAddressId" runat="server" />
	<input type="hidden" value="0" id="hidService1" runat="server" />
	<input type="hidden" value="0" id="hidService2" runat="server" />
	<input type="hidden" value="0" id="hidService3" runat="server" /> <!-- SHRIGANESH 11 Nov 2016 -->
	<input type="hidden" value="0" id="hidService1Tax" runat="server" />
	<input type="hidden" value="0" id="hidService2Tax" runat="server" />
	<input type="hidden" value="0" id="hidService3Tax" runat="server" /> <!-- SHRIGANESH 11 Nov 2016 -->
	<input type="hidden" value="" id="hidCouponCode" runat="server" />
   <input type="hidden" value="" id="hidePrice" runat="server" />
   <input type="hidden" ID="hdncustom1" runat="server" />
	<input type="hidden" ID="hdncustom2" runat="server" />
	 <input type="hidden" ID="hdncustom3" runat="server" />
	<input type="hidden" value="" id="hdisLegalEntity" runat="server" />
	<script type="text/javascript">
		var host = "<%=host %>";
		var intCurrencyId = "<%=intCurrencyId %>";
		var bDeliveryAddress = "<%=bDeliveryAddress %>";
		var bBackOrderAllowed = "<%=bBackOrderAllowed %>";
		var bBASysStore = "<%=bBASysStore %>";  
		var pIsPointsEnbled = "<%=pIsPointsEnbled %>"; 
		var intLanguageId = "<%=intLanguageId %>";
		var strCurrencySymbol = "<%=strCurrencySymbol %>";
		var IsPointsEnbledPoints = "<%=IsPointsEnbledPoints %>";
		var stValid="<%=stValid %>";  
		var Atitle="<%=strBasket_Title_Text %>";
		var ASC_Payment_Types="<%=strSC_Payment_Types %>";
		var ASC_Special_Instruction="<%=strSC_Special_Instruction %>";
		var ABasket_Quantity_Not_More_Than_Stock_Message="<%=strBasket_Quantity_Not_More_Than_Stock_Message %>";
		var ARequired="<%=strRequired %>";
		var aSC_Reapply_Coupon="<%=strSC_Reapply_Coupon %>";
		var aSC_Require_Coupon ="<%=strSC_Require_Coupon %>";
		var astrSC_Invalid_Coupon_Code ="<%=strSC_Invalid_Coupon_Code %>";        
		var astrSC_Place_Order ="<%=strSC_Place_Order %>" ;       
		var astrSC_Enter_Card_Details ="<%=strSC_Enter_Card_Details %>";
		var astr_SessionGCAmount="<%=str_SessionGCAmount %>";
		var IsInvoiceAccountEnabled = "<%=IsInvoiceAccountEnabled%>";
		var IsTotalvalueZeroafterGC = "<%=IsTotalvalueZeroafterGC%>";
	    var IsMultifreight = "<%=IsMultifreight%>";
		var cultureCode= "<%=Culturedata%>";
	</script>
	<%--<script src="<%=host %>js/jquery.blockUI.js"></script>--%>
	<script src="<%=host %>js/checkout.js"></script>
</asp:Content>

