﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.ShoppingCart_OrderDetails" %>

<asp:Content ID="Content1" ClientIDMode="Static" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<asp:Literal ID="ltrGATrackingEcomm" runat="server"></asp:Literal>
	<section id="ORDER_CONFIRM_PAGE">
		<div class="container" id="divOrderDetails">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="pageTitle"><%=Generic_Order_Confirmation%></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<p class="pageText"><%=OrderDetails_Order_Placed_Thankyou_Note%></p>
					<p class="pageText"><%=OrderDetails_Order_Confirmation_Email_Note %></p>
				</div>
			</div>
			<div class="row" id="divGuestCheckout" runat="server">
				<div class="col-xs-12">
					<div class="well well-sm order_confirm_save_details">
						<div class="row">
							<div class="col-xs-12 col-md-5">
								<h4 class="pageSubSubTitle"><%=OrderDetails_SaveDetails_FutureUse_Note%></h4>
								<ul>
									<li class="pageText"><%=OrderDetails_CheckOut_Faster%></li>
									<li class="pageText"><%=Generic_View_Order_History%></li>
									<li class="pageText"><%=Generic_WriteReviews%></li>
								</ul>
							</div>
							<div class="col-xs-12 col-md-7">
								<h4 class="pageSubSubTitle"><%=Order_Details_Set_Password_Reset_Text%></h4>

								<div class="row">
									<div id="divUserType" visible="false" runat="server">
										<div class="col-sm-12">
											<div class="form-group">
												<asp:Panel ID="pnlUserType" runat="server">

													<div id="divddlUserType" class="row form-group" runat="server">
														<asp:Label ID="lblUserTypes" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
														<div class="col-sm-9 col-md-8">
															<asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
															<asp:RequiredFieldValidator ID="reqddlUserType" runat="server" ControlToValidate="ddlUserType" CssClass="text-danger" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit"></asp:RequiredFieldValidator>
														</div>
													</div>

												</asp:Panel>
											</div>
											<!-- OLD LOCATION OF pnlUserType, NOW MOVED TO FIRST PANEL BY SHRIGANESH SINGH 14 JULY 2016 -->
										</div>
									</div>
								</div>

								<div id="divCustomFields" runat="server" visible="false">

									<asp:Repeater ID="rptCustomMapping" runat="server" OnItemDataBound="rptUserTypeCustomFields_OnItemDataBound">
										<HeaderTemplate>
											<div id="divInnerCustomFields" class="form-group"></div>
										</HeaderTemplate>
										<ItemTemplate>
											<asp:Label ID="lblCustomFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
											<asp:HiddenField ID="hdnRegistrationFieldsConfigurationId" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
											<div class="col-sm-9 col-md-8 ">
												<asp:HiddenField ID="hdfControl" runat="server" />
												<asp:HiddenField ID="hdfColumnName" runat="server" />
												<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCustomFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" OnTextChanged="txtCustomFieldName_TextChanged" />
												<asp:DropDownList ID="ddlCustomValue" runat="server" CssClass="form-control">
												</asp:DropDownList>
												<asp:CheckBoxList ID="chkCustomValue" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
												<asp:RequiredFieldValidator ID="reqtxtCustomFieldName" runat="server" ControlToValidate="txtCustomFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
												<asp:RequiredFieldValidator ID="reqddlCustomValue" runat="server" ControlToValidate="ddlCustomValue" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
											</div>
										</ItemTemplate>
										<FooterTemplate>
										</FooterTemplate>
									</asp:Repeater>

									<asp:Repeater ID="rptUserTypeCustomFields" runat="server" OnItemDataBound="rptUserTypeCustomFields_OnItemDataBound">
										<HeaderTemplate>
											<div id="divInnerCustomFields">
										</HeaderTemplate>
										<ItemTemplate>
											<div class="form-group">
												<asp:Label ID="lblCustomFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
												<asp:HiddenField ID="hdnRegistrationFieldsConfigurationId" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
												<div class="col-sm-9 col-md-8 form-group">
													<asp:HiddenField ID="hdfControl" runat="server" />
													<asp:HiddenField ID="hdfColumnName" runat="server" />
													<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCustomFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" />
													<asp:DropDownList ID="ddlCustomValue" runat="server" CssClass="form-control">
													</asp:DropDownList>
													<asp:CheckBoxList ID="chkCustomValue" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
													<asp:RequiredFieldValidator ID="reqtxtCustomFieldName" runat="server" ControlToValidate="txtCustomFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
													<asp:RequiredFieldValidator ID="reqddlCustomValue" runat="server" ControlToValidate="ddlCustomValue" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
												</div>
											</div>
										</ItemTemplate>
										<FooterTemplate>
											</div>
										</FooterTemplate>
									</asp:Repeater>

									<asp:Repeater ID="rptCustomFieldsAll" runat="server" OnItemDataBound="rptUserTypeCustomFields_OnItemDataBound">
										<HeaderTemplate>
											<div id="divInnerCustomFields">
										</HeaderTemplate>
										<ItemTemplate>
											<asp:Label ID="lblCustomFieldName" runat="server" CssClass="col-sm-3 col-md-4 control-label customLabel"></asp:Label>
											<asp:HiddenField ID="hdnRegistrationFieldsConfigurationId" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
											<div class="col-sm-9 col-md-8 form-group">
												<asp:HiddenField ID="hdfControl" runat="server" />
												<asp:HiddenField ID="hdfColumnName" runat="server" />
												<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtCustomFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)" />
												<asp:DropDownList ID="ddlCustomValue" runat="server" CssClass="form-control">
												</asp:DropDownList>
												<asp:CheckBoxList ID="chkCustomValue" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
												<asp:RequiredFieldValidator ID="reqtxtCustomFieldName" runat="server" ControlToValidate="txtCustomFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
												<asp:RequiredFieldValidator ID="reqddlCustomValue" runat="server" ControlToValidate="ddlCustomValue" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
											</div>
										</ItemTemplate>
										<FooterTemplate>
											</div>
										</FooterTemplate>
									</asp:Repeater>
								</div>

								<div id="divGuestUserRegistrationDetails" runat="server" visible="true">

									<div class="form-group row clearfix" id="divUserTitles" runat="server" visible="false">
										<label class="control-label col-sm-3 col-md-4 customLabel lebal-align" for="name" visible="true" id="lblTitle" runat="server"></label>
										<div class="col-sm-9 col-md-8">
											<%--<asp:DropDownList ID="ddlTitles" Visible="true" runat="server" CssClass="form-control customInput">
											</asp:DropDownList>--%>
											<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtTitles" autocomplete="off" />
											<asp:RequiredFieldValidator ID="reqtxtTitles" runat="server" ControlToValidate="txtTitles" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
											<%--<asp:RequiredFieldValidator ID="rfvDdlTitle" runat="server" ControlToValidate="ddlTitles" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>--%>
										</div>
									</div>

									<div class="form-group row" id="divPreferredLanguage" runat="server" visible="false">
										<asp:Label ID="lblPreferredLanguage" CssClass="col-sm-3 col-md-4 control-label customLabel" Visible="true" runat="server"></asp:Label>
										<div class="col-sm-9 col-md-8">
											<asp:DropDownList ID="drpdwnPreferredLanguage" runat="server" Visible="true" CssClass="form-control customInput"></asp:DropDownList>
										</div>
									</div>

									<div class="form-group row" id="divPreferredCurrency" runat="server" visible="false">
										<asp:Label ID="lblPreferredCurrency" CssClass="col-sm-3 col-md-4 control-label customLabel" Visible="true" runat="server"></asp:Label>
										<div class="col-sm-9 col-md-8">
											<asp:DropDownList ID="drpdwnPreferredCurrency" runat="server" Visible="true" CssClass="form-control customInput"></asp:DropDownList>
										</div>
									</div>
								</div>
								<br />

								<div class="form-group row">
									<label class="col-sm-3 col-md-4 control-label customLabel" for=""><%=Register_Password_Text%></label>
									<div class="col-sm-9 col-md-8">

										<%-- Below line commented by SHRIGANESH SINGH for Password Policy 16 May 2016 -- %>
										<%--<input type="password" placeholder="" id="txtPassword" runat="server" class="form-control customInput">--%>

										<!-- Code Added by SHRIGANESH SINGH for Password Policy START 16 May 2016  -->
										<asp:TextBox ID="txtPassword" autocomplete="off" runat="server" CssClass="form-control customInput" placeholder="Password" TextMode="Password"></asp:TextBox>
										<asp:RequiredFieldValidator ID="reqtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
										<div style="display: none">
											<asp:RegularExpressionValidator ID="regexPwdLentxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
											<asp:RegularExpressionValidator ID="regexAlphaNumtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
											<asp:RegularExpressionValidator ID="regexAlphaNumSymtxtPassword" runat="server" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
										</div>

										<asp:HiddenField ID="hdnPasswordPolicyType" runat="server" />
										<asp:HiddenField ID="hdnMinPasswordLength" runat="server" />
										<asp:Label ID="lblPasswordPolicyMessage" runat="server" CssClass="text-danger"></asp:Label>

										<!-- Code Added by SHRIGANESH SINGH for Password Policy END 16 May 2016 -->
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-md-4 control-label customLabel" for=""><%=Register_ConfirmPassword_Text%></label>
									<div class="col-sm-9 col-md-8">
										<!-- Below line commented by SHRIGANESH SINGH for Password Policy 16 May 2016 -->
										<%--<input type="password" placeholder="" id="txtConfirmPassword" runat="server" class="form-control customInput">--%>

										<!-- Code Added by SHRIGANESH SINGH for Password Policy START 16 May 2016 -->
										<asp:TextBox CssClass="form-control customInput" runat="server" ID="txtConfirmPassword" placeholder="Confirm Password" autocomplete="off" TextMode="Password"></asp:TextBox>
										<asp:RequiredFieldValidator ID="reqtxtConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
										<asp:CompareValidator ID="cmpValidatorPwd" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:CompareValidator>
										<!-- Code Added by SHRIGANESH SINGH for Password Policy START 16 May 2016 -->
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-4 control-label customLabel hidden-xs" for="">&nbsp;</label>
									<div class="col-sm-9 col-md-8 order_conform_save_button">
										<%--<a href="javascript:void(0);" id="aCreateAccount" runat="server" onserverclick="aCreateAccount_ServerClick" class="btn btn-primary customActionBtn"><%=Generic_Create_Account_Text%></a>--%>
										<asp:Button ID="btnCreateAccount" runat="server" OnClick="btnCreateAccount_OnClick" CssClass="btn btn-primary collapsed customActionBtn buttonmargin" ValidationGroup="OnClickSubmit" />
										<asp:ValidationSummary ID="vSumm" runat="server" DisplayMode="List" ShowMessageBox="true" ValidationGroup="OnClickSubmit" ShowSummary="false" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 order_confirm_order_number">
					<h3 class="pageSubTitle"><%=Generic_Order_Number_Text%><span class="bolder_weight">
						<asp:Literal ID="ltrOrderNo" runat="server" /></span>
						<a href="javascript:void(0);" id="aPrintOrder" onclick="Popup();" class="btn btn-primary customActionBtn pull-right noprint"><%=Print_Title%></a>

					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<asp:Repeater ID="rptBasketListing" runat="server" OnItemDataBound="rptBasketListing_ItemDataBound">
						<HeaderTemplate>
							<div class="order_confirm_titles">
								<div class="order_confirm_image customTableHead"><%=Product_Title%></div>
								<div class="order_confirm_text customTableHead"><%=Register_Delivery_Address_Title%></div>
								<div class="order_confirm_qty customTableHead"><%=Basket_Quantity_Text%></div>
								<div class="order_confirm_unit_price customTableHead"><%=Basket_Unit_Cost_Text%></div>
								<div class="order_confirm_line_total customTableHead"><%=Basket_Line_Total_Text%></div>
							</div>
						</HeaderTemplate>
						<ItemTemplate>
							<div class="order_confirm_row">
								<div class="order_confirm_image">
									<img id="imgProduct" runat="server" alt="" style="width: 60px;" class="image_bg">
								</div>
								<div class="order_confirm_text customTableText">
									<%# DataBinder.Eval(Container.DataItem, "SKU") %><br>
									<%# DataBinder.Eval(Container.DataItem, "ProductName") %>
								</div>
								<div class="order_confirm_qty customTableText"><%# DataBinder.Eval(Container.DataItem, "Quantity") %></div>
								<div id="dvUnitPrice" runat="server" class="order_confirm_unit_price customTableText"></div>
								<div id="dvTotalPrice" runat="server" class="order_confirm_line_total customTableText"></div>
							</div>
						</ItemTemplate>
						<FooterTemplate>
							<div class="order_confirm_totals_row">
								<div class="order_confirm_totals customTableText"><%=Generic_SubTotal%></div>
								<div id="dvSubTotal" runat="server" class="order_confirm_line_total customTableText"></div>
							</div>

							<div class="order_confirm_totals_row" id="divShipping" runat="server">
								<div class="order_confirm_totals customTableText"><%=Shipping%></div>
								<div id="dvShippingCharges" runat="server" class="order_confirm_line_total customTableText"></div>
							</div>
							<div class="order_confirm_totals_row" id="divaddtionalcharges" runat="server">
								<div class="order_confirm_totals customTableText"><%=strAddtionalcharges%></div>
								<div id="dvadditionalcharges" runat="server" class="order_confirm_line_total customTableText"></div>
							</div>
							<div class="order_confirm_totals_row" id="divTax" runat="server">
								<div class="order_confirm_totals customTableText"><%=Tax%></div>
								<div id="dvTaxes" runat="server" class="order_confirm_line_total customTableText"></div>
							</div>
							<div class="order_confirm_totals_row" id="divGC" runat="server">
								<div class="order_confirm_totals customTableText"><%=SiteLinks_GiftCertificateTitle%></div>
								<div id="dvGC" runat="server" class="order_confirm_line_total customTableText"></div>
							</div>
							<div class="order_confirm_grand_total">
								<div class="order_confirm_totals bolder_weight customTableText"><strong><%=Total%></strong></div>
								<div id="dvTotal" runat="server" class="order_confirm_line_total bolder_weight customTableText"><strong></strong></div>
							</div>
						</FooterTemplate>
					</asp:Repeater>
				</div>
			</div>

			<div id="divGiftCertificate" runat="server">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<div class="order_addresses_heading customTableHead"><%=SiteLinks_GiftCertificateTitle%></div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<div class="order_addresses_text customTableText">
							<asp:Literal ID="ltGiftCertificateCode" runat="server"></asp:Literal>
						</div>
					</div>
					<div class="col-xs-12 col-md-4 hidden">
						<asp:Literal ID="ltGiftCertificateAmount" runat="server" />
					</div>
				</div>
			</div>
			<div class="row">
				<ul>

					<li>
						<div class="col-xs-12 col-md-4">
							<div class="order_addresses_heading customTableHead"><%=Generic_Billing_Address%></div>
							<div class="order_addresses_text customTableText">
								<asp:Literal ID="ltrBillingAddress" runat="server"></asp:Literal>
							</div>
						</div>
					</li>
					<li>
						<div class="col-xs-12 col-md-4">
							<div class="order_addresses_heading customTableHead"><%=Generic_DeliveryAddress%></div>
							<div class="order_addresses_text customTableText">
								<asp:Literal ID="ltrDeliveryAddress" runat="server"></asp:Literal>
							</div>
						</div>
					</li>
					<li>
						<div class="col-xs-12 col-md-4">
							<div class="order_addresses_heading customTableHead"><%=SC_Payment_Method%></div>
							<div class="order_addresses_text customTableText">
								<asp:Literal ID="ltrPaymentMethod" runat="server"></asp:Literal>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<a id="aPrintOrder" href="javascript:void(0);" onclick="Popup();" class="btn btn-primary customActionBtn pull-right noprint">Print</a>
				</div>
			</div>
		</div>
		<!-- COTAINER END -->
	</section>

	<style>
		@media print {
			.footerBg {
				display: none !important;
			}
		}
	</style>

	<script type="text/javascript">
		function Popup() {
			window.print();
		}
		// Region Added by SHRIGANESH SINGH 16 May 2016 for Password Policy START
		$(document).ready(function () {

			$("#txtPassword").blur(function () {
				debugger;
				var MinimumPasswordLength = $('#hdnMinPasswordLength').val();
				var PasswordPolicyType = $('#hdnPasswordPolicyType').val();
				var password = $("#txtPassword").val();
				var passwordLength = $("#txtPassword").val().length;
				var MinPassLenErrorMsg = '<%=strMinPassLenErrorMsg%>';
				var AlphaNumPassReqErrorMsg = '<%=strAlphaNumPassReqErrorMsg%>';
				var AlphaNumSymPassReqErrorMsg = '<%=strAlphaNumSymPassReqErrorMsg%>';

				if (passwordLength != 0) {
					if (passwordLength < MinimumPasswordLength) {
						$("#ContentPlaceHolder1_lblPasswordPolicyMessage").show();
						$("#lblPasswordPolicyMessage").text(MinPassLenErrorMsg);
					} else {

						$("#lblPasswordPolicyMessage").hide();

						// Validate against Alpha Numeric Regular Expression
						if (PasswordPolicyType == "Must Contain Alpha Numeric") {
							var AlphaNumRegex = /^(?=(.*\d){1})(?=.*[a-zA-Z])[0-9a-zA-Z]/;
							if (AlphaNumRegex.test(password)) {
							}
							else {
								$("#lblPasswordPolicyMessage").show();
								$("#lblPasswordPolicyMessage").text(AlphaNumPassReqErrorMsg);
							}
						}

							// Validate against Alpha Numeric and Symbol Regular Expression
						else if (PasswordPolicyType == "Must contain Alphabets Numbers and Symbols") {
							var AlphaNumSymRegex = /^(?=(.*\d){1})(?=.*[a-zA-Z])(?=.*[!@#$%&.+=*)(_-])[0-9a-zA-Z!@#$%&.+=*)(_-]/;

							if (AlphaNumSymRegex.test(password)) {
							}
							else {
								$("#lblPasswordPolicyMessage").show();
								$("#lblPasswordPolicyMessage").text(AlphaNumSymPassReqErrorMsg);
							}
						}

					}
				}
			});
		});
		// Region Added by SHRIGANESH SINGH 16 May 2016 for Password Policy END

	</script>
</asp:Content>
