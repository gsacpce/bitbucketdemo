﻿<%@ Page Language="C#" AutoEventWireup="true"  Inherits="Presentation.ShoppingCart_TeleCashRequest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     
    <form id="form" runat="server">
    <div>
       <input type="hidden" id="hash_algorithm"  name="hash_algorithm" runat="server" />
        <input type="hidden" id="storename"  name="storename" runat="server" />
        <input type="hidden" id="mode" name="mode" runat="server" />
        <input type="hidden" id="timezone" name="timezone" runat="server" />
        <input type="hidden" id="txntype"  name="txntype" runat="server" />
        <input type="hidden" id="currency"  name="currency" runat="server" />
        <input type="hidden" id="txndatetime" name="txndatetime" runat="server" />
        <input type="hidden" id="hash"  name="hash" runat="server" />
		 <input type="hidden" id="language" name="language" value="en_US"   runat="server" />
        <input type="hidden" id="responseSuccessURL" name="responseSuccessURL" runat="server" />
         <input type="hidden" id="responseFailURL"  name="responseFailURL" runat="server" />
         <input type="hidden" id="chargetotal"   runat="server" />
		  <input type="hidden" id="oid"   runat="server" />
        
    </div>
    </form>
     <asp:Literal ID="ltrForm" runat="server" Mode="PassThrough"></asp:Literal>
    <asp:Literal ID="ltrScript" runat="server" Mode="PassThrough"></asp:Literal>
</body>
</html>
