﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Orders_Order_ModalAjax" %>


<div id="divMainContent">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class=" customModal modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><%=Tracking_Details%></h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 tracking_spacer">
                            <div class="tracking_heading customTableHead"><%=Despatch_Number%></div>
                            <div class="tracking_text customTableText">
                                <asp:label id="lblMDespatchNumber" runat="server"></asp:label>
                            </div>
                            <div class="tracking_heading customTableHead"><%=Shipping_Date%></div>
                            <div class="tracking_text customTableText">
                                <asp:label id="lblShippingDate" runat="server"></asp:label>
                            </div>
                            <div class="tracking_heading customTableHead"><%=Shipping_Method%></div>
                            <div class="tracking_text customTableText">
                                <asp:label id="lblShippingMethod" runat="server"></asp:label>
                            </div>
                            <div class="tracking_heading customTableHead"><%=Tracking_Numbers%></div>
                            <div class="tracking_text ">
                                <a class="smlHyperLink">
                                    <asp:label id="lblTrackingNumber" runat="server"></asp:label>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 tracking_spacer">
                            <div class="tracking_heading customTableHead"><%=Delivery_Address%></div>
                            <div class="tracking_text customTableText">
                                <asp:label id="lblMName" runat="server"></asp:label>
                                <br />
                                <asp:label id="lblMCompany" runat="server"></asp:label>
                                <br />
                                <asp:label id="lblMAddress" runat="server"></asp:label>
                                <br />
                                <asp:label id="lblMCity" runat="server"></asp:label>
                                <br />
                                <asp:label id="lblMPostCode" runat="server"></asp:label>
                                <br />
                                <asp:label id="lblMCountry" runat="server"></asp:label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="pageSubSubTitle"><%=Generic_Item_in_Despatch%></h4>
                        </div>

                        <div class="col-xs-12" id="divModalItem">
                            <asp:repeater id="rptModalItem" runat="server" OnItemDataBound="rptModalItem_ItemDataBound">
                                        <HeaderTemplate>
                                            <div class="order_details_titles">
                                                <div class="order_details_line customTableHead tracking_line"><%=Line%></div>
                                                <div class="order_details_code customTableHead tracking_code"><%=Item_Code%></div>
                                                <div class="order_details_description customTableHead tracking_description"><%=Item_Description%></div>
                                                <div class="order_details_qty customTableHead tracking_qty"><%=Qty_Ordered%></div>
                                                <div class="order_details_shipped customTableHead tracking_shipped"><%=Generic_Quantity_Despatch%></div>
                                                <div class="order_details_backorder customTableHead tracking_backorder"><%=Backorder%>&nbsp;<a href="#" data-toggle="tooltip" data-placement="top" title="Backorder items are out of stock and will follow in a future shipment. Contact us if you need further information."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></div>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="order_details_row">
                                                <div class="order_details_xs_titles customTableHead customTableHead ">Line</div>
                                                <div class="order_details_line customTableText tracking_line">
                                                    <asp:Label ID="lblLine" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                                </div>
                                                <div class="order_details_xs_titles customTableHead ">Code</div>
                                                <div class="order_details_code customTableText tracking_code">
                                                    <asp:Label ID="lblCode" runat="server" Text='<%# Eval("ProductCode") %>'></asp:Label>
                                                </div>
                                                <div class="order_details_xs_titles customTableHead ">Description</div>
                                                <div class="order_details_description customTableText tracking_description">
                                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("ProductDescription") %>'></asp:Label>
                                                </div>
                                                <div class="order_details_xs_titles customTableHead ">Quantity</div>
                                                <div class="order_details_qty customTableText tracking_qty">
                                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("OrderQuantity") %>'></asp:Label>
                                                </div>
                                                <div class="order_details_xs_titles customTableHead ">Qty in this Despatch</div>
                                                <div class="order_details_shipped customTableText tracking_shipped">
                                                    <asp:Label ID="lblShipped" runat="server" ></asp:Label>
                                                </div>
                                                <div class="order_details_xs_titles customTableHead ">Backorder&nbsp;<a href="#" data-toggle="tooltip" data-placement="right" title="Backorder items are out of stock and will follow in a future shipment. Contact us if you need further information"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></div>
                                                <div class="order_details_backorder customTableText tracking_backorder">
                                                    <asp:Label ID="lblBackOrder" runat="server" Text='<%# Eval("BackOrderQuantity") %>'></asp:Label>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
