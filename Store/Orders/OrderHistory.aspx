﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Orders_OrderHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="divMainContent" class="my_orderdiv">
        <div id="ORDER_HISTORY_LIST_PAGE">
            <div class="container">
                <div class="row padd_toppx">
                    <div class="col-xs-12 col-sm-3">
                        <h1 class="pageTitle">
                            <span id="spOrder_History_Title" runat="server"></span>
                        </h1>
                    </div>
                    <div class="col-xs-12 col-sm-9" style="margin-top: 10px" id="dvOrderHistorySearch" runat="server">
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group form-group">
                                <asp:TextBox CssClass="form-control customInput" placeholder="Search for..." ID="txtSearch" runat="server" />
                                <span class="input-group-btn">
                                    <asp:Button CssClass="btn btn-primary customActionBtn" Text="Search orders" ID="btnSearch" runat="server" OnClientClick="return ValidateSearch();" OnClick="btnSearch_Click"></asp:Button>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlSearchOption" Width="100%" Height="33" CssClass="customInput" runat="server">
                                    <asp:ListItem Text="All" Value="all"></asp:ListItem>
                                    <asp:ListItem Text="Order Date" Value="date"></asp:ListItem>
                                    <asp:ListItem Text="Order Number" Value="number"></asp:ListItem>
                                    <asp:ListItem Text="Order Value" Value="value"></asp:ListItem>
                                    <asp:ListItem Text="Order Status" Value="status"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <!-- /input-group -->
                    </div>
                    <!-- /.col-lg-6 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="pagination pagination-sm" id="ulPager">
                            <asp:Repeater ID="rptPager" runat="server" OnItemCommand="rptPager_ItemCommand">
                                <ItemTemplate>
                                    <li id="liPage" runat="server">
                                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                            OnClick="Page_Changed" CommandName="clicked"></asp:LinkButton>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="order_list_titles" id="divOrders">
                            <asp:Repeater ID="rptOrders" runat="server" OnItemCommand="rptOrders_ItemCommand" OnItemDataBound="rptOrders_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="order_list_titles">
                                        <div class="order_list_date customTableHead"><%=Order_Date%></div>
                                        <%--Order Date--%>
                                        <div class="order_list_number customTableHead"><%=Order_Number%> <%--Order Number--%></div>
                                        <div class="order_list_address customTableHead"><%=Delivered_to%> <%--Delivered to--%></div>
                                        <div class="order_list_value customTableHead" id="divOrderValueHeader" runat="server"><%=Order_value%> <%--Order value--%></div>
                                        <div class="order_list_status customTableHead"><%=Order_status%> <%--Order_status--%></div>
                                        <div class="order_list_action customTableHead"><%=Action%> <%--Action--%></div>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="order_list_row">
                                        <div class="order_list_xs_titles customTableHead">Order date</div>
                                        <div class="order_list_date customTableText">
                                            <asp:Label ID="lblOrderDate" runat="server" Text='<%# Eval("OrderDate", "{0:dd MMMM yyy}") %>'></asp:Label>
                                        </div>
                                        <div class="order_list_xs_titles customTableHead">Order Number</div>
                                        <div class="order_list_number customTableText">
                                            <asp:Label ID="lblOrderNumber" runat="server" Text='<%# Eval("SalesOrderID") %>'></asp:Label>
                                        </div>
                                        <div class="order_list_xs_titles customTableHead">Delivered to</div>
                                        <div class="order_list_address customTableText">
                                            <asp:Label ID="lblDeliveredTo" runat="server" Text="Undefined"></asp:Label>
                                        </div>
                                        <div class="order_list_xs_titles customTableHead">Order value</div>
                                        <div class="order_list_value customTableText" id="divOrderValueText" runat="server">
                                            <%--<%= GlobalFunctions.GetCurrencySymbol() %><asp:Label ID="lblOrderValue" runat="server" Text='<%# String.Format("{0:f2}",DataBinder.Eval(Container.DataItem, "OrderValue"))%>'></asp:Label>--%>
                                             <asp:Label ID="lblOrderValue" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="order_list_xs_titles customTableHead">Order status</div>
                                        <div class="order_list_status customTableText">
                                            <asp:Label ID="lblOrderStatus" runat="server" Text='<%# Eval("OrderStatus") %>'></asp:Label>
                                        </div>
                                        <div class="order_list_xs_titles customTableHead">Action</div>
                                        <div class="order_list_action customTableText">
                                            <asp:Button CssClass="btn btn-primary customActionBtn" ID="btnView" runat="server" CommandName="ViewItem" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SalesOrderID") %>' />
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <SeparatorTemplate></SeparatorTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
                    <!-- COLUMN END -->
                </div>

                <div class="row" id="dvNoRecord" runat="server" style="min-height: 300px;">
                    <div class="col-xs-12">
                        <h2 class="text-center">
                            <asp:Label ID="lblNoRecord" runat="server"></asp:Label>
                        </h2>
                    </div>

                </div>
                <!-- ROW END -->
            </div>
            <!-- CONTAINER END -->
        </div>
    </div>
    <script>new WOW().init();</script>
    <script>

        function ValidateSearch() {
            debugger;
            var spn = document.getElementById('WarningMessage');
            var ddl = document.getElementById('<%= ddlSearchOption.ClientID %>');
            var txt = document.getElementById('<%= txtSearch.ClientID %>');
            if (txt.value == "") {
                if (ddl.value != "all") {
                    spn.innerHTML = 'Enter search text';
                    $('#myWarningModal').modal('show');
                    return false;
                }
            }
            else {
                if (ddl.value == "date") {
                    if (!isValidDate(txt.value)) {
                        spn.innerHTML = 'Date should be in mm/dd/yyyy format';
                        $('#myWarningModal').modal('show');
                        return false;
                    }
                }
                else if (ddl.value == "number" || ddl.value == "value") {
                    if (isNaN(txt.value)) {
                        spn.innerHTML = 'Only number allowed';
                        $('#myWarningModal').modal('show');
                        return false;
                    }
                }
            }
            return true;
        }

        function isValidDate(dateString) {
            // First check for the pattern
            if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            { return false; }
            // Parse the date parts to integers
            var parts = dateString.split("/");
            var day = parseInt(parts[1], 10);
            var month = parseInt(parts[0], 10);
            var year = parseInt(parts[2], 10);
            // Check the ranges of month and year
            if (year < 1000 || year > 3000 || month == 0 || month > 12)
            { return false; }
            var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            // Adjust for leap years
            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            { monthLength[1] = 29; }
            // Check the range of the day
            return day > 0 && day <= monthLength[month - 1];
        }

    </script>
</asp:Content>



