﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Orders_OrderItem" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
  .my-account-back-link a {color: #666666;text-decoration: none;font-weight: 600;font-size: 0.9em;}
</style>
    <div id="divMainContent">
        <section id="ORDER_HISTORY_ITEM_PAGE">
            <div class="container">
                <div class="row padd_toppx">
                    <div class="col-xs-12" id="divMyAccount" runat="server" clientidmode="Static">
                        <div class="my-account-back-link">
                            <a  href="<%=host %>Dashboard/BudgetTransaction.aspx"><i class="fa fa-long-arrow-left"></i><span id="spnBackToMyAcc" runat="server"></span></a>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <h1 class="pageTitle"><%=Order_details%></h1>
                    </div>


                    <div class="col-xs-6 pageText" style="text-align: right">
                        <%=Order_Number%> &nbsp;
                <ul style="margin-top: 0px; margin-bottom: 0" class="pagination orderItemPagination pagination-sm pull-right">
                    <li>
                        <asp:Button ID="btnPrevious" runat="server" Text="&laquo;" CssClass="customPagination leftArrow" OnClick="btnPrevious_Click" />

                    </li>
                    <li class="active">
                        <asp:Label ID="lblCurrentId" CssClass="customActionBtn" runat="server"></asp:Label>
                        
                    </li>
                    <li>
                        <asp:Button ID="btnNext" runat="server" Text="&raquo;" CssClass="customPagination rightArrow" OnClick="btnNext_Click" />
                    </li>
                </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 order_details_spacer">
                        <div class="order_addresses_heading customTableHead"><%=Payment_Method%> </div>
                        <div class="order_addresses_text customTableText">
                            <asp:Label ID="lblPaymentMethod" runat="server"></asp:Label>
                        </div>
                        <div class="order_addresses_heading customTableHead"><%=Reference%>  </div>
                        <div class="order_addresses_text customTableText">
                            <asp:Label ID="lblCustomerRef" runat="server"></asp:Label>
                        </div>
                        <div class="order_addresses_heading customTableHead"><%=Tracking_Numbers%></div>
                        <div class="order_addresses_text" id="divTrackingNumber">
                            <asp:Repeater ID="rptTrackingNumber" runat="server">
                                <ItemTemplate>
                                    <a data-toggle="modal" class="smlHyperLink" href="javascript:void(0);" onclick='<%# String.Format("OpenModalUp(\"{0}\")", Eval("ConsignmentNumbers")) %>'><%# Eval("ConsignmentNumbers") %></a><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 order_details_spacer">
                        <div class="order_addresses_heading customTableHead">
                            <%=Delivery_Address%>
                        </div>
                        <div class="order_addresses_text customTableText">
                            <asp:Label ID="lblDName" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblDCompany" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblDAddress" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblDCity" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblDPostCode" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblDCountry" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 order_details_spacer">
                        <div class="order_addresses_heading customTableHead"><%=Invoice_Address%></div>
                        <div class="order_addresses_text customTableText">
                            <%--Accounts Payable<br> *Google Samples GBP Account<br> Brand Addition<br> 9 Albert Embankment<br> London<br> SE177SP<br> United Kingdom--%>
                            <asp:Label ID="lblRName" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblRCompany" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblRAddress" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblRCity" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblRPostCode" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblRCountry" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <asp:Repeater ID="rptProductItem" runat="server" OnItemDataBound="rptProductItem_ItemDataBound" OnItemCommand="rptProductItem_ItemCommand">
                            <HeaderTemplate>
                                <div class="order_details_titles">
                                    <div class="order_details_line customTableHead"><%=Line%> </div>
                                    <div class="order_details_code customTableHead"><%=Item_Code%></div>
                                    <div class="order_details_description customTableHead"><%=Item_Description%></div>
                                    <div class="order_details_qty customTableHead"><%=Quantity%> </div>
                                    <div class="order_details_shipped customTableHead"><%=Shipped%> </div>
                                    <div class="order_details_backorder customTableHead"><%=Backorder%> &nbsp;<a href="#" data-toggle="tooltip" data-placement="top" title="Backorder items are out of stock and will follow in a future shipment. Contact us if you need further information."><span class=" glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></div>
                                    <div class="order_details_unit_price customTableHead"><%=Unit_Price%> </div>
                                    <div class="order_details_line_total customTableHead"><%=Line_total%></div>
                                    <div class="order_details_action customTableHead"><%=Action%> </div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="order_details_row">
                                    <div class="order_details_xs_titles customTableHead customTableHead">Line</div>
                                    <div class="order_details_line customTableText">
                                        <asp:Label ID="lblLine" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                    </div>
                                    <div class="order_details_xs_titles customTableHead">Code</div>
                                    <div class="order_details_code customTableText">
                                        <asp:Label ID="lblCode" runat="server" Text='<%# Eval("ProductCode") %>'></asp:Label>
                                    </div>
                                    <div class="order_details_xs_titles customTableHead">Description</div>
                                    <div class="order_details_description customTableText">
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("ProductDescription") %>'></asp:Label>
                                    </div>
                                    <div class="order_details_xs_titles customTableHead">Quantity</div>
                                    <div class="order_details_qty customTableText">
                                        <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("OrderQuantity") %>'></asp:Label>
                                    </div>
                                    <div class="order_details_xs_titles customTableHead">Shipped</div>
                                    <div class="order_details_shipped customTableText">
                                        <asp:Label ID="lblShipped" runat="server"></asp:Label>
                                    </div>
                                    <div class="order_details_xs_titles customTableHead">Backorder&nbsp;<a href="#" data-toggle="tooltip" data-placement="right" title="Backorder items are out of stock and will follow in a future shipment. Contact us if you need further information"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></div>
                                    <div class="order_details_backorder customTableText">
                                        <asp:Label ID="lblBackOrder" runat="server" Text='<%# Eval("BackOrderQuantity") %>'></asp:Label>
                                    </div>
                                    <div class="order_details_xs_titles customTableHead">Unit Price</div>
                                    <div class="order_details_unit_price customTableText">
                                        <%=strCurrencySymbol %>
                                        <asp:Label ID="lblPrice" runat="server" Text='<%# String.Format("{0:f2}",DataBinder.Eval(Container.DataItem, "UnitPrice"))%>'></asp:Label>

                                    </div>
                                    <div class="order_details_xs_titles customTableHead">Line total</div>
                                    <div class="order_details_line_total customTableText"><%=strCurrencySymbol %><asp:Label ID="lblLineTotal" runat="server" Text='<%# Eval("LineValue") %>'></asp:Label></div>
                                    <div class="order_details_action">
                                        <asp:Button ID="btnBuyIt" runat="server" CommandName="AddBasket" CssClass="btn btn-sm btn-primary pull-right customActionBtn" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>' />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        <div class="order_details_totals_row">
                            <div class="order_details_totals customTableText"><%=Subtotal%> </div>
                            <div class="order_details_line_total customTableText"><%=strCurrencySymbol %><asp:Label ID="Label1" runat="server"></asp:Label></div>
                        </div>

                        <div class="order_details_totals_row">
                            <div class="order_details_totals customTableText"><%=Carriage%></div>
                            <div class="order_details_line_total customTableText"><%=strCurrencySymbol %><asp:Label ID="Label2" runat="server"></asp:Label></div>
                        </div>

                        <div class="order_details_totals_row" id="dvTax" runat="server">
                            <div class="order_details_totals customTableText"><%=Tax%></div>
                            <div class="order_details_line_total customTableText"><%=strCurrencySymbol %><asp:Label ID="Label3" runat="server"></asp:Label></div>
                        </div>

                        <div class="order_details_grand_total">
                            <div class="order_details_totals bolder_weight customTableText"><%=Total%></div>
                            <div class="order_details_line_total bolder_weight customTableText"><%=strCurrencySymbol %><asp:Label ID="Label4" runat="server"></asp:Label></div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- COTAINER END -->
        </section>
        <div class="modal fade" id="ModalTracking" role="dialog" aria-hidden="true" data-backdrop="static" data-keyword="false">
        </div>
    </div>
    <script>new WOW().init();</script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        function close_quickbasket() {
            $("#quickbasket").stop();
            $("#quickbasket").fadeOut(50);
            $("#quickbasket_mobile").stop();
            $("#quickbasket_mobile").fadeOut(50);
        }

        function reset_quickbasket_timer() {
            $("#quickbasket").stop();
            $("#quickbasket").fadeIn(50, function () { $("#quickbasket").fadeOut(9000); });
        }

        function add_button() {
            close_quickbasket();
            $("#quickbasket").fadeIn(1000, function () { $("#quickbasket").fadeOut(7000); });
            $("#quickbasket_mobile").fadeIn(1000, function () { $("#quickbasket_mobile").fadeOut(7000); });
        }

        function OpenModalUp(trackId) {
            $('#ModalTracking').html('');
            $.ajax({
                type: "POST",
                url: "Order_ModalAjax.aspx",
                data: { TrackingId: trackId },
                cache: false,
                async: false,
            }).done(function (response) {
                $('#ModalTracking').html(response);
                $('#ModalTracking').modal('show');
                return false;
            });
        }

    </script>
</asp:Content>

