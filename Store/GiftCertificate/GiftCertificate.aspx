﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.GiftCertificate_GiftCertificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="divMainContent">
        <section id="CHECKOUT">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="pageTitle"><span id="spnPageTitle" runat="server"></span></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div id="jump1"></div>
                        <div class="header pageHeading" id="header1">1.<span id="spnRegisterAddress" runat="server"></span></div>
                        <asp:Repeater ID="rptInvoiceAddress" runat="server" OnItemDataBound="rptAddress_ItemDataBound">
                            <HeaderTemplate>
                                <div id="header1_space">
                                    <div class="form-group">
                                        <asp:Literal ID="ltrColumnName" runat="server" />
                                    </div>
                                    <div class="checkout_totals_outer bolder_weight pageSmlText">
                                        <asp:Literal ID="ltrNote" runat="server" />
                                    </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="form-group">
                                    <asp:Label ID="lblRegistrationFieldName" runat="server"></asp:Label>
                                    <%-- id="sub1lbl1" class="col-sm-9 col-md-8" CssClass="col-sm-3 col-md-4 control-label customLabel"  CssClass="form-control input-sm customInput"  CssClass="form-control input-sm customInput"--%>
                                    <div>
                                        <asp:HiddenField ID="hdfColumnName" runat="server" />
                                        <asp:TextBox runat="server" onkeyup="javascript:IsBoxChecked();" ID="txtRegistrationFieldName" autocomplete="off" onblur="return CheckTextbox(this.id)"
                                            CssClass="form-control input-sm" />
                                        <asp:DropDownList ID="ddlRegisterCountry" runat="server" CssClass="form-control input-sm">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqtxtRegistrationFieldName" runat="server" ControlToValidate="txtRegistrationFieldName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="reqddlRegisterCountry" runat="server" ControlToValidate="ddlRegisterCountry" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div class="form-group">
                            <span id="spnGC_Friend_Name_Title" runat="server"></span>
                            <%--Friendly name
                            Your Name* (Tell your recipient/s who their gift is from)--%>
                            <asp:TextBox ID="txtFriendlyName" runat="server" CssClass="form-control input-sm customInput"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqtxtFriendlyName" runat="server" ControlToValidate="txtFriendlyName" Display="Dynamic" SetFocusOnError="true" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                            <%--ErrorMessage="Please enter Friendly name"--%>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div id="jump2"></div>
                        <div class="header pageHeading" id="header3">2. <span id="spnGCDetail" runat="server"></span></div>
                        <%--Gift certificate details--%>
                        <div id="header3_space">
                            <asp:Repeater ID="rptCertificate" runat="server" OnItemDataBound="rptCertificate_ItemDataBound">
                                <HeaderTemplate>
                                    <div id="header2_space" style="margin-top: 15px;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <%--<label id="lblrptAmount" runat="server"></label>--%>
                                                <asp:Label ID="lblrptAmount" runat="server"></asp:Label>
                                                <div class="input-group  input-group-sm">
                                                    <div class="input-group-addon"><%# GlobalFunctions.GetCurrencySymbol() %></div>
                                                    <div id="dvUnitPrice">
                                                        <asp:TextBox ID="txtrptAmount" runat="server" Text='<%# Eval("Amount") %>' CssClass="form-control MyAmt" rel='<%# Container.ItemIndex + 1 %>'></asp:TextBox>

                                                    </div>
                                                </div>
                                                <div class="input-group  input-group-sm">
                                                    <asp:RequiredFieldValidator ID="rfvtxtrptAmount" runat="server" ControlToValidate="txtrptAmount" Display="Dynamic"
                                                        ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regtxtrptAmount" runat="server" ControlToValidate="txtrptAmount"
                                                        Display="dynamic"
                                                        ValidationExpression="^\d+(\.\d{1,2})?$" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <%--<label id="lblrptQuantity" runat="server"></label>--%>
                                                <asp:Label ID="lblrptQuantity" runat="server"></asp:Label>
                                                <div class="input-group input-group-sm " id="dvUnitQty">
                                                    <span class="input-group-btn"><a class="btn btn-default active aGCMinus" rel='<%# Container.ItemIndex + 1 %>' role="button">&ndash;</a></span>
                                                    <asp:TextBox ID="txtrptQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="form-control MyVal" rel='<%# Container.ItemIndex + 1 %>'></asp:TextBox>
                                                    <span class="input-group-btn"><a class="btn btn-default active aGCPlus" rel='<%# Container.ItemIndex + 1 %>' role="button">+</a></span>


                                                </div>
                                                <div class="input-group input-group-sm ">
                                                    <asp:RequiredFieldValidator ID="rfvtxtrptQuantity" runat="server" ControlToValidate="txtrptQuantity" Display="Dynamic"
                                                        ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regtxtrptQuantity" runat="server" ControlToValidate="txtrptQuantity"
                                                        Display="dynamic" 
                                                        ValidationExpression="\d*" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <%--<label>Recipient E-mail</label>--%>
                                                <asp:Label ID="lblrptRecipientEmail" runat="server"></asp:Label>
                                                <asp:TextBox ID="txtrptRecipientEmail" runat="server" Text='<%# Eval("RecipientEmail") %>' CssClass="form-control input-sm"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvtxtrptRecipientEmail" runat="server" ControlToValidate="txtrptRecipientEmail" Display="Dynamic"
                                                    ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regtxtrptRecipientEmail" runat="server" ControlToValidate="txtrptRecipientEmail"
                                                    Display="dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="OnClickSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <%--<label>Personal message</label>--%>
                                                <asp:Label ID="lblrptPersonalMsg" runat="server"></asp:Label>
                                                <asp:TextBox ID="txtrptPersonalMessage" runat="server" Text='<%# Eval("PersonalMessage") %>' CssClass="form-control input-sm" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                            </div>
                                            <div class="checkout_giftcert_bottom">
                                                <asp:LinkButton ID="lnkrptDelete" runat="server" OnClick="lnkrptDelete_Click" CommandArgument='<%# Eval("GiftOrderDetailsID") %>'><span aria-hidden="true" class="glyphicon glyphicon-remove"></span></asp:LinkButton>
                                                <%--<a href="#"><span aria-hidden="true" class="glyphicon glyphicon-remove"></span></a>--%>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>

                            <div id="header2_space" style="margin-top: 15px;" runat="server" class="AddDiv">
                                <div class="checkout_giftcert">
                                    <a><span aria-hidden="true" class="glyphicon glyphicon-remove" onclick="HideDiv();"></span></a>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label id="lblAmount" runat="server"></label>
                                        <div class="input-group  input-group-sm">
                                            <div class="input-group-addon" id="dvEditCurrency" runat="server"></div>
                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control"></asp:TextBox>

                                        </div>
                                        <div class="input-group  input-group-sm">
                                            <asp:RequiredFieldValidator ID="rfvtxtAmount" runat="server" ControlToValidate="txtAmount" Display="Dynamic"
                                                ValidationGroup="OnGiftSubmit" CssClass="text-danger"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter Amount" --%>
                                            <asp:RegularExpressionValidator ID="regtxtAmount" runat="server" ControlToValidate="txtAmount"
                                                Display="dynamic"
                                                ValidationExpression="^\d+(\.\d{1,4})?$" ValidationGroup="OnGiftSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                            <%--ErrorMessage="Please enter correct amount"--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label id="lblQuantity" runat="server"></label>
                                        <div class="input-group input-group-sm ">
                                            <span class="input-group-btn"><a class="btn btn-default active aGCMinus1" role="button">&ndash;</a></span>
                                            <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" Text="1"></asp:TextBox>
                                            <span class="input-group-btn"><a class="btn btn-default active aGCPlus" role="button">+</a></span>

                                        </div>
                                        <div class="input-group input-group-sm ">
                                            <asp:RequiredFieldValidator ID="rfvtxtQuantity" runat="server" ControlToValidate="txtQuantity" Display="Dynamic"
                                                ValidationGroup="OnGiftSubmit" CssClass="text-danger"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter Quantity"--%>
                                            <asp:RegularExpressionValidator ID="regtxtQuantity" runat="server" ControlToValidate="txtQuantity"
                                                Display="dynamic"
                                                ValidationExpression="\d*" ValidationGroup="OnGiftSubmit" CssClass="text-danger"></asp:RegularExpressionValidator><%--ErrorMessage="Please enter Quantity"--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label id="lblEmail" runat="server"></label>
                                        <asp:TextBox ID="txtRecipientEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvtxtRecipientEmail" runat="server" ControlToValidate="txtRecipientEmail" Display="Dynamic"
                                            ValidationGroup="OnGiftSubmit" CssClass="text-danger"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter Recipient E-mail"--%>
                                        <asp:RegularExpressionValidator ID="regtxtRecipientEmail" runat="server" ControlToValidate="txtRecipientEmail"
                                            Display="dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="OnGiftSubmit" CssClass="text-danger"></asp:RegularExpressionValidator>
                                        <%-- ErrorMessage="Please enter Recipient E-mail"--%>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label id="lblMessage_Title" runat="server"></label>
                                        <asp:TextBox ID="txtPersonalMessage" runat="server" TextMode="MultiLine" CssClass="form-control input-sm" Rows="3"></asp:TextBox>
                                    </div>
                                    <div class="checkout_giftcert_bottom">
                                        <asp:Button ID="btnAddGift" runat="server" OnClick="btnAddGift_OnClick" CssClass="btn btn-default btn-sm" ValidationGroup="OnGiftSubmit" />
                                    </div>
                                </div>
                                <%--  <div class="col-xs-12 checkout_giftcert_top  checkout_giftcert_spacer_bottom">
                                </div>--%>
                            </div>
                            <div class="col-xs-12 checkout_giftcert_top  checkout_giftcert_spacer_bottom" id="GCAddMoreDiv">
                                <span class="btn btn-default btn-sm" onclick="showDiv()" id="spnAddMore" runat="server"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div id="jump4"></div>
                        <div class="header pageHeading" id="header4">3. <span id="spnReviewNConfirm" runat="server"></span></div>
                        <asp:Repeater ID="rptListBasket" runat="server" OnItemDataBound="rptListBasket_ItemDataBound">
                            <HeaderTemplate>
                                <div class="checkout_totals_outer checkout_giftcert_review_item" id="dvLstOuter">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="checkout_totals_full_width">
                                    <span class="bolder_weight" id="spnStGC" runat="server"></span>
                                    <%# Eval("RecipientEmail") %>
                                </div>
                                <div class="checkout_totals_left lstQtyOuter">
                                    <span id="spnQty" runat="server"></span>
                                    @ <%# GlobalFunctions.GetCurrencySymbol() %>
                                    <span id="spnUnitAmt" runat="server"></span>
                                </div>
                                <div class="checkout_totals_right lstQtyOuterRight">
                                    <%# GlobalFunctions.GetCurrencySymbol() %>
                                    <span id="spnPrice" runat="server"></span>
                                    <%--<asp:Literal ID="ltGiftPrice" runat="server"></asp:Literal>--%>
                                </div>

                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div id="header4_space">
                            <div class="checkout_totals_outer">
                                <div class="checkout_totals_left pageSmlText"><span id="spnBSubTotal" runat="server"></span></div>
                                <div id="dvTotal" class="checkout_totals_right pageSmlText">
                                    <%= strCurrencySymbol %>
									<asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="checkout_totals_outer hide" id="divShipping" runat="server" style="display:none;">
                                <div class="checkout_totals_left pageSmlText"><span id="spnShipping" runat="server"></span></div>
                                <div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><span id="spnShippingPrice" runat="server">0</span></div>
                            </div>
                            <div class="checkout_totals_outer hide" id="divTax" runat="server">
                                <div class="checkout_totals_left pageSmlText"><span id="spnTax" runat="server"></span></div>
                                <div class="checkout_totals_right pageSmlText"><%= strCurrencySymbol %><span id="spnTaxPrice" runat="server">0</span></div>
                            </div>
                            <div class="checkout_totals_outer">
                                <div class="checkout_totals_left bolder_weight pageSmlText">Total</div>
                                <div class="checkout_totals_right bolder_weight pageSmlText">
                                    <div id="dvTotalPrice">
									 <%= strCurrencySymbol %>
                                        <span id="spnTotalPrice" runat="server"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="checkout_totals_outer">
                                <asp:Button class="btn btn-primary pull-right customActionBtn" ID="aProceed" runat="server" ValidationGroup="OnClickSubmit" OnClick="aProceed_ServerClick" />
                                <%--Text="Place order"--%>
                            </div>
                            <div class="checkout_totals_outer pageSmlText" id="divStatictext" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="../JS/GiftBasket.js"></script>
    <script type="text/javascript">
        function HideDiv() {
            $('#<%=txtAmount.ClientID %>').val('');
            $('#<%=txtQuantity.ClientID %>').val('1');
            $('#<%=txtRecipientEmail.ClientID %>').val('');
            $('#<%=txtPersonalMessage.ClientID %>').val('');
            $('.AddDiv').hide();
            $('#GCAddMoreDiv').show();
        }

        function showDiv() {
            $('.AddDiv').show();
            $('#GCAddMoreDiv').hide();
        }
        $(document).ready(function () {
            $('#GCAddMoreDiv').hide();
        });
    </script>
</asp:Content>

