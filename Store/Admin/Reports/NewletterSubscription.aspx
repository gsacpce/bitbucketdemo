﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Admin_Reports_NewletterSubscription" %>

<script runat="server">

  
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javaScript">
        function validate() {

            var Search = document.getElementById('<%= txtSearch.ClientID %>');

            if (Search.value == '') {
                alert("Please enter search Email Id/User Name");
                return false;
            }
            if (Search.value == 'Email Id/User Name') {
                alert("Please enter search Email Id/User Name");
                return false;
            }

        }
    </script>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Reports </a></li>
            <li>Newsletter Subscription</li>
        </ul>
    </div>

    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container btn_padd_productlist ">
                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Newsletter Subscription
                        </h3>
                        <div class="customers_top">
                            <div style="padding-bottom: 3px">
                                <div class="clear">
                                </div>
                                <div id="accordionx" class="search2 accordion">
                                    <h3>Search Newsletter Subscription(s)</h3>
                                    <div class="searchInnerbox">
                                        <div class="box1 box1Extra" style="background: none !important;">
                                            <asp:Panel runat="server" DefaultButton="btnSearch">
                                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Email Id/User Name" CssClass="input1"></asp:TextBox>
                                                <asp:HiddenField ID="hdnFilter" runat="server" />
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearchName_Click"
                                                    OnClientClick="return validate();" />
                                                <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" CssClass="reset_new" />
                                            </asp:Panel>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="customer">
                                <div id="divSearch" runat="server" visible="false">
                                    <span class="allcosutomer">SEARCHED RECORD FOUND -
                            <asp:Label ID="lblSearch" runat="server" Text=""></asp:Label></span>
                                </div>

                                <span class="allcosutomer">TOTAL Records -
                        <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></span>
                                <ul class="customers_btn">
                                    <li>
                                        <asp:Button ID="btnExportdata" class="save" runat="server" Text="Export Report"
                                            OnClick="btnExportdata_Click" />
                                    </li>
                                </ul>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="sucessfully" id="divMessage" visible="false" runat="server">
                                <asp:Literal ID="ltrMessage" runat="server" />
                            </div>
                        </div>
                        <div class="wrap_prolist">
                            <section class="all_customer dragblnextBlock manage_pro">
                                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="100%"
                                    class="all_customer_inner allcutomerEtracls " AllowSorting="true" AllowCustomPaging="false" AllowPaging="true"
                                    PageSize="50" EmptyDataText="No Product Found." CellPadding="2" BorderColor="#E4E4E4"
                                    BorderWidth="0" OnPageIndexChanging="gvData_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Email Id" HeaderStyle-HorizontalAlign="Left"
                                             HeaderStyle-Width="500">
                                            <ItemTemplate>
                                               <%# DataBinder.Eval(Container.DataItem, "EmailId") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "UserName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SubScribed Date" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "SubscribedDate","{0:dd/MM/yyyy}") %> 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "SubscribedStatus") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmptyGridTemplate" runat="server" Text="No Data Found"></asp:Label>
                                    </EmptyDataTemplate>



<%--                                  <PagerSettings Position="TopAndBottom" Mode="NextPreviousFirstLast" FirstPageText="<<" LastPageText=">>" NextPageText=">" PreviousPageText="<" />--%>
                                    <PagerStyle CssClass="paging" />
                                     <PagerSettings Position="TopAndBottom" />
                                </asp:GridView>


                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</asp:Content>




