﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Admin_Reports_UnCategoriseProducts" %>

<script runat="server">

  
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javaScript">
        function validate() {

            var Search = document.getElementById('<%= txtSearch.ClientID %>');

            if (Search.value == '') {
                alert("Please enter search product code/name");
                return false;
            }
            if (Search.value == 'Product Name / Product Code') {
                alert("Please enter search product code/ product name");
                return false;
            }

        }
    </script>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Reports </a></li>
            <li>Un - Categorised Products</li>
        </ul>
    </div>

    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container btn_padd_productlist ">



                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Un - Categorised Products
                        </h3>
                        <div class="customers_top">
                            <div style="padding-bottom: 3px">
                                <ul  class="customers_btn">
                                    <li>
                                      
                                    </li>
                                </ul>
                                <div class="clear">
                                </div>
                                <div id="accordionx" class="search2 accordion" style="display:none;">
                                    <h3>Search Product(s)</h3>
                                    <div class="searchInnerbox">
                                        <div class="box1 box1Extra" style="background: none !important;">
                                            <asp:Panel runat="server" DefaultButton="btnSearch">
                                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Product Name / Product Code" CssClass="input1"></asp:TextBox>
                                                <asp:HiddenField ID="hdnproductname" runat="server" />
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                                                    OnClientClick="return validate();" />
                                                <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" CssClass="reset_new" />
                                            </asp:Panel>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">


                                </div>
                            </div>
                            <div class="customer" style="margin: 0px !important;">
                                <span class="allcosutomer">All Products -
                    <asp:Literal ID="ltrNoOfProducts" runat="server" Text=""></asp:Literal></span>
                                 <ul id="exportBtn" class="customers_btn">
                                    <li>
                                        <asp:Button ID="btnExportProducts" class="save" runat="server" Text="Export Report"
                                           OnClick="btnExportProducts_Click" />
                                    </li>
                                </ul>
                                <div class="clear">
                                </div>
                            </div>
                              
                        </div>
                        <div class="clear">
                                </div>
                        <div class="wrap_prolist">
                            <section class="all_customer dragblnextBlock manage_pro">
                                <asp:GridView ID="grdProduct" runat="server" AutoGenerateColumns="False" Width="100%" 
                                    AllowPaging="true" AllowCustomPaging="false" PageSize="50" EmptyDataText="No Product Found." CellPadding="2"
                                    BorderColor="#cccccc" BorderWidth="0" OnPageIndexChanging="grdProduct_PageIndexChanging"
                                    class="all_customer_inner allcutomerEtracls " OnRowCommand="grdProduct_OnRowCommand">
                                  
                                    <Columns>
                                        <asp:BoundField DataField="ProductCode" HeaderText="Product Code" HeaderStyle-CssClass="bg"
                                            SortExpression="ProductCode" />
                                        <asp:BoundField DataField="ProductName" HeaderText="Product Name" HeaderStyle-CssClass="bg"
                                            SortExpression="ProductName" />
                                       <%-- <asp:BoundField DataField="ProductDescription" HeaderText="Product Description" HeaderStyle-CssClass="bg"
                                            SortExpression="ProductDescription" />
                                        <asp:BoundField DataField="FurtherDescription" HeaderText="Further Description" HeaderStyle-CssClass="bg"
                                            SortExpression="FurtherDescription" />--%>
                                        <%--<asp:TemplateField HeaderText="Is Active" HeaderStyle-CssClass="bg" HeaderStyle-Width="80px" ItemStyle-Width="80px">
                                            <ItemTemplate>
                                                <div>
                                                    <span>
                                                        <asp:CheckBox ID="chkIsActive" runat="server" rel='<%# DataBinder.Eval(Container.DataItem, "ProductId") %>'
                                                            OnCheckedChanged="chkIsActive_CheckedChanged" Checked='<%# Eval("IsActive") %>' onClick="ShowLoader()" AutoPostBack="true" />
                                                    </span><span class="spnwidt">Is Active</span>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="view_order">
                                                            <asp:LinkButton ID="imgbtnEdit" CommandArgument='<%#Eval("ProductId") %>' CommandName="Edit_Product" 
                                                                 runat="server" Text="Assign Category" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>


                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmptyGridTemplate" runat="server" Text="No Data Found"></asp:Label>
                                    </EmptyDataTemplate>

                                    <PagerSettings Position="TopAndBottom" />

                                   <%-- <PagerSettings Position="TopAndBottom" Mode="NextPreviousFirstLast" FirstPageText="<<" LastPageText=">>" NextPageText=">" PreviousPageText="<" />--%>
                                    <PagerStyle CssClass="paging" />


                                </asp:GridView>


                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</asp:Content>




