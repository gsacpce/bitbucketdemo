﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Admin_Reports_Order_ajax" MasterPageFile="~/Admin/Master/AdminMaster.master" %>

<style>
    .customTable td, .customTable th, customTable tr, .customTable thead th {
        border: 1px solid #CCC !important;
    }

    .customTableHead {
        color: #007cbf !important;
        font-family: open-sans !important;
        font-size: 14px !important;
        font-weight: 300 !important;
        font-style: normal !important;
        text-decoration: none !important;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-6">
            <h2>Order Number:&nbsp; 
            <asp:literal id="ltrOrderNo" runat="server"></asp:literal>
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="order_addresses_heading customTableHead">Billing Address</div>
            <div class="order_addresses_text customTableText">
                <asp:literal id="ltrBillingAddress" runat="server"></asp:literal>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="order_addresses_heading customTableHead">Delivery Address</div>
            <div class="order_addresses_text customTableText">
                <asp:literal id="ltrDeliveryAddress" runat="server"></asp:literal>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="order_addresses_heading customTableHead">Payment Method</div>
            <div class="order_addresses_text customTableText">
                <asp:literal id="ltrPaymentMethod" runat="server"></asp:literal>
            </div>

            <div id="divGiftCertificate" runat="server">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="order_addresses_heading customTableHead">GiftCertificate</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <asp:literal id="ltGiftCertificateCode" runat="server"></asp:literal>
                        <br />
                        <asp:literal id="ltGiftCertificateAmount" runat="server" />
                    </div>
                    <%--<div class="col-xs-12 col-md-4">
                        <div class="order_addresses_text customTableText">
                            <asp:literal id="ltGiftCertificateCode" runat="server"></asp:literal>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <asp:literal id="ltGiftCertificateAmount" runat="server" />
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <br />
    <div class="row">
        <div class="col-xs-12">
            <asp:repeater id="rptBasketListing" runat="server" onitemdatabound="rptBasketListing_ItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-bordered  customTableText ">
                                <tr>
                                    <th>Product</th>
                                    <th>Title</th>
                                    <th>Quantity</th>
                                    <th>Unit cost</th>
                                    <th>Line total</th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <img id="imgProduct" runat="server" alt="" style="width: 60px;" class="image_bg">
                                </td>
                                <td>
                                    <%# DataBinder.Eval(Container.DataItem, "SKU") %><br>
                                    <%# DataBinder.Eval(Container.DataItem, "ProductName") %>
                                </td>
                                <td><%# DataBinder.Eval(Container.DataItem, "Quantity") %></td>
                                <td>
                                <div id="dvUnitPrice" runat="server" class="order_confirm_unit_price customTableText"></div></td>
                                <td><div id="dvTotalPrice" runat="server" class="order_confirm_line_total customTableText"></div></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr>
                            <td>
                                </td><td>
                                    </td><td></td><td class="text-right">
                                        <div class="order_confirm_totals customTableText">Subtotal</div>
                                        
                                        <div class="order_confirm_totals customTableText" id="dvShippingText" runat="server">Shipping</div>
                                         <div class="order_confirm_totals customTableText" id="dvTaxText" runat="server">Tax</div>
                                         
                                        <div class="order_confirm_totals bolder_weight customTableText"><strong>Total</strong></div>
                                                  </td>
                                <td>
                                
                                <div id="dvSubTotal" runat="server" class="order_confirm_line_total customTableText"></div>
                            
                                
                                <div id="dvShippingCharges" runat="server" class="order_confirm_line_total customTableText"></div>
                             
                               
                                <div id="dvTaxes" runat="server" class="order_confirm_line_total customTableText"></div>
                            
                                
                                <div id="dvTotal" runat="server" class="order_confirm_line_total bolder_weight customTableText"><strong></strong></div>
                            </td>
                                </tr>
                             </table>
                        </FooterTemplate>
                    </asp:repeater>
        </div>
    </div>
</div>
