﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Reports_AbandonedCart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javaScript">
        function validateSearch() {
            var Search = $('#txtSearch').val().trim();
            if (Search == '') {
                $('#myWarningModal').find('#WarningMessage').html("Please enter Product Name / Product Code / Email Address");
                $('#myWarningModal').modal('show');
                return false;
            }
            if (Search == 'Product Name / Product Code / Email Address') {
                $('#myWarningModal').find('#WarningMessage').html("Please enter Product Name / Product Code / Email Address");
                $('#myWarningModal').modal('show');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" ClientIDMode="Static">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=HostAdmin %>login/login.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Reports </a></li>
            <li>Abandoned Cart</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Abandoned Cart
                        </h3>
                        <div class="customers_top">
                            <div style="padding-bottom: 3px">
                                <div id="accordionx" class="search2 accordion">
                                    <h3>Search</h3>
                                    <div class="searchInnerbox">
                                        <div class="box1 box1Extra" style="background: none !important;">
                                            <asp:Panel runat="server" DefaultButton="btnSearch">
                                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Product Name / Product Code / Email Address" CssClass="input1"></asp:TextBox>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                                                    OnClientClick="return validateSearch();" />
                                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="reset_new" OnClick="btnReset_Click" />
                                            </asp:Panel>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="customer" style="margin: 0px !important;">
                                <span class="allcosutomer">All Products -
                                    <asp:Literal ID="ltrNoOfProducts" runat="server" Text=""></asp:Literal></span>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                        <div class="wrap_abcart">
                            <section class="all_customer dragblnextBlock manage_pro">
                                <asp:GridView ID="grdProduct" runat="server" AutoGenerateColumns="False" Width="100%"
                                    AllowPaging="true" AllowCustomPaging="true" PageSize="50" EmptyDataText="No Products Found." CellPadding="2"
                                    BorderColor="#cccccc" BorderWidth="0" OnPageIndexChanging="grdProduct_PageIndexChanging"
                                    class="all_customer_inner allcutomerEtracls">
                                    <Columns>
                                        <asp:BoundField DataField="EmailId" HeaderText="EmailId" HeaderStyle-CssClass="bg"
                                            SortExpression="EmailId" />
                                        <asp:TemplateField HeaderText="Product" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <img class="image_bg" src="<%=Host %>Images/Products/Thumbnail/<%#Eval("DefaultImageName") %>" style="width: 60px;" alt="" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ProductCode" HeaderText="Product Code" HeaderStyle-CssClass="bg"
                                            SortExpression="ProductCode" />
                                        <asp:BoundField DataField="ProductName" HeaderText="Product Name" HeaderStyle-CssClass="bg"
                                            SortExpression="ProductName" />
                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" HeaderStyle-CssClass="bg"
                                            SortExpression="Quantity" />
                                        <%-- <asp:BoundField DataField="Price" HeaderText="Unit Cost" HeaderStyle-CssClass="bg"
                                            SortExpression="Price" />--%>
                                        <asp:TemplateField HeaderText="Unit Cost" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# GlobalFunctions.GetCurrencySymbolByCurrencyId(Eval("CurrencyId").To_Int16()) + GlobalFunctions.DisplayPriceOrPoints(Eval("Price").ToString(),"",GlobalFunctions.GetLanguageId()) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Line Total" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# GlobalFunctions.GetCurrencySymbolByCurrencyId(Eval("CurrencyId").To_Int16()) + GlobalFunctions.DisplayPriceOrPoints(Eval("TotalPrice").ToString(),"",GlobalFunctions.GetLanguageId()) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField DataField="TotalPrice" HeaderText="Line Total" HeaderStyle-CssClass="bg"
                                            SortExpression="TotalPrice" />--%>
                                        <asp:TemplateField HeaderText="Added On" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("CreatedDate", "{0:dd-M-yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Abandoned On" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("AbandonedDate", "{0:dd-M-yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmptyGridTemplate" runat="server" Text="No Products Found."></asp:Label>
                                    </EmptyDataTemplate>
                                    <PagerSettings Position="TopAndBottom" Mode="NumericFirstLast" FirstPageText="<<" LastPageText=">>" NextPageText=">" PreviousPageText="<" />
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

