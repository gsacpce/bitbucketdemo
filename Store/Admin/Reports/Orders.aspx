﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Reports_Orders" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javaScript">
        var HostAdmin = "<%=HostAdmin %>";
        var Host = "<%=Host %>";
        $(function () {
            $("#txtFromDate").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#txtToDate").datepicker({ dateFormat: 'yy-mm-dd' });
        });

        function validateSearch() {
            var selectedValue = $("#ddlSearchOption option:selected").val();
            var messages = '';
            if (selectedValue == 'date') {
                var startDate = $("#txtFromDate").val();
                var endDate = $("#txtToDate").val();
                if (startDate != '' && endDate != '') {
                    if (isNaN(Date.parse(startDate)) || isNaN(Date.parse(endDate))) {
                        messages = "Please enter valid From date and To date";
                    }
                    else {
                        if (Date.parse(startDate) > Date.parse(endDate)) {
                            messages = "From date should not be greater than the To date";
                        }
                    }
                }
                else {
                    messages = "Please enter From date and To date";
                }
            }
            else if (selectedValue == 'number') {
                var Search = $('#txtSearch').val().trim();
                if (Search == '') {
                    messages = "Please enter order number.";
                }
                if (Search == 'Enter order number') {
                    messages = "Please enter order number.";
                }
            }
            else if (selectedValue == 'value') {
                var FromOrderValue = $('#txtFromOrderValue').val().trim();
                var ToOrderValue = $('#txtToOrderValue').val().trim();
                if (FromOrderValue == '' || ToOrderValue == '' || FromOrderValue == 'Enter From Order Value' || ToOrderValue == 'Enter To Order Value') {
                    messages = "Please enter From and To order value.";
                }
                else {
                    if (parseInt(FromOrderValue) > parseInt(ToOrderValue)) {
                        messages = "From order value can not be greater than To order value.";
                    }
                }
            }
            if (messages != '') {
                $('#myWarningModal').find('#WarningMessage').html(messages);
                $('#myWarningModal').modal('show');
                return false;
            }
            return true;
        }
        $(document).on('change', '#ddlSearchOption', function () {
            var selectedValue = $(this).val();
            $('#txtSearch').hide();
            $('#divOrderDate').hide();
            $('#divOrderValue').hide();
            $('#divPaymentStatus').hide();
            if (selectedValue == 'date') {
                $('#divOrderDate').show();
            }
            else if (selectedValue == 'number') {
                $('#txtSearch').show();
            }
            else if (selectedValue == 'value') {
                $('#divOrderValue').show();
            }
            else if (selectedValue == 'status') {
                $('#divPaymentStatus').show();
            }
        });

        $(document).on('click', '#btnViewDetails', function () {
            var customerOrderId = $(this).attr('data-value');
            var uid = $(this).attr('data-id');
            alert(HostAdmin + 'Reports/order_ajax.aspx?cid=' + customerOrderId + '&uid=' + uid);
            $.ajax({
                url: HostAdmin + 'Reports/order_ajax.aspx?cid=' + customerOrderId + '&uid=' + uid,
                dataType: "html",
                type: "POST",
                contentType: "html",
                cache: false,
                async: true
            }).done(function (data) {
                alert('hi');
                $('#myModal').find('.modal-dialog').addClass('modal-lg');
                $('#myModal').find('.modal-body').html(data);
                $('#myModal').find('.modal-title').html('Order Details');
                $('#myModal').find('.modal-footer').hide();
                $('#myModal').modal('show');
            });
        });

        $(document).ready(function () {
            var selectedValue = $("#ddlSearchOption option:selected").val();
            $('#txtSearch').hide();
            $('#divOrderDate').hide();
            $('#divOrderValue').hide();
            $('#divPaymentStatus').hide();
            if (selectedValue == 'date') {
                $('#divOrderDate').show();
            }
            else if (selectedValue == 'number') {
                $('#txtSearch').show();
            }
            else if (selectedValue == 'value') {
                $('#divOrderValue').show();
            }
            else if (selectedValue == 'status') {
                $('#divPaymentStatus').show();
            }
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" ClientIDMode="Static">
     <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Reports </a></li>
            <li>Order Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <h3 class="mainHead">Orders</h3>
                    <span class="sectionText">View all the orders placed on the website.</span>
                    <!-- Website tab start -->

                    <div class="butonblock nospace clearfix ">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <asp:DropDownList ID="ddlSearchOption" runat="server">
                                    <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Order Date" Value="date"></asp:ListItem>
                                    <asp:ListItem Text="Order Number" Value="number"></asp:ListItem>
                                    <asp:ListItem Text="Order Value" Value="value"></asp:ListItem>
                                    <asp:ListItem Text="Order Status" Value="status"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox placeholder="Enter order number" ID="txtSearch" onkeypress="return isNumberKey(event)" MaxLength="15"
                                    Style="display: none" runat="server" />
                                <div id="divOrderDate" style="display: none;">
                                    <asp:TextBox ID="txtFromDate" runat="server" />
                                    <asp:TextBox ID="txtToDate" runat="server" />
                                </div>
                                <div id="divOrderValue" style="display: none;">
                                    <asp:TextBox ID="txtFromOrderValue" placeholder="Enter From Order Value" onkeypress="return isNumberKey(event)" MaxLength="10" runat="server" />
                                    <asp:TextBox ID="txtToOrderValue" placeholder="Enter To Order Value" onkeypress="return isNumberKey(event)" MaxLength="10" runat="server" />
                                </div>
                                <div id="divPaymentStatus" style="display: none;">
                                    <asp:DropDownList ID="ddlPaymentStatus" runat="server" AutoPostBack="true">
                                        <asp:ListItem Text="-Select-" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Success" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Failed" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:Button Text="Search orders" ID="btnSearch" runat="server" OnClientClick="return validateSearch();" OnClick="btnSearch_Click"></asp:Button>
                                <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                                <asp:Button Text="Export To Excel" ID="Button1" runat="server" CssClass="btnSubmit" OnClick="btnExportToExcel_Click"></asp:Button>
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="customer" style="margin: 0px !important;">
                        <span class="allcosutomer">Showing
                                    <asp:Literal ID="ltrNoOfProducts" runat="server" Text=""></asp:Literal></span>
                        <div class="clear">
                        </div>
                    </div>
                    <!--mainOutContainer start-->
                    <div class="midleDrgblecontin">
                        <ul class="dragblnextBlock">
                            <li>
                                <asp:GridView ID="grdProduct" runat="server" AutoGenerateColumns="false" Width="100%"
                                    class="all_customer_inner dragbltop" BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true" AllowCustomPaging="true"
                                    EmptyDataText="No record found."
                                    OnPageIndexChanging="grdProduct_PageIndexChanging" AllowSorting="false" PageSize="20">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Order date" HeaderStyle-Width="6%" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Bind("CreatedDate", "{0:dd-M-yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="OrderId_OASIS" HeaderText="Order Number" HeaderStyle-Width="6%" HeaderStyle-CssClass="bg"
                                            SortExpression="OrderId_OASIS" />
                                        <asp:BoundField DataField="InvoiceEmail" HeaderText="Ordered By" HeaderStyle-Width="6%" HeaderStyle-CssClass="bg"
                                            SortExpression="InvoiceEmail" />
                                        <asp:BoundField DataField="DeliveryAddress1" HeaderText="Delivered To" HeaderStyle-Width="25%" HeaderStyle-CssClass="bg"
                                            SortExpression="DeliveryAddress1" />
                                        <%--<asp:BoundField DataField="OrderValue" HeaderText="Order Value" HeaderStyle-Width="6%" HeaderStyle-CssClass="bg"
                                            SortExpression="OrderValue" />--%>
                                          <asp:TemplateField HeaderText="Order Value" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# GlobalFunctions.GetCurrencySymbolByCurrencyId(Eval("CurrencyId").To_Int16()) + GlobalFunctions.DisplayPriceOrPoints(Eval("OrderValue").ToString(),"",GlobalFunctions.GetLanguageId()) + GlobalFunctions.PointsText(GlobalFunctions.GetLanguageId()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Status" HeaderStyle-Width="6%" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("IsOrderCompleted").ToString().ToLower() == "true" ? "Success" : "Failed" %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="6%" HeaderStyle-CssClass="bg" Visible="false">
                                            <ItemTemplate>
                                                <input type="button" value="View Details" id="btnViewDetails" data-id='<%# Eval("OrderedBy") %>' data-value='<%# Eval("CustomerOrderId") %>' class="btn3"></input>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" FirstPageText="First" PageButtonCount="6"
                                        LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <input type="hidden" id="hdnIsSearch" runat="server" value="0" />
        <link rel="stylesheet" type="text/css" href="<%=Host %>JS/bootstrap-datetimepicker.js" />
    <link href="<%=HostAdmin %>CSS/jquery-ui.css" rel="stylesheet" />
    <link href="<%=HostAdmin %>CSS/ui.theme.css" rel="stylesheet" />

</asp:Content>

