﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Admin_SliderManagement_SliderListing_ajax" %>

<ul>
    <asp:repeater id="rptSliderListing" runat="server">
            <HeaderTemplate>
                <li>
                    <table width="90%" class="dragbltop dragbltopextra">
                        <tbody>
                            <tr>
                                <td class="firsttd"><strong>Sr #</strong></td>
                                <td><strong>Name</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <table width="90%" class="dragbltop">
                        <tbody>
                            <tr>
                                <td class="firsttd"><span><%# Container.ItemIndex + 1 %></span></td>
                                <td><span><a href="javascript:void(0);" class="itemclick" placeholder='<%=strPlaceholderId %>' itemtype="<%=strItemType %>" data-value='<%# DataBinder.Eval(Container.DataItem, "SlideShowId") %>'> <%# DataBinder.Eval(Container.DataItem, "SlideShowName") %></a></span></td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            </ItemTemplate>
        </asp:repeater>
        <asp:repeater id="rptSectionListing" runat="server">
            <HeaderTemplate>
                <li>
                    <table width="90%" class="dragbltop dragbltopextra">
                        <tbody>
                            <tr>
                                <td class="firsttd"><strong>Sr #</strong></td>
                                <td><strong>Name</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <table width="90%" class="dragbltop">
                        <tbody>
                            <tr>
                                <td class="firsttd"><span><%# Container.ItemIndex + 1 %></span></td>
                                <td><span><a href="javascript:void(0);" class="itemclick" placeholder='<%=strPlaceholderId %>' itemtype="<%=strItemType %>" data-value='<%# DataBinder.Eval(Container.DataItem, "SectionId") %>'> <%# DataBinder.Eval(Container.DataItem, "SectionName") %></a></span></td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            </ItemTemplate>
        </asp:repeater>
     <asp:repeater id="rptStaticContent" runat="server">
            <HeaderTemplate>
                <li>
                    <table width="90%" class="dragbltop dragbltopextra">
                        <tbody>
                            <tr>
                                <td class="firsttd"><strong>Sr #</strong></td>
                                <td><strong>Name</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <table width="90%" class="dragbltop">
                        <tbody>
                            <tr>
                                <td class="firsttd"><span><%# Container.ItemIndex + 1 %></span></td>
                                <td><span><a href="javascript:void(0);" class="itemclick" placeholder='<%=strPlaceholderId %>' itemtype="<%=strItemType %>" data-value='<%# DataBinder.Eval(Container.DataItem, "HomePageStaticContentId") %>'> <%# DataBinder.Eval(Container.DataItem, "Title") %></a></span></td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            </ItemTemplate>
        </asp:repeater>
</ul>
