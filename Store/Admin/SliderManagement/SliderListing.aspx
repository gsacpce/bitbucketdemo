﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true"
    Inherits="Presentation.Admin_SliderListing" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;


            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;

            });
        });
    </script>
    <style>
        #ContentPlaceHolder1_gvSlideShow {
            border-top: 1px solid #ccc;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Customize Design</a></li>
            <li>Manage Slide Shows</li>
        </ul>
    </div>
    <div class="admin_page">

        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <h3 class="mainHead">Slide Show Management</h3>
                    <p>Manage multiple image slide shows.</p>
                    <!-- Website tab start -->

                    <div class="butonblock nospace clearfix ">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <asp:Button runat="server" ID="btnAddNew" CommandArgument="0" CssClass="btn3" OnClick="Btn_Click" CommandName="Add" Text="Add New Slider" />
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                    <!--mainOutContainer start-->
                    <div class="midleDrgblecontin manage_slide_pegi">
                        <ul class="dragblnextBlock  new_side_show_mangment">
                            <li>
                                <asp:GridView ID="gvSlideShow" runat="server" AutoGenerateColumns="false" Width="100%"
                                    class="all_customer_inner dragbltop " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                    OnPageIndexChanging="gvSlideShow_PageIndexChanging" EmptyDataText="No record found."
                                    OnRowDataBound="gvSlideShow_OnRowDataBound" OnRowDeleting="gvSlideShow_RowDeleting" AllowSorting="false" PageSize="10">
                                    <Columns>
                                        <asp:BoundField HeaderText="Slide Show" HeaderStyle-CssClass="bg" DataField="SlideShowName"
                                            ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Type" HeaderStyle-CssClass="bg" DataField="SlideShowMasterName"
                                            ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Is Active" HeaderStyle-CssClass="bg" DataField="IsActive"
                                            ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                            SortExpression="Status" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="view_order other_option">

                                                            <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" ID="lnkbtnUpdate"
                                                                CommandName="<%# DBAction.Update.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SlideShowId") %>' />
                                                            <asp:LinkButton Text="Delete" runat="server" OnCommand="Btn_Click" ID="lnkbtnDelete" OnClientClick="return confirm('Are you sure deleting item?')"
                                                                CommandName="<%# DBAction.Delete.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SlideShowId") %>' />
                                                            <asp:LinkButton Text="Manage Slides" runat="server" OnCommand="Btn_Click" ID="lnkBtnManageSlides"
                                                                CommandName="Manage Slides" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SlideShowId") %>' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" FirstPageText="First" PageButtonCount="5"
                                        LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

