﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_SlideShowImages" ValidateRequest="false" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <style>
        #sortable1, #sortable2 {
            list-style-type: none;
            margin: 0;
            padding: 0 0 2.5em;
            float: left;
            margin-right: 10px;
        }

            #sortable1 li, #sortable2 li {
                margin: 0 5px 5px 5px;
                padding: 5px;
                font-size: 1.2em;
                width: 120px;
            }

        .overlay_text td {
            height: 45px;
            width: 150px;
        }

        .overlay_text .selectpicker {
            margin-top: 0px;
        }
    </style>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Slide Shows</a></li>
            <li>Update Slide Show Images</li>
        </ul>
    </div>

    <div id="div1" class="divMessage" visible="false" runat="server">

        <asp:Literal ID="Literal1" runat="server" />
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">

            <div class="wrap_container ">
                <div class="content">
                    

                        <h3>Slideshow	</h3>
                        <p>Slide show to manages slides</p>
                        <br />
                        <h3><%=SlideShowName %></h3>

                        <div>
                            <div class="row adminBorderCont ">

                                <div class="col-md-6 col-sm-12 ">
                                    <div id="FUSlideShow">
                                    </div>

                                    <input type="hidden" class="slideshowsrc" clientidmode="static" id="hdnSlideShowSrc" runat="server" />
                                    <div class="uploaderMsg">[Use .jpg, .jpeg , .png , .gif file with Size <= 500 KB]</div>
                                    <span id="error-alert" style="display: none; color: Red">
                                        <b>Please upload image first.</b>
                                    </span>
                                </div>


                                <div class="col-md-6 col-sm-12 ">
                                    <div id="FUSlideShow1">
                                    </div>
                                    <input type="hidden" class="slideshowsrc" clientidmode="static" id="hdnSlideShowSrc1" runat="server" />

                                </div>
                                <div class="col-md-8 col-sm-12 imageContent">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12  ">




                                    <%--<img src="../Images/bg-01.jpg" width="500" height="500" alt="">--%>
                                    <div class="col-md-8 col-sm-12 imageContent">
                                        <div class="form-group">
                                            <label>Image Link: </label>
                                            <input type="text" class="form-control" name="imagelink" id="txtLinkWeb" />
                                        </div>
                                        <div class="checkboxControl clearfix">

















                                            <input type="checkbox" value="" name="check" id="chkNewWindow">
                                            <label for="chkNewWindow">Open in new window</label>

                                            <input type="hidden" clientidmode="static" id="hdnSlideShowImageId" runat="server" />
                                            <input type="hidden" clientidmode="static" id="hdnActiveSlideWeb" runat="server" />
                                            <input type="hidden" clientidmode="static" id="hdnSlideImageLoad" runat="server" />
                                            <input type="hidden" clientidmode="static" id="hdnTxtWebLink" runat="server" />
                                            <input type="hidden" clientidmode="static" id="hdnTxtAltText" runat="server" />
                                            <input type="hidden" clientidmode="static" id="hdnIsNewWindow" runat="server" />
                                            <input type="hidden" id="hdnSlideImagePosition" />
                                            <input id="hdnTrack" type="hidden" />
                                            <input type="hidden" clientidmode="static" id="hdnSaveData" runat="server" />
                                            <input type="hidden" clientidmode="static" id="hdnOverLayText" runat="server" />


                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 imageContent paddy0">
                                        <div class="form-group">
                                            <label>Alt Text: </label>
                                            <input type="text" id="txtAltText" class="form-control" name="alttext" />
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 imageContent">
                                        <div class="form-group" id="divOverLay" runat="server">
                                            <label>Over Lay Text: </label>
                                            <!-- <input type="text" id="txtOverLayText" maxlength="100" class="form-control" name="overlaytext" />-->
									<asp:TextBox id="txtOverLayText" runat="server" TextMode="MultiLine" Width="150%" Height="50%" CssClass="form-control" name="overlaytext"></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 form-group imageContent">
                                        <label></label>
                                        <button type="button" class="btn " clientidmode="static" runat="server" id="btnEdit" style="display: none">Edit </button>
                                    </div>

                                    <%--<a data-toggle="modal" data-target="#imageSlideshow" class="btn yellowWideBtn previewSlideshow ">Preview Slideshow</a>--%>
                                </div>

                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" id="divEditOVerLayText" style="display: none">
                                <p class="panel-title boldTxt">
                                    Edit Over lay text<br />

                                </p>
                                <div>
                                    <table class="overlay_text">
                                        <tr>
                                            <td>Language :

                                            </td>
                                            <td>
                                                <div class="select-style selectpicker">
                                                    <asp:DropDownList ID="ddlLanguage" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                                </div>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Over Lay Text
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEditOverLayText" ClientIDMode="Static" runat="server"></asp:TextBox>


                                            </td>
                                            <td>
                                                <span style="color: Red; display: none;" id="rspan">*</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <input class="save" type="button" value="Update" id="btnEditLanguage" />

                                            </td>
                                            <td>
                                                <input class="save" type="button" value="Cancel" id="btnEditCancel" />
                                            </td>
                                        </tr>

                                    </table>

                                </div>
                            </div>
                            <div class="panel-heading">
                                <p class="panel-title boldTxt">
                                    Select image slides for slideshow<br />
                                    Click & drag to change slide order<br /><br />
                                    Kindly ensure that all the uploaded images dimensions are similiar! 
                                </p>
                            </div>
                            <ul class="sortable">
                                <li class="imageDragPanel active" id="item1">
                                    <div class="imageContainer">
                                        <div class="imageBlk">

                                            <asp:Image CssClass="img-responsive" ClientIDMode="static" ID="id1" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>

                                            <asp:Literal ID="ltlSlideShowName1" runat="server" Text="Slide 1"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close1" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" value="check1" style="display:none" name="check" id="check1" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnLinkText1" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageAltText1" runat="server" />
                                        <%-- <input type="hidden" value="" id="hdnImageFileName1" />--%>
                                        <input type="hidden" clientidmode="static" id="hdnIsActive1" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnIsNewWindow1" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText1" runat="server" />
                                    </div>
                                </li>
                                <li class="imageDragPanel" id="item2">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ID="id2" ClientIDMode="static" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>

                                        <span>
                                            <asp:Literal ID="ltlSlideShowName2" runat="server" Text="Slide 2"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close2" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" value="check1" style="display:none" name="check" id="check2" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText2" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText2" runat="server" />
                                        <%-- <input type="hidden" value="" id="hdnImageFileName2" />--%>
                                        <input type="hidden" clientidmode="static" id="hdnIsActive2" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow2" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText2" runat="server" />
                                    </div>
                                </li>
                                <li class="imageDragPanel" id="item3">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ClientIDMode="static" ID="id3" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>

                                            <asp:Literal ID="ltlSlideShowName3" runat="server" Text="Slide 3"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close3" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" value="check1" style="display:none" name="check" id="check3" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText3" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText3" runat="server" />
                                        <input type="hidden" clientidmode="static" id="hdnIsActive3" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow3" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText3" runat="server" />
                                    </div>
                                </li>
                                <li class="imageDragPanel" id="item4">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ID="id4" ClientIDMode="static" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>

                                            <asp:Literal ID="ltlSlideShowName4" runat="server" Text="Slide 4"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close4" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" style="display:none" value="check1" name="check" id="check4" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText4" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText4" runat="server" />
                                        <input type="hidden" clientidmode="static" id="hdnIsActive4" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow4" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText4" runat="server" />
                                    </div>
                                </li>
                                <li class="imageDragPanel" id="item5">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ClientIDMode="static" ID="id5" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>

                                            <asp:Literal ID="ltlSlideShowName5" runat="server" Text="Slide 5"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close5" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" style="display:none" value="check1" name="check" id="check5" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText5" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText5" runat="server" />
                                        <input type="hidden" clientidmode="static" id="hdnIsActive5" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow5" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText5" runat="server" />
                                    </div>
                                </li>
                                <li class="imageDragPanel" id="item6">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ClientIDMode="static" ID="id6" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>

                                            <asp:Literal ID="ltlSlideShowName6" runat="server" Text="Slide 6"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close6" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" style="display:none" value="check1" name="check" id="check6" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText6" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText6" runat="server" />
                                        <input type="hidden" clientidmode="static" id="hdnIsActive6" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow6" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText6" runat="server" />
                                    </div>
                                </li>
                                <li class="imageDragPanel" id="item7">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ClientIDMode="static" ID="id7" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>

                                            <asp:Literal ID="ltlSlideShowName7" runat="server" Text="Slide 7"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close7" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" style="display:none" value="check1" name="check" id="check7" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText7" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText7" runat="server" />
                                        <input type="hidden" clientidmode="static" id="hdnIsActive7" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow7" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText7" runat="server" />
                                    </div>

                                </li>
                                <li class="imageDragPanel" id="item8">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ClientIDMode="static" ID="id8" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>
                                            <asp:Literal ID="ltlSlideShowName8" runat="server" Text="Slide 8"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close8" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" style="display:none" value="check1" name="check" id="check8" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText8" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText8" runat="server" />
                                        <input type="hidden" clientidmode="static" id="hdnIsActive8" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow8" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText8" runat="server" />
                                    </div>
                                </li>
                                <li class="imageDragPanel" id="item9">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ClientIDMode="static" ID="id9" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>
                                            <asp:Literal ID="ltlSlideShowName9" runat="server" Text="Slide 9"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close9" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" style="display:none" value="check1" name="check" id="check9" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText9" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText9" runat="server" />
                                        <input type="hidden" clientidmode="static" id="hdnIsActive9" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow9" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText9" runat="server" />
                                    </div>
                                </li>
                                <li class="imageDragPanel" id="item10">
                                    <div class="imageContainer">
                                        <div class="imageBlk">
                                            <asp:Image CssClass="img-responsive" ID="id10" ClientIDMode="static" Width="130" Height="100" AlternateText="image" runat="server" />

                                        </div>
                                        <span>
                                            <asp:Literal ID="ltlSlideShowName10" runat="server" Text="Slide 10"></asp:Literal></span>
                                        <a class="closeImageBtn" id="close10" style="display: none"></a>
                                        <input type="checkbox" class="checkBoxImage" style="display:none" value="check1" name="check" id="check10" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnLinkText10" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnImageAltText10" runat="server" />
                                        <input type="hidden" clientidmode="static" id="hdnIsActive10" runat="server" />
                                        <input type="hidden" clientidmode="static" value="" id="hdnIsNewWindow10" runat="server" />
                                        <input type="hidden" value="" clientidmode="static" id="hdnImageOverLayText10" runat="server" />
                                    </div>
                                </li>

                            </ul>

                        </div>

                        <ul class="button_section">
                            <li>
                                <button type="button" class="btn " id="btnSave">Save </button>
                            </li>
                            <li>
                                <button type="button" class="btn  gray" id="btnCancel">Cancel</button>
                            </li>
                        </ul>

                    
                </div>
            </div>





        </section>








    </div>

    <script type="text/javascript" src="../JS/jquery-ui.js"></script>
    <script>
        $(document).ready(function ($) {

            $(".sortable").sortable();
            $(".sortable").disableSelection();
        })
    </script>


    <link href="<%=host %>Admin/FineUploader/fineuploaderCategory-3.2.css" rel="stylesheet" />
    <script src="<%=host %>Admin/FineUploader/jquery.fineuploaderSlider-3.2.js"></script>

    <script>

        $(document).ready(function () {
           
            $('.closeImageBtn').mouseover(function(){
                $('.closeImageBtn').css('cursor','pointer');
            });

            if(($('#hdnImageOverLayText1').val()) == "")
            {
                   
                $('#btnEdit').css("display","none");
            }
            else
            {

                $('#btnEdit').css("display","block");
            }
            $('#btnEdit').click(function(){
                $('#divEditOVerLayText').show();
                //alert('Hello');
            
            });
            
           
            $('#ddlLanguage').change(function(){
                var slideshowimagid = $('#hdnSlideShowImageId').val();
                //  alert(slideshowimagid);
                $.ajax({
                    type:'POST',
                    url:'SlideShowImages.aspx/GetOverLayText',
                    contentType: 'application/json;charset=utf-8',
                    dataType:'JSON',
                    data:JSON.stringify({LanguageId:$('#ddlLanguage').val(), SlideShowImagId: slideshowimagid}),
                    error: function(errorResult)
                    {
                        alert(errorResult.responseText);
                    },
                    success:function(data)
                    {
                        //alert(data.d);
                        $('#txtEditOverLayText').val(data.d);

                    }


                });
               
            });
            
            $('#btnEditLanguage').click(function(){
                var slideshowimagid = $('#hdnSlideShowImageId').val();

                if($('#txtEditOverLayText').val() != '')
                {
                    //  alert(slideshowimagid);
                    $.ajax({
                        type:'POST',
                        url:'SlideShowImages.aspx/SetOverLayText',
                        contentType: 'application/json;charset=utf-8',
                        dataType:'JSON',
                        data:JSON.stringify({LanguageId:$('#ddlLanguage').val(), SlideShowImagId: slideshowimagid , OverLayText: $('#txtEditOverLayText').val()}),
                        error: function(errorResult)
                        {
                            alert(errorResult.responseText);
                        },
                        success:function(data)
                        {
                            //alert(data.d);
                            //show div message after working
                            alert(data.d);
                            $('#divEditOVerLayText').hide();
                        }


                    });
               
                }
                else{
                    $('#rspan').css("display","block");
                
                }

                
            });

            $('#btnEditCancel').click(function(){
            
                $('#divEditOVerLayText').hide()
            });
            
            $('#btnCancel').click(function(){

                
                var r = confirm("Are you sure want to cancel!");
                if (r == true) {
                    window.location.href='SliderListing.aspx';
                } 
                
                
            });
            var host ='<%=host %>';
            //$('.img-responsive').attr('src',host + 'images/slideshow/blankslide_thumb.jpeg');
            
            $('#FUSlideShow').html("");
            $('#FUSlideShow').html("<div class='qq-uploader'><ul class='qq-upload-list'><li class='qq-upload-success'><img style='width:100%' class='uploadedimage'></li></ul></div>");

            if($('#hdnSlideImageLoad').val() == "")
            {
                $('#hdnSlideImagePosition').val(1);
                $('#FUSlideShow').fineUploader({
                    request: {
                        endpoint: '<%=host %>' + 'Admin/fineUploader/uploadNewSS.aspx?type=slideshow&position='+$('#hdnSlideImagePosition').val()+'&slideshowimageid=<%=Guid.NewGuid().ToString()%>'
                    },
                    multiple: false,
                    validation: {
                        sizeLimit: <%= SlideShowImageSize %>,
                        allowedExtensions: [<%=SlideShowExtension%>]
                    },
                    text: {
                        uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                    },
                    retry: {
                        enableAuto: false
                    },
                    chunking: {
                        enabled: false,
                        partSize: <%= SlideShowImageSize %>
                        },
                    showMessage: function (message) {
                        // Using Bootstrap's classes
                        $('#FUSlideShow .fineError').hide();
                        $('#FUSlideShow .FailureDiv').hide();
                        $('#FUSlideShow').append('<div class="alert alert-error fineError">' + message + '</div>');
                        $('#error-alert').css('display','none');
                    },
                    debug: true
                });
      
            }
            else
            {
                $('.uploadedimage').attr('src',$('#hdnSlideImageLoad').val());
                $('#txtLinkWeb').val($('#hdnTxtWebLink').val());
                $('#txtLinkWeb').attr('placeholder','http://www.example.com');
                $('#ContentPlaceHolder1_txtOverLayText').val($('#hdnImageOverLayText1').val());
                $('#txtAltText').val($('#hdnTxtAltText').val());
                $('#txtEditOverLayText').val($('#hdnImageOverLayText1').val());
            }
            $('#txtLinkWeb').val($('#hdnTxtWebLink').val());
            $('#txtLinkWeb').attr('placeholder','http://www.example.com');
            $('#txtAltText').val($('#hdnTxtAltText').val());
            $('#ContentPlaceHolder1_txtOverLayText').val($('#hdnImageOverLayText1').val())
            $('#txtEditOverLayText').val($('#hdnImageOverLayText1').val());
          
            var IsNewWindow = $('#hdnIsNewWindow').val();
          
            if(IsNewWindow == "True")
            {
                $('#chkNewWindow').attr('checked',true);
            }
            else
            {
                $('#chkNewWindow').attr('checked',false);
            }
            
            
            for(var i = 1 ; i <= 10 ;i++)
            {
                

                if($('#id'+i).attr('src') != null)
                {
                    if($('#id'+i).attr('src').toLowerCase() != host + 'admin/images/ui/drop.png')
                    {
                       
                        if($('#id'+i).attr('src') !== host)
                        {
                            $('#close'+i).show();
                        }
                   
                   
                    }
                    else{

                       
                    }
                    if($('#hdnIsActive'+i).val() == "False")
                    {
                        $('#check'+i).attr('checked', false);
                    }
                    else{
                        $('#check'+i).attr('checked', true);
                    }
                    
                    
                }
                else
                {
                    $('#id'+i).attr('src',host + 'admin/images/ui/drop.png');
                    $('.closeImageBtn #close' + i).hide();
                    $('#check'+i).attr('checked', false);
                    
                
                }
               
                
            }

       
            
            //setTimeout(function(){$.initUpload()}, 1000);
            $('#txtLinkWeb').focus(function(){
             
                if($('.uploadedimage').attr('src') == null)
                {
                    alert('Please upload image first.');
                    // $('#error-alert').css('display','block');
                    //    document.getElementById('btnSave').disabled = true;
                    
                }
               
            });


            $('#txtLinkWeb').blur(function(){
                
                if($('#hdnTrack').val() == "")
                {
                    $('#hdnTrack').val("1");
                }
                var id = $('#hdnTrack').val();
               
                Validate();
                $('#hdnLinkText'+ id).val($('#txtLinkWeb').val());
                
                
             
            });
           
            
            $('#txtAltText').focus(function(){
             
                if($('.uploadedimage').attr('src') == null)
                {
                    //$('#error-alert').css('display','block');
                    alert('Please upload image first.');
                    //  document.getElementById('btnSave').disabled = true;
                    
                }
                
            });
            $('#txtAltText').on('click mouseout',function(){
                   
                var item = {};
                var jsonObj = [];
                if($('#hdnTrack').val() == "")
                {
                    $('#hdnTrack').val("1");
                }
                var id = $('#hdnTrack').val();
               
                if($('#divOverLay').css('display') == 'none')
                {
                    
                    if($('#hdnTrack').val() == "")
                    {
                        $('#hdnTrack').val("1");
                    }
                    var id = $('#hdnTrack').val();

                    
                    $('#hdnLinkText'+ id).val($('#txtLinkWeb').val());

                   
                    $('#hdnImageAltText'+ id).val($('#txtAltText').val());
                   
                    $('#hdnIsNewWindow'+ id).val($('#chkNewWindow').val())
                    
                 
                   
                   
                }
                

            
            });
            $('#ContentPlaceHolder1_txtOverLayText').focus(function(){
             
                if($('.uploadedimage').attr('src') == null)
                {
                    //$('#error-alert').css('display','block');
                    alert('Please upload image first.');
                    //  document.getElementById('btnSave').disabled = true;
                    
                }
                
            });
            $('#ContentPlaceHolder1_txtOverLayText').on('click mouseout',function(){
                
              
                if($('#hdnTrack').val() == "")
                {
                    $('#hdnTrack').val("1");
                }
                var id = $('#hdnTrack').val();

                
                $('#hdnLinkText'+ id).val($('#txtLinkWeb').val());

               
                $('#hdnImageAltText'+ id).val($('#txtAltText').val());
               
                $('#hdnIsNewWindow'+ id).val($('#chkNewWindow').val())
               
                $('#hdnImageOverLayText'+ id).val($('#ContentPlaceHolder1_txtOverLayText').val())
                
            
            });
               

            $('.imageBlk img').on("click",function(e){
              
                var id = e.target.id;
              
                var IsNewWindow ;
                var IsActive;
                var src = "";
                if(($('#hdnImageOverLayText'+ id.replace('id',"")).val()) == "")
                {
                   
                    $('#btnEdit').css("display","none");
                }
                else{
                    $('#btnEdit').css("display","block");
                }
              
                if($('#'+ id).attr("src") == null || $('#'+ id).attr("src") == host + 'admin/images/ui/drop.png')
                {
                    src = "NA";
                   
                    
                    $('#hdnTrack').val(id.replace('id',""));
                    $('#ContentPlaceHolder1_txtOverLayText').val('');
                    $('#txtLinkWeb').val('');
                    $('#txtLinkWeb').attr('placeholder','http://www.example.com');
                    $('#txtAltText').val('');
                    $('#txtEditOverLayText').val('');
                    $('#chkNewWindow').attr('checked',false);
                    $('#divEditOVerLayText').hide();
                    //   $('#btnEdit').hide();
                    $('#btnEdit').css("display","none");
                }
                else
                {
                   
                    src = $('#'+ id).attr("src");
                   
                    var rel = $('#'+ id).attr("rel");
                   
                    id= id.replace("id","");
                    $('#close' + id).show();
                    
                    IsNewWindow = IsNewWindow = $('#hdnIsNewWindow' +id).val();
                    IsActive = $('#hdnIsActive' +id).val();
                    $('#txtLinkWeb').val($('#hdnLinkText'+ id).val())
                    $('#txtAltText').val($('#hdnImageAltText'+ id).val())
                    $('#ContentPlaceHolder1_txtOverLayText').val($('#hdnImageOverLayText'+ id).val());
                    $('#txtEditOverLayText').val($('#hdnImageOverLayText'+ id).val());
                    $('#hdnTrack').val(id.replace('id',""));
                   
                    $('#hdnSlideShowImageId').val($('#id'+ id).attr("slideshowimageid"));
                   
                    IsNewWindow = $('#hdnIsNewWindow' +id).val();
                    IsActive = true;

                    //hdnIsNewWindow2
                    if(IsNewWindow == "True")
                    {
                        $('#chkNewWindow').attr('checked',true);
                    }
                    if(IsActive  == "True")
                    {
                        $('#check' + id).attr('checked',true);
                    }
                    // alert(rel);
                    src = src.replace("_Thumb","");
                }

                
               
                
                //alert(src);
                if(src != "NA")
                {
                  
                    $('#FUSlideShow').html("");
                    $('#FUSlideShow').html("<div class='qq-uploader'><ul class='qq-upload-list'><li class='qq-upload-success'><img style='width:100%' class='uploadedimage' src=''></li></ul></div>");
                    $('#FUSlideShow .uploadedimage').attr('src',src);
                    $('#txtLinkWeb').val($('#hdnLinkText'+ id).val())
                    $('#txtAltText').val($('#hdnImageAltText'+ id).val())
                    $('#ContentPlaceHolder1_txtOverLayText').val($('#hdnImageOverLayText'+ id).val());
                    $('#txEditOverLayText').val($('#hdnImageOverLayText '+ id).val())
                    $('#divEditOVerLayText').hide();
                   
                    //$('.qq-upload-success .uploadedimage').attr("src",src);
                    
                    //$('.qq-upload-success .uploadedimage').attr("src",src);
                 
                }
                else{
                  
                    //alert(id);
                    id= id.replace("id","");
                    $('#hdnSlideImagePosition').val(id);
                    
                  
                    $('#FUSlideShow').fineUploader({
                        request: {
                            endpoint: '<%=host %>' + 'Admin/fineUploader/uploadNewSS.aspx?position='+$('#hdnSlideImagePosition').val()+'&type=slideshow'
                        },
                        multiple: false,
                        validation: {
                            sizeLimit: <%= SlideShowImageSize %>,
                            allowedExtensions: [<%=SlideShowExtension%>]
                        },
                        text: {
                            uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                        },
                        retry: {
                            enableAuto: false
                        },
                        chunking: {
                            enabled: false,
                            partSize: <%= SlideShowImageSize %>
                            },
                        showMessage: function (message) {
                            // Using Bootstrap's classes
                            $('#FUSlideShow .fineError').hide();
                            $('#FUSlideShow .FailureDiv').hide();
                            document.getElementById("btnSave").disabled = false; 
                            $('#FUSlideShow').append('<div class="alert alert-error fineError">' + message + '</div>');
                            $('#error-alert').css('display','none');
                        },
                        debug: true
                    });
                               

                }
                
               
                
                //alert($('#hdnSlideImagePosition').val());
                
               
                $('.FailureDiv').hide();
            });
            
            $('#FUSlideShow').on("click",function(e){
               
                $('#FUSlideShow').load(host + 'Admin/fineUploader/uploadNewSS.aspx?position='+$('#hdnSlideImagePosition').val()+'&type=slideshow');

            });

           
            
            ///for validation of url link
            function Validate() {
                return true;
                var sMessage = '';

                //var myRegExp = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
                var myRegExp = /^(?:(?:https?|http):\/\/)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi ;
                if ($('#txtLinkWeb').val() != '') {
                    if (!myRegExp.test($('#txtLinkWeb').val())) {
                        sMessage += 'Please Enter Valid Link.\n';
                       
                    }
                }

                if (sMessage != "") {
                    alert(sMessage);
                    return false;   
                }
                else {
                    return true;
                }
            }

            $('.closeImageBtn').on("click",function(e){
                
                var id = e.target.id;

               
                
                id = id.replace("close","");
               
                // alert($(this).parent().parent().attr('id'));
                $('#id' + id).attr('slideshowimageid',0);
                $('#close'+id).hide();

                $('#txtLinkWeb').val('');
                $('#txtLinkWeb').attr('placeholder','http://www.example.com');
                $('#txtAltText').val('');
                $('#ContentPlaceHolder1_txtOverLayText').val('');
                $('#txtEditOverLayText').val('');

                //remove element from json object if present 

                if($('#hdnSaveData').val() != "")
                {
                    $('#hdnSaveData').val('');
                    
                }
                $('#id' + id).attr("slideshowimageid",'');
                $('#id' + id).attr('src',host + 'admin/images/ui/drop.png')
                $('#id' + id).attr("rel",'');
                $('#hdnLinkText' + id).val('');
                $('#hdnImageOverLayText' + id).val('');
                $('#id' + id).attr("filename",'');
                $('#check' + id).attr("checked",false);

                
                



                // alert(id);
                $('#hdnSlideImagePosition').val(id);

                $('#FUSlideShow').fineUploader({
                    request: {
                        endpoint: '<%=host %>' + 'Admin/fineUploader/uploadNewSS.aspx?position='+$('#hdnSlideImagePosition').val()+'&type=slideshow'
                    },
                    multiple: false,
                    validation: {
                        sizeLimit: <%= SlideShowImageSize %>,
                        allowedExtensions: [<%=SlideShowExtension%>]
                    },
                    text: {
                        uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                    },
                    retry: {
                        enableAuto: false
                    },
                    chunking: {
                        enabled: false,
                        partSize: <%= SlideShowImageSize %>
                        },
                    showMessage: function (message) {
                        // Using Bootstrap's classes
                        document.getElementById("btnSave").disabled = false; 

                        $('#FUSlideShow .fineError').hide();
                        $('#FUSlideShow .FailureDiv').hide();
                        $('#FUSlideShow').append('<div class="alert alert-error fineError">' + message + '</div>');
                        $('#error-alert').css('display','none');
                    },
                    debug: true
                });


            });



            
            $(function() {
                $( ".sortable").sortable({

                    connectWith: "sortable",
                    update : function () {
                        var order1 = $('.sortable').sortable('toArray');
                        var jsonObj = [];
                        var data = "";
                        
                        if($('#hdnSaveData').val() != "")
                        {
                        
                            $('#hdnSaveData').val('');
                        }
                        $.each( order1 , function( i, value ) {
                            i = i + 1;
                            // alert(value+ '_' + i); 
                            var item = {};

                            var element = $('#' + value).children().children();
                            var image = element.find('.img-responsive');
                            var slideshowimageid = element.find('.img-responsive').attr('slideshowimageid') 
                            var rel = element.find('.img-responsive').attr('rel');
                           
                            if(image.attr('src') != null)
                            {
                                element.find('.img-responsive').attr('rel',i);
                                $('.img-responsive #image'+i).attr('rel',i);
                            }
                           
                            
                          
                        });	
                       
                    }
                }).disableSelection();
            });

            $('#btnSave').click(function(){
                //alert('hello');
                var Data = "";
                if(Validate())
                {
                    var JsonData = $('#hdnSaveData').val();
                    if(JsonData == "")
                    {
                    
                        var jsonObj = [];
                   
                    
                        for(var id = 1 ; id <= 10 ;id++)
                        {
                            //if($('#id'+ i).attr('src') != "undefined" && $('#id'+ i).attr('src') != null)
                            //{
                       
                            
                            //if(($('#hdnLinkText'+ id).val() != "" && $('#hdnImageAltText'+ id).val() != "") || ($('#hdnImageOverLayText'+ id).val() != ""))
                            if($('#id'+ id).attr('src') != null)
                            {
                                if($('#id'+ id).attr('src') != "" && $('#id'+ id).attr('src')!= host +  'admin/images/ui/drop.png')
                                {
                                    var item = {};
                                    if($('#id' + id).attr("slideshowimageid") == "")
                                    {
                                        $('#id' + id).attr("slideshowimageid","0")
                                    }
                                
                                    item["SlideShowId"] = <%= Request.QueryString["id"] != null ? Request.QueryString["id"].ToString() : "0"%>
                                item["SlideShowImageId"] = $('#id' + id).attr("slideshowimageid")
                                    //resetting position while saving
                                    //$('#id' + id).attr("rel",id);

                                    //for(var j = 1 ; j <= 10 ; j++ )
                                    //{
                                    //    if($('#id' + j).attr('thumbname') == $('#hdnImageFileName' + j).val())
                                    //    {
                                    //        item["DisplayOrder"]=  $('#id' + j).attr('rel');
                                    //        break;
                                    //    }
                                    //}
                                

                                    item["DisplayOrder"]=  $('#id' + id).attr('rel');
                                    item["ImageExtension"]= $('#id' + id).attr("filename");

                                    item["NavigateUrl"]=  $('#hdnLinkText' + id).val();
                                    item["OverLayText"]= $('#hdnImageOverLayText' + id).val();
                                    item["AltText"] = $('#hdnImageAltText' + id).val()
                                    item["FilePath"]= $('#id' + id).attr("filename");

                                    
                                    if($('#chkNewWindow').is(":checked"))
                                    {
                                        IsNewWindow = true;
                                    }
                                    else
                                    {
                                        IsNewWindow = false;
                                    }
                           
                                    item["IsNewWindow"] = IsNewWindow;


                                    item["IsActive"] = true;
                                    if($('#hdnSaveData').val() != "")
                                    {
                                        jsonObj.push(item);
                                        $('#hdnSaveData').val($('#hdnSaveData').val() + ',' + JSON.stringify(jsonObj));
                                    }
                                    else
                                    {
                                        jsonObj.push(item);
                               
                            
                                    }
                                }
                            
                           
                            }
                       
                            //}
                        
                
                        }
                        debugger;
                        if(jsonObj != null)
                        {
                            JsonData = JSON.stringify(jsonObj);
                        }
                        if(JsonData == '[]')
                        {
                            alert('Please upload the slide show image and other details');
                            return false;
                        }
                    
                    }
               
               

                    $('#divMessage').css({ display: "none" });
                
               
                    $.ajax({
                        url: '<%=host %>' + 'Admin/SliderManagement/SlideShowImages.aspx?MethodName=Insert',
                        data: { W: JsonData },
                        dataType: 'html',
                        type: 'POST',
                        cache: false,
                        error: function (jqXHR, textResponse, errorThrown) {
                            alert(jqXHR.responseText);
                   
                        },
                        success: function (response) {
                            alert('saved successfully');
                            window.location.href='SliderListing.aspx';
                            //if (response.d.toLowerCase() == 'saved successfully') {
                            //    WStatus = true;
                            //    JsonData = "";
                                
                            //    //$('#divMessage').show();
                            //}
                            
                        }
                    });

                }
           
                

            });
                

        });

        


           
    
    </script>




</asp:Content>

