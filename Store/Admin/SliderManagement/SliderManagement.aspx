﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true"
    Inherits="Presentation.Admin_SliderManagement" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Slide Shows</a></li>
            <li>Update Slide Show Settings</li>
        </ul>
    </div>
    <!-- MiniColors -->
    <script src="../JS/jquery.minicolors.min.js"></script>
    <link rel="stylesheet" href="../CSS/jquery.minicolors.css">
    <script>   $(document).ready(function () {
       //SHRIGANESH CODE FOR OVERLAY SETTINGS DISPLAY 29 JAN 2016 START
       $('#spnddlOverlayTextFontSize').hide();
       $('#spntxtOverlayTextHeight').hide();
       $('#spntxtOverlayTextForeColor').hide();
       $('#spntxtOverlayTextBGColor').hide();
       $('#spnddlOverlayTextPosition').hide();
       //SHRIGANESH CODE FOR OVERLAY SETTINGS DISPLAY 29 JAN 2016 END

       //$('#txtSlideShowName').mouseout(function () {

       //    if ($('#txtSlideShowName').val() == "") {
       //        $('#rfvTxtSlideShowName').css('visibility', 'visible');
       //    }


       //});
       //$('#txtSlideEffectTime').mouseout(function () {

       //    if ($('#txtSlideEffectTime').val() == "")
       //    {
       //        $('#rfvTxtSlideEffectTime').css('visibility', 'visible');
       //    }


       //});
       //$('#txtSlideDelayTime').mouseout(function () {
       //    if ($('#txtSlideDelayTime').val() == "") {
       //        $('#rfvTxtSlideDelayTime').css('visibility', 'visible');
       //    }


       //});
       //$('#ddlSlideShowMaster').mouseout(function () {
       //    if ($('#ddlSlideShowMaster').val() == "0") {
       //        $('#rfvDdlSlideShowMaster').css('visibility', 'visible');
       //    }


       //});

       /* Image width
       $('#ddlWidthUnit').change(function () {

           var widthUnit = $('#ddlWidthUnit').val();
           var width = $('#txtWidth').val();

           if (widthUnit == "1") {
               $('#txtWidth').attr('maxlength', '3');
               if (width > 100) {

                   $('#txtWidthError').css('display', 'block');
               }
               else {
                   $('#txtWidthError').css('display', 'none');
               }

           }
           else if (widthUnit == "2") {

               if (width > 100) {

                   $('#txtWidthError').css('display', 'none');
               }


               $('#txtWidth').attr('maxlength', '4');

           }
           else {
               $('#txtWidthError').css('display', 'none');
           }

       });

       $('#txtWidth').keydown(function () {


           var widthUnit = $('#ddlWidthUnit').val();
           var width = $('#txtWidth').val();
           if (widthUnit == "1") {
               $('#txtWidth').attr('maxlength', '3');
               if (width > 100) {

                   $('#txtWidthError').css('display', 'block');
               }
               else {
                   $('#txtWidthError').css('display', 'none');
               }

           }
           else if (widthUnit == "2") {

               if (width > 100) {

                   $('#txtWidthError').css('display', 'none');
               }

               $('#txtWidth').attr('maxlength', '4');

           }

       });

       $('#txtWidth').blur(function () {

           var widthUnit = $('#ddlWidthUnit').val();
           var width = $('#txtWidth').val();
           if (widthUnit == "1") {
               $('#txtWidth').attr('maxlength', '3');
               if (width > 100) {

                   $('#txtWidthError').css('display', 'block');
               }
               else {
                   $('#txtWidthError').css('display', 'none');
               }

           }
           else if (widthUnit == "2") {

               if (width > 100) {

                   $('#txtWidthError').css('display', 'none');
               }

               $('#txtWidth').attr('maxlength', '4');

           }

       });*/

       $('input.colorSelection').minicolors();
       if ($('#chkIsOverLayText').is(':checked')) {
           // debugger;
           $('#liFont').show();
           $('#liForeColor').show();
           $('#liBackColor').show();
           $('#tblOverlaySettings').show();


       }
       else {
           $('#liFont').hide();
           $('#liForeColor').hide();
           $('#liBackColor').hide();
           $('#tblOverlaySettings').hide();

           $('#rfvalDdlFontStyle').remove();
           $('#rfvalTxtForeColor').remove();
           $('#rfvalTxtBgColor').remove
           $('#rgvtxtBgColor').remove();
           $('#rgvtxtForeColor').remove();
           $('#rfvalDdlFontStyle').remove();


       }
       $('#chkIsOverLayText').click(function () {
           if ($(this).is(':checked')) {
               // debugger;
               $('#liFont').show();
               $('#liForeColor').show();
               $('#liBackColor').show();
               $('#tblOverlaySettings').show();

               //$('#rfvalDdlFontStyle').add();
               //$('#rfvalTxtForeColor').add();
               //$('#rfvalTxtBgColor').add();
               //$('#rgvtxtBgColor').add();
               //$('#rgvtxtForeColor').add();
               //$('#rfvalDdlFontStyle').add();


           }
           else {

               $('#liFont').hide();
               $('#liForeColor').hide();
               $('#liBackColor').hide();
               $('#tblOverlaySettings').hide();

               //$('#rfvalDdlFontStyle').remove();
               //$('#rfvalTxtForeColor').remove();
               //$('#rfvalTxtBgColor').remove
               //$('#rgvtxtBgColor').remove();
               //$('#rgvtxtForeColor').remove();
               //$('#rfvalDdlFontStyle').remove();


           }
           $('#txtForeColor').blur(function () {

               if ($('#txtForeColor').val() != '') {
                   $('#txtForeColorError').css('display', 'none');
               }
               else {

                   $('#txtForeColorError').css('display', 'block');
               }
           });

           $('#txtBgColor').blur(function () {
               if ($('#txtBgColor').val() != '') {

                   $('#txtBgColorError').css('display', 'none');
               }
               else {
                   $('#txtBgColorError').css('display', 'block');
               }
           });
       });

       //$('#btnSave').click(function () {
       //    return Validate();
       //});
   })
        function Validate() {
            //debugger;

            if ($('#chkIsOverLayText').is(':checked')) {
                if ($('#ddlFontStyle').val() == "0") {
                    $('#ddlFontStyleError').css('display', 'block');
                    return false;
                }
                if ($('#txtForeColor').val() == "") {
                    $('#txtForeColorError').css('display', 'block');
                    return false;
                }

                if ($('#txtBgColor').val() == "") {
                    $('#txtBgColorError').css('display', 'block');
                    return false;
                }

                if ($('#ddlOverlayTextFontSize').val() == "0" || $('#ddlOverlayTextFontSize').val() == "") {
                    $('#spnddlOverlayTextFontSize').show();
                    return false;
                }

                if ($('#txtOverlayTextHeight').val() == "") {
                    $('#spntxtOverlayTextHeight').show();
                    return false;
                }

                if ($('#txtOverlayTextForeColor').val() == "") {
                    $('#spntxtOverlayTextForeColor').show();
                    return false;
                }

                if ($('#txtOverlayTextBGColor').val() == "") {
                    $('#spntxtOverlayTextBGColor').show();
                    return false;
                }

                if ($('#ddlOverlayTextPosition').val() == "0" || $('#ddlOverlayTextPosition').val() == "") {
                    $('#spnddlOverlayTextPosition').show();
                    return false;
                }

            }
            else {

                $('#ddlFontStyleError').css('display', 'none');
                $('#txtForeColorError').css('display', 'none');
                $('#txtBgColorError').css('display', 'none');
                $('#txtWidthError').css('display', 'none');
                $('#spnddlOverlayTextFontSize').hide();
                $('#spnddlOverlayTextPosition').hide();
                $('#spntxtOverlayTextHeight').hide();
                $('#spntxtOverlayTextForeColor').hide();
                $('#spntxtOverlayTextBGColor').hide();
                return false;
            }
        }
    </script>
    <style>
        .minicolors-theme-default .minicolors-input {
            height: auto;
            line-height: 20px;
        }
    </style>

    <div class="admin_page ">

        <div class="container ">
            <div id="divMessage" visible="false" runat="server">

                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>
                        <asp:Literal ID="ltrMessage" runat="server" /></strong>
                </div>

            </div>
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>Slideshow</h3>
                            <p>Manage the settings for the selected slideshow option using the form below.</p>
                        </div>

                    </div>
                    <div class="slideshow select_data ">
                        <ul>

                            <li>
                                <div class="col-md-3">
                                    <h6>Slideshow Widget Block <span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9 slide">

                                    <div class="size01">
                                        <div class="select-style selectpicker slidesize01">
                                            <asp:DropDownList ID="ddlSlideShowMaster" AutoPostBack="true" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="ddlSlideShowMaster_SelectedIndexChanged">
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <%-- <asp:RequiredFieldValidator ClientIDMode="Static" ValidationGroup="formValid" ID="rfvDdlSlideShowMaster" runat="server" ControlToValidate="ddlSlideShowMaster" InitialValue="0" ErrorMessage="(Required)" Display="Static" Text="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                </div>

                            </li>

                            <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        Enter the name of this slideshow. This name is only for your indicative purpose, and will not be displayed to users.
                                        
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <h6>Slideshow Name <span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9">
                                    <br />
                                    <asp:TextBox ID="txtSlideShowName" ClientIDMode="Static" runat="server" MaxLength="50" CssClass="size01"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ValidationGroup="formValid" ClientIDMode="Static" ID="rfvTxtSlideShowName" runat="server" ControlToValidate="txtSlideShowName" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                </div>
                            </li>

                            <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        What is the delay you want for slide changes?
                                        
                                    </p>
                                </div>

                                <div class="col-md-3">
                                    <br />
                                    <h6>Delay (in milliseconds) <span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9">
                                    <br />
                                    <asp:TextBox ID="txtSlideDelayTime" ClientIDMode="Static" runat="server" MaxLength="4" CssClass="size01"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator  ClientIDMode="Static" ValidationGroup="formValid" ID="rfvTxtSlideDelayTime" runat="server" ControlToValidate="txtSlideDelayTime" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                    <asp:RegularExpressionValidator ClientIDMode="Static" ID="rgvTxtSlideDelayTime" ControlToValidate="txtSlideDelayTime" ValidationGroup="formValid" runat="server" ErrorMessage="Only Numbers Allowed." ForeColor="#ff0000" ValidationExpression="\d+"></asp:RegularExpressionValidator>

                                </div>
                            </li>
                            <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        What is the duration you want for slide effects?
                                        
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <h6>Effect Duration (in milliseconds) <span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9">
                                    <br />
                                    <asp:TextBox ID="txtSlideEffectTime" ClientIDMode="Static" runat="server" MaxLength="4" CssClass="size01"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ClientIDMode="Static" ValidationGroup="formValid" ID="rfvTxtSlideEffectTime" runat="server" ControlToValidate="txtSlideEffectTime" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                    <asp:RegularExpressionValidator ClientIDMode="Static" ID="rgvTxtSlideEffectTime" ControlToValidate="txtSlideEffectTime" ValidationGroup="formValid" runat="server" ErrorMessage="Only Numbers Allowed." ForeColor="#ff0000" ValidationExpression="\d+"></asp:RegularExpressionValidator>

                                </div>
                            </li>


                            <%--   <li>

                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        Check/Uncheck box to activate or deactivate the slider
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <h6>Is Active</h6>
                                </div>


                                <div class="col-md-9">
                                    <br />
                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="size01"></asp:CheckBox>

                                </div>
                            </li>--%>

                            <%-- BELOW PANEL CODE IS ADDED BY SHRIGANESH SINGH 29 JAN 2016  TO PROVIDE SETTINGS FOR OVERLAY TEXT DISPLAY --%>

                            <asp:Panel ID="pnlOverlayTextSettings" Visible="false" runat="server">
                                <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        Check/Uncheck box to keep overlay text
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <h6>Is Overy Lay Text</h6>
                                </div>
                                <div class="col-md-9">
                                    <br />
                                    <asp:CheckBox ID="chkIsOverLayText" runat="server" ClientIDMode="Static" CssClass="size01"></asp:CheckBox>
                                </div>

                                <table id="tblOverlaySettings">
                                    <tr>
                                        <td>
                                            <br />
                                                <h6>Select font size of the Overlay Text <span style="color: red">*</span>:</h6>
                                            
                                        </td>
                                        <td>
                                            <%--<div class="slide">
                                                <div class="size01">
                                                    <div class="select-style selectpicker slidesize01">--%>
                                            <br />
                                                        <asp:DropDownList ID="ddlOverlayTextFontSize" runat="server" ClientIDMode="Static">                                                            
                                                        </asp:DropDownList>
                                                        <span id="spnddlOverlayTextFontSize" style="color: red">Required</span>
                                                  <%--  </div>
                                                </div>
                                            </div>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>    
                                            <br />                                                                                        
                                                <h6>Enter Height of the Overlay <span style="color: red">*</span>:</h6>                                            
                                        </td>
                                        <td>
                                            <br />                                                
                                                <asp:TextBox ID="txtOverlayTextHeight" runat="server" ClientIDMode="Static" MaxLength="3" CssClass="size01"></asp:TextBox>  
                                            <span id="spntxtOverlayTextHeight" style="color: red">Required</span>                                         
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <br />
                                                <h6>Overlay Fore Color <span style="color: red">*</span>:</h6>
                                            </div>
                                        </td>
                                        <td>
                                           
                                                <br />
                                                <asp:TextBox ID="txtOverlayTextForeColor" CssClass="form-control colorSelection" data-control="hue" Text="#ff6161" runat="server" ClientIDMode="Static"></asp:TextBox>
                                           <span id="spntxtOverlayTextForeColor" style="color: red">Required</span>                                         
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <br />
                                                <h6>Overlay Background Color <span style="color: red">*</span>:</h6>
                                            </div>
                                        </td>
                                        <td>
                                            
                                                <br />
                                                <asp:TextBox ID="txtOverlayTextBGColor" CssClass="form-control colorSelection" data-control="hue" Text="#ff6161" runat="server" ClientIDMode="Static"></asp:TextBox>
                                            <span id="spntxtOverlayTextBGColor" style="color: red">Required</span>  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <br />
                                                <h6>Select Position of the Overlay Text <span style="color: red">*</span>:</h6>
                                            </div>
                                        </td>
                                        <td class="slide">
                                            <%--<div class="size01">
                                                <div class="select-style selectpicker slidesize01">--%>
                                                    <asp:DropDownList ID="ddlOverlayTextPosition" runat="server" ClientIDMode="Static">
                                                        <asp:ListItem Text="Select Position" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="TOP" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="BOTTOM" Value="2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <span id="spnddlOverlayTextPosition" style="color: red">Required</span>  
                                            <%--    </div>
                                            </div>--%>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            </asp:Panel>                            
                            <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        Check/Uncheck box to display pagination
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <h6>Show Pagination</h6>
                                </div>
                                <div class="col-md-9">
                                    <br />
                                    <asp:CheckBox ID="chkShowPagination" runat="server" ClientIDMode="Static" CssClass="size01"></asp:CheckBox>
                                </div>
                            </li>
                            <asp:Panel ID="pnlNerveHeight" Visible="false" runat="server">
                                <li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            Nerve Slider images height
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        <h6>Height</h6>
                                    </div>
                                    <div class="col-md-9">
                                        <br />
                                        <asp:TextBox ID="txtHeight" runat="server" MaxLength="3" CssClass="size01"></asp:TextBox>
                                    </div>
                                </li>
                            </asp:Panel>
                            <%--<li id="liFont">
                                
                                <div class="col-md-3">
                                    <h6>Font <span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9">
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlFontStyle" runat="server" OnDataBound="ddlFontStyle_DataBound" ClientIDMode="Static">
                                        </asp:DropDownList>

                                    </div>
                                </div>
                            </li>
                            <li id="liForeColor">

                                <div class="col-md-3">

                                    <h6>Text Fore Color <span style="color: red">*</span>:</h6>
                                </div>

                                <div class="col-md-9">


                                    <asp:TextBox ID="txtForeColor" CssClass="form-control colorSelection" data-control="hue" Text="#ff6161" runat="server" ClientIDMode="Static"></asp:TextBox>
                                  
                                </div>

                            </li>
                            <li id="liBackColor">

                                <div class="col-md-3">
                                    <h6>Text Background Color <span style="color: red">*</span>:</h6>
                                </div>

                                <div class="col-md-9">

                                    <asp:TextBox ID="txtBgColor" CssClass="form-control colorSelection" data-control="hue" Text="#ff6161" runat="server" ClientIDMode="Static"></asp:TextBox>
                                


                                </div>

                            </li>--%>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="buttonPanel container">
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="btnSave" ValidationGroup="formValid" ClientIDMode="Static" runat="server" CssClass="btn" Text="Save" AccessKey="S" OnClick="Btn_Click" CommandArgument="Save" />
                        </li>
                        <li>
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn gray" Text="Cancel" AccessKey="C" OnClick="Btn_Click" OnClientClick="return confirm('are you sure want to cancel ?')" CommandArgument="Cancel" />
                        </li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
</asp:Content>

