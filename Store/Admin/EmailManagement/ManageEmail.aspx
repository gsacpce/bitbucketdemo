﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_EmailManagement_ManageEmail" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Email </a></li>
            <li>Email Template Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12 relative_data">
                            <h3>Email Template Management
                            </h3>
                            <p>Email templates are used as the basis of messages that are sent to customers.</p>
                            <div class="right_selct_dropdwn">
                                <div style="float: right" id="dvLang" runat="server" visible="false">
                                    <label class="sm_select">Select Language :</label>
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="manage_email">

                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">Email Template Name <span style="color: red">*</span>: </label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtEmailName" runat="server" MaxLength="50" onblur="return CheckTextbox(this.id)" autocomplete="off" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">Email Subject <span style="color: red">*</span>: </label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtSubject" runat="server" MaxLength="255" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">From Email ID <span style="color: red">*</span>: </label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtFromEmailId" runat="server" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">CC Email ID : </label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtCCEmailId" runat="server" autocomplete="off"></asp:TextBox>

                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">BCC Email ID : </label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtBCCEmailId" runat="server" autocomplete="off"></asp:TextBox>

                                </div>
                            </div>
                            <div class="form-group clearfix" id="dvVariable" runat="server">
                                <label class="control-label col-sm-3" for="name">Variable : </label>
                                <div class="col-md-8 col-sm-3">
                                    <div class="select-style selectpicker pos_i pos_top">
                                        <asp:DropDownList ID="ddlVariable" runat="server">
                                        </asp:DropDownList>
                                    </div>

                                    <p class="pos_i pos_i2">
                                        <a href="javascript:void(0);" data-toggle="popover" data-content="test hover">
                                            <img src="../Images/UI/info-icon.png" alt="" />
                                        </a>
                                    </p>
                                    <asp:Button ID="btnInsert" runat="server" Text="Insert" OnClientClick="return InsertHTML();" class="btn" CssClass="pos_i pos_i2" />
                                </div>
                                <div class="col-md-6 col-sm-10">
                                </div>
                                <div class="col-md-1 col-sm-10">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">Email Body <span style="color: red">*</span>: </label>
                                <div class="col-md-9 col-sm-10">
                                    <CKEditor:CKEditorControl ID="txtEmailBody" BasePath="~/admin/ckeditor/" runat="server"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="content">
                </div>
                <div class="buttonPanel container">
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="btnSave" runat="server" class="btn" Text="Update" OnClick="btnSaveExit_Click" OnClientClick="return ValidateRequiredFields();" />
                        </li>
                        <li>
                            <asp:Button ID="btnCancel" runat="server" class="btn gray" Text="Cancel" OnClick="btnCancel_Click" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({
                placement: 'right'
            });
        });
    </script>
    <script type="text/javascript" language="javascript">
        function InsertHTML() {
            var Variables = document.getElementById('<%= ddlVariable.ClientID %>');
            var Variables_Text = Variables.options[Variables.selectedIndex].text;
            var Variables_Value = Variables.options[Variables.selectedIndex].value;
            if (Variables_Value != '0') {
                CKEDITOR.instances['<%= txtEmailBody.ClientID %>'].insertText('@' + Variables_Text.replace(/ /g, '_'));
            }
            else {
                alert('Kindly select variable to insert.');
            }
            return false;
        }

        function CheckTextbox(clientId) {

            var obj = document.getElementById(clientId);
            var iChars = "!@#$%^\"&+=[]{}|:<>?"
            for (var i = 0; i < obj.value.length; i++) {
                if (iChars.indexOf(obj.value.charAt(i)) != -1) {
                    alert("Text has special characters. \nThese are not allowed.\n Please remove them and try again.");
                    obj.value = "";
                    return false;
                }
            }
            return true;
        }

        function ValidateRequiredFields() {
            var _Return = true;
            var msg = '';
            if (document.getElementById('<%= txtEmailName.ClientID %>').value.trim() == '') {
                msg += 'Please enter a name for email template.\n';
            }
            if (document.getElementById('<%= txtSubject.ClientID %>').value.trim() == '') {
                msg += 'Please enter a subject for email template.\n';
            }
            if (document.getElementById('<%= txtFromEmailId.ClientID %>').value.trim() == '') {
                msg += 'Please enter a from email address for email template.\n';
            }
            if (CKEDITOR.instances['<%= txtEmailBody.ClientID %>'].getData().trim() == '') {
                msg += 'Please enter some body content for email template.\n';
            }
            if (msg.length > 0) {
                _Return = false;
                alert(msg);
            }
            return _Return;
        }
    </script>
</asp:Content>
