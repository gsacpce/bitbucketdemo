﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_EmailManagement_EmailListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Email </a></li>
            <li>Email Template Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <h3 class="mainHead">Email Template Management</h3>
                    <p>Email templates are used as the basis of messages that are sent to customers.</p>
                    <div class="butonblock nospace clearfix ">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <asp:Button ID="btnAddEmailTemplate" class="btn3" runat="server" Text="Manage Email Template" OnClick="btnAddEmailTemplate_Click" />
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="midleDrgblecontin">
                        <ul class="dragblnextBlock">
                            <li>
                                <asp:GridView ID="grdEmailTemplate" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                    BorderWidth="0" CellPadding="0" CellSpacing="0" class="all_customer_inner dragbltop"
                                    EmptyDataText="Email template has not been created." OnPageIndexChanging="grdEmailTemplate_PageIndexChanging"
                                    PageSize="20" Width="100%">
                                    <Columns>
                                        <asp:TemplateField AccessibleHeaderText="EmailTemplateName,EmailTemplateId" HeaderStyle-CssClass="bg"
                                            HeaderText="Template Name">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltlEmailTemplateName" runat="server" Text='<%# Eval("EmailTemplateName")%>'></asp:Literal>
                                                <asp:HiddenField ID="hfEmailTemplateId" runat="server" Value='<%# Eval("EmailTemplateId")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField AccessibleHeaderText="Subject" HeaderStyle-CssClass="bg" HeaderText="Subject">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltlSubject" runat="server" Text='<%# Eval("Subject")%>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div id="dvAction" runat="server" class="view_order other_option">
                                                            <asp:LinkButton Text="Edit" runat="server" OnCommand="lbtnEdit_Command" ID="lnkbtnEdit"
                                                                CommandName="EditEmailTemplate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EmailTemplateId") %>' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="midleDrgblecontin">
                    <asp:Label ID="lblStoreNameEmail" runat="server" Text="Enter Store Name to be appended in From Email:"></asp:Label>
                    <asp:TextBox ID="txtStoreNameEmail" runat="server"></asp:TextBox>
                    <asp:Button ID="btnSaveUpdate" runat="server" class="btn3" Text="Save / Update" ToolTip="Click here to Save & Update" OnClick="btnSaveUpdate_Click" />
                </div>
            </div>
        </section>
    </div>
</asp:Content>
