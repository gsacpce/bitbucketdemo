﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master"  EnableViewStateMac="false" AutoEventWireup="true" Inherits="Presentation.Admin_ZoneManagement_ZoneManagement" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .categoryMidlBlock {
            float: left;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Region Management</li>
        </ul>
    </div>
    <div class="admin_page">

        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-5">
                            <h3 class="mainHead">Region Management</h3>
                        </div>
                        <span class="sectionText"></span>
                        <div class="clearfix"></div>
                        <div class="col-md-12 manage_st_content">
                            <div class="categoriesmainBlock" style="padding: 0;">
                                <div id="divMessage" visible="false" runat="server">
                                    <div class="alert alert-success">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>
                                            <asp:Literal ID="ltrMessage" runat="server" /></strong>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-1">Filter</div>
                                    <div class="col-md-5">
                                        <asp:DropDownList ID="ddlSelectRegion" runat="server" AutoPostBack="true" CssClass="form-control filter" Width="50%" ClientIDMode="Static" OnSelectedIndexChanged="ddlSelectRegion_SelectedIndexChanged" ></asp:DropDownList>
                                    </div>
                                    <div class="button_section">
                                       
                                                <asp:Button ID="btnManageRegion" runat="server" CssClass="btn" Text="Manage Region" OnClick="btnManageRegion_Click"   />
                                           
                                    </div>
                                    <%--                                    <div class="col-md-1">
                                        <input type="radio" class="filter" id="All" name="A" />
                                        All
                                    </div>
                                    <div class="col-md-1">
                                        <input type="radio" class="filter" id="UK" name="A" />
                                        UK
                                    </div>
                                    <div class="col-md-1">
                                        <input type="radio" class="filter" id="EU" name="A" />
                                        EU
                                    </div>
                                    <div class="col-md-1">
                                        <input type="radio" class="filter" id="ROW" name="A" />
                                        ROW
                                    </div>
                                    <div class="col-md-2">
                                        <input type="radio" class="filter" id="UA" name="A" />
                                        Unallocated
                                    </div>--%>
                                </div>
                                <div class="buttonPanel container">
                                    <ul class="button_section">
                                        <li>
                                            <asp:Button ID="Button1" runat="server" OnClientClick="javascript:ShowLoader();" CssClass="btn" Text="Save" OnClick="btnSave_OnClick" />
                                        </li>
                                        <li>
                                            <asp:Button ID="Button2" CssClass="btn gray" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
                                        </li>
                                    </ul>
                                </div>
                                <div class="categoryMidlBlock">
                                    <div class="midleDrgblecontin">
                                        <ul>
                                            <li>
                                                <table style="width: 100%" class="dragbltop">
                                                    <tbody>
                                                        <tr>
                                                            <th class="firsttd">Country
                                                            </th>
                                                            <th class="secondtd">Zone
                                                            </th>
                                                            <th>Allocate/Un-allocate</th>
                                                            <th>Display Duty Message</th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </li>

                                        </ul>
                                        <ul class="dragblnextBlock" id="Grid">
                                            <asp:Repeater ID="rpZoneManagement" runat="server" OnItemDataBound="rpZoneManagement_OnItemDataBound">
                                                <ItemTemplate>
                                                    <li id='<%# Eval("RegionCode") %>' class='<%# "firstLitab " + Eval("RegionCode") %>'>
                                                        <table style="width: 100%" class="dragbltop">
                                                            <tr>
                                                                <td class="firsttd">
                                                                    <span>
                                                                        <asp:HiddenField ID="hdfCountryID" runat="server" Value='<%# Bind("CountryId") %>' Visible="false" />
                                                                        <asp:Label ID="lblCountry" runat="server" Text='<%# Bind("CountryName") %>'></asp:Label>
                                                                    </span>
                                                                </td>
                                                                <td class="secondtd">
                                                                    <asp:DropDownList ID="ddlZone" runat="server">
                                                                       <%-- <asp:ListItem Text="EU" Value="EU"></asp:ListItem>
                                                                        <asp:ListItem Text="ROW" Value="ROW"></asp:ListItem>
                                                                        <asp:ListItem Text="UK" Value="UK"></asp:ListItem>
                                                                        <asp:ListItem Text="Unallocated" Value="UA"></asp:ListItem>--%>
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="hdfddlZoneVal" runat="server" Visible="false" />
                                                                </td>
                                                                 <td>
                                                                                                                                               
                                                                    <asp:CheckBox ID="chkUnAllocated" runat="server" AutoPostBack="true"  OnCheckedChanged="chkUnAllocated_CheckedChanged" />
                                                                    
                                                                    <%--<asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Unallocated") %>' />--%>
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBox ID="chkDutyMessage" runat="server" Checked='<%# Bind("DisplayDutyMessage") %>' />

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                                <div class="buttonPanel container">
                                    <ul class="button_section">
                                        <li>
                                            <asp:Button ID="btnSave" runat="server" OnClientClick="javascript:ShowLoader();" CssClass="btn" Text="Save" OnClick="btnSave_OnClick" />
                                        </li>
                                        <li>
                                            <asp:Button ID="btnCancel" CssClass="btn gray" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <%--<asp:Repeater ID="rpZoneManagement" runat="server" OnItemDataBound="rpZoneManagement_OnItemDataBound">
        <HeaderTemplate>
            <table>
                <<tr>
                    <td>Sr No</td>
                    <td>Country</td>
                    <td>Zone</td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lblCountry" runat="server" Text='<%# Bind("CountryName") %>'></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddlZone" runat="server">
                        <asp:ListItem Text="EU" Value="EU"></asp:ListItem>
                        <asp:ListItem Text="ROW" Value="ROW"></asp:ListItem>
                        <asp:ListItem Text="UK" Value="UK"></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            <tr>
                <td colspan="3"></td>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>--%>
<%--    <script>
        $(document).ready(function () {
            $('.filter').click(function () {
                var currentFilter = $(this).attr('id');
                $("#Grid li").each(function () {
                    alert(currentFilter);
                    if (currentFilter == $(this).attr('id')) {
                        $(this).show();
                    } else if (currentFilter == "All") {
                        $(this).show();
                    }
                    else {
                        $(this).hide();
                    }
                });

            });
        });

    </script>--%>
  
</asp:Content>

