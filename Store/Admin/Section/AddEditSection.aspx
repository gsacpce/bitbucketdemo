﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Section_AddEditSection" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <asp:Literal ID="litJS" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .paddingTop {
            padding-top: 15px;
        }

        .paddingLeft {
            padding-left: 35px;
            width: 294px;
        }

        .spanStyle {
            padding-left: 45px;
            line-height: 25px;
        }

        .spanPaddingLeft {
            padding-left: 35px;
        }

        .noBottomBorder {
            border-bottom: none !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Section </a></li>
            <li>Section Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12 relative_data">
                            <div class="col-md-7">
                                <h3>
                                    <asp:Label ID="lblHeading" runat="server">Section Management</asp:Label>
                                </h3>
                                <p>Products in your website can be grouped by sections.  Fill out the form below to create a new section.</p>
                            </div>

                            <div class="col-md-5  text-right">

                                <label class="col-md-5 text-right sm_select" id="select_lang">
                                    Select Language :
                                </label>
                                <div style="float: right" id="divLang" runat="server" class="col-md-7">
                                    <div class="select-style  selectpicker languageDropdown">
                                        <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="collapsableMenu ">
                        <a aria-expanded="true" data-target="#webPage" data-toggle="collapse" class="btn btn-info" type="button">Section Details</a>
                        <div aria-expanded="true" class="collapse in coloredChkBox" id="webPage">
                            <div class="row collapsableContent">
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2" for="name">Name <span style="color: red">*</span>: </label>
                                    <div class="col-md-3 col-sm-5">
                                        <asp:TextBox class="form-control" ID="txtName" runat="server" onblur="return CheckTextbox(this.id)" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2" for="category">Description:</label>
                                    <div class="col-md-9 col-sm-10">
                                        <CKEditor:CKEditorControl ID="txtDescription" BasePath="~/admin/ckeditor/" runat="server" onblur="return CheckTextbox(this.id)"></CKEditor:CKEditorControl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="collapsableMenu">
                        <a aria-expanded="true" data-target="#Images" data-toggle="collapse" class="btn btn-info" type="button">Images</a>
                        <div aria-expanded="true" class="collapse in coloredChkBox" id="Images">
                            <div class="row collapsableContent">
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2" for="uploadImage">Upload Section Icon:</label>
                                    <div class="col-md-3 col-sm-5 imageSectionIcon" id="divUploadImage" runat="server">
                                        <div id="FUIcon">
                                        </div>
                                        <div class="uploaderMsg" style="width: 500px">[USe .jpg, .jpeg, .png file <%--with <= 200px X 200px and--%>  <=2.00 MB size only]</div>
                                    </div>
                                </div>
                                <div id="divViewDeleteImage" runat="server" visible="false">
                                    <asp:LinkButton ID="lnkImageView" runat="server" CssClass="links linksedit" ToolTip="Click to view image">View Image</asp:LinkButton>
                                    <asp:LinkButton ID="lnkImageDelete" runat="server" CssClass="links linksedit" OnClientClick="javascript:return confirm('Are you sure you want to delete this image?')"
                                        ToolTip="Click to delete image" OnClick="lnkImageDelete_Click">Delete Image</asp:LinkButton>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label col-sm-2">Image Alt:<span style="color: red">*</span>: </label>
                                    <div class="col-md-3 col-sm-5">
                                        <asp:TextBox class="form-control" ID="txtIconAlt" runat="server" onblur="return CheckTextbox(this.id)" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--Start Handling fee--%>
                    <div class="collapsableMenu ">
                        <a aria-expanded="true" data-target="#handlingfee" data-toggle="collapse" class="btn btn-info" type="button">Add handling fees</a>
                        <div aria-expanded="true" class="collapse in coloredChkBox" id="handlingfee">
                            <div class="row collapsableContent">
                                <div class="row paddingTop noBottomBorder">
                                    <asp:Label class="control-label col-sm-2 spanStyle" ID="lblEnablehandlingfee" runat="server" Text="Enable handling fee"></asp:Label>
                                    <div class="col-md-3 col-sm-5 paddingLeft">
                                        <asp:CheckBox ID="chkFeeIsActive" runat="server" Checked="false" />
                                    </div>
                                </div>

                                <asp:Repeater ID="rptCurrencies" runat="server">
                                    <ItemTemplate>
                                        <div class="row paddingTop noBottomBorder">
                                            <asp:HiddenField ID="hdnCurrencyId" runat="server" Value='<%#Eval("CurrencyId") %>' Visible="false" />
                                            <asp:Label class="control-label col-sm-2 spanStyle" ID="lblCurrencyName" runat="server" Text='<%#Eval("CurrencyName") + "(" + Eval("CurrencySymbol") + ")"%>  '></asp:Label>
                                            <div class="col-md-3 col-sm-5 paddingLeft">
                                                <asp:TextBox ID="txthandlingFees" class="form-control" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <div class="row paddingTop">
                                    <div class="col-xs-9 col-xs-offset-2 spanPaddingLeft">
                                        <asp:RadioButton ID="rdbtnOncePerOrder" runat="server" GroupName="Order" />
                                        <asp:Label ID="lblOncePerOrder" Text="Once per order" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-xs-9 col-xs-offset-2 spanPaddingLeft">
                                        <asp:RadioButton ID="rdbtnOncePerItem" runat="server" GroupName="Order" />
                                        <asp:Label ID="lblOncePerItem" Text="Once per item" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-xs-9 col-xs-offset-2 spanPaddingLeft">
                                        <asp:RadioButton ID="rdbtnOncePerUnit" runat="server" GroupName="Order" />
                                        <asp:Label ID="lblOncePerUnit" Text="Once per unit" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--End Handling fee--%>

                    <div class="collapsableMenu ">
                        <a aria-expanded="true" data-target="#assignProducts" data-toggle="collapse" class="btn btn-info" type="button">Assign Products</a>
                        <div aria-expanded="true" class="collapse in coloredChkBox" id="assignProducts">
                            <div class="collapsableContent">
                                <asp:UpdatePanel runat="server" ID="updatePanelAssign" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="row collapsableContent assign_div">
                                            <div class="form-group clearfix">
                                                <div class="form-group clearfix">
                                                    <label class="control-label col-sm-2" for="name">Assign Products:<span style="color: red">*</span> </label>
                                                </div>
                                                <div class="webAddeditsection">
                                                    <asp:Panel ID="pnlProduct" runat="server">

                                                        <%-- <div class="form-group clearfix" id="trPCategory" runat="server">
                                                            <label class="control-label col-sm-2" for="category">Select Category:</label>
                                                            <div class="col-md-3 col-sm-5">
                                                                <div class="select-style selectpicker">
                                                                    <asp:DropDownList ID="ddlLvl1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLvl1_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group clearfix" id="trPSCategory" runat="server">
                                                            <label class="control-label col-sm-2" for="name">Select Sub Category:</label>
                                                            <div class="col-md-3 col-sm-5">
                                                                <div class="select-style selectpicker ">
                                                                    <asp:DropDownList ID="ddlLvl2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLvl2_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>--%>
                                                        <div class="form-group clearfix" id="trProduct" runat="server">
                                                            <label class="control-label col-sm-2" for="name"></label>
                                                            <div class="form-group clearfix">
                                                                <div class="col-md-4 col-sm-4">
                                                                    Unassigned Products
                                                                <div style="overflow: auto;">
                                                                    <asp:ListBox ID="lstUnAssigned" Height="250px" Width="100%" runat="server"
                                                                        SelectionMode="Multiple"></asp:ListBox>
                                                                </div>
                                                                </div>
                                                                <div class="col-md-1 col-sm-1">
                                                                    <div style="margin-top: 75px">
                                                                        <img runat="server" id="imgLeftToRight" src="~/Admin/Images/UI/front-arrow.gif" style="cursor: hand"
                                                                            alt="Click to move selected item from left box to right box" />
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                        <img runat="server" id="imgRightToLeft" src="~/Admin/Images/UI/back-arrow.gif" style="cursor: hand"
                                                                            alt="Click to move selected item from right box to left box" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 col-sm-4">
                                                                    Assigned Products
                                                                <div style="overflow: auto">
                                                                    <asp:ListBox ID="lstAssigned" runat="server" Height="240px" Width="100%"
                                                                        SelectionMode="Multiple"></asp:ListBox>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="collapsableMenu">
                        <a aria-expanded="true" data-target="#SeoMetadata" data-toggle="collapse" class="btn btn-info" type="button">SEO</a>
                        <div aria-expanded="true" class="collapse in coloredChkBox" id="SeoMetadata">
                            <div class="row collapsableContent">
                                <div class="form-group clearfix seoMetaDataPanel">
                                    <label class="control-label col-sm-2" for="name">Page Title:</label>
                                    <div class="col-md-5 col-sm-5">
                                        <asp:TextBox CssClass="form-control" ID="txtPageTitle" runat="server" onblur="return CheckTextbox(this.id)" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group clearfix seoMetaDataPanel">
                                    <label class="control-label col-sm-2" for="name">Meta Keywords:</label>
                                    <div class="col-md-5 col-sm-5">
                                        <asp:TextBox CssClass="form-control" ID="txtMetaKey" runat="server" onblur="return CheckTextbox(this.id)" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group clearfix seoMetaDataPanel">
                                    <label class="control-label col-sm-2" for="name">Meta Description:</label>
                                    <div class="col-md-5 col-sm-5">
                                        <asp:TextBox ID="txtMetaDesc" runat="server" CssClass="form-control" Rows="3" onblur="return CheckTextbox(this.id)" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="buttonPanel container">
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSaveExit" runat="server" class="btn" Text="Save & Exit" OnClick="btnSubmit_Click" OnClientClick="return ValidateFunc();" />
                            </li>

                            <li>
                                <asp:Button ID="btnSaveAddAnother" runat="server" class="btn" Text="Save & Add Another" OnClick="btnSubmit_Click" OnClientClick="return ValidateFunc();" />
                            </li>

                            <li>
                                <asp:Button ID="btnCancel" runat="server" class="btn gray" Text="Cancel" OnClick="btnCancel_Click" />
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HIDSectionName" runat="server" />
    <asp:HiddenField ID="HIDAssignedIds" runat="server" />
    <asp:HiddenField ID="HIDUnAssignedIds" runat="server" />
    <asp:HiddenField ID="HIDAssignedNameIds" runat="server" />
    <asp:HiddenField ID="HIDUnAssignedNameIds" runat="server" />
    <asp:HiddenField ID="HIDPSelectedId" runat="server" />
    <input type="hidden" id="hidSectionSrc" class="sectionsrc" runat="server" />
    <input type="hidden" id="hidSectionBannerSrc" class="bannersrc" runat="server" />
    <input type="hidden" id="HIDIconId" runat="server" />
    <input type="hidden" id="HIDBannId" runat="server" />
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover({
                placement : 'right'
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('body').on('click', '.paneltab', function () { 
                $(this).toggleClass("paneltabplus");
                $(this).parent().parent().parent().parent().parent().find('ul').slideToggle("slow");
            });
        });
    </script>
    <script type="text/javascript">
        var host ='<%=Host %>';
        CategoryId= '<%=TempSectionId %>';
        $(document).ready(function () {
            $('#FUIcon').fineUploader({
                request: {
                    endpoint: host + 'Admin/fineUploader/UploadCategory.aspx?type=Section&categoryid='+ '<%=TempSectionId %>' +'&action=add'
                },
                multiple: false,
                validation: {
                    sizeLimit: <%= ImageSize %>,
                    allowedExtensions: ["JPG", "jpeg","png"]
                },
                text: {
                    uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                },
                retry: {
                    enableAuto: false
                },
                chunking: {
                    enabled: false,
                    partSize: <%= ImageSize %>
                    },
                showMessage: function (message) {
                    $('#FUIcon .fineError').hide();
                    $('#FUIcon .FailureDiv').hide();
                    $('#FUIcon').append('<div class="alert alert-error fineError">' + message + '</div>');
                },
                debug: true
            });
            $('#FUBann').fineUploader({
                request: {
                    endpoint: host + 'Admin/fineUploader/UploadCategory.aspx?type=SectionBanner&categoryid='+ '<%= TempBannerId %>' +'&action=add'
                },
                multiple: false,
                validation: {
                    sizeLimit: <%= BannerSize %>,
                    allowedExtensions: ["JPG", "jpeg","png"]
                },
                text: {
                    uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                },
                retry: {
                    enableAuto: false
                },
                chunking: {
                    enabled: false,
                    partSize: <%= BannerSize %>
                    },
                showMessage: function (message) {
                    $('#FUBann .fineError').hide();
                    $('#FUBann .FailureDiv').hide();
                    $('#FUBann').append('<div class="alert alert-error fineError">' + message + '</div>');
                },
                debug: true
            });
        });
        
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if ('<%= IconHtml %>' != '')
            {
                $('#FUIcon .qq-upload-button').hide();
                $('#FUIcon .qq-upload-list').html('<%= IconHtml  %>');
                ActionType = 'Section'                      
            } 
            if ('<%= BannerHtml %>' != '') {
                $('#FUBann .qq-upload-button').hide();
                $('#FUBann .qq-upload-list').html('<%= BannerHtml  %>');
                ActionType = 'SectionBanner'
                      
            }
        });
    </script>
    <script type="text/javascript">

        function CheckTextbox(clientId) {
            
            var obj = document.getElementById(clientId);
            var iChars = "!@#$%^\"&+=[]{}|:<>?"
            for (var i = 0; i < obj.value.length; i++) {
                if (iChars.indexOf(obj.value.charAt(i)) != -1) {
                    alert("Text has special characters. \nThese are not allowed.\n Please remove them and try again.");
                    obj.value = "";
                    return false;
                }
            }
            return true;
        }


        function ValidateFunc() {
            var Name = document.getElementById('<%= txtName.ClientID %>');
            var IconAlt = document.getElementById('<%= txtIconAlt.ClientID %>');
            var AssignedIds = document.getElementById('<%= HIDAssignedIds.ClientID %>');
            if (Name.value.trim() == "") {
                alert('Enter Section Name !!')
                Name.focus();
                return false;
            }
            if (IconAlt.value.trim() == "") {
                alert('Enter Alternate Text !!')
                Name.focus();
                return false;
            }
            if(AssignedIds.value.trim() == "")
            {
                alert('Please Assign Product to This Section. !!');
                return false;
            }
            return true;
        }
        function fn_MoveDataLeftToRight() {
            MoveOption('<%= lstUnAssigned.ClientID %>', '<%= lstAssigned.ClientID %>');
            StoreIds('<%= lstUnAssigned.ClientID %>', '<%= lstAssigned.ClientID %>', '<%= HIDUnAssignedIds.ClientID %>', '<%= HIDAssignedIds.ClientID %>');
            StoreNameNIdPipe('<%= lstUnAssigned.ClientID %>', '<%= lstAssigned.ClientID %>', '<%= HIDUnAssignedNameIds.ClientID %>', '<%= HIDAssignedNameIds.ClientID %>');
        }

        function fn_MoveDataRightToLeft() {
            MoveOption('<%= lstAssigned.ClientID %>', '<%= lstUnAssigned.ClientID %>');
            StoreIds('<%= lstUnAssigned.ClientID %>', '<%= lstAssigned.ClientID %>', '<%= HIDUnAssignedIds.ClientID %>', '<%= HIDAssignedIds.ClientID %>');
            StoreNameNIdPipe('<%= lstUnAssigned.ClientID %>', '<%= lstAssigned.ClientID %>', '<%= HIDUnAssignedNameIds.ClientID %>', '<%= HIDAssignedNameIds.ClientID %>');
        }

        function MoveOption(fromControlId, toControlId) {
            var objSourceElement;
            var objTargetElement;

            objSourceElement = document.getElementById(fromControlId);
            objTargetElement = document.getElementById(toControlId);

            var aryTempSourceOptions = new Array();
            var x = 0;
            //looping through source element to find selected options        
            for (var i = 0; i < objSourceElement.length; i++) {//start of for
                if (objSourceElement.options[i].selected) {//start of if within for
                    //need to move this option to target element
                    var intTargetLen = objTargetElement.length++;
                    objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;
                    objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;
                } //end of if within for          
                else//else of if within for
                {//start of else of if within for
                    //storing options that stay to recreate select element 
                    var objTempValues = new Object();
                    objTempValues.text = objSourceElement.options[i].text;
                    objTempValues.value = objSourceElement.options[i].value;
                    aryTempSourceOptions[x] = objTempValues;
                    x++;
                } //end of else of if within for
            } //end of for
            //resetting length of source  
            objSourceElement.length = aryTempSourceOptions.length;
            //looping through temp array to recreate source select element
            for (var i = 0; i < aryTempSourceOptions.length; i++) {//start of for
                objSourceElement.options[i].text = aryTempSourceOptions[i].text;
                objSourceElement.options[i].value = aryTempSourceOptions[i].value;
                objSourceElement.options[i].selected = false;
            } //end of for

        } //end of function

        function StoreIds(leftListBoxId, rightListBoxId, hiddenFieldIdLeft, hiddenFieldIdRight) {
            var leftListId;
            leftListId = document.getElementById(leftListBoxId);

            var rightListId;
            rightListId = document.getElementById(rightListBoxId);

            var hiddenFieldLeft;
            hiddenFieldLeft = document.getElementById(hiddenFieldIdLeft);

            var hiddenFieldRight;
            hiddenFieldRight = document.getElementById(hiddenFieldIdRight);

          

            hiddenFieldLeft.value = "";
            hiddenFieldRight.value = "";
          
            //For left side control
            for (var i = 0; i < leftListId.length; i++) {
                if (hiddenFieldLeft.value == "") {
                    hiddenFieldLeft.value = leftListId.options[i].value;
                }
                else {
                    hiddenFieldLeft.value += "," + leftListId.options[i].value;
                }
            }

            //For right side control
            for (var i = 0; i < rightListId.length; i++) {
                if (hiddenFieldRight.value == "") {
                    hiddenFieldRight.value = rightListId.options[i].value;
                }
                else {
                    hiddenFieldRight.value += "," + rightListId.options[i].value;
                }
            }

        }
        function StoreNameNIdPipe(leftListBoxId, rightListBoxId, hiddenFieldIdLeft, hiddenFieldIdRight) {
            var leftListId;
            leftListId = document.getElementById(leftListBoxId);

            var rightListId;
            rightListId = document.getElementById(rightListBoxId);

            var hiddenFieldLeft;
            hiddenFieldLeft = document.getElementById(hiddenFieldIdLeft);

            var hiddenFieldRight;
            hiddenFieldRight = document.getElementById(hiddenFieldIdRight);

            hiddenFieldLeft.value = "";
            hiddenFieldRight.value = "";

            //For left side control
            for (var i = 0; i < leftListId.length; i++) {
                if (hiddenFieldLeft.value == "") {
                    hiddenFieldLeft.value = leftListId.options[i].text + '|' + leftListId.options[i].value;
                }
                else {
                    hiddenFieldLeft.value += "," + leftListId.options[i].text + '|' + leftListId.options[i].value;
                }
            }

            //For right side control
            for (var i = 0; i < rightListId.length; i++) {
                if (hiddenFieldRight.value == "") {
                    hiddenFieldRight.value = rightListId.options[i].text + '|' + rightListId.options[i].value;
                }
                else {
                    hiddenFieldRight.value += "," + rightListId.options[i].text + '|' + rightListId.options[i].value;
                   
                }
            }
        }
       
    </script>
</asp:Content>

