﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Section_SectionListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;


            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Section </a></li>
            <li>Section Listing</li>
        </ul>
    </div>

   <div class="admin_page">
     <section class="container mainContainer padbtom15">
      <div class="wrap_container ">
           <div class="content">
           <h3 class="mainHead">Section Listing</h3>
        <span class="sectionText">Product can be grouped by assigning them to sections.</span>
        <!-- Website tab start -->

        <div class="butonblock nospace clearfix ">
            <ul class="categories_btn" style="margin: 0;">
                <li>
                    <asp:Button runat="server" ID="btnAddNew" CssClass="btn3" OnClick="btn_Click" Text="Add New Section" />
                </li>
            </ul>
            <div class="clear">
            </div>
        </div>
        <!--mainOutContainer start-->
        <div class="midleDrgblecontin dragblnextBlock">
          
                    <asp:GridView ID="gvSection" runat="server" AutoGenerateColumns="false" Width="100%"
                        class="all_customer_inner dragbltop  " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                        OnPageIndexChanging="gvSection_PageIndexChanging" EmptyDataText="No record found."
                        OnRowDataBound="gvSection_OnRowDataBound" AllowSorting="false" PageSize="10">
                        <Columns>
                            <asp:BoundField HeaderText="Section Name" HeaderStyle-CssClass="bg" DataField="SectionName"
                                ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                <ItemStyle />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="URL" HeaderStyle-CssClass="bg" HeaderStyle-Width="74px">
                                <ItemTemplate>
                                    <asp:Label ID="lblSecURL" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                SortExpression="Status" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="action">
                                        <a class="icon" href="#"></a>
                                        <div class="action_hover">
                                            <span></span>
                                            <div  id="dvAction" runat="server" class="view_order other_option">
                                                <asp:LinkButton Text="Edit" runat="server" OnCommand="lbtnEditDelete_Command" ID="lnkbtnEdit"
                                                    CommandName="EditSection" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SectionId") %>' />
                                                
                                                <asp:LinkButton Text="Delete" runat="server" OnCommand="lbtnEditDelete_Command" ID="lnkbtnDelete"
                                                    CommandName="DeleteSection" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SectionId") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" Position="Top" FirstPageText="First" PageButtonCount="6"
                            LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                        <PagerStyle CssClass="paging" />
                    </asp:GridView>
            
        </div>
               </div>
          </div>
    </section>
       </div>
</asp:Content>

