﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" EnableEventValidation="true" EnableViewState="true" Inherits="Presentation.Admin_FreightManagement_FreightManagement" %>

<script runat="server">

   
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .nav-tabs > li {
            margin-bottom: -2px;
        }

        .tab-content.FreightTabBg {
            border: 1px solid #ccc;
            padding: 0 20px;
        }
    </style>
    <script>
        function ShowSuccess(aMsg) {
            $('#mySuccessModal').find('#SuccessMessage').html(aMsg);
            $('#mySuccessModal').modal('show');
        }
    </script>
    <script>
        function ShowFailure(aMsg) {
            $('#myErrorModal').find('#ErrorMessage').html(aMsg);
            $('#myErrorModal').modal('show');
        }
    </script>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0)">Settings</a></li>
            <li>Freight Management</li>
        </ul>

    </div>
    <%--<asp:ScriptManager ID="scptMgr" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>--%>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12 relative_data">
                            <h3>Freight Management
                            </h3>

                        </div>
                        <div class="fright_div">
                            <div class="form-group clearfix">
                                <label for="CarriageMethod" class="control-label col-xs-2">Region</label>
                                <div class="col-xs-8">
                                    <asp:RadioButtonList runat="server" AutoPostBack="true" RepeatDirection="Horizontal" CellPadding="5" CellSpacing="5" ID="rblFreightRegions" RepeatLayout="Flow" OnSelectedIndexChanged="rbl_SelectedIndexChanged"></asp:RadioButtonList>
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <label for="CarriageMethod" class="control-label col-xs-2">Carriage Mode</label>
                                <div class="col-xs-8">
                                    <asp:RadioButtonList runat="server" AutoPostBack="true" RepeatDirection="Horizontal" ID="rblFreightModes" OnSelectedIndexChanged="rbl_SelectedIndexChanged"></asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label for="CarriageMethod" class="control-label col-xs-2">Carriage Method</label>
                                <div class="col-xs-8">
                                    <asp:RadioButtonList runat="server" AutoPostBack="true" RepeatDirection="Horizontal" ID="rblFreightMethods" OnSelectedIndexChanged="rbl_SelectedIndexChanged"></asp:RadioButtonList>
                                </div>
                            </div>
                            <%-- <div id="divFreightConfiguration" runat="server"></div> Commented by Sripal--%>


                            <div class="form-group clearfix">
                                <label for="CarrierServiceText" class="control-label col-xs-2">Carrier Service Text</label>
                                <div class="col-xs-8">
                                    <asp:DropDownList ID="ddlCarrierService" runat="server" CssClass="form-control" Width="50%" onchange="myFunction()" ClientIDMode="Static"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvddlCarrierService" runat="server" ControlToValidate="ddlCarrierService" InitialValue="0" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please select Carrier Service Text." SetFocusOnError="true" ValidationGroup="valFreightConfig"></asp:RequiredFieldValidator>

                                    <%--<asp:TextBox ID="txtCarrierServiceText" runat="server" Width="50%" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCarrierServiceText" runat="server" CssClass="text-danger" ControlToValidate="txtCarrierServiceText" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgxCarrierServiceText" runat="server" CssClass="text-danger" ControlToValidate="txtCarrierServiceText" Text="Alpaha Numeric Text Only. Allowed charactes [@#$%&*+\-_(),+':;?.,![]\s\\/" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label for="CarrierServiceid" class="control-label col-xs-2">Carrier Service Id</label>
                                <div class="col-xs-8">
                                    <asp:Label ID="lblCarrierServiceId" runat="server" CssClass="form-control" ClientIDMode="Static" Width="50%"></asp:Label>
                                    <%-- <asp:TextBox ID="txtCarrierServiceId" runat="server" Width="50%" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCarrierServiceId" runat="server" CssClass="text-danger" ControlToValidate="txtCarrierServiceId" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgxCarrierServiceId" runat="server" CssClass="text-danger" ControlToValidate="txtCarrierServiceId" Text="( Max. Three digit number only )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label for="TransitTime" class="control-label col-xs-2">Transit Time</label>
                                <div class="col-xs-8">
                                    <asp:TextBox ID="txtTransitTime" runat="server" Width="50%" MaxLength="30" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvTransitTime" runat="server" CssClass="text-danger" ControlToValidate="txtTransitTime" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgxTransitTime" runat="server" CssClass="text-danger" ControlToValidate="txtTransitTime" Text="( Only Alpha Numeric and Special Character (-) Hypen. )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <asp:Panel ID="pnlNonFreightMatrixControls" runat="server">
                                <div class="form-group clearfix">
                                    <asp:Label ID="lblMOV" runat="server" CssClass="control-label col-xs-2" Text="Minimum Order Value"></asp:Label>
                                    <div class="col-xs-3">
                                        <asp:Repeater ID="rptMinimumOrderValue" runat="server" OnItemDataBound="rptMinimumOrderValue_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                <asp:Label ID="lblCurrency" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <br />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                                <%--<div class="form-group clearfix">
                                    <asp:Label ID="lblMOV" runat="server" CssClass="control-label col-xs-2" Text="Minimum Order Value"></asp:Label>
                                    <div class="col-xs-3">
                                        <asp:TextBox ID="txtMOV" runat="server" Width="50%" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtMOV" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgxMOV" runat="server" ControlToValidate="txtMOV" CssClass="text-danger" Text="( Decimal numbers greater than ZERO only )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>--%>
                                <div class="form-group clearfix">
                                    <asp:Label ID="lblVAA" runat="server" CssClass="control-label col-xs-2" Text="Value Applied Above"></asp:Label>
                                    <div class="col-xs-3">
                                        <asp:Repeater ID="rptValueAppliedAbove" runat="server" OnItemDataBound="rptValueAppliedAbove_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                <asp:Label ID="lblCurrency" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <br />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <%-- <asp:TextBox ID="txtVAA" runat="server" Width="50%" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvVAA" runat="server" ControlToValidate="txtVAA" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgxVAA" runat="server" ControlToValidate="txtVAA" CssClass="text-danger" Text="( Decimal numbers greater than ZERO only )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                                    </div>
                                    <div class="col-xs-3">
                                        <asp:RadioButtonList ID="rblPerBoxPerOrder" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Text="Per Box" Value="PerBox"></asp:ListItem>
                                            <asp:ListItem Text="Per Order" Value="PerOrder"></asp:ListItem>
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvPerBoxPerOrder" ControlToValidate="rblPerBoxPerOrder" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <asp:Label ID="lblVAB" runat="server" CssClass="control-label col-xs-2" Text="Value Applied Below"></asp:Label>
                                    <div class="col-xs-3">
                                        <asp:Repeater ID="rptValueAppliedBelow" runat="server" OnItemDataBound="rptValueAppliedBelow_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                <asp:Label ID="lblCurrency" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <br />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <asp:CheckBox runat="server" ID="chkDisallowOrder" Text="Disallow Orders below Minimum Value" />
                                </div>
                                <%--<div class="form-group clearfix">
                                    <asp:Label ID="lblVAB" runat="server" CssClass="control-label col-xs-2" Text="Value Applied Below"></asp:Label>
                                    <div class="col-xs-3">
                                        <asp:TextBox ID="txtVAB" runat="server" Width="50%" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvVAB" runat="server" ControlToValidate="txtVAB" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgxVAB" runat="server" ControlToValidate="txtVAB" CssClass="text-danger" Text="( Decimal numbers greater than ZERO only )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-xs-4">
                                        <asp:CheckBox runat="server" ID="chkDisallowOrder" Text="Disallow Orders below Minimum Value" />
                                    </div>
                                </div>--%>
                            </asp:Panel>
                            <div class="form-group">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <asp:Button ID="btnSaveFreightConfig" runat="server" Text="Save" class="save" ValidationGroup="valFreightConfig" OnClick="btnSaveFreightConfig_Click" />
                                    <asp:Button ID="btnDeleteFreightConfig" runat="server" Text="Delete" class="save" OnClientClick="return confirm('Are you sure want to delete?');" OnClick="btnDeleteFreightConfig_OnClick" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>

    <script type="text/javascript">
        function myFunction() {
            $("#lblCarrierServiceId").text($("#ddlCarrierService").val());
        }
    </script>
</asp:Content>
