﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_StaticTextManagement_StaticTextListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li>Manage Static Text</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <h3 class="mainHead">Static Text Management</h3>
                    <span class="sectionText">Manage Static Text</span>
                    <!-- Website tab start -->

                    <div class="butonblock nospace clearfix ">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <asp:Button runat="server" ID="btnAddNew" CssClass="btn3" OnClick="btnAddNew_Click" Text="Add New Static Text" />
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                    <!--mainOutContainer start-->
                    <div class="midleDrgblecontin">
                        <ul class="dragblnextBlock">
                            <li>
                                <asp:GridView ID="gvStaticText" runat="server" AutoGenerateColumns="false" Width="100%"
                                    class="all_customer_inner dragbltop  " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                    OnPageIndexChanging="gvStaticText_PageIndexChanging" EmptyDataText="No record found."
                                    OnRowDataBound="gvStaticText_RowDataBound" AllowSorting="false" PageSize="10">
                                    <Columns>
                                        <asp:BoundField HeaderText="Static Text Name" HeaderStyle-CssClass="bg" DataField="StaticTextKey"
                                            ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                            <ItemStyle />
                                        </asp:BoundField>

                                        <asp:BoundField HeaderText="Static Text Value" HeaderStyle-CssClass="bg" DataField="StaticTextValue"
                                            ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                            <ItemStyle />
                                        </asp:BoundField>

                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                            SortExpression="Status" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div id="dvAction" runat="server" class="view_order other_option">
                                                            <asp:LinkButton Text="Edit" runat="server" OnCommand="lbtnEditDelete_Command" ID="lnkbtnEdit"
                                                                CommandName="EditKey" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StaticTextId") %>' />

                                                            <asp:LinkButton Text="Delete" runat="server" OnCommand="lbtnEditDelete_Command" ID="lnkbtnDelete"
                                                                CommandName="DeleteKey" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StaticTextId") %>' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" Position="Top" FirstPageText="First" PageButtonCount="6"
                                        LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

