﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_StaticTextManagement_ManageStaticText" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>BrandAddition</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li>Manage Static Text</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <div class="content">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>
                                <asp:Label ID="lblHeading" runat="server"></asp:Label>
                            </h3>
                            <p>Static text in your website can be managed here.  Fill out the form below to create a new static text key.</p>
                        </div>
                        <div class="col-md-3  text-right">
                        </div>
                        <div class="right_selct_dropdwn">
                            <div style="float: right;padding-top:40px;" id="dvLang" runat="server" visible="false">
                                <label class="sm_select" style="display:none;">Select Language :</label>
                                <div class="select-style selectpicker">
                                    <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" style="display:none;" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="collapsableMenu ">
                    <a aria-expanded="true" data-target="#webPage" data-toggle="collapse" class="btn btn-info" type="button">Static Text Management</a>
                    <div aria-expanded="true" class="collapse in coloredChkBox" id="webPage">
                        <div class="row collapsableContent">
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="name">Static Text Key <span style="color: red">*</span>: </label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtKey" runat="server" onblur="return CheckTextbox(this.id)" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="category">Static Text Value <span style="color: red">*</span>:</label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtValue" runat="server"  autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="buttonPanel container">
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="btnSaveExit" runat="server" class="btn" Text="Save & Exit" OnClick="btnSubmit_Click" OnClientClick="return ValidateFunc();" />
                        </li>

                        <li>
                            <asp:Button ID="btnSaveAddAnother" runat="server" class="btn" Text="Save & Add Another" OnClick="btnSubmit_Click" OnClientClick="return ValidateFunc();" />
                        </li>

                        <li>
                            <asp:Button ID="btnCancel" runat="server" class="btn gray" Text="Cancel" OnClick="btnCancel_Click" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        function CheckTextbox(clientId) {

            var obj = document.getElementById(clientId);
            var iChars = "!#$%^\"&+=[]{}|:<>?"
            for (var i = 0; i < obj.value.length; i++) {
                if (iChars.indexOf(obj.value.charAt(i)) != -1) {
                    alert("Text has special characters. \nThese are not allowed.\n Please remove them and try again.");
                    obj.value = "";
                    return false;
                }
            }
            return true;
        }


        function ValidateFunc() {
            var key = document.getElementById('<%= txtKey.ClientID %>');
            var val = document.getElementById('<%= txtValue.ClientID %>');
            if (key.value.trim() == "") {
                alert('Enter Static Text Key !!')
                key.focus();
                return false;
            }
            if (val.value.trim() == "") {
                alert('Enter Static Text Value !!')
                val.focus();
                return false;
            }
            return true;
        }

    </script>
</asp:Content>

