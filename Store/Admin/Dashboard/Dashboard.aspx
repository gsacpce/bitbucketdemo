﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_Dashboard_Dashboard" %>


<asp:Content ID="content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        var host = '<%=hostAdmin %>';
    </script>
    <script src="<%=hostAdmin %>JS/jquery.js"></script>
    <script src="<%=hostAdmin %>JS/script-centered.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dashboard_option').append('<div class="clear"></div>');
        }
        );
    </script>
    <style type="text/css">
        .dashboard_option {
            min-height: 960px;
            border-top: 2px solid #315469;
            margin: 0 auto;
            max-width: 1680px;
            padding-top: 28px;
            position: relative;
            width: 96.87%;
        }

        #dvDashboard .block {
            background: #f0f0f0 none repeat scroll 0 0;
            border: 1px solid #ddd;
            box-sizing: border-box;
            padding: 20px;
            position: absolute;
            transition: all 0.2s ease 0s;
            width: 272px;
            margin-left: 11px;
        }

        .titledivcase a {
            font-family: Arial;
            color: #00528c;
            text-decoration: none;
            text-transform: uppercase;
        }

        #dvDashboard div ul li a {
            color: #000;
            display: block;
            font-size: 14px;
            padding: 5px 0;
            font-family: Arial;
            text-decoration: none;
        }

        #dvDashboard div ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .countsother {
            background: #00528c none repeat scroll 0 0;
            border-radius: 5px 0 0;
            bottom: 0;
            color: #fff;
            font-size: 13px;
            font-weight: normal;
            padding: 5px 15px;
            position: absolute;
            right: 0;
            text-align: right;
        }
    </style>
    <%--</head>
<body onload="setupBlocks();">
    <form id="form1" runat="server">--%>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container dashboard_option" id="dvDashboard" clientidmode="static" runat="server">
            </div>

        </section>

    </div>
</asp:Content>
