﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Points_ImportPointsData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        String strConnection = ConfigurationManager.AppSettings["StoreConnection"];
        string xlsFile = fileuploadExcel.FileName;  //fileImport is the file upload control
        string ext = System.IO.Path.GetExtension(xlsFile);
        if (!(ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx"))
        {
            GlobalFunctions.ShowModalAlertMessages(this.Page, "Invalid Extenstion", AlertType.Failure);
            return;
        }
        string targetFileName = MapPath("~/Admin/Points/") + fileuploadExcel.FileName;
        string originalFileName = xlsFile.Substring(0, xlsFile.LastIndexOf("."));
        string fileName = originalFileName.Replace(" ", "_") + "_" + DateTime.Now.ToString("MMddyyyyHHmmss");
        fileuploadExcel.PostedFile.SaveAs(targetFileName);


        OleDbConnection con = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + targetFileName + @";Extended Properties=""Excel 8.0;IMEX=1;ImportMixedTypes=Text""");
        OleDbCommand myCommand = new OleDbCommand("Select * from [Sheet1$]", con);

        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(strConnection))
        try
        {
            {

                bulkCopy.DestinationTableName = "Tbl_ImportUsers";
                con.Open();
                bulkCopy.WriteToServer(myCommand.ExecuteReader());
                bool Status = UserBL.UpdateImportDataToOriginalTable();
                if (Status)
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Data Imported Succesfully", AlertType.Success);
                }
                else
                {
                    GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured", AlertType.Failure);
                }


                


            }
        }
        catch (Exception ex)
        {
            GlobalFunctions.ShowModalAlertMessages(this.Page, "Error occured", AlertType.Failure);
            Exceptions.WriteExceptionLog(ex);
            
        }

    }

}