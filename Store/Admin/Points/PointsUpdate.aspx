﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Points_PointsUpdate" %>


<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css"
        href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
    <script src="../JS/jquery-ui.js"></script>
    <script src="../JS/Utilities.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $(".paneltab").click(function () {
                $(this).toggleClass("paneltabplus");
                $(this).parent().parent().parent().parent().parent().find

                ('ul').slideToggle("slow");
            });

            $('.chkBox input').click(function () {
                jQuery(this).parent().toggleClass('chkBox1mark');
                return false;
            });


            $('.CsA h3').click(function () {
                $(this).next().stop().slideToggle('fast');
                $(this).toggleClass('rightA');
            });
        });
    </script>
    <style type="text/css">
        .admin_page .container .content .CsA h3 {
            background: #f2f2f2 url("../images/downarrow.png") no-repeat scroll 14px 13px;
            border: medium none;
            font-family: Arial;
            font-size: 18px;
            font-weight: normal;
            margin: 2px 0;
            padding-bottom: 6px;
            padding-left: 2em;
            padding-top: 6px;
        }

        .admin_page .container .content h3 {
            color: #666666;
            font-family: "Arial Regular","Arial";
            font-size: 24px;
            font-weight: normal;
            line-height: 24px;
            margin: 0;
            padding: 17px 0 13px;
            text-transform: capitalize;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Points Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <h3 class="mainHead">Points Update</h3>
                    <div class="CsA">
                        <h3 id="heading1">Update Point for registered users of the stores.</h3>
                        <div id="divheading1" class="divHide">
                            <span class="sectionText">
                                <span class="butonblock clearfix">



                                    <ul class="categories_btn" style="margin: 0;">
                                        <li style="float: left;">
                                            <asp:Panel ID="pnlsearch" runat="server" DefaultButton="btnSearch" Style="margin-right: 381px; margin-top: 11px;">
                                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CommandName="search" OnCommand="btn_Command" CssClass="save" />
                                                <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CssClass="save" />
                                            </asp:Panel>
                                        </li>
                                        <li>                           
                                            <asp:Button runat="server" ID="btnImportNew" CssClass="btn2" OnCommand="btn_Command" Text="Import Points" CommandName="showimportNew" />
                                        </li>
                                        <li>
                                            <asp:Button runat="server" ID="btnExportToExcelNew" CssClass="btn2" Text="Export Points" CommandName="exportNew" OnCommand="btn_Command" />
                                        </li>
                                        <li>
                                            <asp:Button runat="server" ID="btnImport" Visible="false" CssClass="btn2" OnCommand="btn_Command" Text="Import Points" CommandName="showimport" />
                                        </li>
                                        <li>
                                            <asp:Button runat="server" ID="btnExportToExcel" Visible="false" CssClass="btn2" Text="Export Points" CommandName="export" OnCommand="btn_Command" />
                                        </li>
                                    </ul>

                                    <ul style="margin: 0; margin-right: 0.5cm;">
                                        <li style="float: right;">
                                            <asp:LinkButton ID="LinkRegBackup" runat="server" OnClick="LinkRegBackup_Click">Download Registered User Backup File</asp:LinkButton></li>
                                    </ul>
                                    <div class="clear"></div>
                                </span>
                            </span>

                            <div id="divPointsUpdate" runat="server" visible="false">
                                <div class="PointsUpdate_table">
                                    <table width="100%" class="">

                                        <tr>
                                            <td colspan="2">
                                                <asp:GridView ID="gVPoints" runat="server" CssClass="dragbltop table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="30" OnPageIndexChanging="gVPoints_PageIndexChanging" EmptyDataText="No users exists.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Email">
                                                            <ItemTemplate>
                                                                <asp:Literal
                                                                    ID="ltlEmail" runat="server" Text='<%#DataBinder.Eval

(Container.DataItem,"EmailId") %>'></asp:Literal>


                                                                <asp:HiddenField ID="hdnUserId" runat="server" Value='<%#DataBinder.Eval

(Container.DataItem,"UserId") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField
                                                            HeaderText="First name">
                                                            <ItemTemplate>
                                                                <asp:Literal
                                                                    ID="ltlFName" runat="server" Text='<%#DataBinder.Eval

(Container.DataItem,"FirstName") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField
                                                            HeaderText="Last name">
                                                            <ItemTemplate>
                                                                <asp:Literal
                                                                    ID="ltlLName" runat="server" Text='<%#DataBinder.Eval

(Container.DataItem,"LastName") %>'></asp:Literal>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField
                                                            HeaderText="Points">
                                                            <ItemTemplate>


                                                                <asp:HiddenField ID="hdnPreviousPoints" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Points") %>' />
                                                                <asp:TextBox
                                                                    ID="txtPoints" Enabled="false" runat="server" Text='<%#DataBinder.Eval

(Container.DataItem,"Points") %>'></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No Record Found
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="buttonPanel">
                                    <ul class="button_section">
                                        <li>
                                            <asp:Button ID="btnUpdate"
                                                runat="server" Text="Update" OnCommand="btn_Command" CommandName="update"
                                                Visible="false" CssClass="btn" />
                                        </li>
                                    </ul>
                                </div>
                            </div>



                            <div class="PointsUpdate_table" id="divImport"
                                runat="server" visible="false">
                                <table width="50%" style="position: relative;">
                                    <tr>
                                        <td width="25%">
                                            <span style="color: Red">*</span>Attach Excel file
                                        </td>
                                        <td width="25%" style="position: absolute; top: 0;">
                                            <asp:FileUpload
                                                ID="fileuploadExcel" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 35px;">
                                            <asp:Button ID="Button1"
                                                runat="server" Text="Import" class="save" OnCommand="btn_Command"
                                                CommandName="importNew" />
                                            <asp:Button ID="btnSend" Visible="false"
                                                runat="server" Text="Import" class="save" OnCommand="btn_Command"
                                                CommandName="import" /></td>
                                        <td style="padding-top: 35px;">
                                            <asp:Button ID="Button2"
                                                runat="server" Text="Cancel" class="save" OnCommand="btn_Command"
                                                CommandName="cancel" /></td>
                                    </tr>
                                </table>
                                <asp:GridView ID="GridView1" runat="server">
                                </asp:GridView>
                            </div>

                        </div>

                        <h3 id="heading2">Non-registered Users</h3>
                        <div id="divUnRegistered" class="divHide table-responsive" runat="server" style="margin-top: 15px;">
                            <asp:GridView ID="gvUnregisteredUserpoints"
                                runat="server" CssClass="dragbltop table table-hover" AutoGenerateColumns="false"
                                AllowPaging="true" PageSize="30"
                                OnPageIndexChanging="gVPoints_PageIndexChanging" EmptyDataText="No users 

exists.">
                                <Columns>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltlUEmail"
                                                runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EmailId") 

%>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Points">
                                        <ItemTemplate>
                                            <asp:HiddenField
                                                ID="hdnUPreviousPoints" runat="server" Value='<%#DataBinder.Eval

(Container.DataItem,"Points") %>' />
                                            <asp:TextBox ID="txtUPoints"
                                                Enabled="false" runat="server" Text='<%#DataBinder.Eval

(Container.DataItem,"Points") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <span class="sectionText">
                                <h4>Point for non registered users of the stores.</h4>
                                <span class="butonblock clearfix">
                                    <ul class="categories_btn bottom_btn" style="float: left">
                                        <li>
                                            <asp:FileUpload ID="fileuploadUnReg" runat="server" Style="margin: 16px 0; max-height: 45px;" />
                                        </li>
                                        <li>
                                            <asp:Button runat="server" ID="btnUImport" CssClass="btn2" OnClick="btnUImport_Click" Text="Import Points" />
                                        </li>
                                        <li>
                                            <asp:Button runat="server" ID="btnUExport" CssClass="btn2" Text="Export Points" OnClick="btnUExport_Click" />
                                        </li>
                                    </ul>
                                    <ul style="margin: 0; margin-right: 0.5cm;">
                                        <li style="float: right;">
                                            <asp:LinkButton ID="LinkUnRegBackup" runat="server" OnClick="LinkUnRegBackup_Click">Download Un-Registered User Backup File</asp:LinkButton></li>
                                    </ul>
                                    <div class="clear"></div>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

