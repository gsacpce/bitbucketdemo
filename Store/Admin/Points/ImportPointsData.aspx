﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" CodeFile="ImportPointsData.aspx.cs" Inherits="Admin_Points_ImportPointsData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table>
            <tr>
                <td>
                    <span style="color: Red">*</span>Attach Excel file
                </td>
                <td>
                    <asp:FileUpload ID="fileuploadExcel" runat="server" />
                </td>
            </tr>
           
            <tr>
                <td>

                </td>
                <td>
                    <asp:Button ID="btnSend" runat="server" Text="Export" OnClick="btnSend_Click" />
                </td>
            </tr>
        </table>
        <asp:GridView ID="GridView1" runat="server">
        </asp:GridView>
    </div>
</asp:Content>

