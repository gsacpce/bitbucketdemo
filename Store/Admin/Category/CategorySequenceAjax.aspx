﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Admin_CategorySequenceAjax" %>

<style>
    .MainUlContainer ul li ul {
        padding: 0 20px;
    }

    .ui-sortable {
        cursor: pointer !important;
    }
</style>

<script type="text/javascript">
    $(function () {
        $("#websequence ul").sortable({
            /* placeholder: "ui-state-highlight"*/
        });


    });
</script>
<script>
    $(document).ready(function () {
        $(".ClickUlmain").toggle(function () {

            $(this).next('.ToggleBlock').slideUp();
            $(this).addClass('paneltabplus');

        }, function () {

            $(this).next('.ToggleBlock').slideDown();
            $(this).removeClass('paneltabplus');
        });

        $("#divWeb").sortable();


    });
</script>
<ul id="divWeb" class="WCategory MainUlContainer" runat="server" visible="true">
    <asp:repeater id="rptPCategory" runat="server" onitemdatabound="rptPCategory_OnItemDataBound">
            <ItemTemplate>
                <li id="liPC" runat="server">
                    <span id="ParentUl" runat="server">
                        <%# DataBinder.Eval(Container.DataItem, "CategoryName") %></span>
               
                    <table class="ToggleBlock dragblnextBlock">

                        <asp:Repeater ID="rptSCategory" runat="server" OnItemDataBound="rptSCategory_OnItemDataBound">
                            <ItemTemplate>
                                <li id="liSC" runat="server"><span class="textIcon paneltab">
                                    <%# DataBinder.Eval(Container.DataItem, "CategoryName") %></span>
                                    <ul>
                                        <asp:Repeater ID="rptSSCategory" runat="server" OnItemDataBound="rptSSCategory_OnItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltlSSC" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </li>


                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </li>
            </ItemTemplate>
        </asp:repeater>
</ul>




<%--<ul id="Ul1" class="WCategory MainUlContainer" runat="server" visible="true">
    <asp:repeater id="rptPCategory" runat="server" onitemdatabound="rptPCategory_OnItemDataBound">
            <ItemTemplate>
                <li id="liPC" runat="server"><span id="ParentUl" runat="server">
                    <%# DataBinder.Eval(Container.DataItem, "CategoryName") %></span>
                    <ul class="ToggleBlock dragblnextBlock">
                        <asp:Repeater ID="rptSCategory" runat="server" OnItemDataBound="rptSCategory_OnItemDataBound">
                            <ItemTemplate>
                                <li id="liSC" runat="server"><span class="textIcon paneltab">
                                    <%# DataBinder.Eval(Container.DataItem, "CategoryName") %></span>
                                    <ul>
                                        <asp:Repeater ID="rptSSCategory" runat="server" OnItemDataBound="rptSSCategory_OnItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltlSSC" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </li>


                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </li>
            </ItemTemplate>
        </asp:repeater>
</ul>--%>