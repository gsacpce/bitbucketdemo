﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Admin_Category_CategoryManagement" MasterPageFile="~/Admin/Master/AdminMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>
        $(document).ready(function () {
            $(".paneltab").click(function () {
                $(this).toggleClass("paneltabplus");
                $(this).parent().parent().parent().parent().parent().find('ul').slideToggle("slow");

            });


            $('.chkBox input').click(function () {
                jQuery(this).parent().toggleClass('chkBoxmark');
                return false;
            });

            //$('.action').mouseover(function () {
            //    $('.icon', this).css('z-index', '999');
            //    $('.icon', this).addClass("iconDark");
            //    $('.action_hover', this).show();
            //    return false;


            //});

            //$('.action').mouseout(function () {
            //    $('.icon', this).css('z-index', '8');
            //    $('.icon', this).removeClass("iconDark");
            //    $('.action_hover', this).hide();
            //    return false;

            //});


        });
    </script>
    <style>
        .firstDv input[type="checkbox"] {
            opacity: 1 !important;
        }
    </style>
    <div class="container">

        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Categories </a></li>
            <li>Category List</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <h3 class="mainHead">Product Categories</h3>
                    <span class="sectionText">Categories allow you to group products by similar attributes.
            The categories in your website are shown below.</span>
                    <table>
                        <tr style="height: 80px" runat="server">
                            <td align="left" width="20%">
                                <span class="textspace">Select User Type:</span>
                            </td>
                            <td align="left" style="height: 30px" width="40%">
                                <div class="select-style selectpicker ">
                                    <asp:DropDownList ID="ddlUserTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserTypes_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <div class="clearfix"></div>
                    <div class="butonblock clearfix">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <asp:Button runat="server" ID="btnSeq" CssClass="btn1" OnClick="btn_Click" Text="Category Sequence" />
                            </li>
                            <li>
                                <asp:Button runat="server" ID="btnExportToExcel" CssClass="btn2" OnClick="btnExportToExcel_Click"
                                    Text="Export Categories" />
                            </li>
                            <li>
                                <asp:Button runat="server" ID="btnAddNew" CssClass="btn3" OnClick="btn_Click" Text="Add New Category" />
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>

                    <div class="categoriesmainBlock">



                        <div id="divMessage" visible="false" runat="server">

                            <%-- <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <asp:Literal ID="ltrMessage" runat="server" /></strong>
                            </div>--%>
                        </div>
                        <!--butonblock-->
                        <div class="categoryMidlBlock">
                            <div class="midleDrgblecontin">
                                <ul class="dragblnextBlock ">
                                    <li>
                                        <table width="100%" class="dragbltop">
                                            <tbody>
                                                <tr>
                                                    <th class="firsttd firsttdResize">Category Name
                                                    </th>
                                                    <th class="secondtd">Products In Category
                                                    </th>
                                                    <th class="thirdtd">Products In Sub Category
                                                    </th>
                                                    <th class="fourthtd">Products In Sub Sub Category
                                                    </th>
                                                    <th class="fivethtd fivethtdleft">Status
                                                    </th>
                                                    <th class="sixthtd sixthtdResize">Acive for UT
                                                    </th>
                                                    <th class="seventd seventhtdleft">Action
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <asp:Repeater ID="rptPCategory" runat="server" OnItemDataBound="rptPCategory_OnItemDataBound" OnItemCommand="rptPCategory_ItemCommand">
                                        <ItemTemplate>
                                            <li class="firstLitab">
                                                <table width="100%" class="dragbltop">
                                                    <tbody>
                                                        <tr id="trCategory" runat="server">
                                                            <td class="firsttd firsttdResize">
                                                                <span id="spanCategoryName" runat="server">
                                                                    <%# DataBinder.Eval(Container.DataItem, "CategoryName") %></span>
                                                            </td>
                                                            <td class="secondtd">
                                                                <asp:TextBox ID="txtpcatid" Style="display: none;" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'></asp:TextBox>
                                                                <asp:Literal ID="ltlProductCount" runat="server"></asp:Literal>
                                                            </td>
                                                            <td class="thirdtd"></td>
                                                            <td class="fourthtd"></td>
                                                            <td class="fivethtd fivethtdleft">
                                                                <div class="firstDv firstDvleft">
                                                                    <span>
                                                                        <asp:CheckBox ID="chkStatus" rel='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                            runat="server" onclick="ShowLoader()" OnCheckedChanged="chk_OnCheckedChanged" Checked='<%# DataBinder.Eval(Container.DataItem,"IsActive") %>'
                                                                            AutoPostBack="true" />
                                                                    </span>
                                                                </div>

                                                            </td>
                                                            <td class="sixthtd sixthtdResize">
                                                                <div class="firstDv firstDvleft">
                                                                    <span>
                                                                        <asp:CheckBox ID="chkAssignUT" rel='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                            runat="server" onclick="ShowLoader()" OnCheckedChanged="chkAssignUT_CheckedChanged" Checked='<%# DataBinder.Eval(Container.DataItem,"UserTypeStatus") %>'
                                                                            AutoPostBack="true" />
                                                                    </span>
                                                                </div>

                                                            </td>
                                                            <td class="seventhtd seventhtdResize">
                                                                <div class="action">
                                                                    <a class="icon" href="#"></a>
                                                                    <div class="action_hover">
                                                                        <span></span>
                                                                        <div class="view_order other_option ">
                                                                            <a href="<%# Host %>Admin/Category/AddEditCategory.aspx?id=<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>&Type=E&language-id=<%=_DefaultLanguage %>">Edit</a>

                                                                            <asp:LinkButton ID="lnkbtnDisable" Text="Delete" CommandName="DisableCategory" OnClientClick="return confirm('Are you sure deleting this item?')"
                                                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                                runat="server" />
                                                                            <asp:HyperLink runat="server" ID="hrfNewSubCat" Text="New SubCategory" NavigateUrl="<%# Host %>/Admin/Category/AddEditCategory.aspx" />
                                                                            <a id="aAddProduct" runat="server" visible="false" href="#">Add Product</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <ul class="secondul">
                                                    <asp:Repeater ID="rptSCategory" runat="server" OnItemDataBound="rptSCategory_OnItemDataBound" OnItemCommand="rptPCategory_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <table width="100%" class="dragbltop dragbltopextra">
                                                                    <tbody>
                                                                        <tr id="trCategory" runat="server">
                                                                            <td class="firsttd firsttdResize">
                                                                                <span id="spansubCategoryName" runat="server" class="arrowUl">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "CategoryName") %></span>
                                                                            </td>
                                                                            <td class="secondtd "></td>
                                                                            <td class="thirdtd">
                                                                                <asp:TextBox ID="txtscatid" Style="display: none;" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'></asp:TextBox>
                                                                                <asp:TextBox ID="txtspcatid" Style="display: none;" runat="server"></asp:TextBox>
                                                                                <asp:Literal ID="ltlProductCount" runat="server"></asp:Literal>
                                                                                <asp:HiddenField ID="hdnSubSubCatID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SubSUbCategoryId") %>' />
                                                                            </td>
                                                                            <td class="fourthtd"></td>
                                                                            <td class="fivethtd fivethtdleft">
                                                                                <div class="firstDv firstDvleft">
                                                                                    <span>
                                                                                        <asp:CheckBox ID="chkStatus" rel='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                                            runat="server" onclick="ShowLoader()" OnCheckedChanged="chk_OnCheckedChanged" Checked='<%# DataBinder.Eval(Container.DataItem,"IsActive") %>'
                                                                                            AutoPostBack="true" />
                                                                                    </span>
                                                                                </div>

                                                                            </td>
                                                                            <td class="sixthtd sixthtdResize">
                                                                                <div class="firstDv firstDvleft">
                                                                                    <span>
                                                                                        <asp:CheckBox ID="chkAssignUT" rel='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                                            runat="server" onclick="ShowLoader()" OnCheckedChanged="chkAssignUT_CheckedChanged" Checked='<%# DataBinder.Eval(Container.DataItem,"UserTypeStatus") %>'
                                                                                            AutoPostBack="true" />
                                                                                    </span>
                                                                                </div>

                                                                            </td>

                                                                            <td class="seventhtd seventhtdResize">
                                                                                <div class="action">
                                                                                    <a class="icon" href="#"></a>
                                                                                    <div class="action_hover">
                                                                                        <span></span>
                                                                                        <div class="view_order other_option">
                                                                                            <a href="<%# Host %>Admin/Category/AddEditCategory.aspx?id=<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>&pid=<%# DataBinder.Eval(Container.DataItem, "ParentCategoryId") %>&Type=E&language-id=<%=_DefaultLanguage %>">Edit</a>
                                                                                            <asp:LinkButton ID="lnkbtnDisable" Text="Delete" CommandName="DisableCategory" OnClientClick="return confirm('Are you sure deleting this item?')"
                                                                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                                                runat="server" />
                                                                                            <asp:HyperLink runat="server" ID="hrfNewSubCat" Text="New SubCategory" Visible="false" />
                                                                                            <a id="aAddProduct" runat="server" visible="false" href="#">Add Product</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <ul class="secondul secondul1">
                                                                    <asp:Repeater ID="rptSSCategory" runat="server" OnItemDataBound="rptSSCategory_OnItemDataBound" OnItemCommand="rptPCategory_ItemCommand">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <table width="100%" class="dragbltop dragbltopextra">
                                                                                    <tbody>
                                                                                        <tr id="trCategory" runat="server">
                                                                                            <td class="firsttd firsttdResize">
                                                                                                <span id="spansubsubCategoryName" runat="server" class="arrowUl">
                                                                                                    <%# DataBinder.Eval(Container.DataItem, "CategoryName") %></span>
                                                                                            </td>
                                                                                            <td class="secondtd"></td>
                                                                                            <td class="thirdtd"></td>
                                                                                            <td class="fourthtd">
                                                                                                <asp:Literal ID="ltlProductCount" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td class="fivethtd fivethtdleft">
                                                                                                <div class="firstDv firstDvleft">
                                                                                                    <span>
                                                                                                        <asp:CheckBox ID="chkStatus" rel='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                                                            runat="server" onclick="ShowLoader()" OnCheckedChanged="chk_OnCheckedChanged" Checked='<%# DataBinder.Eval(Container.DataItem,"IsActive") %>'
                                                                                                            AutoPostBack="true" />
                                                                                                    </span>
                                                                                                </div>

                                                                                            </td>
                                                                                            <td class="sixthtd sixthtdResize">
                                                                                                <div class="firstDv firstDvleft">
                                                                                                    <span>
                                                                                                        <asp:CheckBox ID="chkAssignUT" rel='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                                                            runat="server" onclick="ShowLoader()" OnCheckedChanged="chkAssignUT_CheckedChanged" Checked='<%# DataBinder.Eval(Container.DataItem,"UserTypeStatus") %>'
                                                                                                            AutoPostBack="true" />
                                                                                                    </span>
                                                                                                </div>

                                                                                            </td>
                                                                                            <td class="seventhtd seventhtdResize">
                                                                                                <div class="action">
                                                                                                    <a class="icon" href="#"></a>
                                                                                                    <div class="action_hover">
                                                                                                        <span></span>
                                                                                                        <div class="view_order">
                                                                                                            <%-- <a href="<%# Host %>Admin/Category/AddEditCategory.aspx?id=<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>&pid=<%# DataBinder.Eval(Container.DataItem, "SubCategoryId") %>&Type=E&language-id=<%=_DefaultLanguage %>">Edit</a>--%>
                                                                                                            <asp:LinkButton ID="lnkbtnDisable" Text="Delete" CommandName="DisableCategory" OnClientClick="return confirm('Are you sure deleting this item?')"
                                                                                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                                                                                                runat="server" />

                                                                                                            <%--  <a href="#">Add Product</a>--%>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <!--dragblnextBlock-->
                            </div>
                        </div>
                        <!--categoryMidlBlock-->
                    </div>
                    <!--categoriesmainBlock-->
                </div>
            </div>
        </section>
    </div>
</asp:Content>
