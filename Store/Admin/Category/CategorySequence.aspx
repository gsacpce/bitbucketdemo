﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master"
    AutoEventWireup="true" Inherits="Presentation.Admin_CategorySequence" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JS/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {


            $(".ClickUlmain").toggle(function () {

                $(this).next('.ToggleBlock').slideUp();
                $(this).addClass('paneltabplus');

            }, function () {

                $(this).next('.ToggleBlock').slideDown();
                $(this).removeClass('paneltabplus');
            });

            $('body').on('click', '.paneltab', function () {

                $(this).toggleClass("paneltabplus");
                $(this).parent().parent().find('ul').slideToggle('slow');

            })
            //$('.textIcon').on("hover",function () {
            //    $('.textIcon').css('cursor', 'pointer');
            //});

            BindData();

        });
        function BindData() {

            $.ajax({
                url: '<%= Host %>Category/CategorySequenceAjax.aspx',
                type: 'POST',
                error: function (jqXHR, textResponse, errorThrown) {
                    // alert(jqXHR.responseText);
                    //  alert('reach here');
                },
                success: function (response) {
                    if (response != 'Failed') {
                        $('#websequence').html(response);
                        //  alert(response)
                        // alert('reach here 2');
                    }
                    else {
                        $('#divMessage').css({ display: "block" });
                        $('#divMessage').html('Data is not found.');
                        $("#divMessage").addClass("Errormsg");
                    }
                }
            }
            );
        }

        function GetData(CatType) {

            var PCat = "";
            var SCat = "";
            var SSCat = "";

            var PN = "";
            var SN = "";
            var SSN = "";

            PCat = $(CatType).find('.category');
            var iP = 1;
            var iS = 1;
            var iSS = 1;
            var PRel = 0;
            var SRel = 0;

            PCat.each(function (index) {
                PN = PN + "," + $(this).attr("rel") + "_" + iP;

                SCat = $(CatType + " .cat" + $(this).attr("rel")).find(".subcategory");

                /* sub category*/
                SCat.each(function (index) {
                    SN = SN + "," + $(this).attr("rel") + "_" + iS;

                    SSCat = $(CatType + " .subcat" + $(this).attr("rel")).find(".subsubcategory");

                    /* sub sub category*/
                    SSCat.each(function (index) {
                        SSN = SSN + "," + $(this).attr("rel") + "_" + iSS;
                        iSS++;
                    });

                    if (SRel != $(this).attr("rel"))
                        iSS = 1;

                    SRel = $(this).attr("rel");

                    iS++;
                });

                if (PRel != $(this).attr("rel"))
                    iS = 1;

                PRel = $(this).attr("rel");

                iP++;
            });

            if (PN.charAt(0) == ',')
                PN = PN.substr(1);

            //console.log("PN: " + PN);
            //  console.log("SN: " + SN);
            // console.log("SSN: " + SSN);

            //return PN + SN + SSN;
            return PN + SSN;
        }


        function SaveData() {
            $('#divMessage').css({ display: "none" });
            var Wdata = GetData('.WCategory');
            var JSonWdata = CreateJSonData(Wdata);
            $.ajax({
                url: '<%= Host %>Category/CategorySequence.aspx?MethodName=SaveData',
                data: { W: JSonWdata },
                dataType: 'html',
                type: 'POST',
                cache: false,
                error: function (jqXHR, textResponse, errorThrown) {
                    // alert(jqXHR.responseText);
                    //alert('reach here 3');
                },
                success: function (response) {
                    //     alert('Web :' + response)
                    //alert('reach here 4');
                    if (response == '1') {
                        WStatus = true;
                        $('#divMessage').show();
                    }
                }
            });


        }




        $('#divMessage').css({ display: "block" });
        //if (WStatus == true) {
        //    $('#divMessage').html('Data saved successfully.');
        //    $("#divMessage").addClass("sucessfully");
        //}
        //else {
        //    $('#divMessage').html('Data saved failed.');
        //    $("#divMessage").addClass("Errormsg");
        //}

        // HideLoader();


        function CreateJSonData(data) {

            var returnData = "";
            var SplitData = data.split(',')

            for (var i = 0; i < SplitData.length; i++) {
                var d = SplitData[i].split('_');
                returnData = returnData + ",{\"CatId\":" + d[0] + ", \"Sequence\":" + d[1] + "}";
            }

            if (returnData.charAt(0) == ',')
                returnData = returnData.substr(1);

            return "[" + returnData + "]";
        }


    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- homepage_slideshow start -->
    <div>

        <div class="admin_page">
            <section class="container mainContainer padbtom15">
                <div class="wrap_container ">
                    <div class="content padding_top">
                        <div id="divMessage" style="display: none">

                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Updated successfully
                                </strong>
                            </div>


                        </div>
                        <div class="left_align">
                            <h3 class="mainHead">Category Sequence</h3>
                            <span class="sectionText">You can set the sequence for  by simply
            dragging and dropping to the one required.
            <br />
                                For categories minimize before setting sequence and for sub categories and sub sub
            categories expand the category to set sequence.</span>
                        </div>
                        <div class="right_align">
                            <div class="butonblock">
                                <ul class="categories_btn">
                                    <li>

                                        <%-- <asp:Button runat="server" ID="btnSaveSequence" CssClass="btn3" OnClientClick="SaveData()" Text="Save Sequence" />--%>
                                        <input type="button" id="btnSave1" class="btn3" onclick="SaveData()" value="Save Sequence" />
                                        <%--<asp:Button runat="server" ID="Button1" CssClass="btn3" OnClientClick="SaveData()" Text="Save Sequence" />--%>
                                        <li>
                                            <li>
                                                <asp:Button runat="server" ID="btnAddNew" CssClass="btn3" OnClick="btn_Click" Text="Add New Category" />
                                            </li>
                                </ul>
                                <div class="clear">
                                </div>
                            </div>






                            <div class="categoriesmainBlock">

                                <%--<input type="button" value="Save" onclick="SaveData()" />--%>
                                <!--butonblock-->
                                <div class="categoryMidlBlock">
                                    <div class="websiteSqBlock  websiteSqBlockSpace" style="width: 30% !important;">
                                        <div class="texxt texxtnwp">

                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="websiteSqBlockInnerBlock">
                                            <h2>Category Name</h2>
                                            <div class="dragebLeBlock" id="websequence">
                                            </div>
                                            <!--dragebLeBlock-->
                                        </div>
                                        <!--websiteSqBlockInnerBlock-->
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <!--categoryMidlBlock-->
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <!--categoriesmainBlock-->
                        <div class="bottom_part margein0 button_section">
                            <ul>
                                <li>
                                    <%--<asp:Button runat="server" ID="btnFooterSequence" CssClass="save" OnClientClick="SaveData()" Text="Save Sequence" />--%>
                                    <input type="button" id="btnSave2" class="save" onclick="SaveData()" value="Save Sequence" />

                                    <li>
                                        <asp:Button ID="btnCancel" class=" save" Text="Cancel" runat="server" OnClick="btnCancel_Click" /></li>
                                    <%--<li><input type="submit" class="cancelM" id="cpContent_btnCancel" value="Cancel" name="ctl00$cpContent$btnCancel"></li>--%>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <div>
            </div>
</asp:Content>