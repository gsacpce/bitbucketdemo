﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master"
    AutoEventWireup="true" Inherits="Presentation.Admin_AssignProductCategory" ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="<%=AdminHost%>JS/Utilities.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {


            $('.Radios input').click(function () {
                jQuery('.Radios').removeClass('chkBoxmark');
                jQuery(this).parent().addClass('chkBoxmark');
                return false;
            });

            $('.chkBox input').click(function () {
                jQuery(this).parent().toggleClass('chkBoxmark');
                return false;
            });

            $('.CsA h3').click(function () {
                $(this).next().stop().slideToggle();
                $(this).toggleClass('rightA');
            });
            $('#imgLeftToRight').click(function () { })

        });


    </script>
    <script type="text/javascript">
        function fn_MoveStoreDataLeftToRight() {

            MoveOption('<%= lstUnAssignedProducts.ClientID %>', '<%= lstAssignedProducts.ClientID %>');
            StoreIds('<%= lstUnAssignedProducts.ClientID %>', '<%= lstAssignedProducts.ClientID %>', '<%= HIDUnAssignedProductIds.ClientID %>', '<%= HIDAssignedProductIds.ClientID %>');
            StoreNameNIdPipe('<%= lstUnAssignedProducts.ClientID %>', '<%= lstAssignedProducts.ClientID %>', '<%= HIDUnAssignedProductNameIds.ClientID %>', '<%= HIDAssignedProductNameIds.ClientID %>');
        }

        function fn_MoveStoreDataRightToLeft() {

            MoveOption('<%= lstAssignedProducts.ClientID %>', '<%= lstUnAssignedProducts.ClientID %>');
            StoreIds('<%= lstUnAssignedProducts.ClientID %>', '<%= lstAssignedProducts.ClientID %>', '<%= HIDUnAssignedProductIds.ClientID %>', '<%= HIDAssignedProductIds.ClientID %>');
            StoreNameNIdPipe('<%= lstUnAssignedProducts.ClientID %>', '<%= lstAssignedProducts.ClientID %>', '<%= HIDUnAssignedProductNameIds.ClientID %>', '<%= HIDAssignedProductNameIds.ClientID %>');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- homepage_slideshow start -->
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=Host %>Admin/Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Categories </a></li>
            <li>Assign Products To Category</li>
        </ul>
    </div>


    <div class="admin_page">
        <section class="container mainContainer padbtom15 assing_prodcut">
            <div class="wrap_container ">
                <div class="content">

                    <div id="divMessage" visible="false" runat="server">

                        <div>
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>
                                <asp:Literal ID="ltrMessage" runat="server" /></strong>
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </div>


                    </div>
                    <div class="wide">
                        <h3 class="mainHead">Assign Products To Category</h3>
                        <%--  <span class="sectionText"></span>--%>
                    </div>
                    <div class="wide" id="divNoCategoryMessage" runat="server">
                        <h4>No Category Created</h4>
                    </div>

                    <div class="addsaTicP categories asin_cagry ">
                        <div class="CsA">
                            <!-- according start -->
                            <%-- <h3>
                    Assign Products</h3>--%>

                            <%--      <asp:UpdatePanel runat="server" ID="updatePanelAssign" UpdateMode="Always">
                    <ContentTemplate>--%>


                            <div class="DetailsB Editsection">
                                <div class="sectionradio">
                                    <div class="webAddeditsection">
                                        <table class="selectcatogytble" width="100%">
                                            <tr style="height: 30px" id="trPCategory" runat="server" visible="false">
                                                <td align="left" width="20%">
                                                    <span class="textspace">Select Category:</span>
                                                </td>
                                                <td align="left" style="height: 30px" width="40%">
                                                    <div class="select-style selectpicker ">
                                                        <asp:DropDownList ID="ddlPParentCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPParentCategory_OnSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td width="40%"></td>
                                            </tr>
                                            <tr style="height: 30px" id="trPSCategory" runat="server" visible="false">
                                                <td align="left" style="width: 200px">
                                                    <span class="textspace">Select Sub Category:</span>
                                                </td>
                                                <td align="left" style="height: 30px">
                                                    <div class="select-style selectpicker">
                                                        <asp:DropDownList ID="ddlPSubCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPSubCategory_OnSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr style="height: 30px" id="trPSSCategory" runat="server" visible="false">
                                                <td align="left" style="width: 200px">
                                                    <span class="textspace">Select Sub Sub Category:</span>
                                                </td>
                                                <td align="left" style="height: 30px">
                                                    <div class="select-style selectpicker">
                                                        <asp:DropDownList ID="ddlPSubSubCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPSubSubCategory_OnSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trProduct" runat="server" visible="false">
                                                <td colspan="2">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <div class="box1 boxManageReviews" style="background-image: none !important;">
                                                                    <asp:Panel runat="server" ID="panel1" DefaultButton="btnSearch">
                                                                        <asp:TextBoxWatermarkExtender ID="txtFilterWaterMark" TargetControlID="txtFilter"
                                                                            runat="server" WatermarkText="Product Code / Name">
                                                                        </asp:TextBoxWatermarkExtender>
                                                                        <asp:TextBox ID="txtFilter" runat="server" Width="260px"></asp:TextBox>
                                                                        <asp:Button ID="btnSearch" runat="server" CssClass="save" Text="Search" OnClick="btnSearch_Click" OnClientClick="javascript:ShowLoader();" />
                                                                        <asp:Button ID="btnReset1" runat="server" CssClass="save" Text="Reset" OnClick="btnReset_Click" OnClientClick="javascript:ShowLoader();" />
                                                                        <asp:HiddenField ID="hdnFilter" runat="server" />
                                                                    </asp:Panel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <br />
                                                                <br />
                                                                <table>
                                                                    <tr>
                                                                        <td width="40%">Unassigned Products
                                                                        </td>
                                                                        <td style="width: 20%"></td>
                                                                        <td width="40%">Assigned Products
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div style="overflow: auto">
                                                                                <asp:ListBox ID="lstUnAssignedProducts" Height="250px" Style="min-width: 200px; width: auto;" runat="server"
                                                                                    SelectionMode="Multiple"></asp:ListBox>
                                                                            </div>
                                                                        </td>
                                                                        <td align="center" style="vertical-align: middle; padding: 5px 18px;">
                                                                            <img runat="server" id="imgLeftToRight" src="~/Admin/Images/UI/front-arrow.gif" style="cursor: hand"
                                                                                alt="Click to move selected item from left box to right box" />
                                                                            <br />
                                                                            <br />
                                                                            <br />
                                                                            <br />
                                                                            <img runat="server" id="imgRightToLeft" src="~/Admin/Images/UI/back-arrow.gif" style="cursor: hand"
                                                                                alt="Click to move selected item from right box to left box" />
                                                                        </td>
                                                                        <td>
                                                                            <div style="overflow: auto">
                                                                                <asp:ListBox ID="lstAssignedProducts" runat="server" Height="250px" Style="min-width: 200px; width: auto;"
                                                                                    SelectionMode="Multiple"></asp:ListBox>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">&nbsp;&nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="HIDPSelectedId" runat="server" />
                            <asp:HiddenField ID="HIDAssignedProductIds" runat="server" />
                            <asp:HiddenField ID="HIDUnAssignedProductIds" runat="server" />
                            <asp:HiddenField ID="HIDAssignedProductNameIds" runat="server" />
                            <asp:HiddenField ID="HIDUnAssignedProductNameIds" runat="server" />
                            <%--</ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanelAssign">
                    <ProgressTemplate>
                        <div style="position: absolute; z-index: 1; left: 600px; top: 450px;">
                            <div id="hideshow">
                                <div id="fade">
                                </div>
                                <div class="popup_block">
                                    <div class="popupLoader">
                                        <asp:Image ID="Imgloader" runat="server" ImageUrl="~/Images/ajax-loader.gif" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>--%>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!--categoriesmainBlock-->
                    <div class="bottom_part my">
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSaveExit" runat="server" Text="Save" OnClick="btnSubmit_Click"
                                    OnClientClick="javascript:ShowLoader();" CssClass="exit save" />
                            </li>
                            <%--<li>
                <asp:Button ID="Button1" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn-primary" />
            </li>--%>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- homepage_slideshow end -->
    <asp:HiddenField ID="HIDRelatedCateoryId" Value="0" runat="server" />
</asp:Content>
