﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Admin_Category_AddEditCategory" MasterPageFile="~/Admin/Master/AdminMaster.master" ValidateRequest="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .red {
            color: #FF0000;
            padding: 7px !important;
            position: absolute;
            width: auto;
        }

        .customer_info_hover {
            background: none repeat scroll 0 0 #ffffff;
            border: 1px solid #cccccc;
            border-radius: 5px;
            box-shadow: 0 0 2px #a6a6a6;
            cursor: default;
            display: none;
            font-family: Arial;
            font-size: 11px;
            font-weight: normal;
            line-height: 18px;
            padding: 6px 7px;
            position: absolute;
            right: -206px;
            text-indent: 0;
            top: -3px;
            width: 180px;
            z-index: 99999;
        }

            .customer_info_hover span {
                position: relative;
            }

                .customer_info_hover span:before {
                    position: absolute;
                    content: "";
                    border: 9px solid transparent;
                    border-right-color: #cccccc;
                    left: -25px;
                    top: -7px;
                    z-index: 9;
                }

                .customer_info_hover span:after {
                    position: absolute;
                    content: "";
                    border: 9px solid transparent;
                    border-right-color: #fff;
                    left: -24px;
                    top: -7px;
                    z-index: 9;
                }

        .info {
            background: url("../images/icon2.png") no-repeat scroll 7px -154px transparent;
            float: left;
            width: 27px;
            height: 20px;
            margin: 6px 0 0 0px;
            text-indent: -5000px;
            cursor: pointer;
            position: relative;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=host %>Admin/JS/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
    <script src="https:////cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>

    <%-- <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
    <script src="https:////cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>--%>

    <link href="<%=host %>Admin/FineUploader/fineuploaderCategory-3.2.css" rel="stylesheet" />
    <script src="<%=host %>Admin/FineUploader/jquery.fineuploaderCategory-3.2.js"></script>

    <div class="container">

        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Categories </a></li>
            <li>Manage Categories</li>
        </ul>
    </div>

    <div class="admin_page">

        <input type="hidden" id="HIDCatMenuId" runat="server" />
        <input type="hidden" id="HIDCatId" runat="server" />
        <input type="hidden" id="HIDBanId" runat="server" />
        <input type="hidden" id="HIDCatMouserover" runat="server" />
        <input type="hidden" class="categorymenusrc" id="hidcategoryMenusrc" runat="server" />
        <input type="hidden" class="categorymouseoversrc" id="hidcategorymouseover" runat="server" />
        <input type="hidden" class="categorysrc" id="hidCategorySrc" runat="server" />
        <input type="hidden" class="bannersrc" id="hideBannerSrc" runat="server" />
        <input type="hidden" id="hdnCategoryId" runat="server" />

        <div class="container ">
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                </div>
            </div>







            <div class="wrap_container ">
                <div class="content">

                    <div class="row">
                        <div class="col-md-12 relative_data">
                            <h3>
                                <asp:Literal ID="ltlHeading" Text="Add New CAtegory" runat="server"></asp:Literal>
                            </h3>
                            <p>Products in your website are grouped by categories, which makes them easier to find. Fill out the form below to create a new category.</p>
                            <div class="right_selct_dropdwn">
                                <div style="float: right" id="divLang" runat="server" visible="false">
                                    <label class="sm_select">Select Language :</label>
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="collapsableMenu ">
                    <a aria-expanded="true" data-target="#webPage" data-toggle="collapse" class="btn btn-info" type="button">Web Page Details</a>
                    <div aria-expanded="true" class="collapse in coloredChkBox" id="webPage">

                        <div class="row collapsableContent">
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="name">Name<span class="redColor">*</span>:</label>
                                <div class="col-md-3 col-sm-5">
                                    <asp:TextBox ID="txtCategoryName" runat="server" class="form-control"></asp:TextBox>

                                </div>
                                <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ControlToValidate="txtCategoryName"
                                    ErrorMessage="Please enter category Name." SetFocusOnError="True"
                                    ValidationGroup="VaildCategory" CssClass="red"></asp:RequiredFieldValidator>
                                <%-- <asp:RegularExpressionValidator ID="rexpCategoryName" runat="server" ErrorMessage="Category Name is not valid"
                                    ValidationExpression="^[A-Za-z0-9-_’&+/. ® ™$£€]*$" ControlToValidate="txtCategoryName" SetFocusOnError="True"
                                    ValidationGroup="VaildCategory" CssClass="red"></asp:RegularExpressionValidator>--%>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="category">Parent Category:</label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:DropDownList ID="ddlParentCategory" CssClass="select-style selectpicker" runat="server" Enabled="true">
                                    </asp:DropDownList>

                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="category">Banner Text:</label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox ID="txtBanner" runat="server" class="form-control"></asp:TextBox>

                                </div>

                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="desc">Description:</label>
                                <div class="col-md-9 col-sm-10">
                                    <CKEditor:CKEditorControl ID="txtDescription" BasePath="~/admin/ckeditor/" runat="server"></CKEditor:CKEditorControl>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="collapsableMenu">
                    <a aria-expanded="true" data-target="#Images" data-toggle="collapse" class="btn btn-info" type="button">Images</a>
                    <div aria-expanded="true" class="collapse in coloredChkBox" id="Images">

                        <div class="row collapsableContent">
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="uploadImage">Upload Category Menu image for landing page:</label>
                                <div class="col-md-10 col-sm-10 uploadImage">
                                    <div id="FUCategoryMenu">
                                    </div>
                                    <div class="uploaderMsg">[Use .jpg, .jpeg , .png file with <= 300px X 100 and <=2.00 MB size only]</div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="uploadImage">Upload Category Mouse Over image for landing page:</label>
                                <div class="col-md-10 col-sm-10 uploadImage">
                                    <div id="FUCategoryMouseOver">
                                    </div>
                                    <div class="uploaderMsg">[Use .jpg, .jpeg , .png file with <= 300px X 100 and <=2.00 MB size only]</div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="uploadImage">Upload category image for landing page:</label>
                                <div class="col-md-10 col-sm-10 uploadImage">
                                    <div id="FUCategory">
                                    </div>

                                    <div class="uploaderMsg">[Use .jpg, .jpeg , .png file with <= 200px X 200px and <=2.00 MB size only]</div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="uploadImage">Upload category banner for listing page:</label>
                                <div class="col-md-10 col-sm-10 uploadImage">
                                    <div id="FUBanner">
                                    </div>
                                    <div class="uploaderMsg" style="display: none;">[Use .jpg, .jpeg , .png file with <= 1200px X 250px and <=2.00 MB size only]</div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-2" for="category">Banner Link:</label>
                                <div class="col-md-4 col-sm-10">
                                    <asp:TextBox ID="txtBannerLink" runat="server" class="form-control"></asp:TextBox>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="collapsableMenu">
                    <a aria-expanded="true" data-target="#SeoMetadata" data-toggle="collapse" class="btn btn-info" type="button">SEO / Meta data</a>
                    <div aria-expanded="true" class="collapse in coloredChkBox" id="SeoMetadata">
                        <div class="row collapsableContent">
                            <div class="form-group clearfix seoMetaDataPanel">
                                <label class="control-label col-sm-2" for="name">Page Title:</label>
                                <div class="col-md-7 col-sm-5">
                                    <asp:TextBox ID="txtPageTitle" class="form-control" runat="server"></asp:TextBox>

                                </div>

                                <div class="col-md-3 col-sm-3">
                                    <p>
                                        <a href="javascript:void(0);" data-toggle="popover" data-content="Defines a title in the browser toolbar,  provides a title for the page when it is added to favorites, displays a title for the page in search-engine results">
                                            <img src="../Images/UI/info-icon.png" alt="" />
                                        </a>
                                    </p>
                                </div>

                            </div>

                            <div class="form-group clearfix seoMetaDataPanel">
                                <label class="control-label col-sm-2" for="name">Meta Description:</label>
                                <div class="col-md-7 col-sm-5">
                                    <asp:TextBox ID="txtMetaDescription" class="form-control" runat="server"></asp:TextBox>

                                    <asp:RegularExpressionValidator ID="revMetaDescription" runat="server" ControlToValidate="txtMetaDescription"
                                        Display="Dynamic" ErrorMessage="Meta description is too long" ForeColor="" SetFocusOnError="True"
                                        ValidationExpression="^[\w\W]{0,1000}$" ValidationGroup="VaildCategory"></asp:RegularExpressionValidator>
                                </div>

                                <div class="col-md-3 col-sm-3">
                                    <p>
                                        <a href="javascript:void(0);" data-toggle="popover" data-content="Give a brief description of your page in this tag. Bear in mind that this description (or part of it) will be displayed in the search engine results so try to make sure you phrase it in such a way that the person searching can tell at a glance that he's found the correct page for his search. Minimize irrelevancies and put the essentials near the beginning of the description so that if the search engine only accepts the initial (say) 150 characters, the essential parts of your description will still be displayed.">
                                            <img src="../Images/UI/info-icon.png" alt="" />
                                        </a>
                                    </p>
                                </div>

                            </div>

                            <div class="form-group clearfix seoMetaDataPanel">
                                <label class="control-label col-sm-2" for="name">Meta Keywords:</label>
                                <div class="col-md-7 col-sm-5">
                                    <asp:TextBox ID="txtMetaKeyWords" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="revMetaKeywords" runat="server" ControlToValidate="txtMetaKeyWords"
                                        Display="Dynamic" ErrorMessage="Meta keywords is too long" ForeColor="" SetFocusOnError="True"
                                        ValidationExpression="^[\w\W]{0,1000}$" ValidationGroup="VaildCategory"></asp:RegularExpressionValidator>



                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <p>
                                        <a href="javascript:void(0);" data-toggle="popover" data-content="The meta keywords tag allows you to provide additional text for crawler-based search engines to index along with the rest of what you've written on your site">
                                            <img src="../Images/UI/info-icon.png" alt="" />
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="buttonPanel container">

                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSaveExit" OnClientClick="javascript:ShowLoader();" CommandName="btnSaveExit" CssClass="btn" runat="server" Text="SAve & Exit" OnCommand="BtnActionCommand" />

                            </li>

                            <li>
                                <asp:Button ID="btnSaveAdd" OnClientClick="javascript:ShowLoader();" CommandName="btnSaveAdd" CssClass="btn" runat="server" Text="SAve & Add Another" OnCommand="BtnActionCommand" />
                            </li>
                            <li>
                                <asp:Button ID="btnSaveAssign" OnClientClick="javascript:ShowLoader();" CommandName="btnSaveAssign" CssClass="btn" runat="server" Text="SAve & Assign Product" OnCommand="BtnActionCommand" Visible="false" />

                            </li>

                            <li>
                                <asp:Button ID="btnCancel" CssClass="btn  gray" OnClientClick="return confirm('are you sure want to cancel ?')" runat="server" Text="Cancel" OnClick="btnCancel_Click" />

                            </li>
                        </ul>
                    </div>




                </div>
            </div>




        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover({
                placement : 'right'
            });
        });
    </script>
    <script type="text/javascript">
        var host ='<%=host %>';
               
                
        $(document).ready(function () {
            $('#FUCategoryMouseOver').fineUploader({
                request: {
                    endpoint: host + 'Admin/fineUploader/UploadCategory.aspx?type=categorymouseover&categoryid='+ '<%=TempIdCategoryMouseOver %>'+'&updLanguageid='+'<%=updLanguageid%>'+'&action=add'
                },
                multiple: false,
                validation: {
                    sizeLimit: <%= ImageSize %>,
                    allowedExtensions: [<%=CategoryExtension%>]
                },
                text: {
                    uploadButton: 'Click here to upload Category Mouse image<br>or<br>Drag and drop file here to upload'
                },
                retry: {
                    enableAuto: false
                },
                chunking: {
                    enabled: false,
                    partSize: <%= ImageSize %>
                    },
                showMessage: function (message) {
                    // Using Bootstrap's classes
                    $('#FUCategoryMouseOver .fineError').hide();
                    $('#FUCategoryMouseOver .FailureDiv').hide();
                    $('#FUCategoryMouseOver').append('<div class="alert alert-error fineError">' + message + '</div>');
                },
                debug: true
            });
            $('#FUCategoryMenu').fineUploader({
                request: {
                    endpoint: host + 'Admin/fineUploader/UploadCategory.aspx?type=categorymenu&categoryid='+ '<%=TempIdforCategoryMenu %>'+'&updLanguageid='+'<%=updLanguageid%>'+'&action=add'
                },
                multiple: false,
                validation: {
                    sizeLimit: <%= ImageSize %>,
                    allowedExtensions: [<%=CategoryExtension%>]
                },
                text: {
                    uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                },
                retry: {
                    enableAuto: false
                },
                chunking: {
                    enabled: false,
                    partSize: <%= ImageSize %>
                    },
                showMessage: function (message) {
                    // Using Bootstrap's classes
                    $('#FUCategoryMenu .fineError').hide();
                    $('#FUCategoryMenu .FailureDiv').hide();
                    $('#FUCategoryMenu').append('<div class="alert alert-error fineError">' + message + '</div>');
                },
                debug: true
            });
            $('#FUCategory').fineUploader({
                request: {
                    endpoint: host + 'Admin/fineUploader/UploadCategory.aspx?type=category&categoryid='+ '<%=TempCategoryId %>' +'&action=add'
                },
                multiple: false,
                validation: {
                    sizeLimit: <%= ImageSize %>,
                    allowedExtensions: [<%=CategoryExtension%>]
                },
                text: {
                    uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                },
                retry: {
                    enableAuto: false
                },
                chunking: {
                    enabled: false,
                    partSize: <%= ImageSize %>
                    },
                showMessage: function (message) {
                    // Using Bootstrap's classes
                    $('#FUCategory .fineError').hide();
                    $('#FUCategory .FailureDiv').hide();
                    $('#FUCategory').append('<div class="alert alert-error fineError">' + message + '</div>');
                },
                debug: true
            });
            $('#FUBanner').fineUploader({
                request: {
                    endpoint: host + 'Admin/fineUploader/UploadCategory.aspx?type=banner&categoryid='+ '<%= TempBannerId %>' +'&action=add'
                },
                multiple: false,
                validation: {
                    sizeLimit: <%= BannerSize %>,
                    allowedExtensions: [<%=BannerExtension%>]
                },
                text: {
                    uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                },
                retry: {
                    enableAuto: false
                },
                chunking: {
                    enabled: false,
                    partSize: <%= BannerSize %>
                    },
                showMessage: function (message) {
                    // Using Bootstrap's classes
                    $('#FUBanner .fineError').hide();
                    $('#FUBanner .FailureDiv').hide();
                    $('#FUBanner').append('<div class="alert alert-error fineError">' + message + '</div>');
                },
                debug: true
            });
        });

        jQuery(document).ready(function () {
            if ('<%= BannerHtml %>' != '')
            {
                $('#FUBanner .qq-upload-button').hide();
                $('#FUBanner .qq-upload-list').html('<%= BannerHtml  %>');
                $('#FUBanner .uploadedimage').css('maxHeight', 190);
                $('#FUBanner .uploadedimage').css('maxWidth', 700);
                ActionType = 'banner'
                      
            } 
            if ('<%= CategoryHtml %>' != '') {
                $('#FUCategory .qq-upload-button').hide();
                $('#FUCategory .qq-upload-list').html('<%= CategoryHtml  %>');
                $('#FUCategory .uploadedimage').css('maxHeight',190);
                $('#FUCategory .uploadedimage').css('maxWidth', 200);
                ActionType = 'category'
                      
            }
            if ('<%= CategoryMenuHtml %>' != '') {
                $('#FUCategoryMenu .qq-upload-button').hide();
                $('#FUCategoryMenu .qq-upload-list').html('<%= CategoryMenuHtml  %>');
                $('#FUCategoryMenu .uploadedimage').css('maxHeight',190);
                $('#FUCategoryMenu .uploadedimage').css('maxWidth', 200);
                ActionType = 'categorymenu'
            }
            if ('<%= CategoryMouseOverHtml %>' != '') {
                $('#FUCategoryMouseOver .qq-upload-button').hide();
                $('#FUCategoryMouseOver .qq-upload-list').html('<%= CategoryMouseOverHtml  %>');
                $('#FUCategoryMouseOver .uploadedimage').css('maxHeight',190);
                $('#FUCategoryMouseOver .uploadedimage').css('maxWidth', 200);
                ActionType = 'categorymouseover'
            }
        });
    </script>
</asp:Content>
