﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Admin_MaintainNotes_ViewNotes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" type="text/css" href="../../JS/bootstrap-datetimepicker.js" />
    <link href="../CSS/jquery-ui.css" rel="stylesheet" />
    <link href="../CSS/ui.theme.css" rel="stylesheet" />
    <style type="text/css">
        .pager span {
            color: #009900;
            font-weight: bold;
            font-size: 16pt;
        }

        .classWidthFull {
            width: 100%;
        }

        .row.paginationBlk {
            border: 1px solid #ccc;
            margin: 10px 0;
            padding: 10px 0 5px;
        }

        .marginTop10 {
            margin-top: 10px;
        }
    </style>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Maintain notes</a></li>
            <li>View notes</li>
        </ul>

    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container">
                <div class="content ">
                    <div class="row ">
                        <div class="col-md-12 relative_data">
                            <h3>View notes
                            </h3>

                        </div>
                        <div class="col-md-12 relative_data">
                            <a href="AddNewNote.aspx" runat="server" class="btn btn-info pull-right">Add Note</a>
                        </div>

                    </div>
                    <div class="row marginTop10">
                        <div class="col-md-12 midleDrgblecontin dragblnextBlock">

                            <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvList_PageIndexChanging"
                                ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid all_customer_inner dragbltop" Style="border-width: 0px; width: 100%; border-collapse: collapse;">
                                <Columns>
                                    <asp:TemplateField HeaderText="Note ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdfNoteID" runat="server" Value='<%# Eval("NoteId") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="1%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Note No" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNoteNo" runat="server" Text='<%# Eval("NoteNo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="1%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Note Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNoteDate" runat="server" Text='<%# Eval("NoteDate", "{0:dd-MM-yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="1%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Created By">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedBy").ToString() %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Note">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNote" CssClass="model" runat="server" Text='<%# Eval("Note") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="80%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Status" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsActive" runat="server" Text='<%# Eval("Status").ToString().ToLower() == "true" ? "Active" : "InActive" %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" FirstPageText="First" PageButtonCount="6"
                                    LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                <PagerStyle CssClass="paging" />
                                <SelectedRowStyle ForeColor="White" Font-Bold="True" BackColor="#9471DE"></SelectedRowStyle>

                                <PagerStyle CssClass="paging" />
                                <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                                <HeaderStyle CssClass="GridHeading" />
                                <AlternatingRowStyle CssClass="GridAlternateItem" />
                            </asp:GridView>
                            <i>You are viewing page
                                    <%=gvList.PageIndex + 1%>
                                    of
                                    <%=gvList.PageCount%>
                            </i>

                        </div>
                    </div>
                    <asp:Label ID="lblNote" runat="server"></asp:Label>
                </div>
            </div>

        </section>
    </div>
</asp:Content>

