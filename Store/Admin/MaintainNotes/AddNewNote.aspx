﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_MaintainNotes_AddNewNote" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" type="text/css" href="../../JS/bootstrap-datetimepicker.js" />
    <link href="../CSS/jquery-ui.css" rel="stylesheet" />
    <link href="../CSS/ui.theme.css" rel="stylesheet" />
    <style type="text/css">
        .pager span {
            color: #009900;
            font-weight: bold;
            font-size: 16pt;
        }

        .classWidthFull {
            width: 100%;
        }

        .row.paginationBlk {
            border: 1px solid #ccc;
            margin: 10px 0;
            padding: 10px 0 5px;
        }

        .margin-Bottom {
            margin-bottom: 7px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("#<%= txtNoteDate.ClientID  %>").datepicker({ dateFormat: 'yy-mm-dd' });
          });
    </script>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Maintain Notes</a></li>
            <li>Add new note</li>
        </ul>

    </div>
    <div class="admin_page">
        <div class="container mainContainer ">
            <div class="wrap_container gift_cupon">
                <div class="content ">
                    <div class="row ">
                        <div class="col-md-12 relative_data">
                            <h3>Add new note
                            </h3>

                        </div>

                        <asp:Panel ID="pnlAdminAddEdit" runat="server">
                            <div class="row margin-Bottom">
                                <div class="col-md-2 text-right">
                                    <asp:Label ID="lblNoteNo" runat="server" Text="Note no:" CssClass="LabelText" Visible="false"></asp:Label>
                                </div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtNoteNo" CssClass="classWidthFull" runat="server" Style="width: 100px;" Visible="false"></asp:TextBox>
                                    <asp:HiddenField ID="hdnNoteNo" runat="server" Visible="false" />
                                </div>
                            </div>
                            <div class="row margin-Bottom" style="display: none;">
                                <div class="col-md-2 text-right">
                                    <asp:Label ID="lblNoteDate" runat="server" CssClass="LabelText" Text="Note date:"></asp:Label>
                                </div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtNoteDate" runat="server" Style="width: 150px; padding: 5px;"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ID="reqtxtNoteDate" runat="server" ControlToValidate="txtNoteDate"
                                        ErrorMessage="Please select note date" Display="None" SetFocusOnError="true"
                                        ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="row margin-Bottom">
                                <div class="col-md-2 text-right">
                                    <asp:Label ID="lblNote" runat="server" Text="Note :" CssClass="LabelText"></asp:Label>
                                </div>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Style="height: 400px; width: 700px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtNote" runat="server" ControlToValidate="txtNote"
                                        ErrorMessage="Please enter the note" Display="None" SetFocusOnError="true"
                                        ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 col-md-offset-6  button_section">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                    ValidationGroup="Add" CssClass="btn" />
                                <a href="ViewNotes.aspx" class="btn" runat="server">View Notes</a>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick"
                                    CssClass="btn gray" />
                                <asp:ValidationSummary ID="valAdmin" runat="server" DisplayMode="List" ShowSummary="true"
                                    ValidationGroup="Add" CssClass="text-danger" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
