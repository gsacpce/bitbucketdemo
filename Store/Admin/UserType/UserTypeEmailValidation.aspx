﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_UserType_UserTypeEmailValidation" %>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">User Types</a></li>
            <li>Import / Export EmailId  </li>
        </ul>

    </div>
    <div class="admin_page">
        <div class="container mainContainer ">
            <div class="wrap_container gift_cupon">
                <div class="content ">
                    <div class="customer" style="margin: 0px !important;">
                        <span class="allcosutomer">All Records -
                    <asp:Literal ID="ltrAllRecords" runat="server" Text=""></asp:Literal></span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="table-responsive">
                        <asp:Panel ID="pnlList" runat="server">
                            <section class=" all_customer dragblnextBlock manage_pro">
                                <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="30" AllowCustomPaging="false" DataKeyNames="UserTypeId,UserTypeName"
                                    ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid" OnPageIndexChanging="gvList_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sequence">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User Type">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "UserTypeName")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="WhiteList">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsWhiteListed" runat="server"  Checked='<%# Eval("IsWhiteList").ToString().ToLower() == "true" ? true : false %>' AutoPostBack="true" OnCheckedChanged="chkIsWhiteListed_CheckedChanged" />
                                             </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Upload Files">
                                            <ItemTemplate>
                                              <asp:FileUpload ID="flImport" runat="server" />
                                            </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Import">
                                            <ItemTemplate>
                                               <asp:Button ID="btnImport" runat="server" Text="Import" CommandArgument='<%# Eval("UserTypeID") %>' OnClick="btnImport_Click" />
                                            </ItemTemplate>
                                          </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Export">
                                            <ItemTemplate>
                                               <asp:Button ID="btnExport" runat="server" Text="Export" CommandArgument='<%# Eval("UserTypeID") %>' OnClick="btnExport_Click" />
                                            </ItemTemplate>
                                          </asp:TemplateField>
                                        
                                    </Columns>
                                    <PagerSettings Position="TopAndBottom" />
                                    <PagerStyle CssClass="paging" />
                                    <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                                    <HeaderStyle CssClass="GridHeading" />
                                    <AlternatingRowStyle CssClass="GridAlternateItem" />
                                </asp:GridView>
                            </section>

                        </asp:Panel>
                    </div>

                </div>
            </div>
        </div>
   </div>
</asp:Content>


