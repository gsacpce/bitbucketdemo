﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_UserType_ManageUserType_Updated" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">User Type </a></li>
            <li>Manage User Type</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <!-- Panel for Adding User Type Name and Currency -->
                    <asp:Panel ID="pnlAdd" runat="server" Visible="true">
                        <h3>
                            <asp:Label ID="lblHeading" runat="server" CssClass="mainHead" Text="Add User Type"></asp:Label></h3>
                        <div class="row">
                            <div class="col-md-3">
                                <label id="select_lang" class="sm_select">
                                    Select Language :
                                </label>
                            </div>
                            <div class="right_selct_dropdwn relative_data col-md-3">
                                <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="select-style selectpicker" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label ID="lbluserTypeName" runat="server" Text="User Type Name:"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtuserTypeName" runat="server" CssClass="inputTxt"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rqvtxtuserTypeName" runat="server" ControlToValidate="txtuserTypeName" ValidationGroup="Add" ErrorMessage="Please Enter User Type name"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                        <div id="divaddFields" runat="server" visible="true">
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:Label ID="lblCurrencies" runat="server" Text="Select Currencies : "></asp:Label>
                                </div>
                                <div class="right_selct_dropdwn relative_data col-md-3">
                                    <asp:DropDownList ID="ddlCurrencies" runat="server" AutoPostBack="true" CssClass="select-style selectpicker" OnSelectedIndexChanged="ddlCurrencies_OnSelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="rfvddlCurrencies" runat="server" Text="*" ControlToValidate="ddlCurrencies" ValidationGroup="Add" ErrorMessage="Please Select Currency for User Type" InitialValue="Please select"></asp:RequiredFieldValidator>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <asp:Label ID="lblCatalogues" runat="server" Text="Select Catalogue : "></asp:Label>
                                </div>
                                <div class="right_selct_dropdwn relative_data col-md-3">
                                    <asp:DropDownList ID="ddlCatalogues" runat="server" CssClass="select-style selectpicker"></asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="rfvddlCatalogues" runat="server" Text="*" ControlToValidate="ddlCatalogues" ValidationGroup="Add" ErrorMessage="Please select catalogue for the User Type" InitialValue="Please select"></asp:RequiredFieldValidator>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:Label ID="lblVatDisplay" runat="server" Text="Vat Display :"></asp:Label>
                                </div>
                                <div class="col-md-3">
                                    <asp:CheckBox ID="chkVatDisplay" runat="server" />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:Label ID="lblVatRate" runat="server" Text="Vat Percentage : "></asp:Label>
                                </div>
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtVatRate" runat="server" CssClass="inputTxt"></asp:TextBox>
                                </div>
                            </div>
                            <%-- Region Added by SHRIGANESH 30 JAN for SSO specific User Types --%>
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:Label ID="lblIsUTSSO" runat="server" Text="Is SSO User Type : "></asp:Label>
                                </div>
                                <div class="col-md-3">
                                    <asp:CheckBox ID="chkIsUTSSO" runat="server" />
                                </div>
                            </div>
                            <%-- Region END --%>

                            <br />
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:Label ID="lblEnablePoints" runat="server" Text="Enable Points : "></asp:Label>
                                </div>
                                <div class="col-md-3">
                                    <asp:CheckBox ID="chkEnablePoints" runat="server" />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:Label ID="lblEnableCurrency" runat="server" Text="Enable Currency : "></asp:Label>
                                </div>
                                <div class="col-md-3">
                                    <asp:CheckBox ID="chkEnableCurrency" runat="server" />
                                </div>
                            </div>
                            <br />
                             <div class="row">
                                <div class="col-md-3">
                                    <asp:Label ID="lblEnableBudget" runat="server" Text="Enable Budget : "></asp:Label>
                                </div>
                                <div class="col-md-3">
                                    <asp:CheckBox ID="chkEnableBudget" runat="server" />
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField ID="hdnfUserTypeID" runat="server" />
                        <asp:HiddenField ID="hdnfCurrencyID" runat="server" />
                        <asp:HiddenField ID="hdnfSelectedCatalogueid" runat="server" />
                        <div class="row new_button_pannel button_section">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn BASYSCancel  gray" Text="Cancel" OnClick="btnCancel_OnClick" />
                        </div>
                    </asp:Panel>                 

                    <!-- Panel Added by SHRIGANESH SINGH 01 July 2016 for Editing Catalog details START-->
                    <asp:Panel ID="pnlEditCatalogueDetails" runat="server" Visible="false">
                        <h3>
                            <asp:Label ID="Label1" runat="server" CssClass="mainHead" Text="Edit Selected Catalogue Details"></asp:Label></h3>
                        <asp:Label ID="lblUserTypeCatalaogueName" runat="server" Text="User Type Name : "></asp:Label>
                        <asp:RadioButton ID="rdbUserTypeCatalaogueName" runat="server" Checked="true" />

                        <br />
                        <br />
                        <div class="button_section text-center">
                            <div class="attributeManagement">
                                <asp:GridView ID="gvEditUSerTypeCatalogueDetails" runat="server" OnRowCommand="gvEditUSerTypeCatalogueDetails_RowCommand" AutoGenerateColumns="false"
                                    ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid all_customer_inner1 inner1newcls  dragbltop dragbltopextra">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Catalogues">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkCatalogueID" runat="server" Text='<%# Eval("StoreCatalogueIds") + " " + Eval("CurrencyCode") %>' Checked='<%#Eval("ActiveUserTypeCatalogue").ToString().ToLower() == "true" ? true : false %>' />
                                                <asp:HiddenField ID="hdfActiveUserTypeCatalogue" runat="server" Value='<%# Eval("StoreCatalogueIds") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Display Vat">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkVatDisplay" runat="server" Checked='<%#Eval("IsVatDisplay").ToString().ToLower() == "true" ? true : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VAT Percentage">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtEditCatalogueVATRate" runat="server" Text='<%# Eval("VatRate") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- Added by SHRIGANESH 30 JAN for SSO specific User Types --%>
                                        <asp:TemplateField HeaderText="Is SSO User Type">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEditIsSSOUT" runat="server" Checked='<%#Eval("IsSSOUT").ToString().ToLower() == "true" ? true : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- Region End --%>

                                        <asp:TemplateField HeaderText="Enable Points">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsPointsEnabled" runat="server" Checked='<%#Eval("IsPointsEnabled").ToString().ToLower() == "true" ? true : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Enable Currency">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsCurrencyEnabled" runat="server" Checked='<%#Eval("IsCurrencyEnabled").ToString().ToLower() == "true" ? true : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                               <asp:TemplateField HeaderText="Enable Budget">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsBudgetEnabled" runat="server" Checked='<%#Eval("IsBudgetEnabled").ToString().ToLower() == "true" ? true : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                        <div class="buttonPanel">
                            <ul class="button_section">
                                <li>
                                    <asp:Button ID="btnUpdateCatalogueDetails" runat="server" Text="Update UserType Catalogue Details" OnCommand="btn_Command" CommandName="UpdateCatalogueDetails" Visible="false" CssClass="btn" />
                                    <asp:Button ID="btnCancelCatalogueDetails" runat="server" Text="Cancel UserType Catalogue Details" Visible="false" CssClass="btn" OnCommand="btn_Command" CommandName="CancelCatalogueDetails" />
                                </li>
                            </ul>
                        </div>

                        <%--<asp:Button ID="btnUpdateUserTypeCatalogueDetails" runat="server" Text="Update UserType Catalogue Data" OnClick="btnUpdateUserTypeCatalogueDetails_OnClick" CssClass="btn3" />
                        <asp:Button ID="btnCancelUserTypecatalogueDetails" runat="server" Text="Cancel" OnClick="btnCancelUserTypecatalogueDetails_OnClick" />--%>
                    </asp:Panel>
                    <!-- Panel Added by SHRIGANESH SINGH 01 July 2016 for Editing Catalog details END-->

                    <asp:Panel ID="pnlUserTypeList" runat="server">
                        <br />
                        <br />
                        <div class="button_section text-center">
                            <div class="attributeManagement">

                                <asp:GridView ID="gvUsertypes" runat="server" OnRowCommand="gvUsertypes_OnRowCommand" AutoGenerateColumns="false"
                                    ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid all_customer_inner1 inner1newcls  dragbltop dragbltopextra">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User Type Name">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hfUserTypeID" runat="server" Value='<%# Eval("UserTypeID") %>' />
                                                <%# Eval("UserTypeName") %>
                                                <asp:TextBox ID="txtUserTypeName" runat="server" Text='<%# Eval("UserTypeName") %>' Visible="false"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Vat Display">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVatDisplay" runat="server" Text='<%# Eval("VatDisplay") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vat Rate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVatRate" runat="server" Text='<%# Eval("VatRate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Currency Code">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hfCurrencyID" runat="server" Value='<%# Eval("CurrencyId") %>' />
                                                <asp:Label ID="lblCurrencyCode" runat="server" Text='<%# Eval("CurrencyCode") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Catalogue ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCatalogueID" runat="server" Text='<%# Eval("UserTypeCatalogueID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="Currency Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCurrencyCode" runat="server" Text='<%# Eval("UserCurrenciescsv") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Catalogue ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCatalogueID" runat="server" Text='<%# Eval("UserCataloguecsv") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <div class="action">

                                                    <asp:Button ID="btnEdit" runat="server" CssClass="btn3" Text="Edit User Type Name" CommandName="edits" CommandArgument='<%# Eval("UserTypeID") %>' OnClientClick="return DisplayEditPanel();" />
                                                    &nbsp;
                                                    <!-- Button Added by SHRIGANESH SINGH 02 July 2016 for Displaying the Edit Catalogue Panel -->
                                                    <asp:Button ID="btnEditCatalogueDetails" runat="server" CssClass="btn3" Text="Edit Catalog Details" CommandName="EditCatalogues" CommandArgument='<%# Eval("UserTypeID") %>' />
                                                </div>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                                    <PagerStyle CssClass="paging" />
                                    <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                                    <HeaderStyle CssClass="GridHeading" />
                                    <AlternatingRowStyle CssClass="GridAlternateItem" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>

                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">

        //function DisplayEditPanel() {
        //    debugger;
        //    var EditPanel = document.getElementById("ContentPlaceHolder1_pnlEdit")
        //    EditPanel.style.visibility = "visible";
        //}

    </script>
</asp:Content>

