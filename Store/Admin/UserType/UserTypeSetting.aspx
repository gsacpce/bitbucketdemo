﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_UserType_UserTypeSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">User Type</a></li>
            <li>User Type Setting</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <div class="col-md-5">
                        <h3>USER TYPE SETTING
                        </h3>
                    </div>
                    <div class="col-md-7 text-right">
                        <label id="select_lang" class="sm_select col-md-7 text-right">
                            Select Language :
                        </label>
                        <div class="right_selct_dropdwn relative_data col-md-5 text-right">
                            <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="select-style selectpicker"></asp:DropDownList>
                        </div>
                    </div>
                    <div id="mainDiv" class="attributeManagement">
                        <asp:Repeater ID="rptUserTypeSetting" runat="server">
                            <HeaderTemplate>
                                <table class="all_customer_inner1 inner1newcls  dragbltop dragbltopextra" pagesize="50" allowpaging="true" allowcustompaging="true"
                                    tabindex="0" cellpadding="2" width="100%" datakeynames="ProductSKUId,ValueA1,ValueA2,ValueB1,ValueB2"
                                    autogeneratecolumns="False" bordercolor="#E4E4E4" borderwidth="0">
                                    <tbody>
                                        <tr>
                                            <th align="left" scope="col">User Type ID</th>
                                            <th align="left" scope="col">User Type Name</th>
                                            <th align="left" scope="col">Vat Dispaly</th>
                                            <th align="left" scope="col">Cat ID(GBP)</th>
                                            <th align="left" scope="col">Cat ID(EUR)</th>
                                            <th align="left" scope="col">Cat ID(USD)</th>
                                        </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr style="border-width: 1px; border-style: Solid;">
                                    <td align="center" scope="col">
                                        <input value="1" type="text" /></td>
                                    <td align="left" scope="col">
                                        <input value="Consumer" type="text" /></td>
                                    <td align="left" scope="col">
                                        <input value="true" type="checkbox" />Include Vat</td>
                                    <td align="left" scope="col">
                                        <select></select></td>
                                    <td align="left" scope="col">
                                        <select></select></td>
                                    <td align="left" scope="col">
                                        <select></select></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </table>
                            </FooterTemplate>
                        </asp:Repeater>

                        <table class="all_customer_inner1 inner1newcls  dragbltop dragbltopextra" pagesize="50" allowpaging="true" allowcustompaging="true"
                            tabindex="0" cellpadding="2" width="100%" datakeynames="ProductSKUId,ValueA1,ValueA2,ValueB1,ValueB2"
                            autogeneratecolumns="False" bordercolor="#E4E4E4" borderwidth="0">
                            <tbody>
                                <tr>
                                    <th align="left" scope="col">User Type ID</th>
                                    <th align="left" scope="col">User Type Name</th>
                                    <th align="left" scope="col">Vat Dispaly</th>
                                    <th align="left" scope="col">Cat ID(GBP)</th>
                                    <th align="left" scope="col">Cat ID(EUR)</th>
                                    <th align="left" scope="col">Cat ID(USD)</th>
                                </tr>
                                <tr style="border-width: 1px; border-style: Solid;">
                                    <td align="center" scope="col">
                                        <input value="1" type="text" /></td>
                                    <td align="left" scope="col">
                                        <input value="Consumer" type="text" /></td>
                                    <td align="left" scope="col">
                                        <input value="true" type="checkbox" />Include Vat</td>
                                    <td align="left" scope="col">
                                        <select></select></td>
                                    <td align="left" scope="col">
                                        <select></select></td>
                                    <td align="left" scope="col">
                                        <select></select></td>
                                </tr>
                            </tbody>
                        </table>

                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <strong>User Types Settings</strong></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GrdDynamic" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label id="lblID" runat="server" Text='<%#Eval("UserTypeID") %>'></asp:Label>
                                                             <asp:Label id="Label1" runat="server" Text='<%#Eval("UserTypeName") %>'></asp:Label>
                                                    <asp:Label id="Label2" runat="server" Text='<%#Eval("IsVatDisplay") %>'></asp:Label>
                                                </ItemTemplate>
                                              <%--  <ItemTemplate>
                                           
                                                </ItemTemplate>
                                                <ItemTemplate>
                                                    
                                                </ItemTemplate>--%>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>

                    </div>

                    <div class="buttonPanel container">

                        <ul class="button_section">
                            <li>
                                <button id="btnSaveExit" class="btn">Update</button>

                            </li>
                            <li>
                                <button id="btnSaveExit" class="btn">+</button>

                            </li>


                            <li>
                                <button id="btnCancel" class="btn  gray">Cancel</button>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>



        </section>
    </div>


</asp:Content>

