﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_UserType_AssignCategories" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="<%=AdminHost%>JS/Utilities.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {


            $('.Radios input').click(function () {
                jQuery('.Radios').removeClass('chkBoxmark');
                jQuery(this).parent().addClass('chkBoxmark');
                return false;
            });

            $('.chkBox input').click(function () {
                jQuery(this).parent().toggleClass('chkBoxmark');
                return false;
            });

            $('.CsA h3').click(function () {
                $(this).next().stop().slideToggle();
                $(this).toggleClass('rightA');
            });
            $('#imgLeftToRight').click(function () { })

        });


    </script>
    <script type="text/javascript">
        function fn_MoveStoreDataLeftToRight() {

            MoveOption('<%= lstUnAssignedCategories.ClientID %>', '<%= lstAssignedCategories.ClientID %>');
            StoreIds('<%= lstUnAssignedCategories.ClientID %>', '<%= lstAssignedCategories.ClientID %>', '<%= HIDUnAssignedCategoryIds.ClientID %>', '<%= HIDAssignedCategoryIds.ClientID %>');
            StoreNameNIdPipe('<%= lstUnAssignedCategories.ClientID %>', '<%= lstAssignedCategories.ClientID %>', '<%= HIDUnAssignedCategoryNameIds.ClientID %>', '<%= HIDAssignedCategoryNameIds.ClientID %>');
        }

        function fn_MoveStoreDataRightToLeft() {

            MoveOption('<%= lstAssignedCategories.ClientID %>', '<%= lstUnAssignedCategories.ClientID %>');
            StoreIds('<%= lstUnAssignedCategories.ClientID %>', '<%= lstAssignedCategories.ClientID %>', '<%= HIDUnAssignedCategoryIds.ClientID %>', '<%= HIDAssignedCategoryIds.ClientID %>');
            StoreNameNIdPipe('<%= lstUnAssignedCategories.ClientID %>', '<%= lstAssignedCategories.ClientID %>', '<%= HIDUnAssignedCategoryNameIds.ClientID %>', '<%= HIDAssignedCategoryNameIds.ClientID %>');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- homepage_slideshow start -->
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=Host %>Admin/Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">User Type </a></li>
            <li>Assign Categories To Users Types</li>
        </ul>
    </div>


    <div class="admin_page">
        <section class="container mainContainer padbtom15 assing_prodcut">
            <div class="wrap_container ">
                <div class="content">
                    <div  visible="false" runat="server">

                        <div>
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </div>


                    </div>
                    <div class="wide">
                        <h3 class="mainHead">Assign Categories To User Types </h3>
                    </div>
                    
                    <div class="addsaTicP categories asin_cagry ">
                        <div class="CsA">
                            <div class="DetailsB Editsection">
                                <div class="sectionradio">
                                    <div class="webAddeditsection">
                                        <table class="selectcatogytble" width="100%">
                                            <tr style="height: 80px" runat="server" >
                                                <td align="left" width="20%">
                                                    <span class="textspace">Select User Type:</span>
                                                </td>
                                                <td align="left" style="height: 30px" width="40%">
                                                    <div class="select-style selectpicker ">
                                                        <asp:DropDownList ID="ddlUserTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserTypes_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" id="divShowListBox" runat="server">
                                                    <table>
                                                         <tr>
                                                                        <td style="width:40%;">Unassigned Categories
                                                                        </td>
                                                                        <td style="width: 10%"></td>
                                                                        <td style="width:40%;">Assigned Categories
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div style="overflow: auto">
                                                                                <asp:ListBox ID="lstUnAssignedCategories" Height="250px" Style="min-width: 200px; width: auto;" runat="server"
                                                                                    SelectionMode="Multiple"></asp:ListBox>
                                                                            </div>
                                                                        </td>
                                                                        <td align="center" style="vertical-align: middle; padding: 5px 18px;">
                                                                            <img runat="server" id="imgLeftToRight" src="~/Admin/Images/UI/front-arrow.gif" style="cursor: hand"
                                                                                alt="Click to move selected item from left box to right box" />
                                                                            <br />
                                                                            <br />
                                                                            <br />
                                                                            <br />
                                                                            <img runat="server" id="imgRightToLeft" src="~/Admin/Images/UI/back-arrow.gif" style="cursor: hand"
                                                                                alt="Click to move selected item from right box to left box" />
                                                                        </td>
                                                                        <td>
                                                                            <div style="overflow: auto">
                                                                                <asp:ListBox ID="lstAssignedCategories" runat="server" Height="250px" Style="min-width: 200px; width: auto;"
                                                                                    SelectionMode="Multiple"></asp:ListBox>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="HIDUserTypeSelectedId" runat="server" />
                            <asp:HiddenField ID="HIDAssignedCategoryIds" runat="server" />
                            <asp:HiddenField ID="HIDUnAssignedCategoryIds" runat="server" />
                            <asp:HiddenField ID="HIDAssignedCategoryNameIds" runat="server" />
                            <asp:HiddenField ID="HIDUnAssignedCategoryNameIds" runat="server" />
                          
                        </div>
                    </div>
                    <div class="clearfix"></div>
                  
                    <div class="bottom_part my">
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSaveExit" runat="server" Text="Save" OnClick="btnSubmit_Click"
                                    OnClientClick="javascript:ShowLoader();" CssClass="exit save" />
                            </li>
                          </ul>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- homepage_slideshow end -->
</asp:Content>

