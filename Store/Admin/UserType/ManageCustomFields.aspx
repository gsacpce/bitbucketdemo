﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" EnableViewStateMac="false" AutoEventWireup="true" Inherits="Presentation.Admin_UserType_ManageCustomFields" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 relative_data">
                            <h3>Manage Custom Fields
                            </h3>

                        </div>
                        <div>
                            <div class="form-group clearfix">
                                <asp:Label ID="lblUserTyepName" runat="server" class="control-label col-xs-2" Text="Select User Type Name :"></asp:Label>
                                <div class="col-xs-8">
                                    <asp:DropDownList ID="ddlUserTyepNames" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserTyepNames_OnSelectedIndexChanged" CssClass="form-control" Width="50%"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <asp:Label ID="lblSelectLanguage" runat="server" class="control-label col-xs-2" Text="Select Language :"></asp:Label>
                                <div class="col-xs-8">
                                    <asp:DropDownList ID="ddlLanguages" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguages_OnSelectedIndexChanged" CssClass="form-control" Width="50%"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="wraptrio addborder" id="dvCustomFields" runat="server" clientidmode="static">

                            <asp:GridView ID="gvCustomFields" runat="server" AutoGenerateColumns="false" Width="100%"
                                DataKeyNames="RegistrationFieldsConfigurationId" ShowFooter="true"
                                OnRowEditing="gvCustomFields_RowEditing" OnRowCancelingEdit="gvCustomFields_RowCancelingEdit" OnRowUpdating="gvCustomFields_RowUpdating"
                                OnRowDeleting="gvCustomFields_RowDeleting" OnRowCommand="gvCustomFields_RowCommand" OnRowDataBound="gvCustomFields_RowDataBound" Border="1"
                                Style="border-color: #ccc !important;">
                                <Columns>
                                    <asp:TemplateField HeaderText="Custom Field">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCustomFieldEdit" runat="server" Text='<%# Eval("LabelTitle") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCustomFieldEdit" runat="server" ForeColor="Red" ControlToValidate="txtCustomFieldEdit" ValidationGroup="ValidateEditCustomFields" Display="Dynamic" ErrorMessage="Enter Custom Field" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomField" runat="server" Text='<%# Eval("LabelTitle") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtCustomField" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCustomField" runat="server" ForeColor="Red" ControlToValidate="txtCustomField" ValidationGroup="ValidateAddCustomFields" Display="Dynamic" ErrorMessage="Enter Custom Field" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Field Type">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlFieldTypesEdit" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlFieldTypesEdit_SelectedIndexChanged"></asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFieldType" runat="server" Text='<%# Eval("FieldTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlFieldTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFieldTypes_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvFieldTypes" runat="server" ControlToValidate="ddlFieldTypes" InitialValue="0" ValidationGroup="ValidateAddCustomFields" Display="Dynamic" ErrorMessage="Select Field Type" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Visible">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsVisibleEdit" runat="server" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsVisible" runat="server" Text='<%# Eval("IsVisible") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkIsVisible" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Mandatory">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsMandatoryEdit" runat="server" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsMandatory" runat="server" Text='<%# Eval("IsMandatory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkIsMandatory" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Custom Field Data">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCustomFieldDataEdit" runat="server" TextMode="MultiLine" Visible="false" />
                                            <asp:RequiredFieldValidator ID="rfvCustomFieldDataEdit" runat="server" ForeColor="Red" ControlToValidate="txtCustomFieldDataEdit" ValidationGroup="ValidateEditCustomFields" Display="Dynamic" ErrorMessage="Enter Custom Field Data" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomFieldData" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtCustomFieldData" runat="server" TextMode="MultiLine" Visible="false" />
                                            <asp:RequiredFieldValidator ID="rfvCustomFieldData" runat="server" ForeColor="Red" ControlToValidate="txtCustomFieldData" ValidationGroup="ValidateAddCustomFields" Display="Dynamic" ErrorMessage="Enter Custom Field Data" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <EditItemTemplate>
                                            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CommandName="Update" CssClass="save" ValidationGroup="ValidateEditCustomFields" />
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="save" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="save" />
                                            <asp:Button ID="cmdDelete" OnClientClick="javascript:return confirm('Are you sure, you want to delete this?')" runat="server" Text="Delete" CommandName="Delete"
                                                CssClass="save" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnAdd" CssClass="save" runat="server" CommandName="AddNew" Text="Add Custom Fields" ValidationGroup="ValidateAddCustomFields" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

