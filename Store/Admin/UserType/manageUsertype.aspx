﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Admin_UserType_manageUsertype" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">User Type </a></li>
            <li>Manage User Type</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <!-- Panel for Adding User Type Name and Currency -->
                    <asp:Panel ID="pnlAdd" runat="server" Visible="true">
                        <h3><asp:Label ID="lblHeading" runat="server" CssClass="mainHead" Text="Add User Type"></asp:Label></h3>
                        <div class="row">
                            <div class="col-md-3">
                                <label id="select_lang" class="sm_select">
                                    Select Language :
                                </label>
                            </div>
                            <div class="right_selct_dropdwn relative_data col-md-3">
                                <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="select-style selectpicker" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label ID="lbluserTypeName" runat="server" Text="User Type Name:"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtuserTypeName" runat="server" CssClass="inputTxt"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rqvtxtuserTypeName" runat="server" ControlToValidate="txtuserTypeName" ValidationGroup="Add" ErrorMessage="Please Enter User Type name"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                        <div id="divaddFields" runat="server" visible="true">
                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label ID="lblCurrencies" runat="server" Text="Select Currencies : "></asp:Label>
                            </div>
                            <div class="right_selct_dropdwn relative_data col-md-3">
                                <asp:DropDownList ID="ddlCurrencies" runat="server" AutoPostBack="true" CssClass="select-style selectpicker" OnSelectedIndexChanged="ddlCurrencies_OnSelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvddlCurrencies" runat="server" Text="*" ControlToValidate="ddlCurrencies" ValidationGroup="Add" ErrorMessage="Please Select Currency for User Type" InitialValue="Please select"></asp:RequiredFieldValidator>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label ID="lblCatalogues" runat="server" Text="Select Catalogue : "></asp:Label>
                            </div>
                            <div class="right_selct_dropdwn relative_data col-md-3">
                                <asp:DropDownList ID="ddlCatalogues" runat="server" CssClass="select-style selectpicker"></asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvddlCatalogues" runat="server" Text="*" ControlToValidate="ddlCatalogues" ValidationGroup="Add" ErrorMessage="Please select catalogue for the User Type" InitialValue="Please select"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label ID="lblVatDisplay" runat="server" Text="Vat Display :"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:CheckBox ID="chkVatDisplay" runat="server" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label ID="lblVatRate" runat="server" Text="Vat Percentage : "></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtVatRate" runat="server" CssClass="inputTxt"></asp:TextBox>
                            </div>
                        </div>
                            </div>
                        <asp:HiddenField ID="hdnfUserTypeID" runat="server" />
                        <asp:HiddenField ID="hdnfCurrencyID" runat="server" />
                        <asp:HiddenField ID="hdnfSelectedCatalogueid" runat="server" />
                        <div class="row new_button_pannel button_section">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn BASYSCancel  gray" Text="Cancel" OnClick="btnCancel_OnClick" />
                        </div>
                    </asp:Panel>

                    <!-- Panel for Edit User Type Button -->
                    <%--  <asp:Panel ID="pnlViewEdit" runat="server">
                <div class="button_section">
                    <asp:Button ID="btnDisplayEditPanel" runat="server" CssClass="btn" Text="Edit User Types" OnClick="btnDisplayEditPanel_OnClick" />
                </div>
            </asp:Panel>--%>

                    <!-- Panel for Editing User Types -->
                    <%--<asp:Panel ID="pnlEdit" runat="server" Visible="false">
                <asp:HiddenField ID="hdnfUserTypeID" runat="server" />
                <asp:HiddenField ID="hdnfCurrencyID" runat="server" />
                <asp:Label ID="lblEditMessaage" runat="server" Text="Below are the editable details for the given User Type"></asp:Label>
                <br />
                <br />
                <asp:DropDownList ID="ddlEditLanguages" runat="server"></asp:DropDownList>
                <br />
                <br />
                <asp:Label ID="lblUserTypeNameEdit" runat="server" Text="User Type Name"></asp:Label>
                <asp:TextBox ID="txtUserTypeNameEdit" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtUserTypeNameEdit" runat="server" ControlToValidate="txtUserTypeNameEdit" ErrorMessage="Please Enter User Type Name" ValidationGroup="Edit"></asp:RequiredFieldValidator>
                <br />
                <br />
                <asp:Label ID="lblVatDisplayEdit" runat="server" Text="Is Vat Displayed"></asp:Label>
                <asp:CheckBox ID="chkVatDisplayEdit" runat="server" />
                <br />
                <br />
                <div id="dvVatRateEdit" runat="server" visible="false">
                    <asp:Label ID="lblVatRateEdit" runat="server" Text="Vat Rate"></asp:Label>
                    <asp:TextBox ID="txtVatRateEdit" runat="server"></asp:TextBox>
                </div>
                <br />
                <br />
                <asp:Label ID="lblCurrencyCodeEdit" runat="server" Text="Currency Code : "></asp:Label>
                <asp:Label ID="lblCurrencyCodeValueEdit" runat="server"></asp:Label>
                <br />
                <br />
                <asp:Label ID="lblcatalogueIDEdit" runat="server" Text="Catalogue ID : "></asp:Label>
                <asp:Label ID="lblcatalogueIDValueEdit" runat="server"></asp:Label>
                <br />
                <br />
                <div class="col-md-12 col-md-offset-6  button_section">
                   
                    <asp:Button ID="btnEditCancel" runat="server" Text="Cancel" OnClick="btnEditCancel_OnClick" CssClass="btn gray" />
                </div>
            </asp:Panel>--%>

                    <asp:Panel ID="pnlUserTypeList" runat="server">
                        <br />
                        <br />
                        <div class="button_section text-center">
                            <div class="attributeManagement">
                                <asp:GridView ID="gvUsertypes" runat="server" OnRowCommand="gvUsertypes_OnRowCommand" AutoGenerateColumns="false"
                                    ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid all_customer_inner1 inner1newcls  dragbltop dragbltopextra">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User Type Name">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hfUserTypeID" runat="server" Value='<%# Eval("UserTypeID") %>' />
                                                <%# Eval("UserTypeName") %>
                                                <asp:TextBox ID="txtUserTypeName" runat="server" Text='<%# Eval("UserTypeName") %>' Visible="false"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vat Display" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVatDisplay" runat="server" Text='<%# Eval("VatDisplay") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Currency Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCurrencyCode" runat="server" Text='<%# Eval("UserCurrenciescsv") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Catalogue ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCatalogueID" runat="server" Text='<%# Eval("UserCataloguecsv") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField HeaderText="Vat Rate" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVatRate" runat="server" Text='<%# Eval("VatRate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Currency Code">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hfCurrencyID" runat="server" Value='<%# Eval("CurrencyId") %>' />
                                                <asp:Label ID="lblCurrencyCode" runat="server" Text='<%# Eval("CurrencyCode") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Catalogue ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCatalogueID" runat="server" Text='<%# Eval("UserTypeCatalogueID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <asp:Button ID="btnEdit" runat="server" CssClass="btn3" Text="Edit" CommandName="edits" CommandArgument='<%# Eval("UserTypeID") %>' OnClientClick="return DisplayEditPanel();" />
                                                </div>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                                    <PagerStyle CssClass="paging" />
                                    <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                                    <HeaderStyle CssClass="GridHeading" />
                                    <AlternatingRowStyle CssClass="GridAlternateItem" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>

                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">

        //function DisplayEditPanel() {
        //    debugger;
        //    var EditPanel = document.getElementById("ContentPlaceHolder1_pnlEdit")
        //    EditPanel.style.visibility = "visible";
        //}

    </script>
</asp:Content>

