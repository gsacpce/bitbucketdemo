﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_UserType_PaymentTypeManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li>User Types</li>
            <li>User Type Payment Mapping</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer role_management role_listing manage_users">
            <div class="wrap_container ">
                <div class="content">
                    <section class="mainContainer">
                        <div class="row">
                            <div class="col-md-12 relative_data">
                                <div class="col-md-7">
                                    <h3>
                                        <asp:Label ID="lblHeading" runat="server">User Type Payment Mapping</asp:Label>
                                    </h3>
                                    <p>Assign the Payment option to particular User </p>
                                </div>

                                <div class="col-md-5  text-right">

                                    <label class="col-md-5 text-right sm_select" id="select_lang">
                                        Select Language :
                                    </label>
                                    <div style="float: right" id="divLang" runat="server" class="col-md-7">
                                        <div class="select-style  selectpicker languageDropdown">
                                            <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                         <div class="clearfix">
                            </div>
                        <div class="col-md-12">
                            <label class="col-md-5 text-right sm_select">
                                Select User Types.
                            </label>
                            <div style="" id="div1" runat="server" class="col-md-7">

                                <div class="select-style  selectpicker">
                                    <asp:HiddenField id="hdnselectedUserTypeId" runat="server" />
                                    <asp:DropDownList ID="ddlUserTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserTypes_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="allcosutomer">
                            </div>
                             
                        </div>
                        <div class="clearfix">
                            </div>
                        <div class="customer" style="margin: 0px !important;">
                            <span class="allcosutomer">
                            <%--<asp:Literal ID="ltrNoOfRecords" runat="server" Text=""></asp:Literal>--%>

                            </span>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="pad_top">
                            <section class=" all_customer dragblnextBlock manage_pro">
                                <asp:GridView runat="server" ID="gvList" TabIndex="0" CellPadding="3" Width="100%" PageSize="30"
                                    DataKeyNames="PaymentTypeID" AutoGenerateColumns="False" OnPageIndexChanging="gvList_PageIndexChanging"
                                    CssClass="all_customer_inner allcutomerEtracls" EmptyDataText="No Group Exits."
                                    AllowPaging="true">
                                    <HeaderStyle Font-Bold="true" />
                                    <RowStyle HorizontalAlign="Center" Height="30px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="PaymentTypeRef" SortExpression="GroupName">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "PaymentTypeLabelName")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Display Name" SortExpression="PaymentTypeRef">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "PaymentTypeRef")%>
                                                <asp:TextBox ID="txtDisplayName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PaymentTypeRef")%>' visible="false"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Show On WebSite">
                                            <ItemTemplate>
                                                <asp:Label ID="lblShowonWebsite" runat="server" Text='<%# Eval("IsShowOnWebsite").ToString().ToLower() == "1" ? "Yes" : "No" %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="5%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active / Inactive">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Eval("IsActive").ToString().ToLower() == "true" ? true : false %>'
                                                   ></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="5%" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </section>
                            <div class="col-md-12 col-md-offset-6  button_section">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn" Text="Submit" OnClick="btnSubmit_Click"/>
                                </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>

                </div>
            </div>
        </section>
    </div>
</asp:Content>

