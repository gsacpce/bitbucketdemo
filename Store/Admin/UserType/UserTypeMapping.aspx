﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_UserType_UserTypeMapping" %>

<script runat="server">

    
    
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function ShowFileUploader() {
            $('#ModalFileUpload').removeClass("modal fade");
            $('#ModalFileUpload').addClass("modal show");
        }

        function CloseFileUploader() {
            $('#ModalFileUpload').removeClass("modal show");
            $('#ModalFileUpload').addClass("modal fade");
        }

    </script>

    <script type="text/javascript">

        function ShowEditModal() {
            $('#ModalEditCustomMappingField').removeClass("modal fade");
            $('#ModalEditCustomMappingField').addClass("modal show in");
        }

        function CloseEditModal() {
            $('#ModalEditCustomMappingField').removeClass("modal show in");
            $('#ModalEditCustomMappingField').addClass("modal fade");
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">User Types</a></li>
            <li>User Type Mapping </li>
        </ul>

    </div>
    <div class="admin_page">
        <div class="container mainContainer ">
            <div class="wrap_container gift_cupon">
                <div class="content">
                     <h3 class="mainHead">User Type Mapping</h3>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-lg-4 col-lg-offset-4" >
                        <asp:RadioButtonList ID="rdList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rdList_SelectedIndexChanged"></asp:RadioButtonList>
                    </div>
                </div>
                <asp:Panel ID="pnlUserSelection" runat="server">
                    <div class="button_section"> 
                        <asp:Button ID="btnUserSelectionSave" CssClass="btn BASYSSave exit save" runat="server" Text="Set User Type Rule" OnClick="btnUserSelectionSave_Click" />
                   
                        <asp:Button ID="btnUserSequenceSave" CssClass="btn BASYSSave exit save" runat="server" Text="Save UsertType Sequence" OnClick="btnUserSequenceSave_Click" Visible="false" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlCountry" runat="server">
                    <div class="content">
                        
                         <div class="button_section text-center">
                            <asp:Repeater ID="rptCountry" runat="server" OnItemDataBound="rptCountry_ItemDataBound">
                            <HeaderTemplate>
                                <table class="all_customer_inner1 inner1newcls  dragbltop dragbltopextra">
                                    <tr>
                                        <th>CountryName
                                        </th>
                                        <th>CountryCode
                                        </th>
                                        <th>IsActive
                                        </th>
                                        <th>User Type
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="hdfCountryID" runat="server" Value='<%#Eval("CountryId") %>' />
                                        <asp:Label ID="lblCountryName" runat="server" Text='<%#Eval("CountryName") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCountryCode" runat="server" Text='<%#Eval("CountryCode") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblIsActive" runat="server" Text='<%#Eval("IsActive") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCountryUserType" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                         </div>
                        <div class="button_section"> 
                            <asp:Button ID="btnCountrySave" runat="server" CssClass="btn BASYSSave exit save" Text="Save" OnClick="btnCountrySave_Click" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlEmail" runat="server">
                    <div class="content ">
                        <div class="customer" style="margin: 0px !important;">
                            <span class="allcosutomer">All Records -
                                <asp:Literal ID="ltrAllRecords" runat="server" Text=""></asp:Literal>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <asp:Panel ID="pnlList" runat="server">
                                <section class=" all_customer dragblnextBlock manage_pro">
                                    <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="30" AllowCustomPaging="false" DataKeyNames="UserTypeId,UserTypeName" 
                                        ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid all_customer_inner1 inner1newcls dragbltop dragbltopextra" OnPageIndexChanging="gvList_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Sequence No.">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtSequenceNo" Text='<%# Eval("UserTypeSequence") %>' runat="server"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnUserTypeID" runat="server" Value='<%# Eval("UserTypeID") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="User Type">
                                                <ItemTemplate>
                                                    <%#DataBinder.Eval(Container.DataItem, "UserTypeName")%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="WhiteList">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkIsWhiteListed" runat="server" Checked='<%# Eval("IsWhiteList").ToString().ToLower() == "true" ? true : false %>' AutoPostBack="true" OnCheckedChanged="chkIsWhiteListed_CheckedChanged" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Upload Files">
                                                <ItemTemplate>
                                                    <asp:FileUpload ID="flImport" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Import">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnImport" CssClass="btn3" runat="server" Text="Import" CommandArgument='<%# Eval("UserTypeID") %>' OnClick="btnImport_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Export">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnExport" CssClass="btn3" runat="server" Text="Export" CommandArgument='<%# Eval("UserTypeID") %>' OnClick="btnExport_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="paging" />
                                        <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                                        <HeaderStyle CssClass="GridHeading" />
                                        <AlternatingRowStyle CssClass="GridAlternateItem" />
                                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom"/>
                                    </asp:GridView>
                                </section>

                            </asp:Panel>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlCustomMapping" runat="server">
                    <div class="content ">
                        <div class="button_section text-center">
                            <asp:Repeater ID="rptCustomMapping" runat="server" OnItemCommand="rptCustomMapping_ItemCommand" DataSourceID="">
                            <HeaderTemplate>
                                <table class="all_customer_inner1 inner1newcls  dragbltop dragbltopextra">
                                    <tr>
                                        <th>User Type Name
                                        </th>
                                        <th>Custom Field Name
                                        </th>
                                        <th>Choose File
                                        </th>
                                        <th>Import / Export
                                        </th>
                                        <%--<th>
                                            Validation Behavior
                                        </th>--%>
                                        <!-- Code Region Added by SHRIGANESH SINGH 06 July 2016 START -->
                                        <th>
                                            Edit
                                        </th>
                                        <!-- Code Region Added by SHRIGANESH SINGH 06 July 2016 END -->
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="hdfCustommapUserTypeID" runat="server" Value='<%#Eval("UserTypeID") %>' />
                                        <asp:Label ID="lblUserType" runat="server" Text='<%#Eval("UserTypeName") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hdfCustomMapRegistrationFieldsConfigurationId" runat="server" Value='<%#Eval("RegistrationFieldsConfigurationId") %>' />
                                        <asp:Label ID="lblCustomField" runat="server" Text='<%#Eval("LabelTitle") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="flCustomImport" runat="server" />    
                                    </td>
                                    <td>
                                       <%-- <asp:HyperLink ID="hplImportEdit" runat="server" data-toggle="modal" CommandName="Import" CommandArgument='<%#Eval("RegistrationFieldsConfigurationId") %>' CssClass="save" data-target="#ModalFileUpload" Visible="true" Text="Import"></asp:HyperLink>--%>
                                        <asp:Button ID="btnCustomMappingImport" CssClass="btn3" runat="server" Text="Import" CommandName="Import"  />
                                        <asp:Button ID="btnCustomMappingExport" CssClass="btn3" runat="server" Text="Export" CommandName="Export" CommandArgument='<%#Eval("RegistrationFieldsConfigurationId") %>' />
                                    </td>
                                    
                                        <%--<asp:RadioButtonList ID="rdblValidation" runat="server" >
                                            <asp:ListItem>Register as Default</asp:ListItem>
                                            <asp:ListItem>Decline Registration</asp:ListItem>
                                        </asp:RadioButtonList>--%>
                                        <%--<asp:RadioButton ID="rdbRegAsDef" runat="server" GroupName="ValidationRule" Text="Register As Default" Checked='<%#Eval("RegisterAsDefault").ToString().ToLower() == "true" ? true : false %>' />
                                        <br />
                                        <asp:RadioButton ID="rdbDecReg" runat="server" GroupName="ValidationRule" Text="Decline Register" Checked='<%#Eval("DeclineRegister").ToString().ToLower() == "true" ? true : false %>' />--%>
                                    
                                    <!-- Code Region Added by SHRIGANESH SINGH 06 July 2016 START -->
                                    <td>
                                        <asp:LinkButton ID="btnEditCustomFieldName" CssClass="btn3" data-toggle="modal" data-target="#ModalEditCustomMappingField" runat="server" Text="Edit Custom Field" CommandName="Edits" />

                                    </td>
                                    <!-- Code Region Added by SHRIGANESH SINGH 06 July 2016 END -->
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalFileUploadCustomMapping" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content customModal">
                <div class="customModal modal-header customPanel">
                    <button type="button" id="btnGCModalClose" onclick="CloseFileUploader()" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title pageSubSubTitle" id="myModalLabel1">UPLOAD File</h4>
                </div>
                <div class="modal-body clearfix">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblFileTypeMessage" runat="server" Text="Please Select an Excel/ CSV file !! "></asp:Label>
                            </td>
                        </tr>
                        <tr>
                             <td>
                                <asp:FileUpload ID="flupldCustomFieldsExcel" CssClass="fileup" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Code Region Added by SHRIGANESH SINGH 06 July 2016 START -->
    <div class="modal fade" id="ModalEditCustomMappingField" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content customModal">
                <div class="customModal modal-header customPanel">
                    <button type="button" id="btnEditCustomFieldModalClose" onclick="CloseEditModal()" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title pageSubSubTitle" id="myModalLabel2">Edit Custom Field Name</h4>
                </div>
                <div class="modal-body clearfix paddingLeft">

                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblSelectLanguage" CssClass="boxWidth lableAlign" runat="server" Text="Select Language : "></asp:Label>
                                <asp:DropDownList ID="ddlLanguage"  CssClass="boxWidth" runat="server"></asp:DropDownList>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" CssClass="boxWidth" Text="Custom Field Name : "></asp:Label>
                                <asp:TextBox ID="txtCustomFieldName" CssClass="boxWidth" runat="server"></asp:TextBox>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4><asp:Label ID="lblValidationBehavior" CssClass="modal-title pageSubSubTitle" runat="server" Text="Validation Behavior"></asp:Label></h4>
                                
                                <asp:RadioButton ID="rdbRegAsDef" runat="server" GroupName="ValidationRule" Text="Register As Default" />
                                        <br />
                                        <asp:RadioButton ID="rdbDecReg" runat="server" GroupName="ValidationRule" Text="Decline Register" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSaveCustomFieldName" CssClass="btn BASYSSave exit save" runat="server" Text="Submit" OnClick="btnSaveCustomFieldName_Click" />
                                <asp:Button ID="btnCancelCustomFieldName" CssClass="btn BASYSSave exit save" runat="server" Text="Cancel" OnClick="btnCancelCustomFieldName_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Code Region Added by SHRIGANESH SINGH 06 July 2016 END -->

</asp:Content>

