﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_EditBreadCrumbManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Breadcrumb Management</li>
        </ul>
    </div>
    <div class="admin_page ">
        <div class="container ">
            <div id="divMessage" visible="false" runat="server">
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>
                        <asp:Literal ID="ltrMessage" runat="server" /></strong>
                </div>
            </div>
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>Bread Crumb</h3>
                            <p>Edit bread crumb information.</p>
                        </div>
                    </div>
                    <div class="slideshow select_data ">
                        <ul>
                            <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9 slide">
                                    <p>
                                        <br />
                                        Enter bread crumb name. This name will be displayed to users.                                      
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <h6>Bread Crumb Name:</h6>
                                </div>
                                <div class="col-md-9 slide">
                                    <div class="size01">
                                        <asp:TextBox ID="txtBreadCrumbName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvTxtBreadCrumbName" runat="server" ControlToValidate="txtBreadCrumbName" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9 slide">
                                    <p>
                                        <br />
                                        Enter bread crumb link.                                      
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <h6>Link:</h6>
                                </div>
                                <div class="col-md-9">
                                    <br />
                                    <asp:TextBox ID="txtBreadCrumbLink" ClientIDMode="Static" runat="server" CssClass="size01"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvTxtBreadCrumbLink" runat="server" ControlToValidate="txtBreadCrumbLink" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="buttonPanel container">
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="btnSave" ValidationGroup="formValid" ClientIDMode="Static" runat="server" CssClass="btn" Text="Save" AccessKey="S" OnClick="Btn_Click" OnClientClick="return Validate()" CommandArgument="Save" />
                        </li>
                        <li>
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn gray" Text="Cancel" AccessKey="C" OnClick="Btn_Click" OnClientClick="javascript:window.document.form.reset();return false;" CommandArgument="Cancel" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>