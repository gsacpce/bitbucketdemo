﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_BreadCrumbManagement_BreadCrumbListing" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;
            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;
            });
        });
    </script>
    <style>
        .alignment td {
            border: 0 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Breadcrumb Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-5">
                            <h3 class="mainHead">Breadcrumb Management</h3>
                            <span class="sectionText">Manage breadcrumb for pages.</span>
                        </div>
                        <div style="float: right" id="divLang" runat="server" visible="true" class="col-md-7  text-right">
                            <label class="sm_select col-md-7 text-right">Select Language :</label>
                            <div class="col-md-5">
                                <div class="select-style selectpicker">
                                    <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <!-- Website tab start -->
                        <div class="clearfix"></div>
                        <!--mainOutContainer start-->
                        <div class="midleDrgblecontin col-md-12">
                            <ul class="dragblnextBlock">
                                <li>
                                    <asp:GridView ID="gvBreadCrumb" runat="server" AutoGenerateColumns="false" Width="100%"
                                        class="all_customer_inner dragbltop  " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                        OnPageIndexChanging="gvBreadCrumb_PageIndexChanging" EmptyDataText="No record found."
                                        OnRowDataBound="gvBreadCrumb_OnRowDataBound" OnRowDeleting="gvBreadCrumb_RowDeleting" AllowSorting="false" PageSize="20">
                                        <Columns>
                                            <%--   <asp:BoundField HeaderText="Bread Crumb" HeaderStyle-CssClass="bg" DataField="breadcrumb"
                                            ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                            <ItemStyle />
                                        </asp:BoundField>--%>
                                            <asp:TemplateField HeaderText="Bread Crumb">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="linkParent" runat="server"></asp:HyperLink>
                                                    <asp:HyperLink ID="linkChild" runat="server"></asp:HyperLink>
                                                    <asp:Literal ID="ltlGrandChild" runat="server"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                                SortExpression="Status" HeaderStyle-Width="7%">
                                                <ItemTemplate>
                                                    <div class="action">
                                                        <a class="icon" href="#"></a>
                                                        <div class="action_hover">
                                                            <span></span>
                                                            <div class="view_order other_option">
                                                                <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" ID="lnkbtnUpdate"
                                                                    CommandName="<%# DBAction.Update.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BreadCrumbId") %>' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerSettings Mode="NumericFirstLast" Position="Top" FirstPageText="First" PageButtonCount="6"
                                            LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                        <PagerStyle CssClass="paging" />
                                    </asp:GridView>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>