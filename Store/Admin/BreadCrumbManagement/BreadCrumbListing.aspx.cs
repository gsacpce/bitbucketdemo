﻿using PWGlobalEcomm.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public class Admin_BreadCrumbManagement_BreadCrumbListing : System.Web.UI.Page
    {
        
        
        protected global::System.Web.UI.WebControls.GridView gvBreadCrumb;

        protected global::System.Web.UI.WebControls.LinkButton lnkbtnUpdate;
        protected global::System.Web.UI.WebControls.LinkButton lnkbtnDelete;
        protected global::System.Web.UI.WebControls.DropDownList ddlLanguage;
        public string Host = GlobalFunctions.GetVirtualPath();
        public string Seperator = " >> ";

        List<BreadCrumbManagementBE> lstBreadCrumb = new List<BreadCrumbManagementBE>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateLanguageDropDownList();
                if (Session["EditBreadCrumb"] != null)
                {
                    BreadCrumbManagementBE EditBreadCrumb = Session["EditBreadCrumb"] as BreadCrumbManagementBE;

                    ddlLanguage.SelectedValue = Convert.ToString(EditBreadCrumb.LanguageId);
                    GetAllBreadCrumbDetails(Convert.ToInt16(ddlLanguage.SelectedValue));

                }
                else {
                    
                    //GetAllBreadCrumbDetails(GlobalFunctions.GetLanguageId());
                    GetAllBreadCrumbDetails(1);
                }

                
                
            }
            
        }
        private void PopulateLanguageDropDownList()
        {
            List<LanguageBE> languages = new List<LanguageBE>();
            languages = LanguageBL.GetAllLanguageDetails(true);

            if (languages != null)
            {
                if (languages.Count() > 0)
                {
                    ddlLanguage.DataSource = languages;
                    ddlLanguage.DataTextField = "LanguageName";
                    ddlLanguage.DataValueField = "LanguageId";
                    ddlLanguage.DataBind();
                }
            }

        }
        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            GetAllBreadCrumbDetails(Convert.ToInt16(ddlLanguage.SelectedValue));

        }
        private void GetAllBreadCrumbDetails(Int16 LanguageId)
        {
            lstBreadCrumb = BreadCrumbManagementBL.GetAllBreadCrumbDetails(LanguageId);
            Session["BreadCrumb"] = lstBreadCrumb;
            gvBreadCrumb.DataSource = lstBreadCrumb;
            gvBreadCrumb.DataBind();
        }

        protected void Btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            
            LinkButton lnkBtn    = sender as LinkButton;

            if (btn != null)
            switch (btn.CommandName.ToLower() )
            {
                
                case "cancel":
                    {
                        break;
                    }
                default:
                    break;
            }
            if (lnkBtn != null)
            switch (lnkBtn.CommandName.ToLower() )
            {
                case "update":
                    {
                        BreadCrumbManagementBE EditBreadCrumb = new BreadCrumbManagementBE();
                        
                        //(Session["BreadCrumb"] as List<BreadCrumbManagementBE>).Find(x => (x as BreadCrumbManagementBE).BreadCrumbId == Convert.ToInt16(lnkBtn.CommandArgument));
                        if (Session["BreadCrumb"] != null)
                        {
                            foreach (var item in (Session["BreadCrumb"] as List<BreadCrumbManagementBE>))
                            {
                                if (((BreadCrumbManagementBE)item).BreadCrumbId == Convert.ToInt16(lnkBtn.CommandArgument))
                                {
                                    EditBreadCrumb = ((BreadCrumbManagementBE)item);
                                }
                            }
                            Session["EditBreadCrumb"] = EditBreadCrumb;
                            Session["EditOperation"] = lnkBtn.CommandName;
                            Response.Redirect("EditBreadCrumbManagement.aspx");
                        }
                        
                        break;
                    }
                
                
                default:
                    break;
            }
        }

        
        protected void gvBreadCrumb_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
            int NewIndex = e.NewPageIndex;
            gvBreadCrumb.PageIndex = NewIndex;
            gvBreadCrumb.DataSource = BreadCrumbManagementBL.GetAllBreadCrumbDetails(Convert.ToInt16(ddlLanguage.SelectedValue));
            gvBreadCrumb.DataBind();
        }
        protected void gvBreadCrumb_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
          //  Response.Write("Delete handled");
        }
        protected void gvBreadCrumb_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink linkParent = (HyperLink) e.Row.FindControl("linkParent");
                HyperLink linkChild = (HyperLink)e.Row.FindControl("linkChild");
                Literal   ltlGrandChild = (Literal)e.Row.FindControl("ltlGrandChild");

                if (lstBreadCrumb != null)
                {
                    if (lstBreadCrumb.Count > 0)
                    {
                        BreadCrumbManagementBE BreadCrumObj = new BreadCrumbManagementBE();
                        Int16 BreadCrumbId = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "BreadCrumbId"));
                        // for checking its a parent bread crumb
                        var lstParentBreadCrumb = lstBreadCrumb.Where(b => b.ParentBreadCrumbId == 0 ).ToList();
                        BreadCrumObj = lstParentBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumbId).FirstOrDefault();
                        
                        if (BreadCrumObj != null)
                        {
                            linkParent.Text = BreadCrumObj.BreadCrumbName;
                            linkParent.NavigateUrl = BreadCrumObj.Link;
                        }
                        else
                        {
                            

                            // child value
                            BreadCrumObj = lstBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumbId).FirstOrDefault();
                            linkChild.Text = BreadCrumObj.BreadCrumbName;
                            linkParent.NavigateUrl = Host + BreadCrumObj.Link; 

                            
                            //parent  value 
                            if (lstParentBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId) != null)
                            {
                                if (lstParentBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId).Count() > 0)
                                {
                                    linkParent.Text = lstParentBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId).FirstOrDefault().BreadCrumbName + " >> ";

                                    
                                    //to check its not having the grand child value
                                    if (BreadCrumObj == null)
                                    {
                                       

                                        ltlGrandChild.Text = BreadCrumObj.BreadCrumbName;
                                        linkChild.Text = linkParent.Text;
                                        linkChild.NavigateUrl = Host + BreadCrumObj.Link;
                                        linkParent.Text = "";

                                    }
                                   
                                }
                                else
                                {
                                    linkChild.Text = BreadCrumObj.ParentCrumbName + " >> ";
                                    linkChild.NavigateUrl = Host + BreadCrumObj.Link; 
                                    ltlGrandChild.Text = BreadCrumObj.BreadCrumbName;

                                    BreadCrumObj = lstBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId).FirstOrDefault();
                                    linkParent.Text = lstParentBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId).FirstOrDefault().BreadCrumbName + " >> ";
                                    linkParent.NavigateUrl = Host + lstParentBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId).FirstOrDefault().Link; 

                                }
                            }
                            else
                            {
                                linkChild.Text = BreadCrumObj.ParentCrumbName + " >> ";
                                linkChild.NavigateUrl = Host + BreadCrumObj.Link; ;

                                ltlGrandChild.Text = BreadCrumObj.BreadCrumbName;

                                BreadCrumObj = lstBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId).FirstOrDefault();
                                linkParent.Text = lstParentBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId).FirstOrDefault().BreadCrumbName + " >> ";
                                linkParent.NavigateUrl = Host + lstParentBreadCrumb.Where(b => b.BreadCrumbId == BreadCrumObj.ParentBreadCrumbId).FirstOrDefault().Link; 
                            }
                            
                        }
                    
                    }
                }
            }
        }
       
    }
}
