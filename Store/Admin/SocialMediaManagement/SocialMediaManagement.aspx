﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_SocialMediaManagement_SocialMediaManagement" %>








<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;


            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;

            });
        });


    </script>

    <style>
        .media_mngmen {
            table-layout: fixed;
            width: 100%;
            position: relative;
        }

            .media_mngmen th {
                text-align: center;
            }

            .media_mngmen td {
                text-align: center;
            }

        .buttonscls {
            background: none repeat scroll 0 0 #7c7c7c;
            color: #fff;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="admin_page ">

        <div class="container ">
            <div class="wrap_container ">

                <div class="content">

                    <div>
                        <div class="row">
                            <div class="col-md-9">
                                <h3>Social Media Management</h3>

                            </div>

                        </div>

                        <span class="sectionText">Drag & Drop to set sequence</span>
                        <table class="media_mngmen">
                            <tr>
                                <th class="first">Image
                                </th>
                                <th class="second">Social Media Name
                                </th>
                                <th class="third">Social Media Url
                                </th>
                                <th width="150" class="fourth">Active
                                </th>
                                <th width="150" class="fifth">Action
                                </th>
                            </tr>
                        </table>
                        <ul id="sortable" class="sortable social_m_mngmen">

                            <asp:Repeater ID="rptSocialMedia" runat="server" OnItemDataBound="rptSocialMedia_ItemDataBound">


                                <ItemTemplate>

                                    <li class="ui-state-default" id="liPC" runat="server">
                                        <table class="media_mngmen all_customer_inner">
                                            <tr>

                                                <td class="first">
                                                    <asp:Image ID="ImageSocialMedia" Style="height: 30px; width: 30px;" runat="server" />

                                                </td>
                                                <td class="second">
                                                    <%# DataBinder.Eval(Container.DataItem, "SocialMediaPlatform") %>

                                                </td>
                                                <td class="third">
                                                    <%# DataBinder.Eval(Container.DataItem, "SocialMediaUrl") %>

                                                </td>
                                                <td class="fourth">
                                                    <%# DataBinder.Eval(Container.DataItem, "IsActive") %>
                                        
                                            

                                                </td>
                                                <td class="fifth">
                                                    <ul class="dragblnextBlock new_side_show_mangment">
                                                        <li>
                                                            <div class="action">
                                                                <a class="icon" href="#"></a>
                                                                <div class="action_hover">
                                                                    <span></span>
                                                                    <div class="view_order other_option">

                                                                        <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" ID="lnkbtnUpdate"
                                                                            CommandName="<%# DBAction.Update.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SocialMediaId") %>' />

                                                                        <asp:LinkButton Text="Disable" runat="server" OnCommand="Btn_Click" ID="lnkDelete" OnClientClick="return confirm('Are you sure Disable item?')"
                                                                            CommandName="<%# DBAction.Delete.ToString() %>" CommandArgument='<%#Eval("SocialMediaId")+","+ Eval("SocialMediaPlatform")%>' />
                                                                           <%-- CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SocialMediaId") %>'--%> 

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>

                        </ul>

                    </div>


                    <p>
                        <asp:Label ID="lblHeading" runat="server" Text="Edit social media"></asp:Label>
                    </p>


                    <div class="SocialMedia select_data new_social">
                        <ul>

                            <li>

                                <div class="col-md-3" style="display:none">

                                    <h6>Social Media Name <span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9" style="display:none">

                                    <asp:TextBox ID="txtSocialMediaName" ClientIDMode="Static" runat="server" MaxLength="50" CssClass="size01"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="formValid"  ID="rfvTxtSocialMediaName" runat="server" ControlToValidate="txtSocialMediaName" ErrorMessage="*" ></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressionValidator ID="regularExpMediaName" runat="server" ValidationGroup="formValid" ValidationExpression="^[+a-zA-Z][a-zA-Z0-9+]*$" ErrorMessage="Invalid Characters" ControlToValidate="txtSocialMediaName"></asp:RegularExpressionValidator>--%>
                                </div>
                            </li>
                            <li>


                                <div class="col-md-3" style="display:none">
                                    <h6>Social Media URL <span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9" style="display:none">
                                    <asp:TextBox ID="txtURL" ClientIDMode="Static" runat="server" placeholder="http://www.example.com" MaxLength="600" CssClass="size01"></asp:TextBox>
                                    <asp:RequiredFieldValidator  ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtURL" ValidationGroup="formValid"  ErrorMessage="*" ></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="formValid" ValidationExpression="^[A-Za-z0-9-/:$?=&.]*$" ErrorMessage="Invalid Characters" ControlToValidate="txtURL"></asp:RegularExpressionValidator>--%>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="formValid" ValidationExpression="/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i" ErrorMessage="Invalid Characters" ControlToValidate="txtURL"></asp:RegularExpressionValidator>--%>
								<%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="formValid" ValidationExpression="^(http|https):\/\/|[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}(:[0-9]{1,5})?(\/.*)?$/ix" ErrorMessage="Invalid Characters" ControlToValidate="txtURL"></asp:RegularExpressionValidator>--%>
                                </div>

                            </li>


                            <li>

                                <div class="col-md-3">
                                    Icon
                                </div>
                                <div class="col-md-9">
                                    <asp:FileUpload ID="flupImage" runat="server" />

                                    <%--     <a href="" target="_blank" id="anchorView" runat="server" style="display: none"><span  class="buttonscls"></span></a>--%>
                                    <span class="icon_pic">
                                        <img id="imgEdit" runat="server" height="30" width="30" style="display: none" />
                                    </span>
                                    <asp:Label ID="lblseperator" Visible="false" runat="server" Text="&nbsp;|&nbsp;"></asp:Label>
                                    <asp:LinkButton ID="lnkbtnDelete" runat="server" class="buttonscls"
                                        Text="Delete" Visible="false" OnClientClick="if (!confirm('Are you sure you want to delete?')) return false;" CssClass="buttonscls"></asp:LinkButton>
                                    <asp:Label ID="lblImageInfo" runat="server" Style="color: Red;" />
                                    <br />
                                </div>




                            </li>
                            <li>

                                <div class="col-md-3">
                                    Is Active
                                </div>
                                <div class="col-md-9">
                                    <asp:CheckBox ID="chkIsActive" Checked="true" runat="server" />
                                </div>




                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>


                    <div class="buttonPanel container">
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnClear" runat="server" CssClass="btn gray" Text="Clear" CommandName="clear" AccessKey="C" OnCommand="btnSave_Command" CommandArgument="Cancel" />
                            </li>
                            <li>
                                <asp:Button ID="btnSave" ValidationGroup="formValid" ClientIDMode="Static" CommandName="save" runat="server" OnCommand="btnSave_Command" CssClass="btn" Text="Update" AccessKey="S"  CommandArgument="Save" />
                            </li>

                            <li id="liSequence" runat="server">
                                <input type="button" value="Save Sequence" id="btnSequence" class="btn" />

                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        var jsonObj = [];
        $(document).ready(function () {


            $(function () {
                $(".sortable").sortable();
                $(".sortable").disableSelection();


            });

            $('#btnSequence').click(function () {

                var jsonObj = new Array();



                $('.ui-state-default').each(function (index, value) {
                    var item = {};


                    if ($(this).attr('rel') != undefined) {
                        item["socialmediaid"] = $(this).attr('rel');
                        //   item["displaylocation"] = $
                        //item["parentstaticpageid"] = $(this).attr('parentstaticpageid');
                        item["displayorder"] = index + 1;

                        jsonObj.push(item);


                    }
                    else {
                        index = index - 1;
                    }



                });



                var SequenceData = JSON.stringify(jsonObj);
                //alert(SequenceData);
                $.ajax(
                    {
                        type: 'POST',
                        url: 'SocialMediaManagement.aspx?method=savesequence',
                        data: { W: SequenceData },
                        dataType: 'JSON',
                        error: function (errorResult) {
                            //  alert(errorResult.responseText);
                            // alert(errorResult);
                            //$('#myErrorModal #ErrorMessage').html('Error Occured');
                            // $('#myErrorModal').modal('show');
                            alert('Error Occured');
                        },
                        success: function (response) {
                            if (response == '1') {

                                $('#mySuccessModal #SuccessMessage').html('Updated Successfully');
                                $('#mySuccessModal').modal('show');
                                // alert('Updated Successfully');

                            }



                        }

                    });


            });
        });


        function Validate() {
            var sMessage = '';

            var myRegExp = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
            if ($('#txtURL').val() != '') {
                if (!myRegExp.test($('#txtURL').val())) {
                    sMessage += 'Please Enter Valid Link.\n';

                }
            }

            if (sMessage != "") {
                alert(sMessage);
                return false;
            }
            else {
                return true;
            }
        }

    </script>
</asp:Content>

