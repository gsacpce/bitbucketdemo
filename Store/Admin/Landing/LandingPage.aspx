﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_Landing_LandingPage" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=host %>Admin/JS/jquery.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
    <script src="http:////cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
    <link href="<%=host %>Admin/FineUploader/fineuploaderCategory-3.2.css" rel="stylesheet" />
    <script src="<%=host %>Admin/FineUploader/jquery.fineuploaderCategory-3.2.js"></script>
    <input type="hidden" class="categorysrc" id="hidCategorySrc" runat="server" />
    <input type="hidden" class="bannersrc" id="hideBannerSrc" runat="server" />
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li>Landing Page Configuration</li>
        </ul>
        <div class="col-xs-12 hide">

            <label id="select_lang" class="sm_select col-sm-2">
                Select Language :
            </label>

            <div class="right_selct_dropdwn relative_data col-md-10 col-sm-10">
                <div class="select-style selectpicker">
                    <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"></asp:DropDownList><br />
                </div>
            </div>
        </div>
        <div class="form-group clearfix hide">
            <label for="CarriageMethod" class="control-label col-xs-4">Upload File</label>
            <div class="col-xs-4">
                <asp:FileUpload ID="flUpload" runat="server" />
            </div>
            <div class="col-xs-4">
                <asp:Image ID="imgThumb" runat="server" Height="200px" Width="200px" />
            </div>
        </div>

        <div class="col-xs-12">
            <label class="control-label col-sm-2" for="uploadImage">Upload category image for landing page:</label>
            <div class="col-md-10 col-sm-10 uploadImage">
                <div id="FULandingPage">
                </div>
                <div class="uploaderMsg">[Use .jpg, .jpeg , .png file with <= 1170px X 446px and <=2.00 MB size only]</div>
            </div>
        </div>

        <div class="form-group clearfix hide">
            <label for="CarriageMethod" class="control-label col-xs-4">Content To show</label>
        </div>
        <div class="form-group clearfix hide">
            <div class="col-xs-12">
                <CKEditor:CKEditorControl ID="txtLandingPageContent" BasePath="~/admin/ckeditor/" runat="server"></CKEditor:CKEditorControl>
            </div>
        </div>

        <div class="form-group clearfix hide">
            <div class="col-xs-8">
                <asp:Button ID="btnUpload" runat="server" Text="Save" OnClick="btnUpload_Click" />
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var host ='<%=host %>';
        $(document).ready(function () {
            $('#FULandingPage').fineUploader({
                request: {
                    endpoint: host + 'Admin/fineUploader/UploadCategory.aspx?type=LandingImage'+'&action=add'
                },
                multiple: false,
                validation: {
                    sizeLimit: <%= ImageSize %>,
                    allowedExtensions: [<%=CategoryExtension%>]
                },
                text: {
                    uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                },
                retry: {
                    enableAuto: false
                },
                chunking: {
                    enabled: false,
                    partSize: <%= ImageSize %>
                    },
                showMessage: function (message) {
                    // Using Bootstrap's classes
                    $('#FULandingPage .fineError').hide();
                    $('#FULandingPage .FailureDiv').hide();
                    $('#FULandingPage').append('<div class="alert alert-error fineError">' + message + '</div>');
                },
                debug: true
            });
        });

        jQuery(document).ready(function () {
            if ('<%= LandingPageImageHtml %>' != '')
            {
                $('#FULandingPage .qq-upload-button').hide();
                $('#FULandingPage .qq-upload-list').html('<%= LandingPageImageHtml  %>');
                $('#FULandingPage .uploadedimage').css('maxHeight', 190);
                $('#FULandingPage .uploadedimage').css('maxWidth', 700);
                ActionType = 'LandingImage'
                      
            } 
        });
    </script>
</asp:Content>
