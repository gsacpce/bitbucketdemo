﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Landing_UpdateLandingPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div>
        <h3>UPDATE LANDING PAGE</h3>
        <table style="width: 100%">
            <tr>
                <td colspan="2">Display :

                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rbtnDisplayType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Currency" Value="134"></asp:ListItem>
                        <asp:ListItem Text="Country" Value="135"></asp:ListItem>
                    </asp:RadioButtonList>

                </td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td colspan="2">Control :

                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rbtnControlType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="DropDown" Value="136"></asp:ListItem>
                        <asp:ListItem Text="Text Update" Value="137"></asp:ListItem>
                    </asp:RadioButtonList>

                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <asp:Button runat="server" ID="btnSave" Text="Save" OnClientClick="ShowLoader()" OnClick="btnSave_Click" />&nbsp;
                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" />

                </td>

            </tr>
        </table>
    </div>
</asp:Content>