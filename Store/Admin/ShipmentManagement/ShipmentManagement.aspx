﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_ShipmentManagement_ShipmentManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Delivery Config Management </a></li>
            <li>Shipment Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <div class="content">
                    <h3 class="mainHead">Shipment Management</h3>
                    <br />

                    <div class="row">
                        <div class="form-group clearfix">
                            <label class="control-label col-sm-2" for="category">Select Country :</label>
                            <div class="col-md-3 col-sm-10">
                                <asp:DropDownList ID="ddlCountry" CssClass="form-control input-sm customInput" AutoPostBack="true" runat="server" Enabled="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                </asp:DropDownList>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group clearfix">
                            <label class="control-label col-sm-2" for="category">Country Code :</label>
                            <div class="col-md-3 col-sm-10">
                                <asp:Label ID="lblCountryCode" runat="server" class="form-control input-sm customInput"></asp:Label>
                            </div>
                        </div>
                        <div id="divChkFreightTables" runat="server" class="form-group clearfix" visible="false">
                            <label class="control-label col-sm-2">Select Freight Tables :</label>
                            <div class="col-md-3 col-sm-10">
                                <asp:CheckBoxList ID="chkListFreightTable" runat="server" ClientIDMode="Static" RepeatDirection="Vertical"></asp:CheckBoxList>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-md-2 col-sm-10"></div>
                            <div class="col-md-4 col-sm-10">
                                <asp:Button ID="btnAdd" CssClass="btnAdd" runat="server" Text="Submit" ValidationGroup="Add" OnClick="btnAdd_Click" />
                            </div>
                        </div>
                    </div>

                    <div class="button_section text-center">
                        <div>
                            <asp:GridView ID="gvShipCountry" runat="server" AutoGenerateColumns="false" DataKeyNames="FreightSourceCountryCode"
                                ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid all_customer_inner1 inner1newcls  dragbltop dragbltopextra">
                                <Columns>
                                    <asp:TemplateField HeaderText="Country Name">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnFreightSourceCountryId" runat="server" Value='<%# Eval("ID") %>'/> 
                                            <asp:Label ID="lblFreightSourceCountryName" runat="server" Text='<%# Eval("FreightSourceCountryName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Country Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFreightSourceCountryCode" runat="server" Text='<%# Eval("FreightSourceCountryCode") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Default">
                                        <ItemTemplate>
                                            <asp:RadioButton runat="server" ID="rdbIsDefault" Checked='<%# Eval("IsDefault") %>' AutoPostBack="true"
                                                onClick="RadioCheck(this);" OnCheckedChanged="rdbIsDefault_CheckedChanged"/>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit"  CssClass="btnAdd" OnClick="btnEdit_Click" CommandArgument='<%# Eval("FreightSourceCountryCode") %>'/>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="glyphicon glyphicon-remove customClose" CommandArgument='<%# Eval("FreightSourceCountryCode") %>' OnClientClick="return confirm('Are you sure deleting item?')" OnClick="lnkDelete_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function RadioCheck(rb) {
            var gv = document.getElementById("<%=gvShipCountry.ClientID%>");
            var rbs = gv.getElementsByTagName("input");

            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }
    </script>
</asp:Content>
