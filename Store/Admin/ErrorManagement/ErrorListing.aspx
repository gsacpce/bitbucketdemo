﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_ErrorManagement_ErrorListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <h3 class="mainHead">Error Template Management</h3>
                    <p>Error templates are used as the basis of messages that are shown to customers.</p>
                    <div class="butonblock nospace clearfix ">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <asp:Button ID="btnAddErrorTemplate" class="btn3" runat="server" Text="Manage Error Template" OnClick="btnAddErrorTemplate_Click" />
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="midleDrgblecontin">
                        <ul class="dragblnextBlock">
                            <li>
                                <asp:GridView ID="grdErrorTemplate" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                    BorderWidth="0" CellPadding="0" CellSpacing="0" class="all_customer_inner dragbltop"
                                    EmptyDataText="Error template has not been created." OnPageIndexChanging="grdErrorTemplate_PageIndexChanging"
                                    PageSize="20" Width="100%">
                                    <Columns>
                                        <asp:TemplateField AccessibleHeaderText="ErrorTemplateName,ErrorTemplateId" HeaderStyle-CssClass="bg"
                                            HeaderText="Template Name">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltlErrorTemplateName" runat="server" Text='<%# Eval("ErrorTemplateName")%>'></asp:Literal>
                                                <asp:HiddenField ID="hfErrorTemplateId" runat="server" Value='<%# Eval("ErrorTemplateId")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div id="dvAction" runat="server" class="view_order other_option">
                                                            <asp:LinkButton Text="Edit" runat="server" OnCommand="lbtnEdit_Command" ID="lnkbtnEdit"
                                                                CommandName="EditErrorTemplate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ErrorTemplateId") %>' />
                                                            <asp:LinkButton Text="Delete" runat="server" OnCommand="lnkbtnDelete_Command" ID="lnkbtnDelete"
                                                                CommandName="DeleteErrorTemplate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ErrorTemplateId") %>' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
