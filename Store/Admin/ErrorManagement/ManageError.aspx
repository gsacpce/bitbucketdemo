﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_ErrorManagement_ManageError" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12 relative_data">
                            <h3>Error Template Management
                            </h3>
                            <p>Error templates are used as the basis of messages that are shown to customers.</p>
                            <div class="right_selct_dropdwn">
                                <div style="float: right" id="dvLang" runat="server" visible="false">
                                    <label class="sm_select">Select Language :</label>
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="manage_email">
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">Error Template Name <span style="color: red">*</span>: </label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtErrorName" runat="server" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">Error Subject <span style="color: red">*</span>: </label>
                                <div class="col-md-3 col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtSubject" runat="server" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="name">Email Body <span style="color: red">*</span>: </label>
                                <div class="col-md-9 col-sm-10">
                                    <CKEditor:CKEditorControl ID="txtBody" BasePath="~/admin/ckeditor/" runat="server"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="content">
                </div>
                <div class="buttonPanel container">
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="btnSaveExit" runat="server" class="btn" Text="Save & Exit" OnClick="btnSave_Click" OnClientClick="return ValidateFunc();" />
                        </li>

                        <li>
                            <asp:Button ID="btnSaveAddAnother" runat="server" class="btn" Text="Save & Add Another" OnClick="btnSave_Click" OnClientClick="return ValidateFunc();" />
                        </li>

                        <li>
                            <asp:Button ID="btnCancel" runat="server" class="btn gray" Text="Cancel" OnClick="btnCancel_Click" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
