﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true"
    Inherits="Presentation.Admin_BrandColorListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;


            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;

            });


        });
    </script>
    <style>
        .alignment td {
            border: 0 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li>Manage Brand Colour</li>
        </ul>
    </div>
    <div class="admin_page">

        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <h3 class="mainHead">Brand Colour Management</h3>
                    <span class="sectionText">Manage preset colours to be displayed for the brand.</span>
                    <!-- Website tab start -->

                    <div class="butonblock nospace clearfix ">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <asp:Button runat="server" ID="btnAddNew" CommandArgument="0" CssClass="btn3" OnClick="Btn_Click" CommandName="Add" Text="Add New" />
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                    <!--mainOutContainer start-->
                    <div class="midleDrgblecontin">
                        <ul class="dragblnextBlock">
                            <li>
                                <asp:GridView ID="gvBrandColor" runat="server" AutoGenerateColumns="false" Width="100%"
                                    class="all_customer_inner dragbltop  " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                    OnPageIndexChanging="gvBrandColor_PageIndexChanging" EmptyDataText="No record found."
                                    OnRowDataBound="gvBrandColor_OnRowDataBound" OnRowDeleting="gvBrandColor_RowDeleting" AllowSorting="false" PageSize="20">
                                    <Columns>
                                        <asp:BoundField HeaderText="Item#" HeaderStyle-CssClass="bg" DataField="rownumber"
                                            ItemStyle-Width="5px" HeaderStyle-Width="5%">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Display Text" HeaderStyle-CssClass="bg" DataField="ColorName"
                                            ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Hex Code" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <table border="0" class="alignment">
                                                    <tr>
                                                        <td style="float: left;">
                                                            <input type="hidden" value='<%#DataBinder.Eval(Container.DataItem, "ColorHexCode") %>' id="hdnColorCode" runat="server" clientidmode="static" />
                                                            <span id="SpanColor" runat="server" clientidmode="static">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span style="margin-right: 20px; display: inline-block;">
                                                                <asp:Label ID="lblColorHexCode" ClientIDMode="Static" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ColorHexCode") %>'></asp:Label>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>




                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField HeaderText="Is Active" HeaderStyle-CssClass="bg" DataField="IsActive"
                                            ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                            SortExpression="Status" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="view_order other_option">

                                                            <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" ID="lnkbtnUpdate"
                                                                CommandName="<%# DBAction.Update.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BrandColorId") %>' />
                                                            <asp:LinkButton Text="Delete" runat="server" OnCommand="Btn_Click" ID="lnkbtnDelete" OnClientClick="return confirm('Are you sure deleting item?')"
                                                                CommandName="<%# DBAction.Delete.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "BrandColorId") %>' />

                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" Position="Top" FirstPageText="First" PageButtonCount="6"
                                        LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
