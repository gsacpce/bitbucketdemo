﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true"
    Inherits="Presentation.Admin_BrandColorManagement" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- MiniColors -->
    <script src="../JS/jquery.minicolors.min.js"></script>
    <link rel="stylesheet" href="../CSS/jquery.minicolors.css">
    <script>   $(document).ready(function () {

       $('input.colorSelection').minicolors();
       //$('#txtHexCode').change(function () {
       //    if ($('#txtHexCode').val() > 7) {
       //        alert('Only 7 Charactes Allowed');

       //    }
       //});

       //$('#txtDisplayText').mouseout(function () {

       //    if ($('#txtDisplayText').val() == "")
       //    {
       //        $('#rfvTxtDisplayText').css('visibility', 'visible');
       //    }



       //});


   });

    </script>
    <style>
        .minicolors-theme-default .minicolors-input {
            height: auto;
            line-height: 20px;
        }
    </style>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li>Manage Brand Colour</li>
        </ul>
    </div>
    <div class="admin_page ">

        <div class="container ">
            <div id="divMessage" visible="false" runat="server">

                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>
                        <asp:Literal ID="ltrMessage" runat="server" /></strong>
                </div>

            </div>
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>Brand Colour</h3>
                            <p>Add or Edit colour information for the Brand.</p>
                        </div>

                    </div>
                    <div class="slideshow select_data ">
                        <ul>

                            <li>
                                <div class="col-md-3">
                                    <h6>Display Text<span style="color: red">*</span>::</h6>
                                </div>
                                <div class="col-md-9 slide">
                                    <div class="size01">
                                        <asp:TextBox ID="txtDisplayText" runat="server" ClientIDMode="Static" MaxLength="100"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator  ClientIDMode="Static" ValidationGroup="formValid"  ID="rfvTxtDisplayText" runat="server" SetFocusOnError="true" ControlToValidate="txtDisplayText" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                    </div>

                                    <p>
                                        <br />
                                        Enter display text. This name will be displayed to users.
                                      
                                    </p>
                                </div>

                            </li>

                            <li>
                                <div class="col-md-3">
                                    <h6>Hex Code<span style="color: red">*</span>::</h6>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="txtHexCode" CssClass="form-control colorSelection" data-control="hue" Text="#ff6161" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <input type="hidden" id="hdnColorHexCode" runat="server" clientidmode="static" />
                                    <%--<asp:RequiredFieldValidator ClientIDMode="Static" ValidationGroup="formValid"  ID="rfvTxtHexCode" runat="server" ControlToValidate="txtHexCode" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                    <asp:RegularExpressionValidator ID="rgvTxtHexCode" ControlToValidate="txtHexCode" ValidationGroup="formValid" runat="server" ErrorMessage="Invalid Color Code." ForeColor="#ff0000" ValidationExpression="^#(?:[0-9a-fA-F]{3}){1,2}$"></asp:RegularExpressionValidator>

                                    <p>
                                        <br />
                                        Enter colour hex value.
                                    </p>
                                </div>
                            </li>




                            <li>
                                <div class="col-md-3">
                                    <h6>Is Active</h6>
                                </div>


                                <div class="col-md-9">
                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="size01"></asp:CheckBox>
                                    <p>
                                        Check/Uncheck box to activate or deactivate
                                    </p>
                                </div>
                            </li>


                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="buttonPanel container">
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="btnSave" ValidationGroup="formValid" ClientIDMode="Static" runat="server" CssClass="btn" Text="Save" AccessKey="S" OnClick="Btn_Click" OnClientClick="return Validate()" CommandArgument="Save" />
                        </li>
                        <li>
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn gray" Text="Cancel" AccessKey="C" OnClick="Btn_Click" OnClientClick="return confirm('are you sure want to cancel ?')" CommandArgument="Cancel" />
                        </li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
</asp:Content>