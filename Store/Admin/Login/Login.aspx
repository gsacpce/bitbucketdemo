﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Admin_Login_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <asp:Literal ID="litCSSnJS" runat="server"></asp:Literal>
    <asp:Literal ID="litJS" runat="server"></asp:Literal>
</head>
<body>
    <form id="form1" runat="server">
        <div class="logpage">
            <div class="col-md-12">
                <div class="text-center logo">
                    <a href="#">
                        <img src='<%= GlobalFunctions.GetVirtualPathAdmin() %>Images/logo-brandAddition.png' width="370" height="69" alt="Logo" /></a>
                </div>
                <div class="col-md-6 border_right">
                    <h6>Welcome to Corporate Store</h6>
                    <p>This is the control panel for corporate stores administration, using this tool you can manage all aspects of a corporate stores, which includes:</p>
                    <ul>
                        <li>Stores</li>
                        <li>Catalogue Setup</li>
                        <li>Orders and Procurement</li>
                        <li>User Setup</li>
                        <li>Store Designs</li>
                        <li>Other Features</li>
                    </ul>
                    <p>Unauthorised access to this area is not allowed if you are an administrator of a corporate store and have troubles signing in please contact support@dummyurl.co.uk</p>
                    <p>If you have forgotten your password you can click the "Forgot password?" link.</p>
                    <p class="no_margin">Please save URL of the site by clicking "Want to Bookmark this Site ?.</p>
                </div>
                <div class="col-md-6">
                    <div class="form-horizontal" role="form" runat="server">
                        <div class="form-group">

                            <div class="col-sm-12">
                                <asp:TextBox CssClass="form-control" runat="server" ID="txtUserName" placeholder="User Name" autocomplete="off" onblur="return CheckTextbox(this.id)" />
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-sm-12">
                                <asp:TextBox CssClass="form-control" runat="server" ID="txtPassword" placeholder="Password" TextMode="Password" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group" id="dvCaptcha" runat="server" visible="false">
                            <div class="col-sm-12">
                                <img id="CaptchaImg" src="" runat="server" ondragstart="return false;" ondrop="return false;" />
                                <div style="margin-top: 10px">
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtCaptcha" placeholder="Captcha Text" autocomplete="off" onblur="return CheckTextbox(this.id)" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" col-sm-6">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-default col-sm-12" Text="Login" OnClick="btnSubmit_Click" OnClientClick="return Validate();" />
                            </div>
                            <div class="col-sm-6 check text-right" id="dvPasswordReset" runat="server">
                                <asp:HyperLink ID="hylnkReset" runat="server" Text="Forgot Password ?" NavigateUrl="~/Admin/Login/ResetPassword.aspx"></asp:HyperLink>
                            </div>
                        </div>
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            var heightx = $('.col-md-12').innerHeight();
            $('.col-md-12').parent().css('height', heightx);
            $('.col-md-12').parent().css('margin-top', -heightx / 2);
            $(window).resize(function () {
                var heightx = $('.col-md-12').innerHeight();
                $('.col-md-12').parent().css('height', heightx);
                $('.col-md-12').parent().css('margin-top', -heightx / 2);
            });
        });
    </script>
    <script type="text/javascript">
        function Validate() {
            var name = document.getElementById('<%= txtUserName.ClientID %>');
            var pass = document.getElementById('<%= txtPassword.ClientID %>');
            var dv = document.getElementById('<%= dvCaptcha.ClientID %>');
            if (name.value == "" || pass.value == "") {
                alert("Enter Username & Password !!!");
                return false;
            }
            if (dv != "" || dv != null) {
                var captcha = document.getElementById('<%= txtCaptcha.ClientID %>');
                if (captcha.value == "") {
                    alert("Captcha text can not be empty !!!");
                    return false;
                }
            }
            return true;
        }

        function CheckTextbox(clientId) {
            var obj = document.getElementById(clientId);
            var iChars = "^\"&+=[]{}|:<>?"
            for (var i = 0; i < obj.value.length; i++) {
                if (iChars.indexOf(obj.value.charAt(i)) != -1) {
                    alert("Text has special characters. \nThese are not allowed.\n Please remove them and try again.");
                    obj.value = "";
                    return false;
                }
            }
            return true;
        }

    </script>
    <script>

        function createCode() {
            var random;
            var temp = "";
            for (var i = 0; i < 5; i++) {
                temp += Math.round(Math.random() * 8);
            }
            //document.getElementById('<%= CaptchaImg.ClientID %>').src = "JpegImage.aspx?code=" + temp;
            document.all.CaptchaImg.src = "JpegImage.aspx?code=" + temp;
        }
        window.onload = createCode();
    </script>
</body>
</html>