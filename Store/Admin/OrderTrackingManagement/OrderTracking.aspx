﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true"  Inherits="Presentation.Admin_OrderTrackingManagement_OrderListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../CSS/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <div class="admin_page">

        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <h3 class="mainHead">Order Tracking</h3>
                    <p></p>
                    <!-- Website tab start -->

                    <div>
                        <table>
                            <tr>
                                <td>Customer :
                                    <asp:TextBox ID="txtCustomerSearch" ClientIDMode="Static" runat="server" Height="30" Width="500"></asp:TextBox>

                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btnAddNew" ClientIDMode="Static" OnCommand="btn_Command" CommandName="search" CommandArgument="0" CssClass="btn3" Text="Search" />

                                </td>
                            </tr>

                        </table>

                        <div class="clear">
                        </div>
                    </div>
                    <!--mainOutContainer start-->
                    <div class="midleDrgblecontin manage_slide_pegi">
                        <ul class="dragblnextBlock  new_side_show_mangment">
                            <li>
                                <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="false" Width="10%"
                                    class="all_customer_inner dragbltop " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                    OnPageIndexChanging="gvOrders_PageIndexChanging" EmptyDataText="No record found."
                                    AllowSorting="false" PageSize="10">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer Orders">
                                            <ItemTemplate>

                                                        <li>
                                                            <asp:LinkButton ID="lnkBtnOrderId" CommandArgument='<%#Eval("OrderId") %>' runat="server" Text='<%#Eval("OrderId") %>' OnCommand="btn_Command" CommandName="orders">

                                                            </asp:LinkButton>
                                                        </li>

                                                    </ItemTemplate>

                                               

                                        </asp:TemplateField>


                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" FirstPageText="First" PageButtonCount="5"
                                        LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                               
                            </li>

                        </ul>
                    </div>
                    <div id="container">
                        <ul>
                        </ul>


                    </div>


                </div>
            </div>
        </section>

        <script type="text/javascript">
            $(document).ready(function () {

                $('#txtCustomerSearch').autocomplete({
                    source: function (request, response) {
                        $.ajax({

                            url: 'OrderTracking.aspx/PopulateUsers',
                            type: 'POST',
                            data: JSON.stringify({ term: request.term }),
                            dataType: 'JSON',
                            contentType: 'application/json;charset=utf-8',
                            success: function (data) {
                                response(data.d);

                            },
                            error: function (errorResult) {
                                //alert('hello');
                                alert(errorResult.responseText);
                            }


                        });

                    }
                    , min: 2
                });


            });

        </script>

    </div>
</asp:Content>

