﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_OrderTrackingManagement_OrderTrackingManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../CSS/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <div class="admin_page">

        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                  
                    <h3 class="mainHead">Order Tracking</h3>
                    <p></p>
                    <!-- Website tab start -->

                    <!--mainOutContainer start-->
                   
                    <div>
                        <asp:HiddenField ID="hdnOrderId" runat="server"></asp:HiddenField>

                        <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="false"
                            CssClass="dragbltop" BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                            OnPageIndexChanging="gvOrders_PageIndexChanging" OnRowDataBound="gvOrders_RowDataBound" 
                            AllowSorting="false" PageSize="10" Width="50%">
                            <Columns>
                                <asp:TemplateField HeaderText="Product Id">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlProductId" Text='<%#Eval("ProductSKUId") %>' runat="server"></asp:Literal>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product Name">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlProductName" Text='<%#Eval("ProductName") %>' runat="server"></asp:Literal>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dispatch Id">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDispatchId" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfValDispatchId" runat="server" ErrorMessage="*" ControlToValidate="txtDispatchId" ForeColor="Red"  ValidationGroup="form"></asp:RequiredFieldValidator>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tracking Url">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtTrackingUrl" runat="server"></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="rfValTrackingUrl" runat="server" ErrorMessage="*" ControlToValidate="txtTrackingUrl" ForeColor="Red" ValidationGroup="form"></asp:RequiredFieldValidator>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Consignment No">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtConsignmentNo" runat="server"></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="rfValConsignmentNo" runat="server" ErrorMessage="*" ControlToValidate="txtConsignmentNo" ForeColor="Red" ValidationGroup="form"></asp:RequiredFieldValidator>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delivered Quantity">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDeliveredQuantity" runat="server"></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="rfDeliveredQuantity" runat="server" ErrorMessage="*" ControlToValidate="txtDeliveredQuantity" ForeColor="Red" ValidationGroup="form"></asp:RequiredFieldValidator>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Shipping Method">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtShippingMethod" runat="server"></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="rfValShippingMethod" runat="server" ErrorMessage="*" ControlToValidate="txtShippingMethod" ValidationGroup="form" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No Record Found

                            </EmptyDataTemplate>
                            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" FirstPageText="First" PageButtonCount="5"
                                LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                            <PagerStyle CssClass="paging" />
                        </asp:GridView>


                    </div>
                    
                       <div class="buttonPanel">
                            <ul class="button_section">
                                <li>
                                    <asp:Button ID="btnUpdate" OnCommand="btnUpdate_Command" runat="server" Text="Update"  ValidationGroup="form" CommandName="update"  CssClass="btn" />
                                </li>
                                 <li>
                                    <asp:Button ID="btnCancel" OnCommand="btnUpdate_Command" runat="server" Text="Cancel"  CommandName="cancel" CssClass="btn gray" />
                                </li>
                            </ul>
                        </div>


                </div>
            </div>
        </section>



    </div>
</asp:Content>

