﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Admin_StaticPageSequenceAjax" MasterPageFile="~/Admin/Master/AdminMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- homepage_slideshow start -->

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Static Page Management</a></li>
            <li>Static Page Sequence</li>
        </ul>
    </div>
    <div>
        <input type="hidden" id="hdnSequenceData" />
        <div class="admin_page">
            <section class="container mainContainer padbtom15">
                <div class="wrap_container ">
                    <div class="content">
                        <div id="divMessage" style="display: none">

                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Updated successfully
                                </strong>
                            </div>


                        </div>
                        <h3 class="mainHead">Static page Sequence</h3>
                        <span class="sectionText">You can set the sequence for  by simply
            dragging and dropping to the one required.
            <br />
                            For static pages  minimize before setting sequence and for sub static pages and sub sub
            static pages expand the static page to set sequence.</span>
                        <div class="butonblock">
                            <ul class="categories_btn">
                                <li>

                                    <%-- <asp:Button runat="server" ID="btnSaveSequence" CssClass="btn3" OnClientClick="SaveData()" Text="Save Sequence" />--%>
                                    <input type="button" id="btnSave1" class="btn3" value="Save Sequence" />
                                    <%--<asp:Button runat="server" ID="Button1" CssClass="btn3" OnClientClick="SaveData()" Text="Save Sequence" />--%>
                                    <li>
                                        <li>
                                            <asp:Button runat="server" ID="btnAddNew" CssClass="btn3" OnCommand="Btn_Command" CommandName="addNew" Text="Add New Static Page" />
                                        </li>
                            </ul>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="categoriesmainBlock">

                            <%--<input type="button" value="Save" onclick="SaveData()" />--%>
                            <!--butonblock-->
                            <div class="categoryMidlBlock">
                                <div class="websiteSqBlock  websiteSqBlockSpace" style="width: 30% !important;">
                                    <div class="texxt texxtnwp">

                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="websiteSqBlockInnerBlock">
                                        <h2>Static Page Name</h2>
                                        <div>

                                            <ul id="sortable" class="sortable">
                                                <asp:Repeater ID="rptPStaticPage" runat="server" OnItemDataBound="rptPStaticPage_OnItemDataBound">
                                                    <ItemTemplate>

                                                        <li class="ui-state-default" id="liPC" runat="server">
                                                            <span class="title_panel">
                                                                <input id="rbtnHeader" type="radio" runat="server" name="headerfooter" value="header" rel='<%# DataBinder.Eval(Container.DataItem, "StaticPageId") %>' />
                                                                Header
                                                                
                                                                <input id="rbtnFooter" type="radio" runat="server" name="headerfooter" value="footer" rel='<%# DataBinder.Eval(Container.DataItem, "StaticPageId") %>' />
                                                                Footer
                                                           
                                                            </span>
                                                            <span class="textIcon paneltab" runat="server"><%# DataBinder.Eval(Container.DataItem, "PageName") %>
                                                                
                                                            </span>

                                                            <ul id="sortableChild" runat="server" clientidmode="static" class="sortable">
                                                                <asp:Repeater ID="rptSStaticPage" runat="server" OnItemDataBound="rptSStaticPage_OnItemDataBound">
                                                                    <ItemTemplate>

                                                                        <li class="ui-state-default" id="liSC" runat="server">
                                                                            <span class="textIcon paneltab" runat="server"><%# DataBinder.Eval(Container.DataItem, "PageName") %></span>
                                                                            <ul id="sortableGrandChild" runat="server" clientidmode="static" class="sortable">


                                                                                <asp:Repeater ID="rptSSStaticPage" runat="server" OnItemDataBound="rptSSStaticPage_OnItemDataBound">
                                                                                    <ItemTemplate>
                                                                                        <li class="ui-state-default" id="liSSC" runat="server">

                                                                                            <span class="textIcon" runat="server"><%# DataBinder.Eval(Container.DataItem, "PageName") %></span>
                                                                                        </li>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>

                                                                            </ul>
                                                                        </li>

                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </li>

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>

                                        </div>


                                        <!--websiteSqBlockInnerBlock-->
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <!--categoryMidlBlock-->
                            </div>
                            <!--categoriesmainBlock-->
                            <div class="bottom_part margein0 button_section">
                                <ul>
                                    <li>
                                        <%--<asp:Button runat="server" ID="btnFooterSequence" CssClass="save" OnClientClick="SaveData()" Text="Save Sequence" />--%>
                                        <input type="button" id="btnSave2" class="save" onclick="SaveData()" value="Save Sequence" />

                                        <li>
                                            <asp:Button ID="btnCancel" class=" save" Text="Cancel" runat="server" OnCommand="Btn_Command" CommandName="addNew" /></li>
                                        <%--<li><input type="submit" class="cancelM" id="cpContent_btnCancel" value="Cancel" name="ctl00$cpContent$btnCancel"></li>--%>
                                </ul>
                            </div>
                        </div>
                    </div>
            </section>
            <div>
            </div>
        </div>
    </div>
    <script src="../JS/jquery.js"></script>
    <script src="../JS/jquery-ui.js"></script>
    <script type="text/javascript">
        var jsonObj = [];
        $(document).ready(function () {




            $('ul').each(function () {
                var element = $(this);

                if (element.children().length == 0) {
                    element.remove();
                }
            });

            $(function () {
                $(".sortable").sortable();
                $(".sortable").disableSelection();


            });




            $(".ClickUIMain").toggle(function () {

                $(this).next('#sortable').slideUp();
                $(this).addClass('paneltabplus');

            }, function () {

                $(this).next('#sortable').slideDown();
                $(this).removeClass('paneltabplus');
            });

            $('body').on('click', '.paneltab', function () {

                $(this).toggleClass("paneltabplus");
                $(this).parent().parent().find('ul').slideToggle('slow');

            })

            $('.ui-state-default').each(function () {
                if ($(this).children().length == 0) {
                    $(this).remove();
                }
            })

            $('#btnSave1,#btnSave2').click(function () {
                var jsonObj = new Array();
                var radioButtonList = new Array();

                $('input[type="radio"]:checked').each(function (index, value) {
                    var headerFooterItem = {};
                    //alert($(this).val());

                    headerFooterItem["displaylocation"] = $(this).val();
                    headerFooterItem["staticpageid"] = $(this).attr('rel');
                    radioButtonList.push(headerFooterItem)




                })

                //alert(JSON.stringify(radioButtonList));

                $('.StaticPage').each(function (index, value) {
                    var item = {};


                    if ($(this).attr('rel') != undefined) {
                        item["staticpageid"] = $(this).attr('rel');
                        //   item["displaylocation"] = $
                        //item["parentstaticpageid"] = $(this).attr('parentstaticpageid');
                        item["displayorder"] = index + 1;

                        jsonObj.push(item);


                    }
                    else {
                        index = index - 1;
                    }


                    //alert($(this).attr('id'));


                });



                var SequenceData = JSON.stringify(jsonObj);
                var DisplayLocation = JSON.stringify(radioButtonList);
                //alert(SequenceData);
                $.ajax(
                    {
                        type: 'POST',
                        url: 'StaticPageSequence.aspx?method=savesequence',
                        data: { W: SequenceData, V: DisplayLocation },
                        dataType: 'JSON',
                        //contentType:'application/json;charset=utf-8',
                        error: function (errorResult) {
                            // alert(errorResult.responseText);
                            alert(errorResult);
                            //$('#myErrorModal #ErrorMessage').html('Error Occured');
                            //$('#myErrorModal').modal('show');
                            alert('Error Occured');
                        },
                        success: function (response) {
                            if (response == '1') {

                                //$('#mySuccessModal #SuccessMessage').html('Updated Successfully');
                                //$('#mySuccessModal').modal('show');
                                alert('Updated Successfully');

                            }



                        }

                    });


            });






        });











    </script>
</asp:Content>


