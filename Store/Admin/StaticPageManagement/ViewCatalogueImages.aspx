﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_StaticPageManagement_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=Host %>Admin/Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Catalogue Images </a></li>
            <li>Upload Catalogue Images</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container">
                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Manage Catalogue Images</h3>
                        <div class="clearfix"></div>
                        <div class="row butonblock clearfix">
                            <div class="col-xs-3 butonblock clearfix">
                                <asp:FileUpload ID="fuMultiple" runat="server" AllowMultiple="true" cssclass="form-control"/>
                            </div>
                            <div class="col-xs-3 butonblock clearfix">
                                <asp:HiddenField runat="server" ID="hdnImageName" />
                                <asp:Button CssClass="btn btn-sm BASYSSave exit save" ID="btnUpload" runat="server" Text="Upload Image" OnClientClick="javascript:ShowLoade();" OnClick="btnUpload_Click" />
                            </div>
                        </div>
                        <div class="row butonblock clearfix marginTop scrollGrid">
                             <div class="buttonPanel container">
                                <ul class="button_section">
                                    <li>
                                        <asp:Button ID="Button2" runat="server" CssClass="btn" OnClick="Button1_Click" OnClientClick="confirm('Are you sure deleting item?')" Text="Delete Images" />
                                    </li>
                                </ul>
                            </div>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="all_customer_inner dragbltop">
                                <Columns>
                            <asp:TemplateField HeaderText="Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblImageId" runat="server" Text='<%# Convert.ToInt32(DataBinder.Eval(Container, "RowIndex"))+1 %>'>></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Thumb Nail">
                                <ItemTemplate>
                                    <asp:Image ID="ImgThumbNail" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem,"ImageName") %>' Height="100" Width="100" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Navigate Url">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"URL") %>' Target="_blank" Style="word-wrap: break-word; word-break: break-all;"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete Image">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="checkbocDelete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                            </asp:GridView>
                            <div class="container">
                                <ul class="button_section">
                                    <li>
                                        <asp:Button ID="Button1" runat="server" CssClass="btn" OnClick="Button1_Click" OnClientClick="return confirm('Are you sure deleting item?')" Text="Delete Images" />
                                    </li>
                                </ul>
                            </div>
                        </div>
        </section>
    </div>
    </div>
        </section>
    </div>
</asp:Content>

