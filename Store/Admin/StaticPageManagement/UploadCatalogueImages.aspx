﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_StaticPageManagement_UploadCatalogueImages" %>

<script runat="server">
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=Host %>Admin/Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Catalogue Images </a></li>
            <li>Upload Catalogue Images</li>
        </ul>
         <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container btn_padd_productlist">
                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Manage Catalogue Images</h3>
                        <div class="clearfix"></div>
                        <div class="row butonblock clearfix">
                            <asp:FileUpload ID="fuMultiple" runat="server" AllowMultiple="true"/>
                            &nbsp
                            <asp:HiddenField runat="server" ID="hdnImageName" />
                            <asp:Button CssClass="btn BASYSSave exit save" ID="btnUpload" runat="server" Text="Upload Image" OnClientClick="javascript:ShowLoader();" OnClick="btnUpload_Click"/>
                            </div>
                            </section>
                    </div>
                </div>
            </section>
            </div>
         </div>
    <div id="dvLoader1" style=" display:none;" class="proOverlay">
                    <table>
                        <tr>
                            <td>Please wait...</td>
                        </tr>
                    </table>
                </div>
    
        <script>
            function ShowLoader() {
            document.getElementById('dvLoader1').style.display = ' ';
        }
        function HideLoader() {
            document.getElementById('dvLoader1').style.display = 'none';
        }
    </script>
</asp:Content>

