﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" ValidateRequest="false" Inherits="Presentation.Admin_StaticPageManagement_StaticPageManagement" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">

        function allowalphanumeric(e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 0);
        }

        function createURL() {
            debugger;
            if (!document.getElementById('ContentPlaceHolder1_chkIsParent').checked) {
                if (document.getElementById('ContentPlaceHolder1_rbtnPageType_0').checked) {
                    if (document.getElementById('<%=txtPageName.ClientID%>').value != "") {
                        var name = document.getElementById('<%=txtPageName.ClientID%>').value.trim();
                        var refinedvalue = name.replace(/[^a-zA-Z0-9]/g, '_');
                        document.getElementById('<%=txtPageURL.ClientID%>').value = "/info/" + refinedvalue.toLowerCase().toString();
                    }
                    else { document.getElementById('<%=txtPageURL.ClientID%>').value = ""; }
                }
            }
            else { document.getElementById('<%=txtPageURL.ClientID%>').value = '#'; }
        }
    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" runat="server" id="hdnAddEdit" />

    <asp:ScriptManager ID="scrpt1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upd1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
                    <li><a href="#">Static Page </a></li>
                    <li>Manage Static Pages</li>
                </ul>
            </div>
            <div class="admin_page">
                <div class="container ">
                    <div class="wrap_container">
                        <div class="content">
                            <div class="row">
                                <div class="col-md-12 relative_data">
                                    <div class="col-md-5">

                                        <h3>
                                            <asp:Literal ID="ltrPageHeading" runat="server"></asp:Literal>
                                        </h3>
                                        <p>Web pages are used to display content that doesn't change often. For example an 'About Us' or a 'General Info' page.</p>
                                    </div>

                                    <div class="col-md-7  text-right">
                                        <div class="right_selct_dropdwn wide">
                                            <div class="wide" style="float: right" id="dvLang" runat="server" visible="false">
                                                <label class="sm_select col-md-7 text-right sm_select">Select Language :</label>
                                                <div class="col-md-5">
                                                    <div class="select-style selectpicker">
                                                        <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="collapsableMenu ">
                            <a aria-expanded="true" data-target="#webLink" data-toggle="collapse" class="btn btn-info" type="button">Web Site Link</a>
                            <div aria-expanded="true" class="collapse in coloredChkBox" id="webLink">
                                <div class="row collapsableContent">
                                    <div class="form-group clearfix"  id="divPageTypes" runat="server" visible="false">
                                        <label class="control-label col-sm-2" for="name">Page Type <span style="color: red">*</span>: </label>
                                        <div class="col-md-10 col-sm-10">
                                            <asp:RadioButtonList ID="rbtnPageType" runat="server" OnSelectedIndexChanged="rbtnPageType_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="Contain content created using the WYSIWYG editor below" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Link to another website or document" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:CheckBox ID="chkIsParent" runat="server" OnCheckedChanged="chkIsParent_CheckedChanged" AutoPostBack="true" />
                                            <label>Is parent page </label>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix"  id="divLinkTypes" runat="server" visible="false">
                                        <asp:CheckBox ID="chkIsSystemLink" runat="server" OnCheckedChanged="chkIsSystemLink_CheckedChanged" AutoPostBack="true" />
                                            <label>Is system link </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="dvContent" runat="server" visible="false">
                            <div class="collapsableMenu ">
                                <a aria-expanded="true" data-target="#webPage" data-toggle="collapse" class="btn btn-info" type="button">Web Page Details</a>
                                <div aria-expanded="true" class="collapse in coloredChkBox" id="webPage">
                                    <div class="row collapsableContent">
                                        <div class="form-group clearfix">
                                            <label class="control-label col-sm-2" for="name">Page Name <span style="color: red">*</span>:</label>
                                            <div class="col-md-3 col-sm-10">
                                                <asp:TextBox runat="server" Width="400px" ID="txtPageName" onkeyup="javascript:createURL();"
                                                    MaxLength="100" onkeypress="javascript:return allowalphanumeric(event);" autocomplete="off" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Page Name is not valid"
                                                    ValidationExpression="^[a-zA-Z0-9 ]*$" ControlToValidate="txtPageName" SetFocusOnError="True"
                                                    ValidationGroup="ValidStatic" CssClass="red"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="control-label col-sm-2" for="category">Page URL <span style="color: red">*</span>:</label>
                                            <div class="col-md-9 col-sm-10">
                                                <asp:TextBox runat="server" Width="400px" ID="txtPageURL" ReadOnly="true" autocomplete="off" />
                                            </div>
                                        </div>
                                        <div id="dvSubPageContent" runat="server" visible="false">
                                            <div class="form-group clearfix">
                                                <label class="control-label col-sm-2" for="category">Web Page Content <span style="color: red">*</span>:</label>
                                                <div class="col-md-9 col-sm-10">
                                                    <%--<CKEditor:CKEditorControl ID="txtWebSiteContent" BasePath="~/admin/ckeditor/" runat="server" onblur="return CheckTextbox(this.id)"></CKEditor:CKEditorControl>--%>
                                                    <!-- Below control Added by SHRIGANESH SINGH 27 May 2016 START -->
                                                    <asp:TextBox ID="txtWebSiteContent" runat="server" TextMode="MultiLine" Width="700px" Height="1000px" ></asp:TextBox>
                                                    <asp:RequiredFieldValidator ValidationGroup="ValidStatic" ID="rfvStaticTextContent" runat="server" ControlToValidate="txtWebSiteContent" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>
                                                    <!-- Below control Added by SHRIGANESH SINGH 27 May 2016 END -->
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="control-label col-sm-2" for="category">Restrict to Customers Only:</label>
                                                <div class="col-md-9 col-sm-10">
                                                    <asp:CheckBox ID="chkLogin" runat="server" />
                                                </div>
                                            </div>
                                        </div>
										<div class="form-group clearfix">
                                                <label class="control-label col-sm-2" for="category">Show this page in:</label>
                                                <div class="col-md-9 col-sm-10">
                                                    <asp:RadioButtonList ID="rbtnShowPageIn" runat="server">
                                                        <asp:ListItem Text="Same Window" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="New Window" Value="1"></asp:ListItem>
                                                        <%--<asp:ListItem Text="Modal Popup" Value="2"></asp:ListItem>--%>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dvNavigation" runat="server" visible="false">
                            <div class="collapsableMenu ">
                                <a aria-expanded="true" data-target="#Navigation" data-toggle="collapse" class="btn btn-info" type="button">Navigation Menu</a>
                                <div aria-expanded="true" class="collapse in coloredChkBox" id="Navigation">
                                    <div class="row collapsableContent">
                                        <div class="form-group clearfix">
                                            <label class="control-label col-sm-2" for="name">Yes, show as a menu below a parent:</label>
                                            <div class="col-md-3 col-sm-10">
                                                <asp:CheckBox ID="chkShowPage" runat="server" OnCheckedChanged="chkShowPage_CheckedChanged" AutoPostBack="true" />
                                            </div>
                                        </div>
                                        <div id="dvParentPage" runat="server" visible="false">
                                            <div class="form-group clearfix">
                                                <label class="control-label col-sm-2" for="category">Select Parent Page:</label>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="select-style selectpicker">
                                                        <asp:DropDownList runat="server" ID="ddlParentPage" AutoPostBack="true" OnSelectedIndexChanged="ddlParentPage_SelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix" id="dvSubParentPage" runat="server" visible="false">
                                                <label class="control-label col-sm-2" for="name">Select Sub Page:</label>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="select-style selectpicker ">
                                                        <asp:DropDownList ID="ddlSubParentPage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlParentPage_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix" id="dvSubSubParentPage" runat="server" visible="false">
                                                <label class="control-label col-sm-2" for="name">Select Sub Sub Page:</label>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="select-style selectpicker">
                                                        <asp:DropDownList ID="ddlSubSubParentPage" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dvSEO" runat="server" visible="false">
                            <div class="collapsableMenu">
                                <a aria-expanded="true" data-target="#SeoMetadata" data-toggle="collapse" class="btn btn-info" type="button">SEO</a>
                                <div aria-expanded="true" class="collapse in coloredChkBox" id="SeoMetadata">
                                    <div class="row collapsableContent">
                                        <div class="form-group clearfix seoMetaDataPanel">
                                            <label class="control-label col-sm-2" for="name">Page Title:</label>
                                            <div class="col-md-5 col-sm-12">
                                                <asp:TextBox ID="txtPageTitle" runat="server" MaxLength="75" Width="400" autocomplete="off"> </asp:TextBox>
                                            </div>
                                            <div class="col-md-3 col-sm-5">
                                                <p>
                                                    <a href="javascript:void(0);" data-toggle="popover" data-content="Defines a title in the browser toolbar,  provides a title for the page when it is added to favorites, displays a title for the page in search-engine results">
                                                        <img src="../Images/UI/info-icon.png" alt="" />
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix seoMetaDataPanel">
                                            <label class="control-label col-sm-2" for="name">Meta Keywords:</label>
                                            <div class="col-md-5 col-sm-12">

                                                <asp:TextBox ID="txtMetaKeywords" TextMode="MultiLine" Columns="60" Rows="4" runat="server" MaxLength="450" autocomplete="off" Width="100%"> </asp:TextBox>
                                            </div>
                                            <div class="col-md-3 col-sm-5">
                                                <p>
                                                    <a href="javascript:void(0);" data-toggle="popover" data-content="Give a brief description of your page in this tag. Bear in mind that this description (or part of it) will be displayed in the search engine results so try to make sure you phrase it in such a way that the person searching can tell at a glance that he's found the correct page for his search. Minimize irrelevancies and put the essentials near the beginning of the description so that if the search engine only accepts the initial (say) 150 characters, the essential parts of your description will still be displayed.">
                                                        <img src="../Images/UI/info-icon.png" alt="" />
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix seoMetaDataPanel">
                                            <label class="control-label col-sm-2" for="name">Meta Description:</label>
                                            <div class="col-md-5 col-sm-12">
                                                <asp:TextBox ID="txtMetaDescription" TextMode="MultiLine" MaxLength="250" Columns="60" Rows="4" runat="server" autocomplete="off" Width="100%"> </asp:TextBox>
                                            </div>
                                            <div class="col-md-3 col-sm-5">
                                                <p>
                                                    <a href="javascript:void(0);" data-toggle="popover" data-content="The meta keywords tag allows you to provide additional text for crawler-based search engines to index along with the rest of what you've written on your site">
                                                        <img src="../Images/UI/info-icon.png" alt="" />
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="buttonPanel container">
                            <ul class="button_section">
                                <li>
                                    <asp:Button ID="btnSave" runat="server" class="btn" Text="Save" OnClick="btnSave_Click" />
                                </li>
                                <li>
                                    <asp:Button ID="btnSaveAddAnother" runat="server" class="btn" Text="Save & Add Another" OnClick="btnSaveAddAnother_Click" />
                                </li>
                                <li>
                                    <asp:Button ID="btnCancel" runat="server" class="btn gray" Text="Cancel" OnClick="btnCancel_Click" OnClientClick="javascript:return confirm('Are you sure you want to continue !!');" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="chkIsParent" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="rbtnPageType" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="chkShowPage" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlParentPage" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlSubParentPage" EventName="SelectedIndexChanged" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnSaveAddAnother" />
            <asp:PostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        //$(document).ready(function () {
        //    $('[data-toggle="popover"]').popover({
        //        placement: 'right'
        //    });
        //});

        function CallPopOver() {
            $('[data-toggle="popover"]').popover({
                placement: 'right'
            });
        }

        function pageLoad() {
            CallPopOver();
        }

    </script>
    <style>
        .collapsableMenu a {
            position: relative;
        }
    </style>
</asp:Content>

