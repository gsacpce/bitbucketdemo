﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_StaticPageManagement_StaticPageListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;


            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;

            });
        });
    </script>
    <style>
        .midleDrgblecontin.new td.thirdtd {
            width: 8% !important;
        }

        .midleDrgblecontin.new th.thirdtd {
            width: 8% !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Static Page</a></li>
            <li>Static Page Listing</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">

            <div class="wrap_container ">
                <div class="content">
                    <h3 class="mainHead">Static Pages Management</h3>
                    <span class="sectionText">Web pages are used to display content that doesn't change often. For example an 'About Us' or a 'General Info' page.</span>
                    <div class="butonblock nospace clearfix ">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <asp:Button Text="Add New Static Page" CssClass="btn1" runat="server" ID="btnAddNewStaticPage" OnClick="btnAddNewStaticPage_Click" />
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="midleDrgblecontin new">
                        <ul class="bg_title">
                            <li>
                                <table style="width: 100%" border="1" class="staticpageTbl">
                                    <tbody>
                                        <tr>
                                            <th class="firsttd">Static Page Name
                                            </th>
                                            <th class="secondtd">URL
                                            </th>
                                            <th class="thirdtd" style="display: table-cell;">Status
                                            </th>
                                            <th class="fourthtd">Action
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </li>
                        </ul>
                        <ul class="dragblnextBlock" id="ULLI">
                            <asp:Repeater ID="rptPStaticPage" runat="server" OnItemDataBound="rptPStaticPage_ItemDataBound"
                                OnItemCommand="rptPStaticPage_ItemCommand">
                                <ItemTemplate>
                                    <li id="liPC" runat="server" class="firstLitab">
                                        <table style="width: 100%" class="staticpageTbl">
                                            <tr id="trParent" runat="server">
                                                <td class="firsttd">
                                                    <span>
                                                        <%# DataBinder.Eval(Container.DataItem, "PageName") %></span>
                                                </td>
                                                <td class="secondtd">
                                                    <asp:Label ID="lblurl" runat="server"></asp:Label>
                                                </td>
                                                <td class="thirdtd" style="display: table-cell;">
                                                    <asp:Label ID="lblPageType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PageTypeText")%>'></asp:Label>
                                                </td>
                                                <td class="fourthtd">
                                                    <div class="action">
                                                        <a class="icon" href="#"></a>
                                                        <div class="action_hover">
                                                            <span></span>
                                                            <div class="other_option">
                                                                <asp:LinkButton Text="Edit" ID="lnkbtnEdit" CommandName="EditStaticPage" CommandArgument='<%#Eval("StaticPageId")+","+ Eval("PageName")%>'
                                                                    runat="server" /> 
                                                                <asp:LinkButton ID="lnkbtnDisable" Text="Disable" CommandName="DisableStaticPage" OnClientClick="return confirm('Are you sure want to Disable this static page?')"
                                                                    CommandArgument='<%#Eval("StaticPageId")+","+ Eval("PageName")%>'
                                                                    runat="server" Visible="false" />                                                                   
                                                                   
                                                                 <asp:LinkButton ID="lnkbtnDisableSystemLink" Text="Disable System Link" CommandName="DisableSystemLink" OnClientClick="return confirm('Are you sure disabling this system link?')"
                                                                      CommandArgument='<%#Eval("StaticPageId")+","+ Eval("PageName")%>'
                                                                    runat="server" Visible="false" />
                                                                <asp:LinkButton ID="lnkbtnActive" Text="Enable" CommandName="EnableStaticPage" 
                                                                    CommandArgument='<%#Eval("StaticPageId")+","+ Eval("PageName")%>'
                                                                    runat="server" Visible="false" />                                                              
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr></tr>
                                        </table>
                                        <ul>
                                            <asp:Repeater ID="rptSStaticPage" runat="server" OnItemDataBound="rptSStaticPage_ItemDataBound"
                                                OnItemCommand="rptSStaticPage_ItemCommand">
                                                <ItemTemplate>
                                                    <li id="liSC" runat="server">
                                                        <table style="width: 100%" class="staticpageTbl">
                                                            <tr id="trSubchild" runat="server">
                                                                <td class="firsttd">
                                                                    <span class="arrowUl" style="margin-left: 25px !important;">
                                                                        <%# DataBinder.Eval(Container.DataItem, "PageName") %></span>
                                                                </td>
                                                                <td class="secondtd">
                                                                    <asp:Label ID="lblsurl" runat="server"></asp:Label>
                                                                </td>
                                                                <td class="thirdtd" style="display: table-cell;">
                                                                    <asp:Label ID="lblSubPageType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PageTypeText")%>'></asp:Label>
                                                                </td>
                                                                <td class="fourthtd">
                                                                    <div class="action">
                                                                        <a class="icon" href="#"></a>
                                                                        <div class="action_hover">
                                                                            <span></span>
                                                                            <div class="other_option">
                                                                                <asp:LinkButton Text="Edit" ID="lnkbtnEdit" CommandName="EditStaticPage" 
                                                                                     CommandArgument='<%#Eval("StaticPageId")+","+ Eval("PageName")%>' runat="server" />
                                                                                 <asp:LinkButton ID="lnkbtnDisable" Text="Disable" CommandName="DisableStaticPage" OnClientClick="return confirm('Are you sure you want to disable this static page?')"
                                                                               CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StaticPageId") +","+ Eval("PageName") %>'
                                                                                runat="server" Visible="false" />
                                                                                <asp:LinkButton ID="lnkbtnDisableSystemLink" Text="Disable System Link" CommandName="DisableSystemLink" OnClientClick="return confirm('Are you sure disabling this system link?')"
                                                                                    runat="server" Visible="false"  CommandArgument='<%#Eval("StaticPageId")+","+ Eval("PageName")%>' /> 
                                                                                    <asp:LinkButton ID="lnkbtnActive" Text="Enable" CommandName="EnableStaticPage" CommandArgument='<%#Eval("StaticPageId")+","+ Eval("PageName")%>'
                                                                                    runat="server" Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr></tr>
                                                        </table>
                                                        <ul>
                                                            <asp:Repeater ID="rptSSStaticPage" runat="server" OnItemDataBound="rptSSStaticPage_ItemDataBound"
                                                                OnItemCommand="rptSSStaticPage_ItemCommand">
                                                                <ItemTemplate>
                                                                    <li id="liSSC" runat="server">
                                                                        <table style="width: 100%" class="staticpageTbl">
                                                                            <tr id="trSubSubchild" runat="server">
                                                                                <td class="firsttd">
                                                                                    <span class="arrowUl" style="margin-left: 45px !important;">
                                                                                        <%# DataBinder.Eval(Container.DataItem, "PageNAme")%>
                                                                                    </span>
                                                                                </td>
                                                                                <td class="secondtd">
                                                                                    <asp:Label ID="lblssurl" runat="server"></asp:Label>
                                                                                </td>
                                                                                <td class="thirdtd" style="display: table-cell;">
                                                                                    <asp:Label ID="lblSubSubPageType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PageTypeText")%>'></asp:Label>
                                                                                </td>
                                                                                <td class="fourthtd">
                                                                                    <div class="action">
                                                                                        <a class="icon" href="#"></a>
                                                                                        <div class="action_hover">
                                                                                            <span></span>
                                                                                            <div class="other_option">
                                                                                                <asp:LinkButton Text="Edit" ID="lnkbtnEdit" CommandName="EditStaticPage" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StaticPageId") %>'
                                                                                                    runat="server" />
                                                                                                <asp:HyperLink Target="_blank" ID="lnkView" runat="server" Text="Preview"></asp:HyperLink>
                                                                                                <asp:LinkButton ID="lnkbtnDisable" Text="Disable" CommandName="DisableStaticPage"
                                                                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StaticPageId") %>'
                                                                                                    runat="server" Visible="false" />

                                                                                                <asp:LinkButton ID="lnkBtnDisableSystemLink" Text="Disable" CommandName="DisableStaticPage"
                                                                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StaticPageId") %>'
                                                                                                    runat="server" Visible="false" />
                                                                                                <asp:LinkButton ID="lnkbtnActive" Text="Enable" CommandName="EnableStaticPage" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StaticPageId") %>'
                                                                                                    runat="server" Visible="false" />

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr></tr>
                                                                        </table>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <asp:HiddenField ID="hdnAddEdit" runat="server" />
</asp:Content>

