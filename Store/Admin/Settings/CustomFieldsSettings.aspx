﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_Settings_CustomFieldsSettings" %>

<asp:Content ID="content1" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        function ShowFileUploader() {
            $('#ModalFileUpload').removeClass("modal fade");
            $('#ModalFileUpload').AddClass("modal show");
        }

        function CloseFileUploader() {
            //$('#ModalFileUpload').removeClass("modal show");		
            $('#ModalFileUpload').AddClass("modal fade out");
            //document.getElementById('ModalFileUpload').style.display = 'none';		
        }
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Registration Settings </a></li>
            <li>Manage Custom Fields</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content remove01">
                    <h3 class="mainHead">Manage Custom Fields </h3>
                    <span class="sectionText">You can set the sequence for  by simply
            dragging and dropping to the one required.
            <br>
                        For static pages  minimize before setting sequence and for sub static pages and sub sub
            static pages expand the static page to set sequence.</span>

                    <div class="select_data">
                        <div class="row details">
                            <div class="col-md-2">
                                <h6>Select User Type : </h6>
                            </div>
                            <%--<div class="col-md-10">
                                <div class="wrap_lang create02">
                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6 type_select  ">
                                            <div class="select-style selectpicker">
                                                <asp:DropDownList ID="ddlUserHierarchies" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlUserHierarchies_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:CustomValidator ID="cvCRCustomerHierarchyLevel" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select hierarchy level"
                                                    ClientValidationFunction="ValidateCRCustomerHierarchyLevel" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>--%>
                            <!-- DropDown User Type Names -->
                            <div class="col-md-10">
                                <div class="wrap_lang create02">
                                    <div class="wrap_creat02">
                                        <div class="col-md-6 col-sm-6 type_select  ">
                                            <div class="select-style selectpicker">
                                                <asp:DropDownList ID="ddlUserTypeNames" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlUserTypeNames_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <%--<asp:CustomValidator ID="CustomValidator1" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select User Type Name"
                                                    ClientValidationFunction="ValidateCRCustomerHierarchyLevel" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>--%>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="dvUserTypesDetails" runat="server" clientidmode="static" style="display: none;" class="select_data">
                        <div class=" create02 panel_data">
                            <div class="wraptrio addborder" id="dvAddCustomField" runat="server" clientidmode="static">
                                <div class="row details">
                                    <div class="col-md-2">
                                        <h6>Select Language :</h6>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="wrap_lang create02">
                                            <div class="wrap_creat02">
                                                <div class="col-md-6 col-sm-6 type_select ">
                                                    <div class="select-style selectpicker">
                                                        <asp:DropDownList ID="ddlLanguage" runat="server" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wraptrio addborder" id="dvCustomFields" runat="server" clientidmode="static">
                            <asp:Label ID="lblImportExportMessage" runat="server" ForeColor="Red" Text="NOTE: For Dropdown / CheckBox Please first export the file in the EDIT to get the sample data format and then only Import the new data"></asp:Label>

                            <asp:GridView ID="gvCustomFields" runat="server" AutoGenerateColumns="false" Width="100%"
                                DataKeyNames="RegistrationFieldsConfigurationId" ShowFooter="true"
                                OnRowEditing="gvCustomFields_RowEditing" OnRowCancelingEdit="gvCustomFields_RowCancelingEdit" OnRowUpdating="gvCustomFields_RowUpdating"
                                OnRowDeleting="gvCustomFields_RowDeleting" OnRowCommand="gvCustomFields_RowCommand" OnRowDataBound="gvCustomFields_RowDataBound" Border="1"
                                Style="border-color: #ccc !important;">
                                <Columns>
                                    <%-- Custom Field --%>
                                    <asp:TemplateField HeaderText="Custom Field">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCustomFieldEdit" runat="server" Style="width: 136px;" Text='<%# Eval("LabelTitle") %>'></asp:TextBox>
                                            <asp:HiddenField ID="hdnRegFieldConfigID" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
                                            <asp:RequiredFieldValidator ID="rfvCustomFieldEdit" runat="server" ForeColor="Red" ControlToValidate="txtCustomFieldEdit" ValidationGroup="ValidateEditCustomFields" Display="Dynamic" ErrorMessage="Enter Custom Field" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomField" runat="server" Text='<%# Eval("LabelTitle") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtCustomField" Style="width: 136px;" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCustomField" runat="server" ForeColor="Red" ControlToValidate="txtCustomField" ValidationGroup="ValidateAddCustomFields" Display="Dynamic" ErrorMessage="Enter Custom Field" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <%-- Field Type --%>
                                    <asp:TemplateField HeaderText="Field Type">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlFieldTypesEdit" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlFieldTypesEdit_SelectedIndexChanged"></asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFieldType" runat="server" Text='<%# Eval("FieldTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlFieldTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFieldTypes_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvFieldTypes" runat="server" ControlToValidate="ddlFieldTypes" InitialValue="0" ValidationGroup="ValidateAddCustomFields" Display="Dynamic" ErrorMessage="Select Field Type" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <%-- Is Visible --%>
                                    <asp:TemplateField HeaderText="Is Visible">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsVisibleEdit" runat="server" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsVisible" runat="server" Text='<%# Eval("IsVisible") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkIsVisible" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <%-- Is Mandatory --%>
                                    <asp:TemplateField HeaderText="Is Mandatory">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsMandatoryEdit" runat="server" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsMandatory" runat="server" Text='<%# Eval("IsMandatory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkIsMandatory" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <%-- Is ID Field --%>
                                    <asp:TemplateField HeaderText="Is ID Field">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsIDFieldEdit" runat="server" Visible="true" AutoPostBack="true" OnCheckedChanged="chkIsIDFieldEdit_CheckedChanged" />
                                            <asp:HiddenField ID="hdnRegFieldConfigIDField" runat="server" Value='<%# Eval("RegistrationFieldsConfigurationId") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsIDField" runat="server" Text='<%# Eval("IsStoreDefault") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkIsIDField" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <%--Is Login Field--%>
                                    <asp:TemplateField HeaderText="Is login field">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsloginFieldEdit" runat="server" />
                                        </EditItemTemplate>
                                         <ItemTemplate>
                                            <asp:Label ID="lblIsLoginField" runat="server" Text='<%# Eval("IsLoginField") %>'></asp:Label>
                                        </ItemTemplate>
                                         <FooterTemplate>
                                            <asp:CheckBox ID="chkIsLoginField" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    
                                    <%--Is User Editable Field--%>
                                    <asp:TemplateField HeaderText="Is editable field">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsEditableFieldEdit" runat="server" />
                                        </EditItemTemplate>
                                          <ItemTemplate>
                                            <asp:Label ID="lblIsUserEditable" runat="server" Text='<%# Eval("IsUserEditable") %>'></asp:Label>
                                        </ItemTemplate>
                                         <FooterTemplate>
                                            <asp:CheckBox ID="chkIsEditableField" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <%-- Custom Field Data --%>
                                    <%--<asp:TemplateField HeaderText="Custom Field Data">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCustomFieldDataEdit" runat="server" TextMode="MultiLine" Visible="false" />
                                            <asp:RequiredFieldValidator ID="rfvCustomFieldDataEdit" runat="server" ForeColor="Red" ControlToValidate="txtCustomFieldDataEdit" ValidationGroup="ValidateEditCustomFields" Display="Dynamic" ErrorMessage="Enter Custom Field Data" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomFieldData" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtCustomFieldData" runat="server" TextMode="MultiLine" Visible="false" />
                                            <asp:RequiredFieldValidator ID="rfvCustomFieldData" runat="server" ForeColor="Red" ControlToValidate="txtCustomFieldData" ValidationGroup="ValidateAddCustomFields" Display="Dynamic" ErrorMessage="Enter Custom Field Data" Text="Required">
                                            </asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>--%>

                                    <%-- Please do not add any Template Field above this as the Column Nos are maintained to hide/show them --%>
                                    <%-- If In case, you need to add a Template Field above this, modify the Column No in the Code Behind file : SHRIGANESH SINGH--%>

                                    <%-- Choose Excel Files --%>
                                    <asp:TemplateField HeaderText="Choose Excel Files">
                                        <EditItemTemplate>
                                            <asp:FileUpload ID="flCustomFieldValues" Style="width: 226px;" runat="server" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Import/Export --%>
                                    <asp:TemplateField HeaderText="Import/Export">
                                        <EditItemTemplate>
                                            <%--<a id="aImportEdit" runat="server" visible="false" href="#" data-toggle="modal" data-target="#ModalFileUpload">Import</a>
                                            <a id="aExportEdit" runat="server" visible="false" href="#" data-toggle="modal" data-targte="#ModalFileUpload">Export</a>--%>
                                            <asp:LinkButton ID="lbImportEdit" runat="server" CommandName="Import" CssClass="save" Visible="true" Text="Import"></asp:LinkButton>
                                            <asp:LinkButton ID="lbExportEdit" runat="server" Visible="true" CommandName="Export" CssClass="save" Text="Export"></asp:LinkButton>
                                            <%--            <asp:Button ID="cmdImportEdit" runat="server" Text="Import" Visible="false" CommandName="Import" CssClass="save" />
                                            <asp:Button ID="cmdExportEdit" runat="server" Text="Export" Visible="false" CommandName="Export" CssClass="save" />--%>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <a id="aImport" runat="server" visible="false" href="#" data-toggle="modal" data-target="#ModalFileUpload">Import</a>
                                            <a id="aExport" runat="server" visible="false" href="#" data-toggle="modal" data-targte="#ModalFileUpload">Export</a>
                                            <%--  <asp:Button ID="cmdImport" runat="server" Text="Import" Visible="false" OnClientClick="ShowFileUploader(); return false;" CssClass="save" />
                                            <asp:Button ID="cmdExport" runat="server" Text="Export" Visible="false" CommandName="Export" CssClass="save" />--%>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <%-- Code added by SHRIGANESH SINGH 26 July 2016 for Is Login Field and Is User Editable --%>
                                    <%-- Is Login Field --%>
                                    <%--<asp:TemplateField HeaderText="Is Login Field">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsLoginFieldEdit" runat="server" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsLoginField" runat="server" Text='<%# Eval("IsLoginField") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkIsLoginField" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>--%>
                                    <%-- Is User Editable --%>
                                    <%--<asp:TemplateField HeaderText="User Editable">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsUserEditableEdit" runat="server" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsUserEdiatable" runat="server" Text='<%# Eval("IsUserEditable") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkIsUserEdiatable" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateField>--%>
                                    <%-- Action --%>
                                    <asp:TemplateField HeaderText="Action">
                                        <EditItemTemplate>
                                            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CommandName="Update" CssClass="save" ValidationGroup="ValidateEditCustomFields" />
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="save" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="save" />
                                            <asp:Button ID="cmdDelete" OnClientClick="javascript:return confirm('Are you sure, you want to delete this?')" runat="server" Text="Delete" CommandName="Delete"
                                                CssClass="save" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnAdd" CssClass="save" runat="server" CommandName="AddNew" Text="Add Custom Fields" ValidationGroup="ValidateAddCustomFields" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <asp:ValidationSummary ID="valAddCustomField" runat="server" CssClass="ErrorText"
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="OnClickSubmit" />
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalFileUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content customModal">
                <div class="customModal modal-header customPanel">
                    <button type="button" id="btnFileUploadModalClose" onclick="CloseFileUploader()" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title pageSubSubTitle" id="myModalLabel1">UPLOAD FILE</h4>
                </div>
                <div class="modal-body clearfix">
                    <table id="tblFileUploadModal" runat="server">
                        <tr>
                            <td>
                                <asp:Label ID="lblFileTypeMessage" runat="server" Text="Please Select an Excel file !! "></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblUploadNote" runat="server" Text="Please Select an Excel file and click on the Update button to complete the Import !! " ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:FileUpload ID="flupldCustomFieldsExcel" CssClass="fileup" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
