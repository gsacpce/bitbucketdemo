﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" ValidateRequest="false" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_ApplyJS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Manage JavaScript</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container  ">
            <div class="wrap_container">
                <div class="row">
                    <div class="col-md-12 relative_data">
                        <div class="col-md-7">
                            <h3>Manage JavaScript</h3>
                        </div>
                    </div>

                    <div class="content dwnlod_page">
                        <ol>
                            <li>
                                <asp:TextBox ID="txtJS" runat="server" Height="510px" Columns="150" TextMode="MultiLine"></asp:TextBox>
                                <asp:Button ID="btnBackup" runat="server" Text="BACKUP" class="save upload_new" OnClick="btnBackup_Click" />
                                <asp:Button ID="btnRestore" runat="server" Text="RESTORE" class="save upload_new" OnClick="btnRestore_Click" />
                                <asp:Button ID="btnSave" runat="server" Text="SAVE JAVASCRIPT" class="save upload_new" OnClick="btnSave_Click" />
                            </li>
                            <li></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
