﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_UpdateGA" %>

<script runat="server">

  
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Google Analytics Management</li>
        </ul>
    </div>
 <div class="admin_page">
       <div class="container mainContainer">
          
            <div class="wrap_container ">
                <div class="content">

        <div class="row">
            <div class="col-md-8">
                <h3 >Update Google Analytics</h3>
                <%-- <asp:RegularExpressionValidator ID="rgvAccountKey" ValidationExpression="\bUA-\d{4,10}-\d{1,4}\b" runat="server" ControlToValidate="txtAccountKey" ErrorMessage="Invalid UA Number" ForeColor="#ff0000"></asp:RegularExpressionValidator>--%>
            </div>
        </div>
        <asp:Repeater ID="rptCodes" runat="server" OnItemDataBound="rptCodes_ItemDataBound">
            <ItemTemplate>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:HiddenField runat="server" ID="hdnCurrencyId" Value='<%# Eval("CurrencyId") %>' />
                            <label for="AccountKey">Account Key: <%--(<%# Eval("CurrencyName") %>)--%></label>
                            <asp:TextBox ID="txtAccountKey" CssClass="form-control" runat="server"  autocomplete="off"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <asp:Button ID="btnSubmit" CssClass="save" OnClick="btnSubmit_Click" runat="server" Text="Submit" OnClientClick="return ValidateCode();" />
                </div>
            </div>
        </div>
    </div>
                </div>
           </div>
     </div>
    <script>
        function ValidateCode() {
            //debugger;
            var cntx = 0;
            var regx = /\bUA-\d{4,10}-\d{1,4}\b/;
            var errx = document.getElementById('WarningMessage');
            var txtx = $("[id*='rptCodes_txtAccountKey_']");
            for (var i = 0; i < txtx.length; i++) {
                if (txtx[i].value != "") {
                    if (!regx.test(txtx[i].value)) {
                        txtx[i].value = "";
                        txtx[i].style.borderColor = "red";
                        cntx = 1;
                    }
                    else {
                        cntx = 2;
                        txtx[i].style.borderColor = "#ccc";
                    }
                }
                else {
                    txtx[i].style.borderColor = "#ccc";
                }
            }
            if (cntx == 0) {
                //errx.innerHTML = "Enter UA Number.";
                //$('#myWarningModal').modal('show');
                //return false;
            }
            else if (cntx == 1) {
                errx.innerHTML = "Invalid UA Number.";
                $('#myWarningModal').modal('show');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>

