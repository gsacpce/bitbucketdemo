﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_Robot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">
        <ol class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Upload Robot Contents</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container  ">
            <div class="wrap_container ">
                <div class="content dwnlod_page">
                    <h3>Upload Robot Content</h3>
                    <p></p>
                    <ul>
                        <li>
                            <asp:TextBox ID="txtNoSearchText" runat="server" Height="600px" Columns="150" TextMode="MultiLine"></asp:TextBox>
                            <asp:Button ID="btnSave" runat="server" Text="Save" class="save upload_new" OnClick="btnSave_Click" />
                        </li>
                        <li>
                        </li>
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</asp:Content>

