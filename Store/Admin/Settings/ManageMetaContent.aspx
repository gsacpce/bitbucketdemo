﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_ManageMetaContent" %>

<script runat="server">

   
</script>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li>Manage Meta Contents</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <%--Added By Hardik START --%>
                    <div class="col-md-7 col-xs-offset-5 text-right">
                        <label class="col-md-7 text-right sm_select" id="select_lang">
                            Select Language :
                        </label>
                        <div style="float: right" id="divLang" runat="server" class="col-md-5">
                            <div class="select-style  selectpicker languageDropdown">
                                <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <%--Added By Hardik END --%>

                <div class="content">
                    <div class="row">
                        <div class="col-md-12 relative_data">
                            <div class="col-md-5">
                                <h3>
                                    <asp:Literal ID="ltrPageHeading" runat="server"></asp:Literal>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div runat="server">
                   
                    <div class="collapsableMenu">
                        <a aria-expanded="true" data-target="#SeoMetadata" data-toggle="collapse" class="btn btn-info" type="button">SEO</a>
                        <div aria-expanded="true" class="collapse in coloredChkBox" id="SeoMetadata">
                            <div class="row collapsableContent">
                                <div class="form-group clearfix seoMetaDataPanel">
                                    <label class="control-label col-sm-2" for="name">Page Title:</label>
                                    <div class="col-md-5 col-sm-12">
                                        <asp:DropDownList ID="ddlPageName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageName_SelectedIndexChanged">
                                            <asp:ListItem>Basket</asp:ListItem>
                                            <asp:ListItem>Edit Profile</asp:ListItem>
                                            <%--<asp:ListItem>Gift Certificate</asp:ListItem>--%>
                                            <asp:ListItem>Home</asp:ListItem>
                                            <asp:ListItem>Order History</asp:ListItem>
                                            <asp:ListItem>Custom Request</asp:ListItem>
                                            <asp:ListItem>Registration</asp:ListItem>
                                            <asp:ListItem>QuickShop</asp:ListItem>
                                            <asp:ListItem>SignIn</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 col-sm-5">
                                        <p>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group clearfix seoMetaDataPanel">
                                    <label class="control-label col-sm-2" for="name">Page Title:</label>
                                    <div class="col-md-5 col-sm-12">

                                        <asp:TextBox ID="txtPageTitle" runat="server" autocomplete="off" Width="100%"> </asp:TextBox>
                                    </div>
                                    <div class="col-md-3 col-sm-5">
                                        <p>
                                            <a href="javascript:void(0);" data-toggle="popover" data-content="Give a brief description of your page in this tag. Bear in mind that this description (or part of it) will be displayed in the search engine results so try to make sure you phrase it in such a way that the person searching can tell at a glance that he's found the correct page for his search. Minimize irrelevancies and put the essentials near the beginning of the description so that if the search engine only accepts the initial (say) 150 characters, the essential parts of your description will still be displayed.">
                                                <%-- <img src="../Images/UI/info-icon.png" alt="" />--%>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group clearfix seoMetaDataPanel">
                                    <label class="control-label col-sm-2" for="name">Meta Keywords:</label>
                                    <div class="col-md-5 col-sm-12">

                                        <asp:TextBox ID="txtMetaKeywords" TextMode="MultiLine" Columns="60" Rows="4" runat="server" MaxLength="450" autocomplete="off" Width="100%"> </asp:TextBox>
                                    </div>
                                    <div class="col-md-3 col-sm-5">
                                        <p>
                                            <a href="javascript:void(0);" data-toggle="popover" data-content="Give a brief description of your page in this tag. Bear in mind that this description (or part of it) will be displayed in the search engine results so try to make sure you phrase it in such a way that the person searching can tell at a glance that he's found the correct page for his search. Minimize irrelevancies and put the essentials near the beginning of the description so that if the search engine only accepts the initial (say) 150 characters, the essential parts of your description will still be displayed.">
                                                <%-- <img src="../Images/UI/info-icon.png" alt="" />--%>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group clearfix seoMetaDataPanel">
                                    <label class="control-label col-sm-2" for="name">Meta Description:</label>
                                    <div class="col-md-5 col-sm-12">
                                        <asp:TextBox ID="txtMetaDescription" TextMode="MultiLine" MaxLength="250" Columns="60" Rows="4" runat="server" autocomplete="off" Width="100%"> </asp:TextBox>
                                    </div>
                                    <div class="col-md-3 col-sm-5">
                                        <p>
                                            <a href="javascript:void(0);" data-toggle="popover" data-content="The meta keywords tag allows you to provide additional text for crawler-based search engines to index along with the rest of what you've written on your site">
                                                <%-- <img src="../Images/UI/info-icon.png" alt="" />--%>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="buttonPanel container">
                    <ul class="button_section">
                        <asp:HiddenField ID="hdnMetaContentId" runat="server" />
                        <li>
                            <asp:Button ID="btnSave" runat="server" class="btn" Text="Save " OnClick="btnSave_Click" />
                        </li>
                        <li>
                            <asp:Button ID="btnCancel" runat="server" class="btn gray" Text="Cancel" OnClick="btnCancel_Click" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

