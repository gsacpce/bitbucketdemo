﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" EnableViewStateMac="false"
    Inherits="Presentation.Admin_Settings_Defaultsettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <link href="../CSS/radio.css" rel="stylesheet" />

    <script src="../JS/StoreCreator.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Default settings</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <div class="content">
                    <h3 class="mainHead">Default settings</h3>
                    <div id="divMessage" visible="false" runat="server" class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>
                            <asp:Literal ID="ltrMessage" runat="server" /></strong>
                    </div>
                    <div class="select_data">
                        <div class="row details">
                            <div class="col-md-2">
                                <h6>Currency settings:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02 no_borderbtm">
                                    <div class="wrap_creat02" id="dvCurrencyDisplay" clientidmode="static">
                                        <ul>
                                            <li>
                                                <div class="col-md-3">
                                                    <asp:Literal ID="lblPLDisplayTypeHeading" runat="server">Currency listing</asp:Literal>:
                                                </div>
                                                <div class="col-md-2 text-center">Is Default</div>
                                            </li>
                                            <asp:Repeater ID="rptCurrencies" runat="server" OnItemDataBound="rptCurrencies_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-3">
                                                            <asp:HiddenField ID="hdnCurrencyId" runat="server" Value='<%#Eval("CurrencyId") %>' Visible="false"/>
                                                            <asp:Label ID="lblCurrencyName" runat="server" Text='<%#Eval("CurrencyName") %>'></asp:Label>
                                                            <asp:HiddenField ID="hdnPLDisplayTypeStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                        <div id="dvDefaultCurrency" runat="server" clientidmode="static" class="col-md-2 text-center radio radio-danger radio-storeType">
                                                            <input id="rbDefaultCurrency" runat="server" type="radio" name="DefaultCurrencyType" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <%--Language settings--%>
                        <div class="row details">
                            <div class="col-md-2">
                                <h6>Language settings:</h6>
                            </div>
                             <div class="col-md-10">
                                <div class="wrap_lang create02 no_borderbtm">
                                    <div class="wrap_creat02" id="dvLanguageDisplay" clientidmode="static">
                                        <ul>
                                            <li>
                                                <div class="col-md-3">
                                                    <asp:Literal ID="Literal1" runat="server">Language listing</asp:Literal>:
                                                </div>
                                                  <div class="col-md-2 text-center">Is Active</div>
                                                <div class="col-md-2 text-center">Is Default</div>
                                                <div class="col-md-2 text-center">IsBASYS Default</div>
                                            </li>
                                            <asp:Repeater ID="rptLanguages" runat="server" OnItemDataBound="rptLanguages_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-3">
                                                            <asp:HiddenField ID="hdnlanguageId" runat="server" Value='<%#Eval("LanguageId") %>' />
                                                            <asp:Label ID="lblLanguagename" runat="server" Text='<%#Eval("LanguageName") %>' ></asp:Label>
                                                        </div>
                                                         <div id="divActivelanguage" runat="server" clientidmode="static" class="col-md-2 text-center radio radio-danger radio-storeType">
                                                            <asp:CheckBox ID="chkActiveLanguage" runat="server" />
                                                        </div>
                                                        <div id="dvDefaultLanguage" runat="server" clientidmode="static" class="col-md-2 text-center radio radio-danger radio-storeType">
                                                            <input id="rbDefaultLanguage" runat="server" type="radio" name="DefaultLanguage" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                          <div id="dvDefaultBasys" runat="server" clientidmode="static" class="col-md-2 text-center radio radio-danger radio-storeType">
                                                            <input id="rbIsBasysDefault" runat="server" type="radio" name="IsDefaultBasys" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <ul class="button_section">
                    <li>
                        <asp:Button ID="cmdUpdate" runat="server" CssClass="btn" Text="Update" CommandName="Update" OnClick="cmdUpdate_Click" />
                    </li>

                    <li>
                        <asp:Button ID="cmdCancel" runat="server" CssClass="btn gray" Text="Cancel" OnClick="cmdCancel_Click" />
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnStoreId" runat="server" ClientIDMode="Static" Value="0" />
</asp:Content>

