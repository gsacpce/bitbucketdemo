﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_ManageRelatedProducts" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function ShowHide() {

            var isChecked = $('#ContentPlaceHolder1_rptPLDisplayType_chkPDDisplayType_1').is(':checked');
            var txtPrice = $('#ContentPlaceHolder1_rptPLDisplayType_txtPrice_1');
            var lblpercentage = $('#ContentPlaceHolder1_rptPLDisplayType_lblpercentage_1');
            var rftPrice = $('#ContentPlaceHolder1_rptPLDisplayType_RftPrice_1');
            if (isChecked == 0) {
                txtPrice.hide();
                lblpercentage.hide();
                rftPrice.hide();
               <%-- $('#<% =divByPriceFilter.ClientID%>').hide();--%>
            }
            else {
                txtPrice.show();
                lblpercentage.show();
                rftPrice.show();
                <%--$('#<% =divByPriceFilter.ClientID%>').show();--%>
            }
        }
    </script>

    <style>
        .add_managpro_div { padding:0px;}
        .select_data .add_managpro_div .col-md-2 { margin:0px;}
         .select_data .add_managpro_div .row { margin:0px;}
   

    </style>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a>Settings </a></li>
            <li>Manage Related Products</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <div class="content">
                    <h3 class="mainHead">Manage Related Products</h3>
                    <div id="divMessage" visible="false" runat="server" class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>
                            <asp:Literal ID="ltrMessage" runat="server" /></strong>
                    </div>
                    <div class="select_data">
                        <div class="row details">
                            <div class="col-md-2">
                                <h6>Settings:</h6>
                            </div>

                            <div class="create02 setting_newmargin">

                                <div class="col-md-10">
                                    <div class="wrap_creat02" id="dvDisplayType">

                                        <ul class="add_managpro_div">
                                            <asp:Repeater ID="rptPLDisplayType" runat="server" OnItemDataBound="rptPLDisplayType_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-3 col-sm-3">
                                                            <asp:Literal ID="ltrRelatedType" runat="server"></asp:Literal>
                                                            <div class="clearfix"></div>
                                                        </div>


                                                        <div class="col-md-8 col-sm-8">
                                                            <div class="row">
                                                                <div class="col-md-1">
                                                            <asp:CheckBox ID="chkPDDisplayType" Checked='<%# Eval("IsEnabled").ToString().ToLower() == "true" ? true : false %>' runat="server" onclick="return ShowHide()" />
                                                            </div>
                                                                    <div class="col-md-11 col-sm-8">
                                                                    <div id="divByPriceFilter" runat="server">
                                                              <div class="col-md-5" style=" padding-right:0;">  <asp:TextBox ID="txtPrice" runat="server" Text='<%# Eval("FeatureDefaultValue").ToString().ToLower()%>'  ></asp:TextBox>
                                                               <asp:RequiredFieldValidator ID="RftPrice" runat="server" ControlToValidate="txtPrice" ErrorMessage="*" Visible="false" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                   </div>
                                                                  <div
                                                                       class="col-md-1" style=" padding-left:0"><asp:Label ID="lblpercentage" runat="server" > </asp:Label>
                                                            </div></div>
                                                                        </div>
                                                            <div class="clearfix"></div>

                                                                </div>
                                                        </div>


                                                    </li>
                                                  
                                                    <asp:HiddenField ID="hdnPDDisplayTypeStoreFeatureDetailId" runat="server" />
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>

                                    </div>
                                    <ul class="button_section">
                                        <li>
                                            <asp:Button ID="cmdSaveClose" runat="server" CssClass="btn" Text="Update" CommandName="SaveClose" OnClick="cmdSaveClose_Click" />
                                        </li>
                                        <li>
                                            <asp:Button ID="cmdCancel" runat="server" CssClass="btn gray" Text="Cancel" />
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

