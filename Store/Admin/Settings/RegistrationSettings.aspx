﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_RegistrationSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Registration Settings</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Registration Settings
                        </h3>
                        <div>
                            <section class="all_customer dragblnextBlock manage_pro">
                                <div class="first_select">
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddl_FilterType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_FilterType_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="White List" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Black List" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="dvWhiteList" runat="server" visible="false">
                                    <asp:LinkButton ID="lnkDownloadWhiteList" runat="server" Text="Download White List" OnClick="lnkDownloadWhiteList_Click"></asp:LinkButton><br />
                                    <asp:Label ID="lblWhiteList" runat="server" Text="Upload White List csv file"></asp:Label>
                                    <asp:FileUpload ID="fuWhiteList" runat="server" />
                                    <asp:Label ID="lblWhiteListNote" runat="server" Text="Size not more than 1.00 MB" ForeColor="Red"></asp:Label>
                                    <br />
                                    <asp:Button ID="btnWhiteListSubmit" runat="server" OnClick="btnWhiteListSubmit_Click" Text="Submit" CssClass="save" />
                                </div>
                                <div id="dvBlackList" runat="server" visible="false">
                                    <asp:LinkButton ID="lnkDownloadBlackList" runat="server" Text="Download Black List" OnClick="lnkDownloadBlackList_Click"></asp:LinkButton><br />
                                    <asp:Label ID="lblBlackList" runat="server" Text="Upload Black List csv file"></asp:Label>
                                    <asp:FileUpload ID="fuBlackList" runat="server" />
                                    <asp:Label ID="lblBlackListNote" runat="server" Text="Size not more than 1.00 MB" ForeColor="Red"></asp:Label>
                                    <br />
                                    <asp:Button ID="btnBlackListSubmit" runat="server" OnClick="btnBlackListSubmit_Click" Text="Submit" CssClass="save" />
                                </div>
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

