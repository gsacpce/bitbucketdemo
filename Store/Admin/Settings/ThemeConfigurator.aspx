﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" EnableViewState="true"
    AutoEventWireup="true" Inherits="Presentation.Admin_Settings_ThemeConfigurator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <!-- MiniColors -->
    <script src="<%=host %>JS/jquery.minicolors.min.js"></script>
    <link rel="stylesheet" href="<%=host %>CSS/jquery.minicolors.css" />
    <style>
        .minicolors-theme-default.minicolors {
            display: block;
        }

        .minicolors-theme-default .minicolors-input {
            height: 35px;
            width: 100%;
        }

        .selectpicker {
            width: 100%;
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            margin: 1px 0;
            top: 0;
        }

        .btn-default.active, .btn-default:active, .open > .dropdown-toggle.btn-default, .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default.focus:active, .btn-default:active:focus, .btn-default:active:hover, .open > .dropdown-toggle.btn-default.focus, .open > .dropdown-toggle.btn-default:focus, .open > .dropdown-toggle.btn-default:hover {
            background-color: #090;
            color: #FFFFFF;
        }

        a:hover {
            text-decoration: none;
        }

        .well {
            clear: both;
            font-weight: bold;
            padding: 10px;
        }

        .panel-body .btn {
            padding: 6px 4px;
        }

        .minicolors-theme-default .minicolors-swatch {
            width: 50px;
            height: 25px;
            border-radius: 15px;
        }

        .minicolors-swatch-color {
            border-radius: 15px;
        }

        .minicolors-theme-default .minicolors-input {
            text-align: center;
        }

        .customCssBlock {
            display: block;
            margin-top: 20px;
        }

            .customCssBlock textarea {
                width: 100%;
                min-height: 100px;
            }

        .textDecoration > label {
            padding: 3px;
        }

        .textDecoration span {
            margin: 0 8px;
        }

        label {
            display: inline-block;
            font-weight: 700;
            margin-bottom: 10px;
            max-width: 100%;
        }

        .theme_config {
            padding-bottom: 20px;
        }
    </style>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Theme Settings</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container">
            <div class="wrap_container ">
                <div class="content theme_config">
                    <h3>Theme Settings</h3>
                    <div class="panel-group" id="accordion">
                        <!-- Header Panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#headerPanel">
                                <h4 class="panel-title">
                                    <span><strong>Header</strong>
                                        <!--<em> Reference :Background Class(.headerBg), Text Class(.headerLink)</em>-->
                                    </span>
                                </h4>
                            </div>
                            <div id="headerPanel" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label>Background Color</label>
                                            <%--<input runat="server"  type="text" id="HeaderBg" class="form-control colorSelection">--%>
                                            <select runat="server" id="HeaderBg" name="HeaderBg" clientidmode="Static" class="selectpicker ColorBox">
                                            </select>
                                            <%--<asp:DropDownList runat="server" id="HeaderBg"></asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <%--                 <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Header Bg Color</label>
                                            <select runat="server" id="HeaderBg" class="form-control colorSelection"></select>
                                        </div>

                                    </div>--%>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="HeaderLinkColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>
                                            <select runat="server" id="HeaderLinkHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Active Color</label>
                                            <select runat="server" id="HeaderLinkActiveColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="HeaderLinkFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="HeaderLinkFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="HeaderLinkFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">
                                                <label>
                                                    <asp:CheckBox runat="server" ID="HeaderLinkFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="HeaderLinkFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="well">Dropdown Menu</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Option Text Color</label>
                                            <select runat="server" id="HeaderLinkDropDownMenuColor" class="form-control colorSelection"></select>
                                        </div>

                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown BG Hover Color</label>
                                            <select runat="server" id="HeaderLinkDropDownMenuHoverBg" class="form-control colorSelection"></select>
                                        </div>

                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Hover Color</label>
                                            <select runat="server" id="HeaderLinkDropDownMenuHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Active Color</label>
                                            <select runat="server" id="HeaderLinkDropDownMenuActiveColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Active Bg Color</label>
                                            <select runat="server" id="HeaderLinkDropDownMenuActiveBgColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Dropdown Font Family</label>
                                            <select runat="server" id="HeaderLinkDropDownMenuFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Dropdown Font Size</label>
                                            <select runat="server" id="HeaderLinkDropDownMenFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="HeaderLinkMenuLinkFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="HeaderLinkDropdownFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="HeaderLinkDropdownFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-3 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label>Dropdown Head Bg Color</label>
                                            <select runat="server" id="HeaderLinkDropDownBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Panel Bg Colors</label>
                                            <select runat="server" id="HeaderLinkPanelDropDownBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Panel Active Item Colors</label>
                                            <select runat="server" id="HeaderLinkPanelDropDownActiveItemBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Header Panel -->
                        <!-- Header Menu -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#headerMenu">
                                <h4 class="panel-title">
                                    <span><strong>Product Categories</strong>
                                        <!--<em> Reference :  Menu Background Class(.menuBg), Text Class(.menuLink)</em>-->
                                    </span>
                                </h4>
                            </div>
                            <div id="headerMenu" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Background Color</label>

                                            <select runat="server" id="MenuBg" class="form-control colorSelection"></select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="MenuLinkColor" class="form-control colorSelection"></select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>

                                            <select runat="server" id="MenuLinkHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="MenuLinkFF" class="headerMenuFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="MenuLinkFS" name="size" class="headerMenuFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="MenuLinkFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="MenuLinkFFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="MenuLinkFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">Dropdown Menu</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Text Default Color</label>
                                            <select runat="server" id="liDropDownMenuColor" class="form-control colorSelection"></select>
                                        </div>

                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown BG Hover Color</label>
                                            <select runat="server" id="liDropDownMenuHoverBg" class="form-control colorSelection"></select>
                                        </div>

                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Text Hover Color</label>
                                            <select runat="server" id="liDropDownMenuHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Panel Bg Colors</label>
                                            <select runat="server" id="ProductCategoryDropDownMenuPanelBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Active Color</label>
                                            <select runat="server" id="liDropDownMenuActiveColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Dropdown Active Bg Color</label>
                                            <select runat="server" id="liDropDownMenuActiveBgColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Dropdown Font Family</label>
                                            <select runat="server" id="liDropDownMenuFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Dropdown Font Size</label>
                                            <select runat="server" id="liDropDownMenFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="liDropDownMenFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="liDropDownMenFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="liDropDownMenFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label>Dropdown Head Bg Color</label>
                                            <select runat="server" id="liDropDownBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Header Menu -->
                        <!-- Breadcrumb Panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#BreadcrumbPanel">
                                <h4 class="panel-title">
                                    <span><strong>Breadcrumb</strong>
                                    </span>
                                </h4>
                            </div>
                            <div id="BreadcrumbPanel" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Background Color</label>
                                            <select runat="server" id="BreadcrumbBgColor" clientidmode="Static" class="selectpicker ColorBox">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="BreadcrumbColor" class="form-control colorSelection"></select>
                                        </div>

                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>
                                            <select runat="server" id="BreadcrumbHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Underline</label>
                                            <label>
                                                <asp:CheckBox runat="server" ID="BreadcrumbHoverFD" data-toggle="button" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Active Color</label>
                                            <select runat="server" id="BreadcrumbActiveColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="BreadcrumbFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="BeradcrumbFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="BeradcrumbFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="BeradcrumbFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="BeradcrumbFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Breadcrumb Panel -->

                        <!-- Common Links -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#commonLinks">
                                <h4 class="panel-title">
                                    <span><strong>Hyperlink</strong><!-- <em> Reference : Text Class(.hyperLink)</em>--></span>
                                </h4>
                            </div>
                            <div id="commonLinks" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="HyperLnkColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>
                                            <select runat="server" id="HyperLnkHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Underline</label>
                                            <label>
                                                <asp:CheckBox runat="server" ID="HyperlinkHoverFD" data-toggle="button" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="HyperLnkFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="HyperLnkFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="HyperLnkFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="HyperLnkFFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="HyperLnkFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">Small Hyperlinks</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="HyperLnkSmlColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>
                                            <select runat="server" id="HyperLnkSmlHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Underline</label>
                                            <label>
                                                <asp:CheckBox runat="server" ID="HyperLnkSmlHoverFD" data-toggle="button" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="HyperLnkSmlFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="HyperLnkSmlFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="HyperLnkSmlFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="HyperLnkSmlFFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="HyperLnkSmlFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Common Links -->
                        <!-- Custom Toggle HomePage -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#customToggle">
                                <h4 class="panel-title">
                                    <span><strong>Login Toggle Bg & Currency Symbol</strong></span>
                                </h4>
                            </div>

                            <div id="customToggle" class="panel-collapse collapse">

                                <div class="panel-body">
                                    <div class="well clearfix">Login Toggle Bg</div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Bg Color</label>
                                            <select runat="server" id="customToggleBgColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Bg Hover Color </label>
                                            <select runat="server" id="customToggleBgHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="customToggleColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Text Hover Color </label>
                                            <select runat="server" id="customToggleHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="well clearfix">Login Currency Symbol</div>



                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="customCurrencyIconColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Hover Text Color</label>
                                            <select runat="server" id="customCurrencyIconColorHover" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customCurrencyIconColorFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customCurrencyIconFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customCurrencyIconFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">
                                                <asp:CheckBox runat="server" ID="customCurrencyIconFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customCurrencyIconFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="well clearfix">Login Currency Text</div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="customCurrencyTextColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Hover Text Color</label>
                                            <select runat="server" id="customCurrencyTextColorHover" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customCurrencyTextFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customCurrencyFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customCurrencyFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">
                                                <asp:CheckBox runat="server" ID="customCurrencyFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customCurrencyFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Page Heading 1 -->
                                    <div class="well">Page Heading 1</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="PageHeading1Color" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageHeading1FF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageHeading1FS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageHeading1FW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageHeading1FStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageHeading1FD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Page Heading 1 -->
                                    <!-- Page Heading 2 -->
                                    <div class="well">Page Heading 2</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="PageHeading2Color" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageHeading2FF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageHeading2FS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageHeading2FW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageHeading2FStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageHeading2FD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Page Heading 2-->
                                    <!-- Page Heading 3 -->
                                    <div class="well">Page Heading 3</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="PageHeading3Color" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageHeading3FF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageHeading3FS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageHeading3FW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageHeading3FStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageHeading3FD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Page Heading 3-->
                                    <!-- Page Sub Title -->
                                    <div class="well clearfix">Page Sub Title(Please Select Currency Text)</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="PageSubTitleColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageSubTitleFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageSubTitleFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageSubTitleFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageSubTitleFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageSubTitleFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Sub Title -->





                                </div>
                            </div>
                        </div>
                        <!-- END Toggle HomePage -->
                        <!-- Page Title / SubTitle -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#pagetTitleText">
                                <h4 class="panel-title">
                                    <span><strong>Page Title & Page Text</strong><!-- <em> Reference : Page Title Class(.pageTitle), Page Text Class(.pageText)</em>--></span>
                                </h4>
                            </div>
                            <div id="pagetTitleText" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <!-- Page Heading -->
                                    <div class="well">Page Heading</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Page Heading Bg</label>
                                            <select runat="server" id="PageHeadingBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="PageHeadingColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageHeadingFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageHeadingFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageHeadingFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageHeadingFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageHeadingFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Page Heading -->
                                    <!-- Page Title -->
                                    <div class="well clearfix">Page Title(Main Title Page)</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Title Bg Color</label>

                                            <select runat="server" id="PageTitleBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="PageTitleColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageTitleFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageTitleFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageTitleFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageTitleFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageTitleFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Title -->
                                    <!-- Page Text -->
                                    <div class="well clearfix">Page Text(Paragraph Text)</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="PageTextColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageTextFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageTextFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageTextFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageTextFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageTextFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Text -->
                                    <!-- Page Small Text -->
                                    <div class="well clearfix">Page Small Text(Checkout Page Text)</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="PageSmlTextColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageSmlTextFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageSmlTextFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageSmlTextFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageSmlTextFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageSmlTextFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Small Text -->



                                    <!-- Page Small Title -->
                                    <div class="well clearfix">Page Small Title</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="PageSubSubTitleColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageSubSubTitleFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageSubSubTitleFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageSubSubTitleFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageSubSubTitleFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageSubSubTitleFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Small Title -->
                                    <!-- Page Scroll Links -->
                                    <div class="well clearfix">Page Scroll Link</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="PageScrollLinksColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>
                                            <select runat="server" id="PageScrollLinksHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Underline</label>
                                            <label>
                                                <asp:CheckBox runat="server" ID="PageScrollLinksHoverFD" data-toggle="button" /></label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="PageScrollLinksFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="PageScrollLinksFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="PageScrollLinksFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageScrollLinksFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="PageScrollLinksFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Scroll Links -->
                                    <!-- Page Highlight Text -->
                                    <div class="well clearfix">Page Highlight Text</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="pageHighlightTextColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="pageHighlightTextFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="pageHighlightTextFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="pageHighlightTextFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="pageHighlightTextFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="pageHighlightTextFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Highlight Text -->
                                </div>
                            </div>
                        </div>
                        <!-- END Page Title / SubTitle -->
                        <!-- Custom Buttons -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#customButtons">
                                <h4 class="panel-title">
                                    <span><strong>Buttons </strong>
                                        <!--<em> Reference : Background Class(.footerBg) Text Class(.footerLink)</em>-->
                                    </span>
                                </h4>
                            </div>
                            <div id="customButtons" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <!-- Custom Button -->
                                    <div class="well clearfix">Custom button</div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Background Color</label>

                                            <select runat="server" id="customButtonBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Text Color</label>

                                            <select runat="server" id="customButtonColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Background Color Hover</label>

                                            <select runat="server" id="customButtonHoverBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Text Color Hover</label>

                                            <select runat="server" id="customButtonHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Border Color</label>

                                            <select runat="server" id="customButtonBorderColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Border Hover Color </label>
                                            <select runat="server" id="customButtonBorderHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END Custom Button -->
                                    <!-- Custom Action Button -->
                                    <div class="well clearfix">Custom Action button</div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Background Color</label>

                                            <select runat="server" id="customActionBtnBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Text Color</label>

                                            <select runat="server" id="customActionBtnColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Background Color Hover</label>

                                            <select runat="server" id="customActionBtnHoverBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Text Color Hover</label>

                                            <select runat="server" id="customActionBtnHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Border Color</label>

                                            <select runat="server" id="customActionBtnBorderColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Border Hover Color </label>
                                            <select runat="server" id="customActionBtnBorderHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END Custom Action Button -->
                                    <!-- Custom Reset Button -->
                                    <div class="well clearfix">Custom Reset button</div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Background Color</label>

                                            <select runat="server" id="customResetBtnBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Text Color</label>

                                            <select runat="server" id="customResetBtnColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Background Color Hover</label>

                                            <select runat="server" id="customResetBtnHoverBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Text Color Hover</label>

                                            <select runat="server" id="customResetBtnHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Border Color</label>

                                            <select runat="server" id="customResetBtnBorderColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Button Border Hover Color </label>
                                            <select runat="server" id="customResetBtnHoverBorderColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END Custom Reset Button -->
                                </div>
                            </div>
                        </div>
                        <!-- END Custom Buttons -->
                        <!-- Product Page -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#productPage">
                                <h4 class="panel-title">
                                    <span><strong>Product Page / Widget</strong><!-- <em> Reference : Page Title Class(.pageTitle), Page Text Class(.pageText)</em>--></span>
                                </h4>
                            </div>
                            <div id="productPage" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <!-- Product Title -->
                                    <div class="well">Product Title</div>
                                    <div class="col-lg-2 col-sm-3">
                                        <div class="form-group">
                                            <label>Title text Color</label>
                                            <select runat="server" id="ProductTitleColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="ProductTitleFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="ProductTitleFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="ProductTitleFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductTitleFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductTitleFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Product Title -->
                                    <!-- Product Title -->
                                    <div class="well">Product Code</div>
                                    <div class="col-lg-2 col-sm-3">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="ProductCodeColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="ProductCodeFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="ProductCodeFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="ProductCodeFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductCodeFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductCodeFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Product Code -->
                                    <!-- Product Name -->
                                    <div class="well">Product Name</div>
                                    <div class="col-lg-2 col-sm-3">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="ProductNameColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>

                                            <select runat="server" id="ProductBorderHoverProductNameColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="ProductNameFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="ProductNameFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="ProductNameFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductNameFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductNameFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Product Name -->
                                    <!-- Product Title -->
                                    <div class="well">List Page Product Title</div>
                                    <%--Product Small Name--%>
                                    <div class="col-lg-2 col-sm-3">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="ProductSmlNameColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="ProductSmlNameFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="ProductSmlNameFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="ProductSmlNameFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductSmlNameFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductSmlNameFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Product Title -->
                                    <!-- Product Price -->
                                    <div class="well">Product Price</div>
                                    <div class="col-lg-2 col-sm-3">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="ProductPriceColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="ProductPriceFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="ProductPriceFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="ProductPriceFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductPriceFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="ProductPriceFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Product Price -->
                                    <!-- Product Description -->
                                    <div class="well">Product Description</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="productDescriptionColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="productDescriptionFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="productDescriptionFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="productDescriptionFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="productDescriptionFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="productDescriptionFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END Product Description -->

                                    <div class="well">Product page Action Icon</div>
                                    <%--Product Page Icon--%>

                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>
                                            <select runat="server" id="pdHyperLnkColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>
                                            <select runat="server" id="pdHyperLnkHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Underline</label>
                                            <label>
                                                <asp:CheckBox runat="server" ID="pdHyperLnkHoverFD" data-toggle="button" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="Select3" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="pdHyperLnkFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="pdHyperLnkFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="pdHyperLnkFFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="pdHyperLnkFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>



                                    <!-- Product Border -->
                                    <div class="well">Product Widget Border</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Border Default Color</label>

                                            <select runat="server" id="ProductBorder" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Border Hover Color</label>

                                            <select runat="server" id="ProductBorderHover" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!--END Product Border -->

                                </div>
                            </div>
                        </div>
                        <!-- END Product Page -->
                        <!-- Custom TABLE -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#customTable">
                                <h4 class="panel-title">
                                    <span><strong>Table</strong>
                                        <!--<em> Reference : Background Class(.footerBg) Text Class(.footerLink)</em>-->
                                    </span>
                                </h4>
                            </div>
                            <div id="customTable" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="well clearfix">Table Border Color</div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Table Border Color</label>
                                            <select runat="server" id="customTableBorder" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="well clearfix">Table Title</div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label>Table Title Bg</label>
                                            <select runat="server" id="customTableHeaderTitleBgColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Table Title Text</label>
                                            <select runat="server" id="customTableHeaderTitleColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customTableHeaderTitleFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customTableHeaderTitleFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customTableHeaderTitleFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTableHeaderTitleFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTableHeaderTitleFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well clearfix">Table Head</div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Table Head Text</label>
                                            <select runat="server" id="customTableHeadColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customTableHeadFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customTableHeadFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customTableHeadFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTableHeadFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTableHeadFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well clearfix">Table Text</div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Table Text</label>
                                            <select runat="server" id="customTableTextColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customTableTextFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customTableTextFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customTableTextFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTableTextFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTableTextFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well clearfix">Table Head Text</div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Table Head Text</label>
                                            <select runat="server" id="customTableHeadingTextColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customTableHeadingTextFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customTableHeadingTextFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customTableHeadingTextFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTableHeadingTextFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTableHeadingTextFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Custom TABLE -->
                        <!-- Custom Tabbings -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#customTabComponents">
                                <h4 class="panel-title">
                                    <span><strong>Tab Component (eg. Product page)</strong>
                                        <!--<em> Reference : Background Class(.footerBg) Text Class(.footerLink)</em>-->
                                    </span>
                                </h4>
                            </div>
                            <div id="customTabComponents" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Tab Background Color</label>
                                            <select runat="server" id="TabBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Active Tab Bg </label>
                                            <select runat="server" id="customTabsActiveBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Active Tab Text</label>

                                            <select runat="server" id="customTabsActiveColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Deactive Tab Bg </label>

                                            <select runat="server" id="customTabsABg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Deactive Tab Bg Hover </label>
                                            <select runat="server" id="customTabsAHoverBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Deactive Tab Text </label>

                                            <select runat="server" id="customTabsAColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Deactive Tab Text Hover </label>

                                            <select runat="server" id="customTabsAHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customTabsAFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customTabsAFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customTabsAFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTabsAFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customTabsAFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Tabbings -->
                        <!-- Custom Form -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#customForm">
                                <h4 class="panel-title">
                                    <span><strong>Form</strong></span>
                                </h4>
                            </div>
                            <div id="customForm" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <!-- Custom Label -->
                                    <div class="well clearfix">Custom Label</div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Label Text Color</label>
                                            <select runat="server" id="customLabelColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customLabelFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customLabelFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customLabelFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="customLabelFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customLabelFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Custom Label -->
                                    <!-- Custom InputBox -->
                                    <div class="well clearfix">Custom Input Box</div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Input Border Color</label>
                                            <select runat="server" id="customInputBorderColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Input Text Color</label>
                                            <select runat="server" id="customInputColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customInputFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customInputFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customInputFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold</label>
                                            <div class="textDecoration">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Custom InputBox -->
                                </div>
                            </div>
                        </div>
                        <!-- Custom Form -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#customComponents">
                                <h4 class="panel-title">
                                    <span><strong>MISC Components</strong>
                                        <!--<em> Reference : Background Class(.footerBg) Text Class(.footerLink)</em>-->
                                    </span>
                                </h4>
                            </div>
                            <div id="customComponents" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <!-- Custom Modal -->
                                    <div class="well clearfix">Modal Heading</div>
                                    <%--Custom Modal--%>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Background Color</label>
                                            <select runat="server" id="customPanelBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="customPanelColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END Custom Modal -->
                                    <!-- Custom PANEL -->
                                    <div class="well clearfix">Panel Heading</div>
                                    <%--Custom Panel--%>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Background Color</label>
                                            <select runat="server" id="customPanelHeadingBgColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="customPanelHeadingColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Panel Boder Color</label>
                                            <select runat="server" id="customPanelBorderColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END Custom PANEL -->
                                    <!-- Custom RATINGS -->
                                    <div class="well clearfix">Custom Ratings</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Background Color</label>
                                            <select runat="server" id="customRatingsContainerBgColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Star Selected Star Color</label>
                                            <select runat="server" id="customRatingsStarColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Star Color</label>
                                            <select runat="server" id="customRatingsContainerColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END Custom RATINGS -->

                                    <!-- Custom CLOSE BUTTON -->
                                    <div class="well clearfix">Close Button</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Background Color</label>
                                            <select runat="server" id="customCloseColorBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customCloseFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Button Hover</label>
                                            <select runat="server" id="customCloseHoverColorBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END  CLOSE BUTTON -->
                                    <!-- SREACH ICON -->
                                    <div class="well clearfix">Search Icon</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="SearchIconTxtColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="SearchIconFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label>Button Hover</label>
                                            <select runat="server" id="SearchIconHoverColorBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END SREACH ICON -->

                                    <!-- BASKET ICON -->
                                    <div class="well clearfix">Basket Icon</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="BasketIconTxtColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="BasketIconFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label>Button Hover</label>
                                            <select runat="server" id="BasketIconHoverColorBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END BASKET ICON -->

                                    <!-- COUNTER-->
                                    <div class="well clearfix">Basket Count</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Background Color</label>
                                            <select runat="server" class="form-control colorSelection" id="BasketCountBgColor"></select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="BasketCountTxtColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>

                                    <!-- END COUNTER -->

                                    <!-- STRIKE PRICE-->
                                    <div class="well clearfix">Strike Price</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="StrikePriceTxtColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>

                                    <!-- END STRIKE PRICE -->

                                </div>
                            </div>
                        </div>
                        <!-- END Custom Components -->


                        <!-- Custom Accordion -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#customAccordion">
                                <h4 class="panel-title">
                                    <span><strong>Accordion</strong></span>
                                </h4>
                            </div>
                            <div id="customAccordion" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Bg Color</label>
                                            <select runat="server" id="customAccordionColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Bg Active Color</label>
                                            <select runat="server" id="customAccordionBgColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <select runat="server" id="customAccordionTextColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label>Active Text Color</label>
                                            <select runat="server" id="customAccordionTextActiveColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="customAccordionTextFF" class="headerFontFamily selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" name="size" id="customAccordionTextFS" class="headerFontSize selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="customAccordionTextFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customAccordionTextFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="customAccordionTextFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Custom Components -->

                        <!--  OWL Slider -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#owlSlider">
                                <h4 class="panel-title">
                                    <span><strong>OWL Slider </strong>
                                        <!--<em> Reference : Background Class(.footerBg) Text Class(.footerLink)</em>-->
                                    </span>
                                </h4>
                            </div>
                            <div id="owlSlider" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <!-- Custom OWL Slider-->
                                    <div class="well clearfix">Custom OWL Slider</div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Slider Bg</label>
                                            <select runat="server" id="OwlCarouselBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Slider Border Color</label>
                                            <select runat="server" id="OwlCarouselBorderColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Button Bg</label>
                                            <select runat="server" id="OwlButtonsBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Button Color</label>
                                            <select runat="server" id="OwlButtonsColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Button Hover Bg</label>
                                            <select runat="server" id="OwlButtonsHoverBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Button Hover Color</label>
                                            <select runat="server" id="OwlButtonsHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <!-- END Custom Owl Slider -->
                                </div>
                            </div>
                        </div>
                        <!-- END Custom Components -->
                        <!-- Footer Page -->
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#footer">
                                <h4 class="panel-title">
                                    <span><strong>Footer </strong>
                                        <!--<em> Reference : Background Class(.footerBg) Text Class(.footerLink)</em>-->
                                    </span>
                                </h4>
                            </div>
                            <div id="footer" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <!-- Footer Text -->
                                    <div class="well clearfix">Footer Text / Background</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Background Color</label>

                                            <select runat="server" id="FooterBg" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="FooterTextColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="FooterTextFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="FooterTextFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="FooterTextFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="FooterTextFFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="FooterTextFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Footer Text -->
                                    <!-- Footer Link -->
                                    <div class="well clearfix">Footer Link</div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Default Color</label>

                                            <select runat="server" id="FooterLinkcolor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 ">
                                        <div class="form-group">
                                            <label>Text Hover Color</label>
                                            <select runat="server" id="FooterLinkHoverColor" class="form-control colorSelection"></select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 hidden">
                                        <div class="form-group">
                                            <label for="font-family">Font Family</label>
                                            <select runat="server" id="FooterLinkFF" name="" class="selectpicker">
                                                <option value="Open-Sans" style="font-family: open sans">open-sans</option>
                                                <option value="Arial" style="font-family: Arial">Arial</option>
                                                <option value="Comic Sans MS" style="font-family: Comic Sans MS">Comic Sans MS</option>
                                                <option value="Georgia" style="font-family: Georgia">Georgia</option>
                                                <option value="Impact" style="font-family: Impact">Impact</option>
                                                <option value="Lucida Sans Unicode" style="font-family: Lucida Sans Unicode">Lucida Sans</option>
                                                <option value="Serif" style="font-family: Serif">Serif</option>
                                                <option value="Sans-Serif" style="font-family: Sans-Serif">Sans-Serif</option>
                                                <option value="Times New Roman" style="font-family: Times New Roman">Times New Roman</option>
                                                <option value="Trebuchet MS" style="font-family: Trebuchet MS">Trebuchet MS</option>
                                                <option value="Verdana" style="font-family: Verdana">Verdana</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <label for="font-size">Font Size</label>
                                            <select runat="server" id="FooterLinkFS" name="size" class="selectpicker">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-2 ">
                                        <div class="form-group">
                                            <label for="font-weight">Font Weight</label>
                                            <select runat="server" name="size" id="FooterLinkFW" class="headerFontSize selectpicker">
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="300">300</option>
                                                <option value="400">400</option>
                                                <option value="500">500</option>
                                                <option value="600">600</option>
                                                <option value="700">700</option>
                                                <option value="800">800</option>
                                                <option value="900">900</option>
                                                <option value="bold">bold</option>
                                                <option value="normal">normal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label>Bold/Italic/Underline</label>
                                            <div class="textDecoration">

                                                <label>
                                                    <asp:CheckBox runat="server" ID="FooterLinkFFStyle" data-toggle="button" /><i>Italic</i></label>
                                                <label>
                                                    <asp:CheckBox runat="server" ID="FooterLinkFD" data-toggle="button" /><u>Underline</u></label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Page Footer Link -->
                                </div>
                            </div>
                        </div>
                        <!-- END Footer Page -->

                    </div>

                    <div>
                        <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="save" OnClick="btnSave_Click" />
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container customCssBlock">
        <!--<p>Modified Css Style</p>
       		<textarea id="customCSS"  ></textarea>
       </div>-->
    </div>


    <script>

        $('input.colorSelection').minicolors();

        $('.minicolors-input').on('change', function () {
            //$(this).val(this.value);
            $(this).attr("value", this.value);

        });
        $(".boldBtn").on('click', function () {
            if ($(this).val() == 'bold') {
                $(this).val('Bold')
            } else {
                $(this).val('Bold')
            }

        });
        $(".italicBtn").on('click', function () {
            if ($(this).val() == 'italic') {
                $(this).val('Italic')
            } else {
                $(this).val('Italic')
            }

        });
        $(".underlineBtn").on('click', function () {
            if ($(this).val() == 'underline') {
                $(this).val('Underline')
            } else {
                $(this).val('Underline')
            }

        });

    </script>
</asp:Content>

