﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_AddCatalogue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../JS/StoreCreator.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=Host %>Admin/Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Catalogue </a></li>
            <li>Add Catalogue</li>
        </ul>
    </div>

    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container btn_padd_productlist">
                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Manage Catalogue</h3>
                        <div class="clearfix"></div>
                        <div class="row butonblock clearfix">
                            <div class="col-md-2 col-sm-10">
                                <label class="customer" style="margin: 0px !important;">Select Currency:</label>
                            </div>
                            <div class="col-md-4 col-sm-10">
                                <asp:DropDownList ID="ddlCurrency" CssClass="select-style selectpicker" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvddlCurrency" runat="server" InitialValue="Please select" Display="Dynamic" ValidationGroup="Add" ControlToValidate="ddlCurrency" ErrorMessage="Please select the currency for adding catalogue !!" Text="*"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4 col-sm-10">
                                <asp:Button ID="btnAdd" CssClass="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" ValidationGroup="Add" />
                            </div>
                        </div>
                        <div class="">
                            <section class="all_customer dragblnextBlock manage_pro">
                                <div>

                                    <table width="100%" class="dragbltop" id="tblNoRecord" runat="server">
                                        <tr>
                                            <th class="bg" scope="col">DivisionId</th>
                                            <th class="bg" scope="col">CatalogueId</th>
                                        </tr>
                                        <tr>
                                            <td colspan="2">No Record found</td>
                                        </tr>
                                    </table>

                                    <asp:Repeater ID="rptCatalogue" runat="server">
                                        <HeaderTemplate>
                                            <table width="100%" class="dragbltop">
                                                <tr>
                                                    <th class="bg" scope="col">DivisionId</th>
                                                    <th class="bg" scope="col">CatalogueId</th>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <table width="100%" class="dragbltop">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <%#Eval("DivisionId") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("CatalogueId") %>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:Panel ID="pnlBASYSDetails" runat="server">
                                        <div>
                                            <h4>Catalogue Details</h4>
                                        </div>
                                        <div>
                                            <table>
                                                <tr>
                                                    <th>DivisionId<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:Label ID="lblDivisionID" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Division Name</th>
                                                    <td>
                                                        <asp:Label ID="lblDivisionName" runat="server"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th>KeyGroupName</th>
                                                    <td>
                                                        <asp:DropDownList ID="ddlKeyGroupName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlKeyGroupName_SelectedIndexChanged"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>KeyGroupId<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:Label ID="lblKeyGroupId" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Catalogue Name</th>
                                                    <td>
                                                        <asp:DropDownList ID="ddlCatalogueName" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlCatalogueName_SelectedIndexChanged"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>CatalogueId <span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:Label ID="lblCatalogueID" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>SourceCode Name</th>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSourceCodeName" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlSourceCodeName_SelectedIndexChanged"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>SourceCodeId<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:Label ID="lblSourceCodeId" Width="100%" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Default Customer Name<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:DropDownList ID="ddlDefaultCustomerName" Width="100%" runat="server" OnSelectedIndexChanged="ddlDefaultCustomerName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        <%--<asp:TextBox ID="txtDefaultCustomerId" Width="100%" MaxLength="10" class="span5" runat="server" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>--%>
                                                        <asp:RequiredFieldValidator ID="rfvDefaultCustomerId" CssClass="rfvDefaultCustomerId" runat="server" ControlToValidate="ddlDefaultCustomerName" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Customer Id<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:TextBox ID="txtCustomerId" Width="100%" class="span5" MaxLength="80" AutoCompleteType="Disabled" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvCustomerId" CssClass="rfvSalesPersonId" runat="server" ControlToValidate="txtCustomerId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>SalesPersonId<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:TextBox ID="txtSalesPersonId" runat="server" Width="100%" class="span5" MaxLength="80" AutoCompleteType="Disabled" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvSalesPersonId" CssClass="rfvSalesPersonId" runat="server" ControlToValidate="txtSalesPersonId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                        </asp:RequiredFieldValidator>
                                                        <%--  <asp:DropDownList ID="ddlSalesPersonID" Width="100%" runat="server"></asp:DropDownList>--%>
                                                    </td>
                                                </tr>
                                                <%--  <tr>
                                                    <th>Global Cost Centre Number</th>
                                                    <td>
                                                        <asp:TextBox ID="txtGlobalCostCentreNumber" Width="100%" class="span5" MaxLength="80" AutoCompleteType="Disabled" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox></td>
                                                </tr>--%>
                                                <tr>
                                                    <th>GroupId<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:TextBox ID="txtGroupId" class="span5" Width="100%" Text="8" MaxLength="10" runat="server" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvGroupId" CssClass="rfvGroupId" runat="server" ControlToValidate="txtGroupId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>

                                                <tr style="display: none;">
                                                    <th>CatalogueAliasId<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:TextBox ID="txtCatalogueAliasId" MaxLength="50" class="span5" runat="server" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvCatalogueAliasId" CssClass="rfvCatalogueAliasId" runat="server" ControlToValidate="txtCatalogueAliasId" Enabled="false" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <%--   <tr>
                                                    <th>DefaultCompanyId<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:TextBox ID="txtDefaultCompanyId" MaxLength="10" Width="100%" class="span5" runat="server" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvDefaultCompanyId" CssClass="rfvDefaultCompanyId" runat="server" ControlToValidate="txtDefaultCompanyId" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <th>Default Customer Contact Id<span class="redColor">*</span></th>
                                                    <td>
                                                        <asp:TextBox ID="txtDefault_CUST_CONTACT_ID" Width="100%" MaxLength="10" class="span5" runat="server" AutoCompleteType="Disabled" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtDefault_CUST_CONTACT_ID" CssClass="rfvDefaultCompanyId" runat="server" ControlToValidate="txtDefault_CUST_CONTACT_ID" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSave" ForeColor="Red">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="new_button_pannel button_section">
                                            <asp:Button type="button" runat="server" CssClass="btn BASYSSave exit save" Text="Save" ID="cmdSave" ValidationGroup="OnClickSave" OnClick="OnClickSave"></asp:Button>
                                            <button type="button" class="btn BASYSCancel  gray" id="cmdCancel">Cancel</button>
                                        </div>
                                    </asp:Panel>
                                    <div id="dvBASYSDetails" runat="server">
                                    </div>
                                </div>
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
