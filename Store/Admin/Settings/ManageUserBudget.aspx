﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_ManageUserBudget" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Budget Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <h3 class="mainHead">Manage User Budget</h3>
                    <div class="CsA">
                        <div id="divUserBudget" class="divHide table-responsive" runat="server" style="margin-top: 15px;">
                            <span class="butonblock clearfix">
                                <ul class="categories_btn bottom_btn" style="float: left">
                                    <li>
                                        <asp:FileUpload ID="fileuploadBudget" runat="server" Style="margin: 16px 0; max-height: 45px;" />
                                    </li>
                                    <li>
                                        <asp:Button runat="server" ID="btnImport" CssClass="btn2" OnClick="btnImport_Click" Text="Import Users Budget" />
                                    </li>
                                    <li>
                                        <asp:Button runat="server" ID="btnExport" CssClass="btn2" OnClick="btnExport_Click" Text="Export Users Budget" />
                                    </li>
                                </ul>
                                <div class="clear"></div>
                            </span>

                            <asp:GridView ID="gvUserBudget" runat="server" CssClass="dragbltop table table-hover" AutoGenerateColumns="false"
                                AllowPaging="true" PageSize="30"
                                EmptyDataText="No users exists.">
                                <Columns>
                                    <asp:TemplateField HeaderText="Email ID">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltlEmailId"
                                                runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EmailId") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Budget Code">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltlBudgetCode"
                                                runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BudgetCode") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--         <asp:TemplateField HeaderText="Currency Symbol">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltlCurrencySymbol"
                                                runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CurrencySymbol") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRemove" runat="server"
                                                CommandArgument='<%# Eval("EmailId")%>'
                                                OnClientClick="return confirm('Do you want to delete?')"
                                                Text="Delete" OnClick="DeleteRecord"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

