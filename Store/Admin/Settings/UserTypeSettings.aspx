﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_Settings_UserTypeSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="<%=hostAdmin %>JS/StoreCreator.js"></script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Registration Settings </a></li>
            <li>Manage User Types</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <h3 class="mainHead">User Type Settings</h3>
                    <div class="select_data">
                        <div class="row details">
                            <div class="col-md-2">
                                <h6>Customer Registration
                  & Account:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02">
                                    <div class="wrap_creat02">
                                        <h5>
                                            <asp:Label ID="lblCRCustomerTypeHeading" runat="server"></asp:Label></h5>
                                        <div class="col-md-4 col-sm-4 ">
                                            <div class="radio_data  radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRCustomerType" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 type_select  ">
                                            <div class="select-style selectpicker">
                                                <asp:DropDownList ID="ddlCRCustomerHierarchyLevel" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="ddlCRCustomerHierarchyLevel_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="Select Level" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CustomValidator ID="cvCRCustomerHierarchyLevel" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select hierarchy level"
                                                    ClientValidationFunction="ValidateCRCustomerHierarchyLevel" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" id="dvApprovalRequired" runat="server" clientidmode="static">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblCRApprovalRequired" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRApprovalRequired" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="dvUserTypesDetails" runat="server" clientidmode="static" style="display: none;" class="select_data">
                        <div class="row details">
                            <div class="col-md-2">
                                <h6></h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02 panel_data">
                                    <div class="wraptrio addborder" id="dvUserType1" runat="server" clientidmode="static">
                                        <div class="col-md-4 col-sm-4  ">
                                            <label class="col-md-5">User Type 1 :</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtUserType1" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvUserType1" runat="server" ControlToValidate="txtUserType1" Enabled="false" ClientIDMode="Static"
                                                    Display="dynamic" ErrorMessage="Please enter User Type 1"
                                                    Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <%--<div class="col-md-4 col-sm-4 type_select  ">
                                            <label class="col-md-5">CatalogueId :</label>
                                            <div class="col-md-7">
                                                <div class="select-style selectpicker">
                                                    <asp:DropDownList ID="ddlUserType1CatalogueIds" runat="server" ClientIDMode="Static">
                                                        <asp:ListItem Text="Select CatalogueId" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:CustomValidator ID="cvUserType1CatalogueIds" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select UserType1 CatalogueId" Enabled="false"
                                                        ClientValidationFunction="ValidateUserType1CatalogueIds" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                                </div>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-4 col-sm-4 ">
                                            <div class="col-md-5">
                                                <asp:CheckBox ID="chkUserType1" runat="server" Text="Include VAT" />
                                            </div>
                                            <div class="col-md-7" id="dvAddCustomFields1" runat="server" style="display: none;">
                                                <input type="submit" class="btn add_cust" id="" value="Add Custom Fields" name="" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wraptrio addborder" id="dvUserType2" runat="server" clientidmode="static">
                                        <div class="col-md-4 col-sm-4  ">
                                            <label class="col-md-5">UserType 2 :</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtUserType2" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvUserType2" runat="server" ControlToValidate="txtUserType2" Enabled="false" ClientIDMode="Static"
                                                    Display="dynamic" ErrorMessage="Please enter User Type 2"
                                                    Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <%--<div class="col-md-4 col-sm-4 type_select  ">
                                            <label class="col-md-5">CatalogueId :</label>
                                            <div class="col-md-7">
                                                <div class="select-style selectpicker">
                                                    <asp:DropDownList ID="ddlUserType2CatalogueIds" runat="server" ClientIDMode="Static">
                                                        <asp:ListItem Text="Select CatalogueId" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:CustomValidator ID="cvUserType2CatalogueIds" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select UserType2 CatalogueId" Enabled="false"
                                                        ClientValidationFunction="ValidateUserType2CatalogueIds" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                                </div>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-4 col-sm-4 ">
                                            <div class="col-md-5">
                                                <asp:CheckBox ID="chkUserType2" runat="server" Text="Include VAT" />
                                            </div>
                                            <div class="col-md-7" id="dvAddCustomFields2" runat="server" style="display: none;">
                                                <input type="submit" class="btn add_cust" id="" value="Add Custom Fields" name="" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wraptrio addborder" id="dvUserType3" runat="server" clientidmode="static">
                                        <div class="col-md-4 col-sm-4  ">
                                            <label class="col-md-5">UserType 3 :</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtUserType3" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvUserType3" runat="server" ControlToValidate="txtUserType3" Enabled="false" ClientIDMode="Static"
                                                    Display="dynamic" ErrorMessage="Please enter User Type 3"
                                                    Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <%--<div class="col-md-4 col-sm-4 type_select  ">
                                            <label class="col-md-5">CatalogueId :</label>
                                            <div class="col-md-7">
                                                <div class="select-style selectpicker">
                                                    <asp:DropDownList ID="ddlUserType3CatalogueIds" runat="server" ClientIDMode="Static">
                                                        <asp:ListItem Text="Select CatalogueId" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:CustomValidator ID="cvUserType3CatalogueIds" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select UserType3 CatalogueId" Enabled="false"
                                                        ClientValidationFunction="ValidateUserType3CatalogueIds" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                                </div>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-4 col-sm-4 ">
                                            <div class="col-md-5">
                                                <asp:CheckBox ID="chkUserType3" runat="server" Text="Include VAT" />
                                            </div>
                                            <div class="col-md-7" id="dvAddCustomFields3" runat="server" style="display: none;">
                                                <input type="submit" class="btn add_cust" id="" value="Add Custom Fields" name="" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <ul class="button_section">
                        <li>
                            <asp:Button ID="cmdSaveClose" runat="server" CssClass="btn" Text="Update" ValidationGroup="OnClickSubmit" CommandName="SaveClose" OnClick="cmdSave_Click" />
                        </li>

                        <li>
                            <asp:Button ID="cmdCancel" runat="server" CssClass="btn gray" Text="Cancel" OnClick="cmdCancel_Click" />
                        </li>

                    </ul>
                    <asp:ValidationSummary ID="valsumStore" runat="server" CssClass="ErrorText"
                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="OnClickSubmit" />
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnStoreId" runat="server" ClientIDMode="Static" Value="0" />
</asp:Content>
