﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master"
    Inherits="Presentation.Admin_Settings_ManageCSS" EnableViewState="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<style type="text/css">
    .linkhover.active {
    color: blue !important;
}
        .linkhover{color:gray}
    </style>
    <script>
        $(function () {
            var value = $('#ContentPlaceHolder1_hdnFileName').attr('value');
            $('.dwnlod_page li a').each(function () {
                var html = $(this).html();
                if (html == value) {
                    $(this).addClass('active');
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Store Settings</li>
        </ol>
    </div>
    <div class="admin_page">
        <div class="container  ">
            <div class="wrap_container">
                <h3>Manage CSS</h3>
                <div class="content dwnlod_page">
                    <ul>
                        <asp:Repeater ID="rptCssList" runat="server" OnItemCommand="rptCssList_ItemCommand">
                            <ItemTemplate>
                                <li>
                                    <asp:LinkButton ID="lnkFileName" runat="server" CssClass="linkhover" CommandArgument='<%# Eval("FileName") %>' Text='<%# Eval("FileName")%>'></asp:LinkButton>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:HiddenField ID="hdnFileName" runat="server" />
                    </ul>
                    <ol>
                        <li>
                            <asp:TextBox ID="txtCSSContent" runat="server" Height="510px" Columns="150" TextMode="MultiLine"></asp:TextBox>
                              <asp:Button ID="btnBackup" runat="server" Text="BACKUP" class="save upload_new" OnClick="btnBackup_Click" />
                             <asp:Button ID="btnRestore" runat="server" Text="RESTORE" class="save upload_new" OnClick="btnRestore_Click" />
                            <asp:Button ID="btnSave" runat="server" Text="Save CSS" class="save upload_new" OnClick="btnSave_Click" />
                            <asp:Button ID="btnShowFont" runat="server" Text="Updated Fonts" class="save upload_new" OnClick="btnShowFont_Click" Visible="false" />
                        </li>
                        <li></li>
                    </ol>
                    <ul>
                        <li>
                            <asp:FileUpload ID="fuUploadTemplate" runat="server" />
                            <asp:Button ID="cmdUploadCSS" runat="server" Text="Upload CSS" class="save upload_new" OnClick="cmdUploadCSS_Click" />
                        </li>
                        <li>
                            <asp:Button ID="cmdDownloadCSS" runat="server" Text="Download CSS" class="save upload_new" OnClick="cmdDownloadCSS_Click" />
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
