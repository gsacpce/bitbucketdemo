﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Settings_RegistrationFields" %>

<script runat="server">

   
    
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=Hostadmin %>login/login.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Manage System Text</li>
        </ul>
    </div>
    <div class="admin_page">

        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-5">
                            <h3 class="mainHead">Manage Registration Text</h3>
                        </div>
                        <div class="col-md-7  text-right">
                            <label class="col-md-7 text-right sm_select" id="select_lang">
                                Select Language :
                            </label>
                            <div style="float: right" id="divLang" runat="server" class="col-md-5">
                                <div class="select-style  selectpicker languageDropdown">
                                    <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <span class="sectionText"></span>
                        <div class="clearfix"></div>
                        <div class="col-md-12 manage_st_content">
                            <div class="categoriesmainBlock" style="padding: 0;">
                                <div id="divMessage" visible="false" runat="server">
                                    <div class="alert alert-success">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>
                                            <asp:Literal ID="ltrMessage" runat="server" /></strong>
                                    </div>
                                </div>
                                <div class="categoryMidlBlock">
                                    <div class="midleDrgblecontin">
                                        <ul class="dragblnextBlock" id="table_look">
                                            <li>
                                                <table style="width: 100%" class="dragbltop">
                                                    <tbody>
                                                        <tr>
                                                            <th class="firsttd">Key
                                                            </th>
                                                            <th class="secondtd">Value
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </li>
                                            <asp:Repeater ID="rptRegisterContent" runat="server" OnItemDataBound="rptRegisterContent_ItemDataBound">
                                                <ItemTemplate>
                                                    <li class="firstLitab">
                                                        <table style="width: 100%" class="dragbltop">
                                                            <tr>
                                                                <td class="firsttd">
                                                                    <span>
                                                                        <%# Convert.ToString(DataBinder.Eval(Container.DataItem, "LabelEnglish")).Replace("_"," ") %>
                                                                        <input type="hidden" id="hdnResourceLanguageId" runat="server" value='<%# DataBinder.Eval(Container.DataItem, "RegistrationLanguageId") %>' />
                                                                    </span>
                                                                </td>
                                                                <td class="secondtd">
                                                                    <asp:TextBox ID="txtContent" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LabelTitle") %>'>
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                                <div class="buttonPanel container">
                                    <ul class="button_section">
                                        <li>
                                            <asp:Button ID="btnSave" runat="server" OnClientClick="javascript:ShowLoader();"  CssClass="btn" Text="Save & Exit" OnClick="btnSave_Click"/>
                                        </li>
                                        <li>
                                            <asp:Button ID="btnCancel" CssClass="btn gray" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

