﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master"  AutoEventWireup="true" Inherits="Presentation.Admin_Settings_FontSettings" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <script type="text/javascript">
            $('#btnFontSubmit').click(function () {
                if (fileUpload.value.length == 0) {    // CHECK IF FILE(S) SELECTED.
                    alert('No files selected.');
                    return false;
                }
            });
</script>
         <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
       <%-- <style type="text/css">
        #dvFonts p {
            font:13px tahoma, arial;
        }
        #divFile h3 {
            font:16px arial, tahoma;
            font-weight:bold;
        }
    </style>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Font Settings</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Font Settings
                        </h3>
                        <div>
                            <section class="all_customer dragblnextBlock manage_pro">
                                <div class="first_select">
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlFontSettingType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFontSettingType_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Typekit" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Fonts" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="dvTypeKit" runat="server" visible="false" class="typekitlable">
                                    <asp:Label ID="lblTypekit" runat="server" Text="Enter Typekit URL (e.g. https://use.typekit.net/vcr1qvd.js)"></asp:Label>

                                    <asp:TextBox ID="txtTypekit" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvTypekit" runat="server" ControlToValidate="txtTypekit" Display="Static" ForeColor="Red"
                                        Text="Please Enter Typekit URL" ValidationGroup="ValidateTypekit"></asp:RequiredFieldValidator>
                                    <br />
                                    <asp:Button ID="btnTypekitSubmit" runat="server" ValidationGroup="ValidateTypekit" OnClick="btnTypekitSubmit_Click" Text="Submit" CssClass="save" />
                                    <asp:Button ID="btnTypekitCSS" runat="server" Text="update CSS" CssClass="save" OnClick="btnShowCSS_Click" Visible="false" />
                                </div>
                                <div id="dvFonts" runat="server" visible="false">
                                    <asp:Label ID="lblFonts" runat="server" Text="Upload Fonts ( Font file must be copied in folder name <b>Fonts</b>. )"></asp:Label><%--Compress that folder with file name <b>FontUpload.zip</b> to upload.--%> 
                                    <asp:FileUpload ID="fuFonts" multiple="true"  runat="server" />
                                    <br />
                                    <asp:Button ID="btnFontSubmit" runat="server" OnClick="btnFontSubmit_Click" Text="Submit" CssClass="save" />
                                    <asp:Button ID="btnShowCSS" runat="server" Text="Update CSS" CssClass="save" OnClick="btnShowCSS_Click" Visible="false" />
                                      <p><asp:label id="lblFileList" runat="server"></asp:label></p>
                                       <p><asp:Label ID="lblUploadStatus" runat="server"></asp:Label></p>
                                        <p><asp:Label ID="lblFailedStatus" runat="server"></asp:Label></p>
                                    <br />
                                    <%--<div id="dvFontCSS" runat="server" visible="false">
                                        <asp:Label ID="lblFontCSS" runat="server" Text="Please download font.css file and update newly added fonts in the file."></asp:Label>
                                        <br />
                                        <asp:LinkButton ID="lnkFontCSS" ForeColor="Red" runat="server" Text="Download Font CSS" OnClick="lnkFontCSS_Click"></asp:LinkButton>
                                        <br />
                                        <asp:Label ID="lblUploadFontCSS" runat="server" Text="Upload updated Font CSS"></asp:Label>
                                        <asp:FileUpload ID="fuFontCSS" runat="server" />
                                        <asp:Label ID="lblFontNote" runat="server" Text="Size not more than 1.00 MB" ForeColor="Red"></asp:Label>
                                        <br />
                                        <asp:Button ID="btnFontCSS" runat="server" Text="Submit" OnClick="btnFontCSS_Click" CssClass="save" />
                                       
                                    </div>--%>
                                    <asp:GridView ID="grdView" runat="server" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="FileName" HeaderText="FileName" />
                                             <asp:BoundField DataField="FilePath" HeaderText="FilePath" />
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID = "lnkDelete" Text = "Delete" CommandArgument = '<%# Eval("FileName") %>' runat = "server" OnClick="lnkDelete_Click" />
                                                   
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                    
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

