﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true"
    Inherits="Presentation.Admin_Settings_StoreSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="<%=hostAdmin %>JS/StoreCreator.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var input_fieldtxtB2CVatPercentage = document.getElementById('txtB2CVatPercentage');

            input_fieldtxtB2CVatPercentage.addEventListener('change', function () {
                var v = parseFloat(this.value);
                if (isNaN(v)) {
                    this.value = '';
                } else {
                    this.value = v.toFixed(2);
                }
            });

            var input_fieldtxtPoints = document.getElementById('txtPoints');

            input_fieldtxtPoints.addEventListener('change', function () {
                var v = parseFloat(this.value);
                if (isNaN(v)) {
                    this.value = '';
                } else {
                    this.value = v.toFixed(4);
                }
            });
        })
        function validateFloatKeyPress(el, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var number = el.value.split('.');
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            //just one dot (thanks ddlab)
            if (number.length > 1 && charCode == 46) {
                alert(" Only 1 dot allowed. ");
                return false;
            }
            //get the carat position
            var caratPos = getSelectionStart(el);
            var dotPos = el.value.indexOf(".");
            if (caratPos > dotPos && dotPos > -1 && (number[1].length > 3)) {
                alert(" You can enter Decimal upto 4 places. ");
                return false;
            }
            return true;
        }
        function getSelectionStart(o) {
            if (o.createTextRange) {
                var r = document.selection.createRange().duplicate()
                r.moveEnd('character', o.value.length)
                if (r.text == '') return o.value.length
                return o.value.lastIndexOf(r.text)
            } else return o.selectionStart
        }

        function validateNumerics(key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            //comparing pressed keycodes

            if (keycode > 31 && (keycode < 48 || keycode > 57) && keycode != 47) {
                alert(" You can enter only Numbers 0 to 9. ");
                return false;
            }
            else return true;
        }
    </script>

    <style>
        .select_data .wrap_lang {
            overflow: hidden;
        }

        .notDisplay {
            visibility: hidden;
        }

        .display {
            visibility: visible;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Store Settings</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <div class="content">
                    <h3 class="mainHead">Store Settings</h3>
                    <div id="divMessage" visible="false" runat="server" class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>
                            <asp:Literal ID="ltrMessage" runat="server" /></strong>

                    </div>
                    <div class="select_data">
                        <div class="row details">
                            <div class="col-md-2">
                                <h6>Store Settings:</h6>
                            </div>
                            <div class="col-md-10">
                                <div class="wrap_lang create02 no_borderbtm">
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="ltrSearchSetting" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div6" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rdoSearchSetting" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" id="dvDisplayType" clientidmode="static">
                                        <h5>Product Listing Type:</h5>
                                        <ul>
                                            <li>
                                                <div class="col-md-3">
                                                    <asp:Literal ID="lblPLDisplayTypeHeading" runat="server"></asp:Literal>:
                                                </div>
                                                <div class="col-md-2 text-center">Is Default </div>
                                            </li>
                                            <asp:Repeater ID="rptPLDisplayType" runat="server" OnItemDataBound="rptPLDisplayType_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-3">
                                                            <asp:CheckBox ID="chkPLDisplayType" CssClass="chkPLDisplayType" runat="server" />
                                                            <asp:HiddenField ID="hdnPLDisplayTypeStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                        <div id="dvDefaultDisplayType" runat="server" clientidmode="static" class="col-md-2 text-center radio radio-danger radio-storeType">

                                                            <input id="rbPLDisplayType" runat="server" type="radio" name="DefaultDisplayType" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <asp:CustomValidator ID="cvDisplayType" runat="server" Text="(Required)" ErrorMessage="Please select at least one Display Type"
                                            ClientValidationFunction="ValidateDisplayType" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                    </div>
                                    <div class="wrap_creat02">

                                        <h5>
                                            <asp:Literal ID="lblPaginationHeading" runat="server"></asp:Literal>:</h5>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblProductPagination" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="dvProductsPerPage" runat="server">
                                            <label>Select number of products per page: </label>
                                            <asp:TextBox ID="txtProductsPerPage" ClientIDMode="Static" runat="server"></asp:TextBox>
                                            <asp:CustomValidator ID="cvProductsPerPage" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter no. products per page"
                                                ClientValidationFunction="ValidateProductsPerPage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_creat02" id="dvPLSorting" clientidmode="static">
                                        <h5>Default Settings:</h5>
                                        <ul>
                                            <li>
                                                <div class="col-md-4">
                                                    <asp:Literal ID="lblPLSortingHeading" runat="server"></asp:Literal>:
                                                </div>
                                                <div class="col-md-2 text-center">Is Default </div>
                                            </li>
                                            <asp:Repeater ID="rptPLSorting" runat="server" OnItemDataBound="rptPLSorting_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-4">
                                                            <asp:CheckBox ID="chkPLSorting" CssClass="chkPLSorting" runat="server" />
                                                            <asp:HiddenField ID="hdnPLSortingStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                        <div id="dvPLSorting" runat="server" clientidmode="static" class="col-md-2 text-center radio radio-danger radio-storeType">
                                                            <input id="rbPLSorting" runat="server" type="radio" name="PLSorting" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="setting_newmargin">
                                            <div class="col-md-6 col-sm-4 marright_label1 ">
                                                <asp:CheckBox ID="chkQuickView" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnPLQuickViewStoreFeatureDetailId" runat="server" />
                                            </div>
                                            <div class="col-md-6 col-sm-4 marright_label1">
                                                <asp:CheckBox ID="chkProductComparison" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnPLProductComparisonStoreFeatureDetailId" runat="server" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <asp:CustomValidator ID="cvPLSorting" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please select atleast one sorting"
                                            ClientValidationFunction="ValidateSorting" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                    </div>

                                    <div class="wrap_creat02 marright_label2" id="dvPLFilters" clientidmode="static">
                                        <ul>
                                            <li>
                                                <div class="col-md-4">
                                                    <asp:Literal ID="lblPLFilterHeading" runat="server"></asp:Literal>:
                                                </div>
                                            </li>
                                            <%--<li>
                                                <div class="col-md-4">
                                                    <asp:CheckBox ID="chkEnableFilter" ClientIDMode="Static" Checked="false" runat="server" Text="Enable/Disable All Filters" />
                                                </div>
                                            </li>--%>
                                            <asp:Repeater ID="rptPLFilters" runat="server" OnItemDataBound="rptPLFilters_ItemDataBound">
                                                <ItemTemplate>
                                                    <li id="liFilter" runat="server">
                                                        <div class="col-md-4">
                                                            <asp:CheckBox ID="chkPLFilters" CssClass="chkPLFilter" runat="server" />
                                                            <asp:HiddenField ID="hdnPLFilterStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin ">

                                        <li>
                                            <h5 class="col-md-12 pro_detail">Product Details Settings:</h5>
                                        </li>
                                        <br />
                                        <div class="col-md-6 col-sm-6">
                                            <asp:HiddenField ID="hdnPDPrintStoreFeatureDetailId" runat="server" />
                                            <asp:CheckBox ID="chkPDPrint" runat="server" TextAlign="Left" />

                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:HiddenField ID="hdnPDEmailStoreFeatureDetailId" runat="server" />
                                            <asp:CheckBox ID="chkPDEmail" runat="server" TextAlign="Left" />

                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:HiddenField ID="hdnPDDownloadImageStoreFeatureDetailId" runat="server" />
                                            <asp:CheckBox ID="chkPDDownloadImage" runat="server" TextAlign="Left" />

                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDSocialMedia" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDSocialMediaStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDRecentlyViewedProducts" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDRecentlyViewedProductsStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDYouMayAlsoLike" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDYouMayAlsoLikeStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDWishlist" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDWishlistStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDReviewRating" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDReviewRatingStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDSavePDF" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDSavePDFStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPDSectionIcons" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPDSectionIconsStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6" style="display: none;">
                                            <asp:CheckBox ID="chkPCustomRequestForm" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnPCustomRequestForm" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Store Settings:</h5>
                                        <br />
                                        <%--<div class="col-md-12 col-sm-12">--%>
                                        <%--<asp:CheckBox ID="chkEnablePoints" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                            <asp:HiddenField ID="hdnEnablePoints" runat="server" />

                                            <div id="dvPoints" runat="server" clientidmode="static" class="enable_text">
                                                <asp:TextBox ID="txtPoints" runat="server"></asp:TextBox>
                                            </div>

                                            <div class="clearfix"></div>--%>
                                        <table>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <div class="col-md-12 col-sm-6 marright_label1 marginLeft">
                                                        <asp:CheckBox ID="chkEnablePoints" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                                        <asp:HiddenField ID="hdnEnablePoints" runat="server" />
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-6 marright_label1 marginLeft">
                                                        <asp:CheckBox ID="ChkInvoiceAccountStatus" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                                        <asp:HiddenField ID="hdnInvoiceAccountStatus" runat="server" />
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div id="dvPoints" runat="server" clientidmode="static" class="enable_text" visible="false">
                                                        1 point :
                                                    <asp:TextBox ID="txtPoints" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                        <asp:Literal ID="ltrSelectedCurrency" runat="server" Text=""></asp:Literal>
                                                        <%--<asp:RequiredFieldValidator ID="rfvPointData" runat="server" ControlToValidate="txtPoints" Text="(Required)" ErrorMessage="Enter point value" Display="Dynamic" ValidationGroup="OnClickSubmit" ForeColor="Red">
                                                    </asp:RequiredFieldValidator>--%>
                                                        <asp:CustomValidator ID="cvPointData" runat="server" Text="(Required)" ErrorMessage="Please enter point value"
                                                            ClientValidationFunction="ValidatePointData" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                                    </div>
                                                    <%--</div>--%>
                                                    <div class="clearfix"></div>
                                                </td>
                                                <td style="width: 350px;">
                                                    <ul>
                                                        <li>
                                                            <div class="col-md-3 text-center">
                                                                <asp:Label ID="lblPointValueHeader" runat="server" Text="Point Value" ClientIDMode="Static"></asp:Label>
                                                                <%--<div id="divPointValueHeader" runat="server" clientidmode="static">Point Value</div>--%>
                                                            </div>
                                                            <div class="col-md-3 text-center">
                                                                <asp:Label ID="lblCurrencyHeader" runat="server" Text="Currency" ClientIDMode="Static"></asp:Label>
                                                                <%--  <div id="divCurrencyHeader" runat="server" clientidmode="static">Currency</div>--%>
                                                            </div>
                                                            <div class="col-md-3 text-center">
                                                                <asp:Label ID="lblInvoiceIDHeader" runat="server" Text="Invoice ID" ClientIDMode="Static"></asp:Label>
                                                                <%-- <div id="divInvoiceIDHeader" runat="server" clientidmode="static">Invoice ID</div>--%>
                                                            </div>
                                                        </li>
                                                        <asp:Repeater ID="rptPLCurrencyPoint" runat="server" OnItemDataBound="rptPLCurrencyPoint_ItemDataBound">
                                                            <ItemTemplate>
                                                                <li>
                                                                    <div class="col-md-3 text-center">
                                                                        <%-- id="divPointValue" runat="server" clientidmode="static"--%>
                                                                        <asp:TextBox runat="server" ID="txtPointValue" Width="80px" ClientIDMode="Static" onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>
                                                                        <%-- <asp:RegularExpressionValidator ID="rgxPointValue" runat="server" ValidationExpression="((\d+)+(\.\d+))$"
                                                                            ErrorMessage="Please enter valid decimal number with any decimal places." ControlToValidate="txtPointValue" />--%>
                                                                    </div>
                                                                    <div class="col-md-3 text-center">
                                                                        <%-- id="divCurrency" runat="server" clientidmode="static"--%>
                                                                        <asp:Label runat="server" ID="lblCurrencySymbol" Width="80px" ClientIDMode="Static"></asp:Label>
                                                                        <asp:HiddenField ID="hdnCurrencyId" runat="server" />
                                                                    </div>
                                                                    <div class="col-md-3 text-center">
                                                                        <%-- id="divInvoiceID" runat="server" clientidmode="static"--%>
                                                                        <asp:TextBox runat="server" ID="txtInvoiceID" Width="80px" MaxLength="10" ClientIDMode="Static" onkeypress="return validateNumerics(event)"></asp:TextBox>
                                                                        <%--<asp:RegularExpressionValidator ID="rgxInvoiceID" runat="server"
                                                                            ControlToValidate="txtInvoiceID" ErrorMessage="Enter only 0-9 numbers."
                                                                            ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>--%>
                                                                    </div>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="ltrHomeLink" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div5" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rdoLinkPosition" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="liPaymentGatewayHeading" clientidmode="static" runat="server" class="wrap_creat02">
                                        <br />
                                        <ul>
                                            <li>
                                                <div class="col-md-4">
                                                    <asp:Literal ID="lblSCPaymentGatewayHeading" runat="server"></asp:Literal>:
                                                </div>
                                                <div class="col-md-2 text-left">Is Default </div>
                                            </li>
                                            <asp:Repeater ID="rptSCPaymentGateway" runat="server" OnItemDataBound="rptSCPaymentGateway_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="col-md-4">
                                                            <asp:CheckBox ID="chkSCPaymentGateway" CssClass="chkSCPaymentGateway marright_label2" runat="server" />
                                                            <asp:HiddenField ID="hdnSCPaymentGatewayStoreFeatureDetailId" runat="server" />
                                                        </div>
                                                        <div id="dvSCPaymentGateway" runat="server" clientidmode="static" class="col-md-1 text-center radio radio-danger radio-storeType">
                                                            <input id="rbSCPaymentGateway" runat="server" type="radio" name="SCPaymentGateway" clientidmode="static" />
                                                            <label></label>
                                                        </div>
                                                        <div id="dvCheckBoxListCurrency" runat="server" clientidmode="Static">
                                                            <asp:CheckBoxList ID="CheckBoxListCurrency" runat="server" ClientIDMode="Static"></asp:CheckBoxList>
                                                        </div>
                                                        <%--<div class="col-md-7" style="display: none;" id="dvPointData" clientidmode="static">
                                                            <asp:TextBox ID="txtPointData" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvPointData" Enabled="false" runat="server" ControlToValidate="txtPointData" Text="(Required)" Display="Dynamic" ValidationGroup="OnClickSubmit" ForeColor="Red">
                                                            </asp:RequiredFieldValidator>
                                                        </div>--%>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <asp:CustomValidator ID="cvSCPaymentGateway" runat="server" Text="(Required)" ErrorMessage="Please select at least one payment gateway"
                                            ClientValidationFunction="ValidateSCPaymentGateway" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                    </div>

                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">

                                        <%--<div class="col-md-12 col-sm-12">--%>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkEnableCouponCode" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnEnableCouponCode" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <%--</div>--%>
                                        <div class="clearfix"></div>
                                    </div>


                                    <%--Added by Snehal 22 09 2016--%>
                                    <div class="wrap_creat02 pro_detail_check  setting_newmargin">
                                        <h5 class="pro_detail">Punchout Settings:</h5>
                                        <br />
                                        <div class="col-md-12 col-sm-6 marright_label1 marginLeft">
                                            <asp:CheckBox ID="chkIsPunchoutRegister" runat="server" Text="IsPunchout Registration" TextAlign="Left" ClientIDMode="static" />
                                            <asp:HiddenField ID="hdnIsPunchoutReg" runat="server" />

                                            <div class="clearfix"></div>

                                        </div>

                                        <div class="col-md-12 col-sm-6 marright_label1 marginLeft">

                                            <asp:CheckBox ID="chkPunchout" runat="server" Text="Allow Punchout" TextAlign="Left" ClientIDMode="static" />
                                            <div class="clearfix"></div>


                                            <div class="col-md-6 col-sm-6" id="divCustomPunchout" style="display: none;" clientidmode="static">
                                                <%--<asp:CheckBox ID="chkCustomPunchout" runat="server" Text="Custom Punchout" TextAlign="Left" />--%>
                                                <asp:RadioButtonList ID="rdoCustomPunchout" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                                <asp:HiddenField ID="hdnCustomPunchout" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 col-sm-6 marright_label1 marginLeft" style="display: none;" id="divHideCurrDD" runat="server" clientidmode="static">
                                            <asp:CheckBox ID="chkHideCurrDD" runat="server" Text="Hide Currency dropdown" TextAlign="Left" ClientIDMode="static" />
                                            <asp:HiddenField ID="hdnHideCurrDD" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 col-sm-6 marright_label1 marginLeft" style="display:none;" id="HidePO_OrderDetails" runat="server" clientidmode="static">
                                            <asp:CheckBox ID="ChkHidePO_OrderDetails" runat="server" Text="Hide Currency dropdown" TextAlign="Left" ClientIDMode="static" />
                                            <asp:HiddenField ID="hdnHidePO_OrderDetails" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <%--</div>--%>
                                        <div class="clearfix"></div>
                                    </div>

                                    <%--Added by Snehal 23 09 2016--%>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">

                                        <%--<div class="col-md-12 col-sm-12">--%>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkMaxAllowQty" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                            <asp:HiddenField ID="hdnMaxOrderQtyStoreFeatureDetailId" runat="server" />

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div7" runat="server">
                                            <label>Max Allowable Order Qty / Item: </label>
                                            <asp:TextBox ID="txtAllowMaxOrder" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:CustomValidator ID="CustomValidator1" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter no. products per page"
                                                ClientValidationFunction="ValidateProductsPerPage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                        </div>
                                        <%--</div>--%>
                                        <div class="clearfix"></div>
                                    </div>



                                    <%--Added by Sripal--%>

                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">

                                        <h5 class="col-md-12 pro_detail">Orders Settings:</h5>

                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkAllowBackOrder" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnODAllowBackOrderStoreFeatureDetailId" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 num_size " style="display: none;">
                                                <label>no of days for backorder: </label>
                                                <asp:TextBox ID="txtAllowBackOrder" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                <%--<asp:CustomValidator ID="cvAllowBackOrder" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter no. of days for backorder"
                                                ClientValidationFunction="ValidateAllowBackOrder" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit""></asp:CustomValidator>--%>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <%--Start Added BY Nilesh--%>
                                    <%--<div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Invoice settings:</h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkInvoiceAccountStatus" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnInvoiceAccountStatus" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>--%>
                                    <%--End--%>
                                    <%-- START Added By Hardik "04/Oct/2016"--%>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Email copy of basket Button settings:</h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkEmailCopyBasketBtn" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnEmailCopyBasketBtn" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Stock Due Date Value:</h5>
                                        <br />
                                        <div class="create02 no_borderbtm store_access noBorder">
                                            <div class="col-md-3 col-sm-3">
                                                <asp:Label ID="lblStockDueDate" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 num_size ">
                                                <asp:TextBox ID="txtStockDueDate" runat="server" MaxLength="3" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvStockDueDate" runat="server" ControlToValidate="txtStockDueDate"
                                                    Display="dynamic" ErrorMessage="Please enter stock due date"
                                                    Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:HiddenField ID="hdnStockDueDate" runat="server" />
                                                <asp:RangeValidator ID="rvStockDueDate" runat="server" ControlToValidate="txtStockDueDate" Type="Integer" MinimumValue="0"
                                                    MaximumValue="365" ErrorMessage="Enter a valid value" Text="(Enter value between 0 - 365)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RangeValidator>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Order History Months to Return:</h5>
                                        <br />
                                        <div class="create02 no_borderbtm store_access noBorder">
                                            <div class="col-md-3 col-sm-3">
                                                <asp:Label ID="lblOrderHistoryMonthstoReturn" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 num_size ">
                                                <asp:TextBox ID="txtOrderHistoryMonthstoReturn" runat="server" MaxLength="2" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvOrderHistoryMonthstoReturn" runat="server" ControlToValidate="txtOrderHistoryMonthstoReturn"
                                                    Display="dynamic" ErrorMessage="Please enter stock due date"
                                                    Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:HiddenField ID="hdnOrderHistoryMonthstoReturn" runat="server" />
                                                <asp:RangeValidator ID="rvOrderHistoryMonthstoReturn" runat="server" ControlToValidate="txtOrderHistoryMonthstoReturn" Type="Integer" MinimumValue="1"
                                                    MaximumValue="12" ErrorMessage="Enter a valid value" Text="(Enter value between 1 - 12)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RangeValidator>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Communigator Group Id :</h5>
                                        <br />
                                        <div class="create02 no_borderbtm store_access noBorder">
                                            <div class="col-md-3 col-sm-3">
                                                <asp:Label ID="lblCommunigatorGroupId" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 num_size setting_communigator">
                                                <asp:TextBox ID="txtCommunigatorGroupId" runat="server" Width="100 px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqCommunigatorGroupId" runat="server" ControlToValidate="txtCommunigatorGroupId"
                                                    Display="dynamic" ErrorMessage="Please Enter Communigator Group Id"
                                                    Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:HiddenField ID="hdnCommunigatorGroupId" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Product Enquiry color box:</h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkProductEnquiryColor" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnProductEnquiryColor" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Enable Total Net</h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkEnableTotalNet" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnEnableTotalNet" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Reg Data for Custom UDFS</h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkCustomUDFSRegData" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnCustomUDFSRegData" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">Submit Enquiry [No Order Placing]: </h5>
                                        <br />
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkSubmitEnquiry" runat="server" TextAlign="Left" />
                                                <asp:HiddenField ID="hdnSubmitEnquiry" runat="server" />
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-12 pro_detail">
                                            <asp:Literal ID="lblCategoryDisplay" runat="server"></asp:Literal>:</h5>
                                        <div class="no_borderbtm">
                                            <div class="col-md-6 col-sm-6 num_size " id="DivCategoryDisplay" runat="server">
                                                <div class="radio_data radio radio-danger">
                                                    <asp:RadioButtonList ID="rblCategoryDisplay" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">
                                        <h5 class="col-md-6 col-sm-6">
                                            <asp:Literal ID="lblBasketPopup" runat="server"></asp:Literal>:</h5>
                                        <div class="col-md-6 col-sm-6 num_size no_borderbtm" id="DivBasketPopup" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblBasketPopup" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <%-- END Added By Hardik --%>
                                    <%--Indeed Code --%>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">

                                        <h5 class="col-md-12 pro_detail">Projected Sales :</h5>
                                        <br />

                                        <table>
                                            <tr>

                                                <td style="width: 350px;">
                                                    <ul>
                                                        <li>
                                                            <div class="col-md-3 text-center">
                                                                <asp:Label ID="Label4" runat="server" Text="Projected Sales" ClientIDMode="Static"></asp:Label>
                                                            </div>
                                                            <div class="col-md-3 text-center">
                                                                <asp:Label ID="Label5" runat="server" Text="Currency" ClientIDMode="Static"></asp:Label>
                                                            </div>
                                                        </li>
                                                        <asp:Repeater ID="rptProjectedSale" runat="server" OnItemDataBound="rptProjectedSale_ItemDataBound">
                                                            <ItemTemplate>
                                                                <li>
                                                                    <div class="col-md-3 text-center">
                                                                        <asp:TextBox runat="server" ID="txtProjected" Width="80px" ClientIDMode="Static" onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-md-3 text-center">
                                                                        <asp:Label runat="server" ID="lblProjCurrSymbol" Width="80px" ClientIDMode="Static"></asp:Label>
                                                                        <asp:HiddenField ID="hdnProjCurrId" runat="server" />
                                                                    </div>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="wrap_creat02 pro_detail_check marright_label1 setting_newmargin">

                                        <h5 class="col-md-12 pro_detail">Birthday and Anniversary Points :</h5>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <asp:Label ID="Label6" runat="server" Text="Birthday Point"></asp:Label>
                                            </div>

                                            <div class="col-md-6 col-sm-6 num_size">
                                                <asp:TextBox ID="txtBirthdayPoint" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                <asp:HiddenField ID="hdnBirthday" runat="server" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <asp:Label ID="Label7" runat="server" Text="Anniversary Point"></asp:Label>
                                            </div>

                                            <div class="col-md-6 col-sm-6 num_size">
                                                <asp:TextBox ID="txtAnnvPoint" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                <asp:HiddenField ID="hdnAnniversary" runat="server" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="create02 setting_newmargin">
                                <div class="col-md-2 ">
                                    <h6>User Settings:</h6>
                                </div>
                                <div class="col-md-10 ">
                                    <div class="wrap_creat02 wrap_lang store_access">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="lblCRStoreAccessHeading" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size " id="Div1" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRStoreAccess" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h6>User Settings:</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="wrap_lang create02 marright_label1 ">
                                        <div class="wrap_creat02">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkCREnableDomainValidation" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                            </div>
                                            <div class="col-md-6 col-sm-6  " id="dvCREnableDomainValidation" runat="server" clientidmode="static">
                                                <div class="radio_data radio radio-danger">
                                                    <asp:RadioButtonList ID="rblCREnableDomainValidation" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkCREnableEmailValidation" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                            </div>
                                            <div class="col-md-6 col-sm-6 " id="dvCREnableEmailValidation" runat="server" clientidmode="static">
                                                <div class="radio_data radio radio-danger">
                                                    <asp:RadioButtonList ID="rblCREnableEmailValidation" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02 no_borderbtm">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="ChkCOnfirmationEmail" runat="server" TextAlign="Left" ClientIDMode="Static" />
                                                <asp:HiddenField ID="hdnCOemail" runat="server" />
                                            </div>
                                            <div class="col-md-6 col-sm-6  " id="Div8" runat="server" clientidmode="static">
                                                <div class="radio_data radio radio-danger">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h6>Password Rules:</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="wrap_lang create02 no_borderbtm store_access">
                                        <div class="col-md-3 col-sm-3">
                                            <asp:Label ID="lblPSMinimumPasswordLength" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size">
                                            <asp:TextBox ID="txtPSMinimumPasswordLength" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPSMinimumPasswordLength" runat="server" ControlToValidate="txtPSMinimumPasswordLength"
                                                Display="dynamic" ErrorMessage="Please enter minimum password length"
                                                Text="(Required)" ValidationGroup="OnClickSubmit" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:HiddenField ID="hdnPSMinimumPasswordLengthStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_lang create02 store_access" style="display: none">
                                        <div class="col-md-3 col-sm-3">
                                            <asp:Label ID="lblPSPasswordExpression" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPSPasswordExpression" runat="server" class="hide" />
                                            <asp:RadioButtonList ID="rdPwdType" runat="server">
                                                <asp:ListItem Text="Only Alpha" Value="A"></asp:ListItem>
                                                <asp:ListItem Text="Only AlphaNumeric" Value="AN"></asp:ListItem>
                                                <asp:ListItem Text="Only Alpha Numeric and Symbol" Value="ANS"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:HiddenField ID="hdnPSPasswordExpressionStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Code Added by SHRIGANESH SINGH for Password Policy Start 09 MAy 2016 -->
                                    <div class="wrap_lang create02 store_access">
                                        <div class="col-md-3 col-sm-3">
                                            <asp:Label ID="lblPSPasswordPolicy" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPSPasswordPolicy" runat="server" Class="hide" />
                                            <asp:RadioButtonList ID="rdPasswordPolicyType" runat="server">
                                                <%--<asp:ListItem Text="Any Combination"></asp:ListItem>
                                                <asp:ListItem Text="Must contain Alpha Numeric"></asp:ListItem>
                                                <asp:ListItem Text="Must contain Alpha Numeric and Symbol"></asp:ListItem>--%>
                                            </asp:RadioButtonList>
                                            <asp:HiddenField ID="hdnPSPasswordPolicyStoreFeatureDetailedID" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Code Added by SHRIGANESH SINGH for Password Policy End 09 MAy 2016 -->
                                    <div class="wrap_lang create02 " style="display: none;">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblPSForcePasswordChange" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkPSForcePasswordChange" runat="server" />
                                            <asp:HiddenField ID="hdnPSForcePasswordChangeStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_lang create02" style="display: none;">
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label ID="lblPSPasswordValidity" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 num_size">
                                            <asp:TextBox ID="txtPSPasswordValidity" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            <asp:HiddenField ID="hdnPSPasswordValidityStoreFeatureDetailId" runat="server" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="wrap_lang create02" style="display: none;">
                                        <div class="col-md-6 col-sm-6">
                                            <asp:CheckBox ID="chkCREnableCaptcha" runat="server" TextAlign="Left" />
                                            <asp:HiddenField ID="hdnCREnableCaptchaStoreFeatureDetailId" runat="server" />
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h6>Store Type</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="wrap_creat02 wrap_lang store_access">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="lblCRStoreTypeHeading" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="Div2" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rblCRStoreType" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="divVatPercentage" clientidmode="Static" runat="server">
                                            <span style="vertical-align: top;">VAT :  </span>
                                            <asp:TextBox ID="txtB2CVatPercentage" runat="server" ClientIDMode="Static" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                            %
                                            <asp:CustomValidator ID="cvB2CVatPercentage" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter VAT percentage"
                                                ClientValidationFunction="ValidateB2CVatPercentage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <h6>Gift Certificate</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="wrap_creat02 wrap_lang store_access">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="ltGiftCertificate" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="Div3" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rdlIsGiftCertificateAllow" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="div4" clientidmode="Static" runat="server">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <h6>Cookies Law (EU/UK):</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="wrap_lang create02">
                                        <div class="wrap_creat02">
                                            <div class="col-md-4 col-sm-4">
                                                <asp:Label ID="lblCLEnableCookie" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkCLEnableCookie" runat="server" ClientIDMode="Static" />
                                                <asp:HiddenField ID="hdnCLEnableCookieStoreFeatureDetailId" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <div class="col-md-4">
                                                <asp:Label ID="lblCLCookieLifetime" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-6 num_size">
                                                <asp:TextBox ID="txtCLCookieLifetime" ClientIDMode="Static" runat="server" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);"></asp:TextBox>
                                                Default is
                                                <label id="days" runat="server"></label>
                                                days
                                            <asp:CustomValidator ID="cvCLCookieLifetime" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter cookie lifetime"
                                                ClientValidationFunction="ValidateCLCookieLifetime" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                                <asp:HiddenField ID="hdnCLCookieLifetimeStoreFeatureDetailId" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <div class="col-md-4">
                                                <asp:Label ID="lblCLCookieMessage" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:TextBox ID="txtCLCookieMessage" ClientIDMode="Static" runat="server" TextMode="MultiLine" Rows="2" Columns="25"></asp:TextBox>
                                                <asp:CustomValidator ID="cvCLCookieMessage" ClientIDMode="Static" runat="server" Text="(Required)" ErrorMessage="Please enter cookie message"
                                                    ClientValidationFunction="ValidateCLCookieMessage" Display="Dynamic" ForeColor="Red" ValidationGroup="OnClickSubmit"></asp:CustomValidator>
                                                <asp:HiddenField ID="hdnCookieMessageStoreFeatureDetailId" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <h6>Google API:</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="wrap_lang create02">
                                        <div class="wrap_creat02">
                                            <div class="col-md-4 col-sm-4">
                                                <asp:Label ID="Label1" runat="server">Login Page :</asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkSocial" CssClass="chkPLFilter" runat="server" ClientIDMode="Static" Text="Social" />
                                                <asp:HiddenField ID="hdnSocialId" runat="server" />

                                                <asp:CheckBox ID="chkLogin" CssClass="chkPLFilter" runat="server" ClientIDMode="Static" Text="Login" />
                                                <asp:HiddenField ID="hdnLoginId" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <div class="col-md-4 col-sm-4">
                                                <asp:Label ID="Label2" runat="server">Basket Page :</asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkBasketSocial" CssClass="chkPLFilter" runat="server" ClientIDMode="Static" Text="Social" />
                                                <asp:HiddenField ID="hdnBasketSocialLogin" runat="server" />

                                                <asp:CheckBox ID="chkBasketLogin" CssClass="chkPLFilter" runat="server" ClientIDMode="Static" Text="Login" />
                                                <asp:HiddenField ID="hdnBasketLogin" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="wrap_creat02">
                                            <div class="col-md-4 col-sm-4">
                                                <asp:Label ID="Label3" runat="server">Register Page :</asp:Label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <asp:CheckBox ID="chkRegisterSocial" CssClass="chkPLFilter" runat="server" ClientIDMode="Static" Text="Social" />
                                                <asp:HiddenField ID="hdnRegisterSocialLogin" runat="server" />

                                                <asp:CheckBox ID="chkRegisterLogin" CssClass="chkPLFilter" runat="server" ClientIDMode="Static" Text="Login" />
                                                <asp:HiddenField ID="hdnRegisterLogin" runat="server" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-2">
                                    <h6>Freight Source country [Single/Multiple]:</h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="wrap_lang create02">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>
                                                <asp:Literal ID="ltrlAllowFreightSrcCountry" runat="server"></asp:Literal>:</h5>
                                        </div>
                                        <div class="col-md-3 col-sm-3 num_size " id="Div9" runat="server">
                                            <div class="radio_data radio radio-danger">
                                                <asp:RadioButtonList ID="rdlAllowFreightSrcCountry" runat="server" RepeatDirection="Horizontal" OnChange="confirm('Previous Values Will Be Deleted');" ClientIDMode="Static">
                                                </asp:RadioButtonList>
                                                <asp:DropDownList runat="server" ID="ddlCountries" ClientIDMode="Static"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="button_section">
                    <li>
                        <asp:Button ID="cmdSaveClose" runat="server" CssClass="btn" Text="Update" ValidationGroup="OnClickSubmit" CommandName="SaveClose" OnClick="cmdSave_Click" />
                    </li>

                    <li>
                        <asp:Button ID="cmdCancel" runat="server" CssClass="btn gray" Text="Cancel" OnClick="cmdCancel_Click" />
                    </li>

                </ul>
                <asp:ValidationSummary ID="valsumStore" runat="server" CssClass="ErrorText"
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="OnClickSubmit" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnStoreId" runat="server" ClientIDMode="Static" Value="0" />
</asp:Content>

