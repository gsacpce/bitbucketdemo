﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Products_ProductFilterSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=HostAdmin %>login/login.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Product Filter Setting</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                   
                        <div class="warp_filterbtn ">
                                <asp:Button ID="btnCreate" Text="Create Index" OnClick="btnCreate_Click" runat="server" CssClass="save add_newrole" />
                       </div>
                  <div class="clear"></div>
                    <ul class="pro_filter">
                        <asp:Repeater ID="rptSettings" runat="server">
                            <HeaderTemplate>
                                <li>
                                    <table width="100%" class="dragbltop dragbltopextra">
                                        <tbody>
                                            <tr>
                                                <th class="firsttd">
                                                    <strong>Sr #</strong>
                                                </th>
                                                <th>
                                                    <strong>Title</strong>
                                                </th>
                                                <th class="secondtd">
                                                    <strong>IsShowInFilter</strong>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <table width="100%" class="dragbltop">
                                        <tbody>
                                            <tr>
                                                <td class="firsttd">
                                                    <span>
                                                        <%# Container.ItemIndex + 1 %></span>
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hdnProductDetailGroupMappingId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ProductDetailGroupMappingId") %>' />
                                                    <span id="spnName" runat="server">
                                                        <%# DataBinder.Eval(Container.DataItem, "Name") %></span>
                                                </td>
                                                <td class="secondtd">
                                                    <asp:CheckBox ID="chkIsShowInFilter" CssClass="chkfilter" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "IsShowInFilter") %>' />
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="buttonPanel container">
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSubmit" runat="server" Text="Update" OnClientClick="return validate();" OnClick="btnSubmit_Click" CssClass="btn" /></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function validate() {

            ShowLoader();
            var IsChecked = 0;
            var ctr = 0;

            $('.chkfilter :checkbox').each(function () {
                if (this.checked == true) {
                    ctr++;
                }
            });


            if ($("input:checkbox:checked").length == 0)
                IsChecked = 1;

            if (IsChecked == 0) {
                if (ctr > 5) {
                    $('#myWarningModal').find('#WarningMessage').html('Maximum 5 attributes can be shown in the filter.');
                    $('#myWarningModal').modal('show');
                    HideLoader();
                    return false;
                }
            }

            //if (IsChecked == 1)
            //    alert("Select atleast one checkbox.");

            HideLoader();
            //if (IsChecked == 1)
            //    return false;
            //else
            return true;
        }

    </script>
</asp:Content>

