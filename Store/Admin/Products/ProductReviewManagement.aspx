﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Products_ProductReviewManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../CSS/star-rating.min.css" rel="stylesheet" />
    <link href="../../CSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript">
        var host = '<%=host %>';
    </script>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Products </a></li>
            <li>Product Review Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Manage Review And Ratings
                        </h3>
                        <div>
                            <section class="all_customer dragblnextBlock manage_pro">
                                <asp:GridView ID="grdReviewAndRating" runat="server" AutoGenerateColumns="False" Width="100%"
                                    AllowPaging="true" AllowCustomPaging="true" PageSize="10" EmptyDataText="No Reviewa Found." CellPadding="2"
                                    BorderColor="#cccccc" BorderWidth="1" OnPageIndexChanging="grdReviewAndRating_PageIndexChanging"
                                    class="all_customer_inner allcutomerEtracls " OnRowCommand="grdReviewAndRating_RowCommand"
                                    OnRowDataBound="grdReviewAndRating_RowDataBound" BackColor="White">
                                    <%--AllowSorting="true" OnSorting="grdProduct_OnSorting"--%>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Title">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%# Eval("ReviewTitle")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Products">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hpkProducts" CssClass="aReview" ClientIDMode="Static" rel='<%# Eval("ReviewRatingId") %>' runat="server" Text='<%# Eval("ProductName") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rating">
                                            <ItemTemplate>
                                                <span id="spnRating1" class="glyphicon glyphicon glyphicon glyphicon-star-empty" runat="server" aria-hidden="true"></span>
                                                <span id="spnRating2" class="glyphicon glyphicon glyphicon glyphicon-star-empty" runat="server" aria-hidden="true"></span>
                                                <span id="spnRating3" class="glyphicon glyphicon glyphicon glyphicon-star-empty" runat="server" aria-hidden="true"></span>
                                                <span id="spnRating4" class="glyphicon glyphicon glyphicon glyphicon-star-empty" runat="server" aria-hidden="true"></span>
                                                <span id="spnRating5" class="glyphicon glyphicon glyphicon glyphicon-star-empty" runat="server" aria-hidden="true"></span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Posted By">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDate" runat="server" Text='<%# Eval("CreatedDate", "{0:yyyy/MM/dd}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="bg" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="view_order other_option ">
                                                            <li id="liApprove" runat="server">
                                                                <asp:LinkButton ID="lnkApproved" CommandName="A" CommandArgument='<%#Eval("ReviewRatingId")%>'
                                                                    runat="server">Approve</asp:LinkButton></li>
                                                            <li id="liDisApprove" runat="server">
                                                                <asp:LinkButton ID="lnkDisapproved" CommandName="D" CommandArgument='<%#Eval("ReviewRatingId")%>'
                                                                    runat="server">Disapprove</asp:LinkButton></li>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmptyGridTemplate" runat="server" Text="No Review Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <PagerSettings Position="TopAndBottom" Mode="NextPreviousFirstLast" FirstPageText="<<" LastPageText=">>" NextPageText=">" PreviousPageText="<" />
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
    <link href="<%=host %>CSS/star-rating.min.css" rel="stylesheet" />
    <script src="<%=host %>JS/star-rating.min.js"></script>
    <script src="<%=host %>JS/moment.js"></script>
    <script type="text/javascript">
        $(document).on('click', '.aReview', function (e) {
            //debugger;
            var ReviewRatingId = $(this).attr("rel");
            $('#myModal').find('.modal-body').html('');
            try {
                $.ajax({
                    type: "POST",
                    url: host + 'Products/ProductReviewDescription_Ajax.aspx',
                    data: { ReviewRatingId: ReviewRatingId },
                    //contentType: "application/json",

                    cache: false,
                    async: false
                    //success: function(response){Test(response)}
                    //beforeSend: function (request) {
                    // $.blockUI({ message: '<h2><img class="newloader" src="' + host + 'images/UI/ajax-loader.gif" /> Just a moment...</h2>' });

                    //}
                }).done(function (response) {
                    $('#myModal').find('.modal-body').html(response);
                    $('#myModal').find('.modal-title').html("Product Review");
                    //$('#myModal').find('.modal-footer').hide();
                    $('#myModal').modal('show');
                });
            } catch (e) { }
            finally { }
        });

        //function Test(response) {
        //    $('#myModal').find('.modal-body').html(response.d);
        //    $('#myModal').find('.modal-title').html("Product Review");
        //    //$('#myModal').find('.modal-footer').hide();
        //    $('#myModal').modal('show');

        //}

    </script>
</asp:Content>

