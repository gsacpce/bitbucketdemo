﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master"
    AutoEventWireup="true" Inherits="Presentation.Admin_AddEditProducts"
    ValidateRequest="false" EnableEventValidation="false" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var host = '<%=AdminHost %>';
    </script>
    <style type="text/css">
        h1 {
            text-decoration: line-through;
        }

        .red {
            color: #FF0000;
            padding: 7px !important;
            position: absolute;
            /* width: 100%;*/
        }

        .product_sku input[type="text"] {
            width: 100%;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />

    <script src="../JS/jquery-ui.js"></script>
    <script src="../JS/Utilities.js" type="text/javascript"></script>
    <link href="../CSS/AddEditProduct.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {


            var control = document.getElementById("txtExpirationDate");
            if (control != null) {

                if (document.getElementById("txtExpirationDate").value == "1/1/0001") {
                    console.log("in");
                    $("#txtExpirationDate").datepicker().datepicker("setDate", "0");
                }
                else { $("#txtExpirationDate").datepicker().datepicker(); }
            }

            $("#txtProductName").change(function () {
                var furtherdescription = $("#txtProductName").val();
                $("#txtFurtherDescription").val = furtherdescription;
                document.getElementById('ContentPlaceHolder1_tab_Product_tab_pan_Product_txtFurtherDescription').value = furtherdescription;
            });


            $(".paneltab").click(function () {
                $(this).toggleClass("paneltabplus");
                $(this).parent().parent().parent().parent().parent().find('ul').slideToggle("slow");
            });

            $('.chkBox input').click(function () {
                jQuery(this).parent().toggleClass('chkBox1mark');
                return false;
            });

            //            $(".CsA").find(".DetailsB").css("display", "none");
            //            $(".CsA h3").addClass('rightA');
            //            $(".CsA div").first().css("display", "block");
            //            $(".CsA h3").first().removeClass('rightA');
            //            $('.CsA h3').click(function () {
            //                $(this).next().stop().slideToggle();
            //                $(this).toggleClass('rightA');
            //            });

            $('.CsA h3').click(function () {
                $(this).next().stop().slideToggle('fast');
                $(this).toggleClass('rightA');
            });

            $("#txtProductCode").change(function () {
                ShowLoader();
                var ProductCode = $("#txtProductCode").val();
                var ProductId = $('#ContentPlaceHolder1_hdnProductId').val();
                if (ProductId == '') {
                    ProductId = "0";
                }
                if (ProductCode != "") {
                    $.ajax({
                        url: "<%=Host %>Admin/Products/AddEditProducts.aspx/CheckProductCodeExists",
                        type: "POST",
                        data: "{ProductCode:'" + ProductCode + "',ProductId:'" + ProductId + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false
                    }).done(function (resp) {
                        // alert(resp.d.IsSuccessful);
                        HideLoader();

                        if (resp.d == true) {
                            //alert('login successfully')
                            // divMessage.Attributes.Remove("class");
                            //  divMessage.Attributes.Add("class", "Errormsg");

                            // alert('in');

                            $("#divMessage").attr("class", "Errormsg");
                            // $('#page_navigation1').addClass('Errormsg');
                            $('#divMessage').html("Product code already exists.");

                            $('#divMessage').show();
                            //txtProductCode.Focus();
                        }
                        else {
                            $('#divMessage').removeClass('Errormsg');
                            $('#divMessage').html('');
                            //  txtProductName.Focus();
                        }
                    }).fail(function (jqXHR, textStatus) {
                        alert(textStatus);
                        alert("Error while processing…");
                        HideLoader();
                        return false;
                    });
                }
                else {

                    $('#divMessage').removeClass('Errormsg');
                    $('#divMessage').html('');
                    HideLoader();
                }
                return false;

            });

        });

        function allowalphanumeric(e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            //alert(k);
            return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 0);
        }

        function allowalphabets(e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            //alert(k);
            return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 0);
        }

        //Modified By snehal 19 09 2016
        function Validate() {
            var msg = "";
            <%--//alert("<%= txtCatalogPageNumber.ClientID %>");
            if (parseInt(document.getElementById("<%= txtCatalogPageNumber.ClientID %>").value) == 0) {
                //alert(msg);
                msg = "Enter valid Catalog Page Number. </br>";
            }--%>
            if (Page_ClientValidate('ValidateProduct')) {
                if (document.getElementById("txtProductCode").value == "") {
                    msg += "Enter Product Code. </br>";
                }
                if (document.getElementById("txtProductName").value == "") {
                    msg += "Enter Product Name.";
                }
                //if (document.getElementById("txtFurtherDescription").value == "") {
                //    msg += "Enter Further Description.";
                //}
                //            if ($(".txtCatalogPageNumber").val() != "") {
                //                if (parseInt($(".txtCatalogPageNumber").val()) == 0) {
                //                    msg += "catalog number should be greater then 0";
                //                }
                //            }
                if (msg != "") {
                    document.getElementById("divMessage").style.display = "";
                    $("#divMessage").removeAttr("class");
                    $("#divMessage").addClass("Errormsg");
                    document.getElementById("divMessage").innerHTML = msg;
                    return false;
                }
            }
            return true;
        }

        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=chkCatelogue.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
        }

        function ValidatePriceBreak() {
            var p = 0;
            var q = 0;
            var msg = "";
            //            if (document.getElementById("txtPriceBreakName").value == "")
            //                msg += "Enter price break name. </br>";
            //            if (document.getElementById("txtPriceBreakName").value.length == 0 ) {
            //                msg = 'Price enter Price Break Name. </br>';
            //            }
            if (document.getElementById("txtPriceBreakName").value.length > 25) {
                msg = 'Price break name should be less than 25 characters. </br>';
            }
            $("#trQuantiry").find("td").each(function () {

                if ($(this).data("donotvalidate") == undefined) {
                    if ($(this).first().find("input:text").val() != null) {
                        if ($(this).first().find("input:text").val() != "") {
                            q++;
                        }
                    }
                }

            });
            //Added By Hardik for "Break Till" row
            //$("#trBreakTill").find("td").each(function () {
            //    if ($(this).data("donotvalidate") == undefined) {
            //        if ($(this).first().find("input:text").val() != null) {
            //            if ($(this).first().find("input:text").val() != "") {
            //                q++;
            //            }
            //        }
            //    }
            //});

            $("#trPrice").find("td").each(function () {
                if ($(this).first().find("input:text").val() != null) {
                    if ($(this).first().find("input:text").val() != "") {
                        p++;
                    }
                }

            });
            if (q == 0 && p == 0) {
                msg += "Enter atleast one Quantity-Price breaks. </br>";
            }
            else if (q != p) {
                //msg += "All Quantity-Price Breaks must match. </br>";
                msg += "Please Enter Quantities to Prices, One to One. </br>";
            }
            if (msg != "") {
                document.getElementById("divMessage").style.display = "";
                $("#divMessage").removeAttr("class");
                $("#divMessage").addClass("Errormsg");
                document.getElementById("divMessage").innerHTML = msg;
                return false;
            }
            return true;
        }

        function SetActiveFlag() {
            if (document.getElementById("cbWebsite").checked == true || document.getElementById("cbMobile").checked == true || document.getElementById("cbTablet").checked == true) {
                document.getElementById("chkIsAvtive").checked = true;
            }

            if (document.getElementById("cbWebsite").checked == false && document.getElementById("cbMobile").checked == false && document.getElementById("cbTablet").checked == false) {
                document.getElementById("chkIsAvtive").checked = false;
            }
        }
    </script>
    <!--Fancybox starts-->
    <script src="../JS/jquery.fancybox.js"></script>
    <link href="../CSS/jquery.fancybox.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
        });
    </script>
    <!--Fancybox end-->
    <style type="text/css">
        .in p {
            float: left;
            padding: 0 10px 0 0;
        }

        .errorText {
            color: Red;
            font-size: 11px;
        }

        .ModalPopupBG {
            background-color: #2D2D2D;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .HellowWorldPopup {
            max-width: 959px;
            max-height: 500px;
            background: white;
        }

        input[type="text"] {
            border: 1px solid #CCCCCC;
            box-shadow: 0 1px 1px 0 #D4D4D4 inset;
            padding: 6px 3px;
        }

        .fancybox-close {
            right: -19px;
            top: -9px;
        }

        .ajax__tab_xp .ajax__tab_tab {
            height: auto !important;
            padding: 0px;
        }
    </style>
    <script type="text/javascript" lang="javascript">

        function toggleSelectionGrid(source) {
            var isChecked = source.checked;
            $("#GirdViewTable input[id*='chkDefault']").each(function (index) {
                $(this).attr('checked', false);
            });
            source.checked = isChecked;
        }
    </script>
    <!--Product Color script start-->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_rbtnAuto').change(function () {
                if ($(this).is(':checked')) {
                    $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_tableAutoResize').show();
                    $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_tableManually').hide();
                }

            });
            $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_rbtnManually').change(function () {
                if ($(this).is(':checked')) {
                    $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_tableAutoResize').hide();
                    $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_tableManually').show();
                }
            });

            $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_drpVariantType').change(function () {
                $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_chkShowVairant').attr('checked', false);
                $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_txtVairantType').val('');
                if ($(this).val() == "0") {

                    $('.trShowVariant').show();
                    $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_txtVairantType').show();
                }
                else {
                    $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_txtVairantType').hide();
                    $('.trShowVariant').hide();
                }
            });

            $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_btnSaveAddMoreImage').click(function () {
                if ($('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_drpVariantType').val() == "0") {
                    if ($('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_txtVairantType').val() == "") {
                        alert("Please enter variant type");
                        return false;
                    }

                    var Typename = $('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_txtVairantType').val().toLowerCase();
                    var items = $("#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_drpVariantType option");
                    var result = false;
                    items.each(function (index) {
                        var item = items[index].text.toLowerCase();
                        if (Typename.replace(/ /g, '') == item.replace(/ /g, '')) {
                            result = true;
                        }
                    });
                    if (result) {
                        alert("Entered variant type name already exists.");
                        return false;
                    }
                }
                if ($('#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_txtVairantName').val() == "") {
                    alert("Please enter variant name");
                    return false;
                }
            });
        });
    </script>
    <!--Product Color script end-->
    <!--Product Groups script start-->
    <script type="text/javascript">
        function CheckDefaultImage(t) {
            var isChk = 0;
            $('.chkDefault.defaultvariant').each(function (i) {
                if ($(this)[0].childNodes[0].checked) {
                    isChk = 1;
                }
            });

            if (isChk == 0) {
                alert('Please select atleast one default image.');
                return false;
            }

            return true;
        }

        function ValidateVariantTypeName(t) {
            var relId = $(t).attr('rel');
            var Typename = $(".txtRenameVariantName_" + relId).val().toLowerCase();

            if (Typename == '') {
                alert("Variant type name should not be blank.");
                return false;
            }

            var items = $("#ContentPlaceHolder1_tab_Product_tab_pan_ProductImage_drpVariantType option");
            var result = false;
            items.each(function (index) {
                var item = items[index].text.toLowerCase();
                if (Typename.replace(/ /g, '') == item.replace(/ /g, '')) {
                    result = true;
                }
            });
            if (result) {
                alert("Entered variant type name already exists.");
                return false;
            }
        }

        function ShowRename(t) {
            var relId = $(t).attr('rel');
            $(t).hide();
            $(".dRenameTextbox_" + relId).show();
            var element = $(".lblVairantType_" + relId).html();
            $(".txtRenameVariantName_" + relId).val(element);
        }
        function HideRename(t) {
            var relId = $(t).attr('rel');
            $(".dRenameTextbox_" + relId).hide();
            $(".dRename_" + relId).show();
            return false;
        }

        $(document).ready(function () {
            $('.AddNewField').click(function () {
                var DetailGroupId = $(this).attr('rel');
                $('.trGroup_' + DetailGroupId).show();
                $('.trField_' + DetailGroupId).hide();

                var data = jQuery('.AddNewField');
                data.each(function (index) {
                    var rel = this.attributes[0].value;
                    if (rel == DetailGroupId) {
                        $(this).hide()
                    }
                });

                var data2 = jQuery('.AddNewGroupField');
                data2.each(function (index) {
                    var rel = this.attributes[0].value;
                    if (rel == DetailGroupId) {
                        $(this).hide()
                    }
                });

                $(this).hide();
            });




            $('.AddNewGroupField').click(function () {
                var DetailGroupId = $(this).attr('rel');
                $('.trField_' + DetailGroupId).show();
                $('.trGroup_' + DetailGroupId).hide();
                $(this).hide();

                var data = jQuery('.AddNewField');
                data.each(function (index) {
                    var rel = this.attributes[0].value;
                    if (rel == DetailGroupId) {
                        $(this).hide()
                    }
                });

                var data2 = jQuery('.AddNewGroupField');
                data2.each(function (index) {
                    var rel = this.attributes[0].value;
                    if (rel == DetailGroupId) {
                        $(this).hide()
                    }
                });
            });

            $('.SaveGroupFieldValidate').click(function () {
                var DetailGroupId = $(this).attr('rel');
                if ($('.txt_AddNew_' + DetailGroupId).val() == "" && $('.txt_AddNewValue_' + DetailGroupId).val() != "") {
                    alert('Please enter field name');
                    return false;
                }
                if ($('.txt_AddNew_' + DetailGroupId).val() != "" && $('.txt_AddNewValue_' + DetailGroupId).val() == "") {
                    alert('Please enter field value');
                    return false;
                }
                if ($('.txt_AddNew_' + DetailGroupId).val() == "" && $('.txt_AddNewValue_' + DetailGroupId).val() == "") {
                    alert('Please enter field name and respective value');
                    return false;
                }
            });

            $('.SaveGroupValidate').click(function () {
                var DetailGroupId = $(this).attr('rel');
                if ($('.txt_AddValue_' + DetailGroupId).val() == "") {
                    alert('Please enter value');
                    return false;
                }
            });

            $(".CancelDetail").click(function () {
                var DetailGroupId = $(this).attr('rel');
                $(".trField_" + DetailGroupId).hide();
                $(".AddNewField").show();
                $(".trGroup_" + DetailGroupId).hide();
                $(".AddNewGroupField").show();
                $(".txt_AddNew_" + DetailGroupId).val('');
                $(".txt_AddNewValue_" + DetailGroupId).val('');
                $(".txt_AddValue_" + DetailGroupId).val('');
                $('.ddl_AddNew_' + DetailGroupId + ' option:eq(0)').attr('selected', 'selected');
            });



            $(".DisplayOrder").keydown(function (event) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                    // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
                    // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });

        });
    </script>
    <!--Product Groups script end-->
    <!--Categories script start-->
    <script type="text/javascript">
        //$(document).ready(function () {
        function SaveCategory() {

            var rdchk = $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_grdCategories').find(':radio');
            ContentPlaceHolder1_tab_Product_tab_pan_Product_grdCategories_chkRelatedCategoryId_1
            var chk = $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_grdCategories').find(':checkbox');

            var result = false;
            var count = 0;
            chk.each(function (index) {
                if ($(this).is(":checked")) {
                    var Id = $(this).parent().attr("rel");
                    count = count + 1;
                    rdchk.each(function (index) {
                        if ($(this).is(":checked")) {
                            var rbnId = $(this).parent().attr("rel");
                            if (rbnId == Id) {
                                result = true;
                            }
                        }
                    });
                }
            });

            var CatIds = "";
            chk.each(function (index) {
                if ($(this).is(":checked")) {
                    CatIds = CatIds + $(this).parent().attr("rel") + ",";
                }
            });

            //alert(CatIds);
            if (CatIds != "") {
                $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_HIDRelatedCategoryIDs').val(CatIds);
                // parent.$.fancybox.close();
            }
            else {
                $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_HIDRelatedCategoryIDs').val("");
            }

            if (!result && count >= 1) {
                alert("Please select a default category");
                return;
            }
            parent.$.fancybox.close();


            //});
        }
        function CloseCategoryFanyBox() {
            $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_HIDRelatedCategoryIDs').val("");
            parent.$.fancybox.close();
            return false;
        }

        function SaveSection() {
            //jQuery('.lnkbtnSaveSection').click(function () {
            var chk = $('#tbl_sections').find(':checkbox');
            var SectionIdsWeb = "";
            var SectionIdsMobile = "";
            var SectionIdsTablet = "";

            chk.each(function (index) {
                if ($(this).is(":checked")) {
                    var ID = this.id;
                    if (ID.indexOf("chkSectionIdWeb") != -1) {
                        SectionIdsWeb = SectionIdsWeb + $(this).parent().attr("rel") + ",";
                    }
                    else if (ID.indexOf("chkSectionIdMobile") != -1) {
                        SectionIdsMobile = SectionIdsMobile + $(this).parent().attr("rel") + ",";
                    }
                    else if (ID.indexOf("chkSectionIdTablet") != -1) {
                        SectionIdsTablet = SectionIdsTablet + $(this).parent().attr("rel") + ",";

                    }
                }
            });

            alert(SectionIdsWeb);
            alert(SectionIdsTablet);

            if (SectionIdsWeb != "" || SectionIdsMobile != "" || SectionIdsTablet != "") {
                $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_hdnSectionUpdate').val("True");
            }

            $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_hdnSectionIdsWeb').val(SectionIdsWeb);
            $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_hdnSectionIdsMobile').val(SectionIdsMobile);
            $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_hdnSectionIdsTablet').val(SectionIdsTablet);

            parent.$.fancybox.close();

            //});
        }
        // });

        function CloseFanyBox() {
            parent.$.fancybox.close();
            return false;
        }


    </script>
    <!--Categories script end-->
    <!--Price script start-->
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery(".chkStrikePrice").change(function () {
                if ($(this)[0].checked) {
                    $(".trStrikePrice").show();
                }
                else {
                    $(".trStrikePrice").hide();
                }
            });
        });
    </script>
    <!--Price script end-->
    <script type="text/javascript">
        function RadioCheck(rb) {
            var gv = document.getElementById("ContentPlaceHolder1_tab_Product_tab_pan_Product_grdCategories");
            var rbs = gv.getElementsByTagName("input");
            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }

        function CategoryCheckbox(chk) {
            var id = $(chk)[0].id;
            var arry = id.split("_");

            var n = arry[arry.length - 1];

            if (!$('#' + id).is(':checked')) {
                $('#ContentPlaceHolder1_tab_Product_tab_pan_Product_grdCategories_chkDefault_' + n).attr('checked', false);
            }

        }
        $(document).ready(function () {
            $("#chkMandatory").click(function () {
                debugger;
                //alert('hi');		
                if ($(this).is(":checked") == true) {
                    var span = document.getElementById("spnUserInput");
                    var elem = document.getElementById("chkUserInput");
                    if (elem.checked) {
                        $('#spnUserInput').css('display', 'none');
                    }
                    else {
                        $('#spnUserInput').css('display', 'block');
                        return false;
                    }
                }
            })
        })
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Button ID="btnBack" class="save" runat="server" Style="float: right; margin: 9px 6px 0 0; display: none;"
        Text="Back" OnClick="btnBack_Click" />
    <asp:HiddenField ID="hdnProductCode" runat="server" />
    <asp:HiddenField ID="hdnProductId" runat="server" />

    <div class="admin_page">
        <section class="container mainContainer padbtom15">

            <div class="wrap_container ">
                <div class="content">

                    <div class="col-md-5">
                        <h3>
                            <asp:Literal ID="ltrPageHeading" runat="server" Text="Add Product"></asp:Literal>
                        </h3>
                    </div>
                    <div class="col-md-7 text-right">
                        <label id="select_lang" class="sm_select col-md-7 text-right">
                            Select Language :
                        </label>

                        <div class="right_selct_dropdwn relative_data col-md-5 text-right">
                            <div class="select-style selectpicker">
                                <asp:DropDownList ID="ddlLanguage" runat="server" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList><br />
                            </div>
                        </div>

                    </div>
                    <div id="DivDropDownProduct" runat="server" class="col-md-12 text-left">
                        <br />
                        <div class="col-md-7">

                            <span class="newtitle12 title12" id="spnProductName" runat="server"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <label class="sm_select col-md-7 text-right">
                                    Products :
                                </label>

                                <div class="right_selct_dropdwn relative_data col-md-5 text-right">
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlProducts" runat="server" OnSelectedIndexChanged="ddlProducts_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div id="divMessage" runat="server" clientidmode="Static">
                    </div>
                    <span class="sectionText"></span>
                    <div class="addsaTicP">
                        <ajaxToolkit:TabContainer ID="tab_Product" runat="server" ScrollBars="Vertical">
                            <ajaxToolkit:TabPanel ID="tab_pan_Product" runat="server" ScrollBars="Vertical">
                                <HeaderTemplate>
                                    Description
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="CsA">
                                        <h3>Products Information
                                        </h3>
                                        <div class="newinner">
                                            <div class="CheckB">
                                                <table width="100%" class="pricing_detail1">
                                                    <tr>
                                                        <td width="185px">Catalogue ID<span style="color: Red;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBoxList ID="chkCatelogue" runat="server" RepeatDirection="Vertical" Width="100%"></asp:CheckBoxList>
                                                            <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one Catalogue ID." ValidationGroup="ValidateProduct"
                                                                ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server" />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td width="185px">Product Code<span style="color: Red;">*</span>
                                                        </td>
                                                         <td>
                                                            <asp:TextBox ID="txtProductCode" ClientIDMode="Static" runat="server" Style="margin-top: 5px;"
                                                                Width="500px" MaxLength="50"></asp:TextBox>
                                                            <asp:Image ID="imgLoad" ClientIDMode="Static" Style="display: none;" runat="server"
                                                                ImageUrl="~/Admin/Images/UI/ajax-loader1.gif" AlternateText="loder" />
                                                            <asp:RequiredFieldValidator ID="rfvProductCode" runat="server" ControlToValidate="txtProductCode"
                                                                ValidationGroup="ValidateProduct" ErrorMessage="Please enter product code" ForeColor="Red"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage=" "
                                                                ValidationExpression="^[a-zA-Z0-9 ]*$" ControlToValidate="txtProductCode" SetFocusOnError="True"
                                                                ValidationGroup="." ></asp:RegularExpressionValidator> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Product Name<span style="color: Red;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtProductName" ClientIDMode="Static" runat="server" Width="500px" ValidationGroup="ValidateProduct"  
                                                                Style="margin-top: 5px;" MaxLength="1000" ></asp:TextBox>
																<!-- onkeypress="javascript:return allowalphanumeric(event);" -->
                                                            <asp:RequiredFieldValidator ID="rfvProductName" runat="server" ControlToValidate="txtProductName"
                                                                ValidationGroup="ValidateProduct" ErrorMessage="Please enter product name" ForeColor="Red"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="."
                                                                ValidationExpression="^[a-zA-Z0-9 ]*$"  ValidationGroup="." ControlToValidate="txtProductName" SetFocusOnError="True"
                                                                 CssClass="red"></asp:RegularExpressionValidator>
																<!-- ValidationGroup="ValidateProduct"  ErrorMessage="Product Name is not valid" -->
                                                        </td>
                                                    </tr>
                                                    <tr><br />
                                                        <td>Product User Input 
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkUserInput" runat="server" Text="IsUserInput" ClientIDMode="Static" /><br /><br />
                                                            <asp:CheckBox ID="chkMandatory" runat="server" Text="IsMandatory" ClientIDMode="Static" /><span id="spnUserInput" ClientIDMode="static" style="color: Red;display:none;">Please select UserInput</span>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 25px;">
                                                        <td style="vertical-align: middle;">Assign Category
                                                        </td>
                                                        <td style="vertical-align: middle;">
                                                            <a class="category_btn fancybox email" href="#inline1">+ Categories</a>
                                                            <div id="inline1" style="display: none; width: 880px; height: 880px;">
                                                                <div class="editSectioncls">
                                                                    Assign Product to Category/SubCategory/SubSubCategory
                                                                </div>
                                                                <table width="100%" border="0">
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <div class="uploadhis">
                                                                                <asp:GridView ID="grdCategories" class="all_customer_inner1 inner1newcls" runat="server"
                                                                                    TabIndex="0" CellPadding="2" EmptyDataText="No Categories Found." Width="100%"
                                                                                    AutoGenerateColumns="False" BorderColor="#E4E4E4" BorderWidth="0" DataKeyNames="RelatedCategoryId"
                                                                                    AllowSorting="true" OnRowDataBound="grdCategories_OnRowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Category">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblCategoryName" runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Assign/Unassign">
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chkRelatedCategoryId" runat="server" Checked='<%#Convert.ToBoolean(Eval("ProductCategory")) %>'
                                                                                                    onclick="CategoryCheckbox(this);" rel='<%# Eval("RelatedCategoryId") %>' />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Default Category">
                                                                                            <ItemTemplate>
                                                                                                <asp:RadioButton ID="chkDefault" onclick="RadioCheck(this);" runat="server" Checked='<%#Convert.ToBoolean(Eval("IsDefault")) %>'
                                                                                                    rel='<%# Eval("RelatedCategoryId") %>' />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 25px;">
                                                                        <td colspan="3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td colspan="2" style="padding-left: 10px;">
                                                                            <%--<asp:LinkButton Text="Close" CssClass="CloseCategory save" OnClientClick="return CloseCategoryFanyBox();"
                                                                    runat="server" />--%>
                                                                            <label class="lnkbtnSaveCategory save" onclick="SaveCategory();">
                                                                                Done</label>
                                                                            <asp:HiddenField ID="HIDRelatedCategoryIDs" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 25px;">
                                                                        <td colspan="3"></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 25px; display: none;">
                                                        <td style="vertical-align: middle;">Assign Section
                                                        </td>
                                                        <td style="vertical-align: middle;">
                                                            <a class="category_btn fancybox email" href="#inline2">+ Sections</a>
                                                            <div id="inline2" style="display: none; width: 880px; height: 500px;">
                                                                <table width="100%" border="0">
                                                                    <tr style="height: 25px; font-weight: bold;">
                                                                        <td colspan="3" style="vertical-align: middle;">Assign Product to Section(s)
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" style="color: Red;">Note: Section(s) assigned to category cannot be edited.
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <div class="uploadhis">
                                                                                <asp:Repeater runat="server" ID="rptSection">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tbl_sections" class="all_customer_inner1" cellspacing="0" cellpadding="2"
                                                                                            style="border-color: #E4E4E4; border-width: 0px; width: 100%; border-collapse: collapse;">
                                                                                            <tr style="background-color: #F2F2F2; height: 30px;">
                                                                                                <th style="text-align: left; vertical-align: middle;">Section
                                                                                                </th>
                                                                                                <th style="text-align: left; vertical-align: middle;">Type
                                                                                                </th>
                                                                                                <th style="text-align: left; vertical-align: middle;">Status
                                                                                                </th>
                                                                                                <th style="text-align: center; vertical-align: middle;">Assign/Unassign To Web
                                                                                                </th>
                                                                                                <th style="text-align: center; vertical-align: middle;">Assign/Unassign To Mobile
                                                                                                </th>
                                                                                                <th style="text-align: center; vertical-align: middle;">Assign/Unassign To Tablet
                                                                                                </th>
                                                                                            </tr>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr style="height: 30px;">
                                                                                            <td style="text-align: left;">
                                                                                                <asp:Label ID="lblSectionName" Text='<%# Eval("SectionName") %>' runat="server" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lblSectionType" runat="server" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lblStatus" Text='<%# Convert.ToString(Eval("IsActive"))=="True"?"Active":"Inactive" %>'
                                                                                                    runat="server" />
                                                                                            </td>
                                                                                            <td style="text-align: center;">
                                                                                                <asp:CheckBox ID="chkSectionIdWeb" runat="server" rel='<%# Eval("SectionId") %>' />
                                                                                            </td>
                                                                                            <td style="text-align: center;">
                                                                                                <asp:CheckBox ID="chkSectionIdMobile" runat="server" rel='<%# Eval("SectionId") %>' />
                                                                                            </td>
                                                                                            <td style="text-align: center;">
                                                                                                <asp:CheckBox ID="chkSectionIdTablet" runat="server" rel='<%# Eval("SectionId") %>' />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 25px;">
                                                                        <td colspan="3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td colspan="2">
                                                                            <asp:LinkButton ID="lnkbtnSectionClose" Text="Close" OnClientClick="return CloseFanyBox();"
                                                                                runat="server" CssClass="CloseSection" />
                                                                            <label class="lnkbtnSaveSection" onclick="SaveSection();">
                                                                                Done</label>
                                                                            <asp:HiddenField ID="hdnSectionUpdate" Value="false" runat="server" />
                                                                            <asp:HiddenField ID="hdnSectionIdsWeb" runat="server" />
                                                                            <asp:HiddenField ID="hdnSectionIdsMobile" runat="server" />
                                                                            <asp:HiddenField ID="hdnSectionIdsTablet" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="display: none;">
                                                        <td>Is Active
                                                        </td>
                                                        <td>:
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkIsAvtive" runat="server" Style="margin-top: 5px;" Checked="True" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <h3>Products Description
                                        </h3>
                                        <table width="100%" class="Pro_description pricing_detail1">
                                            <tr>
                                                <td style="vertical-align: top;">Product Description
                                                </td>
                                                <td>
                                                    <CKEditor:CKEditorControl ID="txtProductDescription" BasePath="~/admin/ckeditor/"
                                                        runat="server"></CKEditor:CKEditorControl>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" style="height: 9px;"></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">Further Description<span style="color: Red; vertical-align: top;">*</span></td>
                                                <%--   <span style="color: Red; vertical-align: top;">*</span>--%>
                                                <td>
                                                    <%--<CKEditor:CKEditorControl ID="txtFurtherDescription" BasePath="~/admin/ckeditor/"
                                                        runat="server"></CKEditor:CKEditorControl>--%>
                                                    <asp:TextBox ID="txtFurtherDescription" runat="server" Style="margin-top: 5px;" Width="500px" MaxLength="200"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvtxtFurtherDescription" runat="server" ControlToValidate="txtFurtherDescription"
                                                        ValidationGroup="ValidateProduct" ErrorMessage="Please enter further description" ForeColor="Red"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- Code Commented as this sections is moved to new tab Product SKU -->
                                        <%--<h3>
                            BASYS Fields
                        </h3>
                            <table width="100%">
                                <tr>
                                    <td>GroupId</td>
                                    <td>
                                        <asp:TextBox ID="txtGroupId" Style="margin-top: 5px;" runat="server" MaxLength="5"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeGroupId" runat="server" TargetControlID="txtGroupId"
                                            FilterType="Numbers" FilterMode="ValidChars"></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="rxvGroupId" runat="server" ControlToValidate="txtGroupId"
                                            ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$"
                                            Display="Dynamic" ValidationGroup="ValidateProduct"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>CatalogueAlias</td>
                                    <td>
                                        <asp:TextBox ID="txtCatalogueAlias" Style="margin-top: 5px;" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>CatalogueReference</td>
                                    <td>
                                        <asp:TextBox ID="txtCatalogueReference" Style="margin-top: 5px;" runat="server" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SupplierId</td>
                                    <td>
                                        <asp:TextBox ID="txtSupplierId" Style="margin-top: 5px;" runat="server" MaxLength="5"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeSupplierId" runat="server" TargetControlID="txtSupplierId"
                                            FilterType="Numbers" FilterMode="ValidChars"></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="rxvSupplierId" runat="server" ControlToValidate="txtSupplierId"
                                            ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$"
                                            Display="Dynamic" ValidationGroup="ValidateProduct"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>TrimColorId</td>
                                    <td>
                                        <asp:TextBox ID="txtTrimColorId" Style="margin-top: 5px;" runat="server" MaxLength="5"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeTrimColorId" runat="server" TargetControlID="txtTrimColorId"
                                            FilterType="Numbers" FilterMode="ValidChars"></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="rxvTrimcolorId" runat="server" ControlToValidate="txtTrimColorId"
                                            ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$"
                                            Display="Dynamic" ValidationGroup="ValidateProduct"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>BaseColorId</td>
                                    <td>
                                        <asp:TextBox ID="txtBaseColorId" Style="margin-top: 5px;" runat="server" MaxLength="5"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeBaseColorId" runat="server" TargetControlID="txtBaseColorId"
                                            FilterType="Numbers" FilterMode="ValidChars"></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="rxvBaseColorId" runat="server" ControlToValidate="txtBaseColorId"
                                            ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$"
                                            Display="Dynamic" ValidationGroup="ValidateProduct"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>BasysProductId</td>
                                    <td>
                                        <asp:TextBox ID="txtBasysProductId" Style="margin-top: 5px;" runat="server" MaxLength="10"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeBasysProductId" runat="server" TargetControlID="txtBasysProductId"
                                            FilterType="Numbers" FilterMode="ValidChars"></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="rxvBasysProductId" runat="server" ControlToValidate="txtBasysProductId"
                                            ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$"
                                            Display="Dynamic" ValidationGroup="ValidateProduct"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>VatCode</td>
                                    <td>
                                        <asp:TextBox ID="txtVatCode" Style="margin-top: 5px;" runat="server" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>DimensionalWeight</td>
                                    <td>
                                        <asp:TextBox ID="txtDimensionalWeight" Style="margin-top: 5px;" runat="server" MaxLength="10"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="rxvDimensionalWeight" runat="server" ControlToValidate="txtDimensionalWeight"
                                            ErrorMessage="Please Enter Only Decimals" ForeColor="Red" ValidationExpression="^\d*\.?\d*$"
                                            Display="Dynamic" ValidationGroup="ValidateProduct"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GermanStock</td>
                                    <td>
                                        <asp:TextBox ID="txtGermanStock" Style="margin-top: 5px;" runat="server" MaxLength="5"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeGermanStock" runat="server" TargetControlID="txtGermanStock"
                                            FilterType="Numbers" FilterMode="ValidChars"></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="rxvGermanStock" runat="server" ControlToValidate="txtGermanStock"
                                            ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$"
                                            Display="Dynamic" ValidationGroup="ValidateProduct"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>

                            </table>--%>
                                        <h3>Resources</h3>
                                        <table width="100%" class="Pro_description pricing_detail1">
                                            <%--<tr>
                                <td>
                                    Catalog Page Number
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCatalogPageNumber" Style="margin-top: 5px;" runat="server" Width="40px"
                                        MaxLength="4" CssClass="txtCatalogPageNumber"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeCatalogPN" runat="server" TargetControlID="txtCatalogPageNumber"
                                        FilterType="Numbers" FilterMode="ValidChars">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCatalogPageNumber"
                                        ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$"
                                        Display="Dynamic" ValidationGroup="ValidateProduct"></asp:RegularExpressionValidator>
                                </td>
                            </tr>--%>
                                            <tr>
                                                <td>Product PDF
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="fldPDF" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>For Product PDF :<span id="spnPDFInfo" class="errorText" runat="server"> </span>
                                                    <asp:HyperLink ID="hypPdf" Target="_blank" runat="server" Text="View PDF" Visible="false" />&nbsp;|
                                    <asp:Button ID="btnDeletePdf" runat="server" Style="color: Blue; cursor: pointer;"
                                        Text="Delete PDF" Visible="false" OnClick="btnDeletePdf_Click" OnClientClick="javascript:return confirm('Are you sure you want to delete PDF file ?')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Image Alt Text
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtImageAltTag" runat="server" Width="300px" Style="margin-top: 5px;"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Video Link
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVideoLink" runat="server" Style="margin-top: 5px;" Width="300px"
                                                        placeholder="http://"></asp:TextBox>
                                                    <br />
                                                    For Video Link <span id="spnInfo" class="errorText" runat="server">http://s.ytimg.com/yts/swfbin/player-vfllj9q_O/watch_as3.swf</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Virtual Sample
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtProductVirtualSampleLink" runat="server" placeholder="http://"
                                                        Style="margin-top: 5px;" Width="300px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tag Attributes
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTagAttributes" Style="margin-top: 5px;" runat="server" Width="300px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <h3>SEO</h3>
                                        <div class="DetailsB">
                                            <div class="CheckB">
                                                <table class="seo pricing_detail1" width="100%">
                                                    <tr>
                                                        <td>Page Title
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPageTitle" runat="server" Style="margin-top: 5px; margin-left: 0px;"
                                                                Width="300px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Meta Keyword
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMetaKeyword" runat="server" Style="margin-top: 5px; margin-left: 0px;"
                                                                Width="300px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Meta Description
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMetaDescription" runat="server" CssClass="textarea" Style="margin-top: 5px; margin-left: 0px;"
                                                                TextMode="MultiLine" Width="400px" Height="150px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <%--<tr style="height: 25px;">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="ValidateProduct" />
                                                <asp:Button ID="btnSeo" Text="Show Statistics" runat="server" OnClick="btnSeo_Click" />
                                                <asp:LinkButton ID="lnkbtnSeo" runat="server" OnClick="btnSeo_Click" OnClientClick="TINY.box.show({iframe:'SEO.aspx',boxid:'frameless',width:900,height:400,fixed:false,maskid:'bluemask',maskopacity:50})"
                                                    Text="Show" Visible="false"></asp:LinkButton>
                                                <uc:Statistics ID="ucStats" runat="server" Visible="false" />
                                            </td>
                                        </tr>--%>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="new_button_pannel button_section">

                                            <asp:Button ID="btnSave" CssClass="exit save" runat="server" Text="Save" OnClick="btnSave_Click"
                                                OnClientClick="return Validate()" /></li>
                                    
                                    <asp:Button ID="btnSaveNext" CssClass="exit save" runat="server" Text="Save and Continue"
                                        CommandName="gotonextpanel" OnClick="btnSave_Click" OnClientClick="return Validate()" />

                                            <asp:Button ID="btnSeo" Text="Show Statistics" Visible="false" runat="server" OnClick="btnSeo_Click" />
                                            <asp:LinkButton ID="lnkbtnSeo" runat="server" OnClick="btnSeo_Click" OnClientClick="TINY.box.show({iframe:'SEO.aspx',boxid:'frameless',width:900,height:400,fixed:false,maskid:'bluemask',maskopacity:50})"
                                                Text="Show" Visible="false"></asp:LinkButton>
                                            <%--<uc:Statistics ID="ucStats" runat="server" Visible="false" />--%>

                                            <asp:Button ID="btnCancelProdInfo" CssClass="btn gray" runat="server" OnClick="btnCancelProdInfo_Click"
                                                Text="Cancel" />

                                            <div class="clear">
                                            </div>
                                        </div>
                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnSeo"
                                            PopupControlID="Panel1" PopupDragHandleControlID="PopupHeader" Drag="true" BackgroundCssClass="ModalPopupBG"
                                            Enabled="false">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Panel ID="Panel1" Style="overflow: scroll; background: white;" runat="server"
                                            Visible="false">
                                            <div class="HellowWorldPopup">
                                                <div class="PopupBody">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Literal ID="ltrProductStat" runat="server"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 25px;">
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnSave1" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="ValidateProduct" />
                                                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="btnCancel_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tab_pan_ProductSKU" CssClass="movex" runat="server" ScrollBars="Vertical" Height="700px">
                                <HeaderTemplate>
                                    Product SKU
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div>
                                        <asp:Label ID="lblresult" runat="server"></asp:Label>
                                    </div>
                                    <asp:GridView CssClass="product_sku" ID="gvProductSKU" runat="server" AutoGenerateColumns="false" Width="100%"
                                        DataKeyNames="ProductSKUId,ProductId,SKUName" ShowFooter="true"
                                        OnRowEditing="gvProductSKU_RowEditing" OnRowCancelingEdit="gvProductSKU_RowCancelingEdit" OnRowUpdating="gvProductSKU_RowUpdating"
                                        OnRowDeleting="gvProductSKU_RowDeleting" OnRowCommand="gvProductSKU_RowCommand" Border="1">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Action">
                                                <EditItemTemplate>
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="save" />
                                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="save" />
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="save" />
                                                    <asp:Button ID="btnDelete" OnClientClick="javascript:return confirm('Are you sure, you want to delete this?')" runat="server" Text="Delete" CommandName="Delete"
                                                        CssClass="save" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Button ID="btnAdd" CssClass="save" runat="server" CommandName="AddNew" Text="Add" ValidationGroup="ValidationProductSKU" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Product SKU">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtProductSKUEdit" runat="server" Text='<%# Eval("SKU") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProductSKU" runat="server" Text='<%# Eval("SKU") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtProductSKU" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvProductSKU" runat="server" ControlToValidate="txtProductSKU" ValidationGroup="ValidationProductSKU" Display="Dynamic" ErrorMessage="Enter Product SKU" Text="Required">
                                                    </asp:RequiredFieldValidator>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SKU Name">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtSKUNameEdit" runat="server" Text='<%# Eval("SKUName") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSKUName" runat="server" Text='<%# Eval("SKUName") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtSKUName" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvSKUName" runat="server" ControlToValidate="txtSKUName" ValidationGroup="ValidationProductSKU" Display="Dynamic" ErrorMessage="Enter SKU Name" Text="Required">
                                                    </asp:RequiredFieldValidator>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vat Code">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtVatCodeEdit" runat="server" Text='<%# Eval("VatCode") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVatCode" runat="server" Text='<%# Eval("VatCode") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtVatCode" runat="server"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Dimensional Weight">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtWeightEdit" runat="server" Text='<%# Eval("DimensionalWeight") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblWeight" runat="server" Text='<%# Eval("DimensionalWeight") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtWeight" runat="server"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Min Order Qty">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMinOrderEdit" runat="server" Text='<%# Eval("MinimumOrderQuantity") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMinOrder" runat="server" Text='<%# Eval("MinimumOrderQuantity") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtMinOrder" runat="server"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Max Order Qty">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMaxOrderEdit" runat="server" Text='<%# Eval("MaximumOrderQuantity") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMaxOrder" runat="server" Text='<%# Eval("MaximumOrderQuantity") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtMaxOrder" runat="server"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Group Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGroupId" runat="server" Text='<%#Eval("GroupId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BASYS Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBASYSId" runat="server" Text='<%# Eval("BASYSProductId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Catalogue Alias">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCatalogueAlias" runat="server" Text='<%#Eval("CatalogueAlias") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Supplier Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSupplierId" runat="server" Text='<%# Eval("SupplierId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trim Colour Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTrimColorId" runat="server" Text='<%# Eval("TrimColorId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Base Colour Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBaseColorId" runat="server" Text='<%# Eval("BaseColorId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="UNSPSC Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUNSPSC_Code" runat="server" Text='<%# Eval("UNSPSC_Code") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tab_pan_PriceDetail" CssClass="movex" runat="server" ScrollBars="Vertical"
                                Height="700px">
                                <HeaderTemplate>
                                    Pricing
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div>
                                        <table class="pricing_detail1" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td>Currency : </td>
                                                <td>
                                                    <div class="select-style selectpicker">
                                                        <asp:DropDownList ID="ddlCurrency" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </td>

                                            </tr>

                                        </table>



                                    </div>

                                    <table class="pricing_detail1" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td>Select Price Type :</td>
                                            <td>
                                                <asp:RadioButton ID="rbPrice" runat="server" AutoPostBack="true" GroupName="PriceType" Text="Price" OnCheckedChanged="rbPriceType_CheckedChanged" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbCallForPrice" runat="server" AutoPostBack="true" GroupName="PriceType" Text="Call For Price" OnCheckedChanged="rbPriceType_CheckedChanged" />
                                                <br />
                                                <br />

                                            </td>
                                        </tr>
                                    </table>
                                    <div class="button_section">
                                        <asp:Button ID="btnCallForPrice" CssClass="exit save" runat="server" Text="Save & Continue"
                                            OnClick="btnSavePrice_Click" Visible="false" CommandName="saveandcontinue" />
                                    </div>


                                    <table class="pricing_detail1" id="tblPriceTable" runat="server" visible="false" cellpadding="0" cellspacing="0" border="0" width="100%"
                                        style="border: 1px solid important;">

                                        <tr>
                                            <td>Display Enquiry Form</td>
                                            <td>
                                                <asp:CheckBox ID="chkDisplayEnquiryForm" runat="server" Checked="false" />Yes,
                                                display Enquiry Form.</td>
                                        </tr>
                                        <tr>
                                            <td width="130px">
                                                <span class="LabelText" id="ctl00_cphMainContent_dtlstPriceBreaks_ctl01_Label23">Price
                                        Break Name</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPriceBreakName" ClientIDMode="Static" runat="server" Width="220px" MaxLength="25"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="rfvPriceBreakName" runat="server" ControlToValidate="txtPriceBreakName"
                                        Display="Static" ValidationGroup="ValidatePrice" ForeColor="Red" ErrorMessage="Please enter price break name"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <%--<td>
                                    Max. Quantity<asp:TextBox ID="txtMaxQty" runat="server" Width="45px" CssClass="box"
                                        MaxLength="6"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender65" runat="server"
                                        TargetControlID="txtMaxQty" FilterMode="ValidChars" FilterType="Numbers">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                </td>
                                <td>
                                    Quantity Unit<asp:TextBox ID="txtQtyUnit" runat="server" Width="45px" CssClass="box"
                                        MaxLength="20"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender66" runat="server"
                                        TargetControlID="txtQtyUnit" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                        ValidChars=".'">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                </td>--%>
                                        </tr>
                                        <tr id="trIsregularPrice" runat="server">
                                            <td>Is Regular Price
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="cbIsRegular" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td colspan="4">FOB Points<asp:TextBox ID="txtFobPoints" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4" style="padding: 0px 0 15px 0px;">
                                                <table border="0">
                                                    <tr id="trQuantiry">
                                                        <td>
                                                            <asp:Label ID="Label12" Text="Break From" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak1" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe1" runat="server" TargetControlID="txtBreak1"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak2" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe2" runat="server" TargetControlID="txtBreak2"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak3" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe3" runat="server" TargetControlID="txtBreak3"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak4" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe4" runat="server" TargetControlID="txtBreak4"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak5" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe5" runat="server" TargetControlID="txtBreak5"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak6" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe6" runat="server" TargetControlID="txtBreak6"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak7" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe7" runat="server" TargetControlID="txtBreak7"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak8" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe8" runat="server" TargetControlID="txtBreak8"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak9" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe9" runat="server" TargetControlID="txtBreak9"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreak10" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe10" runat="server" TargetControlID="txtBreak10"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td data-donotvalidate="true">
                                                            <%--<asp:TextBox ID="txtQuantityText" runat="server" placeholder="Quantity Text" Style="margin-left: 30px;"></asp:TextBox>--%>
                                                        </td>
                                                    </tr>
                                                    <tr style="display: none;">
                                                        <td>
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId1" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId2" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId3" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId4" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId5" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId6" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId7" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId8" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId9" runat="server" />
                                                            <asp:HiddenField ID="hdnPriceBreakDetailId10" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr style="display: none;">
                                                        <td>
                                                            <asp:Label ID="Label15" Text="Qty Break Text" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText1" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText2" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText3" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText4" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText5" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText6" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText7" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText8" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText9" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakText10" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td colspan="4">&nbsp;
                                                        </td>
                                                    </tr>
                                                    
                                                    <%--Added By Hardik - START--%>
                                                    <tr id="trBreakTill">
                                                        <td>
                                                            <asp:Label ID="Label8" Text="Break Till" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill1" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe65" runat="server" TargetControlID="txtBreakTill1"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill2" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe66" runat="server" TargetControlID="txtBreakTill2"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill3" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe69" runat="server" TargetControlID="txtBreakTill3"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill4" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe70" runat="server" TargetControlID="txtBreakTill4"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill5" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe71" runat="server" TargetControlID="txtBreakTill5"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill6" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe72" runat="server" TargetControlID="txtBreakTill6"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill7" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe73" runat="server" TargetControlID="txtBreakTill7"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill8" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe74" runat="server" TargetControlID="txtBreakTill8"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill9" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe75" runat="server" TargetControlID="txtBreakTill9"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBreakTill10" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe76" runat="server" TargetControlID="txtBreakTill10"
                                                                FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td data-donotvalidate="true">
                                                            <%--<asp:TextBox ID="txtQuantityText" runat="server" placeholder="Quantity Text" Style="margin-left: 30px;"></asp:TextBox>--%>
                                                        </td>
                                                    </tr>
                                                    <%--Added By Hardik - END--%>

                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label7" Text="Quantity Text" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText1" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText2" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText3" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText4" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText5" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText6" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText7" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText8" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText9" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtQuantityText10" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                        </td>
                                                        <td colspan="4">&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr id="trPrice">
                                                        <td>
                                                            <asp:Label ID="Label16" Text="Price" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice1" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                                                TargetControlID="txtPrice1" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice2" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                                TargetControlID="txtPrice2" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice3" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                                TargetControlID="txtPrice3" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice4" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                                TargetControlID="txtPrice4" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice5" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                                                TargetControlID="txtPrice5" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice6" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                                                TargetControlID="txtPrice6" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice7" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                                                TargetControlID="txtPrice7" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice8" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                                                TargetControlID="txtPrice8" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice9" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
                                                                TargetControlID="txtPrice9" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPrice10" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
                                                                TargetControlID="txtPrice10" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                ValidChars=".">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td colspan="2">&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label18" Text="Disc. Code" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode1" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Discount Code1 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode1" SetFocusOnError="True"
                                                                ValidationGroup="ValidDiscount" CssClass="red" Display="None"></asp:RegularExpressionValidator>

                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode2" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Discount Code2 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode2" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode3" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Discount Code3 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode3" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode4" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Discount Code4 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode4" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode5" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Discount Code5 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode5" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode6" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ErrorMessage="Discount Code6 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode6" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode7" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ErrorMessage="Discount Code7 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode7" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode8" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ErrorMessage="Discount Code8 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode8" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode9" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ErrorMessage="Discount Code9 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode9" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDiscCode10" runat="server" Width="45px" CssClass="box" MaxLength="1"
                                                                Style="text-transform: uppercase;" onkeypress="javascript:return allowalphabets(event);"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ErrorMessage="Discount Code10 is not valid"
                                                                ValidationExpression="^[a-zA-Z]*$" ControlToValidate="txtDiscCode10" SetFocusOnError="True" Display="None"
                                                                ValidationGroup="ValidDiscount" CssClass="red"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Enable Strike Price
                                                        </td>
                                                        <td colspan="12">
                                                            <input id="cbEnableStrikePrice" runat="server" type="checkbox" class="chkStrikePrice" />Yes,
                                                display strike price
                                                <%--<asp:CheckBox ID="cbEnableStrikePrice" CssClass="chkStrikePrice" runat="server" Text="Yes, display strike price" />--%>
                                                        </td>
                                                    </tr>
                                                    <tr id="trStrikePrice" runat="server" style="display: none;" class="trStrikePrice">
                                                        <td colspan="12">
                                                            <table>
                                                                <tr>
                                                                    <td>Strike Price Break Name
                                                                    </td>
                                                                    <td colspan="12">
                                                                        <asp:TextBox ID="txtStrikePriceBreakName" runat="server" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label6" Text="Strike Price" runat="server" CssClass="LabelText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice1" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server"
                                                                            TargetControlID="txtStrikePrice1" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice2" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server"
                                                                            TargetControlID="txtStrikePrice2" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice3" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server"
                                                                            TargetControlID="txtStrikePrice3" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice4" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server"
                                                                            TargetControlID="txtStrikePrice4" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice5" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server"
                                                                            TargetControlID="txtStrikePrice5" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice6" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server"
                                                                            TargetControlID="txtStrikePrice6" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice7" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server"
                                                                            TargetControlID="txtStrikePrice7" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice8" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server"
                                                                            TargetControlID="txtStrikePrice8" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice9" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server"
                                                                            TargetControlID="txtStrikePrice9" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStrikePrice10" runat="server" Width="45px" CssClass="box" MaxLength="10"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server"
                                                                            TargetControlID="txtStrikePrice10" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td colspan="2">&nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr style="display: none;">
                                                                    <td>
                                                                        <asp:Label ID="Label4" Text="Price Unit" runat="server" CssClass="LabelText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="14">
                                                                        <asp:TextBox ID="txtPriceUnit" runat="server" Width="100"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label5" Text="Expiration Date" runat="server" CssClass="LabelText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="14">
                                                                        <asp:TextBox ID="txtExpirationDate" ClientIDMode="Static" runat="server" Width="100"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label1" Text="Production Time " runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td colspan="14">
                                                            <asp:TextBox ID="txtProductionTime" runat="server" CssClass="box" Width="150"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label2" Text="Maximum Quantity" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td colspan="14">
                                                            <asp:TextBox ID="txtMaximumQuantity" runat="server" Width="100" MaxLength="6"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender67" runat="server"
                                                                TargetControlID="txtMaximumQuantity" FilterMode="ValidChars" FilterType="Numbers">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label3" Text="Quantity Unit" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td colspan="14">
                                                            <asp:TextBox ID="txtQuantityUnit" runat="server" Width="100" MaxLength="20"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender68" runat="server"
                                                                TargetControlID="txtQuantityUnit" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                                ValidChars=".'">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label19" Text="Note" runat="server" CssClass="LabelText"></asp:Label>
                                                        </td>
                                                        <td colspan="14">
                                                            <asp:TextBox ID="txtNote" TextMode="MultiLine" Columns="60" Rows="2" runat="server"
                                                                Style="resize: none; border: 1px solid #CCCCCC;"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px;" id="trAddNewPrice" runat="server" visible="false">
                                            <td></td>
                                            <td>
                                                <asp:Button Text="Add New Price" ID="btnAddNewPrice" CssClass="exit save" OnClick="btnAddNewPrice_OnClick"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="height: 25px;">
                                            <td colspan="4"></td>
                                        </tr>
                                        <tr style="height: 25px;">
                                            <td colspan="4">
                                                <div class="uploadhis">
                                                    <asp:GridView ID="grdPriceBreak" class="all_customer_inner edit12" runat="server"
                                                        TabIndex="0" CellPadding="2" Width="100%" AutoGenerateColumns="False" BorderColor="#E4E4E4"
                                                        BorderWidth="0" AllowPaging="true" PageSize="15" DataKeyNames="PriceBreakId"
                                                        AllowSorting="true" OnRowCommand="grdPriceBreak_OnRowCommand" OnPageIndexChanging="grdPriceBreak_PageIndexChanging"
                                                        OnRowDataBound="grdPriceBreak_OnRowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Price Break" HeaderStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <%#DataBinder.Eval(Container.DataItem, "PriceBreakName")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Strike Price Break Name" HeaderStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <%#DataBinder.Eval(Container.DataItem, "StrikePriceBreakName")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton CommandArgument='<%#Eval("PriceBreakId") %>' CommandName="Edit_Price"
                                                                        ID="imgbtnEdit" CssClass="save" ImageUrl="~/Admin/Images/UI/edit_icn.png" runat="server" Style="float: none !important; background: none !important; border: none !important; margin: 0 !important; padding: 0 !important;" />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Regular" HeaderStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkDefault" rel='<%#Eval("PriceBreakId") %>' OnCheckedChanged="chkDefaultPrice_OnCheckedChanged"
                                                                        AutoPostBack="true" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsRegularPrice")) %>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton CommandArgument='<%#Eval("PriceBreakId") %>' CommandName="Delete_Price"
                                                                        ID="imgbtnDelete" ImageUrl="~/Admin/Images/UI/delete_icn.png" runat="server" CssClass="save"
                                                                        OnClientClick="javascript:return confirm('Are you sure you want to delete product price?')" Style="float: none !important; background: none !important; border: none !important; margin: 0 !important; padding: 0 !important;" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px;">
                                            <td colspan="4">
                                                <div class="new_button_pannel button_section">

                                                    <asp:Button ID="btnAddNew" CssClass="exit cancelM" runat="server" Text="Clear" OnClick="btnAddNew_Click"
                                                        Visible="false" />

                                                    <asp:Button ID="btnSavePrice" CssClass="save" runat="server" Text="Save Price"
                                                        ValidationGroup="ValidDiscount" OnClick="btnSavePrice_Click" OnClientClick="return ValidatePriceBreak()" CausesValidation="false" />

                                                    <asp:Button ID="btnSaveNCont" CssClass="exit save" runat="server" Text="Save & Continue"
                                                        ValidationGroup="ValidatePrice" OnClick="btnSavePrice_Click" CommandName="saveandcontinue"
                                                        OnClientClick="return ValidatePriceBreak()" />

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tbl_pan_ProductCategories" runat="server" Visible="false">
                                <HeaderTemplate>
                                    Categories
                                </HeaderTemplate>
                                <ContentTemplate>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tbl_pan_GroupNewField" runat="server" ScrollBars="Vertical"
                                Height="550px" Visible="false">
                                <HeaderTemplate>
                                    Product Groups
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <table style="width: 800px;">
                                        <tr style="height: 45px;">
                                            <td colspan="3">&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr style="height: 45px;">
                                            <td>Select Group
                                            </td>
                                            <td>:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlProductDetailGroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="height: 45px;">
                                            <td>Enter Field Name
                                            </td>
                                            <td>:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFieldName" runat="server" Width="300px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvFieldName" runat="server" ValidationGroup="validateGroupField"
                                                    Display="Static" ErrorMessage="Please enter field name" ForeColor="Red" ControlToValidate="txtFieldName"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr style="height: 45px;">
                                            <td></td>
                                            <td colspan="2">
                                                <asp:Button ID="btnSubmitGroupfeild" Text="Save" runat="server" OnClick="btnSubmitGroupfeild_Click"
                                                    ValidationGroup="validateGroupField" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tbl_pan_Imprint" runat="server" Height="550px" ScrollBars="Vertical"
                                Visible="false" HeaderText="Product Charges">
                                <HeaderTemplate>
                                    Addtional Charges
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="CsA">
                                        <h3>Charges</h3>
                                        <table width="100%" class="pricing_detail2">
                                            <tr>
                                                <td>Imprint Method
                                                </td>
                                                <td colspan="6">
                                                    <%--  <asp:UpdatePanel ID="upImprintColor" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                                    <div class="select-style selectpicker">
                                                        <asp:DropDownList ID="drpImrpintMethod" runat="server" Width="220px" OnSelectedIndexChanged="drpImrpintMethod_OnSelectedIndexChanged"
                                                            AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <%--  </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                                    <asp:HiddenField ID="hdnImprintMethodChargeId" Value="0" runat="server" />
                                                </td>
                                                <td id="tdaddNewImprint" visible="false" runat="server">
                                                    <asp:Button Text="Select New Imprint Method" ID="Button1" CssClass="exit save" OnClick="btnAddNewImprintMethod_OnClick"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                            </tr>
                                <tr id="trSetUpCharges" runat="server">
                                    <td colspan="9">
                                        <table width="100%">
                                            <tr>
                                                <td>Setup Charges
                                                </td>
                                                <td style="width: 60px;">
                                                    <asp:TextBox ID="txtSetupcharges" runat="server" MaxLength="6" Width="60px"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server"
                                                        TargetControlID="txtSetupcharges" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                        ValidChars=".">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 130px; padding-left: 9px;">Discount Code
                                                </td>
                                                <td style="width: 60px;">
                                                    <asp:TextBox ID="txtSetupchargediscountCode" runat="server" MaxLength="1" Width="60px"
                                                        Style="text-transform: capitalize;"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server"
                                                        TargetControlID="txtSetupchargediscountCode" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 130px; padding-left: 9px;">Charges Type
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpSetupChargestype" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Run Charge Name
                                                </td>
                                                <td style="width: 60px;">
                                                    <asp:TextBox runat="server" ID="txtRunChargeName" Width="200px" />
                                                </td>
                                                <td style="width: 130px; padding-left: 9px;">Discount Code
                                                </td>
                                                <td style="width: 60px;">
                                                    <asp:TextBox ID="txtRunChargeDiscountCode" runat="server" MaxLength="1" Width="60px"
                                                        Style="text-transform: capitalize;"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server"
                                                        TargetControlID="txtRunChargeDiscountCode" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 130px; padding-left: 9px; display: none;">Charges Type
                                                </td>
                                                <td style="display: none;">
                                                    <asp:DropDownList ID="drpRunChargesChargestype" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="9" class="price_break">
                                                    <table>
                                                        <tr>
                                                            <td width="100px;">Price Break
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak1" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server"
                                                                    TargetControlID="txtRunChargeBreak1" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak2" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server"
                                                                    TargetControlID="txtRunChargeBreak2" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak3" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server"
                                                                    TargetControlID="txtRunChargeBreak3" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak4" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server"
                                                                    TargetControlID="txtRunChargeBreak4" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak5" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server"
                                                                    TargetControlID="txtRunChargeBreak5" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak6" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server"
                                                                    TargetControlID="txtRunChargeBreak6" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak7" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server"
                                                                    TargetControlID="txtRunChargeBreak7" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak8" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server"
                                                                    TargetControlID="txtRunChargeBreak8" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak9" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server"
                                                                    TargetControlID="txtRunChargeBreak9" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunChargeBreak10" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server"
                                                                    TargetControlID="txtRunChargeBreak10" FilterMode="ValidChars" FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100px;">Run Charges
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge1" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server"
                                                                    TargetControlID="txtRunCharge1" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge2" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server"
                                                                    TargetControlID="txtRunCharge2" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge3" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server"
                                                                    TargetControlID="txtRunCharge3" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge4" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server"
                                                                    TargetControlID="txtRunCharge4" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge5" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server"
                                                                    TargetControlID="txtRunCharge5" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge6" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server"
                                                                    TargetControlID="txtRunCharge6" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge7" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server"
                                                                    TargetControlID="txtRunCharge7" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge8" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server"
                                                                    TargetControlID="txtRunCharge8" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge9" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server"
                                                                    TargetControlID="txtRunCharge9" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRunCharge10" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender43" runat="server"
                                                                    TargetControlID="txtRunCharge10" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="12">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <table id="trImprintStiches" runat="server" visible="false">
                                                                <tr>
                                                                    <td>Stiches Upto
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtStichesUpto" />
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="fltStichesUpto" runat="server" TargetControlID="txtStichesUpto"
                                                                            FilterType="Numbers">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>No. of Stiches Location
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtNoStichesLocation" />
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="fltNoStichesLocation" runat="server" TargetControlID="txtNoStichesLocation"
                                                                            FilterType="Numbers">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Per X Stiches
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtPerStiches" />
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender60" runat="server"
                                                                            TargetControlID="txtPerStiches" FilterType="Numbers">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Per X Stiches Charges
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtPerStichesCharges" />
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender61" runat="server"
                                                                            TargetControlID="txtPerStichesCharges" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>Discount Code
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPerStichesChargesDiscountCode" runat="server" MaxLength="1" Width="30px"
                                                                            Style="text-transform: capitalize;"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender62" runat="server"
                                                                            TargetControlID="txtPerStichesChargesDiscountCode" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Per X Stiches Setup Charge
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtPerXStichesSetupCharge" />
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender63" runat="server"
                                                                            TargetControlID="txtPerXStichesSetupCharge" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                            ValidChars=".">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>Discount Code
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPerXStichesSetupChargeDiscountCode" runat="server" MaxLength="1"
                                                                            Width="30px" Style="text-transform: capitalize;"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender64" runat="server"
                                                                            TargetControlID="txtPerXStichesSetupChargeDiscountCode" FilterMode="ValidChars"
                                                                            FilterType="UppercaseLetters,LowercaseLetters">
                                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="drpImrpintMethod" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <div id="divImage" style="position: absolute; z-index: 1; left: 600px; top: 450px; display: none;">
                                                        <div id="hideshow">
                                                            <div id="fade">
                                                            </div>
                                                            <div class="popup_block">
                                                                <div class="popupLoader">
                                                                    <img id="Imgloader" src="../Images/ajax-loader.gif" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%--  <div id="divImage" style="display: none">
                                                        <asp:Image ID="img1" runat="server" ImageUrl="~/Admin/Images/UI/Loader.gif" />
                                                    </div>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Number of Imprint Location
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpImprintLocationCount" runat="server" OnSelectedIndexChanged="drpImprintLocationCount_OnSelectedIndexChanged"
                                                        AutoPostBack="true" Width="100">
                                                        <asp:ListItem Text="0" Value="1" Enabled="false"></asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                </td>
                                            </tr>
                                            <%-- <tr style="height: 15px;">
                                                <td colspan="9">
                                                    &nbsp;
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td colspan="9">
                                                    <table width="100%">
                                                        <tr style="height: 15px;">
                                                            <td colspan="9">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr id="trRepeaterHeader" runat="server" visible="false">
                                                            <td colspan="3">
                                                                <strong>Imprint locations</strong>
                                                            </td>
                                                        </tr>
                                                        <tr id="trRepeater" runat="server">
                                                            <td colspan="9">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Repeater ID="rptImprintLocation" runat="server">
                                                                            <ItemTemplate>
                                                                                <table width="50%" border="1">
                                                                                    <tr>
                                                                                        <td>Imprint Location Name
                                                                                            <asp:Literal ID="ltlSr" Text='<%# Container.ItemIndex + 1 %>' runat="server"></asp:Literal>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtImprintlocation" Text='<%# Eval("ImprintLocation") %>' runat="server"
                                                                                                Width="200px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="drpImprintLocationCount" EventName="SelectedIndexChanged" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                                <script type="text/javascript">
                                                                    // Get the instance of PageRequestManager.
                                                                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                                                                    // Add initializeRequest and endRequest
                                                                    prm.add_initializeRequest(prm_InitializeRequest);
                                                                    prm.add_endRequest(prm_EndRequest);

                                                                    // Called when async postback begins
                                                                    function prm_InitializeRequest(sender, args) {
                                                                        // get the divImage and set it to visible
                                                                        var panelProg = $get('divImage');
                                                                        panelProg.style.display = '';
                                                                        // reset label text


                                                                        // Disable button that caused a postback
                                                                        //  $get(args._postBackElement.id).disabled = true;
                                                                    }

                                                                    // Called when async postback ends
                                                                    function prm_EndRequest(sender, args) {
                                                                        // get the divImage and hide it again
                                                                        var panelProg = $get('divImage');
                                                                        panelProg.style.display = 'none';

                                                                        // Enable button that caused a postback
                                                                        //                                                                        //$get(sender._postBackSettings.sourceElement.id).disabled = false;
                                                                    }
                                                                </script>
                                                            </td>
                                                        </tr>
                                                        <tr id="trImprintColorCount" runat="server" style="height: 35px;" visible="true">
                                                            <td>Max imprint color count
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtMaxImprintColorCount" MaxLength="1" Style="width: 40px;" />
                                                                <ajaxToolkit:FilteredTextBoxExtender runat="server" ID="ftbImptrintColorCount" TargetControlID="txtMaxImprintColorCount"
                                                                    FilterType="Numbers">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr id="trImprintColors" runat="server" style="margin-top: 10px;" visible="true">
                                                            <td>Imprint Colors
                                                            </td>
                                                            <td>
                                                                <div style="width: 300px; height: 300px; overflow-y: scroll;" class="colorvarientsClick">
                                                                    <asp:CheckBoxList runat="server" ID="lstColors" SelectionMode="Multiple" Width="230px"
                                                                        Height="200px">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                        </table>
                                        <table width="100%" class="pricing_detail2">
                                            <tr>
                                                <td>
                                                    <div class="bottom_part leftcls postionscls">
                                                        <ul>

                                                            <li>
                                                                <asp:Button ID="btnSaveImprint" CssClass="exit save" runat="server" Text="Save" OnClick="btnSaveProductCharges_Click" />
                                                            </li>
                                                            <li>
                                                                <asp:Button ID="btnSaveImprintContinue" CssClass="exit save" runat="server" Text="Save and Continue"
                                                                    CommandName="gotonextpanel" OnClick="btnSaveProductCharges_Click" />
                                                            </li>
                                                            <li>
                                                                <asp:Button ID="btnImprintCancel" CssClass="exit cancelM" runat="server" OnClick="btnCancelProdInfo_Click"
                                                                    Text="Cancel" /></li>
                                                        </ul>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr style="height: 25px;">
                                                <td colspan="4">
                                                    <div class="uploadhis">
                                                        <asp:GridView ID="gvImprint" class="all_customer_inner edit12" runat="server" TabIndex="0"
                                                            CellPadding="2" Width="100%" AutoGenerateColumns="False" BorderColor="#E4E4E4"
                                                            BorderWidth="0" AllowPaging="true" PageSize="15" DataKeyNames="ImprintMethodChargeId"
                                                            OnRowCommand="gvImprint_OnRowCommand">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Imprint Method">
                                                                    <ItemTemplate>
                                                                        <%#DataBinder.Eval(Container.DataItem, "ImprintMethodName")%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Edit">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton CommandArgument='<%#Eval("ImprintMethodChargeId") %>' CommandName="Edit_Imprint"
                                                                            ID="imgbtnEdit" ImageUrl="~/Admin/Images/UI/edit_icn.png" runat="server" Style="float: none !important; background: none !important; border: none !important; margin: 0 !important; padding: 0 !important;" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="50">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton CommandArgument='<%#Eval("ImprintMethodChargeId") %>' CommandName="Delete_Imprint"
                                                                            ID="imgbtnDelete" ImageUrl="~/Admin/Images/UI/delete_icn.png" runat="server" Style="float: none !important; background: none !important; border: none !important; margin: 0 !important; padding: 0 !important;"
                                                                            OnClientClick="javascript:return confirm('Are you sure you want to delete imprint method?')" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="CsA">
                                        <h3>Apparel</h3>
                                        <table width="100%" class="pricing_detail2">
                                            <tr>
                                                <td colspan="12">
                                                    <table>
                                                        <tr>
                                                            <td width="100px;">Apparel Sizes
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAppearlSize1" runat="server" Width="45px" CssClass="box" Style="text-transform: uppercase;"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender44" runat="server"
                                                                    TargetControlID="txtAppearlSize1" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAppearlSize2" runat="server" Width="45px" CssClass="box" Style="text-transform: uppercase;"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender45" runat="server"
                                                                    TargetControlID="txtAppearlSize2" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAppearlSize3" runat="server" Width="45px" CssClass="box" Style="text-transform: uppercase;"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender46" runat="server"
                                                                    TargetControlID="txtAppearlSize3" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAppearlSize4" runat="server" Width="45px" CssClass="box" Style="text-transform: uppercase;"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender47" runat="server"
                                                                    TargetControlID="txtAppearlSize4" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAppearlSize5" runat="server" Width="45px" CssClass="box" Style="text-transform: uppercase;"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender48" runat="server"
                                                                    TargetControlID="txtAppearlSize5" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAppearlSize6" runat="server" Width="45px" CssClass="box" Style="text-transform: uppercase;"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender49" runat="server"
                                                                    TargetControlID="txtAppearlSize7" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAppearlSize7" runat="server" Width="45px" CssClass="box" Style="text-transform: uppercase;"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender50" runat="server"
                                                                    TargetControlID="txtAppearlSize7" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAppearlSize8" runat="server" Width="45px" CssClass="box" Style="text-transform: uppercase;"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender51" runat="server"
                                                                    TargetControlID="txtAppearlSize8" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100px;">UpCharges
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtUpCharge1" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender52" runat="server"
                                                                    TargetControlID="txtUpCharge1" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtUpCharge2" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender53" runat="server"
                                                                    TargetControlID="txtUpCharge2" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtUpCharge3" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender54" runat="server"
                                                                    TargetControlID="txtUpCharge3" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtUpCharge4" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender55" runat="server"
                                                                    TargetControlID="txtUpCharge4" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtUpCharge5" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender56" runat="server"
                                                                    TargetControlID="txtUpCharge5" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtUpCharge6" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender57" runat="server"
                                                                    TargetControlID="txtUpCharge6" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtUpCharge7" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender58" runat="server"
                                                                    TargetControlID="txtUpCharge7" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtUpCharge8" runat="server" Width="45px" CssClass="box"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender59" runat="server"
                                                                    TargetControlID="txtUpCharge8" FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                    ValidChars=".">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="pricing_detail2">
                                            <tr>
                                                <td>
                                                    <div class="bottom_part leftcls postionscls">
                                                        <ul>

                                                            <li style="margin-left: 0px;">
                                                                <asp:Button ID="btnSaveApparel" CssClass="exit save" runat="server" Text="Save" OnClick="btnSaveProductCharges_Click" />
                                                            </li>
                                                            <li>
                                                                <asp:Button ID="btnSaveApparelContinue" CssClass="exit save" runat="server" Text="Save and Continue"
                                                                    CommandName="gotonextpanel" OnClick="btnSaveProductCharges_Click" />
                                                            </li>

                                                            <li>
                                                                <asp:Button ID="btnApparelCancel" CssClass="exit cancelM" runat="server" OnClick="btnCancelProdInfo_Click"
                                                                    Text="Cancel" /></li>


                                                        </ul>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="CsA">
                                        <h3>Display on detail</h3>
                                        <table width="100%" class="pricing_detail2">
                                            <tr>
                                                <td style="vertical-align: middle; width: 640px;">
                                                    <%--<asp:Panel runat="server" ID="ltrCharges">
                                                            </asp:Panel>--%>
                                                    <asp:Label ID="lblNoRecords" Visible="false" Style="color: Red;" runat="server" />
                                                    <asp:Repeater runat="server" ID="rptCharges">
                                                        <HeaderTemplate>
                                                            <table>
                                                                <tr class="trbgclor">
                                                                    <th>Charges
                                                                    </th>
                                                                    <th>Details
                                                                    </th>
                                                                    <th>Order
                                                                    </th>
                                                                    <th></th>
                                                                </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblChargesTitle" runat="server" Text='<%# Eval("DetailTitle") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Width="400" ID="txtValue" rel='<%# Eval("PDGMappingId") %>' runat="server"
                                                                        Text='<%# Eval("DetailDescription") %>'></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Width="30" ID="txtDisplayOrder" runat="server" Text='<%# Eval("DisplayOrder") %>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>
                                                <td style="padding-bottom: 26px; vertical-align: bottom;">
                                                    <div>
                                                        <%--  <asp:UpdatePanel runat="server" ID="UpdatePanel222" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                                        <span class="flotcls">
                                                            <asp:LinkButton Text="Add" ID="lnkbtnAddCharge" CssClass="AddNewGroupField" OnClick="lnkbtnAddCharge_OnClientClick"
                                                                runat="server" />
                                                        </span>
                                                        <span class="flotcls">
                                                            <asp:LinkButton Text="New" ID="lnkbtnNewCharge" CssClass="AddNewField" OnClick="lnkbtnNewCharge_OnClientClick"
                                                                runat="server" /></span>
                                                        <%-- </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="lnkbtnCancelAddCharges_lnkbtnSaveAddCharges"
                                                    EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="10">
                                                    <%--   <asp:UpdatePanel runat="server" ID="UpdatePanelAdd" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                                    <table id="trAddCharge" class="trAddCharge" runat="server" visible="false">
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="drpProductCharges" Width="150">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>:
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:TextBox runat="server" ID="txtChargesValue" Width="417px" />
                                                                <ajaxToolkit:TextBoxWatermarkExtender ID="wmeChargeValue" runat="server" TargetControlID="txtChargesValue"
                                                                    WatermarkText="Value">
                                                                </ajaxToolkit:TextBoxWatermarkExtender>
                                                                <br />
                                                                <asp:Label ID="lblerror" Style="color: Red;" runat="server" Visible="false" />
                                                            </td>
                                                            <td>&nbsp;<asp:LinkButton Text="Save" class="exit save" ID="lnkbtnSaveAddCharges" runat="server"
                                                                OnClick="lnkbtnSaveAddCharges_lnkbtnSaveAddCharges" />
                                                                &nbsp;&nbsp;<asp:LinkButton class="exit cancelM" Text="Cancel" ID="lnkbtnCancelAddCharges"
                                                                    runat="server" OnClick="lnkbtnCancelAddCharges_lnkbtnSaveAddCharges" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%-- </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="lnkbtnAddCharge" EventName="Click" />
                                                <%-- <asp:AsyncPostBackTrigger ControlID="lnkbtnNewCharge" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="10">
                                                    <%-- <asp:UpdatePanel ID="UpdatePanelNew" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                                    <table id="trNewCharge" runat="server" visible="false">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtNewChargeTitle" Width="220px" />
                                                                <ajaxToolkit:TextBoxWatermarkExtender runat="server" ID="wmtChargesTitle" TargetControlID="txtNewChargeTitle"
                                                                    WatermarkText="Title">
                                                                </ajaxToolkit:TextBoxWatermarkExtender>
                                                            </td>
                                                            <td>:
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:TextBox runat="server" ID="txtNewChargeValue" Width="345px" />
                                                                <ajaxToolkit:TextBoxWatermarkExtender runat="server" ID="wmtChargesValue" TargetControlID="txtNewChargeValue"
                                                                    WatermarkText="Value">
                                                                </ajaxToolkit:TextBoxWatermarkExtender>
                                                            </td>
                                                            <td>
                                                                <td>&nbsp;<asp:LinkButton Text="Save" ID="lnkbtnSaveNewCharges" class="exit save" runat="server"
                                                                    OnClick="lnkbtnSaveNewCharges_lnkbtnSaveAddCharges" />
                                                                    &nbsp;&nbsp;<asp:LinkButton Text="Cancel" ID="lnkbtnCancelNewCharges" class="exit cancelM"
                                                                        runat="server" OnClick="lnkbtnCancelAddCharges_lnkbtnSaveAddCharges" />
                                                                </td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%-- </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="lnkbtnAddCharge" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="lnkbtnNewCharge" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="pricing_detail2">
                                            <tr>
                                                <td>
                                                    <div class="bottom_part news">
                                                        <ul>

                                                            <li>
                                                                <asp:Button ID="btnSaveProductCharges" CssClass="exit save" runat="server" Text="Save"
                                                                    OnClick="btnSaveProductCharges_Click" />
                                                            </li>
                                                            <li>
                                                                <asp:Button ID="btnSaveContinueProductCharges" CssClass="exit save" runat="server"
                                                                    Text="Save and Continue" CommandName="gotonextpanel" OnClick="btnSaveProductCharges_Click" />
                                                            </li>
                                                            <li style="margin-left: 0px;">
                                                                <asp:Button ID="btnCancelCharges" CssClass="exit cancelM" runat="server" OnClick="btnCancelProdInfo_Click"
                                                                    Text="Cancel" /></li>
                                                        </ul>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <%-- <table width="100%" class="pricing_detail2">
                            <tr>
                                <td>
                                    <div class="bottom_part my">
                                        <ul>
                                            <li>
                                                <asp:Button ID="btnCancelCharges" CssClass="exit cancelM" runat="server" OnClick="btnCancelProdInfo_Click"
                                                    Text="Cancel" /></li></li>
                                            <li>
                                                <asp:Button ID="btnSaveProductCharges" CssClass="exit save" runat="server" Text="Save"
                                                    OnClick="btnSaveProductCharges_Click" />
                                            </li>
                                            <li>
                                                <asp:Button ID="btnSaveContinueProductCharges" CssClass="exit save" runat="server"
                                                    Text="Save and Continue" CommandName="gotonextpanel" OnClick="btnSaveProductCharges_Click" />
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </table>--%>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tab_pan_ProductImage" runat="server">
                                <HeaderTemplate>
                                    Images
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="CsA">
                                        <h3>Images
                                        </h3>
                                        <div class="DetailsB" style="padding-left: 0px;">
                                            <div class="CheckB">
                                                <table border="0" class="pricing_detail1" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>Images Resize
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hdnImageName" runat="server" />
                                                            <asp:RadioButton Text="Automatically" ID="rbtnAuto" runat="server" Checked="true"
                                                                GroupName="ImageResize" />&nbsp;&nbsp;
                                                <asp:RadioButton ID="rbtnManually" Text="Manually" runat="server" GroupName="ImageResize" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <%--<asp:Literal ID="ltrVariantTypeLabel" Text="Add Variant Type" runat="server" />--%>
                                                Select Image Type
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="drpVariantType">
                                                            </asp:DropDownList>
                                                            <br />
                                                            <br />
                                                            <%-- <ajaxToolkit:FilteredTextBoxExtender ID="fltVairantType" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters"
                                                    TargetControlID="txtVairantType">
                                                </ajaxToolkit:FilteredTextBoxExtender>--%>
                                                            <asp:TextBox runat="server" ID="txtVairantType" Width="300px" MaxLength="50" />
                                                        </td>
                                                    </tr>
                                                    <tr class="trShowVariant">
                                                        <td>Show Image
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkShowVairant" runat="server" Checked="true" Enabled="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <h3>Upload Image(s)
                                        </h3>
                                        <table class="pricing_detail1" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>Alt Text
                                        <span class="errorText">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtVairantName" MaxLength="30" Width="250px" /><br />
                                                    <br />
                                                    <span id="spn" class="errorText" runat="server">Short Description</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>

                                        </table>
                                        <table class="pricing_detail1" border="0" cellpadding="0" cellspacing="0" id="tableAutoResize" runat="server">
                                            <tr>
                                                <td>Large Image (1000 X 1000) <span style="color: Red;">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="flupHiResAuto" runat="server" /><br />
                                                    <span id="spnHiResInfoImage" class="errorText" runat="server"></span>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="pricing_detail1" border="0" cellpadding="0" cellspacing="0" id="tableManually" style="display: none;"
                                            runat="server">
                                            <%-- <tr>
                                                <td>Hi-Res Image<span style="color: Red;">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="flupHiResManully" runat="server" /><br />
                                                    <span id="spnHiResInfoImageManully" class="errorText" runat="server"></span>
                                                    <asp:LinkButton Text="Delete Image" ID="lnkbtnDeleteHiRes" runat="server" Visible="false"
                                                        CommandName="HiRes" OnClick="DeleteImage_OnCommand" OnClientClick="javascript:return confirm('Are you sure, you want to delete Hi-Res image');" />
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td>Large Image<span style="color: Red;">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="fldLargeImage" runat="server" /><br />
                                                    <span id="spnLargeInfo" class="errorText" runat="server" />
                                                    <asp:LinkButton Text="Delete Image" ID="lnkbtnDeleteLarge" runat="server" CommandName="Large"
                                                        Visible="false" OnClick="DeleteImage_OnCommand" OnClientClick="javascript:return confirm('Are you sure, you want to delete Large image');" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Medium Image<span style="color: Red;">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="fldMediumImage" runat="server" /><br />
                                                    <span id="spnMediumInfo" class="errorText" runat="server" />
                                                    <asp:LinkButton Text="Delete Image" ID="lnkbtnDeleteMedium" runat="server" CommandName="Medium"
                                                        Visible="false" OnClick="DeleteImage_OnCommand" OnClientClick="javascript:return confirm('Are you sure, you want to delete Medium image');" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Thumbnail Image<span style="color: Red;">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="fldTumbImage" runat="server" /><br />
                                                    <span id="spnThumnailInfo" class="errorText" runat="server" />
                                                    <asp:LinkButton Text="Delete Image" ID="lnkbtnDeleteThumb" runat="server" CommandName="Thumbnail"
                                                        Visible="false" OnClick="DeleteImage_OnCommand" OnClientClick="javascript:return confirm('Are you sure, you want to delete Thumbnail image');" />
                                                </td>
                                            </tr>
                                        </table>

                                        <div class="button_section">
                                            <asp:Button Text="+ Save and Add More" CssClass="save" runat="server" ID="btnSaveAddMoreImage"
                                                OnClick="btnSaveAddMoreImage_OnClick" />
                                            <asp:Button Text="Cancel" CssClass="cancelM btn gray" runat="server" ID="btnAddNewImage" OnClick="btnAddNewImage_OnClick" />
                                        </div>


                                        <asp:Repeater runat="server" ID="rptVariantsType">
                                            <ItemTemplate>
                                                <h3>Image Types  </h3>
                                                <div class="DetailsB" style="padding-left: 0px;">
                                                    <div id="dRename" runat="server" rel='<%#  Eval("ProductVariantTypeId") %>' onclick="ShowRename(this);">
                                                        <asp:Label ID="lblVairantType" runat="server" Text='<%# Eval("ProductVariantTypeName") %>' />
                                                        <span class="editbtn"><a id="aRename" runat="server" rel='<%#  Eval("ProductVariantTypeId") %>'>Edit</a></span>
                                                    </div>
                                                    <div id="dRenameTextbox" runat="server" style="display: none;">
                                                        <asp:TextBox ID="txtRenameVariantName" MaxLength="200" Text='<%# Eval("ProductVariantTypeName") %>'
                                                            runat="server"></asp:TextBox>
                                                        <asp:Button Text="Update" CssClass="save" runat="server" ID="btnUpdateVariantName"
                                                            OnClientClick="return ValidateVariantTypeName(this);" OnClick="btnUpdateVariantTypeName_Click"
                                                            rel='<%#  Eval("ProductVariantTypeId") %>' />
                                                        <%--  <input type="button" value="Cancel"  class="save" />--%>
                                                        <asp:Button Text="Cancel" CssClass="save" runat="server" ID="btnCancelVariantName"
                                                            rel='<%#  Eval("ProductVariantTypeId") %>' OnClientClick=" return HideRename(this);" />
                                                    </div>
                                                    <asp:HiddenField ID="hdnVariantTypeId" Value='<%#  Eval("ProductVariantTypeId") %>'
                                                        runat="server" />


                                                </div>

                                                <div class="smalfontcls">
                                                    <asp:CheckBox ID="chkShowVariantVT" Visible="false" Checked='<%#  Eval("ShowVariant1") %>' runat="server"
                                                        Text="Show Variant" rel='<%#  Eval("ProductVariantTypeId") %>' OnCheckedChanged="chkShowVariantVT_OnCheckedChanged"
                                                        AutoPostBack="true" />
                                                    <asp:LinkButton ID="lnkbtnDeleteAll" Text="Delete All" CommandName="DeleteAllVariant"
                                                        OnClientClick="return confirm('Are you sure you want to delete all variants?');"
                                                        CommandArgument='<%#  Eval("ProductVariantTypeId") %>' runat="server" OnCommand="lnkbtnDeleteAll_OnClick"
                                                        CssClass="save" />
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <section class="all_customer edit_product" style="width: 100%;">
                                                    <asp:Repeater runat="server" ID="rptVariants">
                                                        <HeaderTemplate>
                                                            <table width="100%" border="1" class="all_customer_inner ">
                                                                <tr>
                                                                    <th class="bg">Name
                                                                    </th>
                                                                    <%--<th class="bg">Hi-Res
                                                        </th>--%>
                                                                    <th class="bg">Large
                                                                    </th>
                                                                    <th class="bg">Medium
                                                                    </th>
                                                                    <th class="bg">Thumbnail
                                                                    </th>
                                                                    <th class="bg">Default Image
                                                                    </th>
                                                                    <th class="bg">Action
                                                                    </th>
                                                                </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label Text='<%# Eval("ProductVariantName") %>' runat="server" ID="lblVariantName" />
                                                                    <asp:HiddenField ID="hdnProductVariantId" Value='<%# Eval("ProductVariantId") %>'
                                                                        runat="server" />
                                                                    <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                                </td>
                                                                <%--<td>
                                                        <asp:Image runat="server" ID="ImgHiRes" Width="50px" Height="50px" Style="cursor: pointer;" />
                                                        <asp:Label Text="N/A" runat="server" ID="lblHiRes" Visible="false" />
                                                    </td>--%>
                                                                <td>
                                                                    <asp:Image runat="server" ID="ImgLarge" Width="50px" Height="50px" Style="cursor: pointer;" />
                                                                    <asp:Label Text="N/A" runat="server" ID="lblLarge" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Image runat="server" ID="ImgMedium" Width="50px" Height="50px" Style="cursor: pointer;" />
                                                                    <asp:Label Text="N/A" runat="server" ID="lblMedium" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Image runat="server" ID="Imgthumbnail" Width="50px" Height="50px" Style="cursor: pointer;" />
                                                                    <asp:Label Text="N/A" runat="server" ID="lblThumb" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBox runat="server" OnCheckedChanged="chkDefault_OnCheckedChanged" AutoPostBack="true"
                                                                        rel='<%# Eval("ImageName") %>' class="chkDefault defaultvariant" ID="chkDefault" />
                                                                </td>
                                                                <td>
                                                                    <div class="action">
                                                                        <a class="icon" href="#"></a>
                                                                        <div class="action_hover">
                                                                            <span></span>
                                                                            <div class="other_option">
                                                                                <asp:LinkButton Text="Edit" OnCommand="lnkbtnEdit_OnClick" ID="lnkbtnEdit" CommandName="EditVariant"
                                                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductVariantId") %>'
                                                                                    runat="server" />
                                                                                <asp:LinkButton ID="lnkbtnDelete" Text="Delete" CommandName="DeleteVariant" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductVariantId") %>'
                                                                                    runat="server" OnCommand="lnkbtnDelete_OnClick" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </section>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tbl_pan_RelatedProducts" runat="server" Height="550px"
                                ScrollBars="Vertical">
                                <HeaderTemplate>
                                    Related Products
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <%--  <div class="CsA">--%>
                                    <div class="CsA">
                                        <h3>Manage Related Product
                                        </h3>
                                        <div class="DetailsB" style="padding-left: 0px;">
                                            <div class="CheckB">
                                                <asp:UpdatePanel runat="server" ID="updRelated">
                                                    <ContentTemplate>
                                                        <table width="100%" class="pricing_detail1">
                                                            <tr>
                                                                <td>Select Category
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlPParentCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPParentCategory_OnSelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="HIDAssignedProductIds" runat="server" />
                                                                    <asp:HiddenField ID="HIDUnAssignedProductIds" runat="server" />
                                                                    <asp:HiddenField ID="HIDSelectCatId" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr id="trPSCategory" runat="server" visible="false">
                                                                <td>Select Sub Category
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlPSubCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPSubCategory_OnSelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr id="trPSSCategory" runat="server" visible="false">
                                                                <td>Select Sub Category
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlPSubSubCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPSubSubCategory_OnSelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr id="trProduct">
                                                                <td></td>
                                                                <td colspan="3">
                                                                    <br />
                                                                    <br />
                                                                    <table class="space">
                                                                        <tr>
                                                                            <td>Unassigned Products
                                                                            </td>
                                                                            <td style="width: 50px"></td>
                                                                            <td>Assigned Products <font color="red">*</font>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div style="overflow: auto">
                                                                                    <asp:ListBox ID="lstUnAssignedProducts" Height="250px" Width="300px" runat="server"
                                                                                        SelectionMode="Multiple"></asp:ListBox>
                                                                                </div>
                                                                            </td>
                                                                            <td style="padding-left: 15px">
                                                                                <img runat="server" id="imgLeftToRight" src="~/Admin/Images/UI/front-arrow.gif" style="cursor: hand"
                                                                                    alt="Click to move selected item from left box to right box" />
                                                                                <br />
                                                                                <br />
                                                                                <br />
                                                                                <br />
                                                                                <img runat="server" id="imgRightToLeft" src="~/Admin/Images/UI/back-arrow.gif" style="cursor: hand"
                                                                                    alt="Click to move selected item from right box to left box" />
                                                                            </td>
                                                                            <td>
                                                                                <div style="overflow: auto">
                                                                                    <asp:ListBox ID="lstAssignedProducts" runat="server" Height="250px" Width="300px"
                                                                                        SelectionMode="Multiple"></asp:ListBox>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div class="button_section">

                                                    <asp:Button ID="btnSaveRelatedProducts" runat="server" CssClass="save" Text="Save"
                                                        OnClick="btnSaveRelated_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--</div>--%>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </div>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="true" DisplayMode="BulletList"
                        ToolTip="Following are incomplete fields of the form"
                        ValidationGroup="ValidDiscount" ForeColor="Red" ShowMessageBox="false" />
                </div>
            </div>
        </section>
    </div>
</asp:Content>
