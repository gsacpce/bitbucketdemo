﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Products_ProductAttributeManagement" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .attributeManagement select {
            width: 100%;
        }

        .attributeManagement input[type="text"] {
            width: 60px;
        }
        .pager span {
            color: #009900;
            font-weight: bold;
            font-size: 16pt;
        }
        .row.paginationBlk {
            border: 1px solid #ccc;
            margin: 10px 0;
            padding: 10px 0 5px;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        function ClearAttribute() {
            var checkedAtLeastOne = false;
            $('input[type="checkbox"]').each(function () {
                if ($(this).is(":checked")) {
                    checkedAtLeastOne = true;
                }
            });
            if (checkedAtLeastOne) {
                var r = confirm("Are you sure you want to Clear Selected Attribute?");
                if (r == true) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert('Atleast one the checkbox need to be selected to clear the attribute.');
                return false;
            }
        }

        function selectAll(invoker) {
            // Since ASP.NET checkboxes are really HTML input elements
            //  let's get all the inputs
            var inputElements = document.getElementsByTagName('input');
            for (var i = 0 ; i < inputElements.length ; i++) {
                var myElement = inputElements[i];
                // Filter through the input types looking for checkboxes
                if (myElement.type === "checkbox") {
                    // Use the invoker (our calling element) as the reference 
                    //  for our checkbox status
                    myElement.checked = invoker.checked;
                }
            }
        }
    </script>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Products </a></li>
            <li>Product Attribute Management</li>
        </ul>

        <%--<uc1:Section runat="server" ID="Section" />--%>

        <div class="customer cust_codediv ">
            <span class="allcosutomer">All Products -
                    <asp:Literal ID="ltrNoOfProducts" runat="server" Text=""></asp:Literal></span>
            <div class="clear">
            </div>
        </div>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                     
                    <div class="button_section text-center">
                        <asp:Button ID="btnSaveAttribute" CssClass="btn" type="submit" runat="server" Text="Save Attribute" OnClick="btnSaveAttribute_Click" />
                          <asp:Button ID="btnClearAttributetop" CssClass="btn" type="submit" runat="server" Text="Clear Attribute" 
                              OnClientClick="return ClearAttribute();"
                               OnClick="btnClearAttributetop_Click"/>
                    </div>
                    <div id="accordionx" class="search2 accordion">
                        <h3>Search Product</h3>
                        <div class="searchInnerbox">
                            <div class="box1 box1Extra" style="background: none !important;">
                                <asp:Panel runat="server" DefaultButton="btnSearch">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="input1" placeholder="Search"></asp:TextBox>
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                </asp:Panel>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <div class="button_section text-center"></div>
                    <div class="attributeManagement">
                        <asp:GridView ID="grdProductAttribute" class="all_customer_inner1 inner1newcls  dragbltop dragbltopextra" runat="server" PageSize="50"  AllowPaging="true" AllowCustomPaging="true"
                            TabIndex="0" CellPadding="2"  Width="100%" DataKeyNames="ProductSKUId,ValueA1,ValueA2,ValueB1,ValueB2"
                            AutoGenerateColumns="False" BorderColor="#E4E4E4" BorderWidth="0" OnRowDataBound="grdProductAttribute_RowDataBound" OnPageIndexChanging="grdProductAttribute_PageIndexChanging"
                            EmptyDataText="No Products exists." >
                            <Columns>
                                <asp:TemplateField HeaderText="Clear">
                                    <ItemTemplate>
                                      <asp:CheckBox ID="chkClear" runat="server" />
                                    </ItemTemplate>
                                     <HeaderTemplate>
                                              (Select All) <asp:CheckBox ID="chkAll" runat="Server" OnClick="selectAll(this)" ></asp:CheckBox>
                                            </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product Code">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdfProductSKUId" runat="server" Value='<%# Eval("ProductSKUId") %>' />
                                        <asp:Label ID="lblProductCode" runat="server" Text='<%# Eval("ProductCode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblProductDescription" runat="server" Text='<%# Eval("ProductDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Size">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlSize" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Element-A">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlElementA" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UOM-A1">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlUOMA1" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value-A1">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValueA1" runat="server" Text='<%# Eval("ValueA1") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UOM-A2">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlUOMA2" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value-A2">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValueA2" runat="server" Text='<%# Eval("ValueA2") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Element-B">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlElementB" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UOM-B1">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlUOMB1" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value-B1">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValueB1" runat="server" Text='<%# Eval("ValueB1") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UOM-B2">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlUOMB2" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value-B2">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValueB2" runat="server" Text='<%# Eval("ValueB2") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnISChanged" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <%--<PagerSettings Visible="false" Position="TopAndBottom" Mode="NextPreviousFirstLast" FirstPageText="<<" LastPageText=">>" NextPageText=">" PreviousPageText="<" />--%>
                            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom"/>
                            <PagerStyle CssClass="paging" />
                            <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                            <HeaderStyle CssClass="GridHeading" />
                            <AlternatingRowStyle CssClass="GridAlternateItem" />
                        </asp:GridView>

                    </div>
                    <div class="button_section text-center">
                        <asp:Button ID="btnSaveAttributebottom" CssClass="btn" type="submit" runat="server" Text="Save Attribute" OnClick="btnSaveAttribute_Click" />
                        <asp:Button ID="btnClearAttritbutebottom" CssClass="btn" type="submit" runat="server" Text="Clear Attribute" OnClick="btnClearAttritbutebottom_Click"/>
                    </div>
                </div>
            </div>

        </section>

    </div>
</asp:Content>

