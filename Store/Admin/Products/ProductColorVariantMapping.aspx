﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Products_ProductColorVariantMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../JS/Utilities.js" type="text/javascript"></script>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=GlobalFunctions.GetVirtualPathAdmin() %>dashboard/dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Product Color Variant</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container ">
                <div class="content">
                    <div>Product Color Variant</div>
                    <div>
                        Select Product : &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="ddlProducts" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProducts_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <br />
                    <div id="dvProductAssignment" runat="server" visible="false">
                        <asp:HiddenField ID="HIDAssignedProductIds" runat="server" />
                        <asp:HiddenField ID="HidProductColorName" runat="server" />
                        <asp:HiddenField ID="HIDUnAssignedProductIds" runat="server" />
                        <div>
                            Select Language : &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <br />
                        <table class="space">
                            <tr>
                                <td>Unassigned Products
                                </td>
                                <td style="width: 50px"></td>
                                <td>Assigned Products <font color="red">*</font>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="overflow: auto">
                                        <asp:ListBox ID="lstUnAssignedProducts" Height="250px" Width="300px" runat="server"
                                            SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </td>
                                <td style="padding-left: 15px">
                                    <img runat="server" id="imgLeftToRight" src="~/Admin/Images/UI/front-arrow.gif" style="cursor: hand"
                                        alt="Click to move selected item from left box to right box" />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <img runat="server" id="imgRightToLeft" src="~/Admin/Images/UI/back-arrow.gif" style="cursor: hand"
                                        alt="Click to move selected item from right box to left box" />
                                </td>
                                <td>
                                    <div style="overflow: auto">
                                        <asp:ListBox ID="lstAssignedProducts" runat="server" Height="250px" Width="300px"
                                            SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div>
                            Product Color Group Name :
                    <asp:TextBox ID="txtProductColorGroupName" placeholder="Enter Product Color Group Name" runat="server"></asp:TextBox>
                            <asp:Label runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvProductColorGroupName" runat="server" ControlToValidate="txtProductColorGroupName"
                                ErrorMessage="Please Enter Product Color Group Name" Text="Please Enter Product Color Group Name" Display="Dynamic" ValidationGroup="ValidateProductColorGroupName"></asp:RequiredFieldValidator>

                        </div>
                        <br />
                        <br />

                        <asp:Button ID="btnSaveRelatedProducts" runat="server" CssClass="save" Text="Save Group Name" ValidationGroup="ValidateProductColorGroupName"
                            OnClick="btnSaveRelated_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnDeleteGroupName" runat="server" Text="Delete Group Name" OnClick="btnDeleteGroupName_Click" OnClientClick="confirm('Are you sure want to delete Group Name');" CssClass="save"/>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnUpdateProductGroupName" runat="server" CssClass="save" Text="Update Product Group Name" ValidationGroup="ValidateProductColorGroupName"
                OnClick="btnUpdateProductGroupName_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

