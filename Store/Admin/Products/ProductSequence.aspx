﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_Products_ProductSequence" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.Radios input').click(function () {
                jQuery('.Radios').removeClass('chkBoxmark');
                jQuery(this).parent().addClass('chkBoxmark');
                return false;
            });

            $('.chkBox input').click(function () {
                jQuery(this).parent().toggleClass('chkBoxmark');
                return false;
            });

            $('.CsA h3').click(function () {
                $(this).next().stop().slideToggle();
                $(this).toggleClass('rightA');
            });

            $('#btnSaveExit').click(function () {
                var data = $('#sortable li');
                var id = "";
                data.each(function (index) {
                    id = id + this.attributes['rel'].value + "|";

                });
                $('#hdnSeq').val(id);
            });

            $('#Submit').click(function () {
                var data = $('#sortable li');
                var id = "";
                data.each(function (index) {
                    id = id + this.attributes['rel'].value + "|";
                });
                $('#hdnSeq').val(id);
            });
        });
    </script>
    <style>
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

            #sortable li {
                margin: 0 5px 5px 5px;
                padding: 5px;
                font-size: 1.2em;
                height: 1.5em;
                width: 100%;
                border-bottom:none;
            }

            html > body #sortable li {
                height: 1.5em;
                line-height: 1.2em;
            }

        .ui-state-highlight {
            height: 1.5em;
            line-height: 1.2em;
        }
        ul {
            padding:0px;
        }
       
    </style>
    <script>
        $(function () {
            $("#sortable").sortable({
                placeholder: "ui-state-highlight"
            });
            $("#sortable").disableSelection();
        });
    </script>
     <div class="container">
        <ul class="breadcrumb">
            <li><a href="<%=Host %>Admin/Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Products </a></li>
            <li> Products Sequence</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">

            <div class="wrap_container ">
                <div class="content">
                    <div id="div1" visible="false" runat="server">

                        <div>
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>
                                <asp:Literal ID="Literal1" runat="server" /></strong>
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </div>


                    </div>

                     <div class="wide">
                        <h3 class="mainHead"><asp:Literal ID="Literal2" runat="server" Text="Set Sequence for Products"></asp:Literal></h3>
                        <%--  <span class="sectionText"></span>--%>
                    </div>

                  <%--  <div class="col-md-12">
                        <h3>
                            <asp:Literal ID="ltrPageHeading" runat="server" Text="Set Sequence for Products"></asp:Literal>
                        </h3>
                    </div>--%>
                    <section class="mainContainer">
                        <div class="addsaTicP categories">
                            <div class="CsA">
                                <!-- according start -->
                                <div id="divMessage" visible="false" runat="server">
                                    <asp:Literal ID="ltrMessage" runat="server" />
                                </div>
                                <div class="DetailsB Editsection">
                                    <div class="sectionradio">
                                        <div class="webAddeditsection">
                                            <table class="selectcatogytble">
                                                <tr style="height: 30px" id="trPCategory" runat="server" visible="false">
                                                    <td align="left;" width="200px">
                                                        <span class="textspace">Select Category :</span>
                                                    </td>
                                                    <td align="left" width="400px" style="height: 30px">
                                                         <div class="select-style selectpicker">
                                                            <asp:DropDownList ID="ddlPParentCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPParentCategory_OnSelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </td>
                                                </tr>
                                                &nbsp;
                                                <tr style="height: 30px" id="trPSCategory" runat="server" visible="false">
                                                    <td align="left" width="200px">
                                                        <span class="textspace">Select Sub Category : </span>
                                                    </td>
                                                    <td align="left" style="height: 30px">
                                                         <div class="select-style selectpicker">
                                                            <asp:DropDownList ID="ddlPSubCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPSubCategory_OnSelectedIndexChanged">
                                                            </asp:DropDownList>
                                                          </div>
                                                    </td>
                                                </tr>
                                                <tr style="height: 30px" id="trPSSCategory" runat="server" visible="false">
                                                    <td align="left" width="400px">
                                                        <span class="textspace">Select Sub Sub Category :</span>
                                                    </td>
                                                    <td align="left" style="height: 30px">
                                                        <div class="select-style selectpicker">
                                                            <asp:DropDownList ID="ddlPSubSubCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPSubSubCategory_OnSelectedIndexChanged">
                                                            </asp:DropDownList>
                                                         </div>
                                                    </td>
                                                </tr>
                                                <tr id="trProduct" runat="server" visible="false">
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="box1 boxManageReviews" style="background-image: none !important;">
                                                                        <asp:Button ID="Submit" runat="server" Text="Save Sequence" OnClick="btnSubmit_Click"
                                                                            CssClass="exit save" ClientIDMode="Static" Visible="false" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <br />
                                                                    <br />
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:HiddenField ID="hdnSeq" ClientIDMode="Static" runat="server" Value="0" />
                                                                                <asp:HiddenField ID="hfRelatedCat" runat="server" />
                                                                                <asp:Repeater ID="rptPrdducts" runat="server"
                                                                                    OnItemDataBound="rptPrdducts_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <ul class="ui-sortable" id="sortable">
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <li id="liProducts" runat="server" class="ui-state-default ui-sortable-handle"></li>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </ul>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">&nbsp;&nbsp;&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="bottom_part my">
                            <ul>
                                <li>
                                    <asp:Button ID="btnSaveExit" runat="server" Text="Save Sequence" OnClick="btnSubmit_Click"
                                        CssClass="exit save" ClientIDMode="Static" Visible="false" />
                                </li>
                            </ul>
                        </div>
                    </section>
                    <!--categoriesmainBlock-->

                    <!-- homepage_slideshow end -->
                    <asp:HiddenField ID="HIDRelatedCateoryId" Value="0" runat="server" />
                </div>
            </div>
        </section>
    </div>

</asp:Content>
