﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Products_ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javaScript">
        function validate() {

            var Search = document.getElementById('<%= txtSearch.ClientID %>');

            if (Search.value == '') {
                alert("Please enter search product code/name");
                return false;
            }
            if (Search.value == 'Product Name / Product Code') {
                alert("Please enter search product code/ product name");
                return false;
            }

        }
    </script>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Products </a></li>
            <li>Products Management</li>
        </ul>
    </div>

    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container btn_padd_productlist ">



                <div class="content">
                    <section class="mainContainer padingbottom">
                        <h3 class="mainHead">Manage Product
                        </h3>
                        <div class="customers_top">
                            <div style="padding-bottom: 3px">
                                <ul class="customers_btn">
                                    <li>
                                        <asp:Button class="save" ID="btnAddNew" runat="server" Text="Add New Product" OnClick="btnAddNew_Click" />
                                    </li>
                                </ul>
                                <div class="clear">
                                </div>
                                <div id="accordionx" class="search2 accordion">
                                    <h3>Search Product(s)</h3>
                                    <div class="searchInnerbox">
                                        <div class="box1 box1Extra" style="background: none !important;">
                                            <asp:Panel runat="server" DefaultButton="btnSearch">
                                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Product Name / Product Code" CssClass="input1"></asp:TextBox>
                                                <asp:HiddenField ID="hdnproductname" runat="server" />
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                                                    OnClientClick="return validate();" />
                                                <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" CssClass="reset_new" />
                                            </asp:Panel>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="customer" style="margin: 0px !important;">
                                <span class="allcosutomer">All Products -
                    <asp:Literal ID="ltrNoOfProducts" runat="server" Text=""></asp:Literal></span>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                        <div class="wrap_prolist">
                            <section class="all_customer dragblnextBlock manage_pro">
                                <asp:GridView ID="grdProduct" runat="server" AutoGenerateColumns="False" Width="100%"
                                    AllowPaging="true" AllowCustomPaging="true" PageSize="50" EmptyDataText="No Product Found." CellPadding="2"
                                    BorderColor="#cccccc" BorderWidth="0" OnPageIndexChanging="grdProduct_PageIndexChanging"
                                    class="all_customer_inner allcutomerEtracls " OnRowCommand="grdProduct_OnRowCommand">
                                    <%--AllowSorting="true" OnSorting="grdProduct_OnSorting"--%>
                                    <Columns>
                                        <asp:BoundField DataField="ProductCode" HeaderText="Product Code" HeaderStyle-CssClass="bg"
                                            SortExpression="ProductCode" />
                                        <asp:BoundField DataField="ProductName" HeaderText="Product Name" HeaderStyle-CssClass="bg"
                                            SortExpression="ProductName" />
                                        <%--<asp:TemplateField HeaderText="Mapping" HeaderStyle-CssClass="bg">
                        <ItemTemplate>
                            <asp:HyperLink ID="hypProductId" Style="color: Blue; text-decoration: underline;"
                                runat="server" NavigateUrl='<%# Eval("ProductId", "ProductMapping.aspx?pid={0}&action=m") %>'>Mapping</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Relate" Visible="false">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="ProductId" runat="server" NavigateUrl='<%# Eval("ProductId", "ProductMapping.aspx?pid={0}&action=r") %>'>Relate </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CategoryName" HeaderText="Category Name" HeaderStyle-CssClass="bg"
                                            SortExpression="CategoryName" />
                                        <asp:TemplateField HeaderText="Is Active" HeaderStyle-CssClass="bg" HeaderStyle-Width="80px" ItemStyle-Width="80px">
                                            <ItemTemplate>
                                                <div>
                                                    <span>
                                                        <asp:CheckBox ID="chkIsActive" runat="server" rel='<%# DataBinder.Eval(Container.DataItem, "ProductId") %>'
                                                            OnCheckedChanged="chkIsActive_CheckedChanged" Checked='<%# Eval("IsActive") %>' onClick="ShowLoader()" AutoPostBack="true" />
                                                    </span><span class="spnwidt">Is Active</span>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="view_order">
                                                            <asp:LinkButton CommandArgument='<%#Eval("ProductId") %>' CommandName="Edit_Product"
                                                                ID="imgbtnEdit" runat="server" Text="Edit" />
                                                            <asp:LinkButton CommandArgument='<%#Eval("ProductId") %>' CommandName="Delete_Product"
                                                                ID="imgbtnDelete" runat="server" Text="Delete" Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>


                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmptyGridTemplate" runat="server" Text="No Data Found"></asp:Label>
                                    </EmptyDataTemplate>



                                    <PagerSettings Position="TopAndBottom" Mode="NextPreviousFirstLast" FirstPageText="<<" LastPageText=">>" NextPageText=">" PreviousPageText="<" />
                                    <PagerStyle CssClass="paging" />


                                </asp:GridView>


                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
</asp:Content>


